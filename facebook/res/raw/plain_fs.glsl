precision mediump float;
uniform sampler2D inputImage;
uniform sampler2D inputImageY;
uniform sampler2D inputImageUV;
#ifdef NEEDS_SURFACE_TRANSFORM
uniform mat4 uSurfaceTransformMatrix;
#endif
uniform bool inputIsYUV;

// This function is never used on android, but is here for source-level parity with ios
// It works on iOS but has not been tested on android.
vec3 yuvToRgb(vec3 yuv) {
  float y = yuv.r;
  float u = yuv.g;
  float v = yuv.b;
  vec3 rgb;
  rgb.r = 1.402 * v + y;
  rgb.g = (y - (0.299 * 1.402 / 0.587) * v - (0.114 * 1.772 / 0.587) * u);
  rgb.b = 1.772 * u + y;
  return rgb;
}

vec4 inputLookup(vec2 coords) {
#ifdef NEEDS_SURFACE_TRANSFORM
  vec2 adjustedCoords = (uSurfaceTransformMatrix * vec4(coords, 0.0, 1.0)).xy;
#else
  vec2 adjustedCoords = coords;
#endif
  if (inputIsYUV) {
    vec3 yuv;
    yuv.x = texture2D(inputImageY, adjustedCoords).r;
    yuv.yz = texture2D(inputImageUV, adjustedCoords).rg - vec2(0.5, 0.5);
    vec4 texel;
    texel.xyz = yuvToRgb(yuv);
    texel.w = 1.0;
    return texel;
  } else {
    return texture2D(inputImage, adjustedCoords);
  }
}

varying vec2 hdConformedUV;
varying vec2 uv;
uniform vec2 uRenderSize;
uniform float uTime;
#define GLSLIFY 1
void main() {
  vec4 color = inputLookup(uv);
  gl_FragColor = color;
}
