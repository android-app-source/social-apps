// Fragment Shader
precision mediump float;

uniform sampler2D uBirthAnimationTexture;
uniform sampler2D uDeathAnimationTexture;
uniform sampler2D uTexture;

// Input from Vertex Shader
varying mediump vec4 vDestinationColor;
varying float vTextureToUse;
varying vec2 vTextureCoord;

void main(void)
{
  if (vTextureToUse == 0.0) {
    gl_FragColor = texture2D(uTexture, vTextureCoord) * vDestinationColor;
  } else if (vTextureToUse == 1.0) {
    gl_FragColor = texture2D(uBirthAnimationTexture, vTextureCoord) * vDestinationColor;
  } else {
    gl_FragColor = texture2D(uDeathAnimationTexture, vTextureCoord) * vDestinationColor;
  }
}