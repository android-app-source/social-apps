// Vertex Shader

attribute highp vec4 aPosition;
attribute highp vec2 aTextureCoord;
attribute highp float aBirthtime;
attribute highp float aDeathtime;
attribute highp vec4 aColor;

// birth animation
uniform highp float uBirthAnimationNumberOfSprites;
uniform highp float uBirthAnimationPlays;
uniform highp float uBirthAnimationFramerate; // in millis
uniform highp vec2 uBirthAnimationSpriteSize;

// death animation
uniform highp float uDeathAnimationNumberOfSprites;
uniform highp float uDeathAnimationPlays;
uniform highp float uDeathAnimationFramerate; // in millis
uniform highp vec2 uDeathAnimationSpriteSize;

uniform highp float uTimestamp;

varying highp vec4 vDestinationColor;
varying vec2 vTextureCoord;
varying float vTextureToUse; // 0 = main texture, 1 = birth animation texture, 2 = death animation texture

bool isValidFrame(float frame, float sprites, float repeats)
{
  return frame >= 0.0 && frame < sprites * repeats;
}

float frame(float timestamp, float framerate)
{
  return floor(timestamp * framerate / 1000.0);
}

vec2 textureCoordForFrame(float frame, vec2 frameSize, vec2 textureCoord)
{
  float col = floor(frameSize.x * frame);
  float x = frameSize.x * (frame + textureCoord.x);
  float y = frameSize.y * (floor(frame * frameSize.x) + textureCoord.y);
  return vec2(x - col, y);
}

void main(void)
{
  float birthFrame = frame(uTimestamp - aBirthtime, uBirthAnimationFramerate);
  float deathFrame =
    (uDeathAnimationNumberOfSprites * uDeathAnimationPlays) -
    frame(aDeathtime - uTimestamp, uDeathAnimationFramerate) - 1.0;

  if (isValidFrame(birthFrame, uBirthAnimationNumberOfSprites, uBirthAnimationPlays)) {
    vTextureCoord = textureCoordForFrame(
      mod(birthFrame, uBirthAnimationNumberOfSprites),
      uBirthAnimationSpriteSize,
      aTextureCoord);
    vTextureToUse = 1.0;
  } else if (isValidFrame(deathFrame, uDeathAnimationNumberOfSprites, uDeathAnimationPlays)) {
    vTextureCoord = textureCoordForFrame(
      mod(deathFrame, uDeathAnimationNumberOfSprites),
      uDeathAnimationSpriteSize,
      aTextureCoord);
    vTextureToUse = 2.0;
  } else {
    vTextureCoord = aTextureCoord;
    vTextureToUse = 0.0;
  }

  vDestinationColor = aColor;
  gl_Position = aPosition;
}