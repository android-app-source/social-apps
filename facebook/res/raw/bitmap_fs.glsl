precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D sBitmap;

void main() {
  gl_FragColor = texture2D(sBitmap, vTextureCoord);
}
