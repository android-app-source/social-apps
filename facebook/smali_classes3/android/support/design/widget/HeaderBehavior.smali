.class public abstract Landroid/support/design/widget/HeaderBehavior;
.super Landroid/support/design/widget/ViewOffsetBehavior;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Landroid/support/design/widget/ViewOffsetBehavior",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Runnable;

.field public b:LX/1Og;

.field private c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 341200
    invoke-direct {p0}, Landroid/support/design/widget/ViewOffsetBehavior;-><init>()V

    .line 341201
    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341202
    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    .line 341203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 341196
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/ViewOffsetBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 341197
    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341198
    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    .line 341199
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 341193
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 341194
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    .line 341195
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 341192
    invoke-virtual {p0}, Landroid/support/design/widget/ViewOffsetBehavior;->getTopAndBottomOffset()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;I)I"
        }
    .end annotation

    .prologue
    .line 341104
    const/high16 v4, -0x80000000

    const v5, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/HeaderBehavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I

    move-result v0

    return v0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;III)I"
        }
    .end annotation

    .prologue
    .line 341184
    invoke-virtual {p0}, Landroid/support/design/widget/ViewOffsetBehavior;->getTopAndBottomOffset()I

    move-result v1

    .line 341185
    const/4 v0, 0x0

    .line 341186
    if-eqz p4, :cond_0

    if-lt v1, p4, :cond_0

    if-gt v1, p5, :cond_0

    .line 341187
    invoke-static {p3, p4, p5}, LX/3oE;->a(III)I

    move-result v2

    .line 341188
    if-eq v1, v2, :cond_0

    .line 341189
    invoke-virtual {p0, v2}, Landroid/support/design/widget/ViewOffsetBehavior;->setTopAndBottomOffset(I)Z

    .line 341190
    sub-int v0, v1, v2

    .line 341191
    :cond_0
    return v0
.end method

.method public a(Landroid/view/View;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation

    .prologue
    .line 341183
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;IIF)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;IIF)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 341172
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 341173
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->a:Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 341174
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->a:Ljava/lang/Runnable;

    .line 341175
    :cond_0
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    if-nez v0, :cond_1

    .line 341176
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1Og;->a(Landroid/content/Context;)LX/1Og;

    move-result-object v0

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    .line 341177
    :cond_1
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    invoke-virtual {p0}, Landroid/support/design/widget/ViewOffsetBehavior;->getTopAndBottomOffset()I

    move-result v2

    invoke-static {p5}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v3, v1

    move v5, v1

    move v6, v1

    move v7, p3

    move v8, p4

    invoke-virtual/range {v0 .. v8}, LX/1Og;->a(IIIIIIII)V

    .line 341178
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->b:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341179
    new-instance v0, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;

    invoke-direct {v0, p0, p1, p2}, Landroid/support/design/widget/HeaderBehavior$FlingRunnable;-><init>(Landroid/support/design/widget/HeaderBehavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->a:Ljava/lang/Runnable;

    .line 341180
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->a:Ljava/lang/Runnable;

    invoke-static {p2, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 341181
    const/4 v1, 0x1

    .line 341182
    :cond_2
    return v1
.end method

.method public final b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;III)I"
        }
    .end annotation

    .prologue
    .line 341171
    invoke-virtual {p0}, Landroid/support/design/widget/HeaderBehavior;->a()I

    move-result v0

    sub-int v3, v0, p3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/HeaderBehavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)I"
        }
    .end annotation

    .prologue
    .line 341170
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public c(Landroid/view/View;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 341169
    const/4 v0, 0x0

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 341139
    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    if-gez v1, :cond_0

    .line 341140
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    .line 341141
    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 341142
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    if-eqz v1, :cond_1

    .line 341143
    :goto_0
    return v0

    .line 341144
    :cond_1
    invoke-static {p3}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 341145
    :cond_2
    :goto_1
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    .line 341146
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 341147
    :cond_3
    iget-boolean v0, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    goto :goto_0

    .line 341148
    :pswitch_0
    iput-boolean v4, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    .line 341149
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 341150
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 341151
    invoke-virtual {p0, p2}, Landroid/support/design/widget/HeaderBehavior;->c(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341152
    iput v1, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    .line 341153
    invoke-static {p3, v4}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341154
    invoke-direct {p0}, Landroid/support/design/widget/HeaderBehavior;->b()V

    goto :goto_1

    .line 341155
    :pswitch_1
    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341156
    if-eq v1, v3, :cond_2

    .line 341157
    invoke-static {p3, v1}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 341158
    if-eq v1, v3, :cond_2

    .line 341159
    invoke-static {p3, v1}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    .line 341160
    iget v2, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 341161
    iget v3, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    if-le v2, v3, :cond_2

    .line 341162
    iput-boolean v0, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    .line 341163
    iput v1, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    goto :goto_1

    .line 341164
    :pswitch_2
    iput-boolean v4, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    .line 341165
    iput v3, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341166
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    .line 341167
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 341168
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "TV;",
            "Landroid/view/MotionEvent;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, -0x1

    const/4 v5, 0x0

    .line 341105
    iget v0, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    if-gez v0, :cond_0

    .line 341106
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    .line 341107
    :cond_0
    invoke-static {p3}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 341108
    :cond_1
    :goto_0
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    .line 341109
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_2
    move v5, v7

    .line 341110
    :cond_3
    return v5

    .line 341111
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 341112
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 341113
    invoke-virtual {p1, p2, v0, v1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p2}, Landroid/support/design/widget/HeaderBehavior;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341114
    iput v1, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    .line 341115
    invoke-static {p3, v5}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341116
    invoke-direct {p0}, Landroid/support/design/widget/HeaderBehavior;->b()V

    goto :goto_0

    .line 341117
    :pswitch_1
    iget v0, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    invoke-static {p3, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 341118
    if-eq v0, v8, :cond_3

    .line 341119
    invoke-static {p3, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    float-to-int v0, v0

    .line 341120
    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    sub-int v3, v1, v0

    .line 341121
    iget-boolean v1, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    if-nez v1, :cond_4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    if-le v1, v2, :cond_4

    .line 341122
    iput-boolean v7, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    .line 341123
    if-lez v3, :cond_5

    .line 341124
    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    sub-int/2addr v3, v1

    .line 341125
    :cond_4
    :goto_1
    iget-boolean v1, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    if-eqz v1, :cond_1

    .line 341126
    iput v0, p0, Landroid/support/design/widget/HeaderBehavior;->e:I

    .line 341127
    invoke-virtual {p0, p2}, Landroid/support/design/widget/HeaderBehavior;->b(Landroid/view/View;)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/HeaderBehavior;->b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)I

    goto :goto_0

    .line 341128
    :cond_5
    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->f:I

    add-int/2addr v3, v1

    goto :goto_1

    .line 341129
    :pswitch_2
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_6

    .line 341130
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 341131
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 341132
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    invoke-static {v0, v1}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v6

    .line 341133
    invoke-virtual {p0, p2}, Landroid/support/design/widget/HeaderBehavior;->a(Landroid/view/View;)I

    move-result v0

    neg-int v4, v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Landroid/support/design/widget/HeaderBehavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;IIF)Z

    .line 341134
    :cond_6
    :pswitch_3
    iput-boolean v5, p0, Landroid/support/design/widget/HeaderBehavior;->c:Z

    .line 341135
    iput v8, p0, Landroid/support/design/widget/HeaderBehavior;->d:I

    .line 341136
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 341137
    iget-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 341138
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/design/widget/HeaderBehavior;->g:Landroid/view/VelocityTracker;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
