.class public final LX/1vc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1vb;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1dl;

.field public b:I

.field public c:Z

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1SX;

.field public final synthetic f:LX/1vb;


# direct methods
.method public constructor <init>(LX/1vb;)V
    .locals 1

    .prologue
    .line 343290
    iput-object p1, p0, LX/1vc;->f:LX/1vb;

    .line 343291
    move-object v0, p1

    .line 343292
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 343293
    const v0, -0x6e685d

    iput v0, p0, LX/1vc;->b:I

    .line 343294
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1vc;->c:Z

    .line 343295
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343296
    const-string v0, "HeaderMenuComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 343297
    if-ne p0, p1, :cond_1

    .line 343298
    :cond_0
    :goto_0
    return v0

    .line 343299
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 343300
    goto :goto_0

    .line 343301
    :cond_3
    check-cast p1, LX/1vc;

    .line 343302
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 343303
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 343304
    if-eq v2, v3, :cond_0

    .line 343305
    iget-object v2, p0, LX/1vc;->a:LX/1dl;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1vc;->a:LX/1dl;

    iget-object v3, p1, LX/1vc;->a:LX/1dl;

    invoke-virtual {v2, v3}, LX/1dl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 343306
    goto :goto_0

    .line 343307
    :cond_5
    iget-object v2, p1, LX/1vc;->a:LX/1dl;

    if-nez v2, :cond_4

    .line 343308
    :cond_6
    iget v2, p0, LX/1vc;->b:I

    iget v3, p1, LX/1vc;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 343309
    goto :goto_0

    .line 343310
    :cond_7
    iget-boolean v2, p0, LX/1vc;->c:Z

    iget-boolean v3, p1, LX/1vc;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 343311
    goto :goto_0

    .line 343312
    :cond_8
    iget-object v2, p0, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 343313
    goto :goto_0

    .line 343314
    :cond_a
    iget-object v2, p1, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_9

    .line 343315
    :cond_b
    iget-object v2, p0, LX/1vc;->e:LX/1SX;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/1vc;->e:LX/1SX;

    iget-object v3, p1, LX/1vc;->e:LX/1SX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 343316
    goto :goto_0

    .line 343317
    :cond_c
    iget-object v2, p1, LX/1vc;->e:LX/1SX;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
