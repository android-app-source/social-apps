.class public final enum LX/1vY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1vY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1vY;

.field public static final enum ACTION_ICON:LX/1vY;

.field public static final enum ACTION_LINKS:LX/1vY;

.field public static final enum ACTOR_PHOTO:LX/1vY;

.field public static final enum ADD_COMMENT_BOX:LX/1vY;

.field public static final enum ADD_FRIEND_BUTTON:LX/1vY;

.field public static final enum ADD_PHOTO_ACTION:LX/1vY;

.field public static final enum ADS_SHIMMED_LINK:LX/1vY;

.field public static final enum AD_CREATIVE_BODY:LX/1vY;

.field public static final enum AD_CREATIVE_IMAGE:LX/1vY;

.field public static final enum AD_CREATIVE_TITLE:LX/1vY;

.field public static final enum AD_IDENTITY:LX/1vY;

.field public static final enum AD_LOGOUT:LX/1vY;

.field public static final enum AD_SOCIAL_SENTENCE:LX/1vY;

.field public static final enum ALBUM:LX/1vY;

.field public static final enum ALBUM_COLLAGE:LX/1vY;

.field public static final enum APP_CALL_TO_ACTION:LX/1vY;

.field public static final enum APP_NAME:LX/1vY;

.field public static final enum ATTACHMENT:LX/1vY;

.field public static final enum AVATAR_LIST:LX/1vY;

.field public static final enum BAD_AGGREGATION_LINK:LX/1vY;

.field public static final enum BLINGBOX:LX/1vY;

.field public static final enum COLLECTIONS_RATING:LX/1vY;

.field public static final enum COLLECTION_ACTION_LINK:LX/1vY;

.field public static final enum COLLECTION_ADD_BUTTON:LX/1vY;

.field public static final enum COLLECTION_ROBOTEXT_LINK:LX/1vY;

.field public static final enum COLLECTION_TICKER_LINK:LX/1vY;

.field public static final enum COMMENT:LX/1vY;

.field public static final enum COMMENT_LINK:LX/1vY;

.field public static final enum DESCRIPTION:LX/1vY;

.field public static final enum DIGITAL_GOOD:LX/1vY;

.field public static final enum DROPDOWN_BUTTON:LX/1vY;

.field public static final enum EGO_FEED_UNIT:LX/1vY;

.field public static final enum EVENT_INVITE_FRIENDS:LX/1vY;

.field public static final enum EVENT_INVITE_SENT:LX/1vY;

.field public static final enum EVENT_RSVP:LX/1vY;

.field public static final enum EVENT_VIEW:LX/1vY;

.field public static final enum EXP_CALL_TO_ACTION:LX/1vY;

.field public static final enum FAN_PAGE:LX/1vY;

.field public static final enum FEEDBACK_SECTION:LX/1vY;

.field public static final enum FEED_STORY_MESSAGE_FLYOUT:LX/1vY;

.field public static final enum GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

.field public static final enum GIFT_LINK:LX/1vY;

.field public static final enum GROUP_JOIN:LX/1vY;

.field public static final enum HASHTAG:LX/1vY;

.field public static final enum HEADER:LX/1vY;

.field public static final enum HEADLINE:LX/1vY;

.field public static final enum HIDE_ALL_LINK:LX/1vY;

.field public static final enum HIDE_LINK:LX/1vY;

.field public static final enum INSTALL_ACTION:LX/1vY;

.field public static final enum LARGE_MEDIA_ATTACHMENT:LX/1vY;

.field public static final enum LIKE_COUNT:LX/1vY;

.field public static final enum LIKE_LINK:LX/1vY;

.field public static final enum MEDIA_CONTROLS:LX/1vY;

.field public static final enum MEDIA_GENERIC:LX/1vY;

.field public static final enum MUSIC:LX/1vY;

.field public static final enum NAME_LIST:LX/1vY;

.field public static final enum NATIVE_NAME:LX/1vY;

.field public static final enum NEARBY_FRIENDS_LIST:LX/1vY;

.field public static final enum NEKO_PREVIEW:LX/1vY;

.field public static final enum NOTE:LX/1vY;

.field public static final enum OFFERS_CLAIM:LX/1vY;

.field public static final enum OFFERS_CLICK:LX/1vY;

.field public static final enum OFFERS_RESEND:LX/1vY;

.field public static final enum OFFERS_USE_NOW:LX/1vY;

.field public static final enum OG_COMPOSER_OBJECT:LX/1vY;

.field public static final enum OG_LEFT_SLIDE_PAGER:LX/1vY;

.field public static final enum OG_RIGHT_SLIDE_PAGER:LX/1vY;

.field public static final enum OTHER:LX/1vY;

.field public static final enum PAGE_COVER_PHOTO:LX/1vY;

.field public static final enum PAGE_LINK:LX/1vY;

.field public static final enum PAGE_PROFILE_PIC:LX/1vY;

.field public static final enum PARTICIPANT:LX/1vY;

.field public static final enum PERMALINK_COMMENT:LX/1vY;

.field public static final enum PHOTO:LX/1vY;

.field public static final enum PHOTO_GALLERY:LX/1vY;

.field public static final enum PLACE_MORE_INFO_ACTION:LX/1vY;

.field public static final enum PLACE_STAR_SURVEY:LX/1vY;

.field public static final enum PLACE_WRITE_REVIEW_ACTION:LX/1vY;

.field public static final enum PRESENCE_UNIT:LX/1vY;

.field public static final enum PRONOUN:LX/1vY;

.field public static final enum RATING:LX/1vY;

.field public static final enum RAW_NAME:LX/1vY;

.field public static final enum RELATED_SHARE_ARTICLE:LX/1vY;

.field public static final enum RELATED_SHARE_ARTICLE_HIDE:LX/1vY;

.field public static final enum RELATED_SHARE_VIDEO:LX/1vY;

.field public static final enum RELATED_SHARE_VIDEO_HIDE:LX/1vY;

.field public static final enum REPORT_SPAM_LINK:LX/1vY;

.field public static final enum RETRY_BUTTON:LX/1vY;

.field public static final enum REVIEW_MENU:LX/1vY;

.field public static final enum RHC_AD:LX/1vY;

.field public static final enum RHC_CARD:LX/1vY;

.field public static final enum RHC_CARD_HIDE:LX/1vY;

.field public static final enum RHC_COMMENT_BUTTON:LX/1vY;

.field public static final enum RHC_LINK_OPEN:LX/1vY;

.field public static final enum RHC_PHOTO_SLIDER:LX/1vY;

.field public static final enum RHC_SIMPLIFICATION:LX/1vY;

.field public static final enum RHC_SIMPLIFICATION_HIDE:LX/1vY;

.field public static final enum ROBOTEXT:LX/1vY;

.field public static final enum SAVED_COLLECTION:LX/1vY;

.field public static final enum SAVE_ACTION:LX/1vY;

.field public static final enum SAVE_BUTTON:LX/1vY;

.field public static final enum SEE_MORE:LX/1vY;

.field public static final enum SEND:LX/1vY;

.field public static final enum SHARE_LINK:LX/1vY;

.field public static final enum SMALL_ACTOR_PHOTO:LX/1vY;

.field public static final enum SNOWLIFT:LX/1vY;

.field public static final enum SNOWLIFT_MESSAGE:LX/1vY;

.field public static final enum SOCIAL_ACTOR_PHOTO:LX/1vY;

.field public static final enum SOCIAL_CONTEXT:LX/1vY;

.field public static final enum SOURCE:LX/1vY;

.field public static final enum SPONSORED_CONTEXT:LX/1vY;

.field public static final enum SPONSORED_LINK:LX/1vY;

.field public static final enum STORY_FOOTER:LX/1vY;

.field public static final enum STORY_LIST:LX/1vY;

.field public static final enum STORY_LOCATION:LX/1vY;

.field public static final enum SUBSTORY:LX/1vY;

.field public static final enum SUBTITLE:LX/1vY;

.field public static final enum SUB_ATTACHMENT:LX/1vY;

.field public static final enum TIMELINE_GIFTS:LX/1vY;

.field public static final enum TITLE:LX/1vY;

.field public static final enum TOPIC_PIVOT_HEADER:LX/1vY;

.field public static final enum TRENDING_TOPIC_LINK:LX/1vY;

.field public static final enum UFI:LX/1vY;

.field public static final enum UNFAN_PAGE:LX/1vY;

.field public static final enum UNHIDE_LINK:LX/1vY;

.field public static final enum UNLIKE_LINK:LX/1vY;

.field public static final enum USER_MESSAGE:LX/1vY;

.field public static final enum USER_NAME:LX/1vY;

.field public static final enum VIDEO:LX/1vY;

.field public static final enum VIEW_ALL_COMMENTS:LX/1vY;

.field public static final enum XBUTTON:LX/1vY;

.field public static final enum ZERO_UPSELL_BUY:LX/1vY;

.field public static final enum ZERO_UPSELL_FEED_UNIT:LX/1vY;


# instance fields
.field private mDestination:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 342982
    new-instance v0, LX/1vY;

    const-string v1, "HEADLINE"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "headline"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->HEADLINE:LX/1vY;

    .line 342983
    new-instance v0, LX/1vY;

    const-string v1, "USER_NAME"

    const/4 v2, 0x1

    const/4 v3, 0x2

    const-string v4, "user_name"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->USER_NAME:LX/1vY;

    .line 342984
    new-instance v0, LX/1vY;

    const-string v1, "ACTOR_PHOTO"

    const/4 v2, 0x2

    const/4 v3, 0x3

    const-string v4, "actor_photo"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ACTOR_PHOTO:LX/1vY;

    .line 342985
    new-instance v0, LX/1vY;

    const-string v1, "ACTION_LINKS"

    const/4 v2, 0x3

    const/4 v3, 0x4

    const-string v4, "action_links"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ACTION_LINKS:LX/1vY;

    .line 342986
    new-instance v0, LX/1vY;

    const-string v1, "LIKE_LINK"

    const/4 v2, 0x4

    const/4 v3, 0x5

    const-string v4, "like_link"

    const-string v5, "action_like"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->LIKE_LINK:LX/1vY;

    .line 342987
    new-instance v0, LX/1vY;

    const-string v1, "UNLIKE_LINK"

    const/4 v2, 0x5

    const/4 v3, 0x6

    const-string v4, "unlike_link"

    const-string v5, "action_unlike"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->UNLIKE_LINK:LX/1vY;

    .line 342988
    new-instance v0, LX/1vY;

    const-string v1, "PARTICIPANT"

    const/4 v2, 0x6

    const/4 v3, 0x7

    const-string v4, "participant"

    const-string v5, "unknown"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PARTICIPANT:LX/1vY;

    .line 342989
    new-instance v0, LX/1vY;

    const-string v1, "PRONOUN"

    const/4 v2, 0x7

    const/16 v3, 0x8

    const-string v4, "pronoun"

    const-string v5, "unknown"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PRONOUN:LX/1vY;

    .line 342990
    new-instance v0, LX/1vY;

    const-string v1, "ROBOTEXT"

    const/16 v2, 0x8

    const/16 v3, 0x9

    const-string v4, "robotext"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ROBOTEXT:LX/1vY;

    .line 342991
    new-instance v0, LX/1vY;

    const-string v1, "TITLE"

    const/16 v2, 0x9

    const/16 v3, 0xa

    const-string v4, "title"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->TITLE:LX/1vY;

    .line 342992
    new-instance v0, LX/1vY;

    const-string v1, "MEDIA_GENERIC"

    const/16 v2, 0xa

    const/16 v3, 0xb

    const-string v4, "media_generic"

    const-string v5, "media_unknown"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->MEDIA_GENERIC:LX/1vY;

    .line 342993
    new-instance v0, LX/1vY;

    const-string v1, "PHOTO"

    const/16 v2, 0xb

    const/16 v3, 0xc

    const-string v4, "photo"

    const-string v5, "photo"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PHOTO:LX/1vY;

    .line 342994
    new-instance v0, LX/1vY;

    const-string v1, "VIDEO"

    const/16 v2, 0xc

    const/16 v3, 0xd

    const-string v4, "video"

    const-string v5, "video"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->VIDEO:LX/1vY;

    .line 342995
    new-instance v0, LX/1vY;

    const-string v1, "MUSIC"

    const/16 v2, 0xd

    const/16 v3, 0xe

    const-string v4, "music"

    const-string v5, "music"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->MUSIC:LX/1vY;

    .line 342996
    new-instance v0, LX/1vY;

    const-string v1, "ATTACHMENT"

    const/16 v2, 0xe

    const/16 v3, 0xf

    const-string v4, "attachment"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ATTACHMENT:LX/1vY;

    .line 342997
    new-instance v0, LX/1vY;

    const-string v1, "NAME_LIST"

    const/16 v2, 0xf

    const/16 v3, 0x10

    const-string v4, "name_list"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->NAME_LIST:LX/1vY;

    .line 342998
    new-instance v0, LX/1vY;

    const-string v1, "SHARE_LINK"

    const/16 v2, 0x10

    const/16 v3, 0x11

    const-string v4, "share_link"

    const-string v5, "action_share"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SHARE_LINK:LX/1vY;

    .line 342999
    new-instance v0, LX/1vY;

    const-string v1, "USER_MESSAGE"

    const/16 v2, 0x11

    const/16 v3, 0x12

    const-string v4, "user_message"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->USER_MESSAGE:LX/1vY;

    .line 343000
    new-instance v0, LX/1vY;

    const-string v1, "SUBTITLE"

    const/16 v2, 0x12

    const/16 v3, 0x13

    const-string v4, "subtitle"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SUBTITLE:LX/1vY;

    .line 343001
    new-instance v0, LX/1vY;

    const-string v1, "DESCRIPTION"

    const/16 v2, 0x13

    const/16 v3, 0x14

    const-string v4, "description"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->DESCRIPTION:LX/1vY;

    .line 343002
    new-instance v0, LX/1vY;

    const-string v1, "SOURCE"

    const/16 v2, 0x14

    const/16 v3, 0x15

    const-string v4, "source"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SOURCE:LX/1vY;

    .line 343003
    new-instance v0, LX/1vY;

    const-string v1, "BLINGBOX"

    const/16 v2, 0x15

    const/16 v3, 0x16

    const-string v4, "blingbox"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->BLINGBOX:LX/1vY;

    .line 343004
    new-instance v0, LX/1vY;

    const-string v1, "OTHER"

    const/16 v2, 0x16

    const/16 v3, 0x17

    const-string v4, "other"

    const-string v5, "unknown"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OTHER:LX/1vY;

    .line 343005
    new-instance v0, LX/1vY;

    const-string v1, "VIEW_ALL_COMMENTS"

    const/16 v2, 0x17

    const/16 v3, 0x18

    const-string v4, "view_all_comments"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->VIEW_ALL_COMMENTS:LX/1vY;

    .line 343006
    new-instance v0, LX/1vY;

    const-string v1, "COMMENT"

    const/16 v2, 0x18

    const/16 v3, 0x19

    const-string v4, "comment"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COMMENT:LX/1vY;

    .line 343007
    new-instance v0, LX/1vY;

    const-string v1, "COMMENT_LINK"

    const/16 v2, 0x19

    const/16 v3, 0x1a

    const-string v4, "comment_link"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COMMENT_LINK:LX/1vY;

    .line 343008
    new-instance v0, LX/1vY;

    const-string v1, "SMALL_ACTOR_PHOTO"

    const/16 v2, 0x1a

    const/16 v3, 0x1b

    const-string v4, "small_actor_photo"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SMALL_ACTOR_PHOTO:LX/1vY;

    .line 343009
    new-instance v0, LX/1vY;

    const-string v1, "SUBSTORY"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    const-string v4, "substory"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SUBSTORY:LX/1vY;

    .line 343010
    new-instance v0, LX/1vY;

    const-string v1, "XBUTTON"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    const-string v4, "xbutton"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->XBUTTON:LX/1vY;

    .line 343011
    new-instance v0, LX/1vY;

    const-string v1, "HIDE_LINK"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    const-string v4, "hide_link"

    const-string v5, "action_hide"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->HIDE_LINK:LX/1vY;

    .line 343012
    new-instance v0, LX/1vY;

    const-string v1, "REPORT_SPAM_LINK"

    const/16 v2, 0x1e

    const/16 v3, 0x1f

    const-string v4, "report_spam_link"

    const-string v5, "action_report_spam"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->REPORT_SPAM_LINK:LX/1vY;

    .line 343013
    new-instance v0, LX/1vY;

    const-string v1, "HIDE_ALL_LINK"

    const/16 v2, 0x1f

    const/16 v3, 0x20

    const-string v4, "hide_all_link"

    const-string v5, "action_hide_all"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->HIDE_ALL_LINK:LX/1vY;

    .line 343014
    new-instance v0, LX/1vY;

    const-string v1, "BAD_AGGREGATION_LINK"

    const/16 v2, 0x20

    const/16 v3, 0x21

    const-string v4, "bad_aggregation_link"

    const-string v5, "action_bad_aggregation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->BAD_AGGREGATION_LINK:LX/1vY;

    .line 343015
    new-instance v0, LX/1vY;

    const-string v1, "ADD_COMMENT_BOX"

    const/16 v2, 0x21

    const/16 v3, 0x22

    const-string v4, "add_comment_box"

    const-string v5, "action_comment"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ADD_COMMENT_BOX:LX/1vY;

    .line 343016
    new-instance v0, LX/1vY;

    const-string v1, "APP_CALL_TO_ACTION"

    const/16 v2, 0x22

    const/16 v3, 0x23

    const-string v4, "app_call_to_action"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->APP_CALL_TO_ACTION:LX/1vY;

    .line 343017
    new-instance v0, LX/1vY;

    const-string v1, "UFI"

    const/16 v2, 0x23

    const/16 v3, 0x24

    const-string v4, "ufi"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->UFI:LX/1vY;

    .line 343018
    new-instance v0, LX/1vY;

    const-string v1, "OG_LEFT_SLIDE_PAGER"

    const/16 v2, 0x24

    const/16 v3, 0x25

    const-string v4, "og_left_slide_pager"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OG_LEFT_SLIDE_PAGER:LX/1vY;

    .line 343019
    new-instance v0, LX/1vY;

    const-string v1, "OG_RIGHT_SLIDE_PAGER"

    const/16 v2, 0x25

    const/16 v3, 0x26

    const-string v4, "og_right_slide_pager"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OG_RIGHT_SLIDE_PAGER:LX/1vY;

    .line 343020
    new-instance v0, LX/1vY;

    const-string v1, "EXP_CALL_TO_ACTION"

    const/16 v2, 0x26

    const/16 v3, 0x27

    const-string v4, "exp_call_to_action"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EXP_CALL_TO_ACTION:LX/1vY;

    .line 343021
    new-instance v0, LX/1vY;

    const-string v1, "LARGE_MEDIA_ATTACHMENT"

    const/16 v2, 0x27

    const/16 v3, 0x28

    const-string v4, "large_media_attachment"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->LARGE_MEDIA_ATTACHMENT:LX/1vY;

    .line 343022
    new-instance v0, LX/1vY;

    const-string v1, "FAN_PAGE"

    const/16 v2, 0x28

    const/16 v3, 0x2a

    const-string v4, "fan_page"

    const-string v5, "action_like"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->FAN_PAGE:LX/1vY;

    .line 343023
    new-instance v0, LX/1vY;

    const-string v1, "UNFAN_PAGE"

    const/16 v2, 0x29

    const/16 v3, 0x2b

    const-string v4, "unfan_page"

    const-string v5, "action_unlike"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->UNFAN_PAGE:LX/1vY;

    .line 343024
    new-instance v0, LX/1vY;

    const-string v1, "SEE_MORE"

    const/16 v2, 0x2a

    const/16 v3, 0x2c

    const-string v4, "see_more"

    const-string v5, "navigation"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SEE_MORE:LX/1vY;

    .line 343025
    new-instance v0, LX/1vY;

    const-string v1, "COLLECTION_ROBOTEXT_LINK"

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const-string v4, "collection_robotext_link"

    const-string v5, "profile_collections"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COLLECTION_ROBOTEXT_LINK:LX/1vY;

    .line 343026
    new-instance v0, LX/1vY;

    const-string v1, "COLLECTION_ACTION_LINK"

    const/16 v2, 0x2c

    const/16 v3, 0x2e

    const-string v4, "collection_action_link"

    const-string v5, "profile_collections"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COLLECTION_ACTION_LINK:LX/1vY;

    .line 343027
    new-instance v0, LX/1vY;

    const-string v1, "COLLECTION_TICKER_LINK"

    const/16 v2, 0x2d

    const/16 v3, 0x2f

    const-string v4, "collection_ticker_link"

    const-string v5, "profile_collections"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COLLECTION_TICKER_LINK:LX/1vY;

    .line 343028
    new-instance v0, LX/1vY;

    const-string v1, "GIFT_LINK"

    const/16 v2, 0x2e

    const/16 v3, 0x30

    const-string v4, "gift_link"

    const-string v5, "action_gift"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->GIFT_LINK:LX/1vY;

    .line 343029
    new-instance v0, LX/1vY;

    const-string v1, "SPONSORED_LINK"

    const/16 v2, 0x2f

    const/16 v3, 0x31

    const-string v4, "sponsored_link"

    const-string v5, "internal"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SPONSORED_LINK:LX/1vY;

    .line 343030
    new-instance v0, LX/1vY;

    const-string v1, "PAGE_LINK"

    const/16 v2, 0x30

    const/16 v3, 0x32

    const-string v4, "page_link"

    const-string v5, "profile_page"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PAGE_LINK:LX/1vY;

    .line 343031
    new-instance v0, LX/1vY;

    const-string v1, "SOCIAL_CONTEXT"

    const/16 v2, 0x31

    const/16 v3, 0x33

    const-string v4, "social_context"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    .line 343032
    new-instance v0, LX/1vY;

    const-string v1, "SOCIAL_ACTOR_PHOTO"

    const/16 v2, 0x32

    const/16 v3, 0x34

    const-string v4, "social_actor_photo"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SOCIAL_ACTOR_PHOTO:LX/1vY;

    .line 343033
    new-instance v0, LX/1vY;

    const-string v1, "OFFERS_CLAIM"

    const/16 v2, 0x33

    const/16 v3, 0x35

    const-string v4, "offers_claim"

    const-string v5, "offers"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OFFERS_CLAIM:LX/1vY;

    .line 343034
    new-instance v0, LX/1vY;

    const-string v1, "OFFERS_CLICK"

    const/16 v2, 0x34

    const/16 v3, 0x36

    const-string v4, "offers_click"

    const-string v5, "offers"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OFFERS_CLICK:LX/1vY;

    .line 343035
    new-instance v0, LX/1vY;

    const-string v1, "DROPDOWN_BUTTON"

    const/16 v2, 0x35

    const/16 v3, 0x37

    const-string v4, "dropdown_button"

    const-string v5, "action_show_options"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->DROPDOWN_BUTTON:LX/1vY;

    .line 343036
    new-instance v0, LX/1vY;

    const-string v1, "EVENT_VIEW"

    const/16 v2, 0x36

    const/16 v3, 0x38

    const-string v4, "event_view"

    const-string v5, "events"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EVENT_VIEW:LX/1vY;

    .line 343037
    new-instance v0, LX/1vY;

    const-string v1, "EVENT_RSVP"

    const/16 v2, 0x37

    const/16 v3, 0x39

    const-string v4, "event_rsvp"

    const-string v5, "events"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EVENT_RSVP:LX/1vY;

    .line 343038
    new-instance v0, LX/1vY;

    const-string v1, "ADS_SHIMMED_LINK"

    const/16 v2, 0x38

    const/16 v3, 0x3a

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ADS_SHIMMED_LINK:LX/1vY;

    .line 343039
    new-instance v0, LX/1vY;

    const-string v1, "COLLECTION_ADD_BUTTON"

    const/16 v2, 0x39

    const/16 v3, 0x3b

    const-string v4, "collection_add_button"

    const-string v5, "profile_collections"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COLLECTION_ADD_BUTTON:LX/1vY;

    .line 343040
    new-instance v0, LX/1vY;

    const-string v1, "EVENT_INVITE_FRIENDS"

    const/16 v2, 0x3a

    const/16 v3, 0x3c

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EVENT_INVITE_FRIENDS:LX/1vY;

    .line 343041
    new-instance v0, LX/1vY;

    const-string v1, "RHC_AD"

    const/16 v2, 0x3b

    const/16 v3, 0x3d

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_AD:LX/1vY;

    .line 343042
    new-instance v0, LX/1vY;

    const-string v1, "AD_CREATIVE_TITLE"

    const/16 v2, 0x3c

    const/16 v3, 0x3e

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_CREATIVE_TITLE:LX/1vY;

    .line 343043
    new-instance v0, LX/1vY;

    const-string v1, "AD_CREATIVE_BODY"

    const/16 v2, 0x3d

    const/16 v3, 0x3f

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_CREATIVE_BODY:LX/1vY;

    .line 343044
    new-instance v0, LX/1vY;

    const-string v1, "AD_CREATIVE_IMAGE"

    const/16 v2, 0x3e

    const/16 v3, 0x40

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_CREATIVE_IMAGE:LX/1vY;

    .line 343045
    new-instance v0, LX/1vY;

    const-string v1, "AD_SOCIAL_SENTENCE"

    const/16 v2, 0x3f

    const/16 v3, 0x41

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_SOCIAL_SENTENCE:LX/1vY;

    .line 343046
    new-instance v0, LX/1vY;

    const-string v1, "APP_NAME"

    const/16 v2, 0x40

    const/16 v3, 0x42

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->APP_NAME:LX/1vY;

    .line 343047
    new-instance v0, LX/1vY;

    const-string v1, "GROUP_JOIN"

    const/16 v2, 0x41

    const/16 v3, 0x43

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->GROUP_JOIN:LX/1vY;

    .line 343048
    new-instance v0, LX/1vY;

    const-string v1, "PAGE_COVER_PHOTO"

    const/16 v2, 0x42

    const/16 v3, 0x44

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PAGE_COVER_PHOTO:LX/1vY;

    .line 343049
    new-instance v0, LX/1vY;

    const-string v1, "PAGE_PROFILE_PIC"

    const/16 v2, 0x43

    const/16 v3, 0x45

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PAGE_PROFILE_PIC:LX/1vY;

    .line 343050
    new-instance v0, LX/1vY;

    const-string v1, "AD_IDENTITY"

    const/16 v2, 0x44

    const/16 v3, 0x46

    const-string v4, "ad_identity"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_IDENTITY:LX/1vY;

    .line 343051
    new-instance v0, LX/1vY;

    const-string v1, "UNHIDE_LINK"

    const/16 v2, 0x45

    const/16 v3, 0x47

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->UNHIDE_LINK:LX/1vY;

    .line 343052
    new-instance v0, LX/1vY;

    const-string v1, "TRENDING_TOPIC_LINK"

    const/16 v2, 0x46

    const/16 v3, 0x48

    const-string v4, "trending_topic_link"

    const-string v5, "topic"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->TRENDING_TOPIC_LINK:LX/1vY;

    .line 343053
    new-instance v0, LX/1vY;

    const-string v1, "RELATED_SHARE_ARTICLE"

    const/16 v2, 0x47

    const/16 v3, 0x49

    const-string v4, "related_share_article"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RELATED_SHARE_ARTICLE:LX/1vY;

    .line 343054
    new-instance v0, LX/1vY;

    const-string v1, "OFFERS_USE_NOW"

    const/16 v2, 0x48

    const/16 v3, 0x4a

    const-string v4, "offers_use_now"

    const-string v5, "offers"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OFFERS_USE_NOW:LX/1vY;

    .line 343055
    new-instance v0, LX/1vY;

    const-string v1, "RELATED_SHARE_VIDEO"

    const/16 v2, 0x49

    const/16 v3, 0x4b

    const-string v4, "related_share_video"

    const-string v5, "video"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RELATED_SHARE_VIDEO:LX/1vY;

    .line 343056
    new-instance v0, LX/1vY;

    const-string v1, "RHC_CARD"

    const/16 v2, 0x4a

    const/16 v3, 0x4c

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_CARD:LX/1vY;

    .line 343057
    new-instance v0, LX/1vY;

    const-string v1, "RHC_CARD_HIDE"

    const/16 v2, 0x4b

    const/16 v3, 0x4d

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_CARD_HIDE:LX/1vY;

    .line 343058
    new-instance v0, LX/1vY;

    const-string v1, "RHC_SIMPLIFICATION"

    const/16 v2, 0x4c

    const/16 v3, 0x4e

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_SIMPLIFICATION:LX/1vY;

    .line 343059
    new-instance v0, LX/1vY;

    const-string v1, "RHC_SIMPLIFICATION_HIDE"

    const/16 v2, 0x4d

    const/16 v3, 0x4f

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_SIMPLIFICATION_HIDE:LX/1vY;

    .line 343060
    new-instance v0, LX/1vY;

    const-string v1, "TOPIC_PIVOT_HEADER"

    const/16 v2, 0x4e

    const/16 v3, 0x50

    const-string v4, "topic_pivot_header"

    const-string v5, "topic"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->TOPIC_PIVOT_HEADER:LX/1vY;

    .line 343061
    new-instance v0, LX/1vY;

    const-string v1, "ADD_FRIEND_BUTTON"

    const/16 v2, 0x4f

    const/16 v3, 0x51

    const-string v4, "add_friend_button"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ADD_FRIEND_BUTTON:LX/1vY;

    .line 343062
    new-instance v0, LX/1vY;

    const-string v1, "SNOWLIFT"

    const/16 v2, 0x50

    const/16 v3, 0x52

    const-string v4, "snowlift"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SNOWLIFT:LX/1vY;

    .line 343063
    new-instance v0, LX/1vY;

    const-string v1, "SNOWLIFT_MESSAGE"

    const/16 v2, 0x51

    const/16 v3, 0x53

    const-string v4, "snowlift_message"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SNOWLIFT_MESSAGE:LX/1vY;

    .line 343064
    new-instance v0, LX/1vY;

    const-string v1, "OFFERS_RESEND"

    const/16 v2, 0x52

    const/16 v3, 0x54

    const-string v4, "offers_resend"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OFFERS_RESEND:LX/1vY;

    .line 343065
    new-instance v0, LX/1vY;

    const-string v1, "RHC_LINK_OPEN"

    const/16 v2, 0x53

    const/16 v3, 0x55

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_LINK_OPEN:LX/1vY;

    .line 343066
    new-instance v0, LX/1vY;

    const-string v1, "GENERIC_CALL_TO_ACTION_BUTTON"

    const/16 v2, 0x54

    const/16 v3, 0x56

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    .line 343067
    new-instance v0, LX/1vY;

    const-string v1, "AD_LOGOUT"

    const/16 v2, 0x55

    const/16 v3, 0x57

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AD_LOGOUT:LX/1vY;

    .line 343068
    new-instance v0, LX/1vY;

    const-string v1, "RHC_PHOTO_SLIDER"

    const/16 v2, 0x56

    const/16 v3, 0x58

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_PHOTO_SLIDER:LX/1vY;

    .line 343069
    new-instance v0, LX/1vY;

    const-string v1, "RHC_COMMENT_BUTTON"

    const/16 v2, 0x57

    const/16 v3, 0x59

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RHC_COMMENT_BUTTON:LX/1vY;

    .line 343070
    new-instance v0, LX/1vY;

    const-string v1, "HASHTAG"

    const/16 v2, 0x58

    const/16 v3, 0x5a

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->HASHTAG:LX/1vY;

    .line 343071
    new-instance v0, LX/1vY;

    const-string v1, "NOTE"

    const/16 v2, 0x59

    const/16 v3, 0x5b

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->NOTE:LX/1vY;

    .line 343072
    new-instance v0, LX/1vY;

    const-string v1, "RELATED_SHARE_ARTICLE_HIDE"

    const/16 v2, 0x5a

    const/16 v3, 0x5c

    const-string v4, "related_share_article_hide"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RELATED_SHARE_ARTICLE_HIDE:LX/1vY;

    .line 343073
    new-instance v0, LX/1vY;

    const-string v1, "RELATED_SHARE_VIDEO_HIDE"

    const/16 v2, 0x5b

    const/16 v3, 0x5d

    const-string v4, "related_share_video_hide"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RELATED_SHARE_VIDEO_HIDE:LX/1vY;

    .line 343074
    new-instance v0, LX/1vY;

    const-string v1, "NEKO_PREVIEW"

    const/16 v2, 0x5c

    const/16 v3, 0x5e

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->NEKO_PREVIEW:LX/1vY;

    .line 343075
    new-instance v0, LX/1vY;

    const-string v1, "OG_COMPOSER_OBJECT"

    const/16 v2, 0x5d

    const/16 v3, 0x5f

    const-string v4, "og_composer_object"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->OG_COMPOSER_OBJECT:LX/1vY;

    .line 343076
    new-instance v0, LX/1vY;

    const-string v1, "INSTALL_ACTION"

    const/16 v2, 0x5e

    const/16 v3, 0x60

    const-string v4, "install_action"

    const-string v5, "install"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->INSTALL_ACTION:LX/1vY;

    .line 343077
    new-instance v0, LX/1vY;

    const-string v1, "SPONSORED_CONTEXT"

    const/16 v2, 0x5f

    const/16 v3, 0x61

    const-string v4, "sponsored_context"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SPONSORED_CONTEXT:LX/1vY;

    .line 343078
    new-instance v0, LX/1vY;

    const-string v1, "DIGITAL_GOOD"

    const/16 v2, 0x60

    const/16 v3, 0x62

    const-string v4, "digital_good"

    const-string v5, "digital_good"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->DIGITAL_GOOD:LX/1vY;

    .line 343079
    new-instance v0, LX/1vY;

    const-string v1, "STORY_FOOTER"

    const/16 v2, 0x61

    const/16 v3, 0x63

    const-string v4, "story_footer"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->STORY_FOOTER:LX/1vY;

    .line 343080
    new-instance v0, LX/1vY;

    const-string v1, "STORY_LOCATION"

    const/16 v2, 0x62

    const/16 v3, 0x64

    const-string v4, "story_location"

    const-string v5, "location"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->STORY_LOCATION:LX/1vY;

    .line 343081
    new-instance v0, LX/1vY;

    const-string v1, "ADD_PHOTO_ACTION"

    const/16 v2, 0x63

    const/16 v3, 0x65

    const-string v4, "add_photo_action"

    const-string v5, "photo"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ADD_PHOTO_ACTION:LX/1vY;

    .line 343082
    new-instance v0, LX/1vY;

    const-string v1, "ACTION_ICON"

    const/16 v2, 0x64

    const/16 v3, 0x66

    const-string v4, "action_icon"

    const-string v5, "link"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ACTION_ICON:LX/1vY;

    .line 343083
    new-instance v0, LX/1vY;

    const-string v1, "EGO_FEED_UNIT"

    const/16 v2, 0x65

    const/16 v3, 0x67

    const-string v4, "ego_feed_unit"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EGO_FEED_UNIT:LX/1vY;

    .line 343084
    new-instance v0, LX/1vY;

    const-string v1, "PLACE_STAR_SURVEY"

    const/16 v2, 0x66

    const/16 v3, 0x68

    const-string v4, "place_star_survey"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PLACE_STAR_SURVEY:LX/1vY;

    .line 343085
    new-instance v0, LX/1vY;

    const-string v1, "REVIEW_MENU"

    const/16 v2, 0x67

    const/16 v3, 0x69

    const-string v4, "review_menu"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->REVIEW_MENU:LX/1vY;

    .line 343086
    new-instance v0, LX/1vY;

    const-string v1, "SAVE_ACTION"

    const/16 v2, 0x68

    const/16 v3, 0x6a

    const-string v4, "save_action"

    const-string v5, "save"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SAVE_ACTION:LX/1vY;

    .line 343087
    new-instance v0, LX/1vY;

    const-string v1, "PHOTO_GALLERY"

    const/16 v2, 0x69

    const/16 v3, 0x6b

    const-string v4, "photo_gallery"

    const-string v5, "photo"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PHOTO_GALLERY:LX/1vY;

    .line 343088
    new-instance v0, LX/1vY;

    const-string v1, "SUB_ATTACHMENT"

    const/16 v2, 0x6a

    const/16 v3, 0x6c

    const-string v4, "sub_attachment"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SUB_ATTACHMENT:LX/1vY;

    .line 343089
    new-instance v0, LX/1vY;

    const-string v1, "FEEDBACK_SECTION"

    const/16 v2, 0x6b

    const/16 v3, 0x6d

    const-string v4, "feedback_section"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    .line 343090
    new-instance v0, LX/1vY;

    const-string v1, "ALBUM"

    const/16 v2, 0x6c

    const/16 v3, 0x6e

    const-string v4, "album"

    const-string v5, "album"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ALBUM:LX/1vY;

    .line 343091
    new-instance v0, LX/1vY;

    const-string v1, "ALBUM_COLLAGE"

    const/16 v2, 0x6d

    const/16 v3, 0x6f

    const-string v4, "album_collage"

    const-string v5, "album"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ALBUM_COLLAGE:LX/1vY;

    .line 343092
    new-instance v0, LX/1vY;

    const-string v1, "AVATAR_LIST"

    const/16 v2, 0x6e

    const/16 v3, 0x70

    const-string v4, "avatar_list"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->AVATAR_LIST:LX/1vY;

    .line 343093
    new-instance v0, LX/1vY;

    const-string v1, "STORY_LIST"

    const/16 v2, 0x6f

    const/16 v3, 0x71

    const-string v4, "list"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->STORY_LIST:LX/1vY;

    .line 343094
    new-instance v0, LX/1vY;

    const-string v1, "MEDIA_CONTROLS"

    const/16 v2, 0x70

    const/16 v3, 0x72

    const-string v4, "media_controls"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->MEDIA_CONTROLS:LX/1vY;

    .line 343095
    new-instance v0, LX/1vY;

    const-string v1, "ZERO_UPSELL_BUY"

    const/16 v2, 0x71

    const/16 v3, 0x73

    const-string v4, "zero_upsell_buy"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ZERO_UPSELL_BUY:LX/1vY;

    .line 343096
    new-instance v0, LX/1vY;

    const-string v1, "ZERO_UPSELL_FEED_UNIT"

    const/16 v2, 0x72

    const/16 v3, 0x74

    const-string v4, "zero_upsell_feed_unit"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->ZERO_UPSELL_FEED_UNIT:LX/1vY;

    .line 343097
    new-instance v0, LX/1vY;

    const-string v1, "RATING"

    const/16 v2, 0x73

    const/16 v3, 0x75

    const-string v4, "rating"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RATING:LX/1vY;

    .line 343098
    new-instance v0, LX/1vY;

    const-string v1, "PERMALINK_COMMENT"

    const/16 v2, 0x74

    const/16 v3, 0x76

    const-string v4, "permalink_comment"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PERMALINK_COMMENT:LX/1vY;

    .line 343099
    new-instance v0, LX/1vY;

    const-string v1, "LIKE_COUNT"

    const/16 v2, 0x75

    const/16 v3, 0x77

    const-string v4, "like_count"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->LIKE_COUNT:LX/1vY;

    .line 343100
    new-instance v0, LX/1vY;

    const-string v1, "RETRY_BUTTON"

    const/16 v2, 0x76

    const/16 v3, 0x78

    const-string v4, "retry_button"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RETRY_BUTTON:LX/1vY;

    .line 343101
    new-instance v0, LX/1vY;

    const-string v1, "TIMELINE_GIFTS"

    const/16 v2, 0x77

    const/16 v3, 0x79

    const-string v4, "timeline_gifts"

    const-string v5, "gifts"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->TIMELINE_GIFTS:LX/1vY;

    .line 343102
    new-instance v0, LX/1vY;

    const-string v1, "NEARBY_FRIENDS_LIST"

    const/16 v2, 0x78

    const/16 v3, 0x7a

    const-string v4, "nearby_friends_list"

    const-string v5, "friends_list"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->NEARBY_FRIENDS_LIST:LX/1vY;

    .line 343103
    new-instance v0, LX/1vY;

    const-string v1, "PRESENCE_UNIT"

    const/16 v2, 0x79

    const/16 v3, 0x7b

    const-string v4, "presence_unit"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PRESENCE_UNIT:LX/1vY;

    .line 343104
    new-instance v0, LX/1vY;

    const-string v1, "EVENT_INVITE_SENT"

    const/16 v2, 0x7a

    const/16 v3, 0x7c

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->EVENT_INVITE_SENT:LX/1vY;

    .line 343105
    new-instance v0, LX/1vY;

    const-string v1, "COLLECTIONS_RATING"

    const/16 v2, 0x7b

    const/16 v3, 0x7d

    const-string v4, "collections_rating"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->COLLECTIONS_RATING:LX/1vY;

    .line 343106
    new-instance v0, LX/1vY;

    const-string v1, "SAVE_BUTTON"

    const/16 v2, 0x7c

    const/16 v3, 0x7e

    const-string v4, "save_button"

    const-string v5, "profile_collections"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SAVE_BUTTON:LX/1vY;

    .line 343107
    new-instance v0, LX/1vY;

    const-string v1, "SAVED_COLLECTION"

    const/16 v2, 0x7d

    const/16 v3, 0x7f

    const-string v4, "saved_collection"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SAVED_COLLECTION:LX/1vY;

    .line 343108
    new-instance v0, LX/1vY;

    const-string v1, "NATIVE_NAME"

    const/16 v2, 0x7e

    const/16 v3, 0x80

    const-string v4, "native_name"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->NATIVE_NAME:LX/1vY;

    .line 343109
    new-instance v0, LX/1vY;

    const-string v1, "RAW_NAME"

    const/16 v2, 0x7f

    const/16 v3, 0x81

    const-string v4, "raw_name"

    const-string v5, "profile_user"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->RAW_NAME:LX/1vY;

    .line 343110
    new-instance v0, LX/1vY;

    const-string v1, "SEND"

    const/16 v2, 0x80

    const/16 v3, 0x82

    const-string v4, "send"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->SEND:LX/1vY;

    .line 343111
    new-instance v0, LX/1vY;

    const-string v1, "PLACE_MORE_INFO_ACTION"

    const/16 v2, 0x81

    const/16 v3, 0x83

    const-string v4, "place_more_info_action"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PLACE_MORE_INFO_ACTION:LX/1vY;

    .line 343112
    new-instance v0, LX/1vY;

    const-string v1, "FEED_STORY_MESSAGE_FLYOUT"

    const/16 v2, 0x82

    const/16 v3, 0x84

    const-string v4, "feed_story_message_flyout"

    const-string v5, "flyout"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->FEED_STORY_MESSAGE_FLYOUT:LX/1vY;

    .line 343113
    new-instance v0, LX/1vY;

    const-string v1, "HEADER"

    const/16 v2, 0x83

    const/16 v3, 0x85

    const-string v4, "feed_story_header"

    const-string v5, "header"

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->HEADER:LX/1vY;

    .line 343114
    new-instance v0, LX/1vY;

    const-string v1, "PLACE_WRITE_REVIEW_ACTION"

    const/16 v2, 0x84

    const/16 v3, 0x86

    const-string v4, "place_write_review_action"

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, LX/1vY;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/1vY;->PLACE_WRITE_REVIEW_ACTION:LX/1vY;

    .line 343115
    const/16 v0, 0x85

    new-array v0, v0, [LX/1vY;

    const/4 v1, 0x0

    sget-object v2, LX/1vY;->HEADLINE:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/1vY;->USER_NAME:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/1vY;->ACTOR_PHOTO:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/1vY;->ACTION_LINKS:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/1vY;->LIKE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/1vY;->UNLIKE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1vY;->PARTICIPANT:LX/1vY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1vY;->PRONOUN:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1vY;->ROBOTEXT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1vY;->TITLE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1vY;->MEDIA_GENERIC:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1vY;->PHOTO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1vY;->VIDEO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1vY;->MUSIC:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1vY;->ATTACHMENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1vY;->NAME_LIST:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/1vY;->SHARE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/1vY;->USER_MESSAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/1vY;->SUBTITLE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/1vY;->DESCRIPTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/1vY;->SOURCE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/1vY;->BLINGBOX:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/1vY;->OTHER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/1vY;->VIEW_ALL_COMMENTS:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/1vY;->COMMENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/1vY;->COMMENT_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/1vY;->SMALL_ACTOR_PHOTO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/1vY;->SUBSTORY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/1vY;->XBUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/1vY;->HIDE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/1vY;->REPORT_SPAM_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/1vY;->HIDE_ALL_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/1vY;->BAD_AGGREGATION_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/1vY;->ADD_COMMENT_BOX:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/1vY;->APP_CALL_TO_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/1vY;->UFI:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/1vY;->OG_LEFT_SLIDE_PAGER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/1vY;->OG_RIGHT_SLIDE_PAGER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/1vY;->EXP_CALL_TO_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/1vY;->LARGE_MEDIA_ATTACHMENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/1vY;->FAN_PAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/1vY;->UNFAN_PAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/1vY;->SEE_MORE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/1vY;->COLLECTION_ROBOTEXT_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/1vY;->COLLECTION_ACTION_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/1vY;->COLLECTION_TICKER_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/1vY;->GIFT_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/1vY;->SPONSORED_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/1vY;->PAGE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/1vY;->SOCIAL_CONTEXT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/1vY;->SOCIAL_ACTOR_PHOTO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/1vY;->OFFERS_CLAIM:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/1vY;->OFFERS_CLICK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/1vY;->DROPDOWN_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/1vY;->EVENT_VIEW:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/1vY;->EVENT_RSVP:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/1vY;->ADS_SHIMMED_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/1vY;->COLLECTION_ADD_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/1vY;->EVENT_INVITE_FRIENDS:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/1vY;->RHC_AD:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/1vY;->AD_CREATIVE_TITLE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/1vY;->AD_CREATIVE_BODY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/1vY;->AD_CREATIVE_IMAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/1vY;->AD_SOCIAL_SENTENCE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/1vY;->APP_NAME:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/1vY;->GROUP_JOIN:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/1vY;->PAGE_COVER_PHOTO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/1vY;->PAGE_PROFILE_PIC:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/1vY;->AD_IDENTITY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/1vY;->UNHIDE_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/1vY;->TRENDING_TOPIC_LINK:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/1vY;->RELATED_SHARE_ARTICLE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/1vY;->OFFERS_USE_NOW:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/1vY;->RELATED_SHARE_VIDEO:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/1vY;->RHC_CARD:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/1vY;->RHC_CARD_HIDE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/1vY;->RHC_SIMPLIFICATION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/1vY;->RHC_SIMPLIFICATION_HIDE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/1vY;->TOPIC_PIVOT_HEADER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/1vY;->ADD_FRIEND_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/1vY;->SNOWLIFT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/1vY;->SNOWLIFT_MESSAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/1vY;->OFFERS_RESEND:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/1vY;->RHC_LINK_OPEN:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/1vY;->AD_LOGOUT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/1vY;->RHC_PHOTO_SLIDER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/1vY;->RHC_COMMENT_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/1vY;->HASHTAG:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/1vY;->NOTE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, LX/1vY;->RELATED_SHARE_ARTICLE_HIDE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, LX/1vY;->RELATED_SHARE_VIDEO_HIDE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, LX/1vY;->NEKO_PREVIEW:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, LX/1vY;->OG_COMPOSER_OBJECT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, LX/1vY;->INSTALL_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, LX/1vY;->SPONSORED_CONTEXT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, LX/1vY;->DIGITAL_GOOD:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, LX/1vY;->STORY_FOOTER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, LX/1vY;->STORY_LOCATION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, LX/1vY;->ADD_PHOTO_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, LX/1vY;->ACTION_ICON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, LX/1vY;->EGO_FEED_UNIT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, LX/1vY;->PLACE_STAR_SURVEY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, LX/1vY;->REVIEW_MENU:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, LX/1vY;->SAVE_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, LX/1vY;->PHOTO_GALLERY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, LX/1vY;->SUB_ATTACHMENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, LX/1vY;->ALBUM:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, LX/1vY;->ALBUM_COLLAGE:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, LX/1vY;->AVATAR_LIST:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, LX/1vY;->STORY_LIST:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, LX/1vY;->MEDIA_CONTROLS:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, LX/1vY;->ZERO_UPSELL_BUY:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, LX/1vY;->ZERO_UPSELL_FEED_UNIT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, LX/1vY;->RATING:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, LX/1vY;->PERMALINK_COMMENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, LX/1vY;->LIKE_COUNT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, LX/1vY;->RETRY_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, LX/1vY;->TIMELINE_GIFTS:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, LX/1vY;->NEARBY_FRIENDS_LIST:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, LX/1vY;->PRESENCE_UNIT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, LX/1vY;->EVENT_INVITE_SENT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, LX/1vY;->COLLECTIONS_RATING:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, LX/1vY;->SAVE_BUTTON:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, LX/1vY;->SAVED_COLLECTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, LX/1vY;->NATIVE_NAME:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, LX/1vY;->RAW_NAME:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, LX/1vY;->SEND:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, LX/1vY;->PLACE_MORE_INFO_ACTION:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, LX/1vY;->FEED_STORY_MESSAGE_FLYOUT:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, LX/1vY;->HEADER:LX/1vY;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, LX/1vY;->PLACE_WRITE_REVIEW_ACTION:LX/1vY;

    aput-object v2, v0, v1

    sput-object v0, LX/1vY;->$VALUES:[LX/1vY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 343117
    iput p3, p0, LX/1vY;->mType:I

    .line 343118
    iput-object p4, p0, LX/1vY;->mName:Ljava/lang/String;

    .line 343119
    iput-object p5, p0, LX/1vY;->mDestination:Ljava/lang/String;

    .line 343120
    return-void
.end method

.method public static getTrackingNodeByType(I)LX/1vY;
    .locals 5

    .prologue
    .line 343121
    invoke-static {}, LX/1vY;->values()[LX/1vY;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 343122
    iget v4, v0, LX/1vY;->mType:I

    if-ne v4, p0, :cond_0

    .line 343123
    :goto_1
    return-object v0

    .line 343124
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 343125
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/1vY;
    .locals 1

    .prologue
    .line 343126
    const-class v0, LX/1vY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1vY;

    return-object v0
.end method

.method public static values()[LX/1vY;
    .locals 1

    .prologue
    .line 343127
    sget-object v0, LX/1vY;->$VALUES:[LX/1vY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1vY;

    return-object v0
.end method


# virtual methods
.method public final getDestination()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343128
    iget-object v0, p0, LX/1vY;->mDestination:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 343129
    iget-object v0, p0, LX/1vY;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 343130
    iget v0, p0, LX/1vY;->mType:I

    return v0
.end method
