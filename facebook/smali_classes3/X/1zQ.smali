.class public LX/1zQ;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:I

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353237
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 353238
    iput-object p1, p0, LX/1zQ;->a:Landroid/content/Context;

    .line 353239
    const v0, 0x7f0a0042

    iput v0, p0, LX/1zQ;->b:I

    .line 353240
    iput-object p2, p0, LX/1zQ;->c:Landroid/view/View$OnClickListener;

    .line 353241
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 353232
    iget-object v0, p0, LX/1zQ;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 353233
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 353234
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 353235
    iget-object v0, p0, LX/1zQ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LX/1zQ;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 353236
    return-void
.end method
