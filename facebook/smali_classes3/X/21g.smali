.class public LX/21g;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/21g",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357654
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 357655
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/21g;->b:LX/0Zi;

    .line 357656
    iput-object p1, p0, LX/21g;->a:LX/0Ot;

    .line 357657
    return-void
.end method

.method public static a(LX/0QB;)LX/21g;
    .locals 4

    .prologue
    .line 357672
    const-class v1, LX/21g;

    monitor-enter v1

    .line 357673
    :try_start_0
    sget-object v0, LX/21g;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357674
    sput-object v2, LX/21g;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357675
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357676
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357677
    new-instance v3, LX/21g;

    const/16 p0, 0x1f76

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/21g;-><init>(LX/0Ot;)V

    .line 357678
    move-object v0, v3

    .line 357679
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357680
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/21g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357681
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357671
    const v0, -0x315d436

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 357668
    check-cast p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    .line 357669
    iget-object v0, p0, LX/21g;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    iget-object v2, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    iget-object v4, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v5, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->d:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Lcom/facebook/common/callercontext/CallerContext;Z)LX/1Dg;

    move-result-object v0

    .line 357670
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 357658
    invoke-static {}, LX/1dS;->b()V

    .line 357659
    iget v0, p1, LX/1dQ;->b:I

    .line 357660
    packed-switch v0, :pswitch_data_0

    .line 357661
    :goto_0
    return-object v2

    .line 357662
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 357663
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 357664
    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    .line 357665
    iget-object v3, p0, LX/21g;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;

    iget-object p1, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    .line 357666
    iget-object p0, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponentSpec;->d:LX/36i;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, p2, v1}, LX/36i;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;Z)V

    .line 357667
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x315d436
        :pswitch_0
    .end packed-switch
.end method
