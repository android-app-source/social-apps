.class public LX/1vb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1vd;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ve;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343213
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1vb;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1ve;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343280
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 343281
    iput-object p1, p0, LX/1vb;->b:LX/0Ot;

    .line 343282
    return-void
.end method

.method public static a(LX/0QB;)LX/1vb;
    .locals 4

    .prologue
    .line 343269
    const-class v1, LX/1vb;

    monitor-enter v1

    .line 343270
    :try_start_0
    sget-object v0, LX/1vb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343271
    sput-object v2, LX/1vb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343272
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343273
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343274
    new-instance v3, LX/1vb;

    const/16 p0, 0x730

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1vb;-><init>(LX/0Ot;)V

    .line 343275
    move-object v0, v3

    .line 343276
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343277
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1vb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343278
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 343253
    check-cast p2, LX/1vc;

    .line 343254
    iget-object v0, p0, LX/1vb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ve;

    iget-object v1, p2, LX/1vc;->a:LX/1dl;

    iget v2, p2, LX/1vc;->b:I

    iget-boolean v3, p2, LX/1vc;->c:Z

    .line 343255
    sget-object v4, LX/1dl;->HIDDEN:LX/1dl;

    if-ne v1, v4, :cond_0

    .line 343256
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    .line 343257
    :goto_0
    move-object v0, v4

    .line 343258
    return-object v0

    .line 343259
    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    .line 343260
    if-eqz v3, :cond_2

    .line 343261
    const p0, 0x7f020aeb

    invoke-virtual {v4, p0}, LX/1o5;->h(I)LX/1o5;

    .line 343262
    :goto_1
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    sget-object p0, LX/1ve;->a:Landroid/util/SparseArray;

    invoke-interface {v4, p0}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v4

    const p0, 0x7f0810fa

    invoke-interface {v4, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object p0

    sget-object v4, LX/1dl;->CLICKABLE:LX/1dl;

    if-ne v1, v4, :cond_1

    .line 343263
    const v4, -0x73658cd2

    const/4 p2, 0x0

    invoke-static {p1, v4, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 343264
    :goto_2
    invoke-interface {p0, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b00e3

    invoke-interface {v4, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b00e9

    invoke-interface {v4, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 p0, 0x4

    const p2, 0x7f0b00eb

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x1

    const p2, 0x7f0b00ed

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x5

    const p2, 0x7f0b00ea

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x3

    const p2, 0x7f0b00ee

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    .line 343265
    :cond_2
    const p0, -0x6e685d

    if-ne v2, p0, :cond_3

    .line 343266
    invoke-static {v0}, LX/1ve;->a(LX/1ve;)I

    move-result p0

    invoke-virtual {v4, p0}, LX/1o5;->h(I)LX/1o5;

    goto :goto_1

    .line 343267
    :cond_3
    iget-object p0, v0, LX/1ve;->b:LX/1vg;

    invoke-virtual {p0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object p0

    invoke-static {v0}, LX/1ve;->a(LX/1ve;)I

    move-result p2

    invoke-virtual {p0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    invoke-virtual {p0}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    .line 343268
    invoke-virtual {v4, p0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    goto :goto_1
.end method

.method public final a(LX/1De;II)LX/1vd;
    .locals 2

    .prologue
    .line 343283
    new-instance v0, LX/1vc;

    invoke-direct {v0, p0}, LX/1vc;-><init>(LX/1vb;)V

    .line 343284
    sget-object v1, LX/1vb;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1vd;

    .line 343285
    if-nez v1, :cond_0

    .line 343286
    new-instance v1, LX/1vd;

    invoke-direct {v1}, LX/1vd;-><init>()V

    .line 343287
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/1vd;->a$redex0(LX/1vd;LX/1De;IILX/1vc;)V

    .line 343288
    move-object v0, v1

    .line 343289
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 343242
    invoke-static {}, LX/1dS;->b()V

    .line 343243
    iget v0, p1, LX/1dQ;->b:I

    .line 343244
    packed-switch v0, :pswitch_data_0

    .line 343245
    :goto_0
    return-object v2

    .line 343246
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 343247
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 343248
    check-cast v1, LX/1vc;

    .line 343249
    iget-object p1, p0, LX/1vb;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/1vc;->e:LX/1SX;

    .line 343250
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    .line 343251
    invoke-virtual {p2, p1, p0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 343252
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x73658cd2
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1vd;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 343241
    invoke-virtual {p0, p1, v0, v0}, LX/1vb;->a(LX/1De;II)LX/1vd;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 343214
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 343215
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 343216
    iget-object v0, p0, LX/1vb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v3, 0x0

    .line 343217
    sget-object v0, LX/03r;->HeaderMenuComponent:[I

    invoke-virtual {p1, v0, v3}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 343218
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v5

    move v0, v3

    :goto_0
    if-ge v0, v5, :cond_2

    .line 343219
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v6

    .line 343220
    const/16 p0, 0x0

    if-ne v6, p0, :cond_1

    .line 343221
    invoke-virtual {v4, v6, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 343222
    iput-object v6, v1, LX/1np;->a:Ljava/lang/Object;

    .line 343223
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343224
    :cond_1
    const/16 p0, 0x1

    if-ne v6, p0, :cond_0

    .line 343225
    invoke-virtual {v4, v6, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 343226
    iput-object v6, v2, LX/1np;->a:Ljava/lang/Object;

    .line 343227
    goto :goto_1

    .line 343228
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 343229
    check-cast p2, LX/1vc;

    .line 343230
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 343231
    if-eqz v0, :cond_3

    .line 343232
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 343233
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1vc;->b:I

    .line 343234
    :cond_3
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 343235
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 343236
    if-eqz v0, :cond_4

    .line 343237
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 343238
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/1vc;->c:Z

    .line 343239
    :cond_4
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 343240
    return-void
.end method
