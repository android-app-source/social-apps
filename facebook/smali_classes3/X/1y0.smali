.class public final LX/1y0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1xy;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1xy;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 349905
    iput-object p1, p0, LX/1y0;->a:LX/1xy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349906
    iget-boolean v0, p1, LX/1xy;->m:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, LX/1y1;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1y0;->b:Ljava/lang/String;

    .line 349907
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 349908
    iget-object v0, p0, LX/1y0;->a:LX/1xy;

    iget-object v0, v0, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349909
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 349910
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349911
    const/4 v4, 0x0

    .line 349912
    iget-object v1, p0, LX/1y0;->a:LX/1xy;

    iget-object v1, v1, LX/1xy;->o:LX/1PT;

    if-eqz v1, :cond_7

    sget-object v1, LX/1Qt;->GROUPS:LX/1Qt;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1Qt;->GROUPS_MEMBER_INFO:LX/1Qt;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1Qt;->GROUPS_PENDING:LX/1Qt;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1Qt;->GROUPS_PINNED:LX/1Qt;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/1Qt;->GROUPS_REPORTED:LX/1Qt;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 349913
    :cond_0
    iget-object v1, p0, LX/1y0;->a:LX/1xy;

    iget-object v1, v1, LX/1xy;->h:LX/1xv;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/1y0;->a:LX/1xy;

    iget-boolean v3, v3, LX/1xy;->n:Z

    invoke-virtual {v1, v2, v4, v3}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)LX/1y2;

    move-result-object v1

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->o:LX/1PT;

    .line 349914
    iput-object v2, v1, LX/1y2;->j:LX/1PT;

    .line 349915
    move-object v1, v1

    .line 349916
    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    .line 349917
    iget-object v3, v2, LX/1xy;->o:LX/1PT;

    if-eqz v3, :cond_9

    iget-object v3, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    sget-object v4, LX/1Qt;->GROUPS:LX/1Qt;

    if-eq v3, v4, :cond_1

    iget-object v3, v2, LX/1xy;->o:LX/1PT;

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    sget-object v4, LX/1Qt;->GROUPS_MEMBER_INFO:LX/1Qt;

    if-ne v3, v4, :cond_9

    :cond_1
    const/4 v4, 0x0

    .line 349918
    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349919
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 349920
    if-eqz v3, :cond_2

    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349921
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 349922
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_2

    const v5, 0x41e065f

    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349923
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v6

    .line 349924
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    if-ne v5, v3, :cond_2

    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349925
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 349926
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349927
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 349928
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v3

    if-nez v3, :cond_a

    :cond_2
    move v3, v4

    .line 349929
    :goto_0
    move v3, v3

    .line 349930
    if-eqz v3, :cond_9

    const/4 v3, 0x1

    :goto_1
    move v2, v3

    .line 349931
    if-eqz v2, :cond_5

    .line 349932
    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->h:LX/1xv;

    .line 349933
    iget-object v3, v2, LX/1xv;->p:LX/1xx;

    move-object v2, v3

    .line 349934
    iget-object v3, p0, LX/1y0;->a:LX/1xy;

    iget-object v3, v3, LX/1xy;->o:LX/1PT;

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    .line 349935
    iget-object v4, v2, LX/1xx;->g:LX/1Qt;

    if-ne v3, v4, :cond_3

    iget-object v4, v2, LX/1xx;->f:LX/1nV;

    if-nez v4, :cond_4

    .line 349936
    :cond_3
    new-instance v4, LX/3CG;

    invoke-direct {v4, v2, v3}, LX/3CG;-><init>(LX/1xx;LX/1Qt;)V

    iput-object v4, v2, LX/1xx;->f:LX/1nV;

    .line 349937
    iput-object v3, v2, LX/1xx;->g:LX/1Qt;

    .line 349938
    :cond_4
    iget-object v4, v2, LX/1xx;->f:LX/1nV;

    move-object v2, v4

    .line 349939
    iput-object v2, v1, LX/1y2;->k:LX/1nV;

    .line 349940
    :cond_5
    :goto_2
    move-object v1, v1

    .line 349941
    invoke-virtual {v1}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v1

    .line 349942
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->g:LX/1xc;

    invoke-virtual {v2, v0, v1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 349943
    :goto_3
    new-instance v1, LX/1yT;

    invoke-static {v0}, LX/1yU;->a(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v1

    :cond_6
    move-object v0, v1

    .line 349944
    goto :goto_3

    .line 349945
    :cond_7
    iget-object v1, p0, LX/1y0;->a:LX/1xy;

    iget-object v1, v1, LX/1xy;->h:LX/1xv;

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-object v2, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/1y0;->a:LX/1xy;

    iget-boolean v3, v3, LX/1xy;->n:Z

    invoke-virtual {v1, v2, v4, v3}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)LX/1y2;

    move-result-object v1

    invoke-virtual {v1}, LX/1y2;->c()LX/1y2;

    move-result-object v1

    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget v2, v2, LX/1xy;->k:I

    .line 349946
    iput v2, v1, LX/1y2;->h:I

    .line 349947
    move-object v1, v1

    .line 349948
    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-boolean v2, v2, LX/1xy;->l:Z

    if-nez v2, :cond_8

    .line 349949
    invoke-virtual {v1}, LX/1y2;->a()LX/1y2;

    .line 349950
    :cond_8
    iget-object v2, p0, LX/1y0;->a:LX/1xy;

    iget-boolean v2, v2, LX/1xy;->m:Z

    if-eqz v2, :cond_5

    .line 349951
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/1y2;->i:Z

    .line 349952
    goto :goto_2

    :cond_9
    const/4 v3, 0x0

    goto :goto_1

    .line 349953
    :cond_a
    iget-object v3, v2, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349954
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 349955
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->ac()Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupConfigurationsConnection;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_4
    if-ge v5, v7, :cond_c

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLConfiguration;

    .line 349956
    if-eqz v3, :cond_b

    const-string v8, "gk_groups_member_bios"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLConfiguration;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 349957
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLConfiguration;->a()Z

    move-result v3

    goto/16 :goto_0

    .line 349958
    :cond_b
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4

    :cond_c
    move v3, v4

    .line 349959
    goto/16 :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349960
    iget-object v0, p0, LX/1y0;->b:Ljava/lang/String;

    return-object v0
.end method
