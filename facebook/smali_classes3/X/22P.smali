.class public LX/22P;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/32z;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/330;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 359628
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/22P;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/330;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359625
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 359626
    iput-object p1, p0, LX/22P;->b:LX/0Ot;

    .line 359627
    return-void
.end method

.method public static a(LX/0QB;)LX/22P;
    .locals 4

    .prologue
    .line 359614
    const-class v1, LX/22P;

    monitor-enter v1

    .line 359615
    :try_start_0
    sget-object v0, LX/22P;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359616
    sput-object v2, LX/22P;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359619
    new-instance v3, LX/22P;

    const/16 p0, 0x161

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/22P;-><init>(LX/0Ot;)V

    .line 359620
    move-object v0, v3

    .line 359621
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359622
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359623
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359612
    invoke-static {}, LX/1dS;->b()V

    .line 359613
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359629
    iget-object v0, p0, LX/22P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 359630
    new-instance v0, LX/331;

    invoke-direct {v0, p1}, LX/331;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 359631
    return-object v0
.end method

.method public final c(LX/1De;)LX/32z;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 359604
    new-instance v1, LX/32y;

    invoke-direct {v1, p0}, LX/32y;-><init>(LX/22P;)V

    .line 359605
    sget-object v2, LX/22P;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/32z;

    .line 359606
    if-nez v2, :cond_0

    .line 359607
    new-instance v2, LX/32z;

    invoke-direct {v2}, LX/32z;-><init>()V

    .line 359608
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/32z;->a$redex0(LX/32z;LX/1De;IILX/32y;)V

    .line 359609
    move-object v1, v2

    .line 359610
    move-object v0, v1

    .line 359611
    return-object v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 359603
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 359597
    check-cast p3, LX/32y;

    .line 359598
    iget-object v0, p0, LX/22P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/331;

    iget-object v0, p3, LX/32y;->a:LX/1aZ;

    iget-object v1, p3, LX/32y;->b:Landroid/graphics/PointF;

    iget v2, p3, LX/32y;->c:I

    .line 359599
    invoke-virtual {p2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object p0

    check-cast p0, LX/1af;

    invoke-virtual {p0, v1}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 359600
    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 359601
    invoke-virtual {p2, v2}, LX/331;->setContentId(I)V

    .line 359602
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 359594
    iget-object v0, p0, LX/22P;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/331;

    .line 359595
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 359596
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 359593
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 359592
    const/16 v0, 0xf

    return v0
.end method
