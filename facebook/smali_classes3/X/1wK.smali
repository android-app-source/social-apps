.class public interface abstract LX/1wK;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(LX/20X;)Landroid/view/View;
.end method

.method public abstract a()V
.end method

.method public abstract setBottomDividerStyle(LX/1Wl;)V
.end method

.method public abstract setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setButtonContainerHeight(I)V
.end method

.method public abstract setButtonWeights([F)V
.end method

.method public abstract setButtons(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setDownstateType(LX/1Wk;)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setHasCachedComments(Z)V
.end method

.method public abstract setIsLiked(Z)V
.end method

.method public abstract setOnButtonClickedListener(LX/20Z;)V
.end method

.method public abstract setShowIcons(Z)V
.end method

.method public abstract setSprings(Ljava/util/EnumMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setTopDividerStyle(LX/1Wl;)V
.end method
