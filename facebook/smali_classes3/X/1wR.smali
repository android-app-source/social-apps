.class public final LX/1wR;
.super LX/1wS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1wR;


# direct methods
.method public constructor <init>(LX/13J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 345884
    invoke-direct {p0, p1}, LX/1wS;-><init>(LX/13J;)V

    .line 345885
    return-void
.end method

.method public static a(LX/0QB;)LX/1wR;
    .locals 4

    .prologue
    .line 345886
    sget-object v0, LX/1wR;->b:LX/1wR;

    if-nez v0, :cond_1

    .line 345887
    const-class v1, LX/1wR;

    monitor-enter v1

    .line 345888
    :try_start_0
    sget-object v0, LX/1wR;->b:LX/1wR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 345889
    if-eqz v2, :cond_0

    .line 345890
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 345891
    new-instance p0, LX/1wR;

    const-class v3, LX/13J;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13J;

    invoke-direct {p0, v3}, LX/1wR;-><init>(LX/13J;)V

    .line 345892
    move-object v0, p0

    .line 345893
    sput-object v0, LX/1wR;->b:LX/1wR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345894
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 345895
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345896
    :cond_1
    sget-object v0, LX/1wR;->b:LX/1wR;

    return-object v0

    .line 345897
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 345898
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345899
    const-string v0, "1818"

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345900
    const-string v0, "FB4A Full Screen Interstitial"

    return-object v0
.end method
