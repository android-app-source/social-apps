.class public LX/1zZ;
.super LX/1n4;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/20B;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20C;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 353707
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1zZ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/20C;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353708
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 353709
    iput-object p1, p0, LX/1zZ;->b:LX/0Ot;

    .line 353710
    return-void
.end method

.method public static a(LX/0QB;)LX/1zZ;
    .locals 4

    .prologue
    .line 353711
    const-class v1, LX/1zZ;

    monitor-enter v1

    .line 353712
    :try_start_0
    sget-object v0, LX/1zZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 353713
    sput-object v2, LX/1zZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 353714
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353715
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 353716
    new-instance v3, LX/1zZ;

    const/16 p0, 0x7b1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1zZ;-><init>(LX/0Ot;)V

    .line 353717
    move-object v0, v3

    .line 353718
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 353719
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1zZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353720
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 353721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 353722
    check-cast p2, LX/20A;

    .line 353723
    iget-object v0, p0, LX/1zZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20C;

    iget-object v1, p2, LX/20A;->a:Ljava/util/List;

    iget-boolean v2, p2, LX/20A;->b:Z

    .line 353724
    sget-object p0, LX/20C;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/20D;

    .line 353725
    if-nez p0, :cond_0

    .line 353726
    iget-object p0, v0, LX/20C;->b:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/20D;

    .line 353727
    :cond_0
    invoke-virtual {p0, v1}, LX/20D;->a(Ljava/util/List;)V

    .line 353728
    invoke-virtual {p0, v2}, LX/20D;->a(Z)V

    .line 353729
    move-object v0, p0

    .line 353730
    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 1

    .prologue
    .line 353731
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 353732
    iget-object v0, p0, LX/1zZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 353733
    sget-object v0, LX/20C;->a:LX/0Zi;

    check-cast p2, LX/20D;

    invoke-virtual {v0, p2}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 353734
    return-void
.end method
