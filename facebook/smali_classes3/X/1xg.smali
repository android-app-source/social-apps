.class public LX/1xg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Uf;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1xh;

.field private final h:LX/1xi;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Uf;LX/1xh;LX/1xi;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349253
    iput-object p2, p0, LX/1xg;->e:LX/1Uf;

    .line 349254
    iput-object p3, p0, LX/1xg;->g:LX/1xh;

    .line 349255
    iput-object p4, p0, LX/1xg;->h:LX/1xi;

    .line 349256
    const v0, 0x7f081015

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xg;->a:LX/0Ot;

    .line 349257
    const v0, 0x7f081014

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xg;->b:LX/0Ot;

    .line 349258
    const v0, 0x7f080fc7

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xg;->c:LX/0Ot;

    .line 349259
    const v0, 0x7f081a29

    invoke-static {p1, v0}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xg;->d:LX/0Ot;

    .line 349260
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b091e

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/res/Resources;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xg;->f:LX/0Ot;

    .line 349261
    return-void
.end method

.method public static a(LX/0QB;)LX/1xg;
    .locals 7

    .prologue
    .line 349241
    const-class v1, LX/1xg;

    monitor-enter v1

    .line 349242
    :try_start_0
    sget-object v0, LX/1xg;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349243
    sput-object v2, LX/1xg;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349244
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349245
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349246
    new-instance p0, LX/1xg;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v4

    check-cast v4, LX/1Uf;

    invoke-static {v0}, LX/1xh;->a(LX/0QB;)LX/1xh;

    move-result-object v5

    check-cast v5, LX/1xh;

    invoke-static {v0}, LX/1xi;->a(LX/0QB;)LX/1xi;

    move-result-object v6

    check-cast v6, LX/1xi;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1xg;-><init>(Landroid/content/Context;LX/1Uf;LX/1xh;LX/1xi;)V

    .line 349247
    move-object v0, p0

    .line 349248
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349249
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349250
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349251
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/CharSequence;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 349211
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349212
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349213
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 349214
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 349215
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349216
    iget-object v2, p0, LX/1xg;->e:LX/1Uf;

    const/4 v4, 0x0

    .line 349217
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 349218
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->l()Ljava/lang/String;

    move-result-object v9

    .line 349219
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 349220
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 349221
    :cond_0
    :goto_0
    invoke-static {v0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 349222
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 349223
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 349224
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349225
    :cond_1
    :goto_1
    invoke-static {p1}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 349226
    invoke-static {p1}, LX/14w;->t(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    invoke-static {p0, v0}, LX/1xg;->a(LX/1xg;Lcom/facebook/graphql/model/GraphQLSponsoredData;)Ljava/lang/String;

    move-result-object v0

    .line 349227
    invoke-static {p0, v1, v0}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349228
    :cond_2
    return-object v1

    .line 349229
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 349230
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 349231
    :cond_4
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 349232
    const/4 v3, -0x1

    if-ne v10, v3, :cond_5

    .line 349233
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 349234
    :cond_5
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 349235
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->q()Ljava/lang/String;

    move-result-object v5

    .line 349236
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 349237
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0

    .line 349238
    :cond_6
    new-instance v3, LX/2xy;

    iget-object v6, v2, LX/1Uf;->o:LX/17U;

    iget-object v7, v2, LX/1Uf;->j:Landroid/content/Context;

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v8

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, LX/2xy;-><init>(LX/1Uf;Ljava/lang/String;LX/17U;Landroid/content/Context;LX/0lF;)V

    .line 349239
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v10

    const/16 v5, 0x21

    invoke-virtual {v1, v3, v10, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 349240
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto/16 :goto_0
.end method

.method public static a(LX/1xg;Lcom/facebook/graphql/model/GraphQLSponsoredData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 349205
    if-nez p1, :cond_0

    .line 349206
    const/4 v0, 0x0

    .line 349207
    :goto_0
    return-object v0

    .line 349208
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 349209
    const-string v0, ""

    goto :goto_0

    .line 349210
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1xg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/1xg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 349200
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 349201
    :cond_0
    :goto_0
    return-void

    .line 349202
    :cond_1
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 349203
    invoke-virtual {p0, p1}, LX/1xg;->a(Landroid/text/SpannableStringBuilder;)V

    .line 349204
    :cond_2
    invoke-static {p2}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)V
    .locals 3

    .prologue
    .line 349194
    invoke-static {p1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 349195
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 349196
    :cond_0
    :goto_0
    return-void

    .line 349197
    :cond_1
    iget-object v1, p0, LX/1xg;->g:LX/1xh;

    invoke-virtual {v1}, LX/1xh;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1xg;->g:LX/1xh;

    invoke-virtual {v1}, LX/1xh;->g()LX/0Rf;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const-string v1, "good_friends"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1xg;->h:LX/1xi;

    invoke-virtual {v0}, LX/1xi;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 349198
    :cond_3
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 349199
    invoke-virtual {p0, p2}, LX/1xg;->a(Landroid/text/SpannableStringBuilder;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 349184
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349185
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349186
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 349187
    invoke-static {p0, v1, p2}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349188
    invoke-direct {p0, p1}, LX/1xg;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {p0, v1, v2}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349189
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 349190
    const/4 v0, 0x0

    .line 349191
    :goto_0
    return-object v0

    .line 349192
    :cond_0
    invoke-direct {p0, v0, v1}, LX/1xg;->b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)V

    move-object v0, v1

    .line 349193
    goto :goto_0
.end method

.method public final a(Landroid/text/SpannableStringBuilder;)V
    .locals 4

    .prologue
    .line 349180
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 349181
    iget-object v0, p0, LX/1xg;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 349182
    new-instance v0, LX/1z1;

    iget-object v2, p0, LX/1xg;->f:LX/0Ot;

    invoke-direct {v0, v2}, LX/1z1;-><init>(LX/0Ot;)V

    add-int/lit8 v2, v1, 0x1

    const/16 v3, 0x11

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 349183
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349159
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 349160
    :cond_0
    :goto_0
    return-void

    .line 349161
    :cond_1
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 349162
    if-eqz v0, :cond_0

    .line 349163
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 349164
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bL()Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCopyrightBlockInfo;->a()Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 349165
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 349166
    iget-object v0, p0, LX/1xg;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {p0, p2, v0}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349167
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 349168
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/high16 v2, -0x10000

    invoke-direct {v0, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 349169
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349170
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349171
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 349172
    invoke-static {p1}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349173
    invoke-static {p1}, LX/14w;->t(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    .line 349174
    invoke-static {p0, v2}, LX/1xg;->a(LX/1xg;Lcom/facebook/graphql/model/GraphQLSponsoredData;)Ljava/lang/String;

    move-result-object v2

    .line 349175
    if-eqz v2, :cond_0

    .line 349176
    invoke-static {p0, v1, v2}, LX/1xg;->a(LX/1xg;Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)V

    .line 349177
    :cond_0
    move-object v1, v1

    .line 349178
    invoke-direct {p0, v0, v1}, LX/1xg;->b(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)V

    .line 349179
    return-object v1
.end method
