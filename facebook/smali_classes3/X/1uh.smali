.class public LX/1uh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private b:I

.field private c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 341376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341377
    iput-object p1, p0, LX/1uh;->a:Landroid/view/View;

    .line 341378
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 341379
    invoke-static {p0}, LX/0vv;->r(Landroid/view/View;)F

    move-result v0

    .line 341380
    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v1, v0

    invoke-static {p0, v1}, LX/0vv;->b(Landroid/view/View;F)V

    .line 341381
    invoke-static {p0, v0}, LX/0vv;->b(Landroid/view/View;F)V

    .line 341382
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 341383
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    iget v1, p0, LX/1uh;->d:I

    iget-object v2, p0, LX/1uh;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget v3, p0, LX/1uh;->b:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, LX/0vv;->g(Landroid/view/View;I)V

    .line 341384
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    iget v1, p0, LX/1uh;->e:I

    iget-object v2, p0, LX/1uh;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, p0, LX/1uh;->c:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 341385
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 341386
    if-eqz v1, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 341387
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 341388
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    .line 341389
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    invoke-static {v0}, LX/1uh;->a(Landroid/view/View;)V

    .line 341390
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 341391
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 341392
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, LX/1uh;->a(Landroid/view/View;)V

    .line 341393
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 341394
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, LX/1uh;->b:I

    .line 341395
    iget-object v0, p0, LX/1uh;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, p0, LX/1uh;->c:I

    .line 341396
    invoke-direct {p0}, LX/1uh;->d()V

    .line 341397
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 341398
    iget v0, p0, LX/1uh;->d:I

    if-eq v0, p1, :cond_0

    .line 341399
    iput p1, p0, LX/1uh;->d:I

    .line 341400
    invoke-direct {p0}, LX/1uh;->d()V

    .line 341401
    const/4 v0, 0x1

    .line 341402
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 341403
    iget v0, p0, LX/1uh;->e:I

    if-eq v0, p1, :cond_0

    .line 341404
    iput p1, p0, LX/1uh;->e:I

    .line 341405
    invoke-direct {p0}, LX/1uh;->d()V

    .line 341406
    const/4 v0, 0x1

    .line 341407
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
