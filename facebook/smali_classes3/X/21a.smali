.class public final enum LX/21a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21a;

.field public static final enum ICON_ONLY_EQUAL_WIDTH:LX/21a;

.field public static final enum TEXT_AND_ICON_EQUAL_WIDTH:LX/21a;

.field public static final enum TEXT_AND_ICON_VARIABLE_WIDTH:LX/21a;

.field public static final enum TEXT_ONLY_EQUAL_WIDTH:LX/21a;

.field public static final enum TEXT_ONLY_VARIABLE_WIDTH:LX/21a;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 357358
    new-instance v0, LX/21a;

    const-string v1, "TEXT_AND_ICON_EQUAL_WIDTH"

    invoke-direct {v0, v1, v2}, LX/21a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21a;->TEXT_AND_ICON_EQUAL_WIDTH:LX/21a;

    .line 357359
    new-instance v0, LX/21a;

    const-string v1, "TEXT_AND_ICON_VARIABLE_WIDTH"

    invoke-direct {v0, v1, v3}, LX/21a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21a;->TEXT_AND_ICON_VARIABLE_WIDTH:LX/21a;

    .line 357360
    new-instance v0, LX/21a;

    const-string v1, "ICON_ONLY_EQUAL_WIDTH"

    invoke-direct {v0, v1, v4}, LX/21a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21a;->ICON_ONLY_EQUAL_WIDTH:LX/21a;

    .line 357361
    new-instance v0, LX/21a;

    const-string v1, "TEXT_ONLY_EQUAL_WIDTH"

    invoke-direct {v0, v1, v5}, LX/21a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21a;->TEXT_ONLY_EQUAL_WIDTH:LX/21a;

    .line 357362
    new-instance v0, LX/21a;

    const-string v1, "TEXT_ONLY_VARIABLE_WIDTH"

    invoke-direct {v0, v1, v6}, LX/21a;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21a;->TEXT_ONLY_VARIABLE_WIDTH:LX/21a;

    .line 357363
    const/4 v0, 0x5

    new-array v0, v0, [LX/21a;

    sget-object v1, LX/21a;->TEXT_AND_ICON_EQUAL_WIDTH:LX/21a;

    aput-object v1, v0, v2

    sget-object v1, LX/21a;->TEXT_AND_ICON_VARIABLE_WIDTH:LX/21a;

    aput-object v1, v0, v3

    sget-object v1, LX/21a;->ICON_ONLY_EQUAL_WIDTH:LX/21a;

    aput-object v1, v0, v4

    sget-object v1, LX/21a;->TEXT_ONLY_EQUAL_WIDTH:LX/21a;

    aput-object v1, v0, v5

    sget-object v1, LX/21a;->TEXT_ONLY_VARIABLE_WIDTH:LX/21a;

    aput-object v1, v0, v6

    sput-object v0, LX/21a;->$VALUES:[LX/21a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 357364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/21a;
    .locals 1

    .prologue
    .line 357365
    const-class v0, LX/21a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21a;

    return-object v0
.end method

.method public static values()[LX/21a;
    .locals 1

    .prologue
    .line 357366
    sget-object v0, LX/21a;->$VALUES:[LX/21a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21a;

    return-object v0
.end method
