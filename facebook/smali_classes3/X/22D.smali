.class public LX/22D;
.super Landroid/text/style/ImageSpan;
.source ""


# static fields
.field private static a:C


# instance fields
.field public b:Lcom/facebook/ui/emoji/model/Emoji;

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 359275
    const v0, 0xfeff

    sput-char v0, LX/22D;->a:C

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/BitmapDrawable;Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 1

    .prologue
    .line 359276
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 359277
    iput-object p2, p0, LX/22D;->b:Lcom/facebook/ui/emoji/model/Emoji;

    .line 359278
    return-void
.end method

.method private static a(Landroid/graphics/Paint;I)I
    .locals 2

    .prologue
    .line 359279
    check-cast p0, Landroid/text/TextPaint;

    .line 359280
    int-to-float v0, p1

    iget v1, p0, Landroid/text/TextPaint;->density:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static a(Landroid/graphics/Paint;Landroid/graphics/Rect;)I
    .locals 2

    .prologue
    .line 359281
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    const/4 v1, 0x2

    invoke-static {p0, v1}, LX/22D;->a(Landroid/graphics/Paint;I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static a(Ljava/lang/CharSequence;II)Z
    .locals 3

    .prologue
    .line 359282
    move v0, p1

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 359283
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    sget-char v2, LX/22D;->a:C

    if-eq v1, v2, :cond_0

    .line 359284
    const/4 v0, 0x0

    .line 359285
    :goto_1
    return v0

    .line 359286
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359287
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private b()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 359288
    iget-object v1, p0, LX/22D;->c:Ljava/lang/ref/WeakReference;

    .line 359289
    const/4 v0, 0x0

    .line 359290
    if-eqz v1, :cond_0

    .line 359291
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 359292
    :cond_0
    if-nez v0, :cond_1

    .line 359293
    invoke-virtual {p0}, LX/22D;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 359294
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/22D;->c:Ljava/lang/ref/WeakReference;

    .line 359295
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 3

    .prologue
    .line 359296
    invoke-static {p2, p3, p4}, LX/22D;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359297
    :goto_0
    return-void

    .line 359298
    :cond_0
    invoke-direct {p0}, LX/22D;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 359299
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 359300
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 359301
    invoke-static {p9, v1}, LX/22D;->a(Landroid/graphics/Paint;Landroid/graphics/Rect;)I

    move-result v1

    .line 359302
    sub-int v1, p8, v1

    .line 359303
    const/4 v2, 0x1

    invoke-static {p9, v2}, LX/22D;->a(Landroid/graphics/Paint;I)I

    move-result v2

    .line 359304
    int-to-float v2, v2

    add-float/2addr v2, p5

    int-to-float v1, v1

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 359305
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 359306
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 4
    .param p5    # Landroid/graphics/Paint$FontMetricsInt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 359307
    invoke-static {p2, p3, p4}, LX/22D;->a(Ljava/lang/CharSequence;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359308
    :goto_0
    return v0

    .line 359309
    :cond_0
    invoke-direct {p0}, LX/22D;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 359310
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 359311
    const/4 v2, 0x2

    invoke-static {p1, v2}, LX/22D;->a(Landroid/graphics/Paint;I)I

    move-result v2

    .line 359312
    if-eqz p5, :cond_1

    .line 359313
    invoke-static {p1, v1}, LX/22D;->a(Landroid/graphics/Paint;Landroid/graphics/Rect;)I

    move-result v3

    .line 359314
    neg-int v3, v3

    iput v3, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 359315
    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 359316
    iget v3, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v3, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 359317
    iput v0, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 359318
    :cond_1
    iget v0, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    goto :goto_0
.end method
