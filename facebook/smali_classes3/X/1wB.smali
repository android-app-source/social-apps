.class public LX/1wB;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22B;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1wB",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/22B;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 345047
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 345048
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1wB;->b:LX/0Zi;

    .line 345049
    iput-object p1, p0, LX/1wB;->a:LX/0Ot;

    .line 345050
    return-void
.end method

.method public static a(LX/0QB;)LX/1wB;
    .locals 4

    .prologue
    .line 345072
    const-class v1, LX/1wB;

    monitor-enter v1

    .line 345073
    :try_start_0
    sget-object v0, LX/1wB;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 345074
    sput-object v2, LX/1wB;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 345075
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345076
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 345077
    new-instance v3, LX/1wB;

    const/16 p0, 0x723

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1wB;-><init>(LX/0Ot;)V

    .line 345078
    move-object v0, v3

    .line 345079
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 345080
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1wB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345081
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 345082
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 345061
    check-cast p2, LX/229;

    .line 345062
    iget-object v0, p0, LX/1wB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22B;

    iget-object v1, p2, LX/229;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/229;->b:LX/1Pq;

    .line 345063
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {v0, p1, v1, v2}, LX/22B;->b(LX/22B;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 345064
    check-cast v2, LX/1Pk;

    invoke-interface {v2}, LX/1Pk;->e()LX/1SX;

    move-result-object v4

    .line 345065
    iget-object p0, v0, LX/22B;->a:LX/1vb;

    invoke-virtual {p0, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object p0

    iget-object p2, v0, LX/22B;->c:LX/1VE;

    invoke-virtual {p2, v1, v4}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v4

    .line 345066
    iget-object p0, v0, LX/22B;->d:LX/1VI;

    invoke-virtual {p0}, LX/1VI;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 345067
    const p0, 0x7f0a00aa

    .line 345068
    iget-object p2, v4, LX/1vd;->a:LX/1vc;

    invoke-virtual {v4, p0}, LX/1Dp;->d(I)I

    move-result v0

    iput v0, p2, LX/1vc;->b:I

    .line 345069
    :cond_0
    move-object v4, v4

    .line 345070
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 345071
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 345059
    invoke-static {}, LX/1dS;->b()V

    .line 345060
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/22A;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1wB",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 345051
    new-instance v1, LX/229;

    invoke-direct {v1, p0}, LX/229;-><init>(LX/1wB;)V

    .line 345052
    iget-object v2, p0, LX/1wB;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/22A;

    .line 345053
    if-nez v2, :cond_0

    .line 345054
    new-instance v2, LX/22A;

    invoke-direct {v2, p0}, LX/22A;-><init>(LX/1wB;)V

    .line 345055
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/22A;->a$redex0(LX/22A;LX/1De;IILX/229;)V

    .line 345056
    move-object v1, v2

    .line 345057
    move-object v0, v1

    .line 345058
    return-object v0
.end method
