.class public LX/1zt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1zt;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:LX/1zt;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final c:LX/1zt;

.field public static final d:LX/1zt;


# instance fields
.field public final e:I

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Z

.field private i:LX/1zx;

.field public j:LX/1zx;

.field public k:LX/1zx;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v3, -0x1000000

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 354254
    invoke-static {v1}, LX/1zt;->a(I)LX/1zt;

    move-result-object v0

    sput-object v0, LX/1zt;->a:LX/1zt;

    .line 354255
    invoke-static {v4}, LX/1zt;->a(I)LX/1zt;

    move-result-object v0

    sput-object v0, LX/1zt;->b:LX/1zt;

    .line 354256
    new-instance v0, LX/1zt;

    const-string v2, "None"

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, LX/1zt;-><init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    sput-object v0, LX/1zt;->c:LX/1zt;

    .line 354257
    new-instance v0, LX/1zt;

    const/4 v1, -0x1

    const-string v2, "Unknown"

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, LX/1zt;-><init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    sput-object v0, LX/1zt;->d:LX/1zt;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V
    .locals 7

    .prologue
    .line 354234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354235
    iput p1, p0, LX/1zt;->e:I

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 354236
    invoke-virtual/range {v0 .. v6}, LX/1zt;->a(Ljava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    .line 354237
    return-void
.end method

.method private static a(I)LX/1zt;
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 354253
    new-instance v0, LX/1zt;

    const/4 v3, -0x1

    const/4 v4, 0x1

    move v1, p0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, LX/1zt;-><init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 354250
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-nez v0, :cond_1

    .line 354251
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 354252
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 354249
    iget v0, p0, LX/1zt;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V
    .locals 0

    .prologue
    .line 354242
    iput-object p1, p0, LX/1zt;->f:Ljava/lang/String;

    .line 354243
    iput p2, p0, LX/1zt;->g:I

    .line 354244
    iput-boolean p3, p0, LX/1zt;->h:Z

    .line 354245
    iput-object p4, p0, LX/1zt;->i:LX/1zx;

    .line 354246
    iput-object p5, p0, LX/1zt;->j:LX/1zx;

    .line 354247
    iput-object p6, p0, LX/1zt;->k:LX/1zx;

    .line 354248
    return-void
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 354241
    iget-object v0, p0, LX/1zt;->i:LX/1zx;

    invoke-interface {v0}, LX/1zx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 354240
    iget-object v0, p0, LX/1zt;->j:LX/1zx;

    invoke-interface {v0}, LX/1zx;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final h()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 354239
    iget-object v0, p0, LX/1zt;->j:LX/1zx;

    invoke-interface {v0}, LX/1zx;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 354238
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FeedbackReaction{id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/1zt;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1zt;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isDeprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/1zt;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
