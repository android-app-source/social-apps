.class public interface abstract LX/21o;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .param p1    # Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Z)V
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(II)Z
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract getPendingComment()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract setIsVisible(Z)V
.end method

.method public abstract setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V
.end method

.method public abstract setMediaPickerListener(LX/9DF;)V
.end method

.method public abstract setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
.end method
