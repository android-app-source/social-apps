.class public LX/21v;
.super LX/1jo;
.source ""


# instance fields
.field public d:LX/1jk;

.field public e:LX/1jr;

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1jk;LX/1w2;LX/0uc;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 358516
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, LX/1jo;-><init>(LX/1jm;I)V

    .line 358517
    iget-object v0, p2, LX/1w2;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/1w2;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 358518
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Missing outputs field definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358519
    :cond_1
    iget-object v0, p2, LX/1w2;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 358520
    new-array v4, v3, [LX/0uN;

    .line 358521
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/21v;->f:Ljava/util/Map;

    .line 358522
    iget-object v0, p2, LX/1w2;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1w6;

    .line 358523
    iget-object v6, v0, LX/1w6;->b:Ljava/lang/String;

    invoke-static {v6}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v6

    aput-object v6, v4, v1

    .line 358524
    iget-object v6, p0, LX/21v;->f:Ljava/util/Map;

    iget-object v0, v0, LX/1w6;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358525
    aget-object v0, v4, v1

    if-nez v0, :cond_2

    .line 358526
    new-instance v0, LX/5MH;

    const-string v1, "Missing output type definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358527
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 358528
    goto :goto_0

    .line 358529
    :cond_3
    iget-object v0, p2, LX/1w2;->g:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p2, LX/1w2;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_5

    .line 358530
    :cond_4
    new-instance v0, LX/5MH;

    const-string v1, "Missing default value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358531
    :cond_5
    new-array v5, v3, [LX/0uE;

    .line 358532
    iget-object v0, p2, LX/1w2;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21u;

    .line 358533
    iget-object v1, p0, LX/21v;->f:Ljava/util/Map;

    iget-object v7, v0, LX/21u;->a:Ljava/lang/String;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 358534
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-lt v7, v3, :cond_7

    .line 358535
    :cond_6
    new-instance v0, LX/5MH;

    const-string v1, "Undeclaed output param"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358536
    :cond_7
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    new-instance v8, LX/0uE;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v4, v1

    iget-object v0, v0, LX/21u;->b:Ljava/lang/String;

    invoke-direct {v8, v1, v0}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    aput-object v8, v5, v7

    goto :goto_1

    .line 358537
    :cond_8
    add-int/lit8 v2, v2, 0x1

    :cond_9
    if-ge v2, v3, :cond_a

    .line 358538
    aget-object v0, v5, v2

    if-nez v0, :cond_8

    .line 358539
    new-instance v0, LX/5MH;

    const-string v1, "Missing default value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358540
    :cond_a
    iput-object p1, p0, LX/21v;->d:LX/1jk;

    .line 358541
    new-instance v0, LX/1jr;

    invoke-direct {v0, p0, v5, p3}, LX/1jr;-><init>(LX/1jp;[LX/0uE;LX/0uc;)V

    iput-object v0, p0, LX/21v;->e:LX/1jr;

    .line 358542
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 358543
    iget-object v0, p0, LX/21v;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 358544
    if-nez v0, :cond_0

    .line 358545
    const/4 v0, -0x1

    .line 358546
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a()LX/1jk;
    .locals 1

    .prologue
    .line 358547
    iget-object v0, p0, LX/21v;->d:LX/1jk;

    return-object v0
.end method

.method public final a(LX/8K8;)LX/1jr;
    .locals 1
    .param p1    # LX/8K8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 358548
    iget-object v0, p0, LX/21v;->e:LX/1jr;

    return-object v0
.end method
