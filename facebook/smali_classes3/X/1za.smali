.class public LX/1za;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/1za;


# instance fields
.field private final b:LX/0hL;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1zf;

.field private final e:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 353763
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 353764
    invoke-virtual {v0}, LX/1sm;->a()LX/1sm;

    move-result-object v0

    new-instance v1, LX/1zd;

    invoke-direct {v1}, LX/1zd;-><init>()V

    invoke-virtual {v0, v1}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v0

    sput-object v0, LX/1za;->a:LX/1sm;

    return-void
.end method

.method public constructor <init>(LX/0hL;LX/0Or;LX/1zf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0hL;",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;",
            "LX/1zf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353772
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 353773
    new-instance v1, LX/205;

    invoke-direct {v1, p0}, LX/205;-><init>(LX/1za;)V

    invoke-virtual {v0, v1}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v0

    iput-object v0, p0, LX/1za;->e:LX/1sm;

    .line 353774
    iput-object p1, p0, LX/1za;->b:LX/0hL;

    .line 353775
    iput-object p2, p0, LX/1za;->c:LX/0Or;

    .line 353776
    iput-object p3, p0, LX/1za;->d:LX/1zf;

    .line 353777
    return-void
.end method

.method public static a(LX/0QB;)LX/1za;
    .locals 6

    .prologue
    .line 353778
    sget-object v0, LX/1za;->f:LX/1za;

    if-nez v0, :cond_1

    .line 353779
    const-class v1, LX/1za;

    monitor-enter v1

    .line 353780
    :try_start_0
    sget-object v0, LX/1za;->f:LX/1za;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353781
    if-eqz v2, :cond_0

    .line 353782
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353783
    new-instance v5, LX/1za;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v3

    check-cast v3, LX/0hL;

    const/16 v4, 0x7af

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v4

    check-cast v4, LX/1zf;

    invoke-direct {v5, v3, p0, v4}, LX/1za;-><init>(LX/0hL;LX/0Or;LX/1zf;)V

    .line 353784
    move-object v0, v5

    .line 353785
    sput-object v0, LX/1za;->f:LX/1za;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353786
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353787
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353788
    :cond_1
    sget-object v0, LX/1za;->f:LX/1za;

    return-object v0

    .line 353789
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 353765
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/1za;->a(Landroid/widget/TextView;Z)V

    .line 353766
    return-void
.end method

.method public static a(Landroid/widget/TextView;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 353767
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 353768
    if-eqz p1, :cond_0

    .line 353769
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 353770
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353735
    invoke-static {p1}, LX/207;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    .line 353736
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_2

    .line 353737
    :cond_0
    invoke-static {p1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, LX/1za;->d:LX/1zf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 353738
    :goto_0
    return-object v0

    .line 353739
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 353740
    goto :goto_0

    .line 353741
    :cond_2
    sget-object v1, LX/1za;->a:LX/1sm;

    iget-object v2, p0, LX/1za;->e:LX/1sm;

    invoke-virtual {v1, v2}, LX/1sm;->b(Ljava/util/Comparator;)LX/1sm;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1sm;->d(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v2

    .line 353742
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 353743
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 353744
    iget-object v5, p0, LX/1za;->d:LX/1zf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v6

    invoke-virtual {v5, v6}, LX/1zf;->a(I)LX/1zt;

    move-result-object v5

    .line 353745
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v0

    if-lez v0, :cond_3

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-eq v5, v0, :cond_3

    .line 353746
    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 353747
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 353748
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 353749
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;Z)V

    .line 353750
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/widget/TextView;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 353751
    invoke-virtual {p0, p1}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v1

    .line 353752
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353753
    invoke-static {p2}, LX/1za;->a(Landroid/widget/TextView;)V

    .line 353754
    :goto_0
    return-void

    .line 353755
    :cond_0
    if-eqz p3, :cond_1

    .line 353756
    invoke-virtual {p2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2, v0, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 353757
    :cond_1
    iget-object v0, p0, LX/1za;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20D;

    .line 353758
    invoke-virtual {v0, v1}, LX/20D;->a(Ljava/util/List;)V

    .line 353759
    invoke-virtual {p2}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 353760
    iget-object v2, p0, LX/1za;->b:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 353761
    const/4 v2, 0x0

    aget-object v2, v1, v2

    aget-object v3, v1, v3

    aget-object v1, v1, v4

    invoke-virtual {p2, v2, v3, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 353762
    :cond_2
    aget-object v2, v1, v3

    const/4 v3, 0x2

    aget-object v3, v1, v3

    aget-object v1, v1, v4

    invoke-virtual {p2, v0, v2, v3, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
