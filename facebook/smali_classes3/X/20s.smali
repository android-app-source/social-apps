.class public LX/20s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x3
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static q:LX/0Xm;


# instance fields
.field public b:LX/1zm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/20v;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0hB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Landroid/content/res/Resources;

.field public f:Landroid/widget/PopupWindow;

.field public g:LX/9aR;

.field public h:LX/9aS;

.field public i:LX/3K3;

.field public j:Landroid/widget/ImageView;

.field public k:I

.field private l:J

.field public m:LX/9Ug;

.field public final n:Ljava/lang/Runnable;

.field public final o:LX/20u;

.field private final p:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 356430
    const-class v0, LX/20s;

    sput-object v0, LX/20s;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 356423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356424
    const/4 v0, 0x0

    iput-object v0, p0, LX/20s;->i:LX/3K3;

    .line 356425
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/20s;->l:J

    .line 356426
    new-instance v0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$1;

    invoke-direct {v0, p0}, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$1;-><init>(LX/20s;)V

    iput-object v0, p0, LX/20s;->n:Ljava/lang/Runnable;

    .line 356427
    new-instance v0, LX/20t;

    invoke-direct {v0, p0}, LX/20t;-><init>(LX/20s;)V

    iput-object v0, p0, LX/20s;->o:LX/20u;

    .line 356428
    new-instance v0, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;

    invoke-direct {v0, p0}, Lcom/facebook/delights/floating/launcher/DelightsFloatingLauncher$3;-><init>(LX/20s;)V

    iput-object v0, p0, LX/20s;->p:Ljava/lang/Runnable;

    .line 356429
    return-void
.end method

.method public static a(LX/0QB;)LX/20s;
    .locals 6

    .prologue
    .line 356410
    const-class v1, LX/20s;

    monitor-enter v1

    .line 356411
    :try_start_0
    sget-object v0, LX/20s;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356412
    sput-object v2, LX/20s;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356413
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356414
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 356415
    new-instance p0, LX/20s;

    invoke-direct {p0}, LX/20s;-><init>()V

    .line 356416
    invoke-static {v0}, LX/1zm;->a(LX/0QB;)LX/1zm;

    move-result-object v3

    check-cast v3, LX/1zm;

    invoke-static {v0}, LX/20v;->a(LX/0QB;)LX/20v;

    move-result-object v4

    check-cast v4, LX/20v;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    .line 356417
    iput-object v3, p0, LX/20s;->b:LX/1zm;

    iput-object v4, p0, LX/20s;->c:LX/20v;

    iput-object v5, p0, LX/20s;->d:LX/0hB;

    .line 356418
    move-object v0, p0

    .line 356419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 356283
    iget-object v0, p0, LX/20s;->c:LX/20v;

    .line 356284
    invoke-virtual {v0}, LX/20v;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 356285
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0xf

    if-lt v1, p0, :cond_1

    iget-object v1, v0, LX/20v;->a:Landroid/content/Context;

    invoke-static {v1}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v1

    const/16 p0, 0x7da

    if-lt v1, p0, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 356286
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static d(LX/20s;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 356399
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 356400
    :try_start_0
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356401
    :cond_0
    :goto_0
    iput-object v3, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    .line 356402
    iput-object v3, p0, LX/20s;->j:Landroid/widget/ImageView;

    .line 356403
    iput-object v3, p0, LX/20s;->m:LX/9Ug;

    .line 356404
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    if-eqz v0, :cond_1

    .line 356405
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    invoke-virtual {v0, v3}, LX/9aR;->setListener(LX/9Aq;)V

    .line 356406
    iput-object v3, p0, LX/20s;->g:LX/9aR;

    .line 356407
    :cond_1
    return-void

    .line 356408
    :catch_0
    move-exception v0

    .line 356409
    sget-object v1, LX/20s;->a:Ljava/lang/Class;

    const-string v2, "Exception while trying to dismiss the popup window."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static d(LX/20s;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 356393
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    if-nez v0, :cond_0

    .line 356394
    new-instance v0, LX/9aR;

    invoke-direct {v0, p1}, LX/9aR;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/20s;->g:LX/9aR;

    .line 356395
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    new-instance v1, LX/Bhf;

    invoke-direct {v1, p0}, LX/Bhf;-><init>(LX/20s;)V

    invoke-virtual {v0, v1}, LX/9aR;->setListener(LX/9Aq;)V

    .line 356396
    :cond_0
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    if-eqz v0, :cond_1

    .line 356397
    iget-object v0, p0, LX/20s;->g:LX/9aR;

    invoke-virtual {v0}, LX/9aR;->a()V

    .line 356398
    :cond_1
    return-void
.end method

.method public static e(LX/20s;)V
    .locals 9

    .prologue
    const/16 v2, 0x50

    const/4 v7, 0x0

    .line 356371
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 356372
    :cond_0
    :goto_0
    return-void

    .line 356373
    :cond_1
    iget-object v0, p0, LX/20s;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/20s;->i:LX/3K3;

    if-eqz v0, :cond_0

    .line 356374
    iget-object v0, p0, LX/20s;->e:Landroid/content/res/Resources;

    const v1, 0x7f0215ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 356375
    if-eqz v0, :cond_0

    .line 356376
    invoke-virtual {v0, v7, v7, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 356377
    new-instance v1, LX/9Uk;

    invoke-direct {v1}, LX/9Uk;-><init>()V

    iget-object v2, p0, LX/20s;->i:LX/3K3;

    .line 356378
    iput-object v2, v1, LX/9Uk;->a:LX/3K3;

    .line 356379
    move-object v1, v1

    .line 356380
    iget-object v2, v1, LX/9Uk;->c:LX/9Uj;

    move-object v1, v2

    .line 356381
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/util/Pair;

    new-instance v3, Landroid/util/Pair;

    const-string v4, "love"

    new-instance v5, Landroid/util/Pair;

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {v5, v0, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v2, v7

    .line 356382
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, LX/9Uj;->b:Ljava/util/Map;

    .line 356383
    array-length v5, v2

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v3, v2, v4

    .line 356384
    iget-object v6, v1, LX/9Uj;->b:Ljava/util/Map;

    iget-object v7, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    new-instance v8, LX/9Ud;

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Matrix;

    invoke-direct {v8, v0, v3}, LX/9Ud;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356385
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 356386
    :cond_2
    move-object v0, v1

    .line 356387
    iget-object v1, v0, LX/9Uj;->a:LX/9Uk;

    invoke-virtual {v1}, LX/9Uk;->a()LX/9Ug;

    move-result-object v1

    move-object v0, v1

    .line 356388
    iput-object v0, p0, LX/20s;->m:LX/9Ug;

    .line 356389
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 356390
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/20s;->l:J

    sub-long/2addr v0, v2

    .line 356391
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iget-object v3, p0, LX/20s;->p:Ljava/lang/Runnable;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x244

    sub-long v0, v6, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const v4, -0x3f14eec

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 356392
    iget-object v0, p0, LX/20s;->j:Landroid/widget/ImageView;

    iget-object v1, p0, LX/20s;->m:LX/9Ug;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 356287
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 356288
    :cond_0
    iget-object v0, p0, LX/20s;->c:LX/20v;

    invoke-virtual {v0}, LX/20v;->a()Z

    move-result v0

    move v0, v0

    .line 356289
    if-nez v0, :cond_2

    .line 356290
    :cond_1
    :goto_0
    return-void

    .line 356291
    :cond_2
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 356292
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/20s;->l:J

    .line 356293
    invoke-static {p0}, LX/20s;->d(LX/20s;)V

    .line 356294
    invoke-direct {p0}, LX/20s;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 356295
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 356296
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, LX/20s;->e:Landroid/content/res/Resources;

    .line 356297
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 356298
    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 356299
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 356300
    :cond_3
    :goto_1
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    .line 356301
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 356302
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 356303
    const/4 v1, 0x1

    aget v0, v0, v1

    .line 356304
    iget-object v1, p0, LX/20s;->d:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    .line 356305
    iget-object v2, p0, LX/20s;->d:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    .line 356306
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 356307
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 356308
    iget-object v2, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 356309
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 356310
    sub-int/2addr v0, v3

    iput v0, p0, LX/20s;->k:I

    .line 356311
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1, v4, v4, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    .line 356312
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 356313
    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 356314
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 356315
    :cond_5
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 356316
    iget-object v1, p0, LX/20s;->h:LX/9aS;

    if-eqz v1, :cond_b

    .line 356317
    :cond_6
    :goto_3
    invoke-static {p0, v0}, LX/20s;->d(LX/20s;Landroid/content/Context;)V

    .line 356318
    const/4 v1, 0x0

    :goto_4
    const/16 v2, 0x14

    if-ge v1, v2, :cond_7

    .line 356319
    iget-object v2, p0, LX/20s;->g:LX/9aR;

    iget-object v3, p0, LX/20s;->h:LX/9aS;

    invoke-virtual {v2, v3}, LX/9aR;->a(LX/9aS;)V

    .line 356320
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 356321
    :cond_7
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    .line 356322
    iget-object v0, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1, v4, v4, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_0

    .line 356323
    :cond_8
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/20s;->j:Landroid/widget/ImageView;

    .line 356324
    iget-object v1, p0, LX/20s;->i:LX/3K3;

    if-nez v1, :cond_9

    .line 356325
    new-instance v1, LX/Bhg;

    invoke-direct {v1, p0}, LX/Bhg;-><init>(LX/20s;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "delights_choose_love_kf.json"

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 356326
    :goto_5
    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, LX/20s;->j:Landroid/widget/ImageView;

    invoke-direct {v1, v2, v6, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    .line 356327
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    .line 356328
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 356329
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 356330
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 356331
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356332
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    goto/16 :goto_1

    .line 356333
    :cond_9
    invoke-static {p0}, LX/20s;->e(LX/20s;)V

    goto :goto_5

    .line 356334
    :cond_a
    invoke-static {p0, v0}, LX/20s;->d(LX/20s;Landroid/content/Context;)V

    .line 356335
    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, LX/20s;->g:LX/9aR;

    invoke-direct {v1, v2, v5, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    .line 356336
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_5

    .line 356337
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 356338
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 356339
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 356340
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356341
    iget-object v1, p0, LX/20s;->f:Landroid/widget/PopupWindow;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    goto/16 :goto_2

    .line 356342
    :cond_b
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 356343
    const v2, 0x7f0b0fea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 356344
    const v3, 0x7f0b0feb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 356345
    const v5, 0x7f0b0fec

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 356346
    const v6, 0x7f0b0fed

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 356347
    const v7, 0x7f0b0fee

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 356348
    const v8, 0x7f0b0fef

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    .line 356349
    const v9, 0x7f0215ce

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 356350
    if-eqz v1, :cond_6

    .line 356351
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 356352
    new-instance v9, LX/9aS;

    invoke-direct {v9, v1}, LX/9aS;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v1, LX/9aW;

    invoke-direct {v1, v5, v6}, LX/9aW;-><init>(FF)V

    .line 356353
    iput-object v1, v9, LX/9aS;->d:LX/9aT;

    .line 356354
    move-object v1, v9

    .line 356355
    new-instance v5, LX/9aW;

    invoke-direct {v5, v7, v8}, LX/9aW;-><init>(FF)V

    .line 356356
    iput-object v5, v1, LX/9aS;->e:LX/9aT;

    .line 356357
    move-object v1, v1

    .line 356358
    new-instance v5, LX/9aW;

    invoke-direct {v5, v2, v3}, LX/9aW;-><init>(FF)V

    .line 356359
    iput-object v5, v1, LX/9aS;->g:LX/9aT;

    .line 356360
    move-object v1, v1

    .line 356361
    new-instance v2, LX/9aV;

    const/high16 v3, -0x3e600000    # -20.0f

    const/high16 v5, 0x41a00000    # 20.0f

    invoke-direct {v2, v3, v5}, LX/9aV;-><init>(FF)V

    .line 356362
    iput-object v2, v1, LX/9aS;->i:LX/9aT;

    .line 356363
    move-object v1, v1

    .line 356364
    sget-object v2, LX/9aW;->b:LX/9aW;

    .line 356365
    iput-object v2, v1, LX/9aS;->h:LX/9aT;

    .line 356366
    move-object v1, v1

    .line 356367
    new-instance v2, LX/9aW;

    const v3, 0x3f19999a    # 0.6f

    const/high16 v5, 0x3f400000    # 0.75f

    invoke-direct {v2, v3, v5}, LX/9aW;-><init>(FF)V

    .line 356368
    iput-object v2, v1, LX/9aS;->j:LX/9aT;

    .line 356369
    move-object v1, v1

    .line 356370
    iput-object v1, p0, LX/20s;->h:LX/9aS;

    goto/16 :goto_3

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
