.class public LX/1xJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1xJ;


# instance fields
.field private final a:LX/0W3;

.field private b:Z

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348140
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1xJ;->b:Z

    .line 348141
    iput-object p1, p0, LX/1xJ;->a:LX/0W3;

    .line 348142
    return-void
.end method

.method public static a(LX/0QB;)LX/1xJ;
    .locals 4

    .prologue
    .line 348143
    sget-object v0, LX/1xJ;->f:LX/1xJ;

    if-nez v0, :cond_1

    .line 348144
    const-class v1, LX/1xJ;

    monitor-enter v1

    .line 348145
    :try_start_0
    sget-object v0, LX/1xJ;->f:LX/1xJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 348146
    if-eqz v2, :cond_0

    .line 348147
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 348148
    new-instance p0, LX/1xJ;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/1xJ;-><init>(LX/0W3;)V

    .line 348149
    move-object v0, p0

    .line 348150
    sput-object v0, LX/1xJ;->f:LX/1xJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348151
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 348152
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 348153
    :cond_1
    sget-object v0, LX/1xJ;->f:LX/1xJ;

    return-object v0

    .line 348154
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 348155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized e(LX/1xJ;)V
    .locals 4

    .prologue
    .line 348156
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1xJ;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 348157
    :goto_0
    monitor-exit p0

    return-void

    .line 348158
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1xJ;->a:LX/0W3;

    sget-wide v2, LX/0X5;->c:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1xJ;->c:I

    .line 348159
    iget-object v0, p0, LX/1xJ;->a:LX/0W3;

    sget-wide v2, LX/0X5;->b:J

    const/4 v1, 0x5

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1xJ;->d:I

    .line 348160
    iget-object v0, p0, LX/1xJ;->a:LX/0W3;

    sget-wide v2, LX/0X5;->a:J

    const/16 v1, 0xc8

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/1xJ;->e:I

    .line 348161
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1xJ;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 348162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 348163
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/1xJ;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348164
    monitor-exit p0

    return-void

    .line 348165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
