.class public LX/1xa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xb;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static t:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/03V;

.field private final d:LX/1xc;

.field public final e:LX/0Zb;

.field public final f:LX/0gh;

.field private final g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final h:LX/17V;

.field private final i:LX/1xe;

.field private final j:LX/1nG;

.field private final k:LX/1e4;

.field private final l:LX/1xg;

.field private final m:LX/17Q;

.field private final n:LX/0wM;

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Aoa;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/1xh;

.field private final s:LX/1xi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 348968
    const-class v0, LX/1xb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1xa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/1xc;LX/0Zb;LX/0gh;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17V;LX/1xe;LX/1nG;LX/1e4;LX/1xg;LX/17Q;LX/0wM;LX/1xh;LX/0Ot;LX/1xi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1xc;",
            "LX/0Zb;",
            "LX/0gh;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/17V;",
            "Lcom/facebook/feedplugins/graphqlstory/header/HeaderTimeInfoFormatter;",
            "LX/1nG;",
            "Lcom/facebook/privacy/ui/PrivacyScopeResourceResolver;",
            "LX/1xg;",
            "LX/17Q;",
            "LX/0wM;",
            "LX/1xh;",
            "LX/0Ot",
            "<",
            "LX/Aoa;",
            ">;",
            "LX/1xi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349002
    iput-object p1, p0, LX/1xa;->b:Landroid/content/Context;

    .line 349003
    iput-object p2, p0, LX/1xa;->c:LX/03V;

    .line 349004
    iput-object p3, p0, LX/1xa;->d:LX/1xc;

    .line 349005
    iput-object p4, p0, LX/1xa;->e:LX/0Zb;

    .line 349006
    iput-object p5, p0, LX/1xa;->f:LX/0gh;

    .line 349007
    iput-object p6, p0, LX/1xa;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 349008
    iput-object p7, p0, LX/1xa;->h:LX/17V;

    .line 349009
    iput-object p8, p0, LX/1xa;->i:LX/1xe;

    .line 349010
    iput-object p9, p0, LX/1xa;->j:LX/1nG;

    .line 349011
    iput-object p10, p0, LX/1xa;->k:LX/1e4;

    .line 349012
    iput-object p11, p0, LX/1xa;->l:LX/1xg;

    .line 349013
    iput-object p12, p0, LX/1xa;->m:LX/17Q;

    .line 349014
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1xa;->n:LX/0wM;

    .line 349015
    move-object/from16 v0, p15

    iput-object v0, p0, LX/1xa;->q:LX/0Ot;

    .line 349016
    move-object/from16 v0, p14

    iput-object v0, p0, LX/1xa;->r:LX/1xh;

    .line 349017
    move-object/from16 v0, p16

    iput-object v0, p0, LX/1xa;->s:LX/1xi;

    .line 349018
    iget-object v1, p0, LX/1xa;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1055

    invoke-static {v1, v2}, LX/1wD;->a(Landroid/content/res/Resources;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1xa;->o:LX/0Ot;

    .line 349019
    iget-object v1, p0, LX/1xa;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1054

    invoke-static {v1, v2}, LX/1wD;->a(Landroid/content/res/Resources;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/1xa;->p:LX/0Ot;

    .line 349020
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;Z)I
    .locals 2

    .prologue
    .line 348994
    invoke-static {p1, p2}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 348995
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348996
    const/4 v0, 0x0

    .line 348997
    :goto_0
    return v0

    .line 348998
    :cond_0
    if-eqz p3, :cond_1

    invoke-static {p0, v0}, LX/1xa;->b(LX/1xa;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348999
    iget-object v1, p0, LX/1xa;->k:LX/1e4;

    invoke-virtual {v1, v0}, LX/1e4;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 349000
    :cond_1
    iget-object v1, p0, LX/1xa;->k:LX/1e4;

    invoke-virtual {v1, v0}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1xa;
    .locals 3

    .prologue
    .line 348986
    const-class v1, LX/1xa;

    monitor-enter v1

    .line 348987
    :try_start_0
    sget-object v0, LX/1xa;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348988
    sput-object v2, LX/1xa;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348989
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348990
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1xa;->b(LX/0QB;)LX/1xa;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348991
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348992
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348993
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)LX/1z3;
    .locals 3

    .prologue
    .line 348979
    invoke-static {p0, p1}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 348980
    const/4 v0, 0x0

    .line 348981
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 348982
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v0

    .line 348983
    :cond_0
    if-nez v0, :cond_1

    .line 348984
    const-string v0, ""

    .line 348985
    :cond_1
    new-instance v2, LX/1z3;

    invoke-direct {v2, v0, v1}, LX/1z3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;IZ)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p3    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 348969
    invoke-static {p1, p2}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 348970
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348971
    const/4 v0, 0x0

    .line 348972
    :goto_0
    return-object v0

    .line 348973
    :cond_0
    if-eqz p4, :cond_1

    invoke-static {p0, v0}, LX/1xa;->b(LX/1xa;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348974
    iget-object v1, p0, LX/1xa;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LX/1xa;->k:LX/1e4;

    invoke-virtual {v2, v0}, LX/1e4;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 348975
    :cond_1
    iget-object v1, p0, LX/1xa;->k:LX/1e4;

    invoke-virtual {v1, v0}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v0

    .line 348976
    if-eqz p3, :cond_2

    .line 348977
    :goto_1
    iget-object v1, p0, LX/1xa;->n:LX/0wM;

    invoke-virtual {v1, v0, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 348978
    :cond_2
    iget-object v1, p0, LX/1xa;->b:Landroid/content/Context;

    const v2, 0x1010212

    const v3, -0x6e685d

    invoke-static {v1, v2, v3}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result p3

    goto :goto_1
.end method

.method private static a(Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;)Landroid/os/Bundle;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 348948
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    move-object v1, v0

    .line 348949
    :goto_0
    if-eqz v1, :cond_6

    .line 348950
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 348951
    invoke-static {v0, v1}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    move-object v1, v0

    .line 348952
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;

    move-object v4, v0

    .line 348953
    :goto_2
    if-eqz v4, :cond_5

    .line 348954
    invoke-virtual {v4}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_3
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;

    .line 348955
    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 348956
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v7, -0x1e53800c

    if-ne v3, v7, :cond_0

    .line 348957
    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$ActionLinksModel;->a()LX/1vs;

    .line 348958
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3

    .line 348959
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_1
    move-object v1, v2

    .line 348960
    goto :goto_0

    :cond_2
    move-object v4, v2

    .line 348961
    goto :goto_2

    .line 348962
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 348963
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v2}, LX/1VO;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->c()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 348964
    if-nez v1, :cond_4

    .line 348965
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 348966
    :cond_4
    const-string v0, "group_commerce_sell_options_id"

    invoke-virtual {v4}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel;->c()Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$AttachmentsModel$TargetModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 348967
    :cond_5
    return-object v1

    :cond_6
    move-object v1, v2

    goto :goto_1
.end method

.method private static a(LX/1xa;Landroid/text/SpannableStringBuilder;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;
    .locals 13
    .param p3    # Landroid/text/TextPaint;
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/TextPaint;",
            "IZ)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 348925
    if-nez p1, :cond_0

    .line 348926
    const/4 p1, 0x0

    .line 348927
    :goto_0
    return-object p1

    .line 348928
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348929
    invoke-static {v12, p1}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 348930
    const/4 v5, 0x0

    .line 348931
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {p0, v12, p1, v0, v1}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 348932
    if-eqz v3, :cond_1

    .line 348933
    new-instance v5, LX/1z2;

    iget-object v4, p0, LX/1xa;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-direct {v5, v4, v3}, LX/1z2;-><init>(Landroid/util/DisplayMetrics;Landroid/graphics/drawable/Drawable;)V

    .line 348934
    :cond_1
    if-eqz v5, :cond_2

    .line 348935
    invoke-static {v12, p1}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)LX/1z3;

    move-result-object v6

    .line 348936
    invoke-direct {p0, v2}, LX/1xa;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v6}, LX/1z3;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 348937
    invoke-static {v12}, LX/1xa;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 348938
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p()Z

    move-result v11

    .line 348939
    :goto_1
    iget-object v2, p0, LX/1xa;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Aoa;

    iget-object v4, p0, LX/1xa;->l:LX/1xg;

    iget-object v9, p0, LX/1xa;->o:LX/0Ot;

    iget-object v10, p0, LX/1xa;->p:LX/0Ot;

    move-object v7, p1

    move-object/from16 v8, p3

    invoke-virtual/range {v2 .. v11}, LX/Aoa;->a(Ljava/lang/String;LX/1xg;LX/1z2;LX/1z3;Landroid/text/SpannableStringBuilder;Landroid/text/TextPaint;LX/0Ot;LX/0Ot;Z)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    .line 348940
    :cond_2
    :goto_2
    invoke-static {v12}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 348941
    iget-object v2, p0, LX/1xa;->d:LX/1xc;

    invoke-virtual {v2, v12, p1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/Spannable;)V

    .line 348942
    :cond_3
    iget-object v3, p0, LX/1xa;->l:LX/1xg;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v2, p1}, LX/1xg;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    .line 348943
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 348944
    :cond_5
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 348945
    invoke-virtual {p1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 348946
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 348947
    const/16 v4, 0x11

    invoke-virtual {p1, v5, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/TextPaint;",
            "IZ)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 348923
    iget-object v0, p0, LX/1xa;->l:LX/1xg;

    invoke-virtual {v0, p1, p2}, LX/1xg;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/1xa;->a(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 348924
    invoke-static/range {v0 .. v5}, LX/1xa;->a(LX/1xa;Landroid/text/SpannableStringBuilder;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 348917
    if-nez p0, :cond_0

    .line 348918
    const/4 p0, 0x0

    .line 348919
    :goto_0
    return-object p0

    .line 348920
    :cond_0
    instance-of v0, p0, Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_1

    .line 348921
    check-cast p0, Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 348922
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 349021
    if-nez p0, :cond_0

    .line 349022
    const/4 v0, 0x0

    .line 349023
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 348796
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 348797
    :cond_0
    const-string v0, ""

    .line 348798
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 348804
    const-string v0, "good_friends"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1xa;->s:LX/1xi;

    invoke-virtual {v0}, LX/1xi;->a()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1xa;->r:LX/1xh;

    invoke-virtual {v0}, LX/1xh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1xa;->r:LX/1xh;

    invoke-virtual {v0}, LX/1xh;->g()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1xa;
    .locals 19

    .prologue
    .line 348805
    new-instance v2, LX/1xa;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v5

    check-cast v5, LX/1xc;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v7

    check-cast v7, LX/0gh;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/17V;->a(LX/0QB;)LX/17V;

    move-result-object v9

    check-cast v9, LX/17V;

    invoke-static/range {p0 .. p0}, LX/1xe;->a(LX/0QB;)LX/1xe;

    move-result-object v10

    check-cast v10, LX/1xe;

    invoke-static/range {p0 .. p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v11

    check-cast v11, LX/1nG;

    invoke-static/range {p0 .. p0}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v12

    check-cast v12, LX/1e4;

    invoke-static/range {p0 .. p0}, LX/1xg;->a(LX/0QB;)LX/1xg;

    move-result-object v13

    check-cast v13, LX/1xg;

    invoke-static/range {p0 .. p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v14

    check-cast v14, LX/17Q;

    invoke-static/range {p0 .. p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v15

    check-cast v15, LX/0wM;

    invoke-static/range {p0 .. p0}, LX/1xh;->a(LX/0QB;)LX/1xh;

    move-result-object v16

    check-cast v16, LX/1xh;

    const/16 v17, 0x1f74

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/1xi;->a(LX/0QB;)LX/1xi;

    move-result-object v18

    check-cast v18, LX/1xi;

    invoke-direct/range {v2 .. v18}, LX/1xa;-><init>(Landroid/content/Context;LX/03V;LX/1xc;LX/0Zb;LX/0gh;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17V;LX/1xe;LX/1nG;LX/1e4;LX/1xg;LX/17Q;LX/0wM;LX/1xh;LX/0Ot;LX/1xi;)V

    .line 348806
    return-object v2
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/TextPaint;",
            "IZ)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 348807
    iget-object v0, p0, LX/1xa;->l:LX/1xg;

    invoke-virtual {v0, p1, p2}, LX/1xg;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/1xa;->a(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    .line 348808
    invoke-static/range {v0 .. v5}, LX/1xa;->a(LX/1xa;Landroid/text/SpannableStringBuilder;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 348799
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 348800
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 348801
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348802
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 348803
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static b(LX/1xa;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 348809
    iget-object v0, p0, LX/1xa;->k:LX/1e4;

    invoke-virtual {v0, p1}, LX/1e4;->b(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;I)LX/1z4;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/TextPaint;",
            "I)",
            "LX/1z4;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 348810
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LX/1xa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IIZ)LX/1z4;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IIZ)LX/1z4;
    .locals 7
    .param p4    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/TextPaint;",
            "IIZ)",
            "LX/1z4;"
        }
    .end annotation

    .prologue
    .line 348811
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 348812
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348813
    iget-object v0, p0, LX/1xa;->i:LX/1xe;

    invoke-virtual {v0, p1}, LX/1xe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p4

    move v5, p5

    .line 348814
    invoke-direct/range {v0 .. v5}, LX/1xa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 348815
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348816
    sget-object v0, LX/1z4;->d:LX/1z4;

    .line 348817
    :goto_0
    return-object v0

    .line 348818
    :cond_0
    invoke-direct {p0, v6, v1, p5}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;Z)I

    move-result v0

    .line 348819
    if-nez v0, :cond_2

    if-eqz p5, :cond_2

    .line 348820
    const/4 v0, 0x0

    invoke-direct {p0, v6, v1, v0}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;Z)I

    move-result v0

    move v6, v0

    .line 348821
    :goto_1
    invoke-static {v1, p2}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 348822
    if-gt v0, p3, :cond_1

    .line 348823
    new-instance v0, LX/1z4;

    invoke-direct {v0, p3, v1, v6}, LX/1z4;-><init>(ILjava/lang/CharSequence;I)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p4

    move v5, p5

    .line 348824
    invoke-direct/range {v0 .. v5}, LX/1xa;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/text/TextPaint;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 348825
    new-instance v0, LX/1z4;

    invoke-direct {v0, p3, v1, v6}, LX/1z4;-><init>(ILjava/lang/CharSequence;I)V

    goto :goto_0

    :cond_2
    move v6, v0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;LX/6XU;)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "dispatchAvatarClick"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 348826
    invoke-static {p2}, LX/1xa;->a(Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;)Landroid/os/Bundle;

    move-result-object v4

    .line 348827
    invoke-virtual {p2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;

    move-object v3, v0

    .line 348828
    :goto_0
    if-nez v3, :cond_2

    .line 348829
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v3, v1

    .line 348830
    goto :goto_0

    .line 348831
    :cond_2
    iget-object v0, p0, LX/1xa;->j:LX/1nG;

    invoke-virtual {v3}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 348832
    if-nez v0, :cond_6

    .line 348833
    invoke-virtual {v3}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel$ActorsModel;->b()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 348834
    :goto_2
    if-eqz v2, :cond_0

    .line 348835
    invoke-static {v2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 348836
    iget-boolean v0, p3, LX/6XU;->a:Z

    move v0, v0

    .line 348837
    iget-object v1, p3, LX/6XU;->b:LX/162;

    move-object v1, v1

    .line 348838
    invoke-static {v0, v1}, LX/17Q;->c(ZLX/0lF;)Ljava/util/Map;

    move-result-object v0

    .line 348839
    :goto_3
    invoke-static {v2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 348840
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v1

    .line 348841
    iput-object v2, v1, LX/47H;->a:Ljava/lang/String;

    .line 348842
    move-object v1, v1

    .line 348843
    invoke-virtual {p2}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v2}, LX/2yB;->a(LX/1yE;Ljava/lang/String;Ljava/lang/String;)LX/47G;

    move-result-object v2

    .line 348844
    iput-object v2, v1, LX/47H;->d:LX/47G;

    .line 348845
    move-object v1, v1

    .line 348846
    iput-object v4, v1, LX/47H;->b:Landroid/os/Bundle;

    .line 348847
    move-object v1, v1

    .line 348848
    invoke-virtual {v1, v0}, LX/47H;->a(Ljava/util/Map;)LX/47H;

    move-result-object v0

    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 348849
    iget-object v1, p0, LX/1xa;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;LX/47I;)Z

    goto :goto_1

    .line 348850
    :cond_3
    iget-object v0, p0, LX/1xa;->h:LX/17V;

    .line 348851
    iget-boolean v5, p3, LX/6XU;->a:Z

    move v5, v5

    .line 348852
    iget-object v6, p3, LX/6XU;->b:LX/162;

    move-object v6, v6

    .line 348853
    const-string v7, "native_newsfeed"

    invoke-virtual {v0, v2, v5, v6, v7}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 348854
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 348855
    invoke-static {v0, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 348856
    :cond_4
    iget-object v5, p0, LX/1xa;->e:LX/0Zb;

    invoke-interface {v5, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 348857
    iget-object v5, p0, LX/1xa;->f:LX/0gh;

    if-eqz v0, :cond_7

    .line 348858
    iget-boolean v6, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v6

    .line 348859
    if-eqz v0, :cond_7

    const-string v0, "tap_profile_pic_sponsored"

    :goto_4
    invoke-virtual {v5, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 348860
    move-object v0, v1

    goto :goto_3

    .line 348861
    :cond_5
    iget-object v1, p0, LX/1xa;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v3, v2, v4, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto/16 :goto_1

    :cond_6
    move-object v2, v0

    goto :goto_2

    .line 348862
    :cond_7
    const-string v0, "tap_profile_pic"

    goto :goto_4
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 348863
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 348864
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348865
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 348866
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348867
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v1

    cmp-long v1, v3, v1

    if-nez v1, :cond_2

    invoke-static {p1}, LX/1xe;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    invoke-static {p2}, LX/1xe;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 348868
    if-eqz v0, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 348869
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 348870
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348871
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 348872
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348873
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v2

    .line 348874
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->H()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object p0

    .line 348875
    :goto_2
    if-eqz p0, :cond_5

    .line 348876
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v4

    .line 348877
    :goto_3
    move v0, v0

    .line 348878
    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 348879
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 348880
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348881
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 348882
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 348883
    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    if-eqz v4, :cond_10

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v0

    .line 348884
    :goto_4
    invoke-static {v1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v1}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v2

    .line 348885
    :cond_0
    if-eqz v0, :cond_11

    .line 348886
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    move v0, v3

    .line 348887
    :goto_5
    move v0, v0

    .line 348888
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_6

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    move-object v2, v3

    .line 348889
    goto :goto_1

    :cond_4
    move-object p0, v3

    .line 348890
    goto :goto_2

    .line 348891
    :cond_5
    if-eqz v2, :cond_6

    move v0, v4

    .line 348892
    goto :goto_3

    .line 348893
    :cond_6
    invoke-static {v0}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->v()Ljava/lang/String;

    move-result-object v2

    .line 348894
    :goto_7
    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    if-eqz p0, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->X()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->v()Ljava/lang/String;

    move-result-object p0

    .line 348895
    :goto_8
    if-eqz p0, :cond_9

    .line 348896
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v4

    .line 348897
    goto/16 :goto_3

    :cond_7
    move-object v2, v3

    .line 348898
    goto :goto_7

    :cond_8
    move-object p0, v3

    .line 348899
    goto :goto_8

    .line 348900
    :cond_9
    if-eqz v2, :cond_a

    move v0, v4

    .line 348901
    goto/16 :goto_3

    .line 348902
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    .line 348903
    :goto_9
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ah()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    .line 348904
    :cond_b
    if-eqz v3, :cond_d

    .line 348905
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v4

    .line 348906
    goto/16 :goto_3

    :cond_c
    move-object v0, v3

    .line 348907
    goto :goto_9

    .line 348908
    :cond_d
    if-eqz v0, :cond_e

    move v0, v4

    .line 348909
    goto/16 :goto_3

    .line 348910
    :cond_e
    invoke-static {p2}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    invoke-static {p1}, LX/182;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eq v0, v1, :cond_f

    move v0, v4

    .line 348911
    goto/16 :goto_3

    .line 348912
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_10
    move-object v0, v2

    .line 348913
    goto/16 :goto_4

    .line 348914
    :cond_11
    if-eqz v2, :cond_12

    move v0, v3

    .line 348915
    goto/16 :goto_5

    .line 348916
    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_5
.end method
