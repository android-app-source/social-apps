.class public LX/1yn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1yn",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351361
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 351362
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1yn;->b:LX/0Zi;

    .line 351363
    iput-object p1, p0, LX/1yn;->a:LX/0Ot;

    .line 351364
    return-void
.end method

.method public static a(LX/0QB;)LX/1yn;
    .locals 4

    .prologue
    .line 351365
    const-class v1, LX/1yn;

    monitor-enter v1

    .line 351366
    :try_start_0
    sget-object v0, LX/1yn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351367
    sput-object v2, LX/1yn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351368
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351369
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351370
    new-instance v3, LX/1yn;

    const/16 p0, 0x732

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1yn;-><init>(LX/0Ot;)V

    .line 351371
    move-object v0, v3

    .line 351372
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351373
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351374
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351375
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onSaveClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351376
    const v0, 0x5909f750

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 351377
    check-cast p2, LX/Bsg;

    .line 351378
    iget-object v0, p0, LX/1yn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yu;

    iget-object v1, p2, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bsg;->b:LX/1dc;

    iget-object v3, p2, LX/Bsg;->c:LX/1dc;

    const/4 v5, 0x0

    .line 351379
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 351380
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/1yu;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 351381
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v4, v6, :cond_0

    const/4 v4, 0x1

    move v6, v4

    .line 351382
    :goto_0
    if-eqz v6, :cond_1

    .line 351383
    :goto_1
    if-eqz v6, :cond_2

    const v4, 0x7f0810f8

    .line 351384
    :goto_2
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/16 p0, 0x8

    const p2, 0x7f0b00e5

    invoke-interface {v7, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    .line 351385
    const p0, 0x5909f750

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 351386
    invoke-interface {v7, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    invoke-interface {v7, v4}, LX/1Di;->A(I)LX/1Di;

    move-result-object v7

    iget-object v4, v0, LX/1yu;->m:LX/1yf;

    invoke-virtual {v4}, LX/1yf;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v6, :cond_3

    const v4, 0x7f020cba

    :goto_3
    invoke-interface {v7, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 351387
    return-object v0

    :cond_0
    move v6, v5

    .line 351388
    goto :goto_0

    :cond_1
    move-object v2, v3

    .line 351389
    goto :goto_1

    .line 351390
    :cond_2
    const v4, 0x7f0810f7

    goto :goto_2

    .line 351391
    :cond_3
    const v4, 0x7f020cb9

    goto :goto_3

    :cond_4
    move v4, v5

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 351392
    invoke-static {}, LX/1dS;->b()V

    .line 351393
    iget v0, p1, LX/1dQ;->b:I

    .line 351394
    packed-switch v0, :pswitch_data_0

    .line 351395
    :goto_0
    return-object v2

    .line 351396
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 351397
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 351398
    check-cast v1, LX/Bsg;

    .line 351399
    iget-object v3, p0, LX/1yn;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1yu;

    iget-object p1, v1, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/Bsg;->d:LX/1Pq;

    invoke-virtual {v3, v0, p1, p2}, LX/1yu;->onSaveClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 351400
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5909f750
        :pswitch_0
    .end packed-switch
.end method
