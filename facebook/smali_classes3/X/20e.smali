.class public LX/20e;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/20f;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:Landroid/os/Handler;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/20f;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 355040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355041
    iput-object p1, p0, LX/20e;->a:LX/20f;

    .line 355042
    iput-object p2, p0, LX/20e;->b:Ljava/util/concurrent/ExecutorService;

    .line 355043
    iput-object p3, p0, LX/20e;->c:Landroid/os/Handler;

    .line 355044
    iput-object p4, p0, LX/20e;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 355045
    iput-object p5, p0, LX/20e;->e:LX/0ad;

    .line 355046
    return-void
.end method

.method public static b(LX/0QB;)LX/20e;
    .locals 6

    .prologue
    .line 355035
    new-instance v0, LX/20e;

    .line 355036
    new-instance v3, LX/20f;

    invoke-static {p0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v1

    check-cast v1, LX/0si;

    const-class v2, LX/0tp;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/0tp;

    invoke-direct {v3, v1, v2}, LX/20f;-><init>(LX/0si;LX/0tp;)V

    .line 355037
    move-object v1, v3

    .line 355038
    check-cast v1, LX/20f;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct/range {v0 .. v5}, LX/20e;-><init>(LX/20f;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V

    .line 355039
    return-object v0
.end method


# virtual methods
.method public final a(LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 355032
    iget-object v0, p0, LX/20e;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0pP;->y:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355033
    :goto_0
    return-void

    .line 355034
    :cond_0
    iget-object v0, p0, LX/20e;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/feedplugins/base/footer/ui/CommentCacheStateUtil$1;-><init>(LX/20e;LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V

    const v2, -0x5d05d3f5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
