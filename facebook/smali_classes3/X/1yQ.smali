.class public final LX/1yQ;
.super LX/1yR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1yR;"
    }
.end annotation


# instance fields
.field public final synthetic d:LX/1Uf;

.field private final e:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TT;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;LX/0QK;Ljava/lang/Object;)V
    .locals 0
    .param p5    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1yD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/0lF;",
            "I",
            "LX/1yD;",
            "Ljava/lang/String;",
            "LX/0QK",
            "<TT;",
            "Landroid/os/Bundle;",
            ">;TT;)V"
        }
    .end annotation

    .prologue
    .line 350573
    iput-object p1, p0, LX/1yQ;->d:LX/1Uf;

    .line 350574
    invoke-direct/range {p0 .. p8}, LX/1yR;-><init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 350575
    iput-object p9, p0, LX/1yQ;->e:LX/0QK;

    .line 350576
    iput-object p10, p0, LX/1yQ;->f:Ljava/lang/Object;

    .line 350577
    return-void
.end method


# virtual methods
.method public final g()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 350572
    iget-object v0, p0, LX/1yQ;->e:LX/0QK;

    iget-object v1, p0, LX/1yQ;->f:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method
