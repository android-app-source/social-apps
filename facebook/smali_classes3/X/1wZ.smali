.class public final enum LX/1wZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1wZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1wZ;

.field public static final enum ANDROID_PLATFORM:LX/1wZ;

.field public static final enum GOOGLE_PLAY:LX/1wZ;

.field public static final enum MOCK_MPK_STATIC:LX/1wZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 346368
    new-instance v0, LX/1wZ;

    const-string v1, "ANDROID_PLATFORM"

    invoke-direct {v0, v1, v2}, LX/1wZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    .line 346369
    new-instance v0, LX/1wZ;

    const-string v1, "GOOGLE_PLAY"

    invoke-direct {v0, v1, v3}, LX/1wZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    .line 346370
    new-instance v0, LX/1wZ;

    const-string v1, "MOCK_MPK_STATIC"

    invoke-direct {v0, v1, v4}, LX/1wZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    .line 346371
    const/4 v0, 0x3

    new-array v0, v0, [LX/1wZ;

    sget-object v1, LX/1wZ;->ANDROID_PLATFORM:LX/1wZ;

    aput-object v1, v0, v2

    sget-object v1, LX/1wZ;->GOOGLE_PLAY:LX/1wZ;

    aput-object v1, v0, v3

    sget-object v1, LX/1wZ;->MOCK_MPK_STATIC:LX/1wZ;

    aput-object v1, v0, v4

    sput-object v0, LX/1wZ;->$VALUES:[LX/1wZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 346374
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1wZ;
    .locals 1

    .prologue
    .line 346373
    const-class v0, LX/1wZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1wZ;

    return-object v0
.end method

.method public static values()[LX/1wZ;
    .locals 1

    .prologue
    .line 346372
    sget-object v0, LX/1wZ;->$VALUES:[LX/1wZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1wZ;

    return-object v0
.end method
