.class public LX/1vg;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/2xv;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/1vg;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2xw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343379
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1vg;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2xw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343380
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 343381
    iput-object p1, p0, LX/1vg;->b:LX/0Ot;

    .line 343382
    return-void
.end method

.method public static a(LX/0QB;)LX/1vg;
    .locals 4

    .prologue
    .line 343383
    sget-object v0, LX/1vg;->c:LX/1vg;

    if-nez v0, :cond_1

    .line 343384
    const-class v1, LX/1vg;

    monitor-enter v1

    .line 343385
    :try_start_0
    sget-object v0, LX/1vg;->c:LX/1vg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 343386
    if-eqz v2, :cond_0

    .line 343387
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 343388
    new-instance v3, LX/1vg;

    const/16 p0, 0x3b3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1vg;-><init>(LX/0Ot;)V

    .line 343389
    move-object v0, v3

    .line 343390
    sput-object v0, LX/1vg;->c:LX/1vg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 343392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 343393
    :cond_1
    sget-object v0, LX/1vg;->c:LX/1vg;

    return-object v0

    .line 343394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 343395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;)LX/2xv;
    .locals 2

    .prologue
    .line 343396
    new-instance v0, LX/2xu;

    invoke-direct {v0, p0}, LX/2xu;-><init>(LX/1vg;)V

    .line 343397
    sget-object v1, LX/1vg;->a:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2xv;

    .line 343398
    if-nez v1, :cond_0

    .line 343399
    new-instance v1, LX/2xv;

    invoke-direct {v1}, LX/2xv;-><init>()V

    .line 343400
    :cond_0
    invoke-static {v1, p1, v0}, LX/2xv;->a$redex0(LX/2xv;LX/1De;LX/2xu;)V

    .line 343401
    move-object v0, v1

    .line 343402
    return-object v0
.end method

.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 343403
    check-cast p2, LX/2xu;

    .line 343404
    iget-object v0, p0, LX/1vg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2xw;

    iget v1, p2, LX/2xu;->a:I

    iget v2, p2, LX/2xu;->b:I

    .line 343405
    iget-object p0, v0, LX/2xw;->a:LX/0wM;

    invoke-virtual {p0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    move-object v0, p0

    .line 343406
    return-object v0
.end method
