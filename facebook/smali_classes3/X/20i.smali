.class public LX/20i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/20i;


# instance fields
.field private final a:LX/0WJ;


# direct methods
.method public constructor <init>(LX/0WJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 355338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355339
    iput-object p1, p0, LX/20i;->a:LX/0WJ;

    .line 355340
    return-void
.end method

.method public static a(LX/0QB;)LX/20i;
    .locals 4

    .prologue
    .line 355341
    sget-object v0, LX/20i;->b:LX/20i;

    if-nez v0, :cond_1

    .line 355342
    const-class v1, LX/20i;

    monitor-enter v1

    .line 355343
    :try_start_0
    sget-object v0, LX/20i;->b:LX/20i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 355344
    if-eqz v2, :cond_0

    .line 355345
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 355346
    new-instance p0, LX/20i;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-direct {p0, v3}, LX/20i;-><init>(LX/0WJ;)V

    .line 355347
    move-object v0, p0

    .line 355348
    sput-object v0, LX/20i;->b:LX/20i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355349
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 355350
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 355351
    :cond_1
    sget-object v0, LX/20i;->b:LX/20i;

    return-object v0

    .line 355352
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 355353
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 5

    .prologue
    .line 355354
    iget-object v0, p0, LX/20i;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 355355
    const/4 v0, 0x0

    .line 355356
    :goto_0
    return-object v0

    .line 355357
    :cond_0
    iget-object v0, p0, LX/20i;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 355358
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v1

    .line 355359
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    .line 355360
    if-eqz v2, :cond_1

    if-nez v0, :cond_2

    .line 355361
    :cond_1
    const/4 v3, 0x0

    .line 355362
    :goto_1
    move-object v0, v3

    .line 355363
    goto :goto_0

    .line 355364
    :cond_2
    invoke-static {v0, v3, v3}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 355365
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    .line 355366
    iput-object v1, v4, LX/3dL;->E:Ljava/lang/String;

    .line 355367
    iput-object v2, v4, LX/3dL;->ag:Ljava/lang/String;

    .line 355368
    iput-object v3, v4, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 355369
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const p0, 0x285feb

    invoke-direct {v3, p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 355370
    iput-object v3, v4, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 355371
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    goto :goto_1
.end method
