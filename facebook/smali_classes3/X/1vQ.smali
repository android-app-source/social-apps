.class public LX/1vQ;
.super LX/05M;
.source ""


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public final f:Landroid/content/Context;

.field private final g:Z

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private n:Z

.field private o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/760;

.field public q:Lcom/facebook/proxygen/MQTTClientFactory;

.field private r:Lcom/facebook/proxygen/EventBase;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 342690
    const-class v0, LX/1vQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1vQ;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;ZZZZZLX/0Ot;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "ZZZZZ",
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 342691
    invoke-direct {p0}, LX/05M;-><init>()V

    .line 342692
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1vQ;->n:Z

    .line 342693
    iput-object p1, p0, LX/1vQ;->f:Landroid/content/Context;

    .line 342694
    iput-boolean p3, p0, LX/1vQ;->g:Z

    .line 342695
    iput-object p2, p0, LX/1vQ;->h:Ljava/util/concurrent/ExecutorService;

    .line 342696
    iput-boolean p4, p0, LX/1vQ;->i:Z

    .line 342697
    iput-boolean p5, p0, LX/1vQ;->j:Z

    .line 342698
    iput-boolean p6, p0, LX/1vQ;->k:Z

    .line 342699
    iput-boolean p7, p0, LX/1vQ;->l:Z

    .line 342700
    iput-object p8, p0, LX/1vQ;->o:LX/0Ot;

    .line 342701
    iput-boolean p9, p0, LX/1vQ;->m:Z

    .line 342702
    return-void
.end method

.method private static a(LX/05h;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 342703
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 342704
    const-string v1, "reason"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342705
    const-string v1, "throwable"

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342706
    const-string v1, "whistle_failure"

    invoke-virtual {p0, v1, v0}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 342707
    return-void
.end method


# virtual methods
.method public final a(LX/06z;)LX/071;
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 342708
    iget-boolean v0, p0, LX/1vQ;->g:Z

    if-eqz v0, :cond_a

    .line 342709
    iget-boolean v0, p0, LX/1vQ;->n:Z

    if-nez v0, :cond_0

    .line 342710
    :try_start_0
    const-string v0, "fb"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 342711
    const-string v0, "liger-native"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 342712
    const-string v0, "whistle"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 342713
    iput-boolean v11, p0, LX/1vQ;->n:Z

    .line 342714
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/1vQ;->q:Lcom/facebook/proxygen/MQTTClientFactory;

    if-nez v0, :cond_8

    .line 342715
    iget-boolean v0, p0, LX/1vQ;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1vQ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 342716
    iget-object v0, p0, LX/1vQ;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MP;

    .line 342717
    iget-object v1, v0, LX/1MP;->r:Lcom/facebook/proxygen/HTTPClient;

    move-object v0, v1

    .line 342718
    iget-object v1, v0, Lcom/facebook/proxygen/HTTPClient;->mEventBase:Lcom/facebook/proxygen/EventBase;

    move-object v0, v1

    .line 342719
    iput-object v0, p0, LX/1vQ;->r:Lcom/facebook/proxygen/EventBase;

    .line 342720
    :cond_1
    iget-object v0, p0, LX/1vQ;->r:Lcom/facebook/proxygen/EventBase;

    if-nez v0, :cond_2

    .line 342721
    new-instance v0, Lcom/facebook/proxygen/HTTPThread;

    invoke-direct {v0}, Lcom/facebook/proxygen/HTTPThread;-><init>()V

    .line 342722
    const v1, 0x8e4901

    invoke-static {v0, v1}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v1

    .line 342723
    iget v2, p1, LX/06z;->k:I

    move v2, v2

    .line 342724
    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setPriority(I)V

    .line 342725
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 342726
    invoke-virtual {v0}, Lcom/facebook/proxygen/HTTPThread;->waitForInitialization()V

    .line 342727
    invoke-virtual {v0}, Lcom/facebook/proxygen/HTTPThread;->getEventBase()Lcom/facebook/proxygen/EventBase;

    move-result-object v0

    iput-object v0, p0, LX/1vQ;->r:Lcom/facebook/proxygen/EventBase;

    .line 342728
    :cond_2
    new-instance v0, LX/760;

    invoke-direct {v0}, LX/760;-><init>()V

    iput-object v0, p0, LX/1vQ;->p:LX/760;

    .line 342729
    iget-boolean v0, p0, LX/1vQ;->m:Z

    if-eqz v0, :cond_7

    .line 342730
    const/4 v2, 0x0

    .line 342731
    const-string v0, "http.nonProxyHosts"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 342732
    const-string v0, "http.proxyHost"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 342733
    :try_start_2
    const-string v1, "http.proxyPort"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    move-result v1

    .line 342734
    :goto_0
    const-string v3, "https.proxyHost"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 342735
    :try_start_4
    const-string v4, "https.proxyPort"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    move-result v4

    .line 342736
    :goto_1
    if-eqz v0, :cond_3

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 342737
    :cond_3
    const-string v0, "proxyHost"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    .line 342738
    :try_start_6
    const-string v0, "proxyPort"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    :try_start_7
    move-result v5

    .line 342739
    :goto_2
    if-eqz v3, :cond_4

    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_4
    move-object v0, v1

    move v2, v5

    .line 342740
    :goto_3
    if-eqz v1, :cond_5

    .line 342741
    iget-object v3, p0, LX/1vQ;->p:LX/760;

    .line 342742
    iput-object v1, v3, LX/760;->proxyAddress:Ljava/lang/String;

    .line 342743
    iput v5, v3, LX/760;->proxyPort:I

    .line 342744
    :cond_5
    if-eqz v0, :cond_6

    .line 342745
    iget-object v1, p0, LX/1vQ;->p:LX/760;

    .line 342746
    iput-object v0, v1, LX/760;->secureProxyAddress:Ljava/lang/String;

    .line 342747
    iput v2, v1, LX/760;->secureProxyPort:I

    .line 342748
    :cond_6
    if-eqz v6, :cond_7

    .line 342749
    iget-object v0, p0, LX/1vQ;->p:LX/760;

    .line 342750
    iput-object v6, v0, LX/760;->bypassProxyDomains:Ljava/lang/String;

    .line 342751
    :cond_7
    iget-boolean v0, p0, LX/1vQ;->k:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x7530

    .line 342752
    :goto_4
    iget-object v1, p0, LX/1vQ;->p:LX/760;

    const/4 v2, 0x1

    .line 342753
    iput-boolean v2, v1, LX/760;->zlibCompression:Z

    .line 342754
    move-object v1, v1

    .line 342755
    const/4 v2, 0x1

    .line 342756
    iput-boolean v2, v1, LX/760;->verifyCertificates:Z

    .line 342757
    move-object v1, v1

    .line 342758
    iput v0, v1, LX/760;->connectTimeout:I

    .line 342759
    move-object v0, v1

    .line 342760
    const/4 v1, 0x0

    .line 342761
    iput v1, v0, LX/760;->pingRespTimeout:I

    .line 342762
    iget-object v0, p0, LX/1vQ;->p:LX/760;

    invoke-virtual {v0}, LX/760;->build()LX/761;

    move-result-object v0

    .line 342763
    new-instance v1, Lcom/facebook/proxygen/MQTTClientFactory;

    iget-object v2, p0, LX/1vQ;->r:Lcom/facebook/proxygen/EventBase;

    iget-object v3, p0, LX/1vQ;->h:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/proxygen/MQTTClientFactory;-><init>(Lcom/facebook/proxygen/EventBase;Ljava/util/concurrent/Executor;LX/761;)V

    .line 342764
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LX/1vQ;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "WhistleTls.store"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 342765
    new-instance v2, LX/1hJ;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v0, 0xa

    .line 342766
    iput v0, v2, LX/1hJ;->cacheCapacity:I

    .line 342767
    move-object v0, v2

    .line 342768
    const/16 v2, 0x96

    .line 342769
    iput v2, v0, LX/1hJ;->syncInterval:I

    .line 342770
    move-object v0, v0

    .line 342771
    invoke-virtual {v0}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v0

    .line 342772
    move-object v0, v0

    .line 342773
    iput-object v0, v1, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentSSLCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 342774
    move-object v0, v1

    .line 342775
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LX/1vQ;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "WhistleDns.store"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 342776
    new-instance v2, LX/1hJ;

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x14

    .line 342777
    iput v1, v2, LX/1hJ;->cacheCapacity:I

    .line 342778
    move-object v1, v2

    .line 342779
    const/16 v2, 0x96

    .line 342780
    iput v2, v1, LX/1hJ;->syncInterval:I

    .line 342781
    move-object v1, v1

    .line 342782
    invoke-virtual {v1}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v1

    .line 342783
    move-object v1, v1

    .line 342784
    iput-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mPersistentDNSCacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 342785
    move-object v0, v0

    .line 342786
    iget-boolean v1, p1, LX/06z;->q:Z

    move v1, v1

    .line 342787
    iget-boolean v2, p1, LX/06z;->r:Z

    move v2, v2

    .line 342788
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 342789
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, LX/1vQ;->f:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    const-string v7, "WhistleZero.store"

    invoke-direct {v5, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 342790
    new-instance v6, LX/1hJ;

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v5}, LX/1hJ;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1e

    .line 342791
    iput v5, v6, LX/1hJ;->cacheCapacity:I

    .line 342792
    move-object v5, v6

    .line 342793
    const/16 v6, 0x96

    .line 342794
    iput v6, v5, LX/1hJ;->syncInterval:I

    .line 342795
    move-object v5, v5

    .line 342796
    invoke-virtual {v5}, LX/1hJ;->build()Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    move-result-object v5

    .line 342797
    new-instance v6, LX/1hV;

    invoke-direct {v6}, LX/1hV;-><init>()V

    .line 342798
    iput-boolean v1, v6, LX/1hV;->enabled:Z

    .line 342799
    move-object v6, v6

    .line 342800
    iput-boolean v4, v6, LX/1hV;->enforceExpiration:Z

    .line 342801
    move-object v6, v6

    .line 342802
    iput-boolean v3, v6, LX/1hV;->persistentCacheEnabled:Z

    .line 342803
    move-object v6, v6

    .line 342804
    iput-object v5, v6, LX/1hV;->cacheSettings:Lcom/facebook/proxygen/PersistentSSLCacheSettings;

    .line 342805
    move-object v5, v6

    .line 342806
    if-nez v2, :cond_d

    .line 342807
    :goto_5
    iput-boolean v3, v5, LX/1hV;->zeroRttEnabled:Z

    .line 342808
    move-object v3, v5

    .line 342809
    const/4 v4, 0x3

    .line 342810
    iput v4, v3, LX/1hV;->tlsFallback:I

    .line 342811
    move-object v3, v3

    .line 342812
    invoke-virtual {v3}, LX/1hV;->build()Lcom/facebook/proxygen/ZeroProtocolSettings;

    move-result-object v3

    move-object v1, v3

    .line 342813
    iput-object v1, v0, Lcom/facebook/proxygen/MQTTClientFactory;->mZeroProtocolSettings:Lcom/facebook/proxygen/ZeroProtocolSettings;

    .line 342814
    move-object v0, v0

    .line 342815
    iput-object v0, p0, LX/1vQ;->q:Lcom/facebook/proxygen/MQTTClientFactory;

    .line 342816
    iget-object v0, p0, LX/1vQ;->q:Lcom/facebook/proxygen/MQTTClientFactory;

    invoke-virtual {v0}, Lcom/facebook/proxygen/MQTTClientFactory;->init()V

    .line 342817
    iget-object v0, p0, LX/05M;->b:LX/059;

    new-instance v1, LX/6mz;

    invoke-direct {v1, p0}, LX/6mz;-><init>(LX/1vQ;)V

    invoke-virtual {v0, v1}, LX/059;->a(LX/058;)V

    .line 342818
    :cond_8
    const-string v0, "W"

    iput-object v0, p0, LX/1vQ;->a:Ljava/lang/String;

    .line 342819
    new-instance v0, LX/6my;

    .line 342820
    iget v1, p1, LX/06z;->z:I

    move v1, v1

    .line 342821
    invoke-virtual {p1}, LX/06z;->j()I

    move-result v2

    iget-object v3, p0, LX/1vQ;->q:Lcom/facebook/proxygen/MQTTClientFactory;

    iget-object v4, p0, LX/05M;->c:LX/01o;

    iget-boolean v5, p0, LX/1vQ;->i:Z

    iget-boolean v6, p0, LX/1vQ;->j:Z

    new-instance v7, LX/6mu;

    iget-object v8, p0, LX/05M;->d:LX/05h;

    iget-object v9, p0, LX/05M;->b:LX/059;

    invoke-direct {v7, v8, p1, v9}, LX/6mu;-><init>(LX/05h;LX/06z;LX/059;)V

    iget-object v8, p0, LX/1vQ;->h:Ljava/util/concurrent/ExecutorService;

    iget-object v9, p0, LX/1vQ;->r:Lcom/facebook/proxygen/EventBase;

    invoke-direct/range {v0 .. v9}, LX/6my;-><init>(IILcom/facebook/proxygen/MQTTClientFactory;LX/01o;ZZLX/6mu;Ljava/util/concurrent/Executor;Lcom/facebook/proxygen/EventBase;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    .line 342822
    :goto_6
    return-object v0

    .line 342823
    :catch_0
    move-exception v0

    .line 342824
    const-string v1, "JNI load failed"

    .line 342825
    sget-object v2, LX/1vQ;->e:Ljava/lang/String;

    new-array v3, v11, [Ljava/lang/Object;

    aput-object v0, v3, v12

    invoke-static {v2, v1, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342826
    iget-object v2, p0, LX/05M;->d:LX/05h;

    invoke-static {v2, v1, v0}, LX/1vQ;->a(LX/05h;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342827
    const-string v0, "LF"

    iput-object v0, p0, LX/1vQ;->a:Ljava/lang/String;

    move-object v0, v10

    .line 342828
    goto :goto_6

    .line 342829
    :cond_9
    :try_start_8
    invoke-virtual {p1}, LX/06z;->k()I
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1

    move-result v0

    goto/16 :goto_4

    .line 342830
    :catch_1
    move-exception v0

    .line 342831
    const-string v1, "Failed to create whistle factory"

    .line 342832
    sget-object v2, LX/1vQ;->e:Ljava/lang/String;

    new-array v3, v11, [Ljava/lang/Object;

    aput-object v0, v3, v12

    invoke-static {v2, v1, v3}, LX/05D;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342833
    iget-object v2, p0, LX/05M;->d:LX/05h;

    invoke-static {v2, v1, v0}, LX/1vQ;->a(LX/05h;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342834
    const-string v0, "FC"

    iput-object v0, p0, LX/1vQ;->a:Ljava/lang/String;

    move-object v0, v10

    .line 342835
    goto :goto_6

    .line 342836
    :cond_a
    const-string v0, "D"

    iput-object v0, p0, LX/1vQ;->a:Ljava/lang/String;

    move-object v0, v10

    .line 342837
    goto :goto_6

    .line 342838
    :catch_2
    :try_start_9
    const-string v0, ""

    move v1, v2

    .line 342839
    goto/16 :goto_0

    .line 342840
    :catch_3
    const-string v3, ""

    move v4, v2

    .line 342841
    goto/16 :goto_1

    .line 342842
    :catch_4
    const-string v1, ""

    move v5, v2

    .line 342843
    goto/16 :goto_2

    :cond_b
    move-object v0, v3

    move v2, v4

    goto/16 :goto_3

    :cond_c
    move v5, v1

    move-object v1, v0

    goto/16 :goto_2
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1

    :cond_d
    move v3, v4

    goto/16 :goto_5
.end method
