.class public LX/212;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 356544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/util/EnumMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pr;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/1PT;",
            ")",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356545
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, LX/212;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStorySet;LX/1PT;)Ljava/util/EnumMap;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStorySet;LX/1PT;)Ljava/util/EnumMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pr;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            "LX/1PT;",
            ")",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356508
    new-instance v0, LX/213;

    invoke-direct {v0, p1, p2, p3}, LX/213;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStorySet;LX/1PT;)V

    invoke-interface {p0, v0, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumMap;

    return-object v0
.end method

.method private static a(LX/0Or;Ljava/util/EnumMap;LX/20X;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;",
            "LX/20X;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356541
    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    .line 356542
    invoke-virtual {p1, p2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356543
    return-void
.end method

.method public static a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1wK;",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "LX/20Z;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356533
    invoke-virtual {p1}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p0, v0}, LX/1wK;->setButtons(Ljava/util/Set;)V

    .line 356534
    invoke-interface {p0, p1}, LX/1wK;->setSprings(Ljava/util/EnumMap;)V

    .line 356535
    if-nez p2, :cond_0

    .line 356536
    const/4 v0, 0x0

    .line 356537
    :goto_0
    move v0, v0

    .line 356538
    invoke-interface {p0, v0}, LX/1wK;->setIsLiked(Z)V

    .line 356539
    invoke-interface {p0, p3}, LX/1wK;->setOnButtonClickedListener(LX/20Z;)V

    .line 356540
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/0gh;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/0bH;LX/1WR;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0gh;",
            "LX/20p;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "LX/0bH;",
            "LX/1WR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356546
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356547
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356548
    const-string v1, "clicked_on_share"

    invoke-static {p6, v0, v1}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 356549
    const-string v0, "tap_share"

    invoke-virtual {p1, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 356550
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356551
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356552
    invoke-static {v0}, LX/1VF;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 356553
    if-eqz v6, :cond_1

    .line 356554
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    new-instance v0, LX/Anz;

    move-object v1, p5

    move-object v2, p2

    move-object v3, p3

    move-object v4, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Anz;-><init>(LX/0bH;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V

    .line 356555
    const-class v1, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v7, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/activity/FbFragmentActivity;

    .line 356556
    if-nez v1, :cond_2

    .line 356557
    :cond_0
    :goto_0
    return-void

    .line 356558
    :cond_1
    invoke-static {p5, p2, p3, p0, p4}, LX/212;->b(LX/0bH;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V

    goto :goto_0

    .line 356559
    :cond_2
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    .line 356560
    if-eqz v2, :cond_0

    .line 356561
    const-string v1, "MisinformationShareDialogFragment"

    invoke-virtual {v2, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;

    .line 356562
    if-nez v1, :cond_0

    .line 356563
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bL()Ljava/lang/String;

    move-result-object v3

    .line 356564
    sput-object v0, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 356565
    new-instance v4, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;

    invoke-direct {v4}, Lcom/facebook/feedplugins/base/footer/ui/MisinformationShareDialogFragment;-><init>()V

    .line 356566
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 356567
    const-string v6, "title"

    invoke-virtual {v7, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356568
    const-string v6, "message"

    invoke-virtual {v7, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356569
    invoke-virtual {v4, v7}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 356570
    move-object v1, v4

    .line 356571
    const-string v3, "MisinformationShareDialogFragment"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/20n;ZLX/1WR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "LX/20n;",
            "Z",
            "LX/1WR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356528
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356529
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356530
    const-string v1, "clicked_on_comment"

    invoke-static {p5, v0, v1}, LX/1WR;->a(LX/1WR;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 356531
    invoke-virtual {p3, p0, p1, p2, p4}, LX/20n;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Z)V

    .line 356532
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/217;LX/1PT;LX/AnQ;LX/1WR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/217;",
            "LX/1PT;",
            "LX/AnQ;",
            "LX/1WR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356523
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356524
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p4, v0}, LX/1WR;->f(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 356525
    invoke-static {p2}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v0

    .line 356526
    invoke-virtual {p3, p0, v0}, LX/AnQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 356527
    return-void
.end method

.method public static a(ZZZLjava/util/EnumMap;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 356512
    invoke-virtual {p3}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 356513
    sget-object v0, LX/20X;->LIKE:LX/20X;

    invoke-virtual {p3, v0}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne p0, v0, :cond_0

    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {p3, v0}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    sget-object v0, LX/20X;->SHARE:LX/20X;

    invoke-virtual {p3, v0}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eq p2, v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 356514
    if-nez v0, :cond_2

    .line 356515
    :cond_1
    :goto_1
    return-void

    .line 356516
    :cond_2
    invoke-virtual {p3}, Ljava/util/EnumMap;->clear()V

    .line 356517
    :cond_3
    if-eqz p0, :cond_4

    .line 356518
    sget-object v0, LX/20X;->LIKE:LX/20X;

    invoke-static {p4, p3, v0}, LX/212;->a(LX/0Or;Ljava/util/EnumMap;LX/20X;)V

    .line 356519
    :cond_4
    if-eqz p1, :cond_5

    .line 356520
    sget-object v0, LX/20X;->COMMENT:LX/20X;

    invoke-static {p4, p3, v0}, LX/212;->a(LX/0Or;Ljava/util/EnumMap;LX/20X;)V

    .line 356521
    :cond_5
    if-eqz p2, :cond_1

    .line 356522
    sget-object v0, LX/20X;->SHARE:LX/20X;

    invoke-static {p4, p3, v0}, LX/212;->a(LX/0Or;Ljava/util/EnumMap;LX/20X;)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0bH;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "LX/20p;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356509
    new-instance v0, LX/2Ka;

    invoke-direct {v0}, LX/2Ka;-><init>()V

    invoke-virtual {p0, v0}, LX/0b4;->a(LX/0b7;)V

    .line 356510
    invoke-virtual {p1, p2, p3, p4}, LX/20p;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V

    .line 356511
    return-void
.end method
