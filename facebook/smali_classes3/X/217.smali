.class public LX/217;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356695
    return-void
.end method

.method public static a(LX/0QB;)LX/217;
    .locals 1

    .prologue
    .line 356691
    new-instance v0, LX/217;

    invoke-direct {v0}, LX/217;-><init>()V

    .line 356692
    move-object v0, v0

    .line 356693
    return-object v0
.end method

.method public static a(LX/1PT;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 356679
    sget-object v0, LX/218;->a:[I

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 356680
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 356681
    :pswitch_0
    sget-object v0, LX/1EO;->NEWSFEED:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356682
    :pswitch_1
    sget-object v0, LX/1EO;->TIMELINE:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356683
    :pswitch_2
    sget-object v0, LX/1EO;->PERMALINK:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356684
    :pswitch_3
    sget-object v0, LX/1EO;->GROUP:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356685
    :pswitch_4
    sget-object v0, LX/1EO;->SEARCH:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356686
    :pswitch_5
    sget-object v0, LX/1EO;->EVENT:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356687
    :pswitch_6
    sget-object v0, LX/1EO;->PAGE:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356688
    :pswitch_7
    sget-object v0, LX/1EO;->REACTION:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356689
    :pswitch_8
    sget-object v0, LX/1EO;->VIDEO_CHANNEL:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    .line 356690
    :pswitch_9
    sget-object v0, LX/1EO;->GOOD_FRIENDS:LX/1EO;

    iget-object v0, v0, LX/1EO;->analyticModule:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static b(LX/1PT;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 356668
    sget-object v0, LX/218;->a:[I

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 356669
    :pswitch_0
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 356670
    :pswitch_1
    const-string v0, "newsfeed_ufi"

    goto :goto_0

    .line 356671
    :pswitch_2
    const-string v0, "timeline_ufi"

    goto :goto_0

    .line 356672
    :pswitch_3
    const-string v0, "permalink_ufi"

    goto :goto_0

    .line 356673
    :pswitch_4
    const-string v0, "groups_ufi"

    goto :goto_0

    .line 356674
    :pswitch_5
    const-string v0, "search_ufi"

    goto :goto_0

    .line 356675
    :pswitch_6
    const-string v0, "events_ufi"

    goto :goto_0

    .line 356676
    :pswitch_7
    const-string v0, "pages_identity_ufi"

    goto :goto_0

    .line 356677
    :pswitch_8
    const-string v0, "reactions_ufi"

    goto :goto_0

    .line 356678
    :pswitch_9
    const-string v0, "good_friends_ufi"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method
