.class public LX/1v7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342018
    return-void
.end method


# virtual methods
.method public final a()LX/0yY;
    .locals 1

    .prologue
    .line 342019
    sget-object v0, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 342020
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 342021
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 342022
    const-class v1, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342023
    const/4 v0, 0x1

    .line 342024
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
