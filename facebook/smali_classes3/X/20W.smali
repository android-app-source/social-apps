.class public LX/20W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/39j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/39j",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1VI;


# direct methods
.method public constructor <init>(LX/39j;LX/0Ot;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/39j;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354949
    iput-object p1, p0, LX/20W;->a:LX/39j;

    .line 354950
    iput-object p2, p0, LX/20W;->b:LX/0Ot;

    .line 354951
    iput-object p3, p0, LX/20W;->c:LX/1VI;

    .line 354952
    return-void
.end method

.method public static a(Landroid/content/Context;LX/1VI;)I
    .locals 2

    .prologue
    .line 354935
    invoke-virtual {p1}, LX/1VI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0102a4

    const v1, -0xe2ded7

    invoke-static {p0, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0102a3

    const v1, -0xb1a99b

    invoke-static {p0, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/20W;
    .locals 6

    .prologue
    .line 354937
    const-class v1, LX/20W;

    monitor-enter v1

    .line 354938
    :try_start_0
    sget-object v0, LX/20W;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 354939
    sput-object v2, LX/20W;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 354940
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354941
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 354942
    new-instance v5, LX/20W;

    invoke-static {v0}, LX/39j;->a(LX/0QB;)LX/39j;

    move-result-object v3

    check-cast v3, LX/39j;

    const/16 v4, 0x3b2

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v4

    check-cast v4, LX/1VI;

    invoke-direct {v5, v3, p0, v4}, LX/20W;-><init>(LX/39j;LX/0Ot;LX/1VI;)V

    .line 354943
    move-object v0, v5

    .line 354944
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 354945
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354946
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 354947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 354936
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081995

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
