.class public LX/1zO;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353180
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 353181
    iput-object p1, p0, LX/1zO;->a:Landroid/content/Context;

    .line 353182
    const v0, 0x7f0a0042

    iput v0, p0, LX/1zO;->b:I

    .line 353183
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 353184
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 353185
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 353186
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 353187
    iget-object v0, p0, LX/1zO;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LX/1zO;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 353188
    return-void
.end method
