.class public LX/1xu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349785
    iput-object p1, p0, LX/1xu;->a:LX/0Ot;

    .line 349786
    return-void
.end method

.method public static a(LX/0QB;)LX/1xu;
    .locals 4

    .prologue
    .line 349787
    const-class v1, LX/1xu;

    monitor-enter v1

    .line 349788
    :try_start_0
    sget-object v0, LX/1xu;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349789
    sput-object v2, LX/1xu;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349792
    new-instance v3, LX/1xu;

    const/16 p0, 0x767

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xu;-><init>(LX/0Ot;)V

    .line 349793
    move-object v0, v3

    .line 349794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/text/Spannable;LX/1xz;)LX/2nQ;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ":",
            "LX/1Pq;",
            ">(",
            "Landroid/text/Spannable;",
            "LX/1xz;",
            ")",
            "LX/2nQ;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 349798
    iget-object v0, p0, LX/1xu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;

    invoke-interface {p2, p1}, LX/1xz;->a(Landroid/text/Spannable;)I

    move-result v1

    invoke-interface {p2}, LX/1xz;->b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 349799
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 349800
    :cond_0
    const/4 v3, 0x0

    .line 349801
    :goto_0
    move-object v0, v3

    .line 349802
    return-object v0

    .line 349803
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v3

    .line 349804
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    .line 349805
    iget-object v4, v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0b0051

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 349806
    new-instance v7, Ljava/util/TreeSet;

    sget-object v4, LX/1vv;->a:Ljava/util/Comparator;

    invoke-direct {v7, v4}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 349807
    new-instance v4, LX/2nQ;

    invoke-direct {v4, p1}, LX/2nQ;-><init>(Ljava/lang/CharSequence;)V

    .line 349808
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLImageAtRange;

    .line 349809
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLEntityWithImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 349810
    :try_start_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->k()I

    move-result v9

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->j()I

    move-result v10

    invoke-static {v5, v9, v10}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v9

    .line 349811
    new-instance v10, LX/1yN;

    .line 349812
    iget p0, v9, LX/1yN;->a:I

    move p0, p0

    .line 349813
    add-int/2addr p0, v1

    .line 349814
    iget p2, v9, LX/1yN;->b:I

    move v9, p2

    .line 349815
    invoke-direct {v10, p0, v9}, LX/1yN;-><init>(II)V

    .line 349816
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImageAtRange;->a()Lcom/facebook/graphql/model/GraphQLEntityWithImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEntityWithImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 349817
    new-instance v9, LX/34R;

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v3

    invoke-static {p2, v3, v6}, LX/1vv;->a(III)I

    move-result v3

    invoke-direct {v9, p0, v10, v3, v6}, LX/34R;-><init>(Landroid/net/Uri;LX/1yN;II)V

    invoke-virtual {v7, v9}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 349818
    :catch_0
    move-exception v3

    .line 349819
    const-string v9, "FrescoSpannableImageRangeApplicator"

    invoke-virtual {v3}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 349820
    :cond_3
    invoke-virtual {v7}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/34R;

    .line 349821
    iget-object v6, v0, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->c:LX/1vv;

    const/4 v7, 0x2

    sget-object v8, Lcom/facebook/feed/spannable/FrescoSpannableImageRangeApplicator;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v4, v3, v7, v8}, LX/1vv;->a(LX/2nQ;LX/34R;ILcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2

    :cond_4
    move-object v3, v4

    .line 349822
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/1xz;LX/1Pr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ":",
            "LX/1Pq;",
            ">(",
            "LX/1xz;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 349823
    invoke-interface {p1}, LX/1xz;->a()LX/1KL;

    move-result-object v0

    invoke-interface {p1}, LX/1xz;->c()LX/0jW;

    move-result-object v1

    invoke-interface {p2, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yT;

    .line 349824
    iget-boolean v1, v0, LX/1yT;->b:Z

    if-nez v1, :cond_0

    .line 349825
    iget-object v1, v0, LX/1yT;->a:Landroid/text/Spannable;

    invoke-direct {p0, v1, p1}, LX/1xu;->a(Landroid/text/Spannable;LX/1xz;)LX/2nQ;

    move-result-object v1

    .line 349826
    if-nez v1, :cond_1

    .line 349827
    invoke-interface {p1}, LX/1xz;->a()LX/1KL;

    move-result-object v1

    new-instance v2, LX/1yT;

    iget-object v0, v0, LX/1yT;->a:Landroid/text/Spannable;

    invoke-direct {v2, v0, v3}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    invoke-interface {p2, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 349828
    :cond_0
    :goto_0
    return-void

    .line 349829
    :cond_1
    invoke-interface {p1}, LX/1xz;->a()LX/1KL;

    move-result-object v0

    new-instance v2, LX/1yT;

    invoke-direct {v2, v1, v3, v1}, LX/1yT;-><init>(Landroid/text/Spannable;ZLX/2nR;)V

    invoke-interface {p2, v0, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    goto :goto_0
.end method
