.class public LX/1wU;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 345994
    const-class v0, LX/1wU;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1wU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 345990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345991
    iput-object p1, p0, LX/1wU;->b:LX/0Or;

    .line 345992
    iput-object p2, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 345993
    return-void
.end method

.method public static a(LX/0QB;)LX/1wU;
    .locals 1

    .prologue
    .line 345989
    invoke-static {p0}, LX/1wU;->b(LX/0QB;)LX/1wU;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/1wU;
    .locals 3

    .prologue
    .line 345945
    new-instance v1, LX/1wU;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v2, v0}, LX/1wU;-><init>(LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 345946
    return-object v1
.end method

.method public static d(LX/1wU;)LX/0Tn;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 345983
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 345984
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345985
    const/4 v0, 0x0

    .line 345986
    :goto_0
    return-object v0

    .line 345987
    :cond_0
    sget-object v1, LX/2vf;->c:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    move-object v0, v1

    .line 345988
    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    .prologue
    .line 345964
    invoke-static {p0}, LX/1wU;->d(LX/1wU;)LX/0Tn;

    move-result-object v1

    .line 345965
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 345966
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 345967
    const/4 v0, 0x0

    .line 345968
    :goto_0
    move-object v2, v0

    .line 345969
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0, v3}, LX/1nR;->a(Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/0Tn;

    move-result-object v3

    .line 345970
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 345971
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 345972
    :cond_0
    :goto_1
    return-void

    .line 345973
    :cond_1
    iget-object v4, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-interface {v4, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 345974
    iget-object v1, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 345975
    iget-object v1, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v3, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 345976
    iget-object v1, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 345977
    if-nez p1, :cond_0

    .line 345978
    iget-object v0, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2vf;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 345979
    iget-object v0, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2vf;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 345980
    goto :goto_1

    .line 345981
    :cond_2
    sget-object v2, LX/2vf;->d:LX/0Tn;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    move-object v0, v2

    .line 345982
    goto :goto_0
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 345955
    invoke-static {p0}, LX/1wU;->d(LX/1wU;)LX/0Tn;

    move-result-object v3

    .line 345956
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 345957
    if-eqz v3, :cond_0

    if-nez v0, :cond_1

    .line 345958
    :cond_0
    :goto_0
    return v2

    .line 345959
    :cond_1
    iget-object v4, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v4

    .line 345960
    iget-object v0, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v3

    .line 345961
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq v4, v0, :cond_2

    if-eq v4, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-virtual {v4}, LX/03R;->name()Ljava/lang/String;

    invoke-virtual {v3}, LX/03R;->name()Ljava/lang/String;

    .line 345962
    sget-object v0, LX/03R;->UNSET:LX/03R;

    if-eq v4, v0, :cond_0

    if-eq v4, v3, :cond_0

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 345963
    goto :goto_1
.end method

.method public final b()LX/03R;
    .locals 3

    .prologue
    .line 345947
    invoke-static {p0}, LX/1wU;->d(LX/1wU;)LX/0Tn;

    move-result-object v1

    .line 345948
    iget-object v0, p0, LX/1wU;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 345949
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 345950
    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 345951
    :goto_0
    return-object v0

    .line 345952
    :cond_1
    invoke-virtual {p0}, LX/1wU;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 345953
    iget-object v2, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/1wU;->a(Z)V

    .line 345954
    :cond_2
    iget-object v0, p0, LX/1wU;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v0

    goto :goto_0
.end method
