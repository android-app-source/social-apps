.class public LX/22E;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/22E;

.field private static final b:LX/22E;

.field private static final c:LX/22E;

.field private static final d:LX/22E;

.field private static final e:LX/22E;

.field private static final f:LX/22E;

.field private static final g:LX/22E;

.field private static final h:LX/22E;

.field private static final i:LX/22E;

.field private static final j:LX/22E;

.field public static final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/22E;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final l:I

.field public final m:LX/22F;

.field public final n:LX/22F;

.field public final o:LX/22F;

.field public final p:LX/22F;

.field private final q:I

.field public final r:I

.field public final s:I


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v13, 0x1

    const v12, -0xf45eb

    const/4 v2, 0x0

    .line 359336
    new-instance v0, LX/22E;

    const/4 v1, -0x1

    sget-object v4, LX/22F;->b:LX/22F;

    sget-object v5, LX/22F;->b:LX/22F;

    sget-object v6, LX/22F;->b:LX/22F;

    sget-object v7, LX/22F;->b:LX/22F;

    const/high16 v8, -0x1000000

    move v3, v2

    invoke-direct/range {v0 .. v8}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v0, LX/22E;->a:LX/22E;

    .line 359337
    new-instance v0, LX/22E;

    const v3, 0x7f080fc8

    new-instance v4, LX/22F;

    const v1, 0x7f0215f2

    const-string v5, "ufi/reactions/v2/16x16/like"

    invoke-direct {v4, v1, v5}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v5, LX/22F;

    const v1, 0x7f0215cd

    const-string v6, "ufi/reactions/v2/84x84/like"

    invoke-direct {v5, v1, v6}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v6, LX/22F;

    const v1, 0x7f0215f1

    const-string v7, "ufi/reactions/v2/24x24/like"

    invoke-direct {v6, v1, v7}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v7, LX/22F;

    const-string v1, "ufi/reactions/v2/face_models/like"

    invoke-direct {v7, v2, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    const v8, -0xa76f01

    move v1, v13

    invoke-direct/range {v0 .. v8}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v0, LX/22E;->b:LX/22E;

    .line 359338
    new-instance v3, LX/22E;

    const v6, 0x7f081986

    new-instance v7, LX/22F;

    const v0, 0x7f0215f4

    const-string v1, "ufi/reactions/v2/16x16/love"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v8, LX/22F;

    const v0, 0x7f0215cf

    const-string v1, "ufi/reactions/v2/84x84/love"

    invoke-direct {v8, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v9, LX/22F;

    const v0, 0x7f0215f3

    const-string v1, "ufi/reactions/v2/24x24/love"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, "ufi/reactions/v2/face_models/love"

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    const v11, -0xdad98

    move v4, v14

    move v5, v13

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->c:LX/22E;

    .line 359339
    new-instance v3, LX/22E;

    const v6, 0x7f081987

    new-instance v7, LX/22F;

    const v0, 0x7f0215fe

    const-string v1, "ufi/reactions/v2/16x16/wow"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v8, LX/22F;

    const v0, 0x7f0215e3

    const-string v1, "ufi/reactions/v2/84x84/wow"

    invoke-direct {v8, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v9, LX/22F;

    const v0, 0x7f0215fd

    const-string v1, "ufi/reactions/v2/24x24/wow"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, "ufi/reactions/v2/face_models/wow"

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    move v4, v15

    move v5, v14

    move v11, v12

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->d:LX/22E;

    .line 359340
    new-instance v3, LX/22E;

    const/4 v4, 0x4

    const v6, 0x7f081988

    new-instance v7, LX/22F;

    const v0, 0x7f0215ee

    const-string v1, "ufi/reactions/v2/16x16/haha"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v8, LX/22F;

    const v0, 0x7f0215ca

    const-string v1, "ufi/reactions/v2/84x84/haha"

    invoke-direct {v8, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v9, LX/22F;

    const v0, 0x7f0215ed

    const-string v1, "ufi/reactions/v2/24x24/haha"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, "ufi/reactions/v2/face_models/haha"

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    move v5, v15

    move v11, v12

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->e:LX/22E;

    .line 359341
    new-instance v3, LX/22E;

    const/4 v4, 0x5

    const/4 v5, 0x4

    new-instance v7, LX/22F;

    const v0, 0x7f021600

    const-string v1, "ufi/reactions/v2/16x16/yay"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    sget-object v8, LX/22F;->a:LX/22F;

    new-instance v9, LX/22F;

    const v0, 0x7f0215ff

    const-string v1, "ufi/reactions/v2/24x24/yay"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, ""

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    move v6, v2

    move v11, v12

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->f:LX/22E;

    .line 359342
    new-instance v3, LX/22E;

    const/4 v4, 0x7

    const/4 v5, 0x6

    const v6, 0x7f081989

    new-instance v7, LX/22F;

    const v0, 0x7f0215f9

    const-string v1, "ufi/reactions/v2/16x16/sad"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v8, LX/22F;

    const v0, 0x7f0215dd

    const-string v1, "ufi/reactions/v2/84x84/sad"

    invoke-direct {v8, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v9, LX/22F;

    const v0, 0x7f0215f8

    const-string v1, "ufi/reactions/v2/24x24/sad"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, "ufi/reactions/v2/face_models/sad"

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    move v11, v12

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->g:LX/22E;

    .line 359343
    new-instance v3, LX/22E;

    const/16 v4, 0x8

    const/4 v5, 0x5

    const v6, 0x7f08198a

    new-instance v7, LX/22F;

    const v0, 0x7f0215e5

    const-string v1, "ufi/reactions/v2/16x16/anger"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v8, LX/22F;

    const v0, 0x7f0215b6

    const-string v1, "ufi/reactions/v2/84x84/anger"

    invoke-direct {v8, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v9, LX/22F;

    const v0, 0x7f0215e4

    const-string v1, "ufi/reactions/v2/24x24/anger"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, "ufi/reactions/v2/face_models/anger"

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    const v11, -0x88eb5

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->h:LX/22E;

    .line 359344
    new-instance v3, LX/22E;

    const/16 v4, 0xa

    const/4 v5, 0x7

    new-instance v7, LX/22F;

    const v0, 0x7f0215e7

    const-string v1, "ufi/reactions/v2/16x16/confused"

    invoke-direct {v7, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    sget-object v8, LX/22F;->a:LX/22F;

    new-instance v9, LX/22F;

    const v0, 0x7f0215e6

    const-string v1, "ufi/reactions/v2/24x24/confused"

    invoke-direct {v9, v0, v1}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v10, LX/22F;

    const-string v0, ""

    invoke-direct {v10, v2, v0}, LX/22F;-><init>(ILjava/lang/String;)V

    move v6, v2

    move v11, v12

    invoke-direct/range {v3 .. v11}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v3, LX/22E;->i:LX/22E;

    .line 359345
    new-instance v0, LX/22E;

    const/16 v1, 0xb

    const v3, 0x7f08198b

    new-instance v4, LX/22F;

    const v5, 0x7f0215fc

    const-string v6, "ufi/reactions/v6/16x16/dorothy"

    invoke-direct {v4, v5, v6}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v5, LX/22F;

    const v6, 0x7f0215df

    const-string v7, "ufi/reactions/v6/84x84/dorothy"

    invoke-direct {v5, v6, v7}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v6, LX/22F;

    const v7, 0x7f0215fb

    const-string v8, "ufi/reactions/v6/24x24/dorothy"

    invoke-direct {v6, v7, v8}, LX/22F;-><init>(ILjava/lang/String;)V

    new-instance v7, LX/22F;

    const-string v8, "ufi/reactions/v4/face_models/dorothy"

    invoke-direct {v7, v2, v8}, LX/22F;-><init>(ILjava/lang/String;)V

    const v8, -0x63782f

    invoke-direct/range {v0 .. v8}, LX/22E;-><init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V

    sput-object v0, LX/22E;->j:LX/22E;

    .line 359346
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->b:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->c:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->e:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->f:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->d:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->i:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->g:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->h:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    sget-object v1, LX/22E;->j:LX/22E;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    sput-object v0, LX/22E;->k:LX/0Px;

    return-void
.end method

.method private constructor <init>(IIILX/22F;LX/22F;LX/22F;LX/22F;I)V
    .locals 0

    .prologue
    .line 359319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359320
    iput p1, p0, LX/22E;->l:I

    .line 359321
    iput p2, p0, LX/22E;->q:I

    .line 359322
    iput p3, p0, LX/22E;->r:I

    .line 359323
    iput-object p4, p0, LX/22E;->m:LX/22F;

    .line 359324
    iput-object p5, p0, LX/22E;->n:LX/22F;

    .line 359325
    iput-object p6, p0, LX/22E;->o:LX/22F;

    .line 359326
    iput-object p7, p0, LX/22E;->p:LX/22F;

    .line 359327
    iput p8, p0, LX/22E;->s:I

    .line 359328
    return-void
.end method

.method public static a(I)LX/22E;
    .locals 5

    .prologue
    .line 359329
    sget-object v0, LX/22E;->k:LX/0Px;

    move-object v2, v0

    .line 359330
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22E;

    .line 359331
    iget v4, v0, LX/22E;->l:I

    move v4, v4

    .line 359332
    if-ne v4, p0, :cond_0

    .line 359333
    :goto_1
    return-object v0

    .line 359334
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 359335
    :cond_1
    sget-object v0, LX/22E;->a:LX/22E;

    goto :goto_1
.end method
