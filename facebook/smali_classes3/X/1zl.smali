.class public LX/1zl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/1zl;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/1zm;

.field public final e:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/3K3;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354105
    const-class v0, LX/1zl;

    sput-object v0, LX/1zl;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/1zm;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354098
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/1zl;->e:LX/0YU;

    .line 354099
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1zl;->f:Ljava/util/Map;

    .line 354100
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1zl;->g:Ljava/util/Set;

    .line 354101
    iput-object p1, p0, LX/1zl;->b:Landroid/content/res/Resources;

    .line 354102
    iput-object p2, p0, LX/1zl;->c:Ljava/util/concurrent/ExecutorService;

    .line 354103
    iput-object p3, p0, LX/1zl;->d:LX/1zm;

    .line 354104
    return-void
.end method

.method public static a(LX/0QB;)LX/1zl;
    .locals 6

    .prologue
    .line 354084
    sget-object v0, LX/1zl;->h:LX/1zl;

    if-nez v0, :cond_1

    .line 354085
    const-class v1, LX/1zl;

    monitor-enter v1

    .line 354086
    :try_start_0
    sget-object v0, LX/1zl;->h:LX/1zl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 354087
    if-eqz v2, :cond_0

    .line 354088
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 354089
    new-instance p0, LX/1zl;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1zm;->a(LX/0QB;)LX/1zm;

    move-result-object v5

    check-cast v5, LX/1zm;

    invoke-direct {p0, v3, v4, v5}, LX/1zl;-><init>(Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/1zm;)V

    .line 354090
    move-object v0, p0

    .line 354091
    sput-object v0, LX/1zl;->h:LX/1zl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354092
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 354093
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 354094
    :cond_1
    sget-object v0, LX/1zl;->h:LX/1zl;

    return-object v0

    .line 354095
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 354096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 354106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/1zl;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 354083
    iget-object v0, p0, LX/1zl;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1zl;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized c(LX/1zl;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 354079
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1zl;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 354080
    iget-object v0, p0, LX/1zl;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354081
    monitor-exit p0

    return-void

    .line 354082
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(I)LX/3K3;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 354072
    iget-object v0, p0, LX/1zl;->e:LX/0YU;

    invoke-virtual {v0, p1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3K3;

    return-object v0
.end method

.method public final a(ILjava/io/File;Z)V
    .locals 8

    .prologue
    .line 354073
    const-string v0, "disk_face_"

    invoke-static {v0, p1}, LX/1zl;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 354074
    if-nez p3, :cond_0

    invoke-static {p0, v2}, LX/1zl;->b(LX/1zl;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354075
    :goto_0
    return-void

    .line 354076
    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, LX/1zl;->f:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354077
    iget-object v0, p0, LX/1zl;->f:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 354078
    :cond_1
    iget-object v6, p0, LX/1zl;->f:Ljava/util/Map;

    iget-object v7, p0, LX/1zl;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v4}, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareDiskFaceRunnable;-><init>(LX/1zl;Ljava/lang/String;ILjava/io/File;)V

    const v1, 0x2bcb20e

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
