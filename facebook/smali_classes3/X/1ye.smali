.class public LX/1ye;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yj;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1ye",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351006
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 351007
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1ye;->b:LX/0Zi;

    .line 351008
    iput-object p1, p0, LX/1ye;->a:LX/0Ot;

    .line 351009
    return-void
.end method

.method public static a(LX/0QB;)LX/1ye;
    .locals 4

    .prologue
    .line 350995
    const-class v1, LX/1ye;

    monitor-enter v1

    .line 350996
    :try_start_0
    sget-object v0, LX/1ye;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 350997
    sput-object v2, LX/1ye;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 350998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351000
    new-instance v3, LX/1ye;

    const/16 p0, 0x71b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1ye;-><init>(LX/0Ot;)V

    .line 351001
    move-object v0, v3

    .line 351002
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351003
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ye;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351004
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 15

    .prologue
    .line 350916
    check-cast p2, LX/1yh;

    .line 350917
    iget-object v1, p0, LX/1ye;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yj;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/1yh;->b:LX/1Pb;

    move-object/from16 v0, p2

    iget-boolean v5, v0, LX/1yh;->c:Z

    move-object/from16 v0, p2

    iget-boolean v6, v0, LX/1yh;->d:Z

    move-object/from16 v0, p2

    iget-boolean v7, v0, LX/1yh;->e:Z

    move-object/from16 v0, p2

    iget-boolean v8, v0, LX/1yh;->f:Z

    move-object/from16 v0, p2

    iget v9, v0, LX/1yh;->g:I

    move-object/from16 v0, p2

    iget v10, v0, LX/1yh;->h:I

    move-object/from16 v0, p2

    iget v11, v0, LX/1yh;->i:I

    move-object/from16 v0, p2

    iget v12, v0, LX/1yh;->j:I

    move-object/from16 v0, p2

    iget v13, v0, LX/1yh;->k:I

    move-object/from16 v0, p2

    iget v14, v0, LX/1yh;->l:I

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v14}, LX/1yj;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;ZZZZIIIIII)LX/1Dg;

    move-result-object v1

    .line 350918
    return-object v1
.end method

.method public final a(LX/1De;II)LX/1yi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350988
    new-instance v0, LX/1yh;

    invoke-direct {v0, p0}, LX/1yh;-><init>(LX/1ye;)V

    .line 350989
    iget-object v1, p0, LX/1ye;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yi;

    .line 350990
    if-nez v1, :cond_0

    .line 350991
    new-instance v1, LX/1yi;

    invoke-direct {v1, p0}, LX/1yi;-><init>(LX/1ye;)V

    .line 350992
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/1yi;->a$redex0(LX/1yi;LX/1De;IILX/1yh;)V

    .line 350993
    move-object v0, v1

    .line 350994
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 350986
    invoke-static {}, LX/1dS;->b()V

    .line 350987
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 350919
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 350920
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 350921
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 350922
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 350923
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 350924
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 350925
    iget-object v0, p0, LX/1ye;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 v8, 0x0

    .line 350926
    sget-object v7, LX/03r;->FeedStoryHeaderActionButtonComponent:[I

    invoke-virtual {v0, v7, v8}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 350927
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v10

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_6

    .line 350928
    invoke-virtual {v9, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result p0

    .line 350929
    const/16 p1, 0x0

    if-ne p0, p1, :cond_1

    .line 350930
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350931
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 350932
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 350933
    :cond_1
    const/16 p1, 0x1

    if-ne p0, p1, :cond_2

    .line 350934
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350935
    iput-object p0, v2, LX/1np;->a:Ljava/lang/Object;

    .line 350936
    goto :goto_1

    .line 350937
    :cond_2
    const/16 p1, 0x2

    if-ne p0, p1, :cond_3

    .line 350938
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350939
    iput-object p0, v3, LX/1np;->a:Ljava/lang/Object;

    .line 350940
    goto :goto_1

    .line 350941
    :cond_3
    const/16 p1, 0x3

    if-ne p0, p1, :cond_4

    .line 350942
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350943
    iput-object p0, v4, LX/1np;->a:Ljava/lang/Object;

    .line 350944
    goto :goto_1

    .line 350945
    :cond_4
    const/16 p1, 0x4

    if-ne p0, p1, :cond_5

    .line 350946
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350947
    iput-object p0, v5, LX/1np;->a:Ljava/lang/Object;

    .line 350948
    goto :goto_1

    .line 350949
    :cond_5
    const/16 p1, 0x5

    if-ne p0, p1, :cond_0

    .line 350950
    invoke-virtual {v9, p0, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 350951
    iput-object p0, v6, LX/1np;->a:Ljava/lang/Object;

    .line 350952
    goto :goto_1

    .line 350953
    :cond_6
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 350954
    check-cast p2, LX/1yh;

    .line 350955
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350956
    if-eqz v0, :cond_7

    .line 350957
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350958
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->g:I

    .line 350959
    :cond_7
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 350960
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350961
    if-eqz v0, :cond_8

    .line 350962
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350963
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->h:I

    .line 350964
    :cond_8
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 350965
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350966
    if-eqz v0, :cond_9

    .line 350967
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350968
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->i:I

    .line 350969
    :cond_9
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 350970
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350971
    if-eqz v0, :cond_a

    .line 350972
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350973
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->j:I

    .line 350974
    :cond_a
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 350975
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350976
    if-eqz v0, :cond_b

    .line 350977
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350978
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->k:I

    .line 350979
    :cond_b
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 350980
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350981
    if-eqz v0, :cond_c

    .line 350982
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 350983
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yh;->l:I

    .line 350984
    :cond_c
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 350985
    return-void
.end method
