.class public LX/1xc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1xc;


# instance fields
.field private final a:LX/1xd;


# direct methods
.method public constructor <init>(LX/1xd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349054
    iput-object p1, p0, LX/1xc;->a:LX/1xd;

    .line 349055
    return-void
.end method

.method public static a(LX/0QB;)LX/1xc;
    .locals 4

    .prologue
    .line 349040
    sget-object v0, LX/1xc;->b:LX/1xc;

    if-nez v0, :cond_1

    .line 349041
    const-class v1, LX/1xc;

    monitor-enter v1

    .line 349042
    :try_start_0
    sget-object v0, LX/1xc;->b:LX/1xc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 349043
    if-eqz v2, :cond_0

    .line 349044
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 349045
    new-instance p0, LX/1xc;

    invoke-static {v0}, LX/1xd;->a(LX/0QB;)LX/1xd;

    move-result-object v3

    check-cast v3, LX/1xd;

    invoke-direct {p0, v3}, LX/1xc;-><init>(LX/1xd;)V

    .line 349046
    move-object v0, p0

    .line 349047
    sput-object v0, LX/1xc;->b:LX/1xc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349048
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 349049
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349050
    :cond_1
    sget-object v0, LX/1xc;->b:LX/1xc;

    return-object v0

    .line 349051
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 349052
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 349024
    if-nez p0, :cond_1

    .line 349025
    :cond_0
    :goto_0
    return v0

    .line 349026
    :cond_1
    invoke-static {p0}, LX/1xc;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v1

    .line 349027
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349038
    invoke-static {p0}, LX/0x1;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    .line 349039
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->k()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 349032
    invoke-static {p1}, LX/1xc;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v1

    .line 349033
    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 349034
    :cond_0
    :goto_0
    return-object p2

    .line 349035
    :cond_1
    instance-of v0, p2, Landroid/text/Spannable;

    if-eqz v0, :cond_2

    check-cast p2, Landroid/text/Spannable;

    .line 349036
    :goto_1
    iget-object v0, p0, LX/1xc;->a:LX/1xd;

    invoke-virtual {v0, v1, p2}, LX/1xd;->a(LX/0Px;Landroid/text/Spannable;)V

    goto :goto_0

    .line 349037
    :cond_2
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object p2, v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/Spannable;)V
    .locals 2
    .param p2    # Landroid/text/Spannable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 349028
    invoke-static {p1}, LX/1xc;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v0

    .line 349029
    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 349030
    :cond_0
    :goto_0
    return-void

    .line 349031
    :cond_1
    iget-object v1, p0, LX/1xc;->a:LX/1xd;

    invoke-virtual {v1, v0, p2}, LX/1xd;->a(LX/0Px;Landroid/text/Spannable;)V

    goto :goto_0
.end method
