.class public LX/1zY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:I

.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/03V;

.field private final c:LX/1zZ;

.field private final d:LX/1za;

.field private final e:LX/154;

.field private final f:LX/153;

.field private final g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 353706
    invoke-static {v0, v0}, LX/1mh;->a(II)I

    move-result v0

    sput v0, LX/1zY;->a:I

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1zZ;LX/1za;LX/154;LX/153;LX/0Or;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/feedback/reactions/ui/PillsBlingBarTextSize;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1zZ;",
            "LX/1za;",
            "LX/154;",
            "LX/153;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353699
    iput-object p1, p0, LX/1zY;->b:LX/03V;

    .line 353700
    iput-object p2, p0, LX/1zY;->c:LX/1zZ;

    .line 353701
    iput-object p4, p0, LX/1zY;->e:LX/154;

    .line 353702
    iput-object p3, p0, LX/1zY;->d:LX/1za;

    .line 353703
    iput-object p5, p0, LX/1zY;->f:LX/153;

    .line 353704
    invoke-interface {p6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/1zY;->g:I

    .line 353705
    return-void
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 353697
    if-lez p0, :cond_0

    add-int v0, p0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/1De;I)I
    .locals 2

    .prologue
    const v1, -0x6e685d

    .line 353696
    if-ne p1, v1, :cond_0

    const v0, 0x1010098

    invoke-static {p0, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result p1

    :cond_0
    return p1
.end method

.method private a(LX/1De;Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1no;IIIFFF)LX/1Di;
    .locals 11

    .prologue
    .line 353694
    iget-object v0, p0, LX/1zY;->f:LX/153;

    invoke-virtual {v0, p2}, LX/153;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    .line 353695
    invoke-static/range {v0 .. v10}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1De;Lcom/facebook/graphql/model/GraphQLFeedback;ZILX/1no;)LX/1Di;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353671
    iget-object v0, p0, LX/1zY;->d:LX/1za;

    invoke-virtual {v0, p2}, LX/1za;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    .line 353672
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353673
    iput v2, p5, LX/1no;->a:I

    .line 353674
    iput v2, p5, LX/1no;->b:I

    .line 353675
    const/4 v0, 0x0

    .line 353676
    :goto_0
    return-object v0

    .line 353677
    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    iget-object v2, p0, LX/1zY;->c:LX/1zZ;

    .line 353678
    new-instance p0, LX/20A;

    invoke-direct {p0, v2}, LX/20A;-><init>(LX/1zZ;)V

    .line 353679
    sget-object p2, LX/1zZ;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/20B;

    .line 353680
    if-nez p2, :cond_1

    .line 353681
    new-instance p2, LX/20B;

    invoke-direct {p2}, LX/20B;-><init>()V

    .line 353682
    :cond_1
    invoke-static {p2, p1, p0}, LX/20B;->a$redex0(LX/20B;LX/1De;LX/20A;)V

    .line 353683
    move-object p0, p2

    .line 353684
    move-object v2, p0

    .line 353685
    iget-object p0, v2, LX/20B;->a:LX/20A;

    iput-object v0, p0, LX/20A;->a:Ljava/util/List;

    .line 353686
    iget-object p0, v2, LX/20B;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 353687
    move-object v0, v2

    .line 353688
    iget-object v2, v0, LX/20B;->a:LX/20A;

    iput-boolean p3, v2, LX/20A;->b:Z

    .line 353689
    iget-object v2, v0, LX/20B;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 353690
    move-object v0, v0

    .line 353691
    invoke-virtual {v1, v0}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 353692
    sget v1, LX/1zY;->a:I

    invoke-virtual {v0, p1, v1, p4, p5}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 353693
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353568
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353569
    iput v2, p4, LX/1no;->a:I

    .line 353570
    iput v2, p4, LX/1no;->b:I

    .line 353571
    const/4 v0, 0x0

    .line 353572
    :goto_0
    return-object v0

    .line 353573
    :cond_0
    iget v0, p0, LX/1zY;->g:I

    if-eqz v0, :cond_1

    iget v0, p0, LX/1zY;->g:I

    .line 353574
    :goto_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/1ne;->k(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p8}, LX/1ne;->d(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p9}, LX/1ne;->e(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p10}, LX/1ne;->c(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 353575
    sget v1, LX/1zY;->a:I

    invoke-virtual {v0, p1, v1, p3, p4}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 353576
    invoke-static {p1, v0}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v0

    goto :goto_0

    .line 353577
    :cond_1
    const v0, 0x7f0b004e

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/1zY;
    .locals 10

    .prologue
    .line 353660
    const-class v1, LX/1zY;

    monitor-enter v1

    .line 353661
    :try_start_0
    sget-object v0, LX/1zY;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 353662
    sput-object v2, LX/1zY;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 353663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 353665
    new-instance v3, LX/1zY;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/1zZ;->a(LX/0QB;)LX/1zZ;

    move-result-object v5

    check-cast v5, LX/1zZ;

    invoke-static {v0}, LX/1za;->a(LX/0QB;)LX/1za;

    move-result-object v6

    check-cast v6, LX/1za;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v7

    check-cast v7, LX/154;

    invoke-static {v0}, LX/153;->b(LX/0QB;)LX/153;

    move-result-object v8

    check-cast v8, LX/153;

    const/16 v9, 0x15d0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/1zY;-><init>(LX/03V;LX/1zZ;LX/1za;LX/154;LX/153;LX/0Or;)V

    .line 353666
    move-object v0, v3

    .line 353667
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 353668
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1zY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353669
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 353670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 353658
    iget-object v0, p0, LX/1zY;->e:LX/154;

    invoke-virtual {v0, p2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 353659
    if-lez p3, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-virtual {p1, p3, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;ILcom/facebook/graphql/model/GraphQLStory;ZZZLX/1dQ;IIIFFF)LX/1Dg;
    .locals 28
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p12    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param

    .prologue
    .line 353578
    invoke-static/range {p2 .. p2}, LX/1mh;->a(I)I

    move-result v3

    if-nez v3, :cond_0

    .line 353579
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "UFIFeedbackSummaryComponent can\'t be measured with unspecified width."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 353580
    :cond_0
    invoke-static/range {p2 .. p2}, LX/1mh;->b(I)I

    move-result v20

    .line 353581
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 353582
    invoke-virtual/range {p1 .. p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 353583
    new-instance v8, LX/1no;

    invoke-direct {v8}, LX/1no;-><init>()V

    .line 353584
    const/4 v3, 0x7

    new-array v0, v3, [LX/1Di;

    move-object/from16 v22, v0

    .line 353585
    const/4 v3, 0x7

    new-array v0, v3, [I

    move-object/from16 v23, v0

    .line 353586
    const/4 v3, 0x7

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v24, v0

    .line 353587
    const v3, 0x7f0b0fe1

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v25

    .line 353588
    const/high16 v3, -0x80000000

    move/from16 v0, v25

    invoke-static {v0, v3}, LX/1mh;->a(II)I

    move-result v7

    .line 353589
    move-object/from16 v0, p1

    move/from16 v1, p8

    invoke-static {v0, v1}, LX/1zY;->a(LX/1De;I)I

    move-result v14

    .line 353590
    invoke-static {v5}, LX/16z;->p(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v26

    .line 353591
    if-lez v26, :cond_1

    .line 353592
    const/4 v9, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v6, p6

    invoke-direct/range {v3 .. v8}, LX/1zY;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLFeedback;ZILX/1no;)LX/1Di;

    move-result-object v3

    aput-object v3, v22, v9

    .line 353593
    const/4 v3, 0x0

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353594
    const/4 v3, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v5

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-direct/range {v9 .. v19}, LX/1zY;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353595
    const/4 v3, 0x1

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353596
    invoke-static {v5}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_7

    const v3, 0x7f0f0060

    .line 353597
    :goto_0
    const/4 v4, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v24, v4

    .line 353598
    const/4 v3, 0x2

    const v4, 0x7f0f00c1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v26

    invoke-direct {v0, v1, v2, v4}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-static/range {v9 .. v19}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353599
    const/4 v3, 0x2

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353600
    :cond_1
    invoke-static {v5}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    .line 353601
    if-lez v3, :cond_2

    .line 353602
    const v4, 0x7f0f0064

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3, v4}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v11

    .line 353603
    const/4 v3, 0x3

    aput-object v11, v24, v3

    .line 353604
    const/4 v3, 0x3

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-static/range {v9 .. v19}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353605
    const/4 v3, 0x3

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353606
    :cond_2
    invoke-static {v5}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    .line 353607
    if-lez v3, :cond_3

    .line 353608
    const v4, 0x7f0f0063

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3, v4}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v11

    .line 353609
    const/4 v3, 0x4

    aput-object v11, v24, v3

    .line 353610
    const/4 v3, 0x4

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-static/range {v9 .. v19}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353611
    const/4 v3, 0x4

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353612
    :cond_3
    if-eqz p4, :cond_4

    invoke-static/range {p3 .. p3}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 353613
    invoke-static/range {p3 .. p3}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v3

    .line 353614
    if-lez v3, :cond_4

    .line 353615
    const v4, 0x7f0f00c2

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3, v4}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v11

    .line 353616
    const/4 v3, 0x5

    aput-object v11, v24, v3

    .line 353617
    const/4 v3, 0x5

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-static/range {v9 .. v19}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353618
    const/4 v3, 0x5

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353619
    :cond_4
    if-eqz p5, :cond_5

    .line 353620
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/graphql/model/GraphQLStory;->A()I

    move-result v3

    .line 353621
    if-lez v3, :cond_5

    .line 353622
    const v4, 0x7f0f00c3

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3, v4}, LX/1zY;->a(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v11

    .line 353623
    const/4 v3, 0x6

    aput-object v11, v24, v3

    .line 353624
    const/4 v3, 0x6

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v7

    move-object v13, v8

    move/from16 v15, p9

    move/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p12

    move/from16 v19, p13

    invoke-static/range {v9 .. v19}, LX/1zY;->a(LX/1zY;LX/1De;Ljava/lang/String;ILX/1no;IIIFFF)LX/1Di;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353625
    const/4 v3, 0x6

    iget v4, v8, LX/1no;->a:I

    aput v4, v23, v3

    .line 353626
    const/4 v3, 0x6

    aget-object v3, v22, v3

    if-eqz v3, :cond_5

    if-eqz p7, :cond_5

    .line 353627
    const/4 v3, 0x6

    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    aget-object v5, v22, v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-interface {v4, v0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    aput-object v4, v22, v3

    .line 353628
    :cond_5
    const v3, 0x7f0b0fe4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 353629
    const/4 v3, 0x1

    aget-object v3, v22, v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    aget v3, v23, v3

    invoke-static {v3, v9}, LX/1zY;->a(II)I

    move-result v3

    const/4 v4, 0x1

    aget v4, v23, v4

    invoke-static {v4, v9}, LX/1zY;->a(II)I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x3

    aget v4, v23, v4

    invoke-static {v4, v9}, LX/1zY;->a(II)I

    move-result v4

    add-int/2addr v3, v4

    const/4 v4, 0x4

    aget v4, v23, v4

    invoke-static {v4, v9}, LX/1zY;->a(II)I

    move-result v4

    add-int/2addr v3, v4

    move/from16 v0, v20

    if-lt v0, v3, :cond_8

    const/4 v3, 0x1

    .line 353630
    :goto_1
    if-eqz v3, :cond_9

    const/4 v3, 0x2

    move v8, v3

    .line 353631
    :goto_2
    if-lez v26, :cond_a

    const/4 v3, 0x2

    .line 353632
    :goto_3
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    move/from16 v0, v20

    invoke-interface {v4, v0}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    .line 353633
    const/4 v6, 0x1

    .line 353634
    const/4 v5, 0x0

    .line 353635
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 353636
    const/4 v4, 0x0

    move v7, v4

    move v4, v6

    move/from16 v6, v20

    :goto_4
    const/4 v12, 0x7

    if-ge v7, v12, :cond_e

    .line 353637
    if-eq v7, v8, :cond_d

    aget-object v12, v22, v7

    if-eqz v12, :cond_d

    aget v12, v23, v7

    add-int/2addr v12, v9

    if-gt v12, v6, :cond_d

    .line 353638
    if-eqz v4, :cond_b

    .line 353639
    aget-object v4, v22, v7

    invoke-interface {v10, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 353640
    const/4 v4, 0x0

    .line 353641
    :goto_5
    aget v12, v23, v7

    add-int/2addr v12, v9

    sub-int/2addr v6, v12

    .line 353642
    const/4 v12, 0x2

    if-gt v7, v12, :cond_c

    .line 353643
    if-nez v5, :cond_d

    .line 353644
    const/4 v5, 0x2

    aget-object v5, v24, v5

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353645
    const/4 v5, 0x1

    move/from16 v27, v5

    move v5, v4

    move/from16 v4, v27

    .line 353646
    :goto_6
    if-ne v7, v3, :cond_6

    .line 353647
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v12

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-interface {v12, v13}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v12

    invoke-interface {v10, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 353648
    :cond_6
    add-int/lit8 v7, v7, 0x1

    move/from16 v27, v4

    move v4, v5

    move/from16 v5, v27

    goto :goto_4

    .line 353649
    :cond_7
    const v3, 0x7f0f00c4

    goto/16 :goto_0

    .line 353650
    :cond_8
    const/4 v3, 0x0

    goto :goto_1

    .line 353651
    :cond_9
    const/4 v3, 0x1

    move v8, v3

    goto :goto_2

    .line 353652
    :cond_a
    const/4 v3, 0x3

    goto :goto_3

    .line 353653
    :cond_b
    aget-object v12, v22, v7

    const/4 v13, 0x4

    invoke-interface {v12, v13, v9}, LX/1Di;->a(II)LX/1Di;

    move-result-object v12

    invoke-interface {v10, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto :goto_5

    .line 353654
    :cond_c
    aget-object v12, v24, v7

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    move/from16 v27, v5

    move v5, v4

    move/from16 v4, v27

    goto :goto_6

    .line 353655
    :cond_e
    if-eqz v4, :cond_f

    .line 353656
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1zY;->b:LX/03V;

    const-string v4, "UFIFeedbackSummaryComponent"

    const-string v5, "UFIFeedbackSummaryComponent is created with no content. Please make sure the call site has checked UFIFeedbackSummaryHelper.hasFeedbackToShow() before creating this component. If the callsite is a ComponentPartDefinition, check in isNeeded()."

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 353657
    :cond_f
    move/from16 v0, v25

    invoke-interface {v10, v0}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->E(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    return-object v3
.end method
