.class public LX/1xv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Uf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0hL;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Cn;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/Boolean;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0wM;

.field private final o:LX/1xw;

.field public p:LX/1xx;

.field public final q:LX/0W3;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0hL;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;LX/0wM;LX/0W3;LX/1xw;)V
    .locals 2
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsWorkMultiCompanyGroupsEnabledGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1Uf;",
            ">;",
            "LX/0hL;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Cn;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0wM;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/1xw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349831
    iput-object p1, p0, LX/1xv;->a:Landroid/content/Context;

    .line 349832
    iput-object p2, p0, LX/1xv;->b:LX/0Ot;

    .line 349833
    iput-object p3, p0, LX/1xv;->c:LX/0hL;

    .line 349834
    iput-object p4, p0, LX/1xv;->d:LX/0Ot;

    .line 349835
    iput-object p5, p0, LX/1xv;->e:LX/0Ot;

    .line 349836
    iput-object p6, p0, LX/1xv;->f:Ljava/lang/Boolean;

    .line 349837
    iput-object p7, p0, LX/1xv;->n:LX/0wM;

    .line 349838
    iput-object p8, p0, LX/1xv;->q:LX/0W3;

    .line 349839
    iput-object p9, p0, LX/1xv;->o:LX/1xw;

    .line 349840
    iget-object v0, p0, LX/1xv;->o:LX/1xw;

    iget-object v1, p0, LX/1xv;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1xw;->a(Landroid/content/Context;)LX/1xx;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->p:LX/1xx;

    .line 349841
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f081024

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->g:LX/0Ot;

    .line 349842
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fdf

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->h:LX/0Ot;

    .line 349843
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fe0

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->i:LX/0Ot;

    .line 349844
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fe1

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->l:LX/0Ot;

    .line 349845
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fe2

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->m:LX/0Ot;

    .line 349846
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fdd

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->j:LX/0Ot;

    .line 349847
    iget-object v0, p0, LX/1xv;->a:Landroid/content/Context;

    const v1, 0x7f080fde

    invoke-static {v0, v1}, LX/1wD;->a(Landroid/content/Context;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, LX/1xv;->k:LX/0Ot;

    .line 349848
    return-void
.end method

.method public static a(LX/0QB;)LX/1xv;
    .locals 13

    .prologue
    .line 349849
    const-class v1, LX/1xv;

    monitor-enter v1

    .line 349850
    :try_start_0
    sget-object v0, LX/1xv;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349851
    sput-object v2, LX/1xv;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349852
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349853
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349854
    new-instance v3, LX/1xv;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x129c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v6

    check-cast v6, LX/0hL;

    const/16 v7, 0xb19

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x129f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1nN;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v11

    check-cast v11, LX/0W3;

    const-class v12, LX/1xw;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/1xw;

    invoke-direct/range {v3 .. v12}, LX/1xv;-><init>(Landroid/content/Context;LX/0Ot;LX/0hL;LX/0Ot;LX/0Ot;Ljava/lang/Boolean;LX/0wM;LX/0W3;LX/1xw;)V

    .line 349855
    move-object v0, v3

    .line 349856
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349857
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349858
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1y2;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 349860
    new-instance v0, LX/1y2;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    invoke-direct/range {v0 .. v4}, LX/1y2;-><init>(LX/1xv;Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)LX/1y2;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;ZZ)",
            "LX/1y2;"
        }
    .end annotation

    .prologue
    .line 349861
    new-instance v0, LX/1y2;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v4}, LX/1y2;-><init>(LX/1xv;Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    return-object v0
.end method
