.class public LX/1zL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1zL;


# instance fields
.field private final a:LX/1zM;

.field public final b:LX/1zN;

.field private final c:LX/1zO;


# direct methods
.method public constructor <init>(LX/1zM;LX/1zN;LX/1zO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353170
    iput-object p1, p0, LX/1zL;->a:LX/1zM;

    .line 353171
    iput-object p2, p0, LX/1zL;->b:LX/1zN;

    .line 353172
    iput-object p3, p0, LX/1zL;->c:LX/1zO;

    .line 353173
    return-void
.end method

.method public static a(LX/0QB;)LX/1zL;
    .locals 8

    .prologue
    .line 353149
    sget-object v0, LX/1zL;->d:LX/1zL;

    if-nez v0, :cond_1

    .line 353150
    const-class v1, LX/1zL;

    monitor-enter v1

    .line 353151
    :try_start_0
    sget-object v0, LX/1zL;->d:LX/1zL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353152
    if-eqz v2, :cond_0

    .line 353153
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353154
    new-instance v6, LX/1zL;

    const-class v3, LX/1zM;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1zM;

    const-class v4, LX/1zN;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1zN;

    .line 353155
    new-instance v7, LX/1zO;

    const-class v5, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v5, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v7, v5}, LX/1zO;-><init>(Landroid/content/Context;)V

    .line 353156
    move-object v5, v7

    .line 353157
    check-cast v5, LX/1zO;

    invoke-direct {v6, v3, v4, v5}, LX/1zL;-><init>(LX/1zM;LX/1zN;LX/1zO;)V

    .line 353158
    move-object v0, v6

    .line 353159
    sput-object v0, LX/1zL;->d:LX/1zL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353160
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353161
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353162
    :cond_1
    sget-object v0, LX/1zL;->d:LX/1zL;

    return-object v0

    .line 353163
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353164
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)Landroid/text/style/CharacterStyle;
    .locals 3

    .prologue
    .line 353165
    iget-object v0, p0, LX/1zL;->a:LX/1zM;

    .line 353166
    new-instance v2, LX/1zQ;

    const-class v1, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v1, p1}, LX/1zQ;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V

    .line 353167
    move-object v0, v2

    .line 353168
    return-object v0
.end method
