.class public LX/1wc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346519
    iput-object p1, p0, LX/1wc;->a:Landroid/content/Context;

    .line 346520
    return-void
.end method

.method public static b(LX/0QB;)LX/1wc;
    .locals 2

    .prologue
    .line 346521
    new-instance v1, LX/1wc;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/1wc;-><init>(Landroid/content/Context;)V

    .line 346522
    return-object v1
.end method


# virtual methods
.method public final a(LX/1qf;LX/1qg;LX/2vs;)LX/2wX;
    .locals 1

    .prologue
    .line 346523
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/1wc;->a(LX/1qf;LX/1qg;LX/2vs;Landroid/os/Handler;)LX/2wX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1qf;LX/1qg;LX/2vs;Landroid/os/Handler;)LX/2wX;
    .locals 2
    .param p4    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346524
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 346525
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 346526
    new-instance v0, LX/2vz;

    iget-object v1, p0, LX/1wc;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2vz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/2vz;->a(LX/1qf;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2vz;->a(LX/1qg;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2vz;->a(LX/2vs;)LX/2vz;

    move-result-object v0

    .line 346527
    if-eqz p4, :cond_0

    .line 346528
    invoke-virtual {v0, p4}, LX/2vz;->a(Landroid/os/Handler;)LX/2vz;

    .line 346529
    :cond_0
    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2vs;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2vs;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/2wX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346530
    new-instance v0, LX/3KC;

    invoke-direct {v0}, LX/3KC;-><init>()V

    .line 346531
    invoke-virtual {p0, v0, v0, p1}, LX/1wc;->a(LX/1qf;LX/1qg;LX/2vs;)LX/2wX;

    move-result-object v1

    .line 346532
    iput-object v1, v0, LX/3KC;->b:LX/2wX;

    .line 346533
    invoke-virtual {v1}, LX/2wX;->e()V

    .line 346534
    iget-object v0, v0, LX/3KC;->a:Lcom/google/common/util/concurrent/SettableFuture;

    return-object v0
.end method
