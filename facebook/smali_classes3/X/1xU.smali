.class public LX/1xU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1X3;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xU",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1X3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348561
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348562
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xU;->b:LX/0Zi;

    .line 348563
    iput-object p1, p0, LX/1xU;->a:LX/0Ot;

    .line 348564
    return-void
.end method

.method public static a(LX/0QB;)LX/1xU;
    .locals 4

    .prologue
    .line 348525
    const-class v1, LX/1xU;

    monitor-enter v1

    .line 348526
    :try_start_0
    sget-object v0, LX/1xU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348527
    sput-object v2, LX/1xU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348530
    new-instance v3, LX/1xU;

    const/16 p0, 0x736

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xU;-><init>(LX/0Ot;)V

    .line 348531
    move-object v0, v3

    .line 348532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 348546
    check-cast p2, LX/1xn;

    .line 348547
    iget-object v0, p0, LX/1xU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X3;

    iget-object v2, p2, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/1xn;->b:LX/1Pn;

    iget v4, p2, LX/1xn;->c:I

    iget v5, p2, LX/1xn;->d:I

    iget-boolean v6, p2, LX/1xn;->e:Z

    iget v7, p2, LX/1xn;->f:I

    move-object v1, p1

    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 348548
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v8, p0}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/1X3;->b:LX/1xp;

    invoke-virtual {p0, v1, p1, v4}, LX/1xp;->a(LX/1De;II)LX/1xt;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1xt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xt;

    move-result-object p0

    check-cast v3, LX/1Po;

    invoke-virtual {p0, v3}, LX/1xt;->a(LX/1Po;)LX/1xt;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1xt;->i(I)LX/1xt;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/1xt;->b(Z)LX/1xt;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/1X3;->c:LX/1xq;

    .line 348549
    new-instance v0, LX/1yV;

    invoke-direct {v0, p0}, LX/1yV;-><init>(LX/1xq;)V

    .line 348550
    sget-object v3, LX/1xq;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1yW;

    .line 348551
    if-nez v3, :cond_0

    .line 348552
    new-instance v3, LX/1yW;

    invoke-direct {v3}, LX/1yW;-><init>()V

    .line 348553
    :cond_0
    invoke-static {v3, v1, p1, v4, v0}, LX/1yW;->a$redex0(LX/1yW;LX/1De;IILX/1yV;)V

    .line 348554
    move-object v0, v3

    .line 348555
    move-object p0, v0

    .line 348556
    iget-object p1, p0, LX/1yW;->a:LX/1yV;

    iput-object v2, p1, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 348557
    iget-object p1, p0, LX/1yW;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->set(I)V

    .line 348558
    move-object p0, p0

    .line 348559
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 348560
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348544
    invoke-static {}, LX/1dS;->b()V

    .line 348545
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1xo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348536
    new-instance v1, LX/1xn;

    invoke-direct {v1, p0}, LX/1xn;-><init>(LX/1xU;)V

    .line 348537
    iget-object v2, p0, LX/1xU;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1xo;

    .line 348538
    if-nez v2, :cond_0

    .line 348539
    new-instance v2, LX/1xo;

    invoke-direct {v2, p0}, LX/1xo;-><init>(LX/1xU;)V

    .line 348540
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1xo;->a$redex0(LX/1xo;LX/1De;IILX/1xn;)V

    .line 348541
    move-object v1, v2

    .line 348542
    move-object v0, v1

    .line 348543
    return-object v0
.end method
