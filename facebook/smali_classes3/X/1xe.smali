.class public LX/1xe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/11S;


# direct methods
.method public constructor <init>(LX/11S;)V
    .locals 0
    .param p1    # LX/11S;
        .annotation runtime Lcom/facebook/feed/annotations/ForNewsfeed;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349082
    iput-object p1, p0, LX/1xe;->a:LX/11S;

    .line 349083
    return-void
.end method

.method public static a(LX/0QB;)LX/1xe;
    .locals 4

    .prologue
    .line 349084
    const-class v1, LX/1xe;

    monitor-enter v1

    .line 349085
    :try_start_0
    sget-object v0, LX/1xe;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349086
    sput-object v2, LX/1xe;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349089
    new-instance p0, LX/1xe;

    invoke-static {v0}, LX/1xf;->a(LX/0QB;)LX/1xf;

    move-result-object v3

    check-cast v3, LX/11S;

    invoke-direct {p0, v3}, LX/1xe;-><init>(LX/11S;)V

    .line 349090
    move-object v0, p0

    .line 349091
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349092
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349093
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349094
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 349095
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349096
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349097
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    move v0, v1

    .line 349098
    :goto_0
    return v0

    .line 349099
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aN()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aN()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aN()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;->NONE:Lcom/facebook/graphql/enums/GraphQLStoryTimestampStyle;

    if-ne v3, v4, :cond_1

    move v0, v1

    .line 349100
    goto :goto_0

    .line 349101
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v3

    if-nez v3, :cond_4

    .line 349102
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 349103
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 349104
    :cond_4
    invoke-static {p0}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v3

    .line 349105
    sget-object v4, LX/34O;->NCPP:LX/34O;

    if-eq v3, v4, :cond_5

    sget-object v4, LX/34O;->GROUPER:LX/34O;

    if-eq v3, v4, :cond_5

    invoke-static {v0}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x3e8

    .line 349106
    invoke-static {p1}, LX/1xe;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 349107
    const-string v0, ""

    .line 349108
    :goto_0
    return-object v0

    .line 349109
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349110
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349111
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->L()Lcom/facebook/graphql/model/GraphQLBackdatedTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLBackdatedTime;->a()J

    move-result-wide v0

    mul-long/2addr v0, v2

    .line 349112
    :goto_1
    iget-object v2, p0, LX/1xe;->a:LX/11S;

    sget-object v3, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {v2, v3, v0, v1}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 349113
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_1
.end method
