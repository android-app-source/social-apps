.class public LX/1yk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1yk",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351196
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 351197
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1yk;->b:LX/0Zi;

    .line 351198
    iput-object p1, p0, LX/1yk;->a:LX/0Ot;

    .line 351199
    return-void
.end method

.method public static a(LX/0QB;)LX/1yk;
    .locals 4

    .prologue
    .line 351223
    const-class v1, LX/1yk;

    monitor-enter v1

    .line 351224
    :try_start_0
    sget-object v0, LX/1yk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351225
    sput-object v2, LX/1yk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351228
    new-instance v3, LX/1yk;

    const/16 p0, 0x72e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1yk;-><init>(LX/0Ot;)V

    .line 351229
    move-object v0, v3

    .line 351230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 351234
    check-cast p2, LX/357;

    .line 351235
    iget-object v0, p0, LX/1yk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yr;

    iget-object v1, p2, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/357;->b:LX/1Pd;

    iget-object v3, p2, LX/357;->c:LX/1X1;

    const/4 v4, 0x0

    const/4 p2, 0x2

    .line 351236
    iget-object v5, v0, LX/1yr;->e:LX/1yo;

    iget-object v6, v0, LX/1yr;->h:LX/0Uh;

    invoke-static {v1, v5, v6}, LX/1yr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yo;LX/0Uh;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 351237
    if-nez v5, :cond_0

    .line 351238
    :goto_0
    move-object v0, v4

    .line 351239
    return-object v0

    .line 351240
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    .line 351241
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v6

    .line 351242
    iget-object v7, v0, LX/1yr;->d:LX/1vg;

    invoke-virtual {v7, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    const v8, 0x7f020abc

    invoke-virtual {v7, v8}, LX/2xv;->h(I)LX/2xv;

    move-result-object v7

    iget-object v8, v0, LX/1yr;->f:Landroid/content/res/ColorStateList;

    if-eqz v6, :cond_1

    sget-object v4, LX/1yr;->a:[I

    :cond_1
    iget-object p0, v0, LX/1yr;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result p0

    invoke-virtual {v8, v4, p0}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v4

    invoke-virtual {v7, v4}, LX/2xv;->i(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    .line 351243
    iget-object v7, v0, LX/1yr;->g:LX/1yf;

    invoke-virtual {v7}, LX/1yf;->a()Z

    move-result v7

    .line 351244
    iget-object v8, v0, LX/1yr;->c:LX/359;

    invoke-virtual {v8, p1}, LX/359;->c(LX/1De;)LX/35B;

    move-result-object v8

    invoke-virtual {v8, v4}, LX/35B;->a(LX/1dc;)LX/35B;

    move-result-object v4

    check-cast v2, LX/1Pr;

    invoke-virtual {v4, v2}, LX/35B;->a(LX/1Pr;)LX/35B;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/35B;->b(Ljava/lang/String;)LX/35B;

    move-result-object v5

    .line 351245
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 351246
    check-cast v4, LX/0jW;

    invoke-virtual {v5, v4}, LX/35B;->a(LX/0jW;)LX/35B;

    move-result-object v4

    const v5, 0x7f0b00e5

    invoke-virtual {v4, v5}, LX/35B;->i(I)LX/35B;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 351247
    const v5, 0x4e0ae689    # 5.8259104E8f

    const/4 v8, 0x0

    invoke-static {p1, v5, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 351248
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    sget-object v5, LX/1yr;->b:Landroid/util/SparseArray;

    invoke-interface {v4, v5}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v5

    if-eqz v6, :cond_2

    const v4, 0x7f081a61

    :goto_1
    invoke-interface {v5, v4}, LX/1Di;->A(I)LX/1Di;

    move-result-object v5

    .line 351249
    if-eqz v7, :cond_4

    .line 351250
    if-eqz v6, :cond_3

    const v4, 0x7f020cba

    :goto_2
    invoke-interface {v5, v4}, LX/1Di;->x(I)LX/1Di;

    .line 351251
    :goto_3
    if-eqz v3, :cond_5

    .line 351252
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto/16 :goto_0

    .line 351253
    :cond_2
    const v4, 0x7f081a60

    goto :goto_1

    .line 351254
    :cond_3
    const v4, 0x7f020cb9

    goto :goto_2

    .line 351255
    :cond_4
    const/4 v4, 0x1

    invoke-interface {v5, v4, p2}, LX/1Di;->d(II)LX/1Di;

    goto :goto_3

    .line 351256
    :cond_5
    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 351208
    invoke-static {}, LX/1dS;->b()V

    .line 351209
    iget v0, p1, LX/1dQ;->b:I

    .line 351210
    packed-switch v0, :pswitch_data_0

    .line 351211
    :goto_0
    return-object v2

    .line 351212
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 351213
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 351214
    check-cast v1, LX/357;

    .line 351215
    iget-object v3, p0, LX/1yk;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1yr;

    iget-object v4, v1, LX/357;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/357;->b:LX/1Pd;

    iget-object p2, v1, LX/357;->d:LX/DHB;

    .line 351216
    iget-object p0, v3, LX/1yr;->e:LX/1yo;

    iget-object v1, v3, LX/1yr;->h:LX/0Uh;

    invoke-static {v4, p0, v1}, LX/1yr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yo;LX/0Uh;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 351217
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    .line 351218
    new-instance v1, LX/BsA;

    invoke-direct {v1, v4, p0, p1}, LX/BsA;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;LX/1Pd;)V

    .line 351219
    invoke-virtual {v1, v0}, LX/BsA;->onClick(Landroid/view/View;)V

    .line 351220
    if-eqz p2, :cond_0

    .line 351221
    iget-object p0, p2, LX/DHB;->b:LX/DHC;

    iget-object v1, p2, LX/DHB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0, v1}, LX/DHC;->a$redex0(LX/DHC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 351222
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4e0ae689
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/358;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1yk",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 351200
    new-instance v1, LX/357;

    invoke-direct {v1, p0}, LX/357;-><init>(LX/1yk;)V

    .line 351201
    iget-object v2, p0, LX/1yk;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/358;

    .line 351202
    if-nez v2, :cond_0

    .line 351203
    new-instance v2, LX/358;

    invoke-direct {v2, p0}, LX/358;-><init>(LX/1yk;)V

    .line 351204
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/358;->a$redex0(LX/358;LX/1De;IILX/357;)V

    .line 351205
    move-object v1, v2

    .line 351206
    move-object v0, v1

    .line 351207
    return-object v0
.end method
