.class public final LX/20U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:I

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 354914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354915
    const/4 v0, 0x0

    iput v0, p0, LX/20U;->c:I

    .line 354916
    iput-object p1, p0, LX/20U;->a:Landroid/view/View;

    .line 354917
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 354918
    if-eqz p1, :cond_0

    .line 354919
    iput p1, p0, LX/20U;->c:I

    .line 354920
    iget-object v0, p0, LX/20U;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/20U;->d:Landroid/graphics/drawable/Drawable;

    .line 354921
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 354922
    if-eqz p1, :cond_0

    iget v0, p0, LX/20U;->b:I

    if-ne p1, v0, :cond_0

    .line 354923
    :goto_0
    return-void

    .line 354924
    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, LX/20U;->c:I

    if-ne p1, v0, :cond_1

    .line 354925
    iget-object v0, p0, LX/20U;->a:Landroid/view/View;

    iget-object v1, p0, LX/20U;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 354926
    :goto_1
    iput p1, p0, LX/20U;->b:I

    goto :goto_0

    .line 354927
    :cond_1
    iget-object v0, p0, LX/20U;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method
