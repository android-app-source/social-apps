.class public LX/1yo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1yp;


# direct methods
.method public constructor <init>(LX/1yp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351402
    iput-object p1, p0, LX/1yo;->a:LX/1yp;

    .line 351403
    return-void
.end method

.method public static final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .prologue
    .line 351404
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351405
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 351406
    invoke-static {p0}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v1

    sget-object v2, LX/34O;->NCPP:LX/34O;

    if-ne v1, v2, :cond_3

    .line 351407
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 351408
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 351409
    if-nez v1, :cond_0

    .line 351410
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aS()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 351411
    if-nez v1, :cond_0

    .line 351412
    sget-object v1, LX/34O;->SINGLE_CREATOR_COLLECTION_ITEM:LX/34O;

    invoke-static {p0}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/34O;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 351413
    if-eqz v1, :cond_1

    .line 351414
    :cond_0
    invoke-static {v0}, LX/1yp;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351415
    :goto_2
    return-object v0

    .line 351416
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v0, LX/34O;->GROUPER_ATTACHED_STORY:LX/34O;

    invoke-static {p0}, LX/34N;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/34O;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/34O;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 351417
    if-eqz v0, :cond_2

    .line 351418
    invoke-static {p0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/1yp;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_2

    .line 351419
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/1yo;
    .locals 2

    .prologue
    .line 351420
    new-instance v1, LX/1yo;

    invoke-static {p0}, LX/1yp;->a(LX/0QB;)LX/1yp;

    move-result-object v0

    check-cast v0, LX/1yp;

    invoke-direct {v1, v0}, LX/1yo;-><init>(LX/1yp;)V

    .line 351421
    return-object v1
.end method
