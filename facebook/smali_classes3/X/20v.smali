.class public LX/20v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/20v;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 356453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356454
    return-void
.end method

.method public static a(LX/0QB;)LX/20v;
    .locals 6

    .prologue
    .line 356438
    sget-object v0, LX/20v;->d:LX/20v;

    if-nez v0, :cond_1

    .line 356439
    const-class v1, LX/20v;

    monitor-enter v1

    .line 356440
    :try_start_0
    sget-object v0, LX/20v;->d:LX/20v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 356441
    if-eqz v2, :cond_0

    .line 356442
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 356443
    new-instance p0, LX/20v;

    invoke-direct {p0}, LX/20v;-><init>()V

    .line 356444
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 356445
    iput-object v3, p0, LX/20v;->a:Landroid/content/Context;

    iput-object v4, p0, LX/20v;->b:LX/0Uh;

    iput-object v5, p0, LX/20v;->c:LX/0ad;

    .line 356446
    move-object v0, p0

    .line 356447
    sput-object v0, LX/20v;->d:LX/20v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356448
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 356449
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 356450
    :cond_1
    sget-object v0, LX/20v;->d:LX/20v;

    return-object v0

    .line 356451
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 356452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 356434
    iget-object v1, p0, LX/20v;->b:LX/0Uh;

    const/16 v2, 0x2fe

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, LX/20v;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356435
    iget-object v1, p0, LX/20v;->c:LX/0ad;

    sget-short v2, LX/98c;->a:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 356436
    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 356437
    iget-object v0, p0, LX/20v;->c:LX/0ad;

    sget-short v1, LX/98c;->b:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
