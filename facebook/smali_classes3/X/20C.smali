.class public LX/20C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/ReferenceSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 354434
    new-instance v0, LX/0Zi;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/20C;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/20D;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354436
    iput-object p1, p0, LX/20C;->b:LX/0Or;

    .line 354437
    return-void
.end method

.method public static a(LX/0QB;)LX/20C;
    .locals 4

    .prologue
    .line 354438
    const-class v1, LX/20C;

    monitor-enter v1

    .line 354439
    :try_start_0
    sget-object v0, LX/20C;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 354440
    sput-object v2, LX/20C;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 354441
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354442
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 354443
    new-instance v3, LX/20C;

    const/16 p0, 0x7af

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/20C;-><init>(LX/0Or;)V

    .line 354444
    move-object v0, v3

    .line 354445
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 354446
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354447
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 354448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
