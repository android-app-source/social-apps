.class public LX/1zD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1zD;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 352129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1zD;
    .locals 3

    .prologue
    .line 352130
    sget-object v0, LX/1zD;->a:LX/1zD;

    if-nez v0, :cond_1

    .line 352131
    const-class v1, LX/1zD;

    monitor-enter v1

    .line 352132
    :try_start_0
    sget-object v0, LX/1zD;->a:LX/1zD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 352133
    if-eqz v2, :cond_0

    .line 352134
    :try_start_1
    new-instance v0, LX/1zD;

    invoke-direct {v0}, LX/1zD;-><init>()V

    .line 352135
    move-object v0, v0

    .line 352136
    sput-object v0, LX/1zD;->a:LX/1zD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352137
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 352138
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 352139
    :cond_1
    sget-object v0, LX/1zD;->a:LX/1zD;

    return-object v0

    .line 352140
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 352141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(II)Z
    .locals 6

    .prologue
    const/16 v5, 0x39

    const/16 v4, 0x30

    const/16 v3, 0x23

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 352142
    if-lt p0, v3, :cond_0

    const v2, 0xf0002

    if-le p0, v2, :cond_1

    .line 352143
    :cond_0
    :goto_0
    return v0

    .line 352144
    :cond_1
    if-eq p0, v3, :cond_2

    if-lt p0, v4, :cond_3

    if-gt p0, v5, :cond_3

    :cond_2
    const/16 v2, 0x20e3

    if-ne p1, v2, :cond_0

    .line 352145
    :cond_3
    const/16 v2, 0x24

    if-lt p0, v2, :cond_4

    const/16 v2, 0x2f

    if-le p0, v2, :cond_0

    .line 352146
    :cond_4
    const/16 v2, 0x3a

    if-lt p0, v2, :cond_5

    const/16 v2, 0xa8

    if-le p0, v2, :cond_0

    .line 352147
    :cond_5
    const/16 v2, 0xaf

    if-lt p0, v2, :cond_7

    const/16 v2, 0x2121

    if-gt p0, v2, :cond_7

    .line 352148
    const/16 v2, 0x203c

    if-eq p0, v2, :cond_6

    const/16 v2, 0x2049

    if-ne p0, v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    .line 352149
    :cond_7
    const v2, 0x1f6c6

    if-lt p0, v2, :cond_8

    const v2, 0xeffff

    if-le p0, v2, :cond_0

    .line 352150
    :cond_8
    if-lt p0, v3, :cond_0

    if-gt p0, v3, :cond_9

    move v0, v1

    goto :goto_0

    .line 352151
    :cond_9
    if-lt p0, v4, :cond_0

    if-gt p0, v5, :cond_a

    move v0, v1

    goto :goto_0

    .line 352152
    :cond_a
    const/16 v2, 0xa9

    if-lt p0, v2, :cond_0

    const/16 v2, 0xae

    if-gt p0, v2, :cond_b

    move v0, v1

    goto :goto_0

    .line 352153
    :cond_b
    const/16 v2, 0x2122

    if-lt p0, v2, :cond_0

    const/16 v2, 0x21aa

    if-gt p0, v2, :cond_c

    move v0, v1

    goto :goto_0

    .line 352154
    :cond_c
    const/16 v2, 0x231a

    if-lt p0, v2, :cond_0

    const/16 v2, 0x231b

    if-gt p0, v2, :cond_d

    move v0, v1

    goto :goto_0

    .line 352155
    :cond_d
    const/16 v2, 0x23e9

    if-lt p0, v2, :cond_0

    const/16 v2, 0x23ec

    if-gt p0, v2, :cond_e

    move v0, v1

    goto :goto_0

    .line 352156
    :cond_e
    const/16 v2, 0x23f0

    if-lt p0, v2, :cond_0

    const/16 v2, 0x23f3

    if-gt p0, v2, :cond_f

    move v0, v1

    goto :goto_0

    .line 352157
    :cond_f
    const/16 v2, 0x24c2

    if-lt p0, v2, :cond_0

    const/16 v2, 0x24c2

    if-gt p0, v2, :cond_10

    move v0, v1

    goto :goto_0

    .line 352158
    :cond_10
    const/16 v2, 0x25aa

    if-lt p0, v2, :cond_0

    const/16 v2, 0x25fe

    if-gt p0, v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    .line 352159
    :cond_11
    const/16 v2, 0x2600

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2764

    if-gt p0, v2, :cond_12

    move v0, v1

    goto/16 :goto_0

    .line 352160
    :cond_12
    const/16 v2, 0x2795

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2797

    if-gt p0, v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    .line 352161
    :cond_13
    const/16 v2, 0x27a1

    if-lt p0, v2, :cond_0

    const/16 v2, 0x27bf

    if-gt p0, v2, :cond_14

    move v0, v1

    goto/16 :goto_0

    .line 352162
    :cond_14
    const/16 v2, 0x2934

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2935

    if-gt p0, v2, :cond_15

    move v0, v1

    goto/16 :goto_0

    .line 352163
    :cond_15
    const/16 v2, 0x2b05

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2b07

    if-gt p0, v2, :cond_16

    move v0, v1

    goto/16 :goto_0

    .line 352164
    :cond_16
    const/16 v2, 0x2b1b

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2b1c

    if-gt p0, v2, :cond_17

    move v0, v1

    goto/16 :goto_0

    .line 352165
    :cond_17
    const/16 v2, 0x2b50

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2b50

    if-gt p0, v2, :cond_18

    move v0, v1

    goto/16 :goto_0

    .line 352166
    :cond_18
    const/16 v2, 0x2b55

    if-lt p0, v2, :cond_0

    const/16 v2, 0x2b55

    if-gt p0, v2, :cond_19

    move v0, v1

    goto/16 :goto_0

    .line 352167
    :cond_19
    const/16 v2, 0x3030

    if-lt p0, v2, :cond_0

    const/16 v2, 0x3030

    if-gt p0, v2, :cond_1a

    move v0, v1

    goto/16 :goto_0

    .line 352168
    :cond_1a
    const/16 v2, 0x303d

    if-lt p0, v2, :cond_0

    const/16 v2, 0x303d

    if-gt p0, v2, :cond_1b

    move v0, v1

    goto/16 :goto_0

    .line 352169
    :cond_1b
    const/16 v2, 0x3297

    if-lt p0, v2, :cond_0

    const/16 v2, 0x3299

    if-gt p0, v2, :cond_1c

    move v0, v1

    goto/16 :goto_0

    .line 352170
    :cond_1c
    const v2, 0xe001

    if-lt p0, v2, :cond_0

    const v2, 0xe536

    if-gt p0, v2, :cond_1d

    move v0, v1

    goto/16 :goto_0

    .line 352171
    :cond_1d
    const v2, 0x1f004

    if-lt p0, v2, :cond_0

    const v2, 0x1f004

    if-gt p0, v2, :cond_1e

    move v0, v1

    goto/16 :goto_0

    .line 352172
    :cond_1e
    const v2, 0x1f0cf

    if-lt p0, v2, :cond_0

    const v2, 0x1f0cf

    if-gt p0, v2, :cond_1f

    move v0, v1

    goto/16 :goto_0

    .line 352173
    :cond_1f
    const v2, 0x1f170

    if-lt p0, v2, :cond_0

    const v2, 0x1f19a

    if-gt p0, v2, :cond_20

    move v0, v1

    goto/16 :goto_0

    .line 352174
    :cond_20
    const v2, 0x1f1e6

    if-lt p0, v2, :cond_0

    const v2, 0x1f1ff

    if-gt p0, v2, :cond_21

    move v0, v1

    goto/16 :goto_0

    .line 352175
    :cond_21
    const v2, 0x1f201

    if-lt p0, v2, :cond_0

    const v2, 0x1f251

    if-gt p0, v2, :cond_22

    move v0, v1

    goto/16 :goto_0

    .line 352176
    :cond_22
    const v2, 0x1f300

    if-lt p0, v2, :cond_0

    const v2, 0x1f6c5

    if-gt p0, v2, :cond_23

    move v0, v1

    goto/16 :goto_0

    .line 352177
    :cond_23
    const/high16 v2, 0xf0000

    if-lt p0, v2, :cond_0

    const v2, 0xf0002

    if-gt p0, v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method
