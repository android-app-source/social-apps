.class public LX/1zf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1zf;


# instance fields
.field public final a:LX/1zi;

.field public final b:LX/1zn;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/1zg;LX/1qw;LX/0Xl;Landroid/os/Handler;)V
    .locals 3
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/base/broadcast/BackgroundBroadcastThread;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1zh;",
            ">;",
            "LX/1zg;",
            "LX/1qw;",
            "LX/0Xl;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353891
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zi;

    iput-object v0, p0, LX/1zf;->a:LX/1zi;

    .line 353892
    iget-object v0, p0, LX/1zf;->a:LX/1zi;

    invoke-virtual {p2, p0, v0}, LX/1zg;->a(LX/1zf;LX/1zi;)LX/1zn;

    move-result-object v0

    iput-object v0, p0, LX/1zf;->b:LX/1zn;

    .line 353893
    iget-object v0, p0, LX/1zf;->a:LX/1zi;

    invoke-static {p0, v0}, LX/1zf;->a(LX/1zf;LX/1zi;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1zf;->c:LX/0Px;

    .line 353894
    iget-object v0, p0, LX/1zf;->b:LX/1zn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1zn;->a(Z)V

    .line 353895
    new-instance v0, LX/203;

    invoke-direct {v0, p0}, LX/203;-><init>(LX/1zf;)V

    invoke-virtual {p3, v0}, LX/1qw;->a(LX/1rV;)V

    .line 353896
    invoke-interface {p4}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    new-instance v2, LX/204;

    invoke-direct {v2, p0}, LX/204;-><init>(LX/1zf;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0, p5}, LX/0YX;->a(Landroid/os/Handler;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 353897
    return-void
.end method

.method public static a(LX/1zf;LX/1zi;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zi;",
            ")",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353880
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 353881
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 353882
    invoke-interface {p1}, LX/1zi;->a()[I

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget v7, v5, v3

    .line 353883
    if-eqz v0, :cond_1

    invoke-virtual {p0, v7}, LX/1zf;->a(I)LX/1zt;

    move-result-object v2

    :goto_1
    invoke-interface {p1, v7, v2, v1}, LX/1zi;->a(ILX/1zt;Z)LX/1zt;

    move-result-object v2

    .line 353884
    if-eqz v2, :cond_0

    .line 353885
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 353886
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 353887
    :cond_1
    sget-object v2, LX/1zt;->c:LX/1zt;

    goto :goto_1

    .line 353888
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 353889
    return-object v0
.end method

.method public static a(LX/0QB;)LX/1zf;
    .locals 9

    .prologue
    .line 353867
    sget-object v0, LX/1zf;->d:LX/1zf;

    if-nez v0, :cond_1

    .line 353868
    const-class v1, LX/1zf;

    monitor-enter v1

    .line 353869
    :try_start_0
    sget-object v0, LX/1zf;->d:LX/1zf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353870
    if-eqz v2, :cond_0

    .line 353871
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353872
    new-instance v3, LX/1zf;

    const/16 v4, 0x7a5

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, LX/1zg;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1zg;

    invoke-static {v0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v6

    check-cast v6, LX/1qw;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/0Zw;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-direct/range {v3 .. v8}, LX/1zf;-><init>(LX/0Or;LX/1zg;LX/1qw;LX/0Xl;Landroid/os/Handler;)V

    .line 353873
    move-object v0, v3

    .line 353874
    sput-object v0, LX/1zf;->d:LX/1zf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353875
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353876
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353877
    :cond_1
    sget-object v0, LX/1zf;->d:LX/1zf;

    return-object v0

    .line 353878
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedbackReaction;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353858
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353859
    :cond_0
    const/4 v0, 0x0

    .line 353860
    :goto_0
    return-object v0

    .line 353861
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 353862
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 353863
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedbackReaction;->a()I

    move-result v0

    invoke-virtual {p0, v0}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    .line 353864
    sget-object v3, LX/1zt;->c:LX/1zt;

    if-eq v0, v3, :cond_2

    .line 353865
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 353866
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)LX/1zt;
    .locals 3

    .prologue
    .line 353849
    iget-object v0, p0, LX/1zf;->c:LX/0Px;

    move-object v0, v0

    .line 353850
    if-nez v0, :cond_0

    .line 353851
    sget-object v1, LX/1zt;->c:LX/1zt;

    .line 353852
    :goto_0
    move-object v0, v1

    .line 353853
    return-object v0

    .line 353854
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1zt;

    .line 353855
    iget p0, v1, LX/1zt;->e:I

    move p0, p0

    .line 353856
    if-ne p0, p1, :cond_1

    goto :goto_0

    .line 353857
    :cond_2
    sget-object v1, LX/1zt;->c:LX/1zt;

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353846
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    sget-object v1, LX/1zt;->c:LX/1zt;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 353847
    iget-object v1, p0, LX/1zf;->c:LX/0Px;

    move-object v1, v1

    .line 353848
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353836
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 353837
    iget-object v0, p0, LX/1zf;->c:LX/0Px;

    move-object v3, v0

    .line 353838
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 353839
    iget-boolean v5, v0, LX/1zt;->h:Z

    move v5, v5

    .line 353840
    if-nez v5, :cond_0

    .line 353841
    iget-object v5, v0, LX/1zt;->j:LX/1zx;

    invoke-interface {v5}, LX/1zx;->c()Z

    move-result v5

    move v5, v5

    .line 353842
    if-eqz v5, :cond_0

    .line 353843
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 353844
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 353845
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/1zt;
    .locals 1

    .prologue
    .line 353835
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    return-object v0
.end method
