.class public LX/1ww;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/1x3;

.field public c:Z

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Runnable;

.field public h:Z

.field public i:LX/1bH;

.field public j:LX/7FE;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 347178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1ww;->d:Ljava/util/Map;

    .line 347180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1ww;->e:Ljava/util/Map;

    .line 347181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1ww;->f:Ljava/util/Map;

    .line 347182
    return-void
.end method

.method public constructor <init>(LX/1x4;)V
    .locals 2

    .prologue
    .line 347153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347154
    iget v0, p1, LX/1x4;->a:I

    move v0, v0

    .line 347155
    iput v0, p0, LX/1ww;->a:I

    .line 347156
    iget-object v0, p1, LX/1x4;->b:LX/1x3;

    move-object v0, v0

    .line 347157
    iput-object v0, p0, LX/1ww;->b:LX/1x3;

    .line 347158
    iget-boolean v0, p1, LX/1x4;->c:Z

    move v0, v0

    .line 347159
    iput-boolean v0, p0, LX/1ww;->c:Z

    .line 347160
    new-instance v0, Ljava/util/HashMap;

    .line 347161
    iget-object v1, p1, LX/1x4;->d:LX/0P1;

    move-object v1, v1

    .line 347162
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/1ww;->d:Ljava/util/Map;

    .line 347163
    new-instance v0, Ljava/util/HashMap;

    .line 347164
    iget-object v1, p1, LX/1x4;->e:LX/0P1;

    move-object v1, v1

    .line 347165
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/1ww;->e:Ljava/util/Map;

    .line 347166
    new-instance v0, Ljava/util/HashMap;

    .line 347167
    iget-object v1, p1, LX/1x4;->f:LX/0P1;

    move-object v1, v1

    .line 347168
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, LX/1ww;->f:Ljava/util/Map;

    .line 347169
    iget-object v0, p1, LX/1x4;->g:Ljava/lang/Runnable;

    move-object v0, v0

    .line 347170
    iput-object v0, p0, LX/1ww;->g:Ljava/lang/Runnable;

    .line 347171
    iget-boolean v0, p1, LX/1x4;->h:Z

    move v0, v0

    .line 347172
    iput-boolean v0, p0, LX/1ww;->h:Z

    .line 347173
    iget-object v0, p1, LX/1x4;->i:LX/1bH;

    move-object v0, v0

    .line 347174
    iput-object v0, p0, LX/1ww;->i:LX/1bH;

    .line 347175
    iget-object v0, p1, LX/1x4;->j:LX/7FE;

    move-object v0, v0

    .line 347176
    iput-object v0, p0, LX/1ww;->j:LX/7FE;

    .line 347177
    return-void
.end method


# virtual methods
.method public final d()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347152
    iget-object v0, p0, LX/1ww;->d:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347145
    iget-object v0, p0, LX/1ww;->e:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347151
    iget-object v0, p0, LX/1ww;->f:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1x4;
    .locals 1

    .prologue
    .line 347146
    iget v0, p0, LX/1ww;->a:I

    if-nez v0, :cond_0

    .line 347147
    sget-object v0, LX/1x2;->TRANSPARENT:LX/1x2;

    invoke-virtual {v0}, LX/1x2;->getThemeId()I

    move-result v0

    iput v0, p0, LX/1ww;->a:I

    .line 347148
    :cond_0
    iget-object v0, p0, LX/1ww;->b:LX/1x3;

    if-nez v0, :cond_1

    .line 347149
    sget-object v0, LX/1x3;->DEFAULT:LX/1x3;

    iput-object v0, p0, LX/1ww;->b:LX/1x3;

    .line 347150
    :cond_1
    new-instance v0, LX/1x4;

    invoke-direct {v0, p0}, LX/1x4;-><init>(LX/1ww;)V

    return-object v0
.end method
