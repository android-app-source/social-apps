.class public LX/1v8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342026
    return-void
.end method


# virtual methods
.method public final a()LX/0yY;
    .locals 1

    .prologue
    .line 342027
    sget-object v0, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 342028
    invoke-static {p1}, LX/Ehu;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 342029
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 342030
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    const-string v2, "m.facebook.com"

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "v"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "map"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342031
    const/4 v0, 0x1

    .line 342032
    :goto_1
    return v0

    .line 342033
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 342034
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
