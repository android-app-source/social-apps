.class public LX/1wl;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/1wl;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "AppUpdateInjector.class"
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EeL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "AppUpdateInjector.class"
    .end annotation
.end field


# instance fields
.field private final c:LX/1wf;

.field private d:LX/1wh;

.field private e:LX/1wo;

.field private f:Landroid/content/Context;

.field private g:Landroid/app/DownloadManager;

.field private h:I

.field private i:Landroid/content/pm/PackageManager;

.field private j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:LX/1sY;

.field private m:LX/1sV;

.field private n:Landroid/os/Handler;

.field private o:Landroid/os/Handler;

.field private p:LX/1sW;

.field private q:LX/1wi;

.field private r:Landroid/content/SharedPreferences;

.field private s:LX/2zd;

.field private t:Lcom/facebook/appupdate/AppUpdatesCleaner;

.field private u:LX/1sZ;

.field private v:LX/GTF;

.field private w:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346886
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/1wl;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(LX/1wf;)V
    .locals 1

    .prologue
    .line 346887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346888
    const/4 v0, -0x1

    iput v0, p0, LX/1wl;->h:I

    .line 346889
    iput-object p1, p0, LX/1wl;->c:LX/1wf;

    .line 346890
    return-void
.end method

.method public static declared-synchronized a()LX/1wl;
    .locals 2

    .prologue
    .line 346891
    const-class v1, LX/1wl;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1wl;->a:LX/1wl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/1wm;->a(Z)V

    .line 346892
    sget-object v0, LX/1wl;->a:LX/1wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 346893
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(LX/1wf;)V
    .locals 6

    .prologue
    .line 346894
    const-class v1, LX/1wl;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1wl;->a:LX/1wl;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/1wm;->a(Z)V

    .line 346895
    new-instance v2, LX/1wl;

    invoke-direct {v2, p0}, LX/1wl;-><init>(LX/1wf;)V

    .line 346896
    sput-object v2, LX/1wl;->a:LX/1wl;

    .line 346897
    sget-object v0, LX/1wl;->a:LX/1wl;

    invoke-virtual {v0}, LX/1wl;->k()Landroid/os/Handler;

    move-result-object v3

    .line 346898
    sget-object v0, LX/1wl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeL;

    .line 346899
    new-instance v5, Lcom/facebook/appupdate/AppUpdateInjector$1;

    invoke-direct {v5, v0, v2}, Lcom/facebook/appupdate/AppUpdateInjector$1;-><init>(LX/EeL;LX/1wl;)V

    const v0, 0x3d7bc493

    invoke-static {v3, v5, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 346900
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 346901
    :cond_1
    :try_start_1
    sget-object v0, LX/1wl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346902
    monitor-exit v1

    return-void
.end method

.method public static declared-synchronized a(LX/EeL;)V
    .locals 4

    .prologue
    .line 346903
    const-class v1, LX/1wl;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/1wl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346904
    invoke-static {}, LX/1wl;->a()LX/1wl;

    move-result-object v0

    .line 346905
    invoke-virtual {v0}, LX/1wl;->k()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/facebook/appupdate/AppUpdateInjector$2;

    invoke-direct {v3, p0, v0}, Lcom/facebook/appupdate/AppUpdateInjector$2;-><init>(LX/EeL;LX/1wl;)V

    const v0, 0x725f4606

    invoke-static {v2, v3, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346906
    :goto_0
    monitor-exit v1

    return-void

    .line 346907
    :cond_0
    :try_start_1
    sget-object v0, LX/1wl;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 346908
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(LX/EeL;)V
    .locals 2

    .prologue
    .line 346909
    const-class v1, LX/1wl;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1wl;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346910
    monitor-exit v1

    return-void

    .line 346911
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b()Z
    .locals 2

    .prologue
    .line 346912
    const-class v1, LX/1wl;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1wl;->a:LX/1wl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized n(LX/1wl;)Lcom/facebook/appupdate/AppUpdatesCleaner;
    .locals 4

    .prologue
    .line 346941
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->t:Lcom/facebook/appupdate/AppUpdatesCleaner;

    if-nez v0, :cond_0

    .line 346942
    new-instance v0, Lcom/facebook/appupdate/AppUpdatesCleaner;

    invoke-static {p0}, LX/1wl;->o(LX/1wl;)LX/1sZ;

    move-result-object v1

    invoke-virtual {p0}, LX/1wl;->h()LX/1wh;

    move-result-object v2

    invoke-virtual {p0}, LX/1wl;->e()LX/1wo;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/appupdate/AppUpdatesCleaner;-><init>(LX/1sZ;LX/1wh;LX/1wo;)V

    iput-object v0, p0, LX/1wl;->t:Lcom/facebook/appupdate/AppUpdatesCleaner;

    .line 346943
    :cond_0
    iget-object v0, p0, LX/1wl;->t:Lcom/facebook/appupdate/AppUpdatesCleaner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346944
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized o(LX/1wl;)LX/1sZ;
    .locals 2

    .prologue
    .line 346913
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->u:LX/1sZ;

    if-nez v0, :cond_0

    .line 346914
    new-instance v0, LX/1sZ;

    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1sZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1wl;->u:LX/1sZ;

    .line 346915
    :cond_0
    iget-object v0, p0, LX/1wl;->u:LX/1sZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346916
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized p(LX/1wl;)LX/1wi;
    .locals 2

    .prologue
    .line 346917
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->q:LX/1wi;

    if-nez v0, :cond_0

    .line 346918
    iget-object v0, p0, LX/1wl;->c:LX/1wf;

    .line 346919
    iget-object v1, v0, LX/1wf;->e:LX/1wi;

    move-object v0, v1

    .line 346920
    iput-object v0, p0, LX/1wl;->q:LX/1wi;

    .line 346921
    :cond_0
    iget-object v0, p0, LX/1wl;->q:LX/1wi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346922
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized q()Landroid/app/DownloadManager;
    .locals 2

    .prologue
    .line 346923
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->g:Landroid/app/DownloadManager;

    if-nez v0, :cond_0

    .line 346924
    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v0

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, LX/1wl;->g:Landroid/app/DownloadManager;

    .line 346925
    :cond_0
    iget-object v0, p0, LX/1wl;->g:Landroid/app/DownloadManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized r()Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 346927
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->i:Landroid/content/pm/PackageManager;

    if-nez v0, :cond_0

    .line 346928
    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, LX/1wl;->i:Landroid/content/pm/PackageManager;

    .line 346929
    :cond_0
    iget-object v0, p0, LX/1wl;->i:Landroid/content/pm/PackageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346930
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized s()LX/0Or;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346931
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->j:LX/0Or;

    if-nez v0, :cond_0

    .line 346932
    iget-object v0, p0, LX/1wl;->c:LX/1wf;

    .line 346933
    new-instance v1, LX/1sU;

    invoke-direct {v1, v0}, LX/1sU;-><init>(LX/1wf;)V

    move-object v0, v1

    .line 346934
    iput-object v0, p0, LX/1wl;->j:LX/0Or;

    .line 346935
    :cond_0
    iget-object v0, p0, LX/1wl;->j:LX/0Or;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346936
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized t()LX/1sW;
    .locals 3

    .prologue
    .line 346882
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->p:LX/1sW;

    if-nez v0, :cond_0

    .line 346883
    new-instance v0, LX/1sW;

    invoke-static {p0}, LX/1wl;->u(LX/1wl;)LX/1sY;

    move-result-object v1

    invoke-static {p0}, LX/1wl;->p(LX/1wl;)LX/1wi;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/1sW;-><init>(LX/1sY;LX/1wi;)V

    iput-object v0, p0, LX/1wl;->p:LX/1sW;

    .line 346884
    :cond_0
    iget-object v0, p0, LX/1wl;->p:LX/1sW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346885
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized u(LX/1wl;)LX/1sY;
    .locals 2

    .prologue
    .line 346937
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->l:LX/1sY;

    if-nez v0, :cond_0

    .line 346938
    new-instance v0, LX/1sY;

    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1sY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1wl;->l:LX/1sY;

    .line 346939
    :cond_0
    iget-object v0, p0, LX/1wl;->l:LX/1sY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346940
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized v()LX/1sV;
    .locals 2

    .prologue
    .line 346813
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->m:LX/1sV;

    if-nez v0, :cond_0

    .line 346814
    new-instance v0, LX/1sV;

    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1sV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/1wl;->m:LX/1sV;

    .line 346815
    :cond_0
    iget-object v0, p0, LX/1wl;->m:LX/1sV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346816
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized w(LX/1wl;)LX/GTF;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346817
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->v:LX/GTF;

    if-nez v0, :cond_0

    .line 346818
    new-instance v1, LX/GTF;

    invoke-direct {v1}, LX/GTF;-><init>()V

    move-object v0, v1

    .line 346819
    iput-object v0, p0, LX/1wl;->v:LX/GTF;

    .line 346820
    :cond_0
    iget-object v0, p0, LX/1wl;->v:LX/GTF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346821
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized x()LX/0Or;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346822
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->w:LX/0Or;

    if-nez v0, :cond_0

    .line 346823
    new-instance v0, LX/1sa;

    invoke-direct {v0, p0}, LX/1sa;-><init>(LX/1wl;)V

    iput-object v0, p0, LX/1wl;->w:LX/0Or;

    .line 346824
    :cond_0
    iget-object v0, p0, LX/1wl;->w:LX/0Or;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346825
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized y()LX/1wq;
    .locals 2

    .prologue
    .line 346826
    monitor-enter p0

    .line 346827
    :try_start_0
    goto :goto_1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346828
    :goto_0
    monitor-exit p0

    return-object v0

    :goto_1
    :try_start_1
    new-instance v0, LX/1wp;

    invoke-direct {v0}, LX/1wp;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 346829
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized d()LX/2zd;
    .locals 4

    .prologue
    .line 346830
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->s:LX/2zd;

    if-nez v0, :cond_0

    .line 346831
    new-instance v0, LX/2zd;

    invoke-virtual {p0}, LX/1wl;->e()LX/1wo;

    move-result-object v1

    invoke-static {p0}, LX/1wl;->n(LX/1wl;)Lcom/facebook/appupdate/AppUpdatesCleaner;

    move-result-object v2

    invoke-virtual {p0}, LX/1wl;->k()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/2zd;-><init>(LX/1wo;Lcom/facebook/appupdate/AppUpdatesCleaner;Landroid/os/Handler;)V

    iput-object v0, p0, LX/1wl;->s:LX/2zd;

    .line 346832
    :cond_0
    iget-object v0, p0, LX/1wl;->s:LX/2zd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346833
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()LX/1wo;
    .locals 14

    .prologue
    .line 346834
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->e:LX/1wo;

    if-nez v0, :cond_0

    .line 346835
    new-instance v0, LX/1wo;

    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, LX/1wl;->q()Landroid/app/DownloadManager;

    move-result-object v2

    invoke-direct {p0}, LX/1wl;->y()LX/1wq;

    move-result-object v3

    invoke-direct {p0}, LX/1wl;->s()LX/0Or;

    move-result-object v4

    iget-object v5, p0, LX/1wl;->c:LX/1wf;

    .line 346836
    iget-object v6, v5, LX/1wf;->c:LX/0Or;

    move-object v5, v6

    .line 346837
    invoke-direct {p0}, LX/1wl;->v()LX/1sV;

    move-result-object v6

    invoke-virtual {p0}, LX/1wl;->k()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {p0}, LX/1wl;->h()LX/1wh;

    move-result-object v8

    invoke-direct {p0}, LX/1wl;->t()LX/1sW;

    move-result-object v9

    invoke-static {p0}, LX/1wl;->o(LX/1wl;)LX/1sZ;

    move-result-object v10

    invoke-virtual {p0}, LX/1wl;->l()Landroid/content/SharedPreferences;

    move-result-object v11

    invoke-direct {p0}, LX/1wl;->x()LX/0Or;

    move-result-object v12

    invoke-virtual {p0}, LX/1wl;->g()I

    move-result v13

    invoke-direct/range {v0 .. v13}, LX/1wo;-><init>(Landroid/content/Context;Landroid/app/DownloadManager;LX/1wq;LX/0Or;LX/0Or;LX/1sV;Landroid/os/Handler;LX/1wh;LX/1sW;LX/1sZ;Landroid/content/SharedPreferences;LX/0Or;I)V

    iput-object v0, p0, LX/1wl;->e:LX/1wo;

    .line 346838
    :cond_0
    iget-object v0, p0, LX/1wl;->e:LX/1wo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346839
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346840
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 346841
    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1wl;->k:Ljava/lang/String;

    .line 346842
    :cond_0
    iget-object v0, p0, LX/1wl;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346843
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()I
    .locals 3

    .prologue
    .line 346844
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/1wl;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 346845
    :try_start_1
    invoke-direct {p0}, LX/1wl;->r()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 346846
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, LX/1wl;->h:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346847
    :cond_0
    :try_start_2
    iget v0, p0, LX/1wl;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v0

    .line 346848
    :catch_0
    move-exception v0

    .line 346849
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Own PackageInfo not found!"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 346850
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()LX/1wh;
    .locals 2

    .prologue
    .line 346851
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->d:LX/1wh;

    if-nez v0, :cond_0

    .line 346852
    iget-object v0, p0, LX/1wl;->c:LX/1wf;

    .line 346853
    iget-object v1, v0, LX/1wf;->d:LX/1wg;

    move-object v0, v1

    .line 346854
    iput-object v0, p0, LX/1wl;->d:LX/1wh;

    .line 346855
    :cond_0
    iget-object v0, p0, LX/1wl;->d:LX/1wh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346856
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346857
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->c:LX/1wf;

    .line 346858
    iget-object v1, v0, LX/1wf;->f:Ljava/lang/Class;

    move-object v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346859
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized j()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 346860
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->n:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 346861
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/1wl;->n:Landroid/os/Handler;

    .line 346862
    :cond_0
    iget-object v0, p0, LX/1wl;->n:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346863
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()Landroid/os/Handler;
    .locals 3

    .prologue
    .line 346864
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->o:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 346865
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AppUpdate-background"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 346866
    invoke-virtual {p0}, LX/1wl;->h()LX/1wh;

    move-result-object v1

    .line 346867
    new-instance v2, LX/1wn;

    invoke-direct {v2, p0, v1}, LX/1wn;-><init>(LX/1wl;LX/1wh;)V

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 346868
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 346869
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/1wl;->o:Landroid/os/Handler;

    .line 346870
    :cond_0
    iget-object v0, p0, LX/1wl;->o:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346871
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 346872
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->r:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 346873
    invoke-virtual {p0}, LX/1wl;->m()Landroid/content/Context;

    move-result-object v0

    const-string v1, "appupdate_preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/1wl;->r:Landroid/content/SharedPreferences;

    .line 346874
    :cond_0
    iget-object v0, p0, LX/1wl;->r:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346875
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Landroid/content/Context;
    .locals 2

    .prologue
    .line 346876
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wl;->f:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 346877
    iget-object v0, p0, LX/1wl;->c:LX/1wf;

    .line 346878
    iget-object v1, v0, LX/1wf;->a:Landroid/content/Context;

    move-object v0, v1

    .line 346879
    iput-object v0, p0, LX/1wl;->f:Landroid/content/Context;

    .line 346880
    :cond_0
    iget-object v0, p0, LX/1wl;->f:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 346881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
