.class public final LX/1w3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 344986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)LX/1w6;
    .locals 6

    .prologue
    .line 344987
    new-instance v0, LX/1w6;

    invoke-direct {v0}, LX/1w6;-><init>()V

    .line 344988
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 344989
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 344990
    const/4 v0, 0x0

    .line 344991
    :cond_0
    return-object v0

    .line 344992
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 344993
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 344994
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 344995
    const/4 v2, 0x0

    .line 344996
    const-string v4, "name"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 344997
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_2

    :goto_1
    iput-object v2, v0, LX/1w6;->a:Ljava/lang/String;

    .line 344998
    :goto_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 344999
    :cond_2
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 345000
    :cond_3
    const-string v4, "type"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 345001
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_4

    :goto_3
    iput-object v2, v0, LX/1w6;->b:Ljava/lang/String;

    .line 345002
    goto :goto_2

    .line 345003
    :cond_4
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 345004
    :cond_5
    goto :goto_2
.end method
