.class public LX/20J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field public d:LX/1Wl;

.field public e:LX/1Wl;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 354540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354541
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/20J;->f:Z

    .line 354542
    const v0, 0x7f0b010f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/20J;->a:I

    .line 354543
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/20J;->b:Landroid/graphics/Paint;

    .line 354544
    iget-object v0, p0, LX/20J;->b:Landroid/graphics/Paint;

    const v1, 0x7f0a0442

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354545
    iget-object v0, p0, LX/20J;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 354546
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/20J;->c:Landroid/graphics/Paint;

    .line 354547
    iget-object v0, p0, LX/20J;->c:Landroid/graphics/Paint;

    const v1, 0x7f0a0462

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354548
    iget-object v0, p0, LX/20J;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 354549
    return-void
.end method

.method public static b(LX/0QB;)LX/20J;
    .locals 2

    .prologue
    .line 354550
    new-instance v1, LX/20J;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/20J;-><init>(Landroid/content/res/Resources;)V

    .line 354551
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 354552
    iget-object v0, p0, LX/20J;->d:LX/1Wl;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    if-ne v0, v1, :cond_0

    .line 354553
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 354554
    iget-boolean v0, p0, LX/20J;->f:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/20J;->a:I

    .line 354555
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    add-int/2addr v1, v0

    int-to-float v1, v1

    int-to-float v2, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    sub-int v0, v3, v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v4

    iget-object v5, p0, LX/20J;->b:Landroid/graphics/Paint;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 354556
    :cond_0
    iget-object v0, p0, LX/20J;->e:LX/1Wl;

    sget-object v1, LX/1Wl;->VISIBLE:LX/1Wl;

    if-ne v0, v1, :cond_1

    .line 354557
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 354558
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/20J;->b:Landroid/graphics/Paint;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 354559
    :cond_1
    return-void

    .line 354560
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
