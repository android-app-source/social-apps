.class public LX/1yu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:LX/2fk;

.field private final b:LX/2fm;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0bH;

.field private final e:LX/14w;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1Sl;

.field private final h:LX/0tX;

.field private final i:LX/1Sa;

.field private final j:LX/1Sj;

.field private final k:LX/03V;

.field private final l:LX/2fh;

.field public final m:LX/1yf;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/14w;LX/0Ot;LX/1Sl;LX/0tX;LX/2fk;LX/2fm;LX/1yf;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V
    .locals 0
    .param p13    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Sa;",
            "LX/1Sj;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2fh;",
            "LX/0bH;",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "LX/BNw;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/2fk;",
            "LX/2fm;",
            "LX/1yf;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351558
    iput-object p10, p0, LX/1yu;->a:LX/2fk;

    .line 351559
    iput-object p11, p0, LX/1yu;->b:LX/2fm;

    .line 351560
    iput-object p13, p0, LX/1yu;->c:Ljava/util/concurrent/ExecutorService;

    .line 351561
    iput-object p5, p0, LX/1yu;->d:LX/0bH;

    .line 351562
    iput-object p6, p0, LX/1yu;->e:LX/14w;

    .line 351563
    iput-object p7, p0, LX/1yu;->f:LX/0Ot;

    .line 351564
    iput-object p8, p0, LX/1yu;->g:LX/1Sl;

    .line 351565
    iput-object p9, p0, LX/1yu;->h:LX/0tX;

    .line 351566
    iput-object p1, p0, LX/1yu;->i:LX/1Sa;

    .line 351567
    iput-object p2, p0, LX/1yu;->j:LX/1Sj;

    .line 351568
    iput-object p3, p0, LX/1yu;->k:LX/03V;

    .line 351569
    iput-object p4, p0, LX/1yu;->l:LX/2fh;

    .line 351570
    iput-object p12, p0, LX/1yu;->m:LX/1yf;

    .line 351571
    iput-object p14, p0, LX/1yu;->n:LX/0Ot;

    .line 351572
    return-void
.end method

.method public static a(LX/0QB;)LX/1yu;
    .locals 3

    .prologue
    .line 351573
    const-class v1, LX/1yu;

    monitor-enter v1

    .line 351574
    :try_start_0
    sget-object v0, LX/1yu;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351575
    sput-object v2, LX/1yu;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351576
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351577
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1yu;->b(LX/0QB;)LX/1yu;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351578
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351579
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 351581
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v1

    if-nez v1, :cond_1

    .line 351582
    :cond_0
    :goto_0
    return v0

    .line 351583
    :cond_1
    const v1, -0x3625f733

    invoke-static {p0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 351584
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/1yu;
    .locals 15

    .prologue
    .line 351585
    new-instance v0, LX/1yu;

    invoke-static {p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v1

    check-cast v1, LX/1Sa;

    invoke-static {p0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v2

    check-cast v2, LX/1Sj;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/2fh;->a(LX/0QB;)LX/2fh;

    move-result-object v4

    check-cast v4, LX/2fh;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v6

    check-cast v6, LX/14w;

    const/16 v7, 0x3279

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/1Sl;->b(LX/0QB;)LX/1Sl;

    move-result-object v8

    check-cast v8, LX/1Sl;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {p0}, LX/2fk;->b(LX/0QB;)LX/2fk;

    move-result-object v10

    check-cast v10, LX/2fk;

    const-class v11, LX/2fm;

    invoke-interface {p0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/2fm;

    invoke-static {p0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v12

    check-cast v12, LX/1yf;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    const/16 v14, 0x327a

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, LX/1yu;-><init>(LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/14w;LX/0Ot;LX/1Sl;LX/0tX;LX/2fk;LX/2fm;LX/1yf;Ljava/util/concurrent/ExecutorService;LX/0Ot;)V

    .line 351586
    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 1

    .prologue
    .line 351587
    const v0, -0x3625f733

    invoke-static {p0, v0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351588
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onSaveClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V
    .locals 23
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 351589
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1yu;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v16

    .line 351590
    new-instance v1, LX/Bsh;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/1yu;->i:LX/1Sa;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1yu;->j:LX/1Sj;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1yu;->k:LX/03V;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1yu;->l:LX/2fh;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1yu;->d:LX/0bH;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1yu;->a:LX/2fk;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1yu;->e:LX/14w;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, LX/1yu;->g:LX/1Sl;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/1yu;->h:LX/0tX;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/1yu;->c:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/1yu;->f:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/1yu;->b:LX/2fm;

    const-string v17, "native_story"

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/1yu;->n:LX/0Ot;

    move-object/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v18, p2

    move-object/from16 v21, p3

    move-object/from16 v22, p2

    invoke-direct/range {v1 .. v22}, LX/Bsh;-><init>(LX/1yu;LX/1Sa;LX/1Sj;LX/03V;LX/2fh;LX/0bH;LX/2fk;LX/14w;Landroid/content/Context;LX/1Sl;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/2fm;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;LX/0Ot;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 351591
    iget-object v1, v1, LX/2fo;->d:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 351592
    return-void
.end method
