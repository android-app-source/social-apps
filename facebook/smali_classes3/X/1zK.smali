.class public LX/1zK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private a:Landroid/content/Context;

.field public final b:LX/1WB;

.field public final c:LX/1WC;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1WB;LX/1WC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353115
    iput-object p1, p0, LX/1zK;->a:Landroid/content/Context;

    .line 353116
    iput-object p2, p0, LX/1zK;->b:LX/1WB;

    .line 353117
    iput-object p3, p0, LX/1zK;->c:LX/1WC;

    .line 353118
    return-void
.end method

.method public static a(LX/0QB;)LX/1zK;
    .locals 6

    .prologue
    .line 353119
    const-class v1, LX/1zK;

    monitor-enter v1

    .line 353120
    :try_start_0
    sget-object v0, LX/1zK;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 353121
    sput-object v2, LX/1zK;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 353122
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353123
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 353124
    new-instance p0, LX/1zK;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v4

    check-cast v4, LX/1WB;

    invoke-static {v0}, LX/1WC;->a(LX/0QB;)LX/1WC;

    move-result-object v5

    check-cast v5, LX/1WC;

    invoke-direct {p0, v3, v4, v5}, LX/1zK;-><init>(Landroid/content/Context;LX/1WB;LX/1WC;)V

    .line 353125
    move-object v0, p0

    .line 353126
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 353127
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1zK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353128
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 353129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Z)I
    .locals 9

    .prologue
    const/16 v0, 0x15e

    .line 353130
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    .line 353131
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    int-to-double v4, v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    double-to-int v5, v4

    move v2, v3

    move v4, v3

    .line 353132
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 353133
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 353134
    invoke-static {v6}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v7

    .line 353135
    sget-object v8, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    if-eq v7, v8, :cond_0

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->LATIN_1_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq v7, v8, :cond_0

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    if-eq v7, v8, :cond_0

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    if-ne v7, v8, :cond_5

    :cond_0
    const/4 v7, 0x1

    :goto_1
    move v6, v7

    .line 353136
    if-eqz v6, :cond_4

    .line 353137
    add-int/lit8 v4, v4, 0x1

    .line 353138
    if-le v4, v5, :cond_4

    .line 353139
    const/4 v3, 0x1

    .line 353140
    :cond_1
    move v1, v3

    .line 353141
    if-nez v1, :cond_2

    .line 353142
    const/16 v0, 0x96

    .line 353143
    :goto_2
    :pswitch_0
    return v0

    .line 353144
    :cond_2
    if-nez p2, :cond_3

    .line 353145
    const/16 v0, 0x28a

    goto :goto_2

    .line 353146
    :cond_3
    iget-object v1, p0, LX/1zK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    .line 353147
    packed-switch v1, :pswitch_data_0

    :pswitch_1
    goto :goto_2

    .line 353148
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
