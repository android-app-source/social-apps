.class public final enum LX/1vV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1vV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1vV;

.field public static final enum DISMISSED_RECENTLY:LX/1vV;

.field public static final enum NOT_ELIGIBLE_FOR_PROMPT:LX/1vV;

.field public static final enum NOT_ENOUGH_NEW_PHOTOS:LX/1vV;

.field public static final enum NOT_ENOUGH_PHOTOS:LX/1vV;


# instance fields
.field private final mReason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 342970
    new-instance v0, LX/1vV;

    const-string v1, "DISMISSED_RECENTLY"

    const-string v2, "Prompt dismissed recently"

    invoke-direct {v0, v1, v3, v2}, LX/1vV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1vV;->DISMISSED_RECENTLY:LX/1vV;

    .line 342971
    new-instance v0, LX/1vV;

    const-string v1, "NOT_ENOUGH_NEW_PHOTOS"

    const-string v2, "Not enough new photos taken recently"

    invoke-direct {v0, v1, v4, v2}, LX/1vV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1vV;->NOT_ENOUGH_NEW_PHOTOS:LX/1vV;

    .line 342972
    new-instance v0, LX/1vV;

    const-string v1, "NOT_ENOUGH_PHOTOS"

    const-string v2, "Not enough photos fetched"

    invoke-direct {v0, v1, v5, v2}, LX/1vV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1vV;->NOT_ENOUGH_PHOTOS:LX/1vV;

    .line 342973
    new-instance v0, LX/1vV;

    const-string v1, "NOT_ELIGIBLE_FOR_PROMPT"

    const-string v2, "Not eligible for prompt by QE param"

    invoke-direct {v0, v1, v6, v2}, LX/1vV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1vV;->NOT_ELIGIBLE_FOR_PROMPT:LX/1vV;

    .line 342974
    const/4 v0, 0x4

    new-array v0, v0, [LX/1vV;

    sget-object v1, LX/1vV;->DISMISSED_RECENTLY:LX/1vV;

    aput-object v1, v0, v3

    sget-object v1, LX/1vV;->NOT_ENOUGH_NEW_PHOTOS:LX/1vV;

    aput-object v1, v0, v4

    sget-object v1, LX/1vV;->NOT_ENOUGH_PHOTOS:LX/1vV;

    aput-object v1, v0, v5

    sget-object v1, LX/1vV;->NOT_ELIGIBLE_FOR_PROMPT:LX/1vV;

    aput-object v1, v0, v6

    sput-object v0, LX/1vV;->$VALUES:[LX/1vV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 342967
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 342968
    iput-object p3, p0, LX/1vV;->mReason:Ljava/lang/String;

    .line 342969
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1vV;
    .locals 1

    .prologue
    .line 342966
    const-class v0, LX/1vV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1vV;

    return-object v0
.end method

.method public static values()[LX/1vV;
    .locals 1

    .prologue
    .line 342964
    sget-object v0, LX/1vV;->$VALUES:[LX/1vV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1vV;

    return-object v0
.end method


# virtual methods
.method public final getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 342965
    iget-object v0, p0, LX/1vV;->mReason:Ljava/lang/String;

    return-object v0
.end method
