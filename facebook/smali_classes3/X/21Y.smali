.class public LX/21Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/21Z;


# direct methods
.method public varargs constructor <init>(LX/21X;[I)V
    .locals 13

    .prologue
    .line 357293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357294
    iget-object v0, p1, LX/21X;->d:LX/21V;

    .line 357295
    iget v1, v0, LX/21V;->d:I

    move v0, v1

    .line 357296
    array-length v1, p2

    if-gt v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 357297
    iget-object v0, p1, LX/21X;->d:LX/21V;

    .line 357298
    iget v1, v0, LX/21V;->d:I

    move v0, v1

    .line 357299
    new-instance v1, LX/21Z;

    invoke-direct {v1, p1, v0}, LX/21Z;-><init>(LX/21X;I)V

    .line 357300
    const/4 v2, 0x0

    aget v3, p2, v2

    .line 357301
    const/4 v2, 0x1

    move v5, v2

    move v2, v3

    move v3, v5

    :goto_1
    if-ge v3, v0, :cond_1

    .line 357302
    aget v4, p2, v3

    if-le v4, v2, :cond_0

    .line 357303
    aget v2, p2, v3

    .line 357304
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 357305
    :cond_1
    move v2, v2

    .line 357306
    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 357307
    move v8, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    .line 357308
    :goto_2
    if-ge v8, v0, :cond_3

    .line 357309
    aget v9, p2, v8

    .line 357310
    if-lez v9, :cond_2

    .line 357311
    sget-object v10, LX/21a;->TEXT_AND_ICON_EQUAL_WIDTH:LX/21a;

    iget-object v11, p1, LX/21X;->d:LX/21V;

    invoke-static {v10, v11, v2, v9}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v10

    add-int/2addr v7, v10

    .line 357312
    sget-object v10, LX/21a;->TEXT_AND_ICON_VARIABLE_WIDTH:LX/21a;

    iget-object v11, p1, LX/21X;->d:LX/21V;

    invoke-static {v10, v11, v2, v9}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v10

    add-int/2addr v6, v10

    .line 357313
    sget-object v10, LX/21a;->ICON_ONLY_EQUAL_WIDTH:LX/21a;

    iget-object v11, p1, LX/21X;->d:LX/21V;

    invoke-static {v10, v11, v2, v9}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v10

    add-int/2addr v5, v10

    .line 357314
    sget-object v10, LX/21a;->TEXT_ONLY_EQUAL_WIDTH:LX/21a;

    iget-object v11, p1, LX/21X;->d:LX/21V;

    invoke-static {v10, v11, v2, v9}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v10

    add-int/2addr v4, v10

    .line 357315
    sget-object v10, LX/21a;->TEXT_ONLY_VARIABLE_WIDTH:LX/21a;

    iget-object v11, p1, LX/21X;->d:LX/21V;

    invoke-static {v10, v11, v2, v9}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v9

    add-int/2addr v3, v9

    .line 357316
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 357317
    :cond_3
    invoke-static {v12, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 357318
    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 357319
    iput v7, v1, LX/21Z;->b:I

    .line 357320
    iput v6, v1, LX/21Z;->c:I

    .line 357321
    iput v5, v1, LX/21Z;->f:I

    .line 357322
    iput v4, v1, LX/21Z;->d:I

    .line 357323
    iput v3, v1, LX/21Z;->e:I

    .line 357324
    const/high16 v8, 0x3f800000    # 1.0f

    .line 357325
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v0, :cond_5

    .line 357326
    aget v4, p2, v3

    .line 357327
    iget-object v5, v1, LX/21Z;->h:[F

    aput v8, v5, v3

    .line 357328
    if-lez v4, :cond_4

    .line 357329
    iget-object v5, v1, LX/21Z;->g:[F

    move-object v5, v5

    .line 357330
    sget-object v6, LX/21a;->TEXT_AND_ICON_VARIABLE_WIDTH:LX/21a;

    iget-object v7, p1, LX/21X;->d:LX/21V;

    invoke-static {v6, v7, v2, v4}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v6

    int-to-float v6, v6

    .line 357331
    iget v7, v1, LX/21Z;->c:I

    move v7, v7

    .line 357332
    int-to-float v7, v7

    div-float/2addr v6, v7

    aput v6, v5, v3

    .line 357333
    iget-object v5, v1, LX/21Z;->j:[F

    move-object v5, v5

    .line 357334
    sget-object v6, LX/21a;->ICON_ONLY_EQUAL_WIDTH:LX/21a;

    iget-object v7, p1, LX/21X;->d:LX/21V;

    invoke-static {v6, v7, v2, v4}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v6

    int-to-float v6, v6

    .line 357335
    iget v7, v1, LX/21Z;->f:I

    move v7, v7

    .line 357336
    int-to-float v7, v7

    div-float/2addr v6, v7

    aput v6, v5, v3

    .line 357337
    iget-object v5, v1, LX/21Z;->i:[F

    move-object v5, v5

    .line 357338
    sget-object v6, LX/21a;->TEXT_ONLY_VARIABLE_WIDTH:LX/21a;

    iget-object v7, p1, LX/21X;->d:LX/21V;

    invoke-static {v6, v7, v2, v4}, LX/21U;->a(LX/21a;LX/21V;II)I

    move-result v4

    int-to-float v4, v4

    .line 357339
    iget v6, v1, LX/21Z;->e:I

    move v6, v6

    .line 357340
    int-to-float v6, v6

    div-float/2addr v4, v6

    aput v4, v5, v3

    .line 357341
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 357342
    :cond_4
    iget-object v4, v1, LX/21Z;->g:[F

    move-object v4, v4

    .line 357343
    aput v8, v4, v3

    .line 357344
    iget-object v4, v1, LX/21Z;->i:[F

    move-object v4, v4

    .line 357345
    aput v8, v4, v3

    .line 357346
    iget-object v4, v1, LX/21Z;->j:[F

    move-object v4, v4

    .line 357347
    aput v8, v4, v3

    goto :goto_4

    .line 357348
    :cond_5
    move-object v0, v1

    .line 357349
    iput-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357350
    return-void

    .line 357351
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(I)LX/21c;
    .locals 2

    .prologue
    .line 357263
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357264
    iget v1, v0, LX/21Z;->b:I

    move v0, v1

    .line 357265
    if-ge v0, p1, :cond_0

    .line 357266
    sget-object v0, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    .line 357267
    :goto_0
    return-object v0

    .line 357268
    :cond_0
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357269
    iget v1, v0, LX/21Z;->c:I

    move v0, v1

    .line 357270
    if-ge v0, p1, :cond_1

    .line 357271
    sget-object v0, LX/21c;->ICONS_VARIABLE_WIDTH:LX/21c;

    goto :goto_0

    .line 357272
    :cond_1
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357273
    iget v1, v0, LX/21Z;->d:I

    move v0, v1

    .line 357274
    if-ge v0, p1, :cond_2

    .line 357275
    sget-object v0, LX/21c;->NO_ICONS_EQUAL_WIDTH:LX/21c;

    goto :goto_0

    .line 357276
    :cond_2
    sget-object v0, LX/21c;->NO_ICONS_VARIABLE_WIDTH:LX/21c;

    goto :goto_0
.end method

.method public final a(LX/21c;)[F
    .locals 2

    .prologue
    .line 357277
    sget-object v0, LX/21e;->a:[I

    invoke-virtual {p1}, LX/21c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 357278
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357279
    iget-object v1, v0, LX/21Z;->i:[F

    move-object v0, v1

    .line 357280
    :goto_0
    return-object v0

    .line 357281
    :pswitch_0
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357282
    iget-object v1, v0, LX/21Z;->h:[F

    move-object v0, v1

    .line 357283
    goto :goto_0

    .line 357284
    :pswitch_1
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357285
    iget-object v1, v0, LX/21Z;->g:[F

    move-object v0, v1

    .line 357286
    goto :goto_0

    .line 357287
    :pswitch_2
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357288
    iget-object v1, v0, LX/21Z;->h:[F

    move-object v0, v1

    .line 357289
    goto :goto_0

    .line 357290
    :pswitch_3
    iget-object v0, p0, LX/21Y;->a:LX/21Z;

    .line 357291
    iget-object v1, v0, LX/21Z;->j:[F

    move-object v0, v1

    .line 357292
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
