.class public final enum LX/20X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/20X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/20X;

.field public static final enum COMMENT:LX/20X;

.field public static final enum LIKE:LX/20X;

.field public static final enum SAVE:LX/20X;

.field public static final enum SEND:LX/20X;

.field public static final enum SHARE:LX/20X;

.field public static final enum VISIT_LINK:LX/20X;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 354953
    new-instance v0, LX/20X;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v3}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->LIKE:LX/20X;

    .line 354954
    new-instance v0, LX/20X;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v4}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->COMMENT:LX/20X;

    .line 354955
    new-instance v0, LX/20X;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v5}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->SHARE:LX/20X;

    .line 354956
    new-instance v0, LX/20X;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v6}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->SEND:LX/20X;

    .line 354957
    new-instance v0, LX/20X;

    const-string v1, "VISIT_LINK"

    invoke-direct {v0, v1, v7}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->VISIT_LINK:LX/20X;

    .line 354958
    new-instance v0, LX/20X;

    const-string v1, "SAVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/20X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/20X;->SAVE:LX/20X;

    .line 354959
    const/4 v0, 0x6

    new-array v0, v0, [LX/20X;

    sget-object v1, LX/20X;->LIKE:LX/20X;

    aput-object v1, v0, v3

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    aput-object v1, v0, v4

    sget-object v1, LX/20X;->SHARE:LX/20X;

    aput-object v1, v0, v5

    sget-object v1, LX/20X;->SEND:LX/20X;

    aput-object v1, v0, v6

    sget-object v1, LX/20X;->VISIT_LINK:LX/20X;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/20X;->SAVE:LX/20X;

    aput-object v2, v0, v1

    sput-object v0, LX/20X;->$VALUES:[LX/20X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 354960
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/20X;
    .locals 1

    .prologue
    .line 354961
    const-class v0, LX/20X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/20X;

    return-object v0
.end method

.method public static values()[LX/20X;
    .locals 1

    .prologue
    .line 354962
    sget-object v0, LX/20X;->$VALUES:[LX/20X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/20X;

    return-object v0
.end method
