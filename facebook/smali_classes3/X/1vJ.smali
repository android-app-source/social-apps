.class public final LX/1vJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/google/common/annotations/Beta;
.end annotation


# static fields
.field private static final b:LX/1vL;


# instance fields
.field public final a:LX/1vL;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Ljava/io/Closeable;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Throwable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 342220
    sget-object v0, LX/1vK;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 342221
    if-eqz v0, :cond_0

    sget-object v0, LX/1vK;->a:LX/1vK;

    :goto_1
    sput-object v0, LX/1vJ;->b:LX/1vL;

    return-void

    :cond_0
    sget-object v0, LX/525;->a:LX/525;

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(LX/1vL;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 342216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342217
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, LX/1vJ;->c:Ljava/util/Deque;

    .line 342218
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vL;

    iput-object v0, p0, LX/1vJ;->a:LX/1vL;

    .line 342219
    return-void
.end method

.method public static a()LX/1vJ;
    .locals 2

    .prologue
    .line 342215
    new-instance v0, LX/1vJ;

    sget-object v1, LX/1vJ;->b:LX/1vL;

    invoke-direct {v0, v1}, LX/1vJ;-><init>(LX/1vL;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/Closeable;)Ljava/io/Closeable;
    .locals 1
    .param p1    # Ljava/io/Closeable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Ljava/io/Closeable;",
            ">(TC;)TC;"
        }
    .end annotation

    .prologue
    .line 342212
    if-eqz p1, :cond_0

    .line 342213
    iget-object v0, p0, LX/1vJ;->c:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 342214
    :cond_0
    return-object p1
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    .locals 1

    .prologue
    .line 342208
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342209
    iput-object p1, p0, LX/1vJ;->d:Ljava/lang/Throwable;

    .line 342210
    const-class v0, Ljava/io/IOException;

    invoke-static {p1, v0}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 342211
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 342196
    iget-object v0, p0, LX/1vJ;->d:Ljava/lang/Throwable;

    move-object v1, v0

    .line 342197
    :goto_0
    iget-object v0, p0, LX/1vJ;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 342198
    iget-object v0, p0, LX/1vJ;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 342199
    :try_start_0
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342200
    :catch_0
    move-exception v2

    .line 342201
    if-nez v1, :cond_0

    move-object v1, v2

    .line 342202
    goto :goto_0

    .line 342203
    :cond_0
    iget-object v3, p0, LX/1vJ;->a:LX/1vL;

    invoke-interface {v3, v0, v1, v2}, LX/1vL;->a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 342204
    :cond_1
    iget-object v0, p0, LX/1vJ;->d:Ljava/lang/Throwable;

    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 342205
    const-class v0, Ljava/io/IOException;

    invoke-static {v1, v0}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 342206
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 342207
    :cond_2
    return-void
.end method
