.class public LX/1uf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1ug;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 341364
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 341365
    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 341366
    new-instance v0, LX/1vW;

    invoke-direct {v0}, LX/1vW;-><init>()V

    sput-object v0, LX/1uf;->a:LX/1ug;

    .line 341367
    :goto_0
    return-void

    .line 341368
    :cond_0
    new-instance v0, LX/2cn;

    invoke-direct {v0}, LX/2cn;-><init>()V

    sput-object v0, LX/1uf;->a:LX/1ug;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341370
    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 341371
    sget-object v0, LX/1uf;->a:LX/1ug;

    invoke-interface {v0, p0, p1}, LX/1ug;->a(II)I

    move-result v0

    return v0
.end method

.method public static a(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;I)V
    .locals 9

    .prologue
    .line 341372
    sget-object v0, LX/1uf;->a:LX/1ug;

    move v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-interface/range {v0 .. v8}, LX/1ug;->a(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;I)V

    .line 341373
    return-void
.end method

.method public static a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 7

    .prologue
    .line 341374
    sget-object v0, LX/1uf;->a:LX/1ug;

    move v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, LX/1ug;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 341375
    return-void
.end method
