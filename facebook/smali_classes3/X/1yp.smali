.class public LX/1yp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351440
    return-void
.end method

.method public static a(LX/0QB;)LX/1yp;
    .locals 1

    .prologue
    .line 351436
    new-instance v0, LX/1yp;

    invoke-direct {v0}, LX/1yp;-><init>()V

    .line 351437
    move-object v0, v0

    .line 351438
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 351422
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 351423
    if-eqz v0, :cond_1

    const v3, -0x22a42d2a    # -9.8999738E17f

    invoke-static {v0, v3}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351424
    :goto_0
    if-eqz v0, :cond_2

    .line 351425
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    .line 351426
    goto :goto_0

    .line 351427
    :cond_2
    invoke-static {p0}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351428
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 351429
    :cond_3
    invoke-static {p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 351430
    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 351431
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 351432
    :cond_4
    invoke-static {p0}, LX/1VS;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_3
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 351433
    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 351434
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 351435
    goto :goto_1
.end method
