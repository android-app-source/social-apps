.class public LX/1xq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1yW;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 349605
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1xq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349606
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 349607
    iput-object p1, p0, LX/1xq;->b:LX/0Ot;

    .line 349608
    return-void
.end method

.method public static a(LX/0QB;)LX/1xq;
    .locals 4

    .prologue
    .line 349609
    const-class v1, LX/1xq;

    monitor-enter v1

    .line 349610
    :try_start_0
    sget-object v0, LX/1xq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349611
    sput-object v2, LX/1xq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349614
    new-instance v3, LX/1xq;

    const/16 p0, 0x734

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xq;-><init>(LX/0Ot;)V

    .line 349615
    move-object v0, v3

    .line 349616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349620
    invoke-static {}, LX/1dS;->b()V

    .line 349621
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 19

    .prologue
    const/16 v1, 0x8

    const/16 v2, 0x1e

    const v3, -0x1c07b55b

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v18

    .line 349622
    check-cast p6, LX/1yV;

    .line 349623
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v15

    .line 349624
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v16

    .line 349625
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v17

    .line 349626
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yx;

    move-object/from16 v0, p6

    iget-object v6, v0, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p6

    iget v7, v0, LX/1yV;->b:I

    move-object/from16 v0, p6

    iget v8, v0, LX/1yV;->c:I

    move-object/from16 v0, p6

    iget v9, v0, LX/1yV;->d:I

    move-object/from16 v0, p6

    iget v10, v0, LX/1yV;->e:F

    move-object/from16 v0, p6

    iget v11, v0, LX/1yV;->f:F

    move-object/from16 v0, p6

    iget v12, v0, LX/1yV;->g:F

    move-object/from16 v0, p6

    iget v13, v0, LX/1yV;->h:I

    move-object/from16 v0, p6

    iget-boolean v14, v0, LX/1yV;->i:Z

    move-object/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    invoke-virtual/range {v1 .. v17}, LX/1yx;->a(LX/1De;IILX/1no;Lcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLX/1np;LX/1np;LX/1np;)V

    .line 349627
    invoke-virtual {v15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p6

    iput-object v1, v0, LX/1yV;->j:Ljava/lang/CharSequence;

    .line 349628
    invoke-static {v15}, LX/1cy;->a(LX/1np;)V

    .line 349629
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    move-object/from16 v0, p6

    iput-object v1, v0, LX/1yV;->k:Landroid/text/Layout;

    .line 349630
    invoke-static/range {v16 .. v16}, LX/1S3;->a(LX/1np;)V

    .line 349631
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p6

    iput-object v1, v0, LX/1yV;->l:[Landroid/text/style/ClickableSpan;

    .line 349632
    invoke-static/range {v17 .. v17}, LX/1S3;->a(LX/1np;)V

    .line 349633
    const/16 v1, 0x8

    const/16 v2, 0x1f

    const v3, 0x14ddd9bd

    move/from16 v0, v18

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 19

    .prologue
    .line 349634
    check-cast p3, LX/1yV;

    .line 349635
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v16

    .line 349636
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v17

    .line 349637
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v18

    .line 349638
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1yx;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v0, p3

    iget v5, v0, LX/1yV;->b:I

    move-object/from16 v0, p3

    iget v6, v0, LX/1yV;->c:I

    move-object/from16 v0, p3

    iget v7, v0, LX/1yV;->d:I

    move-object/from16 v0, p3

    iget v8, v0, LX/1yV;->e:F

    move-object/from16 v0, p3

    iget v9, v0, LX/1yV;->f:F

    move-object/from16 v0, p3

    iget v10, v0, LX/1yV;->g:F

    move-object/from16 v0, p3

    iget v11, v0, LX/1yV;->h:I

    move-object/from16 v0, p3

    iget-boolean v12, v0, LX/1yV;->i:Z

    move-object/from16 v0, p3

    iget-object v13, v0, LX/1yV;->j:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iget-object v14, v0, LX/1yV;->k:Landroid/text/Layout;

    move-object/from16 v0, p3

    iget-object v15, v0, LX/1yV;->l:[Landroid/text/style/ClickableSpan;

    check-cast v15, [Landroid/text/style/ClickableSpan;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v18}, LX/1yx;->a(LX/1De;LX/1Dg;Lcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLjava/lang/CharSequence;Landroid/text/Layout;[Landroid/text/style/ClickableSpan;LX/1np;LX/1np;LX/1np;)V

    .line 349639
    invoke-virtual/range {v16 .. v16}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1yV;->m:Ljava/lang/CharSequence;

    .line 349640
    invoke-static/range {v16 .. v16}, LX/1S3;->a(LX/1np;)V

    .line 349641
    invoke-virtual/range {v17 .. v17}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1yV;->n:Landroid/text/Layout;

    .line 349642
    invoke-static/range {v17 .. v17}, LX/1S3;->a(LX/1np;)V

    .line 349643
    invoke-virtual/range {v18 .. v18}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p3

    iput-object v1, v0, LX/1yV;->o:[Landroid/text/style/ClickableSpan;

    .line 349644
    invoke-static/range {v18 .. v18}, LX/1S3;->a(LX/1np;)V

    .line 349645
    return-void
.end method

.method public final a(LX/3sp;LX/1X1;)V
    .locals 1

    .prologue
    .line 349646
    check-cast p2, LX/1yV;

    .line 349647
    iget-object v0, p0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/1yV;->m:Ljava/lang/CharSequence;

    .line 349648
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 349649
    invoke-virtual {p1, v0}, LX/3sp;->c(Ljava/lang/CharSequence;)V

    .line 349650
    :cond_0
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349651
    iget-object v0, p0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 349652
    new-instance v0, LX/1oF;

    invoke-direct {v0}, LX/1oF;-><init>()V

    move-object v0, v0

    .line 349653
    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 349554
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 349555
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 349556
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 349557
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 349558
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 349559
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 349560
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 349561
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 349562
    iget-object v0, p0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    invoke-static/range {v0 .. v8}, LX/1yx;->a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 349563
    check-cast p2, LX/1yV;

    .line 349564
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349565
    if-eqz v0, :cond_0

    .line 349566
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349567
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yV;->b:I

    .line 349568
    :cond_0
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 349569
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349570
    if-eqz v0, :cond_1

    .line 349571
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349572
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yV;->c:I

    .line 349573
    :cond_1
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 349574
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349575
    if-eqz v0, :cond_2

    .line 349576
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349577
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yV;->d:I

    .line 349578
    :cond_2
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 349579
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349580
    if-eqz v0, :cond_3

    .line 349581
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349582
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1yV;->e:F

    .line 349583
    :cond_3
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 349584
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349585
    if-eqz v0, :cond_4

    .line 349586
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349587
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1yV;->f:F

    .line 349588
    :cond_4
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 349589
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349590
    if-eqz v0, :cond_5

    .line 349591
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349592
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1yV;->g:F

    .line 349593
    :cond_5
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 349594
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349595
    if-eqz v0, :cond_6

    .line 349596
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349597
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1yV;->h:I

    .line 349598
    :cond_6
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 349599
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349600
    if-eqz v0, :cond_7

    .line 349601
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349602
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/1yV;->i:Z

    .line 349603
    :cond_7
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 349604
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 349553
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 4

    .prologue
    .line 349526
    check-cast p1, LX/1yV;

    .line 349527
    check-cast p2, LX/1yV;

    .line 349528
    iget-object v0, p1, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p2, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 349529
    iget-object v0, p0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yx;

    const/4 p2, 0x0

    .line 349530
    iget-object v2, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 349531
    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349532
    iget-object v3, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 349533
    check-cast v3, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349534
    iget-object p0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 349535
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349536
    iget-object p1, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 349537
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349538
    if-ne p0, p1, :cond_0

    move v2, p2

    .line 349539
    :goto_0
    move v0, v2

    .line 349540
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 349541
    return v0

    :cond_0
    iget-object p0, v0, LX/1yx;->b:LX/1xa;

    invoke-virtual {p0, v2, v3}, LX/1xa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    move v2, p2

    goto :goto_0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 12

    .prologue
    .line 349548
    check-cast p3, LX/1yV;

    .line 349549
    iget-object v0, p0, LX/1xq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/1oF;

    iget-object v1, p3, LX/1yV;->m:Ljava/lang/CharSequence;

    iget-object v2, p3, LX/1yV;->n:Landroid/text/Layout;

    iget-object v0, p3, LX/1yV;->o:[Landroid/text/style/ClickableSpan;

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 349550
    const v3, -0x6e685d

    .line 349551
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v4, p2

    move-object v5, v1

    move-object v6, v2

    move v9, v3

    move-object v11, v0

    invoke-virtual/range {v4 .. v11}, LX/1oF;->a(Ljava/lang/CharSequence;Landroid/text/Layout;FLandroid/content/res/ColorStateList;II[Landroid/text/style/ClickableSpan;)V

    .line 349552
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 349547
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 349546
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 349545
    const/4 v0, 0x1

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 349544
    const/4 v0, 0x1

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 349543
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 349542
    const/16 v0, 0xf

    return v0
.end method
