.class public LX/1vs;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LX/1vs;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/15i;

.field public b:I

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 344778
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, LX/1vs;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 344777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()LX/1vs;
    .locals 2

    .prologue
    .line 344772
    sget-object v0, LX/1vs;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vs;

    .line 344773
    if-nez v0, :cond_0

    .line 344774
    new-instance v0, LX/1vs;

    invoke-direct {v0}, LX/1vs;-><init>()V

    .line 344775
    sget-object v1, LX/1vs;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 344776
    :cond_0
    return-object v0
.end method

.method public static a(LX/15i;I)LX/1vs;
    .locals 2

    .prologue
    .line 344767
    invoke-static {}, LX/1vs;->a()LX/1vs;

    move-result-object v0

    .line 344768
    iput-object p0, v0, LX/1vs;->a:LX/15i;

    .line 344769
    iput p1, v0, LX/1vs;->b:I

    .line 344770
    const/4 v1, 0x0

    iput v1, v0, LX/1vs;->c:I

    .line 344771
    return-object v0
.end method

.method public static a(LX/15i;II)LX/1vs;
    .locals 1

    .prologue
    .line 344762
    invoke-static {}, LX/1vs;->a()LX/1vs;

    move-result-object v0

    .line 344763
    iput-object p0, v0, LX/1vs;->a:LX/15i;

    .line 344764
    iput p1, v0, LX/1vs;->b:I

    .line 344765
    iput p2, v0, LX/1vs;->c:I

    .line 344766
    return-object v0
.end method

.method public static a(LX/186;I)LX/1vs;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 344755
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 344756
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    .line 344757
    invoke-virtual {p0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 344758
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 344759
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 344760
    invoke-static {v1}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 344761
    invoke-static {v0, v1, p1}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v0

    return-object v0
.end method
