.class public final LX/1yN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 350568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350569
    iput p1, p0, LX/1yN;->a:I

    .line 350570
    iput p2, p0, LX/1yN;->b:I

    .line 350571
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 350567
    iget v0, p0, LX/1yN;->a:I

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 350564
    iget v0, p0, LX/1yN;->a:I

    move v0, v0

    .line 350565
    iget v1, p0, LX/1yN;->b:I

    move v1, v1

    .line 350566
    add-int/2addr v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 350556
    if-ne p0, p1, :cond_1

    .line 350557
    :cond_0
    :goto_0
    return v0

    .line 350558
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 350559
    goto :goto_0

    .line 350560
    :cond_3
    check-cast p1, LX/1yN;

    .line 350561
    iget v2, p0, LX/1yN;->b:I

    iget v3, p1, LX/1yN;->b:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/1yN;->a:I

    iget v3, p1, LX/1yN;->a:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 350563
    iget v0, p0, LX/1yN;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/1yN;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 350562
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UTF16Range("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/1yN;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/1yN;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
