.class public final enum LX/20I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/20I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/20I;

.field public static final enum DARK:LX/20I;

.field public static final enum LIGHT:LX/20I;


# instance fields
.field public final backgroundColor:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 354537
    new-instance v0, LX/20I;

    const-string v1, "LIGHT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, LX/20I;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/20I;->LIGHT:LX/20I;

    .line 354538
    new-instance v0, LX/20I;

    const-string v1, "DARK"

    const v2, -0x4cccccd

    invoke-direct {v0, v1, v4, v2}, LX/20I;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/20I;->DARK:LX/20I;

    .line 354539
    const/4 v0, 0x2

    new-array v0, v0, [LX/20I;

    sget-object v1, LX/20I;->LIGHT:LX/20I;

    aput-object v1, v0, v3

    sget-object v1, LX/20I;->DARK:LX/20I;

    aput-object v1, v0, v4

    sput-object v0, LX/20I;->$VALUES:[LX/20I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 354534
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 354535
    iput p3, p0, LX/20I;->backgroundColor:I

    .line 354536
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/20I;
    .locals 1

    .prologue
    .line 354533
    const-class v0, LX/20I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/20I;

    return-object v0
.end method

.method public static values()[LX/20I;
    .locals 1

    .prologue
    .line 354532
    sget-object v0, LX/20I;->$VALUES:[LX/20I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/20I;

    return-object v0
.end method
