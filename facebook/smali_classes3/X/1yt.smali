.class public LX/1yt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/17Q;

.field public final b:LX/0Zb;

.field public final c:LX/1yf;


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;LX/1yf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351532
    iput-object p1, p0, LX/1yt;->a:LX/17Q;

    .line 351533
    iput-object p2, p0, LX/1yt;->b:LX/0Zb;

    .line 351534
    iput-object p3, p0, LX/1yt;->c:LX/1yf;

    .line 351535
    return-void
.end method

.method public static a(LX/0QB;)LX/1yt;
    .locals 6

    .prologue
    .line 351536
    const-class v1, LX/1yt;

    monitor-enter v1

    .line 351537
    :try_start_0
    sget-object v0, LX/1yt;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351538
    sput-object v2, LX/1yt;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351541
    new-instance p0, LX/1yt;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v3

    check-cast v3, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v5

    check-cast v5, LX/1yf;

    invoke-direct {p0, v3, v4, v5}, LX/1yt;-><init>(LX/17Q;LX/0Zb;LX/1yf;)V

    .line 351542
    move-object v0, p0

    .line 351543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/14w;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 351547
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351548
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 351549
    if-nez v0, :cond_1

    .line 351550
    :cond_0
    :goto_0
    move v0, v1

    .line 351551
    return v0

    .line 351552
    :cond_1
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 351553
    if-eqz v2, :cond_0

    .line 351554
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    .line 351555
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    .line 351556
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const p1, 0x285feb

    if-ne p0, p1, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v2, p0, :cond_0

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v2, p0, :cond_0

    const v2, 0x4fb5732f

    invoke-static {v0, v2}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
