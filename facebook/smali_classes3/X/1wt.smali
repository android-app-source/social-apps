.class public LX/1wt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0ad;

.field public final d:LX/1wu;


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1wu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347104
    iput-object p1, p0, LX/1wt;->a:LX/0SG;

    .line 347105
    iput-object p2, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 347106
    iput-object p3, p0, LX/1wt;->c:LX/0ad;

    .line 347107
    iput-object p4, p0, LX/1wt;->d:LX/1wu;

    .line 347108
    return-void
.end method

.method public static b(LX/0QB;)LX/1wt;
    .locals 6

    .prologue
    .line 347109
    new-instance v4, LX/1wt;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    .line 347110
    new-instance v5, LX/1wu;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v5, v3}, LX/1wu;-><init>(LX/0ad;)V

    .line 347111
    move-object v3, v5

    .line 347112
    check-cast v3, LX/1wu;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1wt;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/1wu;)V

    .line 347113
    return-object v4
.end method

.method public static h(LX/1wt;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 347094
    iget-object v0, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3fU;->g:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 347095
    iget-object v1, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3fU;->i:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 347096
    if-nez v1, :cond_1

    .line 347097
    iget-object v1, p0, LX/1wt;->d:LX/1wu;

    invoke-virtual {v1}, LX/1wu;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 347098
    invoke-direct {p0}, LX/1wt;->i()J

    move-result-wide v0

    invoke-direct {p0}, LX/1wt;->j()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 347099
    :goto_0
    iget-object v2, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/3fU;->f:LX/0Tn;

    iget-object v4, p0, LX/1wt;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    add-long/2addr v0, v4

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 347100
    return-void

    .line 347101
    :cond_0
    invoke-direct {p0}, LX/1wt;->i()J

    move-result-wide v0

    goto :goto_0

    .line 347102
    :cond_1
    invoke-static {p0}, LX/1wt;->k(LX/1wt;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private i()J
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 347085
    iget-object v0, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3fU;->g:LX/0Tn;

    invoke-interface {v0, v1, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 347086
    iget-object v1, p0, LX/1wt;->d:LX/1wu;

    invoke-virtual {v1}, LX/1wu;->a()LX/0Px;

    move-result-object v4

    .line 347087
    iget-object v1, p0, LX/1wt;->c:LX/0ad;

    sget-wide v2, LX/Hc1;->a:J

    const-wide/32 v6, 0x5265c00

    invoke-interface {v1, v2, v3, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v2

    .line 347088
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v1, v0

    .line 347089
    const-wide/16 v6, 0x0

    int-to-long v8, v1

    mul-long/2addr v2, v8

    add-long/2addr v2, v6

    .line 347090
    add-int/lit8 v0, v0, -0x1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 347091
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 347092
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 347093
    :cond_0
    return-wide v2
.end method

.method private j()J
    .locals 11

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    .line 347114
    iget-object v2, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3fU;->g:LX/0Tn;

    invoke-interface {v2, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 347115
    iget-object v3, p0, LX/1wt;->d:LX/1wu;

    invoke-virtual {v3}, LX/1wu;->a()LX/0Px;

    move-result-object v4

    .line 347116
    iget-object v3, p0, LX/1wt;->c:LX/0ad;

    sget-wide v6, LX/Hc1;->a:J

    const-wide/32 v8, 0x5265c00

    invoke-interface {v3, v6, v7, v8, v9}, LX/0ad;->a(JJ)J

    move-result-wide v6

    .line 347117
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    sub-int/2addr v3, v2

    .line 347118
    if-lez v3, :cond_0

    .line 347119
    int-to-long v8, v3

    mul-long/2addr v6, v8

    add-long/2addr v0, v6

    .line 347120
    :cond_0
    add-int/lit8 v2, v2, -0x1

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v10, v2

    move-wide v2, v0

    move v1, v10

    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    .line 347121
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 347122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 347123
    :cond_1
    invoke-static {p0}, LX/1wt;->k(LX/1wt;)J

    move-result-wide v0

    add-long/2addr v0, v2

    .line 347124
    return-wide v0
.end method

.method private static k(LX/1wt;)J
    .locals 11

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    .line 347075
    iget-object v2, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3fU;->i:LX/0Tn;

    invoke-interface {v2, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 347076
    iget-object v3, p0, LX/1wt;->d:LX/1wu;

    invoke-virtual {v3}, LX/1wu;->b()LX/0Px;

    move-result-object v4

    .line 347077
    iget-object v3, p0, LX/1wt;->c:LX/0ad;

    sget-wide v6, LX/Hc1;->a:J

    const-wide/32 v8, 0x5265c00

    invoke-interface {v3, v6, v7, v8, v9}, LX/0ad;->a(JJ)J

    move-result-wide v6

    .line 347078
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    sub-int/2addr v3, v2

    .line 347079
    if-lez v3, :cond_0

    .line 347080
    int-to-long v8, v3

    mul-long/2addr v6, v8

    add-long/2addr v0, v6

    .line 347081
    :cond_0
    add-int/lit8 v2, v2, -0x1

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v10, v2

    move-wide v2, v0

    move v1, v10

    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 347082
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 347083
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 347084
    :cond_1
    return-wide v2
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 347072
    iget-object v0, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->g:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->h:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->i:LX/0Tn;

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->j:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->e:LX/0Tn;

    iget-object v2, p0, LX/1wt;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 347073
    invoke-static {p0}, LX/1wt;->h(LX/1wt;)V

    .line 347074
    return-void
.end method

.method public final c()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 347059
    iget-object v0, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/3fU;->g:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 347060
    iget-object v3, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3fU;->h:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 347061
    if-nez v0, :cond_0

    move v0, v1

    .line 347062
    :goto_0
    return v0

    .line 347063
    :cond_0
    iget-object v3, p0, LX/1wt;->d:LX/1wu;

    invoke-virtual {v3}, LX/1wu;->a()LX/0Px;

    move-result-object v3

    .line 347064
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    if-ge v0, v6, :cond_2

    .line 347065
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 347066
    iget-object v0, p0, LX/1wt;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    add-long/2addr v4, v6

    cmp-long v0, v8, v4

    if-lez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 347067
    goto :goto_0
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 347068
    iget-object v0, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3fU;->i:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 347069
    iget-object v1, p0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3fU;->i:LX/0Tn;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->j:LX/0Tn;

    iget-object v2, p0, LX/1wt;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 347070
    invoke-static {p0}, LX/1wt;->h(LX/1wt;)V

    .line 347071
    return-void
.end method
