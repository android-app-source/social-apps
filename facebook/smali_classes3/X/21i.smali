.class public LX/21i;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/21i",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357745
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 357746
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/21i;->b:LX/0Zi;

    .line 357747
    iput-object p1, p0, LX/21i;->a:LX/0Ot;

    .line 357748
    return-void
.end method

.method public static a(LX/0QB;)LX/21i;
    .locals 4

    .prologue
    .line 357749
    const-class v1, LX/21i;

    monitor-enter v1

    .line 357750
    :try_start_0
    sget-object v0, LX/21i;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357751
    sput-object v2, LX/21i;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357752
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357753
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357754
    new-instance v3, LX/21i;

    const/16 p0, 0x913

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/21i;-><init>(LX/0Ot;)V

    .line 357755
    move-object v0, v3

    .line 357756
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357757
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/21i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357758
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357759
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357760
    const v0, 0x519181a9

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 357761
    check-cast p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    .line 357762
    iget-object v0, p0, LX/21i;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;

    iget-object v2, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    iget-boolean v4, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->c:Z

    iget-object v5, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v6, p2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->e:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;ZLcom/facebook/common/callercontext/CallerContext;Z)LX/1Dg;

    move-result-object v0

    .line 357763
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 357764
    invoke-static {}, LX/1dS;->b()V

    .line 357765
    iget v0, p1, LX/1dQ;->b:I

    .line 357766
    packed-switch v0, :pswitch_data_0

    .line 357767
    :goto_0
    return-object v2

    .line 357768
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 357769
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 357770
    check-cast v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    .line 357771
    iget-object v3, p0, LX/21i;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;

    iget-object p1, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    .line 357772
    iget-object p0, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponentSpec;->c:LX/36i;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, LX/36i;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;Z)V

    .line 357773
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x519181a9
        :pswitch_0
    .end packed-switch
.end method
