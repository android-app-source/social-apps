.class public final LX/22O;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1xN;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xN",
            "<TE;>.PhotoAttachmentComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1xN;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1xN;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 359588
    iput-object p1, p0, LX/22O;->b:LX/1xN;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 359589
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/22O;->c:[Ljava/lang/String;

    .line 359590
    iput v3, p0, LX/22O;->d:I

    .line 359591
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/22O;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/22O;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/22O;LX/1De;IILcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1xN",
            "<TE;>.PhotoAttachmentComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 359584
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 359585
    iput-object p4, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359586
    iget-object v0, p0, LX/22O;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 359587
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/22O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1xN",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359581
    iget-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    .line 359582
    iget-object v0, p0, LX/22O;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359583
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/22O;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/1xN",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359560
    iget-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 359561
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1xN",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359578
    iget-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 359579
    iget-object v0, p0, LX/22O;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359580
    return-object p0
.end method

.method public final a(Z)LX/22O;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xN",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359576
    iget-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-boolean p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->e:Z

    .line 359577
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 359572
    invoke-super {p0}, LX/1X5;->a()V

    .line 359573
    const/4 v0, 0x0

    iput-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359574
    iget-object v0, p0, LX/22O;->b:LX/1xN;

    iget-object v0, v0, LX/1xN;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 359575
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1xN;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 359562
    iget-object v1, p0, LX/22O;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/22O;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/22O;->d:I

    if-ge v1, v2, :cond_2

    .line 359563
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 359564
    :goto_0
    iget v2, p0, LX/22O;->d:I

    if-ge v0, v2, :cond_1

    .line 359565
    iget-object v2, p0, LX/22O;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 359566
    iget-object v2, p0, LX/22O;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359567
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359568
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359569
    :cond_2
    iget-object v0, p0, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359570
    invoke-virtual {p0}, LX/22O;->a()V

    .line 359571
    return-object v0
.end method
