.class public LX/1yf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static b:LX/03R;

.field private static volatile c:LX/1yf;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 351010
    sget-object v0, LX/03R;->UNSET:LX/03R;

    sput-object v0, LX/1yf;->b:LX/03R;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 351011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351012
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 351013
    iput-object v0, p0, LX/1yf;->a:LX/0Ot;

    .line 351014
    return-void
.end method

.method public static a(LX/0QB;)LX/1yf;
    .locals 4

    .prologue
    .line 351015
    sget-object v0, LX/1yf;->c:LX/1yf;

    if-nez v0, :cond_1

    .line 351016
    const-class v1, LX/1yf;

    monitor-enter v1

    .line 351017
    :try_start_0
    sget-object v0, LX/1yf;->c:LX/1yf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 351018
    if-eqz v2, :cond_0

    .line 351019
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 351020
    new-instance v3, LX/1yf;

    invoke-direct {v3}, LX/1yf;-><init>()V

    .line 351021
    const/16 p0, 0x1032

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 351022
    iput-object p0, v3, LX/1yf;->a:LX/0Ot;

    .line 351023
    move-object v0, v3

    .line 351024
    sput-object v0, LX/1yf;->c:LX/1yf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 351026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 351027
    :cond_1
    sget-object v0, LX/1yf;->c:LX/1yf;

    return-object v0

    .line 351028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 351029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 351030
    sget-object v0, LX/1yf;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351031
    iget-object v0, p0, LX/1yf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0fe;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    sput-object v0, LX/1yf;->b:LX/03R;

    .line 351032
    :cond_0
    sget-object v0, LX/1yf;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    return v0
.end method
