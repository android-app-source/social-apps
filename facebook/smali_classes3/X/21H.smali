.class public LX/21H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1VF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:LX/20S;

.field public final d:LX/0wd;

.field public final e:Z

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1wL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/Double;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>(LX/20S;LX/0wW;LX/1PT;)V
    .locals 11
    .param p3    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 356835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356836
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 356837
    iput-object v0, p0, LX/21H;->a:LX/0Ot;

    .line 356838
    iput-boolean v1, p0, LX/21H;->k:Z

    .line 356839
    iput-object p1, p0, LX/21H;->c:LX/20S;

    .line 356840
    invoke-static {p1}, LX/20S;->o(LX/20S;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 356841
    iget-object v0, p1, LX/20S;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 356842
    iget-object v0, p1, LX/20S;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/0wj;->m:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, LX/20S;->d:Ljava/lang/Boolean;

    .line 356843
    :cond_0
    iget-object v0, p1, LX/20S;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 356844
    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 356845
    if-eqz v0, :cond_2

    .line 356846
    if-eqz p3, :cond_6

    invoke-interface {p3}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v2, LX/1Qt;->PERMALINK:LX/1Qt;

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 356847
    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/21H;->e:Z

    .line 356848
    invoke-virtual {p0}, LX/21H;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 356849
    const/4 v0, 0x0

    iput-object v0, p0, LX/21H;->d:LX/0wd;

    .line 356850
    :goto_3
    return-void

    .line 356851
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 356852
    :cond_3
    invoke-virtual {p2}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iget-object v2, p0, LX/21H;->c:LX/20S;

    const/high16 v10, 0x41200000    # 10.0f

    .line 356853
    iget-object v6, v2, LX/20S;->h:LX/0wT;

    if-nez v6, :cond_4

    .line 356854
    iget-object v6, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v7

    if-eqz v7, :cond_7

    sget-short v7, LX/0fe;->bk:S

    :goto_4
    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 356855
    iget-object v6, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v7

    if-eqz v7, :cond_8

    sget v7, LX/0fe;->bj:F

    :goto_5
    const/high16 v8, 0x42200000    # 40.0f

    invoke-interface {v6, v7, v8}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v8, v6

    iget-object v6, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v7

    if-eqz v7, :cond_9

    sget v7, LX/0fe;->bh:F

    :goto_6
    invoke-interface {v6, v7, v10}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v6, v6

    invoke-static {v8, v9, v6, v7}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v6

    iput-object v6, v2, LX/20S;->h:LX/0wT;

    .line 356856
    :cond_4
    :goto_7
    iget-object v6, v2, LX/20S;->h:LX/0wT;

    move-object v2, v6

    .line 356857
    invoke-virtual {v0, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 356858
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 356859
    move-object v0, v0

    .line 356860
    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/21H;->d:LX/0wd;

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 356861
    :cond_7
    sget-short v7, LX/0wj;->t:S

    goto :goto_4

    .line 356862
    :cond_8
    sget v7, LX/0wj;->s:F

    goto :goto_5

    :cond_9
    sget v7, LX/0wj;->q:F

    goto :goto_6

    .line 356863
    :cond_a
    iget-object v6, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v7

    if-eqz v7, :cond_b

    sget v7, LX/0fe;->bi:F

    :goto_8
    invoke-interface {v6, v7, v10}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v8, v6

    iget-object v6, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v7

    if-eqz v7, :cond_c

    sget v7, LX/0fe;->bg:F

    :goto_9
    const/4 v10, 0x0

    invoke-interface {v6, v7, v10}, LX/0ad;->a(FF)F

    move-result v6

    float-to-double v6, v6

    invoke-static {v8, v9, v6, v7}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v6

    iput-object v6, v2, LX/20S;->h:LX/0wT;

    goto :goto_7

    :cond_b
    sget v7, LX/0wj;->r:F

    goto :goto_8

    :cond_c
    sget v7, LX/0wj;->p:F

    goto :goto_9
.end method

.method public static c(LX/21H;LX/1zt;)V
    .locals 4

    .prologue
    .line 356934
    iget-object v0, p0, LX/21H;->c:LX/20S;

    .line 356935
    if-eqz p1, :cond_1

    .line 356936
    iget-object v1, v0, LX/20S;->m:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 356937
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_a

    sget-short v2, LX/0fe;->bn:S

    :goto_0
    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/20S;->m:Ljava/lang/Boolean;

    .line 356938
    :cond_0
    iget-object v1, v0, LX/20S;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 356939
    if-nez v1, :cond_2

    .line 356940
    :cond_1
    invoke-static {v0}, LX/20S;->s(LX/20S;)Ljava/lang/String;

    move-result-object v1

    .line 356941
    :goto_1
    move-object v0, v1

    .line 356942
    iput-object v0, p0, LX/21H;->j:Ljava/lang/String;

    .line 356943
    return-void

    .line 356944
    :cond_2
    iget v1, p1, LX/1zt;->e:I

    move v1, v1

    .line 356945
    packed-switch v1, :pswitch_data_0

    .line 356946
    :pswitch_0
    invoke-static {v0}, LX/20S;->s(LX/20S;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 356947
    :pswitch_1
    iget-object v1, v0, LX/20S;->u:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 356948
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_b

    sget-char v2, LX/0fe;->ba:C

    move v3, v2

    :goto_2
    iget-object v2, v0, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f080ffd

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/20S;->u:Ljava/lang/String;

    .line 356949
    :cond_3
    iget-object v1, v0, LX/20S;->u:Ljava/lang/String;

    move-object v1, v1

    .line 356950
    goto :goto_1

    .line 356951
    :pswitch_2
    iget-object v1, v0, LX/20S;->v:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 356952
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_c

    sget-char v2, LX/0fe;->bq:C

    move v3, v2

    :goto_3
    iget-object v2, v0, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f080ffe

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/20S;->v:Ljava/lang/String;

    .line 356953
    :cond_4
    iget-object v1, v0, LX/20S;->v:Ljava/lang/String;

    move-object v1, v1

    .line 356954
    goto :goto_1

    .line 356955
    :pswitch_3
    iget-object v1, v0, LX/20S;->w:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 356956
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_d

    sget-char v2, LX/0fe;->aY:C

    move v3, v2

    :goto_4
    iget-object v2, v0, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f080fff

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/20S;->w:Ljava/lang/String;

    .line 356957
    :cond_5
    iget-object v1, v0, LX/20S;->w:Ljava/lang/String;

    move-object v1, v1

    .line 356958
    goto/16 :goto_1

    .line 356959
    :pswitch_4
    invoke-static {v0}, LX/20S;->r(LX/20S;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v0}, LX/20S;->s(LX/20S;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 356960
    :cond_6
    iget-object v1, v0, LX/20S;->x:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 356961
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_e

    sget-char v2, LX/0fe;->bd:C

    move v3, v2

    :goto_5
    iget-object v2, v0, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f081000

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/20S;->x:Ljava/lang/String;

    .line 356962
    :cond_7
    iget-object v1, v0, LX/20S;->x:Ljava/lang/String;

    move-object v1, v1

    .line 356963
    goto/16 :goto_1

    .line 356964
    :pswitch_5
    invoke-static {v0}, LX/20S;->r(LX/20S;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {v0}, LX/20S;->s(LX/20S;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 356965
    :cond_8
    iget-object v1, v0, LX/20S;->y:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 356966
    iget-object v1, v0, LX/20S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {v0}, LX/20S;->o(LX/20S;)Z

    move-result v2

    if-eqz v2, :cond_f

    sget-char v2, LX/0fe;->aS:C

    move v3, v2

    :goto_6
    iget-object v2, v0, LX/20S;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f081001

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/20S;->y:Ljava/lang/String;

    .line 356967
    :cond_9
    iget-object v1, v0, LX/20S;->y:Ljava/lang/String;

    move-object v1, v1

    .line 356968
    goto/16 :goto_1

    .line 356969
    :cond_a
    sget-short v2, LX/0wj;->w:S

    goto/16 :goto_0

    .line 356970
    :cond_b
    sget-char v2, LX/0wj;->k:C

    move v3, v2

    goto/16 :goto_2

    .line 356971
    :cond_c
    sget-char v2, LX/0wj;->z:C

    move v3, v2

    goto/16 :goto_3

    .line 356972
    :cond_d
    sget-char v2, LX/0wj;->i:C

    move v3, v2

    goto/16 :goto_4

    .line 356973
    :cond_e
    sget-char v2, LX/0wj;->o:C

    move v3, v2

    goto :goto_5

    .line 356974
    :cond_f
    sget-char v2, LX/0wj;->b:C

    move v3, v2

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private g()LX/1wL;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 356933
    iget-object v0, p0, LX/21H;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/21H;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wL;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/21H;)V
    .locals 11

    .prologue
    .line 356923
    iget-object v0, p0, LX/21H;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 356924
    new-instance v0, Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiStateImpl$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiStateImpl$1;-><init>(LX/21H;)V

    iput-object v0, p0, LX/21H;->g:Ljava/lang/Runnable;

    .line 356925
    :cond_0
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/21H;->i:Ljava/lang/Double;

    .line 356926
    iget-object v0, p0, LX/21H;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, LX/21H;->g:Ljava/lang/Runnable;

    iget-object v2, p0, LX/21H;->c:LX/20S;

    .line 356927
    iget-object v5, v2, LX/20S;->g:Ljava/lang/Long;

    if-nez v5, :cond_1

    .line 356928
    iget-object v5, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-wide v7, LX/0fe;->bc:J

    :goto_0
    const-wide/16 v9, 0x1388

    invoke-interface {v5, v7, v8, v9, v10}, LX/0ad;->a(JJ)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, LX/20S;->g:Ljava/lang/Long;

    .line 356929
    :cond_1
    iget-object v5, v2, LX/20S;->g:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-wide v2, v5

    .line 356930
    const v4, 0x1f398103

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 356931
    return-void

    .line 356932
    :cond_2
    sget-wide v7, LX/0wj;->n:J

    goto :goto_0
.end method

.method public static i(LX/21H;)V
    .locals 11

    .prologue
    .line 356912
    iget-object v0, p0, LX/21H;->h:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 356913
    new-instance v0, Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiStateImpl$2;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiStateImpl$2;-><init>(LX/21H;)V

    iput-object v0, p0, LX/21H;->h:Ljava/lang/Runnable;

    .line 356914
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/21H;->k:Z

    .line 356915
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/21H;->i:Ljava/lang/Double;

    .line 356916
    iget-object v0, p0, LX/21H;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, LX/21H;->h:Ljava/lang/Runnable;

    iget-object v2, p0, LX/21H;->c:LX/20S;

    .line 356917
    iget-object v5, v2, LX/20S;->f:Ljava/lang/Long;

    if-nez v5, :cond_1

    .line 356918
    iget-object v5, v2, LX/20S;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v2}, LX/20S;->o(LX/20S;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-wide v7, LX/0fe;->aU:J

    :goto_0
    const-wide/16 v9, 0x320

    invoke-interface {v5, v7, v8, v9, v10}, LX/0ad;->a(JJ)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, LX/20S;->f:Ljava/lang/Long;

    .line 356919
    :cond_1
    iget-object v5, v2, LX/20S;->f:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-wide v2, v5

    .line 356920
    const v4, -0x3a5b3da3

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 356921
    return-void

    .line 356922
    :cond_2
    sget-wide v7, LX/0wj;->d:J

    goto :goto_0
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 356911
    iget-object v0, p0, LX/21H;->d:LX/0wd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/21H;->d:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 356907
    invoke-direct {p0}, LX/21H;->g()LX/1wL;

    move-result-object v0

    .line 356908
    if-eqz v0, :cond_0

    .line 356909
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/1wL;->a(D)V

    .line 356910
    :cond_0
    return-void
.end method

.method public final a(LX/1wL;)V
    .locals 1
    .param p1    # LX/1wL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 356904
    iget-boolean v0, p0, LX/21H;->e:Z

    if-nez v0, :cond_0

    .line 356905
    :goto_0
    return-void

    .line 356906
    :cond_0
    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_1
    iput-object v0, p0, LX/21H;->f:Ljava/lang/ref/WeakReference;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 356897
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 356898
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/21H;->k:Z

    .line 356899
    :cond_0
    :goto_0
    return-void

    .line 356900
    :cond_1
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    .line 356901
    iget-object v0, p0, LX/21H;->c:LX/20S;

    invoke-virtual {v0}, LX/20S;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356902
    invoke-static {p0}, LX/21H;->h(LX/21H;)V

    goto :goto_0

    .line 356903
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Progressive UFI spring is at rest with illegal value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/1zt;)V
    .locals 8

    .prologue
    .line 356875
    invoke-virtual {p0}, LX/21H;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356876
    :goto_0
    return-void

    .line 356877
    :cond_0
    invoke-direct {p0}, LX/21H;->g()LX/1wL;

    move-result-object v0

    .line 356878
    if-eqz v0, :cond_1

    invoke-static {p1}, LX/20S;->b(LX/1zt;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356879
    invoke-static {p0, p1}, LX/21H;->c(LX/21H;LX/1zt;)V

    .line 356880
    iget-object v1, p0, LX/21H;->j:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1wL;->a(Ljava/lang/String;)V

    .line 356881
    :cond_1
    const-wide/16 v4, 0x0

    .line 356882
    invoke-static {p1}, LX/20S;->b(LX/1zt;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 356883
    iget-boolean v2, p0, LX/21H;->k:Z

    move v2, v2

    .line 356884
    if-eqz v2, :cond_3

    .line 356885
    invoke-static {p0}, LX/21H;->i(LX/21H;)V

    .line 356886
    :cond_2
    :goto_1
    goto :goto_0

    .line 356887
    :cond_3
    iget-object v2, p0, LX/21H;->d:LX/0wd;

    .line 356888
    iget-wide v6, v2, LX/0wd;->i:D

    move-wide v2, v6

    .line 356889
    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    .line 356890
    invoke-virtual {p0}, LX/21H;->e()V

    goto :goto_1

    .line 356891
    :cond_4
    iget-object v2, p0, LX/21H;->i:Ljava/lang/Double;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/21H;->i:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    .line 356892
    iget-object v2, p0, LX/21H;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    iget-object v3, p0, LX/21H;->g:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 356893
    invoke-static {p0}, LX/21H;->h(LX/21H;)V

    goto :goto_1

    .line 356894
    :cond_5
    iget-boolean v2, p0, LX/21H;->k:Z

    move v2, v2

    .line 356895
    if-nez v2, :cond_2

    .line 356896
    invoke-virtual {p0}, LX/21H;->f()V

    goto :goto_1
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 356874
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 356873
    iget-boolean v0, p0, LX/21H;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/21H;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/21H;->l:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 356872
    return-void
.end method

.method public final e()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 356869
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/21H;->i:Ljava/lang/Double;

    .line 356870
    iget-object v0, p0, LX/21H;->d:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 356871
    return-void
.end method

.method public final f()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 356864
    iget-object v0, p0, LX/21H;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, LX/21H;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 356865
    iget-object v0, p0, LX/21H;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v1, p0, LX/21H;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 356866
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, LX/21H;->i:Ljava/lang/Double;

    .line 356867
    iget-object v0, p0, LX/21H;->d:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 356868
    return-void
.end method
