.class public LX/1uo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:LX/1Up;

.field public static final b:LX/1Up;

.field public static final c:LX/1Up;

.field public static final d:LX/1Up;

.field public static final e:LX/1Up;

.field private static final f:LX/1Up;

.field private static final g:LX/1Up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 341662
    sget-object v0, LX/1Uo;->b:LX/1Up;

    sput-object v0, LX/1uo;->f:LX/1Up;

    .line 341663
    sget-object v0, LX/1Uo;->a:LX/1Up;

    sput-object v0, LX/1uo;->g:LX/1Up;

    .line 341664
    sget-object v0, LX/1uo;->f:LX/1Up;

    sput-object v0, LX/1uo;->a:LX/1Up;

    .line 341665
    sget-object v0, LX/1uo;->g:LX/1Up;

    sput-object v0, LX/1uo;->b:LX/1Up;

    .line 341666
    sget-object v0, LX/1uo;->g:LX/1Up;

    sput-object v0, LX/1uo;->c:LX/1Up;

    .line 341667
    sget-object v0, LX/1uo;->g:LX/1Up;

    sput-object v0, LX/1uo;->d:LX/1Up;

    .line 341668
    sget-object v0, LX/1uo;->g:LX/1Up;

    sput-object v0, LX/1uo;->e:LX/1Up;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;LX/1oI;LX/4Ab;Landroid/graphics/ColorFilter;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;ILX/1dc;)V
    .locals 1
    .param p2    # LX/4Ab;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/ColorFilter;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p7    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p9    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p12    # LX/1Up;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1oI",
            "<",
            "LX/1oH;",
            ">;",
            "LX/4Ab;",
            "Landroid/graphics/ColorFilter;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "LX/1Up;",
            "I",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 341649
    invoke-virtual {p1}, LX/1oI;->d()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1oH;

    .line 341650
    iput-object p0, v0, LX/1oH;->b:LX/1De;

    .line 341651
    invoke-virtual {v0, p4, p5}, LX/1oH;->a(LX/1dc;LX/1Up;)V

    .line 341652
    invoke-virtual {v0, p6, p7}, LX/1oH;->c(LX/1dc;LX/1Up;)V

    .line 341653
    invoke-virtual {v0, p8, p9, p10}, LX/1oH;->a(LX/1dc;LX/1Up;I)V

    .line 341654
    invoke-virtual {v0, p11, p12}, LX/1oH;->b(LX/1dc;LX/1Up;)V

    .line 341655
    invoke-virtual {v0, p2}, LX/1oH;->a(LX/4Ab;)V

    .line 341656
    invoke-virtual {v0, p3}, LX/1oH;->a(Landroid/graphics/ColorFilter;)V

    .line 341657
    invoke-virtual {v0, p14}, LX/1oH;->a(LX/1dc;)V

    .line 341658
    invoke-virtual {v0, p13}, LX/1oH;->a(I)V

    .line 341659
    invoke-virtual {p1}, LX/1oI;->b()V

    .line 341660
    return-void
.end method
