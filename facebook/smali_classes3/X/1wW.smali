.class public LX/1wW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/1wW;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346229
    const-class v0, LX/1wW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1wW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 346253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346254
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 346255
    iput-object v0, p0, LX/1wW;->b:LX/0Ot;

    .line 346256
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 346257
    iput-object v0, p0, LX/1wW;->c:LX/0Ot;

    .line 346258
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 346259
    iput-object v0, p0, LX/1wW;->d:LX/0Ot;

    .line 346260
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 346261
    iput-object v0, p0, LX/1wW;->e:LX/0Ot;

    .line 346262
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 346263
    iput-object v0, p0, LX/1wW;->f:LX/0Ot;

    .line 346264
    return-void
.end method

.method public static a(LX/0QB;)LX/1wW;
    .locals 8

    .prologue
    .line 346238
    sget-object v0, LX/1wW;->g:LX/1wW;

    if-nez v0, :cond_1

    .line 346239
    const-class v1, LX/1wW;

    monitor-enter v1

    .line 346240
    :try_start_0
    sget-object v0, LX/1wW;->g:LX/1wW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 346241
    if-eqz v2, :cond_0

    .line 346242
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 346243
    new-instance v3, LX/1wW;

    invoke-direct {v3}, LX/1wW;-><init>()V

    .line 346244
    const/16 v4, 0x2db

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1364

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1369

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x136b

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x1381

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 346245
    iput-object v4, v3, LX/1wW;->b:LX/0Ot;

    iput-object v5, v3, LX/1wW;->c:LX/0Ot;

    iput-object v6, v3, LX/1wW;->d:LX/0Ot;

    iput-object v7, v3, LX/1wW;->e:LX/0Ot;

    iput-object p0, v3, LX/1wW;->f:LX/0Ot;

    .line 346246
    move-object v0, v3

    .line 346247
    sput-object v0, LX/1wW;->g:LX/1wW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346248
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 346249
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346250
    :cond_1
    sget-object v0, LX/1wW;->g:LX/1wW;

    return-object v0

    .line 346251
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 346252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 346230
    iget-object v0, p0, LX/1wW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1wW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 346231
    :cond_0
    :goto_0
    return-void

    .line 346232
    :cond_1
    const-string v0, "VideoHomeAppStateManager.onUserEnteredApp"

    const v1, -0x2d2ede8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 346233
    :try_start_0
    iget-object v0, p0, LX/1wW;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    iget-object v1, p0, LX/1wW;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/3AW;->a(J)V

    .line 346234
    iget-object v0, p0, LX/1wW;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    invoke-virtual {v0}, LX/0hn;->e()V

    .line 346235
    iget-object v0, p0, LX/1wW;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 346236
    iget-object v0, p0, LX/1wW;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AX;

    invoke-virtual {v0}, LX/3AX;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346237
    :cond_2
    const v0, -0x13ece837

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x619a4d9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
