.class public LX/20z;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0ih;


# instance fields
.field public final b:LX/0if;

.field public final c:LX/0Zb;

.field public final d:LX/0SG;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:J

.field public i:LX/210;

.field public j:LX/211;

.field public k:LX/20M;

.field public l:LX/1zt;

.field public m:LX/1zt;

.field public n:LX/1zt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 356465
    sget-object v0, LX/0ig;->s:LX/0ih;

    sput-object v0, LX/20z;->a:LX/0ih;

    return-void
.end method

.method public constructor <init>(LX/0if;LX/0Zb;LX/0SG;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356467
    sget-object v0, LX/20M;->NONE:LX/20M;

    iput-object v0, p0, LX/20z;->k:LX/20M;

    .line 356468
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/20z;->l:LX/1zt;

    .line 356469
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/20z;->m:LX/1zt;

    .line 356470
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/20z;->n:LX/1zt;

    .line 356471
    iput-object p1, p0, LX/20z;->b:LX/0if;

    .line 356472
    iput-object p2, p0, LX/20z;->c:LX/0Zb;

    .line 356473
    iput-object p3, p0, LX/20z;->d:LX/0SG;

    .line 356474
    iput-object p4, p0, LX/20z;->e:Ljava/lang/String;

    .line 356475
    iput-object p5, p0, LX/20z;->g:Ljava/lang/String;

    .line 356476
    iput-object p6, p0, LX/20z;->f:Ljava/lang/String;

    .line 356477
    invoke-static {p0}, LX/20z;->c(LX/20z;)V

    .line 356478
    return-void
.end method

.method public static c(LX/20z;)V
    .locals 2

    .prologue
    .line 356479
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/20z;->h:J

    .line 356480
    iget-object v0, p0, LX/20z;->m:LX/1zt;

    .line 356481
    sget-object v1, LX/1zt;->c:LX/1zt;

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 356482
    if-nez v0, :cond_0

    .line 356483
    iget-object v0, p0, LX/20z;->m:LX/1zt;

    iput-object v0, p0, LX/20z;->l:LX/1zt;

    .line 356484
    :cond_0
    sget-object v0, LX/210;->ABOVE_FOOTER:LX/210;

    iput-object v0, p0, LX/20z;->i:LX/210;

    .line 356485
    sget-object v0, LX/211;->UNKNOWN:LX/211;

    iput-object v0, p0, LX/20z;->j:LX/211;

    .line 356486
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/20z;->n:LX/1zt;

    .line 356487
    sget-object v0, LX/1zt;->c:LX/1zt;

    iput-object v0, p0, LX/20z;->m:LX/1zt;

    .line 356488
    sget-object v0, LX/20M;->NONE:LX/20M;

    iput-object v0, p0, LX/20z;->k:LX/20M;

    .line 356489
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1zt;)V
    .locals 0

    .prologue
    .line 356490
    if-nez p1, :cond_0

    sget-object p1, LX/1zt;->d:LX/1zt;

    :cond_0
    iput-object p1, p0, LX/20z;->l:LX/1zt;

    .line 356491
    return-void
.end method
