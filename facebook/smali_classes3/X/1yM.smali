.class public final LX/1yM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 350532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;LX/1yN;)LX/1yL;
    .locals 3

    .prologue
    .line 350533
    iget v0, p1, LX/1yN;->a:I

    move v0, v0

    .line 350534
    if-ltz v0, :cond_0

    .line 350535
    iget v0, p1, LX/1yN;->a:I

    move v0, v0

    .line 350536
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 350537
    iget v0, p1, LX/1yN;->b:I

    move v0, v0

    .line 350538
    if-ltz v0, :cond_0

    .line 350539
    iget v0, p1, LX/1yN;->a:I

    move v0, v0

    .line 350540
    iget v1, p1, LX/1yN;->b:I

    move v1, v1

    .line 350541
    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 350542
    :cond_0
    new-instance v0, LX/47A;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Range "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out of bounds for string: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47A;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350543
    :cond_1
    const/4 v0, 0x0

    .line 350544
    iget v1, p1, LX/1yN;->a:I

    move v1, v1

    .line 350545
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    .line 350546
    iget v1, p1, LX/1yN;->a:I

    move v1, v1

    .line 350547
    invoke-virtual {p1}, LX/1yN;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->codePointCount(II)I

    move-result v1

    .line 350548
    new-instance v2, LX/1yL;

    invoke-direct {v2, v0, v1}, LX/1yL;-><init>(II)V

    return-object v2
.end method

.method public static a(Ljava/lang/String;II)LX/1yN;
    .locals 3

    .prologue
    .line 350549
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v0

    .line 350550
    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 350551
    new-instance v2, LX/1yN;

    sub-int/2addr v1, v0

    invoke-direct {v2, v0, v1}, LX/1yN;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 350552
    :catch_0
    new-instance v0, LX/47A;

    invoke-direct {v0}, LX/47A;-><init>()V

    throw v0
.end method

.method public static a(Ljava/lang/String;LX/1yL;)LX/1yN;
    .locals 2

    .prologue
    .line 350553
    iget v0, p1, LX/1yL;->a:I

    move v0, v0

    .line 350554
    iget v1, p1, LX/1yL;->b:I

    move v1, v1

    .line 350555
    invoke-static {p0, v0, v1}, LX/1yM;->a(Ljava/lang/String;II)LX/1yN;

    move-result-object v0

    return-object v0
.end method
