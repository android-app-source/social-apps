.class public LX/20l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/20l;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0tH;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356007
    iput-object p1, p0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 356008
    iput-object p2, p0, LX/20l;->b:LX/0tH;

    .line 356009
    return-void
.end method

.method public static a(LX/0QB;)LX/20l;
    .locals 5

    .prologue
    .line 355993
    sget-object v0, LX/20l;->c:LX/20l;

    if-nez v0, :cond_1

    .line 355994
    const-class v1, LX/20l;

    monitor-enter v1

    .line 355995
    :try_start_0
    sget-object v0, LX/20l;->c:LX/20l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 355996
    if-eqz v2, :cond_0

    .line 355997
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 355998
    new-instance p0, LX/20l;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v4

    check-cast v4, LX/0tH;

    invoke-direct {p0, v3, v4}, LX/20l;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tH;)V

    .line 355999
    move-object v0, p0

    .line 356000
    sput-object v0, LX/20l;->c:LX/20l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356001
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 356002
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 356003
    :cond_1
    sget-object v0, LX/20l;->c:LX/20l;

    return-object v0

    .line 356004
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 356005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 355992
    iget-object v0, p0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1zs;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 356010
    iget-object v0, p0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1zs;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 356011
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 355991
    iget-object v0, p0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1zs;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 355990
    iget-object v0, p0, LX/20l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1zs;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
