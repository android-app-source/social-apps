.class public final LX/22X;
.super LX/1LD;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/Integer;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:LX/22Q;

.field public final synthetic d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:LX/22W;


# direct methods
.method public constructor <init>(LX/22W;Ljava/lang/Integer;LX/1Pq;LX/22Q;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 359871
    iput-object p1, p0, LX/22X;->e:LX/22W;

    iput-object p2, p0, LX/22X;->a:Ljava/lang/Integer;

    iput-object p3, p0, LX/22X;->b:LX/1Pq;

    iput-object p4, p0, LX/22X;->c:LX/22Q;

    iput-object p5, p0, LX/22X;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, LX/1LD;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 359872
    check-cast p1, LX/1ep;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 359873
    iget-object v0, p0, LX/22X;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/22X;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p1, LX/1ep;->b:I

    if-eq v0, v1, :cond_1

    .line 359874
    :cond_0
    :goto_0
    return-void

    .line 359875
    :cond_1
    iget-object v0, p0, LX/22X;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    iget-object v4, p0, LX/22X;->c:LX/22Q;

    iget-object v1, p0, LX/22X;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 359876
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v5

    .line 359877
    check-cast v1, LX/0jW;

    invoke-interface {v0, v4, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 359878
    iget-boolean v1, p1, LX/1ep;->a:Z

    if-ne v1, v0, :cond_0

    .line 359879
    iget-object v0, p0, LX/22X;->b:LX/1Pq;

    check-cast v0, LX/1Pr;

    iget-object v4, p0, LX/22X;->c:LX/22Q;

    iget-boolean v1, p1, LX/1ep;->a:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v4, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 359880
    iget-object v0, p0, LX/22X;->b:LX/1Pq;

    new-array v1, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/22X;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 359881
    goto :goto_1
.end method
