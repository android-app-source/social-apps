.class public final enum LX/210;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/210;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/210;

.field public static final enum ABOVE_FOOTER:LX/210;

.field public static final enum BELOW_FOOTER:LX/210;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 356495
    new-instance v0, LX/210;

    const-string v1, "ABOVE_FOOTER"

    invoke-direct {v0, v1, v2}, LX/210;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/210;->ABOVE_FOOTER:LX/210;

    .line 356496
    new-instance v0, LX/210;

    const-string v1, "BELOW_FOOTER"

    invoke-direct {v0, v1, v3}, LX/210;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/210;->BELOW_FOOTER:LX/210;

    .line 356497
    const/4 v0, 0x2

    new-array v0, v0, [LX/210;

    sget-object v1, LX/210;->ABOVE_FOOTER:LX/210;

    aput-object v1, v0, v2

    sget-object v1, LX/210;->BELOW_FOOTER:LX/210;

    aput-object v1, v0, v3

    sput-object v0, LX/210;->$VALUES:[LX/210;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 356492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/210;
    .locals 1

    .prologue
    .line 356494
    const-class v0, LX/210;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/210;

    return-object v0
.end method

.method public static values()[LX/210;
    .locals 1

    .prologue
    .line 356493
    sget-object v0, LX/210;->$VALUES:[LX/210;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/210;

    return-object v0
.end method
