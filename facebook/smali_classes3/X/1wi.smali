.class public LX/1wi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/app/NotificationManager;

.field public final c:Landroid/content/res/Resources;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/selfupdate2/AppNameForSelfUpdate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346702
    iput-object p1, p0, LX/1wi;->a:Landroid/content/Context;

    .line 346703
    iput-object p2, p0, LX/1wi;->b:Landroid/app/NotificationManager;

    .line 346704
    iput-object p3, p0, LX/1wi;->c:Landroid/content/res/Resources;

    .line 346705
    iput-object p4, p0, LX/1wi;->d:Ljava/lang/String;

    .line 346706
    return-void
.end method

.method public static b(LX/0QB;)LX/1wi;
    .locals 5

    .prologue
    .line 346707
    new-instance v4, LX/1wi;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1wj;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1wi;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;Landroid/content/res/Resources;Ljava/lang/String;)V

    .line 346708
    return-object v4
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 346709
    iget-object v0, p0, LX/1wi;->b:Landroid/app/NotificationManager;

    const-string v1, "selfupdate_notification"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 346710
    return-void
.end method
