.class public final LX/21A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/162;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/21B;

.field public f:LX/21C;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:LX/21D;

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 356734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356735
    sget-object v0, LX/21B;->UNKNOWN:LX/21B;

    iput-object v0, p0, LX/21A;->e:LX/21B;

    .line 356736
    sget-object v0, LX/21C;->UNKNOWN:LX/21C;

    iput-object v0, p0, LX/21A;->f:LX/21C;

    .line 356737
    sget-object v0, LX/21D;->INVALID:LX/21D;

    iput-object v0, p0, LX/21A;->i:LX/21D;

    .line 356738
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/21A;->j:J

    return-void
.end method

.method public static a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;
    .locals 6

    .prologue
    .line 356711
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    .line 356712
    if-eqz p0, :cond_0

    .line 356713
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    .line 356714
    iput-object v1, v0, LX/21A;->a:LX/162;

    .line 356715
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 356716
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 356717
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 356718
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 356719
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 356720
    iput-object v1, v0, LX/21A;->d:Ljava/lang/String;

    .line 356721
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->e:LX/21B;

    move-object v1, v1

    .line 356722
    iput-object v1, v0, LX/21A;->e:LX/21B;

    .line 356723
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f:LX/21C;

    move-object v1, v1

    .line 356724
    iput-object v1, v0, LX/21A;->f:LX/21C;

    .line 356725
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->g:Ljava/lang/String;

    move-object v1, v1

    .line 356726
    iput-object v1, v0, LX/21A;->g:Ljava/lang/String;

    .line 356727
    iget-boolean v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->h:Z

    move v1, v1

    .line 356728
    iput-boolean v1, v0, LX/21A;->h:Z

    .line 356729
    iget-object v1, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->i:LX/21D;

    move-object v1, v1

    .line 356730
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 356731
    iget-wide v4, p0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v4

    .line 356732
    iput-wide v2, v0, LX/21A;->j:J

    .line 356733
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/162;)LX/21A;
    .locals 0

    .prologue
    .line 356709
    iput-object p1, p0, LX/21A;->a:LX/162;

    .line 356710
    return-object p0
.end method

.method public final a(LX/21C;)LX/21A;
    .locals 0

    .prologue
    .line 356707
    iput-object p1, p0, LX/21A;->f:LX/21C;

    .line 356708
    return-object p0
.end method

.method public final a(LX/21D;)LX/21A;
    .locals 0

    .prologue
    .line 356739
    iput-object p1, p0, LX/21A;->i:LX/21D;

    .line 356740
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/21A;
    .locals 0

    .prologue
    .line 356705
    iput-object p1, p0, LX/21A;->b:Ljava/lang/String;

    .line 356706
    return-object p0
.end method

.method public final a(Z)LX/21A;
    .locals 0

    .prologue
    .line 356703
    iput-boolean p1, p0, LX/21A;->h:Z

    .line 356704
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/21A;
    .locals 0

    .prologue
    .line 356700
    iput-object p1, p0, LX/21A;->c:Ljava/lang/String;

    .line 356701
    return-object p0
.end method

.method public final b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .locals 2

    .prologue
    .line 356702
    new-instance v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-direct {v0, p0}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/21A;)V

    return-object v0
.end method
