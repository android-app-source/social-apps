.class public final LX/21K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/21A;

.field public final synthetic d:LX/21H;

.field public final synthetic e:Z

.field public final synthetic f:LX/1Po;

.field public final synthetic g:LX/20g;


# direct methods
.method public constructor <init>(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/21A;LX/21H;ZLX/1Po;)V
    .locals 0

    .prologue
    .line 357026
    iput-object p1, p0, LX/21K;->g:LX/20g;

    iput-object p2, p0, LX/21K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/21K;->b:Ljava/lang/String;

    iput-object p4, p0, LX/21K;->c:LX/21A;

    iput-object p5, p0, LX/21K;->d:LX/21H;

    iput-boolean p6, p0, LX/21K;->e:Z

    iput-object p7, p0, LX/21K;->f:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 7

    .prologue
    .line 357027
    iget-object v0, p0, LX/21K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357028
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 357029
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 357030
    sget-object v1, LX/C45;->a:[I

    invoke-virtual {p2}, LX/20X;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 357031
    :cond_0
    :goto_0
    return-void

    .line 357032
    :pswitch_0
    iget-object v1, p0, LX/21K;->g:LX/20g;

    iget-object v1, v1, LX/20g;->p:LX/1WR;

    invoke-virtual {v1, v0}, LX/1WR;->f(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 357033
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/21K;->g:LX/20g;

    iget-object v1, v1, LX/20g;->o:LX/1zf;

    invoke-virtual {v1}, LX/1zf;->e()LX/1zt;

    move-result-object v1

    .line 357034
    :goto_1
    iget-object v2, p0, LX/21K;->g:LX/20g;

    .line 357035
    iget-object v3, v2, LX/20g;->j:LX/0Ot;

    invoke-static {v3, v1}, LX/21N;->a(LX/0Ot;LX/1zt;)V

    .line 357036
    iget-object v2, p0, LX/21K;->g:LX/20g;

    iget-object v3, p0, LX/21K;->b:Ljava/lang/String;

    invoke-static {v2, p1, v1, v3, v0}, LX/20g;->b(LX/20g;Landroid/view/View;LX/1zt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    .line 357037
    iget-object v3, p0, LX/21K;->g:LX/20g;

    iget-object v3, v3, LX/20g;->b:LX/1EQ;

    iget-object v4, p0, LX/21K;->c:LX/21A;

    invoke-virtual {v3, v0, v4}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 357038
    iget-object v3, p0, LX/21K;->g:LX/20g;

    iget-object v3, v3, LX/20g;->d:LX/20h;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    iget-object v5, p0, LX/21K;->c:LX/21A;

    invoke-virtual {v5}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v5

    new-instance v6, LX/C44;

    invoke-direct {v6, p0, p1}, LX/C44;-><init>(LX/21K;Landroid/view/View;)V

    invoke-virtual {v3, v4, v1, v5, v6}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    .line 357039
    iget-object v3, p0, LX/21K;->g:LX/20g;

    iget-object v3, v3, LX/20g;->e:LX/0bH;

    new-instance v4, LX/1Zf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0, v1, v2}, LX/1Zf;-><init>(Ljava/lang/String;Ljava/lang/String;LX/1zt;Z)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 357040
    iget-object v0, p0, LX/21K;->d:LX/21H;

    if-eqz v0, :cond_1

    .line 357041
    iget-object v0, p0, LX/21K;->d:LX/21H;

    invoke-virtual {v0, v1}, LX/21H;->b(LX/1zt;)V

    .line 357042
    :cond_1
    instance-of v0, p1, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 357043
    check-cast v0, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/base/footer/ui/FooterLikeButton;->setReaction(LX/1zt;)V

    .line 357044
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 357045
    :cond_2
    sget-object v1, LX/1zt;->c:LX/1zt;

    goto :goto_1

    .line 357046
    :pswitch_1
    iget-boolean v4, p0, LX/21K;->e:Z

    .line 357047
    iget-object v2, p0, LX/21K;->f:LX/1Po;

    .line 357048
    const/4 v3, 0x0

    .line 357049
    instance-of v5, v2, LX/1Pe;

    if-nez v5, :cond_5

    .line 357050
    :cond_3
    :goto_2
    move v3, v3

    .line 357051
    move v0, v3

    .line 357052
    if-eqz v0, :cond_4

    .line 357053
    const/4 v4, 0x0

    .line 357054
    :cond_4
    iget-object v1, p0, LX/21K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/21K;->f:LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v3, v0, LX/20g;->f:LX/20n;

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v5, v0, LX/20g;->p:LX/1WR;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/212;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/20n;ZLX/1WR;)V

    .line 357055
    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v0, v0, LX/20g;->u:LX/20q;

    invoke-virtual {v0}, LX/20q;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v0, v0, LX/20g;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9D6;

    invoke-virtual {v0}, LX/9D6;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 357056
    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v0, v0, LX/20g;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9FJ;

    .line 357057
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/9FJ;->b:Ljava/lang/ref/WeakReference;

    .line 357058
    iget-object v1, v0, LX/9FJ;->c:LX/1AM;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 357059
    goto/16 :goto_0

    .line 357060
    :pswitch_2
    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v0, v0, LX/20g;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v2, v0, LX/20g;->h:LX/20p;

    iget-object v3, p0, LX/21K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/21K;->f:LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v5, v0, LX/20g;->e:LX/0bH;

    iget-object v0, p0, LX/21K;->g:LX/20g;

    iget-object v6, v0, LX/20g;->p:LX/1WR;

    move-object v0, p1

    invoke-static/range {v0 .. v6}, LX/212;->a(Landroid/view/View;LX/0gh;LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;LX/0bH;LX/1WR;)V

    goto/16 :goto_0

    .line 357061
    :cond_5
    check-cast v2, LX/1Pe;

    .line 357062
    invoke-static {v0}, LX/14w;->q(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 357063
    invoke-interface {v2, v5}, LX/1Pe;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 357064
    invoke-interface {v2, v5}, LX/1Pe;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 357065
    const/4 v3, 0x1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
