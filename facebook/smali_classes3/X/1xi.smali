.class public LX/1xi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1xi;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 349317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349318
    iput-object v0, p0, LX/1xi;->b:Ljava/lang/Integer;

    .line 349319
    iput-object v0, p0, LX/1xi;->c:Ljava/lang/Integer;

    .line 349320
    iput-object p1, p0, LX/1xi;->a:LX/0ad;

    .line 349321
    return-void
.end method

.method public static a(LX/0QB;)LX/1xi;
    .locals 4

    .prologue
    .line 349303
    sget-object v0, LX/1xi;->d:LX/1xi;

    if-nez v0, :cond_1

    .line 349304
    const-class v1, LX/1xi;

    monitor-enter v1

    .line 349305
    :try_start_0
    sget-object v0, LX/1xi;->d:LX/1xi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 349306
    if-eqz v2, :cond_0

    .line 349307
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 349308
    new-instance p0, LX/1xi;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/1xi;-><init>(LX/0ad;)V

    .line 349309
    move-object v0, p0

    .line 349310
    sput-object v0, LX/1xi;->d:LX/1xi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349311
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 349312
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349313
    :cond_1
    sget-object v0, LX/1xi;->d:LX/1xi;

    return-object v0

    .line 349314
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 349315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 349316
    iget-object v0, p0, LX/1xi;->a:LX/0ad;

    sget-short v1, LX/0ob;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
