.class public LX/1zU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/util/regex/Pattern;

.field public static final d:Lcom/facebook/ui/emoji/model/Emoji;

.field private static volatile h:LX/1zU;


# instance fields
.field private final e:LX/1zD;

.field public final f:LX/1zH;

.field public g:Ljava/util/regex/Pattern;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 353532
    new-instance v0, Ljava/lang/String;

    const v1, 0x1f44d

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, LX/1zU;->a:Ljava/lang/String;

    .line 353533
    new-instance v0, Ljava/lang/String;

    const/high16 v1, 0xf0000

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, LX/1zU;->b:Ljava/lang/String;

    .line 353534
    new-instance v0, Ljava/lang/String;

    const/16 v1, 0x2764

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/1zU;->c:Ljava/util/regex/Pattern;

    .line 353535
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v1, 0x7f020602

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    sput-object v0, LX/1zU;->d:Lcom/facebook/ui/emoji/model/Emoji;

    return-void
.end method

.method public constructor <init>(LX/1zD;LX/1zH;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353528
    const-string v0, "Binding for emojisData not defined."

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353529
    iput-object p1, p0, LX/1zU;->e:LX/1zD;

    .line 353530
    iput-object p2, p0, LX/1zU;->f:LX/1zH;

    .line 353531
    return-void
.end method

.method public static a(LX/0QB;)LX/1zU;
    .locals 5

    .prologue
    .line 353514
    sget-object v0, LX/1zU;->h:LX/1zU;

    if-nez v0, :cond_1

    .line 353515
    const-class v1, LX/1zU;

    monitor-enter v1

    .line 353516
    :try_start_0
    sget-object v0, LX/1zU;->h:LX/1zU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353517
    if-eqz v2, :cond_0

    .line 353518
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353519
    new-instance p0, LX/1zU;

    invoke-static {v0}, LX/1zD;->a(LX/0QB;)LX/1zD;

    move-result-object v3

    check-cast v3, LX/1zD;

    invoke-static {v0}, LX/1zF;->a(LX/0QB;)LX/1zH;

    move-result-object v4

    check-cast v4, LX/1zH;

    invoke-direct {p0, v3, v4}, LX/1zU;-><init>(LX/1zD;LX/1zH;)V

    .line 353520
    move-object v0, p0

    .line 353521
    sput-object v0, LX/1zU;->h:LX/1zU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353522
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353523
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353524
    :cond_1
    sget-object v0, LX/1zU;->h:LX/1zU;

    return-object v0

    .line 353525
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a([CILX/1zT;)Z
    .locals 7

    .prologue
    const/16 v6, 0x6f

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x3

    const/4 v1, 0x1

    .line 353366
    array-length v2, p0

    if-lt p1, v2, :cond_1

    .line 353367
    :cond_0
    :goto_0
    return v0

    .line 353368
    :cond_1
    aget-char v2, p0, p1

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 353369
    :sswitch_0
    add-int/lit8 v2, p1, 0x4

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x6c

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x74

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x3b

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x4

    aget-char v2, p0, v2

    const/16 v3, 0x33

    if-ne v2, v3, :cond_0

    .line 353370
    const/16 v0, 0x2764

    iput v0, p2, LX/1zT;->a:I

    .line 353371
    const/4 v0, 0x5

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353372
    goto :goto_0

    .line 353373
    :sswitch_1
    add-int/lit8 v2, p1, 0x5

    array-length v3, p0

    if-ge v2, v3, :cond_3

    add-int/lit8 v2, p1, 0x5

    aget-char v2, p0, v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_3

    .line 353374
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x70

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_2

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_2

    add-int/lit8 v2, p1, 0x4

    aget-char v2, p0, v2

    const/16 v3, 0x70

    if-ne v2, v3, :cond_2

    .line 353375
    const v0, 0x1f4a9

    iput v0, p2, LX/1zT;->a:I

    .line 353376
    const/4 v0, 0x6

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353377
    goto :goto_0

    .line 353378
    :cond_2
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x6c

    if-ne v2, v3, :cond_3

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x69

    if-ne v2, v3, :cond_3

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x6b

    if-ne v2, v3, :cond_3

    add-int/lit8 v2, p1, 0x4

    aget-char v2, p0, v2

    const/16 v3, 0x65

    if-ne v2, v3, :cond_3

    .line 353379
    const v0, 0x1f44d

    iput v0, p2, LX/1zT;->a:I

    .line 353380
    const/4 v0, 0x6

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353381
    goto/16 :goto_0

    .line 353382
    :cond_3
    add-int/lit8 v2, p1, 0x1

    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 353383
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    sparse-switch v2, :sswitch_data_1

    .line 353384
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 353385
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x27

    if-ne v2, v3, :cond_4

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_4

    .line 353386
    const v0, 0x1f622

    iput v0, p2, LX/1zT;->a:I

    .line 353387
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353388
    goto/16 :goto_0

    .line 353389
    :sswitch_2
    const v0, 0x1f615

    iput v0, p2, LX/1zT;->a:I

    .line 353390
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353391
    goto/16 :goto_0

    .line 353392
    :sswitch_3
    const v0, 0x1f632

    iput v0, p2, LX/1zT;->a:I

    .line 353393
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353394
    goto/16 :goto_0

    .line 353395
    :sswitch_4
    const v0, 0x1f61c

    iput v0, p2, LX/1zT;->a:I

    .line 353396
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353397
    goto/16 :goto_0

    .line 353398
    :sswitch_5
    const v0, 0x1f603

    iput v0, p2, LX/1zT;->a:I

    .line 353399
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353400
    goto/16 :goto_0

    .line 353401
    :sswitch_6
    const v0, 0x1f61e

    iput v0, p2, LX/1zT;->a:I

    .line 353402
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353403
    goto/16 :goto_0

    .line 353404
    :sswitch_7
    const v0, 0x1f60a

    iput v0, p2, LX/1zT;->a:I

    .line 353405
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353406
    goto/16 :goto_0

    .line 353407
    :sswitch_8
    const v0, 0x1f618

    iput v0, p2, LX/1zT;->a:I

    .line 353408
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353409
    goto/16 :goto_0

    .line 353410
    :sswitch_9
    const v0, 0x1f615

    iput v0, p2, LX/1zT;->a:I

    .line 353411
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353412
    goto/16 :goto_0

    .line 353413
    :cond_4
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_0

    .line 353414
    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    sparse-switch v2, :sswitch_data_2

    goto/16 :goto_0

    .line 353415
    :sswitch_a
    const v0, 0x1f61e

    iput v0, p2, LX/1zT;->a:I

    .line 353416
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353417
    goto/16 :goto_0

    .line 353418
    :sswitch_b
    const v0, 0x1f615

    iput v0, p2, LX/1zT;->a:I

    .line 353419
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353420
    goto/16 :goto_0

    .line 353421
    :sswitch_c
    const v0, 0x1f632

    iput v0, p2, LX/1zT;->a:I

    .line 353422
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353423
    goto/16 :goto_0

    .line 353424
    :sswitch_d
    const v0, 0x1f61c

    iput v0, p2, LX/1zT;->a:I

    .line 353425
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353426
    goto/16 :goto_0

    .line 353427
    :sswitch_e
    const v0, 0x1f603

    iput v0, p2, LX/1zT;->a:I

    .line 353428
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353429
    goto/16 :goto_0

    .line 353430
    :sswitch_f
    const v0, 0x1f60a

    iput v0, p2, LX/1zT;->a:I

    .line 353431
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353432
    goto/16 :goto_0

    .line 353433
    :sswitch_10
    const v0, 0x1f618

    iput v0, p2, LX/1zT;->a:I

    .line 353434
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353435
    goto/16 :goto_0

    .line 353436
    :sswitch_11
    const v0, 0x1f615

    iput v0, p2, LX/1zT;->a:I

    .line 353437
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353438
    goto/16 :goto_0

    .line 353439
    :sswitch_12
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_7

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x3c

    if-ne v2, v3, :cond_5

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_6

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x5f

    if-eq v2, v3, :cond_6

    :cond_5
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_7

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x4f

    if-eq v2, v3, :cond_6

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    if-eq v2, v6, :cond_6

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_7

    .line 353440
    :cond_6
    const v0, 0x1f620

    iput v0, p2, LX/1zT;->a:I

    .line 353441
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353442
    goto/16 :goto_0

    .line 353443
    :cond_7
    add-int/lit8 v2, p1, 0x3

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x4f

    if-eq v2, v3, :cond_8

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    if-eq v2, v6, :cond_8

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_0

    .line 353444
    :cond_8
    const v0, 0x1f620

    iput v0, p2, LX/1zT;->a:I

    .line 353445
    const/4 v0, 0x4

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353446
    goto/16 :goto_0

    .line 353447
    :sswitch_13
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_c

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_c

    .line 353448
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x4e

    if-eq v2, v3, :cond_9

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x6e

    if-ne v2, v3, :cond_a

    .line 353449
    :cond_9
    const v0, 0x1f44e

    iput v0, p2, LX/1zT;->a:I

    .line 353450
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353451
    goto/16 :goto_0

    .line 353452
    :cond_a
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x59

    if-eq v2, v3, :cond_b

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x79

    if-ne v2, v3, :cond_c

    .line 353453
    :cond_b
    const/high16 v0, 0xf0000

    iput v0, p2, LX/1zT;->a:I

    .line 353454
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353455
    goto/16 :goto_0

    .line 353456
    :cond_c
    add-int/lit8 v2, p1, 0x4

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x70

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_0

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_0

    add-int/lit8 v2, p1, 0x4

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_0

    .line 353457
    const v0, 0x1f4a9

    iput v0, p2, LX/1zT;->a:I

    .line 353458
    const/4 v0, 0x5

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353459
    goto/16 :goto_0

    .line 353460
    :sswitch_14
    add-int/lit8 v2, p1, 0x1

    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 353461
    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    sparse-switch v2, :sswitch_data_3

    goto/16 :goto_0

    .line 353462
    :sswitch_15
    const v0, 0x1f61e

    iput v0, p2, LX/1zT;->a:I

    .line 353463
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353464
    goto/16 :goto_0

    .line 353465
    :sswitch_16
    const v0, 0x1f603

    iput v0, p2, LX/1zT;->a:I

    .line 353466
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353467
    goto/16 :goto_0

    .line 353468
    :sswitch_17
    const v0, 0x1f61c

    iput v0, p2, LX/1zT;->a:I

    .line 353469
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353470
    goto/16 :goto_0

    .line 353471
    :sswitch_18
    const v0, 0x1f60a

    iput v0, p2, LX/1zT;->a:I

    .line 353472
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353473
    goto/16 :goto_0

    .line 353474
    :sswitch_19
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_d

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_d

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_d

    .line 353475
    const v0, 0x1f47f

    iput v0, p2, LX/1zT;->a:I

    .line 353476
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353477
    goto/16 :goto_0

    .line 353478
    :cond_d
    add-int/lit8 v2, p1, 0x3

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x3a

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_0

    .line 353479
    const v0, 0x1f47f

    iput v0, p2, LX/1zT;->a:I

    .line 353480
    const/4 v0, 0x4

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353481
    goto/16 :goto_0

    .line 353482
    :sswitch_1a
    add-int/lit8 v2, p1, 0x1

    array-length v3, p0

    if-ge v2, v3, :cond_e

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x33

    if-ne v2, v3, :cond_e

    .line 353483
    const/16 v0, 0x2764

    iput v0, p2, LX/1zT;->a:I

    .line 353484
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353485
    goto/16 :goto_0

    .line 353486
    :cond_e
    add-int/lit8 v2, p1, 0x3

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x22

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x3

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_0

    .line 353487
    const v0, 0x1f427

    iput v0, p2, LX/1zT;->a:I

    .line 353488
    const/4 v0, 0x4

    iput v0, p2, LX/1zT;->b:I

    move v0, v1

    .line 353489
    goto/16 :goto_0

    .line 353490
    :sswitch_1b
    add-int/lit8 v2, p1, 0x1

    array-length v3, p0

    if-ge v2, v3, :cond_f

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_f

    .line 353491
    const v0, 0x1f609

    iput v0, p2, LX/1zT;->a:I

    .line 353492
    iput v5, p2, LX/1zT;->b:I

    move v0, v1

    .line 353493
    goto/16 :goto_0

    .line 353494
    :cond_f
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_0

    .line 353495
    const v0, 0x1f609

    iput v0, p2, LX/1zT;->a:I

    .line 353496
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353497
    goto/16 :goto_0

    .line 353498
    :sswitch_1c
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x4f

    if-eq v2, v3, :cond_10

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x30

    if-ne v2, v3, :cond_0

    .line 353499
    :cond_10
    const v0, 0xf0001

    iput v0, p2, LX/1zT;->a:I

    .line 353500
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353501
    goto/16 :goto_0

    .line 353502
    :sswitch_1d
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_0

    .line 353503
    const v0, 0xf0002

    iput v0, p2, LX/1zT;->a:I

    .line 353504
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353505
    goto/16 :goto_0

    .line 353506
    :sswitch_1e
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x5f

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    const/16 v3, 0x5e

    if-ne v2, v3, :cond_0

    .line 353507
    const v0, 0x1f604

    iput v0, p2, LX/1zT;->a:I

    .line 353508
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353509
    goto/16 :goto_0

    .line 353510
    :sswitch_1f
    add-int/lit8 v2, p1, 0x2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x1

    aget-char v2, p0, v2

    const/16 v3, 0x2e

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, p1, 0x2

    aget-char v2, p0, v2

    if-ne v2, v6, :cond_0

    .line 353511
    const v0, 0xf0002

    iput v0, p2, LX/1zT;->a:I

    .line 353512
    iput v4, p2, LX/1zT;->b:I

    move v0, v1

    .line 353513
    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x26 -> :sswitch_0
        0x28 -> :sswitch_13
        0x30 -> :sswitch_1f
        0x33 -> :sswitch_19
        0x3a -> :sswitch_1
        0x3b -> :sswitch_1b
        0x3c -> :sswitch_1a
        0x3d -> :sswitch_14
        0x3e -> :sswitch_12
        0x4f -> :sswitch_1d
        0x5e -> :sswitch_1e
        0x6f -> :sswitch_1c
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x28 -> :sswitch_6
        0x29 -> :sswitch_7
        0x2a -> :sswitch_8
        0x2f -> :sswitch_2
        0x44 -> :sswitch_5
        0x4f -> :sswitch_3
        0x50 -> :sswitch_4
        0x5b -> :sswitch_6
        0x5c -> :sswitch_9
        0x5d -> :sswitch_7
        0x6f -> :sswitch_3
        0x70 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x28 -> :sswitch_a
        0x29 -> :sswitch_f
        0x2a -> :sswitch_10
        0x2f -> :sswitch_b
        0x44 -> :sswitch_e
        0x4f -> :sswitch_c
        0x50 -> :sswitch_d
        0x5c -> :sswitch_11
        0x6f -> :sswitch_c
        0x70 -> :sswitch_d
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x28 -> :sswitch_15
        0x29 -> :sswitch_18
        0x44 -> :sswitch_16
        0x50 -> :sswitch_17
    .end sparse-switch
.end method

.method private static b(II)I
    .locals 0

    .prologue
    .line 353363
    packed-switch p0, :pswitch_data_0

    .line 353364
    :goto_0
    return p1

    .line 353365
    :pswitch_0
    const p1, 0x1f493

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1f495
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/1zU;Ljava/lang/String;)I
    .locals 8

    .prologue
    .line 353304
    const/4 v1, -0x1

    const v3, 0x1f61e

    const v6, 0x1f615

    const v2, 0x1f60a

    const v4, 0x1f61c

    const v5, 0x1f620

    .line 353305
    const/4 v7, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v7, :pswitch_data_0

    .line 353306
    :goto_1
    move v0, v1

    .line 353307
    return v0

    .line 353308
    :sswitch_0
    const-string p0, ":-)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x0

    goto :goto_0

    :sswitch_1
    const-string p0, ":)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :sswitch_2
    const-string p0, ":]"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x2

    goto :goto_0

    :sswitch_3
    const-string p0, "=)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x3

    goto :goto_0

    :sswitch_4
    const-string p0, ":-("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x4

    goto :goto_0

    :sswitch_5
    const-string p0, ":("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x5

    goto :goto_0

    :sswitch_6
    const-string p0, ":["

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x6

    goto :goto_0

    :sswitch_7
    const-string p0, "=("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v7, 0x7

    goto :goto_0

    :sswitch_8
    const-string p0, ":-P"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x8

    goto :goto_0

    :sswitch_9
    const-string p0, ":P"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x9

    goto :goto_0

    :sswitch_a
    const-string p0, ":-p"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xa

    goto :goto_0

    :sswitch_b
    const-string p0, ":p"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xb

    goto :goto_0

    :sswitch_c
    const-string p0, "=P"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string p0, ":-D"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string p0, ":D"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string p0, "=D"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string p0, ":-O"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string p0, ":O"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string p0, ":-o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string p0, ":o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string p0, ";-)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string p0, ";)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string p0, ">:("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string p0, ">:-("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string p0, ":/"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string p0, ":-/"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string p0, ":\\"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string p0, ":-\\"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string p0, ":\'("

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string p0, "3:)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string p0, "3:-)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string p0, ":-*"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string p0, ":*"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string p0, "<3"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string p0, "&lt;3"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string p0, "^_^"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string p0, "o.O"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string p0, "O.o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string p0, "o.0"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string p0, "0.o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string p0, ">:O"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string p0, ">:-O"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string p0, ">:o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string p0, ">:-o"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string p0, ">_<"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string p0, ">.<"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string p0, "<(\")"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string p0, ":like:"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string p0, ":poop:"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string p0, "(y)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x31

    goto/16 :goto_0

    :sswitch_32
    const-string p0, "(Y)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x32

    goto/16 :goto_0

    :sswitch_33
    const-string p0, "(n)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x33

    goto/16 :goto_0

    :sswitch_34
    const-string p0, "(N)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x34

    goto/16 :goto_0

    :sswitch_35
    const-string p0, "(poo)"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 v7, 0x35

    goto/16 :goto_0

    :pswitch_0
    move v1, v2

    .line 353309
    goto/16 :goto_1

    :pswitch_1
    move v1, v2

    .line 353310
    goto/16 :goto_1

    :pswitch_2
    move v1, v2

    .line 353311
    goto/16 :goto_1

    :pswitch_3
    move v1, v2

    .line 353312
    goto/16 :goto_1

    :pswitch_4
    move v1, v3

    .line 353313
    goto/16 :goto_1

    :pswitch_5
    move v1, v3

    .line 353314
    goto/16 :goto_1

    :pswitch_6
    move v1, v3

    .line 353315
    goto/16 :goto_1

    :pswitch_7
    move v1, v3

    .line 353316
    goto/16 :goto_1

    :pswitch_8
    move v1, v4

    .line 353317
    goto/16 :goto_1

    :pswitch_9
    move v1, v4

    .line 353318
    goto/16 :goto_1

    :pswitch_a
    move v1, v4

    .line 353319
    goto/16 :goto_1

    :pswitch_b
    move v1, v4

    .line 353320
    goto/16 :goto_1

    :pswitch_c
    move v1, v4

    .line 353321
    goto/16 :goto_1

    .line 353322
    :pswitch_d
    const v1, 0x1f603

    goto/16 :goto_1

    .line 353323
    :pswitch_e
    const v1, 0x1f603

    goto/16 :goto_1

    .line 353324
    :pswitch_f
    const v1, 0x1f603

    goto/16 :goto_1

    .line 353325
    :pswitch_10
    const v1, 0x1f632

    goto/16 :goto_1

    .line 353326
    :pswitch_11
    const v1, 0x1f632

    goto/16 :goto_1

    .line 353327
    :pswitch_12
    const v1, 0x1f632

    goto/16 :goto_1

    .line 353328
    :pswitch_13
    const v1, 0x1f632

    goto/16 :goto_1

    .line 353329
    :pswitch_14
    const v1, 0x1f609

    goto/16 :goto_1

    .line 353330
    :pswitch_15
    const v1, 0x1f609

    goto/16 :goto_1

    :pswitch_16
    move v1, v5

    .line 353331
    goto/16 :goto_1

    :pswitch_17
    move v1, v5

    .line 353332
    goto/16 :goto_1

    :pswitch_18
    move v1, v6

    .line 353333
    goto/16 :goto_1

    :pswitch_19
    move v1, v6

    .line 353334
    goto/16 :goto_1

    :pswitch_1a
    move v1, v6

    .line 353335
    goto/16 :goto_1

    :pswitch_1b
    move v1, v6

    .line 353336
    goto/16 :goto_1

    .line 353337
    :pswitch_1c
    const v1, 0x1f622

    goto/16 :goto_1

    .line 353338
    :pswitch_1d
    const v1, 0x1f47f

    goto/16 :goto_1

    .line 353339
    :pswitch_1e
    const v1, 0x1f47f

    goto/16 :goto_1

    .line 353340
    :pswitch_1f
    const v1, 0x1f618

    goto/16 :goto_1

    .line 353341
    :pswitch_20
    const v1, 0x1f618

    goto/16 :goto_1

    .line 353342
    :pswitch_21
    const/16 v1, 0x2764

    goto/16 :goto_1

    .line 353343
    :pswitch_22
    const/16 v1, 0x2764

    goto/16 :goto_1

    .line 353344
    :pswitch_23
    const v1, 0x1f604

    goto/16 :goto_1

    .line 353345
    :pswitch_24
    const v1, 0xf0001

    goto/16 :goto_1

    .line 353346
    :pswitch_25
    const v1, 0xf0002

    goto/16 :goto_1

    .line 353347
    :pswitch_26
    const v1, 0xf0001

    goto/16 :goto_1

    .line 353348
    :pswitch_27
    const v1, 0xf0002

    goto/16 :goto_1

    :pswitch_28
    move v1, v5

    .line 353349
    goto/16 :goto_1

    :pswitch_29
    move v1, v5

    .line 353350
    goto/16 :goto_1

    :pswitch_2a
    move v1, v5

    .line 353351
    goto/16 :goto_1

    :pswitch_2b
    move v1, v5

    .line 353352
    goto/16 :goto_1

    :pswitch_2c
    move v1, v5

    .line 353353
    goto/16 :goto_1

    :pswitch_2d
    move v1, v5

    .line 353354
    goto/16 :goto_1

    .line 353355
    :pswitch_2e
    const v1, 0x1f427

    goto/16 :goto_1

    .line 353356
    :pswitch_2f
    const v1, 0x1f44d

    goto/16 :goto_1

    .line 353357
    :pswitch_30
    const v1, 0x1f4a9

    goto/16 :goto_1

    .line 353358
    :pswitch_31
    const/high16 v1, 0xf0000

    goto/16 :goto_1

    .line 353359
    :pswitch_32
    const/high16 v1, 0xf0000

    goto/16 :goto_1

    .line 353360
    :pswitch_33
    const v1, 0x1f44e

    goto/16 :goto_1

    .line 353361
    :pswitch_34
    const v1, 0x1f44e

    goto/16 :goto_1

    .line 353362
    :pswitch_35
    const v1, 0x1f4a9

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x72e -> :sswitch_5
        0x72f -> :sswitch_1
        0x730 -> :sswitch_20
        0x735 -> :sswitch_18
        0x74a -> :sswitch_e
        0x74e -> :sswitch_15
        0x755 -> :sswitch_11
        0x756 -> :sswitch_9
        0x761 -> :sswitch_6
        0x762 -> :sswitch_1a
        0x763 -> :sswitch_2
        0x775 -> :sswitch_13
        0x776 -> :sswitch_b
        0x777 -> :sswitch_21
        0x78b -> :sswitch_7
        0x78c -> :sswitch_3
        0x7a7 -> :sswitch_f
        0x7b3 -> :sswitch_c
        0x9fc3 -> :sswitch_34
        0xa118 -> :sswitch_32
        0xa3a3 -> :sswitch_33
        0xa4f8 -> :sswitch_31
        0xba31 -> :sswitch_27
        0xc6a2 -> :sswitch_1d
        0xde9b -> :sswitch_1c
        0xdf55 -> :sswitch_4
        0xdf56 -> :sswitch_0
        0xdf57 -> :sswitch_1f
        0xdf5c -> :sswitch_19
        0xdf71 -> :sswitch_d
        0xdf7c -> :sswitch_10
        0xdf7d -> :sswitch_8
        0xdf89 -> :sswitch_1b
        0xdf9c -> :sswitch_12
        0xdf9d -> :sswitch_a
        0xe317 -> :sswitch_14
        0xee8c -> :sswitch_2d
        0xefec -> :sswitch_16
        0xf013 -> :sswitch_28
        0xf033 -> :sswitch_2a
        0xf47b -> :sswitch_2c
        0x12e90 -> :sswitch_25
        0x16cbd -> :sswitch_23
        0x1a671 -> :sswitch_26
        0x1a690 -> :sswitch_24
        0x180e43 -> :sswitch_1e
        0x1be0b3 -> :sswitch_2e
        0x1d0e57 -> :sswitch_17
        0x1d0e7e -> :sswitch_29
        0x1d0e9e -> :sswitch_2b
        0x24a5006 -> :sswitch_22
        0x2684401 -> :sswitch_35
        0x691c5789 -> :sswitch_2f
        0x69578020 -> :sswitch_30
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
    .end packed-switch
.end method

.method public static c(LX/1zU;)Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 353253
    const-string p0, "(?:^|\\s|\'|\"|\\.)((\\:\\-\\))|(\\:\\))|(\\:\\])|(=\\))|(\\:\\-\\()|(\\:\\()|(\\:\\[)|(=\\()|(\\:\\-P)|(\\:poop\\:)|(\\:P)|(\\:\\-p)|(\\:p)|(=P)|(\\:\\-D)|(\\:D)|(=D)|(\\:\\-O)|(\\:O)|(\\:\\-o)|(\\:o)|(\\;\\-\\))|(\\;\\))|(>\\:\\()|(>\\:\\-\\()|(\\:/)|(\\:\\-/)|(\\:\\\\)|(\\:\\-\\\\)|(\\:\'\\()|(3\\:\\))|(3\\:\\-\\))|(\\:\\-\\*)|(\\:\\*)|(<3)|(&lt\\;3)|(\\^_\\^)|(o\\.O)|(o\\.0)|(O\\.o)|(0\\.o)|(>\\:O)|(>\\:\\-O)|(>\\:o)|(>\\:\\-o)|(>_<)|(>\\.<)|(<\\(\"\\))|(\\:like\\:)|(\\(y\\))|(\\(Y\\))|(\\(n\\))|(\\(N\\))|(\\(poo\\)))(?:|\'|\"|\\.|,|!|\\?|$)"

    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p0

    move-object v0, p0

    .line 353254
    return-object v0
.end method

.method public static c(I)Z
    .locals 1

    .prologue
    .line 353303
    const v0, 0x1f3fb

    if-lt p0, v0, :cond_0

    const v0, 0x1f3ff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)Lcom/facebook/ui/emoji/model/Emoji;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v5, 0x1f1ff

    const v2, 0x1f1e6

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 353276
    invoke-static {p1, p2}, LX/1zD;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 353277
    :goto_0
    return-object v0

    .line 353278
    :cond_0
    const/16 v0, 0x23

    if-eq p1, v0, :cond_1

    const/16 v0, 0x30

    if-lt p1, v0, :cond_c

    const/16 v0, 0x39

    if-gt p1, v0, :cond_c

    :cond_1
    const/16 v0, 0x20e3

    if-ne p2, v0, :cond_c

    .line 353279
    const/4 v0, 0x1

    .line 353280
    :goto_1
    move v0, v0

    .line 353281
    if-eqz v0, :cond_3

    .line 353282
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, p1, p2, v3}, LX/1zH;->a(III)I

    move-result v2

    .line 353283
    if-ne v2, v3, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v0, v2, p1, p2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    goto :goto_0

    .line 353284
    :cond_3
    if-lt p1, v2, :cond_5

    if-gt p1, v5, :cond_5

    if-lt p2, v2, :cond_5

    if-gt p2, v5, :cond_5

    .line 353285
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, p1, p2, v3}, LX/1zH;->a(III)I

    move-result v2

    .line 353286
    if-ne v2, v3, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v0, v2, p1, p2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    goto :goto_0

    .line 353287
    :cond_5
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, p1, v3}, LX/1zH;->a(II)I

    move-result v0

    .line 353288
    if-eq v0, v3, :cond_6

    move p1, v0

    .line 353289
    :cond_6
    invoke-static {p2}, LX/1zU;->c(I)Z

    move-result v0

    if-nez v0, :cond_7

    const v0, 0xfe0f

    if-ne p2, v0, :cond_8

    .line 353290
    :cond_7
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, p1, p2, v3}, LX/1zH;->a(III)I

    move-result v2

    .line 353291
    if-eq v2, v3, :cond_8

    .line 353292
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v0, v2, p1, p2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    goto :goto_0

    .line 353293
    :cond_8
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, p1, v4, v3}, LX/1zH;->a(III)I

    move-result v0

    .line 353294
    if-ne v0, v3, :cond_b

    .line 353295
    invoke-static {p1, v3}, LX/1zU;->b(II)I

    move-result v2

    .line 353296
    if-eq v2, v3, :cond_b

    .line 353297
    iget-object v0, p0, LX/1zU;->f:LX/1zH;

    invoke-virtual {v0, v2, v4, v3}, LX/1zH;->a(III)I

    move-result v0

    move p1, v2

    move v2, v0

    .line 353298
    :goto_2
    if-eq v2, v3, :cond_9

    .line 353299
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v0, v2, p1}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    goto :goto_0

    .line 353300
    :cond_9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v0, v2, :cond_a

    .line 353301
    sget-object v0, LX/1zU;->d:Lcom/facebook/ui/emoji/model/Emoji;

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    .line 353302
    goto/16 :goto_0

    :cond_b
    move v2, v0

    goto :goto_2

    :cond_c
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(IILjava/util/List;)Lcom/facebook/ui/emoji/model/Emoji;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/facebook/ui/emoji/model/Emoji;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 353273
    move v1, v2

    .line 353274
    move v1, v1

    .line 353275
    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIILjava/util/List;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/emoji/model/Emoji;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 353266
    iget-object v1, p1, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    move-object v1, v1

    .line 353267
    if-eqz v1, :cond_1

    .line 353268
    :cond_0
    :goto_0
    return v0

    .line 353269
    :cond_1
    iget-object v1, p0, LX/1zU;->f:LX/1zH;

    .line 353270
    iget v2, p1, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v2, v2

    .line 353271
    const v3, 0x1f3fb

    invoke-virtual {v1, v2, v3, v4}, LX/1zH;->a(III)I

    move-result v1

    .line 353272
    if-eq v1, v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ui/emoji/model/Emoji;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353255
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 353256
    invoke-virtual {p0, p1}, LX/1zU;->a(Lcom/facebook/ui/emoji/model/Emoji;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353257
    iget v0, p1, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v0, v0

    .line 353258
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v0

    .line 353259
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 353260
    const v0, 0x1f3fb

    .line 353261
    :goto_0
    const v1, 0x1f3ff

    if-gt v0, v1, :cond_1

    .line 353262
    iget v1, p1, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    move v3, v1

    .line 353263
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v3, v0}, LX/1zU;->a(II)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    goto :goto_0

    .line 353264
    :cond_0
    invoke-virtual {v2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 353265
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
