.class public final enum LX/1vy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1vy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1vy;

.field public static final enum APP_STATE_MANAGER:LX/1vy;

.field public static final enum CACHED_SECTIONS:LX/1vy;

.field public static final enum CREATOR_SPACE:LX/1vy;

.field public static final enum DISABLE_LIVE_STATUS_ANIMATION:LX/1vy;

.field public static final enum DOWNLOAD_BUTTON:LX/1vy;

.field public static final enum DOWNLOAD_SECTION:LX/1vy;

.field public static final enum DOWNLOAD_SEEN_STATE:LX/1vy;

.field public static final enum FAKE_LATW_PAGES_ON_FRAGMENT_CREATION:LX/1vy;

.field public static final enum MQTT:LX/1vy;

.field public static final enum PREFETCHING:LX/1vy;

.field public static final enum PREFETCH_CONTROLLER:LX/1vy;

.field public static final enum PUSH_NOTIF_CACHE:LX/1vy;

.field public static final enum SOUND_ON:LX/1vy;

.field public static final enum VH_LIVE_NOTIFICATIONS:LX/1vy;

.field public static final enum VH_LIVE_NOTIFICATIONS_MENU:LX/1vy;

.field public static final enum VIDEO_BYTES_PREFETCH:LX/1vy;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 344879
    new-instance v0, LX/1vy;

    const-string v1, "APP_STATE_MANAGER"

    invoke-direct {v0, v1, v3}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    .line 344880
    new-instance v0, LX/1vy;

    const-string v1, "CACHED_SECTIONS"

    invoke-direct {v0, v1, v4}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->CACHED_SECTIONS:LX/1vy;

    .line 344881
    new-instance v0, LX/1vy;

    const-string v1, "CREATOR_SPACE"

    invoke-direct {v0, v1, v5}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->CREATOR_SPACE:LX/1vy;

    .line 344882
    new-instance v0, LX/1vy;

    const-string v1, "DISABLE_LIVE_STATUS_ANIMATION"

    invoke-direct {v0, v1, v6}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->DISABLE_LIVE_STATUS_ANIMATION:LX/1vy;

    .line 344883
    new-instance v0, LX/1vy;

    const-string v1, "DOWNLOAD_BUTTON"

    invoke-direct {v0, v1, v7}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    .line 344884
    new-instance v0, LX/1vy;

    const-string v1, "DOWNLOAD_SECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    .line 344885
    new-instance v0, LX/1vy;

    const-string v1, "DOWNLOAD_SEEN_STATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->DOWNLOAD_SEEN_STATE:LX/1vy;

    .line 344886
    new-instance v0, LX/1vy;

    const-string v1, "FAKE_LATW_PAGES_ON_FRAGMENT_CREATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->FAKE_LATW_PAGES_ON_FRAGMENT_CREATION:LX/1vy;

    .line 344887
    new-instance v0, LX/1vy;

    const-string v1, "MQTT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->MQTT:LX/1vy;

    .line 344888
    new-instance v0, LX/1vy;

    const-string v1, "PREFETCHING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->PREFETCHING:LX/1vy;

    .line 344889
    new-instance v0, LX/1vy;

    const-string v1, "PREFETCH_CONTROLLER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    .line 344890
    new-instance v0, LX/1vy;

    const-string v1, "PUSH_NOTIF_CACHE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    .line 344891
    new-instance v0, LX/1vy;

    const-string v1, "SOUND_ON"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->SOUND_ON:LX/1vy;

    .line 344892
    new-instance v0, LX/1vy;

    const-string v1, "VH_LIVE_NOTIFICATIONS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    .line 344893
    new-instance v0, LX/1vy;

    const-string v1, "VIDEO_BYTES_PREFETCH"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->VIDEO_BYTES_PREFETCH:LX/1vy;

    .line 344894
    new-instance v0, LX/1vy;

    const-string v1, "VH_LIVE_NOTIFICATIONS_MENU"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/1vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vy;->VH_LIVE_NOTIFICATIONS_MENU:LX/1vy;

    .line 344895
    const/16 v0, 0x10

    new-array v0, v0, [LX/1vy;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    aput-object v1, v0, v3

    sget-object v1, LX/1vy;->CACHED_SECTIONS:LX/1vy;

    aput-object v1, v0, v4

    sget-object v1, LX/1vy;->CREATOR_SPACE:LX/1vy;

    aput-object v1, v0, v5

    sget-object v1, LX/1vy;->DISABLE_LIVE_STATUS_ANIMATION:LX/1vy;

    aput-object v1, v0, v6

    sget-object v1, LX/1vy;->DOWNLOAD_BUTTON:LX/1vy;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1vy;->DOWNLOAD_SEEN_STATE:LX/1vy;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1vy;->FAKE_LATW_PAGES_ON_FRAGMENT_CREATION:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/1vy;->MQTT:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/1vy;->PREFETCHING:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/1vy;->PUSH_NOTIF_CACHE:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/1vy;->SOUND_ON:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/1vy;->VH_LIVE_NOTIFICATIONS:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/1vy;->VIDEO_BYTES_PREFETCH:LX/1vy;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/1vy;->VH_LIVE_NOTIFICATIONS_MENU:LX/1vy;

    aput-object v2, v0, v1

    sput-object v0, LX/1vy;->$VALUES:[LX/1vy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 344878
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1vy;
    .locals 1

    .prologue
    .line 344876
    const-class v0, LX/1vy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1vy;

    return-object v0
.end method

.method public static values()[LX/1vy;
    .locals 1

    .prologue
    .line 344877
    sget-object v0, LX/1vy;->$VALUES:[LX/1vy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1vy;

    return-object v0
.end method
