.class public LX/1xx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5QP;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1nV;

.field public g:LX/1Qt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5QP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349867
    iput-object p1, p0, LX/1xx;->a:Landroid/content/Context;

    .line 349868
    iput-object p2, p0, LX/1xx;->b:LX/0Ot;

    .line 349869
    iput-object p3, p0, LX/1xx;->c:LX/0Ot;

    .line 349870
    iput-object p4, p0, LX/1xx;->d:LX/0Ot;

    .line 349871
    iput-object p5, p0, LX/1xx;->e:LX/0Ot;

    .line 349872
    return-void
.end method
