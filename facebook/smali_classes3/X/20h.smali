.class public LX/20h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static b:I

.field private static k:LX/0Xm;


# instance fields
.field private final c:LX/1Ck;

.field public final d:LX/0tX;

.field private final e:LX/20i;

.field private final f:LX/20j;

.field public final g:LX/0SI;

.field private final h:LX/0ie;

.field public final i:LX/0pf;

.field private final j:LX/0yx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 355336
    const-class v0, LX/20h;

    sput-object v0, LX/20h;->a:Ljava/lang/Class;

    .line 355337
    const/4 v0, 0x0

    sput v0, LX/20h;->b:I

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/20i;LX/20j;LX/0SI;LX/0ie;LX/0pf;LX/0yx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 355261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355262
    iput-object p1, p0, LX/20h;->c:LX/1Ck;

    .line 355263
    iput-object p2, p0, LX/20h;->d:LX/0tX;

    .line 355264
    iput-object p3, p0, LX/20h;->e:LX/20i;

    .line 355265
    iput-object p4, p0, LX/20h;->f:LX/20j;

    .line 355266
    iput-object p5, p0, LX/20h;->g:LX/0SI;

    .line 355267
    iput-object p6, p0, LX/20h;->h:LX/0ie;

    .line 355268
    iput-object p7, p0, LX/20h;->i:LX/0pf;

    .line 355269
    iput-object p8, p0, LX/20h;->j:LX/0yx;

    .line 355270
    return-void
.end method

.method public static a(LX/0QB;)LX/20h;
    .locals 12

    .prologue
    .line 355142
    const-class v1, LX/20h;

    monitor-enter v1

    .line 355143
    :try_start_0
    sget-object v0, LX/20h;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 355144
    sput-object v2, LX/20h;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 355145
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355146
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 355147
    new-instance v3, LX/20h;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v6

    check-cast v6, LX/20i;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v7

    check-cast v7, LX/20j;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v8

    check-cast v8, LX/0SI;

    invoke-static {v0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v9

    check-cast v9, LX/0ie;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v10

    check-cast v10, LX/0pf;

    invoke-static {v0}, LX/0yx;->a(LX/0QB;)LX/0yx;

    move-result-object v11

    check-cast v11, LX/0yx;

    invoke-direct/range {v3 .. v11}, LX/20h;-><init>(LX/1Ck;LX/0tX;LX/20i;LX/20j;LX/0SI;LX/0ie;LX/0pf;LX/0yx;)V

    .line 355148
    move-object v0, v3

    .line 355149
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 355150
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355151
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 355152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;
    .locals 5

    .prologue
    .line 355271
    const/4 v0, 0x1

    .line 355272
    if-nez p1, :cond_0

    .line 355273
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 355274
    if-ne v1, v0, :cond_0

    .line 355275
    :goto_0
    move v0, v0

    .line 355276
    if-nez p1, :cond_2

    .line 355277
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 355278
    if-eqz v1, :cond_2

    .line 355279
    const/4 v1, 0x1

    .line 355280
    :goto_1
    move v1, v1

    .line 355281
    new-instance v2, LX/3dA;

    invoke-direct {v2}, LX/3dA;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    .line 355282
    iput-object v3, v2, LX/3dA;->c:Ljava/lang/String;

    .line 355283
    move-object v2, v2

    .line 355284
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    .line 355285
    iput-object v3, v2, LX/3dA;->a:Ljava/lang/String;

    .line 355286
    move-object v2, v2

    .line 355287
    sget-object v3, LX/1zt;->c:LX/1zt;

    if-eq p2, v3, :cond_4

    .line 355288
    new-instance v3, LX/3dB;

    invoke-direct {v3}, LX/3dB;-><init>()V

    .line 355289
    iget v4, p2, LX/1zt;->e:I

    move v4, v4

    .line 355290
    iput v4, v3, LX/3dB;->a:I

    .line 355291
    move-object v3, v3

    .line 355292
    invoke-virtual {v3}, LX/3dB;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;

    move-result-object v3

    .line 355293
    :goto_2
    move-object v3, v3

    .line 355294
    iput-object v3, v2, LX/3dA;->h:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;

    .line 355295
    move-object v2, v2

    .line 355296
    iget v3, p2, LX/1zt;->e:I

    move v3, v3

    .line 355297
    iput v3, v2, LX/3dA;->i:I

    .line 355298
    move-object v2, v2

    .line 355299
    new-instance v3, LX/3dD;

    invoke-direct {v3}, LX/3dD;-><init>()V

    .line 355300
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    .line 355301
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v4

    :goto_3
    move v4, v4

    .line 355302
    add-int/2addr v0, v4

    .line 355303
    iput v0, v3, LX/3dD;->a:I

    .line 355304
    move-object v0, v3

    .line 355305
    invoke-virtual {v0}, LX/3dD;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 355306
    iput-object v0, v2, LX/3dA;->d:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;

    .line 355307
    move-object v0, v2

    .line 355308
    new-instance v2, LX/3dE;

    invoke-direct {v2}, LX/3dE;-><init>()V

    .line 355309
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v3

    .line 355310
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->a()I

    move-result v3

    :goto_4
    move v3, v3

    .line 355311
    add-int/2addr v1, v3

    .line 355312
    iput v1, v2, LX/3dE;->a:I

    .line 355313
    move-object v1, v2

    .line 355314
    invoke-virtual {v1}, LX/3dE;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    move-result-object v1

    .line 355315
    iput-object v1, v0, LX/3dA;->e:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    .line 355316
    move-object v0, v0

    .line 355317
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    .line 355318
    iget v2, p2, LX/1zt;->e:I

    move v2, v2

    .line 355319
    invoke-static {v1, p1, v2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;II)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    .line 355320
    invoke-static {v1}, LX/3dC;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v1

    move-object v1, v1

    .line 355321
    iput-object v1, v0, LX/3dA;->f:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    .line 355322
    new-instance v1, LX/3d9;

    invoke-direct {v1}, LX/3d9;-><init>()V

    invoke-virtual {v0}, LX/3dA;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v0

    .line 355323
    iput-object v0, v1, LX/3d9;->a:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    .line 355324
    move-object v0, v1

    .line 355325
    invoke-virtual {v0}, LX/3d9;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v0

    return-object v0

    .line 355326
    :cond_0
    if-ne p1, v0, :cond_1

    .line 355327
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 355328
    if-eq v1, v0, :cond_1

    .line 355329
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 355330
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 355331
    :cond_2
    if-eqz p1, :cond_3

    .line 355332
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 355333
    if-nez v1, :cond_3

    .line 355334
    const/4 v1, -0x1

    goto/16 :goto_1

    .line 355335
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    goto :goto_4
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;ILX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 355244
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0, p2, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;

    move-result-object v1

    .line 355245
    iget-object v0, p0, LX/20h;->e:LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 355246
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 355247
    iget-object v0, p0, LX/20h;->e:LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/3dL;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/3dL;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    .line 355248
    iput-object v2, v0, LX/3dL;->E:Ljava/lang/String;

    .line 355249
    move-object v0, v0

    .line 355250
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    .line 355251
    iput-object v2, v0, LX/3dL;->ag:Ljava/lang/String;

    .line 355252
    move-object v0, v0

    .line 355253
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 355254
    iput-object v2, v0, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 355255
    move-object v0, v0

    .line 355256
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 355257
    :cond_0
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    iget-object v3, p0, LX/20h;->f:LX/20j;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v3, v0, v4, v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 355258
    iput-object v0, v2, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 355259
    move-object v0, v2

    .line 355260
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 355217
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 355218
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 355219
    invoke-direct {p0, p1, v0, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;ILX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    .line 355220
    :cond_0
    :goto_0
    return-object p1

    .line 355221
    :cond_1
    invoke-static {p1, p2}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 355222
    if-eqz v0, :cond_0

    .line 355223
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 355224
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 355225
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    invoke-direct {p0, v0, v1, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;ILX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 355226
    iput-object v0, v2, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 355227
    move-object v0, v2

    .line 355228
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0

    .line 355229
    :cond_2
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355230
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 355231
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 355232
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 355233
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355234
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 355235
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-static {v5}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 355236
    invoke-direct {p0, v0, v5, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;ILX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 355237
    :cond_4
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-static {v1}, LX/4Z1;->a(Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;)LX/4Z1;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 355238
    iput-object v2, v1, LX/4Z1;->c:LX/0Px;

    .line 355239
    move-object v1, v1

    .line 355240
    invoke-virtual {v1}, LX/4Z1;->a()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    .line 355241
    iput-object v1, v0, LX/23u;->e:Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    .line 355242
    move-object v0, v0

    .line 355243
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V
    .locals 7

    .prologue
    .line 355153
    invoke-static {p1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 355154
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mutate_reaction_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 355155
    iget v2, p2, LX/1zt;->e:I

    move v2, v2

    .line 355156
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, LX/20h;->b:I

    add-int/lit8 v2, v2, 0x1

    sput v2, LX/20h;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 355157
    iget-object v2, p0, LX/20h;->c:LX/1Ck;

    .line 355158
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 355159
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 355160
    iget-object v3, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->a:LX/162;

    move-object v3, v3

    .line 355161
    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    const-string v4, "Reaction mutations must include tracking codes."

    invoke-static {v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 355162
    iget-object v3, p0, LX/20h;->i:LX/0pf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0pf;->a(Ljava/lang/String;)LX/1g0;

    move-result-object v4

    .line 355163
    if-eqz v4, :cond_3

    .line 355164
    iget-object v3, v4, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v3, v3

    .line 355165
    if-eqz v3, :cond_3

    .line 355166
    iget-object v3, v4, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v3, v3

    .line 355167
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 355168
    iget-object v3, v4, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v3, v3

    .line 355169
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 355170
    :goto_1
    new-instance v5, LX/4Eo;

    invoke-direct {v5}, LX/4Eo;-><init>()V

    invoke-virtual {v5, v3}, LX/4Eo;->a(Ljava/lang/String;)LX/4Eo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/4Eo;->b(Ljava/lang/String;)LX/4Eo;

    move-result-object v3

    .line 355171
    iget-object v5, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 355172
    const-string v6, "feedback_source"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 355173
    move-object v3, v3

    .line 355174
    iget-object v5, p3, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 355175
    const-string v6, "nectar_module"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 355176
    move-object v3, v3

    .line 355177
    iget v5, p2, LX/1zt;->e:I

    move v5, v5

    .line 355178
    if-lez v5, :cond_5

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v5, v6

    .line 355179
    invoke-virtual {v3, v5}, LX/4Eo;->a(Ljava/lang/Integer;)LX/4Eo;

    move-result-object v3

    invoke-virtual {p3}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->f()LX/0Px;

    move-result-object v5

    .line 355180
    const-string v6, "tracking"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 355181
    move-object v3, v3

    .line 355182
    invoke-static {}, LX/5Cv;->b()LX/15S;

    move-result-object v5

    .line 355183
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 355184
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 355185
    invoke-static {p1, v0, p2}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;ILX/1zt;)Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel;

    move-result-object v5

    .line 355186
    invoke-virtual {v3, v5}, LX/399;->a(LX/0jT;)LX/399;

    .line 355187
    iget-object v5, p0, LX/20h;->g:LX/0SI;

    invoke-interface {v5}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 355188
    iput-object v5, v3, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 355189
    if-eqz v4, :cond_0

    .line 355190
    iget-object v5, v4, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v5, v5

    .line 355191
    if-eqz v5, :cond_0

    .line 355192
    iget-object v5, v4, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v4, v5

    .line 355193
    iput-object v4, v3, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 355194
    :cond_0
    iget-object v4, p0, LX/20h;->d:LX/0tX;

    sget-object v5, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v4, v3, v5}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 355195
    invoke-virtual {v2, v1, v0, p4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 355196
    iget-object v0, p0, LX/20h;->h:LX/0ie;

    .line 355197
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 355198
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 355199
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "reaction_type"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 355200
    iget-object v3, v0, LX/0ie;->a:LX/0if;

    sget-object v4, LX/0ig;->ar:LX/0ih;

    sget-object v5, LX/8D1;->REACTION:LX/8D1;

    invoke-virtual {v5}, LX/8D1;->name()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 355201
    sget-object v0, LX/1zt;->c:LX/1zt;

    if-eq p2, v0, :cond_1

    .line 355202
    iget-object v0, p0, LX/20h;->j:LX/0yx;

    .line 355203
    invoke-static {v0}, LX/0yx;->b(LX/0yx;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/0yx;->d:Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;

    .line 355204
    iget-boolean v2, v1, Lcom/facebook/feed/data/freshfeed/uih/UIHConfig;->mShouldReportReaction:Z

    move v1, v2

    .line 355205
    if-nez v1, :cond_6

    .line 355206
    :cond_1
    :goto_3
    return-void

    .line 355207
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 355208
    :cond_3
    iget-object v3, p0, LX/20h;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/20h;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 355209
    iget-object v5, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v5

    .line 355210
    goto/16 :goto_1

    :cond_4
    iget-object v3, p0, LX/20h;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    .line 355211
    iget-object v5, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v5

    .line 355212
    goto/16 :goto_1

    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 355213
    :cond_6
    iget-object v1, v0, LX/0yx;->e:LX/0z3;

    invoke-virtual {v1}, LX/0z3;->a()LX/6VO;

    move-result-object v1

    .line 355214
    sget-object v2, LX/14q;->REACTION:LX/14q;

    .line 355215
    iput-object v2, v1, LX/6VO;->a:LX/14q;

    .line 355216
    invoke-static {v0, v1}, LX/0yx;->a(LX/0yx;LX/6VO;)V

    goto :goto_3
.end method
