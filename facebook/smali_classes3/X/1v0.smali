.class public LX/1v0;
.super Lorg/apache/http/impl/client/RequestWrapper;
.source ""

# interfaces
.implements Lorg/apache/http/client/methods/AbortableHttpRequest;


# instance fields
.field private final a:Lorg/apache/http/client/methods/AbortableHttpRequest;

.field private b:Lorg/apache/http/RequestLine;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/AbortableHttpRequest;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 1

    .prologue
    .line 341888
    invoke-direct {p0, p2}, Lorg/apache/http/impl/client/RequestWrapper;-><init>(Lorg/apache/http/HttpRequest;)V

    .line 341889
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/AbortableHttpRequest;

    iput-object v0, p0, LX/1v0;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    .line 341890
    return-void
.end method


# virtual methods
.method public final getRequestLine()Lorg/apache/http/RequestLine;
    .locals 1

    .prologue
    .line 341883
    iget-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    .line 341884
    if-eqz v0, :cond_0

    .line 341885
    :goto_0
    return-object v0

    .line 341886
    :cond_0
    invoke-super {p0}, Lorg/apache/http/impl/client/RequestWrapper;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    iput-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    .line 341887
    iget-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    goto :goto_0
.end method

.method public final setConnectionRequest(Lorg/apache/http/conn/ClientConnectionRequest;)V
    .locals 1

    .prologue
    .line 341878
    iget-object v0, p0, LX/1v0;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-interface {v0, p1}, Lorg/apache/http/client/methods/AbortableHttpRequest;->setConnectionRequest(Lorg/apache/http/conn/ClientConnectionRequest;)V

    .line 341879
    return-void
.end method

.method public final setMethod(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 341880
    const/4 v0, 0x0

    iput-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    .line 341881
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/RequestWrapper;->setMethod(Ljava/lang/String;)V

    .line 341882
    return-void
.end method

.method public final setProtocolVersion(Lorg/apache/http/ProtocolVersion;)V
    .locals 1

    .prologue
    .line 341870
    const/4 v0, 0x0

    iput-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    .line 341871
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/RequestWrapper;->setProtocolVersion(Lorg/apache/http/ProtocolVersion;)V

    .line 341872
    return-void
.end method

.method public final setReleaseTrigger(Lorg/apache/http/conn/ConnectionReleaseTrigger;)V
    .locals 1

    .prologue
    .line 341876
    iget-object v0, p0, LX/1v0;->a:Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-interface {v0, p1}, Lorg/apache/http/client/methods/AbortableHttpRequest;->setReleaseTrigger(Lorg/apache/http/conn/ConnectionReleaseTrigger;)V

    .line 341877
    return-void
.end method

.method public final setURI(Ljava/net/URI;)V
    .locals 1

    .prologue
    .line 341873
    const/4 v0, 0x0

    iput-object v0, p0, LX/1v0;->b:Lorg/apache/http/RequestLine;

    .line 341874
    invoke-super {p0, p1}, Lorg/apache/http/impl/client/RequestWrapper;->setURI(Ljava/net/URI;)V

    .line 341875
    return-void
.end method
