.class public LX/1ui;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1uk;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 341413
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 341414
    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 341415
    new-instance v0, LX/1uj;

    invoke-direct {v0}, LX/1uj;-><init>()V

    sput-object v0, LX/1ui;->a:LX/1uk;

    .line 341416
    :goto_0
    return-void

    .line 341417
    :cond_0
    new-instance v0, LX/3on;

    invoke-direct {v0}, LX/3on;-><init>()V

    sput-object v0, LX/1ui;->a:LX/1uk;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341412
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 341408
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 341409
    sget-object v0, LX/1ui;->a:LX/1uk;

    invoke-interface {v0, p0, p1, p2}, LX/1uk;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 341410
    return-void
.end method
