.class public final LX/1xk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/res/Resources;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:I

.field private c:Ljava/lang/Float;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;I)V
    .locals 1

    .prologue
    .line 349334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349335
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, LX/1xk;->a:Landroid/content/res/Resources;

    .line 349336
    iput p2, p0, LX/1xk;->b:I

    .line 349337
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 349338
    iget-object v0, p0, LX/1xk;->a:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    .line 349339
    monitor-enter p0

    .line 349340
    :try_start_0
    iget-object v0, p0, LX/1xk;->a:Landroid/content/res/Resources;

    if-eqz v0, :cond_0

    .line 349341
    iget-object v0, p0, LX/1xk;->a:Landroid/content/res/Resources;

    iget v1, p0, LX/1xk;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/1xk;->c:Ljava/lang/Float;

    .line 349342
    const/4 v0, 0x0

    iput-object v0, p0, LX/1xk;->a:Landroid/content/res/Resources;

    .line 349343
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349344
    :cond_1
    iget-object v0, p0, LX/1xk;->c:Ljava/lang/Float;

    return-object v0

    .line 349345
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
