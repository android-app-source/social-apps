.class public final LX/20A;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public final synthetic c:LX/1zZ;


# direct methods
.method public constructor <init>(LX/1zZ;)V
    .locals 1

    .prologue
    .line 354397
    iput-object p1, p0, LX/20A;->c:LX/1zZ;

    .line 354398
    move-object v0, p1

    .line 354399
    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 354400
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354401
    const-string v0, "TokenPileDrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 354402
    if-ne p0, p1, :cond_1

    .line 354403
    :cond_0
    :goto_0
    return v0

    .line 354404
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 354405
    goto :goto_0

    .line 354406
    :cond_3
    check-cast p1, LX/20A;

    .line 354407
    iget-object v2, p0, LX/20A;->a:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/20A;->a:Ljava/util/List;

    iget-object v3, p1, LX/20A;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 354408
    goto :goto_0

    .line 354409
    :cond_5
    iget-object v2, p1, LX/20A;->a:Ljava/util/List;

    if-nez v2, :cond_4

    .line 354410
    :cond_6
    iget-boolean v2, p0, LX/20A;->b:Z

    iget-boolean v3, p1, LX/20A;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 354411
    goto :goto_0
.end method
