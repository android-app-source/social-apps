.class public LX/1zA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xz;


# instance fields
.field private final a:LX/1nq;

.field private final b:LX/1zB;

.field public final c:LX/1zC;

.field public final d:LX/1xc;

.field public final e:LX/1nA;

.field public final f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final g:I

.field public final h:I

.field public final i:Z

.field private final j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final k:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1zB;LX/1zC;LX/1xc;LX/1nA;LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;IIZLX/1PT;)V
    .locals 6
    .param p5    # LX/1nq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zB;",
            "LX/1zC;",
            "LX/1xc;",
            "LX/1nA;",
            "LX/1nq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IIZ",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351808
    iput-object p5, p0, LX/1zA;->a:LX/1nq;

    .line 351809
    iput-object p1, p0, LX/1zA;->b:LX/1zB;

    .line 351810
    iput-object p2, p0, LX/1zA;->c:LX/1zC;

    .line 351811
    iput-object p3, p0, LX/1zA;->d:LX/1xc;

    .line 351812
    iput-object p4, p0, LX/1zA;->e:LX/1nA;

    .line 351813
    iput-object p6, p0, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 351814
    iput p7, p0, LX/1zA;->g:I

    .line 351815
    iput p8, p0, LX/1zA;->h:I

    .line 351816
    iput-boolean p9, p0, LX/1zA;->i:Z

    .line 351817
    invoke-virtual {p6}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 351818
    new-instance v0, LX/1zI;

    invoke-virtual {p6}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, p0

    move v3, p9

    move-object/from16 v4, p10

    invoke-direct/range {v0 .. v4}, LX/1zI;-><init>(LX/1zA;Lcom/facebook/graphql/model/GraphQLStory;ZLX/1PT;)V

    iput-object v0, p0, LX/1zA;->k:LX/1KL;

    .line 351819
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 351805
    invoke-static {p0}, LX/0x1;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v1

    .line 351806
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 2

    .prologue
    .line 351802
    iget-object v0, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351803
    :cond_0
    const/4 v0, 0x0

    .line 351804
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351801
    iget-object v0, p0, LX/1zA;->k:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 351800
    iget-object v0, p0, LX/1zA;->j:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 351797
    iget-object v0, p0, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 351798
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 351799
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1nq;
    .locals 1

    .prologue
    .line 351795
    iget-object v0, p0, LX/1zA;->a:LX/1nq;

    return-object v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 351796
    iget-object v0, p0, LX/1zA;->b:LX/1zB;

    iget-object v1, p0, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/1zB;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    return v0
.end method
