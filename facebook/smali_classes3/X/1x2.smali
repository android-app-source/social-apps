.class public final enum LX/1x2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1x2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1x2;

.field public static final enum LCAU:LX/1x2;

.field public static final enum MESSENGER:LX/1x2;

.field public static final enum TRANSPARENT:LX/1x2;


# instance fields
.field private final mThemeId:I

.field private final mThemeName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 347560
    new-instance v0, LX/1x2;

    const-string v1, "TRANSPARENT"

    const-string v2, "transparent"

    const v3, 0x7f0e074d

    invoke-direct {v0, v1, v4, v2, v3}, LX/1x2;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/1x2;->TRANSPARENT:LX/1x2;

    .line 347561
    new-instance v0, LX/1x2;

    const-string v1, "LCAU"

    const-string v2, "lcau"

    const v3, 0x7f0e074f

    invoke-direct {v0, v1, v5, v2, v3}, LX/1x2;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/1x2;->LCAU:LX/1x2;

    .line 347562
    new-instance v0, LX/1x2;

    const-string v1, "MESSENGER"

    const-string v2, "messenger"

    const v3, 0x7f0e074e

    invoke-direct {v0, v1, v6, v2, v3}, LX/1x2;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/1x2;->MESSENGER:LX/1x2;

    .line 347563
    const/4 v0, 0x3

    new-array v0, v0, [LX/1x2;

    sget-object v1, LX/1x2;->TRANSPARENT:LX/1x2;

    aput-object v1, v0, v4

    sget-object v1, LX/1x2;->LCAU:LX/1x2;

    aput-object v1, v0, v5

    sget-object v1, LX/1x2;->MESSENGER:LX/1x2;

    aput-object v1, v0, v6

    sput-object v0, LX/1x2;->$VALUES:[LX/1x2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 347564
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 347565
    iput-object p3, p0, LX/1x2;->mThemeName:Ljava/lang/String;

    .line 347566
    iput p4, p0, LX/1x2;->mThemeId:I

    .line 347567
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1x2;
    .locals 1

    .prologue
    .line 347568
    const-class v0, LX/1x2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1x2;

    return-object v0
.end method

.method public static values()[LX/1x2;
    .locals 1

    .prologue
    .line 347569
    sget-object v0, LX/1x2;->$VALUES:[LX/1x2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1x2;

    return-object v0
.end method


# virtual methods
.method public final getThemeId()I
    .locals 1

    .prologue
    .line 347570
    iget v0, p0, LX/1x2;->mThemeId:I

    return v0
.end method

.method public final getThemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347571
    iget-object v0, p0, LX/1x2;->mThemeName:Ljava/lang/String;

    return-object v0
.end method
