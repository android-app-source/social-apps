.class public interface abstract LX/21n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;
.implements LX/21o;
.implements LX/21p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;",
        "LX/21o;",
        "LX/21p;"
    }
.end annotation


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Lcom/facebook/tagging/model/TaggingProfile;)V
.end method

.method public abstract a(Ljava/lang/String;Landroid/os/Bundle;II)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract getInlineReactBannerRootTag()I
.end method

.method public abstract getPhotoButton()Landroid/view/View;
.end method

.method public abstract getSelfAsView()Landroid/view/View;
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public abstract k()Z
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract setCommentComposerManager(LX/9CP;)V
.end method

.method public abstract setFeedbackLoggingParams(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
.end method

.method public abstract setGroupIdForTagging(Ljava/lang/Long;)V
.end method

.method public abstract setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V
.end method

.method public abstract setNotificationLogObject(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
.end method

.method public abstract setReshareButtonExperimentClicked(Z)V
.end method

.method public abstract setTransliterationClickListener(LX/9D8;)V
.end method

.method public abstract setVisibility(I)V
.end method
