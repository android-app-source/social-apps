.class public LX/1uw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/RedirectHandler;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DeprecatedInterface"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/net/Uri;

.field private final c:LX/1Gm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 341817
    const-class v0, LX/1uw;

    sput-object v0, LX/1uw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1Gm;)V
    .locals 0

    .prologue
    .line 341795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341796
    iput-object p1, p0, LX/1uw;->b:Landroid/net/Uri;

    .line 341797
    iput-object p2, p0, LX/1uw;->c:LX/1Gm;

    .line 341798
    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 341808
    const-string v0, "Location"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 341809
    if-nez v0, :cond_0

    .line 341810
    new-instance v0, Lorg/apache/http/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received redirect response "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but no Location header"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341811
    :cond_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 341812
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_1

    .line 341813
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 341814
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    .line 341815
    invoke-virtual {v2, v1}, Ljava/net/URI;->resolve(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, v1

    .line 341816
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final getLocationURI(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Ljava/net/URI;
    .locals 2

    .prologue
    .line 341802
    iget-object v0, p0, LX/1uw;->b:Landroid/net/Uri;

    invoke-static {p1, v0}, LX/1uw;->a(Lorg/apache/http/HttpResponse;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 341803
    iput-object v0, p0, LX/1uw;->b:Landroid/net/Uri;

    .line 341804
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341805
    iget-object v1, p0, LX/1uw;->c:LX/1Gm;

    .line 341806
    invoke-virtual {v1, v0}, LX/1Gm;->b(Ljava/lang/String;)V

    .line 341807
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public final isRedirectRequested(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)Z
    .locals 1

    .prologue
    .line 341799
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 341800
    const/16 p0, 0x12e

    if-eq v0, p0, :cond_0

    const/16 p0, 0x12d

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 341801
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
