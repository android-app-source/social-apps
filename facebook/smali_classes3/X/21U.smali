.class public LX/21U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/21S;",
            "LX/21V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 9

    .prologue
    .line 357212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357213
    iput-object p1, p0, LX/21U;->a:Landroid/content/res/Resources;

    .line 357214
    const-class v1, LX/21S;

    invoke-static {v1}, LX/0PM;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v7

    .line 357215
    sget-object v8, LX/21S;->INLINE:LX/21S;

    new-instance v1, LX/21V;

    iget-object v2, p0, LX/21U;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, LX/21U;->a:Landroid/content/res/Resources;

    const v4, 0x7f0b009d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iget-object v4, p0, LX/21U;->a:Landroid/content/res/Resources;

    const v5, 0x7f0219c6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    const/4 v5, 0x3

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, LX/21V;-><init>(IFIIZ)V

    invoke-virtual {v7, v8, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357216
    move-object v0, v7

    .line 357217
    iput-object v0, p0, LX/21U;->b:Ljava/util/EnumMap;

    .line 357218
    return-void
.end method

.method public static a(LX/21a;LX/21V;II)I
    .locals 3

    .prologue
    .line 357219
    sget-object v0, LX/21b;->a:[I

    invoke-virtual {p0}, LX/21a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 357220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown ButtonWidth Style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/21a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357221
    :pswitch_0
    iget v0, p1, LX/21V;->b:I

    move v0, v0

    .line 357222
    iget v1, p1, LX/21V;->a:I

    move v1, v1

    .line 357223
    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    move v0, v0

    .line 357224
    :goto_0
    return v0

    .line 357225
    :pswitch_1
    iget v0, p1, LX/21V;->b:I

    move v0, v0

    .line 357226
    iget v1, p1, LX/21V;->a:I

    move v1, v1

    .line 357227
    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v0, v1

    add-int/2addr v0, p3

    move v0, v0

    .line 357228
    goto :goto_0

    .line 357229
    :pswitch_2
    iget v0, p1, LX/21V;->a:I

    move v0, v0

    .line 357230
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    move v0, v0

    .line 357231
    goto :goto_0

    .line 357232
    :pswitch_3
    iget v0, p1, LX/21V;->a:I

    move v0, v0

    .line 357233
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p3

    move v0, v0

    .line 357234
    goto :goto_0

    .line 357235
    :pswitch_4
    iget v0, p1, LX/21V;->b:I

    move v0, v0

    .line 357236
    iget v1, p1, LX/21V;->a:I

    move v1, v1

    .line 357237
    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v0, v0

    .line 357238
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/21V;I)I
    .locals 2

    .prologue
    .line 357239
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 357240
    iget v1, p1, LX/21V;->c:F

    move v1, v1

    .line 357241
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 357242
    iget-boolean v1, p1, LX/21V;->e:Z

    move v1, v1

    .line 357243
    if-eqz v1, :cond_0

    .line 357244
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 357245
    :cond_0
    iget-object v1, p0, LX/21U;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method
