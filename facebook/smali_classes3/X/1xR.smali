.class public LX/1xR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1EN;


# direct methods
.method public constructor <init>(LX/0Or;LX/1EN;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/ShouldStoryMessageLinkToFlyout;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1EN;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348407
    iput-object p1, p0, LX/1xR;->a:LX/0Or;

    .line 348408
    iput-object p2, p0, LX/1xR;->b:LX/1EN;

    .line 348409
    return-void
.end method

.method public static a(LX/0QB;)LX/1xR;
    .locals 5

    .prologue
    .line 348395
    const-class v1, LX/1xR;

    monitor-enter v1

    .line 348396
    :try_start_0
    sget-object v0, LX/1xR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348397
    sput-object v2, LX/1xR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348398
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348399
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348400
    new-instance v4, LX/1xR;

    const/16 v3, 0x14b4

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v3

    check-cast v3, LX/1EN;

    invoke-direct {v4, p0, v3}, LX/1xR;-><init>(LX/0Or;LX/1EN;)V

    .line 348401
    move-object v0, v4

    .line 348402
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348403
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348404
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348405
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
