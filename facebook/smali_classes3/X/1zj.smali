.class public LX/1zj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1zi;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1zj;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1zk;

.field private final c:LX/1zl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1zk;LX/1zl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353965
    iput-object p1, p0, LX/1zj;->a:Landroid/content/Context;

    .line 353966
    iput-object p2, p0, LX/1zj;->b:LX/1zk;

    .line 353967
    iput-object p3, p0, LX/1zj;->c:LX/1zl;

    .line 353968
    return-void
.end method

.method public static a(LX/0QB;)LX/1zj;
    .locals 6

    .prologue
    .line 353969
    sget-object v0, LX/1zj;->d:LX/1zj;

    if-nez v0, :cond_1

    .line 353970
    const-class v1, LX/1zj;

    monitor-enter v1

    .line 353971
    :try_start_0
    sget-object v0, LX/1zj;->d:LX/1zj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353972
    if-eqz v2, :cond_0

    .line 353973
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353974
    new-instance p0, LX/1zj;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1zk;->a(LX/0QB;)LX/1zk;

    move-result-object v4

    check-cast v4, LX/1zk;

    invoke-static {v0}, LX/1zl;->a(LX/0QB;)LX/1zl;

    move-result-object v5

    check-cast v5, LX/1zl;

    invoke-direct {p0, v3, v4, v5}, LX/1zj;-><init>(Landroid/content/Context;LX/1zk;LX/1zl;)V

    .line 353975
    move-object v0, p0

    .line 353976
    sput-object v0, LX/1zj;->d:LX/1zj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353977
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353978
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353979
    :cond_1
    sget-object v0, LX/1zj;->d:LX/1zj;

    return-object v0

    .line 353980
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353981
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/22E;LX/1zo;)LX/1zx;
    .locals 5

    .prologue
    .line 353982
    sget-object v0, LX/22G;->a:[I

    invoke-virtual {p2}, LX/1zo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353983
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Asset for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353984
    :pswitch_0
    new-instance v0, LX/2ys;

    iget-object v1, p0, LX/1zj;->a:Landroid/content/Context;

    invoke-static {p1, p2}, LX/1zk;->a(LX/22E;LX/1zo;)LX/22F;

    move-result-object v2

    .line 353985
    iget v3, v2, LX/22F;->c:I

    move v2, v3

    .line 353986
    invoke-direct {v0, v1, v2}, LX/2ys;-><init>(Landroid/content/Context;I)V

    .line 353987
    :goto_0
    return-object v0

    .line 353988
    :pswitch_1
    iget-object v0, p0, LX/1zj;->c:LX/1zl;

    .line 353989
    const-string v1, "apk_faces_"

    .line 353990
    iget v2, p1, LX/22E;->l:I

    move v2, v2

    .line 353991
    invoke-static {v1, v2}, LX/1zl;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 353992
    invoke-static {v0, v1}, LX/1zl;->b(LX/1zl;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353993
    :goto_1
    new-instance v0, LX/9Ao;

    iget-object v1, p0, LX/1zj;->c:LX/1zl;

    .line 353994
    iget v2, p1, LX/22E;->l:I

    move v2, v2

    .line 353995
    sget-object v3, LX/1zo;->LARGE:LX/1zo;

    invoke-direct {p0, p1, v3}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/9Ao;-><init>(LX/1zl;ILX/1zx;)V

    goto :goto_0

    .line 353996
    :cond_0
    iget-object v2, v0, LX/1zl;->f:Ljava/util/Map;

    iget-object v3, v0, LX/1zl;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;

    invoke-direct {v4, v0, p1}, Lcom/facebook/feedback/reactions/ui/ReactionsFaceDataCache$PrepareAPKFaceRunnable;-><init>(LX/1zl;LX/22E;)V

    const p2, -0x76e15387

    invoke-static {v3, v4, p2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 353997
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/1zj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/1zt;Z)LX/1zt;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 353998
    invoke-static {p1}, LX/22E;->a(I)LX/22E;

    move-result-object v8

    .line 353999
    if-nez v8, :cond_0

    .line 354000
    const/4 p2, 0x0

    .line 354001
    :goto_0
    return-object p2

    .line 354002
    :cond_0
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 354003
    if-ne v0, p1, :cond_2

    .line 354004
    iget v0, v8, LX/22E;->r:I

    move v0, v0

    .line 354005
    invoke-direct {p0, v0}, LX/1zj;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 354006
    iget v0, v8, LX/22E;->s:I

    move v2, v0

    .line 354007
    sget-object v0, LX/1zo;->SMALL:LX/1zo;

    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v4

    if-eqz p3, :cond_1

    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    :goto_1
    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v5

    sget-object v0, LX/1zo;->TAB_ICONS:LX/1zo;

    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v6

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, LX/1zt;->a(Ljava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/1zj;->b:LX/1zk;

    invoke-virtual {v0}, LX/1zk;->a()LX/1zo;

    move-result-object v0

    goto :goto_1

    .line 354008
    :cond_2
    new-instance v4, LX/1zt;

    .line 354009
    iget v0, v8, LX/22E;->r:I

    move v0, v0

    .line 354010
    invoke-direct {p0, v0}, LX/1zj;->a(I)Ljava/lang/String;

    move-result-object v6

    .line 354011
    iget v0, v8, LX/22E;->s:I

    move v7, v0

    .line 354012
    sget-object v0, LX/1zo;->SMALL:LX/1zo;

    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v9

    if-eqz p3, :cond_3

    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    :goto_2
    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v10

    sget-object v0, LX/1zo;->TAB_ICONS:LX/1zo;

    invoke-direct {p0, v8, v0}, LX/1zj;->a(LX/22E;LX/1zo;)LX/1zx;

    move-result-object v11

    move v5, p1

    move v8, v3

    invoke-direct/range {v4 .. v11}, LX/1zt;-><init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    move-object p2, v4

    goto :goto_0

    :cond_3
    iget-object v0, p0, LX/1zj;->b:LX/1zk;

    invoke-virtual {v0}, LX/1zk;->a()LX/1zo;

    move-result-object v0

    goto :goto_2
.end method

.method public final a()[I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 354013
    sget-object v1, LX/22E;->k:LX/0Px;

    move-object v1, v1

    .line 354014
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    new-array v4, v1, [I

    .line 354015
    sget-object v1, LX/22E;->k:LX/0Px;

    move-object v5, v1

    .line 354016
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22E;

    .line 354017
    add-int/lit8 v3, v2, 0x1

    .line 354018
    iget p0, v0, LX/22E;->l:I

    move v0, p0

    .line 354019
    aput v0, v4, v2

    .line 354020
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_0

    .line 354021
    :cond_0
    return-object v4
.end method
