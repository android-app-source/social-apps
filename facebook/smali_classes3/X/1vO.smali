.class public final LX/1vO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Pg;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/0Pk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([LX/1vM;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 342321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342322
    new-instance v3, Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-direct {v3, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 342323
    array-length v4, p1

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, p1, v0

    .line 342324
    iget v6, v5, LX/1vM;->b:I

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 342325
    iget v6, v5, LX/1vM;->c:I

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 342326
    iget v6, v5, LX/1vM;->a:I

    sparse-switch v6, :sswitch_data_0

    .line 342327
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342328
    :sswitch_0
    iget v6, v5, LX/1vM;->a:I

    new-instance v7, LX/0Pp;

    iget v8, v5, LX/1vM;->d:I

    iget v9, v5, LX/1vM;->e:I

    iget v10, v5, LX/1vM;->f:I

    iget v5, v5, LX/1vM;->g:I

    invoke-direct {v7, v8, v9, v10, v5}, LX/0Pp;-><init>(IIII)V

    invoke-virtual {v3, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 342329
    :sswitch_1
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 342330
    new-instance v7, Lcom/facebook/loom/config/QPLTraceControlConfiguration;

    iget v8, v5, LX/1vM;->d:I

    iget v9, v5, LX/1vM;->e:I

    iget v10, v5, LX/1vM;->g:I

    invoke-direct {v7, v8, v9, v10}, Lcom/facebook/loom/config/QPLTraceControlConfiguration;-><init>(III)V

    .line 342331
    iget v8, v5, LX/1vM;->f:I

    invoke-virtual {v6, v8, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 342332
    iget v5, v5, LX/1vM;->a:I

    new-instance v7, LX/0Pl;

    invoke-direct {v7, v6}, LX/0Pl;-><init>(Landroid/util/SparseArray;)V

    invoke-virtual {v3, v5, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 342333
    :cond_0
    iput-object v3, p0, LX/1vO;->c:Landroid/util/SparseArray;

    .line 342334
    iput v2, p0, LX/1vO;->a:I

    .line 342335
    iput v1, p0, LX/1vO;->b:I

    .line 342336
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)LX/0Pk;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 342337
    iget-object v0, p0, LX/1vO;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pk;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 342338
    iget v0, p0, LX/1vO;->a:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 342339
    iget v0, p0, LX/1vO;->b:I

    return v0
.end method
