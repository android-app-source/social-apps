.class public final enum LX/21B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21B;

.field public static final enum DEFAULT_FEEDBACK:LX/21B;

.field public static final enum INSTANT_ARTICLE:LX/21B;

.field public static final enum STORY_PERMALINK:LX/21B;

.field public static final enum THREADED_FEEDBACK:LX/21B;

.field public static final enum THREADED_PERMALINK:LX/21B;

.field public static final enum UNKNOWN:LX/21B;

.field public static final enum VIDEO_FEEDBACK:LX/21B;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 356741
    new-instance v0, LX/21B;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->UNKNOWN:LX/21B;

    .line 356742
    new-instance v0, LX/21B;

    const-string v1, "DEFAULT_FEEDBACK"

    invoke-direct {v0, v1, v4}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->DEFAULT_FEEDBACK:LX/21B;

    .line 356743
    new-instance v0, LX/21B;

    const-string v1, "THREADED_FEEDBACK"

    invoke-direct {v0, v1, v5}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->THREADED_FEEDBACK:LX/21B;

    .line 356744
    new-instance v0, LX/21B;

    const-string v1, "STORY_PERMALINK"

    invoke-direct {v0, v1, v6}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->STORY_PERMALINK:LX/21B;

    .line 356745
    new-instance v0, LX/21B;

    const-string v1, "THREADED_PERMALINK"

    invoke-direct {v0, v1, v7}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->THREADED_PERMALINK:LX/21B;

    .line 356746
    new-instance v0, LX/21B;

    const-string v1, "VIDEO_FEEDBACK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->VIDEO_FEEDBACK:LX/21B;

    .line 356747
    new-instance v0, LX/21B;

    const-string v1, "INSTANT_ARTICLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/21B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21B;->INSTANT_ARTICLE:LX/21B;

    .line 356748
    const/4 v0, 0x7

    new-array v0, v0, [LX/21B;

    sget-object v1, LX/21B;->UNKNOWN:LX/21B;

    aput-object v1, v0, v3

    sget-object v1, LX/21B;->DEFAULT_FEEDBACK:LX/21B;

    aput-object v1, v0, v4

    sget-object v1, LX/21B;->THREADED_FEEDBACK:LX/21B;

    aput-object v1, v0, v5

    sget-object v1, LX/21B;->STORY_PERMALINK:LX/21B;

    aput-object v1, v0, v6

    sget-object v1, LX/21B;->THREADED_PERMALINK:LX/21B;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/21B;->VIDEO_FEEDBACK:LX/21B;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/21B;->INSTANT_ARTICLE:LX/21B;

    aput-object v2, v0, v1

    sput-object v0, LX/21B;->$VALUES:[LX/21B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 356749
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/21B;
    .locals 1

    .prologue
    .line 356750
    const-class v0, LX/21B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21B;

    return-object v0
.end method

.method public static values()[LX/21B;
    .locals 1

    .prologue
    .line 356751
    sget-object v0, LX/21B;->$VALUES:[LX/21B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21B;

    return-object v0
.end method
