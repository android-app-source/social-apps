.class public final LX/1yh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1ye;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public final synthetic m:LX/1ye;


# direct methods
.method public constructor <init>(LX/1ye;)V
    .locals 1

    .prologue
    .line 351038
    iput-object p1, p0, LX/1yh;->m:LX/1ye;

    .line 351039
    move-object v0, p1

    .line 351040
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 351041
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351042
    const-string v0, "FeedStoryHeaderActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 351043
    if-ne p0, p1, :cond_1

    .line 351044
    :cond_0
    :goto_0
    return v0

    .line 351045
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 351046
    goto :goto_0

    .line 351047
    :cond_3
    check-cast p1, LX/1yh;

    .line 351048
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 351049
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 351050
    if-eq v2, v3, :cond_0

    .line 351051
    iget-object v2, p0, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 351052
    goto :goto_0

    .line 351053
    :cond_5
    iget-object v2, p1, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 351054
    :cond_6
    iget-object v2, p0, LX/1yh;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1yh;->b:LX/1Pb;

    iget-object v3, p1, LX/1yh;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 351055
    goto :goto_0

    .line 351056
    :cond_8
    iget-object v2, p1, LX/1yh;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 351057
    :cond_9
    iget-boolean v2, p0, LX/1yh;->c:Z

    iget-boolean v3, p1, LX/1yh;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 351058
    goto :goto_0

    .line 351059
    :cond_a
    iget-boolean v2, p0, LX/1yh;->d:Z

    iget-boolean v3, p1, LX/1yh;->d:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 351060
    goto :goto_0

    .line 351061
    :cond_b
    iget-boolean v2, p0, LX/1yh;->e:Z

    iget-boolean v3, p1, LX/1yh;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 351062
    goto :goto_0

    .line 351063
    :cond_c
    iget-boolean v2, p0, LX/1yh;->f:Z

    iget-boolean v3, p1, LX/1yh;->f:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 351064
    goto :goto_0

    .line 351065
    :cond_d
    iget v2, p0, LX/1yh;->g:I

    iget v3, p1, LX/1yh;->g:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 351066
    goto :goto_0

    .line 351067
    :cond_e
    iget v2, p0, LX/1yh;->h:I

    iget v3, p1, LX/1yh;->h:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 351068
    goto :goto_0

    .line 351069
    :cond_f
    iget v2, p0, LX/1yh;->i:I

    iget v3, p1, LX/1yh;->i:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 351070
    goto :goto_0

    .line 351071
    :cond_10
    iget v2, p0, LX/1yh;->j:I

    iget v3, p1, LX/1yh;->j:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 351072
    goto :goto_0

    .line 351073
    :cond_11
    iget v2, p0, LX/1yh;->k:I

    iget v3, p1, LX/1yh;->k:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 351074
    goto/16 :goto_0

    .line 351075
    :cond_12
    iget v2, p0, LX/1yh;->l:I

    iget v3, p1, LX/1yh;->l:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 351076
    goto/16 :goto_0
.end method
