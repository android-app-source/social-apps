.class public LX/1wH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/feed/ui/api/FeedMenuHelper$IFeedUnitMenuOptions",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1SX;


# direct methods
.method public constructor <init>(LX/1SX;)V
    .locals 0

    .prologue
    .line 345709
    iput-object p1, p0, LX/1wH;->a:LX/1SX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/1A0;)I
    .locals 2

    .prologue
    .line 345699
    sget-object v0, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne p1, v0, :cond_0

    .line 345700
    sget-object v1, LX/AkY;->d:[I

    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->m()LX/2qY;

    move-result-object v0

    invoke-virtual {v0}, LX/2qY;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 345701
    const v0, 0x7f081133

    .line 345702
    :goto_0
    return v0

    .line 345703
    :pswitch_0
    const v0, 0x7f081132

    goto :goto_0

    .line 345704
    :pswitch_1
    const v0, 0x7f081134

    goto :goto_0

    .line 345705
    :cond_0
    sget-object v1, LX/AkY;->d:[I

    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->m()LX/2qY;

    move-result-object v0

    invoke-virtual {v0}, LX/2qY;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 345706
    const v0, 0x7f081137

    goto :goto_0

    .line 345707
    :pswitch_2
    const v0, 0x7f081136

    goto :goto_0

    .line 345708
    :pswitch_3
    const v0, 0x7f081138

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 345694
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->f:LX/1Sa;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Sa;->a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;

    move-result-object v2

    .line 345695
    iget v0, v2, LX/AEr;->a:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 345696
    instance-of v0, v1, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 345697
    check-cast v0, LX/3Ai;

    iget v2, v2, LX/AEr;->b:I

    invoke-virtual {v0, v2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 345698
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLStorySaveInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 345693
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345687
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, p3, v1}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345688
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->g:LX/1Sj;

    const-string v1, "caret_menu"

    iget-object v2, p0, LX/1wH;->a:LX/1SX;

    invoke-static {v2}, LX/1SX;->g(LX/1SX;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 345689
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v1, v0, LX/1SX;->l:LX/0bH;

    new-instance v2, LX/1Zg;

    .line 345690
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345691
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-direct {v2, v0, v3}, LX/1Zg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 345692
    return-void
.end method

.method public static b(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 345682
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->f:LX/1Sa;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->l()Lcom/facebook/graphql/enums/GraphQLStorySaveType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Sa;->a(Lcom/facebook/graphql/enums/GraphQLStorySaveType;)LX/AEr;

    move-result-object v2

    .line 345683
    iget v0, v2, LX/AEr;->c:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 345684
    instance-of v0, v1, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 345685
    check-cast v0, LX/3Ai;

    iget v2, v2, LX/AEr;->d:I

    invoke-virtual {v0, v2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 345686
    :cond_0
    return-object v1
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 345570
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345651
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v0

    .line 345652
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345653
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v5

    .line 345654
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v6

    .line 345655
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v7

    .line 345656
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v8

    .line 345657
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v10

    .line 345658
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v9

    .line 345659
    invoke-static {p2}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 345660
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 345661
    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v11

    .line 345662
    :goto_1
    if-eqz v5, :cond_1

    const v0, 0x7f08106a

    .line 345663
    :goto_2
    if-eqz v5, :cond_2

    sget-object v1, LX/Al1;->TURN_OFF_NOTIFICATION:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345664
    :goto_3
    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v12

    .line 345665
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    invoke-interface {v12}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345666
    new-instance v0, LX/Akj;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v11}, LX/Akj;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v12, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345667
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    .line 345668
    const v1, 0x7f0208b2

    move v1, v1

    .line 345669
    invoke-virtual {v0, v12, v1, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345670
    return-void

    .line 345671
    :cond_0
    invoke-static {p2}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v0

    .line 345672
    if-eqz v0, :cond_4

    .line 345673
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    .line 345674
    :goto_4
    move-object v11, v0

    .line 345675
    goto :goto_1

    .line 345676
    :cond_1
    const v0, 0x7f081069

    goto :goto_2

    .line 345677
    :cond_2
    sget-object v1, LX/Al1;->TURN_ON_NOTIFICATION:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 345678
    :cond_4
    invoke-static {p2}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 345679
    if-eqz v0, :cond_5

    .line 345680
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 345681
    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static e(LX/1wH;Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 345642
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "The data below can be used to debug the selected feed story. If available, the value in \'serialized\' field can be used to reproduce it."

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 345643
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345644
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345645
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345646
    :try_start_0
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->e:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->g()LX/4ps;

    move-result-object v0

    invoke-virtual {v0}, LX/4ps;->a()LX/4ps;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 345647
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 345648
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 345649
    :catch_0
    move-exception v0

    .line 345650
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception occured while converting FeedUnit to JSON: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/28F;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345632
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345633
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345634
    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    iget-object v1, v1, LX/1SX;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 345635
    :cond_0
    :goto_0
    return-void

    .line 345636
    :cond_1
    invoke-static {v0}, LX/1wH;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 345637
    invoke-direct {p0, p1, p2}, LX/1wH;->c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    .line 345638
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 345639
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 345640
    if-eqz v0, :cond_0

    .line 345641
    invoke-virtual {p0, p1, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345622
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345623
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345624
    const v1, 0x7f081084

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 345625
    sget-object v2, LX/63n;->COPY_LINK:LX/63n;

    invoke-virtual {v2}, LX/63n;->name()Ljava/lang/String;

    move-result-object v2

    .line 345626
    iget-object v3, p0, LX/1wH;->a:LX/1SX;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, p2, v4, v2, v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345627
    new-instance v3, LX/Akm;

    invoke-direct {v3, p0, p2, v2, p3}, LX/Akm;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345628
    iget-object v2, p0, LX/1wH;->a:LX/1SX;

    .line 345629
    const v3, 0x7f0208fd

    move v3, v3

    .line 345630
    invoke-virtual {v2, v1, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345631
    return-void
.end method

.method public a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345610
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345611
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 345612
    invoke-virtual {p0, v0}, LX/1wH;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345613
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 345614
    iget-object v2, p0, LX/1wH;->a:LX/1SX;

    invoke-virtual {v2}, LX/1SX;->c()LX/0Or;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "Report issue to Feed Team (FB Only)"

    .line 345615
    :goto_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    .line 345616
    new-instance p2, LX/Akt;

    invoke-direct {p2, p0, v1, v0}, LX/Akt;-><init>(LX/1wH;Landroid/content/Context;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-interface {v2, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345617
    iget-object p2, p0, LX/1wH;->a:LX/1SX;

    .line 345618
    const p3, 0x7f0209ae

    move p3, p3

    .line 345619
    invoke-virtual {p2, v2, p3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345620
    :cond_0
    return-void

    .line 345621
    :cond_1
    const-string v2, "Mail story debug info (FB Only)"

    goto :goto_0
.end method

.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/0Ot;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 345597
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345598
    move-object v7, v0

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345599
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 345600
    sget-object v0, LX/Al1;->VIEW_VIDEO_INSIGHT:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345601
    const v0, 0x7f08112c

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 345602
    instance-of v0, v8, LX/3Ai;

    if-eqz v0, :cond_0

    move-object v0, v8

    .line 345603
    check-cast v0, LX/3Ai;

    const v1, 0x7f08112d

    invoke-virtual {v0, v1}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 345604
    :cond_0
    new-instance v0, LX/Akn;

    move-object v1, p0

    move-object v2, p2

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/Akn;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/0Ot;Landroid/view/View;)V

    invoke-interface {v8, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345605
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    .line 345606
    const v1, 0x7f020772

    move v1, v1

    .line 345607
    invoke-virtual {v0, v8, v1, v7}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345608
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    invoke-interface {v8}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, p2, v1, v3, v9}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345609
    return-void
.end method

.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345571
    invoke-static {p2}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 345572
    if-nez v0, :cond_1

    .line 345573
    :cond_0
    :goto_0
    return-void

    .line 345574
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 345575
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 345576
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    .line 345577
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->bx()I

    move-result v7

    .line 345578
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    .line 345579
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 345580
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/BUA;

    .line 345581
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/19w;

    invoke-virtual {v0, v5}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 345582
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-long v2, v7

    invoke-static {v2, v3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 345583
    sget-object v3, LX/AkY;->d:[I

    iget-object v2, p0, LX/1wH;->a:LX/1SX;

    iget-object v2, v2, LX/1SX;->G:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tQ;

    invoke-virtual {v2}, LX/0tQ;->m()LX/2qY;

    move-result-object v2

    invoke-virtual {v2}, LX/2qY;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 345584
    const v2, 0x7f08112f

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v0, v2

    .line 345585
    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v10

    .line 345586
    instance-of v0, v10, LX/3Ai;

    if-eqz v0, :cond_2

    move-object v0, v10

    .line 345587
    check-cast v0, LX/3Ai;

    const v1, 0x7f081131

    invoke-virtual {v0, v1}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 345588
    :cond_2
    sget-object v0, LX/Al1;->SAVE_OFFLINE:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345589
    new-instance v0, LX/Aks;

    move-object v1, p0

    move-object v2, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v9}, LX/Aks;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/feed/rows/core/props/FeedProps;LX/BUA;)V

    invoke-interface {v10, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345590
    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    .line 345591
    const v0, 0x7f020841

    move v2, v0

    .line 345592
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345593
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v10, v2, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345594
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    invoke-interface {v10}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    goto/16 :goto_0

    .line 345595
    :pswitch_0
    const v2, 0x7f08112e

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 345596
    :pswitch_1
    const v2, 0x7f081130

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 345462
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345463
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v0}, LX/1wH;->c(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 345464
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 345465
    const/4 v0, 0x0

    .line 345466
    :goto_0
    return v0

    .line 345467
    :cond_0
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345468
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-static {v0}, LX/1wH;->a(Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 345469
    invoke-static {p1}, LX/2vB;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345470
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345471
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345472
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    new-instance v2, LX/Akk;

    invoke-direct {v2, p0}, LX/Akk;-><init>(LX/1wH;)V

    invoke-static {v1, v2}, LX/0Ph;->f(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 345473
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eI()Z

    move-result v3

    .line 345474
    if-eqz v3, :cond_0

    const v1, 0x7f082504

    move v2, v1

    .line 345475
    :goto_0
    if-eqz v3, :cond_1

    sget-object v1, LX/Al1;->MARK_AS_AVAILABLE:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v1

    .line 345476
    :goto_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 345477
    iget-object v4, p0, LX/1wH;->a:LX/1SX;

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, p2, v5, v1, v6}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345478
    new-instance v4, LX/Akl;

    invoke-direct {v4, p0, p2, v1}, LX/Akl;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345479
    if-eqz v3, :cond_2

    .line 345480
    const v1, 0x7f0207db

    move v1, v1

    .line 345481
    :goto_2
    iget-object v3, p0, LX/1wH;->a:LX/1SX;

    invoke-virtual {v3, v2, v1, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345482
    return-void

    .line 345483
    :cond_0
    const v1, 0x7f082503

    move v2, v1

    goto :goto_0

    .line 345484
    :cond_1
    sget-object v1, LX/Al1;->MARK_AS_SOLD:LX/Al1;

    invoke-virtual {v1}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 345485
    :cond_2
    const v1, 0x7f0207c8

    move v1, v1

    .line 345486
    goto :goto_2
.end method

.method public final b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345487
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 345488
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345489
    const v0, 0x7f08106c

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 345490
    sget-object v0, LX/Al1;->EDIT_REVIEW:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v4

    .line 345491
    invoke-interface {v7}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    .line 345492
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v3, v4, v1}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345493
    new-instance v0, LX/Aku;

    move-object v1, p0

    move-object v2, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/Aku;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345494
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    .line 345495
    const v1, 0x7f020952

    move v1, v1

    .line 345496
    invoke-virtual {v0, v7, v1, v5}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345497
    return-void
.end method

.method public final b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345498
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345499
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    invoke-static {v5}, LX/1SX;->l(LX/1SX;)Ljava/lang/String;

    move-result-object v6

    .line 345500
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v11

    .line 345501
    invoke-static/range {p2 .. p2}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 345502
    if-eqz v5, :cond_0

    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v10

    .line 345503
    :goto_0
    if-eqz v10, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->E:LX/0Ot;

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->E:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BUA;

    move-object v9, v5

    .line 345504
    :goto_1
    if-eqz v10, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->F:LX/0Ot;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->F:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/19w;

    .line 345505
    :goto_2
    if-eqz v5, :cond_3

    invoke-virtual {v5, v10}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v8, 0x1

    .line 345506
    :goto_3
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v7, v12, :cond_4

    if-nez v8, :cond_4

    .line 345507
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->i:LX/16H;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1wH;->a:LX/1SX;

    invoke-static {v8}, LX/1SX;->g(LX/1SX;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, LX/16H;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345508
    sget-object v5, LX/Al1;->SAVE:LX/Al1;

    invoke-virtual {v5}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v5

    .line 345509
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, LX/1wH;->a(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;

    move-result-object v6

    .line 345510
    new-instance v7, LX/Akq;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v7, v0, v1, v5, v2}, LX/Akq;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/view/View;)V

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345511
    move-object/from16 v0, p0

    iget-object v7, v0, LX/1wH;->a:LX/1SX;

    invoke-static {}, LX/Al4;->a()I

    move-result v8

    invoke-virtual {v7, v6, v8, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345512
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1wH;->a:LX/1SX;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v6, v5, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345513
    :goto_4
    return-void

    .line 345514
    :cond_0
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 345515
    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    .line 345516
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 345517
    :cond_3
    const/4 v8, 0x0

    goto :goto_3

    .line 345518
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, LX/1wH;->a:LX/1SX;

    iget-object v7, v7, LX/1SX;->i:LX/16H;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, LX/1wH;->a:LX/1SX;

    invoke-static {v13}, LX/1SX;->g(LX/1SX;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v6, v12, v13}, LX/16H;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345519
    sget-object v6, LX/Al1;->UNSAVE:LX/Al1;

    invoke-virtual {v6}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v7

    .line 345520
    if-eqz v8, :cond_6

    invoke-virtual {v5, v10}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 345521
    invoke-virtual {v5, v10}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v11

    .line 345522
    iget-object v5, v11, LX/2fs;->c:LX/1A0;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, LX/1wH;->a(LX/1A0;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 345523
    iget-object v5, v11, LX/2fs;->c:LX/1A0;

    sget-object v12, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v5, v12, :cond_5

    instance-of v5, v6, LX/3Ai;

    if-eqz v5, :cond_5

    move-object v5, v6

    .line 345524
    check-cast v5, LX/3Ai;

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f081135

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-wide v0, v11, LX/2fs;->a:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 345525
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    invoke-static {}, LX/Al4;->d()I

    move-result v11

    invoke-virtual {v5, v6, v11, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    move-object v11, v6

    .line 345526
    :goto_5
    new-instance v4, LX/Akr;

    move-object/from16 v5, p0

    move-object/from16 v6, p2

    invoke-direct/range {v4 .. v10}, LX/Akr;-><init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZLX/BUA;Ljava/lang/String;)V

    invoke-interface {v11, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345527
    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 345528
    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 345529
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1wH;->a:LX/1SX;

    invoke-interface {v11}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v5, v7, v6}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    goto/16 :goto_4

    .line 345530
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, LX/1wH;->b(LX/1wH;Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStorySaveInfo;)Landroid/view/MenuItem;

    move-result-object v6

    .line 345531
    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    invoke-static {}, LX/Al4;->a()I

    move-result v11

    invoke-virtual {v5, v6, v11, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    move-object v11, v6

    goto :goto_5
.end method

.method public b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 345532
    invoke-static {p1}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 345533
    invoke-virtual {p0, p1}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->G:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 345534
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345535
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345536
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x41e065f

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x403827a

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 345537
    :goto_0
    return v0

    .line 345538
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 345539
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 345540
    goto :goto_0

    .line 345541
    :cond_3
    const v2, -0x674bf213

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 345542
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bE()I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v1

    .line 345543
    goto :goto_0

    .line 345544
    :cond_5
    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->x:LX/0ad;

    sget-short v2, LX/0fe;->bN:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public c(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 345545
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->E_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    iget-object v1, v1, LX/1SX;->v:LX/0Or;

    if-nez v1, :cond_1

    .line 345546
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    iget-object v1, v1, LX/1SX;->v:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 345547
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345548
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 345549
    instance-of v1, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v1, :cond_2

    .line 345550
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 345551
    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    iget-object v1, v1, LX/1SX;->p:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/1wH;->a:LX/1SX;

    .line 345552
    const/4 v4, 0x0

    .line 345553
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v3

    .line 345554
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    move v3, v4

    .line 345555
    :goto_0
    move v3, v3

    .line 345556
    move v0, v3

    .line 345557
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 345558
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 345559
    goto :goto_1

    :cond_2
    move v0, v2

    .line 345560
    goto :goto_1

    .line 345561
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    move v5, v4

    :goto_2
    if-ge v5, p0, :cond_5

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 345562
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, v1, LX/1SX;->J:LX/1Sp;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1Sp;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 345563
    const/4 v3, 0x1

    goto :goto_0

    .line 345564
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_5
    move v3, v4

    .line 345565
    goto :goto_0
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 345566
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 345567
    :goto_0
    return v0

    .line 345568
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345569
    invoke-static {p1}, LX/1wH;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
