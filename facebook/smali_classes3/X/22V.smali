.class public final LX/22V;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/22T;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/22U;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/22T",
            "<TE;>.PhotoAttachmentImageVisibilityComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/22T;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/22T;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 359848
    iput-object p1, p0, LX/22V;->b:LX/22T;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 359849
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "stateKey"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "contentId"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/22V;->c:[Ljava/lang/String;

    .line 359850
    iput v3, p0, LX/22V;->d:I

    .line 359851
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/22V;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/22V;LX/1De;IILX/22U;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/22T",
            "<TE;>.PhotoAttachmentImageVisibilityComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 359844
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 359845
    iput-object p4, p0, LX/22V;->a:LX/22U;

    .line 359846
    iget-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 359847
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/22V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/22T",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359841
    iget-object v0, p0, LX/22V;->a:LX/22U;

    iput-object p1, v0, LX/22U;->a:LX/1Pq;

    .line 359842
    iget-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359843
    return-object p0
.end method

.method public final a(LX/22Q;)LX/22V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/22Q;",
            ")",
            "LX/22T",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359852
    iget-object v0, p0, LX/22V;->a:LX/22U;

    iput-object p1, v0, LX/22U;->c:LX/22Q;

    .line 359853
    iget-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359854
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/22T",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359838
    iget-object v0, p0, LX/22V;->a:LX/22U;

    iput-object p1, v0, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 359839
    iget-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359840
    return-object p0
.end method

.method public final a(Ljava/lang/Integer;)LX/22V;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "LX/22T",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359835
    iget-object v0, p0, LX/22V;->a:LX/22U;

    iput-object p1, v0, LX/22U;->d:Ljava/lang/Integer;

    .line 359836
    iget-object v0, p0, LX/22V;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359837
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 359831
    invoke-super {p0}, LX/1X5;->a()V

    .line 359832
    const/4 v0, 0x0

    iput-object v0, p0, LX/22V;->a:LX/22U;

    .line 359833
    iget-object v0, p0, LX/22V;->b:LX/22T;

    iget-object v0, v0, LX/22T;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 359834
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/22T;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 359821
    iget-object v1, p0, LX/22V;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/22V;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/22V;->d:I

    if-ge v1, v2, :cond_2

    .line 359822
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 359823
    :goto_0
    iget v2, p0, LX/22V;->d:I

    if-ge v0, v2, :cond_1

    .line 359824
    iget-object v2, p0, LX/22V;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 359825
    iget-object v2, p0, LX/22V;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359826
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359827
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359828
    :cond_2
    iget-object v0, p0, LX/22V;->a:LX/22U;

    .line 359829
    invoke-virtual {p0}, LX/22V;->a()V

    .line 359830
    return-object v0
.end method
