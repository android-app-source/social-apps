.class public LX/1xF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xF",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348055
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348056
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xF;->b:LX/0Zi;

    .line 348057
    iput-object p1, p0, LX/1xF;->a:LX/0Ot;

    .line 348058
    return-void
.end method

.method public static a(LX/0QB;)LX/1xF;
    .locals 4

    .prologue
    .line 348044
    const-class v1, LX/1xF;

    monitor-enter v1

    .line 348045
    :try_start_0
    sget-object v0, LX/1xF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348046
    sput-object v2, LX/1xF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348047
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348048
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348049
    new-instance v3, LX/1xF;

    const/16 p0, 0x1e84

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xF;-><init>(LX/0Ot;)V

    .line 348050
    move-object v0, v3

    .line 348051
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348052
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348053
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348054
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348042
    invoke-static {}, LX/1dS;->b()V

    .line 348043
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1432dbc3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 348012
    iget-object v1, p0, LX/1xF;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 348013
    if-nez p4, :cond_0

    invoke-static {p3}, LX/1mh;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 348014
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, LX/1mh;->a(II)I

    move-result p4

    .line 348015
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p3, p4, v1, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 348016
    const/16 v1, 0x1f

    const v2, -0x65b9a69b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348039
    iget-object v0, p0, LX/1xF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 348040
    new-instance v0, LX/8xF;

    invoke-direct {v0, p1}, LX/8xF;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 348041
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 348038
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 9

    .prologue
    .line 348024
    check-cast p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    .line 348025
    iget-object v0, p0, LX/1xF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;

    move-object v1, p2

    check-cast v1, LX/8xF;

    iget-object v2, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    iget-object v3, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    iget-object v5, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    iget-object v6, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    iget-object v7, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    iget-object v8, p3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 348026
    invoke-virtual {v1, v7}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 348027
    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->c:LX/0yc;

    invoke-virtual {p0}, LX/0yc;->b()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 348028
    const/4 p0, 0x1

    .line 348029
    iput-boolean p0, v1, LX/8wv;->t:Z

    .line 348030
    :goto_0
    return-void

    .line 348031
    :cond_0
    iget-object p0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 348032
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, v8, p0, v4}, LX/8xF;->b(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 348033
    invoke-virtual {v1, v5}, LX/8xF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348034
    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->b:LX/1AV;

    invoke-virtual {p0, v1, v6}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 348035
    const/4 p0, 0x0

    .line 348036
    iput-boolean p0, v1, LX/8wv;->t:Z

    .line 348037
    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 348023
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 348018
    iget-object v0, p0, LX/1xF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;

    check-cast p2, LX/8xF;

    .line 348019
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 348020
    invoke-virtual {p2}, LX/8xF;->q()V

    .line 348021
    iget-object p0, v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentSpec;->b:LX/1AV;

    invoke-virtual {p0, p2}, LX/1AV;->a(Landroid/view/View;)V

    .line 348022
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 348017
    const/4 v0, 0x3

    return v0
.end method
