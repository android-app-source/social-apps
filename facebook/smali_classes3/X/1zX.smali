.class public LX/1zX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pe;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1EN;

.field private final b:LX/14w;


# direct methods
.method public constructor <init>(LX/1EN;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353552
    iput-object p1, p0, LX/1zX;->a:LX/1EN;

    .line 353553
    iput-object p2, p0, LX/1zX;->b:LX/14w;

    .line 353554
    return-void
.end method

.method public static a(LX/0QB;)LX/1zX;
    .locals 5

    .prologue
    .line 353555
    const-class v1, LX/1zX;

    monitor-enter v1

    .line 353556
    :try_start_0
    sget-object v0, LX/1zX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 353557
    sput-object v2, LX/1zX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 353558
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353559
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 353560
    new-instance p0, LX/1zX;

    invoke-static {v0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v3

    check-cast v3, LX/1EN;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-direct {p0, v3, v4}, LX/1zX;-><init>(LX/1EN;LX/14w;)V

    .line 353561
    move-object v0, p0

    .line 353562
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 353563
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1zX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353564
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 353565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1zX;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1Po;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 353566
    iget-object v0, p0, LX/1zX;->a:LX/1EN;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    sget-object v2, LX/An0;->BLINGBAR:LX/An0;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/view/View;LX/An0;)V

    .line 353567
    return-void
.end method
