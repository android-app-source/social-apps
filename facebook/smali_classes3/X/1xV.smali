.class public LX/1xV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yd;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348565
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348566
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xV;->b:LX/0Zi;

    .line 348567
    iput-object p1, p0, LX/1xV;->a:LX/0Ot;

    .line 348568
    return-void
.end method

.method public static a(LX/0QB;)LX/1xV;
    .locals 4

    .prologue
    .line 348569
    const-class v1, LX/1xV;

    monitor-enter v1

    .line 348570
    :try_start_0
    sget-object v0, LX/1xV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348571
    sput-object v2, LX/1xV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348572
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348573
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348574
    new-instance v3, LX/1xV;

    const/16 p0, 0x71d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xV;-><init>(LX/0Ot;)V

    .line 348575
    move-object v0, v3

    .line 348576
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348577
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348578
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 348580
    check-cast p2, LX/1yb;

    .line 348581
    iget-object v0, p0, LX/1xV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yd;

    iget-object v2, p2, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/1yb;->b:LX/1Pb;

    iget v4, p2, LX/1yb;->c:I

    iget-boolean v5, p2, LX/1yb;->d:Z

    iget-boolean v6, p2, LX/1yb;->e:Z

    iget-boolean v7, p2, LX/1yb;->f:Z

    iget-boolean v8, p2, LX/1yb;->g:Z

    iget-boolean v9, p2, LX/1yb;->h:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, LX/1yd;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;IZZZZZ)LX/1Dg;

    move-result-object v0

    .line 348582
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348583
    invoke-static {}, LX/1dS;->b()V

    .line 348584
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1yc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348585
    new-instance v1, LX/1yb;

    invoke-direct {v1, p0}, LX/1yb;-><init>(LX/1xV;)V

    .line 348586
    iget-object v2, p0, LX/1xV;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1yc;

    .line 348587
    if-nez v2, :cond_0

    .line 348588
    new-instance v2, LX/1yc;

    invoke-direct {v2, p0}, LX/1yc;-><init>(LX/1xV;)V

    .line 348589
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1yc;->a$redex0(LX/1yc;LX/1De;IILX/1yb;)V

    .line 348590
    move-object v1, v2

    .line 348591
    move-object v0, v1

    .line 348592
    return-object v0
.end method
