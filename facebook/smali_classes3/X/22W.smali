.class public LX/22W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1er;

.field public final b:LX/1L1;


# direct methods
.method public constructor <init>(LX/1er;LX/1L1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359856
    iput-object p1, p0, LX/22W;->a:LX/1er;

    .line 359857
    iget-object v0, p0, LX/22W;->a:LX/1er;

    invoke-virtual {v0}, LX/1er;->a()V

    .line 359858
    iput-object p2, p0, LX/22W;->b:LX/1L1;

    .line 359859
    return-void
.end method

.method public static a(LX/0QB;)LX/22W;
    .locals 5

    .prologue
    .line 359860
    const-class v1, LX/22W;

    monitor-enter v1

    .line 359861
    :try_start_0
    sget-object v0, LX/22W;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359862
    sput-object v2, LX/22W;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359865
    new-instance p0, LX/22W;

    invoke-static {v0}, LX/1er;->a(LX/0QB;)LX/1er;

    move-result-object v3

    check-cast v3, LX/1er;

    invoke-static {v0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v4

    check-cast v4, LX/1L1;

    invoke-direct {p0, v3, v4}, LX/22W;-><init>(LX/1er;LX/1L1;)V

    .line 359866
    move-object v0, p0

    .line 359867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
