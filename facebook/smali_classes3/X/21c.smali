.class public final enum LX/21c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21c;

.field public static final enum ICONS_EQUAL_WIDTH:LX/21c;

.field public static final enum ICONS_ONLY:LX/21c;

.field public static final enum ICONS_VARIABLE_WIDTH:LX/21c;

.field public static final enum NO_ICONS_EQUAL_WIDTH:LX/21c;

.field public static final enum NO_ICONS_VARIABLE_WIDTH:LX/21c;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 357371
    new-instance v0, LX/21c;

    const-string v1, "ICONS_EQUAL_WIDTH"

    invoke-direct {v0, v1, v2}, LX/21c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    .line 357372
    new-instance v0, LX/21c;

    const-string v1, "ICONS_VARIABLE_WIDTH"

    invoke-direct {v0, v1, v3}, LX/21c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21c;->ICONS_VARIABLE_WIDTH:LX/21c;

    .line 357373
    new-instance v0, LX/21c;

    const-string v1, "NO_ICONS_EQUAL_WIDTH"

    invoke-direct {v0, v1, v4}, LX/21c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21c;->NO_ICONS_EQUAL_WIDTH:LX/21c;

    .line 357374
    new-instance v0, LX/21c;

    const-string v1, "NO_ICONS_VARIABLE_WIDTH"

    invoke-direct {v0, v1, v5}, LX/21c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21c;->NO_ICONS_VARIABLE_WIDTH:LX/21c;

    .line 357375
    new-instance v0, LX/21c;

    const-string v1, "ICONS_ONLY"

    invoke-direct {v0, v1, v6}, LX/21c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21c;->ICONS_ONLY:LX/21c;

    .line 357376
    const/4 v0, 0x5

    new-array v0, v0, [LX/21c;

    sget-object v1, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    aput-object v1, v0, v2

    sget-object v1, LX/21c;->ICONS_VARIABLE_WIDTH:LX/21c;

    aput-object v1, v0, v3

    sget-object v1, LX/21c;->NO_ICONS_EQUAL_WIDTH:LX/21c;

    aput-object v1, v0, v4

    sget-object v1, LX/21c;->NO_ICONS_VARIABLE_WIDTH:LX/21c;

    aput-object v1, v0, v5

    sget-object v1, LX/21c;->ICONS_ONLY:LX/21c;

    aput-object v1, v0, v6

    sput-object v0, LX/21c;->$VALUES:[LX/21c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 357370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static hasIcons(LX/21c;)Z
    .locals 1

    .prologue
    .line 357377
    sget-object v0, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/21c;->ICONS_VARIABLE_WIDTH:LX/21c;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/21c;->ICONS_ONLY:LX/21c;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/21c;
    .locals 1

    .prologue
    .line 357369
    const-class v0, LX/21c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21c;

    return-object v0
.end method

.method public static values()[LX/21c;
    .locals 1

    .prologue
    .line 357368
    sget-object v0, LX/21c;->$VALUES:[LX/21c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21c;

    return-object v0
.end method
