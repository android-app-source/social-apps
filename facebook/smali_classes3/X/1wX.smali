.class public final LX/1wX;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "Lcom/facebook/zero/service/ZeroHeaderRequestManager;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/1wX;


# instance fields
.field private a:Z

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/zero/service/ZeroHeaderRequestManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346265
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 346266
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1wX;->a:Z

    .line 346267
    iput-object p2, p0, LX/1wX;->b:LX/0Ot;

    .line 346268
    return-void
.end method

.method public static a(LX/0QB;)LX/1wX;
    .locals 5

    .prologue
    .line 346269
    sget-object v0, LX/1wX;->c:LX/1wX;

    if-nez v0, :cond_1

    .line 346270
    const-class v1, LX/1wX;

    monitor-enter v1

    .line 346271
    :try_start_0
    sget-object v0, LX/1wX;->c:LX/1wX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 346272
    if-eqz v2, :cond_0

    .line 346273
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 346274
    new-instance v3, LX/1wX;

    const/16 v4, 0x13fa

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x245

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/1wX;-><init>(LX/0Ot;LX/0Ot;)V

    .line 346275
    move-object v0, v3

    .line 346276
    sput-object v0, LX/1wX;->c:LX/1wX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346277
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 346278
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346279
    :cond_1
    sget-object v0, LX/1wX;->c:LX/1wX;

    return-object v0

    .line 346280
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 346281
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 346282
    check-cast p3, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    const/4 v2, 0x0

    .line 346283
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 346284
    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 346285
    iget-object v0, p0, LX/1wX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346286
    const-string v0, "network"

    invoke-virtual {p3, v2, v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 346287
    :cond_0
    :goto_0
    return-void

    .line 346288
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1wX;->a:Z

    goto :goto_0

    .line 346289
    :cond_2
    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346290
    iget-boolean v0, p0, LX/1wX;->a:Z

    if-eqz v0, :cond_0

    .line 346291
    const-string v0, "network"

    invoke-virtual {p3, v2, v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 346292
    iput-boolean v2, p0, LX/1wX;->a:Z

    goto :goto_0
.end method
