.class public final LX/1y2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/1xv;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z

.field public final d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

.field private final e:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

.field public final f:Ljava/lang/Boolean;

.field private g:Landroid/text/SpannableStringBuilder;

.field public h:I

.field public i:Z

.field public j:LX/1PT;

.field public k:LX/1nV;


# direct methods
.method public constructor <init>(LX/1xv;Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 350164
    iput-object p1, p0, LX/1y2;->a:LX/1xv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350165
    iput v1, p0, LX/1y2;->h:I

    .line 350166
    iput-object p2, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 350167
    iget-object v0, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 350168
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 350169
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1y3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    move-result-object v0

    iput-object v0, p0, LX/1y2;->e:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    .line 350170
    iget-object v0, p0, LX/1y2;->e:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    invoke-static {v0}, LX/1y4;->a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;)Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    move-result-object v0

    iput-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    .line 350171
    iput v1, p0, LX/1y2;->h:I

    .line 350172
    iput-boolean p3, p0, LX/1y2;->c:Z

    .line 350173
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1y2;->f:Ljava/lang/Boolean;

    .line 350174
    return-void
.end method

.method private a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;ILX/0lF;)Landroid/text/Spannable;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 350154
    if-nez p1, :cond_1

    .line 350155
    const/4 v1, 0x0

    .line 350156
    :cond_0
    :goto_0
    return-object v1

    .line 350157
    :cond_1
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "%1$s"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 350158
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, LX/1y2;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 350159
    new-instance v2, LX/311;

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v2, v0, v3}, LX/311;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 350160
    iget-object v0, v2, LX/311;->a:Ljava/util/List;

    move-object v7, v0

    .line 350161
    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/312;

    sget v4, LX/1Uf;->a:I

    invoke-static {p0, p1}, LX/1y2;->a(LX/1y2;LX/1y5;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v3, p3

    move-object v6, p1

    invoke-static/range {v0 .. v6}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350162
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350163
    invoke-static {p1}, LX/4p9;->a(LX/1y6;)Z

    move-result v0

    invoke-static {p0, v1, v7, v0, v8}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;ZZ)V

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 1

    .prologue
    .line 350152
    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 350153
    invoke-static {v0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1y2;LX/1y5;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 349972
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-interface {p1}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-interface {p1}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V
    .locals 10

    .prologue
    .line 350148
    invoke-virtual {p2}, LX/312;->a()I

    move-result v2

    .line 350149
    invoke-virtual {p2}, LX/312;->c()I

    move-result v1

    .line 350150
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Uf;

    add-int v3, v2, v1

    const/4 v5, 0x1

    const/4 v7, 0x1

    move-object v1, p5

    move-object v4, p1

    move v6, p4

    move-object v8, p3

    move-object/from16 v9, p6

    invoke-virtual/range {v0 .. v9}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;)V

    .line 350151
    return-void
.end method

.method public static a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;LX/0lF;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/util/List",
            "<",
            "LX/312;",
            ">;",
            "Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLInterfaces$HeaderTitleSpannableBuilderGraphQL$Actors;",
            "Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLInterfaces$HeaderTitleSpannableBuilderGraphQL$To;",
            "LX/0lF;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 350132
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/312;

    sget v4, LX/1Uf;->a:I

    invoke-static {p0, p3}, LX/1y2;->a(LX/1y2;LX/1y5;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v3, p5

    move-object v6, p3

    invoke-static/range {v0 .. v6}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350133
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/312;

    sget v4, LX/1Uf;->a:I

    .line 350134
    invoke-virtual {p4}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 350135
    invoke-virtual {p4}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->d()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 350136
    :goto_0
    move-object v5, v0

    .line 350137
    move-object v0, p0

    move-object v1, p1

    move-object v3, p5

    move-object v6, p4

    invoke-static/range {v0 .. v6}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350138
    if-eqz p6, :cond_3

    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 350139
    :goto_1
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 350140
    if-ltz v0, :cond_0

    .line 350141
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v2, p0, LX/1y2;->a:LX/1xv;

    iget-object v2, v2, LX/1xv;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x11

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 350142
    new-instance v1, Landroid/text/style/RelativeSizeSpan;

    const v2, 0x3f6db6db

    invoke-direct {v1, v2}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x11

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 350143
    :cond_0
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350144
    invoke-static {p3}, LX/4p9;->a(LX/1y6;)Z

    move-result v1

    invoke-static {p4}, LX/4p9;->a(LX/1y6;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p4}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->hT_()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_2
    invoke-static {p0, p1, p2, v1, v0}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;ZZ)V

    .line 350145
    :cond_2
    return-void

    .line 350146
    :cond_3
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    .line 350147
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    invoke-static {p0, p4}, LX/1y2;->a(LX/1y2;LX/1y5;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Ljava/util/List",
            "<",
            "LX/312;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 350125
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350126
    :cond_0
    :goto_0
    return-void

    .line 350127
    :cond_1
    if-eqz p3, :cond_2

    .line 350128
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/312;

    invoke-direct {p0, p1, v0, v1}, LX/1y2;->a(Landroid/text/SpannableStringBuilder;LX/312;I)V

    .line 350129
    const/4 v0, 0x2

    move v1, v0

    .line 350130
    :cond_2
    if-eqz p4, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 350131
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/312;

    invoke-direct {p0, p1, v0, v1}, LX/1y2;->a(Landroid/text/SpannableStringBuilder;LX/312;I)V

    goto :goto_0
.end method

.method public static a(LX/1y2;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/text/SpannableStringBuilder;",
            "II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 350115
    iget-object v0, p0, LX/1y2;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350116
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 350117
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/185;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 350118
    :cond_0
    :goto_0
    return-void

    .line 350119
    :cond_1
    const/4 v0, -0x1

    .line 350120
    iget-object v2, p0, LX/1y2;->a:LX/1xv;

    iget-object v2, v2, LX/1xv;->c:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 350121
    add-int/2addr p3, p4

    move v0, v1

    .line 350122
    :cond_2
    iget-object v2, p0, LX/1y2;->a:LX/1xv;

    iget-object v2, v2, LX/1xv;->n:LX/0wM;

    const v3, 0x7f0207dd

    const v4, -0xa76f01

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 350123
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 350124
    const-string v1, " "

    invoke-virtual {p2, p3, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, p3, v3}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v3, LX/34T;

    const/4 v4, 0x2

    invoke-direct {v3, v2, v4}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    add-int/lit8 v2, p3, 0x1

    add-int/2addr v2, v0

    add-int/lit8 v4, p3, 0x2

    add-int/2addr v0, v4

    const/16 v4, 0x12

    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private a(Landroid/text/SpannableStringBuilder;LX/312;I)V
    .locals 3

    .prologue
    .line 350107
    if-nez p2, :cond_0

    .line 350108
    :goto_0
    return-void

    .line 350109
    :cond_0
    iget v0, p2, LX/312;->b:I

    move v1, v0

    .line 350110
    iget v0, p2, LX/312;->e:I

    move v2, v0

    .line 350111
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cn;

    add-int/2addr v1, v2

    add-int/2addr v1, p3

    .line 350112
    const/4 v0, 0x0

    move-object v2, v0

    .line 350113
    invoke-static {p1, v1, v2}, LX/47q;->a(Landroid/text/SpannableStringBuilder;ILandroid/graphics/drawable/Drawable;)V

    .line 350114
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1y2;
    .locals 13

    .prologue
    .line 350077
    iget-object v0, p0, LX/1y2;->e:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;->b()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;

    move-result-object v0

    .line 350078
    iget-object v1, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 350079
    :cond_0
    :goto_0
    return-object p0

    .line 350080
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, LX/1y2;->a:LX/1xv;

    iget-object v1, v1, LX/1xv;->q:LX/0W3;

    sget-wide v2, LX/0X5;->bq:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 350081
    iget-object v1, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->n()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;

    move-result-object v2

    iget-object v3, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    const/4 v7, 0x3

    const/4 v12, 0x1

    const/4 v9, 0x0

    const/4 v11, 0x2

    .line 350082
    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->c:LX/0hL;

    invoke-virtual {v4}, LX/0hL;->a()Z

    move-result v10

    .line 350083
    if-eqz v10, :cond_4

    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 350084
    :goto_1
    new-array v5, v7, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->v_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    invoke-virtual {v2}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->v_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-static {v4, v5}, LX/1y2;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 350085
    new-instance v6, LX/311;

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->v_()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-virtual {v2}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->v_()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-direct {v6, v4, v7}, LX/311;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 350086
    iget-object v4, v6, LX/311;->a:Ljava/util/List;

    move-object v6, v4

    .line 350087
    move-object v4, p0

    move-object v7, v1

    move-object v8, v0

    move-object v9, v3

    .line 350088
    invoke-static/range {v4 .. v10}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;LX/0lF;Z)V

    .line 350089
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v11, :cond_2

    if-eqz v2, :cond_2

    .line 350090
    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/312;

    sget v8, LX/1Uf;->a:I

    .line 350091
    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    const v4, 0x41e065f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel$ParentGroupModel;->e()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-static {v4, v7}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v9, v4

    .line 350092
    move-object v4, p0

    move-object v7, v3

    move-object v10, v2

    invoke-static/range {v4 .. v10}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350093
    :cond_2
    move-object v0, v5

    .line 350094
    iput-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 350095
    :cond_3
    iget-object v1, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    iget-object v2, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    const/4 v7, 0x2

    const/4 v11, 0x1

    const/4 v9, 0x0

    .line 350096
    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->c:LX/0hL;

    invoke-virtual {v4}, LX/0hL;->a()Z

    move-result v10

    .line 350097
    if-eqz v10, :cond_5

    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 350098
    :goto_2
    new-array v5, v7, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->v_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-static {v4, v5}, LX/1y2;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 350099
    new-instance v6, LX/311;

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;->v_()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-direct {v6, v4, v7}, LX/311;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 350100
    iget-object v4, v6, LX/311;->a:Ljava/util/List;

    move-object v6, v4

    .line 350101
    move-object v4, p0

    move-object v7, v1

    move-object v8, v0

    move-object v9, v2

    .line 350102
    invoke-static/range {v4 .. v10}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ToModel;LX/0lF;Z)V

    .line 350103
    move-object v0, v5

    .line 350104
    iput-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 350105
    :cond_4
    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->l:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto/16 :goto_1

    .line 350106
    :cond_5
    iget-object v4, p0, LX/1y2;->a:LX/1xv;

    iget-object v4, v4, LX/1xv;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_2
.end method

.method public final c()LX/1y2;
    .locals 12

    .prologue
    .line 350063
    iget-object v0, p0, LX/1y2;->e:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;->c()Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;

    move-result-object v0

    .line 350064
    iget-object v1, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-nez v1, :cond_1

    iget-object v1, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 350065
    iget-object v1, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    iget-object v2, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    const/4 v6, 0x2

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 350066
    iget-object v3, p0, LX/1y2;->a:LX/1xv;

    iget-object v3, v3, LX/1xv;->a:Landroid/content/Context;

    const v4, 0x7f081023

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 350067
    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->v_()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-static {v3, v4}, LX/1y2;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    .line 350068
    new-instance v5, LX/311;

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ViaModel;->v_()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    invoke-direct {v5, v3, v6}, LX/311;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    .line 350069
    iget-object v3, v5, LX/311;->a:Ljava/util/List;

    move-object v10, v3

    .line 350070
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/312;

    sget v7, LX/1Uf;->a:I

    invoke-static {p0, v1}, LX/1y2;->a(LX/1y2;LX/1y5;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-object v6, v2

    move-object v9, v1

    invoke-static/range {v3 .. v9}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350071
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/312;

    sget v7, LX/1Uf;->a:I

    invoke-static {p0, v0}, LX/1y2;->a(LX/1y2;LX/1y5;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-object v6, v2

    move-object v9, v0

    invoke-static/range {v3 .. v9}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;LX/312;LX/0lF;ILjava/lang/String;LX/1y5;)V

    .line 350072
    iget-object v3, p0, LX/1y2;->a:LX/1xv;

    iget-object v3, v3, LX/1xv;->f:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 350073
    invoke-static {v1}, LX/4p9;->a(LX/1y6;)Z

    move-result v3

    invoke-static {v0}, LX/4p9;->a(LX/1y6;)Z

    move-result v5

    invoke-static {p0, v4, v10, v3, v5}, LX/1y2;->a(LX/1y2;Landroid/text/SpannableStringBuilder;Ljava/util/List;ZZ)V

    .line 350074
    :cond_0
    move-object v0, v4

    .line 350075
    iput-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    .line 350076
    :cond_1
    return-object p0
.end method

.method public final d()Landroid/text/Spannable;
    .locals 15

    .prologue
    const v13, 0x285feb

    const/4 v7, -0x1

    const/4 v9, 0x1

    const/4 v12, 0x0

    .line 349973
    iget-object v0, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349974
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v1

    .line 349975
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 349976
    iget-object v0, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 349977
    iget-boolean v0, p0, LX/1y2;->i:Z

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 349978
    :goto_0
    invoke-static {v0}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v1

    .line 349979
    if-eqz v1, :cond_0

    iget-boolean v0, p0, LX/1y2;->c:Z

    if-nez v0, :cond_0

    .line 349980
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Uf;

    sget-object v3, LX/1eK;->TITLE:LX/1eK;

    iget v5, p0, LX/1y2;->h:I

    if-eq v5, v7, :cond_6

    iget v5, p0, LX/1y2;->h:I

    :goto_1
    iget-object v6, p0, LX/1y2;->j:LX/1PT;

    invoke-virtual/range {v0 .. v6}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;ILX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    .line 349981
    :cond_0
    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349982
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    const/4 v14, 0x0

    const/4 v5, 0x0

    .line 349983
    if-nez v0, :cond_f

    .line 349984
    :goto_2
    move-object v0, v5

    .line 349985
    iput-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    .line 349986
    :cond_1
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 349987
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cn;

    iget-object v3, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1, v3}, LX/3Cn;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Landroid/text/SpannableStringBuilder;)V

    .line 349988
    :cond_2
    iget-object v0, p0, LX/1y2;->k:LX/1nV;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_3

    .line 349989
    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, LX/1y2;->k:LX/1nV;

    new-array v5, v9, [I

    aput v13, v5, v12

    invoke-static {v0, v3, v5}, LX/1Uf;->a(Landroid/text/Spannable;LX/1nV;[I)V

    .line 349990
    :cond_3
    iget-object v0, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    .line 349991
    iget-object v5, p0, LX/1y2;->f:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 349992
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 349993
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/185;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 349994
    :cond_4
    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_7

    .line 349995
    iget-object v0, p0, LX/1y2;->g:Landroid/text/SpannableStringBuilder;

    .line 349996
    :goto_3
    return-object v0

    .line 349997
    :cond_5
    invoke-static {v2}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move v5, v12

    .line 349998
    goto/16 :goto_1

    .line 349999
    :cond_7
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    if-nez v0, :cond_8

    .line 350000
    const/4 v0, 0x0

    goto :goto_3

    .line 350001
    :cond_8
    const/4 v3, 0x0

    .line 350002
    if-eqz v2, :cond_9

    invoke-static {v2}, LX/17E;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 350003
    :cond_9
    :goto_4
    move v0, v3

    .line 350004
    if-eqz v0, :cond_a

    invoke-static {v2}, LX/3hZ;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 350005
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    .line 350006
    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 350007
    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    if-eqz v3, :cond_17

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v3

    if-eqz v3, :cond_17

    .line 350008
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fo()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v1

    .line 350009
    :goto_5
    move v1, v1

    .line 350010
    invoke-direct {p0, v0, v1, v4}, LX/1y2;->a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;ILX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_3

    .line 350011
    :cond_a
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Uf;

    iget-object v6, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    iget v0, p0, LX/1y2;->h:I

    if-eq v0, v7, :cond_b

    iget v8, p0, LX/1y2;->h:I

    :goto_6
    iget-object v11, p0, LX/1y2;->j:LX/1PT;

    move-object v7, v4

    move-object v10, v2

    invoke-virtual/range {v5 .. v11}, LX/1Uf;->a(LX/1y5;LX/0lF;IZLcom/facebook/graphql/model/FeedUnit;LX/1PT;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 350012
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 350013
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-static {v0}, LX/4p9;->a(LX/1y6;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 350014
    iget-object v0, p0, LX/1y2;->a:LX/1xv;

    iget-object v0, v0, LX/1xv;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Cn;

    .line 350015
    const/4 v0, 0x0

    move-object v2, v0

    .line 350016
    invoke-static {v1, v2}, LX/47q;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v2

    move-object v0, v2

    .line 350017
    goto/16 :goto_3

    .line 350018
    :cond_b
    sget v8, LX/1Uf;->a:I

    goto :goto_6

    .line 350019
    :cond_c
    iget-object v0, p0, LX/1y2;->k:LX/1nV;

    if-eqz v0, :cond_d

    .line 350020
    iget-object v0, p0, LX/1y2;->k:LX/1nV;

    new-array v2, v9, [I

    aput v13, v2, v12

    invoke-static {v1, v0, v2}, LX/1Uf;->a(Landroid/text/Spannable;LX/1nV;[I)V

    .line 350021
    :cond_d
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    move v0, v12

    .line 350022
    :goto_7
    iget-object v2, p0, LX/1y2;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0, v2, v1, v12, v0}, LX/1y2;->a(LX/1y2;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;II)V

    move-object v0, v1

    .line 350023
    goto/16 :goto_3

    .line 350024
    :cond_e
    iget-object v0, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_7

    .line 350025
    :cond_f
    invoke-interface {v0}, LX/1y5;->m()LX/3Bf;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 350026
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    invoke-interface {v0}, LX/1y5;->m()LX/3Bf;

    move-result-object v6

    invoke-interface {v6}, LX/3Bf;->b()Ljava/lang/String;

    move-result-object v6

    .line 350027
    iput-object v6, v3, LX/2dc;->h:Ljava/lang/String;

    .line 350028
    move-object v3, v3

    .line 350029
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 350030
    :goto_8
    :try_start_0
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    invoke-interface {v0}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v8

    .line 350031
    iput-object v8, v6, LX/170;->o:Ljava/lang/String;

    .line 350032
    move-object v6, v6

    .line 350033
    invoke-interface {v0}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 350034
    iput-object v8, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 350035
    move-object v6, v6

    .line 350036
    invoke-interface {v0}, LX/1y5;->j()Ljava/lang/String;

    move-result-object v8

    .line 350037
    iput-object v8, v6, LX/170;->Y:Ljava/lang/String;

    .line 350038
    move-object v6, v6

    .line 350039
    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v8

    .line 350040
    iput-object v8, v6, LX/170;->A:Ljava/lang/String;

    .line 350041
    move-object v6, v6

    .line 350042
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v8

    .line 350043
    iput-object v8, v6, LX/170;->e:Ljava/lang/String;

    .line 350044
    move-object v6, v6

    .line 350045
    iput-object v3, v6, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 350046
    move-object v3, v6

    .line 350047
    invoke-virtual {v3}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v6

    new-instance v8, LX/1yN;

    const/4 v10, 0x0

    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-direct {v8, v10, v11}, LX/1yN;-><init>(II)V

    invoke-static {v6, v8}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v6

    invoke-static {v3, v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 350048
    :goto_9
    invoke-interface {v0}, LX/1y5;->v_()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    aput-object v3, v8, v14

    invoke-static {v8}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v6, v3, v5, v5}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 350049
    iget-object v3, p0, LX/1y2;->a:LX/1xv;

    iget-object v3, v3, LX/1xv;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Uf;

    invoke-static {v5}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v5

    sget-object v6, LX/1eK;->TITLE:LX/1eK;

    invoke-virtual {v3, v5, v2, v6, v4}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    goto/16 :goto_2

    .line 350050
    :catch_0
    move-exception v3

    .line 350051
    const-string v6, "HeaderTitleSpannableBuilder"

    invoke-virtual {v3}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v5

    goto :goto_9

    :cond_10
    move-object v3, v5

    goto :goto_8

    .line 350052
    :cond_11
    if-eqz v1, :cond_4

    .line 350053
    invoke-virtual {v1}, Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v10

    const/4 v5, 0x0

    move v6, v5

    :goto_a
    if-ge v6, v10, :cond_4

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1eF;

    .line 350054
    iget-object v11, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    if-eqz v11, :cond_12

    iget-object v11, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v11}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_12

    iget-object v11, p0, LX/1y2;->d:Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    invoke-virtual {v11}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5}, LX/1eF;->d()LX/1y9;

    move-result-object v14

    invoke-interface {v14}, LX/1y9;->e()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 350055
    invoke-interface {v5}, LX/1eF;->c()I

    move-result v11

    invoke-interface {v5}, LX/1eF;->b()I

    move-result v5

    invoke-static {p0, v0, v3, v11, v5}, LX/1y2;->a(LX/1y2;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/SpannableStringBuilder;II)V

    .line 350056
    :cond_12
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_a

    .line 350057
    :cond_13
    invoke-static {v2}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v3

    :goto_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 350058
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-eqz v6, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-eqz v6, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v8, 0x4984e12

    if-ne v6, v8, :cond_16

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    if-eqz v6, :cond_14

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ek()Lcom/facebook/graphql/model/GraphQLInstantArticle;

    move-result-object v0

    if-nez v0, :cond_16

    .line 350059
    :cond_14
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    .line 350060
    :goto_c
    const/4 v1, 0x2

    if-ne v0, v1, :cond_15

    .line 350061
    const/4 v3, 0x1

    goto/16 :goto_4

    :cond_15
    move v1, v0

    .line 350062
    goto :goto_b

    :cond_16
    move v0, v1

    goto :goto_c

    :cond_17
    invoke-static {v2}, LX/17E;->j(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/16 :goto_5
.end method
