.class public LX/1wr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field private final b:Landroid/content/Context;

.field private final c:LX/12x;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/Context;LX/12x;LX/0ad;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347041
    iput-object p1, p0, LX/1wr;->a:LX/0SG;

    .line 347042
    iput-object p2, p0, LX/1wr;->b:Landroid/content/Context;

    .line 347043
    iput-object p3, p0, LX/1wr;->c:LX/12x;

    .line 347044
    iput-object p4, p0, LX/1wr;->d:LX/0ad;

    .line 347045
    return-void
.end method

.method public static b(LX/0QB;)LX/1wr;
    .locals 5

    .prologue
    .line 347030
    new-instance v4, LX/1wr;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v2

    check-cast v2, LX/12x;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v1, v2, v3}, LX/1wr;-><init>(LX/0SG;Landroid/content/Context;LX/12x;LX/0ad;)V

    .line 347031
    return-object v4
.end method

.method private c()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 347037
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/1wr;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 347038
    const-string v1, "action_show_download_reminder"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 347039
    iget-object v1, p0, LX/1wr;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 347034
    iget-object v0, p0, LX/1wr;->d:LX/0ad;

    sget-wide v2, LX/Hc1;->e:J

    const-wide/32 v4, 0x5265c00

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    .line 347035
    iget-object v2, p0, LX/1wr;->c:LX/12x;

    const/4 v3, 0x1

    iget-object v4, p0, LX/1wr;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    add-long/2addr v0, v4

    invoke-direct {p0}, LX/1wr;->c()Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v1, v4}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    .line 347036
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 347032
    iget-object v0, p0, LX/1wr;->c:LX/12x;

    invoke-direct {p0}, LX/1wr;->c()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 347033
    return-void
.end method
