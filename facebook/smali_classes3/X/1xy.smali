.class public LX/1xy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xz;


# instance fields
.field private final a:LX/1SX;

.field private final b:LX/1nq;

.field private final c:LX/Bs7;

.field private final d:LX/1DR;

.field public final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final g:LX/1xc;

.field public final h:LX/1xv;

.field private final i:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I

.field public final k:I

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:LX/1PT;


# direct methods
.method public constructor <init>(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;LX/1xc;LX/Bs7;LX/1DR;LX/1xv;IIZZZLX/1PT;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/ui/api/FeedMenuHelper;",
            "LX/1xc;",
            "LX/Bs7;",
            "LX/1DR;",
            "LX/1xv;",
            "IIZZZ",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 349873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349874
    iput-object p1, p0, LX/1xy;->b:LX/1nq;

    .line 349875
    iput-object p3, p0, LX/1xy;->a:LX/1SX;

    .line 349876
    iput-object p5, p0, LX/1xy;->c:LX/Bs7;

    .line 349877
    iput-object p6, p0, LX/1xy;->d:LX/1DR;

    .line 349878
    iput-object p2, p0, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349879
    iput p8, p0, LX/1xy;->j:I

    .line 349880
    iput-object p4, p0, LX/1xy;->g:LX/1xc;

    .line 349881
    iput-object p7, p0, LX/1xy;->h:LX/1xv;

    .line 349882
    iput-boolean p11, p0, LX/1xy;->m:Z

    .line 349883
    new-instance v2, LX/1y0;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v2, p0, v1}, LX/1y0;-><init>(LX/1xy;Lcom/facebook/graphql/model/GraphQLStory;)V

    iput-object v2, p0, LX/1xy;->i:LX/1KL;

    .line 349884
    iput p9, p0, LX/1xy;->k:I

    .line 349885
    iput-boolean p10, p0, LX/1xy;->l:Z

    .line 349886
    move/from16 v0, p12

    iput-boolean v0, p0, LX/1xy;->n:Z

    .line 349887
    move-object/from16 v0, p13

    iput-object v0, p0, LX/1xy;->o:LX/1PT;

    .line 349888
    iget-boolean v1, p0, LX/1xy;->m:Z

    if-eqz v1, :cond_1

    .line 349889
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    :goto_0
    iput-object v1, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 349890
    :goto_1
    return-void

    .line 349891
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 349892
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1xa;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    iput-object v1, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 2

    .prologue
    .line 349893
    iget-object v0, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349894
    :cond_0
    const/4 v0, 0x0

    .line 349895
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349896
    iget-object v0, p0, LX/1xy;->i:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 349897
    iget-object v0, p0, LX/1xy;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 349898
    iget-object v0, p0, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349899
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 349900
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final d()LX/1nq;
    .locals 1

    .prologue
    .line 349901
    iget-object v0, p0, LX/1xy;->b:LX/1nq;

    return-object v0
.end method

.method public final e()I
    .locals 5

    .prologue
    .line 349902
    iget-object v0, p0, LX/1xy;->c:LX/Bs7;

    if-nez v0, :cond_0

    .line 349903
    const/4 v0, 0x0

    .line 349904
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/1xy;->c:LX/Bs7;

    iget-object v1, p0, LX/1xy;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/1xy;->a:LX/1SX;

    iget-object v3, p0, LX/1xy;->d:LX/1DR;

    invoke-virtual {v3}, LX/1DR;->a()I

    move-result v3

    iget v4, p0, LX/1xy;->j:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Bs7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;II)I

    move-result v0

    goto :goto_0
.end method
