.class public LX/1ym;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ys;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1ym",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1ys;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351357
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 351358
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1ym;->b:LX/0Zi;

    .line 351359
    iput-object p1, p0, LX/1ym;->a:LX/0Ot;

    .line 351360
    return-void
.end method

.method public static a(LX/0QB;)LX/1ym;
    .locals 4

    .prologue
    .line 351346
    const-class v1, LX/1ym;

    monitor-enter v1

    .line 351347
    :try_start_0
    sget-object v0, LX/1ym;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351348
    sput-object v2, LX/1ym;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351349
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351350
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351351
    new-instance v3, LX/1ym;

    const/16 p0, 0x725

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1ym;-><init>(LX/0Ot;)V

    .line 351352
    move-object v0, v3

    .line 351353
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351354
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ym;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351355
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 351333
    check-cast p2, LX/BsW;

    .line 351334
    iget-object v0, p0, LX/1ym;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ys;

    iget-object v2, p2, LX/BsW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/BsW;->b:Ljava/lang/String;

    iget-object v4, p2, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v5, p2, LX/BsW;->d:LX/1Pb;

    iget v6, p2, LX/BsW;->e:I

    iget v7, p2, LX/BsW;->f:I

    move-object v1, p1

    const/4 v10, 0x0

    .line 351335
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v4, v8, :cond_1

    const/4 v8, 0x1

    move v9, v8

    .line 351336
    :goto_0
    iget-object v8, v0, LX/1ys;->e:LX/1vg;

    invoke-virtual {v8, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    if-eqz v9, :cond_2

    :goto_1
    invoke-virtual {v8, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    iget-object p1, v0, LX/1ys;->f:Landroid/content/res/ColorStateList;

    if-eqz v9, :cond_3

    sget-object v8, LX/1ys;->c:[I

    :goto_2
    iget-object p2, v0, LX/1ys;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result p2

    invoke-virtual {p1, v8, p2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v8

    invoke-virtual {p0, v8}, LX/2xv;->i(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v8}, LX/1n6;->b()LX/1dc;

    move-result-object v8

    .line 351337
    iget-object p0, v0, LX/1ys;->d:LX/359;

    invoke-virtual {p0, v1}, LX/359;->c(LX/1De;)LX/35B;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/35B;->a(LX/1dc;)LX/35B;

    move-result-object v8

    check-cast v5, LX/1Pr;

    invoke-virtual {v8, v5}, LX/35B;->a(LX/1Pr;)LX/35B;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/35B;->b(Ljava/lang/String;)LX/35B;

    move-result-object p0

    .line 351338
    iget-object v8, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 351339
    check-cast v8, LX/0jW;

    invoke-virtual {p0, v8}, LX/35B;->a(LX/0jW;)LX/35B;

    move-result-object v8

    const p0, 0x7f0b00e5

    invoke-virtual {v8, p0}, LX/35B;->i(I)LX/35B;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    .line 351340
    const p0, 0x34ae7393

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 351341
    invoke-interface {v8, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    if-eqz v9, :cond_4

    const v8, 0x7f08114c

    :goto_3
    invoke-interface {p0, v8}, LX/1Di;->A(I)LX/1Di;

    move-result-object v8

    iget-object p0, v0, LX/1ys;->g:LX/1yf;

    invoke-virtual {p0}, LX/1yf;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    if-eqz v9, :cond_5

    const v10, 0x7f020cba

    :cond_0
    :goto_4
    invoke-interface {v8, v10}, LX/1Di;->x(I)LX/1Di;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 351342
    return-object v0

    :cond_1
    move v9, v10

    .line 351343
    goto :goto_0

    :cond_2
    move v6, v7

    .line 351344
    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 351345
    :cond_4
    const v8, 0x7f08114b

    goto :goto_3

    :cond_5
    const v10, 0x7f020cb9

    goto :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 351320
    invoke-static {}, LX/1dS;->b()V

    .line 351321
    iget v0, p1, LX/1dQ;->b:I

    .line 351322
    packed-switch v0, :pswitch_data_0

    .line 351323
    :goto_0
    return-object v2

    .line 351324
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 351325
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 351326
    check-cast v1, LX/BsW;

    .line 351327
    iget-object v3, p0, LX/1ym;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v4, v1, LX/BsW;->b:Ljava/lang/String;

    iget-object v5, v1, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v6, v1, LX/BsW;->g:Ljava/lang/String;

    iget-object v7, v1, LX/BsW;->h:Ljava/lang/String;

    iget-object p1, v1, LX/BsW;->d:LX/1Pb;

    iget-object p2, v1, LX/BsW;->i:LX/DHA;

    move-object v3, v0

    .line 351328
    invoke-static {v4, v5, v6, v7, p1}, Lcom/facebook/feed/rows/sections/header/FollowUserButtonPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;Ljava/lang/String;LX/1Pb;)Landroid/view/View$OnClickListener;

    move-result-object p0

    .line 351329
    invoke-interface {p0, v3}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 351330
    if-eqz p2, :cond_0

    .line 351331
    iget-object p0, p2, LX/DHA;->b:LX/DHC;

    iget-object v3, p2, LX/DHA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0, v3}, LX/DHC;->a$redex0(LX/DHC;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 351332
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x34ae7393
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/BsV;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1ym",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 351312
    new-instance v1, LX/BsW;

    invoke-direct {v1, p0}, LX/BsW;-><init>(LX/1ym;)V

    .line 351313
    iget-object v2, p0, LX/1ym;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BsV;

    .line 351314
    if-nez v2, :cond_0

    .line 351315
    new-instance v2, LX/BsV;

    invoke-direct {v2, p0}, LX/BsV;-><init>(LX/1ym;)V

    .line 351316
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BsV;->a$redex0(LX/BsV;LX/1De;IILX/BsW;)V

    .line 351317
    move-object v1, v2

    .line 351318
    move-object v0, v1

    .line 351319
    return-object v0
.end method
