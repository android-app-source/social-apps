.class public LX/1zh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1zi;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1zh;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1zj;

.field private final c:LX/1zk;

.field private final d:LX/1zl;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1zj;LX/1zk;LX/1zl;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353909
    iput-object p1, p0, LX/1zh;->a:Landroid/content/Context;

    .line 353910
    iput-object p2, p0, LX/1zh;->b:LX/1zj;

    .line 353911
    iput-object p3, p0, LX/1zh;->c:LX/1zk;

    .line 353912
    iput-object p4, p0, LX/1zh;->d:LX/1zl;

    .line 353913
    iput-object p5, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 353914
    return-void
.end method

.method public static a(LX/0QB;)LX/1zh;
    .locals 9

    .prologue
    .line 353915
    sget-object v0, LX/1zh;->f:LX/1zh;

    if-nez v0, :cond_1

    .line 353916
    const-class v1, LX/1zh;

    monitor-enter v1

    .line 353917
    :try_start_0
    sget-object v0, LX/1zh;->f:LX/1zh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 353918
    if-eqz v2, :cond_0

    .line 353919
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 353920
    new-instance v3, LX/1zh;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1zj;->a(LX/0QB;)LX/1zj;

    move-result-object v5

    check-cast v5, LX/1zj;

    invoke-static {v0}, LX/1zk;->a(LX/0QB;)LX/1zk;

    move-result-object v6

    check-cast v6, LX/1zk;

    invoke-static {v0}, LX/1zl;->a(LX/0QB;)LX/1zl;

    move-result-object v7

    check-cast v7, LX/1zl;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v3 .. v8}, LX/1zh;-><init>(Landroid/content/Context;LX/1zj;LX/1zk;LX/1zl;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 353921
    move-object v0, v3

    .line 353922
    sput-object v0, LX/1zh;->f:LX/1zh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 353923
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 353924
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353925
    :cond_1
    sget-object v0, LX/1zh;->f:LX/1zh;

    return-object v0

    .line 353926
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 353927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(ILjava/lang/String;LX/1zo;)LX/1zx;
    .locals 4

    .prologue
    .line 353928
    sget-object v0, LX/1zv;->a:[I

    invoke-virtual {p3}, LX/1zo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353929
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Drawables for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353930
    :pswitch_0
    new-instance v0, LX/1zw;

    iget-object v1, p0, LX/1zh;->a:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, LX/1zw;-><init>(Landroid/content/Context;Ljava/io/File;)V

    .line 353931
    :goto_0
    return-object v0

    .line 353932
    :pswitch_1
    iget-object v0, p0, LX/1zh;->d:LX/1zl;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1zl;->a(ILjava/io/File;Z)V

    .line 353933
    new-instance v0, LX/9Ao;

    iget-object v1, p0, LX/1zh;->d:LX/1zl;

    sget-object v2, LX/1zo;->LARGE:LX/1zo;

    invoke-direct {p0, p1, v2}, LX/1zh;->a(ILX/1zo;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/1zo;->LARGE:LX/1zo;

    invoke-direct {p0, p1, v2, v3}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, LX/9Ao;-><init>(LX/1zl;ILX/1zx;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ILX/1zo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 353934
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p2}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/1zs;->a(ILjava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILX/1zt;Z)LX/1zt;
    .locals 12

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 353935
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/1zs;->a(I)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 353936
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/1zs;->c(I)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    move v2, v0

    .line 353937
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/1zs;->b(I)LX/0Tn;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v3, v0

    .line 353938
    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/String;

    sget-object v0, LX/1zo;->SMALL:LX/1zo;

    invoke-direct {p0, p1, v0}, LX/1zh;->a(ILX/1zo;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    if-eqz p3, :cond_1

    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    :goto_0
    invoke-direct {p0, p1, v0}, LX/1zh;->a(ILX/1zo;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    sget-object v0, LX/1zo;->TAB_ICONS:LX/1zo;

    invoke-direct {p0, p1, v0}, LX/1zh;->a(ILX/1zo;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    .line 353939
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v2, :cond_5

    const/4 v0, 0x0

    .line 353940
    array-length v9, v6

    move v4, v0

    :goto_1
    if-ge v4, v9, :cond_7

    aget-object v10, v6, v4

    .line 353941
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_6

    .line 353942
    :cond_0
    :goto_2
    move v0, v0

    .line 353943
    if-eqz v0, :cond_5

    .line 353944
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 353945
    if-ne v0, p1, :cond_3

    .line 353946
    aget-object v0, v6, v5

    sget-object v4, LX/1zo;->SMALL:LX/1zo;

    invoke-direct {p0, p1, v0, v4}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v4

    aget-object v5, v6, v7

    if-eqz p3, :cond_2

    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    :goto_3
    invoke-direct {p0, p1, v5, v0}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v5

    aget-object v0, v6, v8

    sget-object v6, LX/1zo;->TAB_ICONS:LX/1zo;

    invoke-direct {p0, p1, v0, v6}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v6

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, LX/1zt;->a(Ljava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    .line 353947
    :goto_4
    return-object p2

    .line 353948
    :cond_1
    iget-object v0, p0, LX/1zh;->c:LX/1zk;

    invoke-virtual {v0}, LX/1zk;->a()LX/1zo;

    move-result-object v0

    goto :goto_0

    .line 353949
    :cond_2
    iget-object v0, p0, LX/1zh;->c:LX/1zk;

    invoke-virtual {v0}, LX/1zk;->a()LX/1zo;

    move-result-object v0

    goto :goto_3

    .line 353950
    :cond_3
    new-instance v4, LX/1zt;

    aget-object v0, v6, v5

    sget-object v5, LX/1zo;->SMALL:LX/1zo;

    invoke-direct {p0, p1, v0, v5}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v9

    aget-object v5, v6, v7

    if-eqz p3, :cond_4

    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    :goto_5
    invoke-direct {p0, p1, v5, v0}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v10

    aget-object v0, v6, v8

    sget-object v5, LX/1zo;->TAB_ICONS:LX/1zo;

    invoke-direct {p0, p1, v0, v5}, LX/1zh;->a(ILjava/lang/String;LX/1zo;)LX/1zx;

    move-result-object v11

    move v5, p1

    move-object v6, v1

    move v7, v2

    move v8, v3

    invoke-direct/range {v4 .. v11}, LX/1zt;-><init>(ILjava/lang/String;IZLX/1zx;LX/1zx;LX/1zx;)V

    move-object p2, v4

    goto :goto_4

    :cond_4
    iget-object v0, p0, LX/1zh;->c:LX/1zk;

    invoke-virtual {v0}, LX/1zk;->a()LX/1zo;

    move-result-object v0

    goto :goto_5

    .line 353951
    :cond_5
    iget-object v0, p0, LX/1zh;->b:LX/1zj;

    invoke-virtual {v0, p1, p2, p3}, LX/1zj;->a(ILX/1zt;Z)LX/1zt;

    move-result-object p2

    goto :goto_4

    .line 353952
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 353953
    :cond_7
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public final a()[I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 353954
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1zs;->b:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 353955
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 353956
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 353957
    array-length v0, v4

    new-array v0, v0, [I

    .line 353958
    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 353959
    add-int/lit8 v3, v2, 0x1

    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353960
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0

    .line 353961
    :catch_0
    iget-object v0, p0, LX/1zh;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1zs;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 353962
    iget-object v0, p0, LX/1zh;->b:LX/1zj;

    invoke-virtual {v0}, LX/1zj;->a()[I

    move-result-object v0

    .line 353963
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, LX/1zh;->b:LX/1zj;

    invoke-virtual {v0}, LX/1zj;->a()[I

    move-result-object v0

    goto :goto_1
.end method
