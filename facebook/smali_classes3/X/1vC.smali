.class public LX/1vC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1vC;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342089
    iput-object p1, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 342090
    return-void
.end method

.method public static a(LX/0QB;)LX/1vC;
    .locals 4

    .prologue
    .line 342075
    sget-object v0, LX/1vC;->b:LX/1vC;

    if-nez v0, :cond_1

    .line 342076
    const-class v1, LX/1vC;

    monitor-enter v1

    .line 342077
    :try_start_0
    sget-object v0, LX/1vC;->b:LX/1vC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 342078
    if-eqz v2, :cond_0

    .line 342079
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 342080
    new-instance p0, LX/1vC;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/1vC;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 342081
    move-object v0, p0

    .line 342082
    sput-object v0, LX/1vC;->b:LX/1vC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342083
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 342084
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 342085
    :cond_1
    sget-object v0, LX/1vC;->b:LX/1vC;

    return-object v0

    .line 342086
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 342087
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 342073
    iget-object v0, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 342074
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 342059
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 342060
    iget-object v1, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, p1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 342061
    iget-object v1, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "surface:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 342062
    iget-object v1, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reaction_session_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 342063
    return-void
.end method

.method public final b(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 342071
    iget-object v0, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {v0, p1, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 342072
    return-void
.end method

.method public final b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 342068
    invoke-virtual {p0, p1, p2, p3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 342069
    iget-object v0, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "surface"

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;Ljava/lang/String;)V

    .line 342070
    return-void
.end method

.method public final c(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 342066
    iget-object v0, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 342067
    return-void
.end method

.method public final c(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 342064
    iget-object v0, p0, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, p1, v1, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 342065
    return-void
.end method
