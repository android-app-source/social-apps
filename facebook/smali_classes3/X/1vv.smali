.class public LX/1vv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/34R;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/1vv;


# instance fields
.field private final b:LX/1Ad;

.field private final c:LX/1Uo;

.field private final d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 344835
    new-instance v0, LX/1vw;

    invoke-direct {v0}, LX/1vw;-><init>()V

    sput-object v0, LX/1vv;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/1Uo;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344831
    iput-object p1, p0, LX/1vv;->b:LX/1Ad;

    .line 344832
    iput-object p2, p0, LX/1vv;->c:LX/1Uo;

    .line 344833
    iput-object p3, p0, LX/1vv;->d:Landroid/content/Context;

    .line 344834
    return-void
.end method

.method public static a(III)I
    .locals 1

    .prologue
    .line 344825
    if-lez p0, :cond_0

    if-gtz p1, :cond_2

    .line 344826
    :cond_0
    const/4 p0, -0x1

    .line 344827
    :cond_1
    :goto_0
    return p0

    .line 344828
    :cond_2
    if-eq p2, p1, :cond_1

    .line 344829
    mul-int v0, p2, p0

    div-int p0, v0, p1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1vv;
    .locals 6

    .prologue
    .line 344836
    sget-object v0, LX/1vv;->e:LX/1vv;

    if-nez v0, :cond_1

    .line 344837
    const-class v1, LX/1vv;

    monitor-enter v1

    .line 344838
    :try_start_0
    sget-object v0, LX/1vv;->e:LX/1vv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 344839
    if-eqz v2, :cond_0

    .line 344840
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 344841
    new-instance p0, LX/1vv;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-static {v0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v4

    check-cast v4, LX/1Uo;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/1vv;-><init>(LX/1Ad;LX/1Uo;Landroid/content/Context;)V

    .line 344842
    move-object v0, p0

    .line 344843
    sput-object v0, LX/1vv;->e:LX/1vv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344844
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 344845
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344846
    :cond_1
    sget-object v0, LX/1vv;->e:LX/1vv;

    return-object v0

    .line 344847
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 344848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/2nQ;LX/34R;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 8
    .param p3    # I
        .annotation build Lcom/facebook/widget/text/FbImageSpan$FbImageSpanAlignment;
        .end annotation
    .end param

    .prologue
    .line 344823
    iget-object v2, p2, LX/34R;->a:Landroid/net/Uri;

    iget v3, p2, LX/34R;->c:I

    iget v4, p2, LX/34R;->d:I

    iget-object v5, p2, LX/34R;->b:LX/1yN;

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, LX/1vv;->a(LX/2nQ;Landroid/net/Uri;IILX/1yN;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 344824
    return-void
.end method

.method public final a(LX/2nQ;Landroid/net/Uri;IILX/1yN;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 8
    .param p6    # I
        .annotation build Lcom/facebook/widget/text/FbImageSpan$FbImageSpanAlignment;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 344796
    iget v0, p5, LX/1yN;->a:I

    move v0, v0

    .line 344797
    invoke-virtual {p1}, LX/2nQ;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 344798
    :goto_0
    return-void

    .line 344799
    :cond_0
    iget-object v0, p0, LX/1vv;->b:LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->o()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 344800
    iget-object v1, p0, LX/1vv;->c:LX/1Uo;

    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 344801
    iput v6, v1, LX/1Uo;->d:I

    .line 344802
    move-object v1, v1

    .line 344803
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 344804
    iget-object v2, p0, LX/1vv;->d:Landroid/content/Context;

    invoke-static {v1, v2}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    .line 344805
    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 344806
    iget v0, p5, LX/1yN;->a:I

    move v0, v0

    .line 344807
    const-string v2, "\u200c "

    invoke-virtual {p1, v0, v2}, LX/2nQ;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 344808
    iget v0, p5, LX/1yN;->a:I

    move v2, v0

    .line 344809
    invoke-virtual {p5}, LX/1yN;->c()I

    move-result v3

    if-eq p3, v5, :cond_2

    move v4, p3

    :goto_1
    if-eq p4, v5, :cond_1

    const/4 v6, 0x1

    :cond_1
    move-object v0, p1

    move v5, p4

    move v7, p6

    const/4 p2, 0x0

    .line 344810
    invoke-virtual {v0}, LX/2nQ;->length()I

    move-result p0

    if-lt v3, p0, :cond_3

    .line 344811
    :goto_2
    goto :goto_0

    :cond_2
    move v4, p4

    goto :goto_1

    .line 344812
    :cond_3
    invoke-virtual {v1}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 344813
    if-eqz p0, :cond_5

    .line 344814
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 344815
    invoke-virtual {p0, p2, p2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 344816
    :cond_4
    iget-object p1, v0, LX/2nQ;->b:LX/2nS;

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 344817
    :cond_5
    new-instance p1, LX/34S;

    invoke-direct {p1, v1, v7}, LX/34S;-><init>(LX/1aX;I)V

    .line 344818
    iget-object p0, v1, LX/1aX;->f:LX/1aZ;

    move-object p0, p0

    .line 344819
    instance-of p2, p0, LX/1bp;

    if-eqz p2, :cond_6

    .line 344820
    check-cast p0, LX/1bp;

    new-instance p2, LX/34U;

    invoke-direct {p2, v0, p1, v6, v5}, LX/34U;-><init>(LX/2nQ;LX/34S;ZI)V

    invoke-virtual {p0, p2}, LX/1bp;->a(LX/1Ai;)V

    .line 344821
    :cond_6
    iget-object p0, v0, LX/2nQ;->a:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 344822
    add-int/lit8 p0, v3, 0x1

    const/16 p2, 0x21

    invoke-virtual {v0, p1, v2, p0, p2}, LX/2nQ;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method
