.class public final LX/1xo;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1xU;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1xn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xU",
            "<TE;>.HeaderTitleAndSubtitleComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1xU;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1xU;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 349425
    iput-object p1, p0, LX/1xo;->b:LX/1xU;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 349426
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1xo;->c:[Ljava/lang/String;

    .line 349427
    iput v3, p0, LX/1xo;->d:I

    .line 349428
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1xo;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1xo;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1xo;LX/1De;IILX/1xn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1xU",
            "<TE;>.HeaderTitleAndSubtitleComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 349421
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 349422
    iput-object p4, p0, LX/1xo;->a:LX/1xn;

    .line 349423
    iget-object v0, p0, LX/1xo;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 349424
    return-void
.end method


# virtual methods
.method public final a(LX/1Pn;)LX/1xo;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349418
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput-object p1, v0, LX/1xn;->b:LX/1Pn;

    .line 349419
    iget-object v0, p0, LX/1xo;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 349420
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xo;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349415
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput-object p1, v0, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 349416
    iget-object v0, p0, LX/1xo;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 349417
    return-object p0
.end method

.method public final a(Z)LX/1xo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349429
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput-boolean p1, v0, LX/1xn;->e:Z

    .line 349430
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 349395
    invoke-super {p0}, LX/1X5;->a()V

    .line 349396
    const/4 v0, 0x0

    iput-object v0, p0, LX/1xo;->a:LX/1xn;

    .line 349397
    iget-object v0, p0, LX/1xo;->b:LX/1xU;

    iget-object v0, v0, LX/1xU;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 349398
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1xU;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 349399
    iget-object v1, p0, LX/1xo;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1xo;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1xo;->d:I

    if-ge v1, v2, :cond_2

    .line 349400
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 349401
    :goto_0
    iget v2, p0, LX/1xo;->d:I

    if-ge v0, v2, :cond_1

    .line 349402
    iget-object v2, p0, LX/1xo;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 349403
    iget-object v2, p0, LX/1xo;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349404
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 349405
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349406
    :cond_2
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    .line 349407
    invoke-virtual {p0}, LX/1xo;->a()V

    .line 349408
    return-object v0
.end method

.method public final h(I)LX/1xo;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349409
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput p1, v0, LX/1xn;->c:I

    .line 349410
    return-object p0
.end method

.method public final i(I)LX/1xo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349411
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput p1, v0, LX/1xn;->d:I

    .line 349412
    return-object p0
.end method

.method public final j(I)LX/1xo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1xU",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349413
    iget-object v0, p0, LX/1xo;->a:LX/1xn;

    iput p1, v0, LX/1xn;->f:I

    .line 349414
    return-object p0
.end method
