.class public LX/1yx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1nq;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1xa;

.field private final c:Landroid/text/TextPaint;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 351706
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1yx;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1xa;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351702
    iput-object p2, p0, LX/1yx;->b:LX/1xa;

    .line 351703
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, LX/1yx;->c:Landroid/text/TextPaint;

    .line 351704
    iget-object v0, p0, LX/1yx;->c:Landroid/text/TextPaint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0904

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 351705
    return-void
.end method

.method private static a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    const v1, -0x6e685d

    .line 351700
    if-ne p1, v1, :cond_0

    const v0, 0x1010212

    invoke-static {p0, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result p1

    :cond_0
    return p1
.end method

.method public static a(LX/0QB;)LX/1yx;
    .locals 5

    .prologue
    .line 351689
    const-class v1, LX/1yx;

    monitor-enter v1

    .line 351690
    :try_start_0
    sget-object v0, LX/1yx;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351691
    sput-object v2, LX/1yx;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351692
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351693
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351694
    new-instance p0, LX/1yx;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1xa;->a(LX/0QB;)LX/1xa;

    move-result-object v4

    check-cast v4, LX/1xa;

    invoke-direct {p0, v3, v4}, LX/1yx;-><init>(Landroid/content/Context;LX/1xa;)V

    .line 351695
    move-object v0, p0

    .line 351696
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351697
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351698
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 351652
    sget-object v0, LX/03r;->HeaderSubtitleComponent:[I

    invoke-virtual {p0, v0, v1}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 351653
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_8

    .line 351654
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 351655
    const/16 v5, 0x6

    if-ne v4, v5, :cond_1

    .line 351656
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 351657
    iput-object v4, p1, LX/1np;->a:Ljava/lang/Object;

    .line 351658
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351659
    :cond_1
    const/16 v5, 0x0

    if-ne v4, v5, :cond_2

    .line 351660
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 351661
    iput-object v4, p2, LX/1np;->a:Ljava/lang/Object;

    .line 351662
    goto :goto_1

    .line 351663
    :cond_2
    const/16 v5, 0x7

    if-ne v4, v5, :cond_3

    .line 351664
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 351665
    iput-object v4, p7, LX/1np;->a:Ljava/lang/Object;

    .line 351666
    goto :goto_1

    .line 351667
    :cond_3
    const/16 v5, 0x8

    if-ne v4, v5, :cond_4

    .line 351668
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 351669
    iput-object v4, p8, LX/1np;->a:Ljava/lang/Object;

    .line 351670
    goto :goto_1

    .line 351671
    :cond_4
    const/16 v5, 0x3

    if-ne v4, v5, :cond_5

    .line 351672
    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 351673
    iput-object v4, p4, LX/1np;->a:Ljava/lang/Object;

    .line 351674
    goto :goto_1

    .line 351675
    :cond_5
    const/16 v5, 0x4

    if-ne v4, v5, :cond_6

    .line 351676
    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 351677
    iput-object v4, p5, LX/1np;->a:Ljava/lang/Object;

    .line 351678
    goto :goto_1

    .line 351679
    :cond_6
    const/16 v5, 0x5

    if-ne v4, v5, :cond_7

    .line 351680
    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 351681
    iput-object v4, p6, LX/1np;->a:Ljava/lang/Object;

    .line 351682
    goto :goto_1

    .line 351683
    :cond_7
    const/16 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 351684
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 351685
    iput-object v4, p3, LX/1np;->a:Ljava/lang/Object;

    .line 351686
    goto :goto_1

    .line 351687
    :cond_8
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 351688
    return-void
.end method

.method private static a(LX/1nq;)V
    .locals 1

    .prologue
    .line 351649
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 351650
    sget-object v0, LX/1yx;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 351651
    return-void
.end method

.method private static a(LX/1yx;Landroid/content/Context;ILcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLX/1np;LX/1np;LX/1np;)V
    .locals 7
    .param p9    # F
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IIIFFFIZ",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout;",
            ">;",
            "LX/1np",
            "<[",
            "Landroid/text/style/ClickableSpan;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351639
    iget-object v1, p0, LX/1yx;->b:LX/1xa;

    iget-object v3, p0, LX/1yx;->c:Landroid/text/TextPaint;

    move-object v2, p3

    move v4, p2

    move/from16 v5, p10

    move/from16 v6, p11

    invoke-virtual/range {v1 .. v6}, LX/1xa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;IIZ)LX/1z4;

    move-result-object v1

    .line 351640
    iget-object v1, v1, LX/1z4;->b:Ljava/lang/CharSequence;

    .line 351641
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 351642
    invoke-static {}, LX/1yx;->b()LX/1nq;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v3

    const v4, 0x7f0b0904

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/1nq;->b(I)LX/1nq;

    move-result-object v3

    invoke-static {p1, p4}, LX/1yx;->a(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/1nq;->c(I)LX/1nq;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/1nq;->e(I)LX/1nq;

    move-result-object v3

    move/from16 v0, p9

    invoke-virtual {v3, v0, p7, p8, p6}, LX/1nq;->a(FFFI)LX/1nq;

    move-result-object v3

    const v4, 0x7f0b04f4

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v3, v2}, LX/1nq;->a(F)LX/1nq;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1nq;->a(I)LX/1nq;

    move-result-object v2

    .line 351643
    invoke-virtual {v2}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v0, v3}, LX/1np;->a(Ljava/lang/Object;)V

    .line 351644
    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 351645
    invoke-virtual/range {p12 .. p12}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object/from16 v0, p14

    invoke-static {v1, v0}, LX/1yx;->a(Ljava/lang/CharSequence;LX/1np;)V

    .line 351646
    invoke-static {v2}, LX/1yx;->a(LX/1nq;)V

    .line 351647
    invoke-static {}, LX/1yv;->a()LX/1yv;

    move-result-object v2

    invoke-virtual/range {p13 .. p13}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    invoke-virtual {v2, v1}, LX/1yv;->a(Landroid/text/Layout;)V

    .line 351648
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;LX/1np;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "LX/1np",
            "<[",
            "Landroid/text/style/ClickableSpan;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351618
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 351619
    check-cast v0, Landroid/text/Spanned;

    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const-class v3, Landroid/text/style/ClickableSpan;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 351620
    iput-object v0, p1, LX/1np;->a:Ljava/lang/Object;

    .line 351621
    :cond_0
    return-void
.end method

.method private static b()LX/1nq;
    .locals 2

    .prologue
    .line 351635
    sget-object v0, LX/1yx;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nq;

    .line 351636
    if-nez v0, :cond_0

    .line 351637
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1nq;->a(Z)LX/1nq;

    move-result-object v0

    .line 351638
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;IILX/1no;Lcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLX/1np;LX/1np;LX/1np;)V
    .locals 16
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1no;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IIIFFFIZ",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout;",
            ">;",
            "LX/1np",
            "<[",
            "Landroid/text/style/ClickableSpan;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351628
    invoke-static/range {p2 .. p2}, LX/1mh;->a(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 351629
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "HeaderSubtitleComponent does not support UNSPECIFIED width measurement"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 351630
    :cond_0
    invoke-static/range {p2 .. p2}, LX/1mh;->b(I)I

    move-result v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    invoke-static/range {v1 .. v15}, LX/1yx;->a(LX/1yx;Landroid/content/Context;ILcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLX/1np;LX/1np;LX/1np;)V

    .line 351631
    invoke-virtual/range {p15 .. p15}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/Layout;

    .line 351632
    invoke-static {v1}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v2

    move/from16 v0, p2

    invoke-static {v0, v2}, LX/1mh;->b(II)I

    move-result v2

    move-object/from16 v0, p4

    iput v2, v0, LX/1no;->a:I

    .line 351633
    invoke-static {v1}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v1

    move/from16 v0, p3

    invoke-static {v0, v1}, LX/1mh;->b(II)I

    move-result v1

    move-object/from16 v0, p4

    iput v1, v0, LX/1no;->b:I

    .line 351634
    return-void
.end method

.method public final a(LX/1De;LX/1Dg;Lcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLjava/lang/CharSequence;Landroid/text/Layout;[Landroid/text/style/ClickableSpan;LX/1np;LX/1np;LX/1np;)V
    .locals 17
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p7    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p8    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p11    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/components/ComponentLayout;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;IIIFFFIZ",
            "Ljava/lang/CharSequence;",
            "Landroid/text/Layout;",
            "[",
            "Landroid/text/style/ClickableSpan;",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout;",
            ">;",
            "LX/1np",
            "<[",
            "Landroid/text/style/ClickableSpan;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351622
    if-eqz p13, :cond_0

    .line 351623
    move-object/from16 v0, p15

    move-object/from16 v1, p12

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 351624
    move-object/from16 v0, p16

    move-object/from16 v1, p13

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 351625
    move-object/from16 v0, p17

    move-object/from16 v1, p14

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 351626
    :goto_0
    return-void

    .line 351627
    :cond_0
    invoke-virtual/range {p2 .. p2}, LX/1Dg;->c()I

    move-result v4

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move-object/from16 v16, p17

    invoke-static/range {v2 .. v16}, LX/1yx;->a(LX/1yx;Landroid/content/Context;ILcom/facebook/feed/rows/core/props/FeedProps;IIIFFFIZLX/1np;LX/1np;LX/1np;)V

    goto :goto_0
.end method
