.class public LX/1yj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/1yk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1yk",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1yl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1yl",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/1ym;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ym",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/1yn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1yn",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1yo;

.field private final f:LX/1yq;

.field private final g:LX/14w;

.field private final h:LX/0Uh;

.field private final i:LX/1yf;


# direct methods
.method public constructor <init>(LX/1yk;LX/1yl;LX/1ym;LX/1yn;LX/1yo;LX/1yq;LX/0Uh;LX/14w;LX/1yf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351114
    iput-object p1, p0, LX/1yj;->a:LX/1yk;

    .line 351115
    iput-object p2, p0, LX/1yj;->b:LX/1yl;

    .line 351116
    iput-object p3, p0, LX/1yj;->c:LX/1ym;

    .line 351117
    iput-object p4, p0, LX/1yj;->d:LX/1yn;

    .line 351118
    iput-object p5, p0, LX/1yj;->e:LX/1yo;

    .line 351119
    iput-object p6, p0, LX/1yj;->f:LX/1yq;

    .line 351120
    iput-object p8, p0, LX/1yj;->g:LX/14w;

    .line 351121
    iput-object p7, p0, LX/1yj;->h:LX/0Uh;

    .line 351122
    iput-object p9, p0, LX/1yj;->i:LX/1yf;

    .line 351123
    return-void
.end method

.method private static a(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 351124
    iget-object v0, p0, LX/1yj;->a:LX/1yk;

    invoke-virtual {v0, p1}, LX/1yk;->c(LX/1De;)LX/358;

    move-result-object v0

    check-cast p3, LX/1Pd;

    invoke-virtual {v0, p3}, LX/358;->a(LX/1Pd;)LX/358;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/358;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/358;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/1yj;
    .locals 13

    .prologue
    .line 351125
    const-class v1, LX/1yj;

    monitor-enter v1

    .line 351126
    :try_start_0
    sget-object v0, LX/1yj;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351127
    sput-object v2, LX/1yj;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351130
    new-instance v3, LX/1yj;

    invoke-static {v0}, LX/1yk;->a(LX/0QB;)LX/1yk;

    move-result-object v4

    check-cast v4, LX/1yk;

    invoke-static {v0}, LX/1yl;->a(LX/0QB;)LX/1yl;

    move-result-object v5

    check-cast v5, LX/1yl;

    invoke-static {v0}, LX/1ym;->a(LX/0QB;)LX/1ym;

    move-result-object v6

    check-cast v6, LX/1ym;

    invoke-static {v0}, LX/1yn;->a(LX/0QB;)LX/1yn;

    move-result-object v7

    check-cast v7, LX/1yn;

    invoke-static {v0}, LX/1yo;->b(LX/0QB;)LX/1yo;

    move-result-object v8

    check-cast v8, LX/1yo;

    invoke-static {v0}, LX/1yq;->a(LX/0QB;)LX/1yq;

    move-result-object v9

    check-cast v9, LX/1yq;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v11

    check-cast v11, LX/14w;

    invoke-static {v0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v12

    check-cast v12, LX/1yf;

    invoke-direct/range {v3 .. v12}, LX/1yj;-><init>(LX/1yk;LX/1yl;LX/1ym;LX/1yn;LX/1yo;LX/1yq;LX/0Uh;LX/14w;LX/1yf;)V

    .line 351131
    move-object v0, v3

    .line 351132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 351136
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351137
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v1, -0x7a50adf8

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 351138
    iget-object v1, p0, LX/1yj;->c:LX/1ym;

    invoke-virtual {v1, p1}, LX/1ym;->c(LX/1De;)LX/BsV;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/BsV;->a(LX/1Pb;)LX/BsV;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/BsV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BsV;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BsV;->b(Ljava/lang/String;)LX/BsV;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/BsV;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/BsV;

    move-result-object v0

    const-string v1, "FEED_UNCONNECTED_STORY"

    invoke-virtual {v0, v1}, LX/BsV;->c(Ljava/lang/String;)LX/BsV;

    move-result-object v0

    const-string v1, "FEED_UNCONNECTED_STORY"

    invoke-virtual {v0, v1}, LX/BsV;->d(Ljava/lang/String;)LX/BsV;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method private static c(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 351139
    iget-object v0, p0, LX/1yj;->b:LX/1yl;

    const/4 v1, 0x0

    .line 351140
    new-instance v2, LX/33K;

    invoke-direct {v2, v0}, LX/33K;-><init>(LX/1yl;)V

    .line 351141
    iget-object p0, v0, LX/1yl;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/33L;

    .line 351142
    if-nez p0, :cond_0

    .line 351143
    new-instance p0, LX/33L;

    invoke-direct {p0, v0}, LX/33L;-><init>(LX/1yl;)V

    .line 351144
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/33L;->a$redex0(LX/33L;LX/1De;IILX/33K;)V

    .line 351145
    move-object v2, p0

    .line 351146
    move-object v1, v2

    .line 351147
    move-object v0, v1

    .line 351148
    check-cast p3, LX/1Pc;

    .line 351149
    iget-object v1, v0, LX/33L;->a:LX/33K;

    iput-object p3, v1, LX/33K;->b:LX/1Pc;

    .line 351150
    iget-object v1, v0, LX/33L;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 351151
    move-object v0, v0

    .line 351152
    iget-object v1, v0, LX/33L;->a:LX/33K;

    iput-object p2, v1, LX/33K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 351153
    iget-object v1, v0, LX/33L;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 351154
    move-object v0, v0

    .line 351155
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method private static d(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 351156
    iget-object v0, p0, LX/1yj;->d:LX/1yn;

    const/4 v1, 0x0

    .line 351157
    new-instance v2, LX/Bsg;

    invoke-direct {v2, v0}, LX/Bsg;-><init>(LX/1yn;)V

    .line 351158
    iget-object p0, v0, LX/1yn;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bsf;

    .line 351159
    if-nez p0, :cond_0

    .line 351160
    new-instance p0, LX/Bsf;

    invoke-direct {p0, v0}, LX/Bsf;-><init>(LX/1yn;)V

    .line 351161
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Bsf;->a$redex0(LX/Bsf;LX/1De;IILX/Bsg;)V

    .line 351162
    move-object v2, p0

    .line 351163
    move-object v1, v2

    .line 351164
    move-object v0, v1

    .line 351165
    iget-object v1, v0, LX/Bsf;->a:LX/Bsg;

    iput-object p2, v1, LX/Bsg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 351166
    iget-object v1, v0, LX/Bsf;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 351167
    move-object v0, v0

    .line 351168
    check-cast p3, LX/1Pq;

    .line 351169
    iget-object v1, v0, LX/Bsf;->a:LX/Bsg;

    iput-object p3, v1, LX/Bsg;->d:LX/1Pq;

    .line 351170
    iget-object v1, v0, LX/Bsf;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 351171
    move-object v0, v0

    .line 351172
    const v1, 0x7f02178c

    .line 351173
    iget-object v2, v0, LX/Bsf;->a:LX/Bsg;

    invoke-virtual {v0, v1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object p0

    iput-object p0, v2, LX/Bsg;->b:LX/1dc;

    .line 351174
    iget-object v2, v0, LX/Bsf;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 351175
    move-object v0, v0

    .line 351176
    const v1, 0x7f02178b

    .line 351177
    iget-object v2, v0, LX/Bsf;->a:LX/Bsg;

    invoke-virtual {v0, v1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object p0

    iput-object p0, v2, LX/Bsg;->c:LX/1dc;

    .line 351178
    iget-object v2, v0, LX/Bsf;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 351179
    move-object v0, v0

    .line 351180
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;ZZZZIIIIII)LX/1Dg;
    .locals 5
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;ZZZZIIIIII)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 351181
    const/4 v2, 0x0

    .line 351182
    if-nez p6, :cond_0

    iget-object v1, p0, LX/1yj;->f:LX/1yq;

    iget-object v3, p0, LX/1yj;->e:LX/1yo;

    iget-object v4, p0, LX/1yj;->h:LX/0Uh;

    invoke-static {p2, v1, v3, v4}, LX/1yr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yq;LX/1yo;LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351183
    invoke-static {p0, p1, p2, p3}, LX/1yj;->a(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;

    move-result-object v1

    .line 351184
    :goto_0
    if-nez v1, :cond_3

    .line 351185
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 351186
    :goto_1
    return-object v1

    .line 351187
    :cond_0
    if-nez p4, :cond_1

    iget-object v1, p0, LX/1yj;->g:LX/14w;

    invoke-static {p2, v1}, LX/1ys;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 351188
    invoke-static {p0, p1, p2, p3}, LX/1yj;->b(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;

    move-result-object v1

    goto :goto_0

    .line 351189
    :cond_1
    if-nez p7, :cond_2

    iget-object v1, p0, LX/1yj;->g:LX/14w;

    invoke-static {p2, v1}, LX/1yt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 351190
    invoke-static {p0, p1, p2, p3}, LX/1yj;->c(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;

    move-result-object v1

    goto :goto_0

    .line 351191
    :cond_2
    if-nez p5, :cond_5

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1yu;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 351192
    invoke-static {p0, p1, p2, p3}, LX/1yj;->d(LX/1yj;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1Di;

    move-result-object v1

    goto :goto_0

    .line 351193
    :cond_3
    iget-object v2, p0, LX/1yj;->i:LX/1yf;

    invoke-virtual {v2}, LX/1yf;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 351194
    invoke-interface {v1, p8}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p9}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2, p10}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    move/from16 v0, p11

    invoke-interface {v1, v2, v0}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    move/from16 v0, p12

    invoke-interface {v1, v2, v0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    move/from16 v0, p13

    invoke-interface {v1, v2, v0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_1

    .line 351195
    :cond_4
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method
