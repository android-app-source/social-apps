.class public final enum LX/1zu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1zu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1zu;

.field public static final enum IMAGE:LX/1zu;

.field public static final enum VECTOR_ANIMATED_ALWAYS:LX/1zu;

.field public static final enum VECTOR_ANIMATED_SELECTED:LX/1zu;

.field public static final enum VECTOR_STATIC:LX/1zu;


# instance fields
.field public final isVectorBased:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 354258
    new-instance v0, LX/1zu;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v3, v3}, LX/1zu;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1zu;->IMAGE:LX/1zu;

    .line 354259
    new-instance v0, LX/1zu;

    const-string v1, "VECTOR_STATIC"

    invoke-direct {v0, v1, v2, v2}, LX/1zu;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1zu;->VECTOR_STATIC:LX/1zu;

    .line 354260
    new-instance v0, LX/1zu;

    const-string v1, "VECTOR_ANIMATED_ALWAYS"

    invoke-direct {v0, v1, v4, v2}, LX/1zu;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1zu;->VECTOR_ANIMATED_ALWAYS:LX/1zu;

    .line 354261
    new-instance v0, LX/1zu;

    const-string v1, "VECTOR_ANIMATED_SELECTED"

    invoke-direct {v0, v1, v5, v2}, LX/1zu;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/1zu;->VECTOR_ANIMATED_SELECTED:LX/1zu;

    .line 354262
    const/4 v0, 0x4

    new-array v0, v0, [LX/1zu;

    sget-object v1, LX/1zu;->IMAGE:LX/1zu;

    aput-object v1, v0, v3

    sget-object v1, LX/1zu;->VECTOR_STATIC:LX/1zu;

    aput-object v1, v0, v2

    sget-object v1, LX/1zu;->VECTOR_ANIMATED_ALWAYS:LX/1zu;

    aput-object v1, v0, v4

    sget-object v1, LX/1zu;->VECTOR_ANIMATED_SELECTED:LX/1zu;

    aput-object v1, v0, v5

    sput-object v0, LX/1zu;->$VALUES:[LX/1zu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 354263
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 354264
    iput-boolean p3, p0, LX/1zu;->isVectorBased:Z

    .line 354265
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1zu;
    .locals 1

    .prologue
    .line 354266
    const-class v0, LX/1zu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1zu;

    return-object v0
.end method

.method public static values()[LX/1zu;
    .locals 1

    .prologue
    .line 354267
    sget-object v0, LX/1zu;->$VALUES:[LX/1zu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1zu;

    return-object v0
.end method
