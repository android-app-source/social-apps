.class public LX/1zq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0SG;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public d:J

.field private e:Z


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354185
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1zq;->c:Ljava/util/Map;

    .line 354186
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1zq;->e:Z

    .line 354187
    iput-object p1, p0, LX/1zq;->a:LX/0Zb;

    .line 354188
    iput-object p2, p0, LX/1zq;->b:LX/0SG;

    .line 354189
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 354190
    iget-object v0, p0, LX/1zq;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1zq;->d:J

    sub-long/2addr v0, v2

    .line 354191
    monitor-enter p0

    .line 354192
    :try_start_0
    iget-object v2, p0, LX/1zq;->c:Ljava/util/Map;

    const-string v3, "assets_download_start_time_key_%d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354193
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(IZ)V
    .locals 5

    .prologue
    .line 354194
    if-nez p2, :cond_0

    .line 354195
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1zq;->e:Z

    .line 354196
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/1zq;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1zq;->d:J

    sub-long/2addr v0, v2

    .line 354197
    :goto_0
    monitor-enter p0

    .line 354198
    :try_start_0
    iget-object v2, p0, LX/1zq;->c:Ljava/util/Map;

    const-string v3, "reaction_ready_time_key_%d"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354199
    monitor-exit p0

    return-void

    .line 354200
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 354201
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 354202
    iget-object v0, p0, LX/1zq;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/1zq;->d:J

    sub-long/2addr v0, v2

    .line 354203
    monitor-enter p0

    .line 354204
    :try_start_0
    iget-object v2, p0, LX/1zq;->c:Ljava/util/Map;

    const-string v3, "base_settings_fetch_complete_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354205
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354206
    if-nez p1, :cond_0

    .line 354207
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1zq;->e:Z

    .line 354208
    invoke-virtual {p0}, LX/1zq;->b()V

    .line 354209
    :cond_0
    return-void

    .line 354210
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 354211
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "feedback_reaction_settings_fetch"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 354212
    const-string v0, "status"

    iget-boolean v2, p0, LX/1zq;->e:Z

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354213
    monitor-enter p0

    .line 354214
    :try_start_0
    iget-object v0, p0, LX/1zq;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 354215
    iget-object v3, p0, LX/1zq;->c:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 354216
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354217
    const-string v0, "all_reactions_ready_time"

    iget-object v2, p0, LX/1zq;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/1zq;->d:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354218
    iget-object v0, p0, LX/1zq;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 354219
    return-void
.end method
