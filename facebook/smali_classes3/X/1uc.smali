.class public LX/1uc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1uc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 341311
    const-class v0, LX/1uc;

    sput-object v0, LX/1uc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 341312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 341313
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 341314
    instance-of v0, v1, Lcom/facebook/graphql/model/Sponsorable;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 341315
    check-cast v0, Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v0}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v0

    .line 341316
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341317
    const-string v0, "Ad"

    .line 341318
    :goto_0
    return-object v0

    .line 341319
    :cond_0
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    if-eqz v0, :cond_2

    .line 341320
    :cond_1
    const-string v0, "Ad"

    goto :goto_0

    .line 341321
    :cond_2
    if-eqz v1, :cond_3

    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_5

    .line 341322
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 341323
    const-string v0, "FeedUnit is not GraphQLStory so can\'t extract actors.FeedUnitType:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341324
    if-nez v1, :cond_4

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 341325
    sget-object v0, LX/1uc;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 341326
    const-string v0, "Unknown"

    goto :goto_0

    .line 341327
    :cond_4
    invoke-interface {v1}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 341328
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 341329
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 341330
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 341331
    const-string v2, "FeedUnit GraphQLStory does not have any actors.  Id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341332
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341333
    sget-object v1, LX/1uc;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 341334
    const-string v0, "Unknown"

    goto :goto_0

    .line 341335
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 341336
    if-nez v0, :cond_8

    .line 341337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 341338
    const-string v2, "FeedUnit GraphQLStory actor doesn\'t have GraphQLObjectType.  Id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341339
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341340
    sget-object v1, LX/1uc;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 341341
    const-string v0, "Unknown"

    goto/16 :goto_0

    .line 341342
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 341343
    const-string v1, "Page"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "Page"

    goto/16 :goto_0

    :cond_9
    const-string v0, "User"

    goto/16 :goto_0
.end method
