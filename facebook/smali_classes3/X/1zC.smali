.class public LX/1zC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:[I

.field private static volatile m:LX/1zC;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1zU;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1zD;

.field private final e:LX/03V;

.field private final f:Landroid/content/res/Resources;

.field private final g:LX/7HC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:LX/1zH;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7HA;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0W3;

.field private k:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 352127
    const-class v0, LX/1zC;

    sput-object v0, LX/1zC;->a:Ljava/lang/Class;

    .line 352128
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/1zC;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xa9
        0xae
        0x2122
        0x203c
        0x2049
        0x3297
        0x3299
    .end array-data
.end method

.method public constructor <init>(LX/0Ot;LX/1zD;LX/03V;Landroid/content/res/Resources;LX/7HC;LX/1zH;LX/0Ot;LX/0W3;)V
    .locals 1
    .param p5    # LX/7HC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1zU;",
            ">;",
            "LX/1zD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/res/Resources;",
            "LX/7HC;",
            "Lcom/facebook/ui/emoji/model/EmojisData;",
            "LX/0Ot",
            "<",
            "LX/7HA;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 352114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 352115
    iput-object v0, p0, LX/1zC;->k:Ljava/lang/Boolean;

    .line 352116
    iput-object v0, p0, LX/1zC;->l:Ljava/lang/Boolean;

    .line 352117
    const-string v0, "Binding for emojisData not defined."

    invoke-static {p6, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352118
    iput-object p1, p0, LX/1zC;->c:LX/0Ot;

    .line 352119
    iput-object p2, p0, LX/1zC;->d:LX/1zD;

    .line 352120
    iput-object p3, p0, LX/1zC;->e:LX/03V;

    .line 352121
    iput-object p4, p0, LX/1zC;->f:Landroid/content/res/Resources;

    .line 352122
    iput-object p5, p0, LX/1zC;->g:LX/7HC;

    .line 352123
    iput-object p6, p0, LX/1zC;->h:LX/1zH;

    .line 352124
    iput-object p7, p0, LX/1zC;->i:LX/0Ot;

    .line 352125
    iput-object p8, p0, LX/1zC;->j:LX/0W3;

    .line 352126
    return-void
.end method

.method public static a(LX/0QB;)LX/1zC;
    .locals 12

    .prologue
    .line 352101
    sget-object v0, LX/1zC;->m:LX/1zC;

    if-nez v0, :cond_1

    .line 352102
    const-class v1, LX/1zC;

    monitor-enter v1

    .line 352103
    :try_start_0
    sget-object v0, LX/1zC;->m:LX/1zC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 352104
    if-eqz v2, :cond_0

    .line 352105
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 352106
    new-instance v3, LX/1zC;

    const/16 v4, 0x12ad

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/1zD;->a(LX/0QB;)LX/1zD;

    move-result-object v5

    check-cast v5, LX/1zD;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1zE;->a(LX/0QB;)LX/7HC;

    move-result-object v8

    check-cast v8, LX/7HC;

    invoke-static {v0}, LX/1zF;->a(LX/0QB;)LX/1zH;

    move-result-object v9

    check-cast v9, LX/1zH;

    const/16 v10, 0x3766

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v11

    check-cast v11, LX/0W3;

    invoke-direct/range {v3 .. v11}, LX/1zC;-><init>(LX/0Ot;LX/1zD;LX/03V;Landroid/content/res/Resources;LX/7HC;LX/1zH;LX/0Ot;LX/0W3;)V

    .line 352107
    move-object v0, v3

    .line 352108
    sput-object v0, LX/1zC;->m:LX/1zC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352109
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 352110
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 352111
    :cond_1
    sget-object v0, LX/1zC;->m:LX/1zC;

    return-object v0

    .line 352112
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 352113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 352082
    if-nez p3, :cond_0

    iget-object v0, p0, LX/1zC;->g:LX/7HC;

    if-nez v0, :cond_1

    .line 352083
    :cond_0
    iget-object v0, p0, LX/1zC;->f:Landroid/content/res/Resources;

    .line 352084
    iget v1, p1, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    move v1, v1

    .line 352085
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v2, v0

    .line 352086
    :goto_0
    if-nez v2, :cond_3

    .line 352087
    const/4 v0, 0x0

    .line 352088
    :goto_1
    return-object v0

    .line 352089
    :cond_1
    iget-object v0, p0, LX/1zC;->g:LX/7HC;

    invoke-interface {v0}, LX/7HC;->a()I

    move-result v0

    .line 352090
    if-nez v0, :cond_2

    .line 352091
    iget v0, p1, Lcom/facebook/ui/emoji/model/Emoji;->a:I

    move v0, v0

    .line 352092
    :cond_2
    iget-object v1, p0, LX/1zC;->f:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    move-object v2, v0

    goto :goto_0

    .line 352093
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    .line 352094
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    .line 352095
    if-eqz p3, :cond_4

    .line 352096
    iget-object v3, p0, LX/1zC;->f:Landroid/content/res/Resources;

    const/high16 v4, 0x41800000    # 16.0f

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v3

    .line 352097
    mul-int/2addr v1, p2

    div-int/2addr v1, v3

    .line 352098
    mul-int/2addr v0, p2

    div-int/2addr v0, v3

    .line 352099
    :cond_4
    invoke-virtual {v2, v5, v5, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 352100
    new-instance v0, LX/22D;

    invoke-direct {v0, v2, p1}, LX/22D;-><init>(Landroid/graphics/drawable/BitmapDrawable;Lcom/facebook/ui/emoji/model/Emoji;)V

    goto :goto_1
.end method

.method public static a(LX/1zC;Landroid/text/Editable;LX/1zS;)Z
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 351914
    invoke-direct {p0}, LX/1zC;->b()Z

    move-result v7

    .line 351915
    const/4 v4, 0x0

    .line 351916
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 351917
    new-array v8, v0, [C

    .line 351918
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v8, v2}, Landroid/text/Editable;->getChars(II[CI)V

    .line 351919
    iget v3, p2, LX/1zS;->b:I

    .line 351920
    iget v1, p2, LX/1zS;->c:I

    if-gez v1, :cond_1

    move v1, v0

    .line 351921
    :goto_0
    new-instance v9, LX/1zT;

    invoke-direct {v9}, LX/1zT;-><init>()V

    .line 351922
    const/4 v2, 0x0

    .line 351923
    const/4 v5, 0x0

    move v6, v3

    move v3, v4

    .line 351924
    :goto_1
    add-int/lit8 v0, v1, -0x1

    if-ge v6, v0, :cond_1d

    .line 351925
    aget-char v4, v8, v6

    .line 351926
    const/16 v0, 0x2122

    if-ge v4, v0, :cond_c

    const/16 v0, 0x203c

    if-eq v4, v0, :cond_c

    const/16 v0, 0x2049

    if-eq v4, v0, :cond_c

    .line 351927
    sparse-switch v4, :sswitch_data_0

    :cond_0
    move-object v0, v2

    move v2, v3

    .line 351928
    :goto_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v2

    move-object v2, v0

    .line 351929
    goto :goto_1

    .line 351930
    :cond_1
    iget v0, p2, LX/1zS;->c:I

    add-int/2addr v0, v3

    move v1, v0

    goto :goto_0

    .line 351931
    :sswitch_0
    if-nez v7, :cond_3

    add-int/lit8 v0, v6, 0x1

    aget-char v0, v8, v0

    const/16 v10, 0x20e3

    if-ne v0, v10, :cond_3

    .line 351932
    iget-object v0, p0, LX/1zC;->h:LX/1zH;

    const/16 v10, 0x20e3

    const/4 v11, -0x1

    invoke-virtual {v0, v4, v10, v11}, LX/1zH;->a(III)I

    move-result v0

    .line 351933
    const/4 v10, -0x1

    if-eq v0, v10, :cond_2

    .line 351934
    new-instance v10, Lcom/facebook/ui/emoji/model/Emoji;

    const/16 v11, 0x20e3

    invoke-direct {v10, v0, v4, v11}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    .line 351935
    iget v0, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v10, v0, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351936
    if-eqz v0, :cond_2

    .line 351937
    const/4 v3, 0x1

    .line 351938
    add-int/lit8 v4, v6, 0x2

    const/16 v10, 0x21

    invoke-interface {p1, v0, v6, v4, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 351939
    :cond_2
    add-int/lit8 v0, v6, 0x2

    move v6, v0

    .line 351940
    goto :goto_1

    .line 351941
    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    .line 351942
    goto :goto_1

    .line 351943
    :sswitch_1
    if-nez v7, :cond_5

    add-int/lit8 v0, v6, 0x1

    aget-char v0, v8, v0

    const/16 v10, 0x20e3

    if-ne v0, v10, :cond_5

    .line 351944
    iget-object v0, p0, LX/1zC;->h:LX/1zH;

    const/16 v10, 0x20e3

    const/4 v11, -0x1

    invoke-virtual {v0, v4, v10, v11}, LX/1zH;->a(III)I

    move-result v0

    .line 351945
    const/4 v10, -0x1

    if-eq v0, v10, :cond_4

    .line 351946
    new-instance v10, Lcom/facebook/ui/emoji/model/Emoji;

    const/16 v11, 0x20e3

    invoke-direct {v10, v0, v4, v11}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    .line 351947
    iget v0, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v10, v0, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351948
    if-eqz v0, :cond_4

    .line 351949
    const/4 v3, 0x1

    .line 351950
    add-int/lit8 v4, v6, 0x2

    const/16 v10, 0x21

    invoke-interface {p1, v0, v6, v4, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 351951
    :cond_4
    add-int/lit8 v0, v6, 0x2

    move v6, v0

    .line 351952
    goto/16 :goto_1

    .line 351953
    :cond_5
    :sswitch_2
    if-lez v6, :cond_7

    .line 351954
    add-int/lit8 v0, v6, -0x1

    aget-char v0, v8, v0

    .line 351955
    :goto_3
    sparse-switch v0, :sswitch_data_1

    .line 351956
    :cond_6
    :goto_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    .line 351957
    goto/16 :goto_1

    .line 351958
    :cond_7
    const/16 v0, 0x20

    goto :goto_3

    .line 351959
    :sswitch_3
    invoke-static {v8, v6, v9}, LX/1zU;->a([CILX/1zT;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 351960
    iget v0, v9, LX/1zT;->b:I

    add-int/2addr v0, v6

    if-ge v0, v1, :cond_8

    .line 351961
    iget v0, v9, LX/1zT;->b:I

    add-int/2addr v0, v6

    aget-char v0, v8, v0

    .line 351962
    :goto_5
    sparse-switch v0, :sswitch_data_2

    goto :goto_4

    .line 351963
    :sswitch_4
    iget-object v0, p0, LX/1zC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zU;

    iget v4, v9, LX/1zT;->a:I

    const/4 v12, -0x1

    .line 351964
    iget-object v10, v0, LX/1zU;->f:LX/1zH;

    const/4 v11, 0x0

    invoke-virtual {v10, v4, v11, v12}, LX/1zH;->a(III)I

    move-result v11

    .line 351965
    if-eq v11, v12, :cond_28

    .line 351966
    new-instance v10, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v10, v11, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 351967
    :goto_6
    move-object v0, v10

    .line 351968
    if-nez v0, :cond_9

    .line 351969
    iget v0, v9, LX/1zT;->b:I

    add-int/2addr v0, v6

    move v6, v0

    .line 351970
    goto/16 :goto_1

    .line 351971
    :cond_8
    const/16 v0, 0x20

    goto :goto_5

    .line 351972
    :cond_9
    iget v4, p2, LX/1zS;->a:I

    iget-boolean v10, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v4, v10}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351973
    if-eqz v0, :cond_26

    .line 351974
    const/4 v3, 0x1

    .line 351975
    iget v4, v9, LX/1zT;->b:I

    add-int/2addr v4, v6

    const/16 v10, 0x21

    invoke-interface {p1, v0, v6, v4, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 351976
    add-int/lit8 v0, v5, 0x1

    .line 351977
    :goto_7
    const/16 v4, 0x28

    if-le v0, v4, :cond_b

    .line 351978
    :cond_a
    :goto_8
    return v3

    .line 351979
    :cond_b
    iget v4, v9, LX/1zT;->b:I

    add-int/2addr v4, v6

    move v5, v0

    move v6, v4

    .line 351980
    goto/16 :goto_1

    .line 351981
    :sswitch_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v0, v4, :cond_0

    .line 351982
    if-nez v2, :cond_25

    .line 351983
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f020602

    const/4 v4, 0x0

    invoke-direct {v0, v2, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 351984
    iget v2, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v2, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351985
    :goto_9
    if-eqz v0, :cond_27

    .line 351986
    const/4 v2, 0x1

    .line 351987
    add-int/lit8 v3, v6, 0x1

    const/16 v4, 0x21

    invoke-interface {p1, v0, v6, v3, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_2

    .line 351988
    :cond_c
    if-eqz v7, :cond_d

    .line 351989
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    .line 351990
    goto/16 :goto_1

    .line 351991
    :cond_d
    const v0, 0xd800

    if-lt v4, v0, :cond_e

    const v0, 0xdfff

    if-le v4, v0, :cond_12

    .line 351992
    :cond_e
    const v0, 0xe001

    if-lt v4, v0, :cond_24

    const v0, 0xe536

    if-gt v4, v0, :cond_24

    .line 351993
    iget-object v0, p0, LX/1zC;->h:LX/1zH;

    invoke-virtual {v0, v4, v4}, LX/1zH;->a(II)I

    move-result v0

    .line 351994
    :goto_a
    iget-object v10, p0, LX/1zC;->h:LX/1zH;

    const/4 v11, 0x0

    const/4 v12, -0x1

    invoke-virtual {v10, v0, v11, v12}, LX/1zH;->a(III)I

    move-result v0

    .line 351995
    const/4 v10, -0x1

    if-eq v0, v10, :cond_f

    .line 351996
    new-instance v10, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v10, v0, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 351997
    iget v0, p2, LX/1zS;->a:I

    iget-boolean v11, p2, LX/1zS;->e:Z

    invoke-static {p0, v10, v0, v11}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351998
    if-eqz v0, :cond_f

    .line 351999
    const/4 v3, 0x1

    .line 352000
    add-int/lit8 v10, v6, 0x1

    const/16 v11, 0x21

    invoke-interface {p1, v0, v6, v10, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352001
    :cond_f
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x10

    if-ge v0, v10, :cond_11

    invoke-static {v4}, LX/7HB;->a(I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 352002
    if-nez v2, :cond_10

    .line 352003
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f020602

    const/4 v4, 0x0

    invoke-direct {v0, v2, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352004
    iget v2, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v2, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v2

    .line 352005
    :cond_10
    if-eqz v2, :cond_11

    .line 352006
    const/4 v3, 0x1

    .line 352007
    add-int/lit8 v0, v6, 0x1

    const/16 v4, 0x21

    invoke-interface {p1, v2, v6, v0, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352008
    :cond_11
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    .line 352009
    goto/16 :goto_1

    .line 352010
    :cond_12
    add-int/lit8 v0, v6, 0x1

    aget-char v0, v8, v0

    .line 352011
    const v10, 0xdc00

    if-lt v0, v10, :cond_13

    const v10, 0xdfff

    if-le v0, v10, :cond_14

    .line 352012
    :cond_13
    add-int/lit8 v0, v6, 0x2

    move v6, v0

    .line 352013
    goto/16 :goto_1

    .line 352014
    :cond_14
    const v10, 0xd800

    sub-int/2addr v4, v10

    shl-int/lit8 v4, v4, 0xa

    const v10, 0xdc00

    sub-int/2addr v0, v10

    add-int/2addr v0, v4

    const/high16 v4, 0x10000

    add-int/2addr v0, v4

    .line 352015
    const v4, 0x1f1e6

    if-lt v0, v4, :cond_17

    const v4, 0x1f1ff

    if-gt v0, v4, :cond_17

    add-int/lit8 v4, v6, 0x4

    if-ge v4, v1, :cond_17

    add-int/lit8 v4, v6, 0x2

    aget-char v4, v8, v4

    const v10, 0xd800

    if-lt v4, v10, :cond_17

    add-int/lit8 v4, v6, 0x2

    aget-char v4, v8, v4

    const v10, 0xdc00

    if-ge v4, v10, :cond_17

    add-int/lit8 v4, v6, 0x3

    aget-char v4, v8, v4

    const v10, 0xdc00

    if-lt v4, v10, :cond_17

    add-int/lit8 v4, v6, 0x3

    aget-char v4, v8, v4

    const v10, 0xdfff

    if-gt v4, v10, :cond_17

    .line 352016
    add-int/lit8 v4, v6, 0x2

    aget-char v4, v8, v4

    const v10, 0xd800

    sub-int/2addr v4, v10

    shl-int/lit8 v4, v4, 0xa

    add-int/lit8 v10, v6, 0x3

    aget-char v10, v8, v10

    const v11, 0xdc00

    sub-int/2addr v10, v11

    add-int/2addr v4, v10

    const/high16 v10, 0x10000

    add-int/2addr v10, v4

    .line 352017
    iget-object v4, p0, LX/1zC;->h:LX/1zH;

    const/4 v11, -0x1

    invoke-virtual {v4, v0, v10, v11}, LX/1zH;->a(III)I

    move-result v11

    .line 352018
    const/4 v4, -0x1

    if-eq v11, v4, :cond_16

    .line 352019
    new-instance v4, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v4, v11, v0, v10}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(III)V

    move-object v0, v4

    .line 352020
    :goto_b
    if-eqz v0, :cond_15

    .line 352021
    iget v4, p2, LX/1zS;->a:I

    iget-boolean v10, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v4, v10}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 352022
    if-eqz v0, :cond_15

    .line 352023
    const/4 v3, 0x1

    .line 352024
    add-int/lit8 v4, v6, 0x4

    const/16 v10, 0x21

    invoke-interface {p1, v0, v6, v4, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352025
    :cond_15
    add-int/lit8 v0, v6, 0x4

    move v6, v0

    .line 352026
    goto/16 :goto_1

    .line 352027
    :cond_16
    const/4 v0, 0x0

    goto :goto_b

    .line 352028
    :cond_17
    const v4, 0x1f495

    if-ne v0, v4, :cond_18

    .line 352029
    const v0, 0x1f493

    .line 352030
    :cond_18
    iget-object v4, p0, LX/1zC;->h:LX/1zH;

    const/4 v10, 0x0

    const/4 v11, -0x1

    invoke-virtual {v4, v0, v10, v11}, LX/1zH;->a(III)I

    move-result v10

    .line 352031
    const/4 v4, -0x1

    if-eq v10, v4, :cond_1c

    .line 352032
    new-instance v4, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v4, v10, v0}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352033
    :goto_c
    if-eqz v4, :cond_19

    .line 352034
    iget v10, p2, LX/1zS;->a:I

    iget-boolean v11, p2, LX/1zS;->e:Z

    invoke-static {p0, v4, v10, v11}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v4

    .line 352035
    if-eqz v4, :cond_19

    .line 352036
    const/4 v3, 0x1

    .line 352037
    add-int/lit8 v10, v6, 0x2

    const/16 v11, 0x21

    invoke-interface {p1, v4, v6, v10, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352038
    :cond_19
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x10

    if-ge v4, v10, :cond_1b

    invoke-static {v0}, LX/7HB;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 352039
    if-nez v2, :cond_1a

    .line 352040
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f020602

    const/4 v4, 0x0

    invoke-direct {v0, v2, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352041
    iget v2, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v2, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v2

    .line 352042
    :cond_1a
    if-eqz v2, :cond_1b

    .line 352043
    const/4 v3, 0x1

    .line 352044
    add-int/lit8 v0, v6, 0x2

    const/16 v4, 0x21

    invoke-interface {p1, v2, v6, v0, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352045
    :cond_1b
    add-int/lit8 v0, v6, 0x2

    move v6, v0

    .line 352046
    goto/16 :goto_1

    .line 352047
    :cond_1c
    const/4 v4, 0x0

    goto :goto_c

    .line 352048
    :cond_1d
    if-ge v6, v1, :cond_a

    .line 352049
    aget-char v1, v8, v6

    .line 352050
    const v0, 0xd800

    if-lt v1, v0, :cond_1e

    const v0, 0xdfff

    if-le v1, v0, :cond_a

    .line 352051
    :cond_1e
    aget-char v0, v8, v6

    packed-switch v0, :pswitch_data_0

    .line 352052
    const v0, 0xe001

    if-lt v1, v0, :cond_23

    const v0, 0xe536

    if-gt v1, v0, :cond_23

    .line 352053
    iget-object v0, p0, LX/1zC;->h:LX/1zH;

    invoke-virtual {v0, v1, v1}, LX/1zH;->a(II)I

    move-result v0

    .line 352054
    :goto_d
    iget-object v4, p0, LX/1zC;->h:LX/1zH;

    const/4 v5, 0x0

    const/4 v7, -0x1

    invoke-virtual {v4, v0, v5, v7}, LX/1zH;->a(III)I

    move-result v4

    .line 352055
    const/4 v5, -0x1

    if-eq v4, v5, :cond_1f

    .line 352056
    new-instance v5, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v5, v4, v1}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352057
    iget v1, p2, LX/1zS;->a:I

    iget-boolean v4, p2, LX/1zS;->e:Z

    invoke-static {p0, v5, v1, v4}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v1

    .line 352058
    if-eqz v1, :cond_1f

    .line 352059
    const/4 v3, 0x1

    .line 352060
    add-int/lit8 v4, v6, 0x1

    const/16 v5, 0x21

    invoke-interface {p1, v1, v6, v4, v5}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 352061
    :cond_1f
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v1, v4, :cond_a

    .line 352062
    invoke-static {v0}, LX/7HB;->a(I)Z

    move-result v1

    if-nez v1, :cond_20

    invoke-static {v0}, LX/7HB;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 352063
    :cond_20
    if-nez v2, :cond_21

    .line 352064
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v1, 0x7f020602

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352065
    iget v1, p2, LX/1zS;->a:I

    iget-boolean v2, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v1, v2}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v2

    .line 352066
    :cond_21
    if-eqz v2, :cond_a

    .line 352067
    const/4 v3, 0x1

    .line 352068
    add-int/lit8 v0, v6, 0x1

    const/16 v1, 0x21

    invoke-interface {p1, v2, v6, v0, v1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_8

    .line 352069
    :pswitch_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_a

    .line 352070
    if-nez v2, :cond_22

    .line 352071
    new-instance v0, Lcom/facebook/ui/emoji/model/Emoji;

    const v1, 0x7f020602

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    .line 352072
    iget v1, p2, LX/1zS;->a:I

    iget-boolean v2, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v1, v2}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v2

    .line 352073
    :cond_22
    if-eqz v2, :cond_a

    .line 352074
    const/4 v3, 0x1

    .line 352075
    add-int/lit8 v0, v6, 0x1

    const/16 v1, 0x21

    invoke-interface {p1, v2, v6, v0, v1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_8

    :cond_23
    move v0, v1

    goto :goto_d

    :cond_24
    move v0, v4

    goto/16 :goto_a

    :cond_25
    move-object v0, v2

    goto/16 :goto_9

    :cond_26
    move v0, v5

    goto/16 :goto_7

    :cond_27
    move v2, v3

    goto/16 :goto_2

    .line 352076
    :cond_28
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x10

    if-ge v10, v11, :cond_29

    .line 352077
    sget-object v10, LX/1zU;->d:Lcom/facebook/ui/emoji/model/Emoji;

    goto/16 :goto_6

    .line 352078
    :cond_29
    const/4 v10, 0x0

    goto/16 :goto_6

    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_0
        0x26 -> :sswitch_2
        0x28 -> :sswitch_2
        0x30 -> :sswitch_1
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_1
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x3a -> :sswitch_2
        0x3b -> :sswitch_2
        0x3c -> :sswitch_2
        0x3d -> :sswitch_2
        0x3e -> :sswitch_2
        0x4f -> :sswitch_2
        0x5e -> :sswitch_2
        0x6f -> :sswitch_2
        0xa9 -> :sswitch_5
        0xaa -> :sswitch_5
        0xab -> :sswitch_5
        0xac -> :sswitch_5
        0xad -> :sswitch_5
        0xae -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xb -> :sswitch_3
        0xc -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x22 -> :sswitch_3
        0x27 -> :sswitch_3
        0x2e -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xb -> :sswitch_4
        0xc -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x21 -> :sswitch_4
        0x22 -> :sswitch_4
        0x27 -> :sswitch_4
        0x2c -> :sswitch_4
        0x2e -> :sswitch_4
        0x3f -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0xa9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 352079
    iget-object v0, p0, LX/1zC;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 352080
    iget-object v0, p0, LX/1zC;->j:LX/0W3;

    sget-wide v2, LX/0X5;->cc:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/1zC;->l:Ljava/lang/Boolean;

    .line 352081
    :cond_0
    iget-object v0, p0, LX/1zC;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static b(LX/1zC;Landroid/text/Editable;LX/1zS;)Z
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 351852
    iget v0, p2, LX/1zS;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    move v1, v0

    .line 351853
    :goto_0
    iget v0, p2, LX/1zS;->b:I

    move v5, v0

    move v6, v2

    move v4, v2

    .line 351854
    :goto_1
    if-ge v5, v1, :cond_8

    .line 351855
    iget-object v0, p0, LX/1zC;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7HA;

    invoke-virtual {v0, p1, v5}, LX/7HA;->a(Ljava/lang/CharSequence;I)Lcom/facebook/ui/emoji/model/Emoji;

    move-result-object v0

    .line 351856
    if-nez v0, :cond_3

    .line 351857
    if-nez v6, :cond_0

    invoke-interface {p1, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v3

    .line 351858
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move v6, v0

    .line 351859
    goto :goto_1

    .line 351860
    :cond_1
    iget v0, p2, LX/1zS;->b:I

    iget v1, p2, LX/1zS;->c:I

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 351861
    goto :goto_2

    .line 351862
    :cond_3
    iget v7, v0, Lcom/facebook/ui/emoji/model/Emoji;->b:I

    invoke-static {v7}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    .line 351863
    iget v8, v0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    if-eqz v8, :cond_4

    .line 351864
    iget v8, v0, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    invoke-static {v8}, Ljava/lang/Character;->charCount(I)I

    move-result v8

    add-int/2addr v7, v8

    .line 351865
    :cond_4
    iget-object v8, v0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    if-eqz v8, :cond_6

    .line 351866
    iget-object v8, v0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v10

    const/4 v8, 0x0

    move v9, v8

    move v8, v7

    :goto_3
    if-ge v9, v10, :cond_5

    iget-object v7, v0, Lcom/facebook/ui/emoji/model/Emoji;->e:LX/0Px;

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 351867
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->charCount(I)I

    move-result v7

    add-int/2addr v8, v7

    .line 351868
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_3

    :cond_5
    move v7, v8

    .line 351869
    :cond_6
    move v7, v7

    .line 351870
    iget v8, p2, LX/1zS;->a:I

    iget-boolean v9, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v8, v9}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v8

    .line 351871
    if-eqz v8, :cond_d

    .line 351872
    add-int v9, v5, v7

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v10

    if-le v9, v10, :cond_7

    .line 351873
    iget-object v8, p0, LX/1zC;->e:LX/03V;

    sget-object v9, LX/1zC;->a:Ljava/lang/Class;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Would have had IndexOutOfBoundsException for emoji:  "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/ui/emoji/model/Emoji;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v9, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v4

    .line 351874
    :goto_4
    add-int v4, v5, v7

    move v5, v4

    move v4, v0

    .line 351875
    goto/16 :goto_1

    .line 351876
    :cond_7
    add-int v0, v5, v7

    const/16 v4, 0x21

    invoke-interface {p1, v8, v5, v0, v4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    move v0, v3

    goto :goto_4

    .line 351877
    :cond_8
    iget-boolean v0, p2, LX/1zS;->d:Z

    if-nez v0, :cond_9

    .line 351878
    :goto_5
    return v4

    .line 351879
    :cond_9
    if-eqz v6, :cond_a

    .line 351880
    goto :goto_6

    .line 351881
    :cond_a
    :goto_6
    invoke-direct {p0, p1, p2}, LX/1zC;->c(Landroid/text/Editable;LX/1zS;)Z

    move-result v0

    if-nez v0, :cond_b

    if-eqz v4, :cond_c

    :cond_b
    move v4, v3

    goto :goto_5

    :cond_c
    move v4, v2

    goto :goto_5

    :cond_d
    move v0, v4

    goto :goto_4
    .line 351882
.end method

.method private c(Landroid/text/Editable;LX/1zS;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 351883
    iget-object v0, p0, LX/1zC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zU;

    .line 351884
    iget-object v2, v0, LX/1zU;->g:Ljava/util/regex/Pattern;

    if-nez v2, :cond_0

    .line 351885
    invoke-static {v0}, LX/1zU;->c(LX/1zU;)Ljava/util/regex/Pattern;

    move-result-object v2

    iput-object v2, v0, LX/1zU;->g:Ljava/util/regex/Pattern;

    .line 351886
    :cond_0
    iget-object v2, v0, LX/1zU;->g:Ljava/util/regex/Pattern;

    move-object v0, v2

    .line 351887
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    move v2, v1

    .line 351888
    :goto_0
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x28

    if-ge v1, v0, :cond_3

    .line 351889
    iget-object v0, p0, LX/1zC;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zU;

    invoke-virtual {v4, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v10, -0x1

    .line 351890
    invoke-static {v0, v5}, LX/1zU;->b(LX/1zU;Ljava/lang/String;)I

    move-result v7

    .line 351891
    if-ne v7, v10, :cond_4

    .line 351892
    :cond_1
    :goto_1
    move-object v0, v6

    .line 351893
    if-eqz v0, :cond_2

    .line 351894
    iget v5, p2, LX/1zS;->a:I

    iget-boolean v6, p2, LX/1zS;->e:Z

    invoke-static {p0, v0, v5, v6}, LX/1zC;->a(LX/1zC;Lcom/facebook/ui/emoji/model/Emoji;IZ)LX/22D;

    move-result-object v0

    .line 351895
    if-eqz v0, :cond_2

    .line 351896
    invoke-virtual {v4, v3}, Ljava/util/regex/Matcher;->start(I)I

    move-result v2

    invoke-virtual {v4, v3}, Ljava/util/regex/Matcher;->end(I)I

    move-result v5

    const/16 v6, 0x21

    invoke-interface {p1, v0, v2, v5, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    move v2, v3

    .line 351897
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 351898
    :cond_3
    return v2

    .line 351899
    :cond_4
    iget-object v8, v0, LX/1zU;->f:LX/1zH;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9, v10}, LX/1zH;->a(III)I

    move-result v8

    .line 351900
    if-eq v8, v10, :cond_1

    new-instance v6, Lcom/facebook/ui/emoji/model/Emoji;

    invoke-direct {v6, v8, v7}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(II)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/text/Editable;)Z
    .locals 2

    .prologue
    .line 351901
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, LX/1zC;->a(Landroid/text/Editable;II)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/text/Editable;I)Z
    .locals 2

    .prologue
    .line 351902
    new-instance v0, LX/1zS;

    invoke-direct {v0}, LX/1zS;-><init>()V

    .line 351903
    iput p2, v0, LX/1zS;->a:I

    .line 351904
    const/16 v1, 0x1

    if-eqz v1, :cond_0

    .line 351905
    invoke-static {p0, p1, v0}, LX/1zC;->a(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    move-result v0

    .line 351906
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1, v0}, LX/1zC;->b(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/text/Editable;II)Z
    .locals 2

    .prologue
    .line 351907
    new-instance v0, LX/1zS;

    invoke-direct {v0}, LX/1zS;-><init>()V

    .line 351908
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/1zS;->e:Z

    .line 351909
    iput p2, v0, LX/1zS;->b:I

    .line 351910
    iput p3, v0, LX/1zS;->c:I

    .line 351911
    const/16 v1, 0x1

    if-eqz v1, :cond_0

    .line 351912
    invoke-static {p0, p1, v0}, LX/1zC;->a(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    move-result v0

    .line 351913
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1, v0}, LX/1zC;->b(LX/1zC;Landroid/text/Editable;LX/1zS;)Z

    move-result v0

    goto :goto_0
.end method
