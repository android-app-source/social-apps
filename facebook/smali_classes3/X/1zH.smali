.class public final LX/1zH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 352693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static d()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/emoji/model/Emoji;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v11, 0x30

    const/16 v10, 0x23

    const v9, 0x1f1f7

    const/16 v8, 0x20e3

    const/4 v7, 0x0

    .line 352692
    const/16 v0, 0x1db

    new-array v0, v0, [Lcom/facebook/ui/emoji/model/Emoji;

    new-instance v1, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f020566

    const v3, 0x1f604

    const v4, 0x7f08251f

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v1, v0, v7

    const/4 v1, 0x1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020565

    const v4, 0x1f603

    const v5, 0x7f082520

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c2

    const/16 v4, 0x263a

    const v5, 0x7f082521

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020568

    const v4, 0x1f609

    const v5, 0x7f082522

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056c

    const v4, 0x1f60d

    const v5, 0x7f082523

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020573

    const v4, 0x1f618

    const v5, 0x7f082524

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020574

    const v4, 0x1f61a

    const v5, 0x7f082525

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020575

    const v4, 0x1f61c

    const v5, 0x7f082526

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020576

    const v4, 0x1f61d

    const v5, 0x7f082527

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020586

    const v4, 0x1f633

    const v5, 0x7f082528

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020563

    const v4, 0x1f601

    const v5, 0x7f082529

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020570

    const v4, 0x1f614

    const v5, 0x7f08252a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056b

    const v4, 0x1f60c

    const v5, 0x7f08252b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056e

    const v4, 0x1f612

    const v5, 0x7f08252c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020577

    const v4, 0x1f61e

    const v5, 0x7f08252d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057b

    const v4, 0x1f623

    const v5, 0x7f08252e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057a

    const v4, 0x1f622

    const v5, 0x7f08252f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020564

    const v4, 0x1f602

    const v5, 0x7f082530

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020582

    const v4, 0x1f62d

    const v5, 0x7f082531

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020580

    const v4, 0x1f62a

    const v5, 0x7f082532

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057d

    const v4, 0x1f625

    const v5, 0x7f082533

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020583

    const v4, 0x1f630

    const v5, 0x7f082534

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056f

    const v4, 0x1f613

    const v5, 0x7f082535

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057f

    const v4, 0x1f629

    const v5, 0x7f082536

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020581

    const v4, 0x1f62b

    const v5, 0x7f082537

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057e

    const v4, 0x1f628

    const v5, 0x7f082538

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020584

    const v4, 0x1f631

    const v5, 0x7f082539

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020578

    const v4, 0x1f620

    const v5, 0x7f08253a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020579

    const v4, 0x1f621

    const v5, 0x7f08253b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02057c

    const v4, 0x1f624

    const v5, 0x7f08253c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020572

    const v4, 0x1f616

    const v5, 0x7f08253d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020567

    const v4, 0x1f606

    const v5, 0x7f08253e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020569

    const v4, 0x1f60a

    const v5, 0x7f08253f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056a

    const v4, 0x1f60b

    const v5, 0x7f082540

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020588

    const v4, 0x1f637

    const v5, 0x7f082541

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f020587

    const v3, 0x1f635

    const v4, 0x7f082542

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v1, v0, v10

    const/16 v1, 0x24

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020585

    const v4, 0x1f632

    const v5, 0x7f082543

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020500

    const v4, 0x1f47f

    const v5, 0x7f082544

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02056d

    const v4, 0x1f60f

    const v5, 0x7f082545

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020571

    const v4, 0x1f615

    const v5, 0x7f082546

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f5

    const v4, 0x1f472

    const v5, 0x7f082547

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f6

    const v4, 0x1f473

    const v5, 0x7f082548

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f2

    const v4, 0x1f46e

    const v5, 0x7f082549

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204fa

    const v4, 0x1f477

    const v5, 0x7f08254a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020502

    const v4, 0x1f482

    const v5, 0x7f08254b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f9

    const v4, 0x1f476

    const v5, 0x7f08254c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ed

    const v4, 0x1f466

    const v5, 0x7f08254d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ee

    const v4, 0x1f467

    const v5, 0x7f08254e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/facebook/ui/emoji/model/Emoji;

    const v2, 0x7f0204ef

    const v3, 0x1f468

    const v4, 0x7f08254f

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v1, v0, v11

    const/16 v1, 0x31

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f0

    const v4, 0x1f469

    const v5, 0x7f082550

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f7

    const v4, 0x1f474

    const v5, 0x7f082551

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f8

    const v4, 0x1f475

    const v5, 0x7f082552

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f4

    const v4, 0x1f471

    const v5, 0x7f082553

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204fd

    const v4, 0x1f47c

    const v5, 0x7f082554

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204fb

    const v4, 0x1f478

    const v5, 0x7f082555

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x37

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058b

    const v4, 0x1f63a

    const v5, 0x7f082556

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x38

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020589

    const v4, 0x1f638

    const v5, 0x7f082557

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x39

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058c

    const v4, 0x1f63b

    const v5, 0x7f082558

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058e

    const v4, 0x1f63d

    const v5, 0x7f082559

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058d

    const v4, 0x1f63c

    const v5, 0x7f08255a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020590

    const v4, 0x1f640

    const v5, 0x7f08255b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058f

    const v4, 0x1f63f

    const v5, 0x7f08255c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02058a

    const v4, 0x1f639

    const v5, 0x7f08255d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020501

    const v4, 0x1f480

    const v5, 0x7f08255e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x40

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204fe

    const v4, 0x1f47d

    const v5, 0x7f08255f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x41

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020525

    const v4, 0x1f4a9

    const v5, 0x7f082560

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x42

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020553

    const v4, 0x1f525

    const v5, 0x7f082561

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x43

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205de

    const/16 v4, 0x2728

    const v5, 0x7f082562

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x44

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044c

    const v4, 0x1f31f

    const v5, 0x7f082563

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x45

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051f

    const v4, 0x1f4a2

    const v5, 0x7f082564

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x46

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020522

    const v4, 0x1f4a6

    const v5, 0x7f082565

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x47

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020523

    const v4, 0x1f4a7

    const v5, 0x7f082566

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x48

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020521

    const v4, 0x1f4a4

    const v5, 0x7f082567

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x49

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020524

    const v4, 0x1f4a8

    const v5, 0x7f082568

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d2

    const v4, 0x1f442

    const v5, 0x7f082569

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d1

    const v4, 0x1f440

    const v5, 0x7f08256a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d3

    const v4, 0x1f443

    const v5, 0x7f08256b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d5

    const v4, 0x1f445

    const v5, 0x7f08256c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d4

    const v4, 0x1f444

    const v5, 0x7f08256d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204dd

    const v4, 0x1f44d

    const v5, 0x7f08256e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x50

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204de

    const v4, 0x1f44e

    const v5, 0x7f08256f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x51

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204dc

    const v4, 0x1f44c

    const v5, 0x7f082570

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x52

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204da

    const v4, 0x1f44a

    const v5, 0x7f082571

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x53

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205da

    const/16 v4, 0x270a

    const v5, 0x7f082572

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x54

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205dc

    const/16 v4, 0x270c

    const v5, 0x7f082573

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x55

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204db

    const v4, 0x1f44b

    const v5, 0x7f082574

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x56

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205db

    const/16 v4, 0x270b

    const v5, 0x7f082575

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x57

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e0

    const v4, 0x1f450

    const v5, 0x7f082576

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x58

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d6

    const v4, 0x1f446

    const v5, 0x7f082577

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x59

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d7

    const v4, 0x1f447

    const v5, 0x7f082578

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d9

    const v4, 0x1f449

    const v5, 0x7f082579

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d8

    const v4, 0x1f448

    const v5, 0x7f08257a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020592

    const v4, 0x1f64c

    const v5, 0x7f08257b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020594

    const v4, 0x1f64f

    const v5, 0x7f08257c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c1

    const/16 v4, 0x261d

    const v5, 0x7f08257d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204df

    const v4, 0x1f44f

    const v5, 0x7f08257e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x60

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020526

    const v4, 0x1f4aa

    const v5, 0x7f08257f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x61

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020503

    const v4, 0x1f483

    const v5, 0x7f082580

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x62

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f1

    const v4, 0x1f46b

    const v5, 0x7f082581

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x63

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050d

    const v4, 0x1f48f

    const v5, 0x7f082582

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x64

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050f

    const v4, 0x1f491

    const v5, 0x7f082583

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x65

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204f3

    const v4, 0x1f46f

    const v5, 0x7f082584

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x66

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020591

    const v4, 0x1f64b

    const v5, 0x7f082585

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x67

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020505

    const v4, 0x1f485

    const v5, 0x7f082586

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x68

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020593

    const v4, 0x1f64d

    const v5, 0x7f082587

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x69

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048d

    const v4, 0x1f3a9

    const v5, 0x7f082588

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e1

    const v4, 0x1f451

    const v5, 0x7f082589

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e2

    const v4, 0x1f452

    const v5, 0x7f08258a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e9

    const v4, 0x1f45f

    const v5, 0x7f08258b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204eb

    const v4, 0x1f461

    const v5, 0x7f08258c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ea

    const v4, 0x1f460

    const v5, 0x7f08258d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ec

    const v4, 0x1f462

    const v5, 0x7f08258e    # 1.8097E38f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x70

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e4

    const v4, 0x1f455

    const v5, 0x7f08258f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x71

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e3

    const v4, 0x1f454

    const v5, 0x7f082590

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x72

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e5

    const v4, 0x1f457

    const v5, 0x7f082591

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x73

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e6

    const v4, 0x1f458

    const v5, 0x7f082592

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x74

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e7

    const v4, 0x1f459

    const v5, 0x7f082593

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x75

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052d

    const v4, 0x1f4bc

    const v5, 0x7f082594

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x76

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204e8

    const v4, 0x1f45c

    const v5, 0x7f082595

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x77

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020478

    const v4, 0x1f380

    const v5, 0x7f082596

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x78

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020443

    const v4, 0x1f302

    const v5, 0x7f082597

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x79

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020504

    const v4, 0x1f484

    const v5, 0x7f082598

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020518

    const v4, 0x1f49b

    const v5, 0x7f082599

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020516

    const v4, 0x1f499

    const v5, 0x7f08259a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020519

    const v4, 0x1f49c

    const v5, 0x7f08259b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020517

    const v4, 0x1f49a

    const v5, 0x7f08259c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e7

    const/16 v4, 0x2764

    const v5, 0x7f08259d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020512

    const v4, 0x1f494

    const v5, 0x7f08259e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x80

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020514

    const v4, 0x1f497

    const v5, 0x7f08259f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x81

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020511

    const v4, 0x1f493

    const v5, 0x7f0825a0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x82

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020513

    const v4, 0x1f496

    const v5, 0x7f0825a1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x83

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051b

    const v4, 0x1f49e

    const v5, 0x7f0825a2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x84

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020515

    const v4, 0x1f498

    const v5, 0x7f0825a3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x85

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050a

    const v4, 0x1f48c

    const v5, 0x7f0825a4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x86

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020509

    const v4, 0x1f48b

    const v5, 0x7f0825a5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x87

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050b

    const v4, 0x1f48d

    const v5, 0x7f0825a6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x88

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050c

    const v4, 0x1f48e

    const v5, 0x7f0825a7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x89

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ca

    const v4, 0x1f436

    const v5, 0x7f0825a8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ce

    const v4, 0x1f43a

    const v5, 0x7f0825a9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c6

    const v4, 0x1f431

    const v5, 0x7f0825aa

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c2

    const v4, 0x1f42d

    const v5, 0x7f0825ab

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204cd

    const v4, 0x1f439

    const v5, 0x7f0825ac

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c5

    const v4, 0x1f430

    const v5, 0x7f0825ad

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204cc

    const v4, 0x1f438

    const v5, 0x7f0825ae

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x90

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c4

    const v4, 0x1f42f

    const v5, 0x7f0825af

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x91

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204be

    const v4, 0x1f428

    const v5, 0x7f0825b0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x92

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204cf

    const v4, 0x1f43b

    const v5, 0x7f0825b1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x93

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204cb

    const v4, 0x1f437

    const v5, 0x7f0825b2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x94

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c3

    const v4, 0x1f42e

    const v5, 0x7f0825b3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x95

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b3

    const v4, 0x1f417

    const v5, 0x7f0825b4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x96

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c9

    const v4, 0x1f435

    const v5, 0x7f0825b5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x97

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b1

    const v4, 0x1f412

    const v5, 0x7f0825b6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x98

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c8

    const v4, 0x1f434

    const v5, 0x7f0825b7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x99

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b0

    const v4, 0x1f411

    const v5, 0x7f0825b8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b4

    const v4, 0x1f418

    const v5, 0x7f0825b9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204bd

    const v4, 0x1f427

    const v5, 0x7f0825ba

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204bc

    const v4, 0x1f426

    const v5, 0x7f0825bb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204bb

    const v4, 0x1f425

    const v5, 0x7f0825bc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b2

    const v4, 0x1f414

    const v5, 0x7f0825bd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ae

    const v4, 0x1f40d

    const v5, 0x7f0825be

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b7

    const v4, 0x1f41b

    const v5, 0x7f0825bf

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b5

    const v4, 0x1f419

    const v5, 0x7f0825c0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b6

    const v4, 0x1f41a

    const v5, 0x7f0825c1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b9

    const v4, 0x1f420

    const v5, 0x7f0825c2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204b8

    const v4, 0x1f41f

    const v5, 0x7f0825c3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c1

    const v4, 0x1f42c

    const v5, 0x7f0825c4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c7

    const v4, 0x1f433

    const v5, 0x7f0825c5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204af

    const v4, 0x1f40e

    const v5, 0x7f0825c6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ba

    const v4, 0x1f421

    const v5, 0x7f0825c7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204c0

    const v4, 0x1f42b

    const v5, 0x7f0825c8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204bf

    const v4, 0x1f429

    const v5, 0x7f0825c9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xab

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204d0

    const v4, 0x1f43e

    const v5, 0x7f0825ca

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xac

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02050e

    const v4, 0x1f490

    const v5, 0x7f0825cb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xad

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020451

    const v4, 0x1f338

    const v5, 0x7f0825cc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xae

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020450

    const v4, 0x1f337

    const v5, 0x7f0825cd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020456

    const v4, 0x1f340

    const v5, 0x7f0825ce

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020452

    const v4, 0x1f339

    const v5, 0x7f0825cf

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020454

    const v4, 0x1f33b

    const v5, 0x7f0825d0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020453

    const v4, 0x1f33a

    const v5, 0x7f0825d1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020457

    const v4, 0x1f341

    const v5, 0x7f0825d2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020459

    const v4, 0x1f343

    const v5, 0x7f0825d3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020458

    const v4, 0x1f342

    const v5, 0x7f0825d4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020455

    const v4, 0x1f33e

    const v5, 0x7f0825d5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044f

    const v4, 0x1f335

    const v5, 0x7f0825d6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044e

    const v4, 0x1f334

    const v5, 0x7f0825d7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044d

    const v4, 0x1f331

    const v5, 0x7f0825d8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xba

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044b

    const v4, 0x1f319

    const v5, 0x7f0825d9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f1

    const/16 v4, 0x2b50

    const v5, 0x7f0825da

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205bc

    const/16 v4, 0x2600

    const v5, 0x7f0825db

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205bd

    const/16 v4, 0x2601

    const v5, 0x7f0825dc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ca

    const/16 v4, 0x26a1

    const v5, 0x7f0825dd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205bf

    const/16 v4, 0x2614

    const v5, 0x7f0825de

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205cf

    const/16 v4, 0x26c4

    const v5, 0x7f0825df

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020442

    const v4, 0x1f300

    const v5, 0x7f0825e0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020449

    const v4, 0x1f308

    const v5, 0x7f0825e1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02044a

    const v4, 0x1f30a

    const v5, 0x7f0825e2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020480

    const v4, 0x1f38d

    const v5, 0x7f0825e3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051a

    const v4, 0x1f49d

    const v5, 0x7f0825e4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020481

    const v4, 0x1f38e

    const v5, 0x7f0825e5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020484

    const v4, 0x1f392

    const v5, 0x7f0825e6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020485

    const v4, 0x1f393

    const v5, 0x7f0825e7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020482

    const v4, 0x1f38f

    const v5, 0x7f0825e8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xca

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020483

    const v4, 0x1f390

    const v5, 0x7f0825e9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047a

    const v4, 0x1f383

    const v5, 0x7f0825ea

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204fc

    const v4, 0x1f47b

    const v5, 0x7f0825eb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047c

    const v4, 0x1f385

    const v5, 0x7f0825ec

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xce

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047b

    const v4, 0x1f384

    const v5, 0x7f0825ed

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020479

    const v4, 0x1f381

    const v5, 0x7f0825ee

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047e

    const v4, 0x1f389

    const v5, 0x7f0825ef

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047d

    const v4, 0x1f388

    const v5, 0x7f0825f0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02047f

    const v4, 0x1f38c

    const v5, 0x7f0825f1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020489

    const v4, 0x1f3a5

    const v5, 0x7f0825f2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020545

    const v4, 0x1f4f7

    const v5, 0x7f0825f3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020548

    const v4, 0x1f4fc

    const v5, 0x7f0825f4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020530

    const v4, 0x1f4bf

    const v5, 0x7f0825f5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020531

    const v4, 0x1f4c0

    const v5, 0x7f0825f6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052e

    const v4, 0x1f4bd

    const v5, 0x7f0825f7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052f

    const v4, 0x1f4be

    const v5, 0x7f0825f8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xda

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052c

    const v4, 0x1f4bb

    const v5, 0x7f0825f9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020540

    const v4, 0x1f4f1

    const v5, 0x7f0825fa

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205be

    const/16 v4, 0x260e

    const v5, 0x7f0825fb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020534

    const v4, 0x1f4de

    const v5, 0x7f0825fc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xde

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020535

    const v4, 0x1f4e0

    const v5, 0x7f0825fd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020536

    const v4, 0x1f4e1

    const v5, 0x7f0825fe

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020546

    const v4, 0x1f4fa

    const v5, 0x7f0825ff

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020547

    const v4, 0x1f4fb

    const v5, 0x7f082600

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020549

    const v4, 0x1f508

    const v5, 0x7f082601

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020551

    const v4, 0x1f514

    const v5, 0x7f082602

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020537

    const v4, 0x1f4e2

    const v5, 0x7f082603

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020538

    const v4, 0x1f4e3

    const v5, 0x7f082604

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020550

    const v4, 0x1f513

    const v5, 0x7f082605

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054f

    const v4, 0x1f512

    const v5, 0x7f082606

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054c

    const v4, 0x1f50f

    const v5, 0x7f082607

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054d

    const v4, 0x1f510

    const v5, 0x7f082608

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xea

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054e

    const v4, 0x1f511

    const v5, 0x7f082609

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054b

    const v4, 0x1f50e

    const v5, 0x7f08260a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xec

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051e

    const v4, 0x1f4a1

    const v5, 0x7f08260b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xed

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02054a

    const v4, 0x1f50d

    const v5, 0x7f08260c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xee

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b0

    const v4, 0x1f6c0

    const v5, 0x7f08260d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xef

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ae

    const v4, 0x1f6bd

    const v5, 0x7f08260e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020554

    const v4, 0x1f528

    const v5, 0x7f08260f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a7

    const v4, 0x1f6ac

    const v5, 0x7f082610

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020520

    const v4, 0x1f4a3

    const v5, 0x7f082611

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020555

    const v4, 0x1f52b

    const v5, 0x7f082612

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020508

    const v4, 0x1f48a

    const v5, 0x7f082613    # 1.809727E38f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020507

    const v4, 0x1f489

    const v5, 0x7f082614

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020527

    const v4, 0x1f4b0

    const v5, 0x7f082615

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020529

    const v4, 0x1f4b4

    const v5, 0x7f082616

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052a

    const v4, 0x1f4b5

    const v5, 0x7f082617

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020541

    const v4, 0x1f4f2

    const v5, 0x7f082618

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d9

    const/16 v4, 0x2709

    const v5, 0x7f082619

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053a

    const v4, 0x1f4e9

    const v5, 0x7f08261a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020539

    const v4, 0x1f4e8

    const v5, 0x7f08261b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053c

    const v4, 0x1f4eb

    const v5, 0x7f08261c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053b

    const v4, 0x1f4ea

    const v5, 0x7f08261d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0xff

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053d

    const v4, 0x1f4ec

    const v5, 0x7f08261e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x100

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053e

    const v4, 0x1f4ed

    const v5, 0x7f08261f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x101

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02053f

    const v4, 0x1f4ee

    const v5, 0x7f082620

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x102

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020533

    const v4, 0x1f4dd

    const v5, 0x7f082621

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x103

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d7

    const/16 v4, 0x2702

    const v5, 0x7f082622

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x104

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020532

    const v4, 0x1f4d6

    const v5, 0x7f082623

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x105

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048c

    const v4, 0x1f3a8

    const v5, 0x7f082624

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x106

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048f

    const v4, 0x1f3ac

    const v5, 0x7f082625

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x107

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020488

    const v4, 0x1f3a4

    const v5, 0x7f082626

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x108

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048b

    const v4, 0x1f3a7

    const v5, 0x7f082627

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x109

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020498

    const v4, 0x1f3bc

    const v5, 0x7f082628

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020493

    const v4, 0x1f3b5

    const v5, 0x7f082629

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020494

    const v4, 0x1f3b6

    const v5, 0x7f08262a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020497

    const v4, 0x1f3ba

    const v5, 0x7f08262b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020495

    const v4, 0x1f3b7

    const v5, 0x7f08262c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020496

    const v4, 0x1f3b8

    const v5, 0x7f08262d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ff

    const v4, 0x1f47e

    const v5, 0x7f08262e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x110

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020428

    const v4, 0x1f004

    const v5, 0x7f08262f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x111

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020491

    const v4, 0x1f3af

    const v5, 0x7f082630

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x112

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049e

    const v4, 0x1f3c8

    const v5, 0x7f082631

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x113

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049b

    const v4, 0x1f3c0

    const v5, 0x7f082632

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x114

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205cd

    const/16 v4, 0x26bd

    const v5, 0x7f082633

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x115

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ce

    const/16 v4, 0x26be

    const v5, 0x7f082634

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x116

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020499

    const v4, 0x1f3be

    const v5, 0x7f082635

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x117

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020492

    const v4, 0x1f3b1

    const v5, 0x7f082636

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x118

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d3

    const/16 v4, 0x26f3

    const v5, 0x7f082637

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x119

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049c

    const v4, 0x1f3c1

    const v5, 0x7f082638

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049d

    const v4, 0x1f3c6

    const v5, 0x7f082639

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049a

    const v4, 0x1f3bf

    const v5, 0x7f08263a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c0

    const/16 v4, 0x2615

    const v5, 0x7f08263b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020473

    const v4, 0x1f375

    const v5, 0x7f08263c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020474

    const v4, 0x1f376

    const v5, 0x7f08263d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020476

    const v4, 0x1f37a

    const v5, 0x7f08263e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x120

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020477

    const v4, 0x1f37b

    const v5, 0x7f08263f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x121

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020475

    const v4, 0x1f378

    const v5, 0x7f082640

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x122

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020472

    const v4, 0x1f374

    const v5, 0x7f082641

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x123

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020460

    const v4, 0x1f354

    const v5, 0x7f082642

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x124

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020468

    const v4, 0x1f35f

    const v5, 0x7f082643

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x125

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020466

    const v4, 0x1f35d

    const v5, 0x7f082644

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x126

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020464

    const v4, 0x1f35b

    const v5, 0x7f082645

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x127

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046f

    const v4, 0x1f371

    const v5, 0x7f082646

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x128

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046b

    const v4, 0x1f363

    const v5, 0x7f082647

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x129

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020462

    const v4, 0x1f359

    const v5, 0x7f082648

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020461

    const v4, 0x1f358

    const v5, 0x7f082649

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020463

    const v4, 0x1f35a

    const v5, 0x7f08264a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020465

    const v4, 0x1f35c

    const v5, 0x7f08264b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020470

    const v4, 0x1f372

    const v5, 0x7f08264c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046a

    const v4, 0x1f362

    const v5, 0x7f08264d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020469

    const v4, 0x1f361

    const v5, 0x7f08264e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x130

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020471

    const v4, 0x1f373

    const v5, 0x7f08264f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x131

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020467

    const v4, 0x1f35e

    const v5, 0x7f082650

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x132

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046c

    const v4, 0x1f366

    const v5, 0x7f082651

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x133

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046d

    const v4, 0x1f367

    const v5, 0x7f082652

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x134

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02046e

    const v4, 0x1f370

    const v5, 0x7f082653

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x135

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045e

    const v4, 0x1f34e

    const v5, 0x7f082654

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x136

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045d

    const v4, 0x1f34a

    const v5, 0x7f082655

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x137

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045c

    const v4, 0x1f349

    const v5, 0x7f082656

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x138

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045f

    const v4, 0x1f353

    const v5, 0x7f082657

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x139

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045b

    const v4, 0x1f346

    const v5, 0x7f082658

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02045a

    const v4, 0x1f345

    const v5, 0x7f082659

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02049f

    const v4, 0x1f3e0

    const v5, 0x7f08265a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a0

    const v4, 0x1f3e1

    const v5, 0x7f08265b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a9

    const v4, 0x1f3eb

    const v5, 0x7f08265c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a1

    const v4, 0x1f3e2

    const v5, 0x7f08265d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a2

    const v4, 0x1f3e3

    const v5, 0x7f08265e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x140

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a3

    const v4, 0x1f3e5

    const v5, 0x7f08265f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x141

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a4

    const v4, 0x1f3e6

    const v5, 0x7f082660

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x142

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a8

    const v4, 0x1f3ea

    const v5, 0x7f082661

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x143

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a7

    const v4, 0x1f3e9

    const v5, 0x7f082662

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x144

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a6

    const v4, 0x1f3e8

    const v5, 0x7f082663

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x145

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020510

    const v4, 0x1f492

    const v5, 0x7f082664

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x146

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d1

    const/16 v4, 0x26ea

    const v5, 0x7f082665

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x147

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204aa

    const v4, 0x1f3ec

    const v5, 0x7f082666

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x148

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020448

    const v4, 0x1f307

    const v5, 0x7f082667

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x149

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020447

    const v4, 0x1f306

    const v5, 0x7f082668

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ac

    const v4, 0x1f3ef

    const v5, 0x7f082669

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ad

    const v4, 0x1f3f0

    const v5, 0x7f08266a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d5

    const/16 v4, 0x26fa

    const v5, 0x7f08266b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204ab

    const v4, 0x1f3ed

    const v5, 0x7f08266c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020561

    const v4, 0x1f5fc

    const v5, 0x7f08266d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020560

    const v4, 0x1f5fb

    const v5, 0x7f08266e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x150

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020445

    const v4, 0x1f304

    const v5, 0x7f08266f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x151

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020446

    const v4, 0x1f305

    const v5, 0x7f082670

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x152

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020444

    const v4, 0x1f303

    const v5, 0x7f082671

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x153

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020562

    const v4, 0x1f5fd

    const v5, 0x7f082672

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x154

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020486

    const v4, 0x1f3a1

    const v5, 0x7f082673

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x155

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d2

    const/16 v4, 0x26f2

    const v5, 0x7f082674

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x156

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020487

    const v4, 0x1f3a2

    const v5, 0x7f082675

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x157

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a3

    const v4, 0x1f6a2

    const v5, 0x7f082676

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x158

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d4

    const/16 v4, 0x26f5

    const v5, 0x7f082677

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x159

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a4

    const v4, 0x1f6a4

    const v5, 0x7f082678

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020595

    const v4, 0x1f680

    const v5, 0x7f082679

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d8

    const/16 v4, 0x2708

    const v5, 0x7f08267a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02052b

    const v4, 0x1f4ba

    const v5, 0x7f08267b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020599

    const v4, 0x1f689

    const v5, 0x7f08267c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020597

    const v4, 0x1f684

    const v5, 0x7f08267d    # 1.8097485E38f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020598

    const v4, 0x1f685

    const v5, 0x7f08267e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x160

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020596

    const v4, 0x1f683

    const v5, 0x7f08267f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x161

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059a

    const v4, 0x1f68c

    const v5, 0x7f082680

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x162

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a1

    const v4, 0x1f699

    const v5, 0x7f082681

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x163

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a0

    const v4, 0x1f697

    const v5, 0x7f082682

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x164

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059f

    const v4, 0x1f695

    const v5, 0x7f082683

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x165

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a2

    const v4, 0x1f69a

    const v5, 0x7f082684

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x166

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059e

    const v4, 0x1f693

    const v5, 0x7f082685

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x167

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059d

    const v4, 0x1f692

    const v5, 0x7f082686

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x168

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059c

    const v4, 0x1f691

    const v5, 0x7f082687

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x169

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a9

    const v4, 0x1f6b2

    const v5, 0x7f082688

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020506

    const v4, 0x1f488

    const v5, 0x7f082689

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02059b

    const v4, 0x1f68f

    const v5, 0x7f08268a

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048e

    const v4, 0x1f3ab

    const v5, 0x7f08268b

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a5

    const v4, 0x1f6a5

    const v5, 0x7f08268c

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c9

    const/16 v4, 0x26a0

    const v5, 0x7f08268d

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a6

    const v4, 0x1f6a7

    const v5, 0x7f08268e

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x170

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020556

    const v4, 0x1f530

    const v5, 0x7f08268f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x171

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d6

    const/16 v4, 0x26fd

    const v5, 0x7f082690

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x172

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c7

    const/16 v4, 0x2668

    const v5, 0x7f082691

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x173

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020490

    const v4, 0x1f3ad

    const v5, 0x7f082692

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x174

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020432

    const v4, 0x1f1ef

    const v5, 0x1f1f5

    const v6, 0x7f082693

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x175

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020433

    const v4, 0x1f1f0

    const v5, 0x7f082694

    invoke-direct {v2, v3, v4, v9, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x176

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042d

    const v4, 0x1f1e9

    const v5, 0x1f1ea

    const v6, 0x7f082695

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x177

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042c

    const v4, 0x1f1e8

    const v5, 0x1f1f3

    const v6, 0x7f082696

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x178

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020435

    const v4, 0x1f1fa

    const v5, 0x1f1f8

    const v6, 0x7f082697

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x179

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042f

    const v4, 0x1f1eb

    const v5, 0x7f082698

    invoke-direct {v2, v3, v4, v9, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042e

    const v4, 0x1f1ea

    const v5, 0x1f1f8

    const v6, 0x7f082699

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020431

    const v4, 0x1f1ee

    const v5, 0x1f1f9

    const v6, 0x7f08269a

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020434

    const v4, 0x1f1fa

    const v5, 0x7f08269b

    invoke-direct {v2, v3, v9, v4, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020430

    const v4, 0x1f1ec

    const v5, 0x1f1e7

    const v6, 0x7f08269c

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f5

    const/16 v4, 0x31

    const v5, 0x7f08269d

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f8

    const/16 v4, 0x32

    const v5, 0x7f08269e

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x180

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f9

    const/16 v4, 0x33

    const v5, 0x7f08269f

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x181

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205fa

    const/16 v4, 0x34

    const v5, 0x7f0826a0

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x182

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205fb

    const/16 v4, 0x35

    const v5, 0x7f0826a1

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x183

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205fc

    const/16 v4, 0x36

    const v5, 0x7f0826a2

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x184

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205fd

    const/16 v4, 0x37

    const v5, 0x7f0826a3

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x185

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205fe

    const/16 v4, 0x38

    const v5, 0x7f0826a4

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x186

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ff

    const/16 v4, 0x39

    const v5, 0x7f0826a5

    invoke-direct {v2, v3, v4, v8, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x187

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f4

    const v4, 0x7f0826a6

    invoke-direct {v2, v3, v11, v8, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x188

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b5

    const v4, 0x7f0826a7

    invoke-direct {v2, v3, v10, v8, v4}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x189

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ed

    const/16 v4, 0x2b06

    const v5, 0x7f0826a8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ee

    const/16 v4, 0x2b07

    const v5, 0x7f0826a9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ec

    const/16 v4, 0x2b05

    const v5, 0x7f0826aa

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e8

    const/16 v4, 0x27a1

    const v5, 0x7f0826ab

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b2

    const/16 v4, 0x2197

    const v5, 0x7f0826ac

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b1

    const/16 v4, 0x2196

    const v5, 0x7f0826ad

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b3

    const/16 v4, 0x2198

    const v5, 0x7f0826ae

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x190

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b4

    const/16 v4, 0x2199

    const v5, 0x7f0826af

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x191

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205eb

    const/16 v4, 0x2935

    const v5, 0x7f0826b0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x192

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ea

    const/16 v4, 0x2934

    const v5, 0x7f0826b1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x193

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042a

    const v4, 0x1f199

    const v5, 0x7f0826b2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x194

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020429

    const v4, 0x1f192

    const v5, 0x7f0826b3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x195

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020544

    const v4, 0x1f4f6

    const v5, 0x7f0826b4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x196

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02048a

    const v4, 0x1f3a6

    const v5, 0x7f0826b5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x197

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020436

    const v4, 0x1f201

    const v5, 0x7f0826b6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x198

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020439

    const v4, 0x1f22f

    const v5, 0x7f0826b7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x199

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043a

    const v4, 0x1f233

    const v5, 0x7f0826b8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043b

    const v4, 0x1f235

    const v5, 0x7f0826b9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020441

    const v4, 0x1f250

    const v5, 0x7f0826ba

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043f

    const v4, 0x1f239

    const v5, 0x7f0826bb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020440

    const v4, 0x1f23a

    const v5, 0x7f0826bc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043c

    const v4, 0x1f236

    const v5, 0x7f0826bd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020438

    const v4, 0x1f21a

    const v5, 0x7f0826be

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ac

    const v4, 0x1f6bb

    const v5, 0x7f0826bf

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205aa

    const v4, 0x1f6b9

    const v5, 0x7f0826c0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ab

    const v4, 0x1f6ba

    const v5, 0x7f0826c1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ad

    const v4, 0x1f6bc

    const v5, 0x7f0826c2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205af

    const v4, 0x1f6be

    const v5, 0x7f0826c3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c8

    const/16 v4, 0x267f

    const v5, 0x7f0826c4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205a8

    const v4, 0x1f6ad

    const v5, 0x7f0826c5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043d

    const v4, 0x1f237

    const v5, 0x7f0826c6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02043e

    const v4, 0x1f238

    const v5, 0x7f0826c7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020437

    const v4, 0x1f202

    const v5, 0x7f0826c8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1aa

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f7

    const/16 v4, 0x3299

    const v5, 0x7f0826c9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ab

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f6

    const/16 v4, 0x3297

    const v5, 0x7f0826ca

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ac

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020552

    const v4, 0x1f51e

    const v5, 0x7f0826cb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ad

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205d0

    const/16 v4, 0x26d4

    const v5, 0x7f0826cc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ae

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205df

    const/16 v4, 0x2733

    const v5, 0x7f0826cd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1af

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e2

    const/16 v4, 0x274e

    const v5, 0x7f0826ce

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e0

    const/16 v4, 0x2734

    const v5, 0x7f0826cf

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051c

    const v4, 0x1f49f

    const v5, 0x7f0826d0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02042b

    const v4, 0x1f19a

    const v5, 0x7f0826d1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020542

    const v4, 0x1f4f3

    const v5, 0x7f0826d2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020543

    const v4, 0x1f4f4

    const v5, 0x7f0826d3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02051d

    const v4, 0x1f4a0

    const v5, 0x7f0826d4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e9

    const/16 v4, 0x27bf

    const v5, 0x7f0826d5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0204a5

    const v4, 0x1f3e7

    const v5, 0x7f0826d6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020528

    const v4, 0x1f4b2

    const v5, 0x7f0826d7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f3

    const/16 v4, 0x303d

    const v5, 0x7f0826d8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ba

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e1

    const/16 v4, 0x274c

    const v5, 0x7f0826d9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1bb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f2

    const/16 v4, 0x2b55

    const v5, 0x7f0826da

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1bc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e6

    const/16 v4, 0x2757

    const v5, 0x7f0826db

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1bd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e3

    const/16 v4, 0x2753

    const v5, 0x7f0826dc

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1be

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e5

    const/16 v4, 0x2755

    const v5, 0x7f0826dd

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1bf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205e4

    const/16 v4, 0x2754

    const v5, 0x7f0826de

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205dd

    const/16 v4, 0x2716

    const v5, 0x7f0826df

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c3

    const/16 v4, 0x2660

    const v5, 0x7f0826e0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c5

    const/16 v4, 0x2665

    const v5, 0x7f0826e1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c4

    const/16 v4, 0x2663

    const v5, 0x7f0826e2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205c6

    const/16 v4, 0x2666

    const v5, 0x7f0826e3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020557

    const v4, 0x1f531

    const v5, 0x7f0826e4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020558

    const v4, 0x1f532

    const v5, 0x7f0826e5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f020559

    const v4, 0x1f533

    const v5, 0x7f0826e6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b9

    const/16 v4, 0x25fc

    const v5, 0x7f0826e7    # 1.80977E38f

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b8

    const/16 v4, 0x25fb

    const v5, 0x7f0826e8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ca

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205bb

    const/16 v4, 0x25fe

    const v5, 0x7f0826e9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1cb

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ba

    const/16 v4, 0x25fd

    const v5, 0x7f0826ea

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1cc

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b6

    const/16 v4, 0x25aa

    const v5, 0x7f0826eb

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1cd

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205b7

    const/16 v4, 0x25ab

    const v5, 0x7f0826ec

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1ce

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205f0

    const/16 v4, 0x2b1c

    const v5, 0x7f0826ed

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1cf

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205ef

    const/16 v4, 0x2b1b

    const v5, 0x7f0826ee

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d0

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205cc

    const/16 v4, 0x26ab

    const v5, 0x7f0826ef

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d1

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f0205cb

    const/16 v4, 0x26aa

    const v5, 0x7f0826f0

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d2

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055a

    const v4, 0x1f534

    const v5, 0x7f0826f1

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d3

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055b

    const v4, 0x1f535

    const v5, 0x7f0826f2

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d4

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055c

    const v4, 0x1f536

    const v5, 0x7f0826f3

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d5

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055d

    const v4, 0x1f537

    const v5, 0x7f0826f4

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d6

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055e

    const v4, 0x1f538

    const v5, 0x7f0826f5

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d7

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02055f

    const v4, 0x1f539

    const v5, 0x7f0826f6

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d8

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02060d

    const/high16 v4, 0xf0000

    const v5, 0x7f0826f7

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d9

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02060b

    const v4, 0xf0001

    const v5, 0x7f0826f8

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    const/16 v1, 0x1da

    new-instance v2, Lcom/facebook/ui/emoji/model/Emoji;

    const v3, 0x7f02060c

    const v4, 0xf0002

    const v5, 0x7f0826f9

    invoke-direct {v2, v3, v4, v7, v5}, Lcom/facebook/ui/emoji/model/Emoji;-><init>(IIII)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)I
    .locals 0

    .prologue
    .line 352694
    sparse-switch p1, :sswitch_data_0

    .line 352695
    :goto_0
    return p2

    .line 352696
    :sswitch_0
    const/16 p2, 0x2600

    goto :goto_0

    .line 352697
    :sswitch_1
    const/16 p2, 0x2601

    goto :goto_0

    .line 352698
    :sswitch_2
    const/16 p2, 0x2614

    goto :goto_0

    .line 352699
    :sswitch_3
    const/16 p2, 0x26c4

    goto :goto_0

    .line 352700
    :sswitch_4
    const/16 p2, 0x26a1

    goto :goto_0

    .line 352701
    :sswitch_5
    const p2, 0x1f300

    goto :goto_0

    .line 352702
    :sswitch_6
    const p2, 0x1f302

    goto :goto_0

    .line 352703
    :sswitch_7
    const p2, 0x1f303

    goto :goto_0

    .line 352704
    :sswitch_8
    const p2, 0x1f304

    goto :goto_0

    .line 352705
    :sswitch_9
    const p2, 0x1f305

    goto :goto_0

    .line 352706
    :sswitch_a
    const p2, 0x1f306

    goto :goto_0

    .line 352707
    :sswitch_b
    const p2, 0x1f307

    goto :goto_0

    .line 352708
    :sswitch_c
    const p2, 0x1f308

    goto :goto_0

    .line 352709
    :sswitch_d
    const p2, 0x1f30a

    goto :goto_0

    .line 352710
    :sswitch_e
    const p2, 0x1f319

    goto :goto_0

    .line 352711
    :sswitch_f
    const p2, 0x1f31f

    goto :goto_0

    .line 352712
    :sswitch_10
    const p2, 0x1f340

    goto :goto_0

    .line 352713
    :sswitch_11
    const p2, 0x1f337

    goto :goto_0

    .line 352714
    :sswitch_12
    const p2, 0x1f341

    goto :goto_0

    .line 352715
    :sswitch_13
    const p2, 0x1f338

    goto :goto_0

    .line 352716
    :sswitch_14
    const p2, 0x1f339

    goto :goto_0

    .line 352717
    :sswitch_15
    const p2, 0x1f342

    goto :goto_0

    .line 352718
    :sswitch_16
    const p2, 0x1f343

    goto :goto_0

    .line 352719
    :sswitch_17
    const p2, 0x1f33a

    goto :goto_0

    .line 352720
    :sswitch_18
    const p2, 0x1f33b

    goto :goto_0

    .line 352721
    :sswitch_19
    const p2, 0x1f334

    goto :goto_0

    .line 352722
    :sswitch_1a
    const p2, 0x1f335

    goto :goto_0

    .line 352723
    :sswitch_1b
    const p2, 0x1f33e

    goto :goto_0

    .line 352724
    :sswitch_1c
    const p2, 0x1f34e

    goto :goto_0

    .line 352725
    :sswitch_1d
    const p2, 0x1f34a

    goto :goto_0

    .line 352726
    :sswitch_1e
    const p2, 0x1f353

    goto :goto_0

    .line 352727
    :sswitch_1f
    const p2, 0x1f349

    goto :goto_0

    .line 352728
    :sswitch_20
    const p2, 0x1f345

    goto :goto_0

    .line 352729
    :sswitch_21
    const p2, 0x1f346

    goto/16 :goto_0

    .line 352730
    :sswitch_22
    const p2, 0x1f440

    goto/16 :goto_0

    .line 352731
    :sswitch_23
    const p2, 0x1f442

    goto/16 :goto_0

    .line 352732
    :sswitch_24
    const p2, 0x1f443

    goto/16 :goto_0

    .line 352733
    :sswitch_25
    const p2, 0x1f444

    goto/16 :goto_0

    .line 352734
    :sswitch_26
    const p2, 0x1f445

    goto/16 :goto_0

    .line 352735
    :sswitch_27
    const p2, 0x1f484

    goto/16 :goto_0

    .line 352736
    :sswitch_28
    const p2, 0x1f485

    goto/16 :goto_0

    .line 352737
    :sswitch_29
    const p2, 0x1f488

    goto/16 :goto_0

    .line 352738
    :sswitch_2a
    const p2, 0x1f466

    goto/16 :goto_0

    .line 352739
    :sswitch_2b
    const p2, 0x1f467

    goto/16 :goto_0

    .line 352740
    :sswitch_2c
    const p2, 0x1f468

    goto/16 :goto_0

    .line 352741
    :sswitch_2d
    const p2, 0x1f469

    goto/16 :goto_0

    .line 352742
    :sswitch_2e
    const p2, 0x1f46b

    goto/16 :goto_0

    .line 352743
    :sswitch_2f
    const p2, 0x1f46e

    goto/16 :goto_0

    .line 352744
    :sswitch_30
    const p2, 0x1f46f

    goto/16 :goto_0

    .line 352745
    :sswitch_31
    const p2, 0x1f471

    goto/16 :goto_0

    .line 352746
    :sswitch_32
    const p2, 0x1f472

    goto/16 :goto_0

    .line 352747
    :sswitch_33
    const p2, 0x1f473

    goto/16 :goto_0

    .line 352748
    :sswitch_34
    const p2, 0x1f474

    goto/16 :goto_0

    .line 352749
    :sswitch_35
    const p2, 0x1f475

    goto/16 :goto_0

    .line 352750
    :sswitch_36
    const p2, 0x1f476

    goto/16 :goto_0

    .line 352751
    :sswitch_37
    const p2, 0x1f477

    goto/16 :goto_0

    .line 352752
    :sswitch_38
    const p2, 0x1f478

    goto/16 :goto_0

    .line 352753
    :sswitch_39
    const p2, 0x1f47b

    goto/16 :goto_0

    .line 352754
    :sswitch_3a
    const p2, 0x1f47c

    goto/16 :goto_0

    .line 352755
    :sswitch_3b
    const p2, 0x1f47d

    goto/16 :goto_0

    .line 352756
    :sswitch_3c
    const p2, 0x1f47e

    goto/16 :goto_0

    .line 352757
    :sswitch_3d
    const p2, 0x1f47f

    goto/16 :goto_0

    .line 352758
    :sswitch_3e
    const p2, 0x1f480

    goto/16 :goto_0

    .line 352759
    :sswitch_3f
    const p2, 0x1f482

    goto/16 :goto_0

    .line 352760
    :sswitch_40
    const p2, 0x1f483

    goto/16 :goto_0

    .line 352761
    :sswitch_41
    const p2, 0x1f40d

    goto/16 :goto_0

    .line 352762
    :sswitch_42
    const p2, 0x1f40e

    goto/16 :goto_0

    .line 352763
    :sswitch_43
    const p2, 0x1f414

    goto/16 :goto_0

    .line 352764
    :sswitch_44
    const p2, 0x1f417

    goto/16 :goto_0

    .line 352765
    :sswitch_45
    const p2, 0x1f42b

    goto/16 :goto_0

    .line 352766
    :sswitch_46
    const p2, 0x1f418

    goto/16 :goto_0

    .line 352767
    :sswitch_47
    const p2, 0x1f428

    goto/16 :goto_0

    .line 352768
    :sswitch_48
    const p2, 0x1f412

    goto/16 :goto_0

    .line 352769
    :sswitch_49
    const p2, 0x1f411

    goto/16 :goto_0

    .line 352770
    :sswitch_4a
    const p2, 0x1f419

    goto/16 :goto_0

    .line 352771
    :sswitch_4b
    const p2, 0x1f41a

    goto/16 :goto_0

    .line 352772
    :sswitch_4c
    const p2, 0x1f41b

    goto/16 :goto_0

    .line 352773
    :sswitch_4d
    const p2, 0x1f420

    goto/16 :goto_0

    .line 352774
    :sswitch_4e
    const p2, 0x1f421

    goto/16 :goto_0

    .line 352775
    :sswitch_4f
    const p2, 0x1f425

    goto/16 :goto_0

    .line 352776
    :sswitch_50
    const p2, 0x1f426

    goto/16 :goto_0

    .line 352777
    :sswitch_51
    const p2, 0x1f427

    goto/16 :goto_0

    .line 352778
    :sswitch_52
    const p2, 0x1f429

    goto/16 :goto_0

    .line 352779
    :sswitch_53
    const p2, 0x1f42c

    goto/16 :goto_0

    .line 352780
    :sswitch_54
    const p2, 0x1f42d

    goto/16 :goto_0

    .line 352781
    :sswitch_55
    const p2, 0x1f42f

    goto/16 :goto_0

    .line 352782
    :sswitch_56
    const p2, 0x1f431

    goto/16 :goto_0

    .line 352783
    :sswitch_57
    const p2, 0x1f433

    goto/16 :goto_0

    .line 352784
    :sswitch_58
    const p2, 0x1f434

    goto/16 :goto_0

    .line 352785
    :sswitch_59
    const p2, 0x1f435

    goto/16 :goto_0

    .line 352786
    :sswitch_5a
    const p2, 0x1f437

    goto/16 :goto_0

    .line 352787
    :sswitch_5b
    const p2, 0x1f43b

    goto/16 :goto_0

    .line 352788
    :sswitch_5c
    const p2, 0x1f439

    goto/16 :goto_0

    .line 352789
    :sswitch_5d
    const p2, 0x1f43a

    goto/16 :goto_0

    .line 352790
    :sswitch_5e
    const p2, 0x1f42e

    goto/16 :goto_0

    .line 352791
    :sswitch_5f
    const p2, 0x1f430

    goto/16 :goto_0

    .line 352792
    :sswitch_60
    const p2, 0x1f438

    goto/16 :goto_0

    .line 352793
    :sswitch_61
    const p2, 0x1f43e

    goto/16 :goto_0

    .line 352794
    :sswitch_62
    const p2, 0x1f620

    goto/16 :goto_0

    .line 352795
    :sswitch_63
    const p2, 0x1f629

    goto/16 :goto_0

    .line 352796
    :sswitch_64
    const p2, 0x1f632

    goto/16 :goto_0

    .line 352797
    :sswitch_65
    const p2, 0x1f61e

    goto/16 :goto_0

    .line 352798
    :sswitch_66
    const p2, 0x1f635

    goto/16 :goto_0

    .line 352799
    :sswitch_67
    const p2, 0x1f630

    goto/16 :goto_0

    .line 352800
    :sswitch_68
    const p2, 0x1f612

    goto/16 :goto_0

    .line 352801
    :sswitch_69
    const p2, 0x1f60d

    goto/16 :goto_0

    .line 352802
    :sswitch_6a
    const p2, 0x1f624

    goto/16 :goto_0

    .line 352803
    :sswitch_6b
    const p2, 0x1f61c

    goto/16 :goto_0

    .line 352804
    :sswitch_6c
    const p2, 0x1f60b

    goto/16 :goto_0

    .line 352805
    :sswitch_6d
    const p2, 0x1f618

    goto/16 :goto_0

    .line 352806
    :sswitch_6e
    const p2, 0x1f61a

    goto/16 :goto_0

    .line 352807
    :sswitch_6f
    const p2, 0x1f637

    goto/16 :goto_0

    .line 352808
    :sswitch_70
    const p2, 0x1f633

    goto/16 :goto_0

    .line 352809
    :sswitch_71
    const p2, 0x1f603

    goto/16 :goto_0

    .line 352810
    :sswitch_72
    const p2, 0x1f606

    goto/16 :goto_0

    .line 352811
    :sswitch_73
    const p2, 0x1f602

    goto/16 :goto_0

    .line 352812
    :sswitch_74
    const/16 p2, 0x263a

    goto/16 :goto_0

    .line 352813
    :sswitch_75
    const p2, 0x1f604

    goto/16 :goto_0

    .line 352814
    :sswitch_76
    const p2, 0x1f622

    goto/16 :goto_0

    .line 352815
    :sswitch_77
    const p2, 0x1f62d

    goto/16 :goto_0

    .line 352816
    :sswitch_78
    const p2, 0x1f628

    goto/16 :goto_0

    .line 352817
    :sswitch_79
    const p2, 0x1f621

    goto/16 :goto_0

    .line 352818
    :sswitch_7a
    const p2, 0x1f616

    goto/16 :goto_0

    .line 352819
    :sswitch_7b
    const p2, 0x1f631

    goto/16 :goto_0

    .line 352820
    :sswitch_7c
    const p2, 0x1f62a

    goto/16 :goto_0

    .line 352821
    :sswitch_7d
    const p2, 0x1f60f

    goto/16 :goto_0

    .line 352822
    :sswitch_7e
    const p2, 0x1f613

    goto/16 :goto_0

    .line 352823
    :sswitch_7f
    const p2, 0x1f625

    goto/16 :goto_0

    .line 352824
    :sswitch_80
    const p2, 0x1f609

    goto/16 :goto_0

    .line 352825
    :sswitch_81
    const p2, 0x1f64b

    goto/16 :goto_0

    .line 352826
    :sswitch_82
    const p2, 0x1f64c

    goto/16 :goto_0

    .line 352827
    :sswitch_83
    const p2, 0x1f64f

    goto/16 :goto_0

    .line 352828
    :sswitch_84
    const p2, 0x1f3e0

    goto/16 :goto_0

    .line 352829
    :sswitch_85
    const p2, 0x1f3e2

    goto/16 :goto_0

    .line 352830
    :sswitch_86
    const p2, 0x1f3e3

    goto/16 :goto_0

    .line 352831
    :sswitch_87
    const p2, 0x1f3e5

    goto/16 :goto_0

    .line 352832
    :sswitch_88
    const p2, 0x1f3e6

    goto/16 :goto_0

    .line 352833
    :sswitch_89
    const p2, 0x1f3e7

    goto/16 :goto_0

    .line 352834
    :sswitch_8a
    const p2, 0x1f3e8

    goto/16 :goto_0

    .line 352835
    :sswitch_8b
    const p2, 0x1f3e9

    goto/16 :goto_0

    .line 352836
    :sswitch_8c
    const p2, 0x1f3ea

    goto/16 :goto_0

    .line 352837
    :sswitch_8d
    const p2, 0x1f3eb

    goto/16 :goto_0

    .line 352838
    :sswitch_8e
    const/16 p2, 0x26ea

    goto/16 :goto_0

    .line 352839
    :sswitch_8f
    const/16 p2, 0x26f2

    goto/16 :goto_0

    .line 352840
    :sswitch_90
    const p2, 0x1f3ec

    goto/16 :goto_0

    .line 352841
    :sswitch_91
    const p2, 0x1f3ef

    goto/16 :goto_0

    .line 352842
    :sswitch_92
    const p2, 0x1f3f0

    goto/16 :goto_0

    .line 352843
    :sswitch_93
    const p2, 0x1f3ed

    goto/16 :goto_0

    .line 352844
    :sswitch_94
    const p2, 0x1f5fb

    goto/16 :goto_0

    .line 352845
    :sswitch_95
    const p2, 0x1f5fc

    goto/16 :goto_0

    .line 352846
    :sswitch_96
    const p2, 0x1f5fd

    goto/16 :goto_0

    .line 352847
    :sswitch_97
    const p2, 0x1f45f

    goto/16 :goto_0

    .line 352848
    :sswitch_98
    const p2, 0x1f460

    goto/16 :goto_0

    .line 352849
    :sswitch_99
    const p2, 0x1f461

    goto/16 :goto_0

    .line 352850
    :sswitch_9a
    const p2, 0x1f462

    goto/16 :goto_0

    .line 352851
    :sswitch_9b
    const p2, 0x1f455

    goto/16 :goto_0

    .line 352852
    :sswitch_9c
    const p2, 0x1f451

    goto/16 :goto_0

    .line 352853
    :sswitch_9d
    const p2, 0x1f454

    goto/16 :goto_0

    .line 352854
    :sswitch_9e
    const p2, 0x1f452

    goto/16 :goto_0

    .line 352855
    :sswitch_9f
    const p2, 0x1f457

    goto/16 :goto_0

    .line 352856
    :sswitch_a0
    const p2, 0x1f458

    goto/16 :goto_0

    .line 352857
    :sswitch_a1
    const p2, 0x1f459

    goto/16 :goto_0

    .line 352858
    :sswitch_a2
    const p2, 0x1f45c

    goto/16 :goto_0

    .line 352859
    :sswitch_a3
    const p2, 0x1f4b0

    goto/16 :goto_0

    .line 352860
    :sswitch_a4
    const p2, 0x1f525

    goto/16 :goto_0

    .line 352861
    :sswitch_a5
    const p2, 0x1f528

    goto/16 :goto_0

    .line 352862
    :sswitch_a6
    const p2, 0x1f52b

    goto/16 :goto_0

    .line 352863
    :sswitch_a7
    const p2, 0x1f530

    goto/16 :goto_0

    .line 352864
    :sswitch_a8
    const p2, 0x1f531

    goto/16 :goto_0

    .line 352865
    :sswitch_a9
    const p2, 0x1f489

    goto/16 :goto_0

    .line 352866
    :sswitch_aa
    const p2, 0x1f48a

    goto/16 :goto_0

    .line 352867
    :sswitch_ab
    const p2, 0x1f380

    goto/16 :goto_0

    .line 352868
    :sswitch_ac
    const p2, 0x1f381

    goto/16 :goto_0

    .line 352869
    :sswitch_ad
    const p2, 0x1f384

    goto/16 :goto_0

    .line 352870
    :sswitch_ae
    const p2, 0x1f385

    goto/16 :goto_0

    .line 352871
    :sswitch_af
    const p2, 0x1f38c

    goto/16 :goto_0

    .line 352872
    :sswitch_b0
    const p2, 0x1f388

    goto/16 :goto_0

    .line 352873
    :sswitch_b1
    const p2, 0x1f389

    goto/16 :goto_0

    .line 352874
    :sswitch_b2
    const p2, 0x1f38d

    goto/16 :goto_0

    .line 352875
    :sswitch_b3
    const p2, 0x1f38e

    goto/16 :goto_0

    .line 352876
    :sswitch_b4
    const p2, 0x1f393

    goto/16 :goto_0

    .line 352877
    :sswitch_b5
    const p2, 0x1f392

    goto/16 :goto_0

    .line 352878
    :sswitch_b6
    const p2, 0x1f38f

    goto/16 :goto_0

    .line 352879
    :sswitch_b7
    const p2, 0x1f390

    goto/16 :goto_0

    .line 352880
    :sswitch_b8
    const p2, 0x1f383

    goto/16 :goto_0

    .line 352881
    :sswitch_b9
    const/16 p2, 0x260e

    goto/16 :goto_0

    .line 352882
    :sswitch_ba
    const p2, 0x1f4f1

    goto/16 :goto_0

    .line 352883
    :sswitch_bb
    const p2, 0x1f4f2

    goto/16 :goto_0

    .line 352884
    :sswitch_bc
    const p2, 0x1f4dd

    goto/16 :goto_0

    .line 352885
    :sswitch_bd
    const p2, 0x1f4e0

    goto/16 :goto_0

    .line 352886
    :sswitch_be
    const/16 p2, 0x2709

    goto/16 :goto_0

    .line 352887
    :sswitch_bf
    const p2, 0x1f4ea

    goto/16 :goto_0

    .line 352888
    :sswitch_c0
    const p2, 0x1f4ee

    goto/16 :goto_0

    .line 352889
    :sswitch_c1
    const p2, 0x1f4e2

    goto/16 :goto_0

    .line 352890
    :sswitch_c2
    const p2, 0x1f4e3

    goto/16 :goto_0

    .line 352891
    :sswitch_c3
    const p2, 0x1f4e1

    goto/16 :goto_0

    .line 352892
    :sswitch_c4
    const p2, 0x1f4ba

    goto/16 :goto_0

    .line 352893
    :sswitch_c5
    const p2, 0x1f4bb

    goto/16 :goto_0

    .line 352894
    :sswitch_c6
    const p2, 0x1f4bc

    goto/16 :goto_0

    .line 352895
    :sswitch_c7
    const p2, 0x1f4bd

    goto/16 :goto_0

    .line 352896
    :sswitch_c8
    const p2, 0x1f4bf

    goto/16 :goto_0

    .line 352897
    :sswitch_c9
    const p2, 0x1f4c0

    goto/16 :goto_0

    .line 352898
    :sswitch_ca
    const/16 p2, 0x2702

    goto/16 :goto_0

    .line 352899
    :sswitch_cb
    const p2, 0x1f4d6

    goto/16 :goto_0

    .line 352900
    :sswitch_cc
    const/16 p2, 0x26be

    goto/16 :goto_0

    .line 352901
    :sswitch_cd
    const/16 p2, 0x26f3

    goto/16 :goto_0

    .line 352902
    :sswitch_ce
    const p2, 0x1f3be

    goto/16 :goto_0

    .line 352903
    :sswitch_cf
    const/16 p2, 0x26bd

    goto/16 :goto_0

    .line 352904
    :sswitch_d0
    const p2, 0x1f3bf

    goto/16 :goto_0

    .line 352905
    :sswitch_d1
    const p2, 0x1f3c0

    goto/16 :goto_0

    .line 352906
    :sswitch_d2
    const p2, 0x1f3c1

    goto/16 :goto_0

    .line 352907
    :sswitch_d3
    const p2, 0x1f3c6

    goto/16 :goto_0

    .line 352908
    :sswitch_d4
    const p2, 0x1f3c8

    goto/16 :goto_0

    .line 352909
    :sswitch_d5
    const p2, 0x1f683

    goto/16 :goto_0

    .line 352910
    :sswitch_d6
    const p2, 0x1f684

    goto/16 :goto_0

    .line 352911
    :sswitch_d7
    const p2, 0x1f685

    goto/16 :goto_0

    .line 352912
    :sswitch_d8
    const p2, 0x1f699

    goto/16 :goto_0

    .line 352913
    :sswitch_d9
    const p2, 0x1f68c

    goto/16 :goto_0

    .line 352914
    :sswitch_da
    const p2, 0x1f68f

    goto/16 :goto_0

    .line 352915
    :sswitch_db
    const p2, 0x1f6a2

    goto/16 :goto_0

    .line 352916
    :sswitch_dc
    const/16 p2, 0x2708

    goto/16 :goto_0

    .line 352917
    :sswitch_dd
    const/16 p2, 0x26f5

    goto/16 :goto_0

    .line 352918
    :sswitch_de
    const p2, 0x1f689

    goto/16 :goto_0

    .line 352919
    :sswitch_df
    const p2, 0x1f680

    goto/16 :goto_0

    .line 352920
    :sswitch_e0
    const p2, 0x1f6a4

    goto/16 :goto_0

    .line 352921
    :sswitch_e1
    const p2, 0x1f695

    goto/16 :goto_0

    .line 352922
    :sswitch_e2
    const p2, 0x1f69a

    goto/16 :goto_0

    .line 352923
    :sswitch_e3
    const p2, 0x1f692

    goto/16 :goto_0

    .line 352924
    :sswitch_e4
    const p2, 0x1f691

    goto/16 :goto_0

    .line 352925
    :sswitch_e5
    const p2, 0x1f693

    goto/16 :goto_0

    .line 352926
    :sswitch_e6
    const/16 p2, 0x26fd

    goto/16 :goto_0

    .line 352927
    :sswitch_e7
    const p2, 0x1f6a5

    goto/16 :goto_0

    .line 352928
    :sswitch_e8
    const p2, 0x1f6a7

    goto/16 :goto_0

    .line 352929
    :sswitch_e9
    const/16 p2, 0x2668

    goto/16 :goto_0

    .line 352930
    :sswitch_ea
    const/16 p2, 0x26fa

    goto/16 :goto_0

    .line 352931
    :sswitch_eb
    const p2, 0x1f3a1

    goto/16 :goto_0

    .line 352932
    :sswitch_ec
    const p2, 0x1f3a2

    goto/16 :goto_0

    .line 352933
    :sswitch_ed
    const p2, 0x1f3a4

    goto/16 :goto_0

    .line 352934
    :sswitch_ee
    const p2, 0x1f3a5

    goto/16 :goto_0

    .line 352935
    :sswitch_ef
    const p2, 0x1f3a6

    goto/16 :goto_0

    .line 352936
    :sswitch_f0
    const p2, 0x1f3a7

    goto/16 :goto_0

    .line 352937
    :sswitch_f1
    const p2, 0x1f3a8

    goto/16 :goto_0

    .line 352938
    :sswitch_f2
    const p2, 0x1f3a9

    goto/16 :goto_0

    .line 352939
    :sswitch_f3
    const p2, 0x1f3ab

    goto/16 :goto_0

    .line 352940
    :sswitch_f4
    const p2, 0x1f3ac

    goto/16 :goto_0

    .line 352941
    :sswitch_f5
    const p2, 0x1f004

    goto/16 :goto_0

    .line 352942
    :sswitch_f6
    const p2, 0x1f3af

    goto/16 :goto_0

    .line 352943
    :sswitch_f7
    const p2, 0x1f3b1

    goto/16 :goto_0

    .line 352944
    :sswitch_f8
    const p2, 0x1f3b5

    goto/16 :goto_0

    .line 352945
    :sswitch_f9
    const p2, 0x1f3b6

    goto/16 :goto_0

    .line 352946
    :sswitch_fa
    const p2, 0x1f3b7

    goto/16 :goto_0

    .line 352947
    :sswitch_fb
    const p2, 0x1f3b8

    goto/16 :goto_0

    .line 352948
    :sswitch_fc
    const p2, 0x1f3ba

    goto/16 :goto_0

    .line 352949
    :sswitch_fd
    const/16 p2, 0x303d

    goto/16 :goto_0

    .line 352950
    :sswitch_fe
    const p2, 0x1f4f7

    goto/16 :goto_0

    .line 352951
    :sswitch_ff
    const p2, 0x1f4fa

    goto/16 :goto_0

    .line 352952
    :sswitch_100
    const p2, 0x1f4fb

    goto/16 :goto_0

    .line 352953
    :sswitch_101
    const p2, 0x1f4fc

    goto/16 :goto_0

    .line 352954
    :sswitch_102
    const p2, 0x1f48b

    goto/16 :goto_0

    .line 352955
    :sswitch_103
    const p2, 0x1f48d

    goto/16 :goto_0

    .line 352956
    :sswitch_104
    const p2, 0x1f48e

    goto/16 :goto_0

    .line 352957
    :sswitch_105
    const p2, 0x1f48f

    goto/16 :goto_0

    .line 352958
    :sswitch_106
    const p2, 0x1f490

    goto/16 :goto_0

    .line 352959
    :sswitch_107
    const p2, 0x1f491

    goto/16 :goto_0

    .line 352960
    :sswitch_108
    const p2, 0x1f492

    goto/16 :goto_0

    .line 352961
    :sswitch_109
    const p2, 0x1f51e

    goto/16 :goto_0

    .line 352962
    :sswitch_10a
    const p2, 0x1f4f6

    goto/16 :goto_0

    .line 352963
    :sswitch_10b
    const p2, 0x1f4f3

    goto/16 :goto_0

    .line 352964
    :sswitch_10c
    const p2, 0x1f4f4

    goto/16 :goto_0

    .line 352965
    :sswitch_10d
    const p2, 0x1f354

    goto/16 :goto_0

    .line 352966
    :sswitch_10e
    const p2, 0x1f359

    goto/16 :goto_0

    .line 352967
    :sswitch_10f
    const p2, 0x1f370

    goto/16 :goto_0

    .line 352968
    :sswitch_110
    const p2, 0x1f35c

    goto/16 :goto_0

    .line 352969
    :sswitch_111
    const p2, 0x1f35e

    goto/16 :goto_0

    .line 352970
    :sswitch_112
    const p2, 0x1f373

    goto/16 :goto_0

    .line 352971
    :sswitch_113
    const p2, 0x1f366

    goto/16 :goto_0

    .line 352972
    :sswitch_114
    const p2, 0x1f35f

    goto/16 :goto_0

    .line 352973
    :sswitch_115
    const p2, 0x1f361

    goto/16 :goto_0

    .line 352974
    :sswitch_116
    const p2, 0x1f358

    goto/16 :goto_0

    .line 352975
    :sswitch_117
    const p2, 0x1f35a

    goto/16 :goto_0

    .line 352976
    :sswitch_118
    const p2, 0x1f35d

    goto/16 :goto_0

    .line 352977
    :sswitch_119
    const p2, 0x1f35b

    goto/16 :goto_0

    .line 352978
    :sswitch_11a
    const p2, 0x1f362

    goto/16 :goto_0

    .line 352979
    :sswitch_11b
    const p2, 0x1f363

    goto/16 :goto_0

    .line 352980
    :sswitch_11c
    const p2, 0x1f371

    goto/16 :goto_0

    .line 352981
    :sswitch_11d
    const p2, 0x1f372

    goto/16 :goto_0

    .line 352982
    :sswitch_11e
    const p2, 0x1f367

    goto/16 :goto_0

    .line 352983
    :sswitch_11f
    const p2, 0x1f374

    goto/16 :goto_0

    .line 352984
    :sswitch_120
    const/16 p2, 0x2615

    goto/16 :goto_0

    .line 352985
    :sswitch_121
    const p2, 0x1f378

    goto/16 :goto_0

    .line 352986
    :sswitch_122
    const p2, 0x1f37a

    goto/16 :goto_0

    .line 352987
    :sswitch_123
    const p2, 0x1f375

    goto/16 :goto_0

    .line 352988
    :sswitch_124
    const p2, 0x1f376

    goto/16 :goto_0

    .line 352989
    :sswitch_125
    const p2, 0x1f37b

    goto/16 :goto_0

    .line 352990
    :sswitch_126
    const/16 p2, 0x2197

    goto/16 :goto_0

    .line 352991
    :sswitch_127
    const/16 p2, 0x2198

    goto/16 :goto_0

    .line 352992
    :sswitch_128
    const/16 p2, 0x2196

    goto/16 :goto_0

    .line 352993
    :sswitch_129
    const/16 p2, 0x2199

    goto/16 :goto_0

    .line 352994
    :sswitch_12a
    const/16 p2, 0x2b06

    goto/16 :goto_0

    .line 352995
    :sswitch_12b
    const/16 p2, 0x2b07

    goto/16 :goto_0

    .line 352996
    :sswitch_12c
    const/16 p2, 0x27a1

    goto/16 :goto_0

    .line 352997
    :sswitch_12d
    const/16 p2, 0x2b05

    goto/16 :goto_0

    .line 352998
    :sswitch_12e
    const/16 p2, 0x2b55

    goto/16 :goto_0

    .line 352999
    :sswitch_12f
    const/16 p2, 0x274c

    goto/16 :goto_0

    .line 353000
    :sswitch_130
    const/16 p2, 0x2757

    goto/16 :goto_0

    .line 353001
    :sswitch_131
    const/16 p2, 0x2753

    goto/16 :goto_0

    .line 353002
    :sswitch_132
    const/16 p2, 0x2754

    goto/16 :goto_0

    .line 353003
    :sswitch_133
    const/16 p2, 0x2755

    goto/16 :goto_0

    .line 353004
    :sswitch_134
    const/16 p2, 0x27bf

    goto/16 :goto_0

    .line 353005
    :sswitch_135
    const/16 p2, 0x2764

    goto/16 :goto_0

    .line 353006
    :sswitch_136
    const p2, 0x1f493

    goto/16 :goto_0

    .line 353007
    :sswitch_137
    const p2, 0x1f494

    goto/16 :goto_0

    .line 353008
    :sswitch_138
    const p2, 0x1f497

    goto/16 :goto_0

    .line 353009
    :sswitch_139
    const p2, 0x1f498

    goto/16 :goto_0

    .line 353010
    :sswitch_13a
    const p2, 0x1f499

    goto/16 :goto_0

    .line 353011
    :sswitch_13b
    const p2, 0x1f49a

    goto/16 :goto_0

    .line 353012
    :sswitch_13c
    const p2, 0x1f49b

    goto/16 :goto_0

    .line 353013
    :sswitch_13d
    const p2, 0x1f49c

    goto/16 :goto_0

    .line 353014
    :sswitch_13e
    const p2, 0x1f49d

    goto/16 :goto_0

    .line 353015
    :sswitch_13f
    const p2, 0x1f49f

    goto/16 :goto_0

    .line 353016
    :sswitch_140
    const/16 p2, 0x2665

    goto/16 :goto_0

    .line 353017
    :sswitch_141
    const/16 p2, 0x2660

    goto/16 :goto_0

    .line 353018
    :sswitch_142
    const/16 p2, 0x2666

    goto/16 :goto_0

    .line 353019
    :sswitch_143
    const/16 p2, 0x2663

    goto/16 :goto_0

    .line 353020
    :sswitch_144
    const p2, 0x1f6ac

    goto/16 :goto_0

    .line 353021
    :sswitch_145
    const p2, 0x1f6ad

    goto/16 :goto_0

    .line 353022
    :sswitch_146
    const/16 p2, 0x267f

    goto/16 :goto_0

    .line 353023
    :sswitch_147
    const/16 p2, 0x26a0

    goto/16 :goto_0

    .line 353024
    :sswitch_148
    const p2, 0x1f6b2

    goto/16 :goto_0

    .line 353025
    :sswitch_149
    const p2, 0x1f6b9

    goto/16 :goto_0

    .line 353026
    :sswitch_14a
    const p2, 0x1f6ba

    goto/16 :goto_0

    .line 353027
    :sswitch_14b
    const p2, 0x1f6c0

    goto/16 :goto_0

    .line 353028
    :sswitch_14c
    const p2, 0x1f6bb

    goto/16 :goto_0

    .line 353029
    :sswitch_14d
    const p2, 0x1f6bd

    goto/16 :goto_0

    .line 353030
    :sswitch_14e
    const p2, 0x1f6be

    goto/16 :goto_0

    .line 353031
    :sswitch_14f
    const p2, 0x1f6bc

    goto/16 :goto_0

    .line 353032
    :sswitch_150
    const p2, 0x1f192

    goto/16 :goto_0

    .line 353033
    :sswitch_151
    const p2, 0x1f199

    goto/16 :goto_0

    .line 353034
    :sswitch_152
    const p2, 0x1f19a

    goto/16 :goto_0

    .line 353035
    :sswitch_153
    const p2, 0x1f201

    goto/16 :goto_0

    .line 353036
    :sswitch_154
    const p2, 0x1f202

    goto/16 :goto_0

    .line 353037
    :sswitch_155
    const p2, 0x1f233

    goto/16 :goto_0

    .line 353038
    :sswitch_156
    const p2, 0x1f235

    goto/16 :goto_0

    .line 353039
    :sswitch_157
    const p2, 0x1f236

    goto/16 :goto_0

    .line 353040
    :sswitch_158
    const p2, 0x1f21a

    goto/16 :goto_0

    .line 353041
    :sswitch_159
    const p2, 0x1f237

    goto/16 :goto_0

    .line 353042
    :sswitch_15a
    const p2, 0x1f238

    goto/16 :goto_0

    .line 353043
    :sswitch_15b
    const p2, 0x1f239

    goto/16 :goto_0

    .line 353044
    :sswitch_15c
    const p2, 0x1f22f

    goto/16 :goto_0

    .line 353045
    :sswitch_15d
    const p2, 0x1f23a

    goto/16 :goto_0

    .line 353046
    :sswitch_15e
    const/16 p2, 0x3299

    goto/16 :goto_0

    .line 353047
    :sswitch_15f
    const/16 p2, 0x3297

    goto/16 :goto_0

    .line 353048
    :sswitch_160
    const p2, 0x1f250

    goto/16 :goto_0

    .line 353049
    :sswitch_161
    const p2, 0x1f4a1

    goto/16 :goto_0

    .line 353050
    :sswitch_162
    const p2, 0x1f4a2

    goto/16 :goto_0

    .line 353051
    :sswitch_163
    const p2, 0x1f4a3

    goto/16 :goto_0

    .line 353052
    :sswitch_164
    const p2, 0x1f4a4

    goto/16 :goto_0

    .line 353053
    :sswitch_165
    const p2, 0x1f4a6

    goto/16 :goto_0

    .line 353054
    :sswitch_166
    const p2, 0x1f4a8

    goto/16 :goto_0

    .line 353055
    :sswitch_167
    const p2, 0x1f4a9

    goto/16 :goto_0

    .line 353056
    :sswitch_168
    const p2, 0x1f4aa

    goto/16 :goto_0

    .line 353057
    :sswitch_169
    const/16 p2, 0x2728

    goto/16 :goto_0

    .line 353058
    :sswitch_16a
    const/16 p2, 0x2734

    goto/16 :goto_0

    .line 353059
    :sswitch_16b
    const/16 p2, 0x2733

    goto/16 :goto_0

    .line 353060
    :sswitch_16c
    const/16 p2, 0x26aa

    goto/16 :goto_0

    .line 353061
    :sswitch_16d
    const p2, 0x1f535

    goto/16 :goto_0

    .line 353062
    :sswitch_16e
    const p2, 0x1f533

    goto/16 :goto_0

    .line 353063
    :sswitch_16f
    const/16 p2, 0x2b50

    goto/16 :goto_0

    .line 353064
    :sswitch_170
    const p2, 0x1f50d

    goto/16 :goto_0

    .line 353065
    :sswitch_171
    const p2, 0x1f512

    goto/16 :goto_0

    .line 353066
    :sswitch_172
    const p2, 0x1f513

    goto/16 :goto_0

    .line 353067
    :sswitch_173
    const p2, 0x1f511

    goto/16 :goto_0

    .line 353068
    :sswitch_174
    const p2, 0x1f514

    goto/16 :goto_0

    .line 353069
    :sswitch_175
    const/16 p2, 0x270a

    goto/16 :goto_0

    .line 353070
    :sswitch_176
    const/16 p2, 0x270c

    goto/16 :goto_0

    .line 353071
    :sswitch_177
    const p2, 0x1f44a

    goto/16 :goto_0

    .line 353072
    :sswitch_178
    const p2, 0x1f44d

    goto/16 :goto_0

    .line 353073
    :sswitch_179
    const/16 p2, 0x261d

    goto/16 :goto_0

    .line 353074
    :sswitch_17a
    const p2, 0x1f446

    goto/16 :goto_0

    .line 353075
    :sswitch_17b
    const p2, 0x1f447

    goto/16 :goto_0

    .line 353076
    :sswitch_17c
    const p2, 0x1f448

    goto/16 :goto_0

    .line 353077
    :sswitch_17d
    const p2, 0x1f449

    goto/16 :goto_0

    .line 353078
    :sswitch_17e
    const p2, 0x1f44b

    goto/16 :goto_0

    .line 353079
    :sswitch_17f
    const p2, 0x1f44f

    goto/16 :goto_0

    .line 353080
    :sswitch_180
    const p2, 0x1f44c

    goto/16 :goto_0

    .line 353081
    :sswitch_181
    const p2, 0x1f44e

    goto/16 :goto_0

    .line 353082
    :sswitch_182
    const p2, 0x1f450

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xe001 -> :sswitch_2a
        0xe002 -> :sswitch_2b
        0xe003 -> :sswitch_102
        0xe004 -> :sswitch_2c
        0xe005 -> :sswitch_2d
        0xe006 -> :sswitch_9b
        0xe007 -> :sswitch_97
        0xe008 -> :sswitch_fe
        0xe009 -> :sswitch_b9
        0xe00a -> :sswitch_ba
        0xe00b -> :sswitch_bd
        0xe00c -> :sswitch_c5
        0xe00d -> :sswitch_177
        0xe00e -> :sswitch_178
        0xe00f -> :sswitch_179
        0xe010 -> :sswitch_175
        0xe011 -> :sswitch_176
        0xe012 -> :sswitch_81
        0xe013 -> :sswitch_d0
        0xe014 -> :sswitch_cd
        0xe015 -> :sswitch_ce
        0xe016 -> :sswitch_cc
        0xe018 -> :sswitch_cf
        0xe019 -> :sswitch_4e
        0xe01a -> :sswitch_58
        0xe01c -> :sswitch_dd
        0xe01d -> :sswitch_dc
        0xe01e -> :sswitch_d5
        0xe01f -> :sswitch_d7
        0xe020 -> :sswitch_131
        0xe021 -> :sswitch_130
        0xe022 -> :sswitch_135
        0xe023 -> :sswitch_137
        0xe030 -> :sswitch_13
        0xe031 -> :sswitch_a8
        0xe032 -> :sswitch_14
        0xe033 -> :sswitch_ad
        0xe034 -> :sswitch_103
        0xe035 -> :sswitch_104
        0xe036 -> :sswitch_84
        0xe037 -> :sswitch_8e
        0xe038 -> :sswitch_85
        0xe039 -> :sswitch_de
        0xe03a -> :sswitch_e6
        0xe03b -> :sswitch_94
        0xe03c -> :sswitch_ed
        0xe03d -> :sswitch_ee
        0xe03e -> :sswitch_f8
        0xe03f -> :sswitch_173
        0xe040 -> :sswitch_fa
        0xe041 -> :sswitch_fb
        0xe042 -> :sswitch_fc
        0xe043 -> :sswitch_11f
        0xe044 -> :sswitch_121
        0xe045 -> :sswitch_120
        0xe046 -> :sswitch_10f
        0xe047 -> :sswitch_122
        0xe048 -> :sswitch_3
        0xe049 -> :sswitch_1
        0xe04a -> :sswitch_0
        0xe04b -> :sswitch_2
        0xe04c -> :sswitch_e
        0xe04d -> :sswitch_8
        0xe04e -> :sswitch_3a
        0xe04f -> :sswitch_56
        0xe050 -> :sswitch_55
        0xe051 -> :sswitch_5b
        0xe052 -> :sswitch_52
        0xe053 -> :sswitch_54
        0xe054 -> :sswitch_57
        0xe055 -> :sswitch_51
        0xe056 -> :sswitch_6c
        0xe057 -> :sswitch_71
        0xe058 -> :sswitch_65
        0xe059 -> :sswitch_62
        0xe05a -> :sswitch_167
        0xe101 -> :sswitch_bf
        0xe102 -> :sswitch_c0
        0xe103 -> :sswitch_be
        0xe104 -> :sswitch_bb
        0xe105 -> :sswitch_6b
        0xe106 -> :sswitch_69
        0xe107 -> :sswitch_7b
        0xe108 -> :sswitch_7e
        0xe109 -> :sswitch_59
        0xe10a -> :sswitch_4a
        0xe10b -> :sswitch_5a
        0xe10c -> :sswitch_3b
        0xe10d -> :sswitch_df
        0xe10e -> :sswitch_9c
        0xe10f -> :sswitch_161
        0xe110 -> :sswitch_10
        0xe111 -> :sswitch_105
        0xe112 -> :sswitch_ac
        0xe113 -> :sswitch_a6
        0xe114 -> :sswitch_170
        0xe116 -> :sswitch_a5
        0xe118 -> :sswitch_12
        0xe119 -> :sswitch_15
        0xe11a -> :sswitch_3d
        0xe11b -> :sswitch_39
        0xe11c -> :sswitch_3e
        0xe11d -> :sswitch_a4
        0xe11e -> :sswitch_c6
        0xe11f -> :sswitch_c4
        0xe120 -> :sswitch_10d
        0xe121 -> :sswitch_8f
        0xe122 -> :sswitch_ea
        0xe123 -> :sswitch_e9
        0xe124 -> :sswitch_eb
        0xe125 -> :sswitch_f3
        0xe126 -> :sswitch_c8
        0xe127 -> :sswitch_c9
        0xe128 -> :sswitch_100
        0xe129 -> :sswitch_101
        0xe12a -> :sswitch_ff
        0xe12b -> :sswitch_3c
        0xe12c -> :sswitch_fd
        0xe12d -> :sswitch_f5
        0xe12e -> :sswitch_152
        0xe12f -> :sswitch_a3
        0xe130 -> :sswitch_f6
        0xe131 -> :sswitch_d3
        0xe132 -> :sswitch_d2
        0xe134 -> :sswitch_42
        0xe135 -> :sswitch_e0
        0xe136 -> :sswitch_148
        0xe137 -> :sswitch_e8
        0xe138 -> :sswitch_149
        0xe139 -> :sswitch_14a
        0xe13a -> :sswitch_14f
        0xe13b -> :sswitch_a9
        0xe13c -> :sswitch_164
        0xe13d -> :sswitch_4
        0xe13e -> :sswitch_98
        0xe13f -> :sswitch_14b
        0xe140 -> :sswitch_14d
        0xe142 -> :sswitch_c1
        0xe143 -> :sswitch_af
        0xe144 -> :sswitch_171
        0xe145 -> :sswitch_172
        0xe146 -> :sswitch_a
        0xe147 -> :sswitch_112
        0xe148 -> :sswitch_cb
        0xe14b -> :sswitch_c3
        0xe14c -> :sswitch_168
        0xe14d -> :sswitch_88
        0xe14e -> :sswitch_e7
        0xe150 -> :sswitch_da
        0xe151 -> :sswitch_14c
        0xe152 -> :sswitch_2f
        0xe153 -> :sswitch_86
        0xe154 -> :sswitch_89
        0xe155 -> :sswitch_87
        0xe156 -> :sswitch_8c
        0xe157 -> :sswitch_8d
        0xe158 -> :sswitch_8a
        0xe159 -> :sswitch_d9
        0xe15a -> :sswitch_e1
        0xe202 -> :sswitch_db
        0xe203 -> :sswitch_153
        0xe204 -> :sswitch_13f
        0xe205 -> :sswitch_16a
        0xe206 -> :sswitch_16b
        0xe207 -> :sswitch_109
        0xe208 -> :sswitch_145
        0xe209 -> :sswitch_a7
        0xe20a -> :sswitch_146
        0xe20b -> :sswitch_10a
        0xe20c -> :sswitch_140
        0xe20d -> :sswitch_142
        0xe20e -> :sswitch_141
        0xe20f -> :sswitch_143
        0xe211 -> :sswitch_134
        0xe213 -> :sswitch_151
        0xe214 -> :sswitch_150
        0xe215 -> :sswitch_157
        0xe216 -> :sswitch_158
        0xe217 -> :sswitch_159
        0xe218 -> :sswitch_15a
        0xe219 -> :sswitch_16c
        0xe21a -> :sswitch_16d
        0xe21b -> :sswitch_16e
        0xe226 -> :sswitch_160
        0xe227 -> :sswitch_15b
        0xe228 -> :sswitch_154
        0xe22a -> :sswitch_156
        0xe22b -> :sswitch_155
        0xe22c -> :sswitch_15c
        0xe22d -> :sswitch_15d
        0xe22e -> :sswitch_17a
        0xe22f -> :sswitch_17b
        0xe230 -> :sswitch_17c
        0xe231 -> :sswitch_17d
        0xe232 -> :sswitch_12a
        0xe233 -> :sswitch_12b
        0xe234 -> :sswitch_12c
        0xe235 -> :sswitch_12d
        0xe236 -> :sswitch_126
        0xe237 -> :sswitch_128
        0xe238 -> :sswitch_127
        0xe239 -> :sswitch_129
        0xe250 -> :sswitch_10b
        0xe251 -> :sswitch_10c
        0xe252 -> :sswitch_147
        0xe301 -> :sswitch_bc
        0xe302 -> :sswitch_9d
        0xe303 -> :sswitch_17
        0xe304 -> :sswitch_11
        0xe305 -> :sswitch_18
        0xe306 -> :sswitch_106
        0xe307 -> :sswitch_19
        0xe308 -> :sswitch_1a
        0xe309 -> :sswitch_14e
        0xe30a -> :sswitch_f0
        0xe30b -> :sswitch_124
        0xe30c -> :sswitch_125
        0xe30d -> :sswitch_15f
        0xe30e -> :sswitch_144
        0xe30f -> :sswitch_aa
        0xe310 -> :sswitch_b0
        0xe311 -> :sswitch_163
        0xe312 -> :sswitch_b1
        0xe313 -> :sswitch_ca
        0xe314 -> :sswitch_ab
        0xe315 -> :sswitch_15e
        0xe316 -> :sswitch_c7
        0xe317 -> :sswitch_c2
        0xe318 -> :sswitch_9e
        0xe319 -> :sswitch_9f
        0xe31a -> :sswitch_99
        0xe31b -> :sswitch_9a
        0xe31c -> :sswitch_27
        0xe31d -> :sswitch_28
        0xe320 -> :sswitch_29
        0xe321 -> :sswitch_a0
        0xe322 -> :sswitch_a1
        0xe323 -> :sswitch_a2
        0xe324 -> :sswitch_f4
        0xe325 -> :sswitch_174
        0xe326 -> :sswitch_f9
        0xe327 -> :sswitch_136
        0xe328 -> :sswitch_138
        0xe329 -> :sswitch_139
        0xe32a -> :sswitch_13a
        0xe32b -> :sswitch_13b
        0xe32c -> :sswitch_13c
        0xe32d -> :sswitch_13d
        0xe32e -> :sswitch_169
        0xe32f -> :sswitch_16f
        0xe330 -> :sswitch_166
        0xe331 -> :sswitch_165
        0xe332 -> :sswitch_12e
        0xe333 -> :sswitch_12f
        0xe334 -> :sswitch_162
        0xe335 -> :sswitch_f
        0xe336 -> :sswitch_132
        0xe337 -> :sswitch_133
        0xe338 -> :sswitch_123
        0xe339 -> :sswitch_111
        0xe33a -> :sswitch_113
        0xe33b -> :sswitch_114
        0xe33c -> :sswitch_115
        0xe33d -> :sswitch_116
        0xe33e -> :sswitch_117
        0xe33f -> :sswitch_118
        0xe340 -> :sswitch_110
        0xe341 -> :sswitch_119
        0xe342 -> :sswitch_10e
        0xe343 -> :sswitch_11a
        0xe344 -> :sswitch_11b
        0xe345 -> :sswitch_1c
        0xe346 -> :sswitch_1d
        0xe347 -> :sswitch_1e
        0xe348 -> :sswitch_1f
        0xe349 -> :sswitch_20
        0xe34a -> :sswitch_21
        0xe34c -> :sswitch_11c
        0xe34d -> :sswitch_11d
        0xe401 -> :sswitch_7f
        0xe402 -> :sswitch_7d
        0xe403 -> :sswitch_63
        0xe404 -> :sswitch_6a
        0xe405 -> :sswitch_80
        0xe406 -> :sswitch_66
        0xe407 -> :sswitch_7a
        0xe408 -> :sswitch_7c
        0xe409 -> :sswitch_26
        0xe40a -> :sswitch_72
        0xe40b -> :sswitch_78
        0xe40c -> :sswitch_6f
        0xe40d -> :sswitch_70
        0xe40e -> :sswitch_68
        0xe40f -> :sswitch_67
        0xe410 -> :sswitch_64
        0xe411 -> :sswitch_77
        0xe412 -> :sswitch_73
        0xe413 -> :sswitch_76
        0xe414 -> :sswitch_74
        0xe415 -> :sswitch_75
        0xe416 -> :sswitch_79
        0xe417 -> :sswitch_6e
        0xe418 -> :sswitch_6d
        0xe419 -> :sswitch_22
        0xe41a -> :sswitch_24
        0xe41b -> :sswitch_23
        0xe41c -> :sswitch_25
        0xe41d -> :sswitch_83
        0xe41e -> :sswitch_17e
        0xe41f -> :sswitch_17f
        0xe420 -> :sswitch_180
        0xe421 -> :sswitch_181
        0xe422 -> :sswitch_182
        0xe425 -> :sswitch_107
        0xe427 -> :sswitch_82
        0xe428 -> :sswitch_2e
        0xe429 -> :sswitch_30
        0xe42a -> :sswitch_d1
        0xe42b -> :sswitch_d4
        0xe42c -> :sswitch_f7
        0xe42e -> :sswitch_d8
        0xe42f -> :sswitch_e2
        0xe430 -> :sswitch_e3
        0xe431 -> :sswitch_e4
        0xe432 -> :sswitch_e5
        0xe433 -> :sswitch_ec
        0xe435 -> :sswitch_d6
        0xe436 -> :sswitch_b2
        0xe437 -> :sswitch_13e
        0xe438 -> :sswitch_b3
        0xe439 -> :sswitch_b4
        0xe43a -> :sswitch_b5
        0xe43b -> :sswitch_b6
        0xe43c -> :sswitch_6
        0xe43d -> :sswitch_108
        0xe43e -> :sswitch_d
        0xe43f -> :sswitch_11e
        0xe441 -> :sswitch_4b
        0xe442 -> :sswitch_b7
        0xe443 -> :sswitch_5
        0xe444 -> :sswitch_1b
        0xe445 -> :sswitch_b8
        0xe447 -> :sswitch_16
        0xe448 -> :sswitch_ae
        0xe449 -> :sswitch_9
        0xe44a -> :sswitch_b
        0xe44b -> :sswitch_7
        0xe44c -> :sswitch_c
        0xe501 -> :sswitch_8b
        0xe502 -> :sswitch_f1
        0xe503 -> :sswitch_f2
        0xe504 -> :sswitch_90
        0xe505 -> :sswitch_91
        0xe506 -> :sswitch_92
        0xe507 -> :sswitch_ef
        0xe508 -> :sswitch_93
        0xe509 -> :sswitch_95
        0xe515 -> :sswitch_31
        0xe516 -> :sswitch_32
        0xe517 -> :sswitch_33
        0xe518 -> :sswitch_34
        0xe519 -> :sswitch_35
        0xe51a -> :sswitch_36
        0xe51b -> :sswitch_37
        0xe51c -> :sswitch_38
        0xe51d -> :sswitch_96
        0xe51e -> :sswitch_3f
        0xe51f -> :sswitch_40
        0xe520 -> :sswitch_53
        0xe521 -> :sswitch_50
        0xe522 -> :sswitch_4d
        0xe523 -> :sswitch_4f
        0xe524 -> :sswitch_5c
        0xe525 -> :sswitch_4c
        0xe526 -> :sswitch_46
        0xe527 -> :sswitch_47
        0xe528 -> :sswitch_48
        0xe529 -> :sswitch_49
        0xe52a -> :sswitch_5d
        0xe52b -> :sswitch_5e
        0xe52c -> :sswitch_5f
        0xe52d -> :sswitch_41
        0xe52e -> :sswitch_43
        0xe52f -> :sswitch_44
        0xe530 -> :sswitch_45
        0xe531 -> :sswitch_60
        0xe536 -> :sswitch_61
    .end sparse-switch
.end method

.method public final a(III)I
    .locals 1

    .prologue
    .line 352191
    if-nez p2, :cond_0

    .line 352192
    sparse-switch p1, :sswitch_data_0

    .line 352193
    :goto_0
    move v0, p3

    .line 352194
    return v0

    .line 352195
    :sswitch_0
    const p3, 0x7f020566

    goto :goto_0

    .line 352196
    :sswitch_1
    const p3, 0x7f020565

    goto :goto_0

    .line 352197
    :sswitch_2
    const p3, 0x7f0205c2

    goto :goto_0

    .line 352198
    :sswitch_3
    const p3, 0x7f020568

    goto :goto_0

    .line 352199
    :sswitch_4
    const p3, 0x7f02056c

    goto :goto_0

    .line 352200
    :sswitch_5
    const p3, 0x7f020573

    goto :goto_0

    .line 352201
    :sswitch_6
    const p3, 0x7f020574

    goto :goto_0

    .line 352202
    :sswitch_7
    const p3, 0x7f020575

    goto :goto_0

    .line 352203
    :sswitch_8
    const p3, 0x7f020576

    goto :goto_0

    .line 352204
    :sswitch_9
    const p3, 0x7f020586

    goto :goto_0

    .line 352205
    :sswitch_a
    const p3, 0x7f020563

    goto :goto_0

    .line 352206
    :sswitch_b
    const p3, 0x7f020570

    goto :goto_0

    .line 352207
    :sswitch_c
    const p3, 0x7f02056b

    goto :goto_0

    .line 352208
    :sswitch_d
    const p3, 0x7f02056e

    goto :goto_0

    .line 352209
    :sswitch_e
    const p3, 0x7f020577

    goto :goto_0

    .line 352210
    :sswitch_f
    const p3, 0x7f02057b

    goto :goto_0

    .line 352211
    :sswitch_10
    const p3, 0x7f02057a

    goto :goto_0

    .line 352212
    :sswitch_11
    const p3, 0x7f020564

    goto :goto_0

    .line 352213
    :sswitch_12
    const p3, 0x7f020582

    goto :goto_0

    .line 352214
    :sswitch_13
    const p3, 0x7f020580

    goto :goto_0

    .line 352215
    :sswitch_14
    const p3, 0x7f02057d

    goto :goto_0

    .line 352216
    :sswitch_15
    const p3, 0x7f020583

    goto :goto_0

    .line 352217
    :sswitch_16
    const p3, 0x7f02056f

    goto :goto_0

    .line 352218
    :sswitch_17
    const p3, 0x7f02057f

    goto :goto_0

    .line 352219
    :sswitch_18
    const p3, 0x7f020581

    goto :goto_0

    .line 352220
    :sswitch_19
    const p3, 0x7f02057e

    goto :goto_0

    .line 352221
    :sswitch_1a
    const p3, 0x7f020584

    goto :goto_0

    .line 352222
    :sswitch_1b
    const p3, 0x7f020578

    goto :goto_0

    .line 352223
    :sswitch_1c
    const p3, 0x7f020579

    goto :goto_0

    .line 352224
    :sswitch_1d
    const p3, 0x7f02057c

    goto :goto_0

    .line 352225
    :sswitch_1e
    const p3, 0x7f020572

    goto :goto_0

    .line 352226
    :sswitch_1f
    const p3, 0x7f020567

    goto/16 :goto_0

    .line 352227
    :sswitch_20
    const p3, 0x7f020569

    goto/16 :goto_0

    .line 352228
    :sswitch_21
    const p3, 0x7f02056a

    goto/16 :goto_0

    .line 352229
    :sswitch_22
    const p3, 0x7f020588

    goto/16 :goto_0

    .line 352230
    :sswitch_23
    const p3, 0x7f020587

    goto/16 :goto_0

    .line 352231
    :sswitch_24
    const p3, 0x7f020585

    goto/16 :goto_0

    .line 352232
    :sswitch_25
    const p3, 0x7f020500

    goto/16 :goto_0

    .line 352233
    :sswitch_26
    const p3, 0x7f02056d

    goto/16 :goto_0

    .line 352234
    :sswitch_27
    const p3, 0x7f020571

    goto/16 :goto_0

    .line 352235
    :sswitch_28
    const p3, 0x7f0204f5

    goto/16 :goto_0

    .line 352236
    :sswitch_29
    const p3, 0x7f0204f6

    goto/16 :goto_0

    .line 352237
    :sswitch_2a
    const p3, 0x7f0204f2

    goto/16 :goto_0

    .line 352238
    :sswitch_2b
    const p3, 0x7f0204fa

    goto/16 :goto_0

    .line 352239
    :sswitch_2c
    const p3, 0x7f020502

    goto/16 :goto_0

    .line 352240
    :sswitch_2d
    const p3, 0x7f0204f9

    goto/16 :goto_0

    .line 352241
    :sswitch_2e
    const p3, 0x7f0204ed

    goto/16 :goto_0

    .line 352242
    :sswitch_2f
    const p3, 0x7f0204ee

    goto/16 :goto_0

    .line 352243
    :sswitch_30
    const p3, 0x7f0204ef

    goto/16 :goto_0

    .line 352244
    :sswitch_31
    const p3, 0x7f0204f0

    goto/16 :goto_0

    .line 352245
    :sswitch_32
    const p3, 0x7f0204f7

    goto/16 :goto_0

    .line 352246
    :sswitch_33
    const p3, 0x7f0204f8

    goto/16 :goto_0

    .line 352247
    :sswitch_34
    const p3, 0x7f0204f4

    goto/16 :goto_0

    .line 352248
    :sswitch_35
    const p3, 0x7f0204fd

    goto/16 :goto_0

    .line 352249
    :sswitch_36
    const p3, 0x7f0204fb

    goto/16 :goto_0

    .line 352250
    :sswitch_37
    const p3, 0x7f02058b

    goto/16 :goto_0

    .line 352251
    :sswitch_38
    const p3, 0x7f020589

    goto/16 :goto_0

    .line 352252
    :sswitch_39
    const p3, 0x7f02058c

    goto/16 :goto_0

    .line 352253
    :sswitch_3a
    const p3, 0x7f02058e

    goto/16 :goto_0

    .line 352254
    :sswitch_3b
    const p3, 0x7f02058d

    goto/16 :goto_0

    .line 352255
    :sswitch_3c
    const p3, 0x7f020590

    goto/16 :goto_0

    .line 352256
    :sswitch_3d
    const p3, 0x7f02058f

    goto/16 :goto_0

    .line 352257
    :sswitch_3e
    const p3, 0x7f02058a

    goto/16 :goto_0

    .line 352258
    :sswitch_3f
    const p3, 0x7f020501

    goto/16 :goto_0

    .line 352259
    :sswitch_40
    const p3, 0x7f0204fe

    goto/16 :goto_0

    .line 352260
    :sswitch_41
    const p3, 0x7f020525

    goto/16 :goto_0

    .line 352261
    :sswitch_42
    const p3, 0x7f020553

    goto/16 :goto_0

    .line 352262
    :sswitch_43
    const p3, 0x7f0205de

    goto/16 :goto_0

    .line 352263
    :sswitch_44
    const p3, 0x7f02044c

    goto/16 :goto_0

    .line 352264
    :sswitch_45
    const p3, 0x7f02051f

    goto/16 :goto_0

    .line 352265
    :sswitch_46
    const p3, 0x7f020522

    goto/16 :goto_0

    .line 352266
    :sswitch_47
    const p3, 0x7f020523

    goto/16 :goto_0

    .line 352267
    :sswitch_48
    const p3, 0x7f020521

    goto/16 :goto_0

    .line 352268
    :sswitch_49
    const p3, 0x7f020524

    goto/16 :goto_0

    .line 352269
    :sswitch_4a
    const p3, 0x7f0204d2

    goto/16 :goto_0

    .line 352270
    :sswitch_4b
    const p3, 0x7f0204d1

    goto/16 :goto_0

    .line 352271
    :sswitch_4c
    const p3, 0x7f0204d3

    goto/16 :goto_0

    .line 352272
    :sswitch_4d
    const p3, 0x7f0204d5

    goto/16 :goto_0

    .line 352273
    :sswitch_4e
    const p3, 0x7f0204d4

    goto/16 :goto_0

    .line 352274
    :sswitch_4f
    const p3, 0x7f0204dd

    goto/16 :goto_0

    .line 352275
    :sswitch_50
    const p3, 0x7f0204de

    goto/16 :goto_0

    .line 352276
    :sswitch_51
    const p3, 0x7f0204dc

    goto/16 :goto_0

    .line 352277
    :sswitch_52
    const p3, 0x7f0204da

    goto/16 :goto_0

    .line 352278
    :sswitch_53
    const p3, 0x7f0205da

    goto/16 :goto_0

    .line 352279
    :sswitch_54
    const p3, 0x7f0205dc

    goto/16 :goto_0

    .line 352280
    :sswitch_55
    const p3, 0x7f0204db

    goto/16 :goto_0

    .line 352281
    :sswitch_56
    const p3, 0x7f0205db

    goto/16 :goto_0

    .line 352282
    :sswitch_57
    const p3, 0x7f0204e0

    goto/16 :goto_0

    .line 352283
    :sswitch_58
    const p3, 0x7f0204d6

    goto/16 :goto_0

    .line 352284
    :sswitch_59
    const p3, 0x7f0204d7

    goto/16 :goto_0

    .line 352285
    :sswitch_5a
    const p3, 0x7f0204d9

    goto/16 :goto_0

    .line 352286
    :sswitch_5b
    const p3, 0x7f0204d8

    goto/16 :goto_0

    .line 352287
    :sswitch_5c
    const p3, 0x7f020592

    goto/16 :goto_0

    .line 352288
    :sswitch_5d
    const p3, 0x7f020594

    goto/16 :goto_0

    .line 352289
    :sswitch_5e
    const p3, 0x7f0205c1

    goto/16 :goto_0

    .line 352290
    :sswitch_5f
    const p3, 0x7f0204df

    goto/16 :goto_0

    .line 352291
    :sswitch_60
    const p3, 0x7f020526

    goto/16 :goto_0

    .line 352292
    :sswitch_61
    const p3, 0x7f020503

    goto/16 :goto_0

    .line 352293
    :sswitch_62
    const p3, 0x7f0204f1

    goto/16 :goto_0

    .line 352294
    :sswitch_63
    const p3, 0x7f02050d

    goto/16 :goto_0

    .line 352295
    :sswitch_64
    const p3, 0x7f02050f

    goto/16 :goto_0

    .line 352296
    :sswitch_65
    const p3, 0x7f0204f3

    goto/16 :goto_0

    .line 352297
    :sswitch_66
    const p3, 0x7f020591

    goto/16 :goto_0

    .line 352298
    :sswitch_67
    const p3, 0x7f020505

    goto/16 :goto_0

    .line 352299
    :sswitch_68
    const p3, 0x7f020593

    goto/16 :goto_0

    .line 352300
    :sswitch_69
    const p3, 0x7f02048d

    goto/16 :goto_0

    .line 352301
    :sswitch_6a
    const p3, 0x7f0204e1

    goto/16 :goto_0

    .line 352302
    :sswitch_6b
    const p3, 0x7f0204e2

    goto/16 :goto_0

    .line 352303
    :sswitch_6c
    const p3, 0x7f0204e9

    goto/16 :goto_0

    .line 352304
    :sswitch_6d
    const p3, 0x7f0204eb

    goto/16 :goto_0

    .line 352305
    :sswitch_6e
    const p3, 0x7f0204ea

    goto/16 :goto_0

    .line 352306
    :sswitch_6f
    const p3, 0x7f0204ec

    goto/16 :goto_0

    .line 352307
    :sswitch_70
    const p3, 0x7f0204e4

    goto/16 :goto_0

    .line 352308
    :sswitch_71
    const p3, 0x7f0204e3

    goto/16 :goto_0

    .line 352309
    :sswitch_72
    const p3, 0x7f0204e5

    goto/16 :goto_0

    .line 352310
    :sswitch_73
    const p3, 0x7f0204e6

    goto/16 :goto_0

    .line 352311
    :sswitch_74
    const p3, 0x7f0204e7

    goto/16 :goto_0

    .line 352312
    :sswitch_75
    const p3, 0x7f02052d

    goto/16 :goto_0

    .line 352313
    :sswitch_76
    const p3, 0x7f0204e8

    goto/16 :goto_0

    .line 352314
    :sswitch_77
    const p3, 0x7f020478

    goto/16 :goto_0

    .line 352315
    :sswitch_78
    const p3, 0x7f020443

    goto/16 :goto_0

    .line 352316
    :sswitch_79
    const p3, 0x7f020504

    goto/16 :goto_0

    .line 352317
    :sswitch_7a
    const p3, 0x7f020518

    goto/16 :goto_0

    .line 352318
    :sswitch_7b
    const p3, 0x7f020516

    goto/16 :goto_0

    .line 352319
    :sswitch_7c
    const p3, 0x7f020519

    goto/16 :goto_0

    .line 352320
    :sswitch_7d
    const p3, 0x7f020517

    goto/16 :goto_0

    .line 352321
    :sswitch_7e
    const p3, 0x7f0205e7

    goto/16 :goto_0

    .line 352322
    :sswitch_7f
    const p3, 0x7f020512

    goto/16 :goto_0

    .line 352323
    :sswitch_80
    const p3, 0x7f020514

    goto/16 :goto_0

    .line 352324
    :sswitch_81
    const p3, 0x7f020511

    goto/16 :goto_0

    .line 352325
    :sswitch_82
    const p3, 0x7f020513

    goto/16 :goto_0

    .line 352326
    :sswitch_83
    const p3, 0x7f02051b

    goto/16 :goto_0

    .line 352327
    :sswitch_84
    const p3, 0x7f020515

    goto/16 :goto_0

    .line 352328
    :sswitch_85
    const p3, 0x7f02050a

    goto/16 :goto_0

    .line 352329
    :sswitch_86
    const p3, 0x7f020509

    goto/16 :goto_0

    .line 352330
    :sswitch_87
    const p3, 0x7f02050b

    goto/16 :goto_0

    .line 352331
    :sswitch_88
    const p3, 0x7f02050c

    goto/16 :goto_0

    .line 352332
    :sswitch_89
    const p3, 0x7f0204ca

    goto/16 :goto_0

    .line 352333
    :sswitch_8a
    const p3, 0x7f0204ce

    goto/16 :goto_0

    .line 352334
    :sswitch_8b
    const p3, 0x7f0204c6

    goto/16 :goto_0

    .line 352335
    :sswitch_8c
    const p3, 0x7f0204c2

    goto/16 :goto_0

    .line 352336
    :sswitch_8d
    const p3, 0x7f0204cd

    goto/16 :goto_0

    .line 352337
    :sswitch_8e
    const p3, 0x7f0204c5

    goto/16 :goto_0

    .line 352338
    :sswitch_8f
    const p3, 0x7f0204cc

    goto/16 :goto_0

    .line 352339
    :sswitch_90
    const p3, 0x7f0204c4

    goto/16 :goto_0

    .line 352340
    :sswitch_91
    const p3, 0x7f0204be

    goto/16 :goto_0

    .line 352341
    :sswitch_92
    const p3, 0x7f0204cf

    goto/16 :goto_0

    .line 352342
    :sswitch_93
    const p3, 0x7f0204cb

    goto/16 :goto_0

    .line 352343
    :sswitch_94
    const p3, 0x7f0204c3

    goto/16 :goto_0

    .line 352344
    :sswitch_95
    const p3, 0x7f0204b3

    goto/16 :goto_0

    .line 352345
    :sswitch_96
    const p3, 0x7f0204c9

    goto/16 :goto_0

    .line 352346
    :sswitch_97
    const p3, 0x7f0204b1

    goto/16 :goto_0

    .line 352347
    :sswitch_98
    const p3, 0x7f0204c8

    goto/16 :goto_0

    .line 352348
    :sswitch_99
    const p3, 0x7f0204b0

    goto/16 :goto_0

    .line 352349
    :sswitch_9a
    const p3, 0x7f0204b4

    goto/16 :goto_0

    .line 352350
    :sswitch_9b
    const p3, 0x7f0204bd

    goto/16 :goto_0

    .line 352351
    :sswitch_9c
    const p3, 0x7f0204bc

    goto/16 :goto_0

    .line 352352
    :sswitch_9d
    const p3, 0x7f0204bb

    goto/16 :goto_0

    .line 352353
    :sswitch_9e
    const p3, 0x7f0204b2

    goto/16 :goto_0

    .line 352354
    :sswitch_9f
    const p3, 0x7f0204ae

    goto/16 :goto_0

    .line 352355
    :sswitch_a0
    const p3, 0x7f0204b7

    goto/16 :goto_0

    .line 352356
    :sswitch_a1
    const p3, 0x7f0204b5

    goto/16 :goto_0

    .line 352357
    :sswitch_a2
    const p3, 0x7f0204b6

    goto/16 :goto_0

    .line 352358
    :sswitch_a3
    const p3, 0x7f0204b9

    goto/16 :goto_0

    .line 352359
    :sswitch_a4
    const p3, 0x7f0204b8

    goto/16 :goto_0

    .line 352360
    :sswitch_a5
    const p3, 0x7f0204c1

    goto/16 :goto_0

    .line 352361
    :sswitch_a6
    const p3, 0x7f0204c7

    goto/16 :goto_0

    .line 352362
    :sswitch_a7
    const p3, 0x7f0204af

    goto/16 :goto_0

    .line 352363
    :sswitch_a8
    const p3, 0x7f0204ba

    goto/16 :goto_0

    .line 352364
    :sswitch_a9
    const p3, 0x7f0204c0

    goto/16 :goto_0

    .line 352365
    :sswitch_aa
    const p3, 0x7f0204bf

    goto/16 :goto_0

    .line 352366
    :sswitch_ab
    const p3, 0x7f0204d0

    goto/16 :goto_0

    .line 352367
    :sswitch_ac
    const p3, 0x7f02050e

    goto/16 :goto_0

    .line 352368
    :sswitch_ad
    const p3, 0x7f020451

    goto/16 :goto_0

    .line 352369
    :sswitch_ae
    const p3, 0x7f020450

    goto/16 :goto_0

    .line 352370
    :sswitch_af
    const p3, 0x7f020456

    goto/16 :goto_0

    .line 352371
    :sswitch_b0
    const p3, 0x7f020452

    goto/16 :goto_0

    .line 352372
    :sswitch_b1
    const p3, 0x7f020454

    goto/16 :goto_0

    .line 352373
    :sswitch_b2
    const p3, 0x7f020453

    goto/16 :goto_0

    .line 352374
    :sswitch_b3
    const p3, 0x7f020457

    goto/16 :goto_0

    .line 352375
    :sswitch_b4
    const p3, 0x7f020459

    goto/16 :goto_0

    .line 352376
    :sswitch_b5
    const p3, 0x7f020458

    goto/16 :goto_0

    .line 352377
    :sswitch_b6
    const p3, 0x7f020455

    goto/16 :goto_0

    .line 352378
    :sswitch_b7
    const p3, 0x7f02044f

    goto/16 :goto_0

    .line 352379
    :sswitch_b8
    const p3, 0x7f02044e

    goto/16 :goto_0

    .line 352380
    :sswitch_b9
    const p3, 0x7f02044d

    goto/16 :goto_0

    .line 352381
    :sswitch_ba
    const p3, 0x7f02044b

    goto/16 :goto_0

    .line 352382
    :sswitch_bb
    const p3, 0x7f0205f1

    goto/16 :goto_0

    .line 352383
    :sswitch_bc
    const p3, 0x7f0205bc

    goto/16 :goto_0

    .line 352384
    :sswitch_bd
    const p3, 0x7f0205bd

    goto/16 :goto_0

    .line 352385
    :sswitch_be
    const p3, 0x7f0205ca

    goto/16 :goto_0

    .line 352386
    :sswitch_bf
    const p3, 0x7f0205bf

    goto/16 :goto_0

    .line 352387
    :sswitch_c0
    const p3, 0x7f0205cf

    goto/16 :goto_0

    .line 352388
    :sswitch_c1
    const p3, 0x7f020442

    goto/16 :goto_0

    .line 352389
    :sswitch_c2
    const p3, 0x7f020449

    goto/16 :goto_0

    .line 352390
    :sswitch_c3
    const p3, 0x7f02044a

    goto/16 :goto_0

    .line 352391
    :sswitch_c4
    const p3, 0x7f020480

    goto/16 :goto_0

    .line 352392
    :sswitch_c5
    const p3, 0x7f02051a

    goto/16 :goto_0

    .line 352393
    :sswitch_c6
    const p3, 0x7f020481

    goto/16 :goto_0

    .line 352394
    :sswitch_c7
    const p3, 0x7f020484

    goto/16 :goto_0

    .line 352395
    :sswitch_c8
    const p3, 0x7f020485

    goto/16 :goto_0

    .line 352396
    :sswitch_c9
    const p3, 0x7f020482

    goto/16 :goto_0

    .line 352397
    :sswitch_ca
    const p3, 0x7f020483

    goto/16 :goto_0

    .line 352398
    :sswitch_cb
    const p3, 0x7f02047a

    goto/16 :goto_0

    .line 352399
    :sswitch_cc
    const p3, 0x7f0204fc

    goto/16 :goto_0

    .line 352400
    :sswitch_cd
    const p3, 0x7f02047c

    goto/16 :goto_0

    .line 352401
    :sswitch_ce
    const p3, 0x7f02047b

    goto/16 :goto_0

    .line 352402
    :sswitch_cf
    const p3, 0x7f020479

    goto/16 :goto_0

    .line 352403
    :sswitch_d0
    const p3, 0x7f02047e

    goto/16 :goto_0

    .line 352404
    :sswitch_d1
    const p3, 0x7f02047d

    goto/16 :goto_0

    .line 352405
    :sswitch_d2
    const p3, 0x7f02047f

    goto/16 :goto_0

    .line 352406
    :sswitch_d3
    const p3, 0x7f020489

    goto/16 :goto_0

    .line 352407
    :sswitch_d4
    const p3, 0x7f020545

    goto/16 :goto_0

    .line 352408
    :sswitch_d5
    const p3, 0x7f020548

    goto/16 :goto_0

    .line 352409
    :sswitch_d6
    const p3, 0x7f020530

    goto/16 :goto_0

    .line 352410
    :sswitch_d7
    const p3, 0x7f020531

    goto/16 :goto_0

    .line 352411
    :sswitch_d8
    const p3, 0x7f02052e

    goto/16 :goto_0

    .line 352412
    :sswitch_d9
    const p3, 0x7f02052f

    goto/16 :goto_0

    .line 352413
    :sswitch_da
    const p3, 0x7f02052c

    goto/16 :goto_0

    .line 352414
    :sswitch_db
    const p3, 0x7f020540

    goto/16 :goto_0

    .line 352415
    :sswitch_dc
    const p3, 0x7f0205be

    goto/16 :goto_0

    .line 352416
    :sswitch_dd
    const p3, 0x7f020534

    goto/16 :goto_0

    .line 352417
    :sswitch_de
    const p3, 0x7f020535

    goto/16 :goto_0

    .line 352418
    :sswitch_df
    const p3, 0x7f020536

    goto/16 :goto_0

    .line 352419
    :sswitch_e0
    const p3, 0x7f020546

    goto/16 :goto_0

    .line 352420
    :sswitch_e1
    const p3, 0x7f020547

    goto/16 :goto_0

    .line 352421
    :sswitch_e2
    const p3, 0x7f020549

    goto/16 :goto_0

    .line 352422
    :sswitch_e3
    const p3, 0x7f020551

    goto/16 :goto_0

    .line 352423
    :sswitch_e4
    const p3, 0x7f020537

    goto/16 :goto_0

    .line 352424
    :sswitch_e5
    const p3, 0x7f020538

    goto/16 :goto_0

    .line 352425
    :sswitch_e6
    const p3, 0x7f020550

    goto/16 :goto_0

    .line 352426
    :sswitch_e7
    const p3, 0x7f02054f

    goto/16 :goto_0

    .line 352427
    :sswitch_e8
    const p3, 0x7f02054c

    goto/16 :goto_0

    .line 352428
    :sswitch_e9
    const p3, 0x7f02054d

    goto/16 :goto_0

    .line 352429
    :sswitch_ea
    const p3, 0x7f02054e

    goto/16 :goto_0

    .line 352430
    :sswitch_eb
    const p3, 0x7f02054b

    goto/16 :goto_0

    .line 352431
    :sswitch_ec
    const p3, 0x7f02051e

    goto/16 :goto_0

    .line 352432
    :sswitch_ed
    const p3, 0x7f02054a

    goto/16 :goto_0

    .line 352433
    :sswitch_ee
    const p3, 0x7f0205b0

    goto/16 :goto_0

    .line 352434
    :sswitch_ef
    const p3, 0x7f0205ae

    goto/16 :goto_0

    .line 352435
    :sswitch_f0
    const p3, 0x7f020554

    goto/16 :goto_0

    .line 352436
    :sswitch_f1
    const p3, 0x7f0205a7

    goto/16 :goto_0

    .line 352437
    :sswitch_f2
    const p3, 0x7f020520

    goto/16 :goto_0

    .line 352438
    :sswitch_f3
    const p3, 0x7f020555

    goto/16 :goto_0

    .line 352439
    :sswitch_f4
    const p3, 0x7f020508

    goto/16 :goto_0

    .line 352440
    :sswitch_f5
    const p3, 0x7f020507

    goto/16 :goto_0

    .line 352441
    :sswitch_f6
    const p3, 0x7f020527

    goto/16 :goto_0

    .line 352442
    :sswitch_f7
    const p3, 0x7f020529

    goto/16 :goto_0

    .line 352443
    :sswitch_f8
    const p3, 0x7f02052a

    goto/16 :goto_0

    .line 352444
    :sswitch_f9
    const p3, 0x7f020541

    goto/16 :goto_0

    .line 352445
    :sswitch_fa
    const p3, 0x7f0205d9

    goto/16 :goto_0

    .line 352446
    :sswitch_fb
    const p3, 0x7f02053a

    goto/16 :goto_0

    .line 352447
    :sswitch_fc
    const p3, 0x7f020539

    goto/16 :goto_0

    .line 352448
    :sswitch_fd
    const p3, 0x7f02053c

    goto/16 :goto_0

    .line 352449
    :sswitch_fe
    const p3, 0x7f02053b

    goto/16 :goto_0

    .line 352450
    :sswitch_ff
    const p3, 0x7f02053d

    goto/16 :goto_0

    .line 352451
    :sswitch_100
    const p3, 0x7f02053e

    goto/16 :goto_0

    .line 352452
    :sswitch_101
    const p3, 0x7f02053f

    goto/16 :goto_0

    .line 352453
    :sswitch_102
    const p3, 0x7f020533

    goto/16 :goto_0

    .line 352454
    :sswitch_103
    const p3, 0x7f0205d7

    goto/16 :goto_0

    .line 352455
    :sswitch_104
    const p3, 0x7f020532

    goto/16 :goto_0

    .line 352456
    :sswitch_105
    const p3, 0x7f02048c

    goto/16 :goto_0

    .line 352457
    :sswitch_106
    const p3, 0x7f02048f

    goto/16 :goto_0

    .line 352458
    :sswitch_107
    const p3, 0x7f020488

    goto/16 :goto_0

    .line 352459
    :sswitch_108
    const p3, 0x7f02048b

    goto/16 :goto_0

    .line 352460
    :sswitch_109
    const p3, 0x7f020498

    goto/16 :goto_0

    .line 352461
    :sswitch_10a
    const p3, 0x7f020493

    goto/16 :goto_0

    .line 352462
    :sswitch_10b
    const p3, 0x7f020494

    goto/16 :goto_0

    .line 352463
    :sswitch_10c
    const p3, 0x7f020497

    goto/16 :goto_0

    .line 352464
    :sswitch_10d
    const p3, 0x7f020495

    goto/16 :goto_0

    .line 352465
    :sswitch_10e
    const p3, 0x7f020496

    goto/16 :goto_0

    .line 352466
    :sswitch_10f
    const p3, 0x7f0204ff

    goto/16 :goto_0

    .line 352467
    :sswitch_110
    const p3, 0x7f020428

    goto/16 :goto_0

    .line 352468
    :sswitch_111
    const p3, 0x7f020491

    goto/16 :goto_0

    .line 352469
    :sswitch_112
    const p3, 0x7f02049e

    goto/16 :goto_0

    .line 352470
    :sswitch_113
    const p3, 0x7f02049b

    goto/16 :goto_0

    .line 352471
    :sswitch_114
    const p3, 0x7f0205cd

    goto/16 :goto_0

    .line 352472
    :sswitch_115
    const p3, 0x7f0205ce

    goto/16 :goto_0

    .line 352473
    :sswitch_116
    const p3, 0x7f020499

    goto/16 :goto_0

    .line 352474
    :sswitch_117
    const p3, 0x7f020492

    goto/16 :goto_0

    .line 352475
    :sswitch_118
    const p3, 0x7f0205d3

    goto/16 :goto_0

    .line 352476
    :sswitch_119
    const p3, 0x7f02049c

    goto/16 :goto_0

    .line 352477
    :sswitch_11a
    const p3, 0x7f02049d

    goto/16 :goto_0

    .line 352478
    :sswitch_11b
    const p3, 0x7f02049a

    goto/16 :goto_0

    .line 352479
    :sswitch_11c
    const p3, 0x7f0205c0

    goto/16 :goto_0

    .line 352480
    :sswitch_11d
    const p3, 0x7f020473

    goto/16 :goto_0

    .line 352481
    :sswitch_11e
    const p3, 0x7f020474

    goto/16 :goto_0

    .line 352482
    :sswitch_11f
    const p3, 0x7f020476

    goto/16 :goto_0

    .line 352483
    :sswitch_120
    const p3, 0x7f020477

    goto/16 :goto_0

    .line 352484
    :sswitch_121
    const p3, 0x7f020475

    goto/16 :goto_0

    .line 352485
    :sswitch_122
    const p3, 0x7f020472

    goto/16 :goto_0

    .line 352486
    :sswitch_123
    const p3, 0x7f020460

    goto/16 :goto_0

    .line 352487
    :sswitch_124
    const p3, 0x7f020468

    goto/16 :goto_0

    .line 352488
    :sswitch_125
    const p3, 0x7f020466

    goto/16 :goto_0

    .line 352489
    :sswitch_126
    const p3, 0x7f020464

    goto/16 :goto_0

    .line 352490
    :sswitch_127
    const p3, 0x7f02046f

    goto/16 :goto_0

    .line 352491
    :sswitch_128
    const p3, 0x7f02046b

    goto/16 :goto_0

    .line 352492
    :sswitch_129
    const p3, 0x7f020462

    goto/16 :goto_0

    .line 352493
    :sswitch_12a
    const p3, 0x7f020461

    goto/16 :goto_0

    .line 352494
    :sswitch_12b
    const p3, 0x7f020463

    goto/16 :goto_0

    .line 352495
    :sswitch_12c
    const p3, 0x7f020465

    goto/16 :goto_0

    .line 352496
    :sswitch_12d
    const p3, 0x7f020470

    goto/16 :goto_0

    .line 352497
    :sswitch_12e
    const p3, 0x7f02046a

    goto/16 :goto_0

    .line 352498
    :sswitch_12f
    const p3, 0x7f020469

    goto/16 :goto_0

    .line 352499
    :sswitch_130
    const p3, 0x7f020471

    goto/16 :goto_0

    .line 352500
    :sswitch_131
    const p3, 0x7f020467

    goto/16 :goto_0

    .line 352501
    :sswitch_132
    const p3, 0x7f02046c

    goto/16 :goto_0

    .line 352502
    :sswitch_133
    const p3, 0x7f02046d

    goto/16 :goto_0

    .line 352503
    :sswitch_134
    const p3, 0x7f02046e

    goto/16 :goto_0

    .line 352504
    :sswitch_135
    const p3, 0x7f02045e

    goto/16 :goto_0

    .line 352505
    :sswitch_136
    const p3, 0x7f02045d

    goto/16 :goto_0

    .line 352506
    :sswitch_137
    const p3, 0x7f02045c

    goto/16 :goto_0

    .line 352507
    :sswitch_138
    const p3, 0x7f02045f

    goto/16 :goto_0

    .line 352508
    :sswitch_139
    const p3, 0x7f02045b

    goto/16 :goto_0

    .line 352509
    :sswitch_13a
    const p3, 0x7f02045a

    goto/16 :goto_0

    .line 352510
    :sswitch_13b
    const p3, 0x7f02049f

    goto/16 :goto_0

    .line 352511
    :sswitch_13c
    const p3, 0x7f0204a0

    goto/16 :goto_0

    .line 352512
    :sswitch_13d
    const p3, 0x7f0204a9

    goto/16 :goto_0

    .line 352513
    :sswitch_13e
    const p3, 0x7f0204a1

    goto/16 :goto_0

    .line 352514
    :sswitch_13f
    const p3, 0x7f0204a2

    goto/16 :goto_0

    .line 352515
    :sswitch_140
    const p3, 0x7f0204a3

    goto/16 :goto_0

    .line 352516
    :sswitch_141
    const p3, 0x7f0204a4

    goto/16 :goto_0

    .line 352517
    :sswitch_142
    const p3, 0x7f0204a8

    goto/16 :goto_0

    .line 352518
    :sswitch_143
    const p3, 0x7f0204a7

    goto/16 :goto_0

    .line 352519
    :sswitch_144
    const p3, 0x7f0204a6

    goto/16 :goto_0

    .line 352520
    :sswitch_145
    const p3, 0x7f020510

    goto/16 :goto_0

    .line 352521
    :sswitch_146
    const p3, 0x7f0205d1

    goto/16 :goto_0

    .line 352522
    :sswitch_147
    const p3, 0x7f0204aa

    goto/16 :goto_0

    .line 352523
    :sswitch_148
    const p3, 0x7f020448

    goto/16 :goto_0

    .line 352524
    :sswitch_149
    const p3, 0x7f020447

    goto/16 :goto_0

    .line 352525
    :sswitch_14a
    const p3, 0x7f0204ac

    goto/16 :goto_0

    .line 352526
    :sswitch_14b
    const p3, 0x7f0204ad

    goto/16 :goto_0

    .line 352527
    :sswitch_14c
    const p3, 0x7f0205d5

    goto/16 :goto_0

    .line 352528
    :sswitch_14d
    const p3, 0x7f0204ab

    goto/16 :goto_0

    .line 352529
    :sswitch_14e
    const p3, 0x7f020561

    goto/16 :goto_0

    .line 352530
    :sswitch_14f
    const p3, 0x7f020560

    goto/16 :goto_0

    .line 352531
    :sswitch_150
    const p3, 0x7f020445

    goto/16 :goto_0

    .line 352532
    :sswitch_151
    const p3, 0x7f020446

    goto/16 :goto_0

    .line 352533
    :sswitch_152
    const p3, 0x7f020444

    goto/16 :goto_0

    .line 352534
    :sswitch_153
    const p3, 0x7f020562

    goto/16 :goto_0

    .line 352535
    :sswitch_154
    const p3, 0x7f020486

    goto/16 :goto_0

    .line 352536
    :sswitch_155
    const p3, 0x7f0205d2

    goto/16 :goto_0

    .line 352537
    :sswitch_156
    const p3, 0x7f020487

    goto/16 :goto_0

    .line 352538
    :sswitch_157
    const p3, 0x7f0205a3

    goto/16 :goto_0

    .line 352539
    :sswitch_158
    const p3, 0x7f0205d4

    goto/16 :goto_0

    .line 352540
    :sswitch_159
    const p3, 0x7f0205a4

    goto/16 :goto_0

    .line 352541
    :sswitch_15a
    const p3, 0x7f020595

    goto/16 :goto_0

    .line 352542
    :sswitch_15b
    const p3, 0x7f0205d8

    goto/16 :goto_0

    .line 352543
    :sswitch_15c
    const p3, 0x7f02052b

    goto/16 :goto_0

    .line 352544
    :sswitch_15d
    const p3, 0x7f020599

    goto/16 :goto_0

    .line 352545
    :sswitch_15e
    const p3, 0x7f020597

    goto/16 :goto_0

    .line 352546
    :sswitch_15f
    const p3, 0x7f020598

    goto/16 :goto_0

    .line 352547
    :sswitch_160
    const p3, 0x7f020596

    goto/16 :goto_0

    .line 352548
    :sswitch_161
    const p3, 0x7f02059a

    goto/16 :goto_0

    .line 352549
    :sswitch_162
    const p3, 0x7f0205a1

    goto/16 :goto_0

    .line 352550
    :sswitch_163
    const p3, 0x7f0205a0

    goto/16 :goto_0

    .line 352551
    :sswitch_164
    const p3, 0x7f02059f

    goto/16 :goto_0

    .line 352552
    :sswitch_165
    const p3, 0x7f0205a2

    goto/16 :goto_0

    .line 352553
    :sswitch_166
    const p3, 0x7f02059e

    goto/16 :goto_0

    .line 352554
    :sswitch_167
    const p3, 0x7f02059d

    goto/16 :goto_0

    .line 352555
    :sswitch_168
    const p3, 0x7f02059c

    goto/16 :goto_0

    .line 352556
    :sswitch_169
    const p3, 0x7f0205a9

    goto/16 :goto_0

    .line 352557
    :sswitch_16a
    const p3, 0x7f020506

    goto/16 :goto_0

    .line 352558
    :sswitch_16b
    const p3, 0x7f02059b

    goto/16 :goto_0

    .line 352559
    :sswitch_16c
    const p3, 0x7f02048e

    goto/16 :goto_0

    .line 352560
    :sswitch_16d
    const p3, 0x7f0205a5

    goto/16 :goto_0

    .line 352561
    :sswitch_16e
    const p3, 0x7f0205c9

    goto/16 :goto_0

    .line 352562
    :sswitch_16f
    const p3, 0x7f0205a6

    goto/16 :goto_0

    .line 352563
    :sswitch_170
    const p3, 0x7f020556

    goto/16 :goto_0

    .line 352564
    :sswitch_171
    const p3, 0x7f0205d6

    goto/16 :goto_0

    .line 352565
    :sswitch_172
    const p3, 0x7f0205c7

    goto/16 :goto_0

    .line 352566
    :sswitch_173
    const p3, 0x7f020490

    goto/16 :goto_0

    .line 352567
    :sswitch_174
    const p3, 0x7f0205ed

    goto/16 :goto_0

    .line 352568
    :sswitch_175
    const p3, 0x7f0205ee

    goto/16 :goto_0

    .line 352569
    :sswitch_176
    const p3, 0x7f0205ec

    goto/16 :goto_0

    .line 352570
    :sswitch_177
    const p3, 0x7f0205e8

    goto/16 :goto_0

    .line 352571
    :sswitch_178
    const p3, 0x7f0205b2

    goto/16 :goto_0

    .line 352572
    :sswitch_179
    const p3, 0x7f0205b1

    goto/16 :goto_0

    .line 352573
    :sswitch_17a
    const p3, 0x7f0205b3

    goto/16 :goto_0

    .line 352574
    :sswitch_17b
    const p3, 0x7f0205b4

    goto/16 :goto_0

    .line 352575
    :sswitch_17c
    const p3, 0x7f0205eb

    goto/16 :goto_0

    .line 352576
    :sswitch_17d
    const p3, 0x7f0205ea

    goto/16 :goto_0

    .line 352577
    :sswitch_17e
    const p3, 0x7f02042a

    goto/16 :goto_0

    .line 352578
    :sswitch_17f
    const p3, 0x7f020429

    goto/16 :goto_0

    .line 352579
    :sswitch_180
    const p3, 0x7f020544

    goto/16 :goto_0

    .line 352580
    :sswitch_181
    const p3, 0x7f02048a

    goto/16 :goto_0

    .line 352581
    :sswitch_182
    const p3, 0x7f020436

    goto/16 :goto_0

    .line 352582
    :sswitch_183
    const p3, 0x7f020439

    goto/16 :goto_0

    .line 352583
    :sswitch_184
    const p3, 0x7f02043a

    goto/16 :goto_0

    .line 352584
    :sswitch_185
    const p3, 0x7f02043b

    goto/16 :goto_0

    .line 352585
    :sswitch_186
    const p3, 0x7f020441

    goto/16 :goto_0

    .line 352586
    :sswitch_187
    const p3, 0x7f02043f

    goto/16 :goto_0

    .line 352587
    :sswitch_188
    const p3, 0x7f020440

    goto/16 :goto_0

    .line 352588
    :sswitch_189
    const p3, 0x7f02043c

    goto/16 :goto_0

    .line 352589
    :sswitch_18a
    const p3, 0x7f020438

    goto/16 :goto_0

    .line 352590
    :sswitch_18b
    const p3, 0x7f0205ac

    goto/16 :goto_0

    .line 352591
    :sswitch_18c
    const p3, 0x7f0205aa

    goto/16 :goto_0

    .line 352592
    :sswitch_18d
    const p3, 0x7f0205ab

    goto/16 :goto_0

    .line 352593
    :sswitch_18e
    const p3, 0x7f0205ad

    goto/16 :goto_0

    .line 352594
    :sswitch_18f
    const p3, 0x7f0205af

    goto/16 :goto_0

    .line 352595
    :sswitch_190
    const p3, 0x7f0205c8

    goto/16 :goto_0

    .line 352596
    :sswitch_191
    const p3, 0x7f0205a8

    goto/16 :goto_0

    .line 352597
    :sswitch_192
    const p3, 0x7f02043d

    goto/16 :goto_0

    .line 352598
    :sswitch_193
    const p3, 0x7f02043e

    goto/16 :goto_0

    .line 352599
    :sswitch_194
    const p3, 0x7f020437

    goto/16 :goto_0

    .line 352600
    :sswitch_195
    const p3, 0x7f0205f7

    goto/16 :goto_0

    .line 352601
    :sswitch_196
    const p3, 0x7f0205f6

    goto/16 :goto_0

    .line 352602
    :sswitch_197
    const p3, 0x7f020552

    goto/16 :goto_0

    .line 352603
    :sswitch_198
    const p3, 0x7f0205d0

    goto/16 :goto_0

    .line 352604
    :sswitch_199
    const p3, 0x7f0205df

    goto/16 :goto_0

    .line 352605
    :sswitch_19a
    const p3, 0x7f0205e2

    goto/16 :goto_0

    .line 352606
    :sswitch_19b
    const p3, 0x7f0205e0

    goto/16 :goto_0

    .line 352607
    :sswitch_19c
    const p3, 0x7f02051c

    goto/16 :goto_0

    .line 352608
    :sswitch_19d
    const p3, 0x7f02042b

    goto/16 :goto_0

    .line 352609
    :sswitch_19e
    const p3, 0x7f020542

    goto/16 :goto_0

    .line 352610
    :sswitch_19f
    const p3, 0x7f020543

    goto/16 :goto_0

    .line 352611
    :sswitch_1a0
    const p3, 0x7f02051d

    goto/16 :goto_0

    .line 352612
    :sswitch_1a1
    const p3, 0x7f0205e9

    goto/16 :goto_0

    .line 352613
    :sswitch_1a2
    const p3, 0x7f0204a5

    goto/16 :goto_0

    .line 352614
    :sswitch_1a3
    const p3, 0x7f020528

    goto/16 :goto_0

    .line 352615
    :sswitch_1a4
    const p3, 0x7f0205f3

    goto/16 :goto_0

    .line 352616
    :sswitch_1a5
    const p3, 0x7f0205e1

    goto/16 :goto_0

    .line 352617
    :sswitch_1a6
    const p3, 0x7f0205f2

    goto/16 :goto_0

    .line 352618
    :sswitch_1a7
    const p3, 0x7f0205e6

    goto/16 :goto_0

    .line 352619
    :sswitch_1a8
    const p3, 0x7f0205e3

    goto/16 :goto_0

    .line 352620
    :sswitch_1a9
    const p3, 0x7f0205e5

    goto/16 :goto_0

    .line 352621
    :sswitch_1aa
    const p3, 0x7f0205e4

    goto/16 :goto_0

    .line 352622
    :sswitch_1ab
    const p3, 0x7f0205dd

    goto/16 :goto_0

    .line 352623
    :sswitch_1ac
    const p3, 0x7f0205c3

    goto/16 :goto_0

    .line 352624
    :sswitch_1ad
    const p3, 0x7f0205c5

    goto/16 :goto_0

    .line 352625
    :sswitch_1ae
    const p3, 0x7f0205c4

    goto/16 :goto_0

    .line 352626
    :sswitch_1af
    const p3, 0x7f0205c6

    goto/16 :goto_0

    .line 352627
    :sswitch_1b0
    const p3, 0x7f020557

    goto/16 :goto_0

    .line 352628
    :sswitch_1b1
    const p3, 0x7f020558

    goto/16 :goto_0

    .line 352629
    :sswitch_1b2
    const p3, 0x7f020559

    goto/16 :goto_0

    .line 352630
    :sswitch_1b3
    const p3, 0x7f0205b9

    goto/16 :goto_0

    .line 352631
    :sswitch_1b4
    const p3, 0x7f0205b8

    goto/16 :goto_0

    .line 352632
    :sswitch_1b5
    const p3, 0x7f0205bb

    goto/16 :goto_0

    .line 352633
    :sswitch_1b6
    const p3, 0x7f0205ba

    goto/16 :goto_0

    .line 352634
    :sswitch_1b7
    const p3, 0x7f0205b6

    goto/16 :goto_0

    .line 352635
    :sswitch_1b8
    const p3, 0x7f0205b7

    goto/16 :goto_0

    .line 352636
    :sswitch_1b9
    const p3, 0x7f0205f0

    goto/16 :goto_0

    .line 352637
    :sswitch_1ba
    const p3, 0x7f0205ef

    goto/16 :goto_0

    .line 352638
    :sswitch_1bb
    const p3, 0x7f0205cc

    goto/16 :goto_0

    .line 352639
    :sswitch_1bc
    const p3, 0x7f0205cb

    goto/16 :goto_0

    .line 352640
    :sswitch_1bd
    const p3, 0x7f02055a

    goto/16 :goto_0

    .line 352641
    :sswitch_1be
    const p3, 0x7f02055b

    goto/16 :goto_0

    .line 352642
    :sswitch_1bf
    const p3, 0x7f02055c

    goto/16 :goto_0

    .line 352643
    :sswitch_1c0
    const p3, 0x7f02055d

    goto/16 :goto_0

    .line 352644
    :sswitch_1c1
    const p3, 0x7f02055e

    goto/16 :goto_0

    .line 352645
    :sswitch_1c2
    const p3, 0x7f02055f

    goto/16 :goto_0

    .line 352646
    :sswitch_1c3
    const p3, 0x7f02060d

    goto/16 :goto_0

    .line 352647
    :sswitch_1c4
    const p3, 0x7f02060b

    goto/16 :goto_0

    .line 352648
    :sswitch_1c5
    const p3, 0x7f02060c

    goto/16 :goto_0

    .line 352649
    :cond_0
    sparse-switch p1, :sswitch_data_1

    goto/16 :goto_0

    .line 352650
    :sswitch_1c6
    packed-switch p2, :pswitch_data_0

    goto/16 :goto_0

    .line 352651
    :pswitch_0
    const p3, 0x7f0205b5

    goto/16 :goto_0

    .line 352652
    :sswitch_1c7
    packed-switch p2, :pswitch_data_1

    goto/16 :goto_0

    .line 352653
    :pswitch_1
    const p3, 0x7f02042d

    goto/16 :goto_0

    .line 352654
    :sswitch_1c8
    packed-switch p2, :pswitch_data_2

    goto/16 :goto_0

    .line 352655
    :pswitch_2
    const p3, 0x7f02042c

    goto/16 :goto_0

    .line 352656
    :sswitch_1c9
    packed-switch p2, :pswitch_data_3

    goto/16 :goto_0

    .line 352657
    :pswitch_3
    const p3, 0x7f02042f

    goto/16 :goto_0

    .line 352658
    :sswitch_1ca
    packed-switch p2, :pswitch_data_4

    goto/16 :goto_0

    .line 352659
    :pswitch_4
    const p3, 0x7f02042e

    goto/16 :goto_0

    .line 352660
    :sswitch_1cb
    packed-switch p2, :pswitch_data_5

    goto/16 :goto_0

    .line 352661
    :pswitch_5
    const p3, 0x7f020430

    goto/16 :goto_0

    .line 352662
    :sswitch_1cc
    packed-switch p2, :pswitch_data_6

    goto/16 :goto_0

    .line 352663
    :pswitch_6
    const p3, 0x7f020432

    goto/16 :goto_0

    .line 352664
    :sswitch_1cd
    packed-switch p2, :pswitch_data_7

    goto/16 :goto_0

    .line 352665
    :pswitch_7
    const p3, 0x7f020431

    goto/16 :goto_0

    .line 352666
    :sswitch_1ce
    packed-switch p2, :pswitch_data_8

    goto/16 :goto_0

    .line 352667
    :pswitch_8
    const p3, 0x7f0205f4

    goto/16 :goto_0

    .line 352668
    :sswitch_1cf
    packed-switch p2, :pswitch_data_9

    goto/16 :goto_0

    .line 352669
    :pswitch_9
    const p3, 0x7f020433

    goto/16 :goto_0

    .line 352670
    :sswitch_1d0
    packed-switch p2, :pswitch_data_a

    goto/16 :goto_0

    .line 352671
    :pswitch_a
    const p3, 0x7f0205f5

    goto/16 :goto_0

    .line 352672
    :sswitch_1d1
    packed-switch p2, :pswitch_data_b

    goto/16 :goto_0

    .line 352673
    :pswitch_b
    const p3, 0x7f0205f8

    goto/16 :goto_0

    .line 352674
    :sswitch_1d2
    packed-switch p2, :pswitch_data_c

    goto/16 :goto_0

    .line 352675
    :pswitch_c
    const p3, 0x7f0205f9

    goto/16 :goto_0

    .line 352676
    :sswitch_1d3
    packed-switch p2, :pswitch_data_d

    goto/16 :goto_0

    .line 352677
    :pswitch_d
    const p3, 0x7f0205fa

    goto/16 :goto_0

    .line 352678
    :sswitch_1d4
    packed-switch p2, :pswitch_data_e

    goto/16 :goto_0

    .line 352679
    :pswitch_e
    const p3, 0x7f0205fb

    goto/16 :goto_0

    .line 352680
    :sswitch_1d5
    packed-switch p2, :pswitch_data_f

    goto/16 :goto_0

    .line 352681
    :pswitch_f
    const p3, 0x7f020434

    goto/16 :goto_0

    .line 352682
    :sswitch_1d6
    packed-switch p2, :pswitch_data_10

    goto/16 :goto_0

    .line 352683
    :pswitch_10
    const p3, 0x7f0205fc

    goto/16 :goto_0

    .line 352684
    :sswitch_1d7
    packed-switch p2, :pswitch_data_11

    goto/16 :goto_0

    .line 352685
    :pswitch_11
    const p3, 0x7f0205fd

    goto/16 :goto_0

    .line 352686
    :sswitch_1d8
    packed-switch p2, :pswitch_data_12

    goto/16 :goto_0

    .line 352687
    :pswitch_12
    const p3, 0x7f0205fe

    goto/16 :goto_0

    .line 352688
    :sswitch_1d9
    packed-switch p2, :pswitch_data_13

    goto/16 :goto_0

    .line 352689
    :pswitch_13
    const p3, 0x7f0205ff

    goto/16 :goto_0

    .line 352690
    :sswitch_1da
    packed-switch p2, :pswitch_data_14

    goto/16 :goto_0

    .line 352691
    :pswitch_14
    const p3, 0x7f020435

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2196 -> :sswitch_179
        0x2197 -> :sswitch_178
        0x2198 -> :sswitch_17a
        0x2199 -> :sswitch_17b
        0x25aa -> :sswitch_1b7
        0x25ab -> :sswitch_1b8
        0x25fb -> :sswitch_1b4
        0x25fc -> :sswitch_1b3
        0x25fd -> :sswitch_1b6
        0x25fe -> :sswitch_1b5
        0x2600 -> :sswitch_bc
        0x2601 -> :sswitch_bd
        0x260e -> :sswitch_dc
        0x2614 -> :sswitch_bf
        0x2615 -> :sswitch_11c
        0x261d -> :sswitch_5e
        0x263a -> :sswitch_2
        0x2660 -> :sswitch_1ac
        0x2663 -> :sswitch_1ae
        0x2665 -> :sswitch_1ad
        0x2666 -> :sswitch_1af
        0x2668 -> :sswitch_172
        0x267f -> :sswitch_190
        0x26a0 -> :sswitch_16e
        0x26a1 -> :sswitch_be
        0x26aa -> :sswitch_1bc
        0x26ab -> :sswitch_1bb
        0x26bd -> :sswitch_114
        0x26be -> :sswitch_115
        0x26c4 -> :sswitch_c0
        0x26d4 -> :sswitch_198
        0x26ea -> :sswitch_146
        0x26f2 -> :sswitch_155
        0x26f3 -> :sswitch_118
        0x26f5 -> :sswitch_158
        0x26fa -> :sswitch_14c
        0x26fd -> :sswitch_171
        0x2702 -> :sswitch_103
        0x2708 -> :sswitch_15b
        0x2709 -> :sswitch_fa
        0x270a -> :sswitch_53
        0x270b -> :sswitch_56
        0x270c -> :sswitch_54
        0x2716 -> :sswitch_1ab
        0x2728 -> :sswitch_43
        0x2733 -> :sswitch_199
        0x2734 -> :sswitch_19b
        0x274c -> :sswitch_1a5
        0x274e -> :sswitch_19a
        0x2753 -> :sswitch_1a8
        0x2754 -> :sswitch_1aa
        0x2755 -> :sswitch_1a9
        0x2757 -> :sswitch_1a7
        0x2764 -> :sswitch_7e
        0x27a1 -> :sswitch_177
        0x27bf -> :sswitch_1a1
        0x2934 -> :sswitch_17d
        0x2935 -> :sswitch_17c
        0x2b05 -> :sswitch_176
        0x2b06 -> :sswitch_174
        0x2b07 -> :sswitch_175
        0x2b1b -> :sswitch_1ba
        0x2b1c -> :sswitch_1b9
        0x2b50 -> :sswitch_bb
        0x2b55 -> :sswitch_1a6
        0x303d -> :sswitch_1a4
        0x3297 -> :sswitch_196
        0x3299 -> :sswitch_195
        0x1f004 -> :sswitch_110
        0x1f192 -> :sswitch_17f
        0x1f199 -> :sswitch_17e
        0x1f19a -> :sswitch_19d
        0x1f201 -> :sswitch_182
        0x1f202 -> :sswitch_194
        0x1f21a -> :sswitch_18a
        0x1f22f -> :sswitch_183
        0x1f233 -> :sswitch_184
        0x1f235 -> :sswitch_185
        0x1f236 -> :sswitch_189
        0x1f237 -> :sswitch_192
        0x1f238 -> :sswitch_193
        0x1f239 -> :sswitch_187
        0x1f23a -> :sswitch_188
        0x1f250 -> :sswitch_186
        0x1f300 -> :sswitch_c1
        0x1f302 -> :sswitch_78
        0x1f303 -> :sswitch_152
        0x1f304 -> :sswitch_150
        0x1f305 -> :sswitch_151
        0x1f306 -> :sswitch_149
        0x1f307 -> :sswitch_148
        0x1f308 -> :sswitch_c2
        0x1f30a -> :sswitch_c3
        0x1f319 -> :sswitch_ba
        0x1f31f -> :sswitch_44
        0x1f331 -> :sswitch_b9
        0x1f334 -> :sswitch_b8
        0x1f335 -> :sswitch_b7
        0x1f337 -> :sswitch_ae
        0x1f338 -> :sswitch_ad
        0x1f339 -> :sswitch_b0
        0x1f33a -> :sswitch_b2
        0x1f33b -> :sswitch_b1
        0x1f33e -> :sswitch_b6
        0x1f340 -> :sswitch_af
        0x1f341 -> :sswitch_b3
        0x1f342 -> :sswitch_b5
        0x1f343 -> :sswitch_b4
        0x1f345 -> :sswitch_13a
        0x1f346 -> :sswitch_139
        0x1f349 -> :sswitch_137
        0x1f34a -> :sswitch_136
        0x1f34e -> :sswitch_135
        0x1f353 -> :sswitch_138
        0x1f354 -> :sswitch_123
        0x1f358 -> :sswitch_12a
        0x1f359 -> :sswitch_129
        0x1f35a -> :sswitch_12b
        0x1f35b -> :sswitch_126
        0x1f35c -> :sswitch_12c
        0x1f35d -> :sswitch_125
        0x1f35e -> :sswitch_131
        0x1f35f -> :sswitch_124
        0x1f361 -> :sswitch_12f
        0x1f362 -> :sswitch_12e
        0x1f363 -> :sswitch_128
        0x1f366 -> :sswitch_132
        0x1f367 -> :sswitch_133
        0x1f370 -> :sswitch_134
        0x1f371 -> :sswitch_127
        0x1f372 -> :sswitch_12d
        0x1f373 -> :sswitch_130
        0x1f374 -> :sswitch_122
        0x1f375 -> :sswitch_11d
        0x1f376 -> :sswitch_11e
        0x1f378 -> :sswitch_121
        0x1f37a -> :sswitch_11f
        0x1f37b -> :sswitch_120
        0x1f380 -> :sswitch_77
        0x1f381 -> :sswitch_cf
        0x1f383 -> :sswitch_cb
        0x1f384 -> :sswitch_ce
        0x1f385 -> :sswitch_cd
        0x1f388 -> :sswitch_d1
        0x1f389 -> :sswitch_d0
        0x1f38c -> :sswitch_d2
        0x1f38d -> :sswitch_c4
        0x1f38e -> :sswitch_c6
        0x1f38f -> :sswitch_c9
        0x1f390 -> :sswitch_ca
        0x1f392 -> :sswitch_c7
        0x1f393 -> :sswitch_c8
        0x1f3a1 -> :sswitch_154
        0x1f3a2 -> :sswitch_156
        0x1f3a4 -> :sswitch_107
        0x1f3a5 -> :sswitch_d3
        0x1f3a6 -> :sswitch_181
        0x1f3a7 -> :sswitch_108
        0x1f3a8 -> :sswitch_105
        0x1f3a9 -> :sswitch_69
        0x1f3ab -> :sswitch_16c
        0x1f3ac -> :sswitch_106
        0x1f3ad -> :sswitch_173
        0x1f3af -> :sswitch_111
        0x1f3b1 -> :sswitch_117
        0x1f3b5 -> :sswitch_10a
        0x1f3b6 -> :sswitch_10b
        0x1f3b7 -> :sswitch_10d
        0x1f3b8 -> :sswitch_10e
        0x1f3ba -> :sswitch_10c
        0x1f3bc -> :sswitch_109
        0x1f3be -> :sswitch_116
        0x1f3bf -> :sswitch_11b
        0x1f3c0 -> :sswitch_113
        0x1f3c1 -> :sswitch_119
        0x1f3c6 -> :sswitch_11a
        0x1f3c8 -> :sswitch_112
        0x1f3e0 -> :sswitch_13b
        0x1f3e1 -> :sswitch_13c
        0x1f3e2 -> :sswitch_13e
        0x1f3e3 -> :sswitch_13f
        0x1f3e5 -> :sswitch_140
        0x1f3e6 -> :sswitch_141
        0x1f3e7 -> :sswitch_1a2
        0x1f3e8 -> :sswitch_144
        0x1f3e9 -> :sswitch_143
        0x1f3ea -> :sswitch_142
        0x1f3eb -> :sswitch_13d
        0x1f3ec -> :sswitch_147
        0x1f3ed -> :sswitch_14d
        0x1f3ef -> :sswitch_14a
        0x1f3f0 -> :sswitch_14b
        0x1f40d -> :sswitch_9f
        0x1f40e -> :sswitch_a7
        0x1f411 -> :sswitch_99
        0x1f412 -> :sswitch_97
        0x1f414 -> :sswitch_9e
        0x1f417 -> :sswitch_95
        0x1f418 -> :sswitch_9a
        0x1f419 -> :sswitch_a1
        0x1f41a -> :sswitch_a2
        0x1f41b -> :sswitch_a0
        0x1f41f -> :sswitch_a4
        0x1f420 -> :sswitch_a3
        0x1f421 -> :sswitch_a8
        0x1f425 -> :sswitch_9d
        0x1f426 -> :sswitch_9c
        0x1f427 -> :sswitch_9b
        0x1f428 -> :sswitch_91
        0x1f429 -> :sswitch_aa
        0x1f42b -> :sswitch_a9
        0x1f42c -> :sswitch_a5
        0x1f42d -> :sswitch_8c
        0x1f42e -> :sswitch_94
        0x1f42f -> :sswitch_90
        0x1f430 -> :sswitch_8e
        0x1f431 -> :sswitch_8b
        0x1f433 -> :sswitch_a6
        0x1f434 -> :sswitch_98
        0x1f435 -> :sswitch_96
        0x1f436 -> :sswitch_89
        0x1f437 -> :sswitch_93
        0x1f438 -> :sswitch_8f
        0x1f439 -> :sswitch_8d
        0x1f43a -> :sswitch_8a
        0x1f43b -> :sswitch_92
        0x1f43e -> :sswitch_ab
        0x1f440 -> :sswitch_4b
        0x1f442 -> :sswitch_4a
        0x1f443 -> :sswitch_4c
        0x1f444 -> :sswitch_4e
        0x1f445 -> :sswitch_4d
        0x1f446 -> :sswitch_58
        0x1f447 -> :sswitch_59
        0x1f448 -> :sswitch_5b
        0x1f449 -> :sswitch_5a
        0x1f44a -> :sswitch_52
        0x1f44b -> :sswitch_55
        0x1f44c -> :sswitch_51
        0x1f44d -> :sswitch_4f
        0x1f44e -> :sswitch_50
        0x1f44f -> :sswitch_5f
        0x1f450 -> :sswitch_57
        0x1f451 -> :sswitch_6a
        0x1f452 -> :sswitch_6b
        0x1f454 -> :sswitch_71
        0x1f455 -> :sswitch_70
        0x1f457 -> :sswitch_72
        0x1f458 -> :sswitch_73
        0x1f459 -> :sswitch_74
        0x1f45c -> :sswitch_76
        0x1f45f -> :sswitch_6c
        0x1f460 -> :sswitch_6e
        0x1f461 -> :sswitch_6d
        0x1f462 -> :sswitch_6f
        0x1f466 -> :sswitch_2e
        0x1f467 -> :sswitch_2f
        0x1f468 -> :sswitch_30
        0x1f469 -> :sswitch_31
        0x1f46b -> :sswitch_62
        0x1f46e -> :sswitch_2a
        0x1f46f -> :sswitch_65
        0x1f471 -> :sswitch_34
        0x1f472 -> :sswitch_28
        0x1f473 -> :sswitch_29
        0x1f474 -> :sswitch_32
        0x1f475 -> :sswitch_33
        0x1f476 -> :sswitch_2d
        0x1f477 -> :sswitch_2b
        0x1f478 -> :sswitch_36
        0x1f47b -> :sswitch_cc
        0x1f47c -> :sswitch_35
        0x1f47d -> :sswitch_40
        0x1f47e -> :sswitch_10f
        0x1f47f -> :sswitch_25
        0x1f480 -> :sswitch_3f
        0x1f482 -> :sswitch_2c
        0x1f483 -> :sswitch_61
        0x1f484 -> :sswitch_79
        0x1f485 -> :sswitch_67
        0x1f488 -> :sswitch_16a
        0x1f489 -> :sswitch_f5
        0x1f48a -> :sswitch_f4
        0x1f48b -> :sswitch_86
        0x1f48c -> :sswitch_85
        0x1f48d -> :sswitch_87
        0x1f48e -> :sswitch_88
        0x1f48f -> :sswitch_63
        0x1f490 -> :sswitch_ac
        0x1f491 -> :sswitch_64
        0x1f492 -> :sswitch_145
        0x1f493 -> :sswitch_81
        0x1f494 -> :sswitch_7f
        0x1f496 -> :sswitch_82
        0x1f497 -> :sswitch_80
        0x1f498 -> :sswitch_84
        0x1f499 -> :sswitch_7b
        0x1f49a -> :sswitch_7d
        0x1f49b -> :sswitch_7a
        0x1f49c -> :sswitch_7c
        0x1f49d -> :sswitch_c5
        0x1f49e -> :sswitch_83
        0x1f49f -> :sswitch_19c
        0x1f4a0 -> :sswitch_1a0
        0x1f4a1 -> :sswitch_ec
        0x1f4a2 -> :sswitch_45
        0x1f4a3 -> :sswitch_f2
        0x1f4a4 -> :sswitch_48
        0x1f4a6 -> :sswitch_46
        0x1f4a7 -> :sswitch_47
        0x1f4a8 -> :sswitch_49
        0x1f4a9 -> :sswitch_41
        0x1f4aa -> :sswitch_60
        0x1f4b0 -> :sswitch_f6
        0x1f4b2 -> :sswitch_1a3
        0x1f4b4 -> :sswitch_f7
        0x1f4b5 -> :sswitch_f8
        0x1f4ba -> :sswitch_15c
        0x1f4bb -> :sswitch_da
        0x1f4bc -> :sswitch_75
        0x1f4bd -> :sswitch_d8
        0x1f4be -> :sswitch_d9
        0x1f4bf -> :sswitch_d6
        0x1f4c0 -> :sswitch_d7
        0x1f4d6 -> :sswitch_104
        0x1f4dd -> :sswitch_102
        0x1f4de -> :sswitch_dd
        0x1f4e0 -> :sswitch_de
        0x1f4e1 -> :sswitch_df
        0x1f4e2 -> :sswitch_e4
        0x1f4e3 -> :sswitch_e5
        0x1f4e8 -> :sswitch_fc
        0x1f4e9 -> :sswitch_fb
        0x1f4ea -> :sswitch_fe
        0x1f4eb -> :sswitch_fd
        0x1f4ec -> :sswitch_ff
        0x1f4ed -> :sswitch_100
        0x1f4ee -> :sswitch_101
        0x1f4f1 -> :sswitch_db
        0x1f4f2 -> :sswitch_f9
        0x1f4f3 -> :sswitch_19e
        0x1f4f4 -> :sswitch_19f
        0x1f4f6 -> :sswitch_180
        0x1f4f7 -> :sswitch_d4
        0x1f4fa -> :sswitch_e0
        0x1f4fb -> :sswitch_e1
        0x1f4fc -> :sswitch_d5
        0x1f508 -> :sswitch_e2
        0x1f50d -> :sswitch_ed
        0x1f50e -> :sswitch_eb
        0x1f50f -> :sswitch_e8
        0x1f510 -> :sswitch_e9
        0x1f511 -> :sswitch_ea
        0x1f512 -> :sswitch_e7
        0x1f513 -> :sswitch_e6
        0x1f514 -> :sswitch_e3
        0x1f51e -> :sswitch_197
        0x1f525 -> :sswitch_42
        0x1f528 -> :sswitch_f0
        0x1f52b -> :sswitch_f3
        0x1f530 -> :sswitch_170
        0x1f531 -> :sswitch_1b0
        0x1f532 -> :sswitch_1b1
        0x1f533 -> :sswitch_1b2
        0x1f534 -> :sswitch_1bd
        0x1f535 -> :sswitch_1be
        0x1f536 -> :sswitch_1bf
        0x1f537 -> :sswitch_1c0
        0x1f538 -> :sswitch_1c1
        0x1f539 -> :sswitch_1c2
        0x1f5fb -> :sswitch_14f
        0x1f5fc -> :sswitch_14e
        0x1f5fd -> :sswitch_153
        0x1f601 -> :sswitch_a
        0x1f602 -> :sswitch_11
        0x1f603 -> :sswitch_1
        0x1f604 -> :sswitch_0
        0x1f606 -> :sswitch_1f
        0x1f609 -> :sswitch_3
        0x1f60a -> :sswitch_20
        0x1f60b -> :sswitch_21
        0x1f60c -> :sswitch_c
        0x1f60d -> :sswitch_4
        0x1f60f -> :sswitch_26
        0x1f612 -> :sswitch_d
        0x1f613 -> :sswitch_16
        0x1f614 -> :sswitch_b
        0x1f615 -> :sswitch_27
        0x1f616 -> :sswitch_1e
        0x1f618 -> :sswitch_5
        0x1f61a -> :sswitch_6
        0x1f61c -> :sswitch_7
        0x1f61d -> :sswitch_8
        0x1f61e -> :sswitch_e
        0x1f620 -> :sswitch_1b
        0x1f621 -> :sswitch_1c
        0x1f622 -> :sswitch_10
        0x1f623 -> :sswitch_f
        0x1f624 -> :sswitch_1d
        0x1f625 -> :sswitch_14
        0x1f628 -> :sswitch_19
        0x1f629 -> :sswitch_17
        0x1f62a -> :sswitch_13
        0x1f62b -> :sswitch_18
        0x1f62d -> :sswitch_12
        0x1f630 -> :sswitch_15
        0x1f631 -> :sswitch_1a
        0x1f632 -> :sswitch_24
        0x1f633 -> :sswitch_9
        0x1f635 -> :sswitch_23
        0x1f637 -> :sswitch_22
        0x1f638 -> :sswitch_38
        0x1f639 -> :sswitch_3e
        0x1f63a -> :sswitch_37
        0x1f63b -> :sswitch_39
        0x1f63c -> :sswitch_3b
        0x1f63d -> :sswitch_3a
        0x1f63f -> :sswitch_3d
        0x1f640 -> :sswitch_3c
        0x1f64b -> :sswitch_66
        0x1f64c -> :sswitch_5c
        0x1f64d -> :sswitch_68
        0x1f64f -> :sswitch_5d
        0x1f680 -> :sswitch_15a
        0x1f683 -> :sswitch_160
        0x1f684 -> :sswitch_15e
        0x1f685 -> :sswitch_15f
        0x1f689 -> :sswitch_15d
        0x1f68c -> :sswitch_161
        0x1f68f -> :sswitch_16b
        0x1f691 -> :sswitch_168
        0x1f692 -> :sswitch_167
        0x1f693 -> :sswitch_166
        0x1f695 -> :sswitch_164
        0x1f697 -> :sswitch_163
        0x1f699 -> :sswitch_162
        0x1f69a -> :sswitch_165
        0x1f6a2 -> :sswitch_157
        0x1f6a4 -> :sswitch_159
        0x1f6a5 -> :sswitch_16d
        0x1f6a7 -> :sswitch_16f
        0x1f6ac -> :sswitch_f1
        0x1f6ad -> :sswitch_191
        0x1f6b2 -> :sswitch_169
        0x1f6b9 -> :sswitch_18c
        0x1f6ba -> :sswitch_18d
        0x1f6bb -> :sswitch_18b
        0x1f6bc -> :sswitch_18e
        0x1f6bd -> :sswitch_ef
        0x1f6be -> :sswitch_18f
        0x1f6c0 -> :sswitch_ee
        0xf0000 -> :sswitch_1c3
        0xf0001 -> :sswitch_1c4
        0xf0002 -> :sswitch_1c5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x23 -> :sswitch_1c6
        0x30 -> :sswitch_1ce
        0x31 -> :sswitch_1d0
        0x32 -> :sswitch_1d1
        0x33 -> :sswitch_1d2
        0x34 -> :sswitch_1d3
        0x35 -> :sswitch_1d4
        0x36 -> :sswitch_1d6
        0x37 -> :sswitch_1d7
        0x38 -> :sswitch_1d8
        0x39 -> :sswitch_1d9
        0x1f1e8 -> :sswitch_1c8
        0x1f1e9 -> :sswitch_1c7
        0x1f1ea -> :sswitch_1ca
        0x1f1eb -> :sswitch_1c9
        0x1f1ec -> :sswitch_1cb
        0x1f1ee -> :sswitch_1cd
        0x1f1ef -> :sswitch_1cc
        0x1f1f0 -> :sswitch_1cf
        0x1f1f7 -> :sswitch_1d5
        0x1f1fa -> :sswitch_1da
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x20e3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1f1ea
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1f1f3
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1f1f7
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1f1f8
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1f1e7
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1f1f5
        :pswitch_6
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1f1f9
        :pswitch_7
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x20e3
        :pswitch_8
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x1f1f7
        :pswitch_9
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x20e3
        :pswitch_a
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x20e3
        :pswitch_b
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x20e3
        :pswitch_c
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x20e3
        :pswitch_d
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x20e3
        :pswitch_e
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x1f1fa
        :pswitch_f
    .end packed-switch

    :pswitch_data_10
    .packed-switch 0x20e3
        :pswitch_10
    .end packed-switch

    :pswitch_data_11
    .packed-switch 0x20e3
        :pswitch_11
    .end packed-switch

    :pswitch_data_12
    .packed-switch 0x20e3
        :pswitch_12
    .end packed-switch

    :pswitch_data_13
    .packed-switch 0x20e3
        :pswitch_13
    .end packed-switch

    :pswitch_data_14
    .packed-switch 0x1f1f8
        :pswitch_14
    .end packed-switch
.end method

.method public final a()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/7HE;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x17e

    const/16 v8, 0x13b

    const/16 v7, 0xc4

    const/16 v6, 0x89

    const/4 v5, 0x0

    .line 352189
    invoke-static {}, LX/1zH;->d()Ljava/util/List;

    move-result-object v0

    .line 352190
    const/4 v1, 0x5

    new-array v1, v1, [LX/7HE;

    const v2, 0x7f020609

    const v3, 0x7f0809e2

    invoke-interface {v0, v5, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x1

    const v3, 0x7f020606

    const v4, 0x7f0809e3

    invoke-interface {v0, v6, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f020607

    const v4, 0x7f0809e4

    invoke-interface {v0, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f020605

    const v4, 0x7f0809e5

    invoke-interface {v0, v8, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f02060a

    const v4, 0x7f0809e6

    const/16 v5, 0x1d8

    invoke-interface {v0, v9, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v3, v4, v0}, LX/7HE;->a(IILjava/util/List;)LX/7HE;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
