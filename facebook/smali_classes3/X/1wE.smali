.class public final LX/1wE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 345272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 345273
    if-nez p1, :cond_0

    .line 345274
    :goto_0
    return v0

    .line 345275
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 345276
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 345277
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ah()Lcom/facebook/graphql/model/GraphQLProfileBadge;

    move-result-object v3

    const/4 v4, 0x0

    .line 345278
    if-nez v3, :cond_1

    .line 345279
    :goto_1
    move v3, v4

    .line 345280
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    const/4 v5, 0x0

    .line 345281
    if-nez v4, :cond_4

    .line 345282
    :goto_2
    move v4, v5

    .line 345283
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 345284
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 345285
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 345286
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 345287
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 345288
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 345289
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 345290
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfileBadge;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v5

    const/4 v6, 0x0

    .line 345291
    if-nez v5, :cond_2

    .line 345292
    :goto_3
    move v5, v6

    .line 345293
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 345294
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 345295
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 345296
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 345297
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    const/4 v8, 0x0

    .line 345298
    if-nez v7, :cond_3

    .line 345299
    :goto_4
    move v7, v8

    .line 345300
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 345301
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 345302
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 345303
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_3

    .line 345304
    :cond_3
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 345305
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 345306
    invoke-virtual {p0, v8, v3}, LX/186;->b(II)V

    .line 345307
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 345308
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 345309
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 345310
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 345311
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 345312
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 345313
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 345154
    if-nez p1, :cond_0

    .line 345155
    :goto_0
    return v2

    .line 345156
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 345157
    if-eqz v3, :cond_2

    .line 345158
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 345159
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 345160
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v6, 0x0

    .line 345161
    if-nez v0, :cond_3

    .line 345162
    :goto_2
    move v0, v6

    .line 345163
    aput v0, v4, v1

    .line 345164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 345165
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 345166
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 345167
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 345168
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 345169
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 345170
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 345171
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 345172
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->c(Ljava/util/List;)I

    move-result v7

    .line 345173
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v8

    const/4 v9, 0x0

    .line 345174
    if-nez v8, :cond_4

    .line 345175
    :goto_4
    move v8, v9

    .line 345176
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 345177
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 345178
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 345179
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 345180
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 345181
    :cond_4
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 345182
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 345183
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 345184
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 345185
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 345186
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 345187
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_4
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;
    .locals 14
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getShouldDisplayProfilePictureGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 345188
    if-nez p0, :cond_1

    .line 345189
    :cond_0
    :goto_0
    return-object v2

    .line 345190
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 345191
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 345192
    if-nez p0, :cond_3

    .line 345193
    :goto_1
    move v1, v4

    .line 345194
    if-eqz v1, :cond_0

    .line 345195
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 345196
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 345197
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 345198
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 345199
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 345200
    const-string v1, "FeedStoryUtilModelConverter.getShouldDisplayProfilePictureGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 345201
    :cond_2
    new-instance v2, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 345202
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    .line 345203
    if-eqz v5, :cond_7

    .line 345204
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 345205
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 345206
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {v0, v1}, LX/1wE;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v1

    aput v1, v6, v3

    .line 345207
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 345208
    :cond_4
    invoke-virtual {v0, v6, v10}, LX/186;->a([IZ)I

    move-result v1

    move v3, v1

    .line 345209
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 345210
    if-nez v1, :cond_8

    .line 345211
    :goto_4
    move v6, v7

    .line 345212
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    const/4 v5, 0x0

    .line 345213
    if-nez v1, :cond_b

    .line 345214
    :goto_5
    move v7, v5

    .line 345215
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v8

    .line 345216
    if-eqz v8, :cond_6

    .line 345217
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v1

    new-array v9, v1, [I

    move v5, v4

    .line 345218
    :goto_6
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    .line 345219
    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v11, 0x0

    .line 345220
    if-nez v1, :cond_c

    .line 345221
    :goto_7
    move v1, v11

    .line 345222
    aput v1, v9, v5

    .line 345223
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_6

    .line 345224
    :cond_5
    invoke-virtual {v0, v9, v10}, LX/186;->a([IZ)I

    move-result v1

    .line 345225
    :goto_8
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    const/4 v8, 0x0

    .line 345226
    if-nez v5, :cond_d

    .line 345227
    :goto_9
    move v5, v8

    .line 345228
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->at()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    const/4 v9, 0x0

    .line 345229
    if-nez v8, :cond_e

    .line 345230
    :goto_a
    move v8, v9

    .line 345231
    const/4 v9, 0x7

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 345232
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 345233
    invoke-virtual {v0, v10, v6}, LX/186;->b(II)V

    .line 345234
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 345235
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 345236
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 345237
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ad()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 345238
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 345239
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 345240
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto :goto_8

    :cond_7
    move v3, v4

    goto :goto_3

    .line 345241
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v8

    .line 345242
    if-eqz v8, :cond_a

    .line 345243
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    new-array v9, v5, [I

    move v6, v7

    .line 345244
    :goto_b
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_9

    .line 345245
    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v5}, LX/1wE;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v5

    aput v5, v9, v6

    .line 345246
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_b

    .line 345247
    :cond_9
    invoke-virtual {v0, v9, v11}, LX/186;->a([IZ)I

    move-result v5

    .line 345248
    :goto_c
    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 345249
    invoke-virtual {v0, v7, v5}, LX/186;->b(II)V

    .line 345250
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 345251
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto/16 :goto_4

    :cond_a
    move v5, v7

    goto :goto_c

    .line 345252
    :cond_b
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 345253
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 345254
    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 345255
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    .line 345256
    invoke-virtual {v0, v5}, LX/186;->d(I)V

    goto/16 :goto_5

    .line 345257
    :cond_c
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->c(Ljava/util/List;)I

    move-result v12

    .line 345258
    const/4 v13, 0x1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 345259
    invoke-virtual {v0, v11, v12}, LX/186;->b(II)V

    .line 345260
    invoke-virtual {v0}, LX/186;->d()I

    move-result v11

    .line 345261
    invoke-virtual {v0, v11}, LX/186;->d(I)V

    goto/16 :goto_7

    .line 345262
    :cond_d
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 345263
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 345264
    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 345265
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    .line 345266
    invoke-virtual {v0, v8}, LX/186;->d(I)V

    goto/16 :goto_9

    .line 345267
    :cond_e
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 345268
    const/4 v12, 0x1

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 345269
    invoke-virtual {v0, v9, v11}, LX/186;->b(II)V

    .line 345270
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 345271
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_a
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 345089
    if-nez p0, :cond_1

    .line 345090
    :cond_0
    :goto_0
    return-object v2

    .line 345091
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 345092
    const/4 v1, 0x0

    .line 345093
    if-nez p0, :cond_3

    .line 345094
    :goto_1
    move v1, v1

    .line 345095
    if-eqz v1, :cond_0

    .line 345096
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 345097
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 345098
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 345099
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 345100
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 345101
    const-string v1, "FeedStoryUtilModelConverter.getShouldRenderOrganicHScrollGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 345102
    :cond_2
    new-instance v2, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldRenderOrganicHScrollGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 345103
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E()Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v3

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 345104
    if-nez v3, :cond_4

    .line 345105
    :goto_2
    move v3, v6

    .line 345106
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aO()LX/0Px;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->c(Ljava/util/List;)I

    move-result v4

    .line 345107
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 345108
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 345109
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 345110
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 345111
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1

    .line 345112
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v7

    .line 345113
    if-eqz v7, :cond_6

    .line 345114
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    new-array v8, v4, [I

    move v5, v6

    .line 345115
    :goto_3
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 345116
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v4}, LX/1wE;->d(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v4

    aput v4, v8, v5

    .line 345117
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 345118
    :cond_5
    invoke-virtual {v0, v8, v9}, LX/186;->a([IZ)I

    move-result v4

    .line 345119
    :goto_4
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 345120
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->a()I

    move-result v5

    invoke-virtual {v0, v6, v5, v6}, LX/186;->a(III)V

    .line 345121
    invoke-virtual {v0, v9, v4}, LX/186;->b(II)V

    .line 345122
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    .line 345123
    invoke-virtual {v0, v6}, LX/186;->d(I)V

    goto :goto_2

    :cond_6
    move v4, v6

    goto :goto_4
.end method

.method public static d(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 345124
    if-nez p1, :cond_0

    .line 345125
    :goto_0
    return v2

    .line 345126
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 345127
    if-eqz v3, :cond_2

    .line 345128
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 345129
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 345130
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v6, 0x0

    .line 345131
    if-nez v0, :cond_3

    .line 345132
    :goto_2
    move v0, v6

    .line 345133
    aput v0, v4, v1

    .line 345134
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 345135
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 345136
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 345137
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 345138
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 345139
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 345140
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    const/4 v8, 0x0

    .line 345141
    if-nez v7, :cond_4

    .line 345142
    :goto_4
    move v7, v8

    .line 345143
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 345144
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 345145
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 345146
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 345147
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 345148
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 345149
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 345150
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 345151
    const/4 v8, 0x1

    invoke-virtual {p0, v8, p1}, LX/186;->b(II)V

    .line 345152
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 345153
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method
