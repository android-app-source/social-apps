.class public final LX/20k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 355987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 355988
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 355989
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Integer;->compare(II)I

    move-result v0

    return v0
.end method
