.class public LX/1ys;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:[I

.field private static h:LX/0Xm;


# instance fields
.field public final d:LX/359;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/359",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/1vg;

.field public final f:Landroid/content/res/ColorStateList;

.field public final g:LX/1yf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 351528
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a1

    aput v2, v0, v1

    sput-object v0, LX/1ys;->c:[I

    .line 351529
    const v0, 0x7f020881

    sput v0, LX/1ys;->a:I

    .line 351530
    const v0, 0x7f02087f

    sput v0, LX/1ys;->b:I

    return-void
.end method

.method public constructor <init>(LX/359;LX/1vg;Landroid/content/res/Resources;LX/1yf;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351523
    iput-object p1, p0, LX/1ys;->d:LX/359;

    .line 351524
    iput-object p2, p0, LX/1ys;->e:LX/1vg;

    .line 351525
    const v0, 0x7f0a0a29

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/1ys;->f:Landroid/content/res/ColorStateList;

    .line 351526
    iput-object p4, p0, LX/1ys;->g:LX/1yf;

    .line 351527
    return-void
.end method

.method public static a(LX/0QB;)LX/1ys;
    .locals 7

    .prologue
    .line 351511
    const-class v1, LX/1ys;

    monitor-enter v1

    .line 351512
    :try_start_0
    sget-object v0, LX/1ys;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351513
    sput-object v2, LX/1ys;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351514
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351515
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351516
    new-instance p0, LX/1ys;

    invoke-static {v0}, LX/359;->a(LX/0QB;)LX/359;

    move-result-object v3

    check-cast v3, LX/359;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v6

    check-cast v6, LX/1yf;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1ys;-><init>(LX/359;LX/1vg;Landroid/content/res/Resources;LX/1yf;)V

    .line 351517
    move-object v0, p0

    .line 351518
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351519
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ys;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351520
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351521
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/14w;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 351504
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351505
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 351506
    if-nez v0, :cond_1

    .line 351507
    :cond_0
    :goto_0
    move v0, v1

    .line 351508
    return v0

    .line 351509
    :cond_1
    const p0, -0x7a50adf8

    invoke-static {v0, p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 351510
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq p0, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
