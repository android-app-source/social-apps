.class public LX/1xW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1yY;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 348593
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1xW;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348625
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348626
    iput-object p1, p0, LX/1xW;->b:LX/0Ot;

    .line 348627
    return-void
.end method

.method public static a(LX/0QB;)LX/1xW;
    .locals 4

    .prologue
    .line 348614
    const-class v1, LX/1xW;

    monitor-enter v1

    .line 348615
    :try_start_0
    sget-object v0, LX/1xW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348616
    sput-object v2, LX/1xW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348619
    new-instance v3, LX/1xW;

    const/16 p0, 0x714

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xW;-><init>(LX/0Ot;)V

    .line 348620
    move-object v0, v3

    .line 348621
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348622
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348623
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 348628
    iget-object v0, p0, LX/1xW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yZ;

    .line 348629
    iget-object v1, v0, LX/1yZ;->b:LX/0tK;

    const/4 v2, 0x0

    .line 348630
    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 348631
    invoke-virtual {v1}, LX/0tK;->q()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget-short p0, LX/0wm;->I:S

    invoke-interface {v3, p0, p2}, LX/0ad;->a(SZ)Z

    move-result v3

    :goto_0
    move v3, v3

    .line 348632
    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, LX/0tK;->b(Z)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 348633
    if-nez v1, :cond_0

    .line 348634
    const/4 v1, 0x0

    .line 348635
    :goto_2
    move-object v0, v1

    .line 348636
    return-object v0

    .line 348637
    :cond_0
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v1

    const v2, 0x7f02040c

    invoke-virtual {v1, v2}, LX/1nm;->h(I)LX/1nm;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 348638
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    sget-object v2, LX/1yZ;->a:Landroid/util/SparseArray;

    invoke-interface {v1, v2}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v1

    .line 348639
    const v2, 0x7deaaff5

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 348640
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const v2, 0x7f080a16

    invoke-interface {v1, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_2

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    iget-object v3, v1, LX/0tK;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget-short v0, LX/0wm;->o:S

    invoke-interface {v3, v0, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v1, LX/0tK;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Uh;

    const/16 v0, 0x481

    invoke-virtual {v3, v0, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v3, p2

    goto :goto_0

    :cond_4
    move v3, p0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348602
    invoke-static {}, LX/1dS;->b()V

    .line 348603
    iget v0, p1, LX/1dQ;->b:I

    .line 348604
    packed-switch v0, :pswitch_data_0

    .line 348605
    :goto_0
    return-object v2

    .line 348606
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 348607
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 348608
    iget-object p1, p0, LX/1xW;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 348609
    new-instance p1, LX/0hs;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 v1, 0x2

    invoke-direct {p1, p0, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 348610
    invoke-virtual {p1, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 348611
    const p0, 0x7f080a14

    invoke-virtual {p1, p0}, LX/0hs;->b(I)V

    .line 348612
    invoke-virtual {p1}, LX/0ht;->d()V

    .line 348613
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7deaaff5
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1yY;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 348594
    new-instance v1, LX/1yX;

    invoke-direct {v1, p0}, LX/1yX;-><init>(LX/1xW;)V

    .line 348595
    sget-object v2, LX/1xW;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1yY;

    .line 348596
    if-nez v2, :cond_0

    .line 348597
    new-instance v2, LX/1yY;

    invoke-direct {v2}, LX/1yY;-><init>()V

    .line 348598
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1yY;->a$redex0(LX/1yY;LX/1De;IILX/1yX;)V

    .line 348599
    move-object v1, v2

    .line 348600
    move-object v0, v1

    .line 348601
    return-object v0
.end method
