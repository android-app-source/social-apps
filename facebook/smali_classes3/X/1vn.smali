.class public LX/1vn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0rq;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0rq;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344528
    iput-object p1, p0, LX/1vn;->a:LX/0rq;

    .line 344529
    iput-object p2, p0, LX/1vn;->b:Landroid/content/res/Resources;

    .line 344530
    return-void
.end method

.method public static a(LX/0QB;)LX/1vn;
    .locals 5

    .prologue
    .line 344512
    const-class v1, LX/1vn;

    monitor-enter v1

    .line 344513
    :try_start_0
    sget-object v0, LX/1vn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 344514
    sput-object v2, LX/1vn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 344515
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344516
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 344517
    new-instance p0, LX/1vn;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/1vn;-><init>(LX/0rq;Landroid/content/res/Resources;)V

    .line 344518
    move-object v0, p0

    .line 344519
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 344520
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1vn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344521
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 344522
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 344523
    iget-object v0, p0, LX/1vn;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b09a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 344524
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 344525
    iget-object v0, p0, LX/1vn;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b09a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 344526
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
