.class public LX/1yS;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/1nV;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1Uf;

.field public final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field public final e:LX/0lF;

.field public f:I

.field public final g:I

.field public final h:LX/1yD;

.field public final i:Ljava/lang/String;

.field public j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Uf;Ljava/lang/String;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V
    .locals 1
    .param p4    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/1yD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350602
    iput-object p1, p0, LX/1yS;->b:LX/1Uf;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 350603
    iput-object p2, p0, LX/1yS;->c:Ljava/lang/String;

    .line 350604
    iput-object p3, p0, LX/1yS;->d:Landroid/content/Context;

    .line 350605
    iput-object p4, p0, LX/1yS;->e:LX/0lF;

    .line 350606
    const v0, 0x7f0a0427

    iput v0, p0, LX/1yS;->f:I

    .line 350607
    iput p5, p0, LX/1yS;->g:I

    .line 350608
    iput-object p6, p0, LX/1yS;->h:LX/1yD;

    .line 350609
    iput-object p7, p0, LX/1yS;->i:Ljava/lang/String;

    .line 350610
    const/4 v0, 0x0

    iput-object v0, p0, LX/1yS;->j:Ljava/lang/ref/WeakReference;

    .line 350611
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 350612
    iput p1, p0, LX/1yS;->f:I

    .line 350613
    return-void
.end method

.method public final a(LX/1nV;)V
    .locals 1
    .param p1    # LX/1nV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350614
    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    :goto_0
    iput-object v0, p0, LX/1yS;->a:Ljava/lang/ref/WeakReference;

    .line 350615
    return-void

    .line 350616
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;LX/0lF;ZLX/1vY;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const v3, -0x15349e30

    const/4 v1, 0x0

    .line 350617
    iget-object v0, p0, LX/1yS;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/1yS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nV;

    .line 350618
    :goto_0
    iget v2, p0, LX/1yS;->g:I

    move v2, v2

    .line 350619
    if-eqz v0, :cond_2

    .line 350620
    if-eq v2, v3, :cond_0

    invoke-static {p2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 350621
    invoke-virtual {p0, p4, p5, p3}, LX/1yS;->a(ZLX/1vY;LX/0lF;)V

    .line 350622
    :cond_0
    invoke-interface {v0, p0}, LX/1nV;->onClick(LX/1yS;)V

    .line 350623
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 350624
    goto :goto_0

    .line 350625
    :cond_2
    if-ne v2, v3, :cond_8

    .line 350626
    iget-object v0, p0, LX/1yS;->h:LX/1yD;

    move-object v0, v0

    .line 350627
    invoke-interface {v0}, LX/1yD;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, LX/2yB;->a(LX/1yE;Ljava/lang/String;Ljava/lang/String;)LX/47G;

    move-result-object v1

    .line 350628
    invoke-static {v4, p3}, LX/17Q;->c(ZLX/0lF;)Ljava/util/Map;

    move-result-object v0

    .line 350629
    if-eqz p5, :cond_3

    .line 350630
    const-string v3, "tn"

    invoke-interface {v0, v3, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350631
    :cond_3
    :goto_2
    invoke-static {p2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 350632
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v2

    .line 350633
    iput-object p2, v2, LX/47H;->a:Ljava/lang/String;

    .line 350634
    move-object v2, v2

    .line 350635
    iput-object v1, v2, LX/47H;->d:LX/47G;

    .line 350636
    move-object v1, v2

    .line 350637
    invoke-virtual {v1, v0}, LX/47H;->a(Ljava/util/Map;)LX/47H;

    move-result-object v0

    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v1

    .line 350638
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;LX/47I;)Z

    goto :goto_1

    .line 350639
    :cond_4
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->A:LX/0id;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 350640
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 350641
    if-eqz v0, :cond_7

    .line 350642
    invoke-virtual {p0, p4, p5, p3}, LX/1yS;->a(ZLX/1vY;LX/0lF;)V

    .line 350643
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 350644
    invoke-static {v1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 350645
    iget-object v1, p0, LX/1yS;->b:LX/1Uf;

    iget-object v1, v1, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 350646
    :cond_5
    const v1, -0xe9bddb6

    if-ne v2, v1, :cond_6

    .line 350647
    iget-object v1, p0, LX/1yS;->b:LX/1Uf;

    iget-object v1, v1, LX/1Uf;->l:LX/0Zb;

    const-string v2, "instagram_tag_interactions"

    invoke-interface {v1, v2, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 350648
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 350649
    const-string v2, "event"

    const-string v3, "android_click"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 350650
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 350651
    :cond_6
    iget-object v1, p0, LX/1yS;->b:LX/1Uf;

    iget-object v1, v1, LX/1Uf;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 350652
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->A:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    goto/16 :goto_1

    .line 350653
    :cond_7
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->A:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public final a(ZLX/1vY;LX/0lF;)V
    .locals 4
    .param p2    # LX/1vY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350654
    if-eqz p3, :cond_1

    invoke-virtual {p3}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 350655
    :goto_0
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 350656
    iput-object v1, v0, LX/0gh;->y:Ljava/lang/String;

    .line 350657
    iget-object v0, p0, LX/1yS;->e:LX/0lF;

    if-eqz v0, :cond_0

    .line 350658
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17V;

    iget-object v1, p0, LX/1yS;->c:Ljava/lang/String;

    iget-object v2, p0, LX/1yS;->e:LX/0lF;

    const-string v3, "native_newsfeed"

    invoke-virtual {v0, v1, p1, v2, v3}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 350659
    invoke-static {v0, p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1vY;)V

    .line 350660
    iget-object v1, p0, LX/1yS;->b:LX/1Uf;

    iget-object v1, v1, LX/1Uf;->l:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 350661
    :cond_0
    return-void

    .line 350662
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 350663
    const v0, 0x7f0d0081

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 350664
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v4, v0

    .line 350665
    invoke-static {p1}, LX/1vZ;->a(Landroid/view/View;)LX/1vY;

    move-result-object v5

    .line 350666
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/1yS;->j:Ljava/lang/ref/WeakReference;

    .line 350667
    iget-object v0, p0, LX/1yS;->b:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    if-eqz v4, :cond_0

    const-string v1, "tap_sponsored_link"

    :goto_1
    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 350668
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/1yS;->c:Ljava/lang/String;

    iget-object v3, p0, LX/1yS;->e:LX/0lF;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/1yS;->a(Landroid/content/Context;Ljava/lang/String;LX/0lF;ZLX/1vY;)V

    .line 350669
    return-void

    .line 350670
    :cond_0
    const-string v1, "tap_link"

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 350671
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 350672
    iget-object v0, p0, LX/1yS;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, LX/1yS;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 350673
    return-void
.end method
