.class public LX/22M;
.super LX/22N;
.source ""


# instance fields
.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1jk;LX/22I;LX/0ue;LX/0u0;LX/0uc;)V
    .locals 10

    .prologue
    .line 359405
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/22N;-><init>(LX/1jk;LX/1w1;ILX/0ue;LX/0u0;LX/0uc;)V

    .line 359406
    iget-object v0, p2, LX/22I;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/22I;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 359407
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Missing outputs field definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359408
    :cond_1
    iget-object v0, p2, LX/22I;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 359409
    new-array v4, v3, [LX/0uN;

    .line 359410
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, LX/22M;->f:Ljava/util/Map;

    .line 359411
    const/4 v0, 0x0

    .line 359412
    iget-object v1, p2, LX/22I;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1w6;

    .line 359413
    iget-object v5, v0, LX/1w6;->b:Ljava/lang/String;

    invoke-static {v5}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v5

    aput-object v5, v4, v1

    .line 359414
    iget-object v5, p0, LX/22M;->f:Ljava/util/Map;

    iget-object v0, v0, LX/1w6;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359415
    aget-object v0, v4, v1

    if-nez v0, :cond_2

    .line 359416
    new-instance v0, LX/5MH;

    const-string v1, "Missing output type definition"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359417
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 359418
    goto :goto_0

    .line 359419
    :cond_3
    new-array v0, v3, [LX/0uE;

    iput-object v0, p0, LX/22M;->e:[LX/0uE;

    .line 359420
    iget-object v0, p2, LX/22I;->j:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p2, LX/22I;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_5

    .line 359421
    :cond_4
    new-instance v0, LX/5MH;

    const-string v1, "Missing default value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359422
    :cond_5
    iget-object v0, p2, LX/22I;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21u;

    .line 359423
    iget-object v1, p0, LX/22M;->f:Ljava/util/Map;

    iget-object v5, v0, LX/21u;->a:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 359424
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt v5, v3, :cond_7

    .line 359425
    :cond_6
    new-instance v0, LX/5MH;

    const-string v1, "Undeclaed output param"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359426
    :cond_7
    iget-object v5, p0, LX/22N;->e:[LX/0uE;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-instance v7, LX/0uE;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v4, v1

    iget-object v0, v0, LX/21u;->b:Ljava/lang/String;

    invoke-direct {v7, v1, v0}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    aput-object v7, v5, v6

    goto :goto_1

    .line 359427
    :cond_8
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_a

    .line 359428
    iget-object v1, p0, LX/22N;->e:[LX/0uE;

    aget-object v1, v1, v0

    if-nez v1, :cond_9

    .line 359429
    new-instance v0, LX/5MH;

    const-string v1, "Missing default value"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359430
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 359431
    :cond_a
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/22M;->d:Ljava/util/Map;

    .line 359432
    iget-object v0, p2, LX/22I;->i:Ljava/util/List;

    if-nez v0, :cond_b

    .line 359433
    new-instance v0, LX/5MH;

    const-string v1, "Missing table"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359434
    :cond_b
    iget-object v0, p2, LX/22I;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22L;

    .line 359435
    new-array v6, v3, [LX/0uE;

    .line 359436
    iget-object v1, v0, LX/22L;->b:Ljava/util/List;

    if-nez v1, :cond_c

    .line 359437
    new-instance v0, LX/5MH;

    const-string v1, "Missing table item values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359438
    :cond_c
    iget-object v1, v0, LX/22L;->a:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 359439
    new-instance v0, LX/5MH;

    const-string v1, "Missing table item bucket"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359440
    :cond_d
    iget-object v1, v0, LX/22L;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/21u;

    .line 359441
    iget-object v2, p0, LX/22M;->f:Ljava/util/Map;

    iget-object v8, v1, LX/21u;->a:Ljava/lang/String;

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 359442
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-lt v8, v3, :cond_f

    .line 359443
    :cond_e
    new-instance v0, LX/5MH;

    const-string v1, "Undeclaed output param"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359444
    :cond_f
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    new-instance v9, LX/0uE;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v2, v4, v2

    iget-object v1, v1, LX/21u;->b:Ljava/lang/String;

    invoke-direct {v9, v2, v1}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    aput-object v9, v6, v8

    goto :goto_4

    .line 359445
    :cond_10
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v3, :cond_12

    .line 359446
    aget-object v2, v6, v1

    if-nez v2, :cond_11

    .line 359447
    iget-object v2, p0, LX/22N;->e:[LX/0uE;

    aget-object v2, v2, v1

    aput-object v2, v6, v1

    .line 359448
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 359449
    :cond_12
    iget-object v1, p0, LX/22N;->d:Ljava/util/Map;

    iget-object v0, v0, LX/22L;->a:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 359450
    :cond_13
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 359401
    iget-object v0, p0, LX/22M;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 359402
    if-nez v0, :cond_0

    .line 359403
    const/4 v0, -0x1

    .line 359404
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
