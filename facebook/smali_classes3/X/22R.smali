.class public LX/22R;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VW;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/22R",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1VW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359662
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 359663
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/22R;->b:LX/0Zi;

    .line 359664
    iput-object p1, p0, LX/22R;->a:LX/0Ot;

    .line 359665
    return-void
.end method

.method public static a(LX/0QB;)LX/22R;
    .locals 4

    .prologue
    .line 359651
    const-class v1, LX/22R;

    monitor-enter v1

    .line 359652
    :try_start_0
    sget-object v0, LX/22R;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359653
    sput-object v2, LX/22R;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359656
    new-instance v3, LX/22R;

    const/16 p0, 0x827

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/22R;-><init>(LX/0Ot;)V

    .line 359657
    move-object v0, v3

    .line 359658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 359638
    check-cast p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    .line 359639
    iget-object v0, p0, LX/22R;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1VW;

    iget-object v2, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    iget-object v4, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-object v5, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    iget-object v6, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    iget-object v7, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    iget-object v8, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, LX/1VW;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/22Q;Lcom/facebook/graphql/model/GraphQLMedia;LX/1f6;LX/1aZ;LX/1Pr;Lcom/facebook/common/callercontext/CallerContext;)LX/1Dg;

    move-result-object v0

    .line 359640
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359649
    invoke-static {}, LX/1dS;->b()V

    .line 359650
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/22S;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 359641
    new-instance v1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;-><init>(LX/22R;)V

    .line 359642
    iget-object v2, p0, LX/22R;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/22S;

    .line 359643
    if-nez v2, :cond_0

    .line 359644
    new-instance v2, LX/22S;

    invoke-direct {v2, p0}, LX/22S;-><init>(LX/22R;)V

    .line 359645
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/22S;->a$redex0(LX/22S;LX/1De;IILcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;)V

    .line 359646
    move-object v1, v2

    .line 359647
    move-object v0, v1

    .line 359648
    return-object v0
.end method
