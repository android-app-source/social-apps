.class public final enum LX/1yz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1yz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1yz;

.field public static final enum DIFFERENT_YEAR:LX/1yz;

.field public static final enum LESS_THAN_4_DAYS:LX/1yz;

.field public static final enum LESS_THAN_5_MINS:LX/1yz;

.field public static final enum LESS_THAN_HOUR:LX/1yz;

.field public static final enum LESS_THAN_MIN:LX/1yz;

.field public static final enum LESS_THAN_ONE_DAY:LX/1yz;

.field public static final enum SAME_DAY:LX/1yz;

.field public static final enum SAME_YEAR:LX/1yz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 351709
    new-instance v0, LX/1yz;

    const-string v1, "LESS_THAN_MIN"

    invoke-direct {v0, v1, v3}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->LESS_THAN_MIN:LX/1yz;

    .line 351710
    new-instance v0, LX/1yz;

    const-string v1, "LESS_THAN_5_MINS"

    invoke-direct {v0, v1, v4}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->LESS_THAN_5_MINS:LX/1yz;

    .line 351711
    new-instance v0, LX/1yz;

    const-string v1, "LESS_THAN_HOUR"

    invoke-direct {v0, v1, v5}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->LESS_THAN_HOUR:LX/1yz;

    .line 351712
    new-instance v0, LX/1yz;

    const-string v1, "SAME_DAY"

    invoke-direct {v0, v1, v6}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->SAME_DAY:LX/1yz;

    .line 351713
    new-instance v0, LX/1yz;

    const-string v1, "LESS_THAN_ONE_DAY"

    invoke-direct {v0, v1, v7}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->LESS_THAN_ONE_DAY:LX/1yz;

    .line 351714
    new-instance v0, LX/1yz;

    const-string v1, "LESS_THAN_4_DAYS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->LESS_THAN_4_DAYS:LX/1yz;

    .line 351715
    new-instance v0, LX/1yz;

    const-string v1, "SAME_YEAR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->SAME_YEAR:LX/1yz;

    .line 351716
    new-instance v0, LX/1yz;

    const-string v1, "DIFFERENT_YEAR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/1yz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1yz;->DIFFERENT_YEAR:LX/1yz;

    .line 351717
    const/16 v0, 0x8

    new-array v0, v0, [LX/1yz;

    sget-object v1, LX/1yz;->LESS_THAN_MIN:LX/1yz;

    aput-object v1, v0, v3

    sget-object v1, LX/1yz;->LESS_THAN_5_MINS:LX/1yz;

    aput-object v1, v0, v4

    sget-object v1, LX/1yz;->LESS_THAN_HOUR:LX/1yz;

    aput-object v1, v0, v5

    sget-object v1, LX/1yz;->SAME_DAY:LX/1yz;

    aput-object v1, v0, v6

    sget-object v1, LX/1yz;->LESS_THAN_ONE_DAY:LX/1yz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/1yz;->LESS_THAN_4_DAYS:LX/1yz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/1yz;->SAME_YEAR:LX/1yz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/1yz;->DIFFERENT_YEAR:LX/1yz;

    aput-object v2, v0, v1

    sput-object v0, LX/1yz;->$VALUES:[LX/1yz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 351718
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1yz;
    .locals 1

    .prologue
    .line 351719
    const-class v0, LX/1yz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1yz;

    return-object v0
.end method

.method public static values()[LX/1yz;
    .locals 1

    .prologue
    .line 351720
    sget-object v0, LX/1yz;->$VALUES:[LX/1yz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1yz;

    return-object v0
.end method
