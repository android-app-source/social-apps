.class public LX/1vR;
.super LX/0rY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0rY",
        "<",
        "Lcom/facebook/api/feed/Vpv;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1vR;


# direct methods
.method public constructor <init>(LX/0rd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342898
    invoke-direct {p0, p1}, LX/0rY;-><init>(LX/0rd;)V

    .line 342899
    return-void
.end method

.method public static a(LX/0QB;)LX/1vR;
    .locals 4

    .prologue
    .line 342908
    sget-object v0, LX/1vR;->b:LX/1vR;

    if-nez v0, :cond_1

    .line 342909
    const-class v1, LX/1vR;

    monitor-enter v1

    .line 342910
    :try_start_0
    sget-object v0, LX/1vR;->b:LX/1vR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 342911
    if-eqz v2, :cond_0

    .line 342912
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 342913
    new-instance p0, LX/1vR;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-direct {p0, v3}, LX/1vR;-><init>(LX/0rd;)V

    .line 342914
    move-object v0, p0

    .line 342915
    sput-object v0, LX/1vR;->b:LX/1vR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342916
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 342917
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 342918
    :cond_1
    sget-object v0, LX/1vR;->b:LX/1vR;

    return-object v0

    .line 342919
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 342920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;J)V
    .locals 6

    .prologue
    .line 342900
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_1

    .line 342901
    :cond_0
    :goto_0
    return-void

    .line 342902
    :cond_1
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 342903
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bk()Lcom/facebook/graphql/model/GraphQLFeedBackendData;

    move-result-object v0

    .line 342904
    if-eqz v0, :cond_0

    .line 342905
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedBackendData;->j()Ljava/lang/String;

    move-result-object v1

    .line 342906
    if-eqz v1, :cond_0

    .line 342907
    iget-object v2, p0, LX/0rY;->a:LX/0re;

    new-instance v3, Lcom/facebook/api/feed/Vpv;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedBackendData;->a()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, 0x3e8

    div-long v4, p2, v4

    long-to-int v4, v4

    invoke-direct {v3, v0, v1, v4}, Lcom/facebook/api/feed/Vpv;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v2, v1, v3}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
