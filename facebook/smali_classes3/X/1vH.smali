.class public final LX/1vH;
.super LX/1vI;
.source ""


# instance fields
.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 342116
    invoke-direct {p0}, LX/1vI;-><init>()V

    .line 342117
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/1vH;->a:Ljava/io/File;

    .line 342118
    return-void
.end method

.method private c()Ljava/io/FileInputStream;
    .locals 2

    .prologue
    .line 342119
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, LX/1vH;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 342120
    invoke-direct {p0}, LX/1vH;->c()Ljava/io/FileInputStream;

    move-result-object v0

    return-object v0
.end method

.method public final b()[B
    .locals 4

    .prologue
    .line 342121
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 342122
    :try_start_0
    invoke-direct {p0}, LX/1vH;->c()Ljava/io/FileInputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/FileInputStream;

    .line 342123
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/1t3;->a(Ljava/io/InputStream;J)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 342124
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-object v0

    .line 342125
    :catch_0
    move-exception v0

    .line 342126
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342127
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 342128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Files.asByteSource("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/1vH;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
