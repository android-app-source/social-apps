.class public LX/1wf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1wg;

.field public final e:LX/1wi;

.field public final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/1wg;LX/1wi;Ljava/lang/Class;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/http/annotations/UserAgentString;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Class;
        .annotation runtime Lcom/facebook/appupdate/integration/common/AppUpdateActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1wg;",
            "Lcom/facebook/appupdate/AppUpdateNotificationsHandler;",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346564
    iput-object p1, p0, LX/1wf;->a:Landroid/content/Context;

    .line 346565
    iput-object p2, p0, LX/1wf;->b:LX/0Or;

    .line 346566
    iput-object p3, p0, LX/1wf;->c:LX/0Or;

    .line 346567
    iput-object p4, p0, LX/1wf;->d:LX/1wg;

    .line 346568
    iput-object p5, p0, LX/1wf;->e:LX/1wi;

    .line 346569
    iput-object p6, p0, LX/1wf;->f:Ljava/lang/Class;

    .line 346570
    return-void
.end method

.method public static b(LX/0QB;)LX/1wf;
    .locals 9

    .prologue
    .line 346571
    new-instance v0, LX/1wf;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x19e

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x15f5

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 346572
    new-instance v8, LX/1wg;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v6

    check-cast v6, LX/0if;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v7

    check-cast v7, LX/0lC;

    invoke-direct {v8, v4, v5, v6, v7}, LX/1wg;-><init>(LX/0Zb;LX/03V;LX/0if;LX/0lC;)V

    .line 346573
    move-object v4, v8

    .line 346574
    check-cast v4, LX/1wg;

    invoke-static {p0}, LX/1wi;->b(LX/0QB;)LX/1wi;

    move-result-object v5

    check-cast v5, LX/1wi;

    .line 346575
    const-class v6, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    move-object v6, v6

    .line 346576
    move-object v6, v6

    .line 346577
    check-cast v6, Ljava/lang/Class;

    invoke-direct/range {v0 .. v6}, LX/1wf;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/1wg;LX/1wi;Ljava/lang/Class;)V

    .line 346578
    return-object v0
.end method
