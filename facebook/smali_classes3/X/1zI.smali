.class public final LX/1zI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1zA;

.field private final b:Ljava/lang/String;

.field private final c:LX/1PT;


# direct methods
.method public constructor <init>(LX/1zA;Lcom/facebook/graphql/model/GraphQLStory;ZLX/1PT;)V
    .locals 1

    .prologue
    .line 353083
    iput-object p1, p0, LX/1zI;->a:LX/1zA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353084
    invoke-static {p2, p3}, LX/1zJ;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1zI;->b:Ljava/lang/String;

    .line 353085
    iput-object p4, p0, LX/1zI;->c:LX/1PT;

    .line 353086
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 353087
    const/4 v4, 0x0

    .line 353088
    iget-object v0, p0, LX/1zI;->a:LX/1zA;

    iget-object v0, v0, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 353089
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 353090
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 353091
    invoke-static {v0}, LX/1zA;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353092
    new-instance v0, LX/1yT;

    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v4}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 353093
    :goto_0
    return-object v0

    .line 353094
    :cond_0
    iget-object v1, p0, LX/1zI;->a:LX/1zA;

    .line 353095
    iget-object v2, v1, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 353096
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 353097
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/1zA;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 353098
    if-eqz v2, :cond_1

    :goto_1
    move-object v1, v2

    .line 353099
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-static {v1}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 353100
    iget-object v1, p0, LX/1zI;->a:LX/1zA;

    iget-object v1, v1, LX/1zA;->c:LX/1zC;

    iget-object v3, p0, LX/1zI;->a:LX/1zA;

    iget v3, v3, LX/1zA;->g:I

    invoke-virtual {v1, v2, v3}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 353101
    iget-object v1, p0, LX/1zI;->a:LX/1zA;

    iget-object v1, v1, LX/1zA;->d:LX/1xc;

    invoke-virtual {v1, v0, v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/text/Spannable;)V

    .line 353102
    new-instance v0, LX/1yT;

    invoke-direct {v0, v2, v4}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    goto :goto_0

    :cond_1
    iget-object v2, v1, LX/1zA;->e:LX/1nA;

    iget-object v3, v1, LX/1zA;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v5, v1, LX/1zA;->h:I

    iget-boolean v6, v1, LX/1zA;->i:Z

    invoke-virtual {v2, v3, v5, v6}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;IZ)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353103
    iget-object v0, p0, LX/1zI;->b:Ljava/lang/String;

    return-object v0
.end method
