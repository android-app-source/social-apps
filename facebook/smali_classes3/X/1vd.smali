.class public final LX/1vd;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1vb;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1vc;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 343345
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 343346
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "menuConfig"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "feedProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "menuHelper"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1vd;->b:[Ljava/lang/String;

    .line 343347
    iput v3, p0, LX/1vd;->c:I

    .line 343348
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1vd;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1vd;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1vd;LX/1De;IILX/1vc;)V
    .locals 1

    .prologue
    .line 343318
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 343319
    iput-object p4, p0, LX/1vd;->a:LX/1vc;

    .line 343320
    iget-object v0, p0, LX/1vd;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 343321
    return-void
.end method


# virtual methods
.method public final a(LX/1SX;)LX/1vd;
    .locals 2

    .prologue
    .line 343342
    iget-object v0, p0, LX/1vd;->a:LX/1vc;

    iput-object p1, v0, LX/1vc;->e:LX/1SX;

    .line 343343
    iget-object v0, p0, LX/1vd;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 343344
    return-object p0
.end method

.method public final a(LX/1dl;)LX/1vd;
    .locals 2

    .prologue
    .line 343339
    iget-object v0, p0, LX/1vd;->a:LX/1vc;

    iput-object p1, v0, LX/1vc;->a:LX/1dl;

    .line 343340
    iget-object v0, p0, LX/1vd;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 343341
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/1vd;"
        }
    .end annotation

    .prologue
    .line 343336
    iget-object v0, p0, LX/1vd;->a:LX/1vc;

    iput-object p1, v0, LX/1vc;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343337
    iget-object v0, p0, LX/1vd;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 343338
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 343332
    invoke-super {p0}, LX/1X5;->a()V

    .line 343333
    const/4 v0, 0x0

    iput-object v0, p0, LX/1vd;->a:LX/1vc;

    .line 343334
    sget-object v0, LX/1vb;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 343335
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1vb;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 343322
    iget-object v1, p0, LX/1vd;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1vd;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1vd;->c:I

    if-ge v1, v2, :cond_2

    .line 343323
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 343324
    :goto_0
    iget v2, p0, LX/1vd;->c:I

    if-ge v0, v2, :cond_1

    .line 343325
    iget-object v2, p0, LX/1vd;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 343326
    iget-object v2, p0, LX/1vd;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343327
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343328
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 343329
    :cond_2
    iget-object v0, p0, LX/1vd;->a:LX/1vc;

    .line 343330
    invoke-virtual {p0}, LX/1vd;->a()V

    .line 343331
    return-object v0
.end method
