.class public LX/21s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/21s;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 358478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358479
    return-void
.end method

.method public static a(LX/0QB;)LX/21s;
    .locals 3

    .prologue
    .line 358480
    sget-object v0, LX/21s;->a:LX/21s;

    if-nez v0, :cond_1

    .line 358481
    const-class v1, LX/21s;

    monitor-enter v1

    .line 358482
    :try_start_0
    sget-object v0, LX/21s;->a:LX/21s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 358483
    if-eqz v2, :cond_0

    .line 358484
    :try_start_1
    new-instance v0, LX/21s;

    invoke-direct {v0}, LX/21s;-><init>()V

    .line 358485
    move-object v0, v0

    .line 358486
    sput-object v0, LX/21s;->a:LX/21s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 358488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 358489
    :cond_1
    sget-object v0, LX/21s;->a:LX/21s;

    return-object v0

    .line 358490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 358491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;II)Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 358492
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p3, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 358493
    new-instance v1, LX/82R;

    invoke-direct {v1, p0, p1}, LX/82R;-><init>(LX/21s;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 358494
    new-instance v1, LX/82S;

    invoke-direct {v1, p0, p1, p3, p2}, LX/82S;-><init>(LX/21s;Landroid/view/View;II)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 358495
    return-object v0
.end method
