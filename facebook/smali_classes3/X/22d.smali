.class public final LX/22d;
.super Ljava/util/AbstractList;
.source ""

# interfaces
.implements LX/22e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<THelper:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractList",
        "<TTItem;>;",
        "LX/22e",
        "<TTItem;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:I

.field private final c:I

.field private final d:LX/1VQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VQ",
            "<TTHelper;TTItem;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTHelper;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;IILX/1VQ;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "LX/1VQ",
            "<TTHelper;TTItem;>;TTHelper;)V"
        }
    .end annotation

    .prologue
    .line 360166
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 360167
    iput-object p1, p0, LX/22d;->a:Ljava/nio/ByteBuffer;

    .line 360168
    iput p2, p0, LX/22d;->b:I

    .line 360169
    iput p3, p0, LX/22d;->c:I

    .line 360170
    iput-object p4, p0, LX/22d;->d:LX/1VQ;

    .line 360171
    iput-object p5, p0, LX/22d;->e:Ljava/lang/Object;

    .line 360172
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTItem;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 360173
    if-ltz p1, :cond_0

    iget v0, p0, LX/22d;->c:I

    if-lt p1, v0, :cond_1

    .line 360174
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 360175
    :cond_1
    iget-object v0, p0, LX/22d;->d:LX/1VQ;

    iget-object v1, p0, LX/22d;->e:Ljava/lang/Object;

    iget-object v2, p0, LX/22d;->a:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/22d;->b:I

    invoke-interface {v0, v1, v2, v3, p1}, LX/1VQ;->a(Ljava/lang/Object;Ljava/nio/ByteBuffer;II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 360176
    iget v0, p0, LX/22d;->c:I

    return v0
.end method
