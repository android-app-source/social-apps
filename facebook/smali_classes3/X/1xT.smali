.class public LX/1xT;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xT",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348450
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348451
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xT;->b:LX/0Zi;

    .line 348452
    iput-object p1, p0, LX/1xT;->a:LX/0Ot;

    .line 348453
    return-void
.end method

.method public static a(LX/0QB;)LX/1xT;
    .locals 4

    .prologue
    .line 348454
    const-class v1, LX/1xT;

    monitor-enter v1

    .line 348455
    :try_start_0
    sget-object v0, LX/1xT;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348456
    sput-object v2, LX/1xT;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348457
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348458
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348459
    new-instance v3, LX/1xT;

    const/16 p0, 0x72c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xT;-><init>(LX/0Ot;)V

    .line 348460
    move-object v0, v3

    .line 348461
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348462
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348463
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 348465
    check-cast p2, LX/1xX;

    .line 348466
    iget-object v0, p0, LX/1xT;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;

    iget-object v1, p2, LX/1xX;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/1xX;->b:LX/1Po;

    const/4 v4, 0x0

    .line 348467
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 348468
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    .line 348469
    const/4 v3, 0x0

    .line 348470
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->j:LX/03R;

    sget-object p0, LX/03R;->UNSET:LX/03R;

    if-ne v6, p0, :cond_0

    .line 348471
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->f:LX/0ad;

    sget-short p0, LX/0wf;->aI:S

    invoke-interface {v6, p0, v3}, LX/0ad;->a(SZ)Z

    move-result v6

    invoke-static {v6}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v6

    iput-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->j:LX/03R;

    .line 348472
    :cond_0
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->j:LX/03R;

    invoke-virtual {v6, v3}, LX/03R;->asBoolean(Z)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->g:LX/1Bv;

    invoke-virtual {v6}, LX/1Bv;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v3, 0x1

    :cond_1
    move v3, v3

    .line 348473
    if-eqz v3, :cond_6

    .line 348474
    invoke-static {v5}, LX/1xl;->g(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    .line 348475
    :goto_0
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 348476
    iget-object v3, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->e:LX/1xZ;

    const/4 v6, 0x0

    .line 348477
    new-instance p0, LX/BsM;

    invoke-direct {p0, v3}, LX/BsM;-><init>(LX/1xZ;)V

    .line 348478
    iget-object p2, v3, LX/1xZ;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/BsN;

    .line 348479
    if-nez p2, :cond_2

    .line 348480
    new-instance p2, LX/BsN;

    invoke-direct {p2, v3}, LX/BsN;-><init>(LX/1xZ;)V

    .line 348481
    :cond_2
    invoke-static {p2, p1, v6, v6, p0}, LX/BsN;->a$redex0(LX/BsN;LX/1De;IILX/BsM;)V

    .line 348482
    move-object p0, p2

    .line 348483
    move-object v6, p0

    .line 348484
    move-object v3, v6

    .line 348485
    iget-object v6, v3, LX/BsN;->a:LX/BsM;

    iput-object v1, v6, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 348486
    iget-object v6, v3, LX/BsN;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 348487
    move-object v3, v3

    .line 348488
    iget-object v6, v3, LX/BsN;->a:LX/BsM;

    iput-object v2, v6, LX/BsM;->b:LX/1Po;

    .line 348489
    iget-object v6, v3, LX/BsN;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 348490
    move-object v3, v3

    .line 348491
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v6, 0x7f020a91

    invoke-interface {v3, v6}, LX/1Di;->z(I)LX/1Di;

    move-result-object v3

    .line 348492
    :goto_1
    if-eqz v5, :cond_3

    .line 348493
    const v4, -0x3d8bd4c0

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 348494
    :cond_3
    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Di;->f(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    sget-object v4, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->b:Landroid/util/SparseArray;

    invoke-interface {v3, v4}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 348495
    return-object v0

    .line 348496
    :cond_4
    invoke-static {v5}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    .line 348497
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 348498
    const/4 v3, 0x0

    .line 348499
    :goto_2
    move-object v3, v3

    .line 348500
    invoke-static {v1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    .line 348501
    iget-object p0, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->i:LX/0pJ;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, LX/0pJ;->c(Z)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 348502
    if-eqz v3, :cond_5

    if-eqz v6, :cond_5

    .line 348503
    iget-object p0, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->h:LX/1BM;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, v6, p2}, LX/1BM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 348504
    :cond_5
    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->d:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    sget-object p0, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-static {v5}, LX/1xl;->e(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/1nw;->b(Ljava/lang/String;)LX/1nw;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v3

    check-cast v2, LX/1Pp;

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    const v6, 0x7f02111f

    invoke-virtual {v3, v6}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    const v6, 0x7f020a91

    invoke-virtual {v3, v6}, LX/1nw;->l(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto :goto_1

    :cond_6
    move-object v3, v4

    goto/16 :goto_0

    :cond_7
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 348505
    invoke-static {}, LX/1dS;->b()V

    .line 348506
    iget v0, p1, LX/1dQ;->b:I

    .line 348507
    packed-switch v0, :pswitch_data_0

    .line 348508
    :goto_0
    return-object v2

    .line 348509
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 348510
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 348511
    check-cast v1, LX/1xX;

    .line 348512
    iget-object v3, p0, LX/1xT;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;

    iget-object p1, v1, LX/1xX;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 348513
    iget-object p0, v3, Lcom/facebook/feed/rows/sections/header/components/HeaderActorComponentSpec;->c:LX/1xa;

    .line 348514
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 348515
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p2}, LX/1y3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    move-result-object p2

    invoke-static {p1}, LX/6XU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/6XU;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, LX/1xa;->a(Landroid/view/View;Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;LX/6XU;)V

    .line 348516
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x3d8bd4c0
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/1xY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xT",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348517
    new-instance v1, LX/1xX;

    invoke-direct {v1, p0}, LX/1xX;-><init>(LX/1xT;)V

    .line 348518
    iget-object v2, p0, LX/1xT;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1xY;

    .line 348519
    if-nez v2, :cond_0

    .line 348520
    new-instance v2, LX/1xY;

    invoke-direct {v2, p0}, LX/1xY;-><init>(LX/1xT;)V

    .line 348521
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/1xY;->a$redex0(LX/1xY;LX/1De;IILX/1xX;)V

    .line 348522
    move-object v1, v2

    .line 348523
    move-object v0, v1

    .line 348524
    return-object v0
.end method
