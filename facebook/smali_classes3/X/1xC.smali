.class public LX/1xC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0r;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0r;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347976
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 347977
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xC;->b:LX/0Zi;

    .line 347978
    iput-object p1, p0, LX/1xC;->a:LX/0Ot;

    .line 347979
    return-void
.end method

.method public static a(LX/0QB;)LX/1xC;
    .locals 4

    .prologue
    .line 347965
    const-class v1, LX/1xC;

    monitor-enter v1

    .line 347966
    :try_start_0
    sget-object v0, LX/1xC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 347967
    sput-object v2, LX/1xC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 347968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 347970
    new-instance v3, LX/1xC;

    const/16 p0, 0x1e82

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xC;-><init>(LX/0Ot;)V

    .line 347971
    move-object v0, v3

    .line 347972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 347973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 347975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 347980
    check-cast p2, LX/C0q;

    .line 347981
    iget-object v0, p0, LX/1xC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0r;

    iget-object v2, p2, LX/C0q;->a:LX/1Pb;

    iget-object v3, p2, LX/C0q;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-boolean v4, p2, LX/C0q;->c:Z

    iget-object v5, p2, LX/C0q;->d:LX/1f6;

    iget-object v6, p2, LX/C0q;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, p1

    const/4 p0, 0x0

    .line 347982
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/C0r;->a:LX/1xN;

    invoke-virtual {v8, v1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v8

    .line 347983
    new-instance v9, LX/1f5;

    invoke-direct {v9, v5}, LX/1f5;-><init>(LX/1f6;)V

    .line 347984
    iget-object p1, v5, LX/1f6;->b:LX/1bf;

    move-object p1, p1

    .line 347985
    invoke-static {v0, p1}, LX/C0r;->a(LX/C0r;LX/1bf;)LX/1bf;

    move-result-object p1

    .line 347986
    iput-object p1, v9, LX/1f5;->g:LX/1bf;

    .line 347987
    move-object v9, v9

    .line 347988
    iget-object p1, v5, LX/1f6;->c:LX/1bf;

    move-object p1, p1

    .line 347989
    invoke-static {v0, p1}, LX/C0r;->a(LX/C0r;LX/1bf;)LX/1bf;

    move-result-object p1

    .line 347990
    iput-object p1, v9, LX/1f5;->h:LX/1bf;

    .line 347991
    move-object v9, v9

    .line 347992
    iget-object p1, v5, LX/1f6;->a:[LX/1bf;

    move-object p1, p1

    .line 347993
    if-eqz p1, :cond_0

    .line 347994
    const/4 p2, 0x0

    :goto_0
    array-length v5, p1

    if-ge p2, v5, :cond_0

    .line 347995
    aget-object v5, p1, p2

    invoke-static {v0, v5}, LX/C0r;->a(LX/C0r;LX/1bf;)LX/1bf;

    move-result-object v5

    aput-object v5, p1, p2

    .line 347996
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 347997
    :cond_0
    move-object p1, p1

    .line 347998
    iput-object p1, v9, LX/1f5;->f:[LX/1bf;

    .line 347999
    move-object v9, v9

    .line 348000
    invoke-virtual {v9}, LX/1f5;->a()LX/1f6;

    move-result-object v9

    move-object v9, v9

    .line 348001
    iget-object p1, v8, LX/22O;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    iput-object v9, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    .line 348002
    move-object v8, v8

    .line 348003
    invoke-virtual {v8, p0}, LX/22O;->a(Z)LX/22O;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/C0r;->c:LX/Cch;

    invoke-virtual {v8, v1}, LX/Cch;->c(LX/1De;)LX/Ccf;

    move-result-object v8

    .line 348004
    iget-object v9, v8, LX/Ccf;->a:LX/Ccg;

    iput-boolean v4, v9, LX/Ccg;->c:Z

    .line 348005
    move-object v8, v8

    .line 348006
    check-cast v2, LX/1Pq;

    invoke-virtual {v8, v2}, LX/Ccf;->a(LX/1Pq;)LX/Ccf;

    move-result-object v8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/Ccf;->b(Ljava/lang/String;)LX/Ccf;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/Ccf;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Ccf;

    move-result-object v8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->am()Z

    move-result v9

    invoke-virtual {v8, v9}, LX/Ccf;->a(Z)LX/Ccf;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, LX/1Di;->c(I)LX/1Di;

    move-result-object v8

    const/16 v9, 0x8

    invoke-interface {v8, v9, p0}, LX/1Di;->k(II)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 348007
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 347963
    invoke-static {}, LX/1dS;->b()V

    .line 347964
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C0p;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xC",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 347955
    new-instance v1, LX/C0q;

    invoke-direct {v1, p0}, LX/C0q;-><init>(LX/1xC;)V

    .line 347956
    iget-object v2, p0, LX/1xC;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C0p;

    .line 347957
    if-nez v2, :cond_0

    .line 347958
    new-instance v2, LX/C0p;

    invoke-direct {v2, p0}, LX/C0p;-><init>(LX/1xC;)V

    .line 347959
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C0p;->a$redex0(LX/C0p;LX/1De;IILX/C0q;)V

    .line 347960
    move-object v1, v2

    .line 347961
    move-object v0, v1

    .line 347962
    return-object v0
.end method
