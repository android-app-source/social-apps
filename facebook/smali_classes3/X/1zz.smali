.class public final LX/1zz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;)V
    .locals 0

    .prologue
    .line 354309
    iput-object p1, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 354299
    iget-object v0, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->a:LX/1zq;

    .line 354300
    iget-object v4, v0, LX/1zq;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iput-wide v4, v0, LX/1zq;->d:J

    .line 354301
    iget-object v0, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v1, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->f:LX/1zp;

    iget-object v0, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget v2, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->b:F

    iget-object v0, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-boolean v3, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->c:Z

    iget-object v0, p0, LX/1zz;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-boolean v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    .line 354302
    :goto_0
    new-instance v4, LX/201;

    invoke-direct {v4}, LX/201;-><init>()V

    move-object v4, v4

    .line 354303
    const-string v5, "scale"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 354304
    const-string v5, "includeVectorData"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 354305
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    const-wide/32 v6, 0x15180

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 354306
    iget-object v5, v1, LX/1zp;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 354307
    new-instance v5, LX/202;

    invoke-direct {v5, v1}, LX/202;-><init>(LX/1zp;)V

    iget-object v6, v1, LX/1zp;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 354308
    return-object v0

    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
