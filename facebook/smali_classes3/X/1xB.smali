.class public final LX/1xB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1WL;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static ak:LX/0Xm;


# instance fields
.field public volatile A:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile B:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile C:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile D:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile E:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile F:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile G:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentVideoSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile H:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile I:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile J:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile K:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile L:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile M:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile N:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile O:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/list/ListAttachmentRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile P:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile Q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile R:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile S:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile T:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile U:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile V:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/CenteredTextComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile W:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile X:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile Y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile Z:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile aa:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ab:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ac:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ad:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ae:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile af:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ag:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ah:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile ai:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile aj:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile h:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/rooms/RoomLinkAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile k:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile m:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile n:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile o:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field public volatile p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareAttachmentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile r:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile s:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile t:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCardAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile v:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile w:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile x:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile y:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public volatile z:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 347877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347878
    return-void
.end method

.method public static a(LX/0QB;)LX/1xB;
    .locals 3

    .prologue
    .line 347879
    const-class v1, LX/1xB;

    monitor-enter v1

    .line 347880
    :try_start_0
    sget-object v0, LX/1xB;->ak:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 347881
    sput-object v2, LX/1xB;->ak:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 347882
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347883
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/1xB;->b(LX/0QB;)LX/1xB;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 347884
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347885
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 347886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/1xB;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1xB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/rooms/RoomLinkAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSelectorPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareAttachmentSelectorPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCardAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentVideoSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/list/ListAttachmentRootPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/CenteredTextComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347887
    iput-object p1, p0, LX/1xB;->a:LX/0Or;

    iput-object p2, p0, LX/1xB;->b:LX/0Or;

    iput-object p3, p0, LX/1xB;->c:LX/0Or;

    iput-object p4, p0, LX/1xB;->d:LX/0Or;

    iput-object p5, p0, LX/1xB;->e:LX/0Or;

    iput-object p6, p0, LX/1xB;->f:LX/0Or;

    iput-object p7, p0, LX/1xB;->g:LX/0Or;

    iput-object p8, p0, LX/1xB;->h:LX/0Or;

    iput-object p9, p0, LX/1xB;->i:LX/0Or;

    iput-object p10, p0, LX/1xB;->j:LX/0Or;

    iput-object p11, p0, LX/1xB;->k:LX/0Or;

    iput-object p12, p0, LX/1xB;->l:LX/0Or;

    iput-object p13, p0, LX/1xB;->m:LX/0Or;

    iput-object p14, p0, LX/1xB;->n:LX/0Or;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/1xB;->o:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/1xB;->p:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/1xB;->q:LX/0Or;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/1xB;->r:LX/0Or;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/1xB;->s:LX/0Or;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/1xB;->t:LX/0Or;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/1xB;->u:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/1xB;->v:LX/0Or;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/1xB;->w:LX/0Or;

    move-object/from16 v0, p24

    iput-object v0, p0, LX/1xB;->x:LX/0Or;

    move-object/from16 v0, p25

    iput-object v0, p0, LX/1xB;->y:LX/0Or;

    move-object/from16 v0, p26

    iput-object v0, p0, LX/1xB;->z:LX/0Or;

    move-object/from16 v0, p27

    iput-object v0, p0, LX/1xB;->A:LX/0Or;

    move-object/from16 v0, p28

    iput-object v0, p0, LX/1xB;->B:LX/0Or;

    move-object/from16 v0, p29

    iput-object v0, p0, LX/1xB;->C:LX/0Or;

    move-object/from16 v0, p30

    iput-object v0, p0, LX/1xB;->D:LX/0Or;

    move-object/from16 v0, p31

    iput-object v0, p0, LX/1xB;->E:LX/0Or;

    move-object/from16 v0, p32

    iput-object v0, p0, LX/1xB;->F:LX/0Or;

    move-object/from16 v0, p33

    iput-object v0, p0, LX/1xB;->G:LX/0Or;

    move-object/from16 v0, p34

    iput-object v0, p0, LX/1xB;->H:LX/0Or;

    move-object/from16 v0, p35

    iput-object v0, p0, LX/1xB;->I:LX/0Or;

    move-object/from16 v0, p36

    iput-object v0, p0, LX/1xB;->J:LX/0Or;

    move-object/from16 v0, p37

    iput-object v0, p0, LX/1xB;->K:LX/0Or;

    move-object/from16 v0, p38

    iput-object v0, p0, LX/1xB;->L:LX/0Or;

    move-object/from16 v0, p39

    iput-object v0, p0, LX/1xB;->M:LX/0Or;

    move-object/from16 v0, p40

    iput-object v0, p0, LX/1xB;->N:LX/0Or;

    move-object/from16 v0, p41

    iput-object v0, p0, LX/1xB;->O:LX/0Or;

    move-object/from16 v0, p42

    iput-object v0, p0, LX/1xB;->P:LX/0Or;

    move-object/from16 v0, p43

    iput-object v0, p0, LX/1xB;->Q:LX/0Or;

    move-object/from16 v0, p44

    iput-object v0, p0, LX/1xB;->R:LX/0Or;

    move-object/from16 v0, p45

    iput-object v0, p0, LX/1xB;->S:LX/0Or;

    move-object/from16 v0, p46

    iput-object v0, p0, LX/1xB;->T:LX/0Or;

    move-object/from16 v0, p47

    iput-object v0, p0, LX/1xB;->U:LX/0Or;

    move-object/from16 v0, p48

    iput-object v0, p0, LX/1xB;->V:LX/0Or;

    move-object/from16 v0, p49

    iput-object v0, p0, LX/1xB;->W:LX/0Or;

    move-object/from16 v0, p50

    iput-object v0, p0, LX/1xB;->X:LX/0Or;

    move-object/from16 v0, p51

    iput-object v0, p0, LX/1xB;->Y:LX/0Or;

    move-object/from16 v0, p52

    iput-object v0, p0, LX/1xB;->Z:LX/0Or;

    move-object/from16 v0, p53

    iput-object v0, p0, LX/1xB;->aa:LX/0Or;

    move-object/from16 v0, p54

    iput-object v0, p0, LX/1xB;->ab:LX/0Or;

    move-object/from16 v0, p55

    iput-object v0, p0, LX/1xB;->ac:LX/0Or;

    move-object/from16 v0, p56

    iput-object v0, p0, LX/1xB;->ad:LX/0Or;

    move-object/from16 v0, p57

    iput-object v0, p0, LX/1xB;->ae:LX/0Or;

    move-object/from16 v0, p58

    iput-object v0, p0, LX/1xB;->af:LX/0Or;

    move-object/from16 v0, p59

    iput-object v0, p0, LX/1xB;->ag:LX/0Or;

    move-object/from16 v0, p60

    iput-object v0, p0, LX/1xB;->ah:LX/0Or;

    move-object/from16 v0, p61

    iput-object v0, p0, LX/1xB;->ai:LX/0Or;

    move-object/from16 v0, p62

    iput-object v0, p0, LX/1xB;->aj:LX/0Or;

    return-void
.end method

.method private static b(LX/0QB;)LX/1xB;
    .locals 65

    .prologue
    .line 347888
    new-instance v2, LX/1xB;

    invoke-direct {v2}, LX/1xB;-><init>()V

    .line 347889
    const/16 v3, 0x2301

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xa4e

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x21e7

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x21dd

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x218c

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x218b

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x218a

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x2164

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x2156

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x2152

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x20aa

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x20a5

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x2076

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0x204d

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x2044

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x981

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const/16 v19, 0x2001

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const/16 v20, 0x940

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0x93b

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    const/16 v22, 0x932

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    const/16 v23, 0x1f33

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    const/16 v24, 0x1f07

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    const/16 v25, 0x1f02

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    const/16 v26, 0x1f01

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    const/16 v27, 0x1f00

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    const/16 v28, 0x8d9

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v28

    const/16 v29, 0x8d8

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v29

    const/16 v30, 0x8c2

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    const/16 v31, 0x1e93

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v31

    const/16 v32, 0x82f

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v32

    const/16 v33, 0x1e7e

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v33

    const/16 v34, 0x1e6f

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v34

    const/16 v35, 0x1e60

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v35

    const/16 v36, 0x816

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v36

    const/16 v37, 0x1e5d

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v37

    const/16 v38, 0x1e5c

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v38

    const/16 v39, 0x1e4c

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v39

    const/16 v40, 0x1e4a

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v40

    const/16 v41, 0x81b

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v41

    const/16 v42, 0x1e3d

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v42

    const/16 v43, 0x1e7c

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v43

    const/16 v44, 0x1e30

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v44

    const/16 v45, 0x1e2d

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v45

    const/16 v46, 0x1e2a

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v46

    const/16 v47, 0x1e26

    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v47

    const/16 v48, 0x1e1f

    move-object/from16 v0, p0

    move/from16 v1, v48

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v48

    const/16 v49, 0x1e1a

    move-object/from16 v0, p0

    move/from16 v1, v49

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v49

    const/16 v50, 0x1e19

    move-object/from16 v0, p0

    move/from16 v1, v50

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v50

    const/16 v51, 0x1e18

    move-object/from16 v0, p0

    move/from16 v1, v51

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v51

    const/16 v52, 0x1e11

    move-object/from16 v0, p0

    move/from16 v1, v52

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v52

    const/16 v53, 0x7da

    move-object/from16 v0, p0

    move/from16 v1, v53

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v53

    const/16 v54, 0x7f5

    move-object/from16 v0, p0

    move/from16 v1, v54

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v54

    const/16 v55, 0x7f0

    move-object/from16 v0, p0

    move/from16 v1, v55

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v55

    const/16 v56, 0x1e3a

    move-object/from16 v0, p0

    move/from16 v1, v56

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v56

    const/16 v57, 0x7ee

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v57

    const/16 v58, 0x6f2

    move-object/from16 v0, p0

    move/from16 v1, v58

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v58

    const/16 v59, 0x1d19

    move-object/from16 v0, p0

    move/from16 v1, v59

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v59

    const/16 v60, 0x1d16

    move-object/from16 v0, p0

    move/from16 v1, v60

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v60

    const/16 v61, 0x1d15

    move-object/from16 v0, p0

    move/from16 v1, v61

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v61

    const/16 v62, 0x6f1

    move-object/from16 v0, p0

    move/from16 v1, v62

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v62

    const/16 v63, 0x221

    move-object/from16 v0, p0

    move/from16 v1, v63

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v63

    const/16 v64, 0x21d

    move-object/from16 v0, p0

    move/from16 v1, v64

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v64

    invoke-static/range {v2 .. v64}, LX/1xB;->a(LX/1xB;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 347890
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)LX/1RB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ")",
            "LX/1RB",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 347891
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 347892
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 347893
    :sswitch_0
    iget-object v0, p0, LX/1xB;->aj:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentPartDefinition;

    goto :goto_0

    .line 347894
    :sswitch_1
    iget-object v0, p0, LX/1xB;->ai:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    goto :goto_0

    .line 347895
    :sswitch_2
    iget-object v0, p0, LX/1xB;->ah:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentPartDefinition;

    goto :goto_0

    .line 347896
    :sswitch_3
    iget-object v0, p0, LX/1xB;->ag:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;

    goto :goto_0

    .line 347897
    :sswitch_4
    iget-object v0, p0, LX/1xB;->af:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;

    goto :goto_0

    .line 347898
    :sswitch_5
    iget-object v0, p0, LX/1xB;->ae:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;

    goto :goto_0

    .line 347899
    :sswitch_6
    iget-object v0, p0, LX/1xB;->ad:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/places/LocationMultiRowAttachmentSelectorPartDefinition;

    goto :goto_0

    .line 347900
    :sswitch_7
    iget-object v0, p0, LX/1xB;->ac:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/album/AlbumAttachmentSelector;

    goto :goto_0

    .line 347901
    :sswitch_8
    iget-object v0, p0, LX/1xB;->ab:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;

    goto :goto_0

    .line 347902
    :sswitch_9
    iget-object v0, p0, LX/1xB;->aa:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;

    goto :goto_0

    .line 347903
    :sswitch_a
    iget-object v0, p0, LX/1xB;->Z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    goto :goto_0

    .line 347904
    :sswitch_b
    iget-object v0, p0, LX/1xB;->Y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/AvatarAttachmentPartDefinition;

    goto :goto_0

    .line 347905
    :sswitch_c
    iget-object v0, p0, LX/1xB;->X:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/AvatarListAttachmentPartDefinition;

    goto :goto_0

    .line 347906
    :sswitch_d
    iget-object v0, p0, LX/1xB;->W:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/BirthdayAvatarAttachmentSelector;

    goto :goto_0

    .line 347907
    :sswitch_e
    iget-object v0, p0, LX/1xB;->V:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/CenteredTextComponentPartDefinition;

    goto/16 :goto_0

    .line 347908
    :sswitch_f
    iget-object v0, p0, LX/1xB;->U:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/CouponAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347909
    :sswitch_10
    iget-object v0, p0, LX/1xB;->T:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347910
    :sswitch_11
    iget-object v0, p0, LX/1xB;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347911
    :sswitch_12
    iget-object v0, p0, LX/1xB;->R:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/NoteAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347912
    :sswitch_13
    iget-object v0, p0, LX/1xB;->Q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/PollAttachmentSelectorPartDefinition;

    goto/16 :goto_0

    .line 347913
    :sswitch_14
    iget-object v0, p0, LX/1xB;->P:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/SportsMatchAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347914
    :sswitch_15
    iget-object v0, p0, LX/1xB;->O:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/list/ListAttachmentRootPartDefinition;

    goto/16 :goto_0

    .line 347915
    :sswitch_16
    iget-object v0, p0, LX/1xB;->N:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/lifeevent/LifeEventAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347916
    :sswitch_17
    iget-object v0, p0, LX/1xB;->M:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/externalgallery/ExternalGalleryAttachmentComponentPartDefinition;

    goto/16 :goto_0

    .line 347917
    :sswitch_18
    iget-object v0, p0, LX/1xB;->L:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/InstagramAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347918
    :sswitch_19
    iget-object v0, p0, LX/1xB;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/LargeImageAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347919
    :sswitch_1a
    iget-object v0, p0, LX/1xB;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareAttachmentGroupDefinition;

    goto/16 :goto_0

    .line 347920
    :sswitch_1b
    iget-object v0, p0, LX/1xB;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/QuoteShareQuoteOnlyGroupDefinition;

    goto/16 :goto_0

    .line 347921
    :sswitch_1c
    iget-object v0, p0, LX/1xB;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    goto/16 :goto_0

    .line 347922
    :sswitch_1d
    iget-object v0, p0, LX/1xB;->G:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentVideoSelector;

    goto/16 :goto_0

    .line 347923
    :sswitch_1e
    iget-object v0, p0, LX/1xB;->F:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/linkshare/VideoShareHighlightedAttachmentSelector;

    goto/16 :goto_0

    .line 347924
    :sswitch_1f
    iget-object v0, p0, LX/1xB;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/meme/MemeAttachmentComponentPartDefinition;

    goto/16 :goto_0

    .line 347925
    :sswitch_20
    iget-object v0, p0, LX/1xB;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentSelector;

    goto/16 :goto_0

    .line 347926
    :sswitch_21
    iget-object v0, p0, LX/1xB;->C:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentComponentPartDefinition;

    goto/16 :goto_0

    .line 347927
    :sswitch_22
    iget-object v0, p0, LX/1xB;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347928
    :sswitch_23
    iget-object v0, p0, LX/1xB;->A:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347929
    :sswitch_24
    iget-object v0, p0, LX/1xB;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentHolidayCardAttachmentComponentPartDefinition;

    goto/16 :goto_0

    .line 347930
    :sswitch_25
    iget-object v0, p0, LX/1xB;->y:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347931
    :sswitch_26
    iget-object v0, p0, LX/1xB;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347932
    :sswitch_27
    iget-object v0, p0, LX/1xB;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347933
    :sswitch_28
    iget-object v0, p0, LX/1xB;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347934
    :sswitch_29
    iget-object v0, p0, LX/1xB;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCardAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347935
    :sswitch_2a
    iget-object v0, p0, LX/1xB;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347936
    :sswitch_2b
    iget-object v0, p0, LX/1xB;->s:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347937
    :sswitch_2c
    iget-object v0, p0, LX/1xB;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceItemCTAAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347938
    :sswitch_2d
    iget-object v0, p0, LX/1xB;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347939
    :sswitch_2e
    iget-object v0, p0, LX/1xB;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareAttachmentSelectorPartDefinition;

    goto/16 :goto_0

    .line 347940
    :sswitch_2f
    iget-object v0, p0, LX/1xB;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/partselector/MusicStoryAttachmentPartSelector;

    goto/16 :goto_0

    .line 347941
    :sswitch_30
    iget-object v0, p0, LX/1xB;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347942
    :sswitch_31
    iget-object v0, p0, LX/1xB;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;

    goto/16 :goto_0

    .line 347943
    :sswitch_32
    iget-object v0, p0, LX/1xB;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347944
    :sswitch_33
    iget-object v0, p0, LX/1xB;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347945
    :sswitch_34
    iget-object v0, p0, LX/1xB;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaAttachmentSelectorPartDefinition;

    goto/16 :goto_0

    .line 347946
    :sswitch_35
    iget-object v0, p0, LX/1xB;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richmedia/RichMediaCollectionSelectorPartDefinition;

    goto/16 :goto_0

    .line 347947
    :sswitch_36
    iget-object v0, p0, LX/1xB;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/rooms/RoomLinkAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347948
    :sswitch_37
    iget-object v0, p0, LX/1xB;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserForStoryAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347949
    :sswitch_38
    iget-object v0, p0, LX/1xB;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserPageAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347950
    :sswitch_39
    iget-object v0, p0, LX/1xB;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/socialgood/FundraiserPersonToCharityAttachmentGroupPartDefinition;

    goto/16 :goto_0

    .line 347951
    :sswitch_3a
    iget-object v0, p0, LX/1xB;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/tarot/TarotDigestComponentPartDefinition;

    goto/16 :goto_0

    .line 347952
    :sswitch_3b
    iget-object v0, p0, LX/1xB;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/video/AvatarVideoAttachmentPartDefinition;

    goto/16 :goto_0

    .line 347953
    :sswitch_3c
    iget-object v0, p0, LX/1xB;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/video/VideoAttachmentGroupDefinition;

    goto/16 :goto_0

    .line 347954
    :sswitch_3d
    iget-object v0, p0, LX/1xB;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayFeedAttachmentPartDefinition;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_31
        0x2 -> :sswitch_1c
        0x3 -> :sswitch_19
        0x4 -> :sswitch_20
        0x6 -> :sswitch_7
        0x8 -> :sswitch_f
        0xb -> :sswitch_13
        0xe -> :sswitch_1c
        0x12 -> :sswitch_2f
        0x13 -> :sswitch_15
        0x19 -> :sswitch_c
        0x1a -> :sswitch_b
        0x1c -> :sswitch_3b
        0x1d -> :sswitch_10
        0x1f -> :sswitch_10
        0x22 -> :sswitch_16
        0x24 -> :sswitch_1c
        0x25 -> :sswitch_8
        0x26 -> :sswitch_8
        0x27 -> :sswitch_12
        0x29 -> :sswitch_4
        0x2b -> :sswitch_5
        0x2f -> :sswitch_3c
        0x31 -> :sswitch_3c
        0x32 -> :sswitch_1d
        0x33 -> :sswitch_1e
        0x35 -> :sswitch_3c
        0x36 -> :sswitch_3c
        0x38 -> :sswitch_6
        0x41 -> :sswitch_2e
        0x42 -> :sswitch_2e
        0x43 -> :sswitch_2e
        0x45 -> :sswitch_2e
        0x49 -> :sswitch_20
        0x4b -> :sswitch_d
        0x4e -> :sswitch_2b
        0x4f -> :sswitch_2c
        0x50 -> :sswitch_14
        0x51 -> :sswitch_3d
        0x55 -> :sswitch_2a
        0x58 -> :sswitch_a
        0x6c -> :sswitch_1c
        0x79 -> :sswitch_3c
        0x7d -> :sswitch_3
        0x7e -> :sswitch_34
        0x7f -> :sswitch_18
        0x90 -> :sswitch_11
        0x93 -> :sswitch_9
        0x94 -> :sswitch_9
        0x95 -> :sswitch_38
        0x96 -> :sswitch_39
        0x97 -> :sswitch_25
        0x98 -> :sswitch_25
        0x99 -> :sswitch_26
        0x9a -> :sswitch_26
        0x9b -> :sswitch_27
        0x9c -> :sswitch_29
        0x9d -> :sswitch_28
        0xa2 -> :sswitch_23
        0xaf -> :sswitch_1a
        0xb1 -> :sswitch_e
        0xb5 -> :sswitch_1b
        0xb9 -> :sswitch_0
        0xbc -> :sswitch_2d
        0xbd -> :sswitch_2d
        0xc6 -> :sswitch_22
        0xc8 -> :sswitch_30
        0xca -> :sswitch_33
        0xcb -> :sswitch_2
        0xcd -> :sswitch_21
        0xcf -> :sswitch_32
        0xd3 -> :sswitch_36
        0xd8 -> :sswitch_35
        0xde -> :sswitch_1
        0xe3 -> :sswitch_37
        0xee -> :sswitch_17
        0xef -> :sswitch_1f
        0xf6 -> :sswitch_39
        0xf8 -> :sswitch_3a
        0xfa -> :sswitch_24
    .end sparse-switch
.end method
