.class public LX/1vx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Uh;

.field private final b:LX/0xX;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0xX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344857
    iput-object p1, p0, LX/1vx;->a:LX/0Uh;

    .line 344858
    iput-object p2, p0, LX/1vx;->b:LX/0xX;

    .line 344859
    return-void
.end method

.method public static a(LX/0QB;)LX/1vx;
    .locals 5

    .prologue
    .line 344860
    const-class v1, LX/1vx;

    monitor-enter v1

    .line 344861
    :try_start_0
    sget-object v0, LX/1vx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 344862
    sput-object v2, LX/1vx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 344863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 344865
    new-instance p0, LX/1vx;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v4

    check-cast v4, LX/0xX;

    invoke-direct {p0, v3, v4}, LX/1vx;-><init>(LX/0Uh;LX/0xX;)V

    .line 344866
    move-object v0, p0

    .line 344867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 344868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1vx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 344870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 344871
    iget-object v2, p0, LX/1vx;->b:LX/0xX;

    .line 344872
    iget-object v3, v2, LX/0xX;->c:LX/0ad;

    sget v4, LX/0xY;->p:I

    const/4 v5, -0x1

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v2, v3

    .line 344873
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 344874
    iget-object v0, p0, LX/1vx;->a:LX/0Uh;

    sget v2, LX/2SU;->N:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 344875
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method
