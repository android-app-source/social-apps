.class public LX/20o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9F0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 356074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356075
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 356076
    iput-object v0, p0, LX/20o;->a:LX/0Ot;

    .line 356077
    return-void
.end method

.method public static a(LX/0QB;)LX/20o;
    .locals 4

    .prologue
    .line 356061
    const-class v1, LX/20o;

    monitor-enter v1

    .line 356062
    :try_start_0
    sget-object v0, LX/20o;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356063
    sput-object v2, LX/20o;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356064
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356065
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 356066
    new-instance v3, LX/20o;

    invoke-direct {v3}, LX/20o;-><init>()V

    .line 356067
    const/16 p0, 0x1da9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 356068
    iput-object p0, v3, LX/20o;->a:LX/0Ot;

    .line 356069
    move-object v0, v3

    .line 356070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
