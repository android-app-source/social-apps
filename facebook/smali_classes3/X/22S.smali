.class public final LX/22S;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/22R;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/22R",
            "<TE;>.PhotoAttachmentImageComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/22R;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/22R;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 359732
    iput-object p1, p0, LX/22S;->b:LX/22R;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 359733
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "stateKey"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "media"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "photoAttachmentInfo"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "draweeController"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/22S;->c:[Ljava/lang/String;

    .line 359734
    iput v3, p0, LX/22S;->d:I

    .line 359735
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/22S;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/22S;LX/1De;IILcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/22R",
            "<TE;>.PhotoAttachmentImageComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 359739
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 359740
    iput-object p4, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    .line 359741
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 359742
    return-void
.end method


# virtual methods
.method public final a(LX/1Pr;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359715
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    .line 359716
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359717
    return-object p0
.end method

.method public final a(LX/1aZ;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aZ;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359736
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    .line 359737
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359738
    return-object p0
.end method

.method public final a(LX/1f6;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1f6;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359726
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    .line 359727
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359728
    return-object p0
.end method

.method public final a(LX/22Q;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/22Q;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359729
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    .line 359730
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359731
    return-object p0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/22S;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359724
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 359725
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359721
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 359722
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359723
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/22S;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")",
            "LX/22R",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 359718
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    iput-object p1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 359719
    iget-object v0, p0, LX/22S;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 359720
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 359711
    invoke-super {p0}, LX/1X5;->a()V

    .line 359712
    const/4 v0, 0x0

    iput-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    .line 359713
    iget-object v0, p0, LX/22S;->b:LX/22R;

    iget-object v0, v0, LX/22R;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 359714
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/22R;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 359701
    iget-object v1, p0, LX/22S;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/22S;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/22S;->d:I

    if-ge v1, v2, :cond_2

    .line 359702
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 359703
    :goto_0
    iget v2, p0, LX/22S;->d:I

    if-ge v0, v2, :cond_1

    .line 359704
    iget-object v2, p0, LX/22S;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 359705
    iget-object v2, p0, LX/22S;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359706
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359707
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359708
    :cond_2
    iget-object v0, p0, LX/22S;->a:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    .line 359709
    invoke-virtual {p0}, LX/22S;->a()V

    .line 359710
    return-object v0
.end method
