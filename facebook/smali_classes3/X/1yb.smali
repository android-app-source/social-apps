.class public final LX/1yb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public final synthetic i:LX/1xV;


# direct methods
.method public constructor <init>(LX/1xV;)V
    .locals 1

    .prologue
    .line 350801
    iput-object p1, p0, LX/1yb;->i:LX/1xV;

    .line 350802
    move-object v0, p1

    .line 350803
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 350804
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350805
    const-string v0, "FeedStoryHeaderActionsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 350806
    if-ne p0, p1, :cond_1

    .line 350807
    :cond_0
    :goto_0
    return v0

    .line 350808
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 350809
    goto :goto_0

    .line 350810
    :cond_3
    check-cast p1, LX/1yb;

    .line 350811
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 350812
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 350813
    if-eq v2, v3, :cond_0

    .line 350814
    iget-object v2, p0, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 350815
    goto :goto_0

    .line 350816
    :cond_5
    iget-object v2, p1, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 350817
    :cond_6
    iget-object v2, p0, LX/1yb;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1yb;->b:LX/1Pb;

    iget-object v3, p1, LX/1yb;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 350818
    goto :goto_0

    .line 350819
    :cond_8
    iget-object v2, p1, LX/1yb;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 350820
    :cond_9
    iget v2, p0, LX/1yb;->c:I

    iget v3, p1, LX/1yb;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 350821
    goto :goto_0

    .line 350822
    :cond_a
    iget-boolean v2, p0, LX/1yb;->d:Z

    iget-boolean v3, p1, LX/1yb;->d:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 350823
    goto :goto_0

    .line 350824
    :cond_b
    iget-boolean v2, p0, LX/1yb;->e:Z

    iget-boolean v3, p1, LX/1yb;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 350825
    goto :goto_0

    .line 350826
    :cond_c
    iget-boolean v2, p0, LX/1yb;->f:Z

    iget-boolean v3, p1, LX/1yb;->f:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 350827
    goto :goto_0

    .line 350828
    :cond_d
    iget-boolean v2, p0, LX/1yb;->g:Z

    iget-boolean v3, p1, LX/1yb;->g:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 350829
    goto :goto_0

    .line 350830
    :cond_e
    iget-boolean v2, p0, LX/1yb;->h:Z

    iget-boolean v3, p1, LX/1yb;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 350831
    goto :goto_0
.end method
