.class public LX/1xN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xN",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348232
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348233
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xN;->b:LX/0Zi;

    .line 348234
    iput-object p1, p0, LX/1xN;->a:LX/0Ot;

    .line 348235
    return-void
.end method

.method public static a(LX/0QB;)LX/1xN;
    .locals 4

    .prologue
    .line 348182
    const-class v1, LX/1xN;

    monitor-enter v1

    .line 348183
    :try_start_0
    sget-object v0, LX/1xN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348184
    sput-object v2, LX/1xN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348185
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348186
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348187
    new-instance v3, LX/1xN;

    const/16 p0, 0x825

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xN;-><init>(LX/0Ot;)V

    .line 348188
    move-object v0, v3

    .line 348189
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348190
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348191
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 8

    .prologue
    .line 348229
    check-cast p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 348230
    iget-object v0, p0, LX/1xN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    iget-object v2, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    iget-object v1, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    iget-object v5, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    iget-object v7, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Landroid/view/View;LX/1bf;ILX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 348231
    return-void
.end method

.method private a(Landroid/view/View;Ljava/lang/Integer;LX/1X1;)V
    .locals 9

    .prologue
    .line 348226
    check-cast p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 348227
    iget-object v0, p0, LX/1xN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    iget-object v3, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    iget-object v1, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    iget-object v6, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v7, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    iget-object v8, p3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Landroid/view/View;Ljava/lang/Integer;LX/1bf;ILX/1aZ;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 348228
    return-void
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348225
    const v0, -0x1edc6e6d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/8ww;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348224
    const v0, -0x795b1213

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 348209
    check-cast p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 348210
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 348211
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 348212
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v11

    .line 348213
    iget-object v0, p0, LX/1xN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;

    iget-object v2, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    iget-object v4, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    iget-boolean v6, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->e:Z

    iget-object v7, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    iget-boolean v8, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->g:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;Lcom/facebook/common/callercontext/CallerContext;LX/1f6;ZLX/1dQ;ZLX/1np;LX/1np;LX/1np;)LX/1Dg;

    move-result-object v1

    .line 348214
    iget-object v0, v9, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 348215
    check-cast v0, LX/1bf;

    iput-object v0, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    .line 348216
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 348217
    iget-object v0, v10, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 348218
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    .line 348219
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 348220
    iget-object v0, v11, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 348221
    check-cast v0, LX/1aZ;

    iput-object v0, p2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    .line 348222
    invoke-static {v11}, LX/1cy;->a(LX/1np;)V

    .line 348223
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 348201
    invoke-static {}, LX/1dS;->b()V

    .line 348202
    iget v0, p1, LX/1dQ;->b:I

    .line 348203
    sparse-switch v0, :sswitch_data_0

    .line 348204
    :goto_0
    return-object v3

    .line 348205
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 348206
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/1xN;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 348207
    :sswitch_1
    check-cast p2, LX/8ww;

    .line 348208
    iget-object v0, p2, LX/8ww;->a:Landroid/view/View;

    iget v1, p2, LX/8ww;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1, v2}, LX/1xN;->a(Landroid/view/View;Ljava/lang/Integer;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x795b1213 -> :sswitch_1
        -0x1edc6e6d -> :sswitch_0
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/22O;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xN",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348193
    new-instance v1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;-><init>(LX/1xN;)V

    .line 348194
    iget-object v2, p0, LX/1xN;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/22O;

    .line 348195
    if-nez v2, :cond_0

    .line 348196
    new-instance v2, LX/22O;

    invoke-direct {v2, p0}, LX/22O;-><init>(LX/1xN;)V

    .line 348197
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/22O;->a$redex0(LX/22O;LX/1De;IILcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;)V

    .line 348198
    move-object v1, v2

    .line 348199
    move-object v0, v1

    .line 348200
    return-object v0
.end method
