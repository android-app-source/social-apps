.class public final LX/22Z;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/22Y;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMedia;

.field public c:LX/1dQ;

.field public final synthetic d:LX/22Y;


# direct methods
.method public constructor <init>(LX/22Y;)V
    .locals 1

    .prologue
    .line 359917
    iput-object p1, p0, LX/22Z;->d:LX/22Y;

    .line 359918
    move-object v0, p1

    .line 359919
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 359920
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359921
    const-string v0, "PhotoAttachmentPostPostBadgeComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 359922
    if-ne p0, p1, :cond_1

    .line 359923
    :cond_0
    :goto_0
    return v0

    .line 359924
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 359925
    goto :goto_0

    .line 359926
    :cond_3
    check-cast p1, LX/22Z;

    .line 359927
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 359928
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 359929
    if-eq v2, v3, :cond_0

    .line 359930
    iget-object v2, p0, LX/22Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/22Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/22Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 359931
    goto :goto_0

    .line 359932
    :cond_5
    iget-object v2, p1, LX/22Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 359933
    :cond_6
    iget-object v2, p0, LX/22Z;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/22Z;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-object v3, p1, LX/22Z;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 359934
    goto :goto_0

    .line 359935
    :cond_8
    iget-object v2, p1, LX/22Z;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    if-nez v2, :cond_7

    .line 359936
    :cond_9
    iget-object v2, p0, LX/22Z;->c:LX/1dQ;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/22Z;->c:LX/1dQ;

    iget-object v3, p1, LX/22Z;->c:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 359937
    goto :goto_0

    .line 359938
    :cond_a
    iget-object v2, p1, LX/22Z;->c:LX/1dQ;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
