.class public final LX/22J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 359356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1w1;Ljava/lang/String;LX/15w;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 359357
    const-string v2, "contexts"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 359358
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_1

    .line 359359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 359360
    :cond_0
    :goto_0
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_1

    .line 359361
    invoke-static {p2}, LX/1ps;->a(LX/15w;)LX/1pt;

    move-result-object v2

    .line 359362
    if-eqz v2, :cond_0

    .line 359363
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359364
    :cond_1
    iput-object v0, p0, LX/1w1;->f:Ljava/util/List;

    move v0, v1

    .line 359365
    :goto_1
    return v0

    .line 359366
    :cond_2
    const-string v2, "monitors"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 359367
    invoke-virtual {p2}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_4

    .line 359368
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 359369
    :cond_3
    :goto_2
    invoke-virtual {p2}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_4

    .line 359370
    invoke-static {p2}, LX/1ps;->a(LX/15w;)LX/1pt;

    move-result-object v2

    .line 359371
    if-eqz v2, :cond_3

    .line 359372
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 359373
    :cond_4
    iput-object v0, p0, LX/1w1;->g:Ljava/util/List;

    move v0, v1

    .line 359374
    goto :goto_1

    .line 359375
    :cond_5
    invoke-static {p0, p1, p2}, LX/1jn;->a(LX/1jm;Ljava/lang/String;LX/15w;)Z

    move-result v0

    goto :goto_1
.end method
