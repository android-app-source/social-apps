.class public LX/1wg;
.super LX/1wh;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Zb;

.field private final c:LX/03V;

.field private final d:LX/0lC;

.field private final e:LX/0if;


# direct methods
.method public constructor <init>(LX/0Zb;LX/03V;LX/0if;LX/0lC;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346684
    invoke-direct {p0}, LX/1wh;-><init>()V

    .line 346685
    const-string v0, "appupdate"

    iput-object v0, p0, LX/1wg;->a:Ljava/lang/String;

    .line 346686
    iput-object p1, p0, LX/1wg;->b:LX/0Zb;

    .line 346687
    iput-object p2, p0, LX/1wg;->c:LX/03V;

    .line 346688
    iput-object p3, p0, LX/1wg;->e:LX/0if;

    .line 346689
    iput-object p4, p0, LX/1wg;->d:LX/0lC;

    .line 346690
    return-void
.end method

.method private a(Lorg/json/JSONArray;)LX/162;
    .locals 4

    .prologue
    .line 346662
    iget-object v0, p0, LX/1wg;->d:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v2

    .line 346663
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 346664
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 346665
    instance-of v3, v0, Lorg/json/JSONObject;

    if-eqz v3, :cond_1

    .line 346666
    check-cast v0, Lorg/json/JSONObject;

    invoke-static {p0, v0}, LX/1wg;->b(LX/1wg;Lorg/json/JSONObject;)LX/0m9;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 346667
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 346668
    :cond_1
    instance-of v3, v0, Lorg/json/JSONArray;

    if-eqz v3, :cond_2

    .line 346669
    check-cast v0, Lorg/json/JSONArray;

    invoke-direct {p0, v0}, LX/1wg;->a(Lorg/json/JSONArray;)LX/162;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 346670
    :cond_2
    sget-object v3, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-eq v0, v3, :cond_3

    if-nez v0, :cond_4

    .line 346671
    :cond_3
    invoke-virtual {v2}, LX/162;->J()LX/162;

    goto :goto_1

    .line 346672
    :cond_4
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 346673
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 346674
    :cond_5
    instance-of v3, v0, Ljava/lang/Boolean;

    if-eqz v3, :cond_6

    .line 346675
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v2, v0}, LX/162;->a(Ljava/lang/Boolean;)LX/162;

    goto :goto_1

    .line 346676
    :cond_6
    instance-of v3, v0, Ljava/lang/Double;

    if-eqz v3, :cond_7

    .line 346677
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v2, v0}, LX/162;->a(Ljava/lang/Double;)LX/162;

    goto :goto_1

    .line 346678
    :cond_7
    instance-of v3, v0, Ljava/lang/Long;

    if-eqz v3, :cond_8

    .line 346679
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v2, v0}, LX/162;->a(Ljava/lang/Long;)LX/162;

    goto :goto_1

    .line 346680
    :cond_8
    instance-of v3, v0, Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 346681
    instance-of v3, v0, Ljava/lang/Short;

    if-nez v3, :cond_0

    .line 346682
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected value in json: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 346683
    :cond_9
    return-object v2
.end method

.method private a(Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346643
    if-eqz p1, :cond_7

    .line 346644
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 346645
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 346646
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 346647
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 346648
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 346649
    instance-of v4, v1, Lorg/json/JSONObject;

    if-eqz v4, :cond_0

    .line 346650
    check-cast v1, Lorg/json/JSONObject;

    invoke-static {p0, v1}, LX/1wg;->b(LX/1wg;Lorg/json/JSONObject;)LX/0m9;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346651
    :catch_0
    move-exception v0

    .line 346652
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "org.json exception"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 346653
    :cond_0
    :try_start_1
    instance-of v4, v1, Lorg/json/JSONArray;

    if-eqz v4, :cond_1

    .line 346654
    check-cast v1, Lorg/json/JSONArray;

    invoke-direct {p0, v1}, LX/1wg;->a(Lorg/json/JSONArray;)LX/162;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 346655
    :cond_1
    sget-object v4, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-eq v1, v4, :cond_2

    if-nez v1, :cond_3

    .line 346656
    :cond_2
    sget-object v1, LX/2FN;->a:LX/2FN;

    move-object v1, v1

    .line 346657
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 346658
    :cond_3
    instance-of v4, v1, Ljava/lang/String;

    if-nez v4, :cond_4

    instance-of v4, v1, Ljava/lang/Boolean;

    if-nez v4, :cond_4

    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_5

    .line 346659
    :cond_4
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 346660
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected value in json: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_6
    move-object v0, v2

    .line 346661
    :goto_1
    return-object v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(LX/1wg;Lorg/json/JSONObject;)LX/0m9;
    .locals 5

    .prologue
    .line 346620
    iget-object v0, p0, LX/1wg;->d:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v2

    .line 346621
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 346622
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 346623
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 346624
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 346625
    instance-of v4, v1, Lorg/json/JSONObject;

    if-eqz v4, :cond_1

    .line 346626
    check-cast v1, Lorg/json/JSONObject;

    invoke-static {p0, v1}, LX/1wg;->b(LX/1wg;Lorg/json/JSONObject;)LX/0m9;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 346627
    :cond_1
    instance-of v4, v1, Lorg/json/JSONArray;

    if-eqz v4, :cond_2

    .line 346628
    check-cast v1, Lorg/json/JSONArray;

    invoke-direct {p0, v1}, LX/1wg;->a(Lorg/json/JSONArray;)LX/162;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0

    .line 346629
    :cond_2
    sget-object v4, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-eq v1, v4, :cond_3

    if-nez v1, :cond_4

    .line 346630
    :cond_3
    invoke-virtual {v2, v0}, LX/0m9;->k(Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 346631
    :cond_4
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 346632
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0

    .line 346633
    :cond_5
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_6

    .line 346634
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0m9;

    goto :goto_0

    .line 346635
    :cond_6
    instance-of v4, v1, Ljava/lang/Double;

    if-eqz v4, :cond_7

    .line 346636
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Double;)LX/0m9;

    goto :goto_0

    .line 346637
    :cond_7
    instance-of v4, v1, Ljava/lang/Long;

    if-eqz v4, :cond_8

    .line 346638
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v2, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Long;)LX/0m9;

    goto :goto_0

    .line 346639
    :cond_8
    instance-of v0, v1, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 346640
    instance-of v0, v1, Ljava/lang/Short;

    if-nez v0, :cond_0

    .line 346641
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected value in json: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346642
    :cond_9
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 346579
    iget-object v0, p0, LX/1wg;->e:LX/0if;

    sget-object v1, LX/0ig;->ba:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 346580
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V
    .locals 8
    .param p3    # LX/Eeh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346597
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    .line 346598
    invoke-virtual {p2}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v3

    .line 346599
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 346600
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 346601
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 346602
    :try_start_0
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 346603
    instance-of v5, v1, Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 346604
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    goto :goto_0

    .line 346605
    :catch_0
    move-exception v0

    .line 346606
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 346607
    :cond_0
    instance-of v5, v1, Ljava/lang/Boolean;

    if-eqz v5, :cond_1

    .line 346608
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    goto :goto_0

    .line 346609
    :cond_1
    instance-of v5, v1, Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 346610
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v0, v6, v7}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    goto :goto_0

    .line 346611
    :cond_2
    instance-of v5, v1, Ljava/lang/Double;

    if-eqz v5, :cond_3

    .line 346612
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 346613
    iget-object v1, v2, LX/1rQ;->a:LX/0m9;

    invoke-virtual {v1, v0, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 346614
    goto :goto_0

    .line 346615
    :cond_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    goto :goto_0

    .line 346616
    :cond_4
    if-eqz p3, :cond_5

    .line 346617
    const-string v0, "diff_algorithm"

    invoke-virtual {p3}, LX/Eeh;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    .line 346618
    :cond_5
    iget-object v0, p0, LX/1wg;->e:LX/0if;

    sget-object v1, LX/0ig;->ba:LX/0ih;

    invoke-virtual {v0, v1, p1, p4, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 346619
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2
    .param p2    # Lorg/json/JSONObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346591
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 346592
    const-string v1, "appupdate"

    .line 346593
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 346594
    invoke-direct {p0, p2}, LX/1wg;->a(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 346595
    iget-object v1, p0, LX/1wg;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 346596
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Throwable;)V
    .locals 3
    .param p2    # Lorg/json/JSONObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346585
    iget-object v0, p0, LX/1wg;->c:LX/03V;

    const-string v1, "appupdate_extras"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 346586
    if-nez p3, :cond_0

    .line 346587
    iget-object v0, p0, LX/1wg;->c:LX/03V;

    const-string v1, "appupdate"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346588
    :goto_0
    iget-object v0, p0, LX/1wg;->c:LX/03V;

    const-string v1, "appupdate_extras"

    invoke-virtual {v0, v1}, LX/03V;->a(Ljava/lang/String;)V

    .line 346589
    return-void

    .line 346590
    :cond_0
    iget-object v0, p0, LX/1wg;->c:LX/03V;

    const-string v1, "appupdate"

    invoke-virtual {v0, v1, p1, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 346583
    iget-object v0, p0, LX/1wg;->e:LX/0if;

    sget-object v1, LX/0ig;->ba:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->b(LX/0ih;)V

    .line 346584
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 346581
    iget-object v0, p0, LX/1wg;->e:LX/0if;

    sget-object v1, LX/0ig;->ba:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 346582
    return-void
.end method
