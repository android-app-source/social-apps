.class public LX/1yd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1vb;

.field private final b:LX/1ye;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ye",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/1VE;

.field private final d:LX/1JC;

.field private final e:LX/1yf;


# direct methods
.method public constructor <init>(LX/1vb;LX/1ye;LX/1VE;LX/1JC;LX/1yf;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 350872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350873
    iput-object p1, p0, LX/1yd;->a:LX/1vb;

    .line 350874
    iput-object p2, p0, LX/1yd;->b:LX/1ye;

    .line 350875
    iput-object p3, p0, LX/1yd;->c:LX/1VE;

    .line 350876
    iput-object p4, p0, LX/1yd;->d:LX/1JC;

    .line 350877
    iput-object p5, p0, LX/1yd;->e:LX/1yf;

    .line 350878
    return-void
.end method

.method private static a(LX/1yd;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1Dg;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 350879
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 350880
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    move v0, v1

    .line 350881
    if-nez v0, :cond_0

    .line 350882
    const/4 v0, 0x0

    .line 350883
    :goto_0
    return-object v0

    .line 350884
    :cond_0
    const/4 v0, 0x0

    .line 350885
    new-instance v1, LX/35N;

    invoke-direct {v1}, LX/35N;-><init>()V

    .line 350886
    sget-object v2, LX/35O;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/35Q;

    .line 350887
    if-nez v2, :cond_1

    .line 350888
    new-instance v2, LX/35Q;

    invoke-direct {v2}, LX/35Q;-><init>()V

    .line 350889
    :cond_1
    invoke-static {v2, p1, v0, v0, v1}, LX/35Q;->a$redex0(LX/35Q;LX/1De;IILX/35N;)V

    .line 350890
    move-object v1, v2

    .line 350891
    move-object v0, v1

    .line 350892
    move-object v0, v0

    .line 350893
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 350894
    if-nez p3, :cond_2

    .line 350895
    const/4 v1, 0x5

    const v2, 0x7f0b00ea

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    .line 350896
    :cond_2
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1yd;
    .locals 9

    .prologue
    .line 350897
    const-class v1, LX/1yd;

    monitor-enter v1

    .line 350898
    :try_start_0
    sget-object v0, LX/1yd;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 350899
    sput-object v2, LX/1yd;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 350900
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350901
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 350902
    new-instance v3, LX/1yd;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v4

    check-cast v4, LX/1vb;

    invoke-static {v0}, LX/1ye;->a(LX/0QB;)LX/1ye;

    move-result-object v5

    check-cast v5, LX/1ye;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v6

    check-cast v6, LX/1VE;

    invoke-static {v0}, LX/1JC;->a(LX/0QB;)LX/1JC;

    move-result-object v7

    check-cast v7, LX/1JC;

    invoke-static {v0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v8

    check-cast v8, LX/1yf;

    invoke-direct/range {v3 .. v8}, LX/1yd;-><init>(LX/1vb;LX/1ye;LX/1VE;LX/1JC;LX/1yf;)V

    .line 350903
    move-object v0, v3

    .line 350904
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 350905
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 350906
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 350907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;IZZZZZ)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;IZZZZZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 350908
    move-object v1, p3

    check-cast v1, LX/1Pk;

    invoke-interface {v1}, LX/1Pk;->e()LX/1SX;

    move-result-object v3

    .line 350909
    iget-object v1, p0, LX/1yd;->c:LX/1VE;

    invoke-virtual {v1, p2, v3}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;

    move-result-object v4

    .line 350910
    if-nez p5, :cond_0

    invoke-static {v4}, LX/1ve;->a(LX/1dl;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 350911
    :goto_0
    iget-object v2, p0, LX/1yd;->e:LX/1yf;

    invoke-virtual {v2}, LX/1yf;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0e0741

    .line 350912
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p0, p1, p2, v1}, LX/1yd;->a(LX/1yd;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1Dg;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    iget-object v6, p0, LX/1yd;->b:LX/1ye;

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7, v2}, LX/1ye;->a(LX/1De;II)LX/1yi;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1yi;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1yi;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/1yi;->a(LX/1Pb;)LX/1yi;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/1yi;->a(Z)LX/1yi;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/1yi;->b(Z)LX/1yi;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1yi;->c(Z)LX/1yi;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, LX/1yi;->d(Z)LX/1yi;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1yd;->a:LX/1vb;

    const/4 v5, 0x0

    invoke-virtual {v1, p1, v5, p4}, LX/1vb;->a(LX/1De;II)LX/1vd;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v1

    :goto_2
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 350913
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 350914
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 350915
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method
