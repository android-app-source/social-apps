.class public final LX/22U;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/22T;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/22Q;

.field public d:Ljava/lang/Integer;

.field public e:LX/1LD;

.field public final synthetic f:LX/22T;


# direct methods
.method public constructor <init>(LX/22T;)V
    .locals 1

    .prologue
    .line 359790
    iput-object p1, p0, LX/22U;->f:LX/22T;

    .line 359791
    move-object v0, p1

    .line 359792
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 359793
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359794
    const-string v0, "PhotoAttachmentImageVisibilityComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/22T;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 359795
    check-cast p1, LX/22U;

    .line 359796
    iget-object v0, p1, LX/22U;->e:LX/1LD;

    iput-object v0, p0, LX/22U;->e:LX/1LD;

    .line 359797
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 359798
    if-ne p0, p1, :cond_1

    .line 359799
    :cond_0
    :goto_0
    return v0

    .line 359800
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 359801
    goto :goto_0

    .line 359802
    :cond_3
    check-cast p1, LX/22U;

    .line 359803
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 359804
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 359805
    if-eq v2, v3, :cond_0

    .line 359806
    iget-object v2, p0, LX/22U;->a:LX/1Pq;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/22U;->a:LX/1Pq;

    iget-object v3, p1, LX/22U;->a:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 359807
    goto :goto_0

    .line 359808
    :cond_5
    iget-object v2, p1, LX/22U;->a:LX/1Pq;

    if-nez v2, :cond_4

    .line 359809
    :cond_6
    iget-object v2, p0, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 359810
    goto :goto_0

    .line 359811
    :cond_8
    iget-object v2, p1, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 359812
    :cond_9
    iget-object v2, p0, LX/22U;->c:LX/22Q;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/22U;->c:LX/22Q;

    iget-object v3, p1, LX/22U;->c:LX/22Q;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 359813
    goto :goto_0

    .line 359814
    :cond_b
    iget-object v2, p1, LX/22U;->c:LX/22Q;

    if-nez v2, :cond_a

    .line 359815
    :cond_c
    iget-object v2, p0, LX/22U;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/22U;->d:Ljava/lang/Integer;

    iget-object v3, p1, LX/22U;->d:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 359816
    goto :goto_0

    .line 359817
    :cond_d
    iget-object v2, p1, LX/22U;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 359818
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/22U;

    .line 359819
    const/4 v1, 0x0

    iput-object v1, v0, LX/22U;->e:LX/1LD;

    .line 359820
    return-object v0
.end method
