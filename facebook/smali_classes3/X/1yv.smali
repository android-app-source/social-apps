.class public LX/1yv;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:LX/1yv;


# instance fields
.field private c:LX/1yw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 351604
    const-class v0, LX/1yv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1yv;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 351593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351594
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, LX/1yv;->a:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 351595
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 351596
    new-instance v1, LX/1yw;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1yw;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/1yv;->c:LX/1yw;

    .line 351597
    return-void
.end method

.method public static declared-synchronized a()LX/1yv;
    .locals 2

    .prologue
    .line 351600
    const-class v1, LX/1yv;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1yv;->b:LX/1yv;

    if-nez v0, :cond_0

    .line 351601
    new-instance v0, LX/1yv;

    invoke-direct {v0}, LX/1yv;-><init>()V

    sput-object v0, LX/1yv;->b:LX/1yv;

    .line 351602
    :cond_0
    sget-object v0, LX/1yv;->b:LX/1yv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 351603
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/text/Layout;)V
    .locals 3

    .prologue
    .line 351598
    iget-object v0, p0, LX/1yv;->c:LX/1yw;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, LX/1yw;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 351599
    return-void
.end method
