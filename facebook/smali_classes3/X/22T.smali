.class public LX/22T;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22W;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/22T",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/22W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359786
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 359787
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/22T;->b:LX/0Zi;

    .line 359788
    iput-object p1, p0, LX/22T;->a:LX/0Ot;

    .line 359789
    return-void
.end method

.method public static a(LX/0QB;)LX/22T;
    .locals 4

    .prologue
    .line 359743
    const-class v1, LX/22T;

    monitor-enter v1

    .line 359744
    :try_start_0
    sget-object v0, LX/22T;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359745
    sput-object v2, LX/22T;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359746
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359747
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359748
    new-instance v3, LX/22T;

    const/16 p0, 0x829

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/22T;-><init>(LX/0Ot;)V

    .line 359749
    move-object v0, v3

    .line 359750
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359751
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359752
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359753
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359784
    invoke-static {}, LX/1dS;->b()V

    .line 359785
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359781
    iget-object v0, p0, LX/22T;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 359782
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 359783
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 12

    .prologue
    .line 359772
    check-cast p2, LX/22U;

    .line 359773
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 359774
    iget-object v0, p0, LX/22T;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22W;

    iget-object v1, p2, LX/22U;->a:LX/1Pq;

    iget-object v2, p2, LX/22U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/22U;->c:LX/22Q;

    iget-object v4, p2, LX/22U;->d:Ljava/lang/Integer;

    .line 359775
    new-instance v6, LX/22X;

    move-object v7, v0

    move-object v8, v4

    move-object v9, v1

    move-object v10, v3

    move-object v11, v2

    invoke-direct/range {v6 .. v11}, LX/22X;-><init>(LX/22W;Ljava/lang/Integer;LX/1Pq;LX/22Q;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 359776
    iput-object v6, v5, LX/1np;->a:Ljava/lang/Object;

    .line 359777
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 359778
    check-cast v0, LX/1LD;

    iput-object v0, p2, LX/22U;->e:LX/1LD;

    .line 359779
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 359780
    return-void
.end method

.method public final c(LX/1De;)LX/22V;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/22T",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 359764
    new-instance v1, LX/22U;

    invoke-direct {v1, p0}, LX/22U;-><init>(LX/22T;)V

    .line 359765
    iget-object v2, p0, LX/22T;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/22V;

    .line 359766
    if-nez v2, :cond_0

    .line 359767
    new-instance v2, LX/22V;

    invoke-direct {v2, p0}, LX/22V;-><init>(LX/22T;)V

    .line 359768
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/22V;->a$redex0(LX/22V;LX/1De;IILX/22U;)V

    .line 359769
    move-object v1, v2

    .line 359770
    move-object v0, v1

    .line 359771
    return-object v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 359763
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 359759
    check-cast p3, LX/22U;

    .line 359760
    iget-object v0, p0, LX/22T;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22W;

    iget-object v1, p3, LX/22U;->e:LX/1LD;

    .line 359761
    iget-object p0, v0, LX/22W;->b:LX/1L1;

    invoke-virtual {p0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 359762
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 359755
    check-cast p3, LX/22U;

    .line 359756
    iget-object v0, p0, LX/22T;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22W;

    iget-object v1, p3, LX/22U;->e:LX/1LD;

    .line 359757
    iget-object p0, v0, LX/22W;->b:LX/1L1;

    invoke-virtual {p0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 359758
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 359754
    const/16 v0, 0xf

    return v0
.end method
