.class public final enum LX/21y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21y;

.field public static final enum DEFAULT_ORDER:LX/21y;

.field public static final enum RANKED_ORDER:LX/21y;

.field public static final enum THREADED_CHRONOLOGICAL_ORDER:LX/21y;


# instance fields
.field public toString:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 358626
    new-instance v0, LX/21y;

    const-string v1, "RANKED_ORDER"

    const-string v2, "ranked_threaded"

    invoke-direct {v0, v1, v3, v2}, LX/21y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    .line 358627
    new-instance v0, LX/21y;

    const-string v1, "THREADED_CHRONOLOGICAL_ORDER"

    const-string v2, "toplevel"

    invoke-direct {v0, v1, v4, v2}, LX/21y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21y;->THREADED_CHRONOLOGICAL_ORDER:LX/21y;

    .line 358628
    new-instance v0, LX/21y;

    const-string v1, "DEFAULT_ORDER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v2}, LX/21y;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21y;->DEFAULT_ORDER:LX/21y;

    .line 358629
    const/4 v0, 0x3

    new-array v0, v0, [LX/21y;

    sget-object v1, LX/21y;->RANKED_ORDER:LX/21y;

    aput-object v1, v0, v3

    sget-object v1, LX/21y;->THREADED_CHRONOLOGICAL_ORDER:LX/21y;

    aput-object v1, v0, v4

    sget-object v1, LX/21y;->DEFAULT_ORDER:LX/21y;

    aput-object v1, v0, v5

    sput-object v0, LX/21y;->$VALUES:[LX/21y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 358604
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 358605
    iput-object p3, p0, LX/21y;->toString:Ljava/lang/String;

    .line 358606
    return-void
.end method

.method public static getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;
    .locals 6

    .prologue
    .line 358618
    if-nez p0, :cond_1

    .line 358619
    sget-object v0, LX/21y;->DEFAULT_ORDER:LX/21y;

    .line 358620
    :cond_0
    :goto_0
    return-object v0

    .line 358621
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v2

    .line 358622
    invoke-static {}, LX/21y;->values()[LX/21y;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 358623
    iget-object v5, v0, LX/21y;->toString:Ljava/lang/String;

    invoke-static {v5, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 358624
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 358625
    :cond_2
    sget-object v0, LX/21y;->DEFAULT_ORDER:LX/21y;

    goto :goto_0
.end method

.method public static getOrder(Ljava/lang/String;)LX/21y;
    .locals 5

    .prologue
    .line 358613
    invoke-static {}, LX/21y;->values()[LX/21y;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 358614
    iget-object v4, v0, LX/21y;->toString:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 358615
    :goto_1
    return-object v0

    .line 358616
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 358617
    :cond_1
    sget-object v0, LX/21y;->DEFAULT_ORDER:LX/21y;

    goto :goto_1
.end method

.method public static isRanked(LX/21y;)Z
    .locals 1

    .prologue
    .line 358612
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRanked(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 358611
    invoke-static {p0}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    invoke-static {v0}, LX/21y;->isRanked(LX/21y;)Z

    move-result v0

    return v0
.end method

.method public static isReverseOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 358610
    invoke-static {p0}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v0

    invoke-static {v0}, LX/21y;->isRanked(LX/21y;)Z

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/21y;
    .locals 1

    .prologue
    .line 358609
    const-class v0, LX/21y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21y;

    return-object v0
.end method

.method public static values()[LX/21y;
    .locals 1

    .prologue
    .line 358608
    sget-object v0, LX/21y;->$VALUES:[LX/21y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21y;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 358607
    iget-object v0, p0, LX/21y;->toString:Ljava/lang/String;

    return-object v0
.end method
