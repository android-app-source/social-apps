.class public final LX/1yV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:I

.field public e:F

.field public f:F

.field public g:F

.field public h:I

.field public i:Z

.field public j:Ljava/lang/CharSequence;

.field public k:Landroid/text/Layout;

.field public l:[Landroid/text/style/ClickableSpan;

.field public m:Ljava/lang/CharSequence;

.field public n:Landroid/text/Layout;

.field public o:[Landroid/text/style/ClickableSpan;

.field public final synthetic p:LX/1xq;


# direct methods
.method public constructor <init>(LX/1xq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 350685
    iput-object p1, p0, LX/1yV;->p:LX/1xq;

    .line 350686
    move-object v0, p1

    .line 350687
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 350688
    const v0, -0x6e685d

    iput v0, p0, LX/1yV;->b:I

    .line 350689
    iput v1, p0, LX/1yV;->c:I

    .line 350690
    iput-boolean v1, p0, LX/1yV;->i:Z

    .line 350691
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350692
    const-string v0, "HeaderSubtitleComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1xq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 350693
    check-cast p1, LX/1yV;

    .line 350694
    iget-object v0, p1, LX/1yV;->j:Ljava/lang/CharSequence;

    iput-object v0, p0, LX/1yV;->j:Ljava/lang/CharSequence;

    .line 350695
    iget-object v0, p1, LX/1yV;->k:Landroid/text/Layout;

    iput-object v0, p0, LX/1yV;->k:Landroid/text/Layout;

    .line 350696
    iget-object v0, p1, LX/1yV;->l:[Landroid/text/style/ClickableSpan;

    iput-object v0, p0, LX/1yV;->l:[Landroid/text/style/ClickableSpan;

    .line 350697
    iget-object v0, p1, LX/1yV;->m:Ljava/lang/CharSequence;

    iput-object v0, p0, LX/1yV;->m:Ljava/lang/CharSequence;

    .line 350698
    iget-object v0, p1, LX/1yV;->n:Landroid/text/Layout;

    iput-object v0, p0, LX/1yV;->n:Landroid/text/Layout;

    .line 350699
    iget-object v0, p1, LX/1yV;->o:[Landroid/text/style/ClickableSpan;

    iput-object v0, p0, LX/1yV;->o:[Landroid/text/style/ClickableSpan;

    .line 350700
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 350701
    if-ne p0, p1, :cond_1

    .line 350702
    :cond_0
    :goto_0
    return v0

    .line 350703
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 350704
    goto :goto_0

    .line 350705
    :cond_3
    check-cast p1, LX/1yV;

    .line 350706
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 350707
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 350708
    if-eq v2, v3, :cond_0

    .line 350709
    iget-object v2, p0, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 350710
    goto :goto_0

    .line 350711
    :cond_5
    iget-object v2, p1, LX/1yV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 350712
    :cond_6
    iget v2, p0, LX/1yV;->b:I

    iget v3, p1, LX/1yV;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 350713
    goto :goto_0

    .line 350714
    :cond_7
    iget v2, p0, LX/1yV;->c:I

    iget v3, p1, LX/1yV;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 350715
    goto :goto_0

    .line 350716
    :cond_8
    iget v2, p0, LX/1yV;->d:I

    iget v3, p1, LX/1yV;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 350717
    goto :goto_0

    .line 350718
    :cond_9
    iget v2, p0, LX/1yV;->e:F

    iget v3, p1, LX/1yV;->e:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    .line 350719
    goto :goto_0

    .line 350720
    :cond_a
    iget v2, p0, LX/1yV;->f:F

    iget v3, p1, LX/1yV;->f:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_b

    move v0, v1

    .line 350721
    goto :goto_0

    .line 350722
    :cond_b
    iget v2, p0, LX/1yV;->g:F

    iget v3, p1, LX/1yV;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_c

    move v0, v1

    .line 350723
    goto :goto_0

    .line 350724
    :cond_c
    iget v2, p0, LX/1yV;->h:I

    iget v3, p1, LX/1yV;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 350725
    goto :goto_0

    .line 350726
    :cond_d
    iget-boolean v2, p0, LX/1yV;->i:Z

    iget-boolean v3, p1, LX/1yV;->i:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 350727
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 350728
    const/4 v1, 0x0

    .line 350729
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/1yV;

    .line 350730
    iput-object v1, v0, LX/1yV;->j:Ljava/lang/CharSequence;

    .line 350731
    iput-object v1, v0, LX/1yV;->k:Landroid/text/Layout;

    .line 350732
    iput-object v1, v0, LX/1yV;->l:[Landroid/text/style/ClickableSpan;

    .line 350733
    iput-object v1, v0, LX/1yV;->m:Ljava/lang/CharSequence;

    .line 350734
    iput-object v1, v0, LX/1yV;->n:Landroid/text/Layout;

    .line 350735
    iput-object v1, v0, LX/1yV;->o:[Landroid/text/style/ClickableSpan;

    .line 350736
    return-object v0
.end method
