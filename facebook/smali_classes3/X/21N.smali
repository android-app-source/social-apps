.class public LX/21N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 357149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357150
    return-void
.end method

.method public static a(LX/1PT;)LX/20I;
    .locals 2

    .prologue
    .line 357146
    sget-object v0, LX/21O;->a:[I

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 357147
    sget-object v0, LX/20I;->LIGHT:LX/20I;

    :goto_0
    return-object v0

    .line 357148
    :pswitch_0
    sget-object v0, LX/20I;->DARK:LX/20I;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0Ot;LX/1zt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/1zt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357143
    sget-object v0, LX/1zt;->c:LX/1zt;

    if-eq p1, v0, :cond_0

    .line 357144
    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "like_main"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 357145
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V
    .locals 2

    .prologue
    .line 357135
    iget-object v0, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->l:LX/20H;

    move-object v0, v0

    .line 357136
    sget-object v1, LX/20H;->REACTIONS:LX/20H;

    if-ne v0, v1, :cond_0

    .line 357137
    invoke-virtual {p1}, LX/20K;->a()V

    .line 357138
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 357139
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a()V

    .line 357140
    invoke-virtual {p0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->b()V

    .line 357141
    return-void

    .line 357142
    :cond_0
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "LX/21M;",
            "LX/0wd;",
            "LX/20z;",
            "LX/1zf;",
            "LX/20I;",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357113
    invoke-static {p1}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p5, v0}, LX/1zf;->a(I)LX/1zt;

    move-result-object v0

    .line 357114
    iput-object p2, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->m:LX/21M;

    .line 357115
    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    .line 357116
    if-eqz p8, :cond_2

    .line 357117
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    .line 357118
    iget-boolean v1, p8, LX/21H;->e:Z

    if-eqz v1, :cond_0

    .line 357119
    invoke-static {p8, v0}, LX/21H;->c(LX/21H;LX/1zt;)V

    .line 357120
    :cond_0
    iget-boolean v1, p8, LX/21H;->m:Z

    if-nez v1, :cond_1

    iget-boolean v1, p8, LX/21H;->l:Z

    if-eqz v1, :cond_3

    .line 357121
    :cond_1
    invoke-virtual {p8}, LX/21H;->f()V

    .line 357122
    :cond_2
    :goto_0
    invoke-virtual {p0, p3}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setFadeStateSpring(LX/0wd;)V

    .line 357123
    invoke-virtual {p4, v0}, LX/20z;->a(LX/1zt;)V

    .line 357124
    iput-object p4, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->o:LX/20z;

    .line 357125
    iput-object p6, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->q:LX/20I;

    .line 357126
    iput-object p7, p0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->r:LX/0Px;

    .line 357127
    invoke-virtual {p0, p8}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setProgressiveUfiState(LX/21H;)V

    .line 357128
    return-void

    .line 357129
    :cond_3
    invoke-virtual {p8}, LX/21H;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p8, LX/21H;->c:LX/20S;

    invoke-virtual {v1}, LX/20S;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/20S;->b(LX/1zt;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 357130
    iget-boolean v1, p8, LX/21H;->k:Z

    move v1, v1

    .line 357131
    if-eqz v1, :cond_2

    .line 357132
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p8, LX/21H;->i:Ljava/lang/Double;

    .line 357133
    iget-object v1, p8, LX/21H;->d:LX/0wd;

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 357134
    const/4 v1, 0x0

    iput-boolean v1, p8, LX/21H;->k:Z

    goto :goto_0
.end method

.method public static a(LX/0iA;LX/20l;LX/0tH;Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/20m;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/1zt;)Z
    .locals 5
    .param p6    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 357097
    iget v0, p9, LX/1zt;->e:I

    move v0, v0

    .line 357098
    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, LX/20l;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 357099
    :goto_0
    return v0

    .line 357100
    :cond_1
    const-class v0, LX/3lC;

    invoke-virtual {p0, p3, v0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3lC;

    .line 357101
    if-nez v0, :cond_2

    move v0, v1

    .line 357102
    goto :goto_0

    .line 357103
    :cond_2
    invoke-virtual {v0}, LX/3lC;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "4305"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 357104
    if-eqz p6, :cond_4

    invoke-virtual {p6}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p6}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->n()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 357105
    iget-object v3, p2, LX/0tH;->c:LX/0Uh;

    const/16 v4, 0x4ef

    const/4 p6, 0x0

    invoke-virtual {v3, v4, p6}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v3, v3

    .line 357106
    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 357107
    if-nez v3, :cond_3

    move v0, v1

    .line 357108
    goto :goto_0

    .line 357109
    :cond_3
    invoke-virtual {v0, p8, p7}, LX/3lC;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 357110
    invoke-virtual {p7}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/9BS;->FOOTER:LX/9BS;

    invoke-virtual {v0}, LX/3lC;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4, p5, v1, v3, v4}, LX/20m;->a(Ljava/lang/String;Ljava/lang/String;LX/9BS;Ljava/lang/String;)V

    .line 357111
    invoke-virtual {p0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3lC;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    move v0, v2

    .line 357112
    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static a(LX/0iA;LX/20l;LX/20m;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/1zt;I)Z
    .locals 11

    .prologue
    .line 357088
    invoke-virtual/range {p6 .. p6}, LX/1zt;->a()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, LX/20l;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 357089
    :cond_0
    const/4 v2, 0x0

    .line 357090
    :goto_0
    return v2

    .line 357091
    :cond_1
    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHOOSE_LOVE_REACTION:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/C4O;

    invoke-virtual {p0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3lC;

    .line 357092
    if-nez v3, :cond_2

    .line 357093
    const/4 v2, 0x0

    goto :goto_0

    .line 357094
    :cond_2
    new-instance v10, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v10, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 357095
    new-instance v2, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;

    move-object/from16 v4, p5

    move-object v5, p4

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    move-object v9, p0

    invoke-direct/range {v2 .. v9}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterBinderUtil$1;-><init>(LX/3lC;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20l;LX/20m;Ljava/lang/String;LX/0iA;)V

    move/from16 v0, p7

    int-to-long v4, v0

    const v3, -0x19a5e6c2

    invoke-static {v10, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 357096
    const/4 v2, 0x1

    goto :goto_0
.end method
