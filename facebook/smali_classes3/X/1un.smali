.class public final LX/1un;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1um;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:F

.field public b:LX/1Up;

.field public c:Landroid/graphics/PointF;

.field public d:Landroid/graphics/PointF;

.field public e:LX/1Up;

.field public f:LX/1aZ;

.field public g:LX/4Ab;

.field public h:Landroid/graphics/ColorFilter;

.field public i:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/1Up;

.field public l:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/1Up;

.field public n:I

.field public o:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1Up;

.field public q:I

.field public r:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 341581
    invoke-static {}, LX/1um;->q()LX/1um;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 341582
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/1un;->a:F

    .line 341583
    sget-object v0, LX/1uo;->c:LX/1Up;

    iput-object v0, p0, LX/1un;->b:LX/1Up;

    .line 341584
    sget-object v0, LX/1uo;->a:LX/1Up;

    iput-object v0, p0, LX/1un;->e:LX/1Up;

    .line 341585
    sget-object v0, LX/1uo;->b:LX/1Up;

    iput-object v0, p0, LX/1un;->k:LX/1Up;

    .line 341586
    sget-object v0, LX/1uo;->d:LX/1Up;

    iput-object v0, p0, LX/1un;->m:LX/1Up;

    .line 341587
    sget-object v0, LX/1uo;->e:LX/1Up;

    iput-object v0, p0, LX/1un;->p:LX/1Up;

    .line 341588
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341589
    const-string v0, "FbFrescoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 341590
    if-ne p0, p1, :cond_1

    .line 341591
    :cond_0
    :goto_0
    return v0

    .line 341592
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 341593
    goto :goto_0

    .line 341594
    :cond_3
    check-cast p1, LX/1un;

    .line 341595
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 341596
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 341597
    if-eq v2, v3, :cond_0

    .line 341598
    iget v2, p0, LX/1un;->a:F

    iget v3, p1, LX/1un;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 341599
    goto :goto_0

    .line 341600
    :cond_4
    iget-object v2, p0, LX/1un;->b:LX/1Up;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/1un;->b:LX/1Up;

    iget-object v3, p1, LX/1un;->b:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 341601
    goto :goto_0

    .line 341602
    :cond_6
    iget-object v2, p1, LX/1un;->b:LX/1Up;

    if-nez v2, :cond_5

    .line 341603
    :cond_7
    iget-object v2, p0, LX/1un;->c:Landroid/graphics/PointF;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/1un;->c:Landroid/graphics/PointF;

    iget-object v3, p1, LX/1un;->c:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 341604
    goto :goto_0

    .line 341605
    :cond_9
    iget-object v2, p1, LX/1un;->c:Landroid/graphics/PointF;

    if-nez v2, :cond_8

    .line 341606
    :cond_a
    iget-object v2, p0, LX/1un;->d:Landroid/graphics/PointF;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/1un;->d:Landroid/graphics/PointF;

    iget-object v3, p1, LX/1un;->d:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 341607
    goto :goto_0

    .line 341608
    :cond_c
    iget-object v2, p1, LX/1un;->d:Landroid/graphics/PointF;

    if-nez v2, :cond_b

    .line 341609
    :cond_d
    iget-object v2, p0, LX/1un;->e:LX/1Up;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/1un;->e:LX/1Up;

    iget-object v3, p1, LX/1un;->e:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 341610
    goto :goto_0

    .line 341611
    :cond_f
    iget-object v2, p1, LX/1un;->e:LX/1Up;

    if-nez v2, :cond_e

    .line 341612
    :cond_10
    iget-object v2, p0, LX/1un;->f:LX/1aZ;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/1un;->f:LX/1aZ;

    iget-object v3, p1, LX/1un;->f:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 341613
    goto/16 :goto_0

    .line 341614
    :cond_12
    iget-object v2, p1, LX/1un;->f:LX/1aZ;

    if-nez v2, :cond_11

    .line 341615
    :cond_13
    iget-object v2, p0, LX/1un;->g:LX/4Ab;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/1un;->g:LX/4Ab;

    iget-object v3, p1, LX/1un;->g:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 341616
    goto/16 :goto_0

    .line 341617
    :cond_15
    iget-object v2, p1, LX/1un;->g:LX/4Ab;

    if-nez v2, :cond_14

    .line 341618
    :cond_16
    iget-object v2, p0, LX/1un;->h:Landroid/graphics/ColorFilter;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/1un;->h:Landroid/graphics/ColorFilter;

    iget-object v3, p1, LX/1un;->h:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 341619
    goto/16 :goto_0

    .line 341620
    :cond_18
    iget-object v2, p1, LX/1un;->h:Landroid/graphics/ColorFilter;

    if-nez v2, :cond_17

    .line 341621
    :cond_19
    iget-object v2, p0, LX/1un;->i:LX/1dc;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/1un;->i:LX/1dc;

    iget-object v3, p1, LX/1un;->i:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 341622
    goto/16 :goto_0

    .line 341623
    :cond_1b
    iget-object v2, p1, LX/1un;->i:LX/1dc;

    if-nez v2, :cond_1a

    .line 341624
    :cond_1c
    iget-object v2, p0, LX/1un;->j:LX/1dc;

    if-eqz v2, :cond_1e

    iget-object v2, p0, LX/1un;->j:LX/1dc;

    iget-object v3, p1, LX/1un;->j:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 341625
    goto/16 :goto_0

    .line 341626
    :cond_1e
    iget-object v2, p1, LX/1un;->j:LX/1dc;

    if-nez v2, :cond_1d

    .line 341627
    :cond_1f
    iget-object v2, p0, LX/1un;->k:LX/1Up;

    if-eqz v2, :cond_21

    iget-object v2, p0, LX/1un;->k:LX/1Up;

    iget-object v3, p1, LX/1un;->k:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 341628
    goto/16 :goto_0

    .line 341629
    :cond_21
    iget-object v2, p1, LX/1un;->k:LX/1Up;

    if-nez v2, :cond_20

    .line 341630
    :cond_22
    iget-object v2, p0, LX/1un;->l:LX/1dc;

    if-eqz v2, :cond_24

    iget-object v2, p0, LX/1un;->l:LX/1dc;

    iget-object v3, p1, LX/1un;->l:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 341631
    goto/16 :goto_0

    .line 341632
    :cond_24
    iget-object v2, p1, LX/1un;->l:LX/1dc;

    if-nez v2, :cond_23

    .line 341633
    :cond_25
    iget-object v2, p0, LX/1un;->m:LX/1Up;

    if-eqz v2, :cond_27

    iget-object v2, p0, LX/1un;->m:LX/1Up;

    iget-object v3, p1, LX/1un;->m:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 341634
    goto/16 :goto_0

    .line 341635
    :cond_27
    iget-object v2, p1, LX/1un;->m:LX/1Up;

    if-nez v2, :cond_26

    .line 341636
    :cond_28
    iget v2, p0, LX/1un;->n:I

    iget v3, p1, LX/1un;->n:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 341637
    goto/16 :goto_0

    .line 341638
    :cond_29
    iget-object v2, p0, LX/1un;->o:LX/1dc;

    if-eqz v2, :cond_2b

    iget-object v2, p0, LX/1un;->o:LX/1dc;

    iget-object v3, p1, LX/1un;->o:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    :cond_2a
    move v0, v1

    .line 341639
    goto/16 :goto_0

    .line 341640
    :cond_2b
    iget-object v2, p1, LX/1un;->o:LX/1dc;

    if-nez v2, :cond_2a

    .line 341641
    :cond_2c
    iget-object v2, p0, LX/1un;->p:LX/1Up;

    if-eqz v2, :cond_2e

    iget-object v2, p0, LX/1un;->p:LX/1Up;

    iget-object v3, p1, LX/1un;->p:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    :cond_2d
    move v0, v1

    .line 341642
    goto/16 :goto_0

    .line 341643
    :cond_2e
    iget-object v2, p1, LX/1un;->p:LX/1Up;

    if-nez v2, :cond_2d

    .line 341644
    :cond_2f
    iget v2, p0, LX/1un;->q:I

    iget v3, p1, LX/1un;->q:I

    if-eq v2, v3, :cond_30

    move v0, v1

    .line 341645
    goto/16 :goto_0

    .line 341646
    :cond_30
    iget-object v2, p0, LX/1un;->r:LX/1dc;

    if-eqz v2, :cond_31

    iget-object v2, p0, LX/1un;->r:LX/1dc;

    iget-object v3, p1, LX/1un;->r:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 341647
    goto/16 :goto_0

    .line 341648
    :cond_31
    iget-object v2, p1, LX/1un;->r:LX/1dc;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
