.class public LX/1vo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1sD;


# direct methods
.method public constructor <init>(LX/1sD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344532
    iput-object p1, p0, LX/1vo;->a:LX/1sD;

    .line 344533
    return-void
.end method

.method public static a(LX/0QB;)LX/1vo;
    .locals 4

    .prologue
    .line 344534
    const-class v1, LX/1vo;

    monitor-enter v1

    .line 344535
    :try_start_0
    sget-object v0, LX/1vo;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 344536
    sput-object v2, LX/1vo;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 344537
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344538
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 344539
    new-instance p0, LX/1vo;

    invoke-static {v0}, LX/1sC;->a(LX/0QB;)LX/1sC;

    move-result-object v3

    check-cast v3, LX/1sD;

    invoke-direct {p0, v3}, LX/1vo;-><init>(LX/1sD;)V

    .line 344540
    move-object v0, p0

    .line 344541
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 344542
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1vo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344543
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 344544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344545
    iget-object v0, p0, LX/1vo;->a:LX/1sD;

    invoke-interface {v0}, LX/1sD;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1

    .prologue
    .line 344546
    iget-object v0, p0, LX/1vo;->a:LX/1sD;

    invoke-interface {v0, p1}, LX/1sD;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/9oK;)Z
    .locals 2
    .param p1    # LX/9oK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 344547
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/9oK;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1vo;->a:LX/1sD;

    invoke-interface {v0}, LX/1sD;->a()LX/0Px;

    move-result-object v0

    invoke-interface {p1}, LX/9oK;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
