.class public final LX/20c;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/20b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/20b;)V
    .locals 1

    .prologue
    .line 355016
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 355017
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/20c;->a:Ljava/lang/ref/WeakReference;

    .line 355018
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 355019
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/20c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 355020
    :cond_0
    :goto_0
    return-void

    .line 355021
    :cond_1
    iget-object v0, p0, LX/20c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20b;

    .line 355022
    iget-object v1, v0, LX/20b;->b:LX/20E;

    if-nez v1, :cond_2

    .line 355023
    :goto_1
    goto :goto_0

    .line 355024
    :cond_2
    iget-object v1, v0, LX/20b;->b:LX/20E;

    iget-object p0, v0, LX/20b;->e:Landroid/view/View;

    iget-object p1, v0, LX/20b;->d:Landroid/view/MotionEvent;

    invoke-interface {v1, p0, p1}, LX/20E;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 355025
    const/4 v1, 0x0

    .line 355026
    iput-object v1, v0, LX/20b;->e:Landroid/view/View;

    .line 355027
    goto :goto_1
.end method
