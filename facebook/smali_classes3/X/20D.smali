.class public LX/20D;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field public final a:LX/0hL;

.field public final b:I

.field public final c:I

.field public final d:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field public g:I

.field private h:Landroid/graphics/Paint;

.field public i:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(LX/0hL;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354514
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 354515
    iput-object p1, p0, LX/20D;->a:LX/0hL;

    .line 354516
    const v0, 0x7f0b0fe5

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/20D;->b:I

    .line 354517
    const v0, 0x7f0b0fe6

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/20D;->c:I

    .line 354518
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/20D;->d:LX/0YU;

    .line 354519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/20D;->e:Ljava/util/List;

    .line 354520
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/20D;->h:Landroid/graphics/Paint;

    .line 354521
    iget-object v0, p0, LX/20D;->h:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354522
    iget-object v0, p0, LX/20D;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 354523
    return-void
.end method

.method public static a(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 354510
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 354511
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 354512
    iget-object v0, p0, LX/20D;->i:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 354513
    :cond_0
    return-void
.end method

.method public static b(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 354506
    iget v0, p0, LX/20D;->b:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 354507
    iget v1, p0, LX/20D;->c:I

    int-to-float v1, v1

    add-float/2addr v1, v0

    iget-object v2, p0, LX/20D;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 354508
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 354509
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1zt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 354491
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 354492
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1zt;

    .line 354493
    iget-object v2, p0, LX/20D;->d:LX/0YU;

    .line 354494
    iget v3, v0, LX/1zt;->e:I

    move v3, v3

    .line 354495
    invoke-virtual {v2, v3}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    .line 354496
    if-nez v2, :cond_0

    .line 354497
    invoke-virtual {v0}, LX/1zt;->e()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 354498
    iget-object v3, p0, LX/20D;->d:LX/0YU;

    .line 354499
    iget p1, v0, LX/1zt;->e:I

    move p1, p1

    .line 354500
    invoke-virtual {v3, p1, v2}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 354501
    :cond_0
    move-object v0, v2

    .line 354502
    iget v2, p0, LX/20D;->b:I

    iget v3, p0, LX/20D;->b:I

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 354503
    iget-object v2, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 354504
    :cond_1
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, LX/20D;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, LX/20D;->g:I

    .line 354505
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 354485
    iput-boolean p1, p0, LX/20D;->f:Z

    .line 354486
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/20D;->i:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 354487
    iget v0, p0, LX/20D;->b:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 354488
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, LX/20D;->i:Landroid/graphics/Path;

    .line 354489
    iget-object v1, p0, LX/20D;->i:Landroid/graphics/Path;

    iget v2, p0, LX/20D;->c:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    sget-object p1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v0, v0, v2, p1}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 354490
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 354449
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 354450
    invoke-virtual {p0}, LX/20D;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 354451
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354452
    iget-boolean v0, p0, LX/20D;->f:Z

    if-eqz v0, :cond_2

    .line 354453
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 354454
    iget-object v1, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 354455
    iget-object v1, p0, LX/20D;->a:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354456
    iget v1, p0, LX/20D;->g:I

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    move v1, v0

    .line 354457
    :goto_0
    if-ge v1, v2, :cond_1

    .line 354458
    iget v0, p0, LX/20D;->b:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354459
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {p0, p1, v0}, LX/20D;->a(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 354460
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v0

    .line 354461
    :goto_1
    if-ge v1, v2, :cond_1

    .line 354462
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {p0, p1, v0}, LX/20D;->a(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 354463
    iget v0, p0, LX/20D;->b:I

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354464
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 354465
    :cond_1
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 354466
    return-void

    .line 354467
    :cond_2
    const/4 v2, 0x0

    .line 354468
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 354469
    iget-object v1, p0, LX/20D;->a:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 354470
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_4

    .line 354471
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {p0, p1, v0}, LX/20D;->b(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 354472
    iget v0, p0, LX/20D;->b:I

    int-to-float v0, v0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354473
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 354474
    :cond_3
    iget v1, p0, LX/20D;->g:I

    int-to-float v1, v1

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354475
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_4

    .line 354476
    iget v0, p0, LX/20D;->b:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354477
    iget-object v0, p0, LX/20D;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-static {p0, p1, v0}, LX/20D;->b(LX/20D;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 354478
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 354479
    :cond_4
    goto :goto_2
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 354484
    iget v0, p0, LX/20D;->b:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 354483
    iget v0, p0, LX/20D;->g:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 354482
    const/4 v0, -0x2

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 354481
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 354480
    return-void
.end method
