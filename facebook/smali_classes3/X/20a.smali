.class public final LX/20a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/20X;

.field private final b:LX/20Z;

.field private final c:LX/13Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354980
    const-class v0, LX/20a;

    sput-object v0, LX/20a;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/20X;LX/20Z;)V
    .locals 1

    .prologue
    .line 354972
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/20a;-><init>(LX/20X;LX/20Z;LX/13Q;)V

    .line 354973
    return-void
.end method

.method public constructor <init>(LX/20X;LX/20Z;LX/13Q;)V
    .locals 0
    .param p3    # LX/13Q;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 354981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354982
    iput-object p1, p0, LX/20a;->a:LX/20X;

    .line 354983
    iput-object p2, p0, LX/20a;->b:LX/20Z;

    .line 354984
    iput-object p3, p0, LX/20a;->c:LX/13Q;

    .line 354985
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 354974
    sget-object v0, LX/1wM;->a:[I

    iget-object v1, p0, LX/20a;->a:LX/20X;

    invoke-virtual {v1}, LX/20X;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 354975
    sget-object v0, LX/20a;->d:Ljava/lang/Class;

    const-string v1, "Unexpected button ID: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/20a;->a:LX/20X;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354976
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 354977
    :pswitch_0
    const-string v0, "button_press_like"

    goto :goto_0

    .line 354978
    :pswitch_1
    const-string v0, "button_press_comment"

    goto :goto_0

    .line 354979
    :pswitch_2
    const-string v0, "button_press_share"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1da08cd1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 354967
    invoke-direct {p0}, LX/20a;->a()Ljava/lang/String;

    move-result-object v1

    .line 354968
    iget-object v2, p0, LX/20a;->c:LX/13Q;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 354969
    iget-object v2, p0, LX/20a;->c:LX/13Q;

    invoke-virtual {v2, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 354970
    :cond_0
    iget-object v1, p0, LX/20a;->b:LX/20Z;

    iget-object v2, p0, LX/20a;->a:LX/20X;

    invoke-interface {v1, p1, v2}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 354971
    const v1, 0x3b5775d7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
