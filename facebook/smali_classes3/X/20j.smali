.class public LX/20j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;

.field private static volatile c:LX/20j;


# instance fields
.field public final b:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 355650
    new-instance v0, LX/20k;

    invoke-direct {v0}, LX/20k;-><init>()V

    sput-object v0, LX/20j;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 355651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355652
    iput-object p1, p0, LX/20j;->b:LX/0SG;

    .line 355653
    return-void
.end method

.method public static a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)I
    .locals 1

    .prologue
    .line 355654
    invoke-virtual {p0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->n()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;

    move-result-object v0

    .line 355655
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ViewerFeedbackReactionModel;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 355656
    if-nez p1, :cond_0

    .line 355657
    :goto_0
    return-object p0

    .line 355658
    :cond_0
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 355659
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355660
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 355661
    :cond_1
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 355662
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 355663
    :cond_2
    invoke-virtual {v3, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355664
    const/4 v0, 0x1

    goto :goto_2

    .line 355665
    :cond_3
    if-nez v1, :cond_4

    .line 355666
    invoke-virtual {v3, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355667
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/20j;
    .locals 4

    .prologue
    .line 355668
    sget-object v0, LX/20j;->c:LX/20j;

    if-nez v0, :cond_1

    .line 355669
    const-class v1, LX/20j;

    monitor-enter v1

    .line 355670
    :try_start_0
    sget-object v0, LX/20j;->c:LX/20j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 355671
    if-eqz v2, :cond_0

    .line 355672
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 355673
    new-instance p0, LX/20j;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/20j;-><init>(LX/0SG;)V

    .line 355674
    move-object v0, p0

    .line 355675
    sput-object v0, LX/20j;->c:LX/20j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355676
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 355677
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 355678
    :cond_1
    sget-object v0, LX/20j;->c:LX/20j;

    return-object v0

    .line 355679
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 355680
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355681
    if-nez p0, :cond_0

    new-instance v0, LX/4ZH;

    invoke-direct {v0}, LX/4ZH;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/4ZH;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 8

    .prologue
    .line 355594
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355595
    :cond_0
    :goto_0
    return-object p0

    .line 355596
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355597
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355598
    if-eqz v1, :cond_0

    .line 355599
    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v4

    .line 355600
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    .line 355601
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 355602
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 355603
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 355604
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 355605
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 355606
    :cond_3
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355607
    iput-object v2, v4, LX/4XR;->jC:LX/0Px;

    .line 355608
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 355609
    const/4 v3, 0x0

    .line 355610
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 355611
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 355612
    const/4 v2, 0x1

    .line 355613
    :goto_1
    if-nez v2, :cond_5

    .line 355614
    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355615
    :cond_5
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355616
    iput-object v2, v4, LX/4XR;->cn:LX/0Px;

    .line 355617
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    move-object v1, v2

    .line 355618
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355619
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355620
    move-object v0, v0

    .line 355621
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355622
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355623
    move-object v0, v2

    .line 355624
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto/16 :goto_0

    :cond_6
    move v2, v3

    goto :goto_1
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 6

    .prologue
    .line 355682
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355683
    :cond_0
    :goto_0
    return-object p0

    .line 355684
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355685
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355686
    if-eqz v1, :cond_0

    .line 355687
    if-eqz p2, :cond_4

    .line 355688
    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v3

    .line 355689
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    .line 355690
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 355691
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 355692
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 355693
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 355694
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 355695
    :cond_3
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355696
    iput-object v2, v3, LX/4XR;->cn:LX/0Px;

    .line 355697
    move-object v2, v3

    .line 355698
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    move-object v1, v2

    .line 355699
    :goto_1
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355700
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355701
    move-object v0, v0

    .line 355702
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355703
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355704
    move-object v0, v2

    .line 355705
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0

    .line 355706
    :cond_4
    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v3

    .line 355707
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    .line 355708
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 355709
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 355710
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 355711
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 355712
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 355713
    :cond_6
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355714
    iput-object v2, v3, LX/4XR;->jC:LX/0Px;

    .line 355715
    move-object v2, v3

    .line 355716
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    move-object v1, v2

    .line 355717
    goto :goto_1
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3

    .prologue
    .line 355718
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355719
    :cond_0
    :goto_0
    return-object p0

    .line 355720
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355721
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355722
    if-eqz v1, :cond_0

    .line 355723
    invoke-static {v1, p1}, LX/6Ov;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355724
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355725
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355726
    move-object v0, v0

    .line 355727
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355728
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355729
    move-object v0, v2

    .line 355730
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 7

    .prologue
    .line 355731
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355732
    :cond_0
    :goto_0
    return-object p0

    .line 355733
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355734
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355735
    if-eqz v1, :cond_0

    .line 355736
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v2

    .line 355737
    if-nez v2, :cond_2

    .line 355738
    :goto_1
    move-object v1, v1

    .line 355739
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355740
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355741
    move-object v0, v0

    .line 355742
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355743
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355744
    move-object v0, v2

    .line 355745
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0

    .line 355746
    :cond_2
    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v3

    .line 355747
    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 355748
    :cond_3
    :goto_2
    move-object v2, v2

    .line 355749
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355750
    iput-object v2, v3, LX/4XR;->hE:LX/0Px;

    .line 355751
    move-object v2, v3

    .line 355752
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    goto :goto_1

    .line 355753
    :cond_4
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    .line 355754
    const/4 v4, 0x0

    move v5, v4

    :goto_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 355755
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 355756
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 355757
    invoke-interface {v6, v5, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 355758
    :cond_5
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    goto :goto_2

    .line 355759
    :cond_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355760
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355761
    :cond_0
    :goto_0
    return-object p0

    .line 355762
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355763
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355764
    if-eqz v1, :cond_0

    .line 355765
    invoke-static {v1, p1}, LX/6Ov;->c(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355766
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355767
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355768
    move-object v0, v0

    .line 355769
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355770
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355771
    move-object v0, v2

    .line 355772
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3

    .prologue
    .line 355773
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355774
    :cond_0
    :goto_0
    return-object p0

    .line 355775
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355776
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355777
    if-eqz v1, :cond_0

    .line 355778
    invoke-static {v1, p1, p2}, LX/6Ov;->a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355779
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355780
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355781
    move-object v0, v0

    .line 355782
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355783
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355784
    move-object v0, v2

    .line 355785
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 5

    .prologue
    .line 355786
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355787
    :cond_0
    :goto_0
    return-object p1

    .line 355788
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355789
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355790
    if-eqz v1, :cond_0

    .line 355791
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v2

    .line 355792
    if-nez v2, :cond_2

    .line 355793
    :goto_1
    move-object v1, v1

    .line 355794
    invoke-static {p1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355795
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355796
    move-object v0, v0

    .line 355797
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355798
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355799
    move-object v0, v2

    .line 355800
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p1

    goto :goto_0

    .line 355801
    :cond_2
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 355802
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355803
    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v3

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 355804
    iput-object v2, v3, LX/4XR;->hE:LX/0Px;

    .line 355805
    move-object v3, v3

    .line 355806
    if-eqz p0, :cond_5

    .line 355807
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 355808
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 355809
    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 355810
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 355811
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 355812
    :cond_4
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 355813
    iput-object v2, v3, LX/4XR;->jA:LX/0Px;

    .line 355814
    :cond_5
    invoke-virtual {v3}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;LX/0QK;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLFeedback;"
        }
    .end annotation

    .prologue
    .line 355815
    if-nez p3, :cond_0

    .line 355816
    :goto_0
    return-object p1

    :cond_0
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    iget-object v1, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 355817
    iput-wide v2, v0, LX/3dM;->v:J

    .line 355818
    move-object v0, v0

    .line 355819
    invoke-static {p1, p2, p3}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;LX/0QK;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/6PR;Z)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5

    .prologue
    .line 355820
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355821
    :cond_0
    :goto_0
    return-object p1

    .line 355822
    :cond_1
    sget-object v0, LX/6PR;->ADD:LX/6PR;

    invoke-virtual {p3, v0}, LX/6PR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p2}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    sget-object v0, LX/6PR;->EDIT:LX/6PR;

    invoke-virtual {p3, v0}, LX/6PR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p2}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355823
    :cond_3
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    iget-object v1, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 355824
    iput-wide v2, v0, LX/3dM;->v:J

    .line 355825
    move-object v0, v0

    .line 355826
    invoke-static {p1, p2, p4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Z)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3dM;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;I",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLFeedback;"
        }
    .end annotation

    .prologue
    .line 355827
    iget-object v0, p0, LX/3dM;->O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-static {v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;

    move-result-object v0

    .line 355828
    iput p2, v0, LX/4ZH;->b:I

    .line 355829
    move-object v0, v0

    .line 355830
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 355831
    iput-object v1, v0, LX/4ZH;->c:LX/0Px;

    .line 355832
    move-object v0, v0

    .line 355833
    iput-object p3, v0, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 355834
    move-object v0, v0

    .line 355835
    invoke-virtual {v0}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    move-result-object v0

    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 355836
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 355837
    :cond_0
    :goto_0
    return-object p0

    .line 355838
    :cond_1
    invoke-static {p0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    .line 355839
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->j(Z)LX/3dM;

    .line 355840
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 355841
    iput-object v1, v0, LX/3dM;->E:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 355842
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->Y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 355843
    iput-object v1, v0, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 355844
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->U()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 355845
    iput-object v1, v0, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 355846
    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    .line 355847
    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 355848
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355849
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 355850
    sget-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    if-ne p2, v0, :cond_2

    .line 355851
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_5

    .line 355852
    :cond_0
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    .line 355853
    :goto_0
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-nez v1, :cond_7

    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    .line 355854
    :goto_1
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 355855
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v2

    .line 355856
    iput-object v2, v1, LX/17L;->f:Ljava/lang/String;

    .line 355857
    move-object v2, v1

    .line 355858
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v3

    .line 355859
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 355860
    :cond_1
    invoke-static {p0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v2, v0, v3, v1}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    move-object v0, v0

    .line 355861
    :goto_2
    return-object v0

    .line 355862
    :cond_2
    if-eqz p1, :cond_3

    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-nez v0, :cond_9

    .line 355863
    :cond_3
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    .line 355864
    :goto_3
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-nez v1, :cond_b

    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    .line 355865
    :goto_4
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 355866
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 355867
    iput-object v2, v1, LX/17L;->c:Ljava/lang/String;

    .line 355868
    move-object v2, v1

    .line 355869
    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v3

    .line 355870
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 355871
    :cond_4
    invoke-static {p0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v2, v0, v3, v1}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    move-object v0, v0

    .line 355872
    goto :goto_2

    .line 355873
    :cond_5
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 355874
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v4, :cond_8

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355875
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 355876
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 355877
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 355878
    :cond_7
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v1}, LX/17L;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;

    move-result-object v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_0

    .line 355879
    :cond_9
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 355880
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v4, :cond_c

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355881
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 355882
    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    invoke-interface {v1, p2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 355883
    :goto_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 355884
    :cond_a
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_7

    .line 355885
    :cond_b
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-static {v1}, LX/17L;->a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/17L;

    move-result-object v1

    goto/16 :goto_4

    :cond_c
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public static a(I)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;
    .locals 1

    .prologue
    .line 355886
    if-nez p0, :cond_0

    .line 355887
    const/4 v0, 0x0

    .line 355888
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4WL;

    invoke-direct {v0}, LX/4WL;-><init>()V

    .line 355889
    iput p0, v0, LX/4WL;->b:I

    .line 355890
    move-object v0, v0

    .line 355891
    invoke-virtual {v0}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 7

    .prologue
    .line 355892
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 355893
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 355894
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 355895
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355896
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 355897
    :cond_1
    invoke-static {p0}, LX/3dN;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 355898
    iput v1, v0, LX/3dN;->b:I

    .line 355899
    move-object v0, v0

    .line 355900
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 355901
    iput-object v1, v0, LX/3dN;->c:LX/0Px;

    .line 355902
    move-object v0, v0

    .line 355903
    invoke-static {p0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 355904
    iput-object v1, v0, LX/3dN;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 355905
    move-object v0, v0

    .line 355906
    invoke-virtual {v0}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;IIILcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;
    .locals 7

    .prologue
    .line 355907
    new-instance v0, LX/3dO;

    invoke-direct {v0}, LX/3dO;-><init>()V

    .line 355908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->G()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;->j()LX/0Px;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 355909
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 355910
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    .line 355911
    new-instance v2, LX/4YX;

    invoke-direct {v2}, LX/4YX;-><init>()V

    .line 355912
    iput-object p4, v2, LX/4YX;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 355913
    move-object v2, v2

    .line 355914
    invoke-static {p2}, LX/20j;->b(I)Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v3

    .line 355915
    iput-object v3, v2, LX/4YX;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 355916
    move-object v2, v2

    .line 355917
    invoke-virtual {v2}, LX/4YX;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355918
    invoke-virtual {v4, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 355919
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 355920
    iput-object v1, v0, LX/3dO;->c:LX/0Px;

    .line 355921
    move-object v0, v0

    .line 355922
    iput p3, v0, LX/3dO;->b:I

    .line 355923
    move-object v0, v0

    .line 355924
    invoke-virtual {v0}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    return-object v0

    .line 355925
    :cond_1
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 355926
    goto :goto_0

    .line 355927
    :cond_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    .line 355928
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 355929
    if-eqz p2, :cond_3

    .line 355930
    new-instance v6, LX/4YX;

    invoke-direct {v6}, LX/4YX;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;->j()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 355931
    iput-object v2, v6, LX/4YX;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 355932
    move-object v2, v6

    .line 355933
    invoke-static {p2}, LX/20j;->b(I)Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v6

    .line 355934
    iput-object v6, v2, LX/4YX;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 355935
    move-object v2, v2

    .line 355936
    invoke-virtual {v2}, LX/4YX;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentEdge;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355937
    :cond_3
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 355938
    :cond_4
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;Z)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 355625
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 355626
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v4

    .line 355627
    if-nez p2, :cond_0

    invoke-virtual {v4, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 355628
    invoke-virtual {v3, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355629
    :cond_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355630
    invoke-virtual {p1, v0}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 355631
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 355632
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 355633
    :cond_1
    if-nez v1, :cond_4

    .line 355634
    invoke-virtual {v3, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355635
    const/4 v0, 0x1

    goto :goto_1

    .line 355636
    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {v4, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 355637
    invoke-virtual {v3, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355638
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 355639
    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355640
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v1

    invoke-static {v1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;

    move-result-object v1

    .line 355641
    iput v0, v1, LX/4ZH;->b:I

    .line 355642
    move-object v0, v1

    .line 355643
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 355644
    iput-object v1, v0, LX/4ZH;->c:LX/0Px;

    .line 355645
    move-object v0, v0

    .line 355646
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 355647
    iput-object v1, v0, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 355648
    move-object v0, v0

    .line 355649
    invoke-virtual {v0}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;II)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .locals 10
    .param p0    # Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355939
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;->a()LX/0Px;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 355940
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 355941
    if-ne p1, p2, :cond_1

    .line 355942
    :goto_1
    move-object v0, v0

    .line 355943
    new-instance v1, LX/3dQ;

    invoke-direct {v1}, LX/3dQ;-><init>()V

    .line 355944
    iput-object v0, v1, LX/3dQ;->b:LX/0Px;

    .line 355945
    move-object v0, v1

    .line 355946
    invoke-virtual {v0}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    return-object v0

    .line 355947
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 355948
    goto :goto_0

    .line 355949
    :cond_1
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 355950
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v9

    move v7, v1

    move v2, v1

    move v5, v1

    :goto_2
    if-ge v7, v9, :cond_4

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    .line 355951
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result p0

    .line 355952
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result v4

    .line 355953
    const/4 v3, 0x0

    .line 355954
    if-ne p1, p0, :cond_2

    .line 355955
    add-int/lit8 v4, v4, -0x1

    .line 355956
    :goto_3
    if-nez v3, :cond_7

    if-lez v4, :cond_7

    .line 355957
    new-instance v3, LX/4ZJ;

    invoke-direct {v3}, LX/4ZJ;-><init>()V

    .line 355958
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 355959
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object p0

    iput-object p0, v3, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 355960
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->j()I

    move-result p0

    iput p0, v3, LX/4ZJ;->c:I

    .line 355961
    invoke-static {v3, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 355962
    move-object v1, v3

    .line 355963
    iput v4, v1, LX/4ZJ;->c:I

    .line 355964
    move-object v1, v1

    .line 355965
    invoke-virtual {v1}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v1

    .line 355966
    :goto_4
    if-eqz v1, :cond_6

    .line 355967
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355968
    add-int/lit8 v1, v2, 0x1

    .line 355969
    :goto_5
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v2, v1

    goto :goto_2

    .line 355970
    :cond_2
    if-ne p2, p0, :cond_3

    .line 355971
    add-int/lit8 v4, v4, 0x1

    move v5, v6

    .line 355972
    goto :goto_3

    :cond_3
    move-object v3, v1

    .line 355973
    goto :goto_3

    .line 355974
    :cond_4
    if-nez v5, :cond_5

    if-eqz p2, :cond_5

    const/4 v1, 0x4

    if-ge v2, v1, :cond_5

    .line 355975
    new-instance v1, LX/4ZJ;

    invoke-direct {v1}, LX/4ZJ;-><init>()V

    new-instance v2, LX/4WM;

    invoke-direct {v2}, LX/4WM;-><init>()V

    .line 355976
    iput p2, v2, LX/4WM;->f:I

    .line 355977
    move-object v2, v2

    .line 355978
    invoke-virtual {v2}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v2

    .line 355979
    iput-object v2, v1, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 355980
    move-object v1, v1

    .line 355981
    iput v6, v1, LX/4ZJ;->c:I

    .line 355982
    move-object v1, v1

    .line 355983
    invoke-virtual {v1}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v1

    .line 355984
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355985
    :cond_5
    sget-object v1, LX/20j;->a:Ljava/util/Comparator;

    invoke-static {v8, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 355986
    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move v1, v2

    goto :goto_5

    :cond_7
    move-object v1, v3

    goto :goto_4
.end method

.method private static a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 355502
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLComment;
    .locals 3

    .prologue
    .line 355489
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 355490
    :cond_0
    :goto_0
    return-object p0

    .line 355491
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLComment;->j()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355492
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355493
    if-eqz v1, :cond_0

    .line 355494
    invoke-static {v1, p1}, LX/6Ov;->d(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 355495
    invoke-static {p0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v2

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    .line 355496
    iput-object v1, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 355497
    move-object v0, v0

    .line 355498
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 355499
    iput-object v0, v2, LX/4Vu;->d:LX/0Px;

    .line 355500
    move-object v0, v2

    .line 355501
    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object p0

    goto :goto_0
.end method

.method public static b(I)Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;
    .locals 1

    .prologue
    .line 355485
    new-instance v0, LX/4WM;

    invoke-direct {v0}, LX/4WM;-><init>()V

    .line 355486
    iput p0, v0, LX/4WM;->f:I

    .line 355487
    move-object v0, v0

    .line 355488
    invoke-virtual {v0}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;
    .locals 3

    .prologue
    .line 355372
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 355373
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 355374
    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355375
    invoke-static {p0}, LX/3dN;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 355376
    iput v2, v1, LX/3dN;->b:I

    .line 355377
    move-object v1, v1

    .line 355378
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 355379
    iput-object v0, v1, LX/3dN;->c:LX/0Px;

    .line 355380
    move-object v0, v1

    .line 355381
    invoke-static {p0}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 355382
    iput-object v1, v0, LX/3dN;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 355383
    move-object v0, v0

    .line 355384
    invoke-virtual {v0}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLFeedback;LX/0QK;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;"
        }
    .end annotation

    .prologue
    .line 355457
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 355458
    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    .line 355459
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 355460
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355461
    invoke-interface {p1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 355462
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355463
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 355464
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    invoke-static {v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;

    move-result-object v0

    add-int/lit8 v1, v3, -0x1

    .line 355465
    iput v1, v0, LX/4ZH;->b:I

    .line 355466
    move-object v0, v0

    .line 355467
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 355468
    iput-object v1, v0, LX/4ZH;->c:LX/0Px;

    .line 355469
    move-object v0, v0

    .line 355470
    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 355471
    iput-object v1, v0, LX/4ZH;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 355472
    move-object v0, v0

    .line 355473
    invoke-virtual {v0}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 7

    .prologue
    .line 355445
    if-nez p1, :cond_1

    .line 355446
    :cond_0
    :goto_0
    return-object p0

    .line 355447
    :cond_1
    const/4 v1, 0x0

    .line 355448
    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355449
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 355450
    :goto_2
    if-eqz v0, :cond_0

    .line 355451
    invoke-static {v0}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v1

    .line 355452
    iput-object p1, v1, LX/4Vu;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 355453
    move-object v1, v1

    .line 355454
    invoke-virtual {v1}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355455
    invoke-static {p0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    invoke-static {p0}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    invoke-static {p0}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    goto :goto_0

    .line 355456
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Ljava/lang/String;Z)LX/6PS;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355430
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 355431
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 355432
    :goto_0
    return-object v0

    .line 355433
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355434
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 355435
    :goto_2
    if-nez v0, :cond_2

    move-object v0, v1

    .line 355436
    goto :goto_0

    .line 355437
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 355438
    :cond_2
    invoke-static {v0, p2, p4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355439
    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    .line 355440
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    iget-object v3, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 355441
    iput-wide v4, v2, LX/3dM;->v:J

    .line 355442
    move-object v2, v2

    .line 355443
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 355444
    new-instance v0, LX/6PS;

    invoke-direct {v0, v2, v1}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLPage;Ljava/lang/String;)LX/6PS;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355415
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 355416
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 355417
    :goto_0
    return-object v0

    .line 355418
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355419
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 355420
    :goto_2
    if-nez v0, :cond_2

    move-object v0, v1

    .line 355421
    goto :goto_0

    .line 355422
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 355423
    :cond_2
    invoke-static {v0, p2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355424
    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    .line 355425
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    iget-object v3, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 355426
    iput-wide v4, v2, LX/3dM;->v:J

    .line 355427
    move-object v2, v2

    .line 355428
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 355429
    new-instance v0, LX/6PS;

    invoke-direct {v0, v2, v1}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355400
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 355401
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 355402
    :goto_0
    return-object v0

    .line 355403
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355404
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 355405
    :goto_2
    if-nez v0, :cond_3

    move-object v0, v1

    .line 355406
    goto :goto_0

    .line 355407
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 355408
    :cond_3
    invoke-static {v0, p3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355409
    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    .line 355410
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    iget-object v3, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 355411
    iput-wide v4, v2, LX/3dM;->v:J

    .line 355412
    move-object v2, v2

    .line 355413
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 355414
    new-instance v0, LX/6PS;

    invoke-direct {v0, v2, v1}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Z)LX/6PS;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355385
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 355386
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 355387
    :goto_0
    return-object v0

    .line 355388
    :cond_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355389
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 355390
    :goto_2
    if-nez v0, :cond_2

    move-object v0, v1

    .line 355391
    goto :goto_0

    .line 355392
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 355393
    :cond_2
    invoke-static {v0, p3, p4}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355394
    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    .line 355395
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    iget-object v3, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 355396
    iput-wide v4, v2, LX/3dM;->v:J

    .line 355397
    move-object v2, v2

    .line 355398
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 355399
    new-instance v0, LX/6PS;

    invoke-direct {v0, v2, v1}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 355503
    invoke-static {p2}, LX/1zt;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 355504
    invoke-static {p3}, LX/20j;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;)I

    move-result v2

    .line 355505
    invoke-static {p2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v3

    .line 355506
    invoke-virtual {p2}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v4

    iput-object v4, v3, LX/3dM;->b:LX/15i;

    .line 355507
    invoke-virtual {p2}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v4

    iput v4, v3, LX/3dM;->c:I

    .line 355508
    iget-object v4, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 355509
    iput-wide v4, v3, LX/3dM;->v:J

    .line 355510
    move-object v4, v3

    .line 355511
    if-ne v2, v0, :cond_1

    :goto_0
    invoke-virtual {v4, v0}, LX/3dM;->j(Z)LX/3dM;

    move-result-object v0

    invoke-static {v2}, LX/20j;->a(I)Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v4

    .line 355512
    iput-object v4, v0, LX/3dM;->X:Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    .line 355513
    iget-object v5, v0, LX/3dM;->b:LX/15i;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/3dM;->b:LX/15i;

    .line 355514
    iget-boolean v6, v5, LX/15i;->g:Z

    move v5, v6

    .line 355515
    if-eqz v5, :cond_0

    .line 355516
    iget-object v5, v0, LX/3dM;->b:LX/15i;

    iget v6, v0, LX/3dM;->c:I

    const/16 p0, 0x2c

    invoke-virtual {v5, v6, p0, v4}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 355517
    :cond_0
    move-object v0, v0

    .line 355518
    invoke-virtual {v0, v2}, LX/3dM;->b(I)LX/3dM;

    .line 355519
    invoke-static {p2}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-static {v0}, LX/3dN;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->l()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$LikersModel;->a()I

    move-result v4

    .line 355520
    iput v4, v0, LX/3dN;->b:I

    .line 355521
    move-object v0, v0

    .line 355522
    invoke-virtual {v0}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 355523
    invoke-virtual {p3}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel;->m()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsMutationFragmentModel$FeedbackModel$ReactorsModel;->a()I

    move-result v0

    invoke-static {p2, v1, v2, v0, p1}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;IIILcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 355524
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-static {v0, v1, v2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;II)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 355525
    invoke-virtual {v3}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0

    .line 355526
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 355527
    if-nez p1, :cond_0

    .line 355528
    const/4 v0, 0x0

    .line 355529
    :goto_0
    return-object v0

    .line 355530
    :cond_0
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v1

    iget-object v2, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 355531
    iput-wide v2, v1, LX/3dM;->v:J

    .line 355532
    move-object v3, v1

    .line 355533
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 355534
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355535
    const/4 v8, 0x0

    .line 355536
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->p()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v9, v8

    :goto_2
    if-ge v9, v11, :cond_5

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 355537
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    .line 355538
    if-eqz v7, :cond_4

    .line 355539
    invoke-static {v7}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v7

    .line 355540
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v12

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v12, p0, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v12

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->ENCODED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v12, p0, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v12

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADING:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-eq v12, p0, :cond_1

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVideo;->bf()Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    move-result-object v7

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLVideoStatusType;->UPLOADED:Lcom/facebook/graphql/enums/GraphQLVideoStatusType;

    if-ne v7, v12, :cond_4

    .line 355541
    :cond_1
    const/4 v7, 0x1

    .line 355542
    :goto_3
    move v7, v7

    .line 355543
    if-eqz v7, :cond_2

    .line 355544
    add-int/lit8 v0, v1, 0x1

    .line 355545
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 355546
    :cond_2
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    goto :goto_4

    .line 355547
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    sub-int v1, v2, v1

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-static {v3, v0, v1, v2}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0

    .line 355548
    :cond_4
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    goto :goto_2

    :cond_5
    move v7, v8

    .line 355549
    goto :goto_3
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355550
    if-nez p1, :cond_0

    .line 355551
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, p1, p2, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5

    .prologue
    .line 355474
    if-nez p1, :cond_1

    .line 355475
    :cond_0
    :goto_0
    return-object p1

    .line 355476
    :cond_1
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eq p3, v0, :cond_0

    .line 355477
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v1

    .line 355478
    iget-object v0, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 355479
    iput-wide v2, v1, LX/3dM;->v:J

    .line 355480
    invoke-virtual {v1, p3}, LX/3dM;->j(Z)LX/3dM;

    .line 355481
    if-eqz p3, :cond_2

    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    if-nez p3, :cond_3

    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v0

    if-eqz v0, :cond_4

    .line 355482
    :cond_3
    if-eqz p3, :cond_5

    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-static {v0, p2}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 355483
    :cond_4
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0

    .line 355484
    :cond_5
    invoke-static {p1}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    invoke-static {v0, p2}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 355552
    sget-object v0, LX/6PR;->ADD:LX/6PR;

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/20j;->a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/6PR;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 5

    .prologue
    .line 355553
    if-nez p1, :cond_0

    .line 355554
    :goto_0
    return-object p1

    .line 355555
    :cond_0
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    .line 355556
    iget-object v1, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 355557
    iput-wide v2, v0, LX/3dM;->v:J

    .line 355558
    invoke-virtual {v0, p2}, LX/3dM;->l(Z)LX/3dM;

    .line 355559
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;)LX/6PS;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355560
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v3

    .line 355561
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v1

    .line 355562
    :goto_0
    return-object v0

    .line 355563
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355564
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 355565
    :goto_2
    if-nez v0, :cond_3

    move-object v0, v1

    .line 355566
    goto :goto_0

    .line 355567
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 355568
    :cond_3
    invoke-static {v0, p3}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 355569
    invoke-static {v3, v0, v1}, LX/20j;->a(LX/0Px;Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v0

    .line 355570
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    iget-object v3, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 355571
    iput-wide v4, v2, LX/3dM;->v:J

    .line 355572
    move-object v2, v2

    .line 355573
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    invoke-static {v2, v0, v3, v4}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 355574
    new-instance v0, LX/6PS;

    invoke-direct {v0, v2, v1}, LX/6PS;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 355575
    sget-object v0, LX/6PR;->EDIT:LX/6PR;

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/20j;->a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/6PR;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2
    .param p2    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 355576
    if-nez p2, :cond_0

    .line 355577
    :goto_0
    return-object p1

    .line 355578
    :cond_0
    sget-object v0, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-static {p1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/5Gs;->LOAD_AFTER:LX/5Gs;

    .line 355579
    :goto_1
    invoke-static {p1, p2, v0}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0

    .line 355580
    :cond_1
    sget-object v0, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    goto :goto_1
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 355581
    sget-object v0, LX/6PR;->ADD_OR_EDIT:LX/6PR;

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/20j;->a(LX/20j;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;LX/6PR;Z)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 8

    .prologue
    .line 355582
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 355583
    :cond_0
    :goto_0
    return-object p1

    .line 355584
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 355585
    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    .line 355586
    invoke-static {p1}, LX/16z;->h(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    .line 355587
    invoke-virtual {p2, v0}, Lcom/facebook/graphql/model/GraphQLComment;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 355588
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 355589
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 355590
    :cond_3
    invoke-static {p1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v0

    iget-object v1, p0, LX/20j;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 355591
    iput-wide v4, v0, LX/3dM;->v:J

    .line 355592
    move-object v0, v0

    .line 355593
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    add-int/lit8 v2, v3, -0x1

    invoke-static {p1}, LX/16z;->g(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/20j;->a(LX/3dM;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLPageInfo;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method
