.class public LX/1zP;
.super LX/1zQ;
.source ""


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final c:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final d:LX/0id;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/17V;

.field private final g:LX/0Zb;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0bH;

.field private final j:LX/1EQ;

.field private final k:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/performancelogger/PerformanceLogger;LX/0id;LX/17V;LX/0Zb;LX/0Ot;LX/1EQ;LX/0Uh;LX/0bH;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/0id;",
            "LX/17V;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/1EQ;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0bH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353189
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/1zQ;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V

    .line 353190
    iput-object p2, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 353191
    iput-object p7, p0, LX/1zP;->f:LX/17V;

    .line 353192
    iput-object p8, p0, LX/1zP;->g:LX/0Zb;

    .line 353193
    iput-object p4, p0, LX/1zP;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 353194
    iput-object p5, p0, LX/1zP;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 353195
    iput-object p6, p0, LX/1zP;->d:LX/0id;

    .line 353196
    iput-object p3, p0, LX/1zP;->e:Lcom/facebook/content/SecureContextHelper;

    .line 353197
    iput-object p9, p0, LX/1zP;->h:LX/0Ot;

    .line 353198
    iput-object p12, p0, LX/1zP;->i:LX/0bH;

    .line 353199
    iput-object p10, p0, LX/1zP;->j:LX/1EQ;

    .line 353200
    iput-object p11, p0, LX/1zP;->k:LX/0Uh;

    .line 353201
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 353202
    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 353203
    :goto_0
    return-void

    .line 353204
    :cond_0
    iget-object v0, p0, LX/1zP;->d:LX/0id;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "StoryContinueReadingSpan"

    invoke-virtual {v0, v1, v2}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 353205
    iget-object v0, p0, LX/1zP;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0008

    const-string v2, "NNF_PermalinkFromFeedLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 353206
    iget-object v0, p0, LX/1zP;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_continue_reading"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 353207
    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353208
    const/4 v0, 0x1

    iget-object v1, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, LX/17V;->a(ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 353209
    iget-object v1, p0, LX/1zP;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 353210
    :cond_1
    :goto_1
    new-instance v0, LX/1ZT;

    iget-object v1, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v0, v1}, LX/1ZT;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 353211
    iget-object v1, p0, LX/1zP;->i:LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 353212
    iget-object v1, p0, LX/1zP;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 353213
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 353214
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v1, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 353215
    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 353216
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v2

    const-string v3, "story_view"

    .line 353217
    iput-object v3, v2, LX/21A;->c:Ljava/lang/String;

    .line 353218
    move-object v2, v2

    .line 353219
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/82k;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 353220
    iput-object v3, v2, LX/21A;->d:Ljava/lang/String;

    .line 353221
    move-object v2, v2

    .line 353222
    iput-object v0, v2, LX/21A;->a:LX/162;

    .line 353223
    move-object v2, v2

    .line 353224
    iget-object v3, p0, LX/1zP;->j:LX/1EQ;

    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 353225
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 353226
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v0, v2}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 353227
    const-string v0, "feedback_logging_params"

    invoke-virtual {v2}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 353228
    iget-object v0, p0, LX/1zP;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 353229
    :cond_2
    iget-object v0, p0, LX/1zP;->k:LX/0Uh;

    const/16 v1, 0x82

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353230
    iget-object v0, p0, LX/1zP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    const-string v1, "native_newsfeed"

    invoke-static {v3, v0, v1}, LX/17V;->a(ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 353231
    iget-object v1, p0, LX/1zP;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1
.end method
