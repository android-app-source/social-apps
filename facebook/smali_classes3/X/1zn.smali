.class public LX/1zn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:[LX/1zo;


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:LX/1zf;

.field public final e:LX/1zi;

.field public final f:LX/1zk;

.field private final g:LX/1zp;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final i:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final j:LX/1zq;

.field public final k:Ljava/util/concurrent/Executor;

.field public final l:LX/1zr;

.field public final m:LX/1zl;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 354134
    const-class v0, LX/1zn;

    sput-object v0, LX/1zn;->a:Ljava/lang/Class;

    .line 354135
    const/4 v0, 0x4

    new-array v0, v0, [LX/1zo;

    const/4 v1, 0x0

    sget-object v2, LX/1zo;->SMALL:LX/1zo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/1zo;->LARGE:LX/1zo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/1zo;->TAB_ICONS:LX/1zo;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/1zo;->VECTOR:LX/1zo;

    aput-object v2, v0, v1

    sput-object v0, LX/1zn;->b:[LX/1zo;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1zf;LX/1zi;LX/1zk;LX/1zp;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/1zq;Ljava/util/concurrent/ExecutorService;LX/1zr;LX/1zl;)V
    .locals 0
    .param p2    # LX/1zf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1zi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354137
    iput-object p1, p0, LX/1zn;->c:Landroid/content/Context;

    .line 354138
    iput-object p2, p0, LX/1zn;->d:LX/1zf;

    .line 354139
    iput-object p3, p0, LX/1zn;->e:LX/1zi;

    .line 354140
    iput-object p4, p0, LX/1zn;->f:LX/1zk;

    .line 354141
    iput-object p5, p0, LX/1zn;->g:LX/1zp;

    .line 354142
    iput-object p6, p0, LX/1zn;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 354143
    iput-object p7, p0, LX/1zn;->i:Landroid/os/Handler;

    .line 354144
    iput-object p8, p0, LX/1zn;->j:LX/1zq;

    .line 354145
    iput-object p9, p0, LX/1zn;->k:Ljava/util/concurrent/Executor;

    .line 354146
    iput-object p10, p0, LX/1zn;->l:LX/1zr;

    .line 354147
    iput-object p11, p0, LX/1zn;->m:LX/1zl;

    .line 354148
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 14

    .prologue
    .line 354149
    iget-object v0, p0, LX/1zn;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 354150
    iget-object v0, p0, LX/1zn;->g:LX/1zp;

    const/4 v2, 0x0

    new-instance v3, LX/1zy;

    invoke-direct {v3, p0}, LX/1zy;-><init>(LX/1zn;)V

    iget-object v4, p0, LX/1zn;->j:LX/1zq;

    move v5, p1

    .line 354151
    if-nez v3, :cond_0

    .line 354152
    :goto_0
    return-void

    .line 354153
    :cond_0
    iget-object v13, v0, LX/1zp;->d:LX/0Sh;

    new-instance v6, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    move-object v7, v0

    move-object v8, v4

    move v9, v1

    move v10, v2

    move v11, v5

    move-object v12, v3

    invoke-direct/range {v6 .. v12}, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;-><init>(LX/1zp;LX/1zq;FZZLX/0TF;)V

    invoke-virtual {v13, v6}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
