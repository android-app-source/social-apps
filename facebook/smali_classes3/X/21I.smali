.class public final LX/21I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:LX/20b;

.field public final c:LX/20Z;

.field public final d:LX/21M;

.field public final e:LX/0wd;

.field public final f:LX/20z;

.field public final g:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/20I;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/21H;


# direct methods
.method public constructor <init>(ZLX/20b;LX/20Z;LX/21M;LX/0wd;LX/20z;Ljava/util/EnumMap;LX/20I;LX/0Px;LX/21H;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/20b;",
            "LX/20Z;",
            "LX/21M;",
            "LX/0wd;",
            "LX/20z;",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;",
            "LX/20I;",
            "LX/0Px",
            "<",
            "LX/1zt;",
            ">;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356976
    iput-boolean p1, p0, LX/21I;->a:Z

    .line 356977
    iput-object p2, p0, LX/21I;->b:LX/20b;

    .line 356978
    iput-object p3, p0, LX/21I;->c:LX/20Z;

    .line 356979
    iput-object p4, p0, LX/21I;->d:LX/21M;

    .line 356980
    iput-object p5, p0, LX/21I;->e:LX/0wd;

    .line 356981
    iput-object p6, p0, LX/21I;->f:LX/20z;

    .line 356982
    iput-object p7, p0, LX/21I;->g:Ljava/util/EnumMap;

    .line 356983
    iput-object p8, p0, LX/21I;->h:LX/20I;

    .line 356984
    iput-object p9, p0, LX/21I;->i:LX/0Px;

    .line 356985
    iput-object p10, p0, LX/21I;->j:LX/21H;

    .line 356986
    return-void
.end method
