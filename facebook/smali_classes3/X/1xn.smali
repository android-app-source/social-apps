.class public final LX/1xn;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public final synthetic g:LX/1xU;


# direct methods
.method public constructor <init>(LX/1xU;)V
    .locals 1

    .prologue
    .line 349365
    iput-object p1, p0, LX/1xn;->g:LX/1xU;

    .line 349366
    move-object v0, p1

    .line 349367
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 349368
    const/4 v0, 0x0

    iput v0, p0, LX/1xn;->c:I

    .line 349369
    const v0, 0x7fffffff

    iput v0, p0, LX/1xn;->d:I

    .line 349370
    sget v0, LX/1X3;->a:I

    iput v0, p0, LX/1xn;->f:I

    .line 349371
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349372
    const-string v0, "HeaderTitleAndSubtitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 349373
    if-ne p0, p1, :cond_1

    .line 349374
    :cond_0
    :goto_0
    return v0

    .line 349375
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 349376
    goto :goto_0

    .line 349377
    :cond_3
    check-cast p1, LX/1xn;

    .line 349378
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 349379
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 349380
    if-eq v2, v3, :cond_0

    .line 349381
    iget-object v2, p0, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 349382
    goto :goto_0

    .line 349383
    :cond_5
    iget-object v2, p1, LX/1xn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 349384
    :cond_6
    iget-object v2, p0, LX/1xn;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1xn;->b:LX/1Pn;

    iget-object v3, p1, LX/1xn;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 349385
    goto :goto_0

    .line 349386
    :cond_8
    iget-object v2, p1, LX/1xn;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 349387
    :cond_9
    iget v2, p0, LX/1xn;->c:I

    iget v3, p1, LX/1xn;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 349388
    goto :goto_0

    .line 349389
    :cond_a
    iget v2, p0, LX/1xn;->d:I

    iget v3, p1, LX/1xn;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 349390
    goto :goto_0

    .line 349391
    :cond_b
    iget-boolean v2, p0, LX/1xn;->e:Z

    iget-boolean v3, p1, LX/1xn;->e:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 349392
    goto :goto_0

    .line 349393
    :cond_c
    iget v2, p0, LX/1xn;->f:I

    iget v3, p1, LX/1xn;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 349394
    goto :goto_0
.end method
