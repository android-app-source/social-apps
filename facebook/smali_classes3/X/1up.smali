.class public final LX/1up;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1um;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/1un;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 341709
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "controller"

    aput-object v2, v0, v1

    sput-object v0, LX/1up;->b:[Ljava/lang/String;

    .line 341710
    sput v3, LX/1up;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 341711
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 341712
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/1up;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1up;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1up;LX/1De;IILX/1un;)V
    .locals 1

    .prologue
    .line 341713
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 341714
    iput-object p4, p0, LX/1up;->a:LX/1un;

    .line 341715
    iget-object v0, p0, LX/1up;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 341716
    return-void
.end method


# virtual methods
.method public final a(LX/1Up;)LX/1up;
    .locals 1

    .prologue
    .line 341717
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->b:LX/1Up;

    .line 341718
    return-object p0
.end method

.method public final a(LX/1aZ;)LX/1up;
    .locals 2

    .prologue
    .line 341719
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->f:LX/1aZ;

    .line 341720
    iget-object v0, p0, LX/1up;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 341721
    return-object p0
.end method

.method public final a(LX/1dc;)LX/1up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341722
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->i:LX/1dc;

    .line 341723
    return-object p0
.end method

.method public final a(LX/1n6;)LX/1up;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341724
    iget-object v0, p0, LX/1up;->a:LX/1un;

    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1un;->i:LX/1dc;

    .line 341725
    return-object p0
.end method

.method public final a(LX/4Ab;)LX/1up;
    .locals 1

    .prologue
    .line 341726
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->g:LX/4Ab;

    .line 341727
    return-object p0
.end method

.method public final a(Landroid/graphics/ColorFilter;)LX/1up;
    .locals 1

    .prologue
    .line 341728
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->h:Landroid/graphics/ColorFilter;

    .line 341729
    return-object p0
.end method

.method public final a(Landroid/graphics/PointF;)LX/1up;
    .locals 1

    .prologue
    .line 341730
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->c:Landroid/graphics/PointF;

    .line 341731
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 341732
    invoke-super {p0}, LX/1X5;->a()V

    .line 341733
    const/4 v0, 0x0

    iput-object v0, p0, LX/1up;->a:LX/1un;

    .line 341734
    sget-object v0, LX/1um;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 341735
    return-void
.end method

.method public final b(LX/1Up;)LX/1up;
    .locals 1

    .prologue
    .line 341705
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->e:LX/1Up;

    .line 341706
    return-object p0
.end method

.method public final b(LX/1dc;)LX/1up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341701
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->j:LX/1dc;

    .line 341702
    return-object p0
.end method

.method public final b(LX/1n6;)LX/1up;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1n6",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341707
    iget-object v0, p0, LX/1up;->a:LX/1un;

    invoke-virtual {p1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1un;->r:LX/1dc;

    .line 341708
    return-object p0
.end method

.method public final b(Landroid/graphics/PointF;)LX/1up;
    .locals 1

    .prologue
    .line 341703
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->d:Landroid/graphics/PointF;

    .line 341704
    return-object p0
.end method

.method public final c(F)LX/1up;
    .locals 1

    .prologue
    .line 341699
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput p1, v0, LX/1un;->a:F

    .line 341700
    return-object p0
.end method

.method public final c(LX/1Up;)LX/1up;
    .locals 1

    .prologue
    .line 341697
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->k:LX/1Up;

    .line 341698
    return-object p0
.end method

.method public final c(LX/1dc;)LX/1up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341695
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->l:LX/1dc;

    .line 341696
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1um;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 341685
    iget-object v1, p0, LX/1up;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1up;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/1up;->c:I

    if-ge v1, v2, :cond_2

    .line 341686
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 341687
    :goto_0
    sget v2, LX/1up;->c:I

    if-ge v0, v2, :cond_1

    .line 341688
    iget-object v2, p0, LX/1up;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 341689
    sget-object v2, LX/1up;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341690
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341691
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341692
    :cond_2
    iget-object v0, p0, LX/1up;->a:LX/1un;

    .line 341693
    invoke-virtual {p0}, LX/1up;->a()V

    .line 341694
    return-object v0
.end method

.method public final d(LX/1Up;)LX/1up;
    .locals 1

    .prologue
    .line 341683
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->m:LX/1Up;

    .line 341684
    return-object p0
.end method

.method public final d(LX/1dc;)LX/1up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341681
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->o:LX/1dc;

    .line 341682
    return-object p0
.end method

.method public final e(LX/1Up;)LX/1up;
    .locals 1

    .prologue
    .line 341679
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->p:LX/1Up;

    .line 341680
    return-object p0
.end method

.method public final e(LX/1dc;)LX/1up;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "LX/1up;"
        }
    .end annotation

    .prologue
    .line 341677
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput-object p1, v0, LX/1un;->r:LX/1dc;

    .line 341678
    return-object p0
.end method

.method public final h(I)LX/1up;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 341675
    iget-object v0, p0, LX/1up;->a:LX/1un;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1un;->i:LX/1dc;

    .line 341676
    return-object p0
.end method

.method public final i(I)LX/1up;
    .locals 1

    .prologue
    .line 341673
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput p1, v0, LX/1un;->n:I

    .line 341674
    return-object p0
.end method

.method public final j(I)LX/1up;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 341671
    iget-object v0, p0, LX/1up;->a:LX/1un;

    invoke-virtual {p0, p1}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, v0, LX/1un;->o:LX/1dc;

    .line 341672
    return-object p0
.end method

.method public final k(I)LX/1up;
    .locals 1

    .prologue
    .line 341669
    iget-object v0, p0, LX/1up;->a:LX/1un;

    iput p1, v0, LX/1un;->q:I

    .line 341670
    return-object p0
.end method
