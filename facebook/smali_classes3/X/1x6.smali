.class public LX/1x6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/1x6;


# instance fields
.field private final b:LX/03V;

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0gu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 347739
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NaRF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/1x6;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1x6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347766
    iput-object p1, p0, LX/1x6;->b:LX/03V;

    .line 347767
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/1x6;->c:Ljava/util/Set;

    .line 347768
    return-void
.end method

.method public static a(LX/0QB;)LX/1x6;
    .locals 4

    .prologue
    .line 347752
    sget-object v0, LX/1x6;->d:LX/1x6;

    if-nez v0, :cond_1

    .line 347753
    const-class v1, LX/1x6;

    monitor-enter v1

    .line 347754
    :try_start_0
    sget-object v0, LX/1x6;->d:LX/1x6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 347755
    if-eqz v2, :cond_0

    .line 347756
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 347757
    new-instance p0, LX/1x6;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/1x6;-><init>(LX/03V;)V

    .line 347758
    move-object v0, p0

    .line 347759
    sput-object v0, LX/1x6;->d:LX/1x6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347760
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 347761
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 347762
    :cond_1
    sget-object v0, LX/1x6;->d:LX/1x6;

    return-object v0

    .line 347763
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 347764
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gu;)V
    .locals 1

    .prologue
    .line 347750
    iget-object v0, p0, LX/1x6;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 347751
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 347742
    :try_start_0
    iget-object v0, p0, LX/1x6;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 347743
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347744
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gu;

    .line 347745
    invoke-interface {v0}, LX/0gu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347746
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 347747
    :catch_0
    move-exception v0

    .line 347748
    iget-object v1, p0, LX/1x6;->b:LX/03V;

    sget-object v2, LX/1x6;->a:Ljava/lang/String;

    const-string v3, "NaRF:Remove SurveyModelReadyListener Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 347749
    :cond_1
    return-void
.end method

.method public final b(LX/0gu;)V
    .locals 1

    .prologue
    .line 347740
    iget-object v0, p0, LX/1x6;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 347741
    return-void
.end method
