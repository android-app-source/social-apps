.class public final LX/1zc;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1sm",
        "<",
        "Ljava/lang/Comparable;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/1zc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 353800
    new-instance v0, LX/1zc;

    invoke-direct {v0}, LX/1zc;-><init>()V

    sput-object v0, LX/1zc;->a:LX/1zc;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 353818
    invoke-direct {p0}, LX/1sm;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353817
    sget-object v0, LX/1zc;->a:LX/1zc;

    return-object v0
.end method


# virtual methods
.method public final a()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Ljava/lang/Comparable;",
            ">()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 353815
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 353816
    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353814
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1}, LX/1sm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353812
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 353813
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1, p2}, LX/1sm;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final a(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353811
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1}, LX/1sm;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353810
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1}, LX/1sm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353808
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 353809
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1, p2}, LX/1sm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final b(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353807
    sget-object v0, LX/1zb;->a:LX/1zb;

    invoke-virtual {v0, p1}, LX/1sm;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 353802
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 353803
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353804
    if-ne p1, p2, :cond_0

    .line 353805
    const/4 v0, 0x0

    .line 353806
    :goto_0
    return v0

    :cond_0
    invoke-interface {p2, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353801
    const-string v0, "Ordering.natural().reverse()"

    return-object v0
.end method
