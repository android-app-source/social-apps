.class public final LX/1yi;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1ye;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1yh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ye",
            "<TE;>.FeedStoryHeaderActionButtonComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1ye;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1ye;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 351109
    iput-object p1, p0, LX/1yi;->b:LX/1ye;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 351110
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1yi;->c:[Ljava/lang/String;

    .line 351111
    iput v3, p0, LX/1yi;->d:I

    .line 351112
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1yi;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1yi;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1yi;LX/1De;IILX/1yh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1ye",
            "<TE;>.FeedStoryHeaderActionButtonComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 351105
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 351106
    iput-object p4, p0, LX/1yi;->a:LX/1yh;

    .line 351107
    iget-object v0, p0, LX/1yi;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 351108
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/1yi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351102
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-object p1, v0, LX/1yh;->b:LX/1Pb;

    .line 351103
    iget-object v0, p0, LX/1yi;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 351104
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1yi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351099
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-object p1, v0, LX/1yh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 351100
    iget-object v0, p0, LX/1yi;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 351101
    return-object p0
.end method

.method public final a(Z)LX/1yi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351097
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-boolean p1, v0, LX/1yh;->c:Z

    .line 351098
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 351093
    invoke-super {p0}, LX/1X5;->a()V

    .line 351094
    const/4 v0, 0x0

    iput-object v0, p0, LX/1yi;->a:LX/1yh;

    .line 351095
    iget-object v0, p0, LX/1yi;->b:LX/1ye;

    iget-object v0, v0, LX/1ye;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 351096
    return-void
.end method

.method public final b(Z)LX/1yi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351091
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-boolean p1, v0, LX/1yh;->d:Z

    .line 351092
    return-object p0
.end method

.method public final c(Z)LX/1yi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351077
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-boolean p1, v0, LX/1yh;->e:Z

    .line 351078
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1ye;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 351081
    iget-object v1, p0, LX/1yi;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1yi;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1yi;->d:I

    if-ge v1, v2, :cond_2

    .line 351082
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 351083
    :goto_0
    iget v2, p0, LX/1yi;->d:I

    if-ge v0, v2, :cond_1

    .line 351084
    iget-object v2, p0, LX/1yi;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 351085
    iget-object v2, p0, LX/1yi;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351086
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351087
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 351088
    :cond_2
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    .line 351089
    invoke-virtual {p0}, LX/1yi;->a()V

    .line 351090
    return-object v0
.end method

.method public final d(Z)LX/1yi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1ye",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 351079
    iget-object v0, p0, LX/1yi;->a:LX/1yh;

    iput-boolean p1, v0, LX/1yh;->f:Z

    .line 351080
    return-object p0
.end method
