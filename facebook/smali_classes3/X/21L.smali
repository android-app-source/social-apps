.class public final LX/21L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/21A;

.field public final synthetic d:LX/21H;

.field public final synthetic e:LX/20g;


# direct methods
.method public constructor <init>(LX/20g;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;LX/21A;LX/21H;)V
    .locals 0

    .prologue
    .line 357066
    iput-object p1, p0, LX/21L;->e:LX/20g;

    iput-object p2, p0, LX/21L;->a:Ljava/lang/String;

    iput-object p3, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/21L;->c:LX/21A;

    iput-object p5, p0, LX/21L;->d:LX/21H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 357067
    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 357068
    :cond_0
    :goto_0
    return-void

    .line 357069
    :cond_1
    const/4 v0, 0x0

    .line 357070
    if-eqz p1, :cond_2

    .line 357071
    iget v1, p2, LX/1zt;->e:I

    move v1, v1

    .line 357072
    if-ne v1, v5, :cond_2

    .line 357073
    iget-object v0, p0, LX/21L;->e:LX/20g;

    const v1, 0x7f0d0c3f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, LX/21L;->a:Ljava/lang/String;

    iget-object v3, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v1, p2, v2, v3}, LX/20g;->a(LX/20g;Landroid/view/View;LX/1zt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    .line 357074
    :cond_2
    iget-object v1, p0, LX/21L;->e:LX/20g;

    iget-object v1, v1, LX/20g;->b:LX/1EQ;

    iget-object v2, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/21L;->c:LX/21A;

    invoke-virtual {v1, v2, v3}, LX/1EQ;->a(Lcom/facebook/graphql/model/FeedUnit;LX/21A;)V

    .line 357075
    iget-object v1, p0, LX/21L;->e:LX/20g;

    iget-object v1, v1, LX/20g;->l:LX/20l;

    invoke-virtual {v1}, LX/20l;->b()V

    .line 357076
    iget-object v1, p0, LX/21L;->e:LX/20g;

    iget-object v1, v1, LX/20g;->d:LX/20h;

    iget-object v2, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, p0, LX/21L;->c:LX/21A;

    invoke-virtual {v3}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v3

    invoke-virtual {v1, v2, p2, v3, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    .line 357077
    iget-object v1, p0, LX/21L;->e:LX/20g;

    iget-object v1, v1, LX/20g;->e:LX/0bH;

    new-instance v2, LX/1Zf;

    iget-object v3, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/21L;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, p2, v0}, LX/1Zf;-><init>(Ljava/lang/String;Ljava/lang/String;LX/1zt;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 357078
    iget-object v0, p0, LX/21L;->d:LX/21H;

    if-eqz v0, :cond_3

    .line 357079
    iget-object v0, p0, LX/21L;->d:LX/21H;

    invoke-virtual {v0, p2}, LX/21H;->b(LX/1zt;)V

    .line 357080
    :cond_3
    if-eqz p1, :cond_4

    .line 357081
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 357082
    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    .line 357083
    iget-object v0, p0, LX/21L;->e:LX/20g;

    iget-object v0, v0, LX/20g;->r:LX/20r;

    invoke-virtual {v0, p1}, LX/20r;->a(Landroid/view/View;)V

    .line 357084
    :cond_4
    if-eqz p1, :cond_0

    .line 357085
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 357086
    if-ne v0, v5, :cond_0

    .line 357087
    iget-object v0, p0, LX/21L;->e:LX/20g;

    iget-object v0, v0, LX/20g;->s:LX/20s;

    invoke-virtual {v0, p1}, LX/20s;->a(Landroid/view/View;)V

    goto :goto_0
.end method
