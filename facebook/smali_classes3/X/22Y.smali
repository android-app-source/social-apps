.class public LX/22Y;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/22a;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/22b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 359916
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/22Y;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/22b;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359913
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 359914
    iput-object p1, p0, LX/22Y;->b:LX/0Ot;

    .line 359915
    return-void
.end method

.method public static a(LX/0QB;)LX/22Y;
    .locals 4

    .prologue
    .line 359882
    const-class v1, LX/22Y;

    monitor-enter v1

    .line 359883
    :try_start_0
    sget-object v0, LX/22Y;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359884
    sput-object v2, LX/22Y;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359887
    new-instance v3, LX/22Y;

    const/16 p0, 0x82c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/22Y;-><init>(LX/0Ot;)V

    .line 359888
    move-object v0, v3

    .line 359889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 359903
    check-cast p2, LX/22Z;

    .line 359904
    iget-object v0, p0, LX/22Y;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/22b;

    iget-object v1, p2, LX/22Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/22Z;->b:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-object v3, p2, LX/22Z;->c:LX/1dQ;

    const/4 v4, 0x0

    .line 359905
    iget-object p0, v0, LX/22b;->a:LX/1eq;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1eq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 359906
    :cond_0
    :goto_0
    move-object v0, v4

    .line 359907
    return-object v0

    .line 359908
    :cond_1
    iget-object p0, v0, LX/22b;->a:LX/1eq;

    invoke-virtual {p0, v1, v2}, LX/1eq;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result p2

    .line 359909
    if-eqz p2, :cond_0

    .line 359910
    const/4 v4, 0x1

    if-ne p2, v4, :cond_2

    iget-object v4, v0, LX/22b;->a:LX/1eq;

    invoke-virtual {v4, v2}, LX/1eq;->b(Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v4

    move p0, v4

    .line 359911
    :goto_1
    iget-object v4, v0, LX/22b;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8wz;

    invoke-virtual {v4, p1}, LX/8wz;->c(LX/1De;)LX/8wx;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/8wx;->h(I)LX/8wx;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/8wx;->i(I)LX/8wx;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/8wx;->a(LX/1dQ;)LX/8wx;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->b()LX/1Dg;

    move-result-object v4

    goto :goto_0

    .line 359912
    :cond_2
    const/4 v4, 0x0

    move p0, v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359901
    invoke-static {}, LX/1dS;->b()V

    .line 359902
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/22a;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 359893
    new-instance v1, LX/22Z;

    invoke-direct {v1, p0}, LX/22Z;-><init>(LX/22Y;)V

    .line 359894
    sget-object v2, LX/22Y;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/22a;

    .line 359895
    if-nez v2, :cond_0

    .line 359896
    new-instance v2, LX/22a;

    invoke-direct {v2}, LX/22a;-><init>()V

    .line 359897
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/22a;->a$redex0(LX/22a;LX/1De;IILX/22Z;)V

    .line 359898
    move-object v1, v2

    .line 359899
    move-object v0, v1

    .line 359900
    return-object v0
.end method
