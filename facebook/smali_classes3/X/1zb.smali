.class public final LX/1zb;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1sm",
        "<",
        "Ljava/lang/Comparable;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final a:LX/1zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 353791
    new-instance v0, LX/1zb;

    invoke-direct {v0}, LX/1zb;-><init>()V

    sput-object v0, LX/1zb;->a:LX/1zb;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 353799
    invoke-direct {p0}, LX/1sm;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353797
    sget-object v0, LX/1zb;->a:LX/1zb;

    return-object v0
.end method


# virtual methods
.method public final a()LX/1sm;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S::",
            "Ljava/lang/Comparable;",
            ">()",
            "LX/1sm",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 353798
    sget-object v0, LX/1zc;->a:LX/1zc;

    return-object v0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 353793
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    .line 353794
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353795
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353796
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353792
    const-string v0, "Ordering.natural()"

    return-object v0
.end method
