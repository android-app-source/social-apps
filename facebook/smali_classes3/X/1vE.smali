.class public LX/1vE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 342102
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "saved/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 342103
    sput-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "has_seen_saved_dashboard_interstitial"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->b:LX/0Tn;

    .line 342104
    sget-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "has_ever_saved"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->c:LX/0Tn;

    .line 342105
    sget-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "dashboard_loaded_since_login"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->d:LX/0Tn;

    .line 342106
    sget-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "has_pending_saved_bookmark_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->e:LX/0Tn;

    .line 342107
    sget-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "has_pending_saved_offline_bookmark_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->f:LX/0Tn;

    .line 342108
    sget-object v0, LX/1vE;->a:LX/0Tn;

    const-string v1, "has_pending_saved_offline_finished_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1vE;->g:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342110
    return-void
.end method


# virtual methods
.method public final b()LX/0Rf;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342111
    sget-object v0, LX/1vE;->b:LX/0Tn;

    sget-object v1, LX/1vE;->c:LX/0Tn;

    sget-object v2, LX/1vE;->d:LX/0Tn;

    sget-object v3, LX/1vE;->e:LX/0Tn;

    sget-object v4, LX/1vE;->f:LX/0Tn;

    sget-object v5, LX/1vE;->g:LX/0Tn;

    const/4 v6, 0x0

    new-array v6, v6, [LX/0Tn;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
