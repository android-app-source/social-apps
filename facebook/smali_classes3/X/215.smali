.class public LX/215;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:Z

.field public e:LX/0wT;

.field public final f:LX/216;

.field public g:LX/0wd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/20T;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:[I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/Ams;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/216;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356639
    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, LX/215;->a:F

    .line 356640
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, LX/215;->b:F

    .line 356641
    const v0, 0x3fb33333    # 1.4f

    iput v0, p0, LX/215;->c:F

    .line 356642
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/215;->d:Z

    .line 356643
    iput-object p1, p0, LX/215;->f:LX/216;

    .line 356644
    return-void
.end method

.method public static b(LX/0QB;)LX/215;
    .locals 2

    .prologue
    .line 356645
    new-instance v1, LX/215;

    invoke-static {p0}, LX/216;->a(LX/0QB;)LX/216;

    move-result-object v0

    check-cast v0, LX/216;

    invoke-direct {v1, v0}, LX/215;-><init>(LX/216;)V

    .line 356646
    return-object v1
.end method

.method public static c(LX/215;)LX/20T;
    .locals 1

    .prologue
    .line 356633
    iget-object v0, p0, LX/215;->h:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 356634
    const/4 v0, 0x0

    .line 356635
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/215;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20T;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 356636
    const/4 v0, 0x0

    iput-object v0, p0, LX/215;->h:Ljava/lang/ref/WeakReference;

    .line 356637
    return-void
.end method

.method public final a(LX/0wT;)V
    .locals 1

    .prologue
    .line 356629
    iput-object p1, p0, LX/215;->e:LX/0wT;

    .line 356630
    iget-object v0, p0, LX/215;->g:LX/0wd;

    if-eqz v0, :cond_0

    .line 356631
    iget-object v0, p0, LX/215;->g:LX/0wd;

    invoke-virtual {v0, p1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 356632
    :cond_0
    return-void
.end method

.method public final a(LX/20T;)V
    .locals 1

    .prologue
    .line 356627
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/215;->h:Ljava/lang/ref/WeakReference;

    .line 356628
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    .line 356599
    invoke-static {p0}, LX/215;->c(LX/215;)LX/20T;

    move-result-object v0

    .line 356600
    if-nez v0, :cond_1

    .line 356601
    :cond_0
    :goto_0
    return v3

    .line 356602
    :cond_1
    iget-object v1, p0, LX/215;->i:[I

    if-eqz v1, :cond_2

    .line 356603
    :goto_1
    iget-object v1, p0, LX/215;->g:LX/0wd;

    const/4 v8, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 356604
    const/4 v6, 0x1

    .line 356605
    iget-boolean v7, p0, LX/215;->d:Z

    if-eqz v7, :cond_6

    .line 356606
    :goto_2
    move v6, v6

    .line 356607
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 356608
    :goto_3
    move v1, v6

    .line 356609
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_0

    if-eqz v1, :cond_0

    .line 356610
    invoke-interface {v0}, LX/20T;->performClick()Z

    goto :goto_0

    .line 356611
    :cond_2
    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, LX/215;->i:[I

    .line 356612
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, LX/215;->j:Landroid/graphics/Rect;

    .line 356613
    iget-object v1, p0, LX/215;->f:LX/216;

    invoke-virtual {v1}, LX/216;->a()LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/215;->g:LX/0wd;

    .line 356614
    iget-object v1, p0, LX/215;->e:LX/0wT;

    if-eqz v1, :cond_3

    .line 356615
    iget-object v1, p0, LX/215;->g:LX/0wd;

    iget-object v2, p0, LX/215;->e:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 356616
    :cond_3
    iget-object v1, p0, LX/215;->g:LX/0wd;

    new-instance v2, LX/3na;

    invoke-direct {v2, p0}, LX/3na;-><init>(LX/215;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    goto :goto_1

    .line 356617
    :pswitch_0
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    .line 356618
    if-eqz v6, :cond_4

    iget v4, p0, LX/215;->b:F

    float-to-double v4, v4

    :cond_4
    invoke-virtual {v1, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_3

    .line 356619
    :pswitch_1
    invoke-virtual {p1, v8}, Landroid/view/View;->setPressed(Z)V

    .line 356620
    if-eqz v6, :cond_5

    iget v4, p0, LX/215;->c:F

    float-to-double v4, v4

    :cond_5
    invoke-virtual {v1, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_3

    .line 356621
    :pswitch_2
    invoke-virtual {p1, v8}, Landroid/view/View;->setPressed(Z)V

    .line 356622
    invoke-virtual {v1, v4, v5}, LX/0wd;->b(D)LX/0wd;

    goto :goto_3

    .line 356623
    :cond_6
    iget-object v7, p0, LX/215;->j:Landroid/graphics/Rect;

    invoke-virtual {p1, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 356624
    iget-object v7, p0, LX/215;->i:[I

    invoke-virtual {p1, v7}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 356625
    iget-object v7, p0, LX/215;->j:Landroid/graphics/Rect;

    iget-object v9, p0, LX/215;->i:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    iget-object v10, p0, LX/215;->i:[I

    aget v6, v10, v6

    invoke-virtual {v7, v9, v6}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 356626
    iget-object v6, p0, LX/215;->j:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v6, v7, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
