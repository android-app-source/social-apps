.class public final LX/1uZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16X;


# instance fields
.field public final synthetic a:LX/1uY;


# direct methods
.method public constructor <init>(LX/1uY;)V
    .locals 0

    .prologue
    .line 341286
    iput-object p1, p0, LX/1uZ;->a:LX/1uY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ub;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 341287
    iget-object v0, p1, LX/1ub;->a:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v0, v1, :cond_1

    .line 341288
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->m:LX/1BF;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1BF;->a(Ljava/lang/String;)V

    .line 341289
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->i:LX/0qX;

    invoke-virtual {v0, v2}, LX/0qX;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341290
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->k:Ljava/util/Map;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 341291
    if-eqz v0, :cond_0

    .line 341292
    iget-object v1, p0, LX/1uZ;->a:LX/1uY;

    iget-object v1, v1, LX/1uY;->m:LX/1BF;

    .line 341293
    iget-object v3, v1, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1fI;

    .line 341294
    const/4 v1, 0x0

    .line 341295
    invoke-static {v3, v0, v1, v1}, LX/1fI;->a(LX/1fI;Lcom/facebook/graphql/model/FeedUnit;II)V

    .line 341296
    goto :goto_0

    .line 341297
    :cond_0
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->k:Ljava/util/Map;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 341298
    :cond_1
    iget-object v0, p1, LX/1ub;->a:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v0, v1, :cond_2

    .line 341299
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->h:LX/0ad;

    sget-short v1, LX/0ws;->fI:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->j:LX/0pJ;

    invoke-virtual {v0, v2}, LX/0pJ;->f(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341300
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/1uY;->a$redex0(LX/1uY;Ljava/lang/String;)V

    .line 341301
    :cond_2
    :goto_1
    return-void

    .line 341302
    :cond_3
    iget-object v0, p0, LX/1uZ;->a:LX/1uY;

    iget-object v0, v0, LX/1uY;->m:LX/1BF;

    iget-object v1, p1, LX/1ub;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1BF;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
