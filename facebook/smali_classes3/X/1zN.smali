.class public LX/1zN;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/1zP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353176
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 353177
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1zP;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1zP;"
        }
    .end annotation

    .prologue
    .line 353178
    new-instance v0, LX/1zP;

    const-class v1, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v5

    check-cast v5, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v6

    check-cast v6, LX/0id;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v7

    check-cast v7, LX/17V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    const/16 v2, 0x97

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v10

    check-cast v10, LX/1EQ;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v12

    check-cast v12, LX/0bH;

    move-object v2, p1

    invoke-direct/range {v0 .. v12}, LX/1zP;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/performancelogger/PerformanceLogger;LX/0id;LX/17V;LX/0Zb;LX/0Ot;LX/1EQ;LX/0Uh;LX/0bH;)V

    .line 353179
    return-object v0
.end method
