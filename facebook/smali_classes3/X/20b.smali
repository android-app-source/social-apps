.class public LX/20b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final a:LX/20c;

.field public final b:LX/20E;

.field private final c:Landroid/graphics/RectF;

.field public d:Landroid/view/MotionEvent;

.field public e:Landroid/view/View;

.field public f:I


# direct methods
.method public constructor <init>(LX/0tH;LX/20E;)V
    .locals 2
    .param p2    # LX/20E;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354987
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/20b;->c:Landroid/graphics/RectF;

    .line 354988
    const/16 v0, 0xfa

    iput v0, p0, LX/20b;->f:I

    .line 354989
    iput-object p2, p0, LX/20b;->b:LX/20E;

    .line 354990
    new-instance v0, LX/20c;

    invoke-direct {v0, p0}, LX/20c;-><init>(LX/20b;)V

    iput-object v0, p0, LX/20b;->a:LX/20c;

    .line 354991
    iget-object v0, p1, LX/0tH;->b:LX/0ad;

    sget v1, LX/1sg;->e:I

    const/16 p2, 0xfa

    invoke-interface {v0, v1, p2}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 354992
    iput v0, p0, LX/20b;->f:I

    .line 354993
    return-void
.end method

.method public static a(LX/20b;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 354994
    iget-object v0, p0, LX/20b;->d:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 354995
    iget-object v0, p0, LX/20b;->d:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 354996
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/20b;->d:Landroid/view/MotionEvent;

    .line 354997
    return-void

    .line 354998
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 354999
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 355000
    :goto_0
    return v7

    .line 355001
    :pswitch_0
    invoke-static {p0, p2}, LX/20b;->a(LX/20b;Landroid/view/MotionEvent;)V

    .line 355002
    iput-object p1, p0, LX/20b;->e:Landroid/view/View;

    .line 355003
    iget-object v0, p0, LX/20b;->a:LX/20c;

    invoke-virtual {v0, v6}, LX/20c;->removeMessages(I)V

    .line 355004
    iget-object v0, p0, LX/20b;->a:LX/20c;

    .line 355005
    iget v1, p0, LX/20b;->f:I

    const/16 v2, 0xc8

    if-lt v1, v2, :cond_0

    iget v1, p0, LX/20b;->f:I

    const/16 v2, 0x2bc

    if-le v1, v2, :cond_1

    .line 355006
    :cond_0
    const/16 v1, 0xfa

    .line 355007
    :goto_1
    move v1, v1

    .line 355008
    int-to-long v2, v1

    invoke-virtual {v0, v6, v2, v3}, LX/20c;->sendEmptyMessageDelayed(IJ)Z

    .line 355009
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 355010
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 355011
    iget-object v1, p0, LX/20b;->c:Landroid/graphics/RectF;

    aget v2, v0, v7

    int-to-float v2, v2

    aget v3, v0, v6

    int-to-float v3, v3

    aget v4, v0, v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    aget v0, v0, v6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v0, v5

    int-to-float v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 355012
    :pswitch_1
    invoke-static {p0, p2}, LX/20b;->a(LX/20b;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 355013
    :pswitch_2
    iget-object v0, p0, LX/20b;->a:LX/20c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/20c;->removeMessages(I)V

    .line 355014
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/20b;->a(LX/20b;Landroid/view/MotionEvent;)V

    .line 355015
    goto :goto_0

    :cond_1
    iget v1, p0, LX/20b;->f:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
