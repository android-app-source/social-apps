.class public LX/20q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:LX/20q;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Integer;

.field private c:Z

.field private d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356211
    iput-object p1, p0, LX/20q;->a:LX/0ad;

    .line 356212
    return-void
.end method

.method public static a(LX/0QB;)LX/20q;
    .locals 4

    .prologue
    .line 356197
    sget-object v0, LX/20q;->m:LX/20q;

    if-nez v0, :cond_1

    .line 356198
    const-class v1, LX/20q;

    monitor-enter v1

    .line 356199
    :try_start_0
    sget-object v0, LX/20q;->m:LX/20q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 356200
    if-eqz v2, :cond_0

    .line 356201
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 356202
    new-instance p0, LX/20q;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/20q;-><init>(LX/0ad;)V

    .line 356203
    move-object v0, p0

    .line 356204
    sput-object v0, LX/20q;->m:LX/20q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356205
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 356206
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 356207
    :cond_1
    sget-object v0, LX/20q;->m:LX/20q;

    return-object v0

    .line 356208
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 356209
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 356192
    iget-boolean v0, p0, LX/20q;->d:Z

    if-nez v0, :cond_0

    .line 356193
    iget-object v0, p0, LX/20q;->a:LX/0ad;

    sget-short v1, LX/0wn;->B:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/20q;->c:Z

    .line 356194
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/20q;->d:Z

    .line 356195
    iget-boolean v0, p0, LX/20q;->c:Z

    .line 356196
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, LX/20q;->c:Z

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 356177
    invoke-virtual {p0}, LX/20q;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356178
    :goto_0
    return-object v0

    .line 356179
    :cond_0
    iget-object v1, p0, LX/20q;->f:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 356180
    iget-object v1, p0, LX/20q;->a:LX/0ad;

    sget-char v2, LX/0wn;->J:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/20q;->f:Ljava/lang/String;

    .line 356181
    :cond_1
    iget-object v0, p0, LX/20q;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 356187
    invoke-virtual {p0}, LX/20q;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356188
    :goto_0
    return-object v0

    .line 356189
    :cond_0
    iget-object v1, p0, LX/20q;->h:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 356190
    iget-object v1, p0, LX/20q;->a:LX/0ad;

    sget-char v2, LX/0wn;->H:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/20q;->h:Ljava/lang/String;

    .line 356191
    :cond_1
    iget-object v0, p0, LX/20q;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 356182
    invoke-virtual {p0}, LX/20q;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356183
    :goto_0
    return-object v0

    .line 356184
    :cond_0
    iget-object v1, p0, LX/20q;->i:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 356185
    iget-object v1, p0, LX/20q;->a:LX/0ad;

    sget-char v2, LX/0wn;->I:C

    invoke-interface {v1, v2, v0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/20q;->i:Ljava/lang/String;

    .line 356186
    :cond_1
    iget-object v0, p0, LX/20q;->i:Ljava/lang/String;

    goto :goto_0
.end method
