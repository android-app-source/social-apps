.class public LX/1wT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13G;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/1wT;


# instance fields
.field public final a:LX/1wU;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Uh;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/1wU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;Ljava/lang/Boolean;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1wU;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 345929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345930
    iput-object p1, p0, LX/1wT;->a:LX/1wU;

    .line 345931
    iput-object p2, p0, LX/1wT;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 345932
    iput-object p3, p0, LX/1wT;->c:LX/0Uh;

    .line 345933
    iput-object p4, p0, LX/1wT;->d:LX/0Or;

    .line 345934
    iput-object p5, p0, LX/1wT;->e:Ljava/lang/Boolean;

    .line 345935
    return-void
.end method

.method public static a(LX/0QB;)LX/1wT;
    .locals 9

    .prologue
    .line 345916
    sget-object v0, LX/1wT;->f:LX/1wT;

    if-nez v0, :cond_1

    .line 345917
    const-class v1, LX/1wT;

    monitor-enter v1

    .line 345918
    :try_start_0
    sget-object v0, LX/1wT;->f:LX/1wT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 345919
    if-eqz v2, :cond_0

    .line 345920
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 345921
    new-instance v3, LX/1wT;

    invoke-static {v0}, LX/1wU;->b(LX/0QB;)LX/1wU;

    move-result-object v4

    check-cast v4, LX/1wU;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x15e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v8}, LX/1wT;-><init>(LX/1wU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/0Or;Ljava/lang/Boolean;)V

    .line 345922
    move-object v0, v3

    .line 345923
    sput-object v0, LX/1wT;->f:LX/1wT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345924
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 345925
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345926
    :cond_1
    sget-object v0, LX/1wT;->f:LX/1wT;

    return-object v0

    .line 345927
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 345928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 345915
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345914
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 345936
    iget-object v0, p0, LX/1wT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 345937
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/1wT;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wT;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1nR;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 345938
    iget-object v0, p0, LX/1wT;->a:LX/1wU;

    invoke-virtual {v0}, LX/1wU;->b()LX/03R;

    move-result-object v0

    .line 345939
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 345940
    if-nez v0, :cond_0

    .line 345941
    iget-object v0, p0, LX/1wT;->c:LX/0Uh;

    const/16 v1, 0x7e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 345942
    if-nez v0, :cond_1

    .line 345943
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 345944
    :goto_1
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 345913
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/growth/nux/CILegalNuxActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 345912
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 345911
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345910
    const-string v0, "4545"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345909
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
