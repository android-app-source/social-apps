.class public LX/207;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 354365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;
    .locals 2

    .prologue
    .line 354366
    if-nez p0, :cond_0

    .line 354367
    const/4 v0, 0x0

    .line 354368
    :goto_0
    return-object v0

    .line 354369
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->af()Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;

    move-result-object v0

    .line 354370
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->d()Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    move-result-object v1

    if-nez v1, :cond_2

    .line 354371
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->O()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v0

    goto :goto_0

    .line 354372
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback$FeedbackExtra;->d()Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;

    move-result-object v0

    .line 354373
    iget-object v1, v0, Lcom/facebook/graphql/model/ConsistentFeedbackTopReactionsConnection;->a:Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-object v0, v1

    .line 354374
    goto :goto_0
.end method
