.class public final LX/21t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 358496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)LX/21u;
    .locals 6

    .prologue
    .line 358497
    new-instance v0, LX/21u;

    invoke-direct {v0}, LX/21u;-><init>()V

    .line 358498
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 358499
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 358500
    const/4 v0, 0x0

    .line 358501
    :cond_0
    return-object v0

    .line 358502
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 358503
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 358504
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 358505
    const/4 v2, 0x0

    .line 358506
    const-string v4, "name"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 358507
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_2

    :goto_1
    iput-object v2, v0, LX/21u;->a:Ljava/lang/String;

    .line 358508
    :goto_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 358509
    :cond_2
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 358510
    :cond_3
    const-string v4, "value"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 358511
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_4

    :goto_3
    iput-object v2, v0, LX/21u;->b:Ljava/lang/String;

    .line 358512
    goto :goto_2

    .line 358513
    :cond_4
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 358514
    :cond_5
    goto :goto_2
.end method
