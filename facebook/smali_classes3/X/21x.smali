.class public final enum LX/21x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21x;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21x;

.field public static final enum BASE:LX/21x;

.field public static final enum COMPLETE:LX/21x;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358598
    new-instance v0, LX/21x;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v2}, LX/21x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21x;->COMPLETE:LX/21x;

    .line 358599
    new-instance v0, LX/21x;

    const-string v1, "BASE"

    invoke-direct {v0, v1, v3}, LX/21x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/21x;->BASE:LX/21x;

    .line 358600
    const/4 v0, 0x2

    new-array v0, v0, [LX/21x;

    sget-object v1, LX/21x;->COMPLETE:LX/21x;

    aput-object v1, v0, v2

    sget-object v1, LX/21x;->BASE:LX/21x;

    aput-object v1, v0, v3

    sput-object v0, LX/21x;->$VALUES:[LX/21x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 358601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/21x;
    .locals 1

    .prologue
    .line 358602
    const-class v0, LX/21x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21x;

    return-object v0
.end method

.method public static values()[LX/21x;
    .locals 1

    .prologue
    .line 358603
    sget-object v0, LX/21x;->$VALUES:[LX/21x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21x;

    return-object v0
.end method
