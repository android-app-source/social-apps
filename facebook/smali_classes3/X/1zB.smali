.class public LX/1zB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1V8;

.field private final b:LX/1DR;

.field private final c:F


# direct methods
.method public constructor <init>(LX/1V8;LX/1DR;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351821
    iput-object p1, p0, LX/1zB;->a:LX/1V8;

    .line 351822
    iput-object p2, p0, LX/1zB;->b:LX/1DR;

    .line 351823
    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, LX/1zB;->c:F

    .line 351824
    return-void
.end method

.method public static a(Landroid/content/Context;LX/1W9;)LX/1nq;
    .locals 3

    .prologue
    .line 351825
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    .line 351826
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->b(I)LX/1nq;

    .line 351827
    const v1, 0x7f01029a

    const v2, 0x7f0a0158

    invoke-static {p0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v1

    .line 351828
    invoke-virtual {v0, v1}, LX/1nq;->c(I)LX/1nq;

    .line 351829
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->a(F)LX/1nq;

    .line 351830
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/1nq;->b(F)LX/1nq;

    .line 351831
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1nq;->a(Z)LX/1nq;

    .line 351832
    const/4 v1, 0x1

    .line 351833
    iput-boolean v1, v0, LX/1nq;->f:Z

    .line 351834
    iput-object p1, v0, LX/1nq;->d:LX/1WA;

    .line 351835
    return-object v0
.end method

.method public static a(LX/0QB;)LX/1zB;
    .locals 6

    .prologue
    .line 351836
    const-class v1, LX/1zB;

    monitor-enter v1

    .line 351837
    :try_start_0
    sget-object v0, LX/1zB;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351838
    sput-object v2, LX/1zB;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351839
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351840
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351841
    new-instance p0, LX/1zB;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v3

    check-cast v3, LX/1V8;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/1zB;-><init>(LX/1V8;LX/1DR;Landroid/content/res/Resources;)V

    .line 351842
    move-object v0, p0

    .line 351843
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351844
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1zB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351845
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 4
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 351847
    iget-object v0, p0, LX/1zB;->b:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 351848
    iget-object v1, p0, LX/1zB;->a:LX/1V8;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    iget v3, p0, LX/1zB;->c:F

    invoke-virtual {v1, v2, p1, v3}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v1

    .line 351849
    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nq;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351850
    invoke-virtual {p0, p2}, LX/1zB;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    invoke-virtual {p1, v0}, LX/1nq;->a(I)LX/1nq;

    .line 351851
    return-void
.end method
