.class public LX/1uY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1uY;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile n:LX/1uY;


# instance fields
.field private final b:LX/16U;

.field private final c:LX/19l;

.field private final d:LX/19w;

.field private final e:LX/1uZ;

.field private final f:LX/1sy;

.field private final g:LX/1ua;

.field public final h:LX/0ad;

.field public final i:LX/0qX;

.field public final j:LX/0pJ;

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/19s;

.field public final m:LX/1BF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 341285
    const-class v0, LX/1uY;

    sput-object v0, LX/1uY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/16U;LX/19l;LX/19w;LX/0qX;LX/0pJ;LX/19s;LX/0ad;LX/1BF;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 341254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341255
    new-instance v0, LX/1uZ;

    invoke-direct {v0, p0}, LX/1uZ;-><init>(LX/1uY;)V

    iput-object v0, p0, LX/1uY;->e:LX/1uZ;

    .line 341256
    new-instance v0, LX/1sy;

    invoke-direct {v0, p0}, LX/1sy;-><init>(LX/1uY;)V

    iput-object v0, p0, LX/1uY;->f:LX/1sy;

    .line 341257
    new-instance v0, LX/1ua;

    invoke-direct {v0, p0}, LX/1ua;-><init>(LX/1uY;)V

    iput-object v0, p0, LX/1uY;->g:LX/1ua;

    .line 341258
    iput-object p1, p0, LX/1uY;->b:LX/16U;

    .line 341259
    iput-object p2, p0, LX/1uY;->c:LX/19l;

    .line 341260
    iput-object p3, p0, LX/1uY;->d:LX/19w;

    .line 341261
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1uY;->k:Ljava/util/Map;

    .line 341262
    iput-object p4, p0, LX/1uY;->i:LX/0qX;

    .line 341263
    iput-object p5, p0, LX/1uY;->j:LX/0pJ;

    .line 341264
    iput-object p6, p0, LX/1uY;->l:LX/19s;

    .line 341265
    iput-object p7, p0, LX/1uY;->h:LX/0ad;

    .line 341266
    iget-object v0, p0, LX/1uY;->b:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/1uY;->e:LX/1uZ;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 341267
    iput-object p8, p0, LX/1uY;->m:LX/1BF;

    .line 341268
    iget-object v0, p0, LX/1uY;->h:LX/0ad;

    sget-short v1, LX/0ws;->fI:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1uY;->j:LX/0pJ;

    invoke-virtual {v0, v3}, LX/0pJ;->f(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341269
    iget-object v0, p0, LX/1uY;->c:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/7KV;

    iget-object v2, p0, LX/1uY;->f:LX/1sy;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 341270
    iget-object v0, p0, LX/1uY;->c:LX/19l;

    iget-object v0, v0, LX/19l;->a:LX/16V;

    const-class v1, LX/7KU;

    iget-object v2, p0, LX/1uY;->g:LX/1ua;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 341271
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/1uY;
    .locals 12

    .prologue
    .line 341272
    sget-object v0, LX/1uY;->n:LX/1uY;

    if-nez v0, :cond_1

    .line 341273
    const-class v1, LX/1uY;

    monitor-enter v1

    .line 341274
    :try_start_0
    sget-object v0, LX/1uY;->n:LX/1uY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 341275
    if-eqz v2, :cond_0

    .line 341276
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 341277
    new-instance v3, LX/1uY;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v4

    check-cast v4, LX/16U;

    invoke-static {v0}, LX/19l;->a(LX/0QB;)LX/19l;

    move-result-object v5

    check-cast v5, LX/19l;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v6

    check-cast v6, LX/19w;

    invoke-static {v0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v7

    check-cast v7, LX/0qX;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    invoke-static {v0}, LX/19i;->a(LX/0QB;)LX/19s;

    move-result-object v9

    check-cast v9, LX/19s;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/1BF;->b(LX/0QB;)LX/1BF;

    move-result-object v11

    check-cast v11, LX/1BF;

    invoke-direct/range {v3 .. v11}, LX/1uY;-><init>(LX/16U;LX/19l;LX/19w;LX/0qX;LX/0pJ;LX/19s;LX/0ad;LX/1BF;)V

    .line 341278
    move-object v0, v3

    .line 341279
    sput-object v0, LX/1uY;->n:LX/1uY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341280
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 341281
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 341282
    :cond_1
    sget-object v0, LX/1uY;->n:LX/1uY;

    return-object v0

    .line 341283
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 341284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1uY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 341242
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 341243
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341244
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 341245
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 341246
    if-eqz v0, :cond_0

    .line 341247
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341248
    const/4 v1, 0x0

    .line 341249
    invoke-static {v0}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 341250
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    .line 341251
    :goto_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 341252
    invoke-direct {p0, p1, v0}, LX/1uY;->a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    goto :goto_0

    .line 341253
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private declared-synchronized a(Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 341230
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1uY;->i:LX/0qX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0qX;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341231
    iget-object v0, p0, LX/1uY;->k:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341232
    :cond_0
    iget-object v0, p0, LX/1uY;->m:LX/1BF;

    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    .line 341233
    iget-object v2, v0, LX/1BF;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1fI;

    .line 341234
    const-string v0, "VIDEO"

    invoke-static {v2, v1, p2, v0}, LX/1fI;->b(LX/1fI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341235
    goto :goto_0

    .line 341236
    :cond_1
    iget-object v0, p0, LX/1uY;->d:LX/19w;

    invoke-virtual {v0, p2}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341237
    iget-object v0, p0, LX/1uY;->m:LX/1BF;

    invoke-virtual {v0, p2}, LX/1BF;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341238
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 341239
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1uY;->h:LX/0ad;

    sget-short v1, LX/0ws;->fI:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/1uY;->j:LX/0pJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0pJ;->f(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341240
    invoke-static {p0, p2}, LX/1uY;->a$redex0(LX/1uY;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 341241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/1uY;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 341220
    iget-object v0, p0, LX/1uY;->d:LX/19w;

    invoke-virtual {v0, p1}, LX/19w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341221
    iget-object v0, p0, LX/1uY;->m:LX/1BF;

    invoke-virtual {v0, p1}, LX/1BF;->a(Ljava/lang/String;)V

    .line 341222
    :goto_0
    return-void

    .line 341223
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/1uY;->j:LX/0pJ;

    const/4 v1, 0x0

    .line 341224
    sget-wide v3, LX/0X5;->dt:J

    const/4 v5, 0x6

    invoke-virtual {v0, v3, v4, v5, v1}, LX/0pK;->a(JIZ)Z

    move-result v3

    move v0, v3

    .line 341225
    iget-object v1, p0, LX/1uY;->l:LX/19s;

    invoke-virtual {v1}, LX/19s;->b()LX/04j;

    move-result-object v1

    invoke-interface {v1, p1}, LX/04j;->a(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 341226
    iget-object v0, p0, LX/1uY;->l:LX/19s;

    invoke-virtual {v0}, LX/19s;->b()LX/04j;

    move-result-object v0

    invoke-interface {v0, p1}, LX/04j;->a(Ljava/lang/String;)Lcom/facebook/exoplayer/ipc/VideoCacheStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/exoplayer/ipc/VideoCacheStatus;->a:Z

    .line 341227
    :cond_1
    iget-object v1, p0, LX/1uY;->m:LX/1BF;

    invoke-virtual {v1, p1, v0}, LX/1BF;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 341228
    :catch_0
    move-exception v0

    .line 341229
    sget-object v1, LX/1uY;->a:Ljava/lang/Class;

    const-string v2, "Remote Exception while reading video cache state"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 341204
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    move v3, v1

    .line 341205
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 341206
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 341207
    instance-of v1, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_1

    .line 341208
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 341209
    move-object v0, v2

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v0

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341210
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 341211
    const/4 v1, 0x0

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 341212
    if-eqz v1, :cond_0

    .line 341213
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341214
    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 341215
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 341216
    invoke-static {p0, v2, v1}, LX/1uY;->a(LX/1uY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 341217
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 341218
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 341219
    :cond_2
    monitor-exit p0

    return-void
.end method
