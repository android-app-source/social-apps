.class public LX/1yq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351442
    return-void
.end method

.method public static a(LX/0QB;)LX/1yq;
    .locals 1

    .prologue
    .line 351443
    new-instance v0, LX/1yq;

    invoke-direct {v0}, LX/1yq;-><init>()V

    .line 351444
    move-object v0, v0

    .line 351445
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    .line 351446
    invoke-static {p1}, LX/1VS;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351447
    invoke-static {v0}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, LX/36V;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 351448
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 351449
    invoke-static {p1}, LX/1VS;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 351450
    invoke-static {v1}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 351451
    :cond_0
    :goto_1
    move v0, v0

    .line 351452
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 351453
    :cond_4
    const v2, -0x22a42d2a    # -9.8999738E17f

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    if-ne v2, v1, :cond_0

    .line 351454
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 351455
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/36U;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 351456
    invoke-static {v1}, LX/36V;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, LX/36V;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351457
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1yq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_1
.end method
