.class public LX/1wz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Z

.field private c:Landroid/content/Context;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:LX/1x0;

.field public i:F

.field public j:F

.field public k:F

.field public l:F

.field public m:LX/31M;

.field private n:LX/1x1;

.field private o:Landroid/view/VelocityTracker;

.field public p:I

.field public q:LX/4Ba;

.field public r:LX/39D;

.field public s:LX/39E;

.field private t:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 347538
    const-class v0, LX/1wz;

    sput-object v0, LX/1wz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, 0x0

    .line 347358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347359
    sget-object v0, LX/1x0;->VERTICAL:LX/1x0;

    iput-object v0, p0, LX/1wz;->h:LX/1x0;

    .line 347360
    iput v2, p0, LX/1wz;->i:F

    .line 347361
    iput v2, p0, LX/1wz;->j:F

    .line 347362
    iput v3, p0, LX/1wz;->k:F

    .line 347363
    iput v3, p0, LX/1wz;->l:F

    .line 347364
    iput-object v1, p0, LX/1wz;->m:LX/31M;

    .line 347365
    sget-object v0, LX/1x1;->AT_REST:LX/1x1;

    iput-object v0, p0, LX/1wz;->n:LX/1x1;

    .line 347366
    iput-object v1, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347367
    const/4 v0, 0x0

    iput v0, p0, LX/1wz;->p:I

    .line 347368
    iput-object v1, p0, LX/1wz;->q:LX/4Ba;

    .line 347369
    iput-object v1, p0, LX/1wz;->r:LX/39D;

    .line 347370
    iput-object v1, p0, LX/1wz;->s:LX/39E;

    .line 347371
    iput-object p1, p0, LX/1wz;->c:Landroid/content/Context;

    .line 347372
    return-void
.end method

.method public static a(LX/1wz;FFLX/31M;)V
    .locals 2

    .prologue
    .line 347529
    iput p1, p0, LX/1wz;->i:F

    .line 347530
    iput p2, p0, LX/1wz;->j:F

    .line 347531
    iput-object p3, p0, LX/1wz;->m:LX/31M;

    .line 347532
    iget-object v0, p0, LX/1wz;->n:LX/1x1;

    .line 347533
    sget-object v1, LX/1x1;->DRAGGING:LX/1x1;

    iput-object v1, p0, LX/1wz;->n:LX/1x1;

    .line 347534
    sget-object v1, LX/1x1;->DRAGGING:LX/1x1;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, LX/1wz;->r:LX/39D;

    invoke-interface {v1, p1, p2, p3}, LX/39D;->a(FFLX/31M;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 347535
    iput-object v0, p0, LX/1wz;->n:LX/1x1;

    .line 347536
    invoke-virtual {p0}, LX/1wz;->c()V

    .line 347537
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 347516
    if-nez p1, :cond_0

    .line 347517
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Init Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347518
    :cond_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 347519
    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v1

    .line 347520
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 347521
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    .line 347522
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    .line 347523
    iput v1, p0, LX/1wz;->d:I

    .line 347524
    iput v2, p0, LX/1wz;->e:I

    .line 347525
    iput v3, p0, LX/1wz;->f:I

    .line 347526
    iput v0, p0, LX/1wz;->g:I

    .line 347527
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/1wz;->b:Z

    .line 347528
    return-void
.end method

.method public static d(LX/1wz;)Z
    .locals 2

    .prologue
    .line 347515
    sget-object v0, LX/31M;->UP:LX/31M;

    iget v1, p0, LX/1wz;->p:I

    invoke-virtual {v0, v1}, LX/31M;->isSetInFlags(I)Z

    move-result v0

    return v0
.end method

.method public static e(LX/1wz;)Z
    .locals 2

    .prologue
    .line 347514
    sget-object v0, LX/31M;->DOWN:LX/31M;

    iget v1, p0, LX/1wz;->p:I

    invoke-virtual {v0, v1}, LX/31M;->isSetInFlags(I)Z

    move-result v0

    return v0
.end method

.method public static g(LX/1wz;)Z
    .locals 2

    .prologue
    .line 347513
    sget-object v0, LX/31M;->RIGHT:LX/31M;

    iget v1, p0, LX/1wz;->p:I

    invoke-virtual {v0, v1}, LX/31M;->isSetInFlags(I)Z

    move-result v0

    return v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 347512
    iget v0, p0, LX/1wz;->p:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(LX/1wz;)Z
    .locals 2

    .prologue
    .line 347511
    iget-object v0, p0, LX/1wz;->n:LX/1x1;

    sget-object v1, LX/1x1;->CANCELED:LX/1x1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 3

    .prologue
    .line 347509
    invoke-static {p0}, LX/1wz;->m(LX/1wz;)I

    move-result v0

    .line 347510
    iget v1, p0, LX/1wz;->k:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    int-to-float v2, v0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, LX/1wz;->l:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/1wz;)I
    .locals 1

    .prologue
    .line 347507
    invoke-static {p0}, LX/1wz;->p(LX/1wz;)V

    .line 347508
    iget v0, p0, LX/1wz;->e:I

    return v0
.end method

.method public static p(LX/1wz;)V
    .locals 1

    .prologue
    .line 347502
    iget-boolean v0, p0, LX/1wz;->b:Z

    if-nez v0, :cond_0

    .line 347503
    iget-object v0, p0, LX/1wz;->c:Landroid/content/Context;

    invoke-direct {p0, v0}, LX/1wz;->a(Landroid/content/Context;)V

    .line 347504
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/1wz;->b:Z

    .line 347505
    const/4 v0, 0x0

    iput-object v0, p0, LX/1wz;->c:Landroid/content/Context;

    .line 347506
    :cond_0
    return-void
.end method


# virtual methods
.method public final varargs a([LX/31M;)V
    .locals 3

    .prologue
    .line 347495
    const/4 v0, 0x0

    iput v0, p0, LX/1wz;->p:I

    .line 347496
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 347497
    aget-object v1, p1, v0

    if-eqz v1, :cond_0

    .line 347498
    aget-object v1, p1, v0

    invoke-virtual {v1}, LX/31M;->flag()I

    move-result v1

    .line 347499
    iget v2, p0, LX/1wz;->p:I

    or-int/2addr v2, v1

    iput v2, p0, LX/1wz;->p:I

    .line 347500
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347501
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 347430
    iget-object v0, p0, LX/1wz;->r:LX/39D;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/1wz;->h()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 347431
    :cond_1
    :goto_0
    return v0

    .line 347432
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 347433
    iget-boolean v0, p0, LX/1wz;->t:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 347434
    :goto_1
    iget-boolean v2, p0, LX/1wz;->t:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 347435
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 347436
    :cond_3
    :goto_3
    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result v0

    goto :goto_0

    .line 347437
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_1

    .line 347438
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    goto :goto_2

    .line 347439
    :pswitch_0
    sget-object v3, LX/1x1;->DECIDING:LX/1x1;

    iput-object v3, p0, LX/1wz;->n:LX/1x1;

    .line 347440
    iput v4, p0, LX/1wz;->k:F

    .line 347441
    iput v4, p0, LX/1wz;->l:F

    .line 347442
    iget-object v3, p0, LX/1wz;->q:LX/4Ba;

    if-eqz v3, :cond_8

    .line 347443
    iget-object v3, p0, LX/1wz;->q:LX/4Ba;

    invoke-interface {v3, v0, v2}, LX/4Ba;->a(FF)Z

    move-result v3

    .line 347444
    :goto_4
    move v3, v3

    .line 347445
    if-nez v3, :cond_6

    .line 347446
    invoke-virtual {p0}, LX/1wz;->c()V

    move v0, v1

    .line 347447
    goto :goto_0

    .line 347448
    :cond_6
    iput v0, p0, LX/1wz;->i:F

    .line 347449
    iput v2, p0, LX/1wz;->j:F

    .line 347450
    iget-object v1, p0, LX/1wz;->q:LX/4Ba;

    if-eqz v1, :cond_9

    .line 347451
    iget-object v1, p0, LX/1wz;->q:LX/4Ba;

    invoke-interface {v1, v0, v2}, LX/4Ba;->b(FF)Z

    move-result v1

    .line 347452
    :goto_5
    move v1, v1

    .line 347453
    if-eqz v1, :cond_3

    .line 347454
    iget-object v1, p0, LX/1wz;->m:LX/31M;

    if-eqz v1, :cond_a

    .line 347455
    iget-object v1, p0, LX/1wz;->m:LX/31M;

    .line 347456
    :goto_6
    move-object v1, v1

    .line 347457
    invoke-static {p0, v0, v2, v1}, LX/1wz;->a(LX/1wz;FFLX/31M;)V

    goto :goto_3

    .line 347458
    :pswitch_1
    const/high16 p1, 0x3f000000    # 0.5f

    .line 347459
    invoke-static {p0}, LX/1wz;->i(LX/1wz;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 347460
    :cond_7
    :goto_7
    goto :goto_3

    .line 347461
    :pswitch_2
    iget-object v1, p0, LX/1wz;->s:LX/39E;

    if-eqz v1, :cond_3

    invoke-static {p0}, LX/1wz;->i(LX/1wz;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0}, LX/1wz;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 347462
    iget-object v1, p0, LX/1wz;->s:LX/39E;

    invoke-interface {v1, v0, v2}, LX/39E;->c(FF)Z

    move-result v0

    .line 347463
    if-nez v0, :cond_1

    .line 347464
    invoke-virtual {p0}, LX/1wz;->c()V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x1

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 347465
    :cond_a
    invoke-static {p0}, LX/1wz;->g(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 347466
    sget-object v1, LX/31M;->RIGHT:LX/31M;

    goto :goto_6

    .line 347467
    :cond_b
    invoke-static {p0}, LX/1wz;->e(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 347468
    sget-object v1, LX/31M;->DOWN:LX/31M;

    goto :goto_6

    .line 347469
    :cond_c
    invoke-static {p0}, LX/1wz;->d(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 347470
    sget-object v1, LX/31M;->UP:LX/31M;

    goto :goto_6

    .line 347471
    :cond_d
    sget-object v1, LX/31M;->LEFT:LX/31M;

    goto :goto_6

    .line 347472
    :cond_e
    invoke-static {p0}, LX/1wz;->p(LX/1wz;)V

    .line 347473
    iget v1, p0, LX/1wz;->d:I

    move v1, v1

    .line 347474
    invoke-static {p0}, LX/1wz;->m(LX/1wz;)I

    move-result v3

    .line 347475
    iget v4, p0, LX/1wz;->i:F

    sub-float v4, v0, v4

    float-to-int v4, v4

    .line 347476
    iget v5, p0, LX/1wz;->j:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    .line 347477
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 347478
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 347479
    iget v8, p0, LX/1wz;->k:F

    int-to-float v9, v4

    add-float/2addr v8, v9

    iput v8, p0, LX/1wz;->k:F

    .line 347480
    iget v8, p0, LX/1wz;->l:F

    int-to-float v9, v5

    add-float/2addr v8, v9

    iput v8, p0, LX/1wz;->l:F

    .line 347481
    if-le v7, v3, :cond_12

    iget-object v3, p0, LX/1wz;->h:LX/1x0;

    sget-object v8, LX/1x0;->VERTICAL:LX/1x0;

    if-eq v3, v8, :cond_f

    int-to-float v3, v7

    mul-float/2addr v3, p1

    int-to-float v8, v6

    cmpl-float v3, v3, v8

    if-lez v3, :cond_12

    .line 347482
    :cond_f
    if-gez v5, :cond_10

    invoke-static {p0}, LX/1wz;->d(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 347483
    sget-object v1, LX/31M;->UP:LX/31M;

    invoke-static {p0, v0, v2, v1}, LX/1wz;->a(LX/1wz;FFLX/31M;)V

    goto/16 :goto_7

    .line 347484
    :cond_10
    if-lez v5, :cond_11

    invoke-static {p0}, LX/1wz;->e(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 347485
    sget-object v1, LX/31M;->DOWN:LX/31M;

    invoke-static {p0, v0, v2, v1}, LX/1wz;->a(LX/1wz;FFLX/31M;)V

    goto/16 :goto_7

    .line 347486
    :cond_11
    invoke-virtual {p0}, LX/1wz;->c()V

    goto/16 :goto_7

    .line 347487
    :cond_12
    if-le v6, v1, :cond_7

    iget-object v1, p0, LX/1wz;->h:LX/1x0;

    sget-object v3, LX/1x0;->HORIZONTAL:LX/1x0;

    if-eq v1, v3, :cond_13

    int-to-float v1, v6

    mul-float/2addr v1, p1

    int-to-float v3, v7

    cmpl-float v1, v1, v3

    if-lez v1, :cond_7

    .line 347488
    :cond_13
    if-gez v4, :cond_14

    .line 347489
    sget-object v1, LX/31M;->LEFT:LX/31M;

    iget v3, p0, LX/1wz;->p:I

    invoke-virtual {v1, v3}, LX/31M;->isSetInFlags(I)Z

    move-result v1

    move v1, v1

    .line 347490
    if-eqz v1, :cond_14

    .line 347491
    sget-object v1, LX/31M;->LEFT:LX/31M;

    invoke-static {p0, v0, v2, v1}, LX/1wz;->a(LX/1wz;FFLX/31M;)V

    goto/16 :goto_7

    .line 347492
    :cond_14
    if-lez v4, :cond_15

    invoke-static {p0}, LX/1wz;->g(LX/1wz;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 347493
    sget-object v1, LX/31M;->RIGHT:LX/31M;

    invoke-static {p0, v0, v2, v1}, LX/1wz;->a(LX/1wz;FFLX/31M;)V

    goto/16 :goto_7

    .line 347494
    :cond_15
    invoke-virtual {p0}, LX/1wz;->c()V

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 347429
    iget-object v0, p0, LX/1wz;->n:LX/1x1;

    sget-object v1, LX/1x1;->DRAGGING:LX/1x1;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 347383
    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 347384
    invoke-virtual {p0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    .line 347385
    sget-object v1, LX/4BZ;->a:[I

    iget-object v3, p0, LX/1wz;->n:LX/1x1;

    invoke-virtual {v3}, LX/1x1;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v0, v2

    .line 347386
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 347387
    :cond_1
    iget-object v1, p0, LX/1wz;->r:LX/39D;

    if-eqz v1, :cond_0

    invoke-direct {p0}, LX/1wz;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 347388
    iget-object v0, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    if-nez v0, :cond_2

    .line 347389
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347390
    :cond_2
    iget-object v0, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 347391
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 347392
    iget-boolean v0, p0, LX/1wz;->t:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 347393
    :goto_1
    iget-boolean v1, p0, LX/1wz;->t:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    move v3, v1

    .line 347394
    :goto_2
    packed-switch v4, :pswitch_data_1

    :goto_3
    move v0, v2

    .line 347395
    goto :goto_0

    .line 347396
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_1

    .line 347397
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    move v3, v1

    goto :goto_2

    .line 347398
    :pswitch_1
    iget v1, p0, LX/1wz;->i:F

    sub-float v1, v0, v1

    .line 347399
    iget v4, p0, LX/1wz;->j:F

    sub-float v4, v3, v4

    .line 347400
    iput v0, p0, LX/1wz;->i:F

    .line 347401
    iput v3, p0, LX/1wz;->j:F

    .line 347402
    iget v0, p0, LX/1wz;->k:F

    add-float/2addr v0, v1

    iput v0, p0, LX/1wz;->k:F

    .line 347403
    iget v0, p0, LX/1wz;->l:F

    add-float/2addr v0, v4

    iput v0, p0, LX/1wz;->l:F

    .line 347404
    iget-object v0, p0, LX/1wz;->m:LX/31M;

    invoke-virtual {v0}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 347405
    iget-object v3, p0, LX/1wz;->r:LX/39D;

    cmpg-float v0, v4, v5

    if-gez v0, :cond_5

    sget-object v0, LX/31M;->UP:LX/31M;

    :goto_4
    invoke-interface {v3, v1, v4, v0}, LX/39D;->b(FFLX/31M;)V

    goto :goto_3

    :cond_5
    sget-object v0, LX/31M;->DOWN:LX/31M;

    goto :goto_4

    .line 347406
    :cond_6
    iget-object v3, p0, LX/1wz;->r:LX/39D;

    cmpg-float v0, v1, v5

    if-gez v0, :cond_7

    sget-object v0, LX/31M;->LEFT:LX/31M;

    :goto_5
    invoke-interface {v3, v1, v4, v0}, LX/39D;->b(FFLX/31M;)V

    goto :goto_3

    :cond_7
    sget-object v0, LX/31M;->RIGHT:LX/31M;

    goto :goto_5

    .line 347407
    :pswitch_2
    iget-object v4, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347408
    const/4 v1, 0x0

    iput-object v1, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347409
    const/16 v1, 0x3e8

    .line 347410
    invoke-static {p0}, LX/1wz;->p(LX/1wz;)V

    .line 347411
    iget v5, p0, LX/1wz;->g:I

    move v5, v5

    .line 347412
    int-to-float v5, v5

    invoke-virtual {v4, v1, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 347413
    iget-object v1, p0, LX/1wz;->m:LX/31M;

    invoke-virtual {v1}, LX/31M;->isXAxis()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 347414
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    float-to-int v1, v1

    .line 347415
    :goto_6
    invoke-static {p0}, LX/1wz;->p(LX/1wz;)V

    .line 347416
    iget v5, p0, LX/1wz;->f:I

    move v5, v5

    .line 347417
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-le v6, v5, :cond_d

    .line 347418
    if-gez v1, :cond_b

    .line 347419
    iget-object v3, p0, LX/1wz;->r:LX/39D;

    iget-object v0, p0, LX/1wz;->m:LX/31M;

    invoke-virtual {v0}, LX/31M;->isXAxis()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, LX/31M;->LEFT:LX/31M;

    :goto_7
    invoke-interface {v3, v0, v1}, LX/39D;->a(LX/31M;I)V

    .line 347420
    :cond_8
    :goto_8
    sget-object v0, LX/1x1;->AT_REST:LX/1x1;

    iput-object v0, p0, LX/1wz;->n:LX/1x1;

    .line 347421
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->recycle()V

    goto/16 :goto_3

    .line 347422
    :cond_9
    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    float-to-int v1, v1

    goto :goto_6

    .line 347423
    :cond_a
    sget-object v0, LX/31M;->UP:LX/31M;

    goto :goto_7

    .line 347424
    :cond_b
    if-lez v1, :cond_8

    .line 347425
    iget-object v3, p0, LX/1wz;->r:LX/39D;

    iget-object v0, p0, LX/1wz;->m:LX/31M;

    invoke-virtual {v0}, LX/31M;->isXAxis()Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    :goto_9
    invoke-interface {v3, v0, v1}, LX/39D;->a(LX/31M;I)V

    goto :goto_8

    :cond_c
    sget-object v0, LX/31M;->DOWN:LX/31M;

    goto :goto_9

    .line 347426
    :cond_d
    iget-object v1, p0, LX/1wz;->s:LX/39E;

    if-eqz v1, :cond_e

    invoke-direct {p0}, LX/1wz;->j()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 347427
    iget-object v1, p0, LX/1wz;->s:LX/39E;

    invoke-interface {v1, v0, v3}, LX/39E;->d(FF)V

    goto :goto_8

    .line 347428
    :cond_e
    iget-object v0, p0, LX/1wz;->r:LX/39D;

    invoke-interface {v0}, LX/39D;->a()V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 347373
    invoke-virtual {p0}, LX/1wz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347374
    iget-object v0, p0, LX/1wz;->r:LX/39D;

    invoke-interface {v0}, LX/39D;->b()V

    .line 347375
    :cond_0
    iput v1, p0, LX/1wz;->i:F

    .line 347376
    iput v1, p0, LX/1wz;->j:F

    .line 347377
    sget-object v0, LX/1x1;->CANCELED:LX/1x1;

    iput-object v0, p0, LX/1wz;->n:LX/1x1;

    .line 347378
    iget-object v0, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347379
    const/4 v1, 0x0

    iput-object v1, p0, LX/1wz;->o:Landroid/view/VelocityTracker;

    .line 347380
    if-eqz v0, :cond_1

    .line 347381
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 347382
    :cond_1
    return-void
.end method
