.class public LX/1z2;
.super Landroid/text/style/ReplacementSpan;
.source ""


# instance fields
.field private final a:Landroid/util/DisplayMetrics;

.field public final b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/util/DisplayMetrics;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 351734
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 351735
    iput-object p1, p0, LX/1z2;->a:Landroid/util/DisplayMetrics;

    .line 351736
    iput-object p2, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    .line 351737
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 351738
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 351739
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v3, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 351740
    return-void

    :cond_0
    move v0, v2

    .line 351741
    goto :goto_0

    :cond_1
    move v1, v2

    .line 351742
    goto :goto_1
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5

    .prologue
    .line 351743
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 351744
    :goto_0
    return-void

    .line 351745
    :cond_0
    float-to-int v0, p5

    .line 351746
    iget-object v1, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 351747
    invoke-virtual {p9}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v2

    .line 351748
    iget v2, v2, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    mul-int/lit8 v2, v2, -0x1

    .line 351749
    div-int/lit8 v2, v2, 0x2

    sub-int v2, p7, v2

    .line 351750
    sub-int v1, v2, v1

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, LX/1z2;->a:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 351751
    iget-object v2, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 351752
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 1

    .prologue
    .line 351753
    iget-object v0, p0, LX/1z2;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method
