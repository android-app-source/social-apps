.class public LX/216;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0wT;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/0wW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 356647
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/216;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0wW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356649
    iput-object p1, p0, LX/216;->b:LX/0wW;

    .line 356650
    return-void
.end method

.method public static a(LX/0QB;)LX/216;
    .locals 4

    .prologue
    .line 356651
    const-class v1, LX/216;

    monitor-enter v1

    .line 356652
    :try_start_0
    sget-object v0, LX/216;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356653
    sput-object v2, LX/216;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 356656
    new-instance p0, LX/216;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-direct {p0, v3}, LX/216;-><init>(LX/0wW;)V

    .line 356657
    move-object v0, p0

    .line 356658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/216;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0wd;
    .locals 7

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v2, 0x3f50624de0000000L    # 0.0010000000474974513

    .line 356662
    iget-object v0, p0, LX/216;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    .line 356663
    iput-wide v2, v0, LX/0wd;->l:D

    .line 356664
    move-object v0, v0

    .line 356665
    iput-wide v2, v0, LX/0wd;->k:D

    .line 356666
    move-object v0, v0

    .line 356667
    sget-object v1, LX/216;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    return-object v0
.end method
