.class public final LX/205;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1za;


# direct methods
.method public constructor <init>(LX/1za;)V
    .locals 0

    .prologue
    .line 354348
    iput-object p1, p0, LX/205;->a:LX/1za;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 354349
    check-cast p1, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    const v0, 0x7fffffff

    .line 354350
    iget-object v1, p0, LX/205;->a:LX/1za;

    iget-object v1, v1, LX/1za;->d:LX/1zf;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/205;->a:LX/1za;

    iget-object v1, v1, LX/1za;->d:LX/1zf;

    .line 354351
    iget-object v2, v1, LX/1zf;->c:LX/0Px;

    move-object v1, v2

    .line 354352
    if-eqz v1, :cond_1

    .line 354353
    iget-object v1, p0, LX/205;->a:LX/1za;

    iget-object v1, v1, LX/1za;->d:LX/1zf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;->j()I

    move-result v2

    invoke-virtual {v1, v2}, LX/1zf;->a(I)LX/1zt;

    move-result-object v1

    .line 354354
    iget-object v2, p0, LX/205;->a:LX/1za;

    iget-object v2, v2, LX/1za;->d:LX/1zf;

    .line 354355
    iget-object p0, v2, LX/1zf;->c:LX/0Px;

    move-object v2, p0

    .line 354356
    invoke-virtual {v2, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 354357
    if-gez v1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 354358
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 354359
    goto :goto_0

    .line 354360
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method
