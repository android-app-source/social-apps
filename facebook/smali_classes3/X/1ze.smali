.class public final LX/1ze;
.super LX/1sm;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/GwtCompatible;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "LX/1sm",
        "<TF;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field public final function:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<TF;+TT;>;"
        }
    .end annotation
.end field

.field public final ordering:LX/1sm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1sm",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/1sm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<TF;+TT;>;",
            "LX/1sm",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 353830
    invoke-direct {p0}, LX/1sm;-><init>()V

    .line 353831
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iput-object v0, p0, LX/1ze;->function:LX/0QK;

    .line 353832
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sm;

    iput-object v0, p0, LX/1ze;->ordering:LX/1sm;

    .line 353833
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TF;)I"
        }
    .end annotation

    .prologue
    .line 353834
    iget-object v0, p0, LX/1ze;->ordering:LX/1sm;

    iget-object v1, p0, LX/1ze;->function:LX/0QK;

    invoke-interface {v1, p1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/1ze;->function:LX/0QK;

    invoke-interface {v2, p2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1sm;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 353824
    if-ne p1, p0, :cond_1

    .line 353825
    :cond_0
    :goto_0
    return v0

    .line 353826
    :cond_1
    instance-of v2, p1, LX/1ze;

    if-eqz v2, :cond_3

    .line 353827
    check-cast p1, LX/1ze;

    .line 353828
    iget-object v2, p0, LX/1ze;->function:LX/0QK;

    iget-object v3, p1, LX/1ze;->function:LX/0QK;

    invoke-interface {v2, v3}, LX/0QK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/1ze;->ordering:LX/1sm;

    iget-object v3, p1, LX/1ze;->ordering:LX/1sm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 353829
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 353823
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/1ze;->function:LX/0QK;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/1ze;->ordering:LX/1sm;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 353822
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/1ze;->ordering:LX/1sm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onResultOf("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/1ze;->function:LX/0QK;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
