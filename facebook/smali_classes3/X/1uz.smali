.class public LX/1uz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/net/Uri;

.field private final c:LX/1uy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1uy",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field private final e:LX/1Gm;

.field public final f:LX/0Zb;

.field public final g:LX/1Gk;

.field private final h:LX/1Gl;

.field private final i:LX/1Go;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 341825
    const-class v0, LX/1uz;

    sput-object v0, LX/1uz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;LX/1uy;Ljava/lang/String;LX/1Gm;LX/0Zb;LX/1Gk;LX/1Gl;LX/1Go;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/1uy",
            "<TT;>;",
            "Ljava/lang/String;",
            "LX/1Gm;",
            "LX/0Zb;",
            "LX/1Gk;",
            "LX/1Gl;",
            "LX/1Go;",
            ")V"
        }
    .end annotation

    .prologue
    .line 341826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341827
    iput-object p1, p0, LX/1uz;->b:Landroid/net/Uri;

    .line 341828
    iput-object p2, p0, LX/1uz;->c:LX/1uy;

    .line 341829
    iput-object p3, p0, LX/1uz;->d:Ljava/lang/String;

    .line 341830
    iput-object p4, p0, LX/1uz;->e:LX/1Gm;

    .line 341831
    iput-object p5, p0, LX/1uz;->f:LX/0Zb;

    .line 341832
    iput-object p6, p0, LX/1uz;->g:LX/1Gk;

    .line 341833
    iput-object p7, p0, LX/1uz;->h:LX/1Gl;

    .line 341834
    iput-object p8, p0, LX/1uz;->i:LX/1Go;

    .line 341835
    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/16 v5, 0xc8

    .line 341836
    sget-object v0, LX/2ur;->NOT_IN_GK:LX/2ur;

    .line 341837
    iget-object v1, p0, LX/1uz;->i:LX/1Go;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/1uz;->i:LX/1Go;

    invoke-interface {v1}, LX/1Go;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341838
    iget-object v0, p0, LX/1uz;->i:LX/1Go;

    invoke-interface {v0, p1}, LX/1Go;->a(Lorg/apache/http/HttpResponse;)LX/2ur;

    move-result-object v0

    .line 341839
    :cond_0
    iget-object v1, p0, LX/1uz;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 341840
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 341841
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 341842
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 341843
    if-ne v3, v5, :cond_2

    if-eqz v4, :cond_2

    .line 341844
    iget-object v2, p0, LX/1uz;->e:LX/1Gm;

    const-wide/16 v8, 0x1

    .line 341845
    invoke-static {v2}, LX/1Gm;->f(LX/1Gm;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 341846
    :cond_1
    :goto_0
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 341847
    :try_start_0
    iget-object v2, p0, LX/1uz;->c:LX/1uy;

    invoke-static {p1}, LX/1Gl;->c(Lorg/apache/http/HttpResponse;)J

    move-result-wide v4

    invoke-interface {v2, v1, v4, v5, v0}, LX/1uy;->a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 341848
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    .line 341849
    :cond_2
    const-string v0, "MediaDownloader (HTTP code)"

    const/16 v9, 0x5f

    .line 341850
    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v6

    .line 341851
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 341852
    invoke-virtual {v6}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341853
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 341854
    iget-object v6, p0, LX/1uz;->g:LX/1Gk;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-wide/32 v8, 0x36ee80

    invoke-virtual {v6, v7, v8, v9}, LX/1Gk;->a(Ljava/lang/String;J)Z

    move-result v6

    if-nez v6, :cond_3

    .line 341855
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Url: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341856
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "media_downloader_failure"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "category"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, LX/1uz;->d:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "url"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "http_code"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 341857
    iget-object v7, p0, LX/1uz;->f:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 341858
    :cond_3
    if-eq v3, v5, :cond_4

    .line 341859
    new-instance v0, Lorg/apache/http/client/HttpResponseException;

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 341860
    :cond_4
    new-instance v0, Lorg/apache/http/client/ClientProtocolException;

    const-string v1, "Missing HTTP entity"

    invoke-direct {v0, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341861
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    .line 341862
    :cond_5
    const-string v6, "total_succeed"

    invoke-virtual {v2, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 341863
    iget-object v6, v2, LX/1Gm;->j:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 341864
    if-eqz v6, :cond_1

    .line 341865
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 341866
    const-string v6, "succeed_on_fourth_onward_try"

    invoke-virtual {v2, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 341867
    :pswitch_0
    const-string v6, "succeed_on_first_try"

    invoke-virtual {v2, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 341868
    :pswitch_1
    const-string v6, "succeed_on_second_try"

    invoke-virtual {v2, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 341869
    :pswitch_2
    const-string v6, "succeed_on_third_try"

    invoke-virtual {v2, v6, v8, v9}, LX/0Vx;->a(Ljava/lang/String;J)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
