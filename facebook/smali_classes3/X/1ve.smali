.class public LX/1ve;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1vg;

.field private final c:LX/1VI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343371
    new-instance v0, LX/1vf;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1vf;-><init>(I)V

    sput-object v0, LX/1ve;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/1vg;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343373
    iput-object p1, p0, LX/1ve;->b:LX/1vg;

    .line 343374
    iput-object p2, p0, LX/1ve;->c:LX/1VI;

    .line 343375
    return-void
.end method

.method public static a(LX/1ve;)I
    .locals 5
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 343361
    iget-object v0, p0, LX/1ve;->c:LX/1VI;

    .line 343362
    invoke-static {v0}, LX/1VI;->c(LX/1VI;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 343363
    const/4 v1, 0x0

    .line 343364
    :goto_0
    move v0, v1

    .line 343365
    if-eqz v0, :cond_0

    .line 343366
    const v0, 0x7f020723

    .line 343367
    :goto_1
    return v0

    :cond_0
    const v0, 0x7f020aea

    goto :goto_1

    .line 343368
    :cond_1
    iget-object v1, v0, LX/1VI;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    .line 343369
    iget-object v1, v0, LX/1VI;->a:LX/0W3;

    sget-wide v3, LX/0X5;->jN:J

    invoke-interface {v1, v3, v4}, LX/0W4;->a(J)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1VI;->d:Ljava/lang/Boolean;

    .line 343370
    :cond_2
    iget-object v1, v0, LX/1VI;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1ve;
    .locals 5

    .prologue
    .line 343350
    const-class v1, LX/1ve;

    monitor-enter v1

    .line 343351
    :try_start_0
    sget-object v0, LX/1ve;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343352
    sput-object v2, LX/1ve;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343353
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343354
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343355
    new-instance p0, LX/1ve;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v4

    check-cast v4, LX/1VI;

    invoke-direct {p0, v3, v4}, LX/1ve;-><init>(LX/1vg;LX/1VI;)V

    .line 343356
    move-object v0, p0

    .line 343357
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343358
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1ve;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343359
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343360
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1dl;)Z
    .locals 1

    .prologue
    .line 343349
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
