.class public LX/22c;
.super LX/1jx;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 359985
    invoke-direct {p0}, LX/1jx;-><init>()V

    .line 359986
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/22c;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(LX/0jT;)Z
    .locals 5

    .prologue
    .line 359987
    instance-of v0, p1, LX/16i;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 359988
    check-cast v0, LX/16i;

    .line 359989
    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    .line 359990
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 359991
    iget-object v1, p0, LX/22c;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359992
    :cond_0
    instance-of v0, p1, LX/0jX;

    if-eqz v0, :cond_2

    .line 359993
    check-cast p1, LX/0jX;

    invoke-interface {p1}, LX/0jX;->d()LX/0Px;

    move-result-object v2

    .line 359994
    if-eqz v2, :cond_2

    .line 359995
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 359996
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 359997
    iget-object v4, p0, LX/22c;->a:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 359998
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 359999
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
