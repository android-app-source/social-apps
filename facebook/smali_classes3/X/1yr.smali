.class public LX/1yr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:[I

.field public static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final c:LX/359;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/359",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/1vg;

.field public final e:LX/1yo;

.field public final f:Landroid/content/res/ColorStateList;

.field public final g:LX/1yf;

.field public final h:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 351458
    new-array v0, v3, [I

    const v1, 0x10100a1

    aput v1, v0, v2

    sput-object v0, LX/1yr;->a:[I

    .line 351459
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 351460
    sput-object v0, LX/1yr;->b:Landroid/util/SparseArray;

    const v1, 0x7f0d166f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 351461
    return-void
.end method

.method public constructor <init>(LX/359;LX/1vg;LX/1yo;Landroid/content/res/Resources;LX/0Uh;LX/1yf;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351463
    iput-object p1, p0, LX/1yr;->c:LX/359;

    .line 351464
    iput-object p2, p0, LX/1yr;->d:LX/1vg;

    .line 351465
    iput-object p3, p0, LX/1yr;->e:LX/1yo;

    .line 351466
    const v0, 0x7f0a0a29

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, LX/1yr;->f:Landroid/content/res/ColorStateList;

    .line 351467
    iput-object p5, p0, LX/1yr;->h:LX/0Uh;

    .line 351468
    iput-object p6, p0, LX/1yr;->g:LX/1yf;

    .line 351469
    return-void
.end method

.method public static a(LX/0QB;)LX/1yr;
    .locals 10

    .prologue
    .line 351470
    const-class v1, LX/1yr;

    monitor-enter v1

    .line 351471
    :try_start_0
    sget-object v0, LX/1yr;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351472
    sput-object v2, LX/1yr;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351473
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351474
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351475
    new-instance v3, LX/1yr;

    invoke-static {v0}, LX/359;->a(LX/0QB;)LX/359;

    move-result-object v4

    check-cast v4, LX/359;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/1yo;->b(LX/0QB;)LX/1yo;

    move-result-object v6

    check-cast v6, LX/1yo;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/1yf;->a(LX/0QB;)LX/1yf;

    move-result-object v9

    check-cast v9, LX/1yf;

    invoke-direct/range {v3 .. v9}, LX/1yr;-><init>(LX/359;LX/1vg;LX/1yo;Landroid/content/res/Resources;LX/0Uh;LX/1yf;)V

    .line 351476
    move-object v0, v3

    .line 351477
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351478
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351479
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yo;LX/0Uh;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1yo;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 351481
    const/16 v0, 0x3b8

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 351482
    invoke-static {p0}, LX/1yo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351483
    :goto_0
    return-object v0

    .line 351484
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351485
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const v1, -0x22a42d2a    # -9.8999738E17f

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yq;LX/1yo;LX/0Uh;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1yq;",
            "LX/1yo;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 351486
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 351487
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 351488
    invoke-static {v0}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    .line 351489
    const/16 v3, 0x3b9

    invoke-virtual {p3, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    invoke-virtual {p1, v0}, LX/1yq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 351490
    :goto_0
    return v0

    .line 351491
    :cond_0
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 351492
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 351493
    goto :goto_0

    .line 351494
    :cond_1
    const/4 v2, 0x0

    .line 351495
    if-nez v0, :cond_5

    .line 351496
    :cond_2
    :goto_1
    move v0, v2

    .line 351497
    if-eqz v0, :cond_3

    move v0, v1

    .line 351498
    goto :goto_0

    .line 351499
    :cond_3
    invoke-static {p0, p2, p3}, LX/1yr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1yo;LX/0Uh;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 351500
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    .line 351501
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 351502
    if-eqz v3, :cond_2

    .line 351503
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v2

    goto :goto_1
.end method
