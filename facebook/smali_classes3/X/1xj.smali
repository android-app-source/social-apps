.class public final LX/1xj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ot;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Ot",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 349322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349323
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/1xj;->a:Landroid/content/Context;

    .line 349324
    iput p2, p0, LX/1xj;->b:I

    .line 349325
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 349326
    iget-object v0, p0, LX/1xj;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 349327
    monitor-enter p0

    .line 349328
    :try_start_0
    iget-object v0, p0, LX/1xj;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 349329
    iget-object v0, p0, LX/1xj;->a:Landroid/content/Context;

    iget v1, p0, LX/1xj;->b:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/1xj;->c:Ljava/lang/String;

    .line 349330
    const/4 v0, 0x0

    iput-object v0, p0, LX/1xj;->a:Landroid/content/Context;

    .line 349331
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349332
    :cond_1
    iget-object v0, p0, LX/1xj;->c:Ljava/lang/String;

    return-object v0

    .line 349333
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
