.class public LX/20m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/20m;


# instance fields
.field private a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356013
    iput-object p1, p0, LX/20m;->a:LX/0Zb;

    .line 356014
    return-void
.end method

.method public static a(LX/0QB;)LX/20m;
    .locals 4

    .prologue
    .line 356015
    sget-object v0, LX/20m;->b:LX/20m;

    if-nez v0, :cond_1

    .line 356016
    const-class v1, LX/20m;

    monitor-enter v1

    .line 356017
    :try_start_0
    sget-object v0, LX/20m;->b:LX/20m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 356018
    if-eqz v2, :cond_0

    .line 356019
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 356020
    new-instance p0, LX/20m;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/20m;-><init>(LX/0Zb;)V

    .line 356021
    move-object v0, p0

    .line 356022
    sput-object v0, LX/20m;->b:LX/20m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356023
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 356024
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 356025
    :cond_1
    sget-object v0, LX/20m;->b:LX/20m;

    return-object v0

    .line 356026
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 356027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/9BS;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 356028
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feedback_reactions_nux_interaction"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 356029
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 356030
    const-string v1, "feedback_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 356031
    const-string v1, "reactions_nux_source"

    invoke-virtual {p3}, LX/9BS;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 356032
    const-string v1, "nux_id"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 356033
    iget-object v1, p0, LX/20m;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 356034
    return-void
.end method
