.class public LX/21T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/21U;

.field private final b:LX/21V;

.field private final c:Landroid/content/res/Resources;

.field private d:LX/21Y;

.field private e:LX/21Y;

.field private f:LX/21Y;

.field private g:LX/21Y;

.field private h:LX/21Y;

.field private i:LX/21Y;

.field private j:LX/21Y;


# direct methods
.method public constructor <init>(LX/21S;Landroid/content/res/Resources;LX/1qw;)V
    .locals 2
    .param p1    # LX/21S;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357204
    new-instance v0, LX/21U;

    invoke-direct {v0, p2}, LX/21U;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/21T;->a:LX/21U;

    .line 357205
    iget-object v0, p0, LX/21T;->a:LX/21U;

    .line 357206
    iget-object v1, v0, LX/21U;->b:Ljava/util/EnumMap;

    invoke-virtual {v1, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/21V;

    move-object v0, v1

    .line 357207
    iput-object v0, p0, LX/21T;->b:LX/21V;

    .line 357208
    iput-object p2, p0, LX/21T;->c:Landroid/content/res/Resources;

    .line 357209
    new-instance v0, LX/21W;

    invoke-direct {v0, p0}, LX/21W;-><init>(LX/21T;)V

    invoke-virtual {p3, v0}, LX/1qw;->a(LX/1rV;)V

    .line 357210
    invoke-static {p0}, LX/21T;->a$redex0(LX/21T;)V

    .line 357211
    return-void
.end method

.method public static a$redex0(LX/21T;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 357175
    new-instance v0, LX/21X;

    iget-object v1, p0, LX/21T;->c:Landroid/content/res/Resources;

    iget-object v2, p0, LX/21T;->b:LX/21V;

    invoke-direct {v0, v1, v2}, LX/21X;-><init>(Landroid/content/res/Resources;LX/21V;)V

    .line 357176
    iget v1, v0, LX/21X;->a:I

    move v1, v1

    .line 357177
    iget v2, v0, LX/21X;->b:I

    move v2, v2

    .line 357178
    iget v3, v0, LX/21X;->c:I

    move v3, v3

    .line 357179
    new-instance v4, LX/21Y;

    new-array v5, v9, [I

    aput v1, v5, v6

    aput v2, v5, v7

    aput v3, v5, v8

    invoke-direct {v4, v0, v5}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v4, p0, LX/21T;->d:LX/21Y;

    .line 357180
    new-instance v4, LX/21Y;

    new-array v5, v9, [I

    aput v1, v5, v6

    aput v2, v5, v7

    aput v6, v5, v8

    invoke-direct {v4, v0, v5}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v4, p0, LX/21T;->e:LX/21Y;

    .line 357181
    new-instance v4, LX/21Y;

    new-array v5, v9, [I

    aput v6, v5, v6

    aput v2, v5, v7

    aput v3, v5, v8

    invoke-direct {v4, v0, v5}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v4, p0, LX/21T;->f:LX/21Y;

    .line 357182
    new-instance v4, LX/21Y;

    new-array v5, v9, [I

    aput v1, v5, v6

    aput v6, v5, v7

    aput v3, v5, v8

    invoke-direct {v4, v0, v5}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v4, p0, LX/21T;->g:LX/21Y;

    .line 357183
    new-instance v4, LX/21Y;

    new-array v5, v9, [I

    aput v1, v5, v6

    aput v6, v5, v7

    aput v6, v5, v8

    invoke-direct {v4, v0, v5}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v4, p0, LX/21T;->h:LX/21Y;

    .line 357184
    new-instance v1, LX/21Y;

    new-array v4, v9, [I

    aput v6, v4, v6

    aput v2, v4, v7

    aput v6, v4, v8

    invoke-direct {v1, v0, v4}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v1, p0, LX/21T;->i:LX/21Y;

    .line 357185
    new-instance v1, LX/21Y;

    new-array v2, v9, [I

    aput v6, v2, v6

    aput v6, v2, v7

    aput v3, v2, v8

    invoke-direct {v1, v0, v2}, LX/21Y;-><init>(LX/21X;[I)V

    iput-object v1, p0, LX/21T;->j:LX/21Y;

    .line 357186
    return-void
.end method


# virtual methods
.method public final a(ZZZ)LX/21Y;
    .locals 1

    .prologue
    .line 357187
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 357188
    iget-object v0, p0, LX/21T;->d:LX/21Y;

    .line 357189
    :goto_0
    return-object v0

    .line 357190
    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 357191
    iget-object v0, p0, LX/21T;->e:LX/21Y;

    goto :goto_0

    .line 357192
    :cond_1
    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    .line 357193
    iget-object v0, p0, LX/21T;->f:LX/21Y;

    goto :goto_0

    .line 357194
    :cond_2
    if-eqz p1, :cond_3

    if-eqz p3, :cond_3

    .line 357195
    iget-object v0, p0, LX/21T;->g:LX/21Y;

    goto :goto_0

    .line 357196
    :cond_3
    if-eqz p1, :cond_4

    .line 357197
    iget-object v0, p0, LX/21T;->h:LX/21Y;

    goto :goto_0

    .line 357198
    :cond_4
    if-eqz p2, :cond_5

    .line 357199
    iget-object v0, p0, LX/21T;->i:LX/21Y;

    goto :goto_0

    .line 357200
    :cond_5
    if-eqz p3, :cond_6

    .line 357201
    iget-object v0, p0, LX/21T;->j:LX/21Y;

    goto :goto_0

    .line 357202
    :cond_6
    iget-object v0, p0, LX/21T;->d:LX/21Y;

    goto :goto_0
.end method
