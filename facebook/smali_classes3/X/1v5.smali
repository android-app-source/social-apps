.class public final LX/1v5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/DoNotOptimize;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 341955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/location/Location;)LX/0am;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341956
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 341957
    invoke-virtual {p0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v0

    .line 341958
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 341959
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 341960
    :goto_1
    return-object v0

    .line 341961
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 341962
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_1
.end method
