.class public LX/20r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0tH;

.field public b:Landroid/widget/PopupWindow;

.field public c:LX/9aR;

.field public d:LX/9aS;


# direct methods
.method public constructor <init>(LX/0tH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356214
    iput-object p1, p0, LX/20r;->a:LX/0tH;

    .line 356215
    return-void
.end method

.method public static a(LX/0QB;)LX/20r;
    .locals 4

    .prologue
    .line 356216
    const-class v1, LX/20r;

    monitor-enter v1

    .line 356217
    :try_start_0
    sget-object v0, LX/20r;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356218
    sput-object v2, LX/20r;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 356221
    new-instance p0, LX/20r;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v3

    check-cast v3, LX/0tH;

    invoke-direct {p0, v3}, LX/20r;-><init>(LX/0tH;)V

    .line 356222
    move-object v0, p0

    .line 356223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/20r;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 356227
    iget-object v0, p0, LX/20r;->c:LX/9aR;

    if-nez v0, :cond_0

    .line 356228
    new-instance v0, LX/9aR;

    invoke-direct {v0, p1}, LX/9aR;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/20r;->c:LX/9aR;

    .line 356229
    iget-object v0, p0, LX/20r;->c:LX/9aR;

    new-instance v1, LX/9Ar;

    invoke-direct {v1, p0}, LX/9Ar;-><init>(LX/20r;)V

    invoke-virtual {v0, v1}, LX/9aR;->setListener(LX/9Aq;)V

    .line 356230
    :cond_0
    iget-object v0, p0, LX/20r;->c:LX/9aR;

    invoke-virtual {v0}, LX/9aR;->a()V

    .line 356231
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 356232
    iget-object v0, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/20r;->a:LX/0tH;

    .line 356233
    iget-object v2, v0, LX/0tH;->c:LX/0Uh;

    const/16 v3, 0x4b0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 356234
    if-nez v0, :cond_2

    .line 356235
    :cond_1
    :goto_0
    return-void

    .line 356236
    :cond_2
    iget-object v0, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    if-nez v0, :cond_3

    .line 356237
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 356238
    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 356239
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 356240
    :cond_3
    :goto_1
    iget-object v0, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 356241
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 356242
    iget-object v1, p0, LX/20r;->d:LX/9aS;

    if-eqz v1, :cond_6

    .line 356243
    :goto_2
    invoke-static {p0, v0}, LX/20r;->c(LX/20r;Landroid/content/Context;)V

    .line 356244
    const/4 v1, 0x0

    :goto_3
    const/16 v2, 0x14

    if-ge v1, v2, :cond_4

    .line 356245
    iget-object v2, p0, LX/20r;->c:LX/9aR;

    iget-object v3, p0, LX/20r;->d:LX/9aS;

    invoke-virtual {v2, v3}, LX/9aR;->a(LX/9aS;)V

    .line 356246
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 356247
    :cond_4
    goto :goto_0

    .line 356248
    :cond_5
    invoke-static {p0, v0}, LX/20r;->c(LX/20r;Landroid/content/Context;)V

    .line 356249
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, LX/20r;->c:LX/9aR;

    invoke-direct {v2, v3, v5, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    .line 356250
    iget-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 356251
    iget-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 356252
    iget-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 356253
    iget-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356254
    iget-object v2, p0, LX/20r;->b:Landroid/widget/PopupWindow;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    goto :goto_1

    .line 356255
    :cond_6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 356256
    const v2, 0x7f0b0fea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 356257
    const v3, 0x7f0b0feb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 356258
    const v4, 0x7f0b0fec

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 356259
    const v5, 0x7f0b0fed

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 356260
    const v6, 0x7f0b0fee

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 356261
    const v7, 0x7f0b0fef

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 356262
    const p1, 0x7f0215df

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 356263
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 356264
    new-instance p1, LX/9aS;

    invoke-direct {p1, v1}, LX/9aS;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v1, LX/9aW;

    invoke-direct {v1, v4, v5}, LX/9aW;-><init>(FF)V

    .line 356265
    iput-object v1, p1, LX/9aS;->d:LX/9aT;

    .line 356266
    move-object v1, p1

    .line 356267
    new-instance v4, LX/9aW;

    invoke-direct {v4, v6, v7}, LX/9aW;-><init>(FF)V

    .line 356268
    iput-object v4, v1, LX/9aS;->e:LX/9aT;

    .line 356269
    move-object v1, v1

    .line 356270
    new-instance v4, LX/9aW;

    invoke-direct {v4, v2, v3}, LX/9aW;-><init>(FF)V

    .line 356271
    iput-object v4, v1, LX/9aS;->g:LX/9aT;

    .line 356272
    move-object v1, v1

    .line 356273
    new-instance v2, LX/9aV;

    const/high16 v3, -0x3e600000    # -20.0f

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-direct {v2, v3, v4}, LX/9aV;-><init>(FF)V

    .line 356274
    iput-object v2, v1, LX/9aS;->i:LX/9aT;

    .line 356275
    move-object v1, v1

    .line 356276
    sget-object v2, LX/9aW;->b:LX/9aW;

    .line 356277
    iput-object v2, v1, LX/9aS;->h:LX/9aT;

    .line 356278
    move-object v1, v1

    .line 356279
    new-instance v2, LX/9aW;

    const v3, 0x3f19999a    # 0.6f

    const/high16 v4, 0x3f400000    # 0.75f

    invoke-direct {v2, v3, v4}, LX/9aW;-><init>(FF)V

    .line 356280
    iput-object v2, v1, LX/9aS;->j:LX/9aT;

    .line 356281
    move-object v1, v1

    .line 356282
    iput-object v1, p0, LX/20r;->d:LX/9aS;

    goto/16 :goto_2
.end method
