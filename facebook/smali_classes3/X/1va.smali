.class public final LX/1va;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1kR;


# direct methods
.method public constructor <init>(LX/1kR;)V
    .locals 0

    .prologue
    .line 343212
    iput-object p1, p0, LX/1va;->a:LX/1kR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 343206
    iget-object v0, p0, LX/1va;->a:LX/1kR;

    iget-object v0, v0, LX/1kR;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    .line 343207
    iget-object v0, p0, LX/1va;->a:LX/1kR;

    iget-object v0, v0, LX/1kR;->d:LX/0tX;

    iget-object v1, p0, LX/1va;->a:LX/1kR;

    .line 343208
    invoke-static {v1, v2, v3}, LX/1kR;->b(LX/1kR;J)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->d:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    move-object v1, v4

    .line 343209
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    const v1, 0x5d310a2b

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 343210
    iget-object v1, p0, LX/1va;->a:LX/1kR;

    iget-object v1, v1, LX/1kR;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v4, LX/1kp;->b:LX/0Tn;

    invoke-interface {v1, v4, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 343211
    return-object v0
.end method
