.class public LX/20n;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1EN;

.field private final b:LX/0bH;


# direct methods
.method public constructor <init>(LX/1EN;LX/0bH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356036
    iput-object p1, p0, LX/20n;->a:LX/1EN;

    .line 356037
    iput-object p2, p0, LX/20n;->b:LX/0bH;

    .line 356038
    return-void
.end method

.method public static a(LX/0QB;)LX/20n;
    .locals 5

    .prologue
    .line 356039
    const-class v1, LX/20n;

    monitor-enter v1

    .line 356040
    :try_start_0
    sget-object v0, LX/20n;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356041
    sput-object v2, LX/20n;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 356044
    new-instance p0, LX/20n;

    invoke-static {v0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v3

    check-cast v3, LX/1EN;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-direct {p0, v3, v4}, LX/20n;-><init>(LX/1EN;LX/0bH;)V

    .line 356045
    move-object v0, p0

    .line 356046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 356050
    if-nez p1, :cond_0

    .line 356051
    :goto_0
    return-void

    .line 356052
    :cond_0
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356053
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356054
    if-eqz p4, :cond_1

    .line 356055
    iget-object v1, p0, LX/20n;->a:LX/1EN;

    sget-object v2, LX/An0;->FOOTER:LX/An0;

    invoke-virtual {v1, p2, p3, p1, v2}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/view/View;LX/An0;)V

    .line 356056
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 356057
    invoke-static {p2}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 356058
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 356059
    :goto_1
    iget-object v2, p0, LX/20n;->b:LX/0bH;

    new-instance v3, LX/1Zc;

    invoke-direct {v3, v1, v0}, LX/1Zc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 356060
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
