.class public LX/1xp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xs;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xp",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1xs;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349522
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 349523
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xp;->b:LX/0Zi;

    .line 349524
    iput-object p1, p0, LX/1xp;->a:LX/0Ot;

    .line 349525
    return-void
.end method

.method public static a(LX/0QB;)LX/1xp;
    .locals 4

    .prologue
    .line 349511
    const-class v1, LX/1xp;

    monitor-enter v1

    .line 349512
    :try_start_0
    sget-object v0, LX/1xp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349513
    sput-object v2, LX/1xp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349514
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349515
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349516
    new-instance v3, LX/1xp;

    const/16 p0, 0x738

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xp;-><init>(LX/0Ot;)V

    .line 349517
    move-object v0, v3

    .line 349518
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349519
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349520
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349521
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 349508
    check-cast p2, LX/1xr;

    .line 349509
    iget-object v0, p0, LX/1xp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xs;

    iget-object v2, p2, LX/1xr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/1xr;->b:LX/1Po;

    iget-boolean v4, p2, LX/1xr;->c:Z

    iget v5, p2, LX/1xr;->d:I

    iget v6, p2, LX/1xr;->e:I

    iget v7, p2, LX/1xr;->f:I

    iget v8, p2, LX/1xr;->g:F

    iget v9, p2, LX/1xr;->h:F

    iget v10, p2, LX/1xr;->i:F

    iget v11, p2, LX/1xr;->j:I

    iget-boolean v12, p2, LX/1xr;->k:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v12}, LX/1xs;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;ZIIIFFFIZ)LX/1Dg;

    move-result-object v0

    .line 349510
    return-object v0
.end method

.method public final a(LX/1De;II)LX/1xt;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II)",
            "LX/1xp",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 349501
    new-instance v0, LX/1xr;

    invoke-direct {v0, p0}, LX/1xr;-><init>(LX/1xp;)V

    .line 349502
    iget-object v1, p0, LX/1xp;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1xt;

    .line 349503
    if-nez v1, :cond_0

    .line 349504
    new-instance v1, LX/1xt;

    invoke-direct {v1, p0}, LX/1xt;-><init>(LX/1xp;)V

    .line 349505
    :cond_0
    invoke-static {v1, p1, p2, p3, v0}, LX/1xt;->a$redex0(LX/1xt;LX/1De;IILX/1xr;)V

    .line 349506
    move-object v0, v1

    .line 349507
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349499
    invoke-static {}, LX/1dS;->b()V

    .line 349500
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/1xt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xp",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 349431
    invoke-virtual {p0, p1, v0, v0}, LX/1xp;->a(LX/1De;II)LX/1xt;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 349432
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 349433
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 349434
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 349435
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 349436
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 349437
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 349438
    iget-object v0, p0, LX/1xp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v0, p1

    const/4 p1, 0x0

    const/4 v8, 0x0

    .line 349439
    sget-object v7, LX/03r;->HeaderTitleComponent:[I

    invoke-virtual {v0, v7, v8}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 349440
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v10

    move v7, v8

    :goto_0
    if-ge v7, v10, :cond_6

    .line 349441
    invoke-virtual {v9, v7}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v11

    .line 349442
    const/16 p0, 0x5

    if-ne v11, p0, :cond_1

    .line 349443
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 349444
    iput-object v11, v1, LX/1np;->a:Ljava/lang/Object;

    .line 349445
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 349446
    :cond_1
    const/16 p0, 0x6

    if-ne v11, p0, :cond_2

    .line 349447
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 349448
    iput-object v11, v2, LX/1np;->a:Ljava/lang/Object;

    .line 349449
    goto :goto_1

    .line 349450
    :cond_2
    const/16 p0, 0x2

    if-ne v11, p0, :cond_3

    .line 349451
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 349452
    iput-object v11, v4, LX/1np;->a:Ljava/lang/Object;

    .line 349453
    goto :goto_1

    .line 349454
    :cond_3
    const/16 p0, 0x3

    if-ne v11, p0, :cond_4

    .line 349455
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 349456
    iput-object v11, v5, LX/1np;->a:Ljava/lang/Object;

    .line 349457
    goto :goto_1

    .line 349458
    :cond_4
    const/16 p0, 0x4

    if-ne v11, p0, :cond_5

    .line 349459
    invoke-virtual {v9, v11, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 349460
    iput-object v11, v6, LX/1np;->a:Ljava/lang/Object;

    .line 349461
    goto :goto_1

    .line 349462
    :cond_5
    const/16 p0, 0x1

    if-ne v11, p0, :cond_0

    .line 349463
    invoke-virtual {v9, v11, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 349464
    iput-object v11, v3, LX/1np;->a:Ljava/lang/Object;

    .line 349465
    goto :goto_1

    .line 349466
    :cond_6
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 349467
    check-cast p2, LX/1xr;

    .line 349468
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349469
    if-eqz v0, :cond_7

    .line 349470
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349471
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1xr;->d:I

    .line 349472
    :cond_7
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 349473
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349474
    if-eqz v0, :cond_8

    .line 349475
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349476
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1xr;->e:I

    .line 349477
    :cond_8
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 349478
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349479
    if-eqz v0, :cond_9

    .line 349480
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349481
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p2, LX/1xr;->f:I

    .line 349482
    :cond_9
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 349483
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349484
    if-eqz v0, :cond_a

    .line 349485
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349486
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1xr;->g:F

    .line 349487
    :cond_a
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 349488
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349489
    if-eqz v0, :cond_b

    .line 349490
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349491
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1xr;->h:F

    .line 349492
    :cond_b
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 349493
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349494
    if-eqz v0, :cond_c

    .line 349495
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 349496
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, LX/1xr;->i:F

    .line 349497
    :cond_c
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 349498
    return-void
.end method
