.class public LX/20K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/20F;

.field private static s:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0hL;

.field public final e:LX/0hB;

.field public final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final g:Landroid/view/accessibility/AccessibilityManager;

.field private final h:LX/0tH;

.field private final i:F

.field public final j:I

.field private final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public m:LX/9BY;

.field public n:Landroid/widget/PopupWindow;

.field public o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<[F>;"
        }
    .end annotation
.end field

.field public p:LX/20M;

.field private q:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/20F;",
            ">;"
        }
    .end annotation
.end field

.field public r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354683
    new-instance v0, LX/20L;

    invoke-direct {v0}, LX/20L;-><init>()V

    sput-object v0, LX/20K;->a:LX/20F;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0hL;LX/0hB;Lcom/facebook/quicklog/QuickPerformanceLogger;Landroid/view/accessibility/AccessibilityManager;LX/0tH;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0hL;",
            "LX/0hB;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "Landroid/view/accessibility/AccessibilityManager;",
            "LX/0tH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354685
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/20K;->o:LX/0am;

    .line 354686
    sget-object v0, LX/20M;->NONE:LX/20M;

    iput-object v0, p0, LX/20K;->p:LX/20M;

    .line 354687
    const/4 v0, -0x1

    iput v0, p0, LX/20K;->r:I

    .line 354688
    iput-object p1, p0, LX/20K;->b:Landroid/content/Context;

    .line 354689
    iput-object p2, p0, LX/20K;->c:LX/0Ot;

    .line 354690
    iput-object p3, p0, LX/20K;->d:LX/0hL;

    .line 354691
    iput-object p4, p0, LX/20K;->e:LX/0hB;

    .line 354692
    iput-object p5, p0, LX/20K;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 354693
    iput-object p6, p0, LX/20K;->g:Landroid/view/accessibility/AccessibilityManager;

    .line 354694
    iput-object p7, p0, LX/20K;->h:LX/0tH;

    .line 354695
    iget-object v0, p0, LX/20K;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 354696
    const v1, 0x7f0b0fd5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/20K;->j:I

    .line 354697
    const v1, 0x7f0b0fd6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LX/20K;->i:F

    .line 354698
    const v1, 0x7f081998

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/20K;->k:Ljava/lang/String;

    .line 354699
    const v1, 0x7f081996

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/20K;->l:Ljava/lang/String;

    .line 354700
    return-void
.end method

.method public static a(LX/0QB;)LX/20K;
    .locals 11

    .prologue
    .line 354701
    const-class v1, LX/20K;

    monitor-enter v1

    .line 354702
    :try_start_0
    sget-object v0, LX/20K;->s:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 354703
    sput-object v2, LX/20K;->s:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 354704
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354705
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 354706
    new-instance v3, LX/20K;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x3567

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v6

    check-cast v6, LX/0hL;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0sY;->b(LX/0QB;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v9

    check-cast v9, Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v10

    check-cast v10, LX/0tH;

    invoke-direct/range {v3 .. v10}, LX/20K;-><init>(Landroid/content/Context;LX/0Ot;LX/0hL;LX/0hB;Lcom/facebook/quicklog/QuickPerformanceLogger;Landroid/view/accessibility/AccessibilityManager;LX/0tH;)V

    .line 354707
    move-object v0, v3

    .line 354708
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 354709
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354710
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 354711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1zt;)V
    .locals 4

    .prologue
    .line 354712
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    sget-object v1, LX/20K;->a:LX/20F;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/1zt;->c:LX/1zt;

    if-ne p1, v0, :cond_1

    .line 354713
    :cond_0
    :goto_0
    return-void

    .line 354714
    :cond_1
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    .line 354715
    iget-object v1, v0, LX/20z;->b:LX/0if;

    sget-object v2, LX/20z;->a:LX/0ih;

    const-string v3, "reaction_selected"

    .line 354716
    iget p0, p1, LX/1zt;->e:I

    move p0, p0

    .line 354717
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, v3, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 354718
    goto :goto_0
.end method

.method public static a(LX/20K;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 354719
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 354720
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0, p1}, LX/9BY;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 354721
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 354722
    if-eqz p1, :cond_0

    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getSupportedReactions()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354723
    :cond_0
    :goto_0
    return-void

    .line 354724
    :cond_1
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    if-nez v0, :cond_2

    .line 354725
    iget-object v0, p0, LX/20K;->b:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 354726
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 354727
    :cond_2
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    if-nez v0, :cond_7

    .line 354728
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 354729
    new-instance v0, LX/9Be;

    iget-object v1, p0, LX/20K;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/9Be;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354730
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/9BY;->measure(II)V

    .line 354731
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354732
    iput-object p0, v0, LX/9BY;->G:LX/20K;

    .line 354733
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, LX/20K;->m:LX/9BY;

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v4}, LX/9BY;->getMeasuredHeight()I

    move-result v4

    invoke-direct {v0, v1, v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    .line 354734
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 354735
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 354736
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 354737
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 354738
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    new-instance v1, LX/9BV;

    invoke-direct {v1, p0}, LX/9BV;-><init>(LX/20K;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 354739
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 354740
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    new-instance v1, LX/9BU;

    invoke-direct {v1, p0}, LX/9BU;-><init>(LX/20K;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 354741
    iget-object v0, p0, LX/20K;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820003

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 354742
    :goto_1
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354743
    :cond_3
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 354744
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0, v2}, LX/20F;->a(Z)V

    .line 354745
    sget-object v0, LX/20M;->NONE:LX/20M;

    .line 354746
    iput-object v0, p0, LX/20K;->p:LX/20M;

    .line 354747
    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 354748
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getSupportedReactions()LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    .line 354749
    iget-object v1, p0, LX/20K;->m:LX/9BY;

    .line 354750
    if-nez v0, :cond_c

    .line 354751
    :goto_2
    iget-object v1, p0, LX/20K;->m:LX/9BY;

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v3, v4}, LX/9BY;->measure(II)V

    .line 354752
    new-array v3, v7, [I

    .line 354753
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 354754
    new-array v4, v7, [I

    .line 354755
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 354756
    aget v0, v3, v6

    iget-object v1, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v1}, LX/9BY;->getMeasuredHeight()I

    move-result v1

    if-ge v0, v1, :cond_8

    sget-object v0, LX/210;->BELOW_FOOTER:LX/210;

    move-object v1, v0

    .line 354757
    :goto_3
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    .line 354758
    iput-object v1, v0, LX/20z;->i:LX/210;

    .line 354759
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v5

    invoke-interface {v5}, LX/20F;->getDockTheme()LX/20I;

    move-result-object v5

    iget v5, v5, LX/20I;->backgroundColor:I

    .line 354760
    const/4 v8, -0x1

    if-ne v5, v8, :cond_10

    .line 354761
    iget-object v8, v0, LX/9BY;->C:Landroid/graphics/drawable/Drawable;

    iput-object v8, v0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    .line 354762
    :goto_4
    aget v5, v4, v6

    sget-object v0, LX/210;->ABOVE_FOOTER:LX/210;

    if-ne v1, v0, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v6, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v6}, LX/9BY;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v0, v6

    :goto_5
    add-int/2addr v5, v0

    .line 354763
    aget v0, v3, v2

    aget v3, v4, v2

    sub-int v3, v0, v3

    .line 354764
    iget-object v0, p0, LX/20K;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    float-to-int v0, v0

    iget v4, p0, LX/20K;->j:I

    sub-int/2addr v0, v4

    sub-int/2addr v0, v3

    .line 354765
    iget-object v3, p0, LX/20K;->d:LX/0hL;

    invoke-virtual {v3}, LX/0hL;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 354766
    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, LX/20K;->j:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    sub-int/2addr v0, v3

    .line 354767
    :cond_4
    iget-object v3, p0, LX/20K;->e:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    .line 354768
    iget-object v4, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v4}, LX/9BY;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    if-lt v4, v3, :cond_5

    .line 354769
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v3, v0

    iget v3, p0, LX/20K;->j:I

    sub-int/2addr v0, v3

    .line 354770
    :cond_5
    if-gez v0, :cond_a

    .line 354771
    :goto_6
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 354772
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v4}, LX/9BY;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v2, v5, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 354773
    :goto_7
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354774
    iput-object v1, v0, LX/9BY;->H:LX/210;

    .line 354775
    invoke-virtual {v0}, LX/9BY;->d()V

    .line 354776
    iget-object v0, p0, LX/20K;->l:Ljava/lang/String;

    invoke-static {p0, v0}, LX/20K;->a(LX/20K;Ljava/lang/CharSequence;)V

    .line 354777
    iget-object v0, p0, LX/20K;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820003

    invoke-interface {v0, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 354778
    iget-object v0, p0, LX/20K;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820004

    invoke-interface {v0, v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 354779
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    sget-object v1, LX/20K;->a:LX/20F;

    if-eq v0, v1, :cond_0

    .line 354780
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    .line 354781
    iget-wide v7, v0, LX/20z;->h:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_13

    const/4 v7, 0x1

    :goto_8
    move v3, v7

    .line 354782
    if-nez v3, :cond_6

    .line 354783
    iget-object v3, v0, LX/20z;->d:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iput-wide v3, v0, LX/20z;->h:J

    .line 354784
    iget-object v3, v0, LX/20z;->b:LX/0if;

    sget-object v4, LX/20z;->a:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 354785
    iget-object v3, v0, LX/20z;->b:LX/0if;

    sget-object v4, LX/20z;->a:LX/0ih;

    iget-object v5, v0, LX/20z;->i:LX/210;

    invoke-virtual {v5}, LX/210;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 354786
    iget-object v3, v0, LX/20z;->b:LX/0if;

    sget-object v4, LX/20z;->a:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "initial_reaction"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, LX/20z;->l:LX/1zt;

    .line 354787
    iget v7, v6, LX/1zt;->e:I

    move v6, v7

    .line 354788
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 354789
    iget-object v3, v0, LX/20z;->b:LX/0if;

    sget-object v4, LX/20z;->a:LX/0ih;

    iget-object v5, v0, LX/20z;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 354790
    :cond_6
    goto/16 :goto_0

    .line 354791
    :cond_7
    iget-object v0, p0, LX/20K;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x820004

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto/16 :goto_1

    .line 354792
    :cond_8
    sget-object v0, LX/210;->ABOVE_FOOTER:LX/210;

    move-object v1, v0

    goto/16 :goto_3

    :cond_9
    move v0, v2

    .line 354793
    goto/16 :goto_5

    :cond_a
    move v2, v0

    .line 354794
    goto/16 :goto_6

    .line 354795
    :cond_b
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 354796
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 354797
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    const/16 v3, 0x33

    invoke-virtual {v0, p1, v3, v2, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto/16 :goto_7

    .line 354798
    :cond_c
    iget-object v3, v1, LX/9BY;->a:LX/0hL;

    invoke-virtual {v3}, LX/0hL;->a()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 354799
    invoke-static {v0}, LX/0R9;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 354800
    :cond_d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/high16 v11, 0x40100000    # 2.25f

    .line 354801
    iget v5, v1, LX/9BY;->J:I

    if-ne v5, v3, :cond_e

    .line 354802
    :goto_9
    invoke-virtual {v1, v0}, LX/9BY;->setupReactionsImpl(Ljava/util/List;)V

    goto/16 :goto_2

    .line 354803
    :cond_e
    iget-object v5, v1, LX/9BY;->c:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    .line 354804
    iget v8, v1, LX/9BY;->h:I

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v9, v3, -0x1

    iget v10, v1, LX/9BY;->i:I

    mul-int/2addr v9, v10

    add-int/2addr v8, v9

    add-int/lit8 v9, v3, -0x1

    iget v10, v1, LX/9BY;->g:I

    mul-int/2addr v9, v10

    add-int/2addr v8, v9

    iget v9, v1, LX/9BY;->g:I

    int-to-float v9, v9

    mul-float/2addr v9, v11

    float-to-int v9, v9

    add-int/2addr v8, v9

    iget-object v9, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v9

    iget-object v9, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v9

    iget v9, v1, LX/9BY;->j:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    .line 354805
    if-lt v8, v5, :cond_f

    .line 354806
    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v8

    iget v8, v1, LX/9BY;->j:I

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v5, v8

    .line 354807
    int-to-float v8, v5

    const v9, 0x3f5c0ebf    # 0.8596f

    mul-float/2addr v8, v9

    int-to-float v9, v3

    div-float/2addr v8, v9

    float-to-int v8, v8

    iget v9, v1, LX/9BY;->g:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v1, LX/9BY;->r:I

    .line 354808
    iget v8, v1, LX/9BY;->r:I

    mul-int/2addr v8, v3

    sub-int/2addr v5, v8

    add-int/lit8 v8, v3, 0x1

    div-int/2addr v5, v8

    iget v8, v1, LX/9BY;->i:I

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v1, LX/9BY;->t:I

    .line 354809
    iget v5, v1, LX/9BY;->t:I

    iget v8, v1, LX/9BY;->h:I

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v1, LX/9BY;->s:I

    .line 354810
    :goto_a
    iget v5, v1, LX/9BY;->r:I

    mul-int/2addr v5, v3

    int-to-float v5, v5

    iget v8, v1, LX/9BY;->r:I

    int-to-float v8, v8

    mul-float/2addr v8, v11

    sub-float/2addr v5, v8

    add-int/lit8 v8, v3, -0x1

    int-to-float v8, v8

    div-float/2addr v5, v8

    .line 354811
    iget v8, v1, LX/9BY;->r:I

    int-to-float v8, v8

    div-float/2addr v5, v8

    iput v5, v1, LX/9BY;->z:F

    .line 354812
    const/high16 v5, 0x3f800000    # 1.0f

    iget v8, v1, LX/9BY;->z:F

    sub-float/2addr v5, v8

    iget v8, v1, LX/9BY;->z:F

    sub-float v8, v11, v8

    div-float/2addr v5, v8

    iput v5, v1, LX/9BY;->A:F

    .line 354813
    const/high16 v5, 0x40000000    # 2.0f

    iget v8, v1, LX/9BY;->t:I

    int-to-float v8, v8

    mul-float/2addr v5, v8

    iget v8, v1, LX/9BY;->r:I

    int-to-float v8, v8

    div-float/2addr v5, v8

    add-float/2addr v5, v11

    iput v5, v1, LX/9BY;->B:F

    .line 354814
    iget v5, v1, LX/9BY;->r:I

    int-to-float v5, v5

    mul-float/2addr v5, v11

    const/high16 v8, 0x3fc00000    # 1.5f

    mul-float/2addr v5, v8

    float-to-int v5, v5

    iput v5, v1, LX/9BY;->u:I

    .line 354815
    iget v5, v1, LX/9BY;->s:I

    mul-int/lit8 v5, v5, 0x2

    iget v8, v1, LX/9BY;->r:I

    mul-int/2addr v8, v3

    add-int/2addr v5, v8

    add-int/lit8 v8, v3, -0x1

    iget v9, v1, LX/9BY;->t:I

    mul-int/2addr v8, v9

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v8

    iput v5, v1, LX/9BY;->v:I

    .line 354816
    iget v5, v1, LX/9BY;->v:I

    iput v5, v1, LX/9BY;->w:I

    .line 354817
    iget v5, v1, LX/9BY;->s:I

    mul-int/lit8 v5, v5, 0x2

    iget v8, v1, LX/9BY;->r:I

    int-to-float v8, v8

    iget v9, v1, LX/9BY;->z:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v8

    iput v5, v1, LX/9BY;->y:I

    .line 354818
    iget v5, v1, LX/9BY;->s:I

    mul-int/lit8 v5, v5, 0x2

    iget v8, v1, LX/9BY;->r:I

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v8

    iget-object v8, v1, LX/9BY;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v8

    iput v5, v1, LX/9BY;->x:I

    .line 354819
    iput v3, v1, LX/9BY;->J:I

    .line 354820
    invoke-virtual {v1}, LX/9BY;->requestLayout()V

    goto/16 :goto_9

    .line 354821
    :cond_f
    iget v5, v1, LX/9BY;->g:I

    iput v5, v1, LX/9BY;->r:I

    .line 354822
    iget v5, v1, LX/9BY;->h:I

    iput v5, v1, LX/9BY;->s:I

    .line 354823
    iget v5, v1, LX/9BY;->i:I

    iput v5, v1, LX/9BY;->t:I

    goto/16 :goto_a

    .line 354824
    :cond_10
    iget v8, v0, LX/9BY;->D:I

    if-eq v8, v5, :cond_12

    .line 354825
    iput v5, v0, LX/9BY;->D:I

    .line 354826
    iget-object v8, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    if-nez v8, :cond_11

    .line 354827
    iget-object v8, v0, LX/9BY;->C:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    .line 354828
    iget-object v8, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 354829
    :cond_11
    iget-object v8, v0, LX/9BY;->b:LX/0wM;

    iget-object v9, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, v9, v5}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    .line 354830
    :cond_12
    iget-object v8, v0, LX/9BY;->F:Landroid/graphics/drawable/Drawable;

    iput-object v8, v0, LX/9BY;->E:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_4

    :cond_13
    const/4 v7, 0x0

    goto/16 :goto_8
.end method

.method public static a$redex0(LX/20K;Landroid/view/View;Landroid/view/MotionEvent;Z)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 354561
    iget v0, p0, LX/20K;->r:I

    if-eq v0, v4, :cond_0

    iget v0, p0, LX/20K;->r:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    if-eq v0, v3, :cond_0

    .line 354562
    :goto_0
    return-void

    .line 354563
    :cond_0
    iget v0, p0, LX/20K;->r:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    .line 354564
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/20K;->r:I

    .line 354565
    :cond_2
    iget-object v0, p0, LX/20K;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_3

    .line 354566
    const/4 v0, 0x2

    new-array v0, v0, [F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    aput v3, v0, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    aput v3, v0, v1

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/20K;->o:LX/0am;

    .line 354567
    :cond_3
    invoke-direct {p0, p1}, LX/20K;->a(Landroid/view/View;)V

    .line 354568
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/20K;->m:LX/9BY;

    if-nez v0, :cond_5

    .line 354569
    :cond_4
    iput v4, p0, LX/20K;->r:I

    .line 354570
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/20K;->o:LX/0am;

    goto :goto_0

    .line 354571
    :cond_5
    iget-object v0, p0, LX/20K;->p:LX/20M;

    sget-object v3, LX/20M;->NONE:LX/20M;

    if-ne v0, v3, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v1, :cond_6

    .line 354572
    sget-object v0, LX/20M;->LINGER:LX/20M;

    .line 354573
    iput-object v0, p0, LX/20K;->p:LX/20M;

    .line 354574
    sget-object v0, LX/20M;->LINGER:LX/20M;

    invoke-direct {p0, v0}, LX/20K;->b(LX/20M;)V

    .line 354575
    :cond_6
    iget-object v0, p0, LX/20K;->p:LX/20M;

    sget-object v3, LX/20M;->DRAG:LX/20M;

    if-eq v0, v3, :cond_8

    if-nez p3, :cond_7

    iget-object v0, p0, LX/20K;->g:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0, p2}, LX/20K;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 354576
    :cond_7
    sget-object v0, LX/20M;->DRAG:LX/20M;

    .line 354577
    iput-object v0, p0, LX/20K;->p:LX/20M;

    .line 354578
    sget-object v0, LX/20M;->DRAG:LX/20M;

    invoke-direct {p0, v0}, LX/20K;->b(LX/20M;)V

    .line 354579
    :cond_8
    iget-object v0, p0, LX/20K;->p:LX/20M;

    sget-object v3, LX/20M;->DRAG:LX/20M;

    if-ne v0, v3, :cond_f

    .line 354580
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354581
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_10

    .line 354582
    invoke-virtual {v0}, LX/9BY;->c()V

    .line 354583
    :goto_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 354584
    :cond_9
    :goto_2
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v3

    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v0

    sget-object v4, LX/1zt;->c:LX/1zt;

    if-ne v0, v4, :cond_e

    move v0, v1

    :goto_3
    invoke-interface {v3, v0}, LX/20F;->b(Z)V

    .line 354585
    :cond_a
    :goto_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_b

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_c

    .line 354586
    :cond_b
    const/4 v0, -0x1

    iput v0, p0, LX/20K;->r:I

    .line 354587
    :cond_c
    goto/16 :goto_0

    .line 354588
    :pswitch_0
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    sget-object v3, LX/20K;->a:LX/20F;

    if-eq v0, v3, :cond_9

    .line 354589
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v3

    .line 354590
    iget-object v4, v0, LX/20z;->n:LX/1zt;

    if-ne v3, v4, :cond_11

    .line 354591
    :goto_5
    goto :goto_2

    .line 354592
    :pswitch_1
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v0

    invoke-direct {p0, v0}, LX/20K;->a(LX/1zt;)V

    .line 354593
    invoke-virtual {p0}, LX/20K;->b()V

    .line 354594
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    iget-object v3, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v3}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v3

    invoke-interface {v0, p1, v3}, LX/20F;->a(Landroid/view/View;LX/1zt;)V

    .line 354595
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v0

    sget-object v3, LX/1zt;->c:LX/1zt;

    if-ne v0, v3, :cond_d

    .line 354596
    iget-object v0, p0, LX/20K;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v3, "reactions_cancel"

    invoke-virtual {v0, v3}, LX/3RX;->a(Ljava/lang/String;)V

    .line 354597
    iget-object v0, p0, LX/20K;->k:Ljava/lang/String;

    invoke-static {p0, v0}, LX/20K;->a(LX/20K;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 354598
    :cond_d
    iget-object v0, p0, LX/20K;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f081997

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v5}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v5

    .line 354599
    iget-object p1, v5, LX/1zt;->f:Ljava/lang/String;

    move-object v5, p1

    .line 354600
    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 354601
    invoke-static {p0, v0}, LX/20K;->a(LX/20K;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 354602
    :pswitch_2
    invoke-virtual {p0}, LX/20K;->b()V

    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 354603
    goto/16 :goto_3

    .line 354604
    :cond_f
    iget-object v0, p0, LX/20K;->p:LX/20M;

    sget-object v1, LX/20M;->LINGER:LX/20M;

    if-ne v0, v1, :cond_a

    .line 354605
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0, v2}, LX/20F;->b(Z)V

    goto/16 :goto_4

    .line 354606
    :cond_10
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 354607
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    .line 354608
    invoke-virtual {v0, v3, v4}, LX/9BY;->a(FF)V

    goto/16 :goto_1

    .line 354609
    :cond_11
    iput-object v3, v0, LX/20z;->n:LX/1zt;

    .line 354610
    iget-object v4, v0, LX/20z;->b:LX/0if;

    sget-object v5, LX/20z;->a:LX/0ih;

    const-string p1, "reaction_hover"

    .line 354611
    iget p3, v3, LX/1zt;->e:I

    move p3, p3

    .line 354612
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v4, v5, p1, p3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b(LX/20M;)V
    .locals 3

    .prologue
    .line 354675
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    sget-object v1, LX/20K;->a:LX/20F;

    if-eq v0, v1, :cond_0

    .line 354676
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    .line 354677
    sget-object v1, LX/20M;->NONE:LX/20M;

    if-eq p1, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 354678
    iget-object v1, v0, LX/20z;->b:LX/0if;

    sget-object v2, LX/20z;->a:LX/0ih;

    invoke-virtual {p1}, LX/20M;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 354679
    iget-object v1, v0, LX/20z;->k:LX/20M;

    sget-object v2, LX/20M;->NONE:LX/20M;

    if-ne v1, v2, :cond_0

    .line 354680
    iput-object p1, v0, LX/20z;->k:LX/20M;

    .line 354681
    :cond_0
    return-void

    .line 354682
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private c(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 354672
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    iget-object v0, p0, LX/20K;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 354673
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    iget-object v0, p0, LX/20K;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v1

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 354674
    iget v4, p0, LX/20K;->i:F

    float-to-double v4, v4

    float-to-double v6, v3

    float-to-double v8, v0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    cmpg-double v0, v4, v6

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static d(LX/20K;)LX/20F;
    .locals 1

    .prologue
    .line 354669
    iget-object v0, p0, LX/20K;->q:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 354670
    iget-object v0, p0, LX/20K;->q:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20F;

    .line 354671
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/20K;->a:LX/20F;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 354665
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    if-eqz v0, :cond_0

    .line 354666
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->c()V

    .line 354667
    :cond_0
    invoke-virtual {p0}, LX/20K;->b()V

    .line 354668
    return-void
.end method

.method public final a(LX/20F;)V
    .locals 1

    .prologue
    .line 354663
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/20K;->q:Ljava/lang/ref/WeakReference;

    .line 354664
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 354661
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/20K;->a$redex0(LX/20K;Landroid/view/View;Landroid/view/MotionEvent;Z)V

    .line 354662
    return-void
.end method

.method public final b()V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 354621
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/20K;->o:LX/0am;

    .line 354622
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354623
    :cond_0
    :goto_0
    return-void

    .line 354624
    :cond_1
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 354625
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->update()V

    .line 354626
    sget-object v0, LX/20M;->NONE:LX/20M;

    .line 354627
    iput-object v0, p0, LX/20K;->p:LX/20M;

    .line 354628
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354629
    iget-boolean v1, v0, LX/9BY;->I:Z

    move v0, v1

    .line 354630
    if-eqz v0, :cond_0

    .line 354631
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    sget-object v1, LX/20K;->a:LX/20F;

    if-eq v0, v1, :cond_2

    .line 354632
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0}, LX/20F;->getInteractionLogger()LX/20z;

    move-result-object v0

    iget-object v1, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v1}, LX/9BY;->getCurrentReaction()LX/1zt;

    move-result-object v1

    iget-object v2, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v2}, LX/9BY;->getPointerPosition()LX/211;

    move-result-object v2

    .line 354633
    iput-object v1, v0, LX/20z;->m:LX/1zt;

    .line 354634
    iput-object v2, v0, LX/20z;->j:LX/211;

    .line 354635
    iget-object v8, v0, LX/20z;->g:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 354636
    :goto_1
    iget-object v4, v0, LX/20z;->b:LX/0if;

    sget-object v5, LX/20z;->a:LX/0ih;

    const-string v6, "dock_dismiss"

    invoke-virtual {v2}, LX/211;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 354637
    invoke-static {v0}, LX/20z;->c(LX/20z;)V

    .line 354638
    :cond_2
    iget-object v0, p0, LX/20K;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "reactions_dock_away"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 354639
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    invoke-virtual {v0}, LX/9BY;->e()V

    .line 354640
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    invoke-interface {v0, v3}, LX/20F;->a(Z)V

    goto :goto_0

    .line 354641
    :cond_3
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "feedback_reactions_footer_interaction"

    invoke-direct {v9, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 354642
    const/4 v8, -0x1

    .line 354643
    iget-object v10, v0, LX/20z;->k:LX/20M;

    sget-object v11, LX/20M;->NONE:LX/20M;

    if-eq v10, v11, :cond_4

    .line 354644
    iget-object v8, v0, LX/20z;->k:LX/20M;

    sget-object v10, LX/20M;->LINGER:LX/20M;

    if-ne v8, v10, :cond_5

    const/4 v8, 0x1

    .line 354645
    :cond_4
    :goto_2
    iget-object v10, v0, LX/20z;->f:Ljava/lang/String;

    .line 354646
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 354647
    const-string v10, "story_id"

    iget-object v11, v0, LX/20z;->e:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354648
    const-string v10, "feedback_id"

    iget-object v11, v0, LX/20z;->g:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354649
    const-string v10, "dock_location"

    iget-object v11, v0, LX/20z;->i:LX/210;

    invoke-virtual {v11}, LX/210;->ordinal()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354650
    const-string v10, "persisted"

    invoke-virtual {v9, v10, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354651
    const-string v8, "initial_reaction"

    iget-object v10, v0, LX/20z;->l:LX/1zt;

    .line 354652
    iget v11, v10, LX/1zt;->e:I

    move v10, v11

    .line 354653
    invoke-virtual {v9, v8, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354654
    const-string v8, "final_reaction"

    iget-object v10, v0, LX/20z;->m:LX/1zt;

    .line 354655
    iget v11, v10, LX/1zt;->e:I

    move v10, v11

    .line 354656
    invoke-virtual {v9, v8, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354657
    const-string v8, "dismiss_location"

    iget-object v10, v0, LX/20z;->j:LX/211;

    invoke-virtual {v10}, LX/211;->ordinal()I

    move-result v10

    invoke-virtual {v9, v8, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354658
    const-string v8, "time_spent"

    iget-object v10, v0, LX/20z;->d:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    iget-wide v12, v0, LX/20z;->h:J

    sub-long/2addr v10, v12

    invoke-virtual {v9, v8, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 354659
    iget-object v8, v0, LX/20z;->c:LX/0Zb;

    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_1

    .line 354660
    :cond_5
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 354613
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/20K;->o:LX/0am;

    .line 354614
    iget-object v0, p0, LX/20K;->m:LX/9BY;

    .line 354615
    iget-boolean v1, v0, LX/9BY;->I:Z

    move v0, v1

    .line 354616
    if-nez v0, :cond_0

    .line 354617
    :goto_0
    return-void

    .line 354618
    :cond_0
    iget-object v0, p0, LX/20K;->n:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 354619
    invoke-static {p0}, LX/20K;->d(LX/20K;)LX/20F;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/20F;->a(Z)V

    .line 354620
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/20K;->a(LX/20F;)V

    goto :goto_0
.end method
