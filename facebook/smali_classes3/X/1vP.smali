.class public LX/1vP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "LX/1Me;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "Landroid/os/IBinder$DeathRecipient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 342666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342667
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1vP;->a:Ljava/util/Map;

    .line 342668
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/1vP;->b:Ljava/util/Map;

    .line 342669
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1Me;)V
    .locals 4

    .prologue
    .line 342670
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LX/1Me;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 342671
    iget-object v1, p0, LX/1vP;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 342672
    new-instance v1, LX/2Z2;

    invoke-direct {v1, p0, v0}, LX/2Z2;-><init>(LX/1vP;Landroid/os/IBinder;)V

    .line 342673
    invoke-interface {p1}, LX/1Me;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 342674
    iget-object v2, p0, LX/1vP;->a:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342675
    iget-object v2, p0, LX/1vP;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342676
    :cond_0
    monitor-exit p0

    return-void

    .line 342677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V
    .locals 2

    .prologue
    .line 342678
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1vP;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Me;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342679
    :try_start_1
    invoke-interface {v0, p1}, LX/1Me;->a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 342680
    :catch_0
    goto :goto_0

    .line 342681
    :cond_0
    monitor-exit p0

    return-void

    .line 342682
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1Me;)V
    .locals 3

    .prologue
    .line 342683
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, LX/1Me;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 342684
    iget-object v0, p0, LX/1vP;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder$DeathRecipient;

    .line 342685
    iget-object v2, p0, LX/1vP;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342686
    if-eqz v0, :cond_0

    .line 342687
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342688
    :cond_0
    monitor-exit p0

    return-void

    .line 342689
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
