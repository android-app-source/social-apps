.class public final LX/1xY;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1xT;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1xX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xT",
            "<TE;>.HeaderActorComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1xT;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1xT;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 348684
    iput-object p1, p0, LX/1xY;->b:LX/1xT;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 348685
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1xY;->c:[Ljava/lang/String;

    .line 348686
    iput v3, p0, LX/1xY;->d:I

    .line 348687
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1xY;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1xY;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1xY;LX/1De;IILX/1xX;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1xT",
            "<TE;>.HeaderActorComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 348680
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 348681
    iput-object p4, p0, LX/1xY;->a:LX/1xX;

    .line 348682
    iget-object v0, p0, LX/1xY;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 348683
    return-void
.end method


# virtual methods
.method public final a(LX/1Po;)LX/1xY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1xT",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 348677
    iget-object v0, p0, LX/1xY;->a:LX/1xX;

    iput-object p1, v0, LX/1xX;->b:LX/1Po;

    .line 348678
    iget-object v0, p0, LX/1xY;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 348679
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1xT",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 348660
    iget-object v0, p0, LX/1xY;->a:LX/1xX;

    iput-object p1, v0, LX/1xX;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 348661
    iget-object v0, p0, LX/1xY;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 348662
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 348673
    invoke-super {p0}, LX/1X5;->a()V

    .line 348674
    const/4 v0, 0x0

    iput-object v0, p0, LX/1xY;->a:LX/1xX;

    .line 348675
    iget-object v0, p0, LX/1xY;->b:LX/1xT;

    iget-object v0, v0, LX/1xT;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 348676
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1xT;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348663
    iget-object v1, p0, LX/1xY;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1xY;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1xY;->d:I

    if-ge v1, v2, :cond_2

    .line 348664
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 348665
    :goto_0
    iget v2, p0, LX/1xY;->d:I

    if-ge v0, v2, :cond_1

    .line 348666
    iget-object v2, p0, LX/1xY;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 348667
    iget-object v2, p0, LX/1xY;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348668
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 348669
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348670
    :cond_2
    iget-object v0, p0, LX/1xY;->a:LX/1xX;

    .line 348671
    invoke-virtual {p0}, LX/1xY;->a()V

    .line 348672
    return-object v0
.end method
