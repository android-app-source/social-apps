.class public LX/1wo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/DownloadManager;

.field private final c:LX/1wq;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1sV;

.field private final g:Landroid/os/Handler;

.field private final h:LX/1wh;

.field private final i:LX/1sW;

.field private final j:LX/1sZ;

.field private final k:Landroid/content/SharedPreferences;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final m:I

.field private final n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EeS;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private o:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/DownloadManager;LX/1wq;LX/0Or;LX/0Or;LX/1sV;Landroid/os/Handler;LX/1wh;LX/1sW;LX/1sZ;Landroid/content/SharedPreferences;LX/0Or;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/app/DownloadManager;",
            "LX/1wq;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1sV;",
            "Landroid/os/Handler;",
            "LX/1wh;",
            "LX/1sW;",
            "LX/1sZ;",
            "Landroid/content/SharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/appupdate/ApkDiffPatcher;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 346987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346988
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/1wo;->n:Ljava/util/List;

    .line 346989
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1wo;->o:Z

    .line 346990
    iput-object p1, p0, LX/1wo;->a:Landroid/content/Context;

    .line 346991
    iput-object p2, p0, LX/1wo;->b:Landroid/app/DownloadManager;

    .line 346992
    iput-object p3, p0, LX/1wo;->c:LX/1wq;

    .line 346993
    iput-object p4, p0, LX/1wo;->d:LX/0Or;

    .line 346994
    iput-object p5, p0, LX/1wo;->e:LX/0Or;

    .line 346995
    iput-object p6, p0, LX/1wo;->f:LX/1sV;

    .line 346996
    iput-object p7, p0, LX/1wo;->g:Landroid/os/Handler;

    .line 346997
    iput-object p8, p0, LX/1wo;->h:LX/1wh;

    .line 346998
    iput-object p9, p0, LX/1wo;->i:LX/1sW;

    .line 346999
    iput-object p10, p0, LX/1wo;->j:LX/1sZ;

    .line 347000
    iput-object p11, p0, LX/1wo;->k:Landroid/content/SharedPreferences;

    .line 347001
    iput-object p12, p0, LX/1wo;->l:LX/0Or;

    .line 347002
    iput p13, p0, LX/1wo;->m:I

    .line 347003
    return-void
.end method

.method private static a([B)LX/EeX;
    .locals 3

    .prologue
    .line 347015
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 347016
    const/4 v2, 0x0

    .line 347017
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347018
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 347019
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    :cond_0
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 15

    .prologue
    .line 347004
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wo;->f:LX/1sV;

    invoke-virtual {v0}, LX/1sV;->a()Ljava/util/List;

    move-result-object v0

    .line 347005
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347006
    :try_start_1
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [B

    invoke-static {v1}, LX/1wo;->a([B)LX/EeX;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/InvalidClassException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 347007
    :try_start_2
    new-instance v2, LX/EeU;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, LX/1wo;->f:LX/1sV;

    invoke-direct {v2, v0, v3}, LX/EeU;-><init>(Ljava/lang/Long;LX/1sV;)V

    .line 347008
    new-instance v0, LX/EeS;

    iget-object v3, p0, LX/1wo;->h:LX/1wh;

    iget-object v4, p0, LX/1wo;->k:Landroid/content/SharedPreferences;

    iget-object v5, p0, LX/1wo;->a:Landroid/content/Context;

    iget-object v6, p0, LX/1wo;->b:Landroid/app/DownloadManager;

    iget-object v7, p0, LX/1wo;->c:LX/1wq;

    iget-object v8, p0, LX/1wo;->g:Landroid/os/Handler;

    iget v9, p0, LX/1wo;->m:I

    iget-object v10, p0, LX/1wo;->d:LX/0Or;

    iget-object v11, p0, LX/1wo;->e:LX/0Or;

    iget-object v12, p0, LX/1wo;->l:LX/0Or;

    iget-object v13, p0, LX/1wo;->j:LX/1sZ;

    invoke-direct/range {v0 .. v13}, LX/EeS;-><init>(LX/EeX;LX/EeU;LX/1wh;Landroid/content/SharedPreferences;Landroid/content/Context;Landroid/app/DownloadManager;LX/1wq;Landroid/os/Handler;ILX/0Or;LX/0Or;LX/0Or;LX/1sZ;)V

    .line 347009
    iget-object v1, p0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347010
    iget-object v1, p0, LX/1wo;->i:LX/1sW;

    invoke-virtual {v0, v1}, LX/EeS;->a(LX/1sX;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 347011
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 347012
    :catch_0
    :goto_1
    :try_start_3
    iget-object v1, p0, LX/1wo;->f:LX/1sV;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/1sV;->a(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 347013
    :cond_0
    monitor-exit p0

    return-void

    .line 347014
    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/appupdate/ReleaseInfo;ZZZZLjava/util/Map;)LX/EeS;
    .locals 17
    .param p6    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/appupdate/ReleaseInfo;",
            "ZZZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;)",
            "LX/EeS;"
        }
    .end annotation

    .prologue
    .line 346957
    monitor-enter p0

    if-eqz p3, :cond_1

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1wo;->k:Landroid/content/SharedPreferences;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, LX/Eem;->a(Landroid/content/SharedPreferences;Lcom/facebook/appupdate/ReleaseInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v5, 0x1

    .line 346958
    :goto_0
    new-instance v16, LX/EeU;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1wo;->f:LX/1sV;

    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2}, LX/EeU;-><init>(Ljava/lang/Long;LX/1sV;)V

    .line 346959
    new-instance v1, LX/EeX;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v2, p1

    move/from16 v4, p2

    move/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/EeX;-><init>(Lcom/facebook/appupdate/ReleaseInfo;Ljava/lang/String;ZZZZLjava/util/Map;)V

    .line 346960
    new-instance v2, LX/EeS;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/1wo;->h:LX/1wh;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1wo;->k:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/1wo;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/1wo;->b:Landroid/app/DownloadManager;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1wo;->c:LX/1wq;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/1wo;->g:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget v11, v0, LX/1wo;->m:I

    move-object/from16 v0, p0

    iget-object v12, v0, LX/1wo;->d:LX/0Or;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/1wo;->e:LX/0Or;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/1wo;->l:LX/0Or;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/1wo;->j:LX/1sZ;

    move-object v3, v1

    move-object/from16 v4, v16

    invoke-direct/range {v2 .. v15}, LX/EeS;-><init>(LX/EeX;LX/EeU;LX/1wh;Landroid/content/SharedPreferences;Landroid/content/Context;Landroid/app/DownloadManager;LX/1wq;Landroid/os/Handler;ILX/0Or;LX/0Or;LX/0Or;LX/1sZ;)V

    .line 346961
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346962
    move-object/from16 v0, p0

    iget-object v1, v0, LX/1wo;->i:LX/1sW;

    invoke-virtual {v2, v1}, LX/EeS;->a(LX/1sX;)Z

    .line 346963
    invoke-virtual {v2}, LX/EeS;->a()V

    .line 346964
    if-eqz p2, :cond_0

    .line 346965
    invoke-virtual {v2}, LX/EeS;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346966
    :cond_0
    monitor-exit p0

    return-object v2

    .line 346967
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)LX/EeS;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346983
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 346984
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v2

    iget-object v2, v2, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 346985
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 346986
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 347020
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1wo;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 347021
    :try_start_1
    invoke-direct {p0}, LX/1wo;->e()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347022
    :goto_0
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LX/1wo;->o:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 347023
    :cond_0
    monitor-exit p0

    return-void

    .line 347024
    :catch_0
    move-exception v0

    .line 347025
    :try_start_3
    iget-object v1, p0, LX/1wo;->h:LX/1wh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, LX/1wo;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Could not unpersist operations"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/1wh;->b(Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 347026
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/EeS;)V
    .locals 1

    .prologue
    .line 346977
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1wo;->o:Z

    invoke-static {v0}, LX/1wm;->a(Z)V

    .line 346978
    iget-object v0, p0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346979
    iget-object v0, p0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 346980
    :cond_0
    invoke-virtual {p1}, LX/EeS;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346981
    monitor-exit p0

    return-void

    .line 346982
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 346972
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1wo;->o:Z

    invoke-static {v0}, LX/1wm;->a(Z)V

    .line 346973
    iget-object v0, p0, LX/1wo;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 346974
    invoke-virtual {v0}, LX/EeS;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 346975
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 346976
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 346971
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/1wo;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/EeS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346968
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/1wo;->n:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346969
    monitor-exit p0

    return-object v0

    .line 346970
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
