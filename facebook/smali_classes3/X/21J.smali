.class public final LX/21J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20E;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/20g;


# direct methods
.method public constructor <init>(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 356987
    iput-object p1, p0, LX/21J;->b:LX/20g;

    iput-object p2, p0, LX/21J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    .line 356988
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    .line 356989
    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 356990
    invoke-virtual {p1, v2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 356991
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 356992
    :goto_0
    if-eqz v1, :cond_1

    .line 356993
    const-class v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 356994
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 356995
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 356996
    :cond_1
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 356997
    iget-object v0, p0, LX/21J;->b:LX/20g;

    iget-object v0, v0, LX/20g;->g:LX/20o;

    iget-object v1, p0, LX/21J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 356998
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/FragmentActivity;

    .line 356999
    iget-object v3, v0, LX/20o;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9F0;

    .line 357000
    invoke-static {v2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 357001
    iput-object v2, v3, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    .line 357002
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 357003
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v4, v3, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 357004
    iput-object v1, v3, LX/9F0;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357005
    iget-object v4, v3, LX/9F0;->f:Landroid/support/v4/app/FragmentActivity;

    iget-object v5, v3, LX/9F0;->c:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 357006
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Py;->asList()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Py;->asList()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 357007
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Py;->asList()LX/0Px;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p0

    .line 357008
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f081265

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    aput-object p0, p1, v1

    invoke-virtual {p2, v0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 357009
    :goto_1
    move-object v4, p0

    .line 357010
    new-instance v5, Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-direct {v5}, Lcom/facebook/feedback/ui/SpeechCommentDialog;-><init>()V

    .line 357011
    iput-object v4, v5, Lcom/facebook/feedback/ui/SpeechCommentDialog;->w:Ljava/lang/String;

    .line 357012
    iput-object v3, v5, Lcom/facebook/feedback/ui/SpeechCommentDialog;->v:LX/9F0;

    .line 357013
    move-object v4, v5

    .line 357014
    iput-object v4, v3, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    .line 357015
    iget-object v4, v3, LX/9F0;->e:Lcom/facebook/feedback/ui/SpeechCommentDialog;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v5

    const-string p0, "speech_dialog"

    invoke-virtual {v4, v5, p0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 357016
    const/4 v4, 0x0

    iput-boolean v4, v3, LX/9F0;->h:Z

    .line 357017
    iget-object v4, v3, LX/9F0;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9D5;

    .line 357018
    iget-object v5, v4, LX/9D5;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0if;

    .line 357019
    sget-object p0, LX/0ig;->M:LX/0ih;

    move-object p0, p0

    .line 357020
    invoke-virtual {v5, p0}, LX/0if;->a(LX/0ih;)V

    .line 357021
    iget-object v4, v3, LX/9F0;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9D5;

    .line 357022
    iget-object v5, v4, LX/9D5;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0if;

    .line 357023
    sget-object p0, LX/0ig;->M:LX/0ih;

    move-object p0, p0

    .line 357024
    const-string p2, "speech_dialog_launched"

    invoke-virtual {v5, p0, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 357025
    return-void

    :cond_2
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f081266

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method
