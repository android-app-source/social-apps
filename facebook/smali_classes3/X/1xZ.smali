.class public LX/1xZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348791
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348792
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xZ;->b:LX/0Zi;

    .line 348793
    iput-object p1, p0, LX/1xZ;->a:LX/0Ot;

    .line 348794
    return-void
.end method

.method public static a(LX/0QB;)LX/1xZ;
    .locals 4

    .prologue
    .line 348780
    const-class v1, LX/1xZ;

    monitor-enter v1

    .line 348781
    :try_start_0
    sget-object v0, LX/1xZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348782
    sput-object v2, LX/1xZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348783
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348784
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348785
    new-instance v3, LX/1xZ;

    const/16 p0, 0x1d30

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xZ;-><init>(LX/0Ot;)V

    .line 348786
    move-object v0, v3

    .line 348787
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348788
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348789
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348778
    invoke-static {}, LX/1dS;->b()V

    .line 348779
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x40019d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 348776
    iget-object v1, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p3, p4, p5}, LX/1oC;->a(IILX/1no;)V

    .line 348777
    const/16 v1, 0x1f

    const v2, -0x66c86820

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 348765
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v3, 0x0

    .line 348766
    new-instance v0, LX/7LN;

    invoke-direct {v0}, LX/7LN;-><init>()V

    sget-object v1, LX/04D;->ACTOR_PROFILE_VIDEO:LX/04D;

    .line 348767
    iput-object v1, v0, LX/7LN;->a:LX/04D;

    .line 348768
    move-object v0, v0

    .line 348769
    sget-object v1, LX/04G;->OTHERS:LX/04G;

    .line 348770
    iput-object v1, v0, LX/7LN;->b:LX/04G;

    .line 348771
    move-object v0, v0

    .line 348772
    iput-boolean v3, v0, LX/7LN;->d:Z

    .line 348773
    move-object v0, v0

    .line 348774
    const/4 v1, 0x2

    new-array v1, v1, [LX/2oy;

    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v3

    const/4 v2, 0x1

    new-instance v3, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object p0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v3, p1, p0}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/7LN;->a([LX/2oy;)LX/7LN;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/7LN;->a(Landroid/content/Context;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    move-object v0, v0

    .line 348775
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 11

    .prologue
    .line 348707
    check-cast p2, LX/BsM;

    .line 348708
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 348709
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 348710
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;

    iget-object v3, p2, LX/BsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/BsM;->b:LX/1Po;

    .line 348711
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 348712
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    .line 348713
    invoke-static {v6}, LX/1xl;->g(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v5

    .line 348714
    new-instance v7, LX/2oE;

    invoke-direct {v7}, LX/2oE;-><init>()V

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 348715
    iput-object v5, v7, LX/2oE;->a:Landroid/net/Uri;

    .line 348716
    move-object v5, v7

    .line 348717
    sget-object v7, LX/097;->FROM_STREAM:LX/097;

    .line 348718
    iput-object v7, v5, LX/2oE;->e:LX/097;

    .line 348719
    move-object v5, v5

    .line 348720
    invoke-virtual {v5}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v5

    .line 348721
    new-instance v7, LX/2oH;

    invoke-direct {v7}, LX/2oH;-><init>()V

    invoke-virtual {v7, v5}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v5

    const/4 v7, 0x1

    .line 348722
    iput-boolean v7, v5, LX/2oH;->g:Z

    .line 348723
    move-object v5, v5

    .line 348724
    invoke-virtual {v5}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 348725
    new-instance v7, LX/0P2;

    invoke-direct {v7}, LX/0P2;-><init>()V

    .line 348726
    invoke-static {v6}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v8

    .line 348727
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 348728
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v8

    .line 348729
    if-eqz v8, :cond_0

    .line 348730
    const-string v9, "CoverImageParamsKey"

    invoke-virtual {v7, v9, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 348731
    :cond_0
    new-instance v8, LX/2pZ;

    invoke-direct {v8}, LX/2pZ;-><init>()V

    .line 348732
    iput-object v5, v8, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 348733
    move-object v5, v8

    .line 348734
    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v5

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 348735
    iput-wide v7, v5, LX/2pZ;->e:D

    .line 348736
    move-object v5, v5

    .line 348737
    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    .line 348738
    iput-object v5, v1, LX/1np;->a:Ljava/lang/Object;

    .line 348739
    iget-object v5, v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->c:LX/0Zm;

    const-string v7, "feed_profile_video_displayed"

    invoke-virtual {v5, v7}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 348740
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "feed_profile_video_displayed"

    invoke-direct {v5, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "native_newsfeed"

    .line 348741
    iput-object v7, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 348742
    move-object v5, v5

    .line 348743
    const-string v7, "requested_video_quality"

    const-string v8, "THUMB_100"

    invoke-virtual {v5, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v7, "display_context"

    const-string v8, "actor"

    invoke-virtual {v5, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "feed_list_name"

    if-eqz v4, :cond_1

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    if-nez v5, :cond_2

    :cond_1
    const-string v5, ""

    :goto_0
    invoke-virtual {v7, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v7, "video_id"

    .line 348744
    if-eqz v6, :cond_4

    .line 348745
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLActor;->am()Lcom/facebook/graphql/model/GraphQLProfileVideo;

    move-result-object v8

    .line 348746
    if-eqz v8, :cond_4

    .line 348747
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfileVideo;->j()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v8

    .line 348748
    if-eqz v8, :cond_4

    .line 348749
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v8

    .line 348750
    :goto_1
    move-object v6, v8

    .line 348751
    invoke-virtual {v5, v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "tracking"

    invoke-static {v3}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 348752
    iput-object v5, v2, LX/1np;->a:Ljava/lang/Object;

    .line 348753
    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->d:Z

    .line 348754
    :goto_2
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 348755
    check-cast v0, LX/2pa;

    iput-object v0, p2, LX/BsM;->c:LX/2pa;

    .line 348756
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 348757
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 348758
    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object v0, p2, LX/BsM;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348759
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 348760
    return-void

    .line 348761
    :cond_2
    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    goto :goto_0

    .line 348762
    :cond_3
    const/4 v5, 0x0

    .line 348763
    iput-object v5, v2, LX/1np;->a:Ljava/lang/Object;

    .line 348764
    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 348795
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 348703
    check-cast p3, LX/BsM;

    .line 348704
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v0, p3, LX/BsM;->c:LX/2pa;

    .line 348705
    invoke-virtual {p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 348706
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 348702
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 348699
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 348700
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 348701
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 348692
    check-cast p3, LX/BsM;

    .line 348693
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p3, LX/BsM;->d:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348694
    sget-object p0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 348695
    iget-boolean p0, v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->d:Z

    if-nez p0, :cond_0

    if-eqz v1, :cond_0

    .line 348696
    iget-object p0, v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->b:LX/0Zb;

    invoke-interface {p0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 348697
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->d:Z

    .line 348698
    :cond_0
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 348689
    iget-object v0, p0, LX/1xZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 348690
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 348691
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 348688
    const/16 v0, 0xf

    return v0
.end method
