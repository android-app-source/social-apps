.class public LX/1xG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348089
    iput-object p1, p0, LX/1xG;->a:LX/0Zb;

    .line 348090
    return-void
.end method

.method public static a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V
    .locals 2
    .param p1    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 348091
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 348092
    sget-object v0, LX/7Di;->PHOTO_ID:LX/7Di;

    iget-object v0, v0, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348093
    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p3, LX/7Dj;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 348094
    sget-object v0, LX/7Di;->SURFACE:LX/7Di;

    iget-object v0, v0, LX/7Di;->value:Ljava/lang/String;

    iget-object v1, p3, LX/7Dj;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348095
    :cond_1
    const-string v0, "photo_360"

    .line 348096
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 348097
    iget-object v0, p0, LX/1xG;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 348098
    return-void
.end method

.method public static b(LX/0QB;)LX/1xG;
    .locals 2

    .prologue
    .line 348086
    new-instance v1, LX/1xG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/1xG;-><init>(LX/0Zb;)V

    .line 348087
    return-object v1
.end method

.method public static b(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V
    .locals 2
    .param p1    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 348059
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 348060
    sget-object v0, LX/7Di;->COMPOSER_SESSION_ID:LX/7Di;

    iget-object v0, v0, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348061
    :cond_0
    if-eqz p3, :cond_1

    .line 348062
    sget-object v0, LX/7Di;->SURFACE:LX/7Di;

    iget-object v0, v0, LX/7Di;->value:Ljava/lang/String;

    iget-object v1, p3, LX/7Dj;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348063
    :cond_1
    const-string v0, "photo_360"

    .line 348064
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 348065
    iget-object v0, p0, LX/1xG;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 348066
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/7Dj;LX/7DC;FFIIIIIII)V
    .locals 6

    .prologue
    .line 348073
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "spherical_photo_renderer_stats"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 348074
    sget-object v3, LX/7Di;->RENDER_METHOD:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {p3}, LX/7DC;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348075
    sget-object v3, LX/7Di;->AVG_FRAME_RENDER_TIME:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    float-to-double v4, p4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348076
    sget-object v3, LX/7Di;->AVG_TEXTURE_TO_SCREEN_RATIO:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    float-to-double v4, p5

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348077
    sget-object v3, LX/7Di;->DEVICE_PIXEL_RATIO:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348078
    sget-object v3, LX/7Di;->MAX_GPU_MEMORY_USED:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348079
    sget-object v3, LX/7Di;->MAX_TILE_LEVEL:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348080
    sget-object v3, LX/7Di;->TOTAL_DATA_LOADED:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348081
    sget-object v3, LX/7Di;->VIEWPORT_HEIGHT:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    move/from16 v0, p10

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348082
    sget-object v3, LX/7Di;->VIEWPORT_WIDTH:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    move/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348083
    sget-object v3, LX/7Di;->DT:LX/7Di;

    iget-object v3, v3, LX/7Di;->value:Ljava/lang/String;

    move/from16 v0, p12

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 348084
    invoke-static {p0, v2, p1, p2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 348085
    return-void
.end method

.method public final g(Ljava/lang/String;LX/7Dj;)V
    .locals 2

    .prologue
    .line 348070
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "spherical_photo_zoom_start"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 348071
    invoke-static {p0, v0, p1, p2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 348072
    return-void
.end method

.method public final h(Ljava/lang/String;LX/7Dj;)V
    .locals 2

    .prologue
    .line 348067
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "spherical_photo_drag_start"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 348068
    invoke-static {p0, v0, p1, p2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 348069
    return-void
.end method
