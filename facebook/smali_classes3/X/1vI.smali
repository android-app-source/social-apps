.class public abstract LX/1vI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 342195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3AU;)J
    .locals 3

    .prologue
    .line 342129
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342130
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v2

    .line 342131
    :try_start_0
    invoke-virtual {p0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 342132
    invoke-virtual {p1}, LX/3AU;->a()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;

    .line 342133
    invoke-static {v0, v1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 342134
    invoke-virtual {v2}, LX/1vJ;->close()V

    return-wide v0

    .line 342135
    :catch_0
    move-exception v0

    .line 342136
    :try_start_1
    invoke-virtual {v2, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342137
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1vJ;->close()V

    throw v0
.end method

.method public final a(Ljava/io/OutputStream;)J
    .locals 4

    .prologue
    .line 342187
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342188
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 342189
    :try_start_0
    invoke-virtual {p0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 342190
    invoke-static {v0, p1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 342191
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-wide v2

    .line 342192
    :catch_0
    move-exception v0

    .line 342193
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342194
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method

.method public a(JJ)LX/1vI;
    .locals 7

    .prologue
    .line 342186
    new-instance v0, LX/522;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/522;-><init>(LX/1vI;JJ)V

    return-object v0
.end method

.method public abstract a()Ljava/io/InputStream;
.end method

.method public final a(LX/6ec;)Ljava/lang/Object;
    .locals 11
    .annotation build Lcom/google/common/annotations/Beta;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/common/io/ByteProcessor",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 342160
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342161
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 342162
    :try_start_0
    invoke-virtual {p0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 342163
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342164
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342165
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 342166
    :cond_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 342167
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 342168
    move v5, v4

    :goto_0
    iget-boolean v6, p1, LX/6ec;->d:Z

    if-eqz v6, :cond_2

    if-ge v5, v3, :cond_2

    .line 342169
    aget-byte v6, v2, v5

    if-nez v6, :cond_1

    move v6, v7

    :goto_1
    iput-boolean v6, p1, LX/6ec;->d:Z

    .line 342170
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 342171
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 342172
    :cond_2
    iget-wide v5, p1, LX/6ec;->e:J

    int-to-long v9, v3

    add-long/2addr v5, v9

    iput-wide v5, p1, LX/6ec;->e:J

    .line 342173
    iget-object v5, p1, LX/6ec;->a:Ljavax/crypto/Mac;

    invoke-virtual {v5, v2, v4, v3}, Ljavax/crypto/Mac;->update([BII)V

    .line 342174
    move v3, v7

    .line 342175
    if-nez v3, :cond_0

    .line 342176
    :cond_3
    const/4 v9, 0x0

    .line 342177
    iget-boolean v5, p1, LX/6ec;->d:Z

    if-nez v5, :cond_4

    iget-wide v5, p1, LX/6ec;->e:J

    iget-wide v7, p1, LX/6ec;->b:J

    cmp-long v5, v5, v7

    if-eqz v5, :cond_5

    .line 342178
    :cond_4
    sget-object v5, LX/2ML;->a:Ljava/lang/Class;

    const-string v6, "Read bytes failed. IsAllBytesZero: %b, TotalBytesRead: %d, FileSize: %d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    iget-boolean v8, p1, LX/6ec;->d:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x1

    iget-wide v9, p1, LX/6ec;->e:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-wide v9, p1, LX/6ec;->b:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 342179
    const-string v5, ""

    .line 342180
    :goto_2
    move-object v2, v5

    .line 342181
    move-object v0, v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342182
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-object v0

    .line 342183
    :catch_0
    move-exception v0

    .line 342184
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342185
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0

    :cond_5
    iget-object v5, p1, LX/6ec;->a:Ljavax/crypto/Mac;

    invoke-virtual {v5}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v5

    invoke-static {v5, v9}, LX/1u4;->a([BZ)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public final a(LX/1vI;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/16 v9, 0x2000

    .line 342145
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 342146
    new-array v3, v9, [B

    .line 342147
    new-array v4, v9, [B

    .line 342148
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v5

    .line 342149
    :try_start_0
    invoke-virtual {p0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 342150
    invoke-virtual {p1}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    .line 342151
    :cond_0
    const/4 v6, 0x0

    const/16 v7, 0x2000

    invoke-static {v0, v3, v6, v7}, LX/0hW;->a(Ljava/io/InputStream;[BII)I

    move-result v6

    .line 342152
    const/4 v7, 0x0

    const/16 v8, 0x2000

    invoke-static {v1, v4, v7, v8}, LX/0hW;->a(Ljava/io/InputStream;[BII)I

    move-result v7

    .line 342153
    if-ne v6, v7, :cond_1

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_2

    .line 342154
    :cond_1
    invoke-virtual {v5}, LX/1vJ;->close()V

    move v0, v2

    :goto_0
    return v0

    .line 342155
    :cond_2
    if-eq v6, v9, :cond_0

    .line 342156
    invoke-virtual {v5}, LX/1vJ;->close()V

    const/4 v0, 0x1

    goto :goto_0

    .line 342157
    :catch_0
    move-exception v0

    .line 342158
    :try_start_1
    invoke-virtual {v5, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342159
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, LX/1vJ;->close()V

    throw v0
.end method

.method public b()[B
    .locals 2

    .prologue
    .line 342138
    invoke-static {}, LX/1vJ;->a()LX/1vJ;

    move-result-object v1

    .line 342139
    :try_start_0
    invoke-virtual {p0}, LX/1vI;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 342140
    invoke-static {v0}, LX/0hW;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 342141
    invoke-virtual {v1}, LX/1vJ;->close()V

    return-object v0

    .line 342142
    :catch_0
    move-exception v0

    .line 342143
    :try_start_1
    invoke-virtual {v1, v0}, LX/1vJ;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342144
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/1vJ;->close()V

    throw v0
.end method
