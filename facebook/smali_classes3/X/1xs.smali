.class public LX/1xs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field private final c:LX/1xc;

.field private final d:LX/1xv;

.field private final e:LX/1xu;

.field private final f:LX/1VE;

.field private final g:LX/0Uh;

.field private final h:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 349695
    sget v0, LX/1VE;->a:I

    sput v0, LX/1xs;->a:I

    .line 349696
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    .line 349697
    sput-object v0, LX/1xs;->b:Landroid/util/SparseArray;

    const v1, 0x7f0d0081

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 349698
    return-void
.end method

.method public constructor <init>(LX/1xc;LX/1xu;LX/1xv;LX/1VE;LX/0Uh;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349700
    iput-object p1, p0, LX/1xs;->c:LX/1xc;

    .line 349701
    iput-object p2, p0, LX/1xs;->e:LX/1xu;

    .line 349702
    iput-object p3, p0, LX/1xs;->d:LX/1xv;

    .line 349703
    iput-object p4, p0, LX/1xs;->f:LX/1VE;

    .line 349704
    iput-object p5, p0, LX/1xs;->g:LX/0Uh;

    .line 349705
    iput-object p6, p0, LX/1xs;->h:LX/0W3;

    .line 349706
    return-void
.end method

.method private static a(LX/1De;I)I
    .locals 2

    .prologue
    const v1, -0xebe7dd

    .line 349707
    if-ne p1, v1, :cond_0

    const v0, 0x1010036

    invoke-static {p0, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result p1

    :cond_0
    return p1
.end method

.method public static a(LX/0QB;)LX/1xs;
    .locals 10

    .prologue
    .line 349708
    const-class v1, LX/1xs;

    monitor-enter v1

    .line 349709
    :try_start_0
    sget-object v0, LX/1xs;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 349710
    sput-object v2, LX/1xs;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 349711
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349712
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 349713
    new-instance v3, LX/1xs;

    invoke-static {v0}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v4

    check-cast v4, LX/1xc;

    invoke-static {v0}, LX/1xu;->a(LX/0QB;)LX/1xu;

    move-result-object v5

    check-cast v5, LX/1xu;

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v6

    check-cast v6, LX/1xv;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v7

    check-cast v7, LX/1VE;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    invoke-direct/range {v3 .. v9}, LX/1xs;-><init>(LX/1xc;LX/1xu;LX/1xv;LX/1VE;LX/0Uh;LX/0W3;)V

    .line 349714
    move-object v0, v3

    .line 349715
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 349716
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349717
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 349718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/1xs;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Bsi;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1xs",
            "<TE;>.PrimaryActorRatingModel;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 349719
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 349720
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1y3;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    move-result-object v0

    invoke-static {v0}, LX/1y4;->a(Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;)Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;

    move-result-object v0

    .line 349721
    if-nez v0, :cond_0

    move-object v0, v1

    .line 349722
    :goto_0
    return-object v0

    .line 349723
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel$ActorsModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 349724
    if-nez v0, :cond_1

    move-object v0, v1

    .line 349725
    goto :goto_0

    .line 349726
    :cond_1
    iget-object v2, p0, LX/1xs;->h:LX/0W3;

    sget-wide v4, LX/0X5;->jV:J

    const-string v3, ""

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 349727
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 349728
    if-gez v3, :cond_2

    move-object v0, v1

    .line 349729
    goto :goto_0

    .line 349730
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    .line 349731
    const/16 v3, 0x2c

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 349732
    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 349733
    new-instance v0, LX/Bsi;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, p0, v4, v5, v2}, LX/Bsi;-><init>(LX/1xs;DI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349734
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static b(LX/1De;I)I
    .locals 2

    .prologue
    .line 349735
    sget v0, LX/1VE;->a:I

    if-ne p1, v0, :cond_0

    const v0, 0x7f01029b

    sget v1, LX/1VE;->a:I

    invoke-static {p0, v0, v1}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result p1

    :cond_0
    return p1
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;ZIIIFFFIZ)LX/1Dg;
    .locals 16
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p8    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;ZIIIFFFIZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 349736
    move-object/from16 v0, p0

    iget-object v2, v0, LX/1xs;->g:LX/0Uh;

    const/16 v3, 0x579

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/1xs;->g:LX/0Uh;

    const/16 v3, 0x57a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static/range {p2 .. p2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/1xs;->a(LX/1xs;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Bsi;

    move-result-object v2

    if-eqz v2, :cond_3

    :cond_0
    const/4 v14, 0x1

    .line 349737
    :goto_0
    new-instance v2, LX/1xy;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, LX/1xs;->c:LX/1xc;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, LX/1xs;->d:LX/1xv;

    const/4 v10, -0x1

    move-object/from16 v0, p1

    move/from16 v1, p6

    invoke-static {v0, v1}, LX/1xs;->b(LX/1De;I)I

    move-result v11

    if-nez p12, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/1xs;->f:LX/1VE;

    invoke-interface/range {p3 .. p3}, LX/1Po;->c()LX/1PT;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v12}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    const/4 v12, 0x1

    :goto_1
    invoke-interface/range {p3 .. p3}, LX/1Po;->c()LX/1PT;

    move-result-object v15

    move-object/from16 v4, p2

    move/from16 v13, p4

    invoke-direct/range {v2 .. v15}, LX/1xy;-><init>(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;LX/1xc;LX/Bs7;LX/1DR;LX/1xv;IIZZZLX/1PT;)V

    .line 349738
    move-object/from16 v0, p0

    iget-object v4, v0, LX/1xs;->e:LX/1xu;

    move-object/from16 v3, p3

    check-cast v3, LX/1Pr;

    invoke-virtual {v4, v2, v3}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 349739
    check-cast p3, LX/1Pr;

    invoke-interface {v2}, LX/1xz;->a()LX/1KL;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, LX/0jW;

    move-object/from16 v0, p3

    invoke-interface {v0, v3, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1yT;

    .line 349740
    invoke-static/range {p1 .. p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    iget-object v2, v2, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b1064

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-static {v0, v1}, LX/1xs;->a(LX/1De;I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a044e

    invoke-virtual {v2, v3}, LX/1ne;->v(I)LX/1ne;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, LX/1ne;->k(I)LX/1ne;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, LX/1ne;->d(F)LX/1ne;

    move-result-object v2

    move/from16 v0, p9

    invoke-virtual {v2, v0}, LX/1ne;->e(F)LX/1ne;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/1ne;->c(F)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b1063

    invoke-virtual {v2, v3}, LX/1ne;->s(I)LX/1ne;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, LX/1ne;->j(F)LX/1ne;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-static/range {p2 .. p2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, LX/1xs;->b:Landroid/util/SparseArray;

    :goto_2
    invoke-interface {v3, v2}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 349741
    move-object/from16 v0, p0

    iget-object v3, v0, LX/1xs;->g:LX/0Uh;

    const/16 v4, 0x57b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static/range {p2 .. p2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 349742
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, LX/1xs;->a(LX/1xs;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Bsi;

    move-result-object v3

    .line 349743
    if-nez v3, :cond_6

    .line 349744
    :cond_2
    :goto_3
    return-object v2

    .line 349745
    :cond_3
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 349746
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 349747
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 349748
    :cond_6
    invoke-virtual/range {p1 .. p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0814f0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v3, LX/Bsi;->b:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 349749
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-static/range {p1 .. p1}, LX/8yi;->c(LX/1De;)LX/8yg;

    move-result-object v6

    iget v3, v3, LX/Bsi;->a:F

    invoke-virtual {v6, v3}, LX/8yg;->c(F)LX/8yg;

    move-result-object v3

    const/4 v6, 0x3

    invoke-virtual {v3, v6}, LX/8yg;->h(I)LX/8yg;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p1 .. p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0904

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x1010212

    const v6, -0x6e685d

    move-object/from16 v0, p1

    invoke-static {v0, v5, v6}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v5

    invoke-virtual {v4, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_3
.end method
