.class public LX/1yR;
.super LX/1yS;
.source ""


# instance fields
.field public final synthetic c:LX/1Uf;

.field private final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/1Uf;Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V
    .locals 8
    .param p5    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/1yD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350578
    iput-object p1, p0, LX/1yR;->c:LX/1Uf;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    .line 350579
    invoke-direct/range {v0 .. v7}, LX/1yS;-><init>(LX/1Uf;Ljava/lang/String;Landroid/content/Context;LX/0lF;ILX/1yD;Ljava/lang/String;)V

    .line 350580
    iput-object p3, p0, LX/1yR;->d:Lcom/facebook/content/SecureContextHelper;

    .line 350581
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;LX/0lF;ZLX/1vY;)V
    .locals 6
    .param p5    # LX/1vY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 350582
    iget-object v0, p0, LX/1yR;->c:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->A:LX/0id;

    const-string v1, "InternalClickableSpan"

    invoke-virtual {v0, p1, v1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 350583
    invoke-virtual {p0}, LX/1yR;->g()Landroid/os/Bundle;

    move-result-object v1

    .line 350584
    iget-object v0, p0, LX/1yS;->h:LX/1yD;

    move-object v2, v0

    .line 350585
    iget v0, p0, LX/1yS;->g:I

    move v3, v0

    .line 350586
    const v0, -0x7333ac54

    if-ne v3, v0, :cond_0

    .line 350587
    iget-object v0, p0, LX/1yR;->c:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v4, "native_newsfeed"

    invoke-static {v3}, LX/38I;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2}, LX/1yD;->w_()Ljava/lang/String;

    move-result-object v5

    if-eqz p3, :cond_5

    const-string v2, "tracking"

    invoke-static {v2, p3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    :goto_0
    invoke-virtual {v0, v4, v3, v5, v2}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 350588
    :cond_0
    invoke-virtual {p0, p4, p5, p3}, LX/1yS;->a(ZLX/1vY;LX/0lF;)V

    .line 350589
    iget-object v0, p0, LX/1yS;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/1yS;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nV;

    .line 350590
    :goto_1
    if-eqz v0, :cond_1

    .line 350591
    invoke-interface {v0, p0}, LX/1nV;->onClick(LX/1yS;)V

    .line 350592
    :cond_1
    iget-object v0, p0, LX/1yR;->c:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 350593
    if-eqz v0, :cond_4

    .line 350594
    if-eqz v1, :cond_2

    .line 350595
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 350596
    :cond_2
    iget-object v1, p0, LX/1yR;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 350597
    :goto_2
    return-void

    .line 350598
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 350599
    :cond_4
    iget-object v0, p0, LX/1yR;->c:LX/1Uf;

    iget-object v0, v0, LX/1Uf;->A:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    goto :goto_2

    .line 350600
    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public g()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 350601
    const/4 v0, 0x0

    return-object v0
.end method
