.class public LX/1zp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/1zp;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0Sh;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354166
    iput-object p1, p0, LX/1zp;->a:LX/0tX;

    .line 354167
    iput-object p2, p0, LX/1zp;->b:LX/1Ck;

    .line 354168
    iput-object p3, p0, LX/1zp;->c:Ljava/util/concurrent/Executor;

    .line 354169
    iput-object p4, p0, LX/1zp;->d:LX/0Sh;

    .line 354170
    return-void
.end method

.method public static a(LX/0QB;)LX/1zp;
    .locals 7

    .prologue
    .line 354171
    sget-object v0, LX/1zp;->e:LX/1zp;

    if-nez v0, :cond_1

    .line 354172
    const-class v1, LX/1zp;

    monitor-enter v1

    .line 354173
    :try_start_0
    sget-object v0, LX/1zp;->e:LX/1zp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 354174
    if-eqz v2, :cond_0

    .line 354175
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 354176
    new-instance p0, LX/1zp;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v6

    check-cast v6, LX/0Sh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/1zp;-><init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/0Sh;)V

    .line 354177
    move-object v0, p0

    .line 354178
    sput-object v0, LX/1zp;->e:LX/1zp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354179
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 354180
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 354181
    :cond_1
    sget-object v0, LX/1zp;->e:LX/1zp;

    return-object v0

    .line 354182
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 354183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
