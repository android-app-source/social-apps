.class public LX/1w9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0iA;

.field public final b:LX/1xK;

.field public final c:I

.field public final d:I

.field public final e:I

.field public f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            "LX/C0w;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(LX/0iA;LX/1xJ;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 345029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345030
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1w9;->g:Z

    .line 345031
    iput-object p1, p0, LX/1w9;->a:LX/0iA;

    .line 345032
    invoke-static {p2}, LX/1xJ;->e(LX/1xJ;)V

    .line 345033
    iget v0, p2, LX/1xJ;->e:I

    move v0, v0

    .line 345034
    iput v0, p0, LX/1w9;->c:I

    .line 345035
    invoke-static {p2}, LX/1xJ;->e(LX/1xJ;)V

    .line 345036
    iget v0, p2, LX/1xJ;->d:I

    move v0, v0

    .line 345037
    iget v1, p0, LX/1w9;->c:I

    mul-int/2addr v0, v1

    iput v0, p0, LX/1w9;->d:I

    .line 345038
    invoke-static {p2}, LX/1xJ;->e(LX/1xJ;)V

    .line 345039
    iget v0, p2, LX/1xJ;->c:I

    move v0, v0

    .line 345040
    iput v0, p0, LX/1w9;->e:I

    .line 345041
    iget-object v0, p0, LX/1w9;->a:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PHOTO_ATTATCHMENT_PRODUCT_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/1xK;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 345042
    check-cast v0, LX/1xK;

    iput-object v0, p0, LX/1w9;->b:LX/1xK;

    .line 345043
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 345044
    iget-boolean v0, p0, LX/1w9;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/1w9;->b:LX/1xK;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
