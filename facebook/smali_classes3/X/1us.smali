.class public LX/1us;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1cd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1cW;

.field public c:J


# direct methods
.method public constructor <init>(LX/1cd;LX/1cW;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/producers/Consumer",
            "<",
            "LX/1FL;",
            ">;",
            "Lcom/facebook/imagepipeline/producers/ProducerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 341762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 341763
    iput-object p1, p0, LX/1us;->a:LX/1cd;

    .line 341764
    iput-object p2, p0, LX/1us;->b:LX/1cW;

    .line 341765
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/1us;->c:J

    .line 341766
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341767
    iget-object v0, p0, LX/1us;->b:LX/1cW;

    .line 341768
    iget-object p0, v0, LX/1cW;->b:Ljava/lang/String;

    move-object v0, p0

    .line 341769
    return-object v0
.end method

.method public final d()LX/1BV;
    .locals 1

    .prologue
    .line 341759
    iget-object v0, p0, LX/1us;->b:LX/1cW;

    .line 341760
    iget-object p0, v0, LX/1cW;->c:LX/1BV;

    move-object v0, p0

    .line 341761
    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 341755
    iget-object v0, p0, LX/1us;->b:LX/1cW;

    .line 341756
    iget-object p0, v0, LX/1cW;->a:LX/1bf;

    move-object v0, p0

    .line 341757
    iget-object p0, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, p0

    .line 341758
    return-object v0
.end method
