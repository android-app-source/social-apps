.class public LX/1x7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:Ljava/lang/String;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static volatile r:LX/1x7;


# instance fields
.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/03V;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1x6;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/surveysession/listeners/SurveyMultipleModelsReadyListener;",
            ">;>;"
        }
    .end annotation
.end field

.field public final l:LX/0Uh;

.field public final m:LX/0SG;

.field public final n:LX/0kL;

.field public o:Landroid/content/res/Resources;

.field private p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/structuredsurvey/graphql/SurveyIntegrationPointQueryModels$SurveyIntegrationPointQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 347769
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NaRF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, LX/1x7;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1x7;->b:Ljava/lang/String;

    .line 347770
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/survey_cool_down"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1x7;->c:LX/0Tn;

    .line 347771
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/last_invitation_impression_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1x7;->d:LX/0Tn;

    .line 347772
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/intern_dev_mode_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/1x7;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0Uh;LX/0SG;LX/0Ot;LX/0kL;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/1x6;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/surveysession/listeners/SurveyMultipleModelsReadyListener;",
            ">;>;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347838
    iput-object p1, p0, LX/1x7;->h:LX/0Ot;

    .line 347839
    iput-object p2, p0, LX/1x7;->i:LX/03V;

    .line 347840
    iput-object p3, p0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 347841
    iput-object p4, p0, LX/1x7;->f:Ljava/util/concurrent/ExecutorService;

    .line 347842
    iput-object p5, p0, LX/1x7;->j:LX/0Ot;

    .line 347843
    iput-object p6, p0, LX/1x7;->k:LX/0Ot;

    .line 347844
    iput-object p7, p0, LX/1x7;->o:Landroid/content/res/Resources;

    .line 347845
    iput-object p8, p0, LX/1x7;->l:LX/0Uh;

    .line 347846
    iput-object p9, p0, LX/1x7;->m:LX/0SG;

    .line 347847
    iput-object p10, p0, LX/1x7;->e:LX/0Ot;

    .line 347848
    iput-object p11, p0, LX/1x7;->n:LX/0kL;

    .line 347849
    return-void
.end method

.method public static a(LX/0QB;)LX/1x7;
    .locals 15

    .prologue
    .line 347819
    sget-object v0, LX/1x7;->r:LX/1x7;

    if-nez v0, :cond_1

    .line 347820
    const-class v1, LX/1x7;

    monitor-enter v1

    .line 347821
    :try_start_0
    sget-object v0, LX/1x7;->r:LX/1x7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 347822
    if-eqz v2, :cond_0

    .line 347823
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 347824
    new-instance v3, LX/1x7;

    const/16 v4, 0xbc

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 v8, 0x122e

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 347825
    new-instance v9, LX/1x8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    invoke-direct {v9, v10}, LX/1x8;-><init>(LX/0QB;)V

    move-object v9, v9

    .line 347826
    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v10

    invoke-static {v9, v10}, LX/0Sr;->a(LX/0Or;LX/0R7;)LX/0Ot;

    move-result-object v9

    move-object v9, v9

    .line 347827
    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const/16 v13, 0xafd

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-direct/range {v3 .. v14}, LX/1x7;-><init>(LX/0Ot;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;Landroid/content/res/Resources;LX/0Uh;LX/0SG;LX/0Ot;LX/0kL;)V

    .line 347828
    move-object v0, v3

    .line 347829
    sput-object v0, LX/1x7;->r:LX/1x7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347830
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 347831
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 347832
    :cond_1
    sget-object v0, LX/1x7;->r:LX/1x7;

    return-object v0

    .line 347833
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 347834
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1x7;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 347835
    const-string v0, "no_fetch"

    invoke-static {p0, p1, v0, p2}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347836
    return-void
.end method

.method public static a$redex0(LX/1x7;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 347816
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/app/Activity;

    if-eqz v0, :cond_0

    instance-of v0, p2, LX/3Bz;

    if-eqz v0, :cond_0

    .line 347817
    check-cast p2, Landroid/app/Activity;

    new-instance v0, Lcom/facebook/structuredsurvey/StructuredSurveyFetcher$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/structuredsurvey/StructuredSurveyFetcher$4;-><init>(LX/1x7;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 347818
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 347812
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "survey_integration_touched"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 347813
    const-string v0, "integration_point_id"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "action"

    invoke-virtual {v0, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "reason"

    invoke-virtual {v0, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 347814
    iget-object v0, p0, LX/1x7;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 347815
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1x4;Landroid/content/Context;)V
    .locals 12
    .param p3    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 347773
    const/4 v5, 0x0

    .line 347774
    iget-object v6, p0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/1x7;->a:LX/0Tn;

    invoke-interface {v6, v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 347775
    const/4 v5, 0x1

    .line 347776
    :cond_0
    move v5, v5

    .line 347777
    if-nez v5, :cond_3

    .line 347778
    const-wide/16 v10, 0x0

    .line 347779
    iget-object v8, p0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/1x7;->d:LX/0Tn;

    invoke-interface {v8, v9, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 347780
    cmp-long v10, v8, v10

    if-eqz v10, :cond_1

    iget-object v10, p0, LX/1x7;->m:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    sub-long v8, v10, v8

    const-wide/32 v10, 0x240c8400

    cmp-long v8, v8, v10

    if-lez v8, :cond_7

    .line 347781
    :cond_1
    const/4 v8, 0x0

    .line 347782
    :goto_0
    move v6, v8

    .line 347783
    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 347784
    const-string v7, "recently_seen_survey"

    invoke-static {p0, p1, v7}, LX/1x7;->a(LX/1x7;Ljava/lang/String;Ljava/lang/String;)V

    .line 347785
    :cond_2
    move v5, v6

    .line 347786
    if-nez v5, :cond_6

    .line 347787
    sget-object v6, LX/1x7;->c:LX/0Tn;

    invoke-virtual {v6, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    .line 347788
    iget-object v7, p0, LX/1x7;->m:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    iget-object v7, p0, LX/1x7;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v10, 0x0

    invoke-interface {v7, v6, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v6

    cmp-long v6, v8, v6

    if-lez v6, :cond_8

    .line 347789
    const/4 v6, 0x0

    .line 347790
    :goto_1
    move v5, v6

    .line 347791
    if-nez v5, :cond_6

    :cond_3
    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 347792
    if-nez v0, :cond_4

    .line 347793
    iget-object v0, p0, LX/1x7;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    invoke-virtual {v0, p1}, LX/1x6;->a(Ljava/lang/String;)V

    .line 347794
    :goto_3
    return-void

    .line 347795
    :cond_4
    const-string v0, "fetch"

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/1x7;->a$redex0(LX/1x7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347796
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 347797
    iget-object v0, p2, LX/1x4;->d:LX/0P1;

    move-object v0, v0

    .line 347798
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 347799
    new-instance v4, LX/4Je;

    invoke-direct {v4}, LX/4Je;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 347800
    const-string v5, "context_key"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 347801
    move-object v1, v4

    .line 347802
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 347803
    const-string v4, "context_value"

    invoke-virtual {v1, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 347804
    move-object v0, v1

    .line 347805
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 347806
    :cond_5
    iget-object v0, p0, LX/1x7;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    .line 347807
    new-instance v1, LX/352;

    invoke-direct {v1}, LX/352;-><init>()V

    move-object v1, v1

    .line 347808
    const-string v3, "integration_point_id"

    invoke-virtual {v1, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "survey_context_data"

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/352;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0xe10

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/1x7;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 347809
    iget-object v0, p0, LX/1x7;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/353;

    invoke-direct {v1, p0, p1, p3, p2}, LX/353;-><init>(LX/1x7;Ljava/lang/String;Landroid/content/Context;LX/1x4;)V

    iget-object v2, p0, LX/1x7;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_3

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 347810
    :cond_8
    const-string v6, "client_cool_down"

    invoke-static {p0, p1, v6}, LX/1x7;->a(LX/1x7;Ljava/lang/String;Ljava/lang/String;)V

    .line 347811
    const/4 v6, 0x1

    goto/16 :goto_1
.end method
