.class public LX/1zk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/1zk;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0tH;

.field public c:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354069
    iput-object p1, p0, LX/1zk;->a:Landroid/content/Context;

    .line 354070
    iput-object p2, p0, LX/1zk;->b:LX/0tH;

    .line 354071
    return-void
.end method

.method public static a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)LX/1vs;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDynamicAsset"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 354062
    sget-object v0, LX/22H;->a:[I

    invoke-virtual {p1}, LX/1zo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 354063
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reaction Asset for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354064
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 354065
    :goto_0
    return-object v0

    .line 354066
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 354067
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->p()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/1zk;
    .locals 5

    .prologue
    .line 354022
    sget-object v0, LX/1zk;->d:LX/1zk;

    if-nez v0, :cond_1

    .line 354023
    const-class v1, LX/1zk;

    monitor-enter v1

    .line 354024
    :try_start_0
    sget-object v0, LX/1zk;->d:LX/1zk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 354025
    if-eqz v2, :cond_0

    .line 354026
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 354027
    new-instance p0, LX/1zk;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v4

    check-cast v4, LX/0tH;

    invoke-direct {p0, v3, v4}, LX/1zk;-><init>(Landroid/content/Context;LX/0tH;)V

    .line 354028
    move-object v0, p0

    .line 354029
    sput-object v0, LX/1zk;->d:LX/1zk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354030
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 354031
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 354032
    :cond_1
    sget-object v0, LX/1zk;->d:LX/1zk;

    return-object v0

    .line 354033
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 354034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/22E;LX/1zo;)LX/22F;
    .locals 3

    .prologue
    .line 354052
    sget-object v0, LX/22H;->a:[I

    invoke-virtual {p1}, LX/1zo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 354053
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Resource for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354054
    :pswitch_0
    iget-object v0, p0, LX/22E;->m:LX/22F;

    move-object v0, v0

    .line 354055
    :goto_0
    return-object v0

    .line 354056
    :pswitch_1
    iget-object v0, p0, LX/22E;->n:LX/22F;

    move-object v0, v0

    .line 354057
    goto :goto_0

    .line 354058
    :pswitch_2
    iget-object v0, p0, LX/22E;->o:LX/22F;

    move-object v0, v0

    .line 354059
    goto :goto_0

    .line 354060
    :pswitch_3
    iget-object v0, p0, LX/22E;->p:LX/22F;

    move-object v0, v0

    .line 354061
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 354048
    sget-object v0, LX/22H;->a:[I

    invoke-virtual {p1}, LX/1zo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 354049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reaction Asset for image type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/1zo;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354050
    :pswitch_0
    invoke-static {p0, p1}, LX/1zk;->a(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 354051
    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/1zo;
    .locals 1

    .prologue
    .line 354047
    iget-object v0, p0, LX/1zk;->b:LX/0tH;

    invoke-virtual {v0}, LX/0tH;->f()LX/1zu;

    move-result-object v0

    iget-boolean v0, v0, LX/1zu;->isVectorBased:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/1zo;->VECTOR:LX/1zo;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/1zo;->LARGE:LX/1zo;

    goto :goto_0
.end method

.method public final c(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 354035
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 354036
    iget-object v1, p0, LX/1zk;->c:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 354037
    iget-object v1, p0, LX/1zk;->c:Ljava/io/File;

    .line 354038
    :goto_0
    move-object v1, v1

    .line 354039
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LX/1zo;->getImageDirectory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, LX/1zk;->b(Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;LX/1zo;)Ljava/lang/String;

    move-result-object v1

    .line 354040
    const-string v2, "[^\\w\\d]"

    const-string p0, ""

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 354041
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 354042
    :cond_0
    const/4 v1, 0x0

    .line 354043
    :try_start_0
    iget-object v2, p0, LX/1zk;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    .line 354044
    :goto_1
    if-eqz v2, :cond_1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_2
    iput-object v1, p0, LX/1zk;->c:Ljava/io/File;

    .line 354045
    iget-object v1, p0, LX/1zk;->c:Ljava/io/File;

    goto :goto_0

    :catch_0
    move-object v2, v1

    goto :goto_1

    .line 354046
    :cond_1
    iget-object v1, p0, LX/1zk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    goto :goto_2
.end method
