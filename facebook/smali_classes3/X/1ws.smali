.class public LX/1ws;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/0WV;

.field public final d:LX/0Uh;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/pm/PackageManager;

.field public final g:Ljava/lang/String;

.field public final h:LX/1sd;

.field public final i:LX/0VT;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 347046
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.android.vending"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.google.android.gms"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.market"

    aput-object v2, v0, v1

    sput-object v0, LX/1ws;->a:[Ljava/lang/String;

    .line 347047
    const-class v0, LX/1ws;

    sput-object v0, LX/1ws;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WV;LX/0Uh;LX/0Or;Landroid/content/pm/PackageManager;Ljava/lang/String;LX/1sd;LX/0VT;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0WV;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Landroid/content/pm/PackageManager;",
            "Ljava/lang/String;",
            "LX/1sd;",
            "Lcom/facebook/common/process/ProcessUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347049
    iput-object p1, p0, LX/1ws;->c:LX/0WV;

    .line 347050
    iput-object p2, p0, LX/1ws;->d:LX/0Uh;

    .line 347051
    iput-object p3, p0, LX/1ws;->e:LX/0Or;

    .line 347052
    iput-object p4, p0, LX/1ws;->f:Landroid/content/pm/PackageManager;

    .line 347053
    iput-object p5, p0, LX/1ws;->g:Ljava/lang/String;

    .line 347054
    iput-object p6, p0, LX/1ws;->h:LX/1sd;

    .line 347055
    iput-object p7, p0, LX/1ws;->i:LX/0VT;

    .line 347056
    return-void
.end method

.method public static b(LX/0QB;)LX/1ws;
    .locals 8

    .prologue
    .line 347057
    new-instance v0, LX/1ws;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v1

    check-cast v1, LX/0WV;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v3, 0x2fd

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p0}, LX/1sb;->b(LX/0QB;)LX/1sd;

    move-result-object v6

    check-cast v6, LX/1sd;

    invoke-static {p0}, LX/0VT;->a(LX/0QB;)LX/0VT;

    move-result-object v7

    check-cast v7, LX/0VT;

    invoke-direct/range {v0 .. v7}, LX/1ws;-><init>(LX/0WV;LX/0Uh;LX/0Or;Landroid/content/pm/PackageManager;Ljava/lang/String;LX/1sd;LX/0VT;)V

    .line 347058
    return-object v0
.end method
