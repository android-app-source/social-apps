.class public LX/1wQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/1Fg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Fg",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Fg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Fg",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 345861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345862
    iput-object p1, p0, LX/1wQ;->a:LX/1Fg;

    .line 345863
    return-void
.end method


# virtual methods
.method public final a()LX/4dq;
    .locals 7

    .prologue
    .line 345864
    iget-object v2, p0, LX/1wQ;->a:LX/1Fg;

    monitor-enter v2

    .line 345865
    :try_start_0
    new-instance v3, LX/4dq;

    iget-object v0, p0, LX/1wQ;->a:LX/1Fg;

    invoke-virtual {v0}, LX/1Fg;->b()I

    move-result v0

    iget-object v1, p0, LX/1wQ;->a:LX/1Fg;

    invoke-virtual {v1}, LX/1Fg;->d()I

    move-result v1

    iget-object v4, p0, LX/1wQ;->a:LX/1Fg;

    iget-object v4, v4, LX/1Fg;->e:LX/1HR;

    invoke-direct {v3, v0, v1, v4}, LX/4dq;-><init>(IILX/1HR;)V

    .line 345866
    iget-object v0, p0, LX/1wQ;->a:LX/1Fg;

    iget-object v0, v0, LX/1Fg;->c:LX/1HP;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1HP;->a(Lcom/android/internal/util/Predicate;)Ljava/util/ArrayList;

    move-result-object v0

    .line 345867
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 345868
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lX;

    .line 345869
    new-instance v4, LX/4dr;

    iget-object v5, v0, LX/1lX;->a:Ljava/lang/Object;

    iget-object v6, v0, LX/1lX;->b:LX/1FJ;

    invoke-direct {v4, v5, v6}, LX/4dr;-><init>(Ljava/lang/Object;LX/1FJ;)V

    .line 345870
    iget v0, v0, LX/1lX;->c:I

    if-lez v0, :cond_0

    .line 345871
    iget-object v0, v3, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 345872
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 345873
    :cond_0
    :try_start_1
    iget-object v0, v3, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 345874
    :cond_1
    iget-object v0, p0, LX/1wQ;->a:LX/1Fg;

    iget-object v0, v0, LX/1Fg;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 345875
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 345876
    iget-object v1, v3, LX/4dq;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 345877
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method
