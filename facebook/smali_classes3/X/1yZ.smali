.class public LX/1yZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0tK;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 350783
    new-instance v0, LX/1ya;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1ya;-><init>(I)V

    sput-object v0, LX/1yZ;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/0tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 350784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350785
    iput-object p1, p0, LX/1yZ;->b:LX/0tK;

    .line 350786
    return-void
.end method

.method public static a(LX/0QB;)LX/1yZ;
    .locals 4

    .prologue
    .line 350787
    const-class v1, LX/1yZ;

    monitor-enter v1

    .line 350788
    :try_start_0
    sget-object v0, LX/1yZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 350789
    sput-object v2, LX/1yZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 350790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 350792
    new-instance p0, LX/1yZ;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v3

    check-cast v3, LX/0tK;

    invoke-direct {p0, v3}, LX/1yZ;-><init>(LX/0tK;)V

    .line 350793
    move-object v0, p0

    .line 350794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 350795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 350796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 350797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
