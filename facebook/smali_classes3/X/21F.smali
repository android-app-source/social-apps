.class public LX/21F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:LX/21G;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:LX/1PT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 356828
    const-class v0, LX/21F;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/21F;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/1PT;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 356829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356830
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/21F;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/21F;->c:Ljava/lang/String;

    .line 356831
    iput-object p2, p0, LX/21F;->d:LX/1PT;

    .line 356832
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 356822
    iget-object v0, p0, LX/21F;->b:LX/21G;

    iget-object v1, p0, LX/21F;->d:LX/1PT;

    .line 356823
    new-instance p0, LX/21H;

    invoke-static {v0}, LX/20S;->a(LX/0QB;)LX/20S;

    move-result-object v2

    check-cast v2, LX/20S;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v3

    check-cast v3, LX/0wW;

    invoke-direct {p0, v2, v3, v1}, LX/21H;-><init>(LX/20S;LX/0wW;LX/1PT;)V

    .line 356824
    const/16 v2, 0x2a

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v2

    check-cast v2, LX/1VF;

    .line 356825
    iput-object v3, p0, LX/21H;->a:LX/0Ot;

    iput-object v2, p0, LX/21H;->b:LX/1VF;

    .line 356826
    move-object v0, p0

    .line 356827
    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 356821
    iget-object v0, p0, LX/21F;->c:Ljava/lang/String;

    return-object v0
.end method
