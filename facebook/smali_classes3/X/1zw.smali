.class public LX/1zw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1zx;


# static fields
.field public static final a:Landroid/graphics/drawable/Drawable;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Ljava/io/File;

.field private e:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 354287
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, LX/1zw;->a:Landroid/graphics/drawable/Drawable;

    .line 354288
    const-class v0, LX/1zw;

    sput-object v0, LX/1zw;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 354289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354290
    iput-object p1, p0, LX/1zw;->c:Landroid/content/Context;

    .line 354291
    iput-object p2, p0, LX/1zw;->d:Ljava/io/File;

    .line 354292
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 354271
    iget-object v0, p0, LX/1zw;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 354272
    :try_start_0
    iget-object v0, p0, LX/1zw;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 354273
    sget-object v0, LX/1zw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 354274
    :goto_0
    move-object v0, v0

    .line 354275
    iput-object v0, p0, LX/1zw;->e:Landroid/graphics/drawable/Drawable;

    .line 354276
    :cond_0
    iget-object v0, p0, LX/1zw;->e:Landroid/graphics/drawable/Drawable;

    return-object v0

    .line 354277
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, LX/1zw;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 354278
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 354279
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 354280
    if-eqz v1, :cond_2

    .line 354281
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, LX/1zw;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 354282
    :catch_0
    move-exception v0

    .line 354283
    sget-object v1, LX/1zw;->b:Ljava/lang/Class;

    const-string v2, "Failed to load image - "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 354284
    :cond_2
    :goto_1
    sget-object v0, LX/1zw;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 354285
    :catch_1
    move-exception v0

    .line 354286
    sget-object v1, LX/1zw;->b:Ljava/lang/Class;

    const-string v2, "Failed to load image - "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 354270
    invoke-virtual {p0}, LX/1zw;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 354269
    iget-object v0, p0, LX/1zw;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method
