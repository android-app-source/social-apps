.class public LX/20S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final z:Ljava/lang/Object;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0wT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/Float;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/Float;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354910
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/20S;->z:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 354904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354905
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 354906
    iput-object v0, p0, LX/20S;->a:LX/0Ot;

    .line 354907
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 354908
    iput-object v0, p0, LX/20S;->b:LX/0Ot;

    .line 354909
    return-void
.end method

.method public static a(LX/0QB;)LX/20S;
    .locals 8

    .prologue
    .line 354873
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 354874
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 354875
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 354876
    if-nez v1, :cond_0

    .line 354877
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354878
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 354879
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 354880
    sget-object v1, LX/20S;->z:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 354881
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 354882
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 354883
    :cond_1
    if-nez v1, :cond_4

    .line 354884
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 354885
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 354886
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 354887
    new-instance v1, LX/20S;

    invoke-direct {v1}, LX/20S;-><init>()V

    .line 354888
    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 354889
    iput-object v7, v1, LX/20S;->a:LX/0Ot;

    iput-object p0, v1, LX/20S;->b:LX/0Ot;

    .line 354890
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 354891
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 354892
    if-nez v1, :cond_2

    .line 354893
    sget-object v0, LX/20S;->z:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20S;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 354894
    :goto_1
    if-eqz v0, :cond_3

    .line 354895
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 354896
    :goto_3
    check-cast v0, LX/20S;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 354897
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 354898
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 354899
    :catchall_1
    move-exception v0

    .line 354900
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 354901
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 354902
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 354903
    :cond_2
    :try_start_8
    sget-object v0, LX/20S;->z:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20S;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(LX/1zt;)Z
    .locals 1

    .prologue
    .line 354911
    if-eqz p0, :cond_0

    sget-object v0, LX/1zt;->d:LX/1zt;

    if-eq p0, v0, :cond_0

    .line 354912
    iget v0, p0, LX/1zt;->e:I

    move v0, v0

    .line 354913
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/20S;)Z
    .locals 3

    .prologue
    .line 354870
    iget-object v0, p0, LX/20S;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 354871
    iget-object v0, p0, LX/20S;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/0fe;->aX:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/20S;->c:Ljava/lang/Boolean;

    .line 354872
    :cond_0
    iget-object v0, p0, LX/20S;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static r(LX/20S;)Z
    .locals 3

    .prologue
    .line 354866
    iget-object v0, p0, LX/20S;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 354867
    iget-object v0, p0, LX/20S;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/20S;->o(LX/20S;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-short v1, LX/0fe;->bm:S

    :goto_0
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/20S;->n:Ljava/lang/Boolean;

    .line 354868
    :cond_0
    iget-object v0, p0, LX/20S;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 354869
    :cond_1
    sget-short v1, LX/0wj;->v:S

    goto :goto_0
.end method

.method public static s(LX/20S;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 354862
    iget-object v0, p0, LX/20S;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 354863
    iget-object v0, p0, LX/20S;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/20S;->o(LX/20S;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-char v1, LX/0fe;->aZ:C

    move v2, v1

    :goto_0
    iget-object v1, p0, LX/20S;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f080ffc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/20S;->t:Ljava/lang/String;

    .line 354864
    :cond_0
    iget-object v0, p0, LX/20S;->t:Ljava/lang/String;

    return-object v0

    .line 354865
    :cond_1
    sget-char v1, LX/0wj;->j:C

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 3

    .prologue
    .line 354858
    iget-object v0, p0, LX/20S;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 354859
    iget-object v0, p0, LX/20S;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/20S;->o(LX/20S;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-short v1, LX/0fe;->aR:S

    :goto_0
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/20S;->e:Ljava/lang/Boolean;

    .line 354860
    :cond_0
    iget-object v0, p0, LX/20S;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 354861
    :cond_1
    sget-short v1, LX/0wj;->a:S

    goto :goto_0
.end method
