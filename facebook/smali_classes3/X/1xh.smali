.class public LX/1xh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile f:LX/1xh;


# instance fields
.field public final b:LX/0ad;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field private e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 349262
    const-class v0, LX/1xh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1xh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 349263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349264
    iput-object v0, p0, LX/1xh;->c:Ljava/lang/Integer;

    .line 349265
    iput-object v0, p0, LX/1xh;->d:Ljava/lang/Integer;

    .line 349266
    iput-object v0, p0, LX/1xh;->e:LX/0Rf;

    .line 349267
    iput-object p1, p0, LX/1xh;->b:LX/0ad;

    .line 349268
    return-void
.end method

.method public static a(Ljava/lang/String;I)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 349269
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 349270
    :goto_0
    return p1

    .line 349271
    :catch_0
    move-exception v0

    .line 349272
    sget-object v1, LX/1xh;->a:Ljava/lang/String;

    const-string v2, "Error while decoding : %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p0, v3, v4

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349273
    const/16 v0, 0x10

    :try_start_1
    invoke-static {p0, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    long-to-int p1, v0

    goto :goto_0

    .line 349274
    :catch_1
    move-exception v0

    .line 349275
    sget-object v1, LX/1xh;->a:Ljava/lang/String;

    const-string v2, "Error while parsing : %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p0, v3, v4

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/1xh;
    .locals 4

    .prologue
    .line 349276
    sget-object v0, LX/1xh;->f:LX/1xh;

    if-nez v0, :cond_1

    .line 349277
    const-class v1, LX/1xh;

    monitor-enter v1

    .line 349278
    :try_start_0
    sget-object v0, LX/1xh;->f:LX/1xh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 349279
    if-eqz v2, :cond_0

    .line 349280
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 349281
    new-instance p0, LX/1xh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/1xh;-><init>(LX/0ad;)V

    .line 349282
    move-object v0, p0

    .line 349283
    sput-object v0, LX/1xh;->f:LX/1xh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349284
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 349285
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349286
    :cond_1
    sget-object v0, LX/1xh;->f:LX/1xh;

    return-object v0

    .line 349287
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 349288
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 349289
    iget-object v0, p0, LX/1xh;->b:LX/0ad;

    sget-short v1, LX/0ob;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final g()LX/0Rf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349290
    iget-object v0, p0, LX/1xh;->e:LX/0Rf;

    if-eqz v0, :cond_0

    .line 349291
    iget-object v0, p0, LX/1xh;->e:LX/0Rf;

    .line 349292
    :goto_0
    return-object v0

    .line 349293
    :cond_0
    iget-object v0, p0, LX/1xh;->b:LX/0ad;

    sget-char v1, LX/0ob;->j:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349294
    if-nez v0, :cond_1

    .line 349295
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 349296
    goto :goto_0

    .line 349297
    :cond_1
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 349298
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, ","

    invoke-direct {v2, v0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 349299
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 349300
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_1

    .line 349301
    :cond_2
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/1xh;->e:LX/0Rf;

    .line 349302
    iget-object v0, p0, LX/1xh;->e:LX/0Rf;

    goto :goto_0
.end method
