.class public LX/20g;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static B:LX/0Xm;

.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final A:LX/20x;

.field public final b:LX/1EQ;

.field private final c:LX/20P;

.field public final d:LX/20h;

.field public final e:LX/0bH;

.field public final f:LX/20n;

.field public final g:LX/20o;

.field public final h:LX/20p;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0iA;

.field public final l:LX/20l;

.field public final m:LX/0tH;

.field public final n:LX/20m;

.field public final o:LX/1zf;

.field public final p:LX/1WR;

.field private final q:LX/14w;

.field public final r:LX/20r;

.field public final s:LX/20s;

.field public final t:LX/20v;

.field public final u:LX/20q;

.field public final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9FJ;",
            ">;"
        }
    .end annotation
.end field

.field public final w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9D6;",
            ">;"
        }
    .end annotation
.end field

.field private final x:LX/0wW;

.field private final y:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field private final z:LX/20w;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 355141
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/20g;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/20P;LX/20h;LX/0Ot;LX/0iA;LX/20l;LX/20m;LX/1zf;LX/0bH;LX/20n;LX/20o;LX/20p;LX/0Ot;LX/0tH;LX/20q;LX/1WR;LX/14w;LX/1EQ;LX/20r;LX/20s;LX/20v;LX/0Ot;LX/0Ot;LX/0wW;LX/0Or;LX/20w;LX/20x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20P;",
            "LX/20h;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0iA;",
            "LX/20l;",
            "LX/20m;",
            "LX/1zf;",
            "LX/0bH;",
            "LX/20n;",
            "LX/20o;",
            "LX/20p;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0tH;",
            "LX/20q;",
            "LX/1WR;",
            "LX/14w;",
            "LX/1EQ;",
            "LX/20r;",
            "LX/20s;",
            "LX/20v;",
            "LX/0Ot",
            "<",
            "LX/9D6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9FJ;",
            ">;",
            "LX/0wW;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/20w;",
            "LX/20x;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 355113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355114
    iput-object p1, p0, LX/20g;->c:LX/20P;

    .line 355115
    iput-object p2, p0, LX/20g;->d:LX/20h;

    .line 355116
    iput-object p3, p0, LX/20g;->j:LX/0Ot;

    .line 355117
    iput-object p4, p0, LX/20g;->k:LX/0iA;

    .line 355118
    iput-object p5, p0, LX/20g;->l:LX/20l;

    .line 355119
    iput-object p10, p0, LX/20g;->g:LX/20o;

    .line 355120
    iput-object p13, p0, LX/20g;->m:LX/0tH;

    .line 355121
    iput-object p14, p0, LX/20g;->u:LX/20q;

    .line 355122
    iput-object p6, p0, LX/20g;->n:LX/20m;

    .line 355123
    iput-object p7, p0, LX/20g;->o:LX/1zf;

    .line 355124
    iput-object p8, p0, LX/20g;->e:LX/0bH;

    .line 355125
    iput-object p9, p0, LX/20g;->f:LX/20n;

    .line 355126
    iput-object p11, p0, LX/20g;->h:LX/20p;

    .line 355127
    iput-object p12, p0, LX/20g;->i:LX/0Ot;

    .line 355128
    move-object/from16 v0, p15

    iput-object v0, p0, LX/20g;->p:LX/1WR;

    .line 355129
    move-object/from16 v0, p16

    iput-object v0, p0, LX/20g;->q:LX/14w;

    .line 355130
    move-object/from16 v0, p21

    iput-object v0, p0, LX/20g;->w:LX/0Ot;

    .line 355131
    move-object/from16 v0, p17

    iput-object v0, p0, LX/20g;->b:LX/1EQ;

    .line 355132
    move-object/from16 v0, p18

    iput-object v0, p0, LX/20g;->r:LX/20r;

    .line 355133
    move-object/from16 v0, p19

    iput-object v0, p0, LX/20g;->s:LX/20s;

    .line 355134
    move-object/from16 v0, p20

    iput-object v0, p0, LX/20g;->t:LX/20v;

    .line 355135
    move-object/from16 v0, p22

    iput-object v0, p0, LX/20g;->v:LX/0Ot;

    .line 355136
    move-object/from16 v0, p23

    iput-object v0, p0, LX/20g;->x:LX/0wW;

    .line 355137
    move-object/from16 v0, p24

    iput-object v0, p0, LX/20g;->y:LX/0Or;

    .line 355138
    move-object/from16 v0, p25

    iput-object v0, p0, LX/20g;->z:LX/20w;

    .line 355139
    move-object/from16 v0, p26

    iput-object v0, p0, LX/20g;->A:LX/20x;

    .line 355140
    return-void
.end method

.method public static a(LX/0QB;)LX/20g;
    .locals 3

    .prologue
    .line 355105
    const-class v1, LX/20g;

    monitor-enter v1

    .line 355106
    :try_start_0
    sget-object v0, LX/20g;->B:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 355107
    sput-object v2, LX/20g;->B:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 355108
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355109
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/20g;->b(LX/0QB;)LX/20g;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 355110
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355111
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 355112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Ljava/lang/String;)LX/21H;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;"
        }
    .end annotation

    .prologue
    .line 355051
    move-object v0, p2

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/20g;->A:LX/20x;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    .line 355052
    new-instance p2, LX/21F;

    invoke-direct {p2, p3, v2}, LX/21F;-><init>(Ljava/lang/String;LX/1PT;)V

    .line 355053
    const-class p0, LX/21G;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/21G;

    .line 355054
    iput-object p0, p2, LX/21F;->b:LX/21G;

    .line 355055
    move-object v2, p2

    .line 355056
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 355057
    check-cast v1, LX/0jW;

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21H;

    .line 355058
    if-eqz v0, :cond_0

    .line 355059
    iget-boolean v1, v0, LX/21H;->e:Z

    if-nez v1, :cond_1

    .line 355060
    :cond_0
    :goto_0
    return-object v0

    .line 355061
    :cond_1
    invoke-static {p1}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iput-boolean v1, v0, LX/21H;->m:Z

    .line 355062
    iget-object v1, v0, LX/21H;->b:LX/1VF;

    invoke-virtual {v1, p1}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    iput-boolean v1, v0, LX/21H;->l:Z

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 355104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)Ljava/util/EnumMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "TE;)",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;"
        }
    .end annotation

    .prologue
    .line 355097
    move-object v0, p2

    check-cast v0, LX/1Pr;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v0, p1, v1}, LX/212;->a(LX/1Pr;Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/util/EnumMap;

    move-result-object v1

    .line 355098
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v2

    invoke-static {p1}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    iget-object v4, p0, LX/20g;->y:LX/0Or;

    invoke-static {v0, v2, v3, v1, v4}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 355099
    invoke-virtual {v1}, Ljava/util/EnumMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    .line 355100
    const/4 v3, 0x1

    .line 355101
    iput-boolean v3, v0, LX/215;->d:Z

    .line 355102
    goto :goto_0

    .line 355103
    :cond_0
    return-object v1
.end method

.method public static synthetic a(LX/20g;Landroid/view/View;LX/1zt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 9

    .prologue
    .line 355093
    iget-object v1, p0, LX/20g;->k:LX/0iA;

    iget-object v2, p0, LX/20g;->l:LX/20l;

    iget-object v3, p0, LX/20g;->n:LX/20m;

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    iget-object v4, p0, LX/20g;->t:LX/20v;

    .line 355094
    iget-object v6, v4, LX/20v;->c:LX/0ad;

    sget v7, LX/98c;->d:I

    const/16 v8, 0x729

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    move v8, v6

    .line 355095
    move-object v4, p3

    move-object v6, p1

    move-object v7, p2

    invoke-static/range {v1 .. v8}, LX/21N;->a(LX/0iA;LX/20l;LX/20m;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/1zt;I)Z

    move-result v1

    move v0, v1

    .line 355096
    return v0
.end method

.method private static b(LX/0QB;)LX/20g;
    .locals 29

    .prologue
    .line 355091
    new-instance v2, LX/20g;

    const-class v3, LX/20P;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/20P;

    invoke-static/range {p0 .. p0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v4

    check-cast v4, LX/20h;

    const/16 v5, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v6

    check-cast v6, LX/0iA;

    invoke-static/range {p0 .. p0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v7

    check-cast v7, LX/20l;

    invoke-static/range {p0 .. p0}, LX/20m;->a(LX/0QB;)LX/20m;

    move-result-object v8

    check-cast v8, LX/20m;

    invoke-static/range {p0 .. p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v9

    check-cast v9, LX/1zf;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/20n;->a(LX/0QB;)LX/20n;

    move-result-object v11

    check-cast v11, LX/20n;

    invoke-static/range {p0 .. p0}, LX/20o;->a(LX/0QB;)LX/20o;

    move-result-object v12

    check-cast v12, LX/20o;

    invoke-static/range {p0 .. p0}, LX/20p;->a(LX/0QB;)LX/20p;

    move-result-object v13

    check-cast v13, LX/20p;

    const/16 v14, 0x97

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v15

    check-cast v15, LX/0tH;

    invoke-static/range {p0 .. p0}, LX/20q;->a(LX/0QB;)LX/20q;

    move-result-object v16

    check-cast v16, LX/20q;

    invoke-static/range {p0 .. p0}, LX/1WR;->a(LX/0QB;)LX/1WR;

    move-result-object v17

    check-cast v17, LX/1WR;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v18

    check-cast v18, LX/14w;

    invoke-static/range {p0 .. p0}, LX/1EQ;->a(LX/0QB;)LX/1EQ;

    move-result-object v19

    check-cast v19, LX/1EQ;

    invoke-static/range {p0 .. p0}, LX/20r;->a(LX/0QB;)LX/20r;

    move-result-object v20

    check-cast v20, LX/20r;

    invoke-static/range {p0 .. p0}, LX/20s;->a(LX/0QB;)LX/20s;

    move-result-object v21

    check-cast v21, LX/20s;

    invoke-static/range {p0 .. p0}, LX/20v;->a(LX/0QB;)LX/20v;

    move-result-object v22

    check-cast v22, LX/20v;

    const/16 v23, 0x1d95

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x1dab

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v25

    check-cast v25, LX/0wW;

    const/16 v26, 0x13a4

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v26

    const-class v27, LX/20w;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/20w;

    const-class v28, LX/20x;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/20x;

    invoke-direct/range {v2 .. v28}, LX/20g;-><init>(LX/20P;LX/20h;LX/0Ot;LX/0iA;LX/20l;LX/20m;LX/1zf;LX/0bH;LX/20n;LX/20o;LX/20p;LX/0Ot;LX/0tH;LX/20q;LX/1WR;LX/14w;LX/1EQ;LX/20r;LX/20s;LX/20v;LX/0Ot;LX/0Ot;LX/0wW;LX/0Or;LX/20w;LX/20x;)V

    .line 355092
    return-object v2
.end method

.method public static synthetic b(LX/20g;Landroid/view/View;LX/1zt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 11

    .prologue
    .line 355089
    iget-object v1, p0, LX/20g;->k:LX/0iA;

    iget-object v2, p0, LX/20g;->l:LX/20l;

    iget-object v3, p0, LX/20g;->m:LX/0tH;

    sget-object v4, LX/20g;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v5, p0, LX/20g;->n:LX/20m;

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    move-object v6, p3

    move-object v7, p4

    move-object v9, p1

    move-object v10, p2

    invoke-static/range {v1 .. v10}, LX/21N;->a(LX/0iA;LX/20l;LX/0tH;Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/20m;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/1zt;)Z

    move-result v1

    move v0, v1

    .line 355090
    return v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;ZLX/21A;Ljava/lang/String;LX/21H;)LX/20Z;
    .locals 8
    .param p6    # LX/21H;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;Z",
            "LX/21A;",
            "Ljava/lang/String;",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;",
            ")",
            "LX/20Z;"
        }
    .end annotation

    .prologue
    .line 355088
    new-instance v0, LX/21K;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p4

    move-object v5, p6

    move v6, p3

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/21K;-><init>(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/21A;LX/21H;ZLX/1Po;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/20b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/20b;"
        }
    .end annotation

    .prologue
    .line 355080
    iget-object v0, p0, LX/20g;->c:LX/20P;

    new-instance v1, LX/21J;

    invoke-direct {v1, p0, p1}, LX/21J;-><init>(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, LX/20P;->a(LX/20E;)LX/20b;

    move-result-object v0

    .line 355081
    iget-object v1, p0, LX/20g;->u:LX/20q;

    .line 355082
    iget-object v2, v1, LX/20q;->b:Ljava/lang/Integer;

    if-nez v2, :cond_0

    .line 355083
    iget-object v2, v1, LX/20q;->a:LX/0ad;

    sget p0, LX/0wn;->G:I

    const/16 p1, 0xfa

    invoke-interface {v2, p0, p1}, LX/0ad;->a(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, LX/20q;->b:Ljava/lang/Integer;

    .line 355084
    :cond_0
    iget-object v2, v1, LX/20q;->b:Ljava/lang/Integer;

    move-object v1, v2

    .line 355085
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 355086
    iput v1, v0, LX/20b;->f:I

    .line 355087
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/20z;
    .locals 4

    .prologue
    .line 355079
    iget-object v1, p0, LX/20g;->z:LX/20w;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v3, "native_newsfeed"

    invoke-virtual {v1, v2, v0, v3}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/21H;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "Lcom/facebook/feedplugins/base/footer/ui/progressiveufi/ProgressiveUfiState;"
        }
    .end annotation

    .prologue
    .line 355076
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 355077
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v0, v1}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/lang/String;

    move-result-object v0

    .line 355078
    invoke-static {p0, p1, p2, v0}, LX/20g;->a(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Ljava/lang/String;)LX/21H;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Z)LX/21I;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;Z)",
            "LX/21I;"
        }
    .end annotation

    .prologue
    .line 355064
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    move-object v14, v3

    check-cast v14, Lcom/facebook/graphql/model/GraphQLStory;

    .line 355065
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/20z;

    move-result-object v16

    .line 355066
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v14, v1}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)Ljava/util/EnumMap;

    move-result-object v17

    .line 355067
    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-static {v3}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v8

    .line 355068
    invoke-static {}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->newBuilder()LX/21A;

    move-result-object v3

    invoke-static/range {p1 .. p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/21A;->a(LX/162;)LX/21A;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-static {v4}, LX/217;->b(LX/1PT;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/21A;->a(Ljava/lang/String;)LX/21A;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/21A;->b(Ljava/lang/String;)LX/21A;

    move-result-object v7

    .line 355069
    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-static {v14, v3}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/1PT;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v3, p2

    .line 355070
    check-cast v3, LX/1Pr;

    new-instance v5, LX/21E;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/20g;->x:LX/0wW;

    invoke-direct {v5, v4, v6}, LX/21E;-><init>(Ljava/lang/String;LX/0wW;)V

    invoke-interface {v3, v5, v14}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/0wd;

    .line 355071
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v4}, LX/20g;->a(LX/20g;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Ljava/lang/String;)LX/21H;

    move-result-object v9

    .line 355072
    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 355073
    move-object/from16 v0, p0

    iget-object v4, v0, LX/20g;->o:LX/1zf;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v4, v3}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v19

    .line 355074
    new-instance v10, LX/21I;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/20g;->u:LX/20q;

    invoke-virtual {v3}, LX/20q;->a()Z

    move-result v11

    invoke-virtual/range {p0 .. p1}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/20b;

    move-result-object v12

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-virtual/range {v3 .. v9}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;ZLX/21A;Ljava/lang/String;LX/21H;)LX/20Z;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v7, v8, v9}, LX/20g;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/21A;Ljava/lang/String;LX/21H;)LX/21M;

    move-result-object v14

    invoke-interface/range {p2 .. p2}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-static {v3}, LX/21N;->a(LX/1PT;)LX/20I;

    move-result-object v18

    move-object/from16 v20, v9

    invoke-direct/range {v10 .. v20}, LX/21I;-><init>(ZLX/20b;LX/20Z;LX/21M;LX/0wd;LX/20z;Ljava/util/EnumMap;LX/20I;LX/0Px;LX/21H;)V

    return-object v10

    .line 355075
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/21A;Ljava/lang/String;LX/21H;)LX/21M;
    .locals 6

    .prologue
    .line 355063
    new-instance v0, LX/21L;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/21L;-><init>(LX/20g;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;LX/21A;LX/21H;)V

    return-object v0
.end method
