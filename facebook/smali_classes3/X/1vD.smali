.class public final enum LX/1vD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1vD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1vD;

.field public static final enum ELIGIBLE:LX/1vD;

.field public static final enum FORCE:LX/1vD;

.field public static final enum INELIGIBLE:LX/1vD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 342091
    new-instance v0, LX/1vD;

    const-string v1, "INELIGIBLE"

    invoke-direct {v0, v1, v2}, LX/1vD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vD;->INELIGIBLE:LX/1vD;

    .line 342092
    new-instance v0, LX/1vD;

    const-string v1, "ELIGIBLE"

    invoke-direct {v0, v1, v3}, LX/1vD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vD;->ELIGIBLE:LX/1vD;

    .line 342093
    new-instance v0, LX/1vD;

    const-string v1, "FORCE"

    invoke-direct {v0, v1, v4}, LX/1vD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/1vD;->FORCE:LX/1vD;

    .line 342094
    const/4 v0, 0x3

    new-array v0, v0, [LX/1vD;

    sget-object v1, LX/1vD;->INELIGIBLE:LX/1vD;

    aput-object v1, v0, v2

    sget-object v1, LX/1vD;->ELIGIBLE:LX/1vD;

    aput-object v1, v0, v3

    sget-object v1, LX/1vD;->FORCE:LX/1vD;

    aput-object v1, v0, v4

    sput-object v0, LX/1vD;->$VALUES:[LX/1vD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 342095
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convertString(Ljava/lang/String;)LX/1vD;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 342096
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, LX/1vD;->valueOf(Ljava/lang/String;)LX/1vD;

    move-result-object v0

    .line 342097
    :goto_0
    return-object v0

    .line 342098
    :cond_0
    sget-object v0, LX/1vD;->INELIGIBLE:LX/1vD;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342099
    :catch_0
    sget-object v0, LX/1vD;->INELIGIBLE:LX/1vD;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/1vD;
    .locals 1

    .prologue
    .line 342100
    const-class v0, LX/1vD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1vD;

    return-object v0
.end method

.method public static values()[LX/1vD;
    .locals 1

    .prologue
    .line 342101
    sget-object v0, LX/1vD;->$VALUES:[LX/1vD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1vD;

    return-object v0
.end method
