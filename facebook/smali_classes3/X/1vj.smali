.class public LX/1vj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/1vj;


# instance fields
.field public final a:LX/1vl;


# direct methods
.method public constructor <init>(LX/1vl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344425
    iput-object p1, p0, LX/1vj;->a:LX/1vl;

    .line 344426
    return-void
.end method

.method public static a(LX/0QB;)LX/1vj;
    .locals 4

    .prologue
    .line 344411
    sget-object v0, LX/1vj;->b:LX/1vj;

    if-nez v0, :cond_1

    .line 344412
    const-class v1, LX/1vj;

    monitor-enter v1

    .line 344413
    :try_start_0
    sget-object v0, LX/1vj;->b:LX/1vj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 344414
    if-eqz v2, :cond_0

    .line 344415
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 344416
    new-instance p0, LX/1vj;

    invoke-static {v0}, LX/1vk;->a(LX/0QB;)LX/1vk;

    move-result-object v3

    check-cast v3, LX/1vl;

    invoke-direct {p0, v3}, LX/1vj;-><init>(LX/1vl;)V

    .line 344417
    move-object v0, p0

    .line 344418
    sput-object v0, LX/1vj;->b:LX/1vj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344419
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 344420
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344421
    :cond_1
    sget-object v0, LX/1vj;->b:LX/1vj;

    return-object v0

    .line 344422
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 344423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344410
    iget-object v0, p0, LX/1vj;->a:LX/1vl;

    invoke-interface {v0}, LX/1vl;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfk;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 344408
    iget-object v0, p0, LX/1vj;->a:LX/1vl;

    invoke-interface {v0, p1}, LX/1vl;->a(Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)LX/Cfm;

    move-result-object v0

    .line 344409
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/Cfm;->c()LX/Cfk;

    move-result-object v0

    goto :goto_0
.end method
