.class public LX/20p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/0bH;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1EQ;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final f:LX/0Zb;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/17Q;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Aov;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ipc/composer/launch/ShareComposerLauncher;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/1AM;

.field private final m:LX/0pf;


# direct methods
.method public constructor <init>(LX/0bH;LX/0Or;LX/1EQ;LX/0Or;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/0Ot;LX/17Q;LX/0Ot;LX/0Ot;LX/0Ot;LX/1AM;LX/0pf;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/analytics/IsStorySharingAnalyticsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1EQ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/17Q;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Aov;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/ipc/composer/launch/ShareComposerLauncher;",
            ">;",
            "LX/1AM;",
            "LX/0pf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 356078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356079
    iput-object p1, p0, LX/20p;->a:LX/0bH;

    .line 356080
    iput-object p2, p0, LX/20p;->b:LX/0Or;

    .line 356081
    iput-object p3, p0, LX/20p;->c:LX/1EQ;

    .line 356082
    iput-object p4, p0, LX/20p;->d:LX/0Or;

    .line 356083
    iput-object p5, p0, LX/20p;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 356084
    iput-object p6, p0, LX/20p;->f:LX/0Zb;

    .line 356085
    iput-object p7, p0, LX/20p;->g:LX/0Ot;

    .line 356086
    iput-object p8, p0, LX/20p;->h:LX/17Q;

    .line 356087
    iput-object p9, p0, LX/20p;->i:LX/0Ot;

    .line 356088
    iput-object p10, p0, LX/20p;->j:LX/0Ot;

    .line 356089
    iput-object p11, p0, LX/20p;->k:LX/0Ot;

    .line 356090
    iput-object p12, p0, LX/20p;->l:LX/1AM;

    .line 356091
    iput-object p13, p0, LX/20p;->m:LX/0pf;

    .line 356092
    return-void
.end method

.method public static a(LX/0QB;)LX/20p;
    .locals 3

    .prologue
    .line 356093
    const-class v1, LX/20p;

    monitor-enter v1

    .line 356094
    :try_start_0
    sget-object v0, LX/20p;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 356095
    sput-object v2, LX/20p;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 356096
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356097
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/20p;->b(LX/0QB;)LX/20p;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 356098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/20p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 356100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6

    .prologue
    .line 356101
    iget-object v0, p0, LX/20p;->m:LX/0pf;

    invoke-virtual {v0, p2}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 356102
    if-eqz v1, :cond_0

    .line 356103
    iget-object v0, v1, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 356104
    if-eqz v0, :cond_0

    .line 356105
    iget-object v0, v1, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 356106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356107
    iget-object v0, v1, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 356108
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/20p;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 356109
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 356110
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 356111
    iget-object v0, v1, LX/1g0;->p:Lcom/facebook/graphql/model/GraphQLActor;

    move-object v0, v0

    .line 356112
    new-instance v2, LX/89I;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    .line 356113
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 356114
    move-object v2, v2

    .line 356115
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    .line 356116
    iput-object v3, v2, LX/89I;->d:Ljava/lang/String;

    .line 356117
    move-object v2, v2

    .line 356118
    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    .line 356119
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    .line 356120
    iget-object v3, v1, LX/1g0;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v3

    .line 356121
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    .line 356122
    invoke-virtual {p1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 356123
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/20p;
    .locals 14

    .prologue
    .line 356124
    new-instance v0, LX/20p;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v1

    check-cast v1, LX/0bH;

    const/16 v2, 0x1492

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/1EQ;->b(LX/0QB;)LX/1EQ;

    move-result-object v3

    check-cast v3, LX/1EQ;

    const/16 v4, 0x19e

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const/16 v7, 0x97

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v8

    check-cast v8, LX/17Q;

    const/16 v9, 0x259

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2179

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2581

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v12

    check-cast v12, LX/1AM;

    invoke-static {p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v13

    check-cast v13, LX/0pf;

    invoke-direct/range {v0 .. v13}, LX/20p;-><init>(LX/0bH;LX/0Or;LX/1EQ;LX/0Or;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Zb;LX/0Ot;LX/17Q;LX/0Ot;LX/0Ot;LX/0Ot;LX/1AM;LX/0pf;)V

    .line 356125
    return-object v0
.end method

.method public static c(LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 356126
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356127
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356128
    invoke-static {p1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 356129
    iget-object v1, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 356130
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356131
    invoke-static {v5}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 356132
    iget-object v7, p0, LX/20p;->a:LX/0bH;

    new-instance v8, LX/1Zb;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    :goto_0
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-direct {v8, v9, v4, v2}, LX/1Zb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, LX/0b4;->a(LX/0b7;)V

    .line 356133
    iget-object v2, p0, LX/20p;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356134
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    .line 356135
    iget-object v4, p0, LX/20p;->c:LX/1EQ;

    const-string v6, "newsfeed_ufi"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, LX/20p;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 356136
    iget-object v8, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v8

    .line 356137
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v6, v7, v1, v2}, LX/1EQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356138
    :cond_0
    iget-object v1, p0, LX/20p;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p3}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v1

    .line 356139
    iget-object v2, p0, LX/20p;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const-string v4, "shareButton"

    invoke-interface {v2, v5, v1, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 356140
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-static {v1, v4}, LX/17Q;->b(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 356141
    invoke-static {v1, p2}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 356142
    iget-object v4, p0, LX/20p;->f:LX/0Zb;

    invoke-interface {v4, v1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 356143
    invoke-direct {p0, v2, v0}, LX/20p;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 356144
    const-string v1, "newsfeed_ufi"

    invoke-virtual {v2, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-interface {p3}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    .line 356145
    sget-object v5, LX/9Iq;->a:[I

    invoke-virtual {v4}, LX/1Qt;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 356146
    :pswitch_0
    const-string v5, "ANDROID_COMPOSER"

    :goto_2
    move-object v4, v5

    .line 356147
    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 356148
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 356149
    if-eqz v1, :cond_2

    .line 356150
    invoke-static {p1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 356151
    iget-object v5, p0, LX/20p;->a:LX/0bH;

    new-instance v6, LX/1Zb;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    :goto_3
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-direct {v6, v7, v0, v3}, LX/1Zb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/0b4;->a(LX/0b7;)V

    .line 356152
    iget-object v0, p0, LX/20p;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/89P;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x6dc

    invoke-virtual {v0, v2, v3, v1}, LX/89P;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 356153
    :cond_2
    return-void

    :cond_3
    move-object v4, v3

    .line 356154
    goto/16 :goto_0

    :cond_4
    move-object v2, v3

    goto/16 :goto_1

    :cond_5
    move-object v0, v3

    .line 356155
    goto :goto_3

    .line 356156
    :pswitch_1
    const-string v5, "ANDROID_FEED_COMPOSER"

    goto :goto_2

    .line 356157
    :pswitch_2
    const-string v5, "ANDROID_PAGE_COMPOSER"

    goto :goto_2

    .line 356158
    :pswitch_3
    const-string v5, "ANDROID_TIMELINE_COMPOSER"

    goto :goto_2

    .line 356159
    :pswitch_4
    const-string v5, "ANDROID_GROUP_COMPOSER"

    goto :goto_2

    .line 356160
    :pswitch_5
    const-string v5, "ANDROID_AFTER_PARTY_COMPOSER"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356161
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 356162
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 356163
    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356164
    iget-object v1, p0, LX/20p;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Attempting to share a story that is not shareable. Story ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356165
    :goto_0
    return-void

    .line 356166
    :cond_0
    iget-object v0, p0, LX/20p;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 356167
    iget-object v1, v0, LX/0gh;->w:Ljava/lang/String;

    move-object v0, v1

    .line 356168
    if-nez v0, :cond_1

    .line 356169
    iget-object v0, p0, LX/20p;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_share"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 356170
    :cond_1
    iget-object v5, p0, LX/20p;->j:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Aov;

    const-string v8, "newsfeed_ufi"

    new-instance v9, LX/Aoi;

    invoke-direct {v9, p0, p1, p2, p3}, LX/Aoi;-><init>(LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V

    new-instance v10, LX/Aoj;

    invoke-direct {v10, p0}, LX/Aoj;-><init>(LX/20p;)V

    invoke-interface {p3}, LX/1PT;->a()LX/1Qt;

    move-result-object v6

    sget-object v7, LX/1Qt;->FEED:LX/1Qt;

    if-ne v6, v7, :cond_2

    const/4 v11, 0x1

    :goto_1
    move-object v6, p1

    move-object v7, p2

    invoke-virtual/range {v5 .. v11}, LX/Aov;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;LX/Amm;LX/Amo;Z)LX/5OM;

    move-result-object v5

    .line 356171
    if-nez v5, :cond_3

    .line 356172
    invoke-static {p0, p1, p2, p3}, LX/20p;->c(LX/20p;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1PT;)V

    .line 356173
    :goto_2
    goto :goto_0

    .line 356174
    :cond_2
    const/4 v11, 0x0

    goto :goto_1

    .line 356175
    :cond_3
    iget-object v6, p0, LX/20p;->l:LX/1AM;

    new-instance v7, LX/1ZH;

    invoke-direct {v7}, LX/1ZH;-><init>()V

    invoke-virtual {v6, v7}, LX/0b4;->a(LX/0b7;)V

    .line 356176
    invoke-virtual {v5}, LX/0ht;->d()V

    goto :goto_2
.end method
