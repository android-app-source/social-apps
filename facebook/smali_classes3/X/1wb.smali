.class public LX/1wb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/1wo;

.field public final c:LX/0WV;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/1wr;

.field private final g:LX/1ws;

.field private final h:LX/1wi;

.field private final i:LX/1wt;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0V8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346517
    const-class v0, LX/1wb;

    sput-object v0, LX/1wb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1wo;LX/0WV;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/1wr;LX/1ws;LX/1wi;LX/1wt;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1wo;",
            "LX/0WV;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1wr;",
            "LX/1ws;",
            "LX/1wi;",
            "LX/1wt;",
            "LX/0Ot",
            "<",
            "LX/0V8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346463
    iput-object p1, p0, LX/1wb;->b:LX/1wo;

    .line 346464
    iput-object p2, p0, LX/1wb;->c:LX/0WV;

    .line 346465
    iput-object p3, p0, LX/1wb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 346466
    iput-object p4, p0, LX/1wb;->e:Lcom/facebook/content/SecureContextHelper;

    .line 346467
    iput-object p5, p0, LX/1wb;->f:LX/1wr;

    .line 346468
    iput-object p6, p0, LX/1wb;->g:LX/1ws;

    .line 346469
    iput-object p7, p0, LX/1wb;->h:LX/1wi;

    .line 346470
    iput-object p8, p0, LX/1wb;->i:LX/1wt;

    .line 346471
    iput-object p9, p0, LX/1wb;->j:LX/0Ot;

    .line 346472
    return-void
.end method

.method private static a(LX/1wb;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 346513
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346514
    const-string v1, "operation_uuid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346515
    iget-object v1, p0, LX/1wb;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 346516
    return-void
.end method

.method public static a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 346509
    if-eqz p0, :cond_0

    .line 346510
    sget-object v0, LX/1wb;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 346511
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346512
    :cond_0
    return-void
.end method

.method private static b(LX/1wb;)V
    .locals 1

    .prologue
    .line 346506
    iget-object v0, p0, LX/1wb;->f:LX/1wr;

    invoke-virtual {v0}, LX/1wr;->b()V

    .line 346507
    iget-object v0, p0, LX/1wb;->h:LX/1wi;

    invoke-virtual {v0}, LX/1wi;->b()V

    .line 346508
    return-void
.end method

.method private static b(LX/1wb;Landroid/app/Activity;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 346481
    const/4 v2, 0x0

    .line 346482
    iget-object v3, p0, LX/1wb;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3fU;->d:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 346483
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 346484
    const-string v3, "ReleaseInfo is null or empty, skipping"

    invoke-static {p2, v3}, LX/1wb;->a(ZLjava/lang/String;)V

    .line 346485
    :goto_0
    move-object v2, v2

    .line 346486
    if-nez v2, :cond_0

    .line 346487
    :goto_1
    return v0

    .line 346488
    :cond_0
    iget-object v3, p0, LX/1wb;->c:LX/0WV;

    invoke-virtual {v3}, LX/0WV;->b()I

    move-result v3

    .line 346489
    iget v4, v2, Lcom/facebook/appupdate/ReleaseInfo;->versionCode:I

    if-le v4, v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 346490
    if-nez v3, :cond_1

    .line 346491
    const-string v1, "ReleaseInfo has outdated version code, skipping"

    invoke-static {p2, v1}, LX/1wb;->a(ZLjava/lang/String;)V

    goto :goto_1

    .line 346492
    :cond_1
    invoke-direct {p0, v2}, LX/1wb;->b(Lcom/facebook/appupdate/ReleaseInfo;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 346493
    const-string v1, "Insufficient space to upgrade, skipping"

    invoke-static {p2, v1}, LX/1wb;->a(ZLjava/lang/String;)V

    goto :goto_1

    .line 346494
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346495
    const-string v2, "use_release_info"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 346496
    iget-object v2, p0, LX/1wb;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 346497
    iget-object v0, p0, LX/1wb;->i:LX/1wt;

    .line 346498
    iget-object v4, v0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/3fU;->g:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v4

    .line 346499
    iget-object v5, v0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v5

    sget-object v6, LX/3fU;->g:LX/0Tn;

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v5, v6, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v4

    sget-object v5, LX/3fU;->h:LX/0Tn;

    iget-object v6, v0, LX/1wt;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 346500
    invoke-static {v0}, LX/1wt;->h(LX/1wt;)V

    .line 346501
    move v0, v1

    .line 346502
    goto :goto_1

    .line 346503
    :cond_3
    :try_start_0
    new-instance v3, Lcom/facebook/appupdate/ReleaseInfo;

    invoke-direct {v3, v4}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 346504
    goto :goto_0

    .line 346505
    :catch_0
    const-string v3, "Fail to parse ReleaseInfo, skipping"

    invoke-static {p2, v3}, LX/1wb;->a(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private b(Lcom/facebook/appupdate/ReleaseInfo;)Z
    .locals 12

    .prologue
    .line 346473
    iget-object v0, p0, LX/1wb;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0V8;

    .line 346474
    sget-object v1, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    .line 346475
    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v0

    .line 346476
    iget-wide v8, p1, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    .line 346477
    const-wide/16 v10, 0x1

    mul-long/2addr v8, v10

    move-wide v4, v8

    .line 346478
    iget-wide v8, p1, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    .line 346479
    const-wide/16 v10, 0x3

    mul-long/2addr v8, v10

    move-wide v6, v8

    .line 346480
    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Z)Z
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 346383
    iget-object v0, p0, LX/1wb;->g:LX/1ws;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 346384
    iget-object v2, v0, LX/1ws;->d:LX/0Uh;

    const/16 v5, 0x48f

    invoke-virtual {v2, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v3

    .line 346385
    :goto_0
    move v0, v2

    .line 346386
    if-nez v0, :cond_0

    .line 346387
    const-string v0, "Failed eligibility check, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    move v0, v1

    .line 346388
    :goto_1
    return v0

    .line 346389
    :cond_0
    iget-object v0, p0, LX/1wb;->b:LX/1wo;

    invoke-virtual {v0}, LX/1wo;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 346390
    const-string v0, "AppUpdate init not done, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    move v0, v1

    .line 346391
    goto :goto_1

    .line 346392
    :cond_1
    iget-object v0, p0, LX/1wb;->i:LX/1wt;

    .line 346393
    iget-object v3, v0, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/3fU;->f:LX/0Tn;

    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v3

    .line 346394
    iget-object v5, v0, LX/1wt;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    .line 346395
    cmp-long v3, v5, v3

    if-lez v3, :cond_13

    .line 346396
    invoke-virtual {v0}, LX/1wt;->a()V

    .line 346397
    const/4 v3, 0x1

    .line 346398
    :goto_2
    move v0, v3

    .line 346399
    if-eqz v0, :cond_4

    .line 346400
    iget-object v0, p0, LX/1wb;->b:LX/1wo;

    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 346401
    invoke-virtual {v0}, LX/EeS;->g()Z

    goto :goto_3

    .line 346402
    :cond_2
    invoke-static {p0}, LX/1wb;->b(LX/1wb;)V

    .line 346403
    iget-object v0, p0, LX/1wb;->i:LX/1wt;

    invoke-virtual {v0}, LX/1wt;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346404
    invoke-static {p0, p1, p2}, LX/1wb;->b(LX/1wb;Landroid/app/Activity;Z)Z

    move-result v0

    goto :goto_1

    .line 346405
    :cond_3
    const-string v0, "Download prompt not permitted right after reset, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    :goto_4
    move v0, v1

    .line 346406
    goto :goto_1

    .line 346407
    :cond_4
    iget-object v0, p0, LX/1wb;->b:LX/1wo;

    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EeS;

    .line 346408
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v3

    iget-object v3, v3, LX/EeX;->operationState$:Ljava/lang/Integer;

    .line 346409
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, LX/3CW;->c(II)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x8

    invoke-static {v3, v4}, LX/3CW;->c(II)Z

    move-result v3

    if-nez v3, :cond_5

    .line 346410
    :goto_5
    move-object v0, v0

    .line 346411
    if-eqz v0, :cond_8

    .line 346412
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    .line 346413
    iget-object v2, v0, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_4

    .line 346414
    :pswitch_0
    iget-object v2, p0, LX/1wb;->i:LX/1wt;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 346415
    iget-object v3, v2, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/3fU;->i:LX/0Tn;

    invoke-interface {v3, v6, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 346416
    iget-object v6, v2, LX/1wt;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/3fU;->j:LX/0Tn;

    const-wide/16 v9, 0x0

    invoke-interface {v6, v7, v9, v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v7

    .line 346417
    if-nez v3, :cond_15

    move v3, v4

    .line 346418
    :goto_6
    move v2, v3

    .line 346419
    if-eqz v2, :cond_6

    .line 346420
    invoke-static {p0}, LX/1wb;->b(LX/1wb;)V

    .line 346421
    iget-object v0, v0, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-static {p0, p1, v0}, LX/1wb;->a(LX/1wb;Landroid/app/Activity;Ljava/lang/String;)V

    .line 346422
    iget-object v0, p0, LX/1wb;->i:LX/1wt;

    invoke-virtual {v0}, LX/1wt;->g()V

    goto :goto_4

    .line 346423
    :cond_6
    const-string v0, "Install prompt not permitted, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    goto :goto_4

    .line 346424
    :pswitch_1
    const/4 v2, 0x1

    move v2, v2

    .line 346425
    if-eqz v2, :cond_7

    .line 346426
    invoke-static {p0}, LX/1wb;->b(LX/1wb;)V

    .line 346427
    iget-object v0, v0, LX/EeX;->operationUuid:Ljava/lang/String;

    invoke-static {p0, p1, v0}, LX/1wb;->a(LX/1wb;Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 346428
    :cond_7
    const-string v0, "Failure screen not permitted, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    goto/16 :goto_4

    .line 346429
    :cond_8
    iget-object v0, p0, LX/1wb;->i:LX/1wt;

    invoke-virtual {v0}, LX/1wt;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 346430
    invoke-static {p0}, LX/1wb;->b(LX/1wb;)V

    .line 346431
    invoke-static {p0, p1, p2}, LX/1wb;->b(LX/1wb;Landroid/app/Activity;Z)Z

    move-result v0

    goto/16 :goto_1

    .line 346432
    :cond_9
    const-string v0, "Download prompt not permitted, skipping"

    invoke-static {p2, v0}, LX/1wb;->a(ZLjava/lang/String;)V

    goto/16 :goto_4

    .line 346433
    :cond_a
    iget-object v2, v0, LX/1ws;->c:LX/0WV;

    invoke-virtual {v2}, LX/0WV;->d()Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, v0, LX/1ws;->d:LX/0Uh;

    const/16 v5, 0x492

    invoke-virtual {v2, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_b

    move v2, v3

    .line 346434
    goto/16 :goto_0

    .line 346435
    :cond_b
    iget-object v2, v0, LX/1ws;->i:LX/0VT;

    iget-object v5, v0, LX/1ws;->g:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/0VT;->b(Ljava/lang/String;)Z

    move-result v2

    .line 346436
    if-nez v2, :cond_c

    move v2, v3

    .line 346437
    goto/16 :goto_0

    .line 346438
    :cond_c
    iget-object v2, v0, LX/1ws;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    .line 346439
    if-eqz v2, :cond_d

    move v2, v4

    .line 346440
    goto/16 :goto_0

    .line 346441
    :cond_d
    iget-object v2, v0, LX/1ws;->f:Landroid/content/pm/PackageManager;

    iget-object v5, v0, LX/1ws;->g:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 346442
    const/4 v5, 0x0

    .line 346443
    sget-object v7, LX/1ws;->a:[Ljava/lang/String;

    array-length v8, v7

    move v6, v5

    :goto_7
    if-ge v6, v8, :cond_e

    aget-object v9, v7, v6

    .line 346444
    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 346445
    const/4 v5, 0x1

    .line 346446
    :cond_e
    move v5, v5

    .line 346447
    if-eqz v5, :cond_f

    move v2, v3

    .line 346448
    goto/16 :goto_0

    .line 346449
    :cond_f
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    move v2, v5

    .line 346450
    if-nez v2, :cond_10

    iget-object v2, v0, LX/1ws;->d:LX/0Uh;

    const/16 v5, 0x592

    invoke-virtual {v2, v5, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_10

    move v2, v3

    .line 346451
    goto/16 :goto_0

    .line 346452
    :cond_10
    iget-object v2, v0, LX/1ws;->h:LX/1sd;

    invoke-virtual {v2}, LX/1sd;->c()Z

    move-result v2

    move v2, v2

    .line 346453
    if-eqz v2, :cond_11

    move v2, v3

    .line 346454
    goto/16 :goto_0

    :cond_11
    move v2, v4

    .line 346455
    goto/16 :goto_0

    .line 346456
    :cond_12
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 346457
    :cond_15
    iget-object v6, v2, LX/1wt;->d:LX/1wu;

    invoke-virtual {v6}, LX/1wu;->b()LX/0Px;

    move-result-object v6

    .line 346458
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    if-ge v3, v9, :cond_17

    .line 346459
    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    .line 346460
    iget-object v3, v2, LX/1wt;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v11

    add-long/2addr v7, v9

    cmp-long v3, v11, v7

    if-lez v3, :cond_16

    move v3, v4

    goto/16 :goto_6

    :cond_16
    move v3, v5

    goto/16 :goto_6

    :cond_17
    move v3, v5

    .line 346461
    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
