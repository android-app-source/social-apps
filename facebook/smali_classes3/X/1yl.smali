.class public LX/1yl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1yt;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1yl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1yt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 351257
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 351258
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1yl;->b:LX/0Zi;

    .line 351259
    iput-object p1, p0, LX/1yl;->a:LX/0Ot;

    .line 351260
    return-void
.end method

.method public static a(LX/0QB;)LX/1yl;
    .locals 4

    .prologue
    .line 351261
    const-class v1, LX/1yl;

    monitor-enter v1

    .line 351262
    :try_start_0
    sget-object v0, LX/1yl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 351263
    sput-object v2, LX/1yl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 351264
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351265
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 351266
    new-instance v3, LX/1yl;

    const/16 p0, 0x727

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1yl;-><init>(LX/0Ot;)V

    .line 351267
    move-object v0, v3

    .line 351268
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 351269
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1yl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351270
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 351271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 351272
    check-cast p2, LX/33K;

    .line 351273
    iget-object v0, p0, LX/1yl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1yt;

    iget-object v1, p2, LX/33K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/33K;->b:LX/1Pc;

    .line 351274
    check-cast v2, LX/1Pr;

    .line 351275
    new-instance v4, LX/33M;

    invoke-direct {v4}, LX/33M;-><init>()V

    .line 351276
    const v3, 0x7f020b80

    .line 351277
    iput v3, v4, LX/33M;->e:I

    .line 351278
    const v3, 0x7f020b81

    .line 351279
    iput v3, v4, LX/33M;->d:I

    .line 351280
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 351281
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 351282
    invoke-static {v3}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    .line 351283
    new-instance p2, LX/33P;

    invoke-direct {p2, p0}, LX/33P;-><init>(Lcom/facebook/graphql/model/GraphQLProfile;)V

    invoke-interface {v2, p2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 351284
    invoke-virtual {v4, v3}, LX/33M;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 351285
    iput-object v3, v4, LX/33M;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 351286
    move-object v3, v4

    .line 351287
    iget-object v4, v0, LX/1yt;->c:LX/1yf;

    invoke-virtual {v4}, LX/1yf;->a()Z

    move-result v4

    .line 351288
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    .line 351289
    iget p2, v3, LX/33M;->a:I

    move p2, p2

    .line 351290
    invoke-virtual {p0, p2}, LX/1o5;->h(I)LX/1o5;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 351291
    iget p2, v3, LX/33M;->b:I

    move p2, p2

    .line 351292
    invoke-interface {p0, p2}, LX/1Di;->A(I)LX/1Di;

    move-result-object p0

    .line 351293
    const p2, 0x72b0db82

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 351294
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    .line 351295
    if-eqz v4, :cond_1

    .line 351296
    iget-object v4, v3, LX/33M;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v4

    .line 351297
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_0

    const v3, 0x7f020cba

    :goto_0
    invoke-interface {p0, v3}, LX/1Di;->x(I)LX/1Di;

    .line 351298
    :goto_1
    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 351299
    return-object v0

    .line 351300
    :cond_0
    const v3, 0x7f020cb9

    goto :goto_0

    .line 351301
    :cond_1
    const/4 v3, 0x1

    const/4 v4, 0x4

    invoke-interface {p0, v3, v4}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const p2, 0x7f0b00ef

    invoke-interface {v3, v4, p2}, LX/1Di;->g(II)LX/1Di;

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 351302
    invoke-static {}, LX/1dS;->b()V

    .line 351303
    iget v0, p1, LX/1dQ;->b:I

    .line 351304
    packed-switch v0, :pswitch_data_0

    .line 351305
    :goto_0
    return-object v2

    .line 351306
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 351307
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 351308
    check-cast v1, LX/33K;

    .line 351309
    iget-object v3, p0, LX/1yl;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1yt;

    iget-object p1, v1, LX/33K;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/33K;->b:LX/1Pc;

    .line 351310
    iget-object p0, v3, LX/1yt;->a:LX/17Q;

    iget-object v1, v3, LX/1yt;->b:LX/0Zb;

    invoke-static {p1, p2, p0, v1}, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;LX/17Q;LX/0Zb;)V

    .line 351311
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x72b0db82
        :pswitch_0
    .end packed-switch
.end method
