.class public LX/1xS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BsU;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xS",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BsU;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348410
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348411
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xS;->b:LX/0Zi;

    .line 348412
    iput-object p1, p0, LX/1xS;->a:LX/0Ot;

    .line 348413
    return-void
.end method

.method public static a(LX/0QB;)LX/1xS;
    .locals 4

    .prologue
    .line 348414
    const-class v1, LX/1xS;

    monitor-enter v1

    .line 348415
    :try_start_0
    sget-object v0, LX/1xS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348416
    sput-object v2, LX/1xS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348417
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348418
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348419
    new-instance v3, LX/1xS;

    const/16 p0, 0x1d33

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xS;-><init>(LX/0Ot;)V

    .line 348420
    move-object v0, v3

    .line 348421
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348422
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348423
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348424
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 348425
    check-cast p2, LX/BsT;

    .line 348426
    iget-object v0, p0, LX/1xS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BsU;

    iget-object v1, p2, LX/BsT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BsT;->b:LX/1Po;

    .line 348427
    iget-object v3, v0, LX/BsU;->b:LX/0dk;

    invoke-virtual {v3}, LX/0dk;->a()Lcom/facebook/java2js/JSContext;

    move-result-object v3

    .line 348428
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/5KZ;->c(LX/1De;)LX/5KX;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/5KX;->a(Lcom/facebook/java2js/JSContext;)LX/5KX;

    move-result-object p0

    iget-object p2, v0, LX/BsU;->a:LX/5KP;

    invoke-virtual {p2, p1, v3}, LX/5KP;->a(LX/1De;Lcom/facebook/java2js/JSContext;)LX/5KO;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/5KX;->a(Ljava/lang/Object;)LX/5KX;

    move-result-object v3

    const-string p0, "CSHeaderActorWrapper"

    invoke-virtual {v3, p0}, LX/5KX;->b(Ljava/lang/String;)LX/5KX;

    move-result-object v3

    new-instance p0, LX/Bdw;

    invoke-direct {p0}, LX/Bdw;-><init>()V

    .line 348429
    iget-object p2, p0, LX/Bdw;->a:LX/Bdx;

    .line 348430
    iput-object v1, p2, LX/Bdx;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 348431
    move-object p0, p0

    .line 348432
    iget-object p2, p0, LX/Bdw;->a:LX/Bdx;

    .line 348433
    iput-object v2, p2, LX/Bdx;->b:LX/1Po;

    .line 348434
    move-object p0, p0

    .line 348435
    iget-object p2, p0, LX/Bdw;->a:LX/Bdx;

    .line 348436
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bdw;->a:LX/Bdx;

    .line 348437
    move-object p0, p2

    .line 348438
    invoke-virtual {v3, p0}, LX/5KX;->b(Ljava/lang/Object;)LX/5KX;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 348439
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348440
    invoke-static {}, LX/1dS;->b()V

    .line 348441
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/BsS;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1xS",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 348442
    new-instance v1, LX/BsT;

    invoke-direct {v1, p0}, LX/BsT;-><init>(LX/1xS;)V

    .line 348443
    iget-object v2, p0, LX/1xS;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BsS;

    .line 348444
    if-nez v2, :cond_0

    .line 348445
    new-instance v2, LX/BsS;

    invoke-direct {v2, p0}, LX/BsS;-><init>(LX/1xS;)V

    .line 348446
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BsS;->a$redex0(LX/BsS;LX/1De;IILX/BsT;)V

    .line 348447
    move-object v1, v2

    .line 348448
    move-object v0, v1

    .line 348449
    return-object v0
.end method
