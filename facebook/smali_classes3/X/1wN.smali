.class public LX/1wN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/util/Random;

.field public static i:Ljava/lang/reflect/Method;


# instance fields
.field public final d:LX/0ps;

.field public final e:LX/1sz;

.field public final f:LX/0Uo;

.field public final g:LX/0gh;

.field private final h:LX/1wQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 345734
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x3

    :goto_0
    sput v0, LX/1wN;->a:I

    .line 345735
    const-class v0, LX/1wN;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/1wN;->b:Ljava/lang/String;

    .line 345736
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, LX/1wN;->c:Ljava/util/Random;

    return-void

    .line 345737
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(LX/0ps;LX/1sz;LX/0Uo;LX/0gh;LX/1Fg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ps;",
            "LX/1sz;",
            "LX/0Uo;",
            "LX/0gh;",
            "LX/1Fg",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 345738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345739
    iput-object p1, p0, LX/1wN;->d:LX/0ps;

    .line 345740
    iput-object p2, p0, LX/1wN;->e:LX/1sz;

    .line 345741
    iput-object p3, p0, LX/1wN;->f:LX/0Uo;

    .line 345742
    iput-object p4, p0, LX/1wN;->g:LX/0gh;

    .line 345743
    new-instance v0, LX/1wQ;

    invoke-direct {v0, p5}, LX/1wQ;-><init>(LX/1Fg;)V

    iput-object v0, p0, LX/1wN;->h:LX/1wQ;

    .line 345744
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/0oG;)V
    .locals 9

    .prologue
    .line 345745
    monitor-enter p0

    .line 345746
    :try_start_0
    iget-object v1, p0, LX/1wN;->e:LX/1sz;

    invoke-virtual {v1}, LX/1sz;->a()LX/2GO;

    move-result-object v1

    .line 345747
    const-string v2, "device_total_mem"

    .line 345748
    iget-wide v7, v1, LX/2GO;->b:J

    move-wide v3, v7

    .line 345749
    invoke-virtual {p1, v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345750
    const-string v2, "free_device_java_heap_percentage"

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    invoke-virtual {v1}, LX/2GO;->a()J

    move-result-wide v5

    long-to-double v5, v5

    mul-double/2addr v3, v5

    .line 345751
    iget-wide v7, v1, LX/2GO;->b:J

    move-wide v5, v7

    .line 345752
    long-to-double v5, v5

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345753
    iget-object v1, p0, LX/1wN;->d:LX/0ps;

    invoke-virtual {v1}, LX/0ps;->b()LX/0V6;

    move-result-object v1

    .line 345754
    const-string v2, "app_total_memory"

    .line 345755
    iget-wide v7, v1, LX/0V6;->e:J

    move-wide v3, v7

    .line 345756
    invoke-virtual {p1, v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345757
    const-string v2, "free_app_java_heap_percentage"

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    .line 345758
    iget-wide v7, v1, LX/0V6;->d:J

    move-wide v5, v7

    .line 345759
    long-to-double v5, v5

    mul-double/2addr v3, v5

    .line 345760
    iget-wide v7, v1, LX/0V6;->e:J

    move-wide v5, v7

    .line 345761
    long-to-double v5, v5

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345762
    new-instance v1, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v1}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 345763
    invoke-static {v1}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 345764
    const-string v2, "process_dalvik_private_dirty"

    iget v3, v1, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    mul-int/lit16 v3, v3, 0x400

    invoke-virtual {p1, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345765
    const-string v2, "process_native_private_dirty"

    iget v1, v1, Landroid/os/Debug$MemoryInfo;->nativePrivateDirty:I

    mul-int/lit16 v1, v1, 0x400

    invoke-virtual {p1, v2, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345766
    const-string v1, "process_other_private_dirty"

    const/4 v3, 0x0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345767
    :try_start_1
    const-class v2, Landroid/os/Debug$MemoryInfo;

    .line 345768
    sget-object v4, LX/1wN;->i:Ljava/lang/reflect/Method;

    if-nez v4, :cond_0

    .line 345769
    const-string v4, "getOtherPrivateDirty"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, LX/1wN;->i:Ljava/lang/reflect/Method;

    .line 345770
    :cond_0
    sget-object v2, LX/1wN;->i:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    .line 345771
    new-instance v2, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v2}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 345772
    invoke-static {v2}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 345773
    sget-object v4, LX/1wN;->i:Ljava/lang/reflect/Method;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget v7, LX/1wN;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    move-result v2

    mul-int/lit16 v2, v2, 0x400

    .line 345774
    :goto_0
    move v2, v2

    .line 345775
    invoke-virtual {p1, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345776
    monitor-exit p0

    return-void

    .line 345777
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 345778
    :catch_0
    move-exception v2

    .line 345779
    :goto_1
    sget-object v4, LX/1wN;->b:Ljava/lang/String;

    const-string v5, "Unable to find OTHER_PD_STAT field"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v2, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move v2, v3

    .line 345780
    goto :goto_0

    .line 345781
    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    goto :goto_1
.end method

.method public final declared-synchronized b(LX/0oG;)V
    .locals 10

    .prologue
    .line 345782
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1wN;->h:LX/1wQ;

    invoke-virtual {v0}, LX/1wQ;->a()LX/4dq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 345783
    :try_start_1
    const-string v0, "bitmap_cache_shared_count"

    iget-object v2, v1, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345784
    const-string v0, "bitmap_cache_shared_size"

    iget v2, v1, LX/4dq;->d:I

    iget v3, v1, LX/4dq;->e:I

    sub-int/2addr v2, v3

    invoke-virtual {p1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345785
    const-string v0, "bitmap_cache_lru_count"

    iget-object v2, v1, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345786
    const-string v0, "bitmap_cache_lru_size"

    iget v2, v1, LX/4dq;->e:I

    invoke-virtual {p1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345787
    iget-object v2, v1, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 345788
    sget-object v2, LX/1wN;->c:Ljava/util/Random;

    iget-object v3, v1, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 345789
    iget-object v3, v1, LX/4dq;->f:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4dr;

    .line 345790
    iget-object v3, v2, LX/4dr;->a:Ljava/lang/Object;

    instance-of v3, v3, LX/1bg;

    if-eqz v3, :cond_0

    .line 345791
    iget-object v3, v2, LX/4dr;->a:Ljava/lang/Object;

    check-cast v3, LX/1bg;

    .line 345792
    const-string v5, "bitmap_cache_lru_sample_size"

    iget-object v4, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1ln;

    invoke-virtual {v4}, LX/1ln;->b()I

    move-result v4

    invoke-virtual {p1, v5, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345793
    const-string v5, "bitmap_cache_lru_sample_width"

    iget-object v4, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1ln;

    invoke-virtual {v4}, LX/1ln;->g()I

    move-result v4

    invoke-virtual {p1, v5, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345794
    const-string v4, "bitmap_cache_lru_sample_height"

    iget-object v2, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ln;

    invoke-virtual {v2}, LX/1ln;->h()I

    move-result v2

    invoke-virtual {p1, v4, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345795
    const-string v2, "bitmap_cache_lru_sample_caller_context"

    .line 345796
    iget-object v4, v3, LX/1bg;->h:Ljava/lang/Object;

    move-object v4, v4

    .line 345797
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 345798
    const-string v2, "bitmap_cache_lru_sample_duration"

    .line 345799
    sget-object v4, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v4, v4

    .line 345800
    invoke-virtual {v4}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    .line 345801
    iget-wide v8, v3, LX/1bg;->i:J

    move-wide v6, v8

    .line 345802
    sub-long/2addr v4, v6

    invoke-virtual {p1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345803
    :cond_0
    iget-object v2, v1, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 345804
    sget-object v2, LX/1wN;->c:Ljava/util/Random;

    iget-object v3, v1, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 345805
    iget-object v3, v1, LX/4dq;->g:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/4dr;

    .line 345806
    iget-object v3, v2, LX/4dr;->a:Ljava/lang/Object;

    instance-of v3, v3, LX/1bg;

    if-eqz v3, :cond_1

    .line 345807
    iget-object v3, v2, LX/4dr;->a:Ljava/lang/Object;

    check-cast v3, LX/1bg;

    .line 345808
    const-string v5, "bitmap_cache_shared_sample_size"

    iget-object v4, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1ln;

    invoke-virtual {v4}, LX/1ln;->b()I

    move-result v4

    invoke-virtual {p1, v5, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345809
    const-string v5, "bitmap_cache_shared_sample_width"

    iget-object v4, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v4}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1ln;

    invoke-virtual {v4}, LX/1ln;->g()I

    move-result v4

    invoke-virtual {p1, v5, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345810
    const-string v4, "bitmap_cache_shared_sample_height"

    iget-object v2, v2, LX/4dr;->b:LX/1FJ;

    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ln;

    invoke-virtual {v2}, LX/1ln;->h()I

    move-result v2

    invoke-virtual {p1, v4, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345811
    const-string v2, "bitmap_cache_shared_sample_caller_context"

    .line 345812
    iget-object v4, v3, LX/1bg;->h:Ljava/lang/Object;

    move-object v4, v4

    .line 345813
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 345814
    const-string v2, "bitmap_cache_shared_sample_duration"

    .line 345815
    sget-object v4, Lcom/facebook/common/time/RealtimeSinceBootClock;->a:Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-object v4, v4

    .line 345816
    invoke-virtual {v4}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    .line 345817
    iget-wide v8, v3, LX/1bg;->i:J

    move-wide v6, v8

    .line 345818
    sub-long/2addr v4, v6

    invoke-virtual {p1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 345819
    :cond_1
    iget-object v0, v1, LX/4dq;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 345820
    const/4 v2, 0x0

    .line 345821
    sget-object v0, LX/1wN;->c:Ljava/util/Random;

    iget-object v3, v1, LX/4dq;->h:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 345822
    iget-object v3, v1, LX/4dq;->h:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    move v4, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 345823
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 345824
    invoke-static {v2}, LX/1le;->a(Landroid/graphics/Bitmap;)I

    move-result v2

    add-int/2addr v2, v4

    .line 345825
    if-nez v3, :cond_2

    .line 345826
    const-string v4, "bitmap_cache_other_sample_caller_context"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 345827
    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    move v4, v2

    .line 345828
    goto :goto_0

    .line 345829
    :cond_3
    const-string v0, "bitmap_cache_other_count"

    iget-object v2, v1, LX/4dq;->h:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {p1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 345830
    const-string v0, "bitmap_cache_other_size"

    invoke-virtual {p1, v0, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345831
    :cond_4
    :try_start_2
    invoke-virtual {v1}, LX/4dq;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345832
    monitor-exit p0

    return-void

    .line 345833
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, LX/4dq;->a()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 345834
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
