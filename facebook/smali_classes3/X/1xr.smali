.class public final LX/1xr;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:F

.field public h:F

.field public i:F

.field public j:I

.field public k:Z

.field public final synthetic l:LX/1xp;


# direct methods
.method public constructor <init>(LX/1xp;)V
    .locals 1

    .prologue
    .line 349654
    iput-object p1, p0, LX/1xr;->l:LX/1xp;

    .line 349655
    move-object v0, p1

    .line 349656
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 349657
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/1xr;->c:Z

    .line 349658
    const v0, -0xebe7dd

    iput v0, p0, LX/1xr;->d:I

    .line 349659
    sget v0, LX/1xs;->a:I

    iput v0, p0, LX/1xr;->e:I

    .line 349660
    const v0, 0x7fffffff

    iput v0, p0, LX/1xr;->j:I

    .line 349661
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349662
    const-string v0, "HeaderTitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 349663
    if-ne p0, p1, :cond_1

    .line 349664
    :cond_0
    :goto_0
    return v0

    .line 349665
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 349666
    goto :goto_0

    .line 349667
    :cond_3
    check-cast p1, LX/1xr;

    .line 349668
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 349669
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 349670
    if-eq v2, v3, :cond_0

    .line 349671
    iget-object v2, p0, LX/1xr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/1xr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/1xr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 349672
    goto :goto_0

    .line 349673
    :cond_5
    iget-object v2, p1, LX/1xr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 349674
    :cond_6
    iget-object v2, p0, LX/1xr;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/1xr;->b:LX/1Po;

    iget-object v3, p1, LX/1xr;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 349675
    goto :goto_0

    .line 349676
    :cond_8
    iget-object v2, p1, LX/1xr;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 349677
    :cond_9
    iget-boolean v2, p0, LX/1xr;->c:Z

    iget-boolean v3, p1, LX/1xr;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 349678
    goto :goto_0

    .line 349679
    :cond_a
    iget v2, p0, LX/1xr;->d:I

    iget v3, p1, LX/1xr;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 349680
    goto :goto_0

    .line 349681
    :cond_b
    iget v2, p0, LX/1xr;->e:I

    iget v3, p1, LX/1xr;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 349682
    goto :goto_0

    .line 349683
    :cond_c
    iget v2, p0, LX/1xr;->f:I

    iget v3, p1, LX/1xr;->f:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 349684
    goto :goto_0

    .line 349685
    :cond_d
    iget v2, p0, LX/1xr;->g:F

    iget v3, p1, LX/1xr;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_e

    move v0, v1

    .line 349686
    goto :goto_0

    .line 349687
    :cond_e
    iget v2, p0, LX/1xr;->h:F

    iget v3, p1, LX/1xr;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_f

    move v0, v1

    .line 349688
    goto :goto_0

    .line 349689
    :cond_f
    iget v2, p0, LX/1xr;->i:F

    iget v3, p1, LX/1xr;->i:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    .line 349690
    goto/16 :goto_0

    .line 349691
    :cond_10
    iget v2, p0, LX/1xr;->j:I

    iget v3, p1, LX/1xr;->j:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 349692
    goto/16 :goto_0

    .line 349693
    :cond_11
    iget-boolean v2, p0, LX/1xr;->k:Z

    iget-boolean v3, p1, LX/1xr;->k:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 349694
    goto/16 :goto_0
.end method
