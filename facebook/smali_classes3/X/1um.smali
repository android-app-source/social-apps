.class public final LX/1um;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/1um;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1up;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/1uo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 341494
    const/4 v0, 0x0

    sput-object v0, LX/1um;->a:LX/1um;

    .line 341495
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/1um;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 341496
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 341497
    new-instance v0, LX/1uo;

    invoke-direct {v0}, LX/1uo;-><init>()V

    iput-object v0, p0, LX/1um;->c:LX/1uo;

    .line 341498
    return-void
.end method

.method public static c(LX/1De;)LX/1up;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 341486
    new-instance v1, LX/1un;

    invoke-direct {v1}, LX/1un;-><init>()V

    .line 341487
    sget-object v2, LX/1um;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1up;

    .line 341488
    if-nez v2, :cond_0

    .line 341489
    new-instance v2, LX/1up;

    invoke-direct {v2}, LX/1up;-><init>()V

    .line 341490
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/1up;->a$redex0(LX/1up;LX/1De;IILX/1un;)V

    .line 341491
    move-object v1, v2

    .line 341492
    move-object v0, v1

    .line 341493
    return-object v0
.end method

.method public static declared-synchronized q()LX/1um;
    .locals 2

    .prologue
    .line 341482
    const-class v1, LX/1um;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/1um;->a:LX/1um;

    if-nez v0, :cond_0

    .line 341483
    new-instance v0, LX/1um;

    invoke-direct {v0}, LX/1um;-><init>()V

    sput-object v0, LX/1um;->a:LX/1um;

    .line 341484
    :cond_0
    sget-object v0, LX/1um;->a:LX/1um;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 341485
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 341480
    invoke-static {}, LX/1dS;->b()V

    .line 341481
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5694662b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 341476
    check-cast p6, LX/1un;

    .line 341477
    iget v1, p6, LX/1un;->a:F

    .line 341478
    invoke-static {p3, p4, v1, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 341479
    const/16 v1, 0x1f

    const v2, -0x7e6b3a66

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 341473
    new-instance v0, LX/1oH;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object p0

    invoke-virtual {p0}, LX/1Uo;->u()LX/1af;

    move-result-object p0

    invoke-direct {v0, p0}, LX/1oH;-><init>(LX/1af;)V

    .line 341474
    new-instance p0, LX/1oI;

    invoke-direct {p0, p1, v0}, LX/1oI;-><init>(Landroid/content/Context;LX/1aY;)V

    move-object v0, p0

    .line 341475
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 341472
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 13

    .prologue
    .line 341499
    check-cast p1, LX/1un;

    .line 341500
    check-cast p2, LX/1un;

    .line 341501
    iget-object v0, p1, LX/1un;->g:LX/4Ab;

    iget-object v1, p2, LX/1un;->g:LX/4Ab;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 341502
    iget-object v1, p1, LX/1un;->h:Landroid/graphics/ColorFilter;

    iget-object v2, p2, LX/1un;->h:Landroid/graphics/ColorFilter;

    invoke-static {v1, v2}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 341503
    iget-object v2, p1, LX/1un;->i:LX/1dc;

    iget-object v3, p2, LX/1un;->i:LX/1dc;

    invoke-static {v2, v3}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v2

    .line 341504
    iget-object v3, p1, LX/1un;->b:LX/1Up;

    iget-object v4, p2, LX/1un;->b:LX/1Up;

    invoke-static {v3, v4}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v3

    .line 341505
    iget-object v4, p1, LX/1un;->j:LX/1dc;

    iget-object v5, p2, LX/1un;->j:LX/1dc;

    invoke-static {v4, v5}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v4

    .line 341506
    iget-object v5, p1, LX/1un;->k:LX/1Up;

    iget-object v6, p2, LX/1un;->k:LX/1Up;

    invoke-static {v5, v6}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v5

    .line 341507
    iget-object v6, p1, LX/1un;->l:LX/1dc;

    iget-object v7, p2, LX/1un;->l:LX/1dc;

    invoke-static {v6, v7}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v6

    .line 341508
    iget-object v7, p1, LX/1un;->m:LX/1Up;

    iget-object v8, p2, LX/1un;->m:LX/1Up;

    invoke-static {v7, v8}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v7

    .line 341509
    iget-object v8, p1, LX/1un;->o:LX/1dc;

    iget-object v9, p2, LX/1un;->o:LX/1dc;

    invoke-static {v8, v9}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v8

    .line 341510
    iget-object v9, p1, LX/1un;->p:LX/1Up;

    iget-object v10, p2, LX/1un;->p:LX/1Up;

    invoke-static {v9, v10}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v9

    .line 341511
    iget v10, p1, LX/1un;->q:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget v11, p2, LX/1un;->q:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v10, v11}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v10

    .line 341512
    iget-object v11, p1, LX/1un;->r:LX/1dc;

    iget-object v12, p2, LX/1un;->r:LX/1dc;

    invoke-static {v11, v12}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v11

    .line 341513
    const/4 p1, 0x1

    .line 341514
    iget-object v12, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341515
    iget-object p0, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341516
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    move v12, p1

    .line 341517
    :goto_0
    move v12, v12

    .line 341518
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 341519
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 341520
    invoke-static {v2}, LX/1cy;->a(LX/3lz;)V

    .line 341521
    invoke-static {v3}, LX/1cy;->a(LX/3lz;)V

    .line 341522
    invoke-static {v4}, LX/1cy;->a(LX/3lz;)V

    .line 341523
    invoke-static {v5}, LX/1cy;->a(LX/3lz;)V

    .line 341524
    invoke-static {v6}, LX/1cy;->a(LX/3lz;)V

    .line 341525
    invoke-static {v7}, LX/1cy;->a(LX/3lz;)V

    .line 341526
    invoke-static {v8}, LX/1cy;->a(LX/3lz;)V

    .line 341527
    invoke-static {v9}, LX/1cy;->a(LX/3lz;)V

    .line 341528
    invoke-static {v10}, LX/1cy;->a(LX/3lz;)V

    .line 341529
    invoke-static {v11}, LX/1cy;->a(LX/3lz;)V

    .line 341530
    return v12

    .line 341531
    :cond_0
    iget-object v12, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341532
    iget-object p0, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341533
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    move v12, p1

    .line 341534
    goto :goto_0

    .line 341535
    :cond_1
    iget-object v12, v2, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 341536
    check-cast v12, LX/1dc;

    .line 341537
    iget-object p0, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 341538
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_2

    move v12, p1

    .line 341539
    goto :goto_0

    .line 341540
    :cond_2
    iget-object v12, v3, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341541
    iget-object p0, v3, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341542
    if-eq v12, p0, :cond_3

    move v12, p1

    .line 341543
    goto :goto_0

    .line 341544
    :cond_3
    iget-object v12, v4, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 341545
    check-cast v12, LX/1dc;

    .line 341546
    iget-object p0, v4, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 341547
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_4

    move v12, p1

    .line 341548
    goto :goto_0

    .line 341549
    :cond_4
    iget-object v12, v5, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341550
    iget-object p0, v5, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341551
    if-eq v12, p0, :cond_5

    move v12, p1

    .line 341552
    goto :goto_0

    .line 341553
    :cond_5
    iget-object v12, v6, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 341554
    check-cast v12, LX/1dc;

    .line 341555
    iget-object p0, v6, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 341556
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_6

    move v12, p1

    .line 341557
    goto :goto_0

    .line 341558
    :cond_6
    iget-object v12, v7, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341559
    iget-object p0, v7, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341560
    if-eq v12, p0, :cond_7

    move v12, p1

    .line 341561
    goto/16 :goto_0

    .line 341562
    :cond_7
    iget-object v12, v8, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 341563
    check-cast v12, LX/1dc;

    .line 341564
    iget-object p0, v8, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 341565
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_8

    move v12, p1

    .line 341566
    goto/16 :goto_0

    .line 341567
    :cond_8
    iget-object v12, v9, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341568
    iget-object p0, v9, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341569
    if-eq v12, p0, :cond_9

    move v12, p1

    .line 341570
    goto/16 :goto_0

    .line 341571
    :cond_9
    iget-object v12, v10, LX/3lz;->b:Ljava/lang/Object;

    move-object v12, v12

    .line 341572
    iget-object p0, v10, LX/3lz;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 341573
    invoke-static {v12, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_a

    move v12, p1

    .line 341574
    goto/16 :goto_0

    .line 341575
    :cond_a
    iget-object v12, v11, LX/3lz;->a:Ljava/lang/Object;

    move-object v12, v12

    .line 341576
    check-cast v12, LX/1dc;

    .line 341577
    iget-object p0, v11, LX/3lz;->b:Ljava/lang/Object;

    move-object p0, p0

    .line 341578
    check-cast p0, LX/1dc;

    invoke-static {v12, p0}, LX/1dc;->a(LX/1dc;LX/1dc;)Z

    move-result v12

    if-eqz v12, :cond_b

    move v12, p1

    .line 341579
    goto/16 :goto_0

    .line 341580
    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 16

    .prologue
    .line 341469
    check-cast p3, LX/1un;

    move-object/from16 v2, p2

    .line 341470
    check-cast v2, LX/1oI;

    move-object/from16 v0, p3

    iget-object v3, v0, LX/1un;->g:LX/4Ab;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/1un;->h:Landroid/graphics/ColorFilter;

    move-object/from16 v0, p3

    iget-object v5, v0, LX/1un;->i:LX/1dc;

    move-object/from16 v0, p3

    iget-object v6, v0, LX/1un;->b:LX/1Up;

    move-object/from16 v0, p3

    iget-object v7, v0, LX/1un;->j:LX/1dc;

    move-object/from16 v0, p3

    iget-object v8, v0, LX/1un;->k:LX/1Up;

    move-object/from16 v0, p3

    iget-object v9, v0, LX/1un;->l:LX/1dc;

    move-object/from16 v0, p3

    iget-object v10, v0, LX/1un;->m:LX/1Up;

    move-object/from16 v0, p3

    iget v11, v0, LX/1un;->n:I

    move-object/from16 v0, p3

    iget-object v12, v0, LX/1un;->o:LX/1dc;

    move-object/from16 v0, p3

    iget-object v13, v0, LX/1un;->p:LX/1Up;

    move-object/from16 v0, p3

    iget v14, v0, LX/1un;->q:I

    move-object/from16 v0, p3

    iget-object v15, v0, LX/1un;->r:LX/1dc;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v15}, LX/1uo;->a(LX/1De;LX/1oI;LX/4Ab;Landroid/graphics/ColorFilter;LX/1dc;LX/1Up;LX/1dc;LX/1Up;LX/1dc;LX/1Up;ILX/1dc;LX/1Up;ILX/1dc;)V

    .line 341471
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 341468
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 341465
    check-cast p2, LX/1oI;

    .line 341466
    invoke-virtual {p2}, LX/1oI;->c()V

    .line 341467
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 341453
    check-cast p3, LX/1un;

    move-object v0, p2

    .line 341454
    check-cast v0, LX/1oI;

    iget-object v1, p3, LX/1un;->b:LX/1Up;

    iget-object v2, p3, LX/1un;->c:Landroid/graphics/PointF;

    iget-object v3, p3, LX/1un;->d:Landroid/graphics/PointF;

    iget-object v4, p3, LX/1un;->e:LX/1Up;

    iget-object v5, p3, LX/1un;->f:LX/1aZ;

    .line 341455
    invoke-virtual {v0}, LX/1oI;->d()LX/1aY;

    move-result-object p0

    check-cast p0, LX/1oH;

    .line 341456
    if-eqz v3, :cond_2

    .line 341457
    sget-object p1, LX/1Up;->h:LX/1Up;

    invoke-virtual {p0, p1}, LX/1oH;->a(LX/1Up;)V

    .line 341458
    invoke-virtual {p0, v3}, LX/1oH;->b(Landroid/graphics/PointF;)V

    .line 341459
    :cond_0
    :goto_0
    sget-object p1, LX/1Up;->h:LX/1Up;

    if-ne v1, p1, :cond_1

    .line 341460
    invoke-virtual {p0, v2}, LX/1oH;->a(Landroid/graphics/PointF;)V

    .line 341461
    :cond_1
    invoke-virtual {v0, v5}, LX/1oI;->a(LX/1aZ;)V

    .line 341462
    return-void

    .line 341463
    :cond_2
    if-eqz v4, :cond_0

    .line 341464
    invoke-virtual {p0, v4}, LX/1oH;->a(LX/1Up;)V

    goto :goto_0
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 341450
    check-cast p2, LX/1oI;

    .line 341451
    invoke-virtual {p2}, LX/1oI;->e()V

    .line 341452
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 341449
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 341447
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 341448
    const/16 v0, 0xf

    return v0
.end method
