.class public final LX/1yc;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/1xV;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1yb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xV",
            "<TE;>.FeedStoryHeaderActionsComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/1xV;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/1xV;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 350856
    iput-object p1, p0, LX/1yc;->b:LX/1xV;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 350857
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/1yc;->c:[Ljava/lang/String;

    .line 350858
    iput v3, p0, LX/1yc;->d:I

    .line 350859
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/1yc;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/1yc;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/1yc;LX/1De;IILX/1yb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/1xV",
            "<TE;>.FeedStoryHeaderActionsComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 350860
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 350861
    iput-object p4, p0, LX/1yc;->a:LX/1yb;

    .line 350862
    iget-object v0, p0, LX/1yc;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 350863
    return-void
.end method


# virtual methods
.method public final a(LX/1Pb;)LX/1yc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350864
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-object p1, v0, LX/1yb;->b:LX/1Pb;

    .line 350865
    iget-object v0, p0, LX/1yc;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 350866
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1yc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350869
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-object p1, v0, LX/1yb;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 350870
    iget-object v0, p0, LX/1yc;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 350871
    return-object p0
.end method

.method public final a(Z)LX/1yc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350867
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-boolean p1, v0, LX/1yb;->d:Z

    .line 350868
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 350852
    invoke-super {p0}, LX/1X5;->a()V

    .line 350853
    const/4 v0, 0x0

    iput-object v0, p0, LX/1yc;->a:LX/1yb;

    .line 350854
    iget-object v0, p0, LX/1yc;->b:LX/1xV;

    iget-object v0, v0, LX/1xV;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 350855
    return-void
.end method

.method public final b(Z)LX/1yc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350850
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-boolean p1, v0, LX/1yb;->e:Z

    .line 350851
    return-object p0
.end method

.method public final c(Z)LX/1yc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350832
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-boolean p1, v0, LX/1yb;->f:Z

    .line 350833
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/1xV;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 350840
    iget-object v1, p0, LX/1yc;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/1yc;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/1yc;->d:I

    if-ge v1, v2, :cond_2

    .line 350841
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 350842
    :goto_0
    iget v2, p0, LX/1yc;->d:I

    if-ge v0, v2, :cond_1

    .line 350843
    iget-object v2, p0, LX/1yc;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 350844
    iget-object v2, p0, LX/1yc;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350845
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 350846
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350847
    :cond_2
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    .line 350848
    invoke-virtual {p0}, LX/1yc;->a()V

    .line 350849
    return-object v0
.end method

.method public final d(Z)LX/1yc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350838
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-boolean p1, v0, LX/1yb;->g:Z

    .line 350839
    return-object p0
.end method

.method public final e(Z)LX/1yc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350836
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput-boolean p1, v0, LX/1yb;->h:Z

    .line 350837
    return-object p0
.end method

.method public final h(I)LX/1yc;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1xV",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 350834
    iget-object v0, p0, LX/1yc;->a:LX/1yb;

    iput p1, v0, LX/1yb;->c:I

    .line 350835
    return-object p0
.end method
