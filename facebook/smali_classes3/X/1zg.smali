.class public LX/1zg;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/1zn;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 353898
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 353899
    return-void
.end method


# virtual methods
.method public final a(LX/1zf;LX/1zi;)LX/1zn;
    .locals 12

    .prologue
    .line 353900
    new-instance v0, LX/1zn;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1zk;->a(LX/0QB;)LX/1zk;

    move-result-object v4

    check-cast v4, LX/1zk;

    invoke-static {p0}, LX/1zp;->a(LX/0QB;)LX/1zp;

    move-result-object v5

    check-cast v5, LX/1zp;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    .line 353901
    new-instance v8, LX/1zq;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v8, v2, v3}, LX/1zq;-><init>(LX/0Zb;LX/0SG;)V

    .line 353902
    move-object v8, v8

    .line 353903
    check-cast v8, LX/1zq;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    .line 353904
    new-instance v10, LX/1zr;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v10, v2, v3}, LX/1zr;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 353905
    move-object v10, v10

    .line 353906
    check-cast v10, LX/1zr;

    invoke-static {p0}, LX/1zl;->a(LX/0QB;)LX/1zl;

    move-result-object v11

    check-cast v11, LX/1zl;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v11}, LX/1zn;-><init>(Landroid/content/Context;LX/1zf;LX/1zi;LX/1zk;LX/1zp;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/os/Handler;LX/1zq;Ljava/util/concurrent/ExecutorService;LX/1zr;LX/1zl;)V

    .line 353907
    return-object v0
.end method
