.class public final LX/1y3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 350433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 350434
    if-nez p1, :cond_0

    .line 350435
    :goto_0
    return v0

    .line 350436
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 350437
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->m()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    invoke-static {p0, v2}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I

    move-result v2

    .line 350438
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 350439
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 350440
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 350441
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 350442
    if-nez v6, :cond_1

    .line 350443
    :goto_1
    move v6, v7

    .line 350444
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 350445
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 350446
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 350447
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 350448
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 350449
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 350450
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 350451
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 350452
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 350453
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350454
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 350455
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 350456
    const/4 v9, 0x3

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 350457
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v9

    invoke-virtual {p0, v7, v9, v7}, LX/186;->a(III)V

    .line 350458
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v8}, LX/186;->b(II)V

    .line 350459
    const/4 v8, 0x2

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v9

    invoke-virtual {p0, v8, v9, v7}, LX/186;->a(III)V

    .line 350460
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 350461
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLAppStoreApplication;)I
    .locals 18

    .prologue
    .line 350473
    if-nez p1, :cond_0

    .line 350474
    const/4 v2, 0x0

    .line 350475
    :goto_0
    return v2

    .line 350476
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 350477
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->j()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 350478
    const/4 v2, 0x0

    .line 350479
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->k()LX/0Px;

    move-result-object v4

    .line 350480
    if-eqz v4, :cond_4

    .line 350481
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    .line 350482
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 350483
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v7, v3

    .line 350484
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 350485
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2}, LX/186;->a([IZ)I

    move-result v2

    move v3, v2

    .line 350486
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v7

    .line 350487
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->m()Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 350488
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->n()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 350489
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->o()Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 350490
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1y3;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v11

    .line 350491
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->q()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v12

    .line 350492
    const/4 v2, 0x0

    .line 350493
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->r()LX/0Px;

    move-result-object v13

    .line 350494
    if-eqz v13, :cond_3

    .line 350495
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    new-array v14, v2, [I

    .line 350496
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 350497
    invoke-virtual {v13, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLImage;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v2

    aput v2, v14, v4

    .line 350498
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 350499
    :cond_2
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 350500
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->s()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I

    move-result v4

    .line 350501
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->t()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 350502
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->u()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 350503
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/1y3;->b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v15

    .line 350504
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->x()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 350505
    const/16 v17, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 350506
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 350507
    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 350508
    const/4 v5, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v3}, LX/186;->b(II)V

    .line 350509
    const/4 v3, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 350510
    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 350511
    const/4 v3, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 350512
    const/4 v3, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 350513
    const/4 v3, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 350514
    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 350515
    const/16 v3, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 350516
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 350517
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 350518
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 350519
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 350520
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLAppStoreApplication;->w()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 350521
    const/16 v2, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 350522
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 350523
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_4
    move v3, v2

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLApplication;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 350462
    if-nez p1, :cond_0

    .line 350463
    :goto_0
    return v0

    .line 350464
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350465
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 350466
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLApplication;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 350467
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 350468
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 350469
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 350470
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 350471
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350472
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 350382
    if-nez p1, :cond_0

    .line 350383
    :goto_0
    return v0

    .line 350384
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350385
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 350386
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 350387
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 350388
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 350389
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350390
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 350333
    if-nez p1, :cond_0

    .line 350334
    :goto_0
    return v0

    .line 350335
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 350336
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v2

    .line 350337
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->ad()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 350338
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 350339
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 350340
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->Z()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v6

    .line 350341
    if-nez v6, :cond_1

    .line 350342
    const/4 v7, 0x0

    .line 350343
    :goto_1
    move v6, v7

    .line 350344
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    const/4 v8, 0x0

    .line 350345
    if-nez v7, :cond_2

    .line 350346
    :goto_2
    move v7, v8

    .line 350347
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 350348
    const/16 v9, 0xb

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 350349
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 350350
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 350351
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 350352
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 350353
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->y()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 350354
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->B()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 350355
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->C()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 350356
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 350357
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 350358
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 350359
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 350360
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350361
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 350362
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 350363
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 350364
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 350365
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    invoke-static {p0, v10}, LX/1y3;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v10

    .line 350366
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGroup;->K()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 350367
    const/4 v12, 0x6

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 350368
    const/4 v12, 0x1

    invoke-virtual {p0, v12, v7}, LX/186;->b(II)V

    .line 350369
    const/4 v7, 0x2

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 350370
    const/4 v7, 0x3

    invoke-virtual {p0, v7, v9}, LX/186;->b(II)V

    .line 350371
    const/4 v7, 0x4

    invoke-virtual {p0, v7, v10}, LX/186;->b(II)V

    .line 350372
    const/4 v7, 0x5

    invoke-virtual {p0, v7, v11}, LX/186;->b(II)V

    .line 350373
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 350374
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 350375
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 350376
    const/4 v10, 0x3

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 350377
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v10

    invoke-virtual {p0, v8, v10, v8}, LX/186;->a(III)V

    .line 350378
    const/4 v10, 0x1

    invoke-virtual {p0, v10, v9}, LX/186;->b(II)V

    .line 350379
    const/4 v9, 0x2

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v10

    invoke-virtual {p0, v9, v10, v8}, LX/186;->a(III)V

    .line 350380
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 350381
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto/16 :goto_2
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 350391
    if-nez p1, :cond_0

    .line 350392
    :goto_0
    return v2

    .line 350393
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v3

    .line 350394
    if-eqz v3, :cond_2

    .line 350395
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 350396
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 350397
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    const/4 v6, 0x0

    .line 350398
    if-nez v0, :cond_3

    .line 350399
    :goto_2
    move v0, v6

    .line 350400
    aput v0, v4, v1

    .line 350401
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 350402
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 350403
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->c(Ljava/util/List;)I

    move-result v1

    .line 350404
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    const/4 v4, 0x0

    .line 350405
    if-nez v3, :cond_5

    .line 350406
    :goto_4
    move v3, v4

    .line 350407
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 350408
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 350409
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 350410
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 350411
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 350412
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 350413
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    .line 350414
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->af()Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;

    move-result-object v8

    const/4 v9, 0x0

    .line 350415
    if-nez v8, :cond_4

    .line 350416
    :goto_5
    move v8, v9

    .line 350417
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 350418
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 350419
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 350420
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 350421
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 350422
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 350423
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLLinkTargetStoreData;->a()Z

    move-result v0

    invoke-virtual {p0, v9, v0}, LX/186;->a(IZ)V

    .line 350424
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 350425
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_5

    .line 350426
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    .line 350427
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 350428
    const/4 v8, 0x2

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 350429
    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 350430
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 350431
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 350432
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 350175
    if-nez p1, :cond_0

    .line 350176
    :goto_0
    return v0

    .line 350177
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350178
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 350179
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 350180
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350181
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;
    .locals 9
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDispatchAvatarClickGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 350182
    if-nez p0, :cond_1

    .line 350183
    :cond_0
    :goto_0
    return-object v2

    .line 350184
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 350185
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 350186
    if-nez p0, :cond_3

    .line 350187
    :goto_1
    move v1, v4

    .line 350188
    if-eqz v1, :cond_0

    .line 350189
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 350190
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 350191
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 350192
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 350193
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 350194
    const-string v1, "DefaultHeaderPartConverter.getDispatchAvatarClickGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 350195
    :cond_2
    new-instance v2, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/graphqlstory/header/DefaultHeaderPartDataProviderForTextLayoutGraphQLModels$DispatchAvatarClickGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 350196
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    .line 350197
    if-eqz v5, :cond_7

    .line 350198
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    new-array v6, v1, [I

    move v3, v4

    .line 350199
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 350200
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-static {v0, v1}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLActor;)I

    move-result v1

    aput v1, v6, v3

    .line 350201
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 350202
    :cond_4
    invoke-virtual {v0, v6, v8}, LX/186;->a([IZ)I

    move-result v1

    move v3, v1

    .line 350203
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    .line 350204
    if-eqz v6, :cond_6

    .line 350205
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    new-array v7, v1, [I

    move v5, v4

    .line 350206
    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v5, v1, :cond_5

    .line 350207
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0, v1}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v1

    aput v1, v7, v5

    .line 350208
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 350209
    :cond_5
    invoke-virtual {v0, v7, v8}, LX/186;->a([IZ)I

    move-result v1

    .line 350210
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 350211
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 350212
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 350213
    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 350214
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 350215
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    .line 350216
    invoke-virtual {v0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v4

    goto :goto_5

    :cond_7
    move v3, v4

    goto :goto_3
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 350217
    if-nez p1, :cond_0

    .line 350218
    :goto_0
    return v2

    .line 350219
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    .line 350220
    if-eqz v3, :cond_2

    .line 350221
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 350222
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 350223
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v5, 0x0

    .line 350224
    if-nez v0, :cond_3

    .line 350225
    :goto_2
    move v0, v5

    .line 350226
    aput v0, v4, v1

    .line 350227
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 350228
    :cond_1
    invoke-virtual {p0, v4, v7}, LX/186;->a([IZ)I

    move-result v0

    .line 350229
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350230
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-static {p0, v3}, LX/1y3;->a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I

    move-result v3

    .line 350231
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 350232
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bb()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    const/4 v6, 0x0

    .line 350233
    if-nez v5, :cond_4

    .line 350234
    :goto_4
    move v5, v6

    .line 350235
    const/4 v6, 0x5

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 350236
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 350237
    invoke-virtual {p0, v7, v1}, LX/186;->b(II)V

    .line 350238
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 350239
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 350240
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 350241
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 350242
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 350243
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    .line 350244
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 350245
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 350246
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    invoke-static {p0, v10}, LX/1y3;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v10

    .line 350247
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 350248
    const/4 v12, 0x7

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 350249
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 350250
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v8}, LX/186;->b(II)V

    .line 350251
    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 350252
    const/4 v5, 0x3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 350253
    const/4 v5, 0x4

    invoke-virtual {p0, v5, v9}, LX/186;->b(II)V

    .line 350254
    const/4 v5, 0x5

    invoke-virtual {p0, v5, v10}, LX/186;->b(II)V

    .line 350255
    const/4 v5, 0x6

    invoke-virtual {p0, v5, v11}, LX/186;->b(II)V

    .line 350256
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 350257
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 350258
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    .line 350259
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 350260
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 350261
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-static {p0, v11}, LX/1y3;->d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v11

    .line 350262
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 350263
    const/4 p1, 0x7

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 350264
    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 350265
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v9}, LX/186;->b(II)V

    .line 350266
    const/4 v6, 0x2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->S()Z

    move-result v8

    invoke-virtual {p0, v6, v8}, LX/186;->a(IZ)V

    .line 350267
    const/4 v6, 0x3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->U()Z

    move-result v8

    invoke-virtual {p0, v6, v8}, LX/186;->a(IZ)V

    .line 350268
    const/4 v6, 0x4

    invoke-virtual {p0, v6, v10}, LX/186;->b(II)V

    .line 350269
    const/4 v6, 0x5

    invoke-virtual {p0, v6, v11}, LX/186;->b(II)V

    .line 350270
    const/4 v6, 0x6

    invoke-virtual {p0, v6, v12}, LX/186;->b(II)V

    .line 350271
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 350272
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 350273
    if-nez p1, :cond_0

    .line 350274
    :goto_0
    return v2

    .line 350275
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 350276
    if-eqz v3, :cond_2

    .line 350277
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 350278
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 350279
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    const/4 v6, 0x0

    .line 350280
    if-nez v0, :cond_3

    .line 350281
    :goto_2
    move v0, v6

    .line 350282
    aput v0, v4, v1

    .line 350283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 350284
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 350285
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350286
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 350287
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 350288
    invoke-virtual {p0, v5, v1}, LX/186;->b(II)V

    .line 350289
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 350290
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 350291
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    const/4 v8, 0x0

    .line 350292
    if-nez v7, :cond_4

    .line 350293
    :goto_4
    move v7, v8

    .line 350294
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 350295
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 350296
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 350297
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 350298
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    .line 350299
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 350300
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 350301
    if-nez v11, :cond_5

    .line 350302
    :goto_5
    move v11, v12

    .line 350303
    const/4 v12, 0x3

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 350304
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 350305
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 350306
    const/4 v8, 0x2

    invoke-virtual {p0, v8, v11}, LX/186;->b(II)V

    .line 350307
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 350308
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4

    .line 350309
    :cond_5
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 350310
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 350311
    invoke-virtual {p0, v12, v0}, LX/186;->b(II)V

    .line 350312
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 350313
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 350314
    if-nez p0, :cond_1

    .line 350315
    :cond_0
    :goto_0
    return-object v2

    .line 350316
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 350317
    invoke-static {v0, p0}, LX/1y3;->b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v1

    .line 350318
    if-eqz v1, :cond_0

    .line 350319
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 350320
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 350321
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 350322
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 350323
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 350324
    const-string v1, "DefaultHeaderPartConverter.getHeaderTitleSpannableBuilderGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 350325
    :cond_2
    new-instance v2, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/graphqlstory/header/HeaderTitleSpannableBuilderGraphQLModels$HeaderTitleSpannableBuilderGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static d(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 350326
    if-nez p1, :cond_0

    .line 350327
    :goto_0
    return v0

    .line 350328
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 350329
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 350330
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 350331
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 350332
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
