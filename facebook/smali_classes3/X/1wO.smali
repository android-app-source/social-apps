.class public LX/1wO;
.super LX/0hD;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0xX;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hn;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AX;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 345835
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 345836
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 345837
    iput-object v0, p0, LX/1wO;->a:LX/0Ot;

    .line 345838
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 345839
    iput-object v0, p0, LX/1wO;->b:LX/0Ot;

    .line 345840
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 345841
    iput-object v0, p0, LX/1wO;->c:LX/0Ot;

    .line 345842
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 345843
    iput-object v0, p0, LX/1wO;->d:LX/0Ot;

    .line 345844
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 345845
    iput-object v0, p0, LX/1wO;->e:LX/0Ot;

    .line 345846
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 345847
    iget-object v0, p0, LX/1wO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->APP_STATE_MANAGER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 345848
    iget-object v0, p0, LX/1wO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345849
    const-string v0, "FbMainTabFragmentVideoFetchController.onUserEnteredApp"

    const v1, -0x291c30cd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 345850
    :try_start_0
    iget-object v0, p0, LX/1wO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    iget-object v1, p0, LX/1wO;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/3AW;->a(J)V

    .line 345851
    iget-object v0, p0, LX/1wO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hn;

    invoke-virtual {v0}, LX/0hn;->c()V

    .line 345852
    iget-object v0, p0, LX/1wO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0xX;

    sget-object v1, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v0, v1}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345853
    iget-object v0, p0, LX/1wO;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AX;

    invoke-virtual {v0}, LX/3AX;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345854
    :cond_0
    const v0, 0x30cfa1d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 345855
    :cond_1
    return-void

    .line 345856
    :catchall_0
    move-exception v0

    const v1, -0x5130e67d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
