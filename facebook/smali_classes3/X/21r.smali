.class public LX/21r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/21s;

.field public final b:LX/0Sh;


# direct methods
.method public constructor <init>(LX/21s;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 358463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358464
    iput-object p1, p0, LX/21r;->a:LX/21s;

    .line 358465
    iput-object p2, p0, LX/21r;->b:LX/0Sh;

    .line 358466
    return-void
.end method

.method public static a(LX/0QB;)LX/21r;
    .locals 5

    .prologue
    .line 358467
    const-class v1, LX/21r;

    monitor-enter v1

    .line 358468
    :try_start_0
    sget-object v0, LX/21r;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 358469
    sput-object v2, LX/21r;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 358470
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358471
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 358472
    new-instance p0, LX/21r;

    invoke-static {v0}, LX/21s;->a(LX/0QB;)LX/21s;

    move-result-object v3

    check-cast v3, LX/21s;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/21r;-><init>(LX/21s;LX/0Sh;)V

    .line 358473
    move-object v0, p0

    .line 358474
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 358475
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/21r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358476
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 358477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
