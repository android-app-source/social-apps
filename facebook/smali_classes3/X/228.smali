.class public final LX/228;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1kt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1kt",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1ks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ks",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/0sZ;


# direct methods
.method public constructor <init>(LX/0sZ;LX/1ks;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ks",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 359157
    iput-object p1, p0, LX/228;->c:LX/0sZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359158
    const/4 v0, 0x0

    iput-object v0, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    .line 359159
    iput-object p2, p0, LX/228;->b:LX/1ks;

    .line 359160
    return-void
.end method

.method public constructor <init>(LX/0sZ;Lcom/facebook/api/ufiservices/common/FetchNodeListParams;LX/1ks;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/ufiservices/common/FetchNodeListParams;",
            "LX/1ks",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 359161
    iput-object p1, p0, LX/228;->c:LX/0sZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359162
    iput-object p2, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    .line 359163
    iput-object p3, p0, LX/228;->b:LX/1ks;

    .line 359164
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 359165
    iget-object v0, p0, LX/228;->b:LX/1ks;

    invoke-virtual {v0}, LX/1ks;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 359166
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 359167
    if-eqz v0, :cond_4

    .line 359168
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 359169
    if-eqz v0, :cond_0

    .line 359170
    instance-of v1, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    if-eqz v1, :cond_1

    .line 359171
    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359172
    :cond_1
    instance-of v1, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    if-eqz v1, :cond_2

    .line 359173
    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359174
    :cond_2
    instance-of v1, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    if-eqz v1, :cond_3

    .line 359175
    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359176
    :cond_3
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 359177
    iget-object v1, p0, LX/228;->c:LX/0sZ;

    iget-object v1, v1, LX/0sZ;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3HE;

    invoke-virtual {v1, v0}, LX/3HE;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0

    .line 359178
    :cond_4
    iget-object v0, p0, LX/228;->b:LX/1ks;

    invoke-virtual {v0, p1}, LX/1ks;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    .line 359179
    iget-object v0, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    if-eqz v0, :cond_5

    .line 359180
    :cond_5
    return v3
.end method

.method public final b()Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 359181
    iget-object v0, p0, LX/228;->b:LX/1ks;

    invoke-virtual {v0}, LX/1ks;->b()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 359182
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 359183
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_1

    .line 359184
    :cond_0
    return-object p1

    .line 359185
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 359186
    if-eqz v0, :cond_2

    .line 359187
    instance-of v2, v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    if-eqz v2, :cond_3

    .line 359188
    check-cast v0, Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;

    invoke-static {v0}, LX/6BQ;->a(Lcom/facebook/api/graphql/fetchfeedback/FetchLikersGraphQLModels$StaticLikersModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359189
    :cond_3
    instance-of v2, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    if-eqz v2, :cond_4

    .line 359190
    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFIFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359191
    :cond_4
    instance-of v2, v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    if-eqz v2, :cond_5

    .line 359192
    check-cast v0, Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;

    invoke-static {v0}, LX/6BS;->a(Lcom/facebook/api/graphql/fetchcomments/FetchCommentsGraphQLModels$UFILastFeedbackQueryModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 359193
    :cond_5
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 359194
    iget-object v2, p0, LX/228;->c:LX/0sZ;

    .line 359195
    iget-object v3, v2, LX/0sZ;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    .line 359196
    invoke-virtual {v0, v3, v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 359197
    iget-object v2, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    .line 359198
    iget-object v3, v2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->d:Ljava/lang/String;

    move-object v2, v3

    .line 359199
    if-nez v2, :cond_7

    iget-object v2, p0, LX/228;->a:Lcom/facebook/api/ufiservices/common/FetchNodeListParams;

    .line 359200
    iget-object v3, v2, Lcom/facebook/api/ufiservices/common/FetchNodeListParams;->c:Ljava/lang/String;

    move-object v2, v3

    .line 359201
    if-nez v2, :cond_7

    :cond_6
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 359202
    invoke-static {v0, v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_1
.end method
