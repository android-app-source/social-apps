.class public final LX/200;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;)V
    .locals 0

    .prologue
    .line 354310
    iput-object p1, p0, LX/200;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 354311
    iget-object v0, p0, LX/200;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->a:LX/1zq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1zq;->a(Z)V

    .line 354312
    iget-object v0, p0, LX/200;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->e:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 354313
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 354314
    check-cast p1, Ljava/util/List;

    .line 354315
    if-nez p1, :cond_0

    .line 354316
    :goto_0
    return-void

    .line 354317
    :cond_0
    iget-object v0, p0, LX/200;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->a:LX/1zq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1zq;->a(Z)V

    .line 354318
    iget-object v0, p0, LX/200;->a:Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;

    iget-object v0, v0, Lcom/facebook/feedback/reactions/api/FeedbackReactionsSettingsFetcher$1;->e:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
