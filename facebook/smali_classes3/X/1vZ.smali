.class public LX/1vZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/1vZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343131
    const-class v0, LX/1vZ;

    sput-object v0, LX/1vZ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 343204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343205
    return-void
.end method

.method public static a(Landroid/view/View;)LX/1vY;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 343198
    if-nez p0, :cond_0

    move-object v0, v1

    .line 343199
    :goto_0
    return-object v0

    .line 343200
    :cond_0
    const v0, 0x7f0d00f5

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 343201
    if-eqz v0, :cond_1

    .line 343202
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/1vY;->getTrackingNodeByType(I)LX/1vY;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 343203
    goto :goto_0
.end method

.method public static a(LX/1vY;)Landroid/util/SparseArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vY;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343195
    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 343196
    const v1, 0x7f0d00f5

    invoke-virtual {p0}, LX/1vY;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 343197
    return-object v0
.end method

.method public static a(Landroid/view/View;LX/1vY;)V
    .locals 2

    .prologue
    .line 343189
    if-nez p0, :cond_0

    .line 343190
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "View is null, can\'t set tracking node to view"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 343191
    :goto_0
    return-void

    .line 343192
    :cond_0
    if-nez p1, :cond_1

    .line 343193
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "Tracking node is null, can\'t set tracking node to view"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 343194
    :cond_1
    const v0, 0x7f0d00f5

    invoke-virtual {p1}, LX/1vY;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/1vY;)V
    .locals 2
    .param p1    # LX/1vY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 343183
    if-nez p0, :cond_0

    .line 343184
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "HoneyClientEvent is null, can\'t set tracking node to event"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 343185
    :goto_0
    return-void

    .line 343186
    :cond_0
    if-nez p1, :cond_1

    .line 343187
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "Tracking node is null, can\'t set tracking node to event"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 343188
    :cond_1
    const-string v0, "tn"

    invoke-static {p1}, LX/1vZ;->b(LX/1vY;)LX/162;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 343175
    if-nez p0, :cond_1

    .line 343176
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "HoneyClientEvent is null, can\'t set tracking node to event"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 343177
    :cond_0
    :goto_0
    return-void

    .line 343178
    :cond_1
    if-nez p1, :cond_2

    .line 343179
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "View is null, can\'t set tracking node to event"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 343180
    :cond_2
    invoke-static {p1}, LX/1vZ;->b(Landroid/view/View;)LX/162;

    move-result-object v0

    .line 343181
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    if-lez v1, :cond_0

    .line 343182
    const-string v1, "tn"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z
    .locals 1

    .prologue
    .line 343174
    if-eqz p0, :cond_0

    const-string v0, "tn"

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->e(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1vY;)LX/162;
    .locals 3

    .prologue
    .line 343166
    if-nez p0, :cond_0

    .line 343167
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "Tracking node is null, can\'t convert to ArrayNode"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 343168
    const/4 v0, 0x0

    .line 343169
    :goto_0
    return-object v0

    .line 343170
    :cond_0
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 343171
    new-instance v1, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/162;-><init>(LX/0mC;)V

    .line 343172
    invoke-virtual {p0}, LX/1vY;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, LX/162;->c(I)LX/162;

    .line 343173
    invoke-virtual {v0, v1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0
.end method

.method private static b(Landroid/view/View;)LX/162;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 343132
    if-nez p0, :cond_0

    .line 343133
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    const-string v1, "Target View is null, can\'t traverse up view hierarchy"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v6

    .line 343134
    :goto_0
    return-object v0

    .line 343135
    :cond_0
    const v0, 0x7f0d00f5

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 343136
    sget-object v0, LX/1vZ;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View does not have a tracking node set, when it should. Perhaps view is not the view you were expecting. View is of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 343137
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    move-object v0, p0

    .line 343138
    :cond_2
    check-cast v0, Landroid/view/View;

    .line 343139
    const v1, 0x7f0d00f5

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 343140
    if-eqz v1, :cond_4

    .line 343141
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 343142
    instance-of v2, v3, Landroid/view/ViewGroup;

    if-eqz v2, :cond_7

    .line 343143
    const/4 v4, -0x1

    .line 343144
    const/4 v2, 0x0

    move v5, v4

    move v4, v2

    :goto_1
    move-object v2, v3

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v4, v2, :cond_4

    move-object v2, v3

    .line 343145
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 343146
    const v2, 0x7f0d00f5

    invoke-virtual {v8, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 343147
    if-eqz v2, :cond_3

    invoke-virtual {v2, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 343148
    add-int/lit8 v5, v5, 0x1

    .line 343149
    :cond_3
    invoke-virtual {v0, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 343150
    new-instance v2, LX/558;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/558;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343151
    :cond_4
    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 343152
    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_2

    .line 343153
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 343154
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/558;

    .line 343155
    new-instance v3, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/162;-><init>(LX/0mC;)V

    .line 343156
    iget-object v4, v0, LX/558;->a:Ljava/lang/Integer;

    move-object v4, v4

    .line 343157
    invoke-virtual {v3, v4}, LX/162;->a(Ljava/lang/Integer;)LX/162;

    .line 343158
    iget-object v4, v0, LX/558;->b:Ljava/lang/Integer;

    move-object v4, v4

    .line 343159
    if-eqz v4, :cond_5

    .line 343160
    iget-object v4, v0, LX/558;->b:Ljava/lang/Integer;

    move-object v0, v4

    .line 343161
    invoke-virtual {v3, v0}, LX/162;->a(Ljava/lang/Integer;)LX/162;

    .line 343162
    :cond_5
    invoke-virtual {v1, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_3

    .line 343163
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 343164
    :cond_7
    new-instance v2, LX/558;

    invoke-direct {v2, v1, v6}, LX/558;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    move-object v0, v1

    .line 343165
    goto/16 :goto_0
.end method
