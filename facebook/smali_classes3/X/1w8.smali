.class public LX/1w8;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 345018
    const/4 v0, 0x0

    sput-object v0, LX/1w8;->a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 345019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345020
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    .locals 2

    .prologue
    .line 345021
    new-instance v0, LX/18L;

    invoke-direct {v0}, LX/18L;-><init>()V

    .line 345022
    const-string v1, "local_last_negative_feedback_action_type"

    invoke-interface {p0, v1, v0}, LX/16f;->a(Ljava/lang/String;LX/18L;)V

    .line 345023
    iget-object v1, v0, LX/18L;->a:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 345024
    sget-object v0, LX/1w8;->a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 345025
    :goto_0
    return-object v0

    .line 345026
    :cond_0
    :try_start_0
    iget-object v0, v0, LX/18L;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 345027
    :catch_0
    sget-object v0, LX/1w8;->a:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    goto :goto_0
.end method

.method public static a(LX/16e;)Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;
    .locals 1

    .prologue
    .line 345028
    invoke-interface {p0}, LX/16e;->x()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v0

    return-object v0
.end method
