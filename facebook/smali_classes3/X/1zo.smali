.class public final enum LX/1zo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/1zo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/1zo;

.field public static final enum LARGE:LX/1zo;

.field public static final enum SMALL:LX/1zo;

.field public static final enum TAB_ICONS:LX/1zo;

.field public static final enum VECTOR:LX/1zo;


# instance fields
.field private final mImageDirectory:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 354160
    new-instance v0, LX/1zo;

    const-string v1, "SMALL"

    const-string v2, "feedback/reactions/small_images/"

    invoke-direct {v0, v1, v3, v2}, LX/1zo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1zo;->SMALL:LX/1zo;

    .line 354161
    new-instance v0, LX/1zo;

    const-string v1, "LARGE"

    const-string v2, "feedback/reactions/large_images/"

    invoke-direct {v0, v1, v4, v2}, LX/1zo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1zo;->LARGE:LX/1zo;

    .line 354162
    new-instance v0, LX/1zo;

    const-string v1, "TAB_ICONS"

    const-string v2, "feedback/reactions/tab_icons/"

    invoke-direct {v0, v1, v5, v2}, LX/1zo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1zo;->TAB_ICONS:LX/1zo;

    .line 354163
    new-instance v0, LX/1zo;

    const-string v1, "VECTOR"

    const-string v2, "feedback/reactions/vector/"

    invoke-direct {v0, v1, v6, v2}, LX/1zo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/1zo;->VECTOR:LX/1zo;

    .line 354164
    const/4 v0, 0x4

    new-array v0, v0, [LX/1zo;

    sget-object v1, LX/1zo;->SMALL:LX/1zo;

    aput-object v1, v0, v3

    sget-object v1, LX/1zo;->LARGE:LX/1zo;

    aput-object v1, v0, v4

    sget-object v1, LX/1zo;->TAB_ICONS:LX/1zo;

    aput-object v1, v0, v5

    sget-object v1, LX/1zo;->VECTOR:LX/1zo;

    aput-object v1, v0, v6

    sput-object v0, LX/1zo;->$VALUES:[LX/1zo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 354154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 354155
    iput-object p3, p0, LX/1zo;->mImageDirectory:Ljava/lang/String;

    .line 354156
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/1zo;
    .locals 1

    .prologue
    .line 354159
    const-class v0, LX/1zo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/1zo;

    return-object v0
.end method

.method public static values()[LX/1zo;
    .locals 1

    .prologue
    .line 354158
    sget-object v0, LX/1zo;->$VALUES:[LX/1zo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/1zo;

    return-object v0
.end method


# virtual methods
.method public final getImageDirectory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354157
    iget-object v0, p0, LX/1zo;->mImageDirectory:Ljava/lang/String;

    return-object v0
.end method
