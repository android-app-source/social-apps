.class public final LX/1vp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        "LX/3DK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V
    .locals 0

    .prologue
    .line 344748
    iput-object p1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 344737
    sget-object v0, LX/3Cb;->DISK:LX/3Cb;

    if-ne p2, v0, :cond_0

    .line 344738
    :goto_0
    return-void

    .line 344739
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 344740
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    .line 344741
    iget-object v1, v0, LX/3CY;->a:LX/3Ca;

    sget-object v6, LX/3Ca;->INSERT:LX/3Ca;

    if-ne v1, v6, :cond_1

    move v1, v2

    .line 344742
    :goto_2
    iget v6, v0, LX/3CY;->b:I

    if-ge v1, v6, :cond_1

    .line 344743
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v6

    add-int/2addr v6, v1

    invoke-interface {p4, v6}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344744
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 344745
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 344746
    :cond_2
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSD;

    sget-object v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, LX/CSD;->a(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344747
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 344710
    check-cast p3, LX/3DK;

    .line 344711
    if-nez p3, :cond_1

    .line 344712
    :cond_0
    :goto_0
    return-void

    .line 344713
    :cond_1
    iget-boolean v0, p3, LX/3DK;->d:Z

    move v0, v0

    .line 344714
    if-eqz v0, :cond_2

    .line 344715
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 344716
    sget-object v1, LX/2nk;->INITIAL:LX/2nk;

    if-eq v0, v1, :cond_2

    .line 344717
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 344718
    iget-object v1, p3, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v1, v1

    .line 344719
    iget-object v2, v1, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v2

    .line 344720
    invoke-virtual {v0, v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 344721
    :cond_2
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 344722
    sget-object v1, LX/2nk;->AFTER:LX/2nk;

    if-eq v0, v1, :cond_0

    .line 344723
    sget-object v0, LX/3DO;->FULL:LX/3DO;

    .line 344724
    iget-object v1, p3, LX/3DK;->c:LX/3DO;

    move-object v1, v1

    .line 344725
    if-ne v0, v1, :cond_3

    const v0, 0x350002

    .line 344726
    :goto_1
    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 344727
    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 344728
    iget-object v2, p3, LX/3DK;->b:LX/2ub;

    move-object v2, v2

    .line 344729
    iget-object v2, v2, LX/2ub;->name:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 344730
    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v2, v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 344731
    iget-boolean v1, p3, LX/3DK;->e:Z

    move v1, v1

    .line 344732
    if-eqz v1, :cond_4

    const-string v1, "recursive_fetch"

    :goto_2
    invoke-interface {v2, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 344733
    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v2, v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->i:LX/1rU;

    invoke-virtual {v1}, LX/1rU;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "connection_controller"

    :goto_3
    invoke-interface {v2, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 344734
    :cond_3
    const v0, 0x350003

    goto :goto_1

    .line 344735
    :cond_4
    const-string v1, "not_recursive_fetch"

    goto :goto_2

    .line 344736
    :cond_5
    const-string v1, "not_connection_controller"

    goto :goto_3
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 344691
    check-cast p3, LX/3DK;

    .line 344692
    if-eqz p3, :cond_0

    .line 344693
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 344694
    sget-object v1, LX/2nk;->AFTER:LX/2nk;

    if-ne v0, v1, :cond_1

    .line 344695
    :cond_0
    :goto_0
    return-void

    .line 344696
    :cond_1
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v0, LX/3DO;->FULL:LX/3DO;

    .line 344697
    iget-object v2, p3, LX/3DK;->c:LX/3DO;

    move-object v2, v2

    .line 344698
    if-ne v0, v2, :cond_2

    const v0, 0x350002

    :goto_1
    const/4 v2, 0x3

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 344699
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 344700
    iget-object v1, p3, LX/3DK;->b:LX/2ub;

    move-object v1, v1

    .line 344701
    iget-object v2, p3, LX/3DK;->c:LX/3DO;

    move-object v2, v2

    .line 344702
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/2ub;LX/3DO;Z)V

    .line 344703
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    monitor-enter v1

    .line 344704
    :try_start_0
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drn;

    .line 344705
    invoke-virtual {v0, p4}, LX/Drn;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 344706
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 344707
    :cond_2
    const v0, 0x350003

    goto :goto_1

    .line 344708
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 344709
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 344648
    check-cast p3, LX/3DK;

    .line 344649
    if-eqz p3, :cond_0

    .line 344650
    iget-object v0, p1, LX/2nj;->c:LX/2nk;

    move-object v0, v0

    .line 344651
    sget-object v1, LX/2nk;->AFTER:LX/2nk;

    if-ne v0, v1, :cond_1

    .line 344652
    :cond_0
    :goto_0
    return-void

    .line 344653
    :cond_1
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v0, LX/3DO;->FULL:LX/3DO;

    .line 344654
    iget-object v2, p3, LX/3DK;->c:LX/3DO;

    move-object v2, v2

    .line 344655
    if-ne v0, v2, :cond_2

    const v0, 0x350002

    :goto_1
    const/4 v2, 0x2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 344656
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 344657
    iget-object v1, p3, LX/3DK;->b:LX/2ub;

    move-object v1, v1

    .line 344658
    iget-object v2, p3, LX/3DK;->c:LX/3DO;

    move-object v2, v2

    .line 344659
    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/2ub;LX/3DO;Z)V

    .line 344660
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    monitor-enter v1

    .line 344661
    :try_start_0
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drn;

    .line 344662
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/Drn;->onSuccess(Ljava/lang/Object;)V

    goto :goto_2

    .line 344663
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 344664
    :cond_2
    const v0, 0x350003

    goto :goto_1

    .line 344665
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 344666
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344667
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->g:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$NotificationConnectionControllerSyncManagerListener$1;

    invoke-direct {v1, p0, p3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$NotificationConnectionControllerSyncManagerListener$1;-><init>(LX/1vp;LX/3DK;)V

    const v2, -0x55d750a8

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 344668
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    invoke-interface {v0}, LX/2kM;->a()LX/2nj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    invoke-interface {v0}, LX/2kM;->a()LX/2nj;

    move-result-object v0

    .line 344669
    iget-object v1, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v1

    .line 344670
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 344671
    iget-object v0, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 344672
    if-eqz v0, :cond_0

    .line 344673
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    invoke-interface {v0}, LX/2kM;->a()LX/2nj;

    move-result-object v0

    .line 344674
    iget-boolean v1, v0, LX/2nj;->d:Z

    move v0, v1

    .line 344675
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v0

    invoke-interface {v0}, LX/2kM;->a()LX/2nj;

    move-result-object v0

    .line 344676
    iget-object v1, v0, LX/2nj;->b:Ljava/lang/String;

    move-object v0, v1

    .line 344677
    iget-object v1, p1, LX/2nj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 344678
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344679
    iget-object v0, p3, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v0, v0

    .line 344680
    if-nez v0, :cond_4

    .line 344681
    :goto_3
    goto/16 :goto_0

    .line 344682
    :cond_4
    iget-object v0, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    iget-object v1, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v1

    invoke-interface {v1}, LX/2kM;->a()LX/2nj;

    move-result-object v1

    sget-object v2, LX/3DP;->LAST:LX/3DP;

    const/16 v3, 0xa

    new-instance v4, LX/3DK;

    iget-object v5, p0, LX/1vp;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 344683
    iget-object v6, p3, LX/3DK;->a:Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-object v6, v6

    .line 344684
    iget-object p1, v6, Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v6, p1

    .line 344685
    iget-object p1, p3, LX/3DK;->b:LX/2ub;

    move-object p1, p1

    .line 344686
    sget-object p2, LX/0Q7;->a:LX/0Px;

    move-object p2, p2

    .line 344687
    invoke-static {v5, v6, p1, p2}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/0Px;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v5

    .line 344688
    iget-object v6, p3, LX/3DK;->b:LX/2ub;

    move-object v6, v6

    .line 344689
    iget-object p1, p3, LX/3DK;->c:LX/3DO;

    move-object p1, p1

    .line 344690
    const/4 p2, 0x1

    invoke-direct {v4, v5, v6, p1, p2}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;Z)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    goto :goto_3
.end method
