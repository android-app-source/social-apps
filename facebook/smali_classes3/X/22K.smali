.class public final LX/22K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 359376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/15w;)LX/22L;
    .locals 6

    .prologue
    .line 359377
    new-instance v0, LX/22L;

    invoke-direct {v0}, LX/22L;-><init>()V

    .line 359378
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v2, :cond_1

    .line 359379
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 359380
    const/4 v0, 0x0

    .line 359381
    :cond_0
    return-object v0

    .line 359382
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_0

    .line 359383
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 359384
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 359385
    const/4 v2, 0x0

    .line 359386
    const-string v4, "bucket"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 359387
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-ne v4, v5, :cond_2

    :goto_1
    iput-object v2, v0, LX/22L;->a:Ljava/lang/String;

    .line 359388
    :goto_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_0

    .line 359389
    :cond_2
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 359390
    :cond_3
    const-string v4, "values"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 359391
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_5

    .line 359392
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 359393
    :cond_4
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_5

    .line 359394
    invoke-static {p0}, LX/21t;->a(LX/15w;)LX/21u;

    move-result-object v4

    .line 359395
    if-eqz v4, :cond_4

    .line 359396
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 359397
    :cond_5
    iput-object v2, v0, LX/22L;->b:Ljava/util/List;

    .line 359398
    goto :goto_2

    .line 359399
    :cond_6
    goto :goto_2
.end method
