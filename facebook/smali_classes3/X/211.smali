.class public final enum LX/211;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/211;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/211;

.field public static final enum ABOVE:LX/211;

.field public static final enum BELOW:LX/211;

.field public static final enum LEFT:LX/211;

.field public static final enum OVER:LX/211;

.field public static final enum RIGHT:LX/211;

.field public static final enum UNKNOWN:LX/211;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 356498
    new-instance v0, LX/211;

    const-string v1, "OVER"

    invoke-direct {v0, v1, v3}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->OVER:LX/211;

    .line 356499
    new-instance v0, LX/211;

    const-string v1, "ABOVE"

    invoke-direct {v0, v1, v4}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->ABOVE:LX/211;

    .line 356500
    new-instance v0, LX/211;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v5}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->BELOW:LX/211;

    .line 356501
    new-instance v0, LX/211;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v6}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->LEFT:LX/211;

    .line 356502
    new-instance v0, LX/211;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v7}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->RIGHT:LX/211;

    .line 356503
    new-instance v0, LX/211;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/211;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/211;->UNKNOWN:LX/211;

    .line 356504
    const/4 v0, 0x6

    new-array v0, v0, [LX/211;

    sget-object v1, LX/211;->OVER:LX/211;

    aput-object v1, v0, v3

    sget-object v1, LX/211;->ABOVE:LX/211;

    aput-object v1, v0, v4

    sget-object v1, LX/211;->BELOW:LX/211;

    aput-object v1, v0, v5

    sget-object v1, LX/211;->LEFT:LX/211;

    aput-object v1, v0, v6

    sget-object v1, LX/211;->RIGHT:LX/211;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/211;->UNKNOWN:LX/211;

    aput-object v2, v0, v1

    sput-object v0, LX/211;->$VALUES:[LX/211;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 356505
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/211;
    .locals 1

    .prologue
    .line 356506
    const-class v0, LX/211;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/211;

    return-object v0
.end method

.method public static values()[LX/211;
    .locals 1

    .prologue
    .line 356507
    sget-object v0, LX/211;->$VALUES:[LX/211;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/211;

    return-object v0
.end method
