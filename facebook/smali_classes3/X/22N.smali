.class public abstract LX/22N;
.super LX/1jo;
.source ""


# instance fields
.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "LX/0uE;",
            ">;"
        }
    .end annotation
.end field

.field public e:[LX/0uE;

.field private f:LX/1w1;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0u5;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1py;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/1jk;

.field private k:LX/0uc;


# direct methods
.method public constructor <init>(LX/1jk;LX/1w1;ILX/0ue;LX/0u0;LX/0uc;)V
    .locals 8

    .prologue
    .line 359481
    invoke-direct {p0, p2, p3}, LX/1jo;-><init>(LX/1jm;I)V

    .line 359482
    iget-object v0, p2, LX/1w1;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/1w1;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359483
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Missing context in config"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359484
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/22N;->g:Ljava/util/List;

    .line 359485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/22N;->i:Ljava/util/List;

    .line 359486
    iget-object v0, p2, LX/1w1;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pt;

    .line 359487
    iget-object v1, v0, LX/1pt;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v3

    .line 359488
    iget-object v1, v0, LX/1pt;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    .line 359489
    :cond_2
    new-instance v0, LX/5MH;

    const-string v1, "Bad context identifier"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359490
    :cond_3
    new-instance v4, LX/1py;

    invoke-direct {v4}, LX/1py;-><init>()V

    .line 359491
    iget-object v1, v0, LX/1pt;->e:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 359492
    iget-object v1, v0, LX/1pt;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5MT;

    .line 359493
    iget-object v6, v1, LX/5MT;->a:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, LX/5MT;->b:Ljava/lang/String;

    iget-object v1, v1, LX/5MT;->c:Ljava/util/List;

    invoke-virtual {p4, v6, v7, v1}, LX/0ue;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)LX/5MG;

    move-result-object v1

    .line 359494
    invoke-virtual {v4, v1}, LX/1py;->a(LX/5MG;)V

    goto :goto_1

    .line 359495
    :cond_4
    iget-object v1, p0, LX/22N;->i:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359496
    iget-object v1, v0, LX/1pt;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/1pt;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 359497
    iget-object v1, p0, LX/22N;->g:Ljava/util/List;

    new-instance v4, LX/0u5;

    iget-object v5, v0, LX/1pt;->a:Ljava/lang/String;

    new-instance v6, LX/0uE;

    iget-object v0, v0, LX/1pt;->f:Ljava/lang/String;

    invoke-direct {v6, v3, v0}, LX/0uE;-><init>(LX/0uN;Ljava/lang/String;)V

    invoke-direct {v4, v5, v6}, LX/0u5;-><init>(Ljava/lang/String;LX/0uE;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359498
    :cond_5
    iget-boolean v1, v0, LX/1pt;->d:Z

    if-nez v1, :cond_6

    .line 359499
    iget-object v1, p0, LX/22N;->g:Ljava/util/List;

    iget-object v0, v0, LX/1pt;->a:Ljava/lang/String;

    invoke-virtual {p5, v0}, LX/0u0;->a(Ljava/lang/String;)LX/0u5;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 359500
    :cond_6
    iget-object v1, p0, LX/22N;->g:Ljava/util/List;

    new-instance v3, LX/0u5;

    iget-object v0, v0, LX/1pt;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v0, v4, v6, v7}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 359501
    :cond_7
    iget-object v0, p2, LX/1w1;->g:Ljava/util/List;

    if-eqz v0, :cond_b

    iget-object v0, p2, LX/1w1;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 359502
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/22N;->h:Ljava/util/List;

    .line 359503
    iget-object v0, p2, LX/1w1;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pt;

    .line 359504
    iget-object v2, v0, LX/1pt;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0uE;->a(Ljava/lang/String;)LX/0uN;

    move-result-object v2

    .line 359505
    iget-object v3, v0, LX/1pt;->a:Ljava/lang/String;

    if-eqz v3, :cond_8

    if-nez v2, :cond_9

    .line 359506
    :cond_8
    new-instance v0, LX/5MH;

    const-string v1, "Bad context identifier"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359507
    :cond_9
    iget-boolean v2, v0, LX/1pt;->d:Z

    if-nez v2, :cond_a

    .line 359508
    iget-object v2, p0, LX/22N;->h:Ljava/util/List;

    iget-object v0, v0, LX/1pt;->a:Ljava/lang/String;

    invoke-virtual {p5, v0}, LX/0u0;->a(Ljava/lang/String;)LX/0u5;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 359509
    :cond_a
    iget-object v2, p0, LX/22N;->h:Ljava/util/List;

    new-instance v3, LX/0u5;

    iget-object v0, v0, LX/1pt;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    invoke-direct {v3, v0, v4, v6, v7}, LX/0u5;-><init>(Ljava/lang/String;LX/0u4;J)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 359510
    :cond_b
    iput-object p2, p0, LX/22N;->f:LX/1w1;

    .line 359511
    iput-object p1, p0, LX/22N;->j:LX/1jk;

    .line 359512
    iput-object p6, p0, LX/22N;->k:LX/0uc;

    .line 359513
    return-void
.end method


# virtual methods
.method public final a()LX/1jk;
    .locals 1

    .prologue
    .line 359451
    iget-object v0, p0, LX/22N;->j:LX/1jk;

    return-object v0
.end method

.method public final a(LX/8K8;)LX/1jr;
    .locals 10
    .param p1    # LX/8K8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 359452
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 359453
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 359454
    iget-object v0, p0, LX/22N;->f:LX/1w1;

    iget-object v0, v0, LX/1w1;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v6, v5

    .line 359455
    :goto_0
    if-ge v6, v7, :cond_2

    .line 359456
    iget-object v0, p0, LX/22N;->f:LX/1w1;

    iget-object v0, v0, LX/1w1;->f:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pt;

    .line 359457
    iget-object v1, p0, LX/22N;->g:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0u5;

    iget-wide v8, v0, LX/1pt;->b:J

    invoke-virtual {v1, v8, v9, p1}, LX/0u5;->a(JLX/8K8;)LX/0uE;

    move-result-object v1

    .line 359458
    if-eqz v1, :cond_7

    .line 359459
    iget-object v0, v0, LX/1pt;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 359460
    invoke-virtual {v1}, LX/0uE;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 359461
    :goto_1
    if-nez v0, :cond_0

    .line 359462
    const-string v0, "n/a"

    .line 359463
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359464
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359465
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 359466
    :cond_1
    iget-object v0, p0, LX/22N;->i:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1py;

    invoke-virtual {v0, v1}, LX/1py;->a(LX/0uE;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 359467
    :cond_2
    iget-object v0, p0, LX/22N;->h:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 359468
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 359469
    iget-object v0, p0, LX/22N;->f:LX/1w1;

    iget-object v0, v0, LX/1w1;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 359470
    :goto_2
    if-ge v5, v7, :cond_4

    .line 359471
    iget-object v0, p0, LX/22N;->f:LX/1w1;

    iget-object v0, v0, LX/1w1;->g:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pt;

    .line 359472
    iget-object v1, p0, LX/22N;->h:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0u5;

    iget-wide v8, v0, LX/1pt;->b:J

    invoke-virtual {v1, v8, v9, p1}, LX/0u5;->a(JLX/8K8;)LX/0uE;

    move-result-object v0

    .line 359473
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359474
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_3
    move-object v6, v2

    .line 359475
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 359476
    const-string v0, ", "

    invoke-static {v0, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 359477
    iget-object v1, p0, LX/22N;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/0uE;

    move-object v7, v0

    .line 359478
    :goto_3
    if-nez v7, :cond_5

    .line 359479
    iget-object v7, p0, LX/22N;->e:[LX/0uE;

    .line 359480
    :cond_5
    new-instance v0, LX/1jr;

    iget-object v2, p0, LX/22N;->g:Ljava/util/List;

    iget-object v5, p0, LX/22N;->h:Ljava/util/List;

    iget-object v8, p0, LX/22N;->k:LX/0uc;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, LX/1jr;-><init>(LX/1jp;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;[LX/0uE;LX/0uc;)V

    return-object v0

    :cond_6
    move-object v7, v2

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto :goto_1
.end method
