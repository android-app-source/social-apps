.class public LX/1xI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/1xI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 348101
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 348102
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/1xI;->b:LX/0Zi;

    .line 348103
    iput-object p1, p0, LX/1xI;->a:LX/0Ot;

    .line 348104
    return-void
.end method

.method public static a(LX/0QB;)LX/1xI;
    .locals 4

    .prologue
    .line 348105
    const-class v1, LX/1xI;

    monitor-enter v1

    .line 348106
    :try_start_0
    sget-object v0, LX/1xI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 348107
    sput-object v2, LX/1xI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 348108
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348109
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 348110
    new-instance v3, LX/1xI;

    const/16 p0, 0x835

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/1xI;-><init>(LX/0Ot;)V

    .line 348111
    move-object v0, v3

    .line 348112
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 348113
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1xI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348114
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 348115
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 348116
    check-cast p2, LX/C0z;

    .line 348117
    iget-object v0, p0, LX/1xI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;

    iget-object v1, p2, LX/C0z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C0z;->b:LX/1Pb;

    const/4 v4, 0x0

    const/16 p2, 0x8

    const/4 p0, 0x1

    .line 348118
    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 348119
    :goto_0
    move-object v0, v4

    .line 348120
    return-object v0

    .line 348121
    :cond_0
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 348122
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 348123
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1xN;

    invoke-virtual {v3, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v3

    sget-object v7, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v7}, LX/22O;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/22O;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->c:LX/1eq;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/1eq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, v4

    :goto_1
    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8xB;

    const/4 v4, 0x0

    .line 348124
    new-instance v7, LX/8xA;

    invoke-direct {v7, v3}, LX/8xA;-><init>(LX/8xB;)V

    .line 348125
    sget-object v0, LX/8xB;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8x9;

    .line 348126
    if-nez v0, :cond_2

    .line 348127
    new-instance v0, LX/8x9;

    invoke-direct {v0}, LX/8x9;-><init>()V

    .line 348128
    :cond_2
    invoke-static {v0, p1, v4, v4, v7}, LX/8x9;->a$redex0(LX/8x9;LX/1De;IILX/8xA;)V

    .line 348129
    move-object v7, v0

    .line 348130
    move-object v4, v7

    .line 348131
    move-object v3, v4

    .line 348132
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->bR()Ljava/lang/String;

    move-result-object v4

    .line 348133
    iget-object v5, v3, LX/8x9;->a:LX/8xA;

    iput-object v4, v5, LX/8xA;->a:Ljava/lang/String;

    .line 348134
    iget-object v5, v3, LX/8x9;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/util/BitSet;->set(I)V

    .line 348135
    move-object v3, v3

    .line 348136
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->b:Landroid/util/SparseArray;

    invoke-interface {v3, v4}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4, p2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0, p2}, LX/1Di;->m(II)LX/1Di;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 348137
    invoke-static {}, LX/1dS;->b()V

    .line 348138
    const/4 v0, 0x0

    return-object v0
.end method
