.class public LX/1zm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/1zm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 354107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354108
    return-void
.end method

.method public static a(LX/0QB;)LX/1zm;
    .locals 3

    .prologue
    .line 354109
    sget-object v0, LX/1zm;->a:LX/1zm;

    if-nez v0, :cond_1

    .line 354110
    const-class v1, LX/1zm;

    monitor-enter v1

    .line 354111
    :try_start_0
    sget-object v0, LX/1zm;->a:LX/1zm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 354112
    if-eqz v2, :cond_0

    .line 354113
    :try_start_1
    new-instance v0, LX/1zm;

    invoke-direct {v0}, LX/1zm;-><init>()V

    .line 354114
    move-object v0, v0

    .line 354115
    sput-object v0, LX/1zm;->a:LX/1zm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354116
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 354117
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 354118
    :cond_1
    sget-object v0, LX/1zm;->a:LX/1zm;

    return-object v0

    .line 354119
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 354120
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(Ljava/io/File;)LX/3K3;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 354121
    const/4 v2, 0x0

    .line 354122
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354123
    :try_start_1
    invoke-static {v1}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 354124
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 354125
    :goto_0
    return-object v0

    .line 354126
    :catch_0
    move-exception v1

    .line 354127
    const-class v2, LX/1zm;

    const-string v3, "Error closing stream for %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 354128
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 354129
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 354130
    :cond_0
    :goto_2
    throw v0

    .line 354131
    :catch_1
    move-exception v1

    .line 354132
    const-class v2, LX/1zm;

    const-string v3, "Error closing stream for %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 354133
    :catchall_1
    move-exception v0

    goto :goto_1
.end method
