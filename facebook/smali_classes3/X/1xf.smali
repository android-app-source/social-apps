.class public LX/1xf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/11S;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/1xf;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0SG;

.field public final c:LX/11T;

.field public final d:LX/11R;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0SG;LX/0Or;LX/0Or;LX/11R;LX/11T;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/11R;",
            "LX/11T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 349151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349152
    iput-object p5, p0, LX/1xf;->d:LX/11R;

    .line 349153
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/1xf;->a:Landroid/content/res/Resources;

    .line 349154
    iput-object p4, p0, LX/1xf;->f:LX/0Or;

    .line 349155
    iput-object p2, p0, LX/1xf;->b:LX/0SG;

    .line 349156
    iput-object p3, p0, LX/1xf;->e:LX/0Or;

    .line 349157
    iput-object p6, p0, LX/1xf;->c:LX/11T;

    .line 349158
    return-void
.end method

.method public static a(LX/0QB;)LX/1xf;
    .locals 10

    .prologue
    .line 349114
    sget-object v0, LX/1xf;->g:LX/1xf;

    if-nez v0, :cond_1

    .line 349115
    const-class v1, LX/1xf;

    monitor-enter v1

    .line 349116
    :try_start_0
    sget-object v0, LX/1xf;->g:LX/1xf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 349117
    if-eqz v2, :cond_0

    .line 349118
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 349119
    new-instance v3, LX/1xf;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x161a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1617

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v8

    check-cast v8, LX/11R;

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v9

    check-cast v9, LX/11T;

    invoke-direct/range {v3 .. v9}, LX/1xf;-><init>(Landroid/content/Context;LX/0SG;LX/0Or;LX/0Or;LX/11R;LX/11T;)V

    .line 349120
    move-object v0, v3

    .line 349121
    sput-object v0, LX/1xf;->g:LX/1xf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349122
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 349123
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 349124
    :cond_1
    sget-object v0, LX/1xf;->g:LX/1xf;

    return-object v0

    .line 349125
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 349126
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/1xf;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 349148
    iget-object v0, p0, LX/1xf;->c:LX/11T;

    invoke-virtual {v0}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v1

    .line 349149
    iget-object v0, p0, LX/1xf;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 349150
    iget-object v0, p0, LX/1xf;->a:Landroid/content/res/Resources;

    const v2, 0x7f080086

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-static {p0, p1, p2}, LX/1xf;->c(LX/1xf;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/1xf;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 349145
    iget-object v0, p0, LX/1xf;->c:LX/11T;

    invoke-virtual {v0}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v1

    .line 349146
    iget-object v0, p0, LX/1xf;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 349147
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/1xf;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1lB;J)Ljava/lang/String;
    .locals 9

    .prologue
    .line 349127
    sget-object v0, LX/1yy;->a:[I

    invoke-virtual {p1}, LX/1lB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 349128
    iget-object v0, p0, LX/1xf;->d:LX/11R;

    invoke-virtual {v0, p1, p2, p3}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 349129
    :pswitch_0
    iget-object v2, p0, LX/1xf;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 349130
    sub-long/2addr v2, p2

    .line 349131
    iget-object v4, p0, LX/1xf;->d:LX/11R;

    invoke-virtual {v4, p2, p3}, LX/11R;->b(J)LX/1yz;

    move-result-object v4

    .line 349132
    sget-object v5, LX/1yy;->b:[I

    invoke-virtual {v4}, LX/1yz;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    .line 349133
    iget-object v2, p0, LX/1xf;->c:LX/11T;

    invoke-virtual {v2}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v3

    .line 349134
    iget-object v2, p0, LX/1xf;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 349135
    iget-object v2, p0, LX/1xf;->a:Landroid/content/res/Resources;

    const v4, 0x7f080086

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    invoke-static {p0, p2, p3}, LX/1xf;->c(LX/1xf;J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v0, v2

    .line 349136
    goto :goto_0

    .line 349137
    :pswitch_1
    iget-object v2, p0, LX/1xf;->a:Landroid/content/res/Resources;

    const v3, 0x7f080097

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 349138
    :pswitch_2
    invoke-static {v2, v3}, LX/1lQ;->d(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 349139
    iget-object v3, p0, LX/1xf;->a:Landroid/content/res/Resources;

    const v4, 0x7f080091

    const v5, 0x7f080092

    invoke-static {v3, v4, v5, v2}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 349140
    :pswitch_3
    invoke-static {v2, v3}, LX/1lQ;->a(J)J

    move-result-wide v2

    long-to-int v2, v2

    .line 349141
    iget-object v3, p0, LX/1xf;->a:Landroid/content/res/Resources;

    const v4, 0x7f08008c

    const v5, 0x7f08008e

    invoke-static {v3, v4, v5, v2}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 349142
    :pswitch_4
    invoke-static {p0, p2, p3}, LX/1xf;->b(LX/1xf;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 349143
    :pswitch_5
    invoke-static {p0, p2, p3}, LX/1xf;->b(LX/1xf;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 349144
    :pswitch_6
    invoke-static {p0, p2, p3}, LX/1xf;->b(LX/1xf;J)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
