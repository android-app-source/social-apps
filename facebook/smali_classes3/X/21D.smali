.class public final enum LX/21D;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/21D;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/21D;

.field public static final enum ALBUM:LX/21D;

.field public static final enum BUY_SELL_BOOKMARK:LX/21D;

.field public static final enum COMPOST:LX/21D;

.field public static final enum CURATED_COLLECTION:LX/21D;

.field public static final enum DRAFT_POSTS_DASHBOARD:LX/21D;

.field public static final enum ELECTION_HUB:LX/21D;

.field public static final enum EVENT:LX/21D;

.field public static final enum FACEWEB:LX/21D;

.field public static final enum FULLSCREEN_VIDEO_PLAYER:LX/21D;

.field public static final enum FUNDRAISER_PAGE:LX/21D;

.field public static final enum FUNDRAISER_THANK_YOU_PAGE:LX/21D;

.field public static final enum GOODWILL_REACT:LX/21D;

.field public static final enum GOOD_FRIENDS:LX/21D;

.field public static final enum GREETING_CARD:LX/21D;

.field public static final enum GROUP_FEED:LX/21D;

.field public static final enum INSTANT_ARTICLE:LX/21D;

.field public static final enum INSTANT_GAME:LX/21D;

.field public static final enum INVALID:LX/21D;

.field public static final enum INVENTORY_MANAGEMENT:LX/21D;

.field public static final enum IN_APP_BROWSER:LX/21D;

.field public static final enum LIVE_VIDEO:LX/21D;

.field public static final enum LOCAL_SERP:LX/21D;

.field public static final enum MEDIA_GALLERY_DO_NOT_USE:LX/21D;

.field public static final enum NEWSFEED:LX/21D;

.field public static final enum NOTIFICATIONS:LX/21D;

.field public static final enum OFFERS_SPACE:LX/21D;

.field public static final enum ON_THIS_DAY_FEED:LX/21D;

.field public static final enum PAGE_FEED:LX/21D;

.field public static final enum PERMALINK:LX/21D;

.field public static final enum PHOTOS_FEED:LX/21D;

.field public static final enum PRIVACY_CHECKUP:LX/21D;

.field public static final enum REACTION:LX/21D;

.field public static final enum SAVED_STORIES_DASHBOARD:LX/21D;

.field public static final enum SEARCH:LX/21D;

.field public static final enum SELLER_CENTRAL:LX/21D;

.field public static final enum SOUVENIR:LX/21D;

.field public static final enum STORY_GALLERY_SURVEY:LX/21D;

.field public static final enum TAROT_STORY:LX/21D;

.field public static final enum THIRD_PARTY_APP_VIA_FB_API:LX/21D;

.field public static final enum THIRD_PARTY_APP_VIA_INTENT:LX/21D;

.field public static final enum TIMELINE:LX/21D;

.field public static final enum URI_HANDLER:LX/21D;

.field public static final enum USER_REVIEWS_LIST:LX/21D;

.field public static final enum VIDEO_HOME:LX/21D;

.field public static final enum WIDGET:LX/21D;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 356760
    new-instance v0, LX/21D;

    const-string v1, "INVALID"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v4, v2}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->INVALID:LX/21D;

    .line 356761
    new-instance v0, LX/21D;

    const-string v1, "ALBUM"

    const-string v2, "album"

    invoke-direct {v0, v1, v5, v2}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->ALBUM:LX/21D;

    .line 356762
    new-instance v0, LX/21D;

    const-string v1, "BUY_SELL_BOOKMARK"

    const-string v2, "buy_sell_bookmark"

    invoke-direct {v0, v1, v6, v2}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->BUY_SELL_BOOKMARK:LX/21D;

    .line 356763
    new-instance v0, LX/21D;

    const-string v1, "COMPOST"

    const-string v2, "compost"

    invoke-direct {v0, v1, v7, v2}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->COMPOST:LX/21D;

    .line 356764
    new-instance v0, LX/21D;

    const-string v1, "CURATED_COLLECTION"

    const-string v2, "curated_collection"

    invoke-direct {v0, v1, v8, v2}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->CURATED_COLLECTION:LX/21D;

    .line 356765
    new-instance v0, LX/21D;

    const-string v1, "DRAFT_POSTS_DASHBOARD"

    const/4 v2, 0x5

    const-string v3, "draft_posts_dashboard"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->DRAFT_POSTS_DASHBOARD:LX/21D;

    .line 356766
    new-instance v0, LX/21D;

    const-string v1, "ELECTION_HUB"

    const/4 v2, 0x6

    const-string v3, "election_hub"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->ELECTION_HUB:LX/21D;

    .line 356767
    new-instance v0, LX/21D;

    const-string v1, "EVENT"

    const/4 v2, 0x7

    const-string v3, "event"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->EVENT:LX/21D;

    .line 356768
    new-instance v0, LX/21D;

    const-string v1, "FACEWEB"

    const/16 v2, 0x8

    const-string v3, "faceweb"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->FACEWEB:LX/21D;

    .line 356769
    new-instance v0, LX/21D;

    const-string v1, "FULLSCREEN_VIDEO_PLAYER"

    const/16 v2, 0x9

    const-string v3, "fullscreen_video_player"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    .line 356770
    new-instance v0, LX/21D;

    const-string v1, "FUNDRAISER_PAGE"

    const/16 v2, 0xa

    const-string v3, "fundraiser_page"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    .line 356771
    new-instance v0, LX/21D;

    const-string v1, "FUNDRAISER_THANK_YOU_PAGE"

    const/16 v2, 0xb

    const-string v3, "fundraiser_thank_you_page"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->FUNDRAISER_THANK_YOU_PAGE:LX/21D;

    .line 356772
    new-instance v0, LX/21D;

    const-string v1, "GOOD_FRIENDS"

    const/16 v2, 0xc

    const-string v3, "good_friends"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->GOOD_FRIENDS:LX/21D;

    .line 356773
    new-instance v0, LX/21D;

    const-string v1, "GOODWILL_REACT"

    const/16 v2, 0xd

    const-string v3, "goodwill_react"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->GOODWILL_REACT:LX/21D;

    .line 356774
    new-instance v0, LX/21D;

    const-string v1, "GREETING_CARD"

    const/16 v2, 0xe

    const-string v3, "greeting_card"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->GREETING_CARD:LX/21D;

    .line 356775
    new-instance v0, LX/21D;

    const-string v1, "GROUP_FEED"

    const/16 v2, 0xf

    const-string v3, "groups"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->GROUP_FEED:LX/21D;

    .line 356776
    new-instance v0, LX/21D;

    const-string v1, "IN_APP_BROWSER"

    const/16 v2, 0x10

    const-string v3, "in_app_browser"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->IN_APP_BROWSER:LX/21D;

    .line 356777
    new-instance v0, LX/21D;

    const-string v1, "INSTANT_ARTICLE"

    const/16 v2, 0x11

    const-string v3, "instant_article"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->INSTANT_ARTICLE:LX/21D;

    .line 356778
    new-instance v0, LX/21D;

    const-string v1, "INSTANT_GAME"

    const/16 v2, 0x12

    const-string v3, "instant_game"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->INSTANT_GAME:LX/21D;

    .line 356779
    new-instance v0, LX/21D;

    const-string v1, "INVENTORY_MANAGEMENT"

    const/16 v2, 0x13

    const-string v3, "inventory_management"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->INVENTORY_MANAGEMENT:LX/21D;

    .line 356780
    new-instance v0, LX/21D;

    const-string v1, "LIVE_VIDEO"

    const/16 v2, 0x14

    const-string v3, "live_video"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->LIVE_VIDEO:LX/21D;

    .line 356781
    new-instance v0, LX/21D;

    const-string v1, "LOCAL_SERP"

    const/16 v2, 0x15

    const-string v3, "local_serp"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->LOCAL_SERP:LX/21D;

    .line 356782
    new-instance v0, LX/21D;

    const-string v1, "MEDIA_GALLERY_DO_NOT_USE"

    const/16 v2, 0x16

    const-string v3, "media_gallery_temp"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->MEDIA_GALLERY_DO_NOT_USE:LX/21D;

    .line 356783
    new-instance v0, LX/21D;

    const-string v1, "NEWSFEED"

    const/16 v2, 0x17

    const-string v3, "newsfeed"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->NEWSFEED:LX/21D;

    .line 356784
    new-instance v0, LX/21D;

    const-string v1, "NOTIFICATIONS"

    const/16 v2, 0x18

    const-string v3, "notifications"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->NOTIFICATIONS:LX/21D;

    .line 356785
    new-instance v0, LX/21D;

    const-string v1, "OFFERS_SPACE"

    const/16 v2, 0x19

    const-string v3, "offers_space"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->OFFERS_SPACE:LX/21D;

    .line 356786
    new-instance v0, LX/21D;

    const-string v1, "ON_THIS_DAY_FEED"

    const/16 v2, 0x1a

    const-string v3, "on_this_day_feed"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->ON_THIS_DAY_FEED:LX/21D;

    .line 356787
    new-instance v0, LX/21D;

    const-string v1, "PERMALINK"

    const/16 v2, 0x1b

    const-string v3, "permalink"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->PERMALINK:LX/21D;

    .line 356788
    new-instance v0, LX/21D;

    const-string v1, "PAGE_FEED"

    const/16 v2, 0x1c

    const-string v3, "pages"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->PAGE_FEED:LX/21D;

    .line 356789
    new-instance v0, LX/21D;

    const-string v1, "PHOTOS_FEED"

    const/16 v2, 0x1d

    const-string v3, "photos_feed"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->PHOTOS_FEED:LX/21D;

    .line 356790
    new-instance v0, LX/21D;

    const-string v1, "REACTION"

    const/16 v2, 0x1e

    const-string v3, "reaction"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->REACTION:LX/21D;

    .line 356791
    new-instance v0, LX/21D;

    const-string v1, "PRIVACY_CHECKUP"

    const/16 v2, 0x1f

    const-string v3, "privacy_checkup"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->PRIVACY_CHECKUP:LX/21D;

    .line 356792
    new-instance v0, LX/21D;

    const-string v1, "SAVED_STORIES_DASHBOARD"

    const/16 v2, 0x20

    const-string v3, "saved_stories_dashboard"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->SAVED_STORIES_DASHBOARD:LX/21D;

    .line 356793
    new-instance v0, LX/21D;

    const-string v1, "SEARCH"

    const/16 v2, 0x21

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->SEARCH:LX/21D;

    .line 356794
    new-instance v0, LX/21D;

    const-string v1, "SELLER_CENTRAL"

    const/16 v2, 0x22

    const-string v3, "seller_central"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->SELLER_CENTRAL:LX/21D;

    .line 356795
    new-instance v0, LX/21D;

    const-string v1, "SOUVENIR"

    const/16 v2, 0x23

    const-string v3, "souvenir"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->SOUVENIR:LX/21D;

    .line 356796
    new-instance v0, LX/21D;

    const-string v1, "STORY_GALLERY_SURVEY"

    const/16 v2, 0x24

    const-string v3, "story_gallery_survey"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->STORY_GALLERY_SURVEY:LX/21D;

    .line 356797
    new-instance v0, LX/21D;

    const-string v1, "TAROT_STORY"

    const/16 v2, 0x25

    const-string v3, "tarot_story"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->TAROT_STORY:LX/21D;

    .line 356798
    new-instance v0, LX/21D;

    const-string v1, "THIRD_PARTY_APP_VIA_FB_API"

    const/16 v2, 0x26

    const-string v3, "third_party_app_via_fb_api"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    .line 356799
    new-instance v0, LX/21D;

    const-string v1, "THIRD_PARTY_APP_VIA_INTENT"

    const/16 v2, 0x27

    const-string v3, "third_party_app_via_intent"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    .line 356800
    new-instance v0, LX/21D;

    const-string v1, "TIMELINE"

    const/16 v2, 0x28

    const-string v3, "timeline"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->TIMELINE:LX/21D;

    .line 356801
    new-instance v0, LX/21D;

    const-string v1, "URI_HANDLER"

    const/16 v2, 0x29

    const-string v3, "uri_handler"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->URI_HANDLER:LX/21D;

    .line 356802
    new-instance v0, LX/21D;

    const-string v1, "USER_REVIEWS_LIST"

    const/16 v2, 0x2a

    const-string v3, "user_reviews_list"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->USER_REVIEWS_LIST:LX/21D;

    .line 356803
    new-instance v0, LX/21D;

    const-string v1, "VIDEO_HOME"

    const/16 v2, 0x2b

    const-string v3, "video_home"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->VIDEO_HOME:LX/21D;

    .line 356804
    new-instance v0, LX/21D;

    const-string v1, "WIDGET"

    const/16 v2, 0x2c

    const-string v3, "widget"

    invoke-direct {v0, v1, v2, v3}, LX/21D;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/21D;->WIDGET:LX/21D;

    .line 356805
    const/16 v0, 0x2d

    new-array v0, v0, [LX/21D;

    sget-object v1, LX/21D;->INVALID:LX/21D;

    aput-object v1, v0, v4

    sget-object v1, LX/21D;->ALBUM:LX/21D;

    aput-object v1, v0, v5

    sget-object v1, LX/21D;->BUY_SELL_BOOKMARK:LX/21D;

    aput-object v1, v0, v6

    sget-object v1, LX/21D;->COMPOST:LX/21D;

    aput-object v1, v0, v7

    sget-object v1, LX/21D;->CURATED_COLLECTION:LX/21D;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/21D;->DRAFT_POSTS_DASHBOARD:LX/21D;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/21D;->ELECTION_HUB:LX/21D;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/21D;->EVENT:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/21D;->FACEWEB:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/21D;->FUNDRAISER_THANK_YOU_PAGE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/21D;->GOOD_FRIENDS:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/21D;->GOODWILL_REACT:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/21D;->GREETING_CARD:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/21D;->GROUP_FEED:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/21D;->IN_APP_BROWSER:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/21D;->INSTANT_ARTICLE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/21D;->INSTANT_GAME:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/21D;->INVENTORY_MANAGEMENT:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/21D;->LIVE_VIDEO:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/21D;->LOCAL_SERP:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/21D;->MEDIA_GALLERY_DO_NOT_USE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/21D;->NEWSFEED:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/21D;->NOTIFICATIONS:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/21D;->OFFERS_SPACE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/21D;->ON_THIS_DAY_FEED:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/21D;->PERMALINK:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/21D;->PAGE_FEED:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/21D;->PHOTOS_FEED:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/21D;->REACTION:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/21D;->PRIVACY_CHECKUP:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/21D;->SAVED_STORIES_DASHBOARD:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/21D;->SEARCH:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/21D;->SELLER_CENTRAL:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/21D;->SOUVENIR:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/21D;->STORY_GALLERY_SURVEY:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/21D;->TAROT_STORY:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/21D;->TIMELINE:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/21D;->URI_HANDLER:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/21D;->USER_REVIEWS_LIST:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/21D;->VIDEO_HOME:LX/21D;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/21D;->WIDGET:LX/21D;

    aput-object v2, v0, v1

    sput-object v0, LX/21D;->$VALUES:[LX/21D;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356806
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 356807
    iput-object p3, p0, LX/21D;->mAnalyticsName:Ljava/lang/String;

    .line 356808
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/21D;
    .locals 1

    .prologue
    .line 356809
    const-class v0, LX/21D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/21D;

    return-object v0
.end method

.method public static values()[LX/21D;
    .locals 1

    .prologue
    .line 356810
    sget-object v0, LX/21D;->$VALUES:[LX/21D;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/21D;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356811
    iget-object v0, p0, LX/21D;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
