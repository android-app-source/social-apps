.class public LX/22B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1vb;

.field private final b:LX/1xp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xp",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/1VE;

.field public final d:LX/1VI;


# direct methods
.method public constructor <init>(LX/1vb;LX/1xp;LX/1VE;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 359254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359255
    iput-object p1, p0, LX/22B;->a:LX/1vb;

    .line 359256
    iput-object p2, p0, LX/22B;->b:LX/1xp;

    .line 359257
    iput-object p3, p0, LX/22B;->c:LX/1VE;

    .line 359258
    iput-object p4, p0, LX/22B;->d:LX/1VI;

    .line 359259
    return-void
.end method

.method public static a(LX/0QB;)LX/22B;
    .locals 7

    .prologue
    .line 359260
    const-class v1, LX/22B;

    monitor-enter v1

    .line 359261
    :try_start_0
    sget-object v0, LX/22B;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 359262
    sput-object v2, LX/22B;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359263
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359264
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 359265
    new-instance p0, LX/22B;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-static {v0}, LX/1xp;->a(LX/0QB;)LX/1xp;

    move-result-object v4

    check-cast v4, LX/1xp;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v5

    check-cast v5, LX/1VE;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v6

    check-cast v6, LX/1VI;

    invoke-direct {p0, v3, v4, v5, v6}, LX/22B;-><init>(LX/1vb;LX/1xp;LX/1VE;LX/1VI;)V

    .line 359266
    move-object v0, p0

    .line 359267
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 359268
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/22B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 359269
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 359270
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/22B;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 359250
    iget-object v0, p0, LX/22B;->b:LX/1xp;

    invoke-virtual {v0, p1}, LX/1xp;->c(LX/1De;)LX/1xt;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1xt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1xt;

    move-result-object v0

    check-cast p3, LX/1Po;

    invoke-virtual {v0, p3}, LX/1xt;->a(LX/1Po;)LX/1xt;

    move-result-object v0

    .line 359251
    iget-object v1, p0, LX/22B;->d:LX/1VI;

    invoke-virtual {v1}, LX/1VI;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359252
    const v1, 0x7f0a00aa

    invoke-virtual {v0, v1}, LX/1xt;->h(I)LX/1xt;

    .line 359253
    :cond_0
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    const v2, 0x7f0b08fe

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method
