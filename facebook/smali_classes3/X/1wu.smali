.class public LX/1wu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347126
    iput-object p1, p0, LX/1wu;->a:LX/0ad;

    .line 347127
    return-void
.end method

.method private a(CLjava/lang/String;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347128
    iget-object v0, p0, LX/1wu;->a:LX/0ad;

    invoke-interface {v0, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 347129
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 347130
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 347131
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 347132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347133
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347134
    iget-object v0, p0, LX/1wu;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 347135
    sget-char v0, LX/Hc1;->b:C

    const-string v1, "86400000, 259200000, 1209600000"

    invoke-direct {p0, v0, v1}, LX/1wu;->a(CLjava/lang/String;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1wu;->b:LX/0Px;

    .line 347136
    :cond_0
    iget-object v0, p0, LX/1wu;->b:LX/0Px;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347137
    iget-object v0, p0, LX/1wu;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 347138
    sget-char v0, LX/Hc1;->c:C

    const-string v1, "86400000, 259200000, 1209600000"

    invoke-direct {p0, v0, v1}, LX/1wu;->a(CLjava/lang/String;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/1wu;->c:LX/0Px;

    .line 347139
    :cond_0
    iget-object v0, p0, LX/1wu;->c:LX/0Px;

    return-object v0
.end method
