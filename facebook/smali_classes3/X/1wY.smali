.class public LX/1wY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1
    .param p2    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/resources/BaseResources;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346353
    new-instance v0, LX/1sP;

    invoke-direct {v0, p1, p2}, LX/1sP;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    move-object v0, v0

    .line 346354
    iput-object v0, p0, LX/1wY;->a:Landroid/content/Context;

    .line 346355
    return-void
.end method

.method public static a(LX/0QB;)LX/1wY;
    .locals 5

    .prologue
    .line 346356
    const-class v1, LX/1wY;

    monitor-enter v1

    .line 346357
    :try_start_0
    sget-object v0, LX/1wY;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 346358
    sput-object v2, LX/1wY;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 346359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 346361
    new-instance p0, LX/1wY;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0W0;->b(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/1wY;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 346362
    move-object v0, p0

    .line 346363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 346364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1wY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 346366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 346367
    iget-object v0, p0, LX/1wY;->a:Landroid/content/Context;

    invoke-static {v0}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method
