.class public LX/1wJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1VQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/flatbuffers/Flattenable;",
        ">",
        "Ljava/lang/Object;",
        "LX/1VQ",
        "<",
        "Ljava/lang/Class",
        "<TT;>;TT;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1wJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 345713
    new-instance v0, LX/1wJ;

    invoke-direct {v0}, LX/1wJ;-><init>()V

    sput-object v0, LX/1wJ;->a:LX/1wJ;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 345716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/nio/ByteBuffer;II)Ljava/lang/Object;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 345717
    check-cast p1, Ljava/lang/Class;

    .line 345718
    mul-int/lit8 v0, p4, 0x4

    add-int v1, p3, v0

    .line 345719
    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    .line 345720
    if-nez v2, :cond_0

    .line 345721
    const/4 v0, 0x0

    .line 345722
    :goto_0
    return-object v0

    .line 345723
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 345724
    add-int/2addr v1, v2

    invoke-virtual {p0, v0, p2, v1}, LX/1wJ;->a(Lcom/facebook/flatbuffers/Flattenable;Ljava/nio/ByteBuffer;I)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 345725
    :catch_0
    move-exception v0

    .line 345726
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not able to create object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 345727
    :catch_1
    move-exception v0

    .line 345728
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Access to constructor denied"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/facebook/flatbuffers/Flattenable;Ljava/nio/ByteBuffer;I)V
    .locals 0

    .prologue
    .line 345714
    invoke-interface {p1, p2, p3}, Lcom/facebook/flatbuffers/Flattenable;->a(Ljava/nio/ByteBuffer;I)V

    .line 345715
    return-void
.end method
