.class public interface abstract LX/1vq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/2kM;)V
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation
.end method

.method public abstract a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation
.end method

.method public abstract b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation
.end method
