.class public LX/1wG;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 345461
    iput-object p1, p0, LX/1wG;->b:LX/1dt;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method

.method public static a(LX/1wG;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/BoO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 345454
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->q:LX/0Uh;

    const/16 v1, 0x614

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345455
    sget-object v0, LX/BoO;->SAVE_OFFLINE:LX/BoO;

    sget-object v1, LX/BoO;->SAVE:LX/BoO;

    sget-object v2, LX/BoO;->NEGATIVE_FEEDBACK_ACTION:LX/BoO;

    sget-object v3, LX/BoO;->UN_SEE_FIRST:LX/BoO;

    sget-object v4, LX/BoO;->HIDE_TOPIC_FROM_USER:LX/BoO;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 345456
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/BoO;->NEGATIVE_FEEDBACK_ACTION:LX/BoO;

    sget-object v1, LX/BoO;->UN_SEE_FIRST:LX/BoO;

    sget-object v2, LX/BoO;->HIDE_TOPIC_FROM_USER:LX/BoO;

    sget-object v3, LX/BoO;->SAVE_OFFLINE:LX/BoO;

    sget-object v4, LX/BoO;->SAVE:LX/BoO;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345445
    sget-object v0, LX/Al1;->EDIT_POST:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345446
    const v0, 0x7f08106b

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 345447
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const/4 v2, 0x0

    .line 345448
    invoke-virtual {v0, p2, v1, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345449
    new-instance v0, LX/BoS;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/BoS;-><init>(LX/1wG;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345450
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    .line 345451
    const v1, 0x7f020952

    move v1, v1

    .line 345452
    invoke-virtual {v0, v6, v1, p3}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345453
    return-void
.end method

.method private c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 345434
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v0

    .line 345435
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345436
    sget-object v0, LX/Al1;->DELETE:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345437
    const v0, 0x7f08104c

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 345438
    new-instance v0, LX/BoR;

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/BoR;-><init>(LX/1wG;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345439
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    .line 345440
    const v1, 0x7f020a0b

    move v1, v1

    .line 345441
    invoke-virtual {v0, v6, v1, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345442
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const/4 v2, 0x0

    .line 345443
    invoke-virtual {v0, p2, v1, v3, v2}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345444
    return-void
.end method

.method public static e(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 345426
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345427
    invoke-static {p0}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 345428
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 345429
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v5, 0x57490f63

    if-ne v0, v5, :cond_0

    .line 345430
    const/4 v0, 0x1

    .line 345431
    :goto_1
    return v0

    .line 345432
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 345433
    goto :goto_1
.end method

.method private f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 345457
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345458
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 345459
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 345460
    invoke-static {p1}, LX/14w;->n(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 345333
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345334
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345335
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 345336
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_15

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->D()Z

    move-result v3

    if-eqz v3, :cond_15

    move v4, v1

    .line 345337
    :goto_0
    if-eqz v4, :cond_0

    .line 345338
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345339
    :cond_0
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 345340
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345341
    invoke-static {p0}, LX/1wG;->a(LX/1wG;)LX/0Px;

    move-result-object v8

    .line 345342
    const/4 v6, 0x0

    move v7, v6

    :goto_1
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_2

    .line 345343
    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BoO;

    .line 345344
    sget-object v9, LX/Bo5;->b:[I

    invoke-virtual {v6}, LX/BoO;->ordinal()I

    move-result v6

    aget v6, v9, v6

    packed-switch v6, :pswitch_data_0

    .line 345345
    :cond_1
    :goto_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 345346
    :pswitch_0
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345347
    iget-object v6, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v6, p1, p2, p3}, LX/1dt;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    goto :goto_2

    .line 345348
    :pswitch_1
    invoke-virtual {p0, v3}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345349
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    goto :goto_2

    .line 345350
    :pswitch_2
    invoke-virtual {p0, v3}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345351
    sget-object v6, LX/04D;->FEED_CHEVRON:LX/04D;

    invoke-virtual {v6}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1, p2, p3, v6}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_2

    .line 345352
    :pswitch_3
    invoke-static {v3}, LX/1dt;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345353
    iget-object v6, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v6, v9, p1, p2}, LX/1dt;->a$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    .line 345354
    :pswitch_4
    iget-object v6, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v6, v3}, LX/1dt;->k(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 345355
    iget-object v6, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v6, v9, p1, p2}, LX/1dt;->b$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_2

    .line 345356
    :cond_2
    invoke-virtual {p0, p2}, LX/1wH;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 345357
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->J:LX/0Ot;

    invoke-virtual {p0, p1, p2, p3, v3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/0Ot;)V

    .line 345358
    :cond_3
    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 345359
    invoke-virtual {p0, p1, p2}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345360
    :cond_4
    invoke-static {p0}, LX/1wG;->a(LX/1wG;)LX/0Px;

    move-result-object v3

    sget-object v6, LX/BoO;->SAVE:LX/BoO;

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_16

    move v3, v1

    .line 345361
    :goto_3
    if-eqz v3, :cond_5

    .line 345362
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 345363
    :cond_5
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v3, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 345364
    invoke-direct {p0, p1, p2, v5}, LX/1wG;->c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 345365
    :cond_6
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v3, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 345366
    invoke-direct {p0, p1, p2, v0, v5}, LX/1wG;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 345367
    :cond_7
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    .line 345368
    invoke-virtual {v3, v0}, LX/1SX;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    move v3, v6

    .line 345369
    if-eqz v3, :cond_8

    .line 345370
    invoke-virtual {p0, p1, p2, v5}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 345371
    :cond_8
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    move v3, v6

    .line 345372
    if-eqz v3, :cond_9

    .line 345373
    sget-object v3, LX/Al1;->VIEW_EDIT_HISTORY:LX/Al1;

    invoke-virtual {v3}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345374
    const v6, 0x7f08106d

    invoke-interface {p1, v6}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 345375
    iget-object v7, p0, LX/1wG;->b:LX/1dt;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v8

    const/4 v9, 0x0

    .line 345376
    invoke-virtual {v7, p2, v8, v3, v9}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345377
    new-instance v7, LX/BoT;

    invoke-direct {v7, p0, p2, v3, v5}, LX/BoT;-><init>(LX/1wG;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345378
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    .line 345379
    const v7, 0x7f0207fd

    move v7, v7

    .line 345380
    invoke-virtual {v3, v6, v7, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345381
    :cond_9
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    .line 345382
    invoke-virtual {v3, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v6

    move v3, v6

    .line 345383
    if-eqz v3, :cond_a

    .line 345384
    sget-object v3, LX/Al1;->EDIT_PRIVACY:LX/Al1;

    invoke-virtual {v3}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 345385
    const v6, 0x7f081068

    invoke-interface {p1, v6}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 345386
    iget-object v7, p0, LX/1wG;->b:LX/1dt;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v8

    const/4 v9, 0x0

    .line 345387
    invoke-virtual {v7, p2, v8, v3, v9}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 345388
    new-instance v7, LX/BoU;

    invoke-direct {v7, p0, p2, v3, v5}, LX/BoU;-><init>(LX/1wG;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345389
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->r:LX/1e4;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->az()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    .line 345390
    iget-object v7, p0, LX/1wG;->b:LX/1dt;

    .line 345391
    invoke-virtual {v7, v6, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345392
    :cond_a
    invoke-static {p0}, LX/1wG;->a(LX/1wG;)LX/0Px;

    move-result-object v3

    sget-object v6, LX/BoO;->NEGATIVE_FEEDBACK_ACTION:LX/BoO;

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_17

    move v3, v1

    .line 345393
    :goto_4
    if-eqz v3, :cond_b

    .line 345394
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v3, p1, p2, p3}, LX/1dt;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 345395
    :cond_b
    invoke-static {p0}, LX/1wG;->a(LX/1wG;)LX/0Px;

    move-result-object v3

    sget-object v6, LX/BoO;->UN_SEE_FIRST:LX/BoO;

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    invoke-static {v0}, LX/1dt;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_18

    move v3, v1

    .line 345396
    :goto_5
    if-eqz v3, :cond_c

    .line 345397
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v3, v5, p1, p2}, LX/1dt;->a$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345398
    :cond_c
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v3, v0}, LX/1dt;->d(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 345399
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v3, p1, p2, p3}, LX/1dt;->d(LX/1dt;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 345400
    :cond_d
    if-nez v4, :cond_e

    .line 345401
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345402
    :cond_e
    invoke-virtual {p0, p2}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 345403
    invoke-virtual {p0, p1, p2, v5}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 345404
    :cond_f
    invoke-static {v0}, LX/1wG;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 345405
    const v3, 0x7f08105f

    invoke-interface {p1, v3}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 345406
    new-instance v4, LX/BoQ;

    invoke-direct {v4, p0, v5, v0}, LX/BoQ;-><init>(LX/1wG;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 345407
    iget-object v4, p0, LX/1wG;->b:LX/1dt;

    .line 345408
    const v6, 0x7f0209ae

    move v6, v6

    .line 345409
    invoke-virtual {v4, v3, v6, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 345410
    :cond_10
    invoke-virtual {p0, v0}, LX/1wG;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 345411
    invoke-virtual {p0, p1, p2}, LX/1wG;->c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345412
    :cond_11
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->p:LX/1e0;

    invoke-virtual {v3, v0}, LX/1e0;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 345413
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->p:LX/1e0;

    invoke-virtual {v3, p1, v0, v5}, LX/1e0;->a(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 345414
    :cond_12
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->p:LX/1e0;

    invoke-virtual {v3, v0}, LX/1e0;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 345415
    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->p:LX/1e0;

    invoke-virtual {v3, p1, v0, v5}, LX/1e0;->b(Landroid/view/Menu;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    .line 345416
    :cond_13
    invoke-static {p0}, LX/1wG;->a(LX/1wG;)LX/0Px;

    move-result-object v3

    sget-object v4, LX/BoO;->HIDE_TOPIC_FROM_USER:LX/BoO;

    invoke-virtual {v3, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_19

    iget-object v3, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v3, v0}, LX/1dt;->k(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_19

    move v0, v1

    .line 345417
    :goto_6
    if-eqz v0, :cond_14

    .line 345418
    iget-object v0, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v0, v5, p1, p2}, LX/1dt;->b$redex0(LX/1dt;Landroid/content/Context;Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 345419
    :cond_14
    invoke-super {p0, p1, p2, p3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 345420
    return-void

    :cond_15
    move v4, v2

    .line 345421
    goto/16 :goto_0

    :cond_16
    move v3, v2

    .line 345422
    goto/16 :goto_3

    :cond_17
    move v3, v2

    .line 345423
    goto/16 :goto_4

    :cond_18
    move v3, v2

    .line 345424
    goto/16 :goto_5

    :cond_19
    move v0, v2

    .line 345425
    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 345324
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345325
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 345326
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    .line 345327
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 345328
    if-nez v1, :cond_0

    .line 345329
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 345330
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/1wH;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    .line 345331
    invoke-virtual {v1, v0}, LX/1SX;->f(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    move v1, v2

    .line 345332
    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v1, v0}, LX/1dt;->d(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1dt;->j(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/1wG;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    invoke-static {v1, v0}, LX/1dt;->k(LX/1dt;Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345323
    return-void
.end method

.method public d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 345322
    invoke-direct {p0, p1}, LX/1wG;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LX/1wG;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 345319
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 345320
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 345321
    iget-object v1, p0, LX/1wG;->b:LX/1dt;

    iget-object v1, v1, LX/1dt;->F:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->N()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 1

    .prologue
    .line 345318
    const/4 v0, 0x0

    return v0
.end method
