.class public final Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:LX/0ew;

.field public b:LX/1x4;

.field public final synthetic c:Lcom/facebook/rapidfeedback/RapidFeedbackController;


# direct methods
.method public constructor <init>(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;LX/1x4;)V
    .locals 0

    .prologue
    .line 347598
    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347599
    iput-object p2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->a:LX/0ew;

    .line 347600
    iput-object p3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->b:LX/1x4;

    .line 347601
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 347602
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 347603
    iget-object v5, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    const-string v6, "survey_requested"

    iget-wide v7, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->e:J

    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/String;J)V

    .line 347604
    iget-object v5, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    const-string v6, "survey_ready"

    iget-object v7, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->s:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/String;J)V

    .line 347605
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->a:LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 347606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot complete fragment transaction"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347607
    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    const-string v1, "1565141090400626"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-static {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->p(Lcom/facebook/rapidfeedback/RapidFeedbackController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347608
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->a:LX/0ew;

    invoke-static {v0, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;)V

    .line 347609
    :goto_0
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->o:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v1, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0xf

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 347610
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    sget-object v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;->b:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v2, v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/0Tn;Ljava/lang/String;)V

    .line 347611
    return-void

    .line 347612
    :cond_1
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->c:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->a:LX/0ew;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;->b:LX/1x4;

    invoke-static {v0, v1, v2}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;LX/1x4;)V

    goto :goto_0
.end method
