.class public Lcom/facebook/rapidfeedback/RapidFeedbackController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0gu;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/lang/String;

.field public c:LX/1x4;

.field public d:Ljava/lang/Runnable;

.field public e:J

.field public f:Z

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/structuredsurvey/StructuredSurveyController;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1x7;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

.field private j:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

.field private k:LX/03V;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1x6;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7FO;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/concurrent/ScheduledExecutorService;

.field public final p:Ljava/lang/Runnable;

.field private final q:LX/1wz;

.field private final r:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final s:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 347343
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NaRF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1wz;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 1
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/rapidfeedback/gk/NewLCAUSurveyUIEnabled;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/structuredsurvey/StructuredSurveyController;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1x7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7FO;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1x6;",
            ">;",
            "LX/1wz;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 347330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347331
    new-instance v0, Lcom/facebook/rapidfeedback/RapidFeedbackController$1;

    invoke-direct {v0, p0}, Lcom/facebook/rapidfeedback/RapidFeedbackController$1;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackController;)V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->p:Ljava/lang/Runnable;

    .line 347332
    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k:LX/03V;

    .line 347333
    iput-object p2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    .line 347334
    iput-object p3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h:LX/0Ot;

    .line 347335
    iput-object p4, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->m:LX/0Ot;

    .line 347336
    iput-object p5, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->l:LX/0Or;

    .line 347337
    iput-object p6, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->q:LX/1wz;

    .line 347338
    iput-object p7, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->n:LX/0Or;

    .line 347339
    iput-object p8, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->o:Ljava/util/concurrent/ScheduledExecutorService;

    .line 347340
    iput-object p9, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 347341
    iput-object p10, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->s:LX/0SG;

    .line 347342
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/0ew;
    .locals 2

    .prologue
    .line 347327
    const-class v0, LX/0ew;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 347328
    const-string v1, "RapidFeedback Needs A FragmentManager To Launch"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347329
    return-object v0
.end method

.method public static a(Lcom/facebook/rapidfeedback/RapidFeedbackController;Landroid/content/Context;Ljava/lang/String;LX/1x4;)Ljava/lang/Runnable;
    .locals 4

    .prologue
    .line 347323
    iput-object p2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    .line 347324
    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->s:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->e:J

    .line 347325
    invoke-static {p1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v0

    .line 347326
    new-instance v1, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;

    invoke-direct {v1, p0, v0, p3}, Lcom/facebook/rapidfeedback/RapidFeedbackController$PopupSurveyDialogRunnable;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;LX/1x4;)V

    return-object v1
.end method

.method private a(LX/7FJ;Ljava/util/Map;)V
    .locals 1
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7FJ;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347240
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(LX/7FJ;Ljava/util/Map;)V

    .line 347241
    return-void
.end method

.method public static a$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;)V
    .locals 3

    .prologue
    .line 347318
    new-instance v0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-direct {v0}, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->j:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    .line 347319
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->j:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    .line 347320
    iput-object p0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;->t:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 347321
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->j:Lcom/facebook/rapidfeedback/RapidFeedbackLCAUDialogFragment;

    invoke-interface {p1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    sget-object v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 347322
    return-void
.end method

.method public static a$redex0(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;LX/1x4;)V
    .locals 5

    .prologue
    .line 347296
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347297
    iget-boolean v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    move v0, v1

    .line 347298
    iget-boolean v1, p2, LX/1x4;->h:Z

    move v1, v1

    .line 347299
    iget v2, p2, LX/1x4;->a:I

    move v2, v2

    .line 347300
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 347301
    const-string v4, "skip_intro_toast_arg"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347302
    const-string v4, "skip_outro_toast_arg"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347303
    const-string v4, "survey_theme_arg"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 347304
    new-instance v4, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-direct {v4}, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;-><init>()V

    .line 347305
    invoke-virtual {v4, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 347306
    move-object v0, v4

    .line 347307
    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 347308
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 347309
    iput-object p0, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->u:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 347310
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->M:Z

    .line 347311
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    .line 347312
    iget-object v1, p2, LX/1x4;->i:LX/1bH;

    move-object v1, v1

    .line 347313
    iput-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->o:LX/1bH;

    .line 347314
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->q:LX/1wz;

    .line 347315
    iput-object v1, v0, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->H:LX/1wz;

    .line 347316
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->i:Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;

    invoke-interface {p1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    sget-object v2, Lcom/facebook/rapidfeedback/RapidFeedbackDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 347317
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/rapidfeedback/RapidFeedbackController;
    .locals 11

    .prologue
    .line 347294
    new-instance v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    const/16 v2, 0x122b

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x122c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x35b8

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x122e

    invoke-static {p0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/1wx;->b(LX/0QB;)LX/1wz;

    move-result-object v6

    check-cast v6, LX/1wz;

    const/16 v7, 0x356

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v0 .. v10}, Lcom/facebook/rapidfeedback/RapidFeedbackController;-><init>(LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/1wz;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 347295
    return-object v0
.end method

.method public static p(Lcom/facebook/rapidfeedback/RapidFeedbackController;)Z
    .locals 2

    .prologue
    .line 347293
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347292
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Tn;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Tn;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 347288
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, p1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 347289
    if-eqz v1, :cond_0

    .line 347290
    new-instance v0, Ljava/util/ArrayList;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 347291
    :cond_0
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347344
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    invoke-virtual {v0, p0}, LX/1x6;->b(LX/0gu;)V

    .line 347345
    iget-boolean v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->f:Z

    if-eqz v0, :cond_0

    .line 347346
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-object v1, v0

    .line 347347
    :goto_0
    if-nez v1, :cond_1

    .line 347348
    :goto_1
    return-void

    .line 347349
    :cond_0
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FO;

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->c:LX/1x4;

    invoke-virtual {v0, v1, p1, v2}, LX/7FO;->a(Ljava/lang/String;LX/0Px;LX/1x4;)Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 347350
    :cond_1
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;)Lcom/facebook/structuredsurvey/StructuredSurveyController;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(LX/0Tn;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 347279
    invoke-virtual {p0, p1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/0Tn;)Ljava/util/List;

    move-result-object v0

    .line 347280
    if-nez v0, :cond_0

    .line 347281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 347282
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 347283
    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 347284
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 347285
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 347286
    :cond_1
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    const-string v2, ","

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 347287
    return-void
.end method

.method public final a(LX/7FI;)V
    .locals 3

    .prologue
    .line 347275
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 347276
    sget-object v1, LX/7FI;->ACTION:LX/7FI;

    invoke-virtual {v1}, LX/7FI;->getImpressionExtra()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/7FI;->getImpressionExtra()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347277
    sget-object v1, LX/7FJ;->SKIP:LX/7FJ;

    invoke-direct {p0, v1, v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;Ljava/util/Map;)V

    .line 347278
    return-void
.end method

.method public final a(LX/7FJ;)V
    .locals 1

    .prologue
    .line 347273
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/7FJ;Ljava/util/Map;)V

    .line 347274
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;LX/1x4;)V
    .locals 4

    .prologue
    .line 347259
    :try_start_0
    iput-object p1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b:Ljava/lang/String;

    .line 347260
    iput-object p3, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->c:LX/1x4;

    .line 347261
    iget-object v0, p3, LX/1x4;->g:Ljava/lang/Runnable;

    move-object v0, v0

    .line 347262
    if-eqz v0, :cond_0

    .line 347263
    iget-object v0, p3, LX/1x4;->g:Ljava/lang/Runnable;

    move-object v0, v0

    .line 347264
    :goto_0
    iput-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->d:Ljava/lang/Runnable;

    .line 347265
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0, p2, p1, p3}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Landroid/content/Context;Ljava/lang/String;LX/1x4;)Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347266
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x6;

    invoke-virtual {v0, p0}, LX/1x6;->a(LX/0gu;)V

    .line 347267
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1x7;

    invoke-virtual {v0, p1, p3, p2}, LX/1x7;->a(Ljava/lang/String;LX/1x4;Landroid/content/Context;)V

    .line 347268
    :goto_1
    return-void

    .line 347269
    :cond_0
    invoke-static {p0, p2, p1, p3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Lcom/facebook/rapidfeedback/RapidFeedbackController;Landroid/content/Context;Ljava/lang/String;LX/1x4;)Ljava/lang/Runnable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 347270
    :catch_0
    move-exception v0

    .line 347271
    iget-object v1, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->k:LX/03V;

    sget-object v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a:Ljava/lang/String;

    const-string v3, "NaRF:tryShow Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 347272
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    goto :goto_1
.end method

.method public final d()LX/7EX;
    .locals 2

    .prologue
    .line 347254
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347255
    :try_start_0
    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object p0, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    invoke-virtual {p0}, LX/31O;->a()Ljava/util/List;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/7EJ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    .line 347256
    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/util/List;)LX/7EX;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 347257
    :goto_0
    move-object v0, v1

    .line 347258
    return-object v0

    :catch_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 6

    .prologue
    .line 347242
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347243
    iget-boolean v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->x:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    .line 347244
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 347245
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 347246
    iget-object v2, v1, LX/31O;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 347247
    iget-object v0, v1, LX/31O;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347248
    iget-object v0, v1, LX/31O;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347249
    :cond_1
    new-instance v2, LX/7EO;

    invoke-direct {v2, v1}, LX/7EO;-><init>(LX/31O;)V

    invoke-static {v5, v2}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 347250
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_3

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v2

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->RADIO:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne v2, p0, :cond_3

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->a()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    move v2, v2

    .line 347251
    move v1, v2

    .line 347252
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 347253
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    move v2, v4

    goto :goto_1
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 347235
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347236
    iget-object p0, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    .line 347237
    iget v0, p0, LX/31O;->a:I

    move p0, v0

    .line 347238
    move v0, p0

    .line 347239
    return v0
.end method

.method public final h()V
    .locals 8

    .prologue
    .line 347217
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347218
    :try_start_0
    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    if-nez v1, :cond_0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347219
    :goto_0
    return-void

    .line 347220
    :cond_0
    :try_start_1
    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    iget-object v2, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    .line 347221
    iget-object v3, v1, LX/31O;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 347222
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 347223
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7F3;

    .line 347224
    iget-object v6, v5, LX/7F3;->b:Ljava/lang/String;

    move-object v6, v6

    .line 347225
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 347226
    instance-of v6, v5, LX/7F2;

    if-eqz v6, :cond_2

    move-object v6, v5

    .line 347227
    check-cast v6, LX/7F2;

    invoke-interface {v6}, LX/7F2;->b()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 347228
    check-cast v5, LX/7F2;

    invoke-interface {v5}, LX/7F2;->a()LX/7EQ;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 347229
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    :goto_3
    move-object v5, v5

    .line 347230
    if-eqz v5, :cond_1

    .line 347231
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-static {v1, v3, v5}, LX/31O;->a(LX/31O;Ljava/lang/String;LX/0Px;)V

    goto :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 347232
    :cond_4
    goto :goto_0

    .line 347233
    :catch_0
    move-exception v1

    .line 347234
    iget-object v2, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    sget-object v3, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    const-string v4, "NaRF:Page Answer Record Failed"

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    move-object v5, v7

    goto :goto_3
.end method

.method public final i()V
    .locals 14

    .prologue
    .line 347185
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 347186
    new-instance v1, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;

    iget-object v2, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->u:Ljava/lang/String;

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 347187
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 347188
    iget-object v6, v5, LX/7ER;->a:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 347189
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 347190
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Px;

    .line 347191
    new-instance v12, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;

    invoke-direct {v12}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;-><init>()V

    .line 347192
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v13

    const/4 v8, 0x0

    move v9, v8

    :goto_1
    if-ge v9, v13, :cond_0

    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/7EQ;

    .line 347193
    iget-object v5, v8, LX/7EQ;->b:Ljava/lang/String;

    move-object v8, v5

    .line 347194
    invoke-virtual {v12, v8}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;->add(Ljava/lang/Object;)Z

    .line 347195
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_1

    .line 347196
    :cond_0
    invoke-virtual {v10, v7, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 347197
    :cond_1
    invoke-static {v10}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v6

    move-object v5, v6

    .line 347198
    iget-object v6, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 347199
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v10

    .line 347200
    iget-object v7, v6, LX/7ER;->b:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0Px;

    .line 347201
    new-instance v12, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;

    invoke-direct {v12}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;-><init>()V

    .line 347202
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v13

    const/4 v8, 0x0

    move v9, v8

    :goto_3
    if-ge v9, v13, :cond_2

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 347203
    invoke-virtual {v12, v8}, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;->add(Ljava/lang/Object;)Z

    .line 347204
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_3

    .line 347205
    :cond_2
    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 347206
    :cond_3
    invoke-static {v10}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    move-object v6, v7

    .line 347207
    iget-object v7, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 347208
    iget-object v8, v7, LX/7ER;->c:Ljava/util/List;

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v8

    move-object v7, v8

    .line 347209
    iget-object v8, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347210
    iget-object v9, v8, LX/1x4;->d:LX/0P1;

    move-object v8, v9

    .line 347211
    invoke-static {v8}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;-><init>(Ljava/lang/String;Ljava/lang/String;ZLX/0P1;LX/0Px;LX/0Px;LX/0P1;)V

    .line 347212
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 347213
    const-string v2, "postSurveyAnswersParams"

    invoke-virtual {v3, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 347214
    iget-object v1, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0aG;

    const-string v2, "post_survey_answers"

    sget-object v4, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v5, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    const v6, 0x73afe491

    invoke-static/range {v1 .. v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 347215
    new-instance v2, LX/7EL;

    invoke-direct {v2, v0}, LX/7EL;-><init>(Lcom/facebook/structuredsurvey/StructuredSurveyController;)V

    iget-object v3, v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 347216
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 347183
    iget-object v0, p0, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j()V

    .line 347184
    return-void
.end method
