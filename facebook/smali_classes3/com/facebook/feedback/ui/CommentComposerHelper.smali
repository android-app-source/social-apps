.class public Lcom/facebook/feedback/ui/CommentComposerHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/20i;

.field private final c:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 357941
    const-class v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/CommentComposerHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/20i;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357938
    iput-object p1, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->b:LX/20i;

    .line 357939
    iput-object p2, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->c:LX/0ad;

    .line 357940
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedback/ui/CommentComposerHelper;
    .locals 5

    .prologue
    .line 357918
    const-class v1, Lcom/facebook/feedback/ui/CommentComposerHelper;

    monitor-enter v1

    .line 357919
    :try_start_0
    sget-object v0, Lcom/facebook/feedback/ui/CommentComposerHelper;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357920
    sput-object v2, Lcom/facebook/feedback/ui/CommentComposerHelper;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357923
    new-instance p0, Lcom/facebook/feedback/ui/CommentComposerHelper;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v3

    check-cast v3, LX/20i;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedback/ui/CommentComposerHelper;-><init>(LX/20i;LX/0ad;)V

    .line 357924
    move-object v0, p0

    .line 357925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedback/ui/CommentComposerHelper;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/21n;)V
    .locals 2

    .prologue
    .line 357930
    invoke-interface {p0}, LX/21n;->getSelfAsView()Landroid/view/View;

    move-result-object v1

    .line 357931
    const v0, 0x7f0d00a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 357932
    if-nez v0, :cond_0

    .line 357933
    const v0, 0x7f0d09d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 357934
    :cond_0
    if-eqz v0, :cond_1

    .line 357935
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 357936
    :cond_1
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 357942
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 357929
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 1

    .prologue
    .line 357917
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 357913
    if-nez p1, :cond_1

    .line 357914
    :cond_0
    :goto_0
    return v0

    .line 357915
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->c:LX/0ad;

    sget-short v2, LX/36p;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->b:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->b:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedback/ui/CommentComposerHelper;->b:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x285feb

    if-eq v1, v2, :cond_0

    .line 357916
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->d()Z

    move-result v0

    goto :goto_0
.end method
