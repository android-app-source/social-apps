.class public Lcom/facebook/feedback/ui/SingleLineCommentComposerView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/21l;
.implements LX/21m;
.implements LX/21n;
.implements LX/21q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;",
        "LX/21m;",
        "LX/21n;",
        "LX/21q;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/view/ViewStub;

.field private C:Lcom/facebook/fbreact/fragment/FbReactFragment;

.field private D:LX/9Cx;

.field private E:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Landroid/widget/ImageButton;

.field private L:Lcom/facebook/fbui/glyph/GlyphView;

.field private M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

.field public N:LX/9CP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Landroid/graphics/Rect;

.field private P:LX/9kz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Ljava/lang/String;

.field private V:I

.field private W:I

.field private aa:Z

.field public ab:Z

.field private ac:Z

.field private b:Lcom/facebook/feedback/ui/CommentComposerHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/8nK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/3iR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/9l0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/2Yg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/A6X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/A6h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9Cx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private p:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/ipc/media/MediaItem;

.field private r:Lcom/facebook/ipc/media/StickerItem;

.field public s:LX/9D8;

.field private t:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field private u:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field private v:LX/9F4;

.field public w:LX/9EI;

.field private x:Landroid/view/View;

.field private y:Landroid/view/ViewStub;

.field private z:Landroid/view/ViewStub;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 358290
    const-class v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    const-string v1, "story_feedback_flyout"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 358288
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 358289
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 358285
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 358286
    invoke-direct {p0, p1, p2}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 358287
    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 358281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    .line 358282
    invoke-static {p0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(LX/21n;)V

    .line 358283
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358284
    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 358274
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-nez v0, :cond_0

    .line 358275
    :goto_0
    return-void

    .line 358276
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->d:LX/3iR;

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->t:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 358277
    const-string v2, "comment_sticker_keyboard_opened"

    invoke-static {v0, v2, v1}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v2

    .line 358278
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 358279
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 358280
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p0, p0}, LX/9CP;->a(Landroid/content/Context;Landroid/view/View;LX/21p;)V

    goto :goto_0
.end method

.method private C()V
    .locals 1

    .prologue
    .line 358271
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-nez v0, :cond_0

    .line 358272
    :goto_0
    return-void

    .line 358273
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    invoke-virtual {v0}, LX/9CP;->j()V

    goto :goto_0
.end method

.method private D()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 358268
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-nez v0, :cond_1

    .line 358269
    :cond_0
    const/4 v0, 0x0

    .line 358270
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v3}, LX/9EI;->c()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->R:Z

    iget-boolean v5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    iget-object v6, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    iget-object v7, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r:Lcom/facebook/ipc/media/StickerItem;

    move v9, v8

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358174
    const-class v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    invoke-static {v0, p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 358175
    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->setOrientation(I)V

    .line 358176
    const v0, 0x7f0302c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 358177
    const v0, 0x7f0d09c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 358178
    sget-object v3, LX/03r;->CommentComposerView:[I

    invoke-virtual {p1, p2, v3, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 358179
    const/16 v4, 0x3

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "no_upgrade"

    iget-object v5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->g:LX/0ad;

    sget-char v6, LX/0wn;->aC:C

    const-string v7, "no_upgrade"

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 358180
    :goto_0
    if-eqz v1, :cond_0

    .line 358181
    const v1, 0x7f0302c9

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 358182
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->R:Z

    .line 358183
    iput-boolean v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    .line 358184
    const/16 v1, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->V:I

    .line 358185
    const/16 v1, 0x1

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->W:I

    .line 358186
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 358187
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->l:LX/0Sh;

    new-instance v2, Lcom/facebook/feedback/ui/SingleLineCommentComposerView$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView$1;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;Landroid/view/ViewStub;)V

    invoke-virtual {v1, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 358188
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->O:Landroid/graphics/Rect;

    .line 358189
    const v0, 0x7f0d09c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x:Landroid/view/View;

    .line 358190
    const v0, 0x7f0d00a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    .line 358191
    const v0, 0x7f0d09f3

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->L:Lcom/facebook/fbui/glyph/GlyphView;

    .line 358192
    const v0, 0x7f0d00a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    .line 358193
    const v0, 0x7f0d09f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->y:Landroid/view/ViewStub;

    .line 358194
    const v0, 0x7f0d0a5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->z:Landroid/view/ViewStub;

    .line 358195
    const v0, 0x7f0d09bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->B:Landroid/view/ViewStub;

    .line 358196
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    .line 358197
    iput-object p0, v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->e:LX/21m;

    .line 358198
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->setVisibility(I)V

    .line 358199
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358200
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->j:LX/1Ad;

    sget-object v1, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 358201
    return-void

    :cond_1
    move v1, v2

    .line 358202
    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;Lcom/facebook/feedback/ui/CommentComposerHelper;LX/8nK;LX/3iR;LX/0if;LX/9l0;LX/0ad;LX/2Yg;LX/A6X;LX/1Ad;LX/A6h;LX/0Sh;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedback/ui/SingleLineCommentComposerView;",
            "Lcom/facebook/feedback/ui/CommentComposerHelper;",
            "LX/8nK;",
            "LX/3iR;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/9l0;",
            "LX/0ad;",
            "LX/2Yg;",
            "LX/A6X;",
            "LX/1Ad;",
            "LX/A6h;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/9Dp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9Cx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 358266
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b:Lcom/facebook/feedback/ui/CommentComposerHelper;

    iput-object p2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->c:LX/8nK;

    iput-object p3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->d:LX/3iR;

    iput-object p4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->e:LX/0if;

    iput-object p5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->f:LX/9l0;

    iput-object p6, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->g:LX/0ad;

    iput-object p7, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->h:LX/2Yg;

    iput-object p8, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->i:LX/A6X;

    iput-object p9, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->j:LX/1Ad;

    iput-object p10, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->k:LX/A6h;

    iput-object p11, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->l:LX/0Sh;

    iput-object p12, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->m:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->n:LX/0Ot;

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1

    .prologue
    .line 358258
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    if-eqz v0, :cond_1

    .line 358259
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358260
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ac:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    .line 358261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ac:Z

    .line 358262
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 358263
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->j()V

    .line 358264
    :cond_0
    :goto_0
    return-void

    .line 358265
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;

    invoke-static {v13}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(LX/0QB;)Lcom/facebook/feedback/ui/CommentComposerHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedback/ui/CommentComposerHelper;

    invoke-static {v13}, LX/8nK;->b(LX/0QB;)LX/8nK;

    move-result-object v2

    check-cast v2, LX/8nK;

    invoke-static {v13}, LX/3iR;->a(LX/0QB;)LX/3iR;

    move-result-object v3

    check-cast v3, LX/3iR;

    invoke-static {v13}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v4

    check-cast v4, LX/0if;

    const-class v5, LX/9l0;

    invoke-interface {v13, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/9l0;

    invoke-static {v13}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v13}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v7

    check-cast v7, LX/2Yg;

    invoke-static {v13}, LX/A6X;->a(LX/0QB;)LX/A6X;

    move-result-object v8

    check-cast v8, LX/A6X;

    invoke-static {v13}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v9

    check-cast v9, LX/1Ad;

    invoke-static {v13}, LX/A6h;->b(LX/0QB;)LX/A6h;

    move-result-object v10

    check-cast v10, LX/A6h;

    invoke-static {v13}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    const/16 v12, 0x1d9c

    invoke-static {v13, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v14, 0x1d92

    invoke-static {v13, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v0 .. v13}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;Lcom/facebook/feedback/ui/CommentComposerHelper;LX/8nK;LX/3iR;LX/0if;LX/9l0;LX/0ad;LX/2Yg;LX/A6X;LX/1Ad;LX/A6h;LX/0Sh;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 358251
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Dp;

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0, v1}, LX/9Dp;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Ljava/lang/String;

    move-result-object v0

    .line 358252
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 358253
    invoke-static {p2, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358254
    if-eqz p3, :cond_0

    const v0, 0x7f081223

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358255
    :goto_0
    return-object v0

    .line 358256
    :cond_0
    const v0, 0x7f081224

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358257
    :cond_1
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f081222

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081221

    new-array v2, v6, [LX/47s;

    const/4 v3, 0x0

    new-instance v4, LX/47s;

    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v6, 0x21

    invoke-direct {v4, p1, v5, v6}, LX/47s;-><init>(Ljava/lang/Object;Ljava/lang/Object;I)V

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 8

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 358222
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358223
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 358224
    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->setVisibility(I)V

    .line 358225
    :cond_0
    :goto_0
    return-void

    .line 358226
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w()V

    .line 358227
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b:Lcom/facebook/feedback/ui/CommentComposerHelper;

    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2, v3}, Lcom/facebook/feedback/ui/CommentComposerHelper;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 358228
    :cond_2
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 358229
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358230
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 358231
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->I:Landroid/view/View;

    if-nez v0, :cond_4

    .line 358232
    const v0, 0x7f0d0a07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    .line 358233
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 358234
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->I:Landroid/view/View;

    .line 358235
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->c:LX/8nK;

    invoke-virtual {v0, p1}, LX/8nK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 358236
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->c:LX/8nK;

    .line 358237
    iput-object v2, v0, LX/9EI;->f:LX/8nB;

    .line 358238
    iget-object v3, v0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 358239
    iget-object v3, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v4, v0, LX/9EI;->f:LX/8nB;

    invoke-virtual {v3, v4}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8nB;)V

    .line 358240
    :cond_5
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->y()V

    .line 358241
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->z()V

    .line 358242
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358243
    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->setVisibility(I)V

    .line 358244
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->k:LX/A6h;

    invoke-virtual {v0}, LX/A6h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358245
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->L:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 358246
    :cond_6
    invoke-interface {p0}, LX/21n;->getSelfAsView()Landroid/view/View;

    move-result-object v3

    .line 358247
    const v2, 0x7f0d00a0

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 358248
    if-eqz v2, :cond_3

    .line 358249
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 358250
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08111c

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->t()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private b(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358206
    if-nez p1, :cond_1

    .line 358207
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r()V

    .line 358208
    :cond_0
    :goto_0
    return-void

    .line 358209
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->l()V

    .line 358210
    iput-boolean v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->T:Z

    .line 358211
    iget-object v0, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->U:Ljava/lang/String;

    .line 358212
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->g:LX/0ad;

    sget-short v1, LX/0wn;->s:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 358213
    iget-boolean v0, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->e:Z

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    .line 358214
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    .line 358215
    iput-boolean v2, v0, LX/9F4;->b:Z

    .line 358216
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 358217
    iget-object v1, p1, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 358218
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 358219
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    .line 358220
    iput-boolean v3, v0, LX/9F4;->b:Z

    .line 358221
    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 358203
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->T:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 358204
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->n()V

    .line 358205
    :cond_2
    return-void
.end method

.method private r()V
    .locals 1

    .prologue
    .line 358451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->T:Z

    .line 358452
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->U:Ljava/lang/String;

    .line 358453
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 358319
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v1}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 358320
    return-void
.end method

.method public static setUpProgressiveEditTextHelper(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;Landroid/view/ViewStub;)V
    .locals 2

    .prologue
    .line 358445
    new-instance v1, LX/9EI;

    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-direct {v1, v0}, LX/9EI;-><init>(Landroid/widget/EditText;)V

    iput-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 358446
    new-instance v0, LX/9F4;

    invoke-direct {v0, p0}, LX/9F4;-><init>(LX/21q;)V

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    .line 358447
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    .line 358448
    iput-object v1, v0, LX/9EI;->e:Landroid/text/TextWatcher;

    .line 358449
    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 358450
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 358436
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    if-nez v0, :cond_0

    .line 358437
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->y:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    .line 358438
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_1

    .line 358439
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    const v1, 0x7f0d09ee

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    .line 358440
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/9Eg;

    invoke-direct {v1, p0}, LX/9Eg;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358441
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->G:Landroid/view/View;

    if-nez v0, :cond_2

    .line 358442
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    const v1, 0x7f0d09ef

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->G:Landroid/view/View;

    .line 358443
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->G:Landroid/view/View;

    new-instance v1, LX/9Eh;

    invoke-direct {v1, p0}, LX/9Eh;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358444
    :cond_2
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 358431
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 358432
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358433
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_1

    .line 358434
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 358435
    :cond_1
    return-void
.end method

.method private v()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 358402
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 358403
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->T:Z

    if-eqz v0, :cond_2

    .line 358404
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->d:LX/3iR;

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->t:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->U:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v4}, LX/9EI;->c()Ljava/lang/String;

    move-result-object v4

    .line 358405
    const-string v5, "comment_draft_posted"

    .line 358406
    invoke-static {v0, v5, v2}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v6

    .line 358407
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 358408
    const-string v7, "comment_draft_text"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 358409
    :cond_0
    move-object v5, v6

    .line 358410
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 358411
    const-string v6, "comment_text"

    invoke-virtual {v5, v6, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 358412
    invoke-virtual {v5}, LX/0oG;->d()V

    .line 358413
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r()V

    .line 358414
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->u:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->R:Z

    if-nez v0, :cond_3

    .line 358415
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->h:LX/2Yg;

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->u:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    const-string v3, "beeper_caused_comment"

    invoke-virtual {v0, v2, v3}, LX/2Yg;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 358416
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    if-eqz v0, :cond_4

    .line 358417
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->i:LX/A6X;

    .line 358418
    sget-object v2, LX/A6W;->COMMENT_POSTED:LX/A6W;

    iget-object v2, v2, LX/A6W;->eventName:Ljava/lang/String;

    invoke-static {v0, v2}, LX/A6X;->a(LX/A6X;Ljava/lang/String;)V

    .line 358419
    :cond_4
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    .line 358420
    if-eqz v0, :cond_5

    iget-object v2, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 358421
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "Posting failed"

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 358422
    :goto_1
    return-void

    .line 358423
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 358424
    :cond_7
    iget-boolean v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->aa:Z

    if-eqz v1, :cond_9

    .line 358425
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->e:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reshare_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/9FI;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 358426
    :goto_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 358427
    iget-object v2, v1, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 358428
    iget-object v2, v1, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b()V

    .line 358429
    :cond_8
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    invoke-virtual {v1, v0}, LX/9CP;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    goto :goto_1

    .line 358430
    :cond_9
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->e:LX/0if;

    sget-object v2, LX/0ig;->W:LX/0ih;

    invoke-static {v0}, LX/9FI;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 358392
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 358393
    :cond_0
    iput-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    .line 358394
    iput-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->Q:Ljava/lang/String;

    .line 358395
    :cond_1
    :goto_0
    return-void

    .line 358396
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 358397
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    .line 358398
    iget-object v3, v1, LX/9kz;->d:Ljava/lang/String;

    move-object v1, v3

    .line 358399
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 358400
    :cond_3
    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->Q:Ljava/lang/String;

    .line 358401
    iput-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    goto :goto_0
.end method

.method private x()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358379
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v4

    .line 358380
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    invoke-virtual {v0, p0}, LX/9CP;->a(LX/21p;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 358381
    :goto_0
    iget-object v5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->k()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v5, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setShowSticker(Z)V

    .line 358382
    iget-object v5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    move v3, v1

    :goto_2
    invoke-virtual {v5, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setSelected(Z)V

    .line 358383
    iget-object v5, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->k()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    move v3, v1

    :goto_3
    invoke-virtual {v5, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setEnabled(Z)V

    .line 358384
    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b:Lcom/facebook/feedback/ui/CommentComposerHelper;

    iget-object v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v3, v4}, Lcom/facebook/feedback/ui/CommentComposerHelper;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    if-nez v3, :cond_5

    if-nez v0, :cond_5

    .line 358385
    :goto_4
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 358386
    return-void

    :cond_1
    move v0, v2

    .line 358387
    goto :goto_0

    :cond_2
    move v3, v2

    .line 358388
    goto :goto_1

    :cond_3
    move v3, v2

    .line 358389
    goto :goto_2

    :cond_4
    move v3, v2

    .line 358390
    goto :goto_3

    :cond_5
    move v1, v2

    .line 358391
    goto :goto_4
.end method

.method private y()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358454
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 358455
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 358456
    :goto_0
    iget-boolean v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    if-eqz v4, :cond_2

    .line 358457
    if-eqz v0, :cond_1

    const v0, 0x7f081230

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 358458
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v1}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 358459
    return-void

    :cond_0
    move v0, v2

    .line 358460
    goto :goto_0

    .line 358461
    :cond_1
    const v0, 0x7f080ff3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 358462
    :cond_2
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->W:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->V:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private z()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 358370
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->ad()I

    move-result v1

    .line 358371
    if-nez v1, :cond_0

    .line 358372
    :goto_0
    return-void

    .line 358373
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->J:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_1

    .line 358374
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->z:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d0a60

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->J:Lcom/facebook/resources/ui/FbTextView;

    .line 358375
    :cond_1
    if-ne v1, v3, :cond_2

    .line 358376
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08126f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358377
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->J:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 358378
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f081270

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 358364
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-eqz v0, :cond_0

    .line 358365
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 358366
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r:Lcom/facebook/ipc/media/StickerItem;

    .line 358367
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A()V

    .line 358368
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358369
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 358350
    if-nez p1, :cond_0

    .line 358351
    :goto_0
    return-void

    .line 358352
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 358353
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358354
    iput-boolean v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    .line 358355
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_2

    .line 358356
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->setVisibility(I)V

    .line 358357
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358358
    iput-boolean v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ac:Z

    goto :goto_0

    .line 358359
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 358360
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 358361
    if-eqz p2, :cond_3

    .line 358362
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->B()V

    goto :goto_0

    .line 358363
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->j()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/stickers/model/Sticker;)V
    .locals 5

    .prologue
    .line 358336
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 358337
    :cond_0
    :goto_0
    return-void

    .line 358338
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a()V

    .line 358339
    invoke-static {}, Lcom/facebook/ipc/media/StickerItem;->b()LX/4gH;

    move-result-object v0

    .line 358340
    iput-object p1, v0, LX/4gH;->d:Lcom/facebook/stickers/model/Sticker;

    .line 358341
    move-object v0, v0

    .line 358342
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 358343
    iput-wide v2, v0, LX/4gH;->c:J

    .line 358344
    move-object v0, v0

    .line 358345
    iget-object v1, p1, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 358346
    iput-object v1, v0, LX/4gH;->b:Ljava/lang/String;

    .line 358347
    move-object v0, v0

    .line 358348
    invoke-virtual {v0}, LX/4gH;->a()Lcom/facebook/ipc/media/StickerItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r:Lcom/facebook/ipc/media/StickerItem;

    .line 358349
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;)V
    .locals 1

    .prologue
    .line 358331
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 358332
    iput-object p1, v0, LX/9EI;->g:Lcom/facebook/tagging/model/TaggingProfile;

    .line 358333
    iget-object p0, v0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 358334
    iget-object p0, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Lcom/facebook/tagging/model/TaggingProfile;)V

    .line 358335
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V
    .locals 0

    .prologue
    .line 358328
    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)V

    .line 358329
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358330
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;III)V
    .locals 9

    .prologue
    .line 358321
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getTypingContext()LX/9kz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358322
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getTypingContext()LX/9kz;

    move-result-object v0

    invoke-virtual {v0}, LX/9kz;->b()V

    .line 358323
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_1

    if-nez p3, :cond_1

    if-nez p2, :cond_1

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 358324
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358325
    iget-object v1, v0, LX/9CP;->g:LX/9FG;

    invoke-static {v0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v2

    .line 358326
    iget-object v3, v1, LX/9FG;->a:LX/0if;

    sget-object v4, LX/0ig;->j:LX/0ih;

    iget-wide v5, v1, LX/9FG;->b:J

    const-string v7, "text_filled"

    invoke-virtual {v2}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 358327
    :cond_1
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 358267
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, p1}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 358291
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0ew;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 358292
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 358293
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    if-nez v1, :cond_0

    .line 358294
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->B:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    .line 358295
    :cond_0
    if-ltz p3, :cond_1

    .line 358296
    int-to-float v1, p3

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int p3, v1

    .line 358297
    :cond_1
    if-ltz p4, :cond_2

    .line 358298
    int-to-float v1, p4

    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int p4, v1

    .line 358299
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, p3, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 358300
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 358301
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v1

    .line 358302
    iput-object p1, v1, LX/98r;->c:Ljava/lang/String;

    .line 358303
    move-object v1, v1

    .line 358304
    iput-object p2, v1, LX/98r;->f:Landroid/os/Bundle;

    .line 358305
    move-object v1, v1

    .line 358306
    invoke-virtual {v1}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 358307
    invoke-static {v1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->C:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 358308
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 358309
    const-string v1, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 358310
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->C:Lcom/facebook/fbreact/fragment/FbReactFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 358311
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D:LX/9Cx;

    if-nez v0, :cond_3

    .line 358312
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Cx;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D:LX/9Cx;

    .line 358313
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D:LX/9Cx;

    .line 358314
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/9Cx;->c:Ljava/lang/ref/WeakReference;

    .line 358315
    iget-object v1, v0, LX/9Cx;->b:LX/0Yb;

    if-nez v1, :cond_4

    .line 358316
    iget-object v1, v0, LX/9Cx;->a:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "CommentInlineBannerListener.SET_INPUT_TEXT"

    new-instance v3, LX/9Cw;

    invoke-direct {v3, v0}, LX/9Cw;-><init>(LX/9Cx;)V

    invoke-interface {v1, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/9Cx;->b:LX/0Yb;

    .line 358317
    :cond_4
    iget-object v1, v0, LX/9Cx;->b:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 358318
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 357982
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 357983
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->t()V

    .line 357984
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 357985
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->E:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 357986
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 358031
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358032
    :goto_0
    return-void

    .line 358033
    :cond_0
    iput-boolean p2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    .line 358034
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    const/4 v1, 0x0

    .line 358035
    iput-boolean v1, v0, LX/9F4;->b:Z

    .line 358036
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 358037
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 358038
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 358039
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v:LX/9F4;

    const/4 v1, 0x1

    .line 358040
    iput-boolean v1, v0, LX/9F4;->b:Z

    .line 358041
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 358027
    if-eqz p1, :cond_0

    .line 358028
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->B()V

    .line 358029
    :goto_0
    return-void

    .line 358030
    :cond_0
    new-instance v0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView$6;

    invoke-direct {v0, p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView$6;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(II)Z
    .locals 3

    .prologue
    .line 358024
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 358025
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->O:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 358026
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->O:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLineCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/EditText;->getLineHeight()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/widget/EditText;->getHeight()I

    move-result v0

    if-gt v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 358022
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 358023
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 358016
    iget-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 358017
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->u()V

    .line 358018
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->ab:Z

    .line 358019
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-direct {p0, v0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 358020
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 358021
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 358014
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358015
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 358012
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358013
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 358010
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->v()V

    .line 358011
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 358008
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->B()V

    .line 358009
    return-void
.end method

.method public getInlineReactBannerRootTag()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 358001
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->C:Lcom/facebook/fbreact/fragment/FbReactFragment;

    if-nez v1, :cond_1

    .line 358002
    :cond_0
    :goto_0
    return v0

    .line 358003
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->C:Lcom/facebook/fbreact/fragment/FbReactFragment;

    .line 358004
    iget-object p0, v1, Lcom/facebook/fbreact/fragment/FbReactFragment;->l:Lcom/facebook/react/ReactRootView;

    move-object v1, p0

    .line 358005
    if-eqz v1, :cond_0

    .line 358006
    iget v0, v1, Lcom/facebook/react/ReactRootView;->e:I

    move v0, v0

    .line 358007
    goto :goto_0
.end method

.method public getPendingComment()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358000
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D()Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    move-result-object v0

    return-object v0
.end method

.method public getPhotoButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 357999
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public getSelfAsView()Landroid/view/View;
    .locals 0

    .prologue
    .line 357998
    return-object p0
.end method

.method public getTypingContext()LX/9kz;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 357989
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->Q:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 357990
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->f:LX/9l0;

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->Q:Ljava/lang/String;

    .line 357991
    new-instance v5, LX/9kz;

    .line 357992
    new-instance v3, LX/9l3;

    const/16 v2, 0xafd

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v4, v2}, LX/9l3;-><init>(LX/0Ot;LX/0ad;)V

    .line 357993
    move-object v2, v3

    .line 357994
    check-cast v2, LX/9l2;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {v5, v2, v3, v4, v1}, LX/9kz;-><init>(LX/9l2;Ljava/util/concurrent/ScheduledExecutorService;LX/0SG;Ljava/lang/String;)V

    .line 357995
    move-object v0, v5

    .line 357996
    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    .line 357997
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->P:LX/9kz;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 357987
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->C()V

    .line 357988
    return-void
.end method

.method public final i()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 357943
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->s()V

    .line 357944
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q()V

    .line 357945
    const-string v0, ""

    .line 357946
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-eqz v1, :cond_0

    .line 357947
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->c()Ljava/lang/String;

    move-result-object v0

    .line 357948
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 357949
    invoke-virtual {v1}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 357950
    invoke-virtual {v1}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v2

    iget-object v4, v1, LX/9EI;->e:Landroid/text/TextWatcher;

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 357951
    invoke-virtual {v1}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 357952
    iput-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 357953
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->d:LX/3iR;

    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->t:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 357954
    const-string v4, "comment_composer_session_ended"

    invoke-static {v1, v4, v2}, LX/3iR;->b(LX/3iR;Ljava/lang/String;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/0oG;

    move-result-object v4

    .line 357955
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 357956
    const-string v5, "comment_text"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 357957
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 357958
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    .line 357959
    iput-object v3, v0, Lcom/facebook/feedback/ui/CommentComposerPostButton;->e:LX/21m;

    .line 357960
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->M:Lcom/facebook/feedback/ui/CommentComposerPostButton;

    invoke-virtual {v0, v3}, Lcom/facebook/feedback/ui/CommentComposerPostButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357961
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357962
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->L:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357963
    iput-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->s:LX/9D8;

    .line 357964
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_2

    .line 357965
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 357966
    invoke-static {v0}, LX/9CP;->F(LX/9CP;)V

    .line 357967
    iput-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 357968
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_3

    .line 357969
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->F:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357970
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->G:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 357971
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->G:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357972
    :cond_4
    iput-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 357973
    iput-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 357974
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 357975
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->A:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 357976
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D:LX/9Cx;

    if-eqz v0, :cond_7

    .line 357977
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->D:LX/9Cx;

    .line 357978
    iget-object v1, v0, LX/9Cx;->b:LX/0Yb;

    if-eqz v1, :cond_6

    .line 357979
    iget-object v1, v0, LX/9Cx;->b:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 357980
    :cond_6
    const/4 v1, 0x0

    iput-object v1, v0, LX/9Cx;->c:Ljava/lang/ref/WeakReference;

    .line 357981
    :cond_7
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 358101
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-eqz v0, :cond_0

    .line 358102
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object v0

    .line 358103
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 358104
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 358105
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 358172
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-nez v1, :cond_1

    .line 358173
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    invoke-virtual {v1}, LX/9EI;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 358146
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-eqz v0, :cond_0

    .line 358147
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    const/4 p0, 0x0

    const/4 v5, 0x0

    .line 358148
    iget-object v1, v0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358149
    :cond_0
    :goto_0
    return-void

    .line 358150
    :cond_1
    iget-object v1, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 358151
    iget-object v1, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 358152
    const v3, 0x7f0302c8

    invoke-virtual {v2, v3, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 358153
    iget-object v2, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 358154
    iget-object v2, v0, LX/9EI;->b:Landroid/widget/EditText;

    iget-object v3, v0, LX/9EI;->e:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 358155
    iget-object v2, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 358156
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->d:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 358157
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->e:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 358158
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 358159
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->h:Ljava/lang/Long;

    sget-object v4, LX/7Gm;->COMMENT:LX/7Gm;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Ljava/lang/Long;LX/7Gm;)V

    .line 358160
    iget-object v2, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 358161
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/4 v3, 0x2

    invoke-static {v2, v3}, LX/0vv;->d(Landroid/view/View;I)V

    .line 358162
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->requestFocus()Z

    .line 358163
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-static {v2, v5}, LX/0vv;->d(Landroid/view/View;I)V

    .line 358164
    :cond_2
    iget-object v2, v0, LX/9EI;->f:LX/8nB;

    if-eqz v2, :cond_3

    .line 358165
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->f:LX/8nB;

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(LX/8nB;)V

    .line 358166
    :cond_3
    iget-object v2, v0, LX/9EI;->g:Lcom/facebook/tagging/model/TaggingProfile;

    if-eqz v2, :cond_4

    .line 358167
    iget-object v2, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object v3, v0, LX/9EI;->g:Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v2, v3}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Lcom/facebook/tagging/model/TaggingProfile;)V

    .line 358168
    :cond_4
    iget-object v2, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 358169
    iget-object v3, v0, LX/9EI;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 358170
    iget-object v3, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v1, v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 358171
    iput-object p0, v0, LX/9EI;->b:Landroid/widget/EditText;

    goto/16 :goto_0
.end method

.method public final m()V
    .locals 9

    .prologue
    .line 358141
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_0

    .line 358142
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358143
    iget-object v1, v0, LX/9CP;->g:LX/9FG;

    invoke-static {v0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v2

    .line 358144
    iget-object v3, v1, LX/9FG;->a:LX/0if;

    sget-object v4, LX/0ig;->j:LX/0ih;

    iget-wide v5, v1, LX/9FG;->b:J

    const-string v7, "media_item_cleared"

    invoke-virtual {v2}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 358145
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 358138
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getTypingContext()LX/9kz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358139
    invoke-virtual {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->getTypingContext()LX/9kz;

    move-result-object v0

    invoke-virtual {v0}, LX/9kz;->c()V

    .line 358140
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 9

    .prologue
    .line 358131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->S:Z

    .line 358132
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->r()V

    .line 358133
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_0

    .line 358134
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358135
    iget-object v1, v0, LX/9CP;->g:LX/9FG;

    invoke-static {v0}, LX/9CP;->T(LX/9CP;)LX/9FF;

    move-result-object v2

    .line 358136
    iget-object v3, v1, LX/9FG;->a:LX/0if;

    sget-object v4, LX/0ig;->j:LX/0ih;

    iget-wide v5, v1, LX/9FG;->b:J

    const-string v7, "text_cleared"

    invoke-virtual {v2}, LX/9FF;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 358137
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 358129
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358130
    return-void
.end method

.method public setCommentComposerManager(LX/9CP;)V
    .locals 2
    .param p1    # LX/9CP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 358106
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_0

    .line 358107
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358108
    invoke-static {v0}, LX/9CP;->F(LX/9CP;)V

    .line 358109
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358110
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    if-eqz v0, :cond_2

    .line 358111
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->N:LX/9CP;

    .line 358112
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    const-string p1, "sticky composer cannot be changed from this method, use removeStickyComposer() first"

    invoke-static {v1, p1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 358113
    const-string v1, "cannot clear comment composer from this method, use removeStickyComposer()"

    invoke-static {p0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358114
    iput-object p0, v0, LX/9CP;->n:LX/21o;

    .line 358115
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    .line 358116
    iget-object p1, v0, LX/9CP;->v:Landroid/view/View$OnFocusChangeListener;

    if-nez p1, :cond_1

    .line 358117
    new-instance p1, LX/9CJ;

    invoke-direct {p1, v0}, LX/9CJ;-><init>(LX/9CP;)V

    iput-object p1, v0, LX/9CP;->v:Landroid/view/View$OnFocusChangeListener;

    .line 358118
    :cond_1
    iget-object p1, v0, LX/9CP;->v:Landroid/view/View$OnFocusChangeListener;

    move-object p1, p1

    .line 358119
    invoke-interface {v1, p1}, LX/21o;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 358120
    iget-object v1, v0, LX/9CP;->n:LX/21o;

    iget-object p1, v0, LX/9CP;->B:LX/9DF;

    invoke-interface {v1, p1}, LX/21o;->setMediaPickerListener(LX/9DF;)V

    .line 358121
    invoke-static {v0}, LX/9CP;->O(LX/9CP;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 358122
    invoke-static {v0}, LX/9CP;->v(LX/9CP;)V

    .line 358123
    :cond_2
    :goto_1
    return-void

    .line 358124
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 358125
    :cond_4
    invoke-static {v0}, LX/9CP;->L(LX/9CP;)V

    .line 358126
    invoke-static {v0}, LX/9CP;->C(LX/9CP;)V

    .line 358127
    sget-object v1, LX/9CN;->TOP_LEVEL:LX/9CN;

    iput-object v1, v0, LX/9CP;->E:LX/9CN;

    .line 358128
    invoke-static {v0}, LX/9CP;->P(LX/9CP;)V

    goto :goto_1
.end method

.method public setFeedbackLoggingParams(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V
    .locals 0

    .prologue
    .line 358042
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->t:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 358043
    return-void
.end method

.method public setGroupIdForTagging(Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 358096
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 358097
    iput-object p1, v0, LX/9EI;->h:Ljava/lang/Long;

    .line 358098
    iget-object v1, v0, LX/9EI;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 358099
    iget-object v1, v0, LX/9EI;->c:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    sget-object p0, LX/7Gm;->COMMENT:LX/7Gm;

    invoke-virtual {v1, p1, p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Ljava/lang/Long;LX/7Gm;)V

    .line 358100
    :cond_0
    return-void
.end method

.method public setIsVisible(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 358088
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    .line 358089
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 358090
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->H:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 358091
    iget-object v2, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->H:Landroid/view/View;

    iget-object v3, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v3, :cond_0

    move v1, v0

    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358092
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->I:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 358093
    iget-object v1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->I:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 358094
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 358095
    goto :goto_0
.end method

.method public setMediaItem(Lcom/facebook/ipc/media/MediaItem;)V
    .locals 8

    .prologue
    .line 358058
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->q:Lcom/facebook/ipc/media/MediaItem;

    .line 358059
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->j:LX/1Ad;

    const/4 v7, 0x0

    .line 358060
    invoke-interface {p0}, LX/21n;->getSelfAsView()Landroid/view/View;

    move-result-object v3

    .line 358061
    const v1, 0x7f0d00a1

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 358062
    if-nez v1, :cond_0

    .line 358063
    const v1, 0x7f0d09d0

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 358064
    :cond_0
    if-nez p1, :cond_2

    .line 358065
    invoke-static {p0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(LX/21n;)V

    .line 358066
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->H:Landroid/view/View;

    if-nez v0, :cond_1

    .line 358067
    const v0, 0x7f0d09d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    .line 358068
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 358069
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->H:Landroid/view/View;

    .line 358070
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->x()V

    .line 358071
    return-void

    .line 358072
    :cond_2
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 358073
    const v1, 0x7f0d09d3

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 358074
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 358075
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 358076
    new-instance v5, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v5

    new-instance v6, LX/1o9;

    invoke-direct {v6, v2, v4}, LX/1o9;-><init>(II)V

    .line 358077
    iput-object v6, v5, LX/1bX;->c:LX/1o9;

    .line 358078
    move-object v2, v5

    .line 358079
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v4

    .line 358080
    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v2, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object v4, Lcom/facebook/feedback/ui/CommentComposerHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 358081
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 358082
    const v1, 0x7f0d09d1

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 358083
    new-instance v2, LX/9CH;

    invoke-direct {v2, p0}, LX/9CH;-><init>(LX/21n;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358084
    const v1, 0x7f0d09d2

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 358085
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v2

    sget-object v3, LX/4gF;->VIDEO:LX/4gF;

    if-ne v2, v3, :cond_3

    .line 358086
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 358087
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public setMediaPickerListener(LX/9DF;)V
    .locals 2

    .prologue
    .line 358056
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->K:Landroid/widget/ImageButton;

    new-instance v1, LX/9Ei;

    invoke-direct {v1, p0, p1}, LX/9Ei;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;LX/9DF;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358057
    return-void
.end method

.method public setNotificationLogObject(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 0

    .prologue
    .line 358054
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->u:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 358055
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 358049
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    if-eqz v0, :cond_0

    .line 358050
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->w:LX/9EI;

    .line 358051
    iput-object p1, v0, LX/9EI;->d:Landroid/view/View$OnFocusChangeListener;

    .line 358052
    invoke-virtual {v0}, LX/9EI;->a()Landroid/widget/EditText;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 358053
    :cond_0
    return-void
.end method

.method public setReshareButtonExperimentClicked(Z)V
    .locals 0

    .prologue
    .line 358047
    iput-boolean p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->aa:Z

    .line 358048
    return-void
.end method

.method public setTransliterationClickListener(LX/9D8;)V
    .locals 2

    .prologue
    .line 358044
    iput-object p1, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->s:LX/9D8;

    .line 358045
    iget-object v0, p0, Lcom/facebook/feedback/ui/SingleLineCommentComposerView;->L:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/9Ef;

    invoke-direct {v1, p0}, LX/9Ef;-><init>(Lcom/facebook/feedback/ui/SingleLineCommentComposerView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358046
    return-void
.end method
