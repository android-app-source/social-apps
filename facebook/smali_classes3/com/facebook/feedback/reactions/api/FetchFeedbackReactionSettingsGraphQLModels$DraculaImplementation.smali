.class public final Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360080
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 360081
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 360078
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 360079
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 360041
    if-nez p1, :cond_0

    .line 360042
    const/4 v0, 0x0

    .line 360043
    :goto_0
    return v0

    .line 360044
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 360045
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 360046
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 360047
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 360048
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 360049
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 360050
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 360051
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 360052
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 360053
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 360054
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 360055
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 360056
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 360057
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 360058
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 360059
    :sswitch_2
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 360060
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 360061
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 360062
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 360063
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 360064
    :sswitch_3
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 360065
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 360066
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 360067
    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 360068
    const/4 v4, 0x3

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 360069
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 360070
    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, LX/15i;->a(III)I

    move-result v7

    .line 360071
    const/4 v4, 0x5

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 360072
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p3, v4, v0, v5}, LX/186;->a(III)V

    .line 360073
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 360074
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 360075
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 360076
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 360077
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6f6e3c36 -> :sswitch_1
        -0x460f5628 -> :sswitch_0
        -0x6cf5571 -> :sswitch_3
        0x6b592dc1 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 360040
    new-instance v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 360036
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 360037
    if-eqz v0, :cond_0

    .line 360038
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360039
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360029
    sparse-switch p2, :sswitch_data_0

    .line 360030
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 360031
    :sswitch_0
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionSettingsQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 360032
    invoke-static {v0, p3}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 360033
    :goto_0
    :sswitch_1
    return-void

    .line 360034
    :sswitch_2
    const-class v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$FetchFeedbackReactionsVectorDataQueryModel$FeedbackReactionSettingsModel$ReactionInfosModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 360035
    invoke-static {v0, p3}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6f6e3c36 -> :sswitch_1
        -0x460f5628 -> :sswitch_0
        -0x6cf5571 -> :sswitch_1
        0x6b592dc1 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 360028
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 360026
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 360027
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 360082
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 360083
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 360084
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 360085
    iput p2, p0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->b:I

    .line 360086
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 360025
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 360024
    new-instance v0, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 360021
    iget v0, p0, LX/1vt;->c:I

    .line 360022
    move v0, v0

    .line 360023
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 360018
    iget v0, p0, LX/1vt;->c:I

    .line 360019
    move v0, v0

    .line 360020
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 360015
    iget v0, p0, LX/1vt;->b:I

    .line 360016
    move v0, v0

    .line 360017
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 360012
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 360013
    move-object v0, v0

    .line 360014
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 360000
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 360001
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 360002
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 360003
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 360004
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 360005
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 360006
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 360007
    invoke-static {v3, v9, v2}, Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedback/reactions/api/FetchFeedbackReactionSettingsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 360008
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 360009
    iget v0, p0, LX/1vt;->c:I

    .line 360010
    move v0, v0

    .line 360011
    return v0
.end method
