.class public Lcom/facebook/auth/credentials/UserTokenCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/auth/credentials/UserTokenCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 342844
    new-instance v0, LX/1tl;

    invoke-direct {v0}, LX/1tl;-><init>()V

    sput-object v0, Lcom/facebook/auth/credentials/UserTokenCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 342860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342861
    iput-object p1, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    .line 342862
    iput-object p2, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    .line 342863
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 342859
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342850
    if-ne p0, p1, :cond_1

    .line 342851
    :cond_0
    :goto_0
    return v0

    .line 342852
    :cond_1
    instance-of v2, p1, Lcom/facebook/auth/credentials/UserTokenCredentials;

    if-nez v2, :cond_2

    move v0, v1

    .line 342853
    goto :goto_0

    .line 342854
    :cond_2
    check-cast p1, Lcom/facebook/auth/credentials/UserTokenCredentials;

    .line 342855
    iget-object v2, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 342856
    goto :goto_0

    .line 342857
    :cond_3
    iget-object v2, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 342858
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 342849
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 342848
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UserTokenCredentials{userId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 342845
    iget-object v0, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 342846
    iget-object v0, p0, Lcom/facebook/auth/credentials/UserTokenCredentials;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 342847
    return-void
.end method
