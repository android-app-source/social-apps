.class public Lcom/facebook/location/foreground/GeoPixelController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field public static final c:Ljava/util/Random;

.field public static final d:Ljava/util/regex/Pattern;

.field public static final e:Ljava/util/regex/Pattern;

.field private static volatile q:Lcom/facebook/location/foreground/GeoPixelController;


# instance fields
.field public final f:LX/0W3;

.field public final g:LX/0Zm;

.field public final h:LX/0Zb;

.field public final i:LX/0ad;

.field public final j:LX/03V;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Mk;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0y3;

.field public final n:LX/0TD;

.field private final o:LX/1rd;

.field public final p:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 345995
    const-class v0, Lcom/facebook/location/foreground/GeoPixelController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->a:Ljava/lang/String;

    .line 345996
    const-class v0, Lcom/facebook/location/foreground/GeoPixelController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 345997
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->c:Ljava/util/Random;

    .line 345998
    const-string v0, "From (.*?)(([\\d]{1,3})\\.([\\d]{1,3})\\.([\\d]{1,3})\\.([\\d]{1,3})).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->d:Ljava/util/regex/Pattern;

    .line 345999
    const-string v0, ".*bytes from.*time=(\\d+\\.?\\d*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(LX/0W3;LX/0Zm;LX/0Zb;LX/0ad;LX/03V;LX/0Ot;LX/0Ot;LX/0y3;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1rd;LX/0TD;)V
    .locals 0
    .param p11    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/analytics/logger/AnalyticsConfig;",
            "LX/0Zb;",
            "LX/0ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/1Mk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;",
            "LX/0y3;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/1rd;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346072
    iput-object p1, p0, Lcom/facebook/location/foreground/GeoPixelController;->f:LX/0W3;

    .line 346073
    iput-object p2, p0, Lcom/facebook/location/foreground/GeoPixelController;->g:LX/0Zm;

    .line 346074
    iput-object p3, p0, Lcom/facebook/location/foreground/GeoPixelController;->h:LX/0Zb;

    .line 346075
    iput-object p4, p0, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    .line 346076
    iput-object p5, p0, Lcom/facebook/location/foreground/GeoPixelController;->j:LX/03V;

    .line 346077
    iput-object p6, p0, Lcom/facebook/location/foreground/GeoPixelController;->l:LX/0Ot;

    .line 346078
    iput-object p7, p0, Lcom/facebook/location/foreground/GeoPixelController;->k:LX/0Ot;

    .line 346079
    iput-object p8, p0, Lcom/facebook/location/foreground/GeoPixelController;->m:LX/0y3;

    .line 346080
    iput-object p11, p0, Lcom/facebook/location/foreground/GeoPixelController;->n:LX/0TD;

    .line 346081
    iput-object p10, p0, Lcom/facebook/location/foreground/GeoPixelController;->o:LX/1rd;

    .line 346082
    iput-object p9, p0, Lcom/facebook/location/foreground/GeoPixelController;->p:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 346083
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/location/foreground/GeoPixelController;
    .locals 15

    .prologue
    .line 346058
    sget-object v0, Lcom/facebook/location/foreground/GeoPixelController;->q:Lcom/facebook/location/foreground/GeoPixelController;

    if-nez v0, :cond_1

    .line 346059
    const-class v1, Lcom/facebook/location/foreground/GeoPixelController;

    monitor-enter v1

    .line 346060
    :try_start_0
    sget-object v0, Lcom/facebook/location/foreground/GeoPixelController;->q:Lcom/facebook/location/foreground/GeoPixelController;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 346061
    if-eqz v2, :cond_0

    .line 346062
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 346063
    new-instance v3, Lcom/facebook/location/foreground/GeoPixelController;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v5

    check-cast v5, LX/0Zm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const/16 v9, 0x28e

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2ca

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v11

    check-cast v11, LX/0y3;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v12

    check-cast v12, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-static {v0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v13

    check-cast v13, LX/1rd;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v14

    check-cast v14, LX/0TD;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/location/foreground/GeoPixelController;-><init>(LX/0W3;LX/0Zm;LX/0Zb;LX/0ad;LX/03V;LX/0Ot;LX/0Ot;LX/0y3;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/1rd;LX/0TD;)V

    .line 346064
    move-object v0, v3

    .line 346065
    sput-object v0, Lcom/facebook/location/foreground/GeoPixelController;->q:Lcom/facebook/location/foreground/GeoPixelController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346066
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 346067
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346068
    :cond_1
    sget-object v0, Lcom/facebook/location/foreground/GeoPixelController;->q:Lcom/facebook/location/foreground/GeoPixelController;

    return-object v0

    .line 346069
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 346070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;IFF)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 346045
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/facebook/location/foreground/GeoPixelController;->a(Ljava/lang/String;Ljava/lang/Integer;IFF)Ljava/util/List;

    move-result-object v0

    .line 346046
    const/4 p0, 0x0

    .line 346047
    const/4 v1, 0x0

    .line 346048
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move p1, p0

    move p0, v1

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 346049
    sget-object p3, Lcom/facebook/location/foreground/GeoPixelController;->e:Ljava/util/regex/Pattern;

    invoke-virtual {p3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 346050
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result p3

    if-lez p3, :cond_2

    .line 346051
    const/4 p3, 0x1

    invoke-virtual {v1, p3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 346052
    if-eqz v1, :cond_2

    .line 346053
    add-int/lit8 p1, p1, 0x1

    .line 346054
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr p0, v1

    move v1, p0

    move p0, p1

    :goto_1
    move p1, p0

    move p0, v1

    .line 346055
    goto :goto_0

    .line 346056
    :cond_0
    if-gtz p1, :cond_1

    const/4 v1, 0x0

    :goto_2
    move-object v0, v1

    .line 346057
    return-object v0

    :cond_1
    int-to-float v1, p1

    div-float v1, p0, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_2

    :cond_2
    move v1, p0

    move p0, p1

    goto :goto_1
.end method

.method public static a(ILjava/lang/String;FF)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346084
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1, p2, p3}, Lcom/facebook/location/foreground/GeoPixelController;->a(Ljava/lang/String;Ljava/lang/Integer;IFF)Ljava/util/List;

    move-result-object v0

    .line 346085
    const/4 p2, 0x2

    .line 346086
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 346087
    sget-object p1, Lcom/facebook/location/foreground/GeoPixelController;->d:Ljava/util/regex/Pattern;

    invoke-virtual {p1, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 346088
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result p1

    if-lt p1, p2, :cond_0

    .line 346089
    invoke-virtual {v1, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 346090
    :goto_0
    move-object v0, v1

    .line 346091
    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Integer;IFF)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "IFF)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346026
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 346027
    const-string v1, "ping"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346028
    const-string v1, "-c"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346029
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346030
    const-string v1, "-w"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346031
    invoke-static {p3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346032
    const-string v1, "-i"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346033
    invoke-static {p4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346034
    if-eqz p1, :cond_0

    .line 346035
    const-string v1, "-t"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346036
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346037
    :cond_0
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346038
    new-instance v1, Ljava/lang/ProcessBuilder;

    invoke-direct {v1, v0}, Ljava/lang/ProcessBuilder;-><init>(Ljava/util/List;)V

    .line 346039
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 346040
    invoke-virtual {v1}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v1

    .line 346041
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 346042
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 346043
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 346044
    :cond_1
    return-object v0
.end method

.method public static synthetic a(IILjava/lang/String;FFLX/0Zb;)Lorg/json/JSONObject;
    .locals 8

    .prologue
    .line 346002
    const/4 v7, 0x0

    .line 346003
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 346004
    const/4 v1, 0x1

    :goto_0
    if-gt v1, p0, :cond_3

    .line 346005
    :try_start_0
    invoke-static {v1, p2, p3, p4}, Lcom/facebook/location/foreground/GeoPixelController;->a(ILjava/lang/String;FF)Ljava/lang/String;

    move-result-object v2

    .line 346006
    if-eqz v2, :cond_1

    .line 346007
    invoke-static {v2, p1, p3, p4}, Lcom/facebook/location/foreground/GeoPixelController;->a(Ljava/lang/String;IFF)Ljava/lang/Float;

    move-result-object v3

    .line 346008
    if-nez v3, :cond_0

    .line 346009
    const-string v4, "geopixel_ping_failed"

    const/4 v5, 0x0

    invoke-interface {p5, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 346010
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 346011
    const-string v5, "ping_failure"

    const-string v6, "ping_address_failure"

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 346012
    const-string v5, "ping_failure_address"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 346013
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 346014
    :cond_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 346015
    const-string v5, "ping_ip"

    invoke-virtual {v4, v5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 346016
    const-string v2, "ping_rtt"

    invoke-virtual {v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 346017
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 346018
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 346019
    :catch_0
    const-string v0, "geopixel_ping_failed"

    invoke-interface {p5, v0, v7}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 346020
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 346021
    const-string v1, "ping_failure"

    const-string v2, "ping_process_failure"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 346022
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 346023
    :cond_2
    const/4 v0, 0x0

    .line 346024
    :cond_3
    move-object v0, v0

    .line 346025
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/location/foreground/GeoPixelController;I)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x7

    .line 346000
    iget-object v0, p0, Lcom/facebook/location/foreground/GeoPixelController;->o:LX/1rd;

    invoke-virtual {v0, p1}, LX/1rd;->h(I)Ljava/lang/String;

    move-result-object v0

    .line 346001
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v2, :cond_0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
