.class public final Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1rW;

.field private b:I


# direct methods
.method public constructor <init>(LX/1rW;)V
    .locals 0

    .prologue
    .line 346098
    iput-object p1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 346099
    :try_start_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->d:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    .line 346100
    if-nez v0, :cond_0

    .line 346101
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sN;

    invoke-virtual {v0}, LX/1sN;->c()V

    .line 346102
    :goto_0
    return-void

    .line 346103
    :cond_0
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->o:Lcom/facebook/location/foreground/GeoPixelController;

    const/4 v3, 0x0

    .line 346104
    iget-object v1, v0, Lcom/facebook/location/foreground/GeoPixelController;->i:LX/0ad;

    sget-short v2, LX/1rm;->n:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/location/foreground/GeoPixelController;->g:LX/0Zm;

    const-string v2, "geopixel_rtt"

    invoke-virtual {v1, v2, v3}, LX/0Zm;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/location/foreground/GeoPixelController;->m:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/facebook/location/foreground/GeoPixelController;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kb;

    invoke-virtual {v1}, LX/0kb;->v()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 346105
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->h:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 346106
    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_2

    .line 346107
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sN;

    invoke-virtual {v0}, LX/1sN;->c()V

    .line 346108
    iget v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->b:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 346109
    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v2, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    invoke-static {v2}, LX/1rW;->m(LX/1rW;)J

    move-result-wide v2

    shl-long/2addr v2, v0

    invoke-static {v1, v2, v3}, LX/1rW;->a$redex0(LX/1rW;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346110
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 346111
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->k:LX/1ra;

    const-wide/high16 v4, -0x8000000000000000L

    .line 346112
    iput-wide v4, v0, LX/1ra;->g:J

    .line 346113
    iput-wide v4, v0, LX/1ra;->f:J

    .line 346114
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v0, v0, LX/1rW;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/1rW;->a:Ljava/lang/String;

    const-string v3, "Something is wrong when requesting location"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 346115
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->b:I

    .line 346116
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    iget-object v1, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    invoke-static {v1}, LX/1rW;->m(LX/1rW;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/1rW;->a$redex0(LX/1rW;J)V

    .line 346117
    iget-object v0, p0, Lcom/facebook/location/foreground/ForegroundLocationFrameworkController$3;->a:LX/1rW;

    invoke-static {v0}, LX/1rW;->h$redex0(LX/1rW;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 346118
    :cond_3
    iget-object v1, v0, Lcom/facebook/location/foreground/GeoPixelController;->n:LX/0TD;

    new-instance v2, LX/JcP;

    invoke-direct {v2, v0}, LX/JcP;-><init>(Lcom/facebook/location/foreground/GeoPixelController;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 346119
    new-instance v2, LX/JcQ;

    invoke-direct {v2, v0}, LX/JcQ;-><init>(Lcom/facebook/location/foreground/GeoPixelController;)V

    iget-object v3, v0, Lcom/facebook/location/foreground/GeoPixelController;->n:LX/0TD;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
