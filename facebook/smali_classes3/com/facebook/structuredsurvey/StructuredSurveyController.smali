.class public Lcom/facebook/structuredsurvey/StructuredSurveyController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:Ljava/lang/String;

.field public static final d:LX/0Tn;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;"
        }
    .end annotation
.end field

.field public C:Landroid/content/Context;

.field public D:Landroid/content/res/Resources;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7FG;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field public final j:LX/03V;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/surveysession/listeners/SurveyImpressionListener;",
            ">;>;"
        }
    .end annotation
.end field

.field public final m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final n:LX/0SG;

.field public o:LX/31O;

.field public p:LX/7EJ;

.field public q:LX/7ER;

.field public r:LX/1x4;

.field public s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field public w:Z

.field public x:Z

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 347721
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NaRF:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    .line 347722
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/last_invitation_impression_ts"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->d:LX/0Tn;

    .line 347723
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/intern_dev_mode_recent_survey_ ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a:LX/0Tn;

    .line 347724
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "structured_survey/intern_dev_mode_recent_integration_point_ids"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Landroid/content/res/Resources;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 2
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7FG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/surveysession/listeners/SurveyImpressionListener;",
            ">;>;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 347613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347614
    iput-object p1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    .line 347615
    iput-object p2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->e:LX/0Ot;

    .line 347616
    iput-object p3, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->f:LX/0Ot;

    .line 347617
    iput-object p4, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->g:LX/0Ot;

    .line 347618
    iput-object p5, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->k:LX/0Or;

    .line 347619
    iput-object p6, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->l:LX/0Ot;

    .line 347620
    iput-object p7, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->h:Ljava/util/concurrent/ExecutorService;

    .line 347621
    iput-object p8, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->i:Ljava/util/concurrent/ExecutorService;

    .line 347622
    iput-object p9, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    .line 347623
    iput-object p10, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 347624
    iput-object p11, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->n:LX/0SG;

    .line 347625
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    .line 347626
    iput-boolean v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    .line 347627
    iput-boolean v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->x:Z

    .line 347628
    return-void
.end method

.method public static a(Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/util/List;)LX/7EX;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;)",
            "LX/7EX;"
        }
    .end annotation

    .prologue
    .line 347718
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 347719
    new-instance v0, LX/7EX;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->C:Landroid/content/Context;

    check-cast p1, Ljava/util/ArrayList;

    invoke-direct {v0, v1, p1}, LX/7EX;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 347720
    return-object v0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 347714
    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FG;

    .line 347715
    iget-object p0, v0, LX/7FG;->g:LX/7FH;

    move-object v0, p0

    .line 347716
    invoke-virtual {v1, v0}, LX/7EJ;->a(LX/7FH;)V

    .line 347717
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;LX/1x4;)Lcom/facebook/structuredsurvey/StructuredSurveyController;
    .locals 0

    .prologue
    .line 347710
    iput-object p2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->v:Ljava/lang/String;

    .line 347711
    iput-object p1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->C:Landroid/content/Context;

    .line 347712
    iput-object p3, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347713
    return-object p0
.end method

.method public final a(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;)Lcom/facebook/structuredsurvey/StructuredSurveyController;
    .locals 2

    .prologue
    .line 347725
    iput-object p1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 347726
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel$SurveyModel;

    move-result-object v0

    .line 347727
    const-string v1, "NULL IntegrationPoint SurveyModel"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347728
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel$SurveyModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    .line 347729
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->u:Ljava/lang/String;

    .line 347730
    return-object p0
.end method

.method public final a(LX/7FJ;Ljava/util/Map;)V
    .locals 10
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7FJ;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 347692
    sget-object v0, LX/7FJ;->INVITATION_IMPRESSION:LX/7FJ;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/7FJ;->IMPRESSION:LX/7FJ;

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    if-eqz v0, :cond_2

    .line 347693
    :cond_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Fv;

    .line 347694
    iget-object v2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->v:Ljava/lang/String;

    .line 347695
    sget-object v3, LX/7Fv;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 347696
    goto :goto_0

    .line 347697
    :cond_1
    iget-object v6, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, Lcom/facebook/structuredsurvey/StructuredSurveyController;->d:LX/0Tn;

    iget-object v8, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->n:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-interface {v6, v7, v8, v9}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 347698
    :cond_2
    if-nez p2, :cond_3

    .line 347699
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 347700
    :cond_3
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    if-eqz v0, :cond_4

    .line 347701
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347702
    iget-object v1, v0, LX/1x4;->d:LX/0P1;

    move-object v0, v1

    .line 347703
    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 347704
    :cond_4
    new-instance v0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->u:Ljava/lang/String;

    invoke-virtual {p1}, LX/7FJ;->getImpressionEvent()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;)V

    .line 347705
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 347706
    const-string v1, "postSurveyImpressionsParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 347707
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "post_survey_impressions"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x40438ab9

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 347708
    new-instance v1, LX/7EK;

    invoke-direct {v1, p0, p1}, LX/7EK;-><init>(Lcom/facebook/structuredsurvey/StructuredSurveyController;LX/7FJ;)V

    iget-object v2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 347709
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 347676
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 347677
    const/4 v0, 0x0

    .line 347678
    :try_start_0
    const-string v2, "[]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 347679
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 347680
    const-string v1, "notif_graphql_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 347681
    :goto_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347682
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a(Ljava/lang/Runnable;Z)V

    .line 347683
    :goto_1
    return-void

    .line 347684
    :cond_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7FG;

    .line 347685
    new-instance v2, LX/7En;

    invoke-direct {v2}, LX/7En;-><init>()V

    move-object v2, v2

    .line 347686
    const-string v3, "node_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/7En;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 347687
    iget-object v2, v0, LX/7FG;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 347688
    new-instance v3, LX/7FF;

    invoke-direct {v3, v0, p1}, LX/7FF;-><init>(LX/7FG;Ljava/lang/Runnable;)V

    iget-object v4, v0, LX/7FG;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347689
    goto :goto_1

    .line 347690
    :catch_0
    move-exception v0

    .line 347691
    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->j:LX/03V;

    sget-object v2, Lcom/facebook/structuredsurvey/StructuredSurveyController;->c:Ljava/lang/String;

    const-string v3, "NaRF:Survey Render Failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;Z)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 347645
    new-instance v0, LX/7ER;

    invoke-direct {v0}, LX/7ER;-><init>()V

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 347646
    new-instance v0, LX/31O;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel$SurveyModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel$SurveyModel;->k()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    invoke-direct {v0, v1, v2}, LX/31O;-><init>(Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyFlowFragmentModel;LX/7ER;)V

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    .line 347647
    new-instance v0, LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    invoke-direct {v0, v1, v2}, LX/7EJ;-><init>(Landroid/content/res/Resources;LX/1x4;)V

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    .line 347648
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    .line 347649
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    if-nez v0, :cond_1

    .line 347650
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 347651
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const v2, 0x7f081a88

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7EJ;->a(Ljava/lang/String;)V

    .line 347652
    :goto_1
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const v2, 0x7f081a82

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7EJ;->b(Ljava/lang/String;)V

    .line 347653
    :cond_1
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347654
    iget-boolean v1, v0, LX/1x4;->h:Z

    move v0, v1

    .line 347655
    if-nez v0, :cond_2

    .line 347656
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 347657
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->D:Landroid/content/res/Resources;

    const v2, 0x7f081a83

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7EJ;->c(Ljava/lang/String;)V

    .line 347658
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    invoke-virtual {v1}, LX/31O;->b()I

    move-result v1

    .line 347659
    iput v1, v0, LX/7EJ;->d:I

    .line 347660
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->x:Z

    .line 347661
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->y:Ljava/lang/String;

    .line 347662
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->z:Ljava/lang/String;

    .line 347663
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->r()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->A:Ljava/lang/String;

    .line 347664
    if-eqz p2, :cond_3

    .line 347665
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p()V

    .line 347666
    :cond_3
    if-eqz p1, :cond_4

    .line 347667
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347668
    iget-boolean v1, v0, LX/1x4;->c:Z

    move v0, v1

    .line 347669
    if-eqz v0, :cond_5

    .line 347670
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->i:Ljava/util/concurrent/ExecutorService;

    const v1, 0x1b92cf45

    invoke-static {v0, p1, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 347671
    :cond_4
    :goto_3
    return-void

    .line 347672
    :cond_5
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_3

    .line 347673
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 347674
    :cond_7
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7EJ;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 347675
    :cond_8
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    iget-object v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->s:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;->a()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConfigFragmentModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7EJ;->c(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 347641
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "survey_render"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 347642
    const-string v0, "client_time"

    invoke-virtual {v1, v0, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "render_state"

    invoke-virtual {v0, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "integration_point_id"

    iget-object v3, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->v:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "survey_id"

    iget-object v3, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 347643
    iget-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 347644
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 347629
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->t:Ljava/lang/String;

    .line 347630
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->u:Ljava/lang/String;

    .line 347631
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->q:LX/7ER;

    .line 347632
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->o:LX/31O;

    .line 347633
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->p:LX/7EJ;

    .line 347634
    iput-boolean v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->w:Z

    .line 347635
    iput-boolean v1, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->x:Z

    .line 347636
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->v:Ljava/lang/String;

    .line 347637
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->B:Ljava/util/List;

    .line 347638
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->C:Landroid/content/Context;

    .line 347639
    iput-object v0, p0, Lcom/facebook/structuredsurvey/StructuredSurveyController;->r:LX/1x4;

    .line 347640
    return-void
.end method
