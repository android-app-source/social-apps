.class public Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1dh;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "LX/22C",
        "<TV;>;",
        "LX/1Pi;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357498
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 357499
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
    .locals 3

    .prologue
    .line 357500
    const-class v1, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    monitor-enter v1

    .line 357501
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357502
    sput-object v2, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 357505
    new-instance v0, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;-><init>()V

    .line 357506
    move-object v0, v0

    .line 357507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 357511
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    .line 357512
    new-instance v0, LX/22C;

    invoke-direct {v0, p2}, LX/22C;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7322370c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 357513
    check-cast p2, LX/22C;

    check-cast p3, LX/1Pi;

    .line 357514
    iput-object p4, p2, LX/22C;->b:Landroid/view/View;

    .line 357515
    invoke-interface {p3, p2}, LX/1Pi;->a(LX/22C;)V

    .line 357516
    const/16 v1, 0x1f

    const v2, 0x2c591f10

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 357517
    check-cast p2, LX/22C;

    check-cast p3, LX/1Pi;

    .line 357518
    invoke-interface {p3, p2}, LX/1Pi;->b(LX/22C;)V

    .line 357519
    const/4 v0, 0x0

    .line 357520
    iput-object v0, p2, LX/22C;->b:Landroid/view/View;

    .line 357521
    return-void
.end method
