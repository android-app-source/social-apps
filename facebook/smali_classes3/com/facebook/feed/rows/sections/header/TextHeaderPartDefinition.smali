.class public Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2ef;",
        ":",
        "LX/24a;",
        ":",
        "LX/2eg;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357546
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    sput-object v0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357522
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 357523
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 357524
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    .line 357525
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;
    .locals 5

    .prologue
    .line 357535
    const-class v1, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    monitor-enter v1

    .line 357536
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357537
    sput-object v2, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357538
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357539
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357540
    new-instance p0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;)V

    .line 357541
    move-object v0, p0

    .line 357542
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357543
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357544
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357545
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 357532
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357533
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 357534
    invoke-static {p0}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/16y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ae()Lcom/facebook/graphql/model/GraphQLHotConversationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 357528
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357529
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 357530
    const v0, 0x7f0d2ec4

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    new-instance v2, LX/Bs9;

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v2, p2, v3, v4}, LX/Bs9;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZI)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 357531
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x70897944

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 357526
    check-cast p4, LX/2eg;

    sget-object v1, LX/2ei;->STORY_HEADER:LX/2ei;

    invoke-interface {p4, v1}, LX/2eg;->setStyle(LX/2ei;)V

    .line 357527
    const/16 v1, 0x1f

    const v2, -0x63101f91

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
