.class public Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/1wB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1wB",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field private final g:LX/1VH;

.field private final h:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357497
    new-instance v0, LX/1wA;

    invoke-direct {v0}, LX/1wA;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1wB;LX/1V0;LX/1VH;Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357491
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 357492
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->e:LX/1wB;

    .line 357493
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->f:LX/1V0;

    .line 357494
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->g:LX/1VH;

    .line 357495
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->h:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    .line 357496
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pi;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 357487
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->e:LX/1wB;

    invoke-virtual {v0, p1}, LX/1wB;->c(LX/1De;)LX/22A;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/22A;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22A;

    move-result-object v1

    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-virtual {v1, v0}, LX/22A;->a(LX/1Pq;)LX/22A;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 357488
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 357489
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 357490
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->g:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;
    .locals 9

    .prologue
    .line 357476
    const-class v1, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    monitor-enter v1

    .line 357477
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357478
    sput-object v2, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357479
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357480
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357481
    new-instance v3, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1wB;->a(LX/0QB;)LX/1wB;

    move-result-object v5

    check-cast v5, LX/1wB;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v7

    check-cast v7, LX/1VH;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1wB;LX/1V0;LX/1VH;Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;)V

    .line 357482
    move-object v0, v3

    .line 357483
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357484
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357485
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 357463
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pi;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pi;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 357475
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pi;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pi;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 357469
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357470
    invoke-super {p0, p1, p2}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1aD;Ljava/lang/Object;)V

    .line 357471
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->h:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    .line 357472
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 357473
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 357474
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357467
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357468
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 357465
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357466
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 357464
    sget-object v0, Lcom/facebook/feed/rows/sections/header/FeedTextHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
