.class public Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static u:LX/0Xm;


# instance fields
.field private final a:LX/1x9;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14w;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/LimitedAttachedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelHiddenFooterPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentsPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/momentsupsell/MomentsUpsellPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343446
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 343447
    new-instance v1, LX/1x9;

    invoke-direct {v1, p1}, LX/1x9;-><init>(LX/14w;)V

    iput-object v1, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a:LX/1x9;

    .line 343448
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->b:LX/0Ot;

    .line 343449
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->c:LX/0Ot;

    .line 343450
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->d:LX/0Ot;

    .line 343451
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->e:LX/0Ot;

    .line 343452
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->f:LX/0Ot;

    .line 343453
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->g:LX/0Ot;

    .line 343454
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->h:LX/0Ot;

    .line 343455
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->i:LX/0Ot;

    .line 343456
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->j:LX/0Ot;

    .line 343457
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->k:LX/0Ot;

    .line 343458
    iput-object p12, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->l:LX/0Ot;

    .line 343459
    iput-object p13, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->m:LX/0Ot;

    .line 343460
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->n:LX/0Ot;

    .line 343461
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->o:LX/0Ot;

    .line 343462
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->p:LX/0Ot;

    .line 343463
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->q:LX/0Ot;

    .line 343464
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->r:LX/0Ot;

    .line 343465
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->s:LX/0Ot;

    .line 343466
    invoke-static/range {p20 .. p20}, LX/1xA;->a(LX/0Ot;)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->t:LX/0Ot;

    .line 343467
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;
    .locals 3

    .prologue
    .line 343408
    const-class v1, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    monitor-enter v1

    .line 343409
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->u:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343410
    sput-object v2, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->u:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343411
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343412
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;
    .locals 23

    .prologue
    .line 343444
    new-instance v2, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    const/16 v4, 0x6e5

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x720

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x704

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x926

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x92d

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x929

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x7d9

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x7e5

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x6de

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x6ce

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x8eb

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x8f5

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x8f7

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x909

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x90d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x979

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x9be

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x8c0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x74e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;-><init>(LX/14w;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 343445
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 343418
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    .line 343419
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343420
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 343421
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 343422
    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 343423
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v2}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343424
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343425
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343426
    :goto_0
    return-object v4

    .line 343427
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343428
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343429
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343430
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343431
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->t:LX/0Ot;

    invoke-static {p1, v0, v2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->f:LX/0Ot;

    invoke-virtual {v0, v3, v2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 343432
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343433
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343434
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343435
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343436
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343437
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343438
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v3, LX/1Qt;->FEED:LX/1Qt;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->n:LX/0Ot;

    invoke-static {p1, v0, v3, v2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->m:LX/0Ot;

    invoke-virtual {v0, v3, v2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 343439
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343440
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343441
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 343442
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 343443
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 343416
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343417
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a:LX/1x9;

    invoke-virtual {v0, p1}, LX/1x9;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
