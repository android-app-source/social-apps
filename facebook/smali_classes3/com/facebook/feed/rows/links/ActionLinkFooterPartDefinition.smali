.class public Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/Bos;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/1xP;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343885
    const v0, 0x7f030029

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->a:LX/1Cz;

    .line 343886
    sget-object v0, LX/1Ua;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1xP;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343887
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 343888
    iput-object p1, p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 343889
    iput-object p2, p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->d:LX/1xP;

    .line 343890
    iput-object p3, p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->e:Landroid/content/res/Resources;

    .line 343891
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;
    .locals 6

    .prologue
    .line 343892
    const-class v1, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    monitor-enter v1

    .line 343893
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343894
    sput-object v2, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343895
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343896
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343897
    new-instance p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v4

    check-cast v4, LX/1xP;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1xP;Landroid/content/res/Resources;)V

    .line 343898
    move-object v0, p0

    .line 343899
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343900
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343901
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343902
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 343903
    sget-object v0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 343904
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 343905
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343906
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 343907
    iget-object v1, p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->b:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 343908
    const v1, -0x11f22ab2

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 343909
    if-nez v1, :cond_0

    .line 343910
    new-instance v0, LX/Bos;

    const/4 v1, 0x0

    invoke-direct {v0, v4, v4, v1}, LX/Bos;-><init>(Landroid/view/View$OnClickListener;Ljava/lang/String;Z)V

    .line 343911
    :goto_0
    return-object v0

    .line 343912
    :cond_0
    sget-object v0, LX/0ax;->fz:Ljava/lang/String;

    const-string v2, "shared_feed_story"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 343913
    new-instance v2, LX/Bor;

    invoke-direct {v2, p0, v0}, LX/Bor;-><init>(Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;Ljava/lang/String;)V

    .line 343914
    new-instance v0, LX/Bos;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3}, LX/Bos;-><init>(Landroid/view/View$OnClickListener;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x63cba5eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 343915
    check-cast p2, LX/Bos;

    .line 343916
    iget-object v1, p2, LX/Bos;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343917
    const v1, 0x7f0d02a7

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 343918
    iget-object v2, p2, LX/Bos;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343919
    iget-boolean v2, p2, LX/Bos;->c:Z

    if-eqz v2, :cond_0

    .line 343920
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result p1

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result p3

    invoke-virtual {v1, v2, v4, p1, p3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 343921
    :cond_0
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->e:Landroid/content/res/Resources;

    const p1, 0x7f0b004e

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 343922
    const/16 v1, 0x1f

    const v2, 0x1b040fed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 343923
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343924
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343925
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 343926
    invoke-static {p1}, LX/182;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, -0x11f22ab2

    invoke-static {v0, v1}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 343927
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343928
    return-void
.end method
