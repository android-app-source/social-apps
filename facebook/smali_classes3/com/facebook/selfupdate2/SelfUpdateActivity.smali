.class public Lcom/facebook/selfupdate2/SelfUpdateActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:Lcom/facebook/appupdate/ReleaseInfo;

.field public B:LX/EeS;

.field private final C:LX/1sX;

.field public p:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1wh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Landroid/net/ConnectivityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/1wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/1wt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 346787
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 346788
    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->A:Lcom/facebook/appupdate/ReleaseInfo;

    .line 346789
    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    .line 346790
    new-instance v0, LX/Hbw;

    invoke-direct {v0, p0}, LX/Hbw;-><init>(Lcom/facebook/selfupdate2/SelfUpdateActivity;)V

    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->C:LX/1sX;

    return-void
.end method

.method private static a(Lcom/facebook/selfupdate2/SelfUpdateActivity;Landroid/os/Handler;LX/1wh;LX/1wo;Landroid/net/ConnectivityManager;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/1wr;LX/1wt;)V
    .locals 0

    .prologue
    .line 346811
    iput-object p1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->p:Landroid/os/Handler;

    iput-object p2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->q:LX/1wh;

    iput-object p3, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->r:LX/1wo;

    iput-object p4, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->s:Landroid/net/ConnectivityManager;

    iput-object p5, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->t:LX/03V;

    iput-object p6, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p7, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->v:LX/0Uh;

    iput-object p8, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->w:Lcom/facebook/content/SecureContextHelper;

    iput-object p9, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->x:LX/1wr;

    iput-object p10, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->y:LX/1wt;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-static {v10}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {v10}, LX/GTC;->a(LX/0QB;)LX/1wh;

    move-result-object v2

    check-cast v2, LX/1wh;

    invoke-static {v10}, LX/1wd;->a(LX/0QB;)LX/1wo;

    move-result-object v3

    check-cast v3, LX/1wo;

    invoke-static {v10}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v10}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/1wr;->b(LX/0QB;)LX/1wr;

    move-result-object v9

    check-cast v9, LX/1wr;

    invoke-static {v10}, LX/1wt;->b(LX/0QB;)LX/1wt;

    move-result-object v10

    check-cast v10, LX/1wt;

    invoke-static/range {v0 .. v10}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->a(Lcom/facebook/selfupdate2/SelfUpdateActivity;Landroid/os/Handler;LX/1wh;LX/1wo;Landroid/net/ConnectivityManager;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/1wr;LX/1wt;)V

    return-void
.end method

.method public static b(Lcom/facebook/selfupdate2/SelfUpdateActivity;LX/EeX;)V
    .locals 2

    .prologue
    .line 346805
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346806
    invoke-virtual {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->a(LX/EeX;)V

    .line 346807
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->y:LX/1wt;

    invoke-virtual {v0}, LX/1wt;->g()V

    .line 346808
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    .line 346809
    :goto_0
    return-void

    .line 346810
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->c(LX/EeX;)V

    goto :goto_0
.end method

.method private c(LX/EeX;)V
    .locals 3
    .param p1    # LX/EeX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346791
    if-nez p1, :cond_0

    .line 346792
    const-class v0, Lcom/facebook/selfupdate2/DownloadPromptFragment;

    .line 346793
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->c(Ljava/lang/Class;)V

    .line 346794
    :goto_1
    return-void

    .line 346795
    :cond_0
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 346796
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected AppUpdateState state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346797
    :pswitch_1
    const-class v0, Lcom/facebook/selfupdate2/DownloadPromptFragment;

    goto :goto_0

    .line 346798
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->d(LX/EeX;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 346799
    const-class v0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;

    goto :goto_0

    .line 346800
    :cond_1
    const-class v0, Lcom/facebook/selfupdate2/DownloadProgressFragment;

    goto :goto_0

    .line 346801
    :pswitch_3
    const-class v0, Lcom/facebook/selfupdate2/DownloadProgressFragment;

    goto :goto_0

    .line 346802
    :pswitch_4
    const-class v0, Lcom/facebook/selfupdate2/InstallPromptFragment;

    goto :goto_0

    .line 346803
    :pswitch_5
    const-class v0, Lcom/facebook/selfupdate2/DownloadFailedFragment;

    goto :goto_0

    .line 346804
    :pswitch_6
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private c(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/selfupdate2/SelfUpdateFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 346781
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 346782
    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/selfupdate2/SelfUpdateFragment;

    .line 346783
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346784
    invoke-static {p1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->d(Ljava/lang/Class;)Lcom/facebook/selfupdate2/SelfUpdateFragment;

    move-result-object v0

    .line 346785
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 346786
    :cond_0
    return-void
.end method

.method private static d(Ljava/lang/Class;)Lcom/facebook/selfupdate2/SelfUpdateFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/selfupdate2/SelfUpdateFragment;",
            ">;)",
            "Lcom/facebook/selfupdate2/SelfUpdateFragment;"
        }
    .end annotation

    .prologue
    .line 346776
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p0, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/selfupdate2/SelfUpdateFragment;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 346777
    return-object v0

    .line 346778
    :catch_0
    move-exception v0

    .line 346779
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not instantiate fragement: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 346780
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private d(LX/EeX;)Z
    .locals 4

    .prologue
    .line 346812
    iget-boolean v0, p1, LX/EeX;->isWifiOnly:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p1, LX/EeX;->downloadProgress:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(LX/EeX;)V
    .locals 3

    .prologue
    .line 346718
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346719
    :goto_0
    return-void

    .line 346720
    :cond_0
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3fU;->k:LX/0Tn;

    iget-object v2, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {v2}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    sget-object v2, LX/3fU;->l:LX/0Tn;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v0

    invoke-virtual {v0}, LX/Eeh;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private o()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 346721
    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->s:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 346722
    if-eqz v2, :cond_1

    .line 346723
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 346724
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 346725
    goto :goto_0

    :cond_1
    move v0, v1

    .line 346726
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/EeX;)V
    .locals 6

    .prologue
    .line 346727
    invoke-direct {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->e(LX/EeX;)V

    .line 346728
    iget-object v0, p1, LX/EeX;->localFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, LX/1sY;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 346729
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->q:LX/1wh;

    const-string v2, "appupdate_install_start"

    invoke-virtual {p1}, LX/EeX;->c()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 346730
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->q:LX/1wh;

    const-string v2, "appupdate_install_start"

    iget-object v3, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v4

    const-string v5, "task_start"

    invoke-virtual {v1, v2, v3, v4, v5}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 346731
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->w:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 346732
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 346757
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 346758
    invoke-static {p0, p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 346759
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "use_release_info"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 346760
    if-eqz v0, :cond_0

    .line 346761
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->u:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3fU;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346762
    :try_start_0
    new-instance v0, Lcom/facebook/appupdate/ReleaseInfo;

    invoke-direct {v0, v1}, Lcom/facebook/appupdate/ReleaseInfo;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->A:Lcom/facebook/appupdate/ReleaseInfo;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346763
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->z:Z

    .line 346764
    :goto_0
    const v0, 0x7f030bf3

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->setContentView(I)V

    .line 346765
    :goto_1
    return-void

    .line 346766
    :catch_0
    move-exception v0

    .line 346767
    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->t:LX/03V;

    const-class v3, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not parse ReleaseInfo from: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 346768
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    goto :goto_1

    .line 346769
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "operation_uuid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 346770
    if-eqz v0, :cond_1

    .line 346771
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->r:LX/1wo;

    invoke-virtual {v1, v0}, LX/1wo;->a(Ljava/lang/String;)LX/EeS;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    .line 346772
    :cond_1
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    if-nez v1, :cond_2

    .line 346773
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->t:LX/03V;

    const-class v2, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Activity started without an operation (uuid = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 346774
    invoke-virtual {p0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    goto :goto_1

    .line 346775
    :cond_2
    iput-boolean v2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->z:Z

    goto :goto_0
.end method

.method public final l()LX/EeS;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 346733
    iget-boolean v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->z:Z

    move v0, v0

    .line 346734
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 346735
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->r:LX/1wo;

    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->A:Lcom/facebook/appupdate/ReleaseInfo;

    iget-object v3, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->v:LX/0Uh;

    const/16 v5, 0x491

    invoke-virtual {v3, v5, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    const/4 v6, 0x0

    move v5, v4

    invoke-virtual/range {v0 .. v6}, LX/1wo;->a(Lcom/facebook/appupdate/ReleaseInfo;ZZZZLjava/util/Map;)LX/EeS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    .line 346736
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->C:LX/1sX;

    invoke-virtual {v0, v1}, LX/EeS;->a(LX/1sX;)Z

    .line 346737
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    .line 346738
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/selfupdate2/SelfUpdateFragment;

    .line 346739
    instance-of v1, v0, Lcom/facebook/selfupdate2/DownloadPromptFragment;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/facebook/selfupdate2/InstallPromptFragment;

    if-nez v1, :cond_1

    .line 346740
    instance-of v1, v0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;

    if-nez v1, :cond_0

    instance-of v1, v0, Lcom/facebook/selfupdate2/DownloadProgressFragment;

    if-eqz v1, :cond_2

    .line 346741
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 346742
    :cond_1
    :goto_0
    return-void

    .line 346743
    :cond_2
    instance-of v1, v0, Lcom/facebook/selfupdate2/DownloadFailedFragment;

    if-eqz v1, :cond_3

    .line 346744
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->x:LX/1wr;

    invoke-virtual {v0}, LX/1wr;->a()V

    .line 346745
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 346746
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Current fragment class is not handled: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x41830622

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 346747
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 346748
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    if-eqz v1, :cond_0

    .line 346749
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->C:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->b(LX/1sX;)Z

    .line 346750
    :cond_0
    const/16 v1, 0x23

    const v2, -0x6162ddd5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x37b42727

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 346751
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 346752
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    if-eqz v1, :cond_0

    .line 346753
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    invoke-virtual {v1}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->c(LX/EeX;)V

    .line 346754
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->C:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->a(LX/1sX;)Z

    .line 346755
    :goto_0
    const v1, 0x74d3557a

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 346756
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->c(LX/EeX;)V

    goto :goto_0
.end method
