.class public Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1rj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;


# instance fields
.field private final a:LX/1rk;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0SG;

.field private final d:LX/1rU;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/1s5;

.field private final g:LX/0xW;


# direct methods
.method public constructor <init>(LX/1rk;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/1rU;LX/0TD;LX/1s5;LX/0xW;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346214
    iput-object p1, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a:LX/1rk;

    .line 346215
    iput-object p2, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 346216
    iput-object p3, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->c:LX/0SG;

    .line 346217
    iput-object p4, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->d:LX/1rU;

    .line 346218
    iput-object p5, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->e:Ljava/util/concurrent/ExecutorService;

    .line 346219
    iput-object p6, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->f:LX/1s5;

    .line 346220
    iput-object p7, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->g:LX/0xW;

    .line 346221
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;
    .locals 11

    .prologue
    .line 346200
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->h:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    if-nez v0, :cond_1

    .line 346201
    const-class v1, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    monitor-enter v1

    .line 346202
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->h:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 346203
    if-eqz v2, :cond_0

    .line 346204
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 346205
    new-instance v3, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    invoke-static {v0}, LX/1rk;->a(LX/0QB;)LX/1rk;

    move-result-object v4

    check-cast v4, LX/1rk;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v7

    check-cast v7, LX/1rU;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {v0}, LX/1s5;->a(LX/0QB;)LX/1s5;

    move-result-object v9

    check-cast v9, LX/1s5;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v10

    check-cast v10, LX/0xW;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;-><init>(LX/1rk;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/1rU;LX/0TD;LX/1s5;LX/0xW;)V

    .line 346206
    move-object v0, v3

    .line 346207
    sput-object v0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->h:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346208
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 346209
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346210
    :cond_1
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->h:Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;

    return-object v0

    .line 346211
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 346212
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 14

    .prologue
    .line 346195
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2uc;->b:LX/0Tn;

    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 346196
    iget-object v4, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 346197
    iget-object v10, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/0hM;->C:LX/0Tn;

    const-wide/16 v12, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    move-wide v6, v10

    .line 346198
    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->f(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    sub-long/2addr v4, v6

    move-wide v2, v4

    .line 346199
    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2ub;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346185
    sget-object v0, LX/2uc;->c:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DO;

    .line 346186
    sget-object v1, LX/2ub;->MQTT_NEW:LX/2ub;

    if-eq p2, v1, :cond_0

    sget-object v1, LX/2ub;->MQTT_FULL:LX/2ub;

    if-ne p2, v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 346187
    sget-object v0, LX/2uc;->d:Lcom/google/common/util/concurrent/SettableFuture;

    .line 346188
    :goto_0
    return-object v0

    .line 346189
    :cond_1
    sget-object v1, LX/3DO;->FULL:LX/3DO;

    if-ne v0, v1, :cond_2

    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-eq p2, v1, :cond_2

    invoke-direct {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 346190
    sget-object v0, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    .line 346191
    invoke-virtual {p0, p1}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 346192
    :cond_2
    sget-object v1, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne p2, v1, :cond_3

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->d(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 346193
    sget-object v0, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    move-object v2, v0

    .line 346194
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a:LX/1rk;

    const/4 v4, 0x0

    const/16 v5, 0xa

    invoke-direct {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->g()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v8}, LX/1rk;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/3DO;LX/2ub;Lcom/facebook/common/callercontext/CallerContext;ILjava/lang/String;Ljava/util/List;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v2, v0

    goto :goto_1
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 346184
    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->d(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lcom/facebook/auth/viewercontext/ViewerContext;)Z
    .locals 4

    .prologue
    .line 346181
    sget-object v0, LX/1rk;->a:Ljava/util/concurrent/ConcurrentMap;

    move-object v0, v0

    .line 346182
    iget-object v1, p0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 346183
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static d(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J
    .locals 4

    .prologue
    .line 346222
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->f(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static f(Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;)J
    .locals 4

    .prologue
    .line 346180
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->D:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private g()Ljava/lang/String;
    .locals 9

    .prologue
    .line 346163
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->f:LX/1s5;

    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->g:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ANDROID_NOTIFICATIONS_FRIENDING"

    .line 346164
    :goto_0
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 346165
    const-string v3, "unit_styles"

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1s5;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 346166
    const-string v3, "action_styles"

    iget-object v4, v1, LX/1s5;->a:LX/1s6;

    invoke-virtual {v4, v0}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1s5;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 346167
    const-string v3, "component_styles"

    iget-object v4, v1, LX/1s5;->b:LX/1s9;

    .line 346168
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 346169
    iget-object v5, v4, LX/1s9;->b:LX/1sD;

    invoke-interface {v5}, LX/1sD;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 346170
    iget-object v5, v4, LX/1s9;->a:LX/1sB;

    invoke-interface {v5}, LX/1sB;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p0

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, p0, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 346171
    iget-object v1, v4, LX/1s9;->b:LX/1sD;

    invoke-interface {v1}, LX/1sD;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346172
    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 346173
    :cond_0
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 346174
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v4, v5

    .line 346175
    invoke-static {v2, v3, v4}, LX/1s5;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 346176
    const-string v3, "surface"

    invoke-virtual {v2, v3, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 346177
    const-string v3, "request_type"

    const-string v4, "normal"

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 346178
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 346179
    return-object v0

    :cond_2
    const-string v0, "ANDROID_NOTIFICATIONS"

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2ub;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346162
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2ub;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346161
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 346135
    new-instance v0, LX/3DL;

    invoke-direct {v0}, LX/3DL;-><init>()V

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 346136
    iput-object v1, v0, LX/3DL;->a:LX/0rS;

    .line 346137
    move-object v0, v0

    .line 346138
    iput-object p1, v0, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 346139
    move-object v0, v0

    .line 346140
    const/16 v1, 0xa

    .line 346141
    iput v1, v0, LX/3DL;->b:I

    .line 346142
    move-object v0, v0

    .line 346143
    iput-object p2, v0, LX/3DL;->d:Ljava/lang/String;

    .line 346144
    move-object v0, v0

    .line 346145
    iput-boolean v2, v0, LX/3DL;->i:Z

    .line 346146
    move-object v0, v0

    .line 346147
    sget-object v1, LX/2ub;->SCROLL:LX/2ub;

    invoke-virtual {v1}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v1

    .line 346148
    iput-object v1, v0, LX/3DL;->g:Ljava/lang/String;

    .line 346149
    move-object v0, v0

    .line 346150
    iput p4, v0, LX/3DL;->k:I

    .line 346151
    move-object v0, v0

    .line 346152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 346153
    iput-object v1, v0, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 346154
    move-object v0, v0

    .line 346155
    const/4 v1, 0x1

    move v1, v1

    .line 346156
    if-eqz v1, :cond_0

    .line 346157
    iput-boolean v2, v0, LX/3DL;->m:Z

    .line 346158
    invoke-direct {p0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->g()Ljava/lang/String;

    move-result-object v1

    .line 346159
    iput-object v1, v0, LX/3DL;->n:Ljava/lang/String;

    .line 346160
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a:LX/1rk;

    invoke-virtual {v0}, LX/3DL;->q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v0

    invoke-virtual {v1, v0, p3}, LX/1rk;->a(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 6

    .prologue
    .line 346124
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->a:LX/1rk;

    .line 346125
    iget-object v1, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v1

    .line 346126
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 346127
    sget-object v1, LX/1rk;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 346128
    if-eqz v1, :cond_1

    .line 346129
    :cond_0
    :goto_0
    return-void

    .line 346130
    :cond_1
    iget-object v1, v0, LX/1rk;->g:LX/0TD;

    new-instance v4, LX/3cs;

    invoke-direct {v4, v0, v2, v3}, LX/3cs;-><init>(LX/1rk;J)V

    invoke-interface {v1, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 346131
    new-instance v4, LX/3ct;

    invoke-direct {v4, v0}, LX/3ct;-><init>(LX/1rk;)V

    .line 346132
    iget-object v5, v0, LX/1rk;->g:LX/0TD;

    invoke-static {v1, v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 346133
    sget-object v4, LX/1rk;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 346134
    new-instance v4, LX/3Eh;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    sget-object p0, LX/1rk;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-direct {v4, v5, p0}, LX/3Eh;-><init>(Ljava/lang/Long;Ljava/util/concurrent/ConcurrentMap;)V

    invoke-static {v1, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V
    .locals 2
    .param p3    # LX/Drn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 346120
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->b(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 346121
    if-nez p3, :cond_0

    .line 346122
    :goto_0
    return-void

    .line 346123
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsSyncManagerImpl;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
