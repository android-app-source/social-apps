.class public Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1rj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile q:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;


# instance fields
.field public final b:LX/1rp;

.field private final c:LX/0tX;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/1ro;

.field public final f:LX/0Sh;

.field public final g:Ljava/util/concurrent/Executor;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/1rU;

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final k:LX/0SG;

.field public final l:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CSD;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Zb;

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/common/NotificationsSyncManager$NotificationSyncManagerCallback;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mCallbackList"
    .end annotation
.end field

.field private final p:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 344138
    const-class v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1rp;LX/0tX;LX/0TD;LX/1ro;LX/0Sh;Ljava/util/concurrent/Executor;LX/0Ot;LX/1rU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0Zb;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 4
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1rp;",
            "LX/0tX;",
            "LX/0TD;",
            "LX/1ro;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "LX/1rU;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0SG;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "LX/CSD;",
            ">;",
            "LX/0Zb;",
            "Lcom/facebook/reaction/ReactionUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    .line 344122
    iput-object p1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    .line 344123
    iput-object p2, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->c:LX/0tX;

    .line 344124
    iput-object p3, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->d:Ljava/util/concurrent/ExecutorService;

    .line 344125
    iput-object p4, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->e:LX/1ro;

    .line 344126
    iput-object p5, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->f:LX/0Sh;

    .line 344127
    iput-object p6, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->g:Ljava/util/concurrent/Executor;

    .line 344128
    iput-object p7, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->h:LX/0Ot;

    .line 344129
    iput-object p8, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->i:LX/1rU;

    .line 344130
    iput-object p9, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 344131
    iput-object p10, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->k:LX/0SG;

    .line 344132
    iput-object p11, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->l:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 344133
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->m:LX/0Ot;

    .line 344134
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->n:LX/0Zb;

    .line 344135
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->p:Lcom/facebook/reaction/ReactionUtil;

    .line 344136
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    new-instance v2, LX/1vp;

    invoke-direct {v2, p0}, LX/1vp;-><init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V

    invoke-virtual {v1, v2}, LX/1rp;->a(LX/1vq;)V

    .line 344137
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;
    .locals 3

    .prologue
    .line 344110
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->q:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    if-nez v0, :cond_1

    .line 344111
    const-class v1, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    monitor-enter v1

    .line 344112
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->q:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 344113
    if-eqz v2, :cond_0

    .line 344114
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->q:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344115
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 344116
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344117
    :cond_1
    sget-object v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->q:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    return-object v0

    .line 344118
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 344119
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/3DO;Z)V
    .locals 7

    .prologue
    .line 344108
    iget-object v6, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->f:LX/0Sh;

    new-instance v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$2;-><init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/3DO;Z)V

    invoke-virtual {v6, v0}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 344109
    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/0Px;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2ub;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 344085
    new-instance v0, LX/3DL;

    invoke-direct {v0}, LX/3DL;-><init>()V

    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 344086
    iput-object v1, v0, LX/3DL;->a:LX/0rS;

    .line 344087
    move-object v0, v0

    .line 344088
    iput-object p1, v0, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 344089
    move-object v1, v0

    .line 344090
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    .line 344091
    iput-object v0, v1, LX/3DL;->l:Ljava/lang/String;

    .line 344092
    move-object v0, v1

    .line 344093
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 344094
    iput-object v1, v0, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 344095
    move-object v0, v0

    .line 344096
    invoke-virtual {p2}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v1

    .line 344097
    iput-object v1, v0, LX/3DL;->g:Ljava/lang/String;

    .line 344098
    move-object v0, v0

    .line 344099
    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344100
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "null"

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3DL;->a(Ljava/util/List;)LX/3DL;

    .line 344101
    :goto_0
    const/4 v1, 0x1

    move v1, v1

    .line 344102
    if-eqz v1, :cond_0

    .line 344103
    iput-boolean v4, v0, LX/3DL;->m:Z

    .line 344104
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->p:Lcom/facebook/reaction/ReactionUtil;

    const-string v2, "ANDROID_NOTIFICATIONS"

    invoke-virtual {v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344105
    iput-object v1, v0, LX/3DL;->n:Ljava/lang/String;

    .line 344106
    :cond_0
    invoke-virtual {v0}, LX/3DL;->q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v0

    return-object v0

    .line 344107
    :cond_1
    invoke-virtual {v0, p3}, LX/3DL;->a(Ljava/util/List;)LX/3DL;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/2ub;LX/3DO;Z)V
    .locals 3

    .prologue
    .line 344078
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 344079
    :cond_0
    :goto_0
    return-void

    .line 344080
    :cond_1
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "notification_sync"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 344081
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 344082
    move-object v0, v0

    .line 344083
    const-string v1, "syncSource"

    iget-object v2, p1, LX/2ub;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "syncType"

    invoke-virtual {p2}, LX/3DO;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "syncSuccess"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 344084
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->n:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;
    .locals 15

    .prologue
    .line 344076
    new-instance v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-static {p0}, LX/1rp;->a(LX/0QB;)LX/1rp;

    move-result-object v1

    check-cast v1, LX/1rp;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {p0}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v4

    check-cast v4, LX/1ro;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    const/16 v7, 0x4cd

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/1rU;->a(LX/0QB;)LX/1rU;

    move-result-object v8

    check-cast v8, LX/1rU;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v11

    check-cast v11, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v12, 0x2b04

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v13

    check-cast v13, LX/0Zb;

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v14

    check-cast v14, Lcom/facebook/reaction/ReactionUtil;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;-><init>(LX/1rp;LX/0tX;LX/0TD;LX/1ro;LX/0Sh;Ljava/util/concurrent/Executor;LX/0Ot;LX/1rU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/0Zb;Lcom/facebook/reaction/ReactionUtil;)V

    .line 344077
    return-object v0
.end method

.method private e()Z
    .locals 14

    .prologue
    .line 344071
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2uc;->b:LX/0Tn;

    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 344072
    iget-object v4, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->k:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 344073
    iget-object v10, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v11, LX/0hM;->C:LX/0Tn;

    const-wide/16 v12, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v10

    move-wide v6, v10

    .line 344074
    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->i(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    sub-long/2addr v4, v6

    move-wide v2, v4

    .line 344075
    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J
    .locals 4

    .prologue
    .line 344029
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->k:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->i(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static i(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J
    .locals 4

    .prologue
    .line 344070
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0hM;->D:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a()LX/2kW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344069
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2ub;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344068
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 3

    .prologue
    .line 344062
    new-instance v0, LX/BC6;

    invoke-direct {v0}, LX/BC6;-><init>()V

    move-object v0, v0

    .line 344063
    const-string v1, "count"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 344064
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 344065
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 344066
    new-instance v1, LX/Drk;

    invoke-direct {v1, p0}, LX/Drk;-><init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V

    iget-object v2, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 344067
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V
    .locals 10
    .param p3    # LX/Drn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 344041
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/2uc;->c:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DO;

    .line 344042
    sget-object v3, LX/2ub;->MQTT_NEW:LX/2ub;

    if-eq p2, v3, :cond_0

    sget-object v3, LX/2ub;->MQTT_FULL:LX/2ub;

    if-ne p2, v3, :cond_1

    :cond_0
    move v3, v1

    .line 344043
    :goto_0
    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 344044
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    move v3, v2

    .line 344045
    goto :goto_0

    .line 344046
    :cond_2
    :try_start_1
    sget-object v3, LX/3DO;->FULL:LX/3DO;

    if-ne v0, v3, :cond_6

    sget-object v3, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-eq p2, v3, :cond_6

    .line 344047
    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->g(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J

    move-result-wide v6

    const-wide/32 v8, 0x6ddd00

    cmp-long v6, v6, v8

    if-lez v6, :cond_7

    const/4 v6, 0x1

    :goto_2
    move v3, v6

    .line 344048
    if-nez v3, :cond_6

    .line 344049
    sget-object v0, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    .line 344050
    :goto_3
    sget-object v2, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    if-ne p2, v2, :cond_3

    invoke-static {p0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->g(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 344051
    sget-object v0, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    .line 344052
    :cond_3
    if-eqz p3, :cond_4

    .line 344053
    iget-object v2, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344054
    :try_start_2
    iget-object v3, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->o:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344055
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344056
    :cond_4
    :try_start_3
    sget-object v2, LX/3DO;->NEW_NOTIFICATIONS:LX/3DO;

    if-ne v0, v2, :cond_5

    .line 344057
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/3DO;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 344058
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 344059
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    .line 344060
    :cond_5
    iget-object v1, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->g:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$3;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager$3;-><init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/3DO;)V

    const v3, -0x32b1103b

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 344061
    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public final b()LX/2kM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344038
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    .line 344039
    iget-object p0, v0, LX/2kW;->o:LX/2kM;

    move-object v0, p0

    .line 344040
    return-object v0
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 7

    .prologue
    .line 344033
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 344034
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v1

    const/16 v2, 0xa

    new-instance v3, LX/3DK;

    sget-object v0, LX/2ub;->SCROLL:LX/2ub;

    .line 344035
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 344036
    invoke-static {p0, p1, v0, v4}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a$redex0(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/0Px;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v4

    sget-object v5, LX/2ub;->SCROLL:LX/2ub;

    sget-object v0, LX/2uc;->c:LX/0P1;

    sget-object v6, LX/2ub;->SCROLL:LX/2ub;

    invoke-virtual {v0, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DO;

    invoke-direct {v3, v4, v5, v0}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;)V

    invoke-virtual {v1, v2, v3}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 344037
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 344030
    iget-object v0, p0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    .line 344031
    iget-object p0, v0, LX/2kW;->o:LX/2kM;

    move-object v0, p0

    .line 344032
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
