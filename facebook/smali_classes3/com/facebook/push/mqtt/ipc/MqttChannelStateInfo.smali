.class public Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/1u0;

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 342890
    new-instance v0, LX/1u1;

    invoke-direct {v0}, LX/1u1;-><init>()V

    sput-object v0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/1u0;JJJZ)V
    .locals 0

    .prologue
    .line 342891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342892
    iput-object p1, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    .line 342893
    iput-wide p2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->b:J

    .line 342894
    iput-wide p4, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    .line 342895
    iput-wide p6, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    .line 342896
    iput-boolean p8, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    .line 342897
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 342889
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342871
    if-ne p0, p1, :cond_1

    .line 342872
    :cond_0
    :goto_0
    return v0

    .line 342873
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 342874
    goto :goto_0

    .line 342875
    :cond_3
    check-cast p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    .line 342876
    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    move-object v2, v2

    .line 342877
    iget-object v3, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    move-object v3, v3

    .line 342878
    if-ne v2, v3, :cond_4

    .line 342879
    iget-wide v6, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    move-wide v2, v6

    .line 342880
    iget-wide v6, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    move-wide v4, v6

    .line 342881
    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 342882
    iget-wide v6, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    move-wide v2, v6

    .line 342883
    iget-wide v6, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    move-wide v4, v6

    .line 342884
    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 342885
    iget-boolean v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    move v2, v2

    .line 342886
    iget-boolean v3, p1, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    move v3, v3

    .line 342887
    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    .line 342888
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 342864
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mConnectionState"

    iget-object v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mServiceGeneratedMs"

    iget-wide v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "mLastConnectionMs"

    iget-wide v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "mLastDisconnectMs"

    iget-wide v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "mClockSkewDetected"

    iget-boolean v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 342865
    iget-object v0, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->a:LX/1u0;

    invoke-virtual {v0}, LX/1u0;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 342866
    iget-wide v0, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 342867
    iget-wide v0, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 342868
    iget-wide v0, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 342869
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->e:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 342870
    return-void
.end method
