.class public Lcom/facebook/reaction/ReactionUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile H:Lcom/facebook/reaction/ReactionUtil;


# instance fields
.field private final A:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0sU;

.field private final C:Ljava/util/concurrent/ExecutorService;

.field private final D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final E:LX/0hB;

.field private final F:LX/0dC;

.field public final G:LX/0tQ;

.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0sX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0SG;

.field private final d:Landroid/content/Context;

.field public final e:LX/0tX;

.field private final f:LX/0rq;

.field private final g:LX/0sa;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/CK5;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/01T;

.field public final m:LX/1vj;

.field public final n:LX/1vi;

.field public final o:LX/1vC;

.field public final p:LX/1V6;

.field public final q:LX/1vm;

.field public final r:LX/1s6;

.field private final s:LX/1vn;

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final v:LX/0se;

.field public final w:LX/1vo;

.field private final x:LX/0yD;

.field private final y:LX/0ad;

.field private final z:LX/14x;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0SG;Landroid/content/Context;LX/0Ot;LX/0Or;LX/0tX;LX/0rq;LX/01T;LX/0sa;LX/0Or;LX/1vj;LX/1vi;LX/1vC;LX/1V6;LX/1vm;LX/1s6;LX/1vn;LX/1Ck;LX/0se;LX/1vo;LX/0yD;LX/0ad;LX/14x;LX/0Or;LX/0sU;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0hB;LX/0dC;LX/0tQ;)V
    .locals 8
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p27    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0sX;",
            ">;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/CK5;",
            ">;",
            "LX/0tX;",
            "LX/0rq;",
            "LX/01T;",
            "LX/0sa;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1vj;",
            "LX/1vi;",
            "LX/1vC;",
            "LX/1V6;",
            "LX/1vm;",
            "LX/1s6;",
            "LX/1vn;",
            "LX/1Ck;",
            "LX/0se;",
            "LX/1vo;",
            "LX/0yD;",
            "LX/0ad;",
            "LX/14x;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0sU;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0hB;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 344366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344367
    iput-object p1, p0, Lcom/facebook/reaction/ReactionUtil;->a:LX/0Ot;

    .line 344368
    iput-object p2, p0, Lcom/facebook/reaction/ReactionUtil;->b:LX/0Ot;

    .line 344369
    iput-object p3, p0, Lcom/facebook/reaction/ReactionUtil;->c:LX/0SG;

    .line 344370
    iput-object p4, p0, Lcom/facebook/reaction/ReactionUtil;->d:Landroid/content/Context;

    .line 344371
    iput-object p7, p0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    .line 344372
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->l:LX/01T;

    .line 344373
    iput-object p5, p0, Lcom/facebook/reaction/ReactionUtil;->h:LX/0Ot;

    .line 344374
    iput-object p6, p0, Lcom/facebook/reaction/ReactionUtil;->i:LX/0Or;

    .line 344375
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    .line 344376
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->g:LX/0sa;

    .line 344377
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->k:LX/0Or;

    .line 344378
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    .line 344379
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    .line 344380
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    .line 344381
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->s:LX/1vn;

    .line 344382
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->p:LX/1V6;

    .line 344383
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->q:LX/1vm;

    .line 344384
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    .line 344385
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->z:LX/14x;

    .line 344386
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->B:LX/0sU;

    .line 344387
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DESCRIPTIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    .line 344388
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    .line 344389
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->v:LX/0se;

    .line 344390
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    .line 344391
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->x:LX/0yD;

    .line 344392
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->y:LX/0ad;

    .line 344393
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v1 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    .line 344394
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->A:LX/0Or;

    .line 344395
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->C:Ljava/util/concurrent/ExecutorService;

    .line 344396
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->D:LX/0Ot;

    .line 344397
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->E:LX/0hB;

    .line 344398
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->F:LX/0dC;

    .line 344399
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->G:LX/0tQ;

    .line 344400
    return-void
.end method

.method private static a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I
    .locals 2

    .prologue
    .line 344297
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static a(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;
    .locals 34

    .prologue
    .line 344298
    new-instance v2, Lcom/facebook/reaction/ReactionUtil;

    const/16 v3, 0x23d

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x60

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const/16 v7, 0xc83

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x26ca

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v10

    check-cast v10, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v11

    check-cast v11, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v12

    check-cast v12, LX/0sa;

    const/16 v13, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v14

    check-cast v14, LX/1vj;

    invoke-static/range {p0 .. p0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v15

    check-cast v15, LX/1vi;

    invoke-static/range {p0 .. p0}, LX/1vC;->a(LX/0QB;)LX/1vC;

    move-result-object v16

    check-cast v16, LX/1vC;

    const-class v17, LX/1V6;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1V6;

    const-class v18, LX/1vm;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/1vm;

    invoke-static/range {p0 .. p0}, LX/1s6;->a(LX/0QB;)LX/1s6;

    move-result-object v19

    check-cast v19, LX/1s6;

    invoke-static/range {p0 .. p0}, LX/1vn;->a(LX/0QB;)LX/1vn;

    move-result-object v20

    check-cast v20, LX/1vn;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v21

    check-cast v21, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v22

    check-cast v22, LX/0se;

    invoke-static/range {p0 .. p0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v23

    check-cast v23, LX/1vo;

    invoke-static/range {p0 .. p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v24

    check-cast v24, LX/0yD;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v26

    check-cast v26, LX/14x;

    const/16 v27, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v28

    check-cast v28, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v29

    check-cast v29, Ljava/util/concurrent/ExecutorService;

    const/16 v30, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v31

    check-cast v31, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v32

    check-cast v32, LX/0dC;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v33

    check-cast v33, LX/0tQ;

    invoke-direct/range {v2 .. v33}, Lcom/facebook/reaction/ReactionUtil;-><init>(LX/0Ot;LX/0Ot;LX/0SG;Landroid/content/Context;LX/0Ot;LX/0Or;LX/0tX;LX/0rq;LX/01T;LX/0sa;LX/0Or;LX/1vj;LX/1vi;LX/1vC;LX/1V6;LX/1vm;LX/1s6;LX/1vn;LX/1Ck;LX/0se;LX/1vo;LX/0yD;LX/0ad;LX/14x;LX/0Or;LX/0sU;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0hB;LX/0dC;LX/0tQ;)V

    .line 344299
    return-object v2
.end method

.method private static a(LX/0m9;Ljava/lang/String;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0m9;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344300
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 344301
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 344302
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 344303
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 344304
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 344305
    return-void
.end method

.method private static a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/ReactionQueryParams;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 344306
    iget-object v0, p1, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 344307
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    if-ne v0, v1, :cond_0

    .line 344308
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sg;

    invoke-virtual {v0, p2}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 344309
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;
    .locals 3

    .prologue
    .line 344310
    sget-object v0, Lcom/facebook/reaction/ReactionUtil;->H:Lcom/facebook/reaction/ReactionUtil;

    if-nez v0, :cond_1

    .line 344311
    const-class v1, Lcom/facebook/reaction/ReactionUtil;

    monitor-enter v1

    .line 344312
    :try_start_0
    sget-object v0, Lcom/facebook/reaction/ReactionUtil;->H:Lcom/facebook/reaction/ReactionUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 344313
    if-eqz v2, :cond_0

    .line 344314
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v0

    sput-object v0, Lcom/facebook/reaction/ReactionUtil;->H:Lcom/facebook/reaction/ReactionUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 344315
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 344316
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344317
    :cond_1
    sget-object v0, Lcom/facebook/reaction/ReactionUtil;->H:Lcom/facebook/reaction/ReactionUtil;

    return-object v0

    .line 344318
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 344319
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 344401
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/reaction/ReactionUtil;->d(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344402
    :goto_0
    return-void

    .line 344403
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/reaction/ReactionUtil;->c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 344404
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 344405
    invoke-static {p0, p1, v0}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 344406
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    new-instance v2, LX/Cfb;

    invoke-direct {v2, p0, p2}, LX/Cfb;-><init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 344407
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jX;

    invoke-direct {v1, p2, p1}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/ReactionQueryParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344320
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->x:LX/0yD;

    invoke-virtual {p1, v0}, Lcom/facebook/reaction/ReactionQueryParams;->a(LX/0yD;)LX/4J6;

    move-result-object v1

    .line 344321
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 344322
    const-string v2, "subject_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344323
    const-string v0, "surface"

    invoke-virtual {v1, v0, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344324
    new-instance v0, LX/9qP;

    invoke-direct {v0}, LX/9qP;-><init>()V

    move-object v2, v0

    .line 344325
    invoke-virtual {p0, v2, p3}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 344326
    const-string v0, "reaction_trigger_data"

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_after_cursor"

    .line 344327
    iget-object v3, p1, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 344328
    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_result_count"

    .line 344329
    iget-wide v6, p1, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_5

    iget-wide v6, p1, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    :goto_0
    move-wide v4, v6

    .line 344330
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "automatic_photo_captioning_enabled"

    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sX;

    invoke-virtual {v0}, LX/0sX;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "enable_download"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->G:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 344331
    iget-object v0, p1, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    move-object v0, v0

    .line 344332
    if-eqz v0, :cond_0

    .line 344333
    const-string v0, "action_location"

    .line 344334
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    move-object v1, v1

    .line 344335
    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 344336
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->A:LX/0Or;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->A:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 344337
    const/4 v0, 0x1

    move v0, v0

    .line 344338
    if-eqz v0, :cond_1

    .line 344339
    const-string v0, "scrubbing"

    const-string v1, "MPEG_DASH"

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 344340
    :cond_1
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 344341
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 344342
    if-eqz v1, :cond_2

    .line 344343
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 344344
    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 344345
    :cond_2
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    move-object v1, v1

    .line 344346
    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 344347
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    move-object v1, v1

    .line 344348
    iput-object v1, v0, LX/0zO;->d:Ljava/util/Set;

    .line 344349
    :cond_3
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 344350
    iput-object p3, v0, LX/0zO;->z:Ljava/lang/String;

    .line 344351
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 344352
    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    move-object v2, v2

    .line 344353
    iget-object v3, p1, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    move-object v3, v3

    .line 344354
    iget-object v4, p1, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    move-object v4, v4

    .line 344355
    invoke-static {v1, v2, v3, v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 344356
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 344357
    return-object v0

    :cond_5
    const-wide/16 v6, 0x1f4

    goto/16 :goto_0
.end method

.method public static d(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 344358
    if-nez p2, :cond_0

    .line 344359
    :goto_0
    return v0

    .line 344360
    :cond_0
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 344361
    if-eqz v1, :cond_2

    invoke-static {p3}, LX/2s8;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 344362
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v2, LX/2jU;

    invoke-direct {v2, p2}, LX/2jU;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 344363
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private g()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 344364
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1648

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 344365
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private h()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 344294
    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->i(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const v1, 0x3ff33333    # 1.9f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 344295
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static i(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 344296
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    invoke-virtual {v0}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static k(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 344139
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    invoke-virtual {v0}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/4J4;
    .locals 3

    .prologue
    .line 344149
    new-instance v0, LX/4J4;

    invoke-direct {v0}, LX/4J4;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->EXTRA_SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-static {p0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 344150
    const-string v2, "EXTRA_SMALL"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344151
    move-object v0, v0

    .line 344152
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->SMALL:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-static {p0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 344153
    const-string v2, "SMALL"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344154
    move-object v0, v0

    .line 344155
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->MEDIUM:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-static {p0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 344156
    const-string v2, "MEDIUM"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344157
    move-object v0, v0

    .line 344158
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-static {p0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 344159
    const-string v2, "LARGE"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344160
    move-object v0, v0

    .line 344161
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;->EXTRA_LARGE:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    invoke-static {p0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 344162
    const-string v2, "EXTRA_LARGE"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344163
    move-object v0, v0

    .line 344164
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 344140
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 344141
    const-string v1, "unit_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    invoke-static {v0, v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 344142
    const-string v1, "action_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    invoke-virtual {v2, p1}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 344143
    const-string v1, "component_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    invoke-virtual {v2}, LX/1vo;->a()LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 344144
    const-string v1, "story_attachment_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    invoke-virtual {v2}, LX/1vj;->a()LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 344145
    const-string v1, "story_header_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    invoke-static {v0, v1, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0m9;Ljava/lang/String;LX/0Px;)V

    .line 344146
    const-string v1, "surface"

    invoke-virtual {v0, v1, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 344147
    const-string v1, "request_type"

    const-string v2, "normal"

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 344148
    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Ve;ILjava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
            ">;>;I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0m9;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 344165
    new-instance v0, LX/CfZ;

    move-object v1, p0

    move v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/CfZ;-><init>(Lcom/facebook/reaction/ReactionUtil;ILjava/lang/String;LX/0m9;Ljava/lang/String;)V

    .line 344166
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "suggestedEvents"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 344167
    return-void
.end method

.method public final a(LX/0gW;Ljava/lang/String;)V
    .locals 13
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 344168
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 344169
    const v1, 0x7f0b1616

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 344170
    const v2, 0x7f0b160f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 344171
    const v3, 0x7f0b164a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 344172
    const v4, 0x7f0b164b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 344173
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v5, v5, 0x2

    .line 344174
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v6, v6

    const v7, 0x3f2aaaab

    mul-float/2addr v6, v7

    float-to-int v6, v6

    .line 344175
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v7, v7, 0x2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v8, v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 344176
    const v8, 0x7f0b160f

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 344177
    const/high16 v9, 0x42200000    # 40.0f

    iget-object v10, p0, Lcom/facebook/reaction/ReactionUtil;->E:LX/0hB;

    invoke-virtual {v10}, LX/0hB;->b()F

    move-result v10

    mul-float/2addr v9, v10

    float-to-int v9, v9

    .line 344178
    const-string v10, "reaction_aggregated_units_count"

    const/16 v11, 0xa

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v10

    const-string v11, "reaction_client_capabilities"

    invoke-virtual {p0}, Lcom/facebook/reaction/ReactionUtil;->f()LX/0Px;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v10

    const-string v11, "reaction_core_component_image_sizes"

    invoke-direct {p0}, Lcom/facebook/reaction/ReactionUtil;->l()LX/4J4;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v10

    const-string v11, "reaction_critic_review_thumbnail_height"

    invoke-direct {p0}, Lcom/facebook/reaction/ReactionUtil;->h()Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v10

    const-string v11, "reaction_critic_review_thumbnail_width"

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->i(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v10

    const-string v11, "place_question_photo_size"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v10, v11, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v10, "image_height"

    .line 344179
    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->k(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    int-to-float v11, v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    const v12, 0x3fe38e39

    div-float/2addr v11, v12

    float-to-int v11, v11

    .line 344180
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object v11, v11

    .line 344181
    invoke-virtual {v7, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v10, "image_width"

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->k(Lcom/facebook/reaction/ReactionUtil;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v10, "reaction_info_row_icon_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v10, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v7, "media_type"

    iget-object v10, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    invoke-virtual {v10}, LX/0rq;->a()LX/0wF;

    move-result-object v10

    invoke-virtual {v2, v7, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v7, "profile_image_size"

    invoke-direct {p0}, Lcom/facebook/reaction/ReactionUtil;->g()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v2, v7, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v7, "reaction_facepile_profile_pic_size"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_friend_inviter_profile_image_size"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_paginated_components_count"

    const/4 v6, 0x5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_product_item_image_size"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_profile_image_size_medium"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_profile_image_size_small"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_media_type"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->b()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_profile_pic_media_type"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->f:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->b()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_profile_pic_size"

    invoke-direct {p0}, Lcom/facebook/reaction/ReactionUtil;->g()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_review_profile_pic_size"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->s:LX/1vn;

    .line 344182
    iget-object v4, v3, LX/1vn;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b0d41

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 344183
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v3, v4

    .line 344184
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_supported_unit_header_styles"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_supported_unit_styles"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_context_items_surface"

    const-string v3, "PLACE_TIPS"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_context_items_source"

    const-string v3, "PLACE_TIPS"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_context_items_source_id"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_context_items_row_limit"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "reaction_page_surface_context_items_row_limit"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "context_item_icon_size"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_picture_size"

    const/4 v3, 0x1

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v3, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "angora_attachment_cover_image_size"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->g:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "image_large_aspect_height"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->g:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "image_large_aspect_width"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->g:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "device_id"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->F:LX/0dC;

    invoke-virtual {v2}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v1, "review_character_count"

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "page_service_image_width"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "page_service_image_height"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 344185
    invoke-static {p2}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344186
    const-string v0, "local_search_results_page"

    .line 344187
    :goto_0
    move-object v0, v0

    .line 344188
    if-eqz v0, :cond_0

    .line 344189
    const-string v1, "feed_story_render_location"

    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 344190
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->l:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-eq v0, v1, :cond_5

    .line 344191
    const-string v0, "reaction_supported_action_styles"

    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    invoke-virtual {v1, p2}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_supported_component_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    invoke-virtual {v2}, LX/1vo;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    move-result-object v0

    const-string v1, "reaction_supported_attachment_styles"

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    invoke-virtual {v2}, LX/1vj;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 344192
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->v:LX/0se;

    invoke-virtual {v0, p1}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 344193
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->B:LX/0sU;

    invoke-virtual {v0, p1}, LX/0sU;->a(LX/0gW;)V

    .line 344194
    return-void

    .line 344195
    :cond_2
    invoke-static {p2}, LX/2s8;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344196
    const-string v0, "fundraiser_page"

    goto :goto_0

    .line 344197
    :cond_3
    invoke-static {p2}, LX/2s8;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 344198
    const-string v0, "video_home"

    goto :goto_0

    .line 344199
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 344200
    :cond_5
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    const/4 v1, 0x0

    .line 344201
    iget-object v2, v0, LX/1s6;->a:[LX/1s7;

    if-nez v2, :cond_7

    .line 344202
    :cond_6
    :goto_2
    move v0, v1

    .line 344203
    if-eqz v0, :cond_1

    .line 344204
    const-string v0, "reaction_supported_action_styles"

    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    invoke-virtual {v1, p2}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    goto :goto_1

    .line 344205
    :cond_7
    iget-object v3, v0, LX/1s6;->a:[LX/1s7;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 344206
    invoke-interface {v5, p2}, LX/1s7;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 344207
    const/4 v1, 0x1

    goto :goto_2

    .line 344208
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public final a(LX/0v6;ILcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0Ve;Ljava/util/concurrent/ExecutorService;ZJLX/0zT;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0zT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 344209
    const/4 v0, 0x0

    .line 344210
    iput-object v0, p3, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 344211
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e0008

    invoke-virtual {v0, v1, p4, p5}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 344212
    invoke-static {p0, p3, p4, p5}, Lcom/facebook/reaction/ReactionUtil;->d(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344213
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid query params when adding initial Reaction request into batch request. The batch request is not sent."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 344214
    :goto_0
    return-void

    .line 344215
    :cond_0
    invoke-static {p0, p3, p4, p5}, Lcom/facebook/reaction/ReactionUtil;->c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 344216
    iput p2, v0, LX/0zO;->B:I

    .line 344217
    if-eqz p8, :cond_1

    .line 344218
    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const/4 v2, 0x1

    .line 344219
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 344220
    move-object v1, v1

    .line 344221
    invoke-virtual {v1, p9, p10}, LX/0zO;->a(J)LX/0zO;

    .line 344222
    if-eqz p11, :cond_1

    .line 344223
    invoke-virtual {v0, p11}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 344224
    :cond_1
    invoke-static {p5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 344225
    iput-object p5, v0, LX/0zO;->z:Ljava/lang/String;

    .line 344226
    :cond_2
    invoke-virtual {p1, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 344227
    invoke-static {p0, p3, v0}, Lcom/facebook/reaction/ReactionUtil;->a(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 344228
    new-instance v1, LX/Cfb;

    invoke-direct {v1, p0, p4}, LX/Cfb;-><init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->C:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 344229
    if-eqz p6, :cond_3

    if-eqz p7, :cond_3

    .line 344230
    invoke-static {v0, p6, p7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 344231
    :cond_3
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0v6;)V

    .line 344232
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jX;

    invoke-direct {v1, p4, p3}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 344233
    const/4 v0, 0x0

    .line 344234
    iput-object v0, p1, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 344235
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e0008

    invoke-virtual {v0, v1, p2, p3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 344236
    invoke-static {p3}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344237
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e000e

    invoke-virtual {v0, v1, p2, p3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 344238
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/reaction/ReactionUtil;->b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 344239
    return-void
.end method

.method public final a(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0zS;JLX/0zT;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p7    # LX/0zT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 344240
    const/4 v0, 0x0

    .line 344241
    iput-object v0, p1, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 344242
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e0008

    invoke-virtual {v0, v1, p2, p3}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 344243
    invoke-virtual/range {p0 .. p7}, Lcom/facebook/reaction/ReactionUtil;->b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0zS;JLX/0zT;)V

    .line 344244
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Ve;ILjava/lang/String;LX/2jY;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
            ">;>;I",
            "Ljava/lang/String;",
            "LX/2jY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 344245
    iget-object v0, p5, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v0

    .line 344246
    new-instance v0, LX/9u5;

    invoke-direct {v0}, LX/9u5;-><init>()V

    move-object v0, v0

    .line 344247
    const-string v2, "reaction_component_id"

    invoke-virtual {v0, v2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "reaction_after_cursor"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "automatic_photo_captioning_enabled"

    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sX;

    invoke-virtual {v0}, LX/0sX;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "reaction_result_count"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "enable_download"

    iget-object v3, p0, Lcom/facebook/reaction/ReactionUtil;->G:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9u5;

    .line 344248
    invoke-virtual {p0, v0, v1}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 344249
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 344250
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 344251
    iput-object v1, v0, LX/0zO;->z:Ljava/lang/String;

    .line 344252
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 344253
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {v1, p4, v0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 344254
    return-void
.end method

.method public a(LX/2jY;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 344255
    if-eqz p1, :cond_0

    .line 344256
    iget-boolean v2, p1, LX/2jY;->o:Z

    move v2, v2

    .line 344257
    if-eqz v2, :cond_0

    .line 344258
    iget-boolean v2, p1, LX/2jY;->p:Z

    move v2, v2

    .line 344259
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 344260
    :goto_0
    return v0

    .line 344261
    :cond_1
    iget-object v2, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v2, v2

    .line 344262
    invoke-virtual {p1}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v3

    .line 344263
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 344264
    :cond_2
    iput-boolean v1, p1, LX/2jY;->o:Z

    .line 344265
    move v0, v1

    .line 344266
    goto :goto_0

    .line 344267
    :cond_3
    iput-boolean v0, p1, LX/2jY;->p:Z

    .line 344268
    iput-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 344269
    move-object v1, v2

    .line 344270
    iget-object v2, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v2

    .line 344271
    iget-object v3, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v3

    .line 344272
    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/reaction/ReactionUtil;->b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0zS;JLX/0zT;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p7    # LX/0zT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 344273
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/reaction/ReactionUtil;->d(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344274
    :goto_0
    return-void

    .line 344275
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/facebook/reaction/ReactionUtil;->c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 344276
    invoke-virtual {v0, p4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 344277
    const/4 v1, 0x1

    .line 344278
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 344279
    invoke-virtual {v0, p5, p6}, LX/0zO;->a(J)LX/0zO;

    .line 344280
    if-eqz p7, :cond_1

    .line 344281
    invoke-virtual {v0, p7}, LX/0zO;->a(LX/0zT;)LX/0zO;

    .line 344282
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    iget-object v2, p0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v2, LX/Cfb;

    invoke-direct {v2, p0, p2}, LX/Cfb;-><init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V

    invoke-virtual {v1, p2, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 344283
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jX;

    invoke-direct {v1, p2, p1}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final f()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344284
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 344285
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    .line 344286
    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v2, :cond_0

    .line 344287
    const-string v0, "LOCATION_BACKGROUND"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 344288
    const-string v0, "LOCATION_FOREGROUND"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 344289
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->z:LX/14x;

    invoke-virtual {v0}, LX/14x;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344290
    const-string v0, "MESSENGER"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 344291
    :cond_1
    iget-object v0, p0, Lcom/facebook/reaction/ReactionUtil;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CK5;

    invoke-virtual {v0}, LX/CK5;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344292
    const-string v0, "MESSENGER_RIDE_SHARE"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 344293
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
