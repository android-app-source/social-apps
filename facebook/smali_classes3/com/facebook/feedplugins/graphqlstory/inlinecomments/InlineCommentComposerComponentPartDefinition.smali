.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/35L;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field private final e:LX/35M;

.field private final f:LX/1V0;

.field private final g:LX/0tN;

.field private final h:LX/1VF;

.field private final i:LX/21r;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357807
    new-instance v0, LX/21j;

    invoke-direct {v0}, LX/21j;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/35M;LX/1V0;LX/0tN;LX/1VF;LX/21r;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357827
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 357828
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->e:LX/35M;

    .line 357829
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->f:LX/1V0;

    .line 357830
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->g:LX/0tN;

    .line 357831
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->h:LX/1VF;

    .line 357832
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->i:LX/21r;

    .line 357833
    return-void
.end method

.method private a(LX/1De;LX/35L;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/35L;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 357808
    new-instance v0, LX/1X6;

    iget-object v1, p2, LX/35L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    const/4 v3, -0x1

    const v4, 0x7f020a75

    invoke-direct {v0, v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 357809
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->e:LX/35M;

    const/4 v2, 0x0

    .line 357810
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    invoke-direct {v3, v1}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;-><init>(LX/35M;)V

    .line 357811
    iget-object v4, v1, LX/35M;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/39T;

    .line 357812
    if-nez v4, :cond_0

    .line 357813
    new-instance v4, LX/39T;

    invoke-direct {v4, v1}, LX/39T;-><init>(LX/35M;)V

    .line 357814
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/39T;->a$redex0(LX/39T;LX/1De;IILcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;)V

    .line 357815
    move-object v3, v4

    .line 357816
    move-object v2, v3

    .line 357817
    move-object v1, v2

    .line 357818
    iget-object v2, p2, LX/35L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357819
    iget-object v3, v1, LX/39T;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    iput-object v2, v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357820
    iget-object v3, v1, LX/39T;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 357821
    move-object v1, v1

    .line 357822
    iget-object v2, v1, LX/39T;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;

    iput-object p3, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComposerComponent$InlineCommentComposerComponentImpl;->c:LX/1Pn;

    .line 357823
    iget-object v2, v1, LX/39T;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 357824
    move-object v1, v1

    .line 357825
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 357826
    iget-object v2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;
    .locals 10

    .prologue
    .line 357796
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;

    monitor-enter v1

    .line 357797
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357798
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357799
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357800
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357801
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/35M;->a(LX/0QB;)LX/35M;

    move-result-object v5

    check-cast v5, LX/35M;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v7

    check-cast v7, LX/0tN;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-static {v0}, LX/21r;->a(LX/0QB;)LX/21r;

    move-result-object v9

    check-cast v9, LX/21r;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;-><init>(Landroid/content/Context;LX/35M;LX/1V0;LX/0tN;LX/1VF;LX/21r;)V

    .line 357802
    move-object v0, v3

    .line 357803
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357804
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357805
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/35L;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357792
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 357793
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->h:LX/1VF;

    iget-object v1, p1, LX/35L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/1VF;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357794
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->i:LX/21r;

    iget-object v2, p1, LX/35L;->b:LX/35I;

    iget v3, p1, LX/35L;->c:I

    iget-object v4, p1, LX/35L;->d:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->g:LX/0tN;

    invoke-virtual {v1}, LX/0tN;->d()Z

    move-result v5

    move-object v1, p4

    invoke-static/range {v0 .. v5}, LX/35G;->a(LX/21r;Landroid/view/View;LX/35I;ILcom/facebook/graphql/enums/ChainingSectionViewState;Z)V

    .line 357795
    :cond_0
    return-void
.end method

.method public static a(LX/0tN;LX/1VF;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tN;",
            "LX/1VF;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 357790
    iget-object v0, p0, LX/0tN;->a:LX/0ad;

    sget-short v1, LX/0wl;->c:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 357791
    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/35L;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357784
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 357785
    invoke-virtual {p4}, Lcom/facebook/components/feed/FeedComponentView;->clearAnimation()V

    .line 357786
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 357787
    if-eqz p0, :cond_0

    .line 357788
    const/4 p1, -0x2

    iput p1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 357789
    :cond_0
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 357834
    check-cast p2, LX/35L;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/1De;LX/35L;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 357783
    check-cast p2, LX/35L;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/1De;LX/35L;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 357782
    check-cast p1, LX/35L;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4eeec0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 357781
    check-cast p1, LX/35L;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x7854908

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 357779
    check-cast p1, LX/35L;

    .line 357780
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->g:LX/0tN;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->h:LX/1VF;

    iget-object v2, p1, LX/35L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0, v1, v2}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->a(LX/0tN;LX/1VF;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 357777
    check-cast p1, LX/35L;

    .line 357778
    iget-object v0, p1, LX/35L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 357774
    check-cast p1, LX/35L;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->b(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 357776
    check-cast p1, LX/35L;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->b(LX/35L;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 357775
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
