.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/36o;",
        "TE;",
        "LX/36m;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static o:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:LX/1VF;

.field public final f:LX/1Ve;

.field private final g:Lcom/facebook/feedback/ui/CommentComposerHelper;

.field private final h:LX/21r;

.field public final i:LX/1EN;

.field private final j:Landroid/content/res/Resources;

.field private final k:LX/0ad;

.field private final l:LX/0tN;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 357909
    new-instance v0, LX/21k;

    invoke-direct {v0}, LX/21k;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->a:LX/1Cz;

    .line 357910
    const-class v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1VF;LX/1Ve;Lcom/facebook/feedback/ui/CommentComposerHelper;LX/21r;LX/1EN;LX/0ad;Landroid/content/res/Resources;LX/0tN;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/1VF;",
            "LX/1Ve;",
            "Lcom/facebook/feedback/ui/CommentComposerHelper;",
            "LX/21r;",
            "LX/1EN;",
            "LX/0ad;",
            "Landroid/content/res/Resources;",
            "LX/0tN;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357895
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 357896
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 357897
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 357898
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->e:LX/1VF;

    .line 357899
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->f:LX/1Ve;

    .line 357900
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->g:Lcom/facebook/feedback/ui/CommentComposerHelper;

    .line 357901
    iput-object p6, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->h:LX/21r;

    .line 357902
    iput-object p7, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->i:LX/1EN;

    .line 357903
    iput-object p8, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->k:LX/0ad;

    .line 357904
    iput-object p9, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->j:Landroid/content/res/Resources;

    .line 357905
    iput-object p10, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->l:LX/0tN;

    .line 357906
    iput-object p11, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->m:LX/0Ot;

    .line 357907
    iput-object p12, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->n:LX/0Ot;

    .line 357908
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;
    .locals 3

    .prologue
    .line 357887
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;

    monitor-enter v1

    .line 357888
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357889
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357890
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357891
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357892
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357893
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357894
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/36o;LX/36m;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/36o;",
            "LX/36m;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357837
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->k:LX/0ad;

    sget-short v1, LX/0wi;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357838
    iget-object v6, p3, LX/36m;->c:Lcom/facebook/widget/text/BetterTextView;

    if-nez v6, :cond_2

    .line 357839
    :cond_0
    :goto_0
    iget-object v0, p2, LX/36o;->c:Landroid/net/Uri;

    sget-object v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 357840
    iget-object v2, p3, LX/36m;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 357841
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->g:Lcom/facebook/feedback/ui/CommentComposerHelper;

    .line 357842
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357843
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    .line 357844
    iget-object v2, p3, LX/36m;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357845
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->e:LX/1VF;

    invoke-virtual {v0, p1}, LX/1VF;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357846
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->h:LX/21r;

    iget-object v2, p2, LX/36o;->a:LX/35I;

    iget v3, p2, LX/36o;->b:I

    iget-object v4, p2, LX/36o;->d:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->l:LX/0tN;

    invoke-virtual {v1}, LX/0tN;->d()Z

    move-result v5

    move-object v1, p3

    invoke-static/range {v0 .. v5}, LX/35G;->a(LX/21r;Landroid/view/View;LX/35I;ILcom/facebook/graphql/enums/ChainingSectionViewState;Z)V

    .line 357847
    :cond_1
    return-void

    .line 357848
    :cond_2
    iget-object v6, p3, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    if-nez v6, :cond_3

    .line 357849
    new-instance v6, Lcom/facebook/widget/text/FakeCursorHook;

    iget-object v7, p3, LX/36m;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {v6, v7}, Lcom/facebook/widget/text/FakeCursorHook;-><init>(Lcom/facebook/widget/text/BetterTextView;)V

    iput-object v6, p3, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    .line 357850
    :cond_3
    iget-object v6, p3, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    const/4 v9, 0x1

    .line 357851
    iget-boolean v8, v6, Lcom/facebook/widget/text/FakeCursorHook;->d:Z

    if-eqz v8, :cond_4

    .line 357852
    :goto_2
    iget-object v6, p3, LX/36m;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v7, p3, LX/36m;->e:Lcom/facebook/widget/text/FakeCursorHook;

    invoke-virtual {v6, v7}, Lcom/facebook/widget/text/BetterTextView;->a(LX/636;)V

    goto :goto_0

    .line 357853
    :cond_4
    iput-boolean v9, v6, Lcom/facebook/widget/text/FakeCursorHook;->d:Z

    .line 357854
    iput-boolean v9, v6, Lcom/facebook/widget/text/FakeCursorHook;->c:Z

    .line 357855
    iget-object v8, v6, Lcom/facebook/widget/text/FakeCursorHook;->a:Lcom/facebook/widget/text/BetterTextView;

    const-wide/16 v10, 0x1f4

    invoke-virtual {v8, v6, v10, v11}, Lcom/facebook/widget/text/BetterTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 357856
    :cond_5
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;
    .locals 13

    .prologue
    .line 357885
    new-instance v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {p0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v3

    check-cast v3, LX/1VF;

    invoke-static {p0}, LX/1Ve;->a(LX/0QB;)LX/1Ve;

    move-result-object v4

    check-cast v4, LX/1Ve;

    invoke-static {p0}, Lcom/facebook/feedback/ui/CommentComposerHelper;->a(LX/0QB;)Lcom/facebook/feedback/ui/CommentComposerHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedback/ui/CommentComposerHelper;

    invoke-static {p0}, LX/21r;->a(LX/0QB;)LX/21r;

    move-result-object v6

    check-cast v6, LX/21r;

    invoke-static {p0}, LX/1EN;->a(LX/0QB;)LX/1EN;

    move-result-object v7

    check-cast v7, LX/1EN;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v10

    check-cast v10, LX/0tN;

    const/16 v11, 0x17d

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x259

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1VF;LX/1Ve;Lcom/facebook/feedback/ui/CommentComposerHelper;LX/21r;LX/1EN;LX/0ad;Landroid/content/res/Resources;LX/0tN;LX/0Ot;LX/0Ot;)V

    .line 357886
    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 357884
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 357864
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 357865
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357866
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 357867
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    const/4 v4, -0x1

    const v5, 0x7f020a80

    invoke-direct {v2, p2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 357868
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/36n;

    invoke-direct {v2, p0, p2, p3}, LX/36n;-><init>(Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 357869
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 357870
    const/4 v2, 0x0

    .line 357871
    if-eqz v1, :cond_1

    .line 357872
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 357873
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v2, v1

    :goto_0
    move-object v1, p3

    .line 357874
    check-cast v1, LX/1Pt;

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v1, v3, v4}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object v1, p3

    .line 357875
    check-cast v1, LX/1Pr;

    new-instance v3, LX/35H;

    invoke-direct {v3, v0}, LX/35H;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v1, v3, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/35I;

    .line 357876
    iget-object v3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->j:Landroid/content/res/Resources;

    const v4, 0x7f0b1072

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 357877
    new-instance v4, LX/35J;

    invoke-direct {v4, v0}, LX/35J;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 357878
    check-cast p3, LX/1Pr;

    invoke-interface {p3, v4, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35K;

    .line 357879
    iget-object v4, v0, LX/35K;->b:Lcom/facebook/graphql/enums/ChainingSectionViewState;

    move-object v0, v4

    .line 357880
    new-instance v4, LX/36o;

    invoke-direct {v4, v1, v3, v2, v0}, LX/36o;-><init>(LX/35I;ILandroid/net/Uri;Lcom/facebook/graphql/enums/ChainingSectionViewState;)V

    return-object v4

    .line 357881
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "User acting as Page but no profile picture available!"

    invoke-virtual {v1, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 357882
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 357883
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v2, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3e432814

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 357863
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/36o;

    check-cast p4, LX/36m;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/36o;LX/36m;)V

    const/16 v1, 0x1f

    const v2, -0x2298de85

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357861
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357862
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/InlineCommentComposerPartDefinition;->e:LX/1VF;

    invoke-virtual {v0, p1}, LX/1VF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 357857
    check-cast p4, LX/36m;

    .line 357858
    invoke-virtual {p4}, LX/36m;->e()V

    .line 357859
    invoke-virtual {p4}, LX/36m;->clearAnimation()V

    .line 357860
    return-void
.end method
