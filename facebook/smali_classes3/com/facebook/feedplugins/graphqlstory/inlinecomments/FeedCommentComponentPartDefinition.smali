.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/14w;

.field private final e:LX/0tN;

.field private final f:LX/21i;

.field private final g:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/14w;LX/0tN;LX/21i;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357738
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 357739
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->d:LX/14w;

    .line 357740
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->e:LX/0tN;

    .line 357741
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->f:LX/21i;

    .line 357742
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->g:LX/1V0;

    .line 357743
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 357695
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->f:LX/21i;

    const/4 v1, 0x0

    .line 357696
    new-instance v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;-><init>(LX/21i;)V

    .line 357697
    iget-object v3, v0, LX/21i;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/36h;

    .line 357698
    if-nez v3, :cond_0

    .line 357699
    new-instance v3, LX/36h;

    invoke-direct {v3, v0}, LX/36h;-><init>(LX/21i;)V

    .line 357700
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/36h;->a$redex0(LX/36h;LX/1De;IILcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;)V

    .line 357701
    move-object v2, v3

    .line 357702
    move-object v1, v2

    .line 357703
    move-object v0, v1

    .line 357704
    iget-object v1, v0, LX/36h;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    iput-object p2, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357705
    iget-object v1, v0, LX/36h;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 357706
    move-object v0, v0

    .line 357707
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->e:LX/0tN;

    invoke-virtual {v1}, LX/0tN;->a()Z

    move-result v1

    .line 357708
    iget-object v2, v0, LX/36h;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    iput-boolean v1, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->e:Z

    .line 357709
    move-object v0, v0

    .line 357710
    iget-object v1, v0, LX/36h;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    iput-object p3, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->b:LX/1Pn;

    .line 357711
    iget-object v1, v0, LX/36h;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 357712
    move-object v0, v0

    .line 357713
    sget-object v1, LX/1EO;->NEWSFEED:LX/1EO;

    .line 357714
    iget-object v2, v0, LX/36h;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/InlineCommentComponent$InlineCommentComponentImpl;->f:LX/1EO;

    .line 357715
    iget-object v2, v0, LX/36h;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 357716
    move-object v0, v0

    .line 357717
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    .line 357718
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v0

    .line 357719
    invoke-static {v3}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 357720
    const v1, 0x7f020a75

    .line 357721
    sget-object v0, LX/1Ua;->d:LX/1Ua;

    .line 357722
    invoke-static {v3}, LX/14w;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->d:LX/14w;

    invoke-virtual {v5, v4}, LX/14w;->j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 357723
    const v1, 0x7f020a77

    .line 357724
    sget-object v0, LX/1Ua;->a:LX/1Ua;

    .line 357725
    :cond_1
    new-instance v4, LX/1X6;

    const/4 v5, -0x1

    invoke-direct {v4, v3, v0, v5, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 357726
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p1, p3, v4, v2}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;
    .locals 9

    .prologue
    .line 357727
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;

    monitor-enter v1

    .line 357728
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357729
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357732
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-static {v0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v6

    check-cast v6, LX/0tN;

    invoke-static {v0}, LX/21i;->a(LX/0QB;)LX/21i;

    move-result-object v7

    check-cast v7, LX/21i;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;-><init>(Landroid/content/Context;LX/14w;LX/0tN;LX/21i;LX/1V0;)V

    .line 357733
    move-object v0, v3

    .line 357734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 357744
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 357691
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357692
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 357693
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357694
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
