.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357547
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 357548
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->a:LX/0Ot;

    .line 357549
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->b:LX/0Ot;

    .line 357550
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->c:LX/0Ot;

    .line 357551
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;
    .locals 6

    .prologue
    .line 357552
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;

    monitor-enter v1

    .line 357553
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357554
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357555
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357556
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357557
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;

    const/16 v4, 0x907

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x906

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x905

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 357558
    move-object v0, v3

    .line 357559
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357560
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357561
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357562
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 357563
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 357564
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->c:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->b:LX/0Ot;

    new-instance v2, LX/21h;

    invoke-direct {v2, p2, v3, v3}, LX/21h;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/FeedCommentSelectorPartDefinition;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 357565
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357566
    const/4 v0, 0x1

    return v0
.end method
