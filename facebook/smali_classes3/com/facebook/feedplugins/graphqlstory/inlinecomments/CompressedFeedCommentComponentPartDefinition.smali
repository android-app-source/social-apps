.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/21h;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/14w;

.field private final e:LX/0tN;

.field private final f:LX/21g;

.field private final g:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/14w;LX/0tN;LX/21g;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 357599
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 357600
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->d:LX/14w;

    .line 357601
    iput-object p3, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->e:LX/0tN;

    .line 357602
    iput-object p4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->f:LX/21g;

    .line 357603
    iput-object p5, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->g:LX/1V0;

    .line 357604
    return-void
.end method

.method private a(LX/1De;LX/21h;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/21h;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 357605
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->f:LX/21g;

    const/4 v1, 0x0

    .line 357606
    new-instance v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    invoke-direct {v2, v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;-><init>(LX/21g;)V

    .line 357607
    iget-object v3, v0, LX/21g;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C6B;

    .line 357608
    if-nez v3, :cond_0

    .line 357609
    new-instance v3, LX/C6B;

    invoke-direct {v3, v0}, LX/C6B;-><init>(LX/21g;)V

    .line 357610
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/C6B;->a$redex0(LX/C6B;LX/1De;IILcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;)V

    .line 357611
    move-object v2, v3

    .line 357612
    move-object v1, v2

    .line 357613
    move-object v0, v1

    .line 357614
    iget-object v1, p2, LX/21h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357615
    iget-object v2, v0, LX/C6B;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357616
    iget-object v2, v0, LX/C6B;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 357617
    move-object v0, v0

    .line 357618
    iget-object v1, v0, LX/C6B;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    iput-object p3, v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->b:LX/1Pn;

    .line 357619
    iget-object v1, v0, LX/C6B;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 357620
    move-object v0, v0

    .line 357621
    iget-boolean v1, p2, LX/21h;->b:Z

    .line 357622
    iget-object v2, v0, LX/C6B;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    iput-boolean v1, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->d:Z

    .line 357623
    move-object v0, v0

    .line 357624
    sget-object v1, LX/1EO;->NEWSFEED:LX/1EO;

    .line 357625
    iget-object v2, v0, LX/C6B;->a:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;

    iput-object v1, v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/components/CompressedInlineCommentComponent$CompressedInlineCommentComponentImpl;->e:LX/1EO;

    .line 357626
    iget-object v2, v0, LX/C6B;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 357627
    move-object v0, v0

    .line 357628
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 357629
    iget-object v0, p2, LX/21h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357630
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 357631
    invoke-static {v2}, LX/182;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 357632
    const v0, 0x7f020a75

    .line 357633
    invoke-static {v2}, LX/14w;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->d:LX/14w;

    invoke-virtual {v4, v3}, LX/14w;->j(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 357634
    const v0, 0x7f020a77

    .line 357635
    :cond_1
    new-instance v3, LX/1X6;

    sget-object v4, LX/1Ua;->d:LX/1Ua;

    const/4 v5, -0x1

    invoke-direct {v3, v2, v4, v5, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    .line 357636
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p1, p3, v3, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;
    .locals 9

    .prologue
    .line 357637
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    monitor-enter v1

    .line 357638
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357639
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357640
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357641
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357642
    new-instance v3, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    invoke-static {v0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v6

    check-cast v6, LX/0tN;

    invoke-static {v0}, LX/21g;->a(LX/0QB;)LX/21g;

    move-result-object v7

    check-cast v7, LX/21g;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;-><init>(Landroid/content/Context;LX/14w;LX/0tN;LX/21g;LX/1V0;)V

    .line 357643
    move-object v0, v3

    .line 357644
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 357648
    check-cast p2, LX/21h;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->a(LX/1De;LX/21h;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 357649
    check-cast p2, LX/21h;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->a(LX/1De;LX/21h;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357650
    check-cast p1, LX/21h;

    .line 357651
    iget-boolean v0, p1, LX/21h;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->e:LX/0tN;

    invoke-virtual {v0}, LX/0tN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 357652
    check-cast p1, LX/21h;

    .line 357653
    iget-object v0, p1, LX/21h;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
