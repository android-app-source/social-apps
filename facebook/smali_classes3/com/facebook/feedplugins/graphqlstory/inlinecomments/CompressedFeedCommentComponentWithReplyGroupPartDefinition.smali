.class public Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0tN;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;


# direct methods
.method public constructor <init>(LX/0tN;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 357567
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 357568
    iput-object p1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->a:LX/0tN;

    .line 357569
    iput-object p2, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    .line 357570
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;
    .locals 5

    .prologue
    .line 357582
    const-class v1, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;

    monitor-enter v1

    .line 357583
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 357584
    sput-object v2, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 357585
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357586
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 357587
    new-instance p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;

    invoke-static {v0}, LX/0tN;->a(LX/0QB;)LX/0tN;

    move-result-object v3

    check-cast v3, LX/0tN;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;-><init>(LX/0tN;Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;)V

    .line 357588
    move-object v0, p0

    .line 357589
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 357590
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 357591
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 357592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 357593
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x1

    .line 357594
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357595
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 357596
    iget-object v1, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    new-instance v2, LX/21h;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v4}, LX/21h;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 357597
    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentPartDefinition;

    new-instance v1, LX/21h;

    invoke-direct {v1, p2, v4, v4}, LX/21h;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZZ)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 357598
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 357571
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 357572
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357573
    if-eqz v0, :cond_0

    .line 357574
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357575
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 357576
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357577
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->q()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 357578
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357579
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 357580
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 357581
    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/graphqlstory/inlinecomments/CompressedFeedCommentComponentWithReplyGroupPartDefinition;->a:LX/0tN;

    invoke-virtual {v0}, LX/0tN;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
