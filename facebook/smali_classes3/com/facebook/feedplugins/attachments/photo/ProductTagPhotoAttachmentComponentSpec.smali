.class public Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field public final c:LX/1eq;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8xB;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xN",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343782
    const-class v0, LX/1VW;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 343783
    new-instance v0, LX/1xM;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/1xM;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(LX/1eq;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1eq;",
            "LX/0Ot",
            "<",
            "LX/8xB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1xN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343785
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->c:LX/1eq;

    .line 343786
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->d:LX/0Ot;

    .line 343787
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->e:LX/0Ot;

    .line 343788
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;
    .locals 6

    .prologue
    .line 343789
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;

    monitor-enter v1

    .line 343790
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343791
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343792
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343793
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343794
    new-instance v4, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;

    invoke-static {v0}, LX/1eq;->a(LX/0QB;)LX/1eq;

    move-result-object v3

    check-cast v3, LX/1eq;

    const/16 v5, 0x17a5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x823

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;-><init>(LX/1eq;LX/0Ot;LX/0Ot;)V

    .line 343795
    move-object v0, v4

    .line 343796
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343797
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343798
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343799
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 343800
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343801
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 343802
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343803
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    const/4 v2, 0x0

    .line 343804
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->j()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    move v1, v2

    .line 343805
    :goto_0
    move v0, v1

    .line 343806
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 343807
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bt()Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhotoTagsConnection;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLProfile;

    .line 343808
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p0, 0xa7c5482

    if-ne v1, p0, :cond_3

    .line 343809
    const/4 v1, 0x1

    goto :goto_0

    .line 343810
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_4
    move v1, v2

    .line 343811
    goto :goto_0
.end method
