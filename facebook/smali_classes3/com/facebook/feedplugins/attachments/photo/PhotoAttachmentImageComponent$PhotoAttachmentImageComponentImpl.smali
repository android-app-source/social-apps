.class public final Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/22R;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/22Q;

.field public c:Lcom/facebook/graphql/model/GraphQLMedia;

.field public d:LX/1f6;

.field public e:LX/1aZ;

.field public f:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic h:LX/22R;


# direct methods
.method public constructor <init>(LX/22R;)V
    .locals 1

    .prologue
    .line 359666
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->h:LX/22R;

    .line 359667
    move-object v0, p1

    .line 359668
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 359669
    sget-object v0, LX/1VW;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 359670
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359671
    const-string v0, "PhotoAttachmentImageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 359672
    if-ne p0, p1, :cond_1

    .line 359673
    :cond_0
    :goto_0
    return v0

    .line 359674
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 359675
    goto :goto_0

    .line 359676
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;

    .line 359677
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 359678
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 359679
    if-eq v2, v3, :cond_0

    .line 359680
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 359681
    goto :goto_0

    .line 359682
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 359683
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 359684
    goto :goto_0

    .line 359685
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->b:LX/22Q;

    if-nez v2, :cond_7

    .line 359686
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 359687
    goto :goto_0

    .line 359688
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->c:Lcom/facebook/graphql/model/GraphQLMedia;

    if-nez v2, :cond_a

    .line 359689
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 359690
    goto :goto_0

    .line 359691
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->d:LX/1f6;

    if-nez v2, :cond_d

    .line 359692
    :cond_f
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 359693
    goto :goto_0

    .line 359694
    :cond_11
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->e:LX/1aZ;

    if-nez v2, :cond_10

    .line 359695
    :cond_12
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 359696
    goto/16 :goto_0

    .line 359697
    :cond_14
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->f:LX/1Pr;

    if-nez v2, :cond_13

    .line 359698
    :cond_15
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 359699
    goto/16 :goto_0

    .line 359700
    :cond_16
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentImageComponent$PhotoAttachmentImageComponentImpl;->g:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
