.class public Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/1xI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xI",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1et",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1V0;

.field public final g:LX/1w9;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/1xI;LX/1et;LX/1V0;LX/1w9;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343734
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 343735
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->d:LX/1xI;

    .line 343736
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->e:LX/1et;

    .line 343737
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->f:LX/1V0;

    .line 343738
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->g:LX/1w9;

    .line 343739
    sget-short v0, LX/1xL;->c:S

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->h:Z

    .line 343740
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 343741
    sget-object v0, LX/1Ua;->e:LX/1Ua;

    move-object v0, v0

    .line 343742
    invoke-static {p2, v0}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;

    move-result-object v0

    .line 343743
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->d:LX/1xI;

    const/4 v2, 0x0

    .line 343744
    new-instance v3, LX/C0z;

    invoke-direct {v3, v1}, LX/C0z;-><init>(LX/1xI;)V

    .line 343745
    iget-object v4, v1, LX/1xI;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C0y;

    .line 343746
    if-nez v4, :cond_0

    .line 343747
    new-instance v4, LX/C0y;

    invoke-direct {v4, v1}, LX/C0y;-><init>(LX/1xI;)V

    .line 343748
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/C0y;->a$redex0(LX/C0y;LX/1De;IILX/C0z;)V

    .line 343749
    move-object v3, v4

    .line 343750
    move-object v2, v3

    .line 343751
    move-object v1, v2

    .line 343752
    iget-object v2, v1, LX/C0y;->a:LX/C0z;

    iput-object p2, v2, LX/C0z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343753
    iget-object v2, v1, LX/C0y;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 343754
    move-object v1, v1

    .line 343755
    iget-object v2, v1, LX/C0y;->a:LX/C0z;

    iput-object p3, v2, LX/C0z;->b:LX/1Pb;

    .line 343756
    iget-object v2, v1, LX/C0y;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 343757
    move-object v1, v1

    .line 343758
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 343759
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;
    .locals 12

    .prologue
    .line 343768
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 343769
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343770
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343771
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343772
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343773
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/1xI;->a(LX/0QB;)LX/1xI;

    move-result-object v6

    check-cast v6, LX/1xI;

    invoke-static {v0}, LX/1et;->a(LX/0QB;)LX/1et;

    move-result-object v7

    check-cast v7, LX/1et;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    .line 343774
    new-instance v11, LX/1w9;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    invoke-static {v0}, LX/1xJ;->a(LX/0QB;)LX/1xJ;

    move-result-object v10

    check-cast v10, LX/1xJ;

    invoke-direct {v11, v9, v10}, LX/1w9;-><init>(LX/0iA;LX/1xJ;)V

    .line 343775
    move-object v9, v11

    .line 343776
    check-cast v9, LX/1w9;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/1xI;LX/1et;LX/1V0;LX/1w9;)V

    .line 343777
    move-object v0, v3

    .line 343778
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343779
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343780
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343760
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 343761
    if-nez p4, :cond_0

    .line 343762
    :goto_0
    return-void

    .line 343763
    :cond_0
    new-instance v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition$1;

    invoke-direct {v0, p0, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition$1;-><init>(Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;Lcom/facebook/components/feed/FeedComponentView;)V

    invoke-virtual {p4, v0}, Lcom/facebook/components/feed/FeedComponentView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1dV;",
            "TE;",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343764
    check-cast p3, LX/1Pn;

    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/components/feed/ComponentPartDefinition;->b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 343765
    if-nez p4, :cond_0

    .line 343766
    :goto_0
    return-void

    .line 343767
    :cond_0
    new-instance v0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition$2;

    invoke-direct {v0, p0, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition$2;-><init>(Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;Lcom/facebook/components/feed/FeedComponentView;)V

    invoke-virtual {p4, v0}, Lcom/facebook/components/feed/FeedComponentView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 343732
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 343733
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 343731
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x747c52ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 343724
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pb;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    const/16 v1, 0x1f

    const v2, 0x391ffad1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 343729
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343730
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 343727
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343728
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;LX/1dV;LX/1Pn;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 0

    .prologue
    .line 343726
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 343725
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/1dV;

    check-cast p3, LX/1Pb;

    check-cast p4, Lcom/facebook/components/feed/FeedComponentView;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/attachments/photo/ProductTagPhotoAttachmentComponentPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dV;LX/1Pb;Lcom/facebook/components/feed/FeedComponentView;)V

    return-void
.end method
