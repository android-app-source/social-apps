.class public Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static n:LX/0Xm;


# instance fields
.field private final e:LX/1xF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xF",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1et",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/1Ad;

.field private final h:LX/1qa;

.field private final i:LX/1V0;

.field private final j:LX/1xG;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343659
    const-class v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1xF;LX/1et;LX/1Ad;LX/1qa;LX/1V0;LX/1xG;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1xF;",
            "LX/1et;",
            "LX/1Ad;",
            "LX/1qa;",
            "LX/1V0;",
            "LX/1xG;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343649
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 343650
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->e:LX/1xF;

    .line 343651
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->f:LX/1et;

    .line 343652
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->g:LX/1Ad;

    .line 343653
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->h:LX/1qa;

    .line 343654
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->i:LX/1V0;

    .line 343655
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->j:LX/1xG;

    .line 343656
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->k:LX/0Ot;

    .line 343657
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->l:LX/0Ot;

    .line 343658
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 343604
    sget-object v0, LX/1Ua;->e:LX/1Ua;

    move-object v0, v0

    .line 343605
    invoke-static {p2, v0}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;

    move-result-object v3

    .line 343606
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->h:LX/1qa;

    .line 343607
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343608
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v2, LX/26P;->Photo:LX/26P;

    invoke-virtual {v1, v0, v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v4

    move-object v0, p3

    .line 343609
    check-cast v0, LX/1Pt;

    sget-object v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v4, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 343610
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->g:LX/1Ad;

    sget-object v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    .line 343611
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 343612
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343613
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 343614
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 343615
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Dh;

    invoke-virtual {v2}, LX/7Dh;->e()Z

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v1

    move-object v0, p3

    .line 343616
    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/1PT;)LX/7Dj;

    move-result-object v2

    .line 343617
    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->l:LX/0Ot;

    iget-object v7, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->j:LX/1xG;

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    invoke-static {v6, p2, v2, v7, v4}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/0Ot;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7Dj;LX/1xG;LX/1bf;)Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 343618
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343619
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/2oV;

    move-result-object v0

    .line 343620
    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->e:LX/1xF;

    const/4 v7, 0x0

    .line 343621
    new-instance v8, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    invoke-direct {v8, v6}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;-><init>(LX/1xF;)V

    .line 343622
    iget-object v9, v6, LX/1xF;->b:LX/0Zi;

    invoke-virtual {v9}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/C10;

    .line 343623
    if-nez v9, :cond_0

    .line 343624
    new-instance v9, LX/C10;

    invoke-direct {v9, v6}, LX/C10;-><init>(LX/1xF;)V

    .line 343625
    :cond_0
    invoke-static {v9, p1, v7, v7, v8}, LX/C10;->a$redex0(LX/C10;LX/1De;IILcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;)V

    .line 343626
    move-object v8, v9

    .line 343627
    move-object v7, v8

    .line 343628
    move-object v6, v7

    .line 343629
    iget-object v7, v6, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object p2, v7, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343630
    iget-object v7, v6, LX/C10;->e:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 343631
    move-object v6, v6

    .line 343632
    iget-object v7, v6, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object v1, v7, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->a:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 343633
    iget-object v7, v6, LX/C10;->e:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 343634
    move-object v1, v6

    .line 343635
    iget-object v6, v1, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object v2, v6, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->c:LX/7Dj;

    .line 343636
    iget-object v6, v1, LX/C10;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 343637
    move-object v1, v1

    .line 343638
    iget-object v2, v1, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object v4, v2, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->d:Landroid/view/View$OnClickListener;

    .line 343639
    iget-object v2, v1, LX/C10;->e:Ljava/util/BitSet;

    const/4 v6, 0x3

    invoke-virtual {v2, v6}, Ljava/util/BitSet;->set(I)V

    .line 343640
    move-object v1, v1

    .line 343641
    iget-object v2, v1, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object v0, v2, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->e:LX/2oV;

    .line 343642
    iget-object v2, v1, LX/C10;->e:Ljava/util/BitSet;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    .line 343643
    move-object v0, v1

    .line 343644
    iget-object v1, v0, LX/C10;->a:Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;

    iput-object v5, v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponent$SphericalPhotoAttachmentComponentImpl;->f:LX/1aZ;

    .line 343645
    iget-object v1, v0, LX/C10;->e:Ljava/util/BitSet;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 343646
    move-object v0, v0

    .line 343647
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 343648
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->i:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v3, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/2oV;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ")",
            "LX/2oV",
            "<",
            "LX/8xF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343601
    if-eqz p0, :cond_0

    .line 343602
    new-instance v0, LX/C11;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/C11;-><init>(Ljava/lang/String;)V

    .line 343603
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/1PT;)LX/7Dj;
    .locals 2
    .param p0    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 343594
    if-nez p0, :cond_0

    .line 343595
    sget-object v0, LX/7Dj;->UNKNOWN:LX/7Dj;

    .line 343596
    :goto_0
    return-object v0

    .line 343597
    :cond_0
    sget-object v0, LX/C14;->a:[I

    invoke-interface {p0}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 343598
    sget-object v0, LX/7Dj;->UNKNOWN:LX/7Dj;

    goto :goto_0

    .line 343599
    :pswitch_0
    sget-object v0, LX/7Dj;->NEWSFEED:LX/7Dj;

    goto :goto_0

    .line 343600
    :pswitch_1
    sget-object v0, LX/7Dj;->TIMELINE:LX/7Dj;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0Ot;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7Dj;LX/1xG;LX/1bf;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/7Dj;",
            "LX/1xG;",
            "LX/1bf;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 343570
    new-instance v0, LX/C13;

    move-object v1, p3

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/C13;-><init>(LX/1xG;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7Dj;LX/0Ot;LX/1bf;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;
    .locals 13

    .prologue
    .line 343583
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 343584
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343585
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343586
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343587
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343588
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1xF;->a(LX/0QB;)LX/1xF;

    move-result-object v5

    check-cast v5, LX/1xF;

    invoke-static {v0}, LX/1et;->a(LX/0QB;)LX/1et;

    move-result-object v6

    check-cast v6, LX/1et;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v9

    check-cast v9, LX/1V0;

    invoke-static {v0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v10

    check-cast v10, LX/1xG;

    const/16 v11, 0x3572

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x179e

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/1xF;LX/1et;LX/1Ad;LX/1qa;LX/1V0;LX/1xG;LX/0Ot;LX/0Ot;)V

    .line 343589
    move-object v0, v3

    .line 343590
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343591
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343592
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;
    .locals 6

    .prologue
    .line 343581
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/7Dx;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/List;

    move-result-object v0

    .line 343582
    invoke-static {p0}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v2

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-static {p2}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/7E3;->a(Ljava/util/List;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v3, ""

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 343580
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 343579
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 343571
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343572
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343573
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 343574
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343575
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 343576
    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v1, LX/1xO;->e:S

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    .line 343577
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v0, v0

    .line 343578
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
