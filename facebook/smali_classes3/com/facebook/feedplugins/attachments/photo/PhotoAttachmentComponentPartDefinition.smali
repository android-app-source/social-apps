.class public Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static j:LX/0Xm;


# instance fields
.field private final e:LX/1xN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xN",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1et",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final g:LX/1V0;

.field private final h:Z

.field private final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343841
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/1xN;LX/1et;LX/1V0;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 343834
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 343835
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->e:LX/1xN;

    .line 343836
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->f:LX/1et;

    .line 343837
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->g:LX/1V0;

    .line 343838
    sget-short v0, LX/1xO;->a:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->h:Z

    .line 343839
    sget-short v0, LX/1xO;->c:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->i:Z

    .line 343840
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 343812
    sget-object v0, LX/1Ua;->e:LX/1Ua;

    move-object v0, v0

    .line 343813
    invoke-static {p2, v0}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;

    move-result-object v0

    .line 343814
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->e:LX/1xN;

    invoke-virtual {v1, p1}, LX/1xN;->c(LX/1De;)LX/22O;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/22O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/22O;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/22O;->a(LX/1Pb;)LX/22O;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 343815
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;
    .locals 9

    .prologue
    .line 343823
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 343824
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343825
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343826
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343827
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343828
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/1xN;->a(LX/0QB;)LX/1xN;

    move-result-object v6

    check-cast v6, LX/1xN;

    invoke-static {v0}, LX/1et;->a(LX/0QB;)LX/1et;

    move-result-object v7

    check-cast v7, LX/1et;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/1xN;LX/1et;LX/1V0;)V

    .line 343829
    move-object v0, v3

    .line 343830
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343831
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343832
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 343822
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 343821
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 343819
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343820
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 343817
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343818
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 343816
    iget-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->i:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentPartDefinition;->d:LX/1Cz;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/components/feed/ComponentPartDefinition;->d()LX/1Cz;

    move-result-object v0

    goto :goto_0
.end method
