.class public Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/24n;",
        "TE;",
        "LX/BaB;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition",
            "<TE;",
            "LX/BaB;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343569
    new-instance v0, LX/1xD;

    invoke-direct {v0}, LX/1xD;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343546
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 343547
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    .line 343548
    sget-short v0, LX/1xE;->a:S

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->c:Z

    .line 343549
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 343558
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 343559
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343560
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343561
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343562
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343563
    new-instance p0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;LX/0ad;)V

    .line 343564
    move-object v0, p0

    .line 343565
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343566
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343567
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 343557
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 343554
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343555
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/BasePhotoAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 343556
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 343550
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343551
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343552
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 343553
    iget-boolean v1, p0, Lcom/facebook/feedplugins/attachments/photo/LivePhotoAttachmentPartDefinition;->c:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aL()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
