.class public Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/C15;",
        "TE;",
        "LX/8xF;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Ad;

.field private final e:LX/1qa;

.field private final f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final g:LX/1xG;

.field public final h:LX/1AV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1AV",
            "<",
            "LX/8xF;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/content/Context;

.field public final j:LX/0yc;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 343722
    const-class v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 343723
    new-instance v0, LX/1xH;

    invoke-direct {v0}, LX/1xH;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1qa;LX/0Ot;LX/1AV;LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1xG;Landroid/content/Context;LX/0yc;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qa;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;",
            "LX/1AV;",
            "LX/1Ad;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/1xG;",
            "Landroid/content/Context;",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "LX/BaD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343711
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 343712
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->e:LX/1qa;

    .line 343713
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->c:LX/0Ot;

    .line 343714
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->h:LX/1AV;

    .line 343715
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->d:LX/1Ad;

    .line 343716
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 343717
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->g:LX/1xG;

    .line 343718
    iput-object p7, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->i:Landroid/content/Context;

    .line 343719
    iput-object p8, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->j:LX/0yc;

    .line 343720
    iput-object p9, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->k:LX/0Ot;

    .line 343721
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;
    .locals 13

    .prologue
    .line 343700
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 343701
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343702
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343703
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343704
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343705
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const/16 v5, 0x3572

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1AV;->a(LX/0QB;)LX/1AV;

    move-result-object v6

    check-cast v6, LX/1AV;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v9

    check-cast v9, LX/1xG;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v11

    check-cast v11, LX/0yc;

    const/16 v12, 0x179e

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;-><init>(LX/1qa;LX/0Ot;LX/1AV;LX/1Ad;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1xG;Landroid/content/Context;LX/0yc;LX/0Ot;)V

    .line 343706
    move-object v0, v3

    .line 343707
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343708
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343709
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343710
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 343699
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 343660
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 343661
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343662
    move-object v2, v0

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 343663
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 343664
    sget-object v1, LX/1Ua;->e:LX/1Ua;

    .line 343665
    new-instance v3, LX/1X6;

    invoke-direct {v3, v0, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 343666
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->f:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 343667
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->e:LX/1qa;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    sget-object v3, LX/26P;->Photo:LX/26P;

    invoke-virtual {v0, v1, v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v3

    move-object v0, p3

    .line 343668
    check-cast v0, LX/1Pt;

    sget-object v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v0, v3, v1}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 343669
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->d:LX/1Ad;

    sget-object v1, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    move-object v0, p3

    .line 343670
    check-cast v0, LX/1Po;

    invoke-interface {v0}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/1PT;)LX/7Dj;

    move-result-object v5

    .line 343671
    invoke-static {p2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 343672
    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->i:Landroid/content/Context;

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->e()Z

    move-result v0

    invoke-static {v6, v2, v4, v0}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Z)Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    move-result-object v4

    .line 343673
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->k:LX/0Ot;

    iget-object v6, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->g:LX/1xG;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    invoke-static {v0, p2, v5, v6, v3}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(LX/0Ot;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7Dj;LX/1xG;LX/1bf;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 343674
    new-instance v0, LX/C15;

    invoke-static {v2}, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentComponentPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/2oV;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, LX/C15;-><init>(LX/1aZ;LX/2oV;Landroid/view/View$OnClickListener;Lcom/facebook/spherical/photo/model/SphericalPhotoParams;LX/7Dj;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5a07e313

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 343685
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/C15;

    check-cast p4, LX/8xF;

    .line 343686
    iget-object v1, p2, LX/C15;->a:LX/1aZ;

    invoke-virtual {p4, v1}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 343687
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->j:LX/0yc;

    invoke-virtual {v1}, LX/0yc;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343688
    const/4 v1, 0x1

    .line 343689
    iput-boolean v1, p4, LX/8wv;->t:Z

    .line 343690
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x42cbc33f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 343691
    :cond_0
    iget-object v2, p2, LX/C15;->d:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    sget-object v4, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 343692
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 343693
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    iget-object p3, p2, LX/C15;->e:LX/7Dj;

    invoke-virtual {p4, v2, v4, v1, p3}, LX/8xF;->b(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/7Dj;)V

    .line 343694
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->h:LX/1AV;

    iget-object v2, p2, LX/C15;->b:LX/2oV;

    invoke-virtual {v1, p4, v2}, LX/1AV;->a(Landroid/view/View;LX/2oV;)V

    .line 343695
    iget-object v1, p2, LX/C15;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/8xF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343696
    const/4 v1, 0x0

    .line 343697
    iput-boolean v1, p4, LX/8wv;->t:Z

    .line 343698
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 343679
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343680
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 343682
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/SphericalPhotoAttachmentPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343683
    const/4 v0, 0x1

    .line 343684
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 343675
    check-cast p4, LX/8xF;

    .line 343676
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/8wv;->setPreviewPhotoDraweeController(LX/1aZ;)V

    .line 343677
    invoke-virtual {p4}, LX/8xF;->q()V

    .line 343678
    return-void
.end method
