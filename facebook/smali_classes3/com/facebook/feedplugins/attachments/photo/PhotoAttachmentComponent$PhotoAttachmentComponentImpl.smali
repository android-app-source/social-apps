.class public final Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1xN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:LX/1f6;

.field public e:Z

.field public f:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:LX/1bf;

.field public i:Ljava/lang/Integer;

.field public j:LX/1aZ;

.field public final synthetic k:LX/1xN;


# direct methods
.method public constructor <init>(LX/1xN;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 359514
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->k:LX/1xN;

    .line 359515
    move-object v0, p1

    .line 359516
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 359517
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 359518
    iput-boolean v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->e:Z

    .line 359519
    iput-boolean v1, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->g:Z

    .line 359520
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359521
    const-string v0, "PhotoAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/1xN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 359522
    check-cast p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359523
    iget-object v0, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    .line 359524
    iget-object v0, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    .line 359525
    iget-object v0, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    .line 359526
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 359527
    if-ne p0, p1, :cond_1

    .line 359528
    :cond_0
    :goto_0
    return v0

    .line 359529
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 359530
    goto :goto_0

    .line 359531
    :cond_3
    check-cast p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359532
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 359533
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 359534
    if-eq v2, v3, :cond_0

    .line 359535
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 359536
    goto :goto_0

    .line 359537
    :cond_5
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 359538
    :cond_6
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 359539
    goto :goto_0

    .line 359540
    :cond_8
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 359541
    :cond_9
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 359542
    goto :goto_0

    .line 359543
    :cond_b
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_a

    .line 359544
    :cond_c
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 359545
    goto :goto_0

    .line 359546
    :cond_e
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->d:LX/1f6;

    if-nez v2, :cond_d

    .line 359547
    :cond_f
    iget-boolean v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->e:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 359548
    goto :goto_0

    .line 359549
    :cond_10
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 359550
    goto :goto_0

    .line 359551
    :cond_12
    iget-object v2, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->f:LX/1dQ;

    if-nez v2, :cond_11

    .line 359552
    :cond_13
    iget-boolean v2, p0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->g:Z

    iget-boolean v3, p1, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 359553
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 359554
    const/4 v1, 0x0

    .line 359555
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;

    .line 359556
    iput-object v1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->h:LX/1bf;

    .line 359557
    iput-object v1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->i:Ljava/lang/Integer;

    .line 359558
    iput-object v1, v0, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponent$PhotoAttachmentComponentImpl;->j:LX/1aZ;

    .line 359559
    return-object v0
.end method
