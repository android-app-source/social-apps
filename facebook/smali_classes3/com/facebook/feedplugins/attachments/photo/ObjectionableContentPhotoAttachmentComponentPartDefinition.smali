.class public Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1et",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1WM;

.field private final f:LX/1V0;

.field private final g:LX/1xC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xC",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1et;LX/1WM;LX/1V0;LX/1xC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 343514
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 343515
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->d:LX/1et;

    .line 343516
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->e:LX/1WM;

    .line 343517
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->f:LX/1V0;

    .line 343518
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->g:LX/1xC;

    .line 343519
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 343520
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343521
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 343522
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->d:LX/1et;

    move-object v0, p3

    check-cast v0, LX/1Po;

    invoke-virtual {v2, p2, v0}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/1f6;

    move-result-object v0

    .line 343523
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->g:LX/1xC;

    invoke-virtual {v2, p1}, LX/1xC;->c(LX/1De;)LX/C0p;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/C0p;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/C0p;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/C0p;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C0p;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/C0p;->a(LX/1Pb;)LX/C0p;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/C0p;->a(LX/1f6;)LX/C0p;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 343524
    sget-object v1, LX/1Ua;->e:LX/1Ua;

    move-object v1, v1

    .line 343525
    invoke-static {p2, v1}, LX/1et;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)LX/1X6;

    move-result-object v1

    .line 343526
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;
    .locals 9

    .prologue
    .line 343527
    const-class v1, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 343528
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 343529
    sput-object v2, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 343530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 343532
    new-instance v3, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1et;->a(LX/0QB;)LX/1et;

    move-result-object v5

    check-cast v5, LX/1et;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v6

    check-cast v6, LX/1WM;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1xC;->a(LX/0QB;)LX/1xC;

    move-result-object v8

    check-cast v8, LX/1xC;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/1et;LX/1WM;LX/1V0;LX/1xC;)V

    .line 343533
    move-object v0, v3

    .line 343534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 343535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 343537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 343538
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 343539
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 343540
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343541
    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/photo/ObjectionableContentPhotoAttachmentComponentPartDefinition;->e:LX/1WM;

    .line 343542
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 343543
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 343544
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 343545
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
