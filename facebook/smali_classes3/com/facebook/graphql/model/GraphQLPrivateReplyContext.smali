.class public final Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/0jS;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivateReplyContext$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/graphql/model/GraphQLPrivateReplyContext$Serializer;
.end annotation


# instance fields
.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLPage;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 344975
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 344974
    const-class v0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 344972
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 344973
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 344963
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 344964
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 344965
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 344966
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->a()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, LX/186;->a(IZ)V

    .line 344967
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 344968
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->k()Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, LX/186;->a(ILjava/lang/Enum;)V

    .line 344969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 344970
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0

    .line 344971
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->k()Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 344955
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 344956
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 344957
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 344958
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->j()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 344959
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;

    .line 344960
    iput-object v0, v1, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 344961
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 344962
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 344952
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 344953
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->e:Z

    .line 344954
    return-void
.end method

.method public final a()Z
    .locals 2
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 344949
    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_0

    .line 344950
    invoke-virtual {p0, v1, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 344951
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->e:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 344948
    const v0, -0x4d0e6b98

    return v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLPage;
    .locals 3
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 344942
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->f:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 344943
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->f:Lcom/facebook/graphql/model/GraphQLPage;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 344944
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->f:Lcom/facebook/graphql/model/GraphQLPage;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;
    .locals 4
    .annotation build Lcom/facebook/dracula/api/FieldOffset;
    .end annotation

    .prologue
    .line 344945
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->g:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/facebook/graphql/modelutil/BaseModel;->a_:Z

    if-eqz v0, :cond_1

    .line 344946
    :cond_0
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->g:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    iput-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->g:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    .line 344947
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/model/GraphQLPrivateReplyContext;->g:Lcom/facebook/graphql/enums/GraphQLPrivateReplyStatus;

    return-object v0
.end method
