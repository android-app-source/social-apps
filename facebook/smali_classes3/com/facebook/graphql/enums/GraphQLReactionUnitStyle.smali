.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum ADS_AFTER_PARTY_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum GRAVITY_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum GRAVITY_PAGE_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum QUESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum STORY_WITHOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum VERTICAL_COMPONENTS_RECYCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

.field public static final enum XHP:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 344595
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344596
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "STORY"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344597
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "UNIT_STACK"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344598
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "VERTICAL_COMPONENTS"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344599
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "PLACEHOLDER"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344600
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344601
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "ACORN_HIDE_CONFIRMATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344602
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "FLUSH_TO_BOTTOM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344603
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "XHP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->XHP:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344604
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "VERTICAL_COMPONENTS_RECYCLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_RECYCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344605
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "STORY_WITHOUT_HEADER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY_WITHOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344606
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "QUESTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344607
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "GRAVITY_PAGE_ABOUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_PAGE_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344608
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "GRAVITY_NUX"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344609
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "ADS_AFTER_PARTY_HIDE_CONFIRMATION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ADS_AFTER_PARTY_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344610
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    const-string v1, "VERTICAL_COMPONENTS_NO_GAPS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344611
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->XHP:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_RECYCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY_WITHOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_PAGE_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ADS_AFTER_PARTY_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 344612
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .locals 1

    .prologue
    .line 344613
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    .line 344614
    :goto_0
    return-object v0

    .line 344615
    :cond_1
    const-string v0, "STORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344616
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344617
    :cond_2
    const-string v0, "UNIT_STACK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344618
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNIT_STACK:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344619
    :cond_3
    const-string v0, "VERTICAL_COMPONENTS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 344620
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344621
    :cond_4
    const-string v0, "PLACEHOLDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 344622
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->PLACEHOLDER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344623
    :cond_5
    const-string v0, "VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 344624
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_WITH_TRANSPARENT_BACKGROUND:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344625
    :cond_6
    const-string v0, "ACORN_HIDE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 344626
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344627
    :cond_7
    const-string v0, "FLUSH_TO_BOTTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 344628
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->FLUSH_TO_BOTTOM:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344629
    :cond_8
    const-string v0, "XHP"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 344630
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->XHP:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344631
    :cond_9
    const-string v0, "VERTICAL_COMPONENTS_RECYCLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 344632
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_RECYCLE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344633
    :cond_a
    const-string v0, "VERTICAL_COMPONENTS_NO_GAPS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 344634
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->VERTICAL_COMPONENTS_NO_GAPS:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344635
    :cond_b
    const-string v0, "STORY_WITHOUT_HEADER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 344636
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->STORY_WITHOUT_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto :goto_0

    .line 344637
    :cond_c
    const-string v0, "QUESTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 344638
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->QUESTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto/16 :goto_0

    .line 344639
    :cond_d
    const-string v0, "GRAVITY_PAGE_ABOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 344640
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_PAGE_ABOUT:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto/16 :goto_0

    .line 344641
    :cond_e
    const-string v0, "GRAVITY_NUX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 344642
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->GRAVITY_NUX:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto/16 :goto_0

    .line 344643
    :cond_f
    const-string v0, "ADS_AFTER_PARTY_HIDE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 344644
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->ADS_AFTER_PARTY_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto/16 :goto_0

    .line 344645
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .locals 1

    .prologue
    .line 344646
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;
    .locals 1

    .prologue
    .line 344647
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    return-object v0
.end method
