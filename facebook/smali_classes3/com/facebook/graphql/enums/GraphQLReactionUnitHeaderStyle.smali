.class public final enum Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum DEPRECATED_ICON_FIELDS_ON_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum DESCRIPTIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum ICON_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum LARGE_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum PLACE_RANKING:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum PLACE_REVIEWS_WITH_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum PLACE_SPOTLIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 344580
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    invoke-direct {v0, v1, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344581
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "ICON"

    invoke-direct {v0, v1, v4}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344582
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "THIN_FACEPILE"

    invoke-direct {v0, v1, v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344583
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "ICON_INLINE_ACTION"

    invoke-direct {v0, v1, v6}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344584
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "DESCRIPTIVE"

    invoke-direct {v0, v1, v7}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DESCRIPTIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344585
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "ICON_PIVOT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344586
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "DEPRECATED_ICON_FIELDS_ON_UNIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DEPRECATED_ICON_FIELDS_ON_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344587
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "LARGE_ICON"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->LARGE_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344588
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "PLACE_RANKING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_RANKING:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344589
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "PLACE_SPOTLIGHT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_SPOTLIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344590
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "ICON_WITH_AUX_ACTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344591
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "ACORN_HIDE_CONFIRMATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344592
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "CENTER_ALIGNED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344593
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    const-string v1, "PLACE_REVIEWS_WITH_SECONDARY_TEXT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_REVIEWS_WITH_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344594
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DESCRIPTIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DEPRECATED_ICON_FIELDS_ON_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->LARGE_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_RANKING:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_SPOTLIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_REVIEWS_WITH_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 344579
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;
    .locals 1

    .prologue
    .line 344548
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    .line 344549
    :goto_0
    return-object v0

    .line 344550
    :cond_1
    const-string v0, "ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 344551
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344552
    :cond_2
    const-string v0, "THIN_FACEPILE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344553
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->THIN_FACEPILE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344554
    :cond_3
    const-string v0, "ICON_INLINE_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 344555
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_INLINE_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344556
    :cond_4
    const-string v0, "DESCRIPTIVE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 344557
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DESCRIPTIVE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344558
    :cond_5
    const-string v0, "ICON_PIVOT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 344559
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_PIVOT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344560
    :cond_6
    const-string v0, "DEPRECATED_ICON_FIELDS_ON_UNIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 344561
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->DEPRECATED_ICON_FIELDS_ON_UNIT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344562
    :cond_7
    const-string v0, "LARGE_ICON"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 344563
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->LARGE_ICON:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344564
    :cond_8
    const-string v0, "PLACE_RANKING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 344565
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_RANKING:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344566
    :cond_9
    const-string v0, "PLACE_SPOTLIGHT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 344567
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_SPOTLIGHT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344568
    :cond_a
    const-string v0, "ICON_WITH_AUX_ACTION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 344569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ICON_WITH_AUX_ACTION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344570
    :cond_b
    const-string v0, "ACORN_HIDE_CONFIRMATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 344571
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->ACORN_HIDE_CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto :goto_0

    .line 344572
    :cond_c
    const-string v0, "CENTER_ALIGNED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 344573
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto/16 :goto_0

    .line 344574
    :cond_d
    const-string v0, "PLACE_REVIEWS_WITH_SECONDARY_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 344575
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->PLACE_REVIEWS_WITH_SECONDARY_TEXT:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto/16 :goto_0

    .line 344576
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;
    .locals 1

    .prologue
    .line 344578
    const-class v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    return-object v0
.end method

.method public static values()[Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;
    .locals 1

    .prologue
    .line 344577
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;->$VALUES:[Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/graphql/enums/GraphQLReactionUnitHeaderStyle;

    return-object v0
.end method
