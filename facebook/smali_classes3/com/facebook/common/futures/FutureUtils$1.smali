.class public final Lcom/facebook/common/futures/FutureUtils$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/0TF;


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 0

    .prologue
    .line 341936
    iput-object p1, p0, Lcom/facebook/common/futures/FutureUtils$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p2, p0, Lcom/facebook/common/futures/FutureUtils$1;->b:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 341937
    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/futures/FutureUtils$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 341938
    iget-object v1, p0, Lcom/facebook/common/futures/FutureUtils$1;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 341939
    :goto_0
    return-void

    .line 341940
    :catch_0
    move-exception v0

    .line 341941
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Error;

    if-eqz v1, :cond_0

    .line 341942
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 341943
    :cond_0
    iget-object v1, p0, Lcom/facebook/common/futures/FutureUtils$1;->b:LX/0TF;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 341944
    :catch_1
    move-exception v0

    .line 341945
    iget-object v1, p0, Lcom/facebook/common/futures/FutureUtils$1;->b:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
