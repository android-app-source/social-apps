.class public Lcom/facebook/mqttlite/MqttService;
.super LX/051;
.source ""

# interfaces
.implements LX/0Xn;
.implements LX/0dN;


# static fields
.field public static final Y:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static Z:Ljava/lang/Boolean;

.field private static aa:Lcom/facebook/mqttlite/MqttService;


# instance fields
.field public A:LX/0Or;
    .annotation runtime Lcom/facebook/push/prefs/IsMobileOnlineAvailabilityEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/1qm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/1tL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/1tM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/1tN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:Landroid/content/pm/PackageManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/WhistleSingleThreadExecutorService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Or;
    .annotation runtime Lcom/facebook/mqttlite/persistence/HighestMqttPersistence;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/1tP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/1tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/0mh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/1tA;

.field public V:LX/1vQ;
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation
.end field

.field public W:LX/1vP;

.field private X:LX/05N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/05N",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private ab:Landroid/os/Looper;

.field public final ac:LX/1tC;

.field private ad:LX/1tU;

.field private final ae:LX/0jb;

.field private final af:LX/1tG;

.field public o:LX/1t1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1t2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/mqtt/debug/MqttStats;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1p6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1tJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1fT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Ma;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/push/mqtt/external/MqttThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Or;
    .annotation runtime Lcom/facebook/mqtt/capabilities/MqttEndpointCapability;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1sj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 342559
    const-class v0, Lcom/facebook/mqttlite/MqttService;

    sput-object v0, Lcom/facebook/mqttlite/MqttService;->Y:Ljava/lang/Class;

    .line 342560
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 342561
    invoke-direct {p0}, LX/051;-><init>()V

    .line 342562
    new-instance v0, LX/1tA;

    invoke-direct {v0}, LX/1tA;-><init>()V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342563
    new-instance v0, LX/1tC;

    invoke-direct {v0}, LX/1tC;-><init>()V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ac:LX/1tC;

    .line 342564
    new-instance v0, LX/0jb;

    invoke-direct {v0}, LX/0jb;-><init>()V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ae:LX/0jb;

    .line 342565
    new-instance v0, LX/1tF;

    invoke-direct {v0, p0}, LX/1tF;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->af:LX/1tG;

    return-void
.end method

.method private static a(LX/0Or;)LX/05N;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Or",
            "<TT;>;)",
            "LX/05N",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 342566
    new-instance v0, LX/1te;

    invoke-direct {v0, p0}, LX/1te;-><init>(LX/0Or;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/mqttlite/MqttService;Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;
    .locals 1

    .prologue
    .line 342567
    invoke-direct {p0, p1}, Lcom/facebook/mqttlite/MqttService;->a(Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;
    .locals 11

    .prologue
    .line 342568
    sget-object v1, LX/1u0;->DISCONNECTED:LX/1u0;

    .line 342569
    :try_start_0
    invoke-static {p1}, LX/1u0;->valueOf(Ljava/lang/String;)LX/1u0;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 342570
    :goto_0
    new-instance v0, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->w:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342571
    iget-wide v9, v4, LX/056;->f:J

    move-wide v4, v9

    .line 342572
    iget-object v6, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342573
    iget-wide v9, v6, LX/056;->h:J

    move-wide v6, v9

    .line 342574
    iget-object v8, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342575
    iget-boolean v9, v8, LX/056;->i:Z

    move v8, v9

    .line 342576
    invoke-direct/range {v0 .. v8}, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;-><init>(LX/1u0;JJJZ)V

    .line 342577
    return-object v0

    .line 342578
    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method private static a(Lcom/facebook/mqttlite/MqttService;LX/1t1;LX/1t2;Lcom/facebook/mqtt/debug/MqttStats;LX/1p6;LX/1tJ;Ljava/util/Set;LX/1Ma;Landroid/os/Handler;LX/0So;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1sj;LX/0Or;LX/1qm;LX/03V;LX/1tL;LX/0Xl;LX/0Xl;LX/1tM;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/1tN;Landroid/content/pm/PackageManager;LX/0ad;LX/0Ot;LX/0Ot;LX/0Or;LX/1tP;LX/01T;LX/1tQ;LX/0mh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/mqttlite/MqttService;",
            "LX/1t1;",
            "LX/1t2;",
            "Lcom/facebook/mqtt/debug/MqttStats;",
            "LX/1p6;",
            "LX/1tJ;",
            "Ljava/util/Set",
            "<",
            "LX/1fT;",
            ">;",
            "LX/1Ma;",
            "Landroid/os/Handler;",
            "LX/0So;",
            "LX/0Or",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1sj;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1qm;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1tL;",
            "LX/0Xl;",
            "LX/0Xl;",
            "LX/1tM;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1tN;",
            "Landroid/content/pm/PackageManager;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/liger/LigerHttpClientProvider;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2HQ;",
            ">;",
            "LX/1tP;",
            "LX/01T;",
            "LX/1tQ;",
            "LX/0mh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 342554
    iput-object p1, p0, Lcom/facebook/mqttlite/MqttService;->o:LX/1t1;

    iput-object p2, p0, Lcom/facebook/mqttlite/MqttService;->p:LX/1t2;

    iput-object p3, p0, Lcom/facebook/mqttlite/MqttService;->q:Lcom/facebook/mqtt/debug/MqttStats;

    iput-object p4, p0, Lcom/facebook/mqttlite/MqttService;->r:LX/1p6;

    iput-object p5, p0, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iput-object p6, p0, Lcom/facebook/mqttlite/MqttService;->t:Ljava/util/Set;

    iput-object p7, p0, Lcom/facebook/mqttlite/MqttService;->u:LX/1Ma;

    iput-object p8, p0, Lcom/facebook/mqttlite/MqttService;->v:Landroid/os/Handler;

    iput-object p9, p0, Lcom/facebook/mqttlite/MqttService;->w:LX/0So;

    iput-object p10, p0, Lcom/facebook/mqttlite/MqttService;->x:LX/0Or;

    iput-object p11, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p12, p0, Lcom/facebook/mqttlite/MqttService;->z:LX/1sj;

    iput-object p13, p0, Lcom/facebook/mqttlite/MqttService;->A:LX/0Or;

    iput-object p14, p0, Lcom/facebook/mqttlite/MqttService;->B:LX/1qm;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->C:LX/03V;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->D:LX/1tL;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->E:LX/0Xl;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->F:LX/0Xl;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->G:LX/1tM;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->H:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->I:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->K:LX/1tN;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->L:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->M:LX/0ad;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->N:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->O:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->P:LX/0Or;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->Q:LX/1tP;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->R:LX/01T;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->S:LX/1tQ;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->T:LX/0mh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 35

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v34

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/mqttlite/MqttService;

    invoke-static/range {v34 .. v34}, LX/1t1;->a(LX/0QB;)LX/1t1;

    move-result-object v3

    check-cast v3, LX/1t1;

    invoke-static/range {v34 .. v34}, LX/1t2;->a(LX/0QB;)LX/1t2;

    move-result-object v4

    check-cast v4, LX/1t2;

    invoke-static/range {v34 .. v34}, Lcom/facebook/mqtt/debug/MqttStats;->a(LX/0QB;)Lcom/facebook/mqtt/debug/MqttStats;

    move-result-object v5

    check-cast v5, Lcom/facebook/mqtt/debug/MqttStats;

    invoke-static/range {v34 .. v34}, LX/1p6;->a(LX/0QB;)LX/1p6;

    move-result-object v6

    check-cast v6, LX/1p6;

    invoke-static/range {v34 .. v34}, LX/1tJ;->a(LX/0QB;)LX/1tJ;

    move-result-object v7

    check-cast v7, LX/1tJ;

    invoke-static/range {v34 .. v34}, LX/1tK;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v8

    invoke-static/range {v34 .. v34}, LX/1Ma;->a(LX/0QB;)LX/1Ma;

    move-result-object v9

    check-cast v9, LX/1Ma;

    invoke-static/range {v34 .. v34}, LX/1rR;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v10

    check-cast v10, Landroid/os/Handler;

    invoke-static/range {v34 .. v34}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    const/16 v12, 0x15dd

    move-object/from16 v0, v34

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static/range {v34 .. v34}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v13

    check-cast v13, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v34 .. v34}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v14

    check-cast v14, LX/1sj;

    const/16 v15, 0x155d

    move-object/from16 v0, v34

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {v34 .. v34}, LX/1qm;->a(LX/0QB;)LX/1qm;

    move-result-object v16

    check-cast v16, LX/1qm;

    invoke-static/range {v34 .. v34}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v17

    check-cast v17, LX/03V;

    invoke-static/range {v34 .. v34}, LX/1tL;->a(LX/0QB;)LX/1tL;

    move-result-object v18

    check-cast v18, LX/1tL;

    invoke-static/range {v34 .. v34}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v19

    check-cast v19, LX/0Xl;

    invoke-static/range {v34 .. v34}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v20

    check-cast v20, LX/0Xl;

    invoke-static/range {v34 .. v34}, LX/1tM;->a(LX/0QB;)LX/1tM;

    move-result-object v21

    check-cast v21, LX/1tM;

    invoke-static/range {v34 .. v34}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v22

    check-cast v22, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v34 .. v34}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v23

    check-cast v23, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v34 .. v34}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v24

    check-cast v24, LX/0Uh;

    invoke-static/range {v34 .. v34}, LX/1tN;->a(LX/0QB;)LX/1tN;

    move-result-object v25

    check-cast v25, LX/1tN;

    invoke-static/range {v34 .. v34}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v26

    check-cast v26, Landroid/content/pm/PackageManager;

    invoke-static/range {v34 .. v34}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v27

    check-cast v27, LX/0ad;

    const/16 v28, 0x164e

    move-object/from16 v0, v34

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0xb66

    move-object/from16 v0, v34

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0xe0b

    move-object/from16 v0, v34

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v30

    invoke-static/range {v34 .. v34}, LX/1tP;->a(LX/0QB;)LX/1tP;

    move-result-object v31

    check-cast v31, LX/1tP;

    invoke-static/range {v34 .. v34}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v32

    check-cast v32, LX/01T;

    invoke-static/range {v34 .. v34}, LX/1tQ;->a(LX/0QB;)LX/1tQ;

    move-result-object v33

    check-cast v33, LX/1tQ;

    invoke-static/range {v34 .. v34}, LX/0mF;->a(LX/0QB;)LX/0mh;

    move-result-object v34

    check-cast v34, LX/0mh;

    invoke-static/range {v2 .. v34}, Lcom/facebook/mqttlite/MqttService;->a(Lcom/facebook/mqttlite/MqttService;LX/1t1;LX/1t2;Lcom/facebook/mqtt/debug/MqttStats;LX/1p6;LX/1tJ;Ljava/util/Set;LX/1Ma;Landroid/os/Handler;LX/0So;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1sj;LX/0Or;LX/1qm;LX/03V;LX/1tL;LX/0Xl;LX/0Xl;LX/1tM;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/1tN;Landroid/content/pm/PackageManager;LX/0ad;LX/0Ot;LX/0Ot;LX/0Or;LX/1tP;LX/01T;LX/1tQ;LX/0mh;)V

    return-void
.end method

.method private a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 342579
    new-instance v0, Lcom/facebook/mqttlite/MqttService$14;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/mqttlite/MqttService$14;-><init>(Lcom/facebook/mqttlite/MqttService;Ljava/lang/String;[B)V

    .line 342580
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->H:Ljava/util/concurrent/ExecutorService;

    const v2, 0x5666f0e1

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 342581
    return-void
.end method

.method public static a$redex0(Lcom/facebook/mqttlite/MqttService;Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 342582
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 342583
    iget-object v0, p0, LX/051;->h:LX/05i;

    const-class v1, LX/0BB;

    invoke-virtual {v0, v1}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BB;

    sget-object v1, LX/0BC;->StackReceivingLatencyMs:LX/0BC;

    iget-object v2, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/0BB;->a(LX/0BC;J)V

    .line 342584
    iget-object v0, p0, LX/051;->g:LX/05h;

    iget-object v1, p0, LX/051;->f:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    sub-long/2addr v2, p2

    .line 342585
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 p0, 0x0

    const-string p2, "operation"

    aput-object p2, v1, p0

    const/4 p0, 0x1

    aput-object p1, v1, p0

    const/4 p0, 0x2

    const-string p2, "timespan_ms"

    aput-object p2, v1, p0

    const/4 p0, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p0

    invoke-static {v1}, LX/06c;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 342586
    iget-object p0, v0, LX/05h;->f:LX/059;

    invoke-virtual {p0}, LX/059;->d()Landroid/net/NetworkInfo;

    move-result-object p0

    invoke-static {v1, p0}, LX/05h;->a(Ljava/util/Map;Landroid/net/NetworkInfo;)V

    .line 342587
    const-string p0, "mqtt_publish_arrive_processing_latency"

    invoke-virtual {v0, p0, v1}, LX/05h;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 342588
    :cond_0
    return-void
.end method

.method public static b(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/push/mqtt/ipc/SubscribeTopic;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/0AF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342589
    if-nez p0, :cond_0

    .line 342590
    const/4 v0, 0x0

    .line 342591
    :goto_0
    return-object v0

    .line 342592
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 342593
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;

    .line 342594
    new-instance v3, LX/0AF;

    .line 342595
    iget-object v4, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->a:Ljava/lang/String;

    move-object v4, v4

    .line 342596
    iget p0, v0, Lcom/facebook/push/mqtt/ipc/SubscribeTopic;->b:I

    move v0, p0

    .line 342597
    invoke-direct {v3, v4, v0}, LX/0AF;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 342598
    goto :goto_0
.end method

.method public static synthetic h(Lcom/facebook/mqttlite/MqttService;)V
    .locals 0

    .prologue
    .line 342599
    invoke-virtual {p0}, LX/052;->a()V

    return-void
.end method

.method public static s(Lcom/facebook/mqttlite/MqttService;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 342600
    sget-object v0, LX/053;->DISCONNECTED:LX/053;

    .line 342601
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v1}, LX/056;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 342602
    sget-object v0, LX/053;->CONNECTED:LX/053;

    .line 342603
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/053;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 342604
    :cond_1
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    invoke-virtual {v1}, LX/056;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342605
    sget-object v0, LX/053;->CONNECTING:LX/053;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/07W;)V
    .locals 4

    .prologue
    .line 342606
    invoke-super {p0, p1}, LX/051;->a(LX/07W;)V

    .line 342607
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->K:LX/1tN;

    invoke-virtual {v0}, LX/05I;->c()I

    move-result v1

    .line 342608
    iget-object v0, p0, LX/051;->h:LX/05i;

    const-class v2, LX/0BF;

    invoke-virtual {v0, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v2, LX/0BG;->FbnsNotificationDeliveryRetried:LX/0BG;

    invoke-virtual {v0, v2}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 342609
    return-void
.end method

.method public final a(LX/0Hs;)V
    .locals 2

    .prologue
    .line 342342
    sget-object v0, LX/0Hs;->FAILED_CONNECTION_REFUSED_NOT_AUTHORIZED:LX/0Hs;

    if-ne p1, v0, :cond_0

    .line 342343
    new-instance v0, Landroid/content/Intent;

    const-string v1, "ACTION_MQTT_NO_AUTH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342344
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->E:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 342345
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 4

    .prologue
    .line 342610
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1qz;->a:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 342611
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 342612
    :try_start_0
    const-string v2, "make_user_available_when_in_foreground"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 342613
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    const-string v2, "/set_client_settings"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/0AL;->FIRE_AND_FORGET:LX/0AL;

    invoke-virtual {v0, v2, v1, v3}, LX/056;->a(Ljava/lang/String;Ljava/lang/String;LX/0AL;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/0BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 342614
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 342615
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/facebook/mqttlite/MqttService;->Y:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 342616
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "notificationCounter="

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/051;->h:LX/05i;

    const-class v2, LX/0BF;

    invoke-virtual {v0, v2}, LX/05i;->a(Ljava/lang/Class;)LX/06r;

    move-result-object v0

    check-cast v0, LX/0BF;

    sget-object v2, LX/0BG;->FbnsNotificationReceived:LX/0BG;

    invoke-virtual {v0, v2}, LX/06q;->a(LX/06x;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 342617
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->X:LX/05N;

    invoke-interface {v0}, LX/05N;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342618
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "appId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->p:LX/1t2;

    invoke-virtual {v1}, LX/1t2;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 342619
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "userId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->o:LX/1t1;

    invoke-virtual {v1}, LX/1t1;->a()LX/06k;

    move-result-object v1

    invoke-virtual {v1}, LX/06k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 342620
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1p8;->c:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 342621
    invoke-static {v0}, LX/05V;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 342622
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fbnsToken=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342623
    :cond_0
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/1p8;->d:LX/0Tn;

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 342624
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fbnsliteToken=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "k"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 342625
    :goto_0
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "deviceId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->p:LX/1t2;

    invoke-virtual {v1}, LX/1t2;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 342626
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3}, LX/051;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 342627
    return-void

    :catch_0
    goto :goto_1

    :catch_1
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JZ)V
    .locals 2

    .prologue
    .line 342555
    const-string v0, "PUBLISH_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342556
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0AJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 342557
    :cond_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->q:Lcom/facebook/mqtt/debug/MqttStats;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/mqtt/debug/MqttStats;->a(Ljava/lang/String;JZ)V

    .line 342558
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 342340
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->C:LX/03V;

    invoke-virtual {v0, p1, p2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 342341
    return-void
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 9

    .prologue
    .line 342346
    :try_start_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 342347
    const-string v0, "/t_ec"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342348
    invoke-direct {p0, p1, p2}, Lcom/facebook/mqttlite/MqttService;->a(Ljava/lang/String;[B)V

    .line 342349
    :goto_0
    return-void

    .line 342350
    :cond_0
    new-instance v0, LX/2gX;

    invoke-direct {v0, p1, p2, p3, p4}, LX/2gX;-><init>(Ljava/lang/String;[BJ)V

    .line 342351
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->u:LX/1Ma;

    .line 342352
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.facebook.push.mqtt.ACTION_MQTT_PUBLISH_ARRIVED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342353
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 342354
    const-string v6, "topic_name"

    iget-object v7, v0, LX/2gX;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 342355
    const-string v6, "payload"

    iget-object v7, v0, LX/2gX;->b:[B

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 342356
    const-string v6, "received_time_ms"

    iget-wide v7, v0, LX/2gX;->c:J

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 342357
    move-object v4, v5

    .line 342358
    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 342359
    iget-object v4, v1, LX/1Ma;->c:LX/0Xl;

    invoke-interface {v4, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 342360
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->I:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/mqttlite/MqttService$13;

    invoke-direct {v2, p0, v0}, Lcom/facebook/mqttlite/MqttService$13;-><init>(Lcom/facebook/mqttlite/MqttService;LX/2gX;)V

    const v0, 0x59f5b765

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342361
    :catch_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->s:LX/1tJ;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5
    .annotation build Lcom/facebook/rti/common/guavalite/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 342362
    if-eqz p1, :cond_0

    .line 342363
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342364
    iget-object v1, v0, LX/056;->b:LX/072;

    .line 342365
    if-nez v1, :cond_1

    .line 342366
    :cond_0
    :goto_0
    return-void

    .line 342367
    :cond_1
    iget-object v2, v0, LX/056;->g:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-virtual {v2}, Lcom/facebook/rti/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v3

    invoke-virtual {v1}, LX/072;->g()J

    move-result-wide v1

    sub-long v1, v3, v1

    const-wide/32 v3, 0x493e0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 342368
    const-string v1, "SCREEN_ON"

    invoke-virtual {v0, v1}, LX/056;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/053;)Z
    .locals 3

    .prologue
    .line 342369
    sget-object v0, LX/053;->CONNECT_SENT:LX/053;

    if-ne p1, v0, :cond_1

    .line 342370
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->W:LX/1vP;

    invoke-virtual {p1}, LX/053;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/mqttlite/MqttService;->a(Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1vP;->a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V

    .line 342371
    const/4 v0, 0x1

    .line 342372
    :cond_0
    :goto_0
    return v0

    .line 342373
    :cond_1
    invoke-super {p0, p1}, LX/051;->a(LX/053;)Z

    move-result v0

    .line 342374
    if-eqz v0, :cond_0

    .line 342375
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->W:LX/1vP;

    invoke-virtual {p1}, LX/053;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/mqttlite/MqttService;->a(Ljava/lang/String;)Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1vP;->a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V

    goto :goto_0
.end method

.method public final b()Landroid/os/Looper;
    .locals 2

    .prologue
    .line 342376
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ab:Landroid/os/Looper;

    if-nez v0, :cond_0

    .line 342377
    new-instance v0, LX/1tI;

    invoke-direct {v0, p0}, LX/1tI;-><init>(Landroid/content/Context;)V

    .line 342378
    iget-object v1, v0, LX/1tI;->a:Landroid/os/Looper;

    move-object v0, v1

    .line 342379
    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ab:Landroid/os/Looper;

    .line 342380
    :cond_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ab:Landroid/os/Looper;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 342381
    invoke-super {p0}, LX/051;->d()V

    .line 342382
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 342383
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    .line 342384
    :cond_0
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->aa:Lcom/facebook/mqttlite/MqttService;

    if-ne v0, p0, :cond_1

    .line 342385
    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/mqttlite/MqttService;->aa:Lcom/facebook/mqttlite/MqttService;

    .line 342386
    :cond_1
    return-void
.end method

.method public final e()LX/05Q;
    .locals 15

    .prologue
    .line 342387
    invoke-static {p0}, LX/1mU;->a(Landroid/content/Context;)V

    .line 342388
    invoke-static {p0, p0}, Lcom/facebook/mqttlite/MqttService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 342389
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->aa:Lcom/facebook/mqttlite/MqttService;

    if-eqz v0, :cond_0

    .line 342390
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->aa:Lcom/facebook/mqttlite/MqttService;

    invoke-virtual {v0}, LX/051;->j()V

    .line 342391
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->F:LX/0Xl;

    const-string v1, "ACTION_MQTT_FORCE_REBIND"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 342392
    :cond_0
    sput-object p0, Lcom/facebook/mqttlite/MqttService;->aa:Lcom/facebook/mqttlite/MqttService;

    .line 342393
    new-instance v10, LX/05K;

    invoke-direct {v10}, LX/05K;-><init>()V

    .line 342394
    new-instance v0, LX/1tU;

    invoke-direct {v0, v10}, LX/1tU;-><init>(LX/05K;)V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ad:LX/1tU;

    .line 342395
    new-instance v0, LX/1vP;

    invoke-direct {v0}, LX/1vP;-><init>()V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->W:LX/1vP;

    .line 342396
    new-instance v0, LX/1vQ;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->N:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v3, 0x676

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v3

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v4, 0x213

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v4

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v5, 0x526

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v5

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v6, 0x622

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v6

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v7, 0x2da

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v7

    iget-object v8, p0, Lcom/facebook/mqttlite/MqttService;->O:LX/0Ot;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v9, 0x2db

    const/4 v11, 0x0

    invoke-virtual {v1, v9, v11}, LX/0Uh;->a(IZ)Z

    move-result v9

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, LX/1vQ;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;ZZZZZLX/0Ot;Z)V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->V:LX/1vQ;

    .line 342397
    new-instance v1, LX/1tV;

    invoke-direct {v1, p0}, LX/1tV;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342398
    new-instance v2, LX/1tW;

    invoke-direct {v2, p0}, LX/1tW;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342399
    new-instance v3, LX/1tX;

    invoke-direct {v3, p0}, LX/1tX;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342400
    new-instance v4, LX/1tY;

    invoke-direct {v4, p0}, LX/1tY;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342401
    new-instance v0, LX/1tZ;

    invoke-direct {v0, p0}, LX/1tZ;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    iput-object v0, p0, Lcom/facebook/mqttlite/MqttService;->X:LX/05N;

    .line 342402
    new-instance v5, LX/1ta;

    invoke-direct {v5, p0}, LX/1ta;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342403
    new-instance v6, LX/1tb;

    invoke-direct {v6, p0}, LX/1tb;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342404
    new-instance v7, LX/1tc;

    invoke-direct {v7, p0}, LX/1tc;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342405
    new-instance v8, LX/1td;

    invoke-direct {v8, p0}, LX/1td;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342406
    new-instance v9, LX/04v;

    invoke-direct {v9, p0}, LX/04v;-><init>(Landroid/content/Context;)V

    .line 342407
    new-instance v11, LX/05P;

    invoke-direct {v11}, LX/05P;-><init>()V

    .line 342408
    new-instance v12, LX/05Q;

    invoke-direct {v12}, LX/05Q;-><init>()V

    .line 342409
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v13, 0x676

    const/4 v14, 0x0

    invoke-virtual {v0, v13, v14}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v13, 0x208

    const/4 v14, 0x0

    invoke-virtual {v0, v13, v14}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 342410
    :goto_0
    iput-object p0, v11, LX/05P;->a:Landroid/content/Context;

    .line 342411
    move-object v11, v11

    .line 342412
    const-string v13, "MqttLite"

    move-object v13, v13

    .line 342413
    iput-object v13, v11, LX/05P;->b:Ljava/lang/String;

    .line 342414
    move-object v11, v11

    .line 342415
    iget-object v13, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    .line 342416
    iput-object v13, v11, LX/05P;->c:LX/056;

    .line 342417
    move-object v11, v11

    .line 342418
    iget-object v13, p0, LX/051;->n:LX/055;

    .line 342419
    iput-object v13, v11, LX/05P;->d:LX/055;

    .line 342420
    move-object v11, v11

    .line 342421
    iget-object v13, p0, Lcom/facebook/mqttlite/MqttService;->p:LX/1t2;

    .line 342422
    iput-object v13, v11, LX/05P;->e:LX/05G;

    .line 342423
    move-object v11, v11

    .line 342424
    iget-object v13, p0, Lcom/facebook/mqttlite/MqttService;->o:LX/1t1;

    .line 342425
    iput-object v13, v11, LX/05P;->f:LX/05F;

    .line 342426
    move-object v11, v11

    .line 342427
    const/4 v13, 0x0

    .line 342428
    iput-object v13, v11, LX/05P;->g:LX/05G;

    .line 342429
    move-object v11, v11

    .line 342430
    const/4 v13, 0x0

    .line 342431
    iput-object v13, v11, LX/05P;->h:LX/05F;

    .line 342432
    move-object v11, v11

    .line 342433
    iget-object v13, p0, Lcom/facebook/mqttlite/MqttService;->ad:LX/1tU;

    .line 342434
    iput-object v13, v11, LX/05P;->i:LX/05L;

    .line 342435
    move-object v11, v11

    .line 342436
    iput-object v10, v11, LX/05P;->j:LX/05K;

    .line 342437
    move-object v10, v11

    .line 342438
    iget-object v11, p0, Lcom/facebook/mqttlite/MqttService;->r:LX/1p6;

    .line 342439
    iput-object v11, v10, LX/05P;->k:LX/04p;

    .line 342440
    move-object v10, v10

    .line 342441
    invoke-static {v1}, Lcom/facebook/mqttlite/MqttService;->a(LX/0Or;)LX/05N;

    move-result-object v11

    .line 342442
    iput-object v11, v10, LX/05P;->l:LX/05N;

    .line 342443
    move-object v10, v10

    .line 342444
    iget-object v11, p0, Lcom/facebook/mqttlite/MqttService;->x:LX/0Or;

    invoke-static {v11}, Lcom/facebook/mqttlite/MqttService;->a(LX/0Or;)LX/05N;

    move-result-object v11

    .line 342445
    iput-object v11, v10, LX/05P;->m:LX/05N;

    .line 342446
    move-object v10, v10

    .line 342447
    iget-object v11, p0, Lcom/facebook/mqttlite/MqttService;->v:Landroid/os/Handler;

    .line 342448
    iput-object v11, v10, LX/05P;->n:Landroid/os/Handler;

    .line 342449
    move-object v10, v10

    .line 342450
    new-instance v11, LX/1tf;

    iget-object v13, p0, Lcom/facebook/mqttlite/MqttService;->z:LX/1sj;

    invoke-direct {v11, v13, v1}, LX/1tf;-><init>(LX/1sj;LX/0Or;)V

    .line 342451
    iput-object v11, v10, LX/05P;->o:LX/05R;

    .line 342452
    move-object v1, v10

    .line 342453
    iput-object v8, v1, LX/05P;->p:LX/05O;

    .line 342454
    move-object v1, v1

    .line 342455
    iput-object v9, v1, LX/05P;->r:LX/04v;

    .line 342456
    move-object v1, v1

    .line 342457
    iget-object v8, p0, Lcom/facebook/mqttlite/MqttService;->A:LX/0Or;

    invoke-static {v8}, Lcom/facebook/mqttlite/MqttService;->a(LX/0Or;)LX/05N;

    move-result-object v8

    .line 342458
    iput-object v8, v1, LX/05P;->s:LX/05N;

    .line 342459
    move-object v1, v1

    .line 342460
    iput-object v4, v1, LX/05P;->v:LX/05N;

    .line 342461
    move-object v1, v1

    .line 342462
    iput-object v5, v1, LX/05P;->w:LX/05N;

    .line 342463
    move-object v1, v1

    .line 342464
    iput-object v6, v1, LX/05P;->x:LX/05N;

    .line 342465
    move-object v1, v1

    .line 342466
    iput-object v2, v1, LX/05P;->t:LX/05N;

    .line 342467
    move-object v1, v1

    .line 342468
    iput-object v7, v1, LX/05P;->u:LX/05N;

    .line 342469
    move-object v1, v1

    .line 342470
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->B:LX/1qm;

    .line 342471
    iput-object v2, v1, LX/05P;->y:LX/05H;

    .line 342472
    move-object v1, v1

    .line 342473
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->V:LX/1vQ;

    .line 342474
    iput-object v2, v1, LX/05P;->z:LX/05M;

    .line 342475
    move-object v1, v1

    .line 342476
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->G:LX/1tM;

    .line 342477
    iput-object v2, v1, LX/05P;->A:Ljava/util/concurrent/atomic/AtomicReference;

    .line 342478
    move-object v1, v1

    .line 342479
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->p:LX/1t2;

    invoke-virtual {v2}, LX/1t2;->a()Ljava/lang/String;

    move-result-object v2

    .line 342480
    iput-object v2, v1, LX/05P;->B:Ljava/lang/String;

    .line 342481
    move-object v1, v1

    .line 342482
    iput-object v3, v1, LX/05P;->C:LX/05N;

    .line 342483
    move-object v1, v1

    .line 342484
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->M:LX/0ad;

    sget v3, LX/1tg;->b:I

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 342485
    iput v2, v1, LX/05P;->D:I

    .line 342486
    move-object v1, v1

    .line 342487
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v3, 0x205

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 342488
    iput-boolean v2, v1, LX/05P;->E:Z

    .line 342489
    move-object v1, v1

    .line 342490
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->S:LX/1tQ;

    .line 342491
    iget-object v3, v2, LX/1tQ;->a:LX/0Uh;

    const/16 v4, 0x207

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 342492
    iput-boolean v2, v1, LX/05P;->F:Z

    .line 342493
    move-object v1, v1

    .line 342494
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v3, 0x204

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 342495
    iput-boolean v2, v1, LX/05P;->G:Z

    .line 342496
    move-object v1, v1

    .line 342497
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v3, 0x20d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 342498
    iput-boolean v2, v1, LX/05P;->H:Z

    .line 342499
    move-object v1, v1

    .line 342500
    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->J:LX/0Uh;

    const/16 v3, 0x212

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    .line 342501
    iput-boolean v2, v1, LX/05P;->I:Z

    .line 342502
    move-object v1, v1

    .line 342503
    iput-boolean v0, v1, LX/05P;->J:Z

    .line 342504
    move-object v0, v1

    .line 342505
    invoke-virtual {v0}, LX/05P;->a()LX/05S;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, LX/05Q;->a(LX/05S;Ljava/util/List;)V

    .line 342506
    return-object v12

    .line 342507
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final f()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 342508
    invoke-super {p0}, LX/051;->f()V

    .line 342509
    sget-object v0, LX/01p;->l:LX/01q;

    invoke-static {p0, v0}, LX/01p;->a(Landroid/content/Context;LX/01q;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 342510
    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->ac:LX/1tC;

    iget-object v2, p0, LX/051;->b:LX/05Q;

    iget-object v2, v2, LX/05Q;->c:LX/059;

    iget-object v3, p0, LX/051;->b:LX/05Q;

    iget-object v3, v3, LX/05Q;->x:Landroid/os/PowerManager;

    iget-object v4, p0, LX/051;->b:LX/05Q;

    iget-object v4, v4, LX/05Q;->C:LX/05A;

    .line 342511
    iput-object v0, v1, LX/1tC;->a:Landroid/content/SharedPreferences;

    .line 342512
    iput-object v2, v1, LX/1tC;->b:LX/059;

    .line 342513
    iput-object v3, v1, LX/1tC;->c:Landroid/os/PowerManager;

    .line 342514
    iput-object v4, v1, LX/1tC;->d:LX/05A;

    .line 342515
    new-instance v4, LX/1th;

    iget-object v0, p0, LX/051;->b:LX/05Q;

    iget-object v0, v0, LX/05Q;->q:LX/05m;

    iget-object v1, p0, LX/051;->b:LX/05Q;

    iget-object v1, v1, LX/05Q;->f:LX/05h;

    iget-object v2, p0, LX/051;->b:LX/05Q;

    iget-object v2, v2, LX/05Q;->r:Lcom/facebook/rti/common/time/RealtimeSinceBootClock;

    invoke-direct {v4, v0, v1, v2}, LX/1th;-><init>(LX/05n;LX/05h;Lcom/facebook/rti/common/time/RealtimeSinceBootClock;)V

    .line 342516
    new-instance v3, LX/1ti;

    invoke-direct {v3, p0}, LX/1ti;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342517
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->U:LX/1tA;

    iget-object v1, p0, Lcom/facebook/mqttlite/MqttService;->ac:LX/1tC;

    iget-object v2, p0, Lcom/facebook/mqttlite/MqttService;->ad:LX/1tU;

    new-instance v5, LX/1tj;

    invoke-direct {v5}, LX/1tj;-><init>()V

    iget-object v6, p0, Lcom/facebook/mqttlite/MqttService;->M:LX/0ad;

    sget v7, LX/1tg;->c:I

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(II)I

    move-result v6

    .line 342518
    iput-object v1, v0, LX/1tA;->t:LX/1tC;

    .line 342519
    iput-object v2, v0, LX/1tA;->u:LX/1tU;

    .line 342520
    iput-object v3, v0, LX/1tA;->v:LX/05N;

    .line 342521
    iput-object v4, v0, LX/1tA;->w:LX/1th;

    .line 342522
    iput-object v5, v0, LX/1tA;->x:LX/1tj;

    .line 342523
    iput v6, v0, LX/1tA;->A:I

    .line 342524
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 342525
    sget-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342526
    sput-object v11, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    .line 342527
    iget-object v1, p0, LX/051;->g:LX/05h;

    const-string v2, "SERVICE_DOUBLE_BOOTSTRAP"

    .line 342528
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v4, v0

    .line 342529
    sget-object v0, LX/06b;->a:LX/06b;

    move-object v5, v0

    .line 342530
    iget-object v0, p0, LX/051;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    const/4 v7, -0x1

    const-wide/16 v8, 0x0

    move-object v3, v11

    move-object v10, v11

    invoke-virtual/range {v1 .. v10}, LX/05h;->a(Ljava/lang/String;Ljava/lang/String;LX/05B;LX/05B;ZIJLandroid/net/NetworkInfo;)V

    .line 342531
    :cond_0
    :goto_0
    return-void

    .line 342532
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/facebook/mqttlite/MqttService;->Z:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final getProperty(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 342533
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ae:LX/0jb;

    invoke-virtual {v0, p1}, LX/0jb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 7

    .prologue
    .line 342534
    invoke-super {p0}, LX/051;->h()V

    .line 342535
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->R:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_0

    .line 342536
    new-instance v0, LX/6mr;

    invoke-direct {v0, p0}, LX/6mr;-><init>(Lcom/facebook/mqttlite/MqttService;)V

    .line 342537
    iget-object v1, p0, LX/051;->i:LX/05b;

    .line 342538
    iget-object v2, v1, LX/05b;->d:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ScreenStateListener registration should be called on MqttThread. Current Looper:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/05Y;->a(ZLjava/lang/String;)V

    .line 342539
    iput-object v0, v1, LX/05b;->h:LX/0AD;

    .line 342540
    iget-object v2, v1, LX/05b;->b:Landroid/content/Context;

    iget-object v3, v1, LX/05b;->e:Landroid/content/BroadcastReceiver;

    sget-object v4, LX/05b;->a:Landroid/content/IntentFilter;

    const/4 v5, 0x0

    iget-object v6, v1, LX/05b;->d:Landroid/os/Handler;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 342541
    :cond_0
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1qz;->a:LX/0Tn;

    invoke-interface {v0, v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 342542
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->K:LX/1tN;

    invoke-virtual {v0}, LX/05I;->a()V

    .line 342543
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 342544
    invoke-super {p0}, LX/051;->i()V

    .line 342545
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->y:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1qz;->a:LX/0Tn;

    invoke-interface {v0, v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;LX/0dN;)V

    .line 342546
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->K:LX/1tN;

    invoke-virtual {v0}, LX/05I;->b()V

    .line 342547
    return-void
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 342548
    invoke-super {p0}, LX/051;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->o:LX/1t1;

    invoke-virtual {v0}, LX/1t1;->a()LX/06k;

    move-result-object v0

    sget-object v1, LX/06k;->a:LX/06k;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 342549
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->af:LX/1tG;

    return-object v0
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x7d4f5580

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 342550
    invoke-super {p0, p1, p2, p3}, LX/051;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    const/16 v2, 0x25

    const v3, -0x69cb8914

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 342551
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->P:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HQ;

    invoke-virtual {v0}, LX/2HQ;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final setProperty(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 342552
    iget-object v0, p0, Lcom/facebook/mqttlite/MqttService;->ae:LX/0jb;

    invoke-virtual {v0, p1, p2}, LX/0jb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 342553
    return-void
.end method
