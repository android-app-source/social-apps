.class public Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:LX/21x;

.field public final d:LX/0rS;

.field public final e:LX/21y;

.field public final f:Z

.field public final g:Z

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 358594
    new-instance v0, LX/21w;

    invoke-direct {v0}, LX/21w;-><init>()V

    sput-object v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 358579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358580
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->a:Ljava/lang/String;

    .line 358581
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    .line 358582
    invoke-static {}, LX/21x;->values()[LX/21x;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->c:LX/21x;

    .line 358583
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->d:LX/0rS;

    .line 358584
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    .line 358585
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->f:Z

    .line 358586
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->h:Ljava/lang/String;

    .line 358587
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->i:Ljava/lang/String;

    .line 358588
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->g:Z

    .line 358589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->j:Z

    .line 358590
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->k:Z

    .line 358591
    return-void

    :cond_0
    move v0, v2

    .line 358592
    goto :goto_0

    :cond_1
    move v1, v2

    .line 358593
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ILX/0rS;LX/21y;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 358577
    sget-object v3, LX/21x;->COMPLETE:LX/21x;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 358578
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V
    .locals 0

    .prologue
    .line 358564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358565
    iput-object p1, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->a:Ljava/lang/String;

    .line 358566
    iput p2, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    .line 358567
    iput-object p3, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->c:LX/21x;

    .line 358568
    iput-object p4, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->d:LX/0rS;

    .line 358569
    iput-object p5, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    .line 358570
    iput-object p6, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->h:Ljava/lang/String;

    .line 358571
    iput-object p7, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->i:Ljava/lang/String;

    .line 358572
    iput-boolean p8, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->g:Z

    .line 358573
    iput-boolean p9, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->j:Z

    .line 358574
    iput-boolean p10, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->f:Z

    .line 358575
    iput-boolean p11, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->k:Z

    .line 358576
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 358549
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358550
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358551
    iget v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 358552
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->c:LX/21x;

    invoke-virtual {v0}, LX/21x;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 358553
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->d:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358554
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->e:LX/21y;

    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358555
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 358556
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358557
    iget-object v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358558
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->g:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 358559
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->j:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 358560
    iget-boolean v0, p0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 358561
    return-void

    :cond_0
    move v0, v2

    .line 358562
    goto :goto_0

    :cond_1
    move v1, v2

    .line 358563
    goto :goto_1
.end method
