.class public final Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/220;
.implements LX/221;
.implements LX/224;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18520dba
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$Serializer;
.end annotation


# instance fields
.field private A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:I

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Z

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 359046
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 359045
    const-class v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 359043
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 359044
    return-void
.end method

.method private I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359041
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 359042
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359039
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 359040
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    return-object v0
.end method

.method private K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359037
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    .line 359038
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    return-object v0
.end method

.method private L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359035
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 359036
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    return-object v0
.end method

.method private M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358955
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    .line 358956
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    return-object v0
.end method

.method private N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358983
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    const/16 v1, 0x17

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    .line 358984
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    return-object v0
.end method

.method private O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358981
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 358982
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    return-object v0
.end method

.method private P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTopReactions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358979
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 358980
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    return-object v0
.end method

.method private Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358977
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->F:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->F:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    .line 358978
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->F:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    return-object v0
.end method

.method private R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358975
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 358976
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 358969
    iput p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->H:I

    .line 358970
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 358971
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 358972
    if-eqz v0, :cond_0

    .line 358973
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1d

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 358974
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 358963
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 358964
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 358965
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 358966
    if-eqz v0, :cond_0

    .line 358967
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 358968
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 358957
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    .line 358958
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 358959
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 358960
    if-eqz v0, :cond_0

    .line 358961
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 358962
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 359145
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 359146
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359147
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359148
    if-eqz v0, :cond_0

    .line 359149
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 359150
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 359047
    iput-object p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 359048
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359049
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359050
    if-eqz v0, :cond_0

    .line 359051
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 359052
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 359139
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->g:Z

    .line 359140
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359141
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359142
    if-eqz v0, :cond_0

    .line 359143
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 359144
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 359133
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->k:Z

    .line 359134
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359135
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359136
    if-eqz v0, :cond_0

    .line 359137
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 359138
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 359127
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->r:Z

    .line 359128
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359129
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359130
    if-eqz v0, :cond_0

    .line 359131
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 359132
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 359121
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->s:Z

    .line 359122
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 359123
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 359124
    if-eqz v0, :cond_0

    .line 359125
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 359126
    :cond_0
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 358921
    iput-boolean p1, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v:Z

    .line 358922
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 358923
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 358924
    if-eqz v0, :cond_0

    .line 358925
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 358926
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359120
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic B()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359119
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v0

    return-object v0
.end method

.method public final C()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 359117
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C:Ljava/util/List;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C:Ljava/util/List;

    .line 359118
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359116
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359115
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359114
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    return-object v0
.end method

.method public final G()I
    .locals 2

    .prologue
    .line 359112
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 359113
    iget v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->H:I

    return v0
.end method

.method public final synthetic H()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTopReactions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 359111
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 358985
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 358986
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 358987
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->l()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 358988
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 358989
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 358990
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 358991
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 358992
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 358993
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 358994
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->q()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 358995
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 358996
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 358997
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v14

    .line 358998
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 358999
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 359000
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 359001
    invoke-direct/range {p0 .. p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 359002
    const/16 v19, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 359003
    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->e:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359004
    const/16 v19, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->f:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359005
    const/16 v19, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->g:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359006
    const/16 v19, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->h:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359007
    const/16 v19, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->i:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359008
    const/16 v19, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->j:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359009
    const/16 v19, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->k:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359010
    const/16 v19, 0x7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->l:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359011
    const/16 v19, 0x8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->m:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 359012
    const/16 v19, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 359013
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 359014
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p:I

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v4, v1}, LX/186;->a(III)V

    .line 359015
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 359016
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 359017
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 359018
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 359019
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 359020
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 359021
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 359022
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 359023
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 359024
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 359025
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 359026
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 359027
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 359028
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 359029
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 359030
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 359031
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 359032
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->H:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 359033
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 359034
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 359053
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 359054
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 359055
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 359056
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 359057
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359058
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 359059
    :cond_0
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 359060
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 359061
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 359062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359063
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->u:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    .line 359064
    :cond_1
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 359065
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    .line 359066
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 359067
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359068
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    .line 359069
    :cond_2
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 359070
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 359071
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 359072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359073
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 359074
    :cond_3
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 359075
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    .line 359076
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 359077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359078
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    .line 359079
    :cond_4
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 359080
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    .line 359081
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 359082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359083
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    .line 359084
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 359085
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 359086
    if-eqz v2, :cond_6

    .line 359087
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359088
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->C:Ljava/util/List;

    move-object v1, v0

    .line 359089
    :cond_6
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 359090
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 359091
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 359092
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359093
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 359094
    :cond_7
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 359095
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 359096
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->P()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 359097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359098
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->E:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    .line 359099
    :cond_8
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 359100
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    .line 359101
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 359102
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359103
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->F:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    .line 359104
    :cond_9
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 359105
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 359106
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->R()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 359107
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    .line 359108
    iput-object v0, v1, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    .line 359109
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 359110
    if-nez v1, :cond_b

    :goto_0
    return-object p0

    :cond_b
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 358927
    new-instance v0, LX/59P;

    invoke-direct {v0, p1}, LX/59P;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358920
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 358904
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 358905
    invoke-virtual {p1, p2, v1}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->e:Z

    .line 358906
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->f:Z

    .line 358907
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->g:Z

    .line 358908
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->h:Z

    .line 358909
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->i:Z

    .line 358910
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->j:Z

    .line 358911
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->k:Z

    .line 358912
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->l:Z

    .line 358913
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->m:Z

    .line 358914
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p:I

    .line 358915
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->r:Z

    .line 358916
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->s:Z

    .line 358917
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v:Z

    .line 358918
    const/16 v0, 0x1d

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->H:I

    .line 358919
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 358842
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358843
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358845
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 358846
    :goto_0
    return-void

    .line 358847
    :cond_0
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358848
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358850
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 358851
    :cond_1
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358852
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358854
    const/16 v0, 0xd

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 358855
    :cond_2
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358856
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->w()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358858
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 358859
    :cond_3
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358860
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->o()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358862
    const/16 v0, 0x11

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 358863
    :cond_4
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 358864
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v0

    .line 358865
    if-eqz v0, :cond_b

    .line 358866
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358867
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358868
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358869
    :cond_5
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 358870
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 358871
    if-eqz v0, :cond_b

    .line 358872
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358873
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358874
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358875
    :cond_6
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 358876
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v0

    .line 358877
    if-eqz v0, :cond_b

    .line 358878
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358879
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358880
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358881
    :cond_7
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 358882
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v0

    .line 358883
    if-eqz v0, :cond_b

    .line 358884
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358885
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358886
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358887
    :cond_8
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358888
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 358889
    if-eqz v0, :cond_b

    .line 358890
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358891
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358892
    iput v2, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358893
    :cond_9
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 358894
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 358895
    if-eqz v0, :cond_b

    .line 358896
    invoke-virtual {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358897
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358898
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358899
    :cond_a
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 358900
    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->G()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 358901
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 358902
    const/16 v0, 0x1d

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 358903
    :cond_b
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 358833
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358834
    check-cast p2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;)V

    .line 358835
    :cond_0
    :goto_0
    return-void

    .line 358836
    :cond_1
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358837
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;)V

    goto :goto_0

    .line 358838
    :cond_2
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358839
    check-cast p2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;)V

    goto :goto_0

    .line 358840
    :cond_3
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358841
    check-cast p2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsCountFieldsModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 358772
    const-string v0, "can_viewer_comment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358773
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(Z)V

    .line 358774
    :cond_0
    :goto_0
    return-void

    .line 358775
    :cond_1
    const-string v0, "can_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358776
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->b(Z)V

    goto :goto_0

    .line 358777
    :cond_2
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358778
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->c(Z)V

    goto :goto_0

    .line 358779
    :cond_3
    const-string v0, "have_comments_been_disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358780
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->d(Z)V

    goto :goto_0

    .line 358781
    :cond_4
    const-string v0, "is_viewer_subscribed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 358782
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->e(Z)V

    goto :goto_0

    .line 358783
    :cond_5
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 358784
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v0

    .line 358785
    if-eqz v0, :cond_0

    .line 358786
    if-eqz p3, :cond_6

    .line 358787
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    .line 358788
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;->a(I)V

    .line 358789
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->x:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    goto :goto_0

    .line 358790
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;->a(I)V

    goto :goto_0

    .line 358791
    :cond_7
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358792
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 358793
    if-eqz v0, :cond_0

    .line 358794
    if-eqz p3, :cond_8

    .line 358795
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    .line 358796
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    .line 358797
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->y:Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    goto/16 :goto_0

    .line 358798
    :cond_8
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;->a(I)V

    goto/16 :goto_0

    .line 358799
    :cond_9
    const-string v0, "reshares.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 358800
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->M()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    move-result-object v0

    .line 358801
    if-eqz v0, :cond_0

    .line 358802
    if-eqz p3, :cond_a

    .line 358803
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    .line 358804
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;->a(I)V

    .line 358805
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->A:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;

    goto/16 :goto_0

    .line 358806
    :cond_a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ResharesModel;->a(I)V

    goto/16 :goto_0

    .line 358807
    :cond_b
    const-string v0, "seen_by.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 358808
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->N()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    move-result-object v0

    .line 358809
    if-eqz v0, :cond_0

    .line 358810
    if-eqz p3, :cond_c

    .line 358811
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    .line 358812
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->a(I)V

    .line 358813
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->B:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;

    goto/16 :goto_0

    .line 358814
    :cond_c
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$SeenByModel;->a(I)V

    goto/16 :goto_0

    .line 358815
    :cond_d
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 358816
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 358817
    if-eqz v0, :cond_0

    .line 358818
    if-eqz p3, :cond_e

    .line 358819
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 358820
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    .line 358821
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 358822
    :cond_e
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->a(I)V

    goto/16 :goto_0

    .line 358823
    :cond_f
    const-string v0, "top_level_comments.total_count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 358824
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->O()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    move-result-object v0

    .line 358825
    if-eqz v0, :cond_0

    .line 358826
    if-eqz p3, :cond_10

    .line 358827
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    .line 358828
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    .line 358829
    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->D:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;

    goto/16 :goto_0

    .line 358830
    :cond_10
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsCommentsGraphQLModels$TopLevelCommentsConnectionFragmentModel;->b(I)V

    goto/16 :goto_0

    .line 358831
    :cond_11
    const-string v0, "viewer_feedback_reaction_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358832
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->a(I)V

    goto/16 :goto_0
.end method

.method public final ac_()Z
    .locals 2

    .prologue
    .line 358770
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358771
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->i:Z

    return v0
.end method

.method public final ad_()Z
    .locals 2

    .prologue
    .line 358753
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358754
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->j:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 358767
    new-instance v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;

    invoke-direct {v0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;-><init>()V

    .line 358768
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 358769
    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 358765
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358766
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->e:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 358763
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358764
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 358761
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358762
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 358760
    const v0, 0x55065833

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 358758
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358759
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 358757
    const v0, -0x78fb05b

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 358755
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358756
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 358928
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358929
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->m:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358930
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->o:Ljava/lang/String;

    .line 358931
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 358932
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358933
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->r:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358934
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->t:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->t:Ljava/lang/String;

    .line 358935
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 358936
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358937
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->v:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358938
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->w:Ljava/lang/String;

    .line 358939
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358940
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->z:Ljava/lang/String;

    const/16 v1, 0x15

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->z:Ljava/lang/String;

    .line 358941
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic r()LX/59N;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358942
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->Q()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$ViewerActsAsPageModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 358943
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358944
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->l:Z

    return v0
.end method

.method public final synthetic t()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358945
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->I()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final u()I
    .locals 2

    .prologue
    .line 358946
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358947
    iget v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->p:I

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358948
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->q:Ljava/lang/String;

    .line 358949
    iget-object v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 358950
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 358951
    iget-boolean v0, p0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->s:Z

    return v0
.end method

.method public final synthetic x()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358952
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->J()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358953
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel$LikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic z()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 358954
    invoke-direct {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsFeedbackGraphQLModels$NewsFeedDefaultsCompleteFeedbackModel;->L()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$CompleteReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method
