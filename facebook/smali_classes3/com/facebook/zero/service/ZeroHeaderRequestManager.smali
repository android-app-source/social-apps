.class public Lcom/facebook/zero/service/ZeroHeaderRequestManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0dc;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile l:Lcom/facebook/zero/service/ZeroHeaderRequestManager;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation runtime Lcom/facebook/zero/sdk/annotations/IsZeroHeaderRequestFeatureEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/0Uh;

.field private k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 346342
    const-class v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    sput-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a:Ljava/lang/Class;

    .line 346343
    const-class v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/sdk/annotations/IsZeroHeaderRequestFeatureEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Xl;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 346332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346333
    iput-object p1, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->c:LX/0Ot;

    .line 346334
    iput-object p2, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->d:LX/0Ot;

    .line 346335
    iput-object p3, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->e:LX/0Ot;

    .line 346336
    iput-object p4, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->f:LX/0Or;

    .line 346337
    iput-object p5, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->g:LX/0Ot;

    .line 346338
    iput-object p6, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->h:LX/0Ot;

    .line 346339
    iput-object p7, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 346340
    iput-object p8, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->j:LX/0Uh;

    .line 346341
    return-void
.end method

.method public static a(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Ljava/lang/String;Landroid/os/Bundle;)LX/1ML;
    .locals 6

    .prologue
    .line 346329
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x36c9f564

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 346330
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/1ML;->updatePriority(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 346331
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/zero/service/ZeroHeaderRequestManager;
    .locals 12

    .prologue
    .line 346316
    sget-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->l:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    if-nez v0, :cond_1

    .line 346317
    const-class v1, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    monitor-enter v1

    .line 346318
    :try_start_0
    sget-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->l:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 346319
    if-eqz v2, :cond_0

    .line 346320
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 346321
    new-instance v3, Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    const/16 v4, 0x542

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xbc

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13ec

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x38a

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1ce

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x140f

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V

    .line 346322
    move-object v0, v3

    .line 346323
    sput-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->l:Lcom/facebook/zero/service/ZeroHeaderRequestManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346324
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 346325
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 346326
    :cond_1
    sget-object v0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->l:Lcom/facebook/zero/service/ZeroHeaderRequestManager;

    return-object v0

    .line 346327
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 346328
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/zero/service/ZeroHeaderRequestManager;IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 346344
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->j:LX/0Uh;

    const/16 v1, 0x682

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 346345
    if-eqz v0, :cond_0

    .line 346346
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "zero_header_request_result"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 346347
    const-string v0, "ttl"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 346348
    const-string v0, "should_call_header_api"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 346349
    const-string v0, "request_reason"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 346350
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 346351
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346310
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 346311
    new-instance v1, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;

    invoke-virtual {p1}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/zero/server/FetchZeroHeaderRequestResult;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/zero/server/SendZeroHeaderRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 346312
    const-string v2, "sendZeroHeaderRequestParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 346313
    const-string v1, "send_zero_header_request"

    invoke-static {p0, v1, v0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Ljava/lang/String;Landroid/os/Bundle;)LX/1ML;

    move-result-object v0

    move-object v1, v0

    .line 346314
    new-instance v2, LX/7Xf;

    invoke-direct {v2, p0}, LX/7Xf;-><init>(Lcom/facebook/zero/service/ZeroHeaderRequestManager;)V

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 346315
    return-object v1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 346307
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 346308
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 346309
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346296
    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346297
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "Zero header request not sent because header request feature is not enabled"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 346298
    :goto_0
    return-object v0

    .line 346299
    :cond_0
    invoke-direct {p0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->c()V

    .line 346300
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 346301
    new-instance v2, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pA;

    invoke-virtual {v0}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4, p1}, Lcom/facebook/zero/server/FetchZeroHeaderRequestParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 346302
    const-string v0, "fetchZeroHeaderRequestParams"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 346303
    const-string v0, "fetch_zero_header_request"

    invoke-static {p0, v0, v1}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->a(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Ljava/lang/String;Landroid/os/Bundle;)LX/1ML;

    move-result-object v0

    move-object v0, v0

    .line 346304
    iput-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 346305
    iget-object v1, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/7Xd;

    invoke-direct {v2, p0}, LX/7Xd;-><init>(Lcom/facebook/zero/service/ZeroHeaderRequestManager;)V

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 346306
    iget-object v1, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/7Xe;

    invoke-direct {v2, p0, p2}, LX/7Xe;-><init>(Lcom/facebook/zero/service/ZeroHeaderRequestManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346293
    invoke-direct {p0}, Lcom/facebook/zero/service/ZeroHeaderRequestManager;->c()V

    .line 346294
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 346295
    return-object v0
.end method
