.class public Lcom/facebook/mqtt/debug/MqttStats;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:Lcom/facebook/mqtt/debug/MqttStats;


# instance fields
.field private final a:LX/0So;

.field private b:J

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1tT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 342628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342629
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/mqtt/debug/MqttStats;->c:Ljava/util/Map;

    .line 342630
    iput-object p1, p0, Lcom/facebook/mqtt/debug/MqttStats;->a:LX/0So;

    .line 342631
    invoke-interface {p1}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/mqtt/debug/MqttStats;->b:J

    .line 342632
    return-void
.end method

.method private static declared-synchronized a(Lcom/facebook/mqtt/debug/MqttStats;Ljava/lang/String;)LX/1tT;
    .locals 2
    .param p0    # Lcom/facebook/mqtt/debug/MqttStats;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 342633
    monitor-enter p0

    if-nez p1, :cond_0

    .line 342634
    :try_start_0
    const-string p1, "<not-specified>"

    .line 342635
    :cond_0
    iget-object v0, p0, Lcom/facebook/mqtt/debug/MqttStats;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1tT;

    .line 342636
    if-nez v0, :cond_1

    .line 342637
    new-instance v0, LX/1tT;

    invoke-direct {v0, p1}, LX/1tT;-><init>(Ljava/lang/String;)V

    .line 342638
    iget-object v1, p0, Lcom/facebook/mqtt/debug/MqttStats;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342639
    :cond_1
    monitor-exit p0

    return-object v0

    .line 342640
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/mqtt/debug/MqttStats;
    .locals 4

    .prologue
    .line 342641
    sget-object v0, Lcom/facebook/mqtt/debug/MqttStats;->d:Lcom/facebook/mqtt/debug/MqttStats;

    if-nez v0, :cond_1

    .line 342642
    const-class v1, Lcom/facebook/mqtt/debug/MqttStats;

    monitor-enter v1

    .line 342643
    :try_start_0
    sget-object v0, Lcom/facebook/mqtt/debug/MqttStats;->d:Lcom/facebook/mqtt/debug/MqttStats;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 342644
    if-eqz v2, :cond_0

    .line 342645
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 342646
    new-instance p0, Lcom/facebook/mqtt/debug/MqttStats;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, Lcom/facebook/mqtt/debug/MqttStats;-><init>(LX/0So;)V

    .line 342647
    move-object v0, p0

    .line 342648
    sput-object v0, Lcom/facebook/mqtt/debug/MqttStats;->d:Lcom/facebook/mqtt/debug/MqttStats;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342649
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 342650
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 342651
    :cond_1
    sget-object v0, Lcom/facebook/mqtt/debug/MqttStats;->d:Lcom/facebook/mqtt/debug/MqttStats;

    return-object v0

    .line 342652
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 342653
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;JZ)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 342654
    monitor-enter p0

    if-nez p1, :cond_0

    .line 342655
    :try_start_0
    const-string p1, "<not-specified>"

    .line 342656
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/mqtt/debug/MqttStats;->a(Lcom/facebook/mqtt/debug/MqttStats;Ljava/lang/String;)LX/1tT;

    move-result-object v0

    .line 342657
    if-eqz p4, :cond_1

    .line 342658
    iget-object v1, v0, LX/1tT;->data:LX/1jU;

    iget-wide v2, v1, LX/1jU;->sent:J

    add-long/2addr v2, p2

    iput-wide v2, v1, LX/1jU;->sent:J

    .line 342659
    :goto_0
    iget v1, v0, LX/1tT;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/1tT;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342660
    monitor-exit p0

    return-void

    .line 342661
    :cond_1
    :try_start_1
    iget-object v1, v0, LX/1tT;->data:LX/1jU;

    iget-wide v2, v1, LX/1jU;->recvd:J

    add-long/2addr v2, p2

    iput-wide v2, v1, LX/1jU;->recvd:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 342662
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
