.class public final LX/JPZ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JPa;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/2dx;

.field public final synthetic e:LX/JPa;


# direct methods
.method public constructor <init>(LX/JPa;)V
    .locals 1

    .prologue
    .line 2689980
    iput-object p1, p0, LX/JPZ;->e:LX/JPa;

    .line 2689981
    move-object v0, p1

    .line 2689982
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2689983
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2689984
    const-string v0, "ResponseInsightsCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2689985
    if-ne p0, p1, :cond_1

    .line 2689986
    :cond_0
    :goto_0
    return v0

    .line 2689987
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2689988
    goto :goto_0

    .line 2689989
    :cond_3
    check-cast p1, LX/JPZ;

    .line 2689990
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2689991
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2689992
    if-eq v2, v3, :cond_0

    .line 2689993
    iget-object v2, p0, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2689994
    goto :goto_0

    .line 2689995
    :cond_5
    iget-object v2, p1, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2689996
    :cond_6
    iget-object v2, p0, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2689997
    goto :goto_0

    .line 2689998
    :cond_8
    iget-object v2, p1, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2689999
    :cond_9
    iget-object v2, p0, LX/JPZ;->c:LX/1Pn;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JPZ;->c:LX/1Pn;

    iget-object v3, p1, LX/JPZ;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2690000
    goto :goto_0

    .line 2690001
    :cond_b
    iget-object v2, p1, LX/JPZ;->c:LX/1Pn;

    if-nez v2, :cond_a

    .line 2690002
    :cond_c
    iget-object v2, p0, LX/JPZ;->d:LX/2dx;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JPZ;->d:LX/2dx;

    iget-object v3, p1, LX/JPZ;->d:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2690003
    goto :goto_0

    .line 2690004
    :cond_d
    iget-object v2, p1, LX/JPZ;->d:LX/2dx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
