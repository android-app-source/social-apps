.class public LX/Ifq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:LX/Ifl;

.field private final b:LX/0aG;

.field private final c:LX/Ifj;

.field private final d:LX/J7S;

.field private final e:LX/0Xl;

.field private final f:LX/03V;

.field private final g:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/Ifl;LX/0aG;LX/Ifj;LX/J7S;LX/0Xl;LX/03V;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2600143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600144
    iput-object p1, p0, LX/Ifq;->a:LX/Ifl;

    .line 2600145
    iput-object p2, p0, LX/Ifq;->b:LX/0aG;

    .line 2600146
    iput-object p3, p0, LX/Ifq;->c:LX/Ifj;

    .line 2600147
    iput-object p4, p0, LX/Ifq;->d:LX/J7S;

    .line 2600148
    iput-object p5, p0, LX/Ifq;->e:LX/0Xl;

    .line 2600149
    iput-object p6, p0, LX/Ifq;->f:LX/03V;

    .line 2600150
    iput-object p7, p0, LX/Ifq;->g:Ljava/util/concurrent/Executor;

    .line 2600151
    return-void
.end method
