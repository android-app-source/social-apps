.class public final LX/Hcz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hd1;


# direct methods
.method public constructor <init>(LX/Hd1;)V
    .locals 0

    .prologue
    .line 2487749
    iput-object p1, p0, LX/Hcz;->a:LX/Hd1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2487750
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2487751
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2487752
    if-eqz p1, :cond_0

    .line 2487753
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2487754
    if-eqz v0, :cond_0

    .line 2487755
    iget-object v0, p0, LX/Hcz;->a:LX/Hd1;

    .line 2487756
    iget-object v2, v0, LX/Hd1;->d:LX/1My;

    new-instance v3, LX/Hd0;

    invoke-direct {v3, v0}, LX/Hd0;-><init>(LX/Hd1;)V

    .line 2487757
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2487758
    check-cast v1, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2487759
    iget-object v1, p0, LX/Hcz;->a:LX/Hd1;

    .line 2487760
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2487761
    check-cast v0, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-static {v1, v0}, LX/Hd1;->c(LX/Hd1;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V

    .line 2487762
    :cond_0
    return-void
.end method
