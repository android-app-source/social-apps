.class public LX/Idn;
.super LX/Idl;
.source ""

# interfaces
.implements LX/Idk;


# static fields
.field private static final i:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final k:[Ljava/lang/String;


# instance fields
.field private j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2597938
    const-class v0, LX/Idn;

    sput-object v0, LX/Idn;->i:Ljava/lang/Class;

    .line 2597939
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, LX/Idn;->k:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Idm;Landroid/content/ContentResolver;JILandroid/net/Uri;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 2597940
    invoke-direct/range {p0 .. p14}, LX/Idl;-><init>(LX/Idm;Landroid/content/ContentResolver;JILandroid/net/Uri;Ljava/lang/String;JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 2597941
    move/from16 v0, p15

    iput v0, p0, LX/Idn;->j:I

    .line 2597942
    return-void
.end method


# virtual methods
.method public final a(Z)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 2597943
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2597944
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 2597945
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 2597946
    iget-object v1, p0, LX/Idl;->a:Landroid/content/ContentResolver;

    iget-wide v2, p0, LX/Idl;->c:J

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4, v0}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2597947
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2597948
    invoke-virtual {p0}, LX/Idl;->c()I

    move-result v1

    invoke-static {v0, v1}, LX/IdZ;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2597949
    :cond_0
    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2597950
    iget v0, p0, LX/Idn;->j:I

    return v0
.end method
