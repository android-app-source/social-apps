.class public LX/ICw;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:LX/ICy;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ICk;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/ICk;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/ICy;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2550227
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2550228
    iput-object p1, p0, LX/ICw;->a:LX/ICy;

    .line 2550229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ICw;->b:Ljava/util/List;

    .line 2550230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ICw;->c:Ljava/util/Map;

    .line 2550231
    iput-object p2, p0, LX/ICw;->d:Landroid/view/View$OnClickListener;

    .line 2550232
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2550233
    new-instance v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;-><init>(Landroid/content/Context;)V

    .line 2550234
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2550235
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2550236
    check-cast p2, LX/ICk;

    .line 2550237
    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    .line 2550238
    iget-object v0, p0, LX/ICw;->a:LX/ICy;

    .line 2550239
    invoke-virtual {p2}, LX/ICk;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2550240
    invoke-virtual {p2}, LX/ICk;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2550241
    const v1, 0x7f083818

    invoke-static {v0, v1}, LX/ICy;->a(LX/ICy;I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f083819

    invoke-static {v0, v2}, LX/ICy;->a(LX/ICy;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2550242
    iget-boolean v1, p2, LX/ICk;->e:Z

    move v1, v1

    .line 2550243
    if-eqz v1, :cond_0

    .line 2550244
    const v1, 0x7f08381a

    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2550245
    iget-object v1, p3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2550246
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2550247
    :goto_0
    iget-object v0, p3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 2550248
    const v1, 0x7f0d0211

    invoke-virtual {v0, v1, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2550249
    iget-object v0, p0, LX/ICw;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2550250
    return-void

    .line 2550251
    :cond_0
    invoke-virtual {p2}, LX/ICk;->e()I

    move-result v1

    .line 2550252
    if-lez v1, :cond_1

    iget-object v2, v0, LX/ICy;->a:Landroid/content/res/Resources;

    const v3, 0x7f0f005c

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    aput-object p5, p1, p4

    invoke-virtual {v2, v3, v1, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 2550253
    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2550254
    iget-object v1, p3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2550255
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const-string v1, ""

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2550256
    iget-object v0, p0, LX/ICw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2550257
    iget-object v0, p0, LX/ICw;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2550258
    int-to-long v0, p1

    return-wide v0
.end method
