.class public final LX/I4v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 0

    .prologue
    .line 2534653
    iput-object p1, p0, LX/I4v;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2534654
    iget-object v0, p0, LX/I4v;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534655
    iput-object p1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->G:Ljava/util/List;

    .line 2534656
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;

    .line 2534657
    iget-object v2, p0, LX/I4v;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel$EventCategoryListModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2534658
    :cond_0
    iget-object v0, p0, LX/I4v;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2534659
    iget-object v0, p0, LX/I4v;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-static {v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->m(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V

    .line 2534660
    :cond_1
    return-void
.end method
