.class public final LX/HNB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

.field public final synthetic c:LX/HN7;

.field public final synthetic d:LX/HNE;


# direct methods
.method public constructor <init>(LX/HNE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/HN7;)V
    .locals 0

    .prologue
    .line 2458207
    iput-object p1, p0, LX/HNB;->d:LX/HNE;

    iput-object p2, p0, LX/HNB;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object p3, p0, LX/HNB;->b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    iput-object p4, p0, LX/HNB;->c:LX/HN7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    .line 2458208
    iget-object v0, p0, LX/HNB;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v1, p0, LX/HNB;->b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-static {v0, v1}, LX/HNE;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HNB;->c:LX/HN7;

    if-nez v0, :cond_1

    .line 2458209
    :cond_0
    :goto_0
    return-void

    .line 2458210
    :cond_1
    iget-object v0, p0, LX/HNB;->c:LX/HN7;

    iget-object v1, p0, LX/HNB;->b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2458211
    if-eqz p2, :cond_2

    const-string v1, "published"

    :goto_1
    move-object v1, v1

    .line 2458212
    new-instance v4, LX/HNA;

    invoke-direct {v4, p0}, LX/HNA;-><init>(LX/HNB;)V

    invoke-virtual {v0, v2, v3, v1, v4}, LX/HN7;->a(JLjava/lang/String;LX/HMl;)V

    goto :goto_0

    :cond_2
    const-string v1, "staging"

    goto :goto_1
.end method
