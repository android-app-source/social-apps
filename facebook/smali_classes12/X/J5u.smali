.class public final LX/J5u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;)V
    .locals 0

    .prologue
    .line 2649014
    iput-object p1, p0, LX/J5u;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x15c7d999

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649015
    iget-object v1, p0, LX/J5u;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    .line 2649016
    new-instance v3, Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    invoke-direct {v3}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;-><init>()V

    iput-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    .line 2649017
    new-instance v3, LX/J5v;

    invoke-direct {v3, v1}, LX/J5v;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;)V

    iput-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->o:LX/8Q5;

    .line 2649018
    new-instance v3, LX/J5w;

    invoke-direct {v3, v1}, LX/J5w;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;)V

    iput-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->p:LX/8Qm;

    .line 2649019
    iget-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    iget-object p0, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->o:LX/8Q5;

    .line 2649020
    iput-object p0, v3, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    .line 2649021
    iget-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    iget-object p0, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->p:LX/8Qm;

    invoke-virtual {v3, p0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->a(LX/8Qm;)V

    .line 2649022
    iget-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->l:LX/8RJ;

    sget-object p0, LX/8RI;->PRIVACY_CHECKUP_STEP_FRAGMENT:LX/8RI;

    invoke-virtual {v3, p0}, LX/8RJ;->a(LX/8RI;)V

    .line 2649023
    iget-object v3, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    .line 2649024
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object p0, p0

    .line 2649025
    const-string p1, "PRIVACY_CHECKUP_AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v3, p0, p1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2649026
    const v1, 0x6d0f68cd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
