.class public LX/Izl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Izl;


# instance fields
.field private final a:LX/IzO;

.field private final b:LX/IzM;

.field private final c:LX/Duh;

.field private final d:LX/Izt;

.field private final e:LX/Izs;

.field private final f:LX/Izr;

.field private final g:LX/03V;


# direct methods
.method public constructor <init>(LX/IzO;LX/Izp;LX/IzM;LX/Duh;LX/Izt;LX/Izs;LX/Izr;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635539
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635540
    iput-object p1, p0, LX/Izl;->a:LX/IzO;

    .line 2635541
    iput-object p3, p0, LX/Izl;->b:LX/IzM;

    .line 2635542
    iput-object p4, p0, LX/Izl;->c:LX/Duh;

    .line 2635543
    iput-object p5, p0, LX/Izl;->d:LX/Izt;

    .line 2635544
    iput-object p6, p0, LX/Izl;->e:LX/Izs;

    .line 2635545
    iput-object p7, p0, LX/Izl;->f:LX/Izr;

    .line 2635546
    iput-object p8, p0, LX/Izl;->g:LX/03V;

    .line 2635547
    return-void
.end method

.method public static a(LX/0QB;)LX/Izl;
    .locals 12

    .prologue
    .line 2635526
    sget-object v0, LX/Izl;->h:LX/Izl;

    if-nez v0, :cond_1

    .line 2635527
    const-class v1, LX/Izl;

    monitor-enter v1

    .line 2635528
    :try_start_0
    sget-object v0, LX/Izl;->h:LX/Izl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635529
    if-eqz v2, :cond_0

    .line 2635530
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635531
    new-instance v3, LX/Izl;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v4

    check-cast v4, LX/IzO;

    invoke-static {v0}, LX/Izp;->a(LX/0QB;)LX/Izp;

    move-result-object v5

    check-cast v5, LX/Izp;

    invoke-static {v0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v6

    check-cast v6, LX/IzM;

    invoke-static {v0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v7

    check-cast v7, LX/Duh;

    invoke-static {v0}, LX/Izt;->b(LX/0QB;)LX/Izt;

    move-result-object v8

    check-cast v8, LX/Izt;

    invoke-static {v0}, LX/Izs;->b(LX/0QB;)LX/Izs;

    move-result-object v9

    check-cast v9, LX/Izs;

    invoke-static {v0}, LX/Izr;->b(LX/0QB;)LX/Izr;

    move-result-object v10

    check-cast v10, LX/Izr;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-direct/range {v3 .. v11}, LX/Izl;-><init>(LX/IzO;LX/Izp;LX/IzM;LX/Duh;LX/Izt;LX/Izs;LX/Izr;LX/03V;)V

    .line 2635532
    move-object v0, v3

    .line 2635533
    sput-object v0, LX/Izl;->h:LX/Izl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635534
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635535
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635536
    :cond_1
    sget-object v0, LX/Izl;->h:LX/Izl;

    return-object v0

    .line 2635537
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635538
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2635505
    const-string v0, "insertOrUpdateTransactionInRecentAll"

    const v1, -0x78088d57

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635506
    :try_start_0
    iget-object v0, p0, LX/Izl;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635507
    const v0, -0x7f9fd774

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2635508
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635509
    iget-object v2, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2635510
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, p3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635511
    iget-object v2, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2635512
    invoke-virtual {v0, p4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2635514
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 2635515
    iget-object v5, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2635516
    aput-object v5, v3, v4

    .line 2635517
    invoke-virtual {v1, p2, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2635518
    if-nez v2, :cond_0

    .line 2635519
    const/4 v2, 0x0

    const v3, -0x5dda9fdf

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v1, p2, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x6e1c8f34

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635520
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635521
    const v0, -0x2fc08ce0

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635522
    const v0, 0x672b0e7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635523
    return-void

    .line 2635524
    :catchall_0
    move-exception v0

    const v2, -0x1581fb05

    :try_start_3
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635525
    :catchall_1
    move-exception v0

    const v1, 0x2a1cfe47

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V
    .locals 4

    .prologue
    .line 2635548
    const-string v0, "insertOrUpdateTransactionInRecentTables"

    const v1, 0x51810c4e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635549
    :try_start_0
    const-string v0, "recent_all_transactions"

    sget-object v1, LX/IzT;->a:LX/0U1;

    .line 2635550
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2635551
    sget-object v2, LX/IzT;->b:LX/0U1;

    .line 2635552
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635553
    invoke-static {p0, p1, v0, v1, v2}, LX/Izl;->a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2635554
    iget-object v0, p0, LX/Izl;->c:LX/Duh;

    invoke-virtual {v0, p1}, LX/Duh;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2635555
    const-string v0, "recent_incoming_transactions"

    sget-object v1, LX/IzU;->a:LX/0U1;

    .line 2635556
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2635557
    sget-object v2, LX/IzU;->b:LX/0U1;

    .line 2635558
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635559
    invoke-static {p0, p1, v0, v1, v2}, LX/Izl;->a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635560
    :goto_0
    const v0, -0x4140b25c

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635561
    return-void

    .line 2635562
    :cond_0
    :try_start_1
    const-string v0, "recent_outgoing_transactions"

    sget-object v1, LX/IzV;->a:LX/0U1;

    .line 2635563
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2635564
    sget-object v2, LX/IzV;->b:LX/0U1;

    .line 2635565
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635566
    invoke-static {p0, p1, v0, v1, v2}, LX/Izl;->a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2635567
    :catchall_0
    move-exception v0

    const v1, 0x543ff302

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;)V
    .locals 8

    .prologue
    .line 2635491
    const-string v0, "insertMoreTransactions"

    const v1, -0xbc5e65d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635492
    :try_start_0
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    move-object v0, v0

    .line 2635493
    invoke-static {v0}, LX/Izp;->a(LX/DtK;)LX/Izo;

    move-result-object v2

    .line 2635494
    iget-object v0, p2, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->a:LX/0Px;

    move-object v3, v0

    .line 2635495
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2635496
    iget-object v5, v2, LX/Izo;->a:Ljava/lang/String;

    iget-object v6, v2, LX/Izo;->c:Ljava/lang/String;

    iget-object v7, v2, LX/Izo;->b:Ljava/lang/String;

    invoke-static {p0, v0, v5, v6, v7}, LX/Izl;->a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2635497
    invoke-virtual {p0, v0}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2635498
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2635499
    :cond_0
    iget-object v0, p0, LX/Izl;->b:LX/IzM;

    iget-object v1, v2, LX/Izo;->d:LX/IzK;

    .line 2635500
    iget-boolean v2, p2, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;->b:Z

    move v2, v2

    .line 2635501
    invoke-virtual {v0, v1, v2}, LX/2Iu;->b(LX/0To;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635502
    const v0, -0x83abbad

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635503
    return-void

    .line 2635504
    :catchall_0
    move-exception v0

    const v1, -0x6c621cf1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;)V
    .locals 9

    .prologue
    .line 2635471
    const-string v0, "insertTransactionList"

    const v1, 0x55467b41

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635472
    :try_start_0
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    move-object v0, v0

    .line 2635473
    invoke-static {v0}, LX/Izp;->a(LX/DtK;)LX/Izo;

    move-result-object v2

    .line 2635474
    iget-object v0, p0, LX/Izl;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2635475
    const v0, -0x3893c1c

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2635476
    :try_start_1
    iget-object v0, v2, LX/Izo;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2635477
    iget-object v0, p2, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->a:LX/0Px;

    move-object v4, v0

    .line 2635478
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2635479
    iget-object v6, v2, LX/Izo;->a:Ljava/lang/String;

    iget-object v7, v2, LX/Izo;->c:Ljava/lang/String;

    iget-object v8, v2, LX/Izo;->b:Ljava/lang/String;

    invoke-static {p0, v0, v6, v7, v8}, LX/Izl;->a(LX/Izl;Lcom/facebook/payments/p2p/model/PaymentTransaction;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2635480
    invoke-virtual {p0, v0}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2635481
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2635482
    :cond_0
    iget-object v0, p0, LX/Izl;->b:LX/IzM;

    iget-object v1, v2, LX/Izo;->d:LX/IzK;

    .line 2635483
    iget-boolean v2, p2, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->b:Z

    move v2, v2

    .line 2635484
    invoke-virtual {v0, v1, v2}, LX/2Iu;->b(LX/0To;Z)V

    .line 2635485
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635486
    const v0, 0xd7609d8

    :try_start_2
    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635487
    const v0, 0x544c83c4

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635488
    return-void

    .line 2635489
    :catchall_0
    move-exception v0

    const v1, 0x4f6cb30f    # 3.97115776E9f

    :try_start_3
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635490
    :catchall_1
    move-exception v0

    const v1, 0x47c99b24

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V
    .locals 6

    .prologue
    .line 2635386
    const-string v0, "insertOrUpdatePaymentTransaction"

    const v1, -0x6b91f8b2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635387
    :try_start_0
    iget-object v0, p0, LX/Izl;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635388
    const v0, 0x4c83e2a6    # 6.9145904E7f

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2635389
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635390
    sget-object v2, LX/IzZ;->a:LX/0U1;

    .line 2635391
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635392
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2635393
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635394
    sget-object v2, LX/IzZ;->b:LX/0U1;

    .line 2635395
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635396
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    move-object v3, v3

    .line 2635397
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Sender;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635398
    sget-object v2, LX/IzZ;->c:LX/0U1;

    .line 2635399
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635400
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->e:Lcom/facebook/payments/p2p/model/Receiver;

    move-object v3, v3

    .line 2635401
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Receiver;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635402
    sget-object v2, LX/IzZ;->d:LX/0U1;

    .line 2635403
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635404
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v3, v3

    .line 2635405
    invoke-virtual {v3}, LX/DtQ;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635406
    sget-object v2, LX/IzZ;->e:LX/0U1;

    .line 2635407
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635408
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2635409
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635410
    sget-object v2, LX/IzZ;->f:LX/0U1;

    .line 2635411
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635412
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->i:Ljava/lang/String;

    move-object v3, v3

    .line 2635413
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635414
    sget-object v2, LX/IzZ;->g:LX/0U1;

    .line 2635415
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635416
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2635417
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635418
    sget-object v2, LX/IzZ;->h:LX/0U1;

    .line 2635419
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635420
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v3, v3

    .line 2635421
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2635422
    sget-object v2, LX/IzZ;->i:LX/0U1;

    .line 2635423
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635424
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v3, v3

    .line 2635425
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2635426
    sget-object v2, LX/IzZ;->j:LX/0U1;

    .line 2635427
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635428
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v3, v3

    .line 2635429
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635430
    sget-object v2, LX/IzZ;->k:LX/0U1;

    .line 2635431
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635432
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->k:Lcom/facebook/payments/p2p/model/Amount;

    move-object v3, v3

    .line 2635433
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2635434
    sget-object v2, LX/IzZ;->l:LX/0U1;

    .line 2635435
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635436
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-object v3, v3

    .line 2635437
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635438
    sget-object v2, LX/IzZ;->m:LX/0U1;

    .line 2635439
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635440
    iget-object v3, p0, LX/Izl;->d:LX/Izt;

    .line 2635441
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-object v4, v4

    .line 2635442
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Izt;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635443
    sget-object v2, LX/IzZ;->n:LX/0U1;

    .line 2635444
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635445
    iget-object v3, p0, LX/Izl;->e:LX/Izs;

    .line 2635446
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->m:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-object v4, v4

    .line 2635447
    invoke-virtual {v3, v4}, LX/Izs;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635448
    sget-object v2, LX/IzZ;->o:LX/0U1;

    .line 2635449
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635450
    iget-object v3, p0, LX/Izl;->f:LX/Izr;

    .line 2635451
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v4, v4

    .line 2635452
    invoke-virtual {v3, v4}, LX/Izr;->a(Lcom/facebook/payments/p2p/model/CommerceOrder;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635453
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/IzZ;->a:LX/0U1;

    .line 2635454
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635455
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2635456
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 2635457
    iget-object v5, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2635458
    aput-object v5, v3, v4

    .line 2635459
    const-string v4, "transactions"

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2635460
    if-nez v2, :cond_0

    .line 2635461
    const-string v2, "transactions"

    const/4 v3, 0x0

    const v4, -0x7ed41c34

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x443bf5f3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635462
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2635463
    const v0, 0x1c5f96f4

    :try_start_2
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2635464
    :goto_0
    const v0, -0x154abb47

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635465
    return-void

    .line 2635466
    :catch_0
    move-exception v0

    .line 2635467
    :try_start_3
    iget-object v2, p0, LX/Izl;->g:LX/03V;

    const-string v3, "DbInsertPaymentTransactionsHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2635468
    const v0, 0x49c72638    # 1631431.0f

    :try_start_4
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2635469
    :catchall_0
    move-exception v0

    const v1, -0x5aaf524d    # -1.8099976E-16f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635470
    :catchall_1
    move-exception v0

    const v2, 0x28a81586

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
