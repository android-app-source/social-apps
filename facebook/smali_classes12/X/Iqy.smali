.class public final LX/Iqy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2617358
    iput-object p1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x1df8b39b

    invoke-static {v4, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617359
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2617360
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-boolean v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez v1, :cond_0

    .line 2617361
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-static {v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->l(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617362
    :cond_0
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v1, v3}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 2617363
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v1}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2617364
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->J:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2617365
    :cond_1
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2617366
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->N:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 2617367
    iget-object v1, p0, LX/Iqy;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 2617368
    :cond_2
    const v1, -0x645deca

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
