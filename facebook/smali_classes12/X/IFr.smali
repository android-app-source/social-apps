.class public final LX/IFr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;",
        ">;",
        "LX/IFN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IFv;


# direct methods
.method public constructor <init>(LX/IFv;)V
    .locals 0

    .prologue
    .line 2554921
    iput-object p1, p0, LX/IFr;->a:LX/IFv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2554922
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2554923
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554924
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v3, v0

    .line 2554925
    :goto_0
    new-instance v0, LX/IFN;

    .line 2554926
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2554927
    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;

    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2554928
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2554929
    check-cast v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;->j()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/IFN;-><init>(LX/0am;LX/0am;LX/0am;LX/0am;LX/0am;)V

    return-object v0

    .line 2554930
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554931
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel$PrivacySettingsModel;

    move-result-object v0

    move-object v3, v0

    goto :goto_0
.end method
