.class public final LX/J6n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V
    .locals 0

    .prologue
    .line 2650172
    iput-object p1, p0, LX/J6n;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x81b57a6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650173
    iget-object v1, p0, LX/J6n;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    sget-object v2, LX/J6q;->LEARN_MORE:LX/J6q;

    iget-object v3, p0, LX/J6n;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iget-object v3, v3, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->b:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a$redex0(Lcom/facebook/privacy/educator/AudienceEducatorFragment;LX/J6q;Ljava/lang/String;)V

    .line 2650174
    iget-object v1, p0, LX/J6n;->a:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    .line 2650175
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreActivity;

    invoke-direct {v2, v3, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2650176
    const-string v3, "extra_audience_educator_type"

    iget-object p0, v1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2650177
    iget-object v3, v1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v3, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2650178
    const v1, 0x6e04d80f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
