.class public final LX/Iwt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;I)V
    .locals 0

    .prologue
    .line 2631055
    iput-object p1, p0, LX/Iwt;->b:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    iput p2, p0, LX/Iwt;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2c901793

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2631056
    iget-object v1, p0, LX/Iwt;->b:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    iget-object v1, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->c:LX/Ix1;

    iget v2, p0, LX/Iwt;->a:I

    .line 2631057
    iget-object v4, v1, LX/Ix1;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->f:LX/0Px;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2631058
    iget-object v5, v1, LX/Ix1;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    .line 2631059
    iget-object v6, v5, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    .line 2631060
    iget-object v7, v6, LX/Iws;->a:LX/0if;

    sget-object p0, LX/0ig;->S:LX/0ih;

    const-string p1, "branded_content_select_search_pages"

    const/4 v1, 0x0

    invoke-static {v6}, LX/Iws;->h(LX/Iws;)LX/1rQ;

    move-result-object v2

    invoke-virtual {v7, p0, p1, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2631061
    iget-object v6, v5, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    invoke-virtual {v6}, LX/Iws;->g()V

    .line 2631062
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 2631063
    const-string v7, "tag_branded_content"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2631064
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const/4 p0, -0x1

    invoke-virtual {v7, p0, v6}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2631065
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2631066
    const v1, -0x64d818c5

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
