.class public final LX/HXR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V
    .locals 0

    .prologue
    .line 2479070
    iput-object p1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x5d1e5dbd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2479071
    const-string v0, "extra_result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v0

    .line 2479072
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->D:LX/4BY;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->D:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2479073
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->D:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->dismiss()V

    .line 2479074
    :cond_0
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-wide v4, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->w:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_1

    .line 2479075
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->j:LX/0hx;

    iget-object v3, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v3, v3, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    iget-object v3, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-wide v6, v3, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->w:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, LX/0hx;->a(J)V

    .line 2479076
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    .line 2479077
    iput-wide v8, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->w:J

    .line 2479078
    :cond_1
    sget-object v1, LX/7m7;->SUCCESS:LX/7m7;

    if-ne v0, v1, :cond_3

    .line 2479079
    iget-object v0, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->mJ_()V

    .line 2479080
    :cond_2
    :goto_0
    const v0, 0x5e41b30a

    invoke-static {v0, v2}, LX/02F;->e(II)V

    return-void

    .line 2479081
    :cond_3
    sget-object v1, LX/7m7;->EXCEPTION:LX/7m7;

    if-ne v0, v1, :cond_2

    .line 2479082
    const-string v0, "extra_error_details"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/ErrorDetails;

    .line 2479083
    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, LX/27k;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->userMessage:Ljava/lang/String;

    invoke-direct {v1, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 2479084
    :goto_1
    iget-object v1, p0, LX/HXR;->a:Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->n:LX/0kL;

    invoke-virtual {v1, v0}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 2479085
    :cond_4
    new-instance v0, LX/27k;

    const v1, 0x7f081407

    invoke-direct {v0, v1}, LX/27k;-><init>(I)V

    goto :goto_1
.end method
