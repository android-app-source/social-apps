.class public LX/Ha5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/12x;

.field public final c:Landroid/app/NotificationManager;

.field public final d:LX/HZt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/12x;Landroid/app/NotificationManager;LX/HZt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483347
    iput-object p1, p0, LX/Ha5;->a:Landroid/content/Context;

    .line 2483348
    iput-object p2, p0, LX/Ha5;->b:LX/12x;

    .line 2483349
    iput-object p3, p0, LX/Ha5;->c:Landroid/app/NotificationManager;

    .line 2483350
    iput-object p4, p0, LX/Ha5;->d:LX/HZt;

    .line 2483351
    return-void
.end method

.method public static a(LX/0QB;)LX/Ha5;
    .locals 7

    .prologue
    .line 2483352
    const-class v1, LX/Ha5;

    monitor-enter v1

    .line 2483353
    :try_start_0
    sget-object v0, LX/Ha5;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2483354
    sput-object v2, LX/Ha5;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2483355
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483356
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2483357
    new-instance p0, LX/Ha5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v4

    check-cast v4, LX/12x;

    invoke-static {v0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    invoke-static {v0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v6

    check-cast v6, LX/HZt;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Ha5;-><init>(Landroid/content/Context;LX/12x;Landroid/app/NotificationManager;LX/HZt;)V

    .line 2483358
    move-object v0, p0

    .line 2483359
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2483360
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ha5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2483361
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2483362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/Ha5;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 2483363
    iget-object v0, p0, LX/Ha5;->a:Landroid/content/Context;

    sget-object v1, LX/Ha7;->CREATE_FINISH_REGISTRATION_NOTIFICATION:LX/Ha7;

    invoke-static {v0, v1}, Lcom/facebook/registration/notification/RegistrationNotificationService;->a(Landroid/content/Context;LX/Ha7;)Landroid/content/Intent;

    move-result-object v0

    .line 2483364
    iget-object v1, p0, LX/Ha5;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
