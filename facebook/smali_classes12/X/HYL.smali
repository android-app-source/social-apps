.class public LX/HYL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/HYL;


# instance fields
.field private a:LX/3HP;

.field public b:LX/20j;

.field private c:LX/0SG;


# direct methods
.method public constructor <init>(LX/3HP;LX/20j;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480477
    iput-object p1, p0, LX/HYL;->a:LX/3HP;

    .line 2480478
    iput-object p2, p0, LX/HYL;->b:LX/20j;

    .line 2480479
    iput-object p3, p0, LX/HYL;->c:LX/0SG;

    .line 2480480
    return-void
.end method

.method public static a(LX/0QB;)LX/HYL;
    .locals 6

    .prologue
    .line 2480481
    sget-object v0, LX/HYL;->d:LX/HYL;

    if-nez v0, :cond_1

    .line 2480482
    const-class v1, LX/HYL;

    monitor-enter v1

    .line 2480483
    :try_start_0
    sget-object v0, LX/HYL;->d:LX/HYL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480484
    if-eqz v2, :cond_0

    .line 2480485
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480486
    new-instance p0, LX/HYL;

    invoke-static {v0}, LX/3HP;->a(LX/0QB;)LX/3HP;

    move-result-object v3

    check-cast v3, LX/3HP;

    invoke-static {v0}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v4

    check-cast v4, LX/20j;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/HYL;-><init>(LX/3HP;LX/20j;LX/0SG;)V

    .line 2480487
    move-object v0, p0

    .line 2480488
    sput-object v0, LX/HYL;->d:LX/HYL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480489
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480490
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480491
    :cond_1
    sget-object v0, LX/HYL;->d:LX/HYL;

    return-object v0

    .line 2480492
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480493
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 2480494
    :goto_0
    if-eqz p1, :cond_2

    .line 2480495
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 2480496
    if-eqz v0, :cond_1

    .line 2480497
    iget-object v1, p0, LX/HYL;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->a(J)V

    .line 2480498
    iget-object v1, p0, LX/HYL;->a:LX/3HP;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 2480499
    iget-object v4, v1, LX/3HP;->a:LX/0sZ;

    invoke-static {v2}, LX/3HP;->c(Ljava/lang/String;)Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v4

    const/4 v5, 0x0

    .line 2480500
    iput-boolean v5, v4, LX/0zO;->p:Z

    .line 2480501
    move-object v4, v4

    .line 2480502
    :try_start_0
    iget-object v5, v1, LX/3HP;->b:LX/0si;

    invoke-interface {v5, v4}, LX/0sj;->b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 2480503
    if-nez v4, :cond_3

    :goto_1
    move-object v1, v3

    .line 2480504
    if-eqz v1, :cond_0

    .line 2480505
    sget-object v3, LX/5Gs;->LOAD_BEFORE:LX/5Gs;

    invoke-static {v1, v0, v3}, LX/20j;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5Gs;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2480506
    invoke-static {v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    .line 2480507
    iget-object v3, v2, LX/3dM;->O:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    invoke-static {v3}, LX/4ZH;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/4ZH;

    move-result-object v3

    invoke-static {v0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    .line 2480508
    iput v4, v3, LX/4ZH;->e:I

    .line 2480509
    move-object v3, v3

    .line 2480510
    invoke-virtual {v3}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 2480511
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    invoke-static {v3}, LX/3dN;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dN;

    move-result-object v3

    invoke-virtual {v3}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 2480512
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    move-object v0, v2

    .line 2480513
    :cond_0
    new-instance v1, LX/1lO;

    invoke-direct {v1}, LX/1lO;-><init>()V

    .line 2480514
    iput-object v0, v1, LX/1lO;->k:Ljava/lang/Object;

    .line 2480515
    move-object v1, v1

    .line 2480516
    invoke-virtual {v1}, LX/1lO;->a()Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v1

    .line 2480517
    iget-object v2, p0, LX/HYL;->a:LX/3HP;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3HP;->a(Ljava/lang/String;)LX/1kt;

    move-result-object v0

    .line 2480518
    :try_start_1
    invoke-interface {v0, v1}, LX/1kt;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2480519
    :cond_1
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p1

    goto :goto_0

    .line 2480520
    :catch_0
    move-exception v0

    .line 2480521
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Error while updating feedback from notification"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2480522
    :cond_2
    return-void

    .line 2480523
    :catch_1
    move-exception v4

    .line 2480524
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "Error while getting cached feedback"

    invoke-static {v5, v6, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2480525
    :cond_3
    iget-object v3, v4, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 2480526
    check-cast v3, Lcom/facebook/graphql/model/GraphQLFeedback;

    goto :goto_1
.end method
