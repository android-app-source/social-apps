.class public LX/I76;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/Blh;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/Blh;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/Blh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2538519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2538520
    iput-object p1, p0, LX/I76;->a:LX/0Or;

    .line 2538521
    iput-object p2, p0, LX/I76;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2538522
    iput-object p3, p0, LX/I76;->c:LX/0Or;

    .line 2538523
    iput-object p4, p0, LX/I76;->d:LX/Blh;

    .line 2538524
    return-void
.end method

.method public static b(LX/0QB;)LX/I76;
    .locals 5

    .prologue
    .line 2538551
    new-instance v2, LX/I76;

    const/16 v0, 0xbc6

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v1

    check-cast v1, LX/Blh;

    invoke-direct {v2, v3, v0, v4, v1}, LX/I76;-><init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/Blh;)V

    .line 2538552
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventActionContext;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2538553
    iget-object v0, p0, LX/I76;->d:LX/Blh;

    .line 2538554
    iget-object v1, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2538555
    invoke-virtual {v0, p1, v1, p3}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V

    .line 2538556
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/events/model/EventArtist;)V
    .locals 2

    .prologue
    .line 2538545
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    .line 2538546
    iget-object v1, p2, Lcom/facebook/events/model/EventArtist;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2538547
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2538548
    iget-object v0, p0, LX/I76;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0, p1, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2538549
    iget-object v1, p0, LX/I76;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2538550
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/events/model/EventUser;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2538525
    sget-object v0, LX/I75;->a:[I

    .line 2538526
    iget-object v1, p2, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v1, v1

    .line 2538527
    invoke-virtual {v1}, LX/7vJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2538528
    const-string v0, "EventPermalinkController"

    const-string v1, "Unknown event eventUser type %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2538529
    iget-object v4, p2, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v4, v4

    .line 2538530
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2538531
    :goto_0
    return-void

    .line 2538532
    :pswitch_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2538533
    iget-object v0, p2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2538534
    iget-object v2, p2, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2538535
    iget-object v4, p2, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2538536
    invoke-static {v1, v0, v2, v4}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2538537
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    .line 2538538
    iget-object v2, p2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2538539
    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 2538540
    :goto_1
    iget-object v0, p0, LX/I76;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    goto :goto_0

    .line 2538541
    :pswitch_1
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    .line 2538542
    iget-object v1, p2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2538543
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move-object v2, v3

    .line 2538544
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
