.class public final LX/IcU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/15i;

.field public final synthetic c:LX/IcV;


# direct methods
.method public constructor <init>(LX/IcV;ILX/15i;)V
    .locals 0

    .prologue
    .line 2595543
    iput-object p1, p0, LX/IcU;->c:LX/IcV;

    iput p2, p0, LX/IcU;->a:I

    iput-object p3, p0, LX/IcU;->b:LX/15i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2595544
    iget v0, p0, LX/IcU;->a:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 2595545
    :goto_1
    return-void

    .line 2595546
    :cond_0
    iget-object v0, p0, LX/IcU;->b:LX/15i;

    iget v2, p0, LX/IcU;->a:I

    invoke-virtual {v0, v2, v4}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_0

    .line 2595547
    :cond_2
    iget-object v0, p0, LX/IcU;->b:LX/15i;

    .line 2595548
    iget v2, p0, LX/IcU;->a:I

    invoke-virtual {v0, v2, v4}, LX/15i;->g(II)I

    move-result v5

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2595549
    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v5, v4}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-virtual {v0, v5, v1}, LX/15i;->l(II)D

    move-result-wide v0

    invoke-direct {v2, v6, v7, v0, v1}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2595550
    iget-object v0, p0, LX/IcU;->c:LX/IcV;

    iget-object v0, v0, LX/IcV;->a:LX/IcX;

    iget-object v1, p0, LX/IcU;->b:LX/15i;

    iget v4, p0, LX/IcU;->a:I

    const/4 v5, 0x2

    invoke-virtual {v1, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v4, v3

    invoke-static/range {v0 .. v5}, LX/IcX;->a(LX/IcX;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;)V

    goto :goto_1

    .line 2595551
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
