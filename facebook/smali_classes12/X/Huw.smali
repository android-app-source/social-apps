.class public LX/Huw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0jD;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0wM;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/user/model/User;

.field public final f:LX/HqJ;

.field public final g:LX/0gd;

.field public final h:LX/3kp;

.field public final i:Landroid/content/Context;

.field private final j:LX/ASX;

.field public k:Z

.field private l:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public m:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

.field private final n:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0wM;LX/0Ot;LX/0gd;Lcom/facebook/user/model/User;LX/0il;LX/HqJ;LX/0zw;LX/ASX;LX/3kp;Landroid/content/Context;)V
    .locals 2
    .param p4    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p5    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/HqJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/ASX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;",
            "LX/0gd;",
            "Lcom/facebook/user/model/User;",
            "TServices;",
            "Lcom/facebook/composer/privacy/controller/TagExpansionPillViewController$TagExpansionPillClickedListener;",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;",
            "LX/ASX;",
            "LX/3kp;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518082
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Huw;->k:Z

    .line 2518083
    new-instance v0, LX/Hut;

    invoke-direct {v0, p0}, LX/Hut;-><init>(LX/Huw;)V

    iput-object v0, p0, LX/Huw;->n:Landroid/view/View$OnClickListener;

    .line 2518084
    iput-object p1, p0, LX/Huw;->a:LX/0wM;

    .line 2518085
    iput-object p2, p0, LX/Huw;->b:LX/0Ot;

    .line 2518086
    iput-object p3, p0, LX/Huw;->g:LX/0gd;

    .line 2518087
    iput-object p4, p0, LX/Huw;->e:Lcom/facebook/user/model/User;

    .line 2518088
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Huw;->c:Ljava/lang/ref/WeakReference;

    .line 2518089
    iput-object p6, p0, LX/Huw;->f:LX/HqJ;

    .line 2518090
    iput-object p7, p0, LX/Huw;->d:LX/0zw;

    .line 2518091
    iput-object p8, p0, LX/Huw;->j:LX/ASX;

    .line 2518092
    iput-object p9, p0, LX/Huw;->h:LX/3kp;

    .line 2518093
    iput-object p10, p0, LX/Huw;->i:Landroid/content/Context;

    .line 2518094
    invoke-direct {p0}, LX/Huw;->d()V

    .line 2518095
    invoke-direct {p0}, LX/Huw;->c()V

    .line 2518096
    return-void
.end method

.method private c()V
    .locals 11

    .prologue
    const v10, -0x6e685d

    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2518097
    iget-object v0, p0, LX/Huw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0il;

    .line 2518098
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_0

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518099
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 2518100
    if-nez v0, :cond_2

    .line 2518101
    :cond_0
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2518102
    :cond_1
    :goto_0
    return-void

    .line 2518103
    :cond_2
    iget-object v0, p0, LX/Huw;->e:Lcom/facebook/user/model/User;

    .line 2518104
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2518105
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, LX/Huw;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7l0;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jD;

    invoke-interface {v3}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v3

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j2;

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/7ky;->a(JLX/7l0;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v7

    .line 2518106
    :goto_1
    iget-boolean v0, p0, LX/Huw;->k:Z

    if-ne v1, v0, :cond_3

    iget-object v2, p0, LX/Huw;->l:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eq v2, v0, :cond_1

    .line 2518107
    :cond_3
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518108
    invoke-virtual {v2}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v3

    .line 2518109
    iput-boolean v1, p0, LX/Huw;->k:Z

    .line 2518110
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iput-object v0, p0, LX/Huw;->l:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518111
    iput-object v3, p0, LX/Huw;->m:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 2518112
    invoke-virtual {v2}, Lcom/facebook/privacy/model/SelectablePrivacyData;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2518113
    iget-boolean v0, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, v0

    .line 2518114
    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    if-eqz v3, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq v3, v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v3, v0, :cond_6

    .line 2518115
    :cond_4
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto/16 :goto_0

    :cond_5
    move v1, v8

    .line 2518116
    goto :goto_1

    .line 2518117
    :cond_6
    iget-object v0, p0, LX/Huw;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2518118
    iget-object v0, p0, LX/Huw;->m:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_7

    .line 2518119
    const v2, 0x7f0200c8

    .line 2518120
    const v0, 0x7f0812f8

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2518121
    iget-object v0, p0, LX/Huw;->a:LX/0wM;

    const v3, 0x7f0202f6

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3, v10}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2518122
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v5, p0, LX/Huw;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2518123
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    move v0, v2

    move-object v2, v3

    .line 2518124
    :goto_2
    iget-object v3, p0, LX/Huw;->a:LX/0wM;

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0, v10}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2518125
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2518126
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, v3, v9, v2, v9}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2518127
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2518128
    iget-object v0, p0, LX/Huw;->j:LX/ASX;

    new-array v1, v7, [LX/ASW;

    sget-object v2, LX/ASW;->COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    goto/16 :goto_0

    .line 2518129
    :cond_7
    iget-object v0, p0, LX/Huw;->m:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v0, v1, :cond_8

    .line 2518130
    const v2, 0x7f0200d2

    .line 2518131
    const v0, 0x7f0812f9

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2518132
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v9}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2518133
    iget-object v0, p0, LX/Huw;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    move v0, v2

    move-object v2, v9

    goto :goto_2

    .line 2518134
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GraphQLPrivacyOptionTagExpansionType cannot be "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2518135
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2518136
    iget-object v0, p0, LX/Huw;->h:LX/3kp;

    sget-object v1, LX/0dp;->v:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2518137
    iget-object v0, p0, LX/Huw;->h:LX/3kp;

    const/4 v1, 0x3

    .line 2518138
    iput v1, v0, LX/3kp;->b:I

    .line 2518139
    new-instance v0, LX/Huu;

    invoke-direct {v0, p0}, LX/Huu;-><init>(LX/Huw;)V

    .line 2518140
    iget-object v1, p0, LX/Huw;->j:LX/ASX;

    sget-object v2, LX/ASW;->COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

    .line 2518141
    iget-object p0, v1, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p0, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518142
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2518143
    sget-object v0, LX/Huv;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2518144
    :goto_0
    return-void

    .line 2518145
    :pswitch_0
    invoke-direct {p0}, LX/Huw;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2518146
    invoke-direct {p0}, LX/Huw;->c()V

    .line 2518147
    return-void
.end method
