.class public final LX/IA8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public final synthetic b:Lcom/facebook/events/common/EventActionContext;

.field public final synthetic c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 0

    .prologue
    .line 2544740
    iput-object p1, p0, LX/IA8;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iput-object p2, p0, LX/IA8;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iput-object p3, p0, LX/IA8;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x68280ad5

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544711
    iget-object v1, p0, LX/IA8;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b:LX/I76;

    iget-object v2, p0, LX/IA8;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-virtual {v2}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/IA8;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iget-object v4, p0, LX/IA8;->b:Lcom/facebook/events/common/EventActionContext;

    iget-object v5, p0, LX/IA8;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-static {v5}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getEventGuestListTypes(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)LX/0Px;

    move-result-object v5

    .line 2544712
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    iget-object v7, v1, LX/I76;->c:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/ComponentName;

    invoke-virtual {v8, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 2544713
    const-string v8, "target_fragment"

    sget-object v9, LX/0cQ;->EVENTS_MESSAGE_GUESTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v9}, LX/0cQ;->ordinal()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2544714
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2544715
    const-string v10, "EVENT_ID"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544716
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 2544717
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v10

    .line 2544718
    const-string v12, "EVENT_HAS_SEEN_STATE"

    sget-object v13, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eq v10, v13, :cond_0

    sget-object v13, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v10, v13, :cond_7

    :cond_0
    const/4 v10, 0x1

    :goto_0
    invoke-virtual {v11, v12, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2544719
    const-string v10, "EVENT_IS_HOST"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2544720
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 2544721
    const-string v10, "EVENT_KIND"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544722
    :cond_2
    const-string v10, "EVENT_NAME"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544723
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 2544724
    const-string v10, "EVENT_PHOTO_URL"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel$PhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544725
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->j()J

    move-result-wide v12

    invoke-static {v12, v13}, LX/5O7;->a(J)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2544726
    const-string v10, "EVENT_TIME"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aS()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544727
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 2544728
    const-string v10, "EVENT_LOCATION"

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544729
    :cond_5
    const-string v10, "extras_event_action_context"

    invoke-virtual {v11, v10, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2544730
    const-string v10, "EVENT_GUEST_LIST_TYPES"

    invoke-static {v11, v10, v5}, LX/Blc;->writeGuestListTypesList(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2544731
    move-object v8, v11

    .line 2544732
    invoke-virtual {v7, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2544733
    iget-object v8, v1, LX/I76;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v8, v7, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2544734
    iget-object v1, p0, LX/IA8;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->f:LX/1nQ;

    iget-object v2, p0, LX/IA8;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2544735
    iget-object v3, v1, LX/1nQ;->i:LX/0Zb;

    const-string v4, "message_event_guests_button_clicked"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2544736
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2544737
    const-string v4, "event_message_guests"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v1, LX/1nQ;->j:LX/0kv;

    iget-object v5, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, v5}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2544738
    :cond_6
    const v1, -0x57c5be05

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2544739
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_0
.end method
