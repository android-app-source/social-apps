.class public final LX/I4W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;)V
    .locals 0

    .prologue
    .line 2534157
    iput-object p1, p0, LX/I4W;->a:Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2c59d1cb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2534158
    iget-object v1, p0, LX/I4W;->a:Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    iget-object v1, v1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    if-eqz v1, :cond_1

    .line 2534159
    iget-object v1, p0, LX/I4W;->a:Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/I4W;->a:Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;

    iget-object v3, v3, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->g:Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    invoke-virtual {v3}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2534160
    iget-object v5, v1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->b:LX/17Y;

    sget-object p0, LX/0ax;->cG:Ljava/lang/String;

    invoke-interface {v5, v2, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2534161
    const-string p0, "extra_event_collection_id"

    invoke-virtual {v5, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2534162
    iget-object p0, v1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz p0, :cond_0

    .line 2534163
    const-string p0, "extras_event_analytics_params"

    iget-object p1, v1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v5, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2534164
    :cond_0
    iget-object p0, v1, Lcom/facebook/events/eventcollections/ui/EventCollectionsCarouselCardView;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v5, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2534165
    :cond_1
    const v1, 0x1cb5017

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
