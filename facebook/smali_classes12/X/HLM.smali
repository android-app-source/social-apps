.class public LX/HLM;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/3mX",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/HIQ;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/961;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/9XE;

.field public final f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/2km;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HIQ;LX/9XE;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ">;TE;",
            "LX/25M;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HIQ;",
            "LX/9XE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2455827
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2455828
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2455829
    iput-object v0, p0, LX/HLM;->d:LX/0Ot;

    .line 2455830
    iput-object p6, p0, LX/HLM;->c:LX/HIQ;

    .line 2455831
    iput-object p7, p0, LX/HLM;->e:LX/9XE;

    .line 2455832
    iput-object p5, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455833
    return-void
.end method

.method public static a$redex0(LX/HLM;LX/HIR;Z)V
    .locals 4

    .prologue
    .line 2455802
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2455803
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455804
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455805
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2455806
    iget-object v0, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455807
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455808
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p1, LX/HIR;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455809
    iget-object v0, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455810
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455811
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-static {v0}, LX/9qZ;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/9qZ;

    move-result-object v0

    .line 2455812
    iput-boolean p2, v0, LX/9qZ;->c:Z

    .line 2455813
    move-object v0, v0

    .line 2455814
    invoke-virtual {v0}, LX/9qZ;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2455815
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2455816
    :cond_0
    iget-object v0, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455817
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455818
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2455819
    :cond_1
    iget-object v0, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455820
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 2455821
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v0

    invoke-static {v0}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2455822
    iput-object v1, v0, LX/9vz;->cj:LX/0Px;

    .line 2455823
    move-object v0, v0

    .line 2455824
    invoke-virtual {v0}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v1

    .line 2455825
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/2km;

    check-cast v0, LX/3U8;

    iget-object v2, p0, LX/HLM;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v2, v1}, LX/3U8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2455826
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2455792
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 2

    .prologue
    .line 2455794
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2455795
    iget-object v0, p0, LX/HLM;->c:LX/HIQ;

    invoke-virtual {v0, p1}, LX/HIQ;->c(LX/1De;)LX/HIO;

    move-result-object v1

    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/2km;

    invoke-virtual {v1, v0}, LX/HIO;->a(LX/2km;)LX/HIO;

    move-result-object v0

    .line 2455796
    new-instance v1, LX/HLI;

    invoke-direct {v1, p0}, LX/HLI;-><init>(LX/HLM;)V

    move-object v1, v1

    .line 2455797
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIT;)LX/HIO;

    move-result-object v0

    .line 2455798
    new-instance v1, LX/HLJ;

    invoke-direct {v1, p0}, LX/HLJ;-><init>(LX/HLM;)V

    move-object v1, v1

    .line 2455799
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIS;)LX/HIO;

    move-result-object v0

    .line 2455800
    new-instance v1, LX/HLL;

    invoke-direct {v1, p0}, LX/HLL;-><init>(LX/HLM;)V

    move-object v1, v1

    .line 2455801
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIU;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/HIO;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2455793
    const/4 v0, 0x0

    return v0
.end method
