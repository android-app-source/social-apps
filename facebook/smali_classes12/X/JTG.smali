.class public final LX/JTG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JTF;


# instance fields
.field public final synthetic a:LX/JTY;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/JTO;

.field public final synthetic d:LX/JTK;

.field public final synthetic e:LX/JTI;


# direct methods
.method public constructor <init>(LX/JTI;LX/JTY;Landroid/net/Uri;LX/JTO;LX/JTK;)V
    .locals 0

    .prologue
    .line 2696545
    iput-object p1, p0, LX/JTG;->e:LX/JTI;

    iput-object p2, p0, LX/JTG;->a:LX/JTY;

    iput-object p3, p0, LX/JTG;->b:Landroid/net/Uri;

    iput-object p4, p0, LX/JTG;->c:LX/JTO;

    iput-object p5, p0, LX/JTG;->d:LX/JTK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 2696529
    iget-object v0, p0, LX/JTG;->e:LX/JTI;

    iget-object v1, p0, LX/JTG;->a:LX/JTY;

    iget-object v2, p0, LX/JTG;->b:Landroid/net/Uri;

    iget-object v3, p0, LX/JTG;->c:LX/JTO;

    iget-object v4, p0, LX/JTG;->d:LX/JTK;

    const/4 v8, 0x0

    .line 2696530
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696531
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696532
    iget-object v5, v0, LX/JTI;->c:LX/0gc;

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/JTI;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2696533
    iget-object v5, v0, LX/JTI;->c:LX/0gc;

    sget-object v6, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->m:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2696534
    iget-object v5, v0, LX/JTI;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2696535
    :cond_0
    :goto_0
    return-void

    .line 2696536
    :cond_1
    new-instance v5, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;

    invoke-direct {v5}, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;-><init>()V

    .line 2696537
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696538
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696539
    new-instance v9, LX/JTE;

    move-object v10, v0

    move-object v11, v1

    move-object v12, v3

    move-object v13, v2

    move-object v14, v4

    invoke-direct/range {v9 .. v14}, LX/JTE;-><init>(LX/JTI;LX/JTY;LX/JTO;Landroid/net/Uri;LX/JTK;)V

    move-object v6, v9

    .line 2696540
    iput-object v6, v5, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->o:LX/JTE;

    .line 2696541
    iget-object v6, v0, LX/JTI;->c:LX/0gc;

    invoke-virtual {v6}, LX/0gc;->a()LX/0hH;

    move-result-object v6

    sget-object v7, Lcom/facebook/feedplugins/musicstory/providers/AuthorizationDialog;->m:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v5

    invoke-virtual {v5}, LX/0hH;->c()I

    .line 2696542
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    if-ne v5, v6, :cond_2

    .line 2696543
    iget-object v5, v0, LX/JTI;->c:LX/0gc;

    invoke-virtual {v5}, LX/0gc;->b()Z

    .line 2696544
    :cond_2
    iget-object v5, v0, LX/JTI;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method
