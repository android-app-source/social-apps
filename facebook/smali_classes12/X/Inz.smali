.class public final LX/Inz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611173
    iput-object p1, p0, LX/Inz;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2611174
    iget-object v0, p0, LX/Inz;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611175
    new-instance v1, LX/Inv;

    invoke-direct {v1, v0}, LX/Inv;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611176
    invoke-static {}, LX/Ik2;->newBuilder()LX/Ik0;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611177
    iget-object p0, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    move-object v3, p0

    .line 2611178
    iput-object v3, v2, LX/Ik0;->b:LX/0Px;

    .line 2611179
    move-object v2, v2

    .line 2611180
    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    .line 2611181
    iput-object v3, v2, LX/Ik0;->e:LX/5g0;

    .line 2611182
    move-object v2, v2

    .line 2611183
    const/4 v3, 0x0

    move v3, v3

    .line 2611184
    iput-boolean v3, v2, LX/Ik0;->k:Z

    .line 2611185
    move-object v2, v2

    .line 2611186
    iput-object v0, v2, LX/Ik0;->c:Landroid/support/v4/app/Fragment;

    .line 2611187
    move-object v2, v2

    .line 2611188
    invoke-virtual {v2}, LX/Ik0;->a()LX/Ik2;

    move-result-object v2

    .line 2611189
    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->f:LX/Ijy;

    invoke-virtual {v3, v2, v1}, LX/Ijy;->a(LX/Ik2;LX/Ijx;)V

    .line 2611190
    return-void
.end method

.method public final a(Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 2

    .prologue
    .line 2611191
    iget-object v0, p0, LX/Inz;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611192
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(LX/0am;)V

    .line 2611193
    return-void
.end method
