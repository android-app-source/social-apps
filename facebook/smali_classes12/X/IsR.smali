.class public final enum LX/IsR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IsR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IsR;

.field public static final enum CONTACTS:LX/IsR;

.field public static final enum FRIENDS:LX/IsR;

.field public static final enum GROUP_THREADS:LX/IsR;

.field public static final enum UNCONNECTED_USERS:LX/IsR;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2619711
    new-instance v0, LX/IsR;

    const-string v1, "CONTACTS"

    invoke-direct {v0, v1, v2}, LX/IsR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsR;->CONTACTS:LX/IsR;

    .line 2619712
    new-instance v0, LX/IsR;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v3}, LX/IsR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsR;->FRIENDS:LX/IsR;

    .line 2619713
    new-instance v0, LX/IsR;

    const-string v1, "GROUP_THREADS"

    invoke-direct {v0, v1, v4}, LX/IsR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsR;->GROUP_THREADS:LX/IsR;

    .line 2619714
    new-instance v0, LX/IsR;

    const-string v1, "UNCONNECTED_USERS"

    invoke-direct {v0, v1, v5}, LX/IsR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IsR;->UNCONNECTED_USERS:LX/IsR;

    .line 2619715
    const/4 v0, 0x4

    new-array v0, v0, [LX/IsR;

    sget-object v1, LX/IsR;->CONTACTS:LX/IsR;

    aput-object v1, v0, v2

    sget-object v1, LX/IsR;->FRIENDS:LX/IsR;

    aput-object v1, v0, v3

    sget-object v1, LX/IsR;->GROUP_THREADS:LX/IsR;

    aput-object v1, v0, v4

    sget-object v1, LX/IsR;->UNCONNECTED_USERS:LX/IsR;

    aput-object v1, v0, v5

    sput-object v0, LX/IsR;->$VALUES:[LX/IsR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2619716
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IsR;
    .locals 1

    .prologue
    .line 2619717
    const-class v0, LX/IsR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IsR;

    return-object v0
.end method

.method public static values()[LX/IsR;
    .locals 1

    .prologue
    .line 2619718
    sget-object v0, LX/IsR;->$VALUES:[LX/IsR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IsR;

    return-object v0
.end method
