.class public final enum LX/Iyu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iyu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iyu;

.field public static final enum DEFAULT:LX/Iyu;

.field public static final enum MFS:LX/Iyu;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2633996
    new-instance v0, LX/Iyu;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/Iyu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iyu;->DEFAULT:LX/Iyu;

    .line 2633997
    new-instance v0, LX/Iyu;

    const-string v1, "MFS"

    invoke-direct {v0, v1, v3}, LX/Iyu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iyu;->MFS:LX/Iyu;

    .line 2633998
    const/4 v0, 0x2

    new-array v0, v0, [LX/Iyu;

    sget-object v1, LX/Iyu;->DEFAULT:LX/Iyu;

    aput-object v1, v0, v2

    sget-object v1, LX/Iyu;->MFS:LX/Iyu;

    aput-object v1, v0, v3

    sput-object v0, LX/Iyu;->$VALUES:[LX/Iyu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2633993
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iyu;
    .locals 1

    .prologue
    .line 2633994
    const-class v0, LX/Iyu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iyu;

    return-object v0
.end method

.method public static values()[LX/Iyu;
    .locals 1

    .prologue
    .line 2633995
    sget-object v0, LX/Iyu;->$VALUES:[LX/Iyu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iyu;

    return-object v0
.end method
