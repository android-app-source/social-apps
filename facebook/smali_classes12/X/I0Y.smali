.class public final enum LX/I0Y;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I0Y;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I0Y;

.field public static final enum BIRTHDAYS:LX/I0Y;

.field public static final enum DATE_HEADER:LX/I0Y;

.field public static final enum EVENT_CANT_ATTEND_ROW:LX/I0Y;

.field public static final enum EVENT_NULLSTATE_ROW:LX/I0Y;

.field public static final enum EVENT_ROW:LX/I0Y;

.field public static final enum LOADING:LX/I0Y;

.field public static final enum PRIVATE_INVITES_HSCROLL:LX/I0Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2527396
    new-instance v0, LX/I0Y;

    const-string v1, "PRIVATE_INVITES_HSCROLL"

    invoke-direct {v0, v1, v3}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    .line 2527397
    new-instance v0, LX/I0Y;

    const-string v1, "DATE_HEADER"

    invoke-direct {v0, v1, v4}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->DATE_HEADER:LX/I0Y;

    .line 2527398
    new-instance v0, LX/I0Y;

    const-string v1, "BIRTHDAYS"

    invoke-direct {v0, v1, v5}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->BIRTHDAYS:LX/I0Y;

    .line 2527399
    new-instance v0, LX/I0Y;

    const-string v1, "EVENT_ROW"

    invoke-direct {v0, v1, v6}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->EVENT_ROW:LX/I0Y;

    .line 2527400
    new-instance v0, LX/I0Y;

    const-string v1, "EVENT_CANT_ATTEND_ROW"

    invoke-direct {v0, v1, v7}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->EVENT_CANT_ATTEND_ROW:LX/I0Y;

    .line 2527401
    new-instance v0, LX/I0Y;

    const-string v1, "EVENT_NULLSTATE_ROW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->EVENT_NULLSTATE_ROW:LX/I0Y;

    .line 2527402
    new-instance v0, LX/I0Y;

    const-string v1, "LOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/I0Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0Y;->LOADING:LX/I0Y;

    .line 2527403
    const/4 v0, 0x7

    new-array v0, v0, [LX/I0Y;

    sget-object v1, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    aput-object v1, v0, v3

    sget-object v1, LX/I0Y;->DATE_HEADER:LX/I0Y;

    aput-object v1, v0, v4

    sget-object v1, LX/I0Y;->BIRTHDAYS:LX/I0Y;

    aput-object v1, v0, v5

    sget-object v1, LX/I0Y;->EVENT_ROW:LX/I0Y;

    aput-object v1, v0, v6

    sget-object v1, LX/I0Y;->EVENT_CANT_ATTEND_ROW:LX/I0Y;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/I0Y;->EVENT_NULLSTATE_ROW:LX/I0Y;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/I0Y;->LOADING:LX/I0Y;

    aput-object v2, v0, v1

    sput-object v0, LX/I0Y;->$VALUES:[LX/I0Y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2527404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I0Y;
    .locals 1

    .prologue
    .line 2527405
    const-class v0, LX/I0Y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I0Y;

    return-object v0
.end method

.method public static values()[LX/I0Y;
    .locals 1

    .prologue
    .line 2527406
    sget-object v0, LX/I0Y;->$VALUES:[LX/I0Y;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I0Y;

    return-object v0
.end method
