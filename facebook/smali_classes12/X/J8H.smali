.class public LX/J8H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/reflect/Field;

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/reflect/Field;

.field private final e:LX/J8I;

.field private final f:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/J8I;)V
    .locals 2

    .prologue
    .line 2651915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2651916
    const-string v0, "android.os.StrictMode$ViolationInfo"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/J8H;->a:Ljava/lang/Class;

    .line 2651917
    iget-object v0, p0, LX/J8H;->a:Ljava/lang/Class;

    const-string v1, "crashInfo"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/J8H;->b:Ljava/lang/reflect/Field;

    .line 2651918
    iget-object v0, p0, LX/J8H;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/J8H;->c:Ljava/lang/Class;

    .line 2651919
    iget-object v0, p0, LX/J8H;->c:Ljava/lang/Class;

    const-string v1, "stackTrace"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, LX/J8H;->d:Ljava/lang/reflect/Field;

    .line 2651920
    iput-object p1, p0, LX/J8H;->f:Ljava/lang/Object;

    .line 2651921
    iput-object p2, p0, LX/J8H;->e:LX/J8I;

    .line 2651922
    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2651923
    const-string v0, "handleApplicationStrictModeViolation"

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2651924
    array-length v0, p3

    if-le v0, v2, :cond_0

    .line 2651925
    aget-object v0, p3, v2

    .line 2651926
    iget-object v1, p0, LX/J8H;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2651927
    iget-object v1, p0, LX/J8H;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2651928
    iget-object v1, p0, LX/J8H;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2651929
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2651930
    iget-object v1, p0, LX/J8H;->e:LX/J8I;

    check-cast v0, Ljava/lang/String;

    const/4 p2, 0x1

    .line 2651931
    if-nez v0, :cond_2

    .line 2651932
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 2651933
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, LX/J8H;->f:Ljava/lang/Object;

    invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 2651934
    :cond_2
    iget-object v2, v1, LX/J8I;->b:Ljava/util/Random;

    sget p0, LX/J8J;->a:I

    invoke-virtual {v2, p0}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    if-nez v2, :cond_0

    .line 2651935
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x0

    aget-object v2, v2, p0

    .line 2651936
    const-string p0, ": "

    invoke-virtual {v2, p0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    .line 2651937
    array-length p1, p0

    if-le p1, p2, :cond_3

    .line 2651938
    aget-object v2, p0, p2

    .line 2651939
    :cond_3
    iget-object p0, v1, LX/J8I;->a:LX/03V;

    invoke-virtual {p0, v2, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
