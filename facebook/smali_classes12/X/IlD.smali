.class public LX/IlD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607562
    new-instance v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2607563
    iget-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2607564
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1eaa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2607565
    iget-object v1, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2607566
    iget-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a03a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2607567
    iget-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextIsSelectable(Z)V

    .line 2607568
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607569
    iget-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 6

    .prologue
    .line 2607570
    iget-object v0, p1, LX/Il0;->a:LX/0am;

    move-object v0, v0

    .line 2607571
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2607572
    iget-object v0, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/IlD;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082d43

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2607573
    iget-object v5, p1, LX/Il0;->a:LX/0am;

    move-object v5, v5

    .line 2607574
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607575
    return-void
.end method
