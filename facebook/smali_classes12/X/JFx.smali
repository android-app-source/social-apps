.class public LX/JFx;
.super LX/JFS;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/JFu;

.field private final c:LX/JFF;

.field private final d:LX/JFv;

.field private final e:LX/JFw;

.field private final f:LX/JFD;

.field private final g:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/JFx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/JFx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/JFD;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, LX/JFS;-><init>()V

    iput-object v0, p0, LX/JFx;->b:LX/JFu;

    iput-object v0, p0, LX/JFx;->c:LX/JFF;

    iput-object v0, p0, LX/JFx;->d:LX/JFv;

    iput-object v0, p0, LX/JFx;->e:LX/JFw;

    iput-object p1, p0, LX/JFx;->f:LX/JFD;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/JFx;->g:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(LX/JFF;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, LX/JFS;-><init>()V

    iput-object v0, p0, LX/JFx;->b:LX/JFu;

    iput-object p1, p0, LX/JFx;->c:LX/JFF;

    iput-object v0, p0, LX/JFx;->d:LX/JFv;

    iput-object v0, p0, LX/JFx;->e:LX/JFw;

    iput-object v0, p0, LX/JFx;->f:LX/JFD;

    iput-object v0, p0, LX/JFx;->g:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    iget-object v0, p0, LX/JFx;->e:LX/JFw;

    invoke-virtual {v0, p1}, LX/2wf;->a(LX/2NW;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, LX/JFx;->b:LX/JFu;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "placeEstimator cannot be null"

    invoke-static {v0, v1}, LX/1ol;->a(ZLjava/lang/Object;)V

    if-nez p1, :cond_3

    sget-object v0, LX/JFx;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, LX/JFx;->a:Ljava/lang/String;

    const-string v2, "onPlaceEstimated received null DataHolder: "

    invoke-static {}, LX/4tc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, LX/JFx;->b:LX/JFu;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/common/data/DataHolder;->i:Landroid/os/Bundle;

    move-object v0, v0

    if-nez v0, :cond_4

    const/16 v0, 0x64

    :goto_3
    new-instance v1, LX/JF0;

    iget-object v2, p0, LX/JFx;->g:Landroid/content/Context;

    invoke-direct {v1, p1, v0, v2}, LX/JF0;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V

    iget-object v0, p0, LX/JFx;->b:LX/JFu;

    invoke-virtual {v0, v1}, LX/2wf;->a(LX/2NW;)V

    goto :goto_2

    :cond_4
    const-string v1, "com.google.android.gms.location.places.PlaceLikelihoodBuffer.SOURCE_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move v0, v1

    goto :goto_3
.end method

.method public final b(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    if-nez p1, :cond_2

    sget-object v0, LX/JFx;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, LX/JFx;->a:Ljava/lang/String;

    const-string v2, "onAutocompletePrediction received null DataHolder: "

    invoke-static {}, LX/4tc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, LX/JFx;->c:LX/JFF;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/JFx;->c:LX/JFF;

    new-instance v1, LX/JEs;

    invoke-direct {v1, p1}, LX/JEs;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, LX/2wf;->a(LX/2NW;)V

    goto :goto_1
.end method

.method public final c(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    const/4 v4, 0x0

    if-nez p1, :cond_2

    sget-object v0, LX/JFx;->a:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, LX/JFx;->a:Ljava/lang/String;

    const-string v2, "onPlaceUserDataFetched received null DataHolder: "

    invoke-static {}, LX/4tc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v4, v0}, LX/2we;->b(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, LX/JFk;

    invoke-direct {v0, p1}, LX/JFk;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v4, v0}, LX/2wf;->a(LX/2NW;)V

    goto :goto_1
.end method

.method public final d(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    new-instance v0, LX/JEv;

    iget-object v1, p0, LX/JFx;->g:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, LX/JEv;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/content/Context;)V

    iget-object v1, p0, LX/JFx;->f:LX/JFD;

    invoke-virtual {v1, v0}, LX/2wf;->a(LX/2NW;)V

    return-void
.end method
