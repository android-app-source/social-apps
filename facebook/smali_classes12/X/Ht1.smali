.class public final LX/Ht1;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ht2;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/ADz;

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

.field public d:LX/HtA;

.field public final synthetic e:LX/Ht2;


# direct methods
.method public constructor <init>(LX/Ht2;)V
    .locals 1

    .prologue
    .line 2515280
    iput-object p1, p0, LX/Ht1;->e:LX/Ht2;

    .line 2515281
    move-object v0, p1

    .line 2515282
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2515283
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2515284
    const-string v0, "ReshareAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2515285
    if-ne p0, p1, :cond_1

    .line 2515286
    :cond_0
    :goto_0
    return v0

    .line 2515287
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2515288
    goto :goto_0

    .line 2515289
    :cond_3
    check-cast p1, LX/Ht1;

    .line 2515290
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2515291
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2515292
    if-eq v2, v3, :cond_0

    .line 2515293
    iget-object v2, p0, LX/Ht1;->a:LX/ADz;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ht1;->a:LX/ADz;

    iget-object v3, p1, LX/Ht1;->a:LX/ADz;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2515294
    goto :goto_0

    .line 2515295
    :cond_5
    iget-object v2, p1, LX/Ht1;->a:LX/ADz;

    if-nez v2, :cond_4

    .line 2515296
    :cond_6
    iget-object v2, p0, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2515297
    goto :goto_0

    .line 2515298
    :cond_8
    iget-object v2, p1, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_7

    .line 2515299
    :cond_9
    iget-object v2, p0, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iget-object v3, p1, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2515300
    goto :goto_0

    .line 2515301
    :cond_b
    iget-object v2, p1, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-nez v2, :cond_a

    .line 2515302
    :cond_c
    iget-object v2, p0, LX/Ht1;->d:LX/HtA;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Ht1;->d:LX/HtA;

    iget-object v3, p1, LX/Ht1;->d:LX/HtA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2515303
    goto :goto_0

    .line 2515304
    :cond_d
    iget-object v2, p1, LX/Ht1;->d:LX/HtA;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2515305
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Ht1;

    .line 2515306
    const/4 v1, 0x0

    iput-object v1, v0, LX/Ht1;->a:LX/ADz;

    .line 2515307
    return-object v0
.end method
