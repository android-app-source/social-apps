.class public LX/JPN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0Ot;LX/0Or;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2689657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2689658
    iput-object p1, p0, LX/JPN;->a:LX/0Ot;

    .line 2689659
    iput-object p2, p0, LX/JPN;->b:LX/0Or;

    .line 2689660
    iput-object p3, p0, LX/JPN;->c:LX/0Ot;

    .line 2689661
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/JPM;
    .locals 2

    .prologue
    .line 2689639
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2689640
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal hpp card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689641
    :sswitch_0
    const-string v1, "ActivityInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "AdsActiveCampaignInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "AdsCompletedCampaignInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "AdsMultiCampaignsInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "AYMTUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "LikeFollowerUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "LikeInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "MultiPagesUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "PostEngagementInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "ReachInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "ResponseInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v1, "VideoInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "ViewInsightsUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "VisitPageUnitItem"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    .line 2689642
    :pswitch_0
    sget-object v0, LX/JPM;->ACTIVITY_INSIGHTS:LX/JPM;

    .line 2689643
    :goto_1
    return-object v0

    .line 2689644
    :pswitch_1
    sget-object v0, LX/JPM;->ADS_ACTIVE_CAMPAIGN_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689645
    :pswitch_2
    sget-object v0, LX/JPM;->ADS_COMPLETED_CAMPAIGN_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689646
    :pswitch_3
    sget-object v0, LX/JPM;->ADS_MULTI_CAMPAIGNS_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689647
    :pswitch_4
    sget-object v0, LX/JPM;->AYMT:LX/JPM;

    goto :goto_1

    .line 2689648
    :pswitch_5
    sget-object v0, LX/JPM;->LIKE_FOLLOWER:LX/JPM;

    goto :goto_1

    .line 2689649
    :pswitch_6
    sget-object v0, LX/JPM;->LIKE_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689650
    :pswitch_7
    sget-object v0, LX/JPM;->MULTI_PAGE:LX/JPM;

    goto :goto_1

    .line 2689651
    :pswitch_8
    sget-object v0, LX/JPM;->POST_ENGAGEMENT:LX/JPM;

    goto :goto_1

    .line 2689652
    :pswitch_9
    sget-object v0, LX/JPM;->REACH_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689653
    :pswitch_a
    sget-object v0, LX/JPM;->RESPONSE_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689654
    :pswitch_b
    sget-object v0, LX/JPM;->VIDEO_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689655
    :pswitch_c
    sget-object v0, LX/JPM;->VIEW_INSIGHTS:LX/JPM;

    goto :goto_1

    .line 2689656
    :pswitch_d
    sget-object v0, LX/JPM;->VISIT_PAGE:LX/JPM;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7209f934 -> :sswitch_5
        -0x3e86eb54 -> :sswitch_3
        -0x3573a04a -> :sswitch_4
        -0x342e2dfe -> :sswitch_7
        -0x1d776863 -> :sswitch_2
        -0x14f72dcd -> :sswitch_a
        0xf56ef1 -> :sswitch_8
        0x1a1dbeb8 -> :sswitch_1
        0x27914a2d -> :sswitch_b
        0x27f61189 -> :sswitch_6
        0x2bd25501 -> :sswitch_0
        0x2fde1f25 -> :sswitch_9
        0x3fe77731 -> :sswitch_d
        0x44cec097 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static a(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            "Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2689619
    if-nez p3, :cond_0

    .line 2689620
    const-string v1, "campaignInsightSummary == null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2689621
    :goto_0
    return v0

    .line 2689622
    :cond_0
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2689623
    const-string v1, "campaignInsightSummary campaign name is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689624
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2689625
    const-string v1, "campaignInsightSummary campaign status title is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689626
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2689627
    const-string v1, "campaignInsightSummary image uri is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689628
    :cond_3
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2689629
    const-string v1, "campaignInsightSummary landing uri is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689630
    :cond_4
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2689631
    const-string v1, "campaignInsightSummary objective result title is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689632
    :cond_5
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2689633
    const-string v1, "campaignInsightSummary people reached title is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689634
    :cond_6
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2689635
    const-string v1, "campaignInsightSummary spent meter spent title is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689636
    :cond_7
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2689637
    const-string v1, "campaignInsightSummary spent meter total title is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689638
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)LX/JPN;
    .locals 4

    .prologue
    .line 2689617
    new-instance v0, LX/JPN;

    const/16 v1, 0xbc

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/JPN;-><init>(LX/0Ot;LX/0Or;LX/0Ot;)V

    .line 2689618
    return-object v0
.end method

.method public static g(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2689453
    if-nez p2, :cond_0

    .line 2689454
    const-string v0, "feedUnitItem == null"

    invoke-virtual {p0, p1, p2, v0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2689455
    const/4 v0, 0x0

    .line 2689456
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2689516
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2689517
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2689518
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2689519
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2689520
    :cond_0
    if-nez v0, :cond_2

    .line 2689521
    const-string v0, "no page id. cannot log tap"

    invoke-virtual {p0, p1, p2, v0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2689522
    :cond_1
    :goto_0
    return-void

    .line 2689523
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2689524
    const-string v0, "feedUnitItem.getGraphQLObjectType() == null. cannot log tap"

    invoke-virtual {p0, p1, p2, v0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689525
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2689526
    iget-object v0, p0, LX/JPN;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2689527
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2689528
    check-cast v0, LX/16h;

    invoke-static {p2, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    .line 2689529
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/JPN;->a(Ljava/lang/String;)LX/JPM;

    move-result-object v0

    .line 2689530
    sget-object v6, LX/JPL;->a:[I

    invoke-virtual {v0}, LX/JPM;->ordinal()I

    move-result v0

    aget v0, v6, v0

    packed-switch v0, :pswitch_data_0

    .line 2689531
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal hpp card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689532
    :pswitch_0
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689533
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_activity_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->ACTIVITY_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689534
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689535
    move-object v6, v6

    .line 2689536
    move-object v1, v6

    .line 2689537
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689538
    :pswitch_1
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->x()Ljava/lang/String;

    move-result-object v6

    .line 2689539
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "page_admin_panel_active_campaign_insights_tap"

    invoke-direct {v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "admin_id"

    invoke-virtual {v7, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "event_name"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_id"

    invoke-virtual {v7, p0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "source"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->ACTIVE_CAMPAIGN_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "campaign_id"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_admin_panel"

    .line 2689540
    iput-object p0, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689541
    move-object v7, v7

    .line 2689542
    move-object v1, v7

    .line 2689543
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689544
    :pswitch_2
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->x()Ljava/lang/String;

    move-result-object v6

    .line 2689545
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "page_admin_panel_completed_campaign_insights_tap"

    invoke-direct {v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "admin_id"

    invoke-virtual {v7, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "event_name"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_id"

    invoke-virtual {v7, p0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "source"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->COMPLETED_CAMPAIGN_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "campaign_id"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_admin_panel"

    .line 2689546
    iput-object p0, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689547
    move-object v7, v7

    .line 2689548
    move-object v1, v7

    .line 2689549
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689550
    :pswitch_3
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689551
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_multi_campaigns_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->MULTI_CAMPAIGNS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689552
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689553
    move-object v6, v6

    .line 2689554
    move-object v1, v6

    .line 2689555
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689556
    :pswitch_4
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2689557
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v6

    .line 2689558
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "page_admin_panel_new_aymt_tap"

    invoke-direct {v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "admin_id"

    invoke-virtual {v7, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "event_name"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_id"

    invoke-virtual {v7, p0, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "source"

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->NEW_AYMT:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "action"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "page_admin_panel"

    .line 2689559
    iput-object p0, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689560
    move-object v7, v7

    .line 2689561
    move-object v1, v7

    .line 2689562
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689563
    :pswitch_5
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689564
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_like_followers_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->LIKE_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689565
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689566
    move-object v6, v6

    .line 2689567
    move-object v1, v6

    .line 2689568
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689569
    :pswitch_6
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689570
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_like_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->LIKE_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689571
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689572
    move-object v6, v6

    .line 2689573
    move-object v1, v6

    .line 2689574
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689575
    :pswitch_7
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689576
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_multi_pages_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->MULTI_PAGES:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689577
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689578
    move-object v6, v6

    .line 2689579
    move-object v1, v6

    .line 2689580
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689581
    :pswitch_8
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689582
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_post_engagement_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->POST_ENGAGEMENT_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689583
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689584
    move-object v6, v6

    .line 2689585
    move-object v1, v6

    .line 2689586
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689587
    :pswitch_9
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689588
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_reach_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->REACH_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689589
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689590
    move-object v6, v6

    .line 2689591
    move-object v1, v6

    .line 2689592
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689593
    :pswitch_a
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689594
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_response_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->RESPONSE_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689595
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689596
    move-object v6, v6

    .line 2689597
    move-object v1, v6

    .line 2689598
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689599
    :pswitch_b
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689600
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_video_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->VIDEO_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689601
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689602
    move-object v6, v6

    .line 2689603
    move-object v1, v6

    .line 2689604
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689605
    :pswitch_c
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689606
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_view_insights_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->VIEW_INSIGHTS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689607
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689608
    move-object v6, v6

    .line 2689609
    move-object v1, v6

    .line 2689610
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    .line 2689611
    :pswitch_d
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689612
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "page_admin_panel_visit_page_tap"

    invoke-direct {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "tracking"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "admin_id"

    invoke-virtual {v6, v7, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "event_name"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source"

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "page_admin_panel"

    .line 2689613
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689614
    move-object v6, v6

    .line 2689615
    move-object v1, v6

    .line 2689616
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2689501
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2689502
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 2689503
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2689504
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    move-object v4, v0

    .line 2689505
    :goto_0
    if-nez v4, :cond_0

    .line 2689506
    const-string v0, "no page id. cannot log tap"

    invoke-virtual {p0, p1, p2, v0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2689507
    :goto_1
    return-void

    .line 2689508
    :cond_0
    iget-object v0, p0, LX/JPN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 2689509
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2689510
    check-cast v1, LX/16h;

    invoke-static {p2, v1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    iget-object v2, p0, LX/JPN;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object v6, p3

    .line 2689511
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "page_admin_panel_multi_campaign_row_tap"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "admin_id"

    invoke-virtual {p0, p1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "event_name"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "page_id"

    invoke-virtual {p0, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source"

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->MULTI_CAMPAIGNS:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "campaign_id"

    invoke-virtual {p0, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "page_admin_panel"

    .line 2689512
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2689513
    move-object p0, p0

    .line 2689514
    move-object v1, p0

    .line 2689515
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    :cond_1
    move-object v4, v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2689477
    if-nez p2, :cond_0

    .line 2689478
    iget-object v0, p0, LX/JPN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "mhpp_error"

    const-string v2, "feedUnitItem is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2689479
    :goto_0
    return-void

    .line 2689480
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2689481
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2689482
    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/JPN;->a(Ljava/lang/String;)LX/JPM;

    move-result-object v0

    .line 2689483
    const-string v2, "card: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689484
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2689485
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2689486
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2689487
    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 2689488
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2689489
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2689490
    :cond_2
    if-nez v0, :cond_3

    .line 2689491
    const-string v0, "no page id. "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689492
    :goto_2
    const-string v0, "user: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689493
    iget-object v0, p0, LX/JPN;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2689494
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689495
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689496
    iget-object v0, p0, LX/JPN;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "mhpp_error"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2689497
    :catch_0
    const-string v0, "no card type. "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2689498
    :cond_3
    const-string v2, "page: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2689499
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 2689500
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public final c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2689466
    invoke-virtual {p0, p1, p2}, LX/JPN;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2689467
    :goto_0
    return v0

    .line 2689468
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y()LX/0Px;

    move-result-object v3

    .line 2689469
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2689470
    :cond_1
    const-string v0, "campaignInsightSummaryList is null or empty"

    invoke-virtual {p0, p1, p2, v0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    move v0, v1

    .line 2689471
    goto :goto_0

    .line 2689472
    :cond_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 2689473
    invoke-static {p0, p1, p2, v0}, LX/JPN;->a(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2689474
    goto :goto_0

    .line 2689475
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2689476
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2689457
    invoke-static {p0, p1, p2}, LX/JPN;->g(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2689458
    :goto_0
    return v0

    .line 2689459
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2689460
    const-string v1, "button GraphQLTextWithEntities is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689461
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2689462
    const-string v1, "button text is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689463
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2689464
    const-string v1, "button uri is null"

    invoke-virtual {p0, p1, p2, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_0

    .line 2689465
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
