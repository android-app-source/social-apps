.class public LX/IkP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/IkB;

.field private final b:LX/InT;

.field private final c:[I


# direct methods
.method public constructor <init>(LX/IkB;LX/InT;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606699
    iput-object p1, p0, LX/IkP;->a:LX/IkB;

    .line 2606700
    iput-object p2, p0, LX/IkP;->b:LX/InT;

    .line 2606701
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/IkP;->c:[I

    .line 2606702
    return-void
.end method

.method public static a(LX/0QB;)LX/IkP;
    .locals 5

    .prologue
    .line 2606687
    const-class v1, LX/IkP;

    monitor-enter v1

    .line 2606688
    :try_start_0
    sget-object v0, LX/IkP;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606689
    sput-object v2, LX/IkP;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606690
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606691
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606692
    new-instance p0, LX/IkP;

    invoke-static {v0}, LX/IkB;->a(LX/0QB;)LX/IkB;

    move-result-object v3

    check-cast v3, LX/IkB;

    invoke-static {v0}, LX/InT;->b(LX/0QB;)LX/InT;

    move-result-object v4

    check-cast v4, LX/InT;

    invoke-direct {p0, v3, v4}, LX/IkP;-><init>(LX/IkB;LX/InT;)V

    .line 2606693
    move-object v0, p0

    .line 2606694
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606695
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606696
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606697
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
