.class public LX/HFr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HFr;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2444340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2444341
    iput-object p1, p0, LX/HFr;->a:LX/0ad;

    .line 2444342
    return-void
.end method

.method public static a(LX/0QB;)LX/HFr;
    .locals 4

    .prologue
    .line 2444327
    sget-object v0, LX/HFr;->b:LX/HFr;

    if-nez v0, :cond_1

    .line 2444328
    const-class v1, LX/HFr;

    monitor-enter v1

    .line 2444329
    :try_start_0
    sget-object v0, LX/HFr;->b:LX/HFr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2444330
    if-eqz v2, :cond_0

    .line 2444331
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2444332
    new-instance p0, LX/HFr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/HFr;-><init>(LX/0ad;)V

    .line 2444333
    move-object v0, p0

    .line 2444334
    sput-object v0, LX/HFr;->b:LX/HFr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2444335
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2444336
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2444337
    :cond_1
    sget-object v0, LX/HFr;->b:LX/HFr;

    return-object v0

    .line 2444338
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2444339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
