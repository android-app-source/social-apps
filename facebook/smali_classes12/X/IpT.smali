.class public LX/IpT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ing;


# instance fields
.field private final a:LX/5fv;

.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:LX/3Ed;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field public final f:Landroid/content/Context;

.field public final g:LX/IpQ;

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field public j:LX/Io2;


# direct methods
.method public constructor <init>(LX/5fv;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/3Ed;LX/03V;Ljava/util/concurrent/Executor;Landroid/content/Context;LX/IpQ;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612981
    iput-object p1, p0, LX/IpT;->a:LX/5fv;

    .line 2612982
    iput-object p2, p0, LX/IpT;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2612983
    iput-object p3, p0, LX/IpT;->c:LX/3Ed;

    .line 2612984
    iput-object p4, p0, LX/IpT;->e:LX/03V;

    .line 2612985
    iput-object p5, p0, LX/IpT;->d:Ljava/util/concurrent/Executor;

    .line 2612986
    iput-object p6, p0, LX/IpT;->f:Landroid/content/Context;

    .line 2612987
    iput-object p7, p0, LX/IpT;->g:LX/IpQ;

    .line 2612988
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2612976
    iget-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2612977
    iget-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612978
    const/4 v0, 0x0

    iput-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612979
    :cond_0
    return-void
.end method

.method public final a(LX/Io2;)V
    .locals 0

    .prologue
    .line 2612974
    iput-object p1, p0, LX/IpT;->j:LX/Io2;

    .line 2612975
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 2612946
    iput-object p2, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612947
    iget-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612948
    :goto_0
    return-void

    .line 2612949
    :cond_0
    const-string v0, "orion_messenger_pay_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2612950
    new-instance v9, LX/IpR;

    invoke-direct {v9, p0}, LX/IpR;-><init>(LX/IpT;)V

    .line 2612951
    :try_start_0
    iget-object v0, p0, LX/IpT;->a:LX/5fv;

    iget-object v1, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612952
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v1, v2

    .line 2612953
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2612954
    iget-object v2, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612955
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v3

    .line 2612956
    iget-object v3, v2, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2612957
    invoke-virtual {v0, v1, v2}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2612958
    iget-object v0, p0, LX/IpT;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2612959
    iget-object v2, v1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v1, v2

    .line 2612960
    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/IpT;->c:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612961
    iget-object v4, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v3, v4

    .line 2612962
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612963
    iget-object v5, v4, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    move-object v4, v5

    .line 2612964
    iget-object v5, v7, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->g:Ljava/lang/String;

    iget-object v6, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612965
    iget-object v10, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v6, v10

    .line 2612966
    if-nez v6, :cond_1

    move-object v6, v8

    :goto_1
    iget-object v10, v7, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    if-nez v10, :cond_2

    move-object v7, v8

    :goto_2
    invoke-virtual/range {v0 .. v7}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612967
    iget-object v0, p0, LX/IpT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/IpT;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v9, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2612968
    iget-object v0, p0, LX/IpT;->g:LX/IpQ;

    const-string v1, "p2p_confirm_request"

    iget-object v2, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/IpQ;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0

    .line 2612969
    :catch_0
    move-exception v0

    .line 2612970
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2612971
    :cond_1
    iget-object v6, p0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612972
    iget-object v10, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v6, v10

    .line 2612973
    invoke-virtual {v6}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->d()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    iget-object v7, v7, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-virtual {v7}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_2
.end method
