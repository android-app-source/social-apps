.class public final LX/IIs;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562217
    iput-object p1, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 3

    .prologue
    .line 2562218
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->a()V

    .line 2562219
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562220
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562221
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562222
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const v2, 0x7f083851

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V

    .line 2562223
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    sget-object v1, LX/IIC;->LOCATION_DISABLED:LX/IIC;

    invoke-virtual {v0, v1}, LX/IID;->a(LX/IIC;)V

    .line 2562224
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 2562225
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2562226
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->au:LX/6Zb;

    new-instance v2, LX/2si;

    invoke-direct {v2}, LX/2si;-><init>()V

    const-string v3, "surface_nearby_friends"

    const-string p0, "mechanism_turn_on_button"

    invoke-virtual {v1, v2, v3, p0}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2562227
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 2562228
    iget-object v0, p0, LX/IIs;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Z)V

    .line 2562229
    return-void
.end method
