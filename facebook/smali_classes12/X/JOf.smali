.class public LX/JOf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JOq;

.field public final b:LX/JOt;


# direct methods
.method public constructor <init>(LX/JOq;LX/JOt;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2687999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688000
    iput-object p1, p0, LX/JOf;->a:LX/JOq;

    .line 2688001
    iput-object p2, p0, LX/JOf;->b:LX/JOt;

    .line 2688002
    return-void
.end method

.method public static a(LX/0QB;)LX/JOf;
    .locals 5

    .prologue
    .line 2687988
    const-class v1, LX/JOf;

    monitor-enter v1

    .line 2687989
    :try_start_0
    sget-object v0, LX/JOf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687990
    sput-object v2, LX/JOf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687991
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687992
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687993
    new-instance p0, LX/JOf;

    invoke-static {v0}, LX/JOq;->a(LX/0QB;)LX/JOq;

    move-result-object v3

    check-cast v3, LX/JOq;

    invoke-static {v0}, LX/JOt;->a(LX/0QB;)LX/JOt;

    move-result-object v4

    check-cast v4, LX/JOt;

    invoke-direct {p0, v3, v4}, LX/JOf;-><init>(LX/JOq;LX/JOt;)V

    .line 2687994
    move-object v0, p0

    .line 2687995
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687996
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687997
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687998
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
