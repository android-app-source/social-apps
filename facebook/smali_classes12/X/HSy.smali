.class public LX/HSy;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HSy;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/pages/fb4a/intent_builder/impl/IsDefaultPageUriIntentEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2468188
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2468189
    iput-object p1, p0, LX/HSy;->a:LX/0Or;

    .line 2468190
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2468191
    const-string v1, "extra_parent_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2468192
    const-string v1, "page/{#%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.facebook.katana.profile.id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->NATIVE_PAGES_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2468193
    return-void
.end method

.method public static a(LX/0QB;)LX/HSy;
    .locals 4

    .prologue
    .line 2468195
    sget-object v0, LX/HSy;->b:LX/HSy;

    if-nez v0, :cond_1

    .line 2468196
    const-class v1, LX/HSy;

    monitor-enter v1

    .line 2468197
    :try_start_0
    sget-object v0, LX/HSy;->b:LX/HSy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2468198
    if-eqz v2, :cond_0

    .line 2468199
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2468200
    new-instance v3, LX/HSy;

    const/16 p0, 0x153e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HSy;-><init>(LX/0Or;)V

    .line 2468201
    move-object v0, v3

    .line 2468202
    sput-object v0, LX/HSy;->b:LX/HSy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2468203
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2468204
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2468205
    :cond_1
    sget-object v0, LX/HSy;->b:LX/HSy;

    return-object v0

    .line 2468206
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2468207
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2468194
    iget-object v0, p0, LX/HSy;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
