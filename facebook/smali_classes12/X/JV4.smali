.class public LX/JV4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JV2;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JV7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700177
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JV4;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JV7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700174
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700175
    iput-object p1, p0, LX/JV4;->b:LX/0Ot;

    .line 2700176
    return-void
.end method

.method public static a(LX/0QB;)LX/JV4;
    .locals 4

    .prologue
    .line 2700116
    const-class v1, LX/JV4;

    monitor-enter v1

    .line 2700117
    :try_start_0
    sget-object v0, LX/JV4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700118
    sput-object v2, LX/JV4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700119
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700120
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700121
    new-instance v3, LX/JV4;

    const/16 p0, 0x208c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JV4;-><init>(LX/0Ot;)V

    .line 2700122
    move-object v0, v3

    .line 2700123
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700124
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JV4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700125
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700126
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2700129
    check-cast p2, LX/JV3;

    .line 2700130
    iget-object v0, p0, LX/JV4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JV7;

    iget-object v1, p2, LX/JV3;->a:LX/1Pf;

    iget-object v2, p2, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700131
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JV7;->a:LX/JVJ;

    const/4 v5, 0x0

    .line 2700132
    new-instance p0, LX/JVI;

    invoke-direct {p0, v4}, LX/JVI;-><init>(LX/JVJ;)V

    .line 2700133
    sget-object p2, LX/JVJ;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JVH;

    .line 2700134
    if-nez p2, :cond_0

    .line 2700135
    new-instance p2, LX/JVH;

    invoke-direct {p2}, LX/JVH;-><init>()V

    .line 2700136
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/JVH;->a$redex0(LX/JVH;LX/1De;IILX/JVI;)V

    .line 2700137
    move-object p0, p2

    .line 2700138
    move-object v5, p0

    .line 2700139
    move-object v4, v5

    .line 2700140
    iget-object v5, v4, LX/JVH;->a:LX/JVI;

    iput-object v1, v5, LX/JVI;->b:LX/1Pf;

    .line 2700141
    iget-object v5, v4, LX/JVH;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2700142
    move-object v4, v4

    .line 2700143
    iget-object v5, v4, LX/JVH;->a:LX/JVI;

    iput-object v2, v5, LX/JVI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700144
    iget-object v5, v4, LX/JVH;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2700145
    move-object v4, v4

    .line 2700146
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JV7;->b:LX/JVE;

    const/4 v5, 0x0

    .line 2700147
    new-instance p0, LX/JVD;

    invoke-direct {p0, v4}, LX/JVD;-><init>(LX/JVE;)V

    .line 2700148
    sget-object p2, LX/JVE;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JVC;

    .line 2700149
    if-nez p2, :cond_1

    .line 2700150
    new-instance p2, LX/JVC;

    invoke-direct {p2}, LX/JVC;-><init>()V

    .line 2700151
    :cond_1
    invoke-static {p2, p1, v5, v5, p0}, LX/JVC;->a$redex0(LX/JVC;LX/1De;IILX/JVD;)V

    .line 2700152
    move-object p0, p2

    .line 2700153
    move-object v5, p0

    .line 2700154
    move-object v4, v5

    .line 2700155
    iget-object v5, v4, LX/JVC;->a:LX/JVD;

    iput-object v1, v5, LX/JVD;->a:LX/1Pf;

    .line 2700156
    iget-object v5, v4, LX/JVC;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2700157
    move-object v4, v4

    .line 2700158
    iget-object v5, v4, LX/JVC;->a:LX/JVD;

    iput-object v2, v5, LX/JVD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700159
    iget-object v5, v4, LX/JVC;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2700160
    move-object v4, v4

    .line 2700161
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/4 p0, 0x1

    .line 2700162
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a009f

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b00cf

    invoke-interface {v4, p0, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 2700163
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JV7;->c:LX/JVA;

    const/4 v5, 0x0

    .line 2700164
    new-instance p0, LX/JV9;

    invoke-direct {p0, v4}, LX/JV9;-><init>(LX/JVA;)V

    .line 2700165
    sget-object p2, LX/JVA;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JV8;

    .line 2700166
    if-nez p2, :cond_2

    .line 2700167
    new-instance p2, LX/JV8;

    invoke-direct {p2}, LX/JV8;-><init>()V

    .line 2700168
    :cond_2
    invoke-static {p2, p1, v5, v5, p0}, LX/JV8;->a$redex0(LX/JV8;LX/1De;IILX/JV9;)V

    .line 2700169
    move-object p0, p2

    .line 2700170
    move-object v5, p0

    .line 2700171
    move-object v4, v5

    .line 2700172
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2700173
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2700127
    invoke-static {}, LX/1dS;->b()V

    .line 2700128
    const/4 v0, 0x0

    return-object v0
.end method
