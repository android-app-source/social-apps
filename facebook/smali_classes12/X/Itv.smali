.class public final enum LX/Itv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Itv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Itv;

.field public static final enum MQTT_EXCEPTION:LX/Itv;

.field public static final enum MQTT_IOEXCEPTION:LX/Itv;

.field public static final enum MQTT_REMOTEEXCEPTION:LX/Itv;

.field public static final enum NONE:LX/Itv;

.field public static final enum SEND_FAILED_NOT_CONNECTED:LX/Itv;

.field public static final enum SEND_FAILED_NO_RETRY:LX/Itv;

.field public static final enum SEND_FAILED_PUBLISH_FAILED:LX/Itv;

.field public static final enum SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

.field public static final enum SEND_FAILED_SERVER_RETURNED_FAILURE:LX/Itv;

.field public static final enum SEND_FAILED_THRIFT_EXCEPTION:LX/Itv;

.field public static final enum SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

.field public static final enum SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

.field public static final enum SEND_FAILED_UNKNOWN_EXCEPTION:LX/Itv;

.field public static final enum SEND_SKIPPED_BROADCAST:LX/Itv;

.field public static final enum SEND_SKIPPED_DYNAMIC_PRICING_ENABLED:LX/Itv;

.field public static final enum SEND_SKIPPED_FB_SHARE:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_APP_ATTRIBUTION:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_EVENT_MESSAGE:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_FLOWER_BORDER:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_LOCATION_SHARE:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_MONTAGE_REPLY:LX/Itv;

.field public static final enum SEND_SKIPPED_HAS_PLATFORM_METADATA:LX/Itv;

.field public static final enum SEND_SKIPPED_JSON_EXCEPTION:LX/Itv;

.field public static final enum SEND_SKIPPED_LIGHTWEIGHT_ACTION_MESSAGE:LX/Itv;

.field public static final enum SEND_SKIPPED_LIVE_LOCATION_MESSAGE:LX/Itv;

.field public static final enum SEND_SKIPPED_MEDIA_UPLOAD_FAILED:LX/Itv;

.field public static final enum SEND_SKIPPED_PAYMENT_MESSAGE:LX/Itv;

.field public static final enum SEND_SKIPPED_XMA_MESSAGE:LX/Itv;


# instance fields
.field public final errorCode:I

.field public final message:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2625122
    new-instance v0, LX/Itv;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v2, v5}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->NONE:LX/Itv;

    .line 2625123
    new-instance v0, LX/Itv;

    const-string v1, "MQTT_EXCEPTION"

    const-string v2, "mqtt exception"

    const/16 v3, 0x384

    invoke-direct {v0, v1, v6, v2, v3}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    .line 2625124
    new-instance v0, LX/Itv;

    const-string v1, "MQTT_REMOTEEXCEPTION"

    const-string v2, "mqtt remote exception"

    const/16 v3, 0x385

    invoke-direct {v0, v1, v7, v2, v3}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->MQTT_REMOTEEXCEPTION:LX/Itv;

    .line 2625125
    new-instance v0, LX/Itv;

    const-string v1, "MQTT_IOEXCEPTION"

    const-string v2, "mqtt io exception"

    const/16 v3, 0x386

    invoke-direct {v0, v1, v8, v2, v3}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->MQTT_IOEXCEPTION:LX/Itv;

    .line 2625126
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_BROADCAST"

    const-string v2, "message is a broadcast"

    const/16 v3, 0x387

    invoke-direct {v0, v1, v9, v2, v3}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_BROADCAST:LX/Itv;

    .line 2625127
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_FB_SHARE"

    const/4 v2, 0x5

    const-string v3, "message is a fb story share"

    const/16 v4, 0x388

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_FB_SHARE:LX/Itv;

    .line 2625128
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_PAYMENT_MESSAGE"

    const/4 v2, 0x6

    const-string v3, "message is a payment message"

    const/16 v4, 0x389

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_PAYMENT_MESSAGE:LX/Itv;

    .line 2625129
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_APP_ATTRIBUTION"

    const/4 v2, 0x7

    const-string v3, "message has app attribution"

    const/16 v4, 0x38a

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_APP_ATTRIBUTION:LX/Itv;

    .line 2625130
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_LOCATION_SHARE"

    const/16 v2, 0x8

    const-string v3, "message has location share"

    const/16 v4, 0x38b

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_LOCATION_SHARE:LX/Itv;

    .line 2625131
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_MEDIA_UPLOAD_FAILED"

    const/16 v2, 0x9

    const-string v3, "media upload failed"

    const/16 v4, 0x38d

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_MEDIA_UPLOAD_FAILED:LX/Itv;

    .line 2625132
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_DYNAMIC_PRICING_ENABLED"

    const/16 v2, 0xa

    const-string v3, "dynamic message pricing is enabled"

    const/16 v4, 0x38e

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_DYNAMIC_PRICING_ENABLED:LX/Itv;

    .line 2625133
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_NOT_CONNECTED"

    const/16 v2, 0xb

    const-string v3, "MQTT not connected"

    const/16 v4, 0x38f

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_NOT_CONNECTED:LX/Itv;

    .line 2625134
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_TIMED_OUT_AFTER_PUBLISH"

    const/16 v2, 0xc

    const-string v3, "timed out after publish"

    const/16 v4, 0x390

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    .line 2625135
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE"

    const/16 v2, 0xd

    const-string v3, "timed out waiting for response"

    const/16 v4, 0x391

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    .line 2625136
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_SERVER_RETURNED_FAILURE"

    const/16 v2, 0xe

    const-string v3, "server returned failure"

    const/16 v4, 0x392

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_SERVER_RETURNED_FAILURE:LX/Itv;

    .line 2625137
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_PUBLISH_FAILED"

    const/16 v2, 0xf

    const-string v3, "publish failed"

    const/16 v4, 0x393

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    .line 2625138
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_UNKNOWN_EXCEPTION"

    const/16 v2, 0x10

    const-string v3, "Failed to send via MQTT (%1$s)"

    const/16 v4, 0x394

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_UNKNOWN_EXCEPTION:LX/Itv;

    .line 2625139
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_NO_RETRY"

    const/16 v2, 0x11

    const-string v3, "Failure, no retry"

    const/16 v4, 0x395

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_NO_RETRY:LX/Itv;

    .line 2625140
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_THRIFT_EXCEPTION"

    const/16 v2, 0x12

    const-string v3, "thrift serialize failed"

    const/16 v4, 0x396

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_THRIFT_EXCEPTION:LX/Itv;

    .line 2625141
    new-instance v0, LX/Itv;

    const-string v1, "SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION"

    const/16 v2, 0x13

    const-string v3, "publish failed with exception"

    const/16 v4, 0x397

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    .line 2625142
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_EVENT_MESSAGE"

    const/16 v2, 0x14

    const-string v3, "message has event message"

    const/16 v4, 0x398

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_EVENT_MESSAGE:LX/Itv;

    .line 2625143
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_FLOWER_BORDER"

    const/16 v2, 0x15

    const-string v3, "message has flower border"

    const/16 v4, 0x399

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_FLOWER_BORDER:LX/Itv;

    .line 2625144
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_PLATFORM_METADATA"

    const/16 v2, 0x16

    const-string v3, "message has platform metadata"

    const/16 v4, 0x39a

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_PLATFORM_METADATA:LX/Itv;

    .line 2625145
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_XMA_MESSAGE"

    const/16 v2, 0x17

    const-string v3, "message has XMA attachment"

    const/16 v4, 0x39b

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_XMA_MESSAGE:LX/Itv;

    .line 2625146
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_HAS_MONTAGE_REPLY"

    const/16 v2, 0x18

    const-string v3, "message has montage reply id"

    const/16 v4, 0x39c

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_HAS_MONTAGE_REPLY:LX/Itv;

    .line 2625147
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_LIVE_LOCATION_MESSAGE"

    const/16 v2, 0x19

    const-string v3, "message is a live location"

    const/16 v4, 0x39d

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_LIVE_LOCATION_MESSAGE:LX/Itv;

    .line 2625148
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_JSON_EXCEPTION"

    const/16 v2, 0x1a

    const-string v3, "failed to create JSON object"

    const/16 v4, 0x39e

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_JSON_EXCEPTION:LX/Itv;

    .line 2625149
    new-instance v0, LX/Itv;

    const-string v1, "SEND_SKIPPED_LIGHTWEIGHT_ACTION_MESSAGE"

    const/16 v2, 0x1b

    const-string v3, "message is a lightweight action"

    const/16 v4, 0x39f

    invoke-direct {v0, v1, v2, v3, v4}, LX/Itv;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/Itv;->SEND_SKIPPED_LIGHTWEIGHT_ACTION_MESSAGE:LX/Itv;

    .line 2625150
    const/16 v0, 0x1c

    new-array v0, v0, [LX/Itv;

    sget-object v1, LX/Itv;->NONE:LX/Itv;

    aput-object v1, v0, v5

    sget-object v1, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    aput-object v1, v0, v6

    sget-object v1, LX/Itv;->MQTT_REMOTEEXCEPTION:LX/Itv;

    aput-object v1, v0, v7

    sget-object v1, LX/Itv;->MQTT_IOEXCEPTION:LX/Itv;

    aput-object v1, v0, v8

    sget-object v1, LX/Itv;->SEND_SKIPPED_BROADCAST:LX/Itv;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/Itv;->SEND_SKIPPED_FB_SHARE:LX/Itv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Itv;->SEND_SKIPPED_PAYMENT_MESSAGE:LX/Itv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_APP_ATTRIBUTION:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_LOCATION_SHARE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Itv;->SEND_SKIPPED_MEDIA_UPLOAD_FAILED:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Itv;->SEND_SKIPPED_DYNAMIC_PRICING_ENABLED:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Itv;->SEND_FAILED_NOT_CONNECTED:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Itv;->SEND_FAILED_SERVER_RETURNED_FAILURE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Itv;->SEND_FAILED_UNKNOWN_EXCEPTION:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Itv;->SEND_FAILED_NO_RETRY:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Itv;->SEND_FAILED_THRIFT_EXCEPTION:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_EVENT_MESSAGE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_FLOWER_BORDER:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_PLATFORM_METADATA:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Itv;->SEND_SKIPPED_XMA_MESSAGE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Itv;->SEND_SKIPPED_HAS_MONTAGE_REPLY:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Itv;->SEND_SKIPPED_LIVE_LOCATION_MESSAGE:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/Itv;->SEND_SKIPPED_JSON_EXCEPTION:LX/Itv;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/Itv;->SEND_SKIPPED_LIGHTWEIGHT_ACTION_MESSAGE:LX/Itv;

    aput-object v2, v0, v1

    sput-object v0, LX/Itv;->$VALUES:[LX/Itv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2625118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2625119
    iput-object p3, p0, LX/Itv;->message:Ljava/lang/String;

    .line 2625120
    iput p4, p0, LX/Itv;->errorCode:I

    .line 2625121
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Itv;
    .locals 1

    .prologue
    .line 2625117
    const-class v0, LX/Itv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Itv;

    return-object v0
.end method

.method public static values()[LX/Itv;
    .locals 1

    .prologue
    .line 2625116
    sget-object v0, LX/Itv;->$VALUES:[LX/Itv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Itv;

    return-object v0
.end method
