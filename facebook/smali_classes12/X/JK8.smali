.class public final LX/JK8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5qR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2680239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/5qQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2680240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2680241
    const-class v1, LX/JG6;

    new-instance v2, LX/5qQ;

    const-string v3, "AccessibilityInfo"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680242
    const-class v1, LX/9nO;

    new-instance v2, LX/5qQ;

    const-string v3, "AsyncSQLiteDBStorage"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680243
    const-class v1, LX/Gd3;

    new-instance v2, LX/5qQ;

    const-string v3, "RKAutoUpdater"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680244
    const-class v1, LX/GVm;

    new-instance v2, LX/5qQ;

    const-string v3, "RKBuildInfo"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680245
    const-class v1, LX/Jzf;

    new-instance v2, LX/5qQ;

    const-string v3, "RKCameraRollManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680246
    const-class v1, LX/Jzg;

    new-instance v2, LX/5qQ;

    const-string v3, "Clipboard"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680247
    const-class v1, LX/8xQ;

    new-instance v2, LX/5qQ;

    const-string v3, "CurrentViewer"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680248
    const-class v1, LX/Jzk;

    new-instance v2, LX/5qQ;

    const-string v3, "DatePickerAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680249
    const-class v1, LX/Jzq;

    new-instance v2, LX/5qQ;

    const-string v3, "DialogManagerAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680250
    const-class v1, LX/JJd;

    new-instance v2, LX/5qQ;

    const-string v3, "AppState"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680251
    const-class v1, LX/98w;

    new-instance v2, LX/5qQ;

    const-string v3, "FbMemoryConfig"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680252
    const-class v1, LX/JMT;

    new-instance v2, LX/5qQ;

    const-string v3, "RCTNetworking"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680253
    const-class v1, LX/JMV;

    new-instance v2, LX/5qQ;

    const-string v3, "FBPerformanceLogger"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680254
    const-class v1, LX/98j;

    new-instance v2, LX/5qQ;

    const-string v3, "RKExceptionsManager"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680255
    const-class v1, LX/GdJ;

    new-instance v2, LX/5qQ;

    const-string v3, "RKI18n"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680256
    const-class v1, LX/IBz;

    new-instance v2, LX/5qQ;

    const-string v3, "FBFacebookReactNavigator"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680257
    const-class v1, LX/JMU;

    new-instance v2, LX/5qQ;

    const-string v3, "RelayAPIConfig"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680258
    const-class v1, LX/JMX;

    new-instance v2, LX/5qQ;

    const-string v3, "FbRelayNativeAdapter"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680259
    const-class v1, LX/GVo;

    new-instance v2, LX/5qQ;

    const-string v3, "FBUserAgent"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680260
    const-class v1, LX/Jzs;

    new-instance v2, LX/5qQ;

    const-string v3, "FrescoModule"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680261
    const-class v1, LX/JG7;

    new-instance v2, LX/5qQ;

    const-string v3, "HostStateAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680262
    const-class v1, LX/5qZ;

    new-instance v2, LX/5qQ;

    const-string v3, "I18nManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680263
    const-class v1, LX/Jzu;

    new-instance v2, LX/5qQ;

    const-string v3, "IntentAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680264
    const-class v1, LX/JLQ;

    new-instance v2, LX/5qQ;

    const-string v3, "JSCPerfRecorder"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680265
    const-class v1, LX/Jzz;

    new-instance v2, LX/5qQ;

    const-string v3, "LocationObserver"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680266
    const-class v1, LX/JzR;

    new-instance v2, LX/5qQ;

    const-string v3, "NativeAnimatedModule"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680267
    const-class v1, LX/K02;

    new-instance v2, LX/5qQ;

    const-string v3, "NetInfo"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680268
    const-class v1, LX/K05;

    new-instance v2, LX/5qQ;

    const-string v3, "PermissionsAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680269
    const-class v1, LX/8xe;

    new-instance v2, LX/5qQ;

    const-string v3, "RelayPersistedQueryPreloader"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680270
    const-class v1, LX/JGL;

    new-instance v2, LX/5qQ;

    const-string v3, "Sounds"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680271
    const-class v1, LX/K08;

    new-instance v2, LX/5qQ;

    const-string v3, "StatusBarManager"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680272
    const-class v1, LX/9nQ;

    new-instance v2, LX/5qQ;

    const-string v3, "ToastAndroid"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680273
    const-class v1, LX/K0A;

    new-instance v2, LX/5qQ;

    const-string v3, "WebSocketModule"

    invoke-direct {v2, v3, v5, v5, v5}, LX/5qQ;-><init>(Ljava/lang/String;ZZZ)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2680274
    return-object v0
.end method
