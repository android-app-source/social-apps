.class public LX/ISr;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2578441
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/ISr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2578442
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2578443
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/ISr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2578444
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2578445
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2578446
    const p1, 0x7f030855

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2578447
    const p1, 0x7f0d15bc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/ISr;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2578448
    const p1, 0x7f0d15bd

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbTextView;

    iput-object p1, p0, LX/ISr;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2578449
    const p1, 0x7f0d15be

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, LX/ISr;->c:Landroid/widget/ProgressBar;

    .line 2578450
    return-void
.end method
