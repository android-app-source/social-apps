.class public LX/I5K;
.super LX/0gF;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field public c:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

.field public d:I

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field private g:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 1

    .prologue
    .line 2535251
    invoke-direct {p0, p1}, LX/0gF;-><init>(LX/0gc;)V

    .line 2535252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I5K;->a:Ljava/util/List;

    .line 2535253
    iput-object p2, p0, LX/I5K;->b:Ljava/lang/String;

    .line 2535254
    iput-object p3, p0, LX/I5K;->g:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2535255
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2535256
    iget-boolean v0, p0, LX/I5K;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I5K;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I5K;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2535257
    const/4 v0, -0x2

    return v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2535258
    new-instance v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-direct {v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;-><init>()V

    .line 2535259
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2535260
    const-string v0, "extra_events_discovery_suggestion_token"

    iget-object v3, p0, LX/I5K;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535261
    iget-boolean v0, p0, LX/I5K;->f:Z

    if-nez v0, :cond_0

    .line 2535262
    const-string v3, "extra_events_discovery_filter_time"

    iget-object v0, p0, LX/I5K;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535263
    :cond_0
    const-string v0, "extra_events_discovery_filter_location"

    iget-object v3, p0, LX/I5K;->c:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2535264
    const-string v0, "extra_events_discovery_fragment_selected_category"

    iget-object v3, p0, LX/I5K;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2535265
    const-string v0, "extra_events_discovery_filter_location_range"

    iget v3, p0, LX/I5K;->d:I

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2535266
    const-string v0, "extra_fragment_tag"

    invoke-virtual {p0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535267
    const-string v0, "extra_reaction_analytics_params"

    iget-object v3, p0, LX/I5K;->g:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2535268
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2535269
    return-object v1
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;I)V
    .locals 7

    .prologue
    .line 2535270
    iput-object p1, p0, LX/I5K;->c:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2535271
    iput p2, p0, LX/I5K;->d:I

    .line 2535272
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2535273
    invoke-virtual {p0, v1}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2535274
    if-eqz v0, :cond_0

    .line 2535275
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->v:Z

    .line 2535276
    iput-object p1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->t:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2535277
    iput p2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->u:I

    .line 2535278
    invoke-static {v0}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->Q(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;)V

    .line 2535279
    iget-object v2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->r:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->s:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, p2, v4}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;ILjava/util/List;)LX/0m9;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    .line 2535280
    iget-object v2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->i:LX/I5M;

    const-string v3, "ANDROID_EVENT_DISCOVER_EVENT_LIST"

    iget-object v4, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->j:LX/I5L;

    iget-object v5, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->n:LX/0m9;

    iget-object v6, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->q:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2535281
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2535282
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2535283
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/I5K;->f:Z

    .line 2535284
    iput-object p1, p0, LX/I5K;->a:Ljava/util/List;

    .line 2535285
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2535286
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2535287
    iget-boolean v0, p0, LX/I5K;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/I5K;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
