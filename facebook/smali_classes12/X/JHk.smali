.class public final LX/JHk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;Z)V
    .locals 0

    .prologue
    .line 2677070
    iput-object p1, p0, LX/JHk;->b:Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    iput-boolean p2, p0, LX/JHk;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2677071
    iget-object v1, p0, LX/JHk;->b:Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    iget-boolean v0, p0, LX/JHk;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 p2, 0x1

    .line 2677072
    iget-object v2, v1, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/1qz;->a:LX/0Tn;

    invoke-interface {v2, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 2677073
    if-ne v0, v2, :cond_1

    .line 2677074
    :goto_1
    iget-object v0, p0, LX/JHk;->b:Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2677075
    return-void

    .line 2677076
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2677077
    :cond_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "click"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "button"

    .line 2677078
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->d:Ljava/lang/String;

    .line 2677079
    move-object v2, v2

    .line 2677080
    const-string p1, "divebar_availability_dialog"

    .line 2677081
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 2677082
    move-object v2, v2

    .line 2677083
    iget-object p1, v1, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->o:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2677084
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "chat_bar_online_status_change"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "chat_bar"

    .line 2677085
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2677086
    move-object v2, v2

    .line 2677087
    const-string p1, "state"

    invoke-virtual {v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p1, "source"

    const-string p2, "divebar_availability_dialog"

    invoke-virtual {v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2677088
    iget-object p1, v1, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->o:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2677089
    iget-object v2, v1, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object p1, LX/1qz;->a:LX/0Tn;

    invoke-interface {v2, p1, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_1
.end method
