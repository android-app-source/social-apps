.class public final LX/HWm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477052
    iput-object p1, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2477053
    iget-object v0, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1}, LX/HQB;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    move-result-object v0

    .line 2477054
    if-eqz v0, :cond_0

    .line 2477055
    iget-object v1, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b(Z)V

    .line 2477056
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2477057
    :cond_0
    iget-object v0, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/HWm;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Default tab fragment is supposed to be a wrapper but it\'s null."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
