.class public LX/Ibs;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:LX/IdF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2594860
    const-class v0, LX/Ibs;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ibs;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2594861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2594862
    iput-object p1, p0, LX/Ibs;->b:LX/03V;

    .line 2594863
    iput-object p2, p0, LX/Ibs;->c:LX/0tX;

    .line 2594864
    iput-object p3, p0, LX/Ibs;->d:Ljava/util/concurrent/ExecutorService;

    .line 2594865
    return-void
.end method

.method public static a(LX/0QB;)LX/Ibs;
    .locals 4

    .prologue
    .line 2594866
    new-instance v3, LX/Ibs;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/Ibs;-><init>(LX/03V;LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 2594867
    move-object v0, v3

    .line 2594868
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2594869
    iget-object v0, p0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2594870
    :cond_0
    :goto_0
    return-void

    .line 2594871
    :cond_1
    iget-object v0, p0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method
