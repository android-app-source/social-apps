.class public LX/Icx;
.super LX/6mH;
.source ""


# instance fields
.field public b:LX/Ibv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/maps/FbMapViewDelegate;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

.field public g:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2595977
    invoke-direct {p0, p1}, LX/6mH;-><init>(Landroid/content/Context;)V

    .line 2595978
    const-class v0, LX/Icx;

    invoke-static {v0, p0}, LX/Icx;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2595979
    const v0, 0x7f031221

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2595980
    const v0, 0x7f0d2a6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, LX/Icx;->d:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2595981
    const v0, 0x7f0d2a6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Icx;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2595982
    const v0, 0x7f0d2a6c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

    iput-object v0, p0, LX/Icx;->f:Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

    .line 2595983
    const v0, 0x7f0d2a6d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Icx;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2595984
    iget-object v0, p0, LX/Icx;->d:Lcom/facebook/maps/FbMapViewDelegate;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2595985
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Icx;

    invoke-static {p0}, LX/Ibv;->a(LX/0QB;)LX/Ibv;

    move-result-object v1

    check-cast v1, LX/Ibv;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, LX/Icx;->b:LX/Ibv;

    iput-object p0, p1, LX/Icx;->c:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2595986
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2595987
    iget-object v1, p0, LX/Icx;->d:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, Lcom/facebook/maps/FbMapViewDelegate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2595988
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2595989
    int-to-float v0, v0

    float-to-double v2, v0

    const-wide v4, 0x3ffe666666666666L    # 1.9

    div-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2595990
    iget-object v0, p0, LX/Icx;->d:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbMapViewDelegate;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2595991
    invoke-super {p0, p1, p2}, LX/6mH;->onMeasure(II)V

    .line 2595992
    return-void
.end method
