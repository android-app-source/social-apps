.class public LX/I1i;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final c:Landroid/content/Context;

.field private final d:LX/I21;


# direct methods
.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I21;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529291
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2529292
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I1i;->a:Ljava/util/List;

    .line 2529293
    iput-object p2, p0, LX/I1i;->c:Landroid/content/Context;

    .line 2529294
    iput-object p1, p0, LX/I1i;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529295
    iput-object p3, p0, LX/I1i;->d:LX/I21;

    .line 2529296
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2529285
    new-instance v0, LX/I1h;

    new-instance v1, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/I1i;->c:Landroid/content/Context;

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/I1h;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2529287
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2529288
    iget-object v1, p0, LX/I1i;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2529289
    iget-object v2, p0, LX/I1i;->d:LX/I21;

    iget-object v3, p0, LX/I1i;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v2, v0, v1, v3}, LX/I21;->a(Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2529290
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2529286
    iget-object v0, p0, LX/I1i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
