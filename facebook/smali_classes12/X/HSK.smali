.class public final enum LX/HSK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HSK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HSK;

.field public static final enum NEW:LX/HSK;

.field public static final enum PAST:LX/HSK;

.field public static final enum UPCOMING:LX/HSK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2466842
    new-instance v0, LX/HSK;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v2}, LX/HSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HSK;->NEW:LX/HSK;

    .line 2466843
    new-instance v0, LX/HSK;

    const-string v1, "UPCOMING"

    invoke-direct {v0, v1, v3}, LX/HSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HSK;->UPCOMING:LX/HSK;

    .line 2466844
    new-instance v0, LX/HSK;

    const-string v1, "PAST"

    invoke-direct {v0, v1, v4}, LX/HSK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HSK;->PAST:LX/HSK;

    .line 2466845
    const/4 v0, 0x3

    new-array v0, v0, [LX/HSK;

    sget-object v1, LX/HSK;->NEW:LX/HSK;

    aput-object v1, v0, v2

    sget-object v1, LX/HSK;->UPCOMING:LX/HSK;

    aput-object v1, v0, v3

    sget-object v1, LX/HSK;->PAST:LX/HSK;

    aput-object v1, v0, v4

    sput-object v0, LX/HSK;->$VALUES:[LX/HSK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2466840
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HSK;
    .locals 1

    .prologue
    .line 2466846
    const-class v0, LX/HSK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HSK;

    return-object v0
.end method

.method public static values()[LX/HSK;
    .locals 1

    .prologue
    .line 2466841
    sget-object v0, LX/HSK;->$VALUES:[LX/HSK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HSK;

    return-object v0
.end method
