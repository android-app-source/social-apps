.class public final enum LX/HYj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HYj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HYj;

.field public static final enum ADDITIONAL_EMAIL_ACQUIRED:LX/HYj;

.field public static final enum BIRTHDAY_ACQUIRED:LX/HYj;

.field public static final enum CREATE_ERROR:LX/HYj;

.field public static final enum CREATE_SUCCESS:LX/HYj;

.field public static final enum EMAIL_ACQUIRED:LX/HYj;

.field public static final enum EMAIL_SWITCH_TO_PHONE:LX/HYj;

.field public static final enum ERROR_CONTINUE:LX/HYj;

.field public static final enum GENDER_ACQUIRED:LX/HYj;

.field public static final enum NAME_ACQUIRED:LX/HYj;

.field public static final enum PASSWORD_ACQUIRED:LX/HYj;

.field public static final enum PHONE_ACQUIRED:LX/HYj;

.field public static final enum PHONE_SWITCH_TO_EMAIL:LX/HYj;

.field public static final enum PREFILL_EMAIL_UNFINISHED:LX/HYj;

.field public static final enum START_COMPLETED:LX/HYj;

.field public static final enum TERMS_ACCEPTED:LX/HYj;

.field public static final enum UNKNOWN_ERROR:LX/HYj;

.field public static final enum VALIDATION_ERROR:LX/HYj;

.field public static final enum VALIDATION_START:LX/HYj;

.field public static final enum VALIDATION_SUCCESS:LX/HYj;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2480997
    new-instance v0, LX/HYj;

    const-string v1, "START_COMPLETED"

    invoke-direct {v0, v1, v3}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->START_COMPLETED:LX/HYj;

    .line 2480998
    new-instance v0, LX/HYj;

    const-string v1, "TERMS_ACCEPTED"

    invoke-direct {v0, v1, v4}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    .line 2480999
    new-instance v0, LX/HYj;

    const-string v1, "PHONE_ACQUIRED"

    invoke-direct {v0, v1, v5}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    .line 2481000
    new-instance v0, LX/HYj;

    const-string v1, "PHONE_SWITCH_TO_EMAIL"

    invoke-direct {v0, v1, v6}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->PHONE_SWITCH_TO_EMAIL:LX/HYj;

    .line 2481001
    new-instance v0, LX/HYj;

    const-string v1, "EMAIL_ACQUIRED"

    invoke-direct {v0, v1, v7}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    .line 2481002
    new-instance v0, LX/HYj;

    const-string v1, "EMAIL_SWITCH_TO_PHONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->EMAIL_SWITCH_TO_PHONE:LX/HYj;

    .line 2481003
    new-instance v0, LX/HYj;

    const-string v1, "PREFILL_EMAIL_UNFINISHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->PREFILL_EMAIL_UNFINISHED:LX/HYj;

    .line 2481004
    new-instance v0, LX/HYj;

    const-string v1, "NAME_ACQUIRED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    .line 2481005
    new-instance v0, LX/HYj;

    const-string v1, "BIRTHDAY_ACQUIRED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->BIRTHDAY_ACQUIRED:LX/HYj;

    .line 2481006
    new-instance v0, LX/HYj;

    const-string v1, "GENDER_ACQUIRED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->GENDER_ACQUIRED:LX/HYj;

    .line 2481007
    new-instance v0, LX/HYj;

    const-string v1, "PASSWORD_ACQUIRED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    .line 2481008
    new-instance v0, LX/HYj;

    const-string v1, "CREATE_ERROR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->CREATE_ERROR:LX/HYj;

    .line 2481009
    new-instance v0, LX/HYj;

    const-string v1, "VALIDATION_START"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->VALIDATION_START:LX/HYj;

    .line 2481010
    new-instance v0, LX/HYj;

    const-string v1, "VALIDATION_SUCCESS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    .line 2481011
    new-instance v0, LX/HYj;

    const-string v1, "VALIDATION_ERROR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->VALIDATION_ERROR:LX/HYj;

    .line 2481012
    new-instance v0, LX/HYj;

    const-string v1, "CREATE_SUCCESS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->CREATE_SUCCESS:LX/HYj;

    .line 2481013
    new-instance v0, LX/HYj;

    const-string v1, "ERROR_CONTINUE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->ERROR_CONTINUE:LX/HYj;

    .line 2481014
    new-instance v0, LX/HYj;

    const-string v1, "UNKNOWN_ERROR"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->UNKNOWN_ERROR:LX/HYj;

    .line 2481015
    new-instance v0, LX/HYj;

    const-string v1, "ADDITIONAL_EMAIL_ACQUIRED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/HYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYj;->ADDITIONAL_EMAIL_ACQUIRED:LX/HYj;

    .line 2481016
    const/16 v0, 0x13

    new-array v0, v0, [LX/HYj;

    sget-object v1, LX/HYj;->START_COMPLETED:LX/HYj;

    aput-object v1, v0, v3

    sget-object v1, LX/HYj;->TERMS_ACCEPTED:LX/HYj;

    aput-object v1, v0, v4

    sget-object v1, LX/HYj;->PHONE_ACQUIRED:LX/HYj;

    aput-object v1, v0, v5

    sget-object v1, LX/HYj;->PHONE_SWITCH_TO_EMAIL:LX/HYj;

    aput-object v1, v0, v6

    sget-object v1, LX/HYj;->EMAIL_ACQUIRED:LX/HYj;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/HYj;->EMAIL_SWITCH_TO_PHONE:LX/HYj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HYj;->PREFILL_EMAIL_UNFINISHED:LX/HYj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HYj;->NAME_ACQUIRED:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HYj;->BIRTHDAY_ACQUIRED:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HYj;->GENDER_ACQUIRED:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HYj;->PASSWORD_ACQUIRED:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HYj;->CREATE_ERROR:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HYj;->VALIDATION_START:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/HYj;->VALIDATION_ERROR:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/HYj;->CREATE_SUCCESS:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/HYj;->ERROR_CONTINUE:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/HYj;->UNKNOWN_ERROR:LX/HYj;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/HYj;->ADDITIONAL_EMAIL_ACQUIRED:LX/HYj;

    aput-object v2, v0, v1

    sput-object v0, LX/HYj;->$VALUES:[LX/HYj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2480996
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HYj;
    .locals 1

    .prologue
    .line 2480995
    const-class v0, LX/HYj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HYj;

    return-object v0
.end method

.method public static valueOfKey(Ljava/lang/String;)LX/HYj;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2480989
    if-eqz p0, :cond_0

    const-string v1, "com.facebook.registration."

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2480990
    :cond_0
    :goto_0
    return-object v0

    .line 2480991
    :cond_1
    const/16 v1, 0x1a

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/HYj;->valueOf(Ljava/lang/String;)LX/HYj;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2480992
    :catch_0
    goto :goto_0
.end method

.method public static values()[LX/HYj;
    .locals 1

    .prologue
    .line 2480994
    sget-object v0, LX/HYj;->$VALUES:[LX/HYj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HYj;

    return-object v0
.end method


# virtual methods
.method public final getKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2480993
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.facebook.registration."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/HYj;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
