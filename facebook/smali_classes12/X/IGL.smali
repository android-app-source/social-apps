.class public final LX/IGL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2555897
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2555898
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2555899
    :goto_0
    return v1

    .line 2555900
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2555901
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2555902
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2555903
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2555904
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2555905
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2555906
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2555907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2555908
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2555909
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2555910
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_b

    .line 2555911
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2555912
    :goto_3
    move v2, v3

    .line 2555913
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2555914
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2555915
    goto :goto_1

    .line 2555916
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2555917
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2555918
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2555919
    :cond_5
    const-string v9, "remaining_duration"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2555920
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v5, v2

    move v2, v4

    .line 2555921
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2555922
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2555923
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2555924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_6

    if-eqz v8, :cond_6

    .line 2555925
    const-string v9, "message"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2555926
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 2555927
    :cond_7
    const-string v9, "ping_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2555928
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLLocationPingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLocationPingType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_4

    .line 2555929
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2555930
    :cond_9
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2555931
    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 2555932
    invoke-virtual {p1, v4, v6}, LX/186;->b(II)V

    .line 2555933
    if-eqz v2, :cond_a

    .line 2555934
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5, v3}, LX/186;->a(III)V

    .line 2555935
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_b
    move v2, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2555936
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2555937
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2555938
    if-eqz v0, :cond_4

    .line 2555939
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555940
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2555941
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2555942
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x1

    const/4 p1, 0x0

    .line 2555943
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2555944
    invoke-virtual {p0, v2, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2555945
    if-eqz v3, :cond_0

    .line 2555946
    const-string v4, "message"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555947
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2555948
    :cond_0
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 2555949
    if-eqz v3, :cond_1

    .line 2555950
    const-string v3, "ping_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555951
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2555952
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, p1}, LX/15i;->a(III)I

    move-result v3

    .line 2555953
    if-eqz v3, :cond_2

    .line 2555954
    const-string v4, "remaining_duration"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555955
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 2555956
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2555957
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2555958
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2555959
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2555960
    return-void
.end method
