.class public final enum LX/IWB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IWB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IWB;

.field public static final enum ALBUM_ROW:LX/IWB;

.field public static final enum CREATE_ALBUM_ROW:LX/IWB;

.field public static final enum LOADING_BAR:LX/IWB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2583479
    new-instance v0, LX/IWB;

    const-string v1, "ALBUM_ROW"

    invoke-direct {v0, v1, v2}, LX/IWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IWB;->ALBUM_ROW:LX/IWB;

    .line 2583480
    new-instance v0, LX/IWB;

    const-string v1, "LOADING_BAR"

    invoke-direct {v0, v1, v3}, LX/IWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IWB;->LOADING_BAR:LX/IWB;

    .line 2583481
    new-instance v0, LX/IWB;

    const-string v1, "CREATE_ALBUM_ROW"

    invoke-direct {v0, v1, v4}, LX/IWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IWB;->CREATE_ALBUM_ROW:LX/IWB;

    .line 2583482
    const/4 v0, 0x3

    new-array v0, v0, [LX/IWB;

    sget-object v1, LX/IWB;->ALBUM_ROW:LX/IWB;

    aput-object v1, v0, v2

    sget-object v1, LX/IWB;->LOADING_BAR:LX/IWB;

    aput-object v1, v0, v3

    sget-object v1, LX/IWB;->CREATE_ALBUM_ROW:LX/IWB;

    aput-object v1, v0, v4

    sput-object v0, LX/IWB;->$VALUES:[LX/IWB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2583483
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IWB;
    .locals 1

    .prologue
    .line 2583484
    const-class v0, LX/IWB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IWB;

    return-object v0
.end method

.method public static values()[LX/IWB;
    .locals 1

    .prologue
    .line 2583485
    sget-object v0, LX/IWB;->$VALUES:[LX/IWB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IWB;

    return-object v0
.end method
