.class public LX/IsJ;
.super LX/Iqk;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public final b:LX/1zC;

.field public final c:Lcom/facebook/messaging/photos/editing/LayerEditText;

.field private final d:LX/0wd;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DeK;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final g:LX/IsC;

.field private final h:Landroid/text/TextWatcher;

.field private final i:Landroid/widget/FrameLayout;

.field public j:LX/Irm;

.field public k:Z

.field private l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2619602
    new-instance v0, LX/0wT;

    const-wide v2, 0x4062c00000000000L    # 150.0

    const-wide/high16 v4, 0x402e000000000000L    # 15.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    sput-object v0, LX/IsJ;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/1zC;Lcom/facebook/messaging/photos/editing/LayerEditText;LX/0Ot;LX/0TD;LX/0wW;LX/IsC;)V
    .locals 4
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1zC;",
            "Lcom/facebook/messaging/photos/editing/LayerEditText;",
            "LX/0Ot",
            "<",
            "LX/DeK;",
            ">;",
            "LX/0TD;",
            "LX/0wW;",
            "LX/IsC;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 2619577
    invoke-direct {p0, p6, p2, p5}, LX/Iqk;-><init>(LX/Iqg;Landroid/view/View;LX/0wW;)V

    .line 2619578
    new-instance v0, LX/IsD;

    invoke-direct {v0, p0}, LX/IsD;-><init>(LX/IsJ;)V

    iput-object v0, p0, LX/IsJ;->h:Landroid/text/TextWatcher;

    .line 2619579
    iput-object v1, p0, LX/IsJ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2619580
    iput-object p1, p0, LX/IsJ;->b:LX/1zC;

    .line 2619581
    iput-object p2, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    .line 2619582
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setImeOptions(I)V

    .line 2619583
    iput-object p3, p0, LX/IsJ;->e:LX/0Ot;

    .line 2619584
    iput-object p6, p0, LX/IsJ;->g:LX/IsC;

    .line 2619585
    iput-object p4, p0, LX/IsJ;->f:LX/0TD;

    .line 2619586
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619587
    iget-boolean v2, v0, LX/Iqg;->k:Z

    move v0, v2

    .line 2619588
    if-eqz v0, :cond_0

    .line 2619589
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setVisibility(I)V

    .line 2619590
    :cond_0
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619591
    iget-boolean v2, v0, LX/IsC;->e:Z

    move v0, v2

    .line 2619592
    if-eqz v0, :cond_1

    invoke-virtual {p5}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v2, LX/IsJ;->a:LX/0wT;

    invoke-virtual {v0, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v2, LX/IsI;

    invoke-direct {v2, p0}, LX/IsI;-><init>(LX/IsJ;)V

    invoke-virtual {v0, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/IsJ;->d:LX/0wd;

    .line 2619593
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619594
    iget-boolean v2, v0, LX/IsC;->e:Z

    move v0, v2

    .line 2619595
    if-eqz v0, :cond_2

    .line 2619596
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    .line 2619597
    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2619598
    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2619599
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 2619600
    goto :goto_0

    .line 2619601
    :cond_2
    iput-object v1, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    goto :goto_1
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2619538
    iget-boolean v0, p0, LX/IsJ;->k:Z

    if-ne v0, p1, :cond_0

    if-nez p2, :cond_0

    .line 2619539
    :goto_0
    return-void

    .line 2619540
    :cond_0
    iput-boolean p1, p0, LX/IsJ;->k:Z

    .line 2619541
    iget-object v0, p0, LX/IsJ;->d:LX/0wd;

    if-eqz v0, :cond_1

    .line 2619542
    iget-object v3, p0, LX/IsJ;->d:LX/0wd;

    if-eqz p1, :cond_6

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_1
    invoke-virtual {v3, v0, v1}, LX/0wd;->b(D)LX/0wd;

    .line 2619543
    :cond_1
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2619544
    if-nez p1, :cond_2

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619545
    iget-boolean v3, v1, LX/IsC;->e:Z

    move v1, v3

    .line 2619546
    if-eqz v1, :cond_2

    .line 2619547
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2619548
    :cond_2
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTextIsSelectable(Z)V

    .line 2619549
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setFocusable(Z)V

    .line 2619550
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setFocusableInTouchMode(Z)V

    .line 2619551
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setEnabled(Z)V

    .line 2619552
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setClickable(Z)V

    .line 2619553
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, p1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setLongClickable(Z)V

    .line 2619554
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2619555
    if-eqz p1, :cond_7

    .line 2619556
    iget-object v3, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v3}, Lcom/facebook/messaging/photos/editing/LayerEditText;->requestFocus()Z

    .line 2619557
    iget-object v3, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v4, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v4}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setSelection(I)V

    .line 2619558
    iget-object v3, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2619559
    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2619560
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->bringToFront()V

    .line 2619561
    invoke-virtual {v1}, Landroid/view/ViewGroup;->invalidate()V

    .line 2619562
    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    .line 2619563
    :cond_3
    :goto_2
    iget-object v0, p0, LX/IsJ;->j:LX/Irm;

    if-eqz v0, :cond_5

    .line 2619564
    iget-object v0, p0, LX/IsJ;->j:LX/Irm;

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619565
    if-nez p1, :cond_4

    invoke-virtual {v1}, LX/Iqg;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2619566
    iget-object v3, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v3, v1}, LX/Ird;->c(LX/Iqg;)V

    .line 2619567
    :cond_4
    iget-object v3, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v3, :cond_5

    iget-object v3, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-boolean v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->y:Z

    if-nez v3, :cond_5

    .line 2619568
    iget-object v3, v0, LX/Irm;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v3, v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    invoke-virtual {v3, p1}, LX/IrL;->b(Z)V

    .line 2619569
    :cond_5
    if-nez p1, :cond_8

    const/4 v0, 0x1

    .line 2619570
    :goto_3
    iget-boolean v1, p0, LX/Iqk;->g:Z

    if-ne v1, v0, :cond_9

    .line 2619571
    :goto_4
    goto/16 :goto_0

    .line 2619572
    :cond_6
    const-wide/16 v0, 0x0

    goto/16 :goto_1

    .line 2619573
    :cond_7
    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2619574
    iget-object v0, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LX/IsJ;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_2

    :cond_8
    move v0, v2

    .line 2619575
    goto :goto_3

    .line 2619576
    :cond_9
    iput-boolean v0, p0, LX/Iqk;->g:Z

    goto :goto_4
.end method

.method private t()F
    .locals 2

    .prologue
    .line 2619537
    iget-object v0, p0, LX/IsJ;->d:LX/0wd;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IsJ;->d:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2619489
    invoke-super {p0}, LX/Iqk;->a()V

    .line 2619490
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619491
    iget-object v1, v0, LX/IsC;->g:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 2619492
    if-eqz v0, :cond_0

    .line 2619493
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2619494
    iget-object v2, p0, LX/IsJ;->b:LX/1zC;

    invoke-virtual {v2, v1}, LX/1zC;->a(Landroid/text/Editable;)Z

    .line 2619495
    iget-object v2, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2619496
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v2, p0, LX/IsJ;->g:LX/IsC;

    .line 2619497
    iget v3, v2, LX/IsC;->h:I

    move v2, v3

    .line 2619498
    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTextColor(I)V

    .line 2619499
    :cond_0
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v2, p0, LX/IsJ;->g:LX/IsC;

    .line 2619500
    iget v3, v2, LX/Iqg;->g:F

    move v2, v3

    .line 2619501
    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setAlpha(F)V

    .line 2619502
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v2, p0, LX/IsJ;->g:LX/IsC;

    .line 2619503
    iget v3, v2, LX/Iqg;->e:F

    move v2, v3

    .line 2619504
    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setRotation(F)V

    .line 2619505
    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619506
    iget-object v2, v1, LX/IsC;->f:LX/Dhi;

    move-object v1, v2

    .line 2619507
    sget-object v2, LX/Dhi;->USER_PROMPT:LX/Dhi;

    if-ne v1, v2, :cond_1

    .line 2619508
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2619509
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619510
    iget v2, v1, LX/IsC;->h:I

    move v1, v2

    .line 2619511
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setHintTextColor(I)V

    .line 2619512
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2619513
    :cond_1
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619514
    iget-object v1, v0, LX/IsC;->b:Lcom/facebook/messaging/font/FontAsset;

    move-object v0, v1

    .line 2619515
    if-eqz v0, :cond_2

    .line 2619516
    iget-object v0, p0, LX/IsJ;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DeK;

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619517
    iget-object v2, v1, LX/IsC;->b:Lcom/facebook/messaging/font/FontAsset;

    move-object v1, v2

    .line 2619518
    new-instance v3, LX/DeE;

    iget-object v2, v1, Lcom/facebook/messaging/font/FontAsset;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/facebook/messaging/font/FontAsset;->b:Ljava/lang/String;

    invoke-direct {v3, v2, v4}, LX/DeE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2619519
    iget-object v2, v0, LX/DeK;->d:Landroid/util/LruCache;

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Typeface;

    .line 2619520
    if-eqz v2, :cond_5

    .line 2619521
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2619522
    :goto_0
    move-object v0, v2

    .line 2619523
    iput-object v0, p0, LX/IsJ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2619524
    iget-object v0, p0, LX/IsJ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/IsE;

    invoke-direct {v1, p0}, LX/IsE;-><init>(LX/IsJ;)V

    iget-object v2, p0, LX/IsJ;->f:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2619525
    :cond_2
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619526
    iget-boolean v1, v0, LX/IsC;->e:Z

    move v0, v1

    .line 2619527
    if-eqz v0, :cond_3

    .line 2619528
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    new-instance v1, LX/IsF;

    invoke-direct {v1, p0}, LX/IsF;-><init>(LX/IsJ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2619529
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    new-instance v1, LX/IsG;

    invoke-direct {v1, p0}, LX/IsG;-><init>(LX/IsJ;)V

    .line 2619530
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/LayerEditText;->a:LX/Iqp;

    .line 2619531
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v1, p0, LX/IsJ;->h:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2619532
    :cond_3
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619533
    iget-boolean v1, v0, LX/Iqg;->m:Z

    move v0, v1

    .line 2619534
    if-nez v0, :cond_4

    .line 2619535
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Iqg;->c(Z)V

    .line 2619536
    :cond_4
    return-void

    :cond_5
    iget-object v2, v1, Lcom/facebook/messaging/font/FontAsset;->c:Ljava/lang/String;

    invoke-static {v0, v3, v2}, LX/DeK;->a(LX/DeK;LX/DeE;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v4, LX/DeJ;

    invoke-direct {v4, v0, v3}, LX/DeJ;-><init>(LX/DeK;LX/DeE;)V

    iget-object v3, v0, LX/DeK;->a:LX/0TD;

    invoke-static {v2, v4, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2619458
    invoke-super {p0, p1}, LX/Iqk;->a(Ljava/lang/Object;)V

    .line 2619459
    instance-of v0, p1, LX/Iqn;

    if-eqz v0, :cond_2

    .line 2619460
    sget-object v0, LX/IsH;->a:[I

    check-cast p1, LX/Iqn;

    invoke-virtual {p1}, LX/Iqn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2619461
    :cond_0
    :goto_0
    return-void

    .line 2619462
    :pswitch_0
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619463
    iget-object v1, v0, LX/IsC;->a:LX/Dhh;

    move-object v0, v1

    .line 2619464
    sget-object v1, LX/Dhh;->DOMINANT_COLOR_OF_STICKER:LX/Dhh;

    if-ne v0, v1, :cond_0

    .line 2619465
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    const v1, 0x7f021450

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setBackgroundResource(I)V

    .line 2619466
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2619467
    instance-of v1, v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v1, :cond_0

    .line 2619468
    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619469
    iget v2, v1, LX/Iqg;->n:I

    move v1, v2

    .line 2619470
    const/16 v2, 0x80

    invoke-static {v1, v2}, LX/3qk;->b(II)I

    move-result v1

    .line 2619471
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0

    .line 2619472
    :pswitch_1
    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619473
    iget-boolean v2, v0, LX/Iqg;->l:Z

    move v0, v2

    .line 2619474
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 2619475
    :cond_2
    instance-of v0, p1, LX/IsB;

    if-eqz v0, :cond_0

    .line 2619476
    sget-object v0, LX/IsH;->b:[I

    check-cast p1, LX/IsB;

    invoke-virtual {p1}, LX/IsB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2619477
    :pswitch_2
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619478
    iget-object v1, v0, LX/IsC;->g:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 2619479
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2619480
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619481
    iget-object v2, v1, LX/IsC;->g:Ljava/lang/CharSequence;

    move-object v1, v2

    .line 2619482
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2619483
    :pswitch_3
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619484
    iget v2, v1, LX/IsC;->h:I

    move v1, v2

    .line 2619485
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTextColor(I)V

    goto :goto_0

    .line 2619486
    :pswitch_4
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/IsJ;->g:LX/IsC;

    .line 2619487
    iget p0, v2, LX/IsC;->i:I

    move v2, p0

    .line 2619488
    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 2619456
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IsJ;->a(ZZ)V

    .line 2619457
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2619603
    invoke-super {p0}, LX/Iqk;->e()V

    .line 2619604
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2619605
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    .line 2619606
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/LayerEditText;->a:LX/Iqp;

    .line 2619607
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2619608
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    iget-object v1, p0, LX/IsJ;->h:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2619609
    iget-object v0, p0, LX/IsJ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2619610
    iget-object v0, p0, LX/IsJ;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2619611
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 2619447
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619448
    iget-boolean v1, v0, LX/IsC;->e:Z

    move v0, v1

    .line 2619449
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619450
    iget-boolean v1, v0, LX/Iqg;->k:Z

    move v0, v1

    .line 2619451
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, LX/IsJ;->g:LX/IsC;

    .line 2619452
    iget-boolean v2, v1, LX/Iqg;->k:Z

    move v1, v2

    .line 2619453
    invoke-direct {p0, v0, v1}, LX/IsJ;->a(ZZ)V

    .line 2619454
    return-void

    .line 2619455
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2619444
    invoke-super {p0}, LX/Iqk;->g()V

    .line 2619445
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IsJ;->d(Z)V

    .line 2619446
    return-void
.end method

.method public final k()F
    .locals 3

    .prologue
    .line 2619443
    invoke-super {p0}, LX/Iqk;->k()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0}, LX/IsJ;->t()F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0
.end method

.method public final l()F
    .locals 3

    .prologue
    .line 2619437
    iget-object v0, p0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2619438
    if-nez v0, :cond_0

    .line 2619439
    invoke-super {p0}, LX/Iqk;->l()F

    move-result v0

    .line 2619440
    :goto_0
    return v0

    .line 2619441
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 2619442
    invoke-super {p0}, LX/Iqk;->l()F

    move-result v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    invoke-direct {p0}, LX/IsJ;->t()F

    move-result v2

    invoke-static {v1, v0, v2}, LX/Iqu;->a(FFF)F

    move-result v0

    goto :goto_0
.end method

.method public final m()F
    .locals 4

    .prologue
    .line 2619431
    invoke-super {p0}, LX/Iqk;->m()F

    move-result v0

    .line 2619432
    const/high16 v3, 0x43b40000    # 360.0f

    const/high16 v2, 0x43340000    # 180.0f

    .line 2619433
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 2619434
    sub-float v1, v0, v2

    div-float/2addr v1, v3

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0x168

    int-to-float v1, v1

    .line 2619435
    :goto_0
    move v1, v1

    .line 2619436
    invoke-direct {p0}, LX/IsJ;->t()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/Iqu;->a(FFF)F

    move-result v0

    return v0

    :cond_0
    add-float v1, v0, v2

    div-float/2addr v1, v3

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0x168

    int-to-float v1, v1

    goto :goto_0
.end method

.method public final n()F
    .locals 3

    .prologue
    .line 2619430
    invoke-super {p0}, LX/Iqk;->n()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0}, LX/IsJ;->t()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/Iqu;->a(FFF)F

    move-result v0

    return v0
.end method

.method public final o()F
    .locals 3

    .prologue
    .line 2619429
    invoke-super {p0}, LX/Iqk;->o()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0}, LX/IsJ;->t()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/Iqu;->a(FFF)F

    move-result v0

    return v0
.end method

.method public onClick()V
    .locals 2

    .prologue
    .line 2619424
    iget-object v0, p0, LX/IsJ;->g:LX/IsC;

    .line 2619425
    iget-boolean v1, v0, LX/IsC;->e:Z

    move v0, v1

    .line 2619426
    if-eqz v0, :cond_0

    .line 2619427
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/IsJ;->d(Z)V

    .line 2619428
    :cond_0
    return-void
.end method
