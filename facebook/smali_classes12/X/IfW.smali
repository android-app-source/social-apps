.class public LX/IfW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/1mR;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/3fv;

.field public final f:LX/3fw;

.field public g:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2599781
    const-class v0, LX/IfW;

    sput-object v0, LX/IfW;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/1mR;Ljava/util/concurrent/Executor;LX/3fv;LX/3fw;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599783
    iput-object p1, p0, LX/IfW;->b:LX/0tX;

    .line 2599784
    iput-object p2, p0, LX/IfW;->c:LX/1mR;

    .line 2599785
    iput-object p3, p0, LX/IfW;->d:Ljava/util/concurrent/Executor;

    .line 2599786
    iput-object p4, p0, LX/IfW;->e:LX/3fv;

    .line 2599787
    iput-object p5, p0, LX/IfW;->f:LX/3fw;

    .line 2599788
    return-void
.end method

.method public static a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)LX/0jT;
    .locals 8

    .prologue
    .line 2599789
    new-instance v0, LX/Ddm;

    invoke-direct {v0}, LX/Ddm;-><init>()V

    iget-object v1, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599790
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2599791
    iput-object v1, v0, LX/Ddm;->a:Ljava/lang/String;

    .line 2599792
    move-object v0, v0

    .line 2599793
    const/4 v1, 0x1

    .line 2599794
    iput-boolean v1, v0, LX/Ddm;->b:Z

    .line 2599795
    move-object v0, v0

    .line 2599796
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 2599797
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2599798
    iget-object v3, v0, LX/Ddm;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2599799
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 2599800
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 2599801
    iget-boolean v3, v0, LX/Ddm;->b:Z

    invoke-virtual {v2, v6, v3}, LX/186;->a(IZ)V

    .line 2599802
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2599803
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2599804
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2599805
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2599806
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2599807
    new-instance v3, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel$SuggestionModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel$SuggestionModel;-><init>(LX/15i;)V

    .line 2599808
    move-object v0, v3

    .line 2599809
    new-instance v1, LX/CLG;

    invoke-direct {v1}, LX/CLG;-><init>()V

    .line 2599810
    iput-object v0, v1, LX/CLG;->a:Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel$SuggestionModel;

    .line 2599811
    move-object v0, v1

    .line 2599812
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2599813
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2599814
    iget-object v3, v0, LX/CLG;->a:Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel$SuggestionModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2599815
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 2599816
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 2599817
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2599818
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2599819
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2599820
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2599821
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2599822
    new-instance v3, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel;

    invoke-direct {v3, v2}, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$HideCYMKSuggestionModel;-><init>(LX/15i;)V

    .line 2599823
    move-object v0, v3

    .line 2599824
    return-object v0
.end method
