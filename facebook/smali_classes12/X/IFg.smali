.class public LX/IFg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/IFg;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:LX/0SG;

.field private final c:LX/0yD;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;LX/0yD;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554738
    iput-object p1, p0, LX/IFg;->b:LX/0SG;

    .line 2554739
    const v0, 0x7f0b23da

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IFg;->a:Ljava/lang/String;

    .line 2554740
    iput-object p3, p0, LX/IFg;->c:LX/0yD;

    .line 2554741
    iput-object p4, p0, LX/IFg;->d:LX/0ad;

    .line 2554742
    return-void
.end method

.method public static a(LX/IFg;ZLjava/lang/String;LX/0am;)LX/0zO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;)",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554727
    new-instance v0, LX/IGm;

    invoke-direct {v0}, LX/IGm;-><init>()V

    move-object v2, v0

    .line 2554728
    const-string v0, "pic_size"

    iget-object v1, p0, LX/IFg;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v3, "is_preview"

    if-eqz p1, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "set_items_fetch_count"

    const-string v3, "3"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "sections_cursor"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554729
    invoke-virtual {p3}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554730
    invoke-virtual {p3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 2554731
    const-string v1, "latitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554732
    const-string v1, "longitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554733
    const-string v3, "accuracy_meters"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554734
    const-string v1, "stale_time_seconds"

    iget-object v3, p0, LX/IFg;->c:LX/0yD;

    invoke-virtual {v3, v0}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554735
    :cond_0
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    .line 2554736
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/IFg;
    .locals 7

    .prologue
    .line 2554714
    sget-object v0, LX/IFg;->e:LX/IFg;

    if-nez v0, :cond_1

    .line 2554715
    const-class v1, LX/IFg;

    monitor-enter v1

    .line 2554716
    :try_start_0
    sget-object v0, LX/IFg;->e:LX/IFg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2554717
    if-eqz v2, :cond_0

    .line 2554718
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2554719
    new-instance p0, LX/IFg;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v5

    check-cast v5, LX/0yD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/IFg;-><init>(LX/0SG;Landroid/content/res/Resources;LX/0yD;LX/0ad;)V

    .line 2554720
    move-object v0, p0

    .line 2554721
    sput-object v0, LX/IFg;->e:LX/IFg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2554722
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2554723
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2554724
    :cond_1
    sget-object v0, LX/IFg;->e:LX/IFg;

    return-object v0

    .line 2554725
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2554726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0yG;LX/0am;)LX/0zO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0yG;",
            "LX/0am",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;)",
            "LX/0zO",
            "<",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554704
    new-instance v0, LX/IGp;

    invoke-direct {v0}, LX/IGp;-><init>()V

    move-object v2, v0

    .line 2554705
    const-string v1, "get_friends_sharing_when_enabled"

    sget-object v0, LX/0yG;->OKAY:LX/0yG;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "pic_size"

    iget-object v3, p0, LX/IFg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "max_friends_sharing_faces"

    const-string v3, "16"

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "image_size"

    const/16 v3, 0x40

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2554706
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554707
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    .line 2554708
    const-string v1, "latitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554709
    const-string v1, "longitude"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554710
    const-string v3, "accuracy_meters"

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554711
    const-string v1, "stale_time_seconds"

    iget-object v3, p0, LX/IFg;->c:LX/0yD;

    invoke-virtual {v3, v0}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2554712
    :cond_0
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    .line 2554713
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
