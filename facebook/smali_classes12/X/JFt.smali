.class public LX/JFt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/location/places/PlaceRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 11

    const/4 v8, 0x0

    invoke-static {p1}, LX/2xb;->b(Landroid/os/Parcel;)I

    move-result v9

    const/4 v2, 0x0

    const-wide/16 v3, -0x1

    const/4 v5, -0x1

    const-wide v6, 0x7fffffffffffffffL

    move v1, v8

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v9, :cond_0

    invoke-static {p1}, LX/2xb;->a(Landroid/os/Parcel;)I

    move-result v0

    invoke-static {v0}, LX/2xb;->a(I)I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    invoke-static {p1, v0}, LX/2xb;->a(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    sget-object v2, Lcom/google/android/gms/location/places/PlaceFilter;->CREATOR:LX/JFq;

    invoke-static {p1, v0, v2}, LX/2xb;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceFilter;

    move-object v2, v0

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v0}, LX/2xb;->g(Landroid/os/Parcel;I)J

    move-result-wide v3

    goto :goto_0

    :sswitch_2
    invoke-static {p1, v0}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v5

    goto :goto_0

    :sswitch_3
    invoke-static {p1, v0}, LX/2xb;->g(Landroid/os/Parcel;I)J

    move-result-wide v6

    goto :goto_0

    :sswitch_4
    invoke-static {p1, v0}, LX/2xb;->b(Landroid/os/Parcel;I)Z

    move-result v8

    goto :goto_0

    :sswitch_5
    invoke-static {p1, v0}, LX/2xb;->e(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v9, :cond_1

    new-instance v0, LX/4sr;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Overread allowed size end="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/4sr;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/location/places/PlaceRequest;-><init>(ILcom/google/android/gms/location/places/PlaceFilter;JIJZ)V

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/location/places/PlaceRequest;

    return-object v0
.end method
