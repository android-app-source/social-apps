.class public final LX/Ih2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 0

    .prologue
    .line 2601935
    iput-object p1, p0, LX/Ih2;->a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2601912
    iget-object v0, p0, LX/Ih2;->a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    invoke-static {v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2601913
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2601914
    check-cast p1, LX/0Px;

    .line 2601915
    iget-object v0, p0, LX/Ih2;->a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    .line 2601916
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2601917
    :goto_0
    return-void

    .line 2601918
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->G:LX/Ika;

    if-eqz v1, :cond_2

    .line 2601919
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->G:LX/Ika;

    .line 2601920
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2601921
    iget-object v2, v1, LX/Ika;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    iget-object v3, v2, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601922
    new-instance v4, LX/5zn;

    invoke-direct {v4}, LX/5zn;-><init>()V

    invoke-virtual {v4, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v4

    sget-object p1, LX/2MK;->ENT_PHOTO:LX/2MK;

    .line 2601923
    iput-object p1, v4, LX/5zn;->c:LX/2MK;

    .line 2601924
    move-object v4, v4

    .line 2601925
    invoke-virtual {v4}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v4

    move-object v2, v4

    .line 2601926
    iput-object v2, v3, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601927
    iget-object p0, v3, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    if-eqz v2, :cond_3

    iget-object v4, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    :goto_1
    sget-object p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v2, 0x0

    .line 2601928
    iget-object v3, p0, LX/Ikl;->i:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2601929
    iget-object v3, p0, LX/Ikl;->j:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2601930
    iget-object v3, p0, LX/Ikl;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2601931
    iget-object v3, p0, LX/Ikl;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v4, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2601932
    :cond_1
    iget-object v2, v1, LX/Ika;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->invalidateOptionsMenu()V

    .line 2601933
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0

    .line 2601934
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method
