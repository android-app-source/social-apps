.class public final LX/IYK;
.super LX/5OM;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/instantarticles/view/ShareBar;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/view/ShareBar;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2587495
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IYK;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Landroid/content/Context;B)V

    .line 2587496
    return-void
.end method

.method private constructor <init>(Lcom/facebook/instantarticles/view/ShareBar;Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 2587492
    iput-object p1, p0, LX/IYK;->l:Lcom/facebook/instantarticles/view/ShareBar;

    .line 2587493
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, LX/5OM;-><init>(Landroid/content/Context;I)V

    .line 2587494
    return-void
.end method


# virtual methods
.method public final b()LX/5OK;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2587482
    invoke-super {p0}, LX/5OM;->b()LX/5OK;

    move-result-object v1

    .line 2587483
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v2, v0

    const-wide v4, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v2, v4

    iget-object v0, p0, LX/IYK;->l:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b01d7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v4, v0

    div-double/2addr v2, v4

    double-to-int v0, v2

    .line 2587484
    int-to-float v0, v0

    invoke-virtual {v1, v0}, LX/5OK;->setMaxRows(F)V

    .line 2587485
    invoke-virtual {v1, v7}, LX/5OK;->setVerticalScrollBarEnabled(Z)V

    .line 2587486
    invoke-virtual {v1, v6}, LX/5OK;->setScrollbarFadingEnabled(Z)V

    .line 2587487
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587488
    iget-object v0, p0, LX/IYK;->l:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v0, v0, Lcom/facebook/instantarticles/view/ShareBar;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2587489
    invoke-virtual {v1, v7}, LX/5OK;->setLayoutDirection(I)V

    .line 2587490
    :cond_0
    :goto_0
    return-object v1

    .line 2587491
    :cond_1
    invoke-virtual {v1, v6}, LX/5OK;->setLayoutDirection(I)V

    goto :goto_0
.end method
