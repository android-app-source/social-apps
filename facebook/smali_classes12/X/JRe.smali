.class public final LX/JRe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3It;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

.field private final b:LX/2oL;

.field private final c:LX/2oO;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/2oL;LX/2oO;)V
    .locals 0

    .prologue
    .line 2693574
    iput-object p1, p0, LX/JRe;->a:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693575
    iput-object p2, p0, LX/JRe;->b:LX/2oL;

    .line 2693576
    iput-object p3, p0, LX/JRe;->c:LX/2oO;

    .line 2693577
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2693573
    return-void
.end method

.method public final a(LX/2op;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2693566
    iget-object v0, p0, LX/JRe;->c:LX/2oO;

    invoke-virtual {v0}, LX/2oO;->h()V

    .line 2693567
    iget-object v0, p0, LX/JRe;->b:LX/2oL;

    invoke-virtual {v0, v2}, LX/2oL;->a(I)V

    .line 2693568
    iget-object v0, p0, LX/JRe;->b:LX/2oL;

    const/4 v1, 0x1

    .line 2693569
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 2693570
    iget-object v0, p0, LX/JRe;->b:LX/2oL;

    .line 2693571
    iput-boolean v2, v0, LX/2oL;->d:Z

    .line 2693572
    return-void
.end method

.method public final a(LX/2oq;)V
    .locals 0

    .prologue
    .line 2693562
    return-void
.end method

.method public final a(LX/2or;)V
    .locals 2

    .prologue
    .line 2693563
    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->ERROR_IO:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->PLAYBACK_EXCEPTION:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->SERVER_DIED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->UNSUPPORTED:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2or;->b:LX/7Jj;

    iget-object v0, v0, LX/7Jj;->value:Ljava/lang/String;

    sget-object v1, LX/7Jj;->DISMISS:LX/7Jj;

    iget-object v1, v1, LX/7Jj;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2693564
    :cond_0
    iget-object v0, p0, LX/JRe;->c:LX/2oO;

    invoke-virtual {v0}, LX/2oO;->j()V

    .line 2693565
    :cond_1
    return-void
.end method
