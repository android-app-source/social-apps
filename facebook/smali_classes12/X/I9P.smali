.class public final LX/I9P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;)V
    .locals 0

    .prologue
    .line 2542935
    iput-object p1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ko_()Z
    .locals 4

    .prologue
    const v3, 0x6000c

    const/4 v0, 0x1

    .line 2542925
    iget-object v1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    invoke-virtual {v1}, LX/I9L;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2542926
    iget-object v1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "EventGuestListTTI"

    invoke-interface {v1, v3, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2542927
    :goto_0
    return v0

    .line 2542928
    :cond_0
    iget-object v1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    .line 2542929
    iget-object v2, v1, LX/I9l;->c:LX/Blc;

    move-object v1, v2

    .line 2542930
    if-nez v1, :cond_1

    iget-object v1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    .line 2542931
    iget-object v2, v1, LX/I9l;->e:LX/I9k;

    move-object v1, v2

    .line 2542932
    sget-object v2, LX/I9k;->ERROR:LX/I9k;

    if-ne v1, v2, :cond_1

    .line 2542933
    iget-object v1, p0, LX/I9P;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v2, "EventGuestListTTI"

    invoke-interface {v1, v3, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    goto :goto_0

    .line 2542934
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
