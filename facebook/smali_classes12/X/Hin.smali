.class public LX/Hin;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/His;

.field private final b:LX/HiN;

.field private final c:LX/Hii;

.field private final d:LX/0Uh;

.field public e:Z

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/His;LX/HiN;LX/0Uh;LX/Hiu;LX/Hit;)V
    .locals 3
    .param p5    # LX/Hiu;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Hit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497957
    iput-object p2, p0, LX/Hin;->a:LX/His;

    .line 2497958
    iput-object p3, p0, LX/Hin;->b:LX/HiN;

    .line 2497959
    iput-object p4, p0, LX/Hin;->d:LX/0Uh;

    .line 2497960
    iget-object v0, p0, LX/Hin;->a:LX/His;

    .line 2497961
    iput-object p5, v0, LX/His;->d:LX/Hiu;

    .line 2497962
    new-instance v0, LX/Hii;

    .line 2497963
    new-instance v1, LX/Him;

    invoke-static {p0}, LX/Hin;->c(LX/Hin;)LX/0Px;

    move-result-object v2

    sget-object p2, LX/Hil;->RECENT:LX/Hil;

    invoke-direct {v1, p1, v2, p2, p6}, LX/Him;-><init>(Landroid/content/Context;LX/0Px;LX/Hil;LX/Hit;)V

    move-object v1, v1

    .line 2497964
    new-instance v2, LX/Him;

    invoke-static {p0}, LX/Hin;->d(LX/Hin;)LX/0Px;

    move-result-object p2

    sget-object p3, LX/Hil;->CONVERSATION:LX/Hil;

    invoke-direct {v2, p1, p2, p3, p6}, LX/Him;-><init>(Landroid/content/Context;LX/0Px;LX/Hil;LX/Hit;)V

    move-object v2, v2

    .line 2497965
    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hii;-><init>(LX/0Px;)V

    iput-object v0, p0, LX/Hin;->c:LX/Hii;

    .line 2497966
    return-void
.end method

.method public static c(LX/Hin;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2497967
    iget-object v0, p0, LX/Hin;->f:LX/0Px;

    if-nez v0, :cond_0

    .line 2497968
    iget-object v0, p0, LX/Hin;->b:LX/HiN;

    sget-object v1, LX/HiM;->RECENT:LX/HiM;

    invoke-virtual {v0, v1}, LX/HiN;->a(LX/HiM;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Hin;->f:LX/0Px;

    .line 2497969
    :cond_0
    iget-object v0, p0, LX/Hin;->f:LX/0Px;

    return-object v0
.end method

.method public static d(LX/Hin;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2497970
    iget-object v0, p0, LX/Hin;->g:LX/0Px;

    if-nez v0, :cond_0

    .line 2497971
    iget-object v0, p0, LX/Hin;->b:LX/HiN;

    sget-object v1, LX/HiM;->CONVERSATION:LX/HiM;

    invoke-virtual {v0, v1}, LX/HiN;->a(LX/HiM;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Hin;->g:LX/0Px;

    .line 2497972
    :cond_0
    iget-object v0, p0, LX/Hin;->g:LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)LX/1OM;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2497973
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Hin;->e:Z

    .line 2497974
    invoke-virtual {p0}, LX/Hin;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Hin;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hin;->c:LX/Hii;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hin;->a:LX/His;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2497975
    iget-object v0, p0, LX/Hin;->d:LX/0Uh;

    const/16 v1, 0x100

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
