.class public final LX/JGU;
.super LX/JGN;
.source ""


# static fields
.field private static final c:Landroid/graphics/Paint;


# instance fields
.field private final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2667392
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, LX/JGU;->c:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2667393
    invoke-direct {p0}, LX/JGN;-><init>()V

    .line 2667394
    iput p1, p0, LX/JGU;->d:I

    .line 2667395
    return-void
.end method


# virtual methods
.method public final c(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2667396
    sget-object v0, LX/JGU;->c:Landroid/graphics/Paint;

    iget v1, p0, LX/JGU;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2667397
    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v1

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v2

    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v3

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v4

    sget-object v5, LX/JGU;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2667398
    return-void
.end method
