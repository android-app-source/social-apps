.class public final LX/I2g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I2o;

.field public final synthetic b:Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;LX/I2o;)V
    .locals 0

    .prologue
    .line 2530658
    iput-object p1, p0, LX/I2g;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    iput-object p2, p0, LX/I2g;->a:LX/I2o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xa9373e0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2530659
    iget-object v0, p0, LX/I2g;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    iget-object v0, v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->b:LX/1nQ;

    iget-object v2, p0, LX/I2g;->a:LX/I2o;

    invoke-interface {v2}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2530660
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v2, v3

    .line 2530661
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v2

    invoke-virtual {v0, v2}, LX/1nQ;->a(I)V

    .line 2530662
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/I2g;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    iget-object v0, v0, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2530663
    const-string v2, "target_fragment"

    sget-object v3, LX/0cQ;->EVENTS_DISCOVERY_UPCOMING_EVENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2530664
    const-string v2, "extra_ref_module"

    iget-object v3, p0, LX/I2g;->a:LX/I2o;

    invoke-interface {v3}, LX/I2j;->s()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530665
    iget-object v2, p0, LX/I2g;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardViewAllEventsRowPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2530666
    const v0, -0x3018aee7

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
