.class public final enum LX/I5e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I5e;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I5e;

.field public static final enum LOAD_INITIAL_NEWER_FEED:LX/I5e;

.field public static final enum LOAD_INITIAL_OLDER_FEED:LX/I5e;

.field public static final enum LOAD_NEXT_PAGE:LX/I5e;

.field public static final enum REFRESH_FEED:LX/I5e;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2535927
    new-instance v0, LX/I5e;

    const-string v1, "LOAD_INITIAL_OLDER_FEED"

    invoke-direct {v0, v1, v2}, LX/I5e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I5e;->LOAD_INITIAL_OLDER_FEED:LX/I5e;

    .line 2535928
    new-instance v0, LX/I5e;

    const-string v1, "LOAD_INITIAL_NEWER_FEED"

    invoke-direct {v0, v1, v3}, LX/I5e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I5e;->LOAD_INITIAL_NEWER_FEED:LX/I5e;

    .line 2535929
    new-instance v0, LX/I5e;

    const-string v1, "LOAD_NEXT_PAGE"

    invoke-direct {v0, v1, v4}, LX/I5e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I5e;->LOAD_NEXT_PAGE:LX/I5e;

    .line 2535930
    new-instance v0, LX/I5e;

    const-string v1, "REFRESH_FEED"

    invoke-direct {v0, v1, v5}, LX/I5e;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I5e;->REFRESH_FEED:LX/I5e;

    .line 2535931
    const/4 v0, 0x4

    new-array v0, v0, [LX/I5e;

    sget-object v1, LX/I5e;->LOAD_INITIAL_OLDER_FEED:LX/I5e;

    aput-object v1, v0, v2

    sget-object v1, LX/I5e;->LOAD_INITIAL_NEWER_FEED:LX/I5e;

    aput-object v1, v0, v3

    sget-object v1, LX/I5e;->LOAD_NEXT_PAGE:LX/I5e;

    aput-object v1, v0, v4

    sget-object v1, LX/I5e;->REFRESH_FEED:LX/I5e;

    aput-object v1, v0, v5

    sput-object v0, LX/I5e;->$VALUES:[LX/I5e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2535933
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I5e;
    .locals 1

    .prologue
    .line 2535934
    const-class v0, LX/I5e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I5e;

    return-object v0
.end method

.method public static values()[LX/I5e;
    .locals 1

    .prologue
    .line 2535932
    sget-object v0, LX/I5e;->$VALUES:[LX/I5e;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I5e;

    return-object v0
.end method
