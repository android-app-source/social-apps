.class public final LX/Ia1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ia2;

.field public final synthetic b:LX/Ia3;


# direct methods
.method public constructor <init>(LX/Ia3;LX/Ia2;)V
    .locals 0

    .prologue
    .line 2590517
    iput-object p1, p0, LX/Ia1;->b:LX/Ia3;

    iput-object p2, p0, LX/Ia1;->a:LX/Ia2;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2590518
    iget-object v0, p0, LX/Ia1;->b:LX/Ia3;

    iget-object v0, v0, LX/Ia3;->a:LX/03V;

    const-string v1, "SendConfirmationMutator"

    const-string v2, "Fail to request confirmation code"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2590519
    iget-object v0, p0, LX/Ia1;->a:LX/Ia2;

    invoke-interface {v0}, LX/Ia2;->a()V

    .line 2590520
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2590521
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2590522
    if-nez p1, :cond_0

    .line 2590523
    iget-object v0, p0, LX/Ia1;->a:LX/Ia2;

    invoke-interface {v0}, LX/Ia2;->a()V

    .line 2590524
    :goto_0
    return-void

    .line 2590525
    :cond_0
    iget-object v1, p0, LX/Ia1;->a:LX/Ia2;

    .line 2590526
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2590527
    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;

    invoke-interface {v1, v0}, LX/Ia2;->a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;)V

    goto :goto_0
.end method
