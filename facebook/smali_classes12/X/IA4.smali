.class public LX/IA4;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# static fields
.field public static r:LX/IAI;

.field private static s:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static t:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# instance fields
.field public A:LX/I9f;

.field public B:LX/8ry;

.field public C:Lcom/facebook/friends/ui/SmartButtonLite;

.field public j:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/B9v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/2hX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/I9g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Ljava/lang/String;

.field public v:Lcom/facebook/events/model/EventUser;

.field public w:LX/Blc;

.field private x:Lcom/facebook/events/common/EventActionContext;

.field public y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

.field private z:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 2544647
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2544648
    const/4 v4, 0x0

    const/4 v2, -0x2

    .line 2544649
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/IA4;

    invoke-static {v0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v5

    check-cast v5, LX/I76;

    invoke-static {v0}, LX/B9v;->b(LX/0QB;)LX/B9v;

    move-result-object v6

    check-cast v6, LX/B9v;

    invoke-static {v0}, LX/2hX;->b(LX/0QB;)LX/2hX;

    move-result-object v7

    check-cast v7, LX/2hX;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    const-class p1, LX/I9g;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/I9g;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v5, v3, LX/IA4;->j:LX/I76;

    iput-object v6, v3, LX/IA4;->k:LX/B9v;

    iput-object v7, v3, LX/IA4;->l:LX/2hX;

    iput-object v8, v3, LX/IA4;->m:LX/0wM;

    iput-object v9, v3, LX/IA4;->n:Landroid/content/res/Resources;

    iput-object v10, v3, LX/IA4;->o:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v3, LX/IA4;->p:LX/I9g;

    iput-object v0, v3, LX/IA4;->q:LX/23P;

    .line 2544650
    sget-object v0, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2544651
    new-instance v0, Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2544652
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object v1, p0, LX/IA4;->q:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2544653
    new-instance v0, LX/6VC;

    invoke-direct {v0, v2, v2}, LX/6VC;-><init>(II)V

    .line 2544654
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/6VC;->b:Z

    .line 2544655
    iget-object v1, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0, v1, v0}, LX/IA4;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2544656
    new-instance v0, LX/8ry;

    invoke-direct {v0}, LX/8ry;-><init>()V

    iput-object v0, p0, LX/IA4;->B:LX/8ry;

    .line 2544657
    new-instance v0, LX/I9u;

    invoke-direct {v0, p0}, LX/I9u;-><init>(LX/IA4;)V

    invoke-virtual {p0, v0}, LX/IA4;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544658
    iget-object v0, p0, LX/IA4;->m:LX/0wM;

    const v1, 0x7f0207d6

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2544659
    iget-object v1, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v2, 0x7f0b1437

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v3, 0x7f0b1437

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2544660
    new-instance v1, LX/IAI;

    iget-object v2, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v3, 0x7f0b1436

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v0, v2}, LX/IAI;-><init>(Landroid/graphics/drawable/Drawable;I)V

    sput-object v1, LX/IA4;->r:LX/IAI;

    .line 2544661
    return-void
.end method

.method public static a(LX/IA4;LX/7vJ;)Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 2544624
    sget-object v0, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    if-ne p1, v0, :cond_0

    .line 2544625
    sget-object v0, LX/IA4;->s:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 2544626
    sget-object v0, LX/IA4;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2544627
    :goto_0
    return-object v0

    .line 2544628
    :cond_0
    sget-object v0, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    if-ne p1, v0, :cond_1

    .line 2544629
    sget-object v0, LX/IA4;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 2544630
    sget-object v0, LX/IA4;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2544631
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Synthetic Guest Profile Photo doesn\'t exist for this User Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2544632
    :cond_2
    iget-object v0, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v2, 0x7f0b1461

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 2544633
    iget-object v0, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v2, 0x7f0b1462

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2544634
    iget-object v4, p0, LX/IA4;->m:LX/0wM;

    sget-object v0, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    if-ne p1, v0, :cond_3

    const v0, 0x7f020850

    :goto_1
    const/4 v5, -0x1

    invoke-virtual {v4, v0, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2544635
    float-to-int v0, v2

    float-to-int v5, v2

    invoke-virtual {v4, v7, v7, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2544636
    sub-float v0, v3, v2

    float-to-int v0, v0

    div-int/lit8 v2, v0, 0x2

    .line 2544637
    new-instance v5, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v5, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 2544638
    float-to-int v0, v3

    float-to-int v3, v3

    invoke-virtual {v5, v7, v7, v0, v3}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 2544639
    invoke-virtual {v5}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget-object v3, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v6, 0x7f0a009e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2544640
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v5, v3, v7

    aput-object v4, v3, v1

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 2544641
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/drawable/LayerDrawable;->setLayerInset(IIIII)V

    .line 2544642
    sget-object v1, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    if-ne p1, v1, :cond_4

    .line 2544643
    sput-object v0, LX/IA4;->s:Landroid/graphics/drawable/Drawable;

    .line 2544644
    :goto_2
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 2544645
    :cond_3
    const v0, 0x7f020809

    goto :goto_1

    .line 2544646
    :cond_4
    sput-object v0, LX/IA4;->t:Landroid/graphics/drawable/Drawable;

    goto :goto_2
.end method

.method public static a(LX/IA4;IILandroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 2

    .prologue
    .line 2544622
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v1, LX/IA3;

    invoke-direct {v1, p0, p1, p2, p3}, LX/IA3;-><init>(LX/IA4;IILandroid/view/MenuItem$OnMenuItemClickListener;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544623
    return-void
.end method

.method public static a$redex0(LX/IA4;LX/7TY;)V
    .locals 4

    .prologue
    .line 2544444
    const v0, 0x7f081f41

    invoke-virtual {p1, v0}, LX/34c;->e(I)LX/3Ai;

    move-result-object v0

    iget-object v1, p0, LX/IA4;->m:LX/0wM;

    const v2, 0x7f0208cf

    const v3, -0x958e80

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/I9q;

    invoke-direct {v1, p0}, LX/I9q;-><init>(LX/IA4;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2544445
    return-void
.end method

.method public static a$redex0(LX/IA4;LX/Blc;)V
    .locals 2

    .prologue
    .line 2544614
    iput-object p1, p0, LX/IA4;->w:LX/Blc;

    .line 2544615
    iget-object v0, p0, LX/IA4;->w:LX/Blc;

    if-nez v0, :cond_0

    .line 2544616
    const v0, 0x7f081f2c

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2544617
    :goto_0
    return-void

    .line 2544618
    :cond_0
    sget-object v0, LX/I9t;->a:[I

    iget-object v1, p0, LX/IA4;->w:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2544619
    :pswitch_0
    const v0, 0x7f081f2d

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 2544620
    :pswitch_1
    const v0, 0x7f081f2e

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 2544621
    :pswitch_2
    const v0, 0x7f081f2f

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(LX/IA4;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2544606
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2544607
    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2544608
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2544609
    const-string v1, "android.intent.extra.EMAIL"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544610
    iget-object p1, v4, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    move-object v4, p1

    .line 2544611
    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2544612
    iget-object v1, p0, LX/IA4;->o:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081f2b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2544613
    return-void
.end method

.method public static h$redex0(LX/IA4;)V
    .locals 4

    .prologue
    .line 2544600
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2544601
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sms:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544602
    iget-object v3, v2, Lcom/facebook/events/model/EventUser;->f:Ljava/lang/String;

    move-object v2, v3

    .line 2544603
    invoke-static {v2}, LX/3Lz;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2544604
    iget-object v1, p0, LX/IA4;->o:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v3, 0x7f081f2b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2544605
    return-void
.end method

.method public static j(LX/IA4;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2544596
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, LX/IA4;->setAlpha(F)V

    .line 2544597
    invoke-virtual {p0, v1}, LX/IA4;->setClickable(Z)V

    .line 2544598
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544599
    return-void
.end method

.method public static setEditButtonAppearance(LX/IA4;I)V
    .locals 4

    .prologue
    .line 2544589
    sget-object v0, LX/I9a;->EDIT:LX/I9a;

    .line 2544590
    invoke-virtual {v0}, LX/I9a;->getGuestButtonDrawableResId()I

    move-result v1

    .line 2544591
    invoke-virtual {v0}, LX/I9a;->getGuestButtonColorResId()I

    move-result v0

    .line 2544592
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object v3, p0, LX/IA4;->m:LX/0wM;

    invoke-virtual {v3, v1, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544593
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2544594
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544595
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/events/model/EventUser;LX/Blc;LX/Blc;Lcom/facebook/events/common/EventActionContext;ZLX/0Px;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/events/model/EventUser;",
            "LX/Blc;",
            "LX/Blc;",
            "Lcom/facebook/events/common/EventActionContext;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2544458
    iput-object p1, p0, LX/IA4;->u:Ljava/lang/String;

    .line 2544459
    iget-object v6, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544460
    iput-object p3, p0, LX/IA4;->w:LX/Blc;

    .line 2544461
    iput-object p5, p0, LX/IA4;->x:Lcom/facebook/events/common/EventActionContext;

    .line 2544462
    iput-object p2, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544463
    iput-object p7, p0, LX/IA4;->z:LX/0Px;

    .line 2544464
    iget-object v0, p0, LX/IA4;->p:LX/I9g;

    iget-object v1, p0, LX/IA4;->u:Ljava/lang/String;

    iget-object v2, p0, LX/IA4;->w:LX/Blc;

    iget-object v3, p0, LX/IA4;->z:LX/0Px;

    iget-object v4, p0, LX/IA4;->x:Lcom/facebook/events/common/EventActionContext;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->GUEST_LIST_EDIT_GUEST_RSVP:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/I9g;->a(Ljava/lang/String;LX/Blc;LX/0Px;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)LX/I9f;

    move-result-object v0

    iput-object v0, p0, LX/IA4;->A:LX/I9f;

    .line 2544465
    if-eqz v6, :cond_0

    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544466
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2544467
    iget-object v1, v6, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2544468
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2544469
    :cond_0
    iput-object p2, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544470
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    invoke-virtual {v0}, Lcom/facebook/events/model/EventUser;->i()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2544471
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544472
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2544473
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2544474
    :cond_1
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const-string v1, ""

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2544475
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544476
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544477
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v1, LX/6VC;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, LX/6VC;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2544478
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544479
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v0, v1

    .line 2544480
    sget-object v1, LX/7vJ;->EMAIL_USER:LX/7vJ;

    if-ne v0, v1, :cond_3

    .line 2544481
    if-eqz p6, :cond_2

    .line 2544482
    const v0, 0x7f081f29

    invoke-static {p0, v0}, LX/IA4;->setEditButtonAppearance(LX/IA4;I)V

    .line 2544483
    const v0, 0x7f081f29

    const v1, 0x7f020850

    new-instance v2, LX/I9v;

    invoke-direct {v2, p0, p8}, LX/I9v;-><init>(LX/IA4;Ljava/lang/String;)V

    invoke-static {p0, v0, v1, v2}, LX/IA4;->a(LX/IA4;IILandroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 2544484
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2544485
    :goto_0
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    const/4 p1, 0x0

    .line 2544486
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v1, v1

    .line 2544487
    sget-object v2, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    if-ne v1, v2, :cond_8

    .line 2544488
    invoke-static {p0, v1}, LX/IA4;->a(LX/IA4;LX/7vJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544489
    iget-object v1, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v2, 0x7f081f27

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2544490
    :goto_1
    iget-object v0, p0, LX/IA4;->w:LX/Blc;

    if-ne p4, v0, :cond_7

    .line 2544491
    const/4 v1, 0x1

    .line 2544492
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, LX/IA4;->setAlpha(F)V

    .line 2544493
    invoke-virtual {p0, v1}, LX/IA4;->setClickable(Z)V

    .line 2544494
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544495
    :goto_2
    return-void

    .line 2544496
    :cond_2
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object v1, p0, LX/IA4;->m:LX/0wM;

    const v2, 0x7f020850

    const v3, -0x423e37

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544497
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f29

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2544498
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544499
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v1, LX/I9w;

    invoke-direct {v1, p0, p8}, LX/I9w;-><init>(LX/IA4;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544500
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2544501
    goto :goto_0

    .line 2544502
    :cond_3
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544503
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->a:LX/7vJ;

    move-object v0, v1

    .line 2544504
    sget-object v1, LX/7vJ;->SMS_USER:LX/7vJ;

    if-ne v0, v1, :cond_5

    .line 2544505
    if-eqz p6, :cond_4

    .line 2544506
    const v0, 0x7f081f2a

    invoke-static {p0, v0}, LX/IA4;->setEditButtonAppearance(LX/IA4;I)V

    .line 2544507
    const v0, 0x7f081f2a

    const v1, 0x7f020809

    new-instance v2, LX/I9x;

    invoke-direct {v2, p0}, LX/I9x;-><init>(LX/IA4;)V

    invoke-static {p0, v0, v1, v2}, LX/IA4;->a(LX/IA4;IILandroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 2544508
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2544509
    goto/16 :goto_0

    .line 2544510
    :cond_4
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object v1, p0, LX/IA4;->m:LX/0wM;

    const v2, 0x7f020809

    const v3, -0x423e37

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544511
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081f2a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2544512
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544513
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v1, LX/I9y;

    invoke-direct {v1, p0}, LX/I9y;-><init>(LX/IA4;)V

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544514
    iget-object v0, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2544515
    goto/16 :goto_0

    .line 2544516
    :cond_5
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544517
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v1

    .line 2544518
    iget-object v1, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544519
    iget-boolean v2, v1, Lcom/facebook/events/model/EventUser;->j:Z

    move v1, v2

    .line 2544520
    const/16 p1, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2544521
    if-eqz v0, :cond_6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v2, :cond_6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_11

    .line 2544522
    :cond_6
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    .line 2544523
    :goto_3
    goto/16 :goto_0

    .line 2544524
    :cond_7
    invoke-static {p0}, LX/IA4;->j(LX/IA4;)V

    .line 2544525
    iget-object v0, p0, LX/IA4;->w:LX/Blc;

    invoke-static {p0, v0}, LX/IA4;->a$redex0(LX/IA4;LX/Blc;)V

    goto/16 :goto_2

    .line 2544526
    :cond_8
    sget-object v2, LX/7vJ;->EMAIL_USER:LX/7vJ;

    if-ne v1, v2, :cond_9

    .line 2544527
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2544528
    iget-object v2, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2544529
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2544530
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2544531
    :cond_9
    sget-object v2, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    if-ne v1, v2, :cond_a

    .line 2544532
    invoke-static {p0, v1}, LX/IA4;->a(LX/IA4;LX/7vJ;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544533
    iget-object v1, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v2, 0x7f081f28

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2544534
    :cond_a
    sget-object v2, LX/7vJ;->SMS_USER:LX/7vJ;

    if-ne v1, v2, :cond_b

    .line 2544535
    iget-object v1, v0, Lcom/facebook/events/model/EventUser;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2544536
    iget-object v2, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2544537
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2544538
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2544539
    :cond_b
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2544540
    iget-object v2, p0, LX/IA4;->w:LX/Blc;

    sget-object v3, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-ne v2, v3, :cond_c

    .line 2544541
    iget-object v2, v0, Lcom/facebook/events/model/EventUser;->i:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-object v2, v2

    .line 2544542
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v2, v3, :cond_c

    .line 2544543
    const-string v2, "[checkmark_placeholder]"

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544544
    sget-object v2, LX/IA4;->r:LX/IAI;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v1, v2, p1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2544545
    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544546
    iget-object v2, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v3, 0x7f081f20

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544547
    :cond_c
    iget-object v2, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544548
    iget v3, v2, Lcom/facebook/events/model/EventUser;->g:I

    move v2, v3

    .line 2544549
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v3

    .line 2544550
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_f

    .line 2544551
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_d

    .line 2544552
    const-string v2, " \u22c5 "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544553
    :cond_d
    iget-object v2, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v3, 0x7f080f85

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544554
    :cond_e
    :goto_4
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2544555
    :cond_f
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v3

    .line 2544556
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v3, v4, :cond_e

    if-lez v2, :cond_e

    .line 2544557
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_10

    .line 2544558
    const-string v3, " \u22c5 "

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2544559
    :cond_10
    iget-object v3, p0, LX/IA4;->n:Landroid/content/res/Resources;

    const v4, 0x7f0f00e1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p1

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_4

    .line 2544560
    :cond_11
    invoke-static {v0, p6}, LX/I9a;->getEventGuestListButton(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)LX/I9a;

    move-result-object v6

    .line 2544561
    if-nez v6, :cond_13

    move v2, v3

    .line 2544562
    :goto_5
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v5, :cond_12

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v5, :cond_12

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v5, :cond_14

    :cond_12
    move v5, v3

    .line 2544563
    :goto_6
    if-eqz v2, :cond_17

    .line 2544564
    if-eqz v5, :cond_16

    .line 2544565
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-static {v2, v0}, LX/8ry;->a(Lcom/facebook/friends/ui/SmartButtonLite;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2544566
    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b08ab

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2544567
    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0b08aa

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2544568
    iget-object p1, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance p2, LX/6VC;

    invoke-direct {p2, v2, v5}, LX/6VC;-><init>(II)V

    invoke-virtual {p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2544569
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_15

    .line 2544570
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0a00a3

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 2544571
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f020b0b

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/friends/ui/SmartButtonLite;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544572
    :goto_7
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544573
    sget-object v2, LX/I9a;->MESSAGE:LX/I9a;

    if-ne v6, v2, :cond_18

    .line 2544574
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v3, LX/I9z;

    invoke-direct {v3, p0}, LX/I9z;-><init>(LX/IA4;)V

    invoke-virtual {v2, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2544575
    :goto_8
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, v4}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto/16 :goto_3

    :cond_13
    move v2, v4

    .line 2544576
    goto/16 :goto_5

    :cond_14
    move v5, v4

    .line 2544577
    goto :goto_6

    .line 2544578
    :cond_15
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0a00d5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 2544579
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f020b08

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/friends/ui/SmartButtonLite;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    .line 2544580
    :cond_16
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setVisibility(I)V

    goto/16 :goto_3

    .line 2544581
    :cond_17
    invoke-virtual {v6}, LX/I9a;->getGuestButtonDrawableResId()I

    move-result v2

    .line 2544582
    invoke-virtual {v6}, LX/I9a;->getGuestButtonColorResId()I

    move-result v5

    .line 2544583
    invoke-virtual {v6}, LX/I9a;->getGuestButtonDescriptionResId()I

    move-result p1

    .line 2544584
    iget-object p2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object p3, p0, LX/IA4;->m:LX/0wM;

    invoke-virtual {p3, v2, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2544585
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/IA4;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2544586
    :cond_18
    sget-object v2, LX/I9a;->EDIT:LX/I9a;

    if-ne v6, v2, :cond_19

    .line 2544587
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v3, LX/IA1;

    invoke-direct {v3, p0, v1}, LX/IA1;-><init>(LX/IA4;Z)V

    invoke-virtual {v2, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_8

    .line 2544588
    :cond_19
    iget-object v2, p0, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    new-instance v3, LX/IA2;

    invoke-direct {v3, p0, v0}, LX/IA2;-><init>(LX/IA4;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual {v2, v3}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_8
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2544454
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    if-eqz v0, :cond_0

    .line 2544455
    iget-object v0, p0, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544456
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v0, p0

    .line 2544457
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x9faa47a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544450
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onAttachedToWindow()V

    .line 2544451
    iget-object v1, p0, LX/IA4;->l:LX/2hX;

    const/4 v2, 0x0

    .line 2544452
    iput-boolean v2, v1, LX/2hY;->d:Z

    .line 2544453
    const/16 v1, 0x2d

    const v2, 0x78928b07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xbd13863

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544446
    iget-object v1, p0, LX/IA4;->l:LX/2hX;

    const/4 v2, 0x1

    .line 2544447
    iput-boolean v2, v1, LX/2hY;->d:Z

    .line 2544448
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onDetachedFromWindow()V

    .line 2544449
    const/16 v1, 0x2d

    const v2, -0xda40f88

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
