.class public final LX/ICN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/ICO;


# direct methods
.method public constructor <init>(LX/ICO;)V
    .locals 0

    .prologue
    .line 2549336
    iput-object p1, p0, LX/ICN;->a:LX/ICO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x78231228

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549337
    iget-object v1, p0, LX/ICN;->a:LX/ICO;

    iget-object v1, v1, LX/ICO;->l:LX/ICK;

    .line 2549338
    iget-object v3, v1, LX/ICK;->a:LX/ICO;

    invoke-virtual {v3}, LX/0ht;->l()V

    .line 2549339
    iget-object v3, v1, LX/ICK;->c:LX/ICL;

    iget-object v3, v3, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v4, v1, LX/ICK;->c:LX/ICL;

    iget-object v4, v4, LX/ICL;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    iget-object p0, v1, LX/ICK;->b:Landroid/view/View;

    invoke-static {v3, v4, p0}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;Landroid/view/View;)V

    .line 2549340
    iget-object v3, v1, LX/ICK;->c:LX/ICL;

    iget-object v3, v3, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v4, LX/ICQ;->START:LX/ICQ;

    iget-object p0, v1, LX/ICK;->c:LX/ICL;

    iget-object p0, p0, LX/ICL;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-virtual {v3, v4, p0}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    .line 2549341
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2549342
    const-string v4, "story_gallery_survey_feed_unit"

    iget-object p0, v1, LX/ICK;->c:LX/ICL;

    iget-object p0, p0, LX/ICL;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-static {v3, v4, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2549343
    iget-object v4, v1, LX/ICK;->c:LX/ICL;

    iget-object v4, v4, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v4, v4, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->b:LX/17W;

    iget-object p0, v1, LX/ICK;->b:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object p1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->a:Ljava/lang/String;

    invoke-virtual {v4, p0, p1, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2549344
    const v1, 0x2ade9dea

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
