.class public LX/Ite;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Mk;

.field private final b:LX/03V;

.field private final c:LX/2Mv;

.field private final d:LX/1sj;


# direct methods
.method public constructor <init>(LX/2Mk;LX/03V;LX/2Mv;LX/1sj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2623701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2623702
    iput-object p1, p0, LX/Ite;->a:LX/2Mk;

    .line 2623703
    iput-object p2, p0, LX/Ite;->b:LX/03V;

    .line 2623704
    iput-object p3, p0, LX/Ite;->c:LX/2Mv;

    .line 2623705
    iput-object p4, p0, LX/Ite;->d:LX/1sj;

    .line 2623706
    return-void
.end method

.method private static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6mT;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2623691
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    .line 2623692
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2623693
    if-eqz v1, :cond_1

    .line 2623694
    const/4 v8, 0x0

    .line 2623695
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dh()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;

    move-result-object v2

    .line 2623696
    new-instance v3, LX/6mM;

    const-string v4, "%.6f"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "%.6f"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2, v8}, LX/6mM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2623697
    new-instance v2, LX/6mT;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bs()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v3, v4, v8}, LX/6mT;-><init>(LX/6mM;Ljava/lang/Boolean;Ljava/lang/Long;)V

    move-object v0, v2

    .line 2623698
    :goto_1
    return-object v0

    :cond_1
    const/4 v6, 0x0

    .line 2623699
    new-instance v2, LX/6mT;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, v6, v6, v3}, LX/6mT;-><init>(LX/6mM;Ljava/lang/Boolean;Ljava/lang/Long;)V

    move-object v0, v2

    .line 2623700
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(LX/1u2;Lcom/facebook/fbtrace/FbTraceNode;)[B
    .locals 4

    .prologue
    .line 2623682
    new-instance v1, LX/3ll;

    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-ne p1, v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-direct {v1, v0}, LX/3ll;-><init>(Ljava/lang/String;)V

    .line 2623683
    new-instance v0, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v0, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 2623684
    invoke-virtual {v0, v1}, LX/1so;->a(LX/1u2;)[B

    move-result-object v1

    .line 2623685
    invoke-virtual {v0, p0}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    .line 2623686
    array-length v2, v1

    array-length v3, v0

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 2623687
    const/4 v3, 0x0

    array-length p0, v1

    array-length p1, v0

    invoke-static {v0, v3, v2, p0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2623688
    move-object v0, v2

    .line 2623689
    return-object v0

    .line 2623690
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;)[B
    .locals 3
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2623678
    invoke-static {p0, p1, p2, p3}, LX/Ite;->b(LX/Ite;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;)LX/6mb;

    move-result-object v0

    .line 2623679
    :try_start_0
    invoke-static {v0, p2}, LX/Ite;->a(LX/1u2;Lcom/facebook/fbtrace/FbTraceNode;)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2623680
    :catch_0
    move-exception v0

    .line 2623681
    new-instance v1, LX/Itq;

    sget-object v2, LX/Itv;->SEND_FAILED_THRIFT_EXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v0, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Itq;-><init>(LX/Itx;)V

    throw v1
.end method

.method private a(Ljava/util/List;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;J)[B
    .locals 4
    .param p3    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            "Ljava/lang/Integer;",
            "J)[B"
        }
    .end annotation

    .prologue
    .line 2623579
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2623580
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623581
    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v3

    invoke-static {p0, v0, v3, p3}, LX/Ite;->b(LX/Ite;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;)LX/6mb;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2623582
    :cond_0
    new-instance v0, LX/6mc;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, LX/6mc;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;)V

    .line 2623583
    :try_start_0
    invoke-static {v0, p2}, LX/Ite;->a(LX/1u2;Lcom/facebook/fbtrace/FbTraceNode;)[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2623584
    :catch_0
    move-exception v0

    .line 2623585
    new-instance v1, LX/Itq;

    sget-object v2, LX/Itv;->SEND_FAILED_THRIFT_EXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v0, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Itq;-><init>(LX/Itx;)V

    throw v1
.end method

.method private static b(LX/Ite;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;)LX/6mb;
    .locals 8
    .param p2    # Lcom/facebook/fbtrace/FbTraceNode;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2623592
    new-instance v2, LX/Itn;

    invoke-direct {v2}, LX/Itn;-><init>()V

    .line 2623593
    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2623594
    iget-object v0, p0, LX/Ite;->c:LX/2Mv;

    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v4}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide v4, 0x3743985c30223L

    invoke-static {v4, v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2623595
    :goto_0
    iget-object v4, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v5, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v4, v5, :cond_5

    iget-wide v4, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2623596
    :goto_1
    iput-object v0, v2, LX/Itn;->a:Ljava/lang/String;

    .line 2623597
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2623598
    iput-object v0, v2, LX/Itn;->b:Ljava/lang/String;

    .line 2623599
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2623600
    iput-object v0, v2, LX/Itn;->c:Ljava/lang/Long;

    .line 2623601
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2623602
    iput-object v0, v2, LX/Itn;->m:Ljava/lang/Integer;

    .line 2623603
    invoke-static {v3}, LX/2Mk;->q(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623604
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 2623605
    iput-object v0, v2, LX/Itn;->e:Ljava/util/Map;

    .line 2623606
    :cond_0
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_6

    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    .line 2623607
    :goto_2
    if-eqz v0, :cond_7

    iget-object v4, v0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2623608
    iget-object v0, v0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    .line 2623609
    iput-object v0, v2, LX/Itn;->f:Ljava/lang/String;

    .line 2623610
    :cond_1
    :goto_3
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2623611
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    invoke-static {v0}, LX/DoA;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2623612
    iput-object v0, v2, LX/Itn;->g:Ljava/lang/String;

    .line 2623613
    :cond_2
    :goto_4
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2623614
    iput-object v0, v2, LX/Itn;->k:Ljava/lang/Long;

    .line 2623615
    invoke-static {v3}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2623616
    const/4 v4, 0x0

    .line 2623617
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2623618
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2623619
    :cond_3
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2623620
    :goto_6
    move-object v0, v0

    .line 2623621
    if-nez v0, :cond_9

    .line 2623622
    new-instance v0, LX/Itq;

    sget-object v1, LX/Itv;->SEND_SKIPPED_MEDIA_UPLOAD_FAILED:LX/Itv;

    invoke-static {v1}, LX/Itx;->a(LX/Itv;)LX/Itx;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Itq;-><init>(LX/Itx;)V

    throw v0

    .line 2623623
    :cond_4
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto/16 :goto_0

    .line 2623624
    :cond_5
    iget-wide v4, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    .line 2623625
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "tfbid_"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2623626
    goto/16 :goto_1

    .line 2623627
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 2623628
    :cond_7
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2623629
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    .line 2623630
    iput-object v0, v2, LX/Itn;->f:Ljava/lang/String;

    .line 2623631
    goto :goto_3

    .line 2623632
    :cond_8
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2623633
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    .line 2623634
    iput-object v0, v2, LX/Itn;->h:Ljava/lang/String;

    .line 2623635
    goto :goto_4

    .line 2623636
    :cond_9
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2623637
    iput-object v0, v2, LX/Itn;->i:Ljava/util/List;

    .line 2623638
    :cond_a
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/DgF;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2623639
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/Ite;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)LX/6mT;

    move-result-object v0

    .line 2623640
    iput-object v0, v2, LX/Itn;->l:LX/6mT;

    .line 2623641
    :cond_b
    sget-object v0, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    if-eq p2, v0, :cond_c

    .line 2623642
    invoke-virtual {p2}, Lcom/facebook/fbtrace/FbTraceNode;->a()Ljava/lang/String;

    move-result-object v0

    .line 2623643
    iput-object v0, v2, LX/Itn;->j:Ljava/lang/String;

    .line 2623644
    :cond_c
    iput-object p3, v2, LX/Itn;->n:Ljava/lang/Integer;

    .line 2623645
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    if-eqz v0, :cond_10

    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    .line 2623646
    :goto_7
    iget-object v4, v3, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-static {v4}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v4

    .line 2623647
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2623648
    if-eqz v3, :cond_18

    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    const/4 p0, 0x3

    new-array p0, p0, [Ljava/lang/String;

    const-string p1, "montage_composition"

    aput-object p1, p0, v6

    const-string p1, "montage_camera"

    aput-object p1, p0, v5

    const/4 p1, 0x2

    const-string p2, "montage_media_library"

    aput-object p2, p0, p1

    invoke-static {v7, p0}, LX/0YN;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_18

    :goto_8
    move v5, v5

    .line 2623649
    if-nez v0, :cond_d

    if-nez v5, :cond_d

    if-eqz v4, :cond_13

    .line 2623650
    :cond_d
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    .line 2623651
    if-eqz v0, :cond_e

    .line 2623652
    iget-object v0, v3, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-static {v0}, LX/5dt;->b(LX/0P1;)Landroid/util/Pair;

    move-result-object v0

    .line 2623653
    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v6, v7, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2623654
    :cond_e
    if-eqz v5, :cond_f

    .line 2623655
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2623656
    :try_start_0
    const-string v5, "message_source"

    iget-object v7, v3, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v0, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623657
    const-string v5, "message_source_data"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v5, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2623658
    :cond_f
    if-eqz v4, :cond_12

    .line 2623659
    new-instance v4, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2623660
    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_9
    if-ge v1, v5, :cond_11

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ProfileRange;

    .line 2623661
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ProfileRange;->a()LX/0m9;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 2623662
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_10
    move v0, v1

    .line 2623663
    goto :goto_7

    .line 2623664
    :catch_0
    move-exception v0

    .line 2623665
    new-instance v1, LX/Itq;

    sget-object v2, LX/Itv;->SEND_SKIPPED_JSON_EXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v0, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Itq;-><init>(LX/Itx;)V

    throw v1

    .line 2623666
    :cond_11
    const-string v0, "prng"

    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2623667
    :cond_12
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2623668
    iput-object v0, v2, LX/Itn;->o:Ljava/util/Map;

    .line 2623669
    :cond_13
    invoke-virtual {v2}, LX/Itn;->a()LX/6mb;

    move-result-object v0

    return-object v0

    :cond_14
    move v0, v4

    .line 2623670
    goto/16 :goto_5

    .line 2623671
    :cond_15
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2623672
    iget-object v6, v3, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    :goto_a
    if-ge v4, v7, :cond_17

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2623673
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_16

    .line 2623674
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623675
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 2623676
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_17
    move-object v0, v5

    .line 2623677
    goto/16 :goto_6

    :cond_18
    move v5, v6

    goto/16 :goto_8
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;LX/ItY;Ljava/lang/Integer;)[B
    .locals 2
    .param p4    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2623589
    sget-object v0, LX/Itd;->a:[I

    invoke-virtual {p3}, LX/ItY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2623590
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported send-message serialization protocol"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2623591
    :pswitch_0
    invoke-direct {p0, p1, p2, p4}, LX/Ite;->a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;)[B

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/util/List;Lcom/facebook/fbtrace/FbTraceNode;LX/ItY;Ljava/lang/Integer;J)[B
    .locals 7
    .param p4    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            "LX/ItY;",
            "Ljava/lang/Integer;",
            "J)[B"
        }
    .end annotation

    .prologue
    .line 2623586
    sget-object v0, LX/Itd;->a:[I

    invoke-virtual {p3}, LX/ItY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2623587
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported send-message serialization protocol"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-wide v4, p5

    .line 2623588
    invoke-direct/range {v0 .. v5}, LX/Ite;->a(Ljava/util/List;Lcom/facebook/fbtrace/FbTraceNode;Ljava/lang/Integer;J)[B

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
