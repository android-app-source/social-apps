.class public final LX/IP5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V
    .locals 0

    .prologue
    .line 2573702
    iput-object p1, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3430ee31    # -2.7141022E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573703
    iget-object v1, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    .line 2573704
    new-instance v4, LX/2qr;

    iget-object v5, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->q:Landroid/view/View;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, LX/2qr;-><init>(Landroid/view/View;I)V

    .line 2573705
    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, LX/2qr;->setDuration(J)V

    .line 2573706
    invoke-virtual {v1, v4}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2573707
    iget-object v1, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v2, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2573708
    if-eqz v2, :cond_0

    .line 2573709
    new-instance v4, LX/4Fn;

    invoke-direct {v4}, LX/4Fn;-><init>()V

    iget-object v5, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->g:Ljava/lang/String;

    .line 2573710
    const-string v6, "actor_id"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573711
    move-object v4, v4

    .line 2573712
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 2573713
    const-string v6, "group_id"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573714
    move-object v4, v4

    .line 2573715
    invoke-virtual {v2}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 2573716
    const-string v6, "reason"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573717
    move-object v4, v4

    .line 2573718
    const-string v5, "suggestion"

    .line 2573719
    const-string v6, "source_type"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573720
    move-object v4, v4

    .line 2573721
    new-instance v5, LX/IOG;

    invoke-direct {v5}, LX/IOG;-><init>()V

    move-object v5, v5

    .line 2573722
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2573723
    iget-object v4, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->h:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2573724
    :cond_0
    iget-object v1, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->t:LX/IPB;

    iget-object v2, p0, LX/IP5;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2573725
    if-eqz v2, :cond_1

    .line 2573726
    iget-object v4, v1, LX/IPB;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v4, v4, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    invoke-virtual {v4, v2}, LX/IPA;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;)V

    .line 2573727
    iget-object v4, v1, LX/IPB;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v4, v4, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    invoke-virtual {v4}, LX/IPA;->a()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2573728
    iget-object v4, v1, LX/IPB;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    invoke-static {v4}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->d(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    .line 2573729
    :cond_1
    const v1, -0x50dbab87

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
