.class public final LX/I0A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final synthetic a:LX/I0B;


# direct methods
.method public constructor <init>(LX/I0B;)V
    .locals 0

    .prologue
    .line 2526599
    iput-object p1, p0, LX/I0A;->a:LX/I0B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2526600
    const-string v0, "birthday_view_start_date"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2526601
    if-eqz v0, :cond_0

    const-string v1, "unset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2526602
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2526603
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2526604
    :goto_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2526605
    iget-object v0, p0, LX/I0A;->a:LX/I0B;

    iget-object v0, v0, LX/I0B;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2526606
    const-string v0, "force_tabbed_dashboard"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2526607
    const-string v0, "birthday_view_start_date"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2526608
    const-string v0, "birthday_view_referrer_param"

    const-string v1, "notification"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2526609
    const-string v0, "target_fragment"

    sget-object v1, LX/0cQ;->EVENTS_DASHBOARD_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2526610
    const-string v0, "section_name"

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2526611
    return-object v2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
