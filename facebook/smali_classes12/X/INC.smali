.class public final LX/INC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsInterfaces$CommunityGroupsSearchQuery;",
        "LX/7Hc",
        "<",
        "LX/INB;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7B6;

.field public final synthetic b:LX/IND;


# direct methods
.method public constructor <init>(LX/IND;LX/7B6;)V
    .locals 0

    .prologue
    .line 2570640
    iput-object p1, p0, LX/INC;->b:LX/IND;

    iput-object p2, p0, LX/INC;->a:LX/7B6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2570641
    check-cast p1, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    .line 2570642
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2570643
    invoke-virtual {p1}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    .line 2570644
    if-nez v0, :cond_0

    .line 2570645
    new-instance v0, LX/7Hc;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    .line 2570646
    :goto_0
    return-object v0

    .line 2570647
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2570648
    if-eqz v0, :cond_1

    .line 2570649
    new-instance v6, LX/INB;

    iget-object v1, p0, LX/INC;->b:LX/IND;

    iget-object v7, v1, LX/IND;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    iget-object v1, p0, LX/INC;->a:LX/7B6;

    check-cast v1, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2570650
    iget-object p1, v1, LX/7B6;->b:Ljava/lang/String;

    move-object v1, p1

    .line 2570651
    invoke-direct {v6, v0, v7, v1}, LX/INB;-><init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570652
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2570653
    :cond_2
    new-instance v0, LX/7Hc;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    goto :goto_0
.end method
