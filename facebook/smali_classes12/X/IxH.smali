.class public LX/IxH;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IxF;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IxJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2631396
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IxH;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IxJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631433
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2631434
    iput-object p1, p0, LX/IxH;->b:LX/0Ot;

    .line 2631435
    return-void
.end method

.method public static a(LX/0QB;)LX/IxH;
    .locals 4

    .prologue
    .line 2631422
    const-class v1, LX/IxH;

    monitor-enter v1

    .line 2631423
    :try_start_0
    sget-object v0, LX/IxH;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2631424
    sput-object v2, LX/IxH;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2631425
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631426
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2631427
    new-instance v3, LX/IxH;

    const/16 p0, 0x2c69

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IxH;-><init>(LX/0Ot;)V

    .line 2631428
    move-object v0, v3

    .line 2631429
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2631430
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IxH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631431
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2631432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;ZLX/1X1;)V
    .locals 11

    .prologue
    .line 2631417
    iget-object v0, p0, LX/IxH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IxJ;

    const/4 v4, 0x0

    .line 2631418
    iget-object v2, v0, LX/IxJ;->e:LX/0if;

    sget-object v3, LX/0ig;->ai:LX/0ih;

    if-eqz p3, :cond_0

    sget-object v1, LX/IxE;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v3, v1, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2631419
    iget-object v1, v0, LX/IxJ;->d:LX/961;

    const-string v5, "reaction_dialog"

    const/4 v9, 0x1

    new-instance v10, LX/IxI;

    invoke-direct {v10, v0, p3, p2}, LX/IxI;-><init>(LX/IxJ;ZLjava/lang/String;)V

    move-object v2, p2

    move v3, p3

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v1 .. v10}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2631420
    return-void

    .line 2631421
    :cond_0
    sget-object v1, LX/IxE;->b:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2631403
    check-cast p2, LX/IxG;

    .line 2631404
    iget-object v0, p0, LX/IxH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IxJ;

    iget-object v1, p2, LX/IxG;->a:LX/IxK;

    const/4 v4, 0x0

    const/4 p2, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 p0, 0x2

    .line 2631405
    iget-object v2, v0, LX/IxJ;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/154;

    .line 2631406
    invoke-interface {v1}, LX/IxK;->e()Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v2, ""

    .line 2631407
    :goto_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0a0a29

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 2631408
    invoke-interface {v1}, LX/IxK;->c()Z

    move-result v8

    .line 2631409
    if-eqz v8, :cond_2

    sget-object v3, LX/IxJ;->b:[I

    :goto_1
    invoke-virtual {v7}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v9

    invoke-virtual {v7, v3, v9}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v7

    .line 2631410
    invoke-interface {v1}, LX/IxK;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, LX/IxK;->d()Ljava/lang/String;

    move-result-object v4

    if-nez v8, :cond_3

    move v3, v5

    .line 2631411
    :goto_2
    const v8, 0x32fe8178

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    const/4 v10, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v9, v10

    invoke-static {p1, v8, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v4, v8

    .line 2631412
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v8

    const v9, 0x7f02156f

    invoke-virtual {v8, v9}, LX/1nm;->h(I)LX/1nm;

    move-result-object v8

    invoke-interface {v3, v8}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v8, 0x7f0b004e

    invoke-virtual {v2, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v8, 0x7f0a00a8

    invoke-virtual {v2, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v2, v8}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const/16 v8, 0x8

    const v9, 0x7f0b0061

    invoke-interface {v2, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v8, 0x7f0a009f

    invoke-virtual {v3, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    const/4 v8, 0x7

    const v9, 0x7f0b0061

    invoke-interface {v3, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v8, 0x4

    invoke-interface {v3, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v8, v0, LX/IxJ;->c:LX/1vg;

    invoke-virtual {v8, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    const v9, 0x7f0208fa

    invoke-virtual {v8, v9}, LX/2xv;->h(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/2xv;->i(I)LX/2xv;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v3

    sget-object v7, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v7}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, v5, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0060

    invoke-interface {v3, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0061

    invoke-interface {v3, v6, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0061

    invoke-interface {v3, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2631413
    return-object v0

    .line 2631414
    :cond_1
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0f0160

    invoke-interface {v1}, LX/IxK;->e()Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;->a()I

    move-result v8

    new-array v9, p0, [Ljava/lang/Object;

    invoke-interface {v1}, LX/IxK;->e()Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;->a()I

    move-result v10

    invoke-virtual {v2, v10}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v5

    invoke-virtual {v3, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    move-object v3, v4

    .line 2631415
    goto/16 :goto_1

    :cond_3
    move v3, v6

    .line 2631416
    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2631397
    invoke-static {}, LX/1dS;->b()V

    .line 2631398
    iget v0, p1, LX/1dQ;->b:I

    .line 2631399
    packed-switch v0, :pswitch_data_0

    .line 2631400
    :goto_0
    return-object v4

    .line 2631401
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2631402
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v2, v0, v1, v3}, LX/IxH;->a(Landroid/view/View;Ljava/lang/String;ZLX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x32fe8178
        :pswitch_0
    .end packed-switch
.end method
