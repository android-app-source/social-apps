.class public final enum LX/IdV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IdV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IdV;

.field public static final enum ALLOW:LX/IdV;

.field public static final enum CANCEL:LX/IdV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2596885
    new-instance v0, LX/IdV;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v2}, LX/IdV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IdV;->CANCEL:LX/IdV;

    new-instance v0, LX/IdV;

    const-string v1, "ALLOW"

    invoke-direct {v0, v1, v3}, LX/IdV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IdV;->ALLOW:LX/IdV;

    const/4 v0, 0x2

    new-array v0, v0, [LX/IdV;

    sget-object v1, LX/IdV;->CANCEL:LX/IdV;

    aput-object v1, v0, v2

    sget-object v1, LX/IdV;->ALLOW:LX/IdV;

    aput-object v1, v0, v3

    sput-object v0, LX/IdV;->$VALUES:[LX/IdV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2596886
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IdV;
    .locals 1

    .prologue
    .line 2596887
    const-class v0, LX/IdV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IdV;

    return-object v0
.end method

.method public static values()[LX/IdV;
    .locals 1

    .prologue
    .line 2596888
    sget-object v0, LX/IdV;->$VALUES:[LX/IdV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IdV;

    return-object v0
.end method
