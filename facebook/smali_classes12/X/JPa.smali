.class public LX/JPa;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPa",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690042
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2690043
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPa;->b:LX/0Zi;

    .line 2690044
    iput-object p1, p0, LX/JPa;->a:LX/0Ot;

    .line 2690045
    return-void
.end method

.method public static a(LX/0QB;)LX/JPa;
    .locals 4

    .prologue
    .line 2690046
    const-class v1, LX/JPa;

    monitor-enter v1

    .line 2690047
    :try_start_0
    sget-object v0, LX/JPa;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690048
    sput-object v2, LX/JPa;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690049
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690050
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690051
    new-instance v3, LX/JPa;

    const/16 p0, 0x1fe4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPa;-><init>(LX/0Ot;)V

    .line 2690052
    move-object v0, v3

    .line 2690053
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690054
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690055
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2690022
    check-cast p2, LX/JPZ;

    .line 2690023
    iget-object v0, p0, LX/JPa;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;

    iget-object v1, p2, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2690024
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b257f

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2580

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020a3d

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 2690025
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b258c

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    .line 2690026
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2690027
    iget-object v4, v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->a:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    sget-object p0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->v()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    sget-object p0, LX/1Up;->c:LX/1Up;

    invoke-virtual {v4, p0}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b257f

    invoke-interface {v4, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b2593

    invoke-interface {v4, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 p0, 0x1

    const p2, 0x7f0b2579

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x3

    const p2, 0x7f0b257b

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2690028
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2690029
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a010c

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b2582

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/4 p0, 0x6

    invoke-virtual {v4, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/16 p0, 0x8

    const p2, 0x7f0b2571

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2690030
    :cond_1
    move-object v3, v3

    .line 2690031
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/JPK;->a(LX/1De;)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v0, 0x7

    const/4 v4, 0x2

    const/4 p2, 0x1

    .line 2690032
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    .line 2690033
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2690034
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a010e

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b2582

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f0b2571

    invoke-interface {v4, v0, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2690035
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s()I

    move-result v4

    if-lez v4, :cond_3

    .line 2690036
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->s()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const p0, 0x7f0a00d5

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const p0, 0x7f0b2589

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const p0, 0x7f020cd4

    invoke-interface {v4, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const/4 p0, 0x6

    const p2, 0x7f0b257b

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x0

    const p2, 0x7f0b2571

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b2571

    invoke-interface {v4, v0, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2690037
    :cond_3
    move-object v3, v3

    .line 2690038
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b2581

    invoke-interface {v2, v3, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    .line 2690039
    const v3, -0x46cf44cf

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2690040
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2690041
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2690005
    invoke-static {}, LX/1dS;->b()V

    .line 2690006
    iget v0, p1, LX/1dQ;->b:I

    .line 2690007
    packed-switch v0, :pswitch_data_0

    .line 2690008
    :goto_0
    return-object v2

    .line 2690009
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2690010
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2690011
    check-cast v1, LX/JPZ;

    .line 2690012
    iget-object v3, p0, LX/JPa;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;

    iget-object v4, v1, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2690013
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    .line 2690014
    if-nez v6, :cond_1

    if-eqz v4, :cond_1

    .line 2690015
    iget-object v6, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 2690016
    check-cast v6, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    move-object p1, v6

    .line 2690017
    :goto_1
    if-nez p1, :cond_0

    .line 2690018
    iget-object v6, v3, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/JPN;

    const-string p1, "Cannot open profile without GraphQLPage"

    invoke-virtual {v6, v4, v5, p1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2690019
    :goto_2
    goto :goto_0

    .line 2690020
    :cond_0
    iget-object v6, v3, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/JPN;

    invoke-virtual {v6, v4, v5}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2690021
    iget-object v6, v3, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->c:LX/17X;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, LX/0ax;->bf:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p0, p1}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {v6, p1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    :cond_1
    move-object p1, v6

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x46cf44cf
        :pswitch_0
    .end packed-switch
.end method
