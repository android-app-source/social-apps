.class public LX/HnT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6C8;

.field private b:LX/CKT;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6C8;LX/CKT;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6C8;",
            "LX/CKT;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2502387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2502388
    iput-object p1, p0, LX/HnT;->a:LX/6C8;

    .line 2502389
    iput-object p2, p0, LX/HnT;->b:LX/CKT;

    .line 2502390
    iput-object p3, p0, LX/HnT;->c:LX/0Ot;

    .line 2502391
    iput-object p4, p0, LX/HnT;->d:LX/0Ot;

    .line 2502392
    return-void
.end method

.method public static a(LX/0QB;)LX/HnT;
    .locals 5

    .prologue
    .line 2502393
    new-instance v2, LX/HnT;

    invoke-static {p0}, LX/6C8;->b(LX/0QB;)LX/6C8;

    move-result-object v0

    check-cast v0, LX/6C8;

    invoke-static {p0}, LX/CKT;->b(LX/0QB;)LX/CKT;

    move-result-object v1

    check-cast v1, LX/CKT;

    const/16 v3, 0x3be

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2978

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v2, v0, v1, v3, v4}, LX/HnT;-><init>(LX/6C8;LX/CKT;LX/0Ot;LX/0Ot;)V

    .line 2502394
    move-object v0, v2

    .line 2502395
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/util/Map;)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2502396
    const-string v0, "action"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "url"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "url"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2502397
    :goto_0
    return v0

    .line 2502398
    :cond_1
    const-string v0, "action"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2502399
    const-string v0, "url"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2502400
    const-string v4, "SHARE_MESSENGER"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2502401
    const/4 v8, 0x1

    .line 2502402
    iget-object v5, p0, LX/HnT;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/B9y;

    const-string v10, "browser"

    move-object v6, p1

    move-object v7, v0

    move v9, v8

    invoke-virtual/range {v5 .. v10}, LX/B9y;->a(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 2502403
    move v0, v1

    .line 2502404
    goto :goto_0

    .line 2502405
    :cond_2
    const-string v4, "SHARE_TIMELINE"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2502406
    sget-object v2, LX/21D;->IN_APP_BROWSER:LX/21D;

    const-string v3, "shareAsNewPost"

    invoke-static {v0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    sget-object v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 2502407
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2502408
    iget-object v2, p0, LX/HnT;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-interface {v2, v4, v3, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2502409
    move v0, v1

    .line 2502410
    goto :goto_0

    .line 2502411
    :cond_3
    const-string v4, "SAVE_LINK"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2502412
    iget-object v2, p0, LX/HnT;->a:LX/6C8;

    .line 2502413
    iget-object p0, v2, LX/6C8;->d:LX/6C7;

    .line 2502414
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, LX/6C7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/6C7;->sendMessage(Landroid/os/Message;)Z

    .line 2502415
    move v0, v1

    .line 2502416
    goto :goto_0

    .line 2502417
    :cond_4
    const-string v4, "COPY_LINK"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2502418
    iget-object v2, p0, LX/HnT;->a:LX/6C8;

    .line 2502419
    iget-object p0, v2, LX/6C8;->d:LX/6C7;

    .line 2502420
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v0}, LX/6C7;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/6C7;->sendMessage(Landroid/os/Message;)Z

    .line 2502421
    move v0, v1

    .line 2502422
    goto/16 :goto_0

    .line 2502423
    :cond_5
    const-string v0, "MESSENGER_CONTENT_SUBSCRIBE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2502424
    const-string v0, "id"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 2502425
    goto/16 :goto_0

    .line 2502426
    :cond_6
    const-string v0, "id"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2502427
    iget-object v2, p0, LX/HnT;->b:LX/CKT;

    const-string v3, "browser"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, LX/CKT;->a(Ljava/lang/String;Ljava/lang/String;LX/CKR;)V

    move v0, v1

    .line 2502428
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 2502429
    goto/16 :goto_0
.end method
