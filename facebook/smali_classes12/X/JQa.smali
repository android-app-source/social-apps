.class public final LX/JQa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25K;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

.field public final synthetic b:LX/JQb;


# direct methods
.method public constructor <init>(LX/JQb;Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)V
    .locals 0

    .prologue
    .line 2691962
    iput-object p1, p0, LX/JQa;->b:LX/JQb;

    iput-object p2, p0, LX/JQa;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 2

    .prologue
    .line 2691963
    invoke-virtual {p2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    .line 2691964
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2691965
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLJobOpening;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/17Q;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2691966
    iget-object v1, p0, LX/JQa;->b:LX/JQb;

    iget-object v1, v1, LX/JQb;->e:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2691967
    :cond_0
    iget-object v0, p0, LX/JQa;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-static {v0, p2, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 2691968
    return-void
.end method
