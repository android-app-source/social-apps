.class public final LX/HS6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2466540
    iput-object p1, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    iput-wide p2, p0, LX/HS6;->a:J

    iput-object p4, p0, LX/HS6;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x1efa44f9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2466535
    iget-object v1, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    iget-wide v2, p0, LX/HS6;->a:J

    invoke-virtual {v1, v2, v3}, LX/HS4;->a(J)V

    .line 2466536
    iget-object v1, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->h:LX/17Y;

    iget-object v2, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/HS6;->b:Ljava/lang/String;

    iget-wide v4, p0, LX/HS6;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2466537
    if-eqz v1, :cond_0

    .line 2466538
    iget-object v2, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    iget-object v2, v2, LX/HS4;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HS6;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2466539
    :cond_0
    const v1, -0x4b339d5b

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
