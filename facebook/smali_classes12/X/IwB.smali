.class public final enum LX/IwB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IwB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IwB;

.field public static final enum HEADER:LX/IwB;

.field public static final enum LOADING:LX/IwB;

.field public static final enum MORE:LX/IwB;

.field public static final enum PAGE:LX/IwB;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2629452
    new-instance v0, LX/IwB;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/IwB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IwB;->HEADER:LX/IwB;

    .line 2629453
    new-instance v0, LX/IwB;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v3}, LX/IwB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IwB;->PAGE:LX/IwB;

    .line 2629454
    new-instance v0, LX/IwB;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/IwB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IwB;->LOADING:LX/IwB;

    .line 2629455
    new-instance v0, LX/IwB;

    const-string v1, "MORE"

    invoke-direct {v0, v1, v5}, LX/IwB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IwB;->MORE:LX/IwB;

    .line 2629456
    const/4 v0, 0x4

    new-array v0, v0, [LX/IwB;

    sget-object v1, LX/IwB;->HEADER:LX/IwB;

    aput-object v1, v0, v2

    sget-object v1, LX/IwB;->PAGE:LX/IwB;

    aput-object v1, v0, v3

    sget-object v1, LX/IwB;->LOADING:LX/IwB;

    aput-object v1, v0, v4

    sget-object v1, LX/IwB;->MORE:LX/IwB;

    aput-object v1, v0, v5

    sput-object v0, LX/IwB;->$VALUES:[LX/IwB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2629451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IwB;
    .locals 1

    .prologue
    .line 2629457
    const-class v0, LX/IwB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IwB;

    return-object v0
.end method

.method public static values()[LX/IwB;
    .locals 1

    .prologue
    .line 2629450
    sget-object v0, LX/IwB;->$VALUES:[LX/IwB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IwB;

    return-object v0
.end method
