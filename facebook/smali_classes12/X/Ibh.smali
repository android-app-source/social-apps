.class public final LX/Ibh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 2594441
    const/16 v17, 0x0

    .line 2594442
    const/16 v16, 0x0

    .line 2594443
    const/4 v15, 0x0

    .line 2594444
    const/4 v14, 0x0

    .line 2594445
    const/4 v13, 0x0

    .line 2594446
    const/4 v12, 0x0

    .line 2594447
    const-wide/16 v10, 0x0

    .line 2594448
    const/4 v9, 0x0

    .line 2594449
    const/4 v8, 0x0

    .line 2594450
    const/4 v7, 0x0

    .line 2594451
    const/4 v6, 0x0

    .line 2594452
    const/4 v5, 0x0

    .line 2594453
    const/4 v4, 0x0

    .line 2594454
    const/4 v3, 0x0

    .line 2594455
    const/4 v2, 0x0

    .line 2594456
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_12

    .line 2594457
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2594458
    const/4 v2, 0x0

    .line 2594459
    :goto_0
    return v2

    .line 2594460
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_f

    .line 2594461
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2594462
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2594463
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 2594464
    const-string v6, "__type__"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "__typename"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2594465
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 2594466
    :cond_2
    const-string v6, "current_location"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2594467
    invoke-static/range {p0 .. p1}, LX/Ibe;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2594468
    :cond_3
    const-string v6, "destination_location"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2594469
    invoke-static/range {p0 .. p1}, LX/Ibf;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2594470
    :cond_4
    const-string v6, "driver_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2594471
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 2594472
    :cond_5
    const-string v6, "driver_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2594473
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 2594474
    :cond_6
    const-string v6, "driver_phone"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2594475
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto/16 :goto_1

    .line 2594476
    :cond_7
    const-string v6, "driver_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2594477
    const/4 v2, 0x1

    .line 2594478
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2594479
    :cond_8
    const-string v6, "eta_in_minutes"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2594480
    const/4 v2, 0x1

    .line 2594481
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v14, v6

    goto/16 :goto_1

    .line 2594482
    :cond_9
    const-string v6, "ride_status"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2594483
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2594484
    :cond_a
    const-string v6, "source_location"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2594485
    invoke-static/range {p0 .. p1}, LX/Ibg;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2594486
    :cond_b
    const-string v6, "vehicle_make"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2594487
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 2594488
    :cond_c
    const-string v6, "vehicle_model_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2594489
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2594490
    :cond_d
    const-string v6, "vehicle_plate"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2594491
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 2594492
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2594493
    :cond_f
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2594494
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2594495
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2594496
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2594497
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2594498
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2594499
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2594500
    if-eqz v3, :cond_10

    .line 2594501
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2594502
    :cond_10
    if-eqz v8, :cond_11

    .line 2594503
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v3}, LX/186;->a(III)V

    .line 2594504
    :cond_11
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2594505
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2594506
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2594507
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2594508
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2594509
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move/from16 v18, v16

    move/from16 v19, v17

    move/from16 v16, v14

    move/from16 v17, v15

    move v14, v9

    move v15, v13

    move v13, v8

    move v9, v4

    move v8, v2

    move/from16 v21, v7

    move v7, v12

    move/from16 v12, v21

    move/from16 v22, v5

    move-wide v4, v10

    move v11, v6

    move/from16 v10, v22

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2594510
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594511
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2594512
    if-eqz v0, :cond_0

    .line 2594513
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594514
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2594515
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2594516
    if-eqz v0, :cond_3

    .line 2594517
    const-string v1, "current_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594518
    const-wide/16 v10, 0x0

    .line 2594519
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594520
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594521
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_1

    .line 2594522
    const-string v8, "latitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594523
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594524
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594525
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_2

    .line 2594526
    const-string v8, "longitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594527
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594528
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594529
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2594530
    if-eqz v0, :cond_6

    .line 2594531
    const-string v1, "destination_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594532
    const-wide/16 v10, 0x0

    .line 2594533
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594534
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594535
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_4

    .line 2594536
    const-string v8, "latitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594537
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594538
    :cond_4
    const/4 v6, 0x1

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594539
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_5

    .line 2594540
    const-string v8, "longitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594541
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594542
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594543
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594544
    if-eqz v0, :cond_7

    .line 2594545
    const-string v1, "driver_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594546
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594547
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594548
    if-eqz v0, :cond_8

    .line 2594549
    const-string v1, "driver_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594550
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594551
    :cond_8
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594552
    if-eqz v0, :cond_9

    .line 2594553
    const-string v1, "driver_phone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594554
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594555
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2594556
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 2594557
    const-string v2, "driver_rating"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594558
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2594559
    :cond_a
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2594560
    if-eqz v0, :cond_b

    .line 2594561
    const-string v1, "eta_in_minutes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594562
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2594563
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594564
    if-eqz v0, :cond_c

    .line 2594565
    const-string v1, "ride_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594566
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594567
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2594568
    if-eqz v0, :cond_f

    .line 2594569
    const-string v1, "source_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594570
    const-wide/16 v10, 0x0

    .line 2594571
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594572
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594573
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_d

    .line 2594574
    const-string v8, "latitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594575
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594576
    :cond_d
    const/4 v6, 0x1

    invoke-virtual {p0, v0, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2594577
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_e

    .line 2594578
    const-string v8, "longitude"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594579
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2594580
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594581
    :cond_f
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594582
    if-eqz v0, :cond_10

    .line 2594583
    const-string v1, "vehicle_make"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594584
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594585
    :cond_10
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594586
    if-eqz v0, :cond_11

    .line 2594587
    const-string v1, "vehicle_model_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594588
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594589
    :cond_11
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2594590
    if-eqz v0, :cond_12

    .line 2594591
    const-string v1, "vehicle_plate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594592
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2594593
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594594
    return-void
.end method
