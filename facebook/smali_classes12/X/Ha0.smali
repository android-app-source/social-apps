.class public LX/Ha0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public code:I

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2483028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483029
    iput p1, p0, LX/Ha0;->code:I

    .line 2483030
    iput-object p2, p0, LX/Ha0;->message:Ljava/lang/String;

    .line 2483031
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 2483032
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, LX/Ha0;->code:I

    .line 2483033
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Ha0;->message:Ljava/lang/String;

    .line 2483034
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 2483025
    iget v0, p0, LX/Ha0;->code:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 2483026
    iget-object v0, p0, LX/Ha0;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 2483027
    return-void
.end method
