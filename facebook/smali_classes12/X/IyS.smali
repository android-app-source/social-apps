.class public final LX/IyS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/IyK;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/currency/CurrencyAmount;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 1

    .prologue
    .line 2633315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633316
    const/4 v0, 0x1

    iput v0, p0, LX/IyS;->g:I

    .line 2633317
    const v0, 0x7fffffff

    iput v0, p0, LX/IyS;->h:I

    .line 2633318
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2633319
    iput-object v0, p0, LX/IyS;->i:LX/0Px;

    .line 2633320
    iput-object p1, p0, LX/IyS;->a:LX/IyK;

    .line 2633321
    iput-object p2, p0, LX/IyS;->b:Ljava/lang/String;

    .line 2633322
    iput-object p3, p0, LX/IyS;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2633323
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/cart/model/SimpleCartItem;)LX/IyS;
    .locals 1

    .prologue
    .line 2633324
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2633325
    iput-object v0, p0, LX/IyS;->d:Ljava/lang/String;

    .line 2633326
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2633327
    iput-object v0, p0, LX/IyS;->e:Ljava/lang/String;

    .line 2633328
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2633329
    iput-object v0, p0, LX/IyS;->f:Ljava/lang/String;

    .line 2633330
    iget v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    move v0, v0

    .line 2633331
    iput v0, p0, LX/IyS;->g:I

    .line 2633332
    iget v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->h:I

    move v0, v0

    .line 2633333
    iput v0, p0, LX/IyS;->h:I

    .line 2633334
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->i:LX/0Px;

    move-object v0, v0

    .line 2633335
    iput-object v0, p0, LX/IyS;->i:LX/0Px;

    .line 2633336
    return-object p0
.end method

.method public final a()Lcom/facebook/payments/cart/model/SimpleCartItem;
    .locals 2

    .prologue
    .line 2633337
    new-instance v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    invoke-direct {v0, p0}, Lcom/facebook/payments/cart/model/SimpleCartItem;-><init>(LX/IyS;)V

    return-object v0
.end method
