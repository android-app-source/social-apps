.class public final LX/HIO;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/HIQ;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/HIP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HIQ",
            "<TE;>.HScrollPageCardComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/HIQ;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/HIQ;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2451066
    iput-object p1, p0, LX/HIO;->b:LX/HIQ;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2451067
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "hScrollPageCardFields"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/HIO;->c:[Ljava/lang/String;

    .line 2451068
    iput v3, p0, LX/HIO;->d:I

    .line 2451069
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/HIO;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/HIO;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/HIO;LX/1De;IILX/HIP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/HIQ",
            "<TE;>.HScrollPageCardComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2451062
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2451063
    iput-object p4, p0, LX/HIO;->a:LX/HIP;

    .line 2451064
    iget-object v0, p0, LX/HIO;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2451065
    return-void
.end method


# virtual methods
.method public final a(LX/2km;)LX/HIO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451059
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput-object p1, v0, LX/HIP;->a:LX/2km;

    .line 2451060
    iget-object v0, p0, LX/HIO;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2451061
    return-object p0
.end method

.method public final a(LX/HIS;)LX/HIO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HIS;",
            ")",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451057
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput-object p1, v0, LX/HIP;->e:LX/HIS;

    .line 2451058
    return-object p0
.end method

.method public final a(LX/HIT;)LX/HIO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HIT;",
            ")",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451070
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput-object p1, v0, LX/HIP;->d:LX/HIT;

    .line 2451071
    return-object p0
.end method

.method public final a(LX/HIU;)LX/HIO;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HIU;",
            ")",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451055
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput-object p1, v0, LX/HIP;->f:LX/HIU;

    .line 2451056
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ")",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451036
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput-object p1, v0, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2451037
    iget-object v0, p0, LX/HIO;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2451038
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2451051
    invoke-super {p0}, LX/1X5;->a()V

    .line 2451052
    const/4 v0, 0x0

    iput-object v0, p0, LX/HIO;->a:LX/HIP;

    .line 2451053
    iget-object v0, p0, LX/HIO;->b:LX/HIQ;

    iget-object v0, v0, LX/HIQ;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2451054
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/HIQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2451041
    iget-object v1, p0, LX/HIO;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HIO;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/HIO;->d:I

    if-ge v1, v2, :cond_2

    .line 2451042
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2451043
    :goto_0
    iget v2, p0, LX/HIO;->d:I

    if-ge v0, v2, :cond_1

    .line 2451044
    iget-object v2, p0, LX/HIO;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2451045
    iget-object v2, p0, LX/HIO;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2451046
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2451047
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2451048
    :cond_2
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    .line 2451049
    invoke-virtual {p0}, LX/HIO;->a()V

    .line 2451050
    return-object v0
.end method

.method public final h(I)LX/HIO;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/Px;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2451039
    iget-object v0, p0, LX/HIO;->a:LX/HIP;

    iput p1, v0, LX/HIP;->c:I

    .line 2451040
    return-object p0
.end method
