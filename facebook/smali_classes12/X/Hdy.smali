.class public LX/Hdy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hdw;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hdz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2489492
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hdy;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hdz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489493
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2489494
    iput-object p1, p0, LX/Hdy;->b:LX/0Ot;

    .line 2489495
    return-void
.end method

.method public static a(LX/0QB;)LX/Hdy;
    .locals 4

    .prologue
    .line 2489496
    const-class v1, LX/Hdy;

    monitor-enter v1

    .line 2489497
    :try_start_0
    sget-object v0, LX/Hdy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2489498
    sput-object v2, LX/Hdy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2489499
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489500
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2489501
    new-instance v3, LX/Hdy;

    const/16 p0, 0x3722

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hdy;-><init>(LX/0Ot;)V

    .line 2489502
    move-object v0, v3

    .line 2489503
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2489504
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hdy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2489505
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2489506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2489507
    check-cast p2, LX/Hdx;

    .line 2489508
    iget-object v0, p0, LX/Hdy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hdz;

    iget-object v1, p2, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    const/4 p2, 0x0

    const/4 p0, 0x6

    const/4 v7, 0x3

    .line 2489509
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0058

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00ab

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v4, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->LIGHT:LX/0xr;

    invoke-static {p1, v4, v5, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f08366f

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a4

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v6, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {p1, v5, v6, p2}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x5

    const/4 v6, 0x4

    invoke-interface {v4, v5, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    .line 2489510
    iget-object v5, v0, LX/Hdz;->a:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f020a08

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    const v6, 0x7f0a008a

    invoke-virtual {v5, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    move-object v5, v5

    .line 2489511
    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0x12

    invoke-interface {v2, v3, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b22f6

    invoke-interface {v2, p0, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2489512
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2489513
    invoke-static {}, LX/1dS;->b()V

    .line 2489514
    const/4 v0, 0x0

    return-object v0
.end method
