.class public final enum LX/IJV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IJV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IJV;

.field public static final enum EXPANDED:LX/IJV;

.field public static final enum HEADER:LX/IJV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2563563
    new-instance v0, LX/IJV;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/IJV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJV;->HEADER:LX/IJV;

    .line 2563564
    new-instance v0, LX/IJV;

    const-string v1, "EXPANDED"

    invoke-direct {v0, v1, v3}, LX/IJV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJV;->EXPANDED:LX/IJV;

    .line 2563565
    const/4 v0, 0x2

    new-array v0, v0, [LX/IJV;

    sget-object v1, LX/IJV;->HEADER:LX/IJV;

    aput-object v1, v0, v2

    sget-object v1, LX/IJV;->EXPANDED:LX/IJV;

    aput-object v1, v0, v3

    sput-object v0, LX/IJV;->$VALUES:[LX/IJV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2563566
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IJV;
    .locals 1

    .prologue
    .line 2563562
    const-class v0, LX/IJV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IJV;

    return-object v0
.end method

.method public static values()[LX/IJV;
    .locals 1

    .prologue
    .line 2563561
    sget-object v0, LX/IJV;->$VALUES:[LX/IJV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IJV;

    return-object v0
.end method
