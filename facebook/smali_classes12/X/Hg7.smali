.class public LX/Hg7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hg5;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HgD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2492772
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hg7;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HgD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492769
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2492770
    iput-object p1, p0, LX/Hg7;->b:LX/0Ot;

    .line 2492771
    return-void
.end method

.method public static a(LX/0QB;)LX/Hg7;
    .locals 4

    .prologue
    .line 2492758
    const-class v1, LX/Hg7;

    monitor-enter v1

    .line 2492759
    :try_start_0
    sget-object v0, LX/Hg7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492760
    sput-object v2, LX/Hg7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492761
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492762
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492763
    new-instance v3, LX/Hg7;

    const/16 p0, 0x38d4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hg7;-><init>(LX/0Ot;)V

    .line 2492764
    move-object v0, v3

    .line 2492765
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492766
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hg7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492767
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492768
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2492708
    const v0, -0xfc6dcbb

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2492747
    check-cast p2, LX/Hg6;

    .line 2492748
    iget-object v0, p0, LX/Hg7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HgD;

    iget-object v1, p2, LX/Hg6;->a:LX/Hfw;

    const/4 p2, 0x4

    const/4 p0, 0x0

    const v8, -0x777778

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 2492749
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b2349

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b234c

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b2347

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f083711

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b2348

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    .line 2492750
    sget-object v4, LX/HgC;->a:[I

    invoke-virtual {v1}, LX/Hfw;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2492751
    const v4, 0x7f083713

    :goto_0
    move v4, v4

    .line 2492752
    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-static {p1}, LX/Hg7;->d(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v4, v0, LX/HgD;->a:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v5, 0x7f020a13

    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/2xv;->i(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b2353

    invoke-interface {v3, p0, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b2354

    invoke-interface {v3, v7, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/Hg7;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 2492753
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v5, 0x7f0a0940

    invoke-virtual {v4, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Di;->o(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a0940

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2492754
    return-object v0

    .line 2492755
    :pswitch_0
    const v4, 0x7f083712

    goto/16 :goto_0

    .line 2492756
    :pswitch_1
    const v4, 0x7f083714

    goto/16 :goto_0

    .line 2492757
    :pswitch_2
    const v4, 0x7f083715

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2492709
    invoke-static {}, LX/1dS;->b()V

    .line 2492710
    iget v0, p1, LX/1dQ;->b:I

    .line 2492711
    packed-switch v0, :pswitch_data_0

    .line 2492712
    :goto_0
    return-object v2

    .line 2492713
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2492714
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2492715
    check-cast v1, LX/Hg6;

    .line 2492716
    iget-object v3, p0, LX/Hg7;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HgD;

    iget-object v4, v1, LX/Hg6;->a:LX/Hfw;

    iget-object v5, v1, LX/Hg6;->b:LX/Hfg;

    .line 2492717
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v1, 0x1

    .line 2492718
    new-instance v7, LX/5OM;

    invoke-direct {v7, v6}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2492719
    invoke-virtual {v7}, LX/5OM;->c()LX/5OG;

    move-result-object v8

    .line 2492720
    iput-boolean v1, v7, LX/0ht;->w:Z

    .line 2492721
    iput-boolean v1, v7, LX/0ht;->x:Z

    .line 2492722
    const v9, 0x7f083712

    invoke-virtual {v8, v9}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v9

    .line 2492723
    invoke-interface {v9, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2492724
    const p1, 0x7f083713

    invoke-virtual {v8, p1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object p1

    .line 2492725
    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2492726
    const p2, 0x7f083714

    invoke-virtual {v8, p2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object p2

    .line 2492727
    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2492728
    const p0, 0x7f083715

    invoke-virtual {v8, p0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v8

    .line 2492729
    invoke-interface {v8, v1}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2492730
    sget-object p0, LX/Hfw;->RECENT:LX/Hfw;

    if-ne v4, p0, :cond_1

    .line 2492731
    invoke-interface {v9, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2492732
    :cond_0
    :goto_1
    new-instance p0, LX/Hg8;

    invoke-direct {p0, v3, v5}, LX/Hg8;-><init>(LX/HgD;LX/Hfg;)V

    invoke-interface {v9, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492733
    new-instance v9, LX/Hg9;

    invoke-direct {v9, v3, v5}, LX/Hg9;-><init>(LX/HgD;LX/Hfg;)V

    invoke-interface {p1, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492734
    new-instance v9, LX/HgA;

    invoke-direct {v9, v3, v5}, LX/HgA;-><init>(LX/HgD;LX/Hfg;)V

    invoke-interface {p2, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492735
    new-instance v9, LX/HgB;

    invoke-direct {v9, v3, v5}, LX/HgB;-><init>(LX/HgD;LX/Hfg;)V

    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2492736
    move-object v6, v7

    .line 2492737
    invoke-virtual {v6, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2492738
    iget-object v6, v3, LX/HgD;->b:LX/HgN;

    .line 2492739
    iget-object v7, v6, LX/HgN;->a:LX/0if;

    sget-object v8, LX/0ig;->af:LX/0ih;

    const-string v9, "filters_clicked"

    const/4 p1, 0x0

    iget-object p2, v6, LX/HgN;->b:LX/Hfw;

    invoke-static {p2}, LX/HgN;->c(LX/Hfw;)LX/1rQ;

    move-result-object p2

    invoke-virtual {v7, v8, v9, p1, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2492740
    goto/16 :goto_0

    .line 2492741
    :cond_1
    sget-object p0, LX/Hfw;->ALL:LX/Hfw;

    if-ne v4, p0, :cond_2

    .line 2492742
    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 2492743
    :cond_2
    sget-object p0, LX/Hfw;->FAVORITED:LX/Hfw;

    if-ne v4, p0, :cond_3

    .line 2492744
    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 2492745
    :cond_3
    sget-object p0, LX/Hfw;->TOP:LX/Hfw;

    if-ne v4, p0, :cond_0

    .line 2492746
    invoke-interface {v8, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0xfc6dcbb
        :pswitch_0
    .end packed-switch
.end method
