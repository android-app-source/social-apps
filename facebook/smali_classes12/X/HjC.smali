.class public final LX/HjC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/HjF;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:I

.field public i:I

.field private j:Z


# direct methods
.method public constructor <init>(LX/HjF;)V
    .locals 0

    iput-object p1, p0, LX/HjC;->a:LX/HjF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x607bd3e0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    iget-object v1, p0, LX/HjC;->a:LX/HjF;

    iget-object v1, v1, LX/HjF;->f:LX/Hj1;

    if-eqz v1, :cond_0

    :cond_0
    iget-boolean v1, p0, LX/HjC;->j:Z

    if-nez v1, :cond_1

    const-string v1, "FBAudienceNetworkLog"

    const-string v2, "No touch data recorded, please ensure touch events reach the ad View by returning false if you intercept the event."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "clickX"

    iget v3, p0, LX/HjC;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "clickY"

    iget v3, p0, LX/HjC;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "width"

    iget v3, p0, LX/HjC;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "height"

    iget v3, p0, LX/HjC;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "adPositionX"

    iget v3, p0, LX/HjC;->f:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "adPositionY"

    iget v3, p0, LX/HjC;->g:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "visibleWidth"

    iget v3, p0, LX/HjC;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "visibleHeight"

    iget v3, p0, LX/HjC;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v1

    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->s:LX/HjG;

    if-eqz v2, :cond_2

    const-string v2, "nti"

    iget-object v3, p0, LX/HjC;->a:LX/HjF;

    iget-object v3, v3, LX/HjF;->s:LX/HjG;

    invoke-virtual {v3}, LX/HjG;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-boolean v2, v2, LX/HjF;->t:Z

    if-eqz v2, :cond_3

    const-string v2, "nhs"

    iget-object v3, p0, LX/HjC;->a:LX/HjF;

    iget-boolean v3, v3, LX/HjF;->t:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v2, v1}, LX/Hjj;->b(Ljava/util/Map;)V

    const v1, 0x693f6cb1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->l:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iput v2, p0, LX/HjC;->d:I

    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, p0, LX/HjC;->e:I

    new-array v2, v5, [I

    iget-object v3, p0, LX/HjC;->a:LX/HjF;

    iget-object v3, v3, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationInWindow([I)V

    aget v3, v2, v1

    int-to-float v3, v3

    iput v3, p0, LX/HjC;->f:F

    aget v3, v2, v0

    int-to-float v3, v3

    iput v3, p0, LX/HjC;->g:F

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p0, LX/HjC;->a:LX/HjF;

    iget-object v4, v4, LX/HjF;->l:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, p0, LX/HjC;->i:I

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, p0, LX/HjC;->h:I

    new-array v3, v5, [I

    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    aget v5, v3, v1

    add-int/2addr v4, v5

    aget v5, v2, v1

    sub-int/2addr v4, v5

    iput v4, p0, LX/HjC;->b:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    aget v3, v3, v0

    add-int/2addr v3, v4

    aget v2, v2, v0

    sub-int v2, v3, v2

    iput v2, p0, LX/HjC;->c:I

    iput-boolean v0, p0, LX/HjC;->j:Z

    :cond_0
    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->n:Landroid/view/View$OnTouchListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/HjC;->a:LX/HjF;

    iget-object v2, v2, LX/HjF;->n:Landroid/view/View$OnTouchListener;

    invoke-interface {v2, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
