.class public LX/He2;
.super LX/He1;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# instance fields
.field public g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

.field public h:LX/G5R;

.field public i:LX/0gc;


# direct methods
.method public constructor <init>(LX/0hx;Landroid/support/v4/app/FragmentActivity;LX/G5R;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489573
    new-instance v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-direct {v0}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, LX/He2;-><init>(LX/0hx;Landroid/support/v4/app/FragmentActivity;LX/G5R;Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    .line 2489574
    return-void
.end method

.method public constructor <init>(LX/0hx;Landroid/support/v4/app/FragmentActivity;LX/G5R;Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V
    .locals 2

    .prologue
    .line 2489575
    invoke-direct {p0, p1, p2}, LX/He1;-><init>(LX/0hx;Landroid/app/Activity;)V

    .line 2489576
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, LX/He2;->i:LX/0gc;

    .line 2489577
    iput-object p4, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489578
    iput-object p3, p0, LX/He2;->h:LX/G5R;

    .line 2489579
    iget-object v0, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    if-eqz v0, :cond_0

    .line 2489580
    iget-object v0, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v1, p0, LX/He2;->h:LX/G5R;

    .line 2489581
    iput-object v1, v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->q:LX/G5R;

    .line 2489582
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/G5R;
    .locals 1

    .prologue
    .line 2489583
    iget-boolean v0, p0, LX/He1;->c:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2489584
    iget-object v0, p0, LX/He2;->h:LX/G5R;

    return-object v0
.end method

.method public a(Landroid/widget/EditText;Landroid/view/ViewGroup;Landroid/text/TextWatcher;)V
    .locals 2

    .prologue
    .line 2489585
    iput-object p1, p0, LX/He1;->d:Landroid/widget/EditText;

    .line 2489586
    iput-object p2, p0, LX/He2;->e:Landroid/view/ViewGroup;

    .line 2489587
    iget-object v0, p0, LX/He2;->i:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2489588
    iget-object v1, p0, LX/He1;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    iget-object p1, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-virtual {v0, v1, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2489589
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2489590
    invoke-virtual {p0, p3}, LX/He1;->a(Landroid/text/TextWatcher;)V

    .line 2489591
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/He2;->c:Z

    .line 2489592
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2489593
    iget-object v0, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->a(Ljava/lang/String;)V

    .line 2489594
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2489595
    iget-object v0, p0, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->a(Ljava/lang/String;)V

    .line 2489596
    return-void
.end method
