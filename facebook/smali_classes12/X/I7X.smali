.class public LX/I7X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0kx;


# direct methods
.method public constructor <init>(LX/0kx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2539752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2539753
    iput-object p1, p0, LX/I7X;->a:LX/0kx;

    .line 2539754
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2539755
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, LX/I7X;->a:LX/0kx;

    const-string v2, "unknown"

    invoke-virtual {v1, v2}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2539756
    new-instance v2, Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {v2}, Lcom/facebook/events/permalink/EventPermalinkFragment;-><init>()V

    .line 2539757
    if-eqz v1, :cond_0

    .line 2539758
    const-string p0, "extra_ref_module"

    invoke-virtual {v0, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2539759
    :cond_0
    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2539760
    move-object v0, v2

    .line 2539761
    return-object v0
.end method
