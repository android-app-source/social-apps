.class public final LX/JMI;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/JMK;

.field public final b:Landroid/graphics/PointF;

.field private final c:Landroid/graphics/PointF;

.field private d:F

.field private e:Z


# direct methods
.method public constructor <init>(LX/JMK;)V
    .locals 1

    .prologue
    .line 2683439
    iput-object p1, p0, LX/JMI;->a:LX/JMK;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 2683440
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/JMI;->b:Landroid/graphics/PointF;

    .line 2683441
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/JMI;->c:Landroid/graphics/PointF;

    .line 2683442
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/JMI;->d:F

    .line 2683443
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JMI;->e:Z

    return-void
.end method

.method private b(Landroid/graphics/PointF;)F
    .locals 4

    .prologue
    .line 2683444
    iget v0, p1, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, LX/JMI;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    .line 2683445
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v3, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 2683446
    const/4 v2, 0x0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget v0, p0, LX/JMI;->d:F

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/JMI;->d:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v8, 0x0

    const/4 v5, 0x7

    const/4 v9, 0x1

    .line 2683447
    iget-object v0, p0, LX/JMI;->a:LX/JMK;

    invoke-virtual {v0}, LX/5ui;->getZoomableController()LX/5ua;

    move-result-object v1

    check-cast v1, LX/5ue;

    .line 2683448
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2683449
    invoke-virtual {v1, v4}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    .line 2683450
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2683451
    :cond_0
    :goto_0
    return v9

    .line 2683452
    :pswitch_0
    iget-object v0, p0, LX/JMI;->a:LX/JMK;

    invoke-static {v0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2683453
    iget-object v0, p0, LX/JMI;->a:LX/JMK;

    .line 2683454
    iput-boolean v9, v0, LX/JMK;->f:Z

    .line 2683455
    iget-object v0, p0, LX/JMI;->b:Landroid/graphics/PointF;

    invoke-virtual {v0, v4}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 2683456
    iget-object v0, p0, LX/JMI;->c:Landroid/graphics/PointF;

    invoke-virtual {v0, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 2683457
    invoke-virtual {v1}, LX/5ua;->m()F

    move-result v0

    iput v0, p0, LX/JMI;->d:F

    goto :goto_0

    .line 2683458
    :pswitch_1
    iget-boolean v0, p0, LX/JMI;->e:Z

    .line 2683459
    iget v10, v4, Landroid/graphics/PointF;->x:F

    iget-object v11, p0, LX/JMI;->b:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v11

    float-to-double v10, v10

    iget v12, v4, Landroid/graphics/PointF;->y:F

    iget-object v13, p0, LX/JMI;->b:Landroid/graphics/PointF;

    iget v13, v13, Landroid/graphics/PointF;->y:F

    sub-float/2addr v12, v13

    float-to-double v12, v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v10

    .line 2683460
    const-wide/high16 v12, 0x4034000000000000L    # 20.0

    cmpl-double v10, v10, v12

    if-lez v10, :cond_3

    const/4 v10, 0x1

    :goto_1
    move v2, v10

    .line 2683461
    or-int/2addr v0, v2

    iput-boolean v0, p0, LX/JMI;->e:Z

    .line 2683462
    iget-boolean v0, p0, LX/JMI;->e:Z

    if-eqz v0, :cond_0

    .line 2683463
    invoke-direct {p0, v4}, LX/JMI;->b(Landroid/graphics/PointF;)F

    move-result v0

    .line 2683464
    iget-object v2, p0, LX/JMI;->c:Landroid/graphics/PointF;

    iget-object v3, p0, LX/JMI;->b:Landroid/graphics/PointF;

    invoke-virtual {v1, v0, v2, v3}, LX/5ua;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 2683465
    :pswitch_2
    iget-boolean v0, p0, LX/JMI;->e:Z

    if-eqz v0, :cond_1

    .line 2683466
    invoke-direct {p0, v4}, LX/JMI;->b(Landroid/graphics/PointF;)F

    move-result v0

    .line 2683467
    iget-object v2, p0, LX/JMI;->c:Landroid/graphics/PointF;

    iget-object v3, p0, LX/JMI;->b:Landroid/graphics/PointF;

    invoke-virtual {v1, v0, v2, v3}, LX/5ua;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 2683468
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JMI;->e:Z

    goto :goto_0

    .line 2683469
    :cond_1
    invoke-virtual {v1}, LX/5ua;->m()F

    move-result v0

    const/high16 v2, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    .line 2683470
    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual/range {v1 .. v8}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_2

    .line 2683471
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual/range {v1 .. v8}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
