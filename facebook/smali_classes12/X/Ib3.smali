.class public LX/Ib3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Ib5;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/Ib5;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2591701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2591702
    iput-object p1, p0, LX/Ib3;->a:LX/Ib5;

    .line 2591703
    iput-object p2, p0, LX/Ib3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2591704
    return-void
.end method

.method public static a(LX/0QB;)LX/Ib3;
    .locals 3

    .prologue
    .line 2591705
    new-instance v2, LX/Ib3;

    .line 2591706
    new-instance v1, LX/Ib5;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-direct {v1, v0}, LX/Ib5;-><init>(LX/2N8;)V

    .line 2591707
    move-object v0, v1

    .line 2591708
    check-cast v0, LX/Ib5;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/Ib3;-><init>(LX/Ib5;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2591709
    move-object v0, v2

    .line 2591710
    return-object v0
.end method

.method public static a(LX/Ib3;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2591711
    iget-object v0, p0, LX/Ib3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->aH:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Ib3;LX/Ib4;)V
    .locals 7

    .prologue
    .line 2591712
    iget-object v0, p0, LX/Ib3;->a:LX/Ib5;

    invoke-static {p0}, LX/Ib3;->a(LX/Ib3;)Ljava/lang/String;

    move-result-object v1

    .line 2591713
    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-virtual {v2}, LX/0mC;->b()LX/162;

    move-result-object v3

    .line 2591714
    new-instance v2, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2591715
    const-string v4, "ride_provider_name"

    iget-object v5, p1, LX/Ib4;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2591716
    const-string v4, "latest_selected_ride_type_name"

    iget-object v5, p1, LX/Ib4;->b:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2591717
    move-object v4, v2

    .line 2591718
    invoke-virtual {v3, v4}, LX/162;->a(LX/0lF;)LX/162;

    .line 2591719
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2591720
    const/4 v2, 0x0

    .line 2591721
    :try_start_0
    iget-object v5, v0, LX/Ib5;->a:LX/2N8;

    invoke-virtual {v5, v1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/462; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2591722
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0lF;->h()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2591723
    invoke-virtual {v2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 2591724
    const-string v6, "ride_provider_name"

    invoke-virtual {v2, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "ride_provider_name"

    invoke-virtual {v4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    move v6, v6

    .line 2591725
    if-nez v6, :cond_0

    .line 2591726
    invoke-virtual {v3, v2}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 2591727
    :cond_1
    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2591728
    iget-object v1, p0, LX/Ib3;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0db;->aH:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2591729
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static b(LX/Ib3;Ljava/lang/String;)LX/Ib4;
    .locals 5

    .prologue
    .line 2591730
    iget-object v0, p0, LX/Ib3;->a:LX/Ib5;

    invoke-static {p0}, LX/Ib3;->a(LX/Ib3;)Ljava/lang/String;

    move-result-object v1

    .line 2591731
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2591732
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2591733
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2591734
    :goto_0
    move-object v2, v2

    .line 2591735
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ib4;

    .line 2591736
    iget-object v4, v0, LX/Ib4;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2591737
    :goto_2
    move-object v0, v0

    .line 2591738
    if-eqz v0, :cond_0

    .line 2591739
    :goto_3
    return-object v0

    .line 2591740
    :cond_0
    new-instance v0, LX/Ib4;

    invoke-direct {v0, p1}, LX/Ib4;-><init>(Ljava/lang/String;)V

    .line 2591741
    invoke-static {p0, v0}, LX/Ib3;->a(LX/Ib3;LX/Ib4;)V

    goto :goto_3

    .line 2591742
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2591743
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2591744
    :cond_3
    const/4 v2, 0x0

    .line 2591745
    :try_start_0
    iget-object v4, v0, LX/Ib5;->a:LX/2N8;

    invoke-virtual {v4, v1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/462; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2591746
    :goto_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, LX/0lF;->h()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2591747
    invoke-virtual {v2}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 2591748
    const-string v0, "ride_provider_name"

    invoke-virtual {v2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2591749
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2591750
    const/4 v0, 0x0

    .line 2591751
    :goto_6
    move-object v2, v0

    .line 2591752
    if-eqz v2, :cond_4

    .line 2591753
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2591754
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_0

    :catch_0
    goto :goto_4

    .line 2591755
    :cond_6
    new-instance v0, LX/Ib4;

    invoke-direct {v0, v1}, LX/Ib4;-><init>(Ljava/lang/String;)V

    .line 2591756
    const-string v1, "latest_selected_ride_type_name"

    invoke-virtual {v2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Ib4;->b:Ljava/lang/String;

    goto :goto_6
.end method
