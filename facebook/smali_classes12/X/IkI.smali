.class public LX/IkI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606619
    return-void
.end method

.method private static a()LX/IkI;
    .locals 1

    .prologue
    .line 2606616
    new-instance v0, LX/IkI;

    invoke-direct {v0}, LX/IkI;-><init>()V

    .line 2606617
    return-object v0
.end method

.method public static a(LX/0QB;)LX/IkI;
    .locals 3

    .prologue
    .line 2606608
    const-class v1, LX/IkI;

    monitor-enter v1

    .line 2606609
    :try_start_0
    sget-object v0, LX/IkI;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606610
    sput-object v2, LX/IkI;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606611
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606612
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    invoke-static {}, LX/IkI;->a()LX/IkI;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606613
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606614
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
