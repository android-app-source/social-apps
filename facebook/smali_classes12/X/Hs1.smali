.class public LX/Hs1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/ListView;

.field private final b:LX/0Sh;

.field public final c:LX/9bq;

.field public final d:Ljava/lang/Long;

.field private final e:LX/9b2;

.field public final f:LX/Hrw;

.field public final g:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final h:LX/03V;

.field public final i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final j:Z

.field public k:LX/9b1;

.field public final l:I


# direct methods
.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/content/Context;Landroid/widget/ListView;LX/0Sh;LX/9bq;LX/9b2;LX/Hrw;Lcom/facebook/performancelogger/PerformanceLogger;LX/03V;Z)V
    .locals 2

    .prologue
    .line 2513866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513867
    iput-object p2, p0, LX/Hs1;->d:Ljava/lang/Long;

    .line 2513868
    iput-object p4, p0, LX/Hs1;->a:Landroid/widget/ListView;

    .line 2513869
    iput-object p5, p0, LX/Hs1;->b:LX/0Sh;

    .line 2513870
    iput-object p6, p0, LX/Hs1;->c:LX/9bq;

    .line 2513871
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0ab6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Hs1;->l:I

    .line 2513872
    iput-object p7, p0, LX/Hs1;->e:LX/9b2;

    .line 2513873
    iput-object p8, p0, LX/Hs1;->f:LX/Hrw;

    .line 2513874
    iput-object p9, p0, LX/Hs1;->g:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2513875
    iput-object p10, p0, LX/Hs1;->h:LX/03V;

    .line 2513876
    iput-object p1, p0, LX/Hs1;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2513877
    iput-boolean p11, p0, LX/Hs1;->j:Z

    .line 2513878
    invoke-direct {p0}, LX/Hs1;->a()V

    .line 2513879
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 2513859
    iget-object v0, p0, LX/Hs1;->g:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xe000b

    const-string v2, "perf_albums_list_fetch"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2513860
    iget-object v3, p0, LX/Hs1;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    if-ne v3, v4, :cond_0

    .line 2513861
    iget-object v3, p0, LX/Hs1;->c:LX/9bq;

    iget-object v4, p0, LX/Hs1;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v5, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/Hs1;->l:I

    invoke-virtual {v3, v4, v5}, LX/9bq;->b(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2513862
    :goto_0
    move-object v0, v3

    .line 2513863
    new-instance v1, LX/Hs0;

    invoke-direct {v1, p0}, LX/Hs0;-><init>(LX/Hs1;)V

    .line 2513864
    iget-object v2, p0, LX/Hs1;->b:LX/0Sh;

    invoke-virtual {v2, v0, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2513865
    return-void

    :cond_0
    iget-object v3, p0, LX/Hs1;->c:LX/9bq;

    iget-object v4, p0, LX/Hs1;->d:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/Hs1;->l:I

    invoke-virtual {v3, v4, v5}, LX/9bq;->a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0
.end method
