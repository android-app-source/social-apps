.class public LX/HwS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2520901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2520902
    const-string v0, "extra_media_items"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2520903
    const-string v0, "extra_media_items"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2520904
    if-eqz v0, :cond_0

    .line 2520905
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2520906
    :goto_0
    return-object v0

    .line 2520907
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2520908
    goto :goto_0
.end method

.method public static a(LX/0il;)LX/8AA;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            ":",
            "LX/0j3;",
            "DerivedData::",
            "LX/2rh;",
            ":",
            "LX/5Qw;",
            ":",
            "LX/2ri;",
            ":",
            "LX/5Qx;",
            ":",
            "LX/5Qy;",
            ":",
            "LX/5Qz;",
            ":",
            "LX/5R1;",
            "Services::",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(TServices;)",
            "LX/8AA;"
        }
    .end annotation

    .prologue
    .line 2520909
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2520910
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->COMPOSER_ADD_MORE_MEDIA:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    .line 2520911
    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->p()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->q()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->r()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->s()LX/8AA;

    .line 2520912
    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseInspirationCam()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2520913
    iget-object v0, v1, LX/8AA;->d:LX/8A6;

    .line 2520914
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/8A6;->s:Z

    .line 2520915
    :cond_0
    move-object v0, p0

    .line 2520916
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    invoke-interface {v0}, LX/2rh;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2520917
    invoke-virtual {v1}, LX/8AA;->v()LX/8AA;

    :cond_1
    move-object v0, p0

    .line 2520918
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/5Qw;

    invoke-interface {v0}, LX/5Qw;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2520919
    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    :cond_2
    move-object v0, p0

    .line 2520920
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/2ri;

    invoke-interface {v0}, LX/2ri;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2520921
    invoke-virtual {v1}, LX/8AA;->c()LX/8AA;

    :cond_3
    move-object v0, p0

    .line 2520922
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/5Qy;

    invoke-interface {v0}, LX/5Qy;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2520923
    invoke-virtual {v1}, LX/8AA;->g()LX/8AA;

    move-object v0, p0

    .line 2520924
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/5Qx;

    invoke-interface {v0}, LX/5Qx;->y()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2520925
    invoke-virtual {v1}, LX/8AA;->h()LX/8AA;

    :cond_4
    move-object v0, p0

    .line 2520926
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/5Qz;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2520927
    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    .line 2520928
    :cond_5
    check-cast p0, LX/0ik;

    invoke-interface {p0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rh;

    check-cast v0, LX/5R1;

    invoke-interface {v0}, LX/5R1;->H()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2520929
    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    .line 2520930
    :cond_6
    return-object v1
.end method
