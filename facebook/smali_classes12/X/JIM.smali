.class public final LX/JIM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;",
        ">;",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

.field public final synthetic b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;)V
    .locals 0

    .prologue
    .line 2677827
    iput-object p1, p0, LX/JIM;->b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;

    iput-object p2, p0, LX/JIM;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2677828
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2677829
    if-eqz p1, :cond_0

    .line 2677830
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677831
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2677832
    :cond_0
    iget-object v0, p0, LX/JIM;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-static {v0}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->b(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;)Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    .line 2677833
    :goto_0
    return-object v0

    .line 2677834
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677835
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    goto :goto_0
.end method
