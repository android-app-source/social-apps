.class public final LX/Ilo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wv;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V
    .locals 0

    .prologue
    .line 2608286
    iput-object p1, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 2608290
    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->f:LX/Dui;

    iget-object v1, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->j:Ljava/lang/String;

    .line 2608291
    const-string v3, "https://m.facebook.com/p2p/verify/?id=%s&source=orca_android&ts=%d&seed=%s"

    iget-object v4, v0, LX/Dui;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/16 v5, 0xa

    .line 2608292
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2608293
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v5, :cond_0

    .line 2608294
    const-string v8, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

    iget-object v9, v0, LX/Dui;->b:Ljava/util/Random;

    const/16 v10, 0x3e

    invoke-virtual {v9, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2608295
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2608296
    :cond_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2608297
    invoke-static {v3, v1, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2608298
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    move-object v0, v3

    .line 2608299
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2608300
    iget-object v1, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2608301
    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2608302
    return-void

    .line 2608303
    :cond_1
    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2608304
    invoke-virtual {p0}, LX/Ilo;->c()V

    .line 2608305
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2608287
    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->e:LX/0Zb;

    iget-object v1, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-static {v1}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "p2p_mobile_browser_risk_cancel"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2608288
    iget-object v0, p0, LX/Ilo;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2608289
    return-void
.end method
