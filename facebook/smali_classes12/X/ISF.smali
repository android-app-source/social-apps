.class public final LX/ISF;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/INL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;LX/DML;)V
    .locals 0

    .prologue
    .line 2577571
    iput-object p1, p0, LX/ISF;->a:LX/ISI;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2577572
    check-cast p1, LX/INL;

    .line 2577573
    iget-object v0, p0, LX/ISF;->a:LX/ISI;

    iget-object v0, v0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    new-instance v1, LX/ISE;

    invoke-direct {v1, p0}, LX/ISE;-><init>(LX/ISF;)V

    iget-object v2, p0, LX/ISF;->a:LX/ISI;

    .line 2577574
    new-instance p0, LX/IS3;

    invoke-direct {p0, v2}, LX/IS3;-><init>(LX/ISI;)V

    move-object v2, p0

    .line 2577575
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2577576
    :cond_0
    :goto_0
    return-void

    .line 2577577
    :cond_1
    iget-object v3, p1, LX/INL;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2577578
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 2577579
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;

    .line 2577580
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 2577581
    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v3

    invoke-static {v3}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v3

    invoke-virtual {v3}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v3

    .line 2577582
    if-nez v3, :cond_4

    .line 2577583
    :cond_2
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2577584
    :cond_3
    iget-object v3, p1, LX/INL;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2577585
    :cond_4
    new-instance v7, LX/DaR;

    invoke-virtual {p1}, LX/INL;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, LX/DaR;-><init>(Landroid/content/Context;)V

    .line 2577586
    new-instance v8, LX/1a3;

    const/4 p0, -0x1

    const/4 v0, -0x2

    invoke-direct {v8, p0, v0}, LX/1a3;-><init>(II)V

    invoke-virtual {v7, v8}, LX/DaR;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2577587
    invoke-virtual {v7, v2}, LX/DaR;->setGroupEventRsvpUpdateListener(LX/DMP;)V

    .line 2577588
    const-string v8, "future"

    const/4 p0, 0x1

    invoke-virtual {v7, v3, v8, p0}, LX/DaR;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;Z)V

    .line 2577589
    new-instance v8, LX/INK;

    invoke-direct {v8, p1, v3}, LX/INK;-><init>(LX/INL;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v7, v8}, LX/DaR;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577590
    iget-object v8, p1, LX/INL;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2
.end method
