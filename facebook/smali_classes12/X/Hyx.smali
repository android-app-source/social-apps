.class public LX/Hyx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private final g:LX/I08;

.field public final h:LX/0SG;

.field public final i:Landroid/content/ContentResolver;

.field public final j:LX/Bky;

.field public final k:LX/HyO;

.field public final l:LX/0tX;

.field public final m:LX/1My;

.field public final n:LX/0TD;

.field public final o:LX/0ad;

.field public final p:LX/0hB;

.field public final q:LX/1Ck;


# direct methods
.method public constructor <init>(LX/I08;LX/0SG;Landroid/content/ContentResolver;LX/Bky;LX/HyO;LX/0tX;LX/1My;LX/0TD;LX/0ad;LX/0hB;LX/1Ck;)V
    .locals 1
    .param p8    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2525084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2525085
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hyx;->d:Z

    .line 2525086
    iput-object p1, p0, LX/Hyx;->g:LX/I08;

    .line 2525087
    iput-object p2, p0, LX/Hyx;->h:LX/0SG;

    .line 2525088
    iput-object p3, p0, LX/Hyx;->i:Landroid/content/ContentResolver;

    .line 2525089
    iput-object p4, p0, LX/Hyx;->j:LX/Bky;

    .line 2525090
    iput-object p5, p0, LX/Hyx;->k:LX/HyO;

    .line 2525091
    iput-object p6, p0, LX/Hyx;->l:LX/0tX;

    .line 2525092
    iput-object p7, p0, LX/Hyx;->m:LX/1My;

    .line 2525093
    iput-object p8, p0, LX/Hyx;->n:LX/0TD;

    .line 2525094
    iput-object p9, p0, LX/Hyx;->o:LX/0ad;

    .line 2525095
    iput-object p10, p0, LX/Hyx;->p:LX/0hB;

    .line 2525096
    iput-object p11, p0, LX/Hyx;->q:LX/1Ck;

    .line 2525097
    return-void
.end method

.method public static a(LX/0QB;)LX/Hyx;
    .locals 1

    .prologue
    .line 2525028
    invoke-static {p0}, LX/Hyx;->b(LX/0QB;)LX/Hyx;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Hyx;
    .locals 12

    .prologue
    .line 2525082
    new-instance v0, LX/Hyx;

    invoke-static {p0}, LX/I08;->b(LX/0QB;)LX/I08;

    move-result-object v1

    check-cast v1, LX/I08;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v4

    check-cast v4, LX/Bky;

    invoke-static {p0}, LX/HyO;->a(LX/0QB;)LX/HyO;

    move-result-object v5

    check-cast v5, LX/HyO;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v7

    check-cast v7, LX/1My;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-direct/range {v0 .. v11}, LX/Hyx;-><init>(LX/I08;LX/0SG;Landroid/content/ContentResolver;LX/Bky;LX/HyO;LX/0tX;LX/1My;LX/0TD;LX/0ad;LX/0hB;LX/1Ck;)V

    .line 2525083
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2525070
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2525071
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oa;

    .line 2525072
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2525073
    invoke-interface {v2}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2525074
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525075
    iget-object v3, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2525076
    iget-wide v9, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v9

    .line 2525077
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2525078
    iget-object v3, p0, LX/Hyx;->m:LX/1My;

    iget-object v4, p0, LX/Hyx;->c:LX/0TF;

    invoke-interface {v2}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2525079
    invoke-static {v2}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2525080
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2525081
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Hx6;ZIILjava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hx6;",
            "ZII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525069
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Hx6;ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Hx6;",
            "ZII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525050
    new-instance v0, LX/7oS;

    invoke-direct {v0}, LX/7oS;-><init>()V

    const-string v1, "profile_image_size"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "first_count"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 2525051
    if-eqz p5, :cond_0

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2525052
    const-string v1, "event_state"

    invoke-virtual {v0, v1, p5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2525053
    :cond_0
    if-eqz p6, :cond_1

    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2525054
    const-string v1, "filter"

    invoke-virtual {v0, v1, p6}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2525055
    :cond_1
    sget-object v1, LX/Hyw;->a:[I

    invoke-virtual {p1}, LX/Hx6;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2525056
    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2525057
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2525058
    const-string v2, "filter"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525059
    :cond_2
    if-eqz p2, :cond_3

    .line 2525060
    const-string v1, "after_cursor"

    iget-object v2, p0, LX/Hyx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525061
    :cond_3
    invoke-static {}, LX/7oV;->g()LX/7oS;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2525062
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2525063
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2525064
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2525065
    move-object v0, v0

    .line 2525066
    iget-object v1, p0, LX/Hyx;->l:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 2525067
    :pswitch_0
    const-string v1, "HOST"

    goto :goto_0

    .line 2525068
    :pswitch_1
    const-string v1, "INVITED"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525098
    new-instance v0, LX/7oL;

    invoke-direct {v0}, LX/7oL;-><init>()V

    .line 2525099
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f74

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2525101
    const-string v2, "profile_image_size"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525102
    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2525103
    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2525104
    invoke-static {}, LX/7oV;->c()LX/7oL;

    move-result-object v1

    .line 2525105
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2525106
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2525107
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2525108
    iget-object v1, p0, LX/Hyx;->l:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525039
    new-instance v0, LX/7oO;

    invoke-direct {v0}, LX/7oO;-><init>()V

    const-string v1, "profile_image_size"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/Hyx;->p:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "first_count"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2525040
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2525041
    const-string v1, "event_state"

    invoke-virtual {v0, v1, p4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2525042
    :cond_0
    if-eqz p1, :cond_1

    .line 2525043
    const-string v1, "after_cursor"

    iget-object v2, p0, LX/Hyx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2525044
    :cond_1
    if-eqz p5, :cond_2

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2525045
    const-string v1, "filter"

    invoke-virtual {v0, v1, p5}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2525046
    :cond_2
    invoke-static {}, LX/7oV;->h()LX/7oO;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2525047
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2525048
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2525049
    iget-object v1, p0, LX/Hyx;->l:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2525033
    iput-object v1, p0, LX/Hyx;->a:Ljava/lang/String;

    .line 2525034
    iput-object v1, p0, LX/Hyx;->b:Ljava/lang/String;

    .line 2525035
    iput-object v1, p0, LX/Hyx;->f:Ljava/lang/String;

    .line 2525036
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hyx;->d:Z

    .line 2525037
    iput-object v1, p0, LX/Hyx;->e:Ljava/lang/String;

    .line 2525038
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2525031
    iget-object v0, p0, LX/Hyx;->n:LX/0TD;

    new-instance v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$3;-><init>(LX/Hyx;LX/0Px;)V

    const v2, -0x60f947b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2525032
    return-void
.end method

.method public final a(ZLX/Hyh;)V
    .locals 1

    .prologue
    .line 2525029
    const/16 v0, 0x10

    invoke-virtual {p0, p1, p2, v0}, LX/Hyx;->a(ZLX/Hyh;I)V

    .line 2525030
    return-void
.end method

.method public final a(ZLX/Hyh;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2525019
    iget-boolean v0, p0, LX/Hyx;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hyx;->g:LX/I08;

    invoke-virtual {v0}, LX/I08;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2525020
    :cond_0
    :goto_0
    return-void

    .line 2525021
    :cond_1
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2525022
    const/16 v0, 0xa

    invoke-virtual {v4, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 2525023
    const/16 v0, 0xc

    invoke-virtual {v4, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 2525024
    const/16 v0, 0xd

    invoke-virtual {v4, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 2525025
    const/16 v0, 0xe

    invoke-virtual {v4, v0, v1}, Ljava/util/GregorianCalendar;->set(II)V

    .line 2525026
    const/4 v0, 0x6

    iget-object v1, p0, LX/Hyx;->o:LX/0ad;

    sget v2, LX/347;->j:I

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Ljava/util/GregorianCalendar;->roll(II)V

    .line 2525027
    iget-object v0, p0, LX/Hyx;->g:LX/I08;

    iget-object v2, p0, LX/Hyx;->f:Ljava/lang/String;

    if-eqz p1, :cond_2

    iget-object v3, p0, LX/Hyx;->e:Ljava/lang/String;

    :goto_1
    new-instance v5, LX/Hyu;

    invoke-direct {v5, p0, p2}, LX/Hyu;-><init>(LX/Hyx;LX/Hyh;)V

    move v1, p3

    invoke-virtual/range {v0 .. v5}, LX/I08;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
