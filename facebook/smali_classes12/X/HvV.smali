.class public final LX/HvV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/textstyle/ComposerRichTextController;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/textstyle/ComposerRichTextController;)V
    .locals 0

    .prologue
    .line 2519181
    iput-object p1, p0, LX/HvV;->a:Lcom/facebook/composer/textstyle/ComposerRichTextController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;ILX/IF4;)V
    .locals 5

    .prologue
    .line 2519182
    iget-object v0, p0, LX/HvV;->a:Lcom/facebook/composer/textstyle/ComposerRichTextController;

    iget-object v0, v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519183
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j9;

    invoke-interface {v1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/IF4;->DRAFT_RECOVERY:LX/IF4;

    if-eq p3, v1, :cond_2

    .line 2519184
    :cond_1
    :goto_0
    return-void

    .line 2519185
    :cond_2
    iget-object v1, p0, LX/HvV;->a:Lcom/facebook/composer/textstyle/ComposerRichTextController;

    invoke-static {v1, p1}, Lcom/facebook/composer/textstyle/ComposerRichTextController;->a$redex0(Lcom/facebook/composer/textstyle/ComposerRichTextController;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V

    .line 2519186
    sget-object v1, LX/IF4;->USER_SELECT:LX/IF4;

    if-ne p3, v1, :cond_3

    .line 2519187
    iget-object v1, p0, LX/HvV;->a:Lcom/facebook/composer/textstyle/ComposerRichTextController;

    iget-object v1, v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2519188
    sget-object v2, Lcom/facebook/composer/textstyle/ComposerRichTextController;->a:LX/0Tn;

    invoke-interface {v1, v2, p2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    .line 2519189
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2519190
    :cond_3
    iget-object v1, p0, LX/HvV;->a:Lcom/facebook/composer/textstyle/ComposerRichTextController;

    iget-object v2, v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;->i:LX/IF5;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    .line 2519191
    iget-object v4, v2, LX/IF5;->a:LX/0Zb;

    sget-object p0, LX/IF3;->STYLE_CHANGED:LX/IF3;

    invoke-virtual {p0}, LX/IF3;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "name"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "reason"

    invoke-virtual {p3}, LX/IF4;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "content_type"

    invoke-virtual {v0}, LX/5RF;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v4, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519192
    goto :goto_0
.end method
