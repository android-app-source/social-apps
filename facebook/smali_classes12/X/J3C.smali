.class public final LX/J3C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

.field public final synthetic b:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/widget/text/ClearableAutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 2642227
    iput-object p1, p0, LX/J3C;->b:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    iput-object p2, p0, LX/J3C;->a:Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2642228
    iget-object v0, p0, LX/J3C;->b:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    invoke-static {}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->newBuilder()LX/J34;

    move-result-object v1

    iget-object v2, p0, LX/J3C;->b:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    iget-object v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {v1, v2}, LX/J34;->a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;

    move-result-object v1

    iget-object v2, p0, LX/J3C;->a:Lcom/facebook/widget/text/ClearableAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/ClearableAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2642229
    iput-object v2, v1, LX/J34;->q:Ljava/lang/String;

    .line 2642230
    move-object v1, v1

    .line 2642231
    invoke-virtual {v1}, LX/J34;->a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;Lcom/facebook/payments/sample/PaymentsFlowSampleData;)V

    .line 2642232
    return-void
.end method
