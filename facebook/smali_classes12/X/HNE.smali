.class public LX/HNE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HNI;

.field private final b:LX/HNN;

.field private final c:LX/HNQ;

.field private final d:LX/HNO;

.field private final e:LX/HNR;

.field public final f:LX/HN9;

.field private final g:Landroid/os/ParcelUuid;


# direct methods
.method public constructor <init>(LX/HNI;LX/HNN;LX/HNQ;LX/HNO;LX/HNR;LX/HN9;Landroid/os/ParcelUuid;)V
    .locals 0
    .param p7    # Landroid/os/ParcelUuid;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458241
    iput-object p1, p0, LX/HNE;->a:LX/HNI;

    .line 2458242
    iput-object p2, p0, LX/HNE;->b:LX/HNN;

    .line 2458243
    iput-object p3, p0, LX/HNE;->c:LX/HNQ;

    .line 2458244
    iput-object p4, p0, LX/HNE;->d:LX/HNO;

    .line 2458245
    iput-object p5, p0, LX/HNE;->e:LX/HNR;

    .line 2458246
    iput-object p6, p0, LX/HNE;->f:LX/HN9;

    .line 2458247
    iput-object p7, p0, LX/HNE;->g:Landroid/os/ParcelUuid;

    .line 2458248
    return-void
.end method

.method private a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/H8Z;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2458249
    if-nez p1, :cond_1

    .line 2458250
    :cond_0
    :goto_0
    return-object v0

    .line 2458251
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    .line 2458252
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v2, :cond_2

    .line 2458253
    iget-object v0, p0, LX/HNE;->c:LX/HNQ;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2458254
    new-instance p0, LX/HNP;

    const/16 v2, 0x3be

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {p0, p2, v2, v1}, LX/HNP;-><init>(LX/0Ot;Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 2458255
    move-object v0, p0

    .line 2458256
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/HNP;->g:Ljava/lang/String;

    .line 2458257
    goto :goto_0

    .line 2458258
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v2, :cond_3

    .line 2458259
    iget-object v0, p0, LX/HNE;->a:LX/HNI;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2458260
    new-instance p0, LX/HNH;

    const/16 v2, 0x12c4

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {p0, p2, v2, v1}, LX/HNH;-><init>(LX/0Ot;Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 2458261
    move-object v0, p0

    .line 2458262
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/HNH;->g:Ljava/lang/String;

    .line 2458263
    goto :goto_0

    .line 2458264
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v2, :cond_5

    .line 2458265
    iget-object v0, p0, LX/HNE;->b:LX/HNN;

    iget-object v1, p0, LX/HNE;->g:Landroid/os/ParcelUuid;

    .line 2458266
    new-instance v3, LX/HNM;

    const/16 v4, 0x2bee

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2c04

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12c4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    move-object v8, v1

    invoke-direct/range {v3 .. v8}, LX/HNM;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;Landroid/content/Context;Landroid/os/ParcelUuid;)V

    .line 2458267
    move-object v0, v3

    .line 2458268
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->r()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    iput-object v1, v0, LX/HNM;->i:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2458269
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2458270
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/HNM;->j:Ljava/lang/String;

    .line 2458271
    :cond_4
    goto/16 :goto_0

    .line 2458272
    :cond_5
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v2, :cond_7

    .line 2458273
    iget-object v0, p0, LX/HNE;->d:LX/HNO;

    .line 2458274
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2458275
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/HNO;->g:Ljava/lang/String;

    .line 2458276
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/HNO;->h:Ljava/lang/String;

    .line 2458277
    :cond_6
    iget-object v0, p0, LX/HNE;->d:LX/HNO;

    goto/16 :goto_0

    .line 2458278
    :cond_7
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v2, :cond_0

    .line 2458279
    iget-object v0, p0, LX/HNE;->e:LX/HNR;

    .line 2458280
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 2458281
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, v0, LX/HNR;->f:J

    .line 2458282
    :cond_8
    iget-object v0, p0, LX/HNE;->e:LX/HNR;

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z
    .locals 1

    .prologue
    .line 2458283
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq p0, v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->d()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabAdminSettingChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->d()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabAdminSettingChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabAdminSettingChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z
    .locals 1

    .prologue
    .line 2458284
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Z)LX/34b;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2458285
    invoke-static {p2, p3}, LX/HNE;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2458286
    const/4 v0, 0x0

    .line 2458287
    :goto_0
    return-object v0

    .line 2458288
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 2458289
    invoke-virtual {p3}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->d()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabAdminSettingChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabAdminSettingChannelModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2458290
    invoke-direct {p0, v0, p4}, LX/HNE;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/H8Z;

    move-result-object v6

    .line 2458291
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    .line 2458292
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {v3, v0, v6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2458293
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2458294
    :cond_2
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2458295
    new-instance v1, LX/34b;

    invoke-direct {v1, p1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2458296
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2458297
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8Z;

    .line 2458298
    invoke-interface {v0}, LX/H8Z;->a()LX/HA7;

    move-result-object v4

    .line 2458299
    iget-boolean v5, v4, LX/HA7;->f:Z

    move v5, v5

    .line 2458300
    if-eqz v5, :cond_3

    .line 2458301
    iget v5, v4, LX/HA7;->a:I

    move v5, v5

    .line 2458302
    iget v6, v4, LX/HA7;->b:I

    move v6, v6

    .line 2458303
    invoke-virtual {v1, v5, v2, v6}, LX/34c;->a(III)LX/3Ai;

    move-result-object v5

    .line 2458304
    iget v6, v4, LX/HA7;->d:I

    move v4, v6

    .line 2458305
    invoke-virtual {v5, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2458306
    new-instance v4, LX/HND;

    invoke-direct {v4, p0, v0, p3, p2}, LX/HND;-><init>(LX/HNE;LX/H8Z;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    invoke-virtual {v5, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 2458307
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/HN7;Z)LX/5fq;
    .locals 10
    .param p4    # LX/HN7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458308
    invoke-static {p2, p3}, LX/HNE;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2458309
    const/4 v0, 0x0

    .line 2458310
    :goto_0
    return-object v0

    .line 2458311
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p5}, LX/HNE;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Z)LX/34b;

    move-result-object v4

    .line 2458312
    new-instance v6, LX/5fq;

    .line 2458313
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq p2, v0, :cond_1

    .line 2458314
    const/4 v0, 0x1

    .line 2458315
    :goto_1
    move v7, v0

    .line 2458316
    invoke-static {p2, p3}, LX/HNE;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v8

    new-instance v9, LX/HNB;

    invoke-direct {v9, p0, p2, p3, p4}, LX/HNB;-><init>(LX/HNE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/HN7;)V

    new-instance v0, LX/HNC;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/HNC;-><init>(LX/HNE;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/34b;Landroid/content/Context;)V

    invoke-direct {v6, v7, v8, v9, v0}, LX/5fq;-><init>(ZZLandroid/widget/CompoundButton$OnCheckedChangeListener;Landroid/view/View$OnClickListener;)V

    move-object v0, v6

    goto :goto_0

    .line 2458317
    :cond_1
    if-eqz p4, :cond_2

    .line 2458318
    const-string v0, "published"

    .line 2458319
    iget-object v1, p4, LX/HN7;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2458320
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2458321
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2458322
    invoke-virtual {p3}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2458323
    invoke-static {v0}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2458324
    const-string v1, "published"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2458325
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
