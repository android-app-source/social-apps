.class public final LX/Hl1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Hl3;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Hl3;)V
    .locals 1

    iput-object p1, p0, LX/Hl1;->a:LX/Hl3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, LX/Hl1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Hl1;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public alert(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, LX/Hl1;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public getAnalogInfo()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, LX/Hke;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, LX/Hko;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onPageInitialized()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, LX/Hl1;->a:LX/Hl3;

    iget-boolean v1, v0, LX/Hl2;->a:Z

    move v0, v1

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LX/Hl1;->a:LX/Hl3;

    iget-object v0, v0, LX/Hl3;->a:LX/Hje;

    iget-object v1, v0, LX/Hje;->b:LX/Hjg;

    iget-object v1, v1, LX/Hjg;->c:LX/Hjm;

    invoke-virtual {v1}, LX/Hjm;->c()V

    iget-object v0, p0, LX/Hl1;->a:LX/Hl3;

    iget-object v0, v0, LX/Hl3;->b:LX/HjZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hl1;->a:LX/Hl3;

    iget-object v0, v0, LX/Hl3;->b:LX/HjZ;

    invoke-virtual {v0}, LX/HjZ;->a()V

    goto :goto_0
.end method
