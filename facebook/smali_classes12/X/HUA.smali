.class public final LX/HUA;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V
    .locals 0

    .prologue
    .line 2471614
    iput-object p1, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 2471615
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2471616
    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->j:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 2471617
    iget-object v1, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->j:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    .line 2471618
    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->K:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-nez v2, :cond_1

    .line 2471619
    :cond_0
    :goto_0
    return-void

    .line 2471620
    :cond_1
    invoke-virtual {p1, v3}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    .line 2471621
    iget-object v3, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->K:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->L:I

    if-eq v3, v2, :cond_2

    iget-object v3, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2471622
    iget-object v3, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->K:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v3, p1, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2471623
    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    .line 2471624
    iput v2, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->L:I

    .line 2471625
    :cond_2
    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->l:LX/1ON;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-le v1, v0, :cond_0

    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    if-nez v0, :cond_0

    .line 2471626
    iget-object v0, p0, LX/HUA;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    const/4 p1, 0x1

    .line 2471627
    iput-boolean p1, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471628
    new-instance v1, LX/HUD;

    invoke-direct {v1, v0}, LX/HUD;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    .line 2471629
    new-instance v2, LX/HUE;

    invoke-direct {v2, v0}, LX/HUE;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    .line 2471630
    iget-object v3, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a:LX/1Ck;

    const-string p0, "fetchPagesPoliticalEndorsements"

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    iget-object p3, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    aput-object p3, p1, p2

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2471631
    goto :goto_0
.end method
