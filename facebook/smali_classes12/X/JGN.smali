.class public abstract LX/JGN;
.super LX/JGM;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public c:F

.field public d:F

.field public d_:Z

.field public e:F

.field public f:F

.field public g:Z

.field public h:F

.field public i:F

.field public j:F

.field public k:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2667171
    invoke-direct {p0}, LX/JGM;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(FFFFFFFF)LX/JGN;
    .locals 3

    .prologue
    .line 2667155
    iget-boolean v0, p0, LX/JGN;->g:Z

    if-eqz v0, :cond_3

    .line 2667156
    invoke-virtual {p0, p1, p2, p3, p4}, LX/JGN;->d(FFFF)Z

    move-result v1

    .line 2667157
    invoke-virtual {p0, p5, p6, p7, p8}, LX/JGN;->a(FFFF)Z

    move-result v2

    .line 2667158
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2667159
    :goto_0
    return-object p0

    .line 2667160
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGN;

    .line 2667161
    if-nez v1, :cond_1

    .line 2667162
    invoke-virtual {v0, p1, p2, p3, p4}, LX/JGN;->c(FFFF)V

    .line 2667163
    :cond_1
    if-nez v2, :cond_2

    .line 2667164
    invoke-virtual {v0, p5, p6, p7, p8}, LX/JGN;->b(FFFF)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    move-object p0, v0

    .line 2667165
    goto :goto_0

    .line 2667166
    :catch_0
    move-exception v0

    .line 2667167
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2667168
    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, LX/JGN;->c(FFFF)V

    .line 2667169
    invoke-virtual {p0, p5, p6, p7, p8}, LX/JGN;->b(FFFF)V

    .line 2667170
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JGN;->g:Z

    goto :goto_0
.end method

.method public a(LX/JGs;Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2667108
    iget-boolean v0, p0, LX/JGN;->d_:Z

    if-eqz v0, :cond_1

    .line 2667109
    iget v0, p0, LX/JGN;->c:F

    .line 2667110
    iget p1, p0, LX/JGN;->h:F

    move p1, p1

    .line 2667111
    cmpg-float v0, v0, p1

    if-ltz v0, :cond_0

    iget v0, p0, LX/JGN;->d:F

    .line 2667112
    iget p1, p0, LX/JGN;->i:F

    move p1, p1

    .line 2667113
    cmpg-float v0, v0, p1

    if-ltz v0, :cond_0

    iget v0, p0, LX/JGN;->e:F

    .line 2667114
    iget p1, p0, LX/JGN;->j:F

    move p1, p1

    .line 2667115
    cmpl-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, LX/JGN;->f:F

    .line 2667116
    iget p1, p0, LX/JGN;->k:F

    move p1, p1

    .line 2667117
    cmpl-float v0, v0, p1

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2667118
    if-eqz v0, :cond_1

    .line 2667119
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 2667120
    invoke-virtual {p0, p2}, LX/JGN;->b(Landroid/graphics/Canvas;)V

    .line 2667121
    invoke-virtual {p0, p2}, LX/JGN;->c(Landroid/graphics/Canvas;)V

    .line 2667122
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 2667123
    :goto_1
    return-void

    .line 2667124
    :cond_1
    invoke-virtual {p0, p2}, LX/JGN;->c(Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FFFF)Z
    .locals 1

    .prologue
    .line 2667154
    iget v0, p0, LX/JGN;->h:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->i:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->j:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->k:F

    cmpl-float v0, v0, p4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(FFFF)V
    .locals 2

    .prologue
    .line 2667147
    iput p1, p0, LX/JGN;->h:F

    .line 2667148
    iput p2, p0, LX/JGN;->i:F

    .line 2667149
    iput p3, p0, LX/JGN;->j:F

    .line 2667150
    iput p4, p0, LX/JGN;->k:F

    .line 2667151
    iget v0, p0, LX/JGN;->h:F

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/JGN;->d_:Z

    .line 2667152
    return-void

    .line 2667153
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/JGs;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2667145
    invoke-virtual {p0, p1, p2}, LX/JGN;->c(LX/JGs;Landroid/graphics/Canvas;)V

    .line 2667146
    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2667143
    iget v0, p0, LX/JGN;->h:F

    iget v1, p0, LX/JGN;->i:F

    iget v2, p0, LX/JGN;->j:F

    iget v3, p0, LX/JGN;->k:F

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 2667144
    return-void
.end method

.method public final c(FFFF)V
    .locals 0

    .prologue
    .line 2667137
    iput p1, p0, LX/JGN;->c:F

    .line 2667138
    iput p2, p0, LX/JGN;->d:F

    .line 2667139
    iput p3, p0, LX/JGN;->e:F

    .line 2667140
    iput p4, p0, LX/JGN;->f:F

    .line 2667141
    invoke-virtual {p0}, LX/JGN;->d()V

    .line 2667142
    return-void
.end method

.method public c(LX/JGs;Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2667133
    const v0, -0xff0001

    move v2, v0

    .line 2667134
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 2667135
    iget v3, p0, LX/JGN;->c:F

    iget v4, p0, LX/JGN;->d:F

    iget v5, p0, LX/JGN;->e:F

    iget v6, p0, LX/JGN;->f:F

    move-object v0, p1

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, LX/JGs;->a(Landroid/graphics/Canvas;IFFFF)V

    .line 2667136
    return-void
.end method

.method public abstract c(Landroid/graphics/Canvas;)V
.end method

.method public d()V
    .locals 0

    .prologue
    .line 2667132
    return-void
.end method

.method public final d(FFFF)Z
    .locals 1

    .prologue
    .line 2667131
    iget v0, p0, LX/JGN;->c:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->d:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->e:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget v0, p0, LX/JGN;->f:F

    cmpl-float v0, v0, p4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 2667130
    iget-boolean v0, p0, LX/JGN;->g:Z

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2667128
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JGN;->g:Z

    .line 2667129
    return-void
.end method

.method public final j()F
    .locals 1

    .prologue
    .line 2667127
    iget v0, p0, LX/JGN;->c:F

    return v0
.end method

.method public final k()F
    .locals 1

    .prologue
    .line 2667126
    iget v0, p0, LX/JGN;->d:F

    return v0
.end method

.method public final l()F
    .locals 1

    .prologue
    .line 2667125
    iget v0, p0, LX/JGN;->e:F

    return v0
.end method

.method public final m()F
    .locals 1

    .prologue
    .line 2667107
    iget v0, p0, LX/JGN;->f:F

    return v0
.end method

.method public final ng_()LX/JGN;
    .locals 2

    .prologue
    .line 2667102
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGN;

    .line 2667103
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/JGN;->g:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2667104
    return-object v0

    .line 2667105
    :catch_0
    move-exception v0

    .line 2667106
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
