.class public final LX/JMk;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JMl;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JMj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JMl",
            "<TE;>.ActionYouMayTakeFooterComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JMl;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JMl;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2684627
    iput-object p1, p0, LX/JMk;->b:LX/JMl;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2684628
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JMk;->c:[Ljava/lang/String;

    .line 2684629
    iput v3, p0, LX/JMk;->d:I

    .line 2684630
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JMk;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JMk;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JMk;LX/1De;IILX/JMj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JMl",
            "<TE;>.ActionYouMayTakeFooterComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2684631
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2684632
    iput-object p4, p0, LX/JMk;->a:LX/JMj;

    .line 2684633
    iget-object v0, p0, LX/JMk;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2684634
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2684635
    invoke-super {p0}, LX/1X5;->a()V

    .line 2684636
    const/4 v0, 0x0

    iput-object v0, p0, LX/JMk;->a:LX/JMj;

    .line 2684637
    iget-object v0, p0, LX/JMk;->b:LX/JMl;

    iget-object v0, v0, LX/JMl;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2684638
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JMl;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2684639
    iget-object v1, p0, LX/JMk;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JMk;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JMk;->d:I

    if-ge v1, v2, :cond_2

    .line 2684640
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2684641
    :goto_0
    iget v2, p0, LX/JMk;->d:I

    if-ge v0, v2, :cond_1

    .line 2684642
    iget-object v2, p0, LX/JMk;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2684643
    iget-object v2, p0, LX/JMk;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2684644
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2684645
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684646
    :cond_2
    iget-object v0, p0, LX/JMk;->a:LX/JMj;

    .line 2684647
    invoke-virtual {p0}, LX/JMk;->a()V

    .line 2684648
    return-object v0
.end method
