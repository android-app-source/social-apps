.class public LX/Iw6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Iw6;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2629399
    iput-object p1, p0, LX/Iw6;->a:LX/0Zb;

    .line 2629400
    return-void
.end method

.method public static a(LX/0QB;)LX/Iw6;
    .locals 4

    .prologue
    .line 2629407
    sget-object v0, LX/Iw6;->b:LX/Iw6;

    if-nez v0, :cond_1

    .line 2629408
    const-class v1, LX/Iw6;

    monitor-enter v1

    .line 2629409
    :try_start_0
    sget-object v0, LX/Iw6;->b:LX/Iw6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2629410
    if-eqz v2, :cond_0

    .line 2629411
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2629412
    new-instance p0, LX/Iw6;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Iw6;-><init>(LX/0Zb;)V

    .line 2629413
    move-object v0, p0

    .line 2629414
    sput-object v0, LX/Iw6;->b:LX/Iw6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2629415
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2629416
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2629417
    :cond_1
    sget-object v0, LX/Iw6;->b:LX/Iw6;

    return-object v0

    .line 2629418
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2629419
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2629403
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pages_browser"

    .line 2629404
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2629405
    move-object v0, v0

    .line 2629406
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 2629401
    iget-object v0, p0, LX/Iw6;->a:LX/0Zb;

    invoke-static {p1}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "page_id"

    invoke-virtual {v1, v2, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2629402
    return-void
.end method
