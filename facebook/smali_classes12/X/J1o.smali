.class public LX/J1o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field public static final a:Lcom/facebook/payments/currency/CurrencyAmount;


# instance fields
.field private final b:LX/5fv;

.field private c:Z

.field private d:Z

.field private e:Ljava/lang/String;

.field public f:LX/J1e;

.field private g:Z

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2639358
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "USD"

    const-wide/32 v2, 0xf4240

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sput-object v0, LX/J1o;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    return-void
.end method

.method public constructor <init>(LX/5fv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639355
    iput-object p1, p0, LX/J1o;->b:LX/5fv;

    .line 2639356
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J1o;->c:Z

    .line 2639357
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2639338
    iget-object v0, p0, LX/J1o;->f:LX/J1e;

    if-eqz v0, :cond_1

    .line 2639339
    iget-object v0, p0, LX/J1o;->f:LX/J1e;

    .line 2639340
    iget-object v1, v0, LX/J1e;->a:LX/J1i;

    iget-object v1, v1, LX/J1i;->l:LX/Ioo;

    new-instance v2, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iget-object v3, v0, LX/J1e;->a:LX/J1i;

    iget-object v3, v3, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/J1e;->a:LX/J1i;

    iget-object v4, v4, LX/J1i;->i:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LX/Ioo;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V

    .line 2639341
    iget-boolean v0, p0, LX/J1o;->g:Z

    if-eqz v0, :cond_2

    .line 2639342
    iget-object v0, p0, LX/J1o;->f:LX/J1e;

    iget v1, p0, LX/J1o;->h:I

    .line 2639343
    iget-object v2, v0, LX/J1e;->a:LX/J1i;

    iget-object v2, v2, LX/J1i;->f:LX/Duk;

    .line 2639344
    invoke-static {v2}, LX/Duk;->d(LX/Duk;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2639345
    iget-object v3, v2, LX/Duk;->e:LX/3RX;

    const v4, 0x7f07003e

    iget-object v5, v2, LX/Duk;->f:LX/3RZ;

    invoke-virtual {v5}, LX/3RZ;->a()I

    move-result v5

    const/high16 p0, 0x3e800000    # 0.25f

    invoke-virtual {v3, v4, v5, p0}, LX/3RX;->a(IIF)LX/7Cb;

    .line 2639346
    :cond_0
    iget-object v2, v0, LX/J1e;->a:LX/J1i;

    iget-boolean v2, v2, LX/J1i;->o:Z

    if-eqz v2, :cond_1

    .line 2639347
    iget-object v2, v0, LX/J1e;->a:LX/J1i;

    invoke-static {v2, v1}, LX/J1i;->a$redex0(LX/J1i;I)V

    .line 2639348
    :cond_1
    :goto_0
    return-void

    .line 2639349
    :cond_2
    iget-object v0, p0, LX/J1o;->f:LX/J1e;

    .line 2639350
    iget-object v1, v0, LX/J1e;->a:LX/J1i;

    iget-object v1, v1, LX/J1i;->f:LX/Duk;

    .line 2639351
    invoke-static {v1}, LX/Duk;->d(LX/Duk;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2639352
    iget-object v2, v1, LX/Duk;->e:LX/3RX;

    const v3, 0x7f07003f

    iget-object v4, v1, LX/Duk;->f:LX/3RZ;

    invoke-virtual {v4}, LX/3RZ;->a()I

    move-result v4

    const/high16 v0, 0x3e800000    # 0.25f

    invoke-virtual {v2, v3, v4, v0}, LX/3RX;->a(IIF)LX/7Cb;

    .line 2639353
    :cond_3
    goto :goto_0
.end method

.method private a(Landroid/text/Editable;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2639359
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J1o;->c:Z

    .line 2639360
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-interface {p1, v1, v0, p2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2639361
    iput-boolean v1, p0, LX/J1o;->c:Z

    .line 2639362
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2639295
    iget-boolean v0, p0, LX/J1o;->c:Z

    if-eqz v0, :cond_0

    .line 2639296
    :goto_0
    return-void

    .line 2639297
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2639298
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2639299
    const-string v1, ","

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2639300
    :cond_1
    move-object v0, v0

    .line 2639301
    const-string v1, "([1-9]\\d*)?0?(\\.\\d{0,2})?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2639302
    const-string v1, "^0[1-9]$"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2639303
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/J1o;->a(Landroid/text/Editable;Ljava/lang/String;)V

    .line 2639304
    iput-boolean v4, p0, LX/J1o;->g:Z

    .line 2639305
    invoke-direct {p0}, LX/J1o;->a()V

    goto :goto_0

    .line 2639306
    :cond_2
    iget-object v0, p0, LX/J1o;->e:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, LX/J1o;->a(Landroid/text/Editable;Ljava/lang/String;)V

    .line 2639307
    iget-object v0, p0, LX/J1o;->f:LX/J1e;

    invoke-virtual {v0}, LX/J1e;->d()V

    goto :goto_0

    .line 2639308
    :cond_3
    :try_start_0
    iget-object v1, p0, LX/J1o;->b:LX/5fv;

    const-string v2, "USD"

    invoke-virtual {v1, v2, v0}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 2639309
    sget-object v2, LX/J1o;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Lcom/facebook/payments/currency/CurrencyAmount;)I

    move-result v1

    if-lez v1, :cond_4

    .line 2639310
    iget-object v1, p0, LX/J1o;->f:LX/J1e;

    .line 2639311
    iget-object v5, v1, LX/J1e;->a:LX/J1i;

    iget-object v5, v5, LX/J1i;->g:Landroid/os/Vibrator;

    const-wide/16 v7, 0x32

    invoke-virtual {v5, v7, v8}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2639312
    iget-object v1, p0, LX/J1o;->e:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, LX/J1o;->a(Landroid/text/Editable;Ljava/lang/String;)V

    .line 2639313
    iget-object v1, p0, LX/J1o;->f:LX/J1e;

    invoke-virtual {v1}, LX/J1e;->d()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2639314
    :catch_0
    :cond_4
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2639315
    iput-boolean v4, p0, LX/J1o;->c:Z

    .line 2639316
    const-string v0, "0"

    invoke-interface {p1, v3, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2639317
    iput-boolean v3, p0, LX/J1o;->c:Z

    .line 2639318
    invoke-direct {p0}, LX/J1o;->a()V

    goto :goto_0

    .line 2639319
    :cond_5
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/J1o;->e:Ljava/lang/String;

    const-string v2, "0."

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2639320
    iput-boolean v4, p0, LX/J1o;->c:Z

    .line 2639321
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 2639322
    iput-boolean v3, p0, LX/J1o;->c:Z

    .line 2639323
    invoke-direct {p0}, LX/J1o;->a()V

    goto/16 :goto_0

    .line 2639324
    :cond_6
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2639325
    const/4 v2, -0x1

    if-ne v1, v2, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    :cond_7
    add-int/lit8 v1, v1, -0x3

    .line 2639326
    if-lez v1, :cond_8

    .line 2639327
    const-string v2, ","

    .line 2639328
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2639329
    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 2639330
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 2639331
    :cond_8
    move-object v0, v0

    .line 2639332
    iput-boolean v4, p0, LX/J1o;->c:Z

    .line 2639333
    iput-boolean v4, p0, LX/J1o;->d:Z

    .line 2639334
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p1, v3, v1, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2639335
    iput-boolean v3, p0, LX/J1o;->c:Z

    .line 2639336
    iput-boolean v3, p0, LX/J1o;->d:Z

    .line 2639337
    invoke-direct {p0}, LX/J1o;->a()V

    goto/16 :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2639292
    iget-boolean v0, p0, LX/J1o;->c:Z

    if-eqz v0, :cond_0

    .line 2639293
    :goto_0
    return-void

    .line 2639294
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/J1o;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2639288
    add-int v0, p2, p4

    iput v0, p0, LX/J1o;->h:I

    .line 2639289
    iget-boolean v0, p0, LX/J1o;->d:Z

    if-eqz v0, :cond_0

    .line 2639290
    :goto_0
    return-void

    .line 2639291
    :cond_0
    if-le p4, p3, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/J1o;->g:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
