.class public final LX/JSb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JSc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/JSe;

.field public b:LX/JTY;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/JSc;


# direct methods
.method public constructor <init>(LX/JSc;)V
    .locals 1

    .prologue
    .line 2695631
    iput-object p1, p0, LX/JSb;->e:LX/JSc;

    .line 2695632
    move-object v0, p1

    .line 2695633
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2695634
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2695635
    const-string v0, "SingleSongComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2695636
    if-ne p0, p1, :cond_1

    .line 2695637
    :cond_0
    :goto_0
    return v0

    .line 2695638
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2695639
    goto :goto_0

    .line 2695640
    :cond_3
    check-cast p1, LX/JSb;

    .line 2695641
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2695642
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2695643
    if-eq v2, v3, :cond_0

    .line 2695644
    iget-object v2, p0, LX/JSb;->a:LX/JSe;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JSb;->a:LX/JSe;

    iget-object v3, p1, LX/JSb;->a:LX/JSe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2695645
    goto :goto_0

    .line 2695646
    :cond_5
    iget-object v2, p1, LX/JSb;->a:LX/JSe;

    if-nez v2, :cond_4

    .line 2695647
    :cond_6
    iget-object v2, p0, LX/JSb;->b:LX/JTY;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JSb;->b:LX/JTY;

    iget-object v3, p1, LX/JSb;->b:LX/JTY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2695648
    goto :goto_0

    .line 2695649
    :cond_8
    iget-object v2, p1, LX/JSb;->b:LX/JTY;

    if-nez v2, :cond_7

    .line 2695650
    :cond_9
    iget-object v2, p0, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2695651
    goto :goto_0

    .line 2695652
    :cond_b
    iget-object v2, p1, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_a

    .line 2695653
    :cond_c
    iget-object v2, p0, LX/JSb;->d:LX/1Pn;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JSb;->d:LX/1Pn;

    iget-object v3, p1, LX/JSb;->d:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2695654
    goto :goto_0

    .line 2695655
    :cond_d
    iget-object v2, p1, LX/JSb;->d:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
