.class public LX/Hvp;
.super LX/1aq;
.source ""


# instance fields
.field private a:LX/Hvo;

.field private b:I

.field private c:I

.field private d:J

.field private e:J

.field private f:F


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2519958
    invoke-direct {p0, p1}, LX/1aq;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2519959
    sget-object v0, LX/Hvo;->NONE:LX/Hvo;

    iput-object v0, p0, LX/Hvp;->a:LX/Hvo;

    .line 2519960
    iput v2, p0, LX/Hvp;->b:I

    .line 2519961
    iput v1, p0, LX/Hvp;->c:I

    .line 2519962
    const/4 v0, 0x0

    iput v0, p0, LX/Hvp;->f:F

    .line 2519963
    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Only supports toggling 2 drawables"

    invoke-static {v0, v3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 2519964
    array-length v3, p1

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v0, p1, v2

    .line 2519965
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 2519966
    instance-of v4, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_0

    .line 2519967
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    .line 2519968
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 2519969
    goto :goto_0

    .line 2519970
    :cond_2
    return-void
.end method

.method public static a(LX/Hvp;II)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    .line 2519940
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2519941
    iget-object v1, p0, LX/Hvp;->a:LX/Hvo;

    sget-object v4, LX/Hvo;->NONE:LX/Hvo;

    if-eq v1, v4, :cond_0

    iget-wide v4, p0, LX/Hvp;->e:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 2519942
    :cond_0
    iget-object v1, p0, LX/Hvp;->a:LX/Hvo;

    sget-object v4, LX/Hvo;->NONE:LX/Hvo;

    if-eq v1, v4, :cond_1

    .line 2519943
    invoke-direct {p0}, LX/Hvp;->b()V

    .line 2519944
    :cond_1
    iget v1, p0, LX/Hvp;->b:I

    if-ne p2, v1, :cond_2

    .line 2519945
    :goto_0
    return-void

    .line 2519946
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, LX/Hvp;->f:F

    .line 2519947
    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/Hvp;->e:J

    .line 2519948
    :goto_1
    if-ne p2, v0, :cond_5

    :goto_2
    iput v0, p0, LX/Hvp;->c:I

    .line 2519949
    iget-wide v0, p0, LX/Hvp;->e:J

    int-to-long v2, p1

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/Hvp;->d:J

    .line 2519950
    sget-object v0, LX/Hvo;->RUNNING:LX/Hvo;

    iput-object v0, p0, LX/Hvp;->a:LX/Hvo;

    .line 2519951
    invoke-virtual {p0}, LX/Hvp;->invalidateSelf()V

    goto :goto_0

    .line 2519952
    :cond_3
    iget v1, p0, LX/Hvp;->b:I

    iget v4, p0, LX/Hvp;->c:I

    add-int/2addr v1, v4

    invoke-direct {p0, v1}, LX/Hvp;->e(I)I

    move-result v1

    if-ne v1, p2, :cond_4

    .line 2519953
    int-to-float v1, p1

    iget v4, p0, LX/Hvp;->f:F

    sub-float v4, v6, v4

    mul-float/2addr v1, v4

    long-to-float v2, v2

    add-float/2addr v1, v2

    float-to-long v2, v1

    iput-wide v2, p0, LX/Hvp;->e:J

    goto :goto_1

    .line 2519954
    :cond_4
    iget v1, p0, LX/Hvp;->f:F

    sub-float v1, v6, v1

    iput v1, p0, LX/Hvp;->f:F

    .line 2519955
    iget v1, p0, LX/Hvp;->b:I

    iget v4, p0, LX/Hvp;->c:I

    add-int/2addr v1, v4

    invoke-direct {p0, v1}, LX/Hvp;->e(I)I

    move-result v1

    iput v1, p0, LX/Hvp;->b:I

    .line 2519956
    int-to-float v1, p1

    iget v4, p0, LX/Hvp;->f:F

    sub-float v4, v6, v4

    mul-float/2addr v1, v4

    long-to-float v2, v2

    add-float/2addr v1, v2

    float-to-long v2, v1

    iput-wide v2, p0, LX/Hvp;->e:J

    goto :goto_1

    .line 2519957
    :cond_5
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2519936
    iget-object v0, p0, LX/Hvp;->a:LX/Hvo;

    sget-object v1, LX/Hvo;->RUNNING:LX/Hvo;

    if-ne v0, v1, :cond_0

    .line 2519937
    sget-object v0, LX/Hvo;->NONE:LX/Hvo;

    iput-object v0, p0, LX/Hvp;->a:LX/Hvo;

    .line 2519938
    iget v0, p0, LX/Hvp;->b:I

    iget v1, p0, LX/Hvp;->c:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/Hvp;->e(I)I

    move-result v0

    iput v0, p0, LX/Hvp;->b:I

    .line 2519939
    :cond_0
    return-void
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 2519900
    invoke-virtual {p0}, LX/1aq;->a()I

    move-result v0

    .line 2519901
    add-int v1, p1, v0

    rem-int v0, v1, v0

    return v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/high16 v9, -0x3ccc0000    # -180.0f

    const/4 v1, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    .line 2519902
    sget-object v0, LX/Hvn;->a:[I

    iget-object v2, p0, LX/Hvp;->a:LX/Hvo;

    invoke-virtual {v2}, LX/Hvo;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 2519903
    :goto_0
    if-eqz v0, :cond_3

    .line 2519904
    invoke-direct {p0}, LX/Hvp;->b()V

    .line 2519905
    iget v0, p0, LX/Hvp;->b:I

    invoke-virtual {p0, v0}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2519906
    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2519907
    if-eqz v0, :cond_1

    .line 2519908
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2519909
    :cond_1
    :goto_1
    return-void

    .line 2519910
    :pswitch_0
    iget-wide v2, p0, LX/Hvp;->d:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    .line 2519911
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LX/Hvp;->d:J

    sub-long/2addr v2, v4

    long-to-float v0, v2

    iget-wide v2, p0, LX/Hvp;->e:J

    iget-wide v4, p0, LX/Hvp;->d:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, p0, LX/Hvp;->f:F

    .line 2519912
    iget v0, p0, LX/Hvp;->f:F

    cmpl-float v0, v0, v8

    if-ltz v0, :cond_2

    move v0, v1

    .line 2519913
    :goto_2
    iget v2, p0, LX/Hvp;->f:F

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, p0, LX/Hvp;->f:F

    goto :goto_0

    .line 2519914
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2519915
    :cond_3
    iget v0, p0, LX/Hvp;->b:I

    invoke-virtual {p0, v0}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2519916
    iget v2, p0, LX/Hvp;->b:I

    iget v3, p0, LX/Hvp;->c:I

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, LX/Hvp;->e(I)I

    move-result v2

    invoke-virtual {p0, v2}, LX/1aq;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2519917
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 2519918
    const/high16 v3, 0x437f0000    # 255.0f

    iget v4, p0, LX/Hvp;->f:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 2519919
    rsub-int v4, v3, 0xff

    .line 2519920
    invoke-virtual {p0}, LX/Hvp;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    .line 2519921
    invoke-virtual {p0}, LX/Hvp;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    .line 2519922
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2519923
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2519924
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2519925
    iget v4, p0, LX/Hvp;->c:I

    const/4 v7, -0x1

    if-ne v4, v7, :cond_4

    .line 2519926
    iget v4, p0, LX/Hvp;->f:F

    mul-float/2addr v4, v9

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2519927
    :cond_4
    iget v4, p0, LX/Hvp;->f:F

    sub-float v4, v8, v4

    iget v7, p0, LX/Hvp;->f:F

    sub-float v7, v8, v7

    invoke-virtual {p1, v4, v7, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 2519928
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2519929
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2519930
    iget v0, p0, LX/Hvp;->c:I

    if-ne v0, v1, :cond_5

    .line 2519931
    iget v0, p0, LX/Hvp;->f:F

    sub-float v0, v8, v0

    mul-float/2addr v0, v9

    invoke-virtual {p1, v0, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2519932
    :cond_5
    iget v0, p0, LX/Hvp;->f:F

    iget v1, p0, LX/Hvp;->f:F

    invoke-virtual {p1, v0, v1, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 2519933
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2519934
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2519935
    invoke-virtual {p0}, LX/Hvp;->invalidateSelf()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
