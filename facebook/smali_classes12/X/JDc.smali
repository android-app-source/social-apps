.class public final LX/JDc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2664299
    iput-object p1, p0, LX/JDc;->d:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iput-object p2, p0, LX/JDc;->a:LX/0Px;

    iput-object p3, p0, LX/JDc;->b:Ljava/lang/String;

    iput-object p4, p0, LX/JDc;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2664300
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2664301
    iget-object v1, p0, LX/JDc;->d:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    .line 2664302
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h:Landroid/app/AlertDialog;

    .line 2664303
    iget-object v1, p0, LX/JDc;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    iget-object v0, p0, LX/JDc;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    .line 2664304
    :cond_0
    if-nez v0, :cond_1

    .line 2664305
    iget-object v0, p0, LX/JDc;->d:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f:LX/03V;

    const-string v1, "TimelineCollectionPlusButton"

    const-string v2, "Could not add item to collection as no valid collection was found"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2664306
    :goto_0
    return-void

    .line 2664307
    :cond_1
    iget-object v1, p0, LX/JDc;->d:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iget-object v2, p0, LX/JDc;->b:Ljava/lang/String;

    iget-object v3, p0, LX/JDc;->c:Ljava/lang/String;

    .line 2664308
    invoke-static {v1, v0, v2, v3}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a$redex0(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;LX/1oP;Ljava/lang/String;Ljava/lang/String;)V

    .line 2664309
    goto :goto_0
.end method
