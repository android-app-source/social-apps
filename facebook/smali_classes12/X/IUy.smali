.class public final LX/IUy;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IUz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IV1;

.field public b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;


# direct methods
.method public constructor <init>(LX/IV1;)V
    .locals 0

    .prologue
    .line 2581207
    iput-object p1, p0, LX/IUy;->a:LX/IV1;

    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2581208
    return-void
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2581183
    iget-object v1, p0, LX/IUy;->a:LX/IV1;

    iget-object v1, v1, LX/IV1;->d:LX/0Uh;

    const/16 v2, 0x318

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IUy;->a:LX/IV1;

    iget-object v1, v1, LX/IV1;->e:LX/3my;

    iget-object v2, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 2581184
    sget-object v0, LX/IV0;->SUGGESTION_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2581185
    new-instance v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    iget-object v0, p0, LX/IUy;->a:LX/IV1;

    invoke-virtual {v0}, LX/IV1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;-><init>(Landroid/content/Context;)V

    .line 2581186
    new-instance v0, LX/IUz;

    invoke-direct {v0, v1}, LX/IUz;-><init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;)V

    .line 2581187
    :goto_0
    return-object v0

    .line 2581188
    :cond_0
    sget-object v0, LX/IV0;->DISCOVER_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2581189
    new-instance v0, LX/IUz;

    iget-object v1, p0, LX/IUy;->a:LX/IV1;

    .line 2581190
    new-instance v2, LX/1De;

    invoke-virtual {v1}, LX/IV1;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2581191
    new-instance v3, Lcom/facebook/components/ComponentView;

    invoke-direct {v3, v2}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    .line 2581192
    iget-object v4, v1, LX/IV1;->a:LX/3ma;

    invoke-virtual {v4, v2}, LX/3ma;->c(LX/1De;)LX/DCu;

    move-result-object v4

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, LX/DCu;->h(I)LX/DCu;

    move-result-object v4

    invoke-static {v2, v4}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v2

    invoke-virtual {v2}, LX/1me;->b()LX/1dV;

    move-result-object v2

    .line 2581193
    invoke-virtual {v3, v2}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2581194
    move-object v1, v3

    .line 2581195
    invoke-direct {v0, v1}, LX/IUz;-><init>(Lcom/facebook/components/ComponentView;)V

    goto :goto_0

    .line 2581196
    :cond_1
    sget-object v0, LX/IV0;->VISIT_COMMUNITY_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2581197
    new-instance v0, LX/IUz;

    iget-object v1, p0, LX/IUy;->a:LX/IV1;

    iget-object v2, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    .line 2581198
    new-instance v4, LX/1De;

    invoke-virtual {v1}, LX/IV1;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v4, v3}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2581199
    new-instance p0, Lcom/facebook/components/ComponentView;

    invoke-direct {p0, v4}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    .line 2581200
    iget-object v3, v1, LX/IV1;->b:LX/3mb;

    invoke-virtual {v3, v4}, LX/3mb;->c(LX/1De;)LX/DCx;

    move-result-object v3

    const/4 p1, 0x1

    invoke-virtual {v3, p1}, LX/DCx;->h(I)LX/DCx;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/DCx;->b(Ljava/lang/String;)LX/DCx;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->m()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/DCx;->c(Ljava/lang/String;)LX/DCx;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$ParentGroupModel$CoverPhotoModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {p1, v3}, LX/DCx;->d(Ljava/lang/String;)LX/DCx;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-static {v4, v3}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v3

    invoke-virtual {v3}, LX/1me;->b()LX/1dV;

    move-result-object v3

    .line 2581201
    invoke-virtual {p0, v3}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2581202
    move-object v1, p0

    .line 2581203
    invoke-direct {v0, v1}, LX/IUz;-><init>(Lcom/facebook/components/ComponentView;)V

    goto/16 :goto_0

    .line 2581204
    :cond_2
    const-class v0, LX/IV1;

    const-string v1, "Trying to create a view for an unsupported view type: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2581205
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2581206
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2581178
    check-cast p1, LX/IUz;

    .line 2581179
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 2581180
    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    .line 2581181
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-virtual {v1, v0}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;)V

    .line 2581182
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2581172
    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-static {v0}, LX/IV1;->b(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2581173
    invoke-direct {p0}, LX/IUy;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581174
    sget-object v0, LX/IV0;->VISIT_COMMUNITY_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    .line 2581175
    :goto_0
    return v0

    .line 2581176
    :cond_0
    sget-object v0, LX/IV0;->DISCOVER_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2581177
    :cond_1
    sget-object v0, LX/IV0;->SUGGESTION_CARD_VIEW_TYPE:LX/IV0;

    invoke-virtual {v0}, LX/IV0;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2581169
    iget-object v0, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-static {v0}, LX/IV1;->b(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;)I

    move-result v0

    .line 2581170
    iget-object v1, p0, LX/IUy;->b:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;

    invoke-static {v1}, LX/IV1;->b(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel;)I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, LX/IUy;->a:LX/IV1;

    iget-object v1, v1, LX/IV1;->c:LX/DN3;

    invoke-virtual {v1}, LX/DN3;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2581171
    if-nez v1, :cond_0

    invoke-direct {p0}, LX/IUy;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    :cond_1
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
