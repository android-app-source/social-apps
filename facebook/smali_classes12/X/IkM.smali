.class public LX/IkM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1zC;


# direct methods
.method public constructor <init>(LX/1zC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606643
    iput-object p1, p0, LX/IkM;->a:LX/1zC;

    .line 2606644
    return-void
.end method

.method public static a(LX/0QB;)LX/IkM;
    .locals 4

    .prologue
    .line 2606645
    const-class v1, LX/IkM;

    monitor-enter v1

    .line 2606646
    :try_start_0
    sget-object v0, LX/IkM;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606647
    sput-object v2, LX/IkM;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606648
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606649
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606650
    new-instance p0, LX/IkM;

    invoke-static {v0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v3

    check-cast v3, LX/1zC;

    invoke-direct {p0, v3}, LX/IkM;-><init>(LX/1zC;)V

    .line 2606651
    move-object v0, p0

    .line 2606652
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606653
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606654
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606655
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
