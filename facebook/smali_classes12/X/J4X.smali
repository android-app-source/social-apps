.class public final LX/J4X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;I)V
    .locals 0

    .prologue
    .line 2644187
    iput-object p1, p0, LX/J4X;->b:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iput p2, p0, LX/J4X;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2644188
    iget-object v0, p0, LX/J4X;->b:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a:LX/J4p;

    const/16 v1, 0xa

    iget v2, p0, LX/J4X;->a:I

    iget-object v3, p0, LX/J4X;->b:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644189
    iget-object v4, v3, LX/J4j;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2644190
    if-nez v4, :cond_0

    .line 2644191
    iget-object v4, v0, LX/J4p;->a:LX/J5g;

    .line 2644192
    iget-object v5, v3, LX/J4j;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2644193
    new-instance v6, LX/J4t;

    invoke-direct {v6}, LX/J4t;-><init>()V

    move-object v6, v6

    .line 2644194
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2644195
    new-instance v7, LX/J4t;

    invoke-direct {v7}, LX/J4t;-><init>()V

    const-string v8, "first_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "cover_photo_height"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "cover_photo_width"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "checkup_type"

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "header_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p0

    invoke-virtual {v7, v8, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v7

    .line 2644196
    iget-object v8, v4, LX/J5g;->b:LX/0se;

    invoke-virtual {v8, v7}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 2644197
    iget-object v8, v4, LX/J5g;->c:LX/0tG;

    invoke-virtual {v8, v7}, LX/0tG;->a(LX/0gW;)V

    .line 2644198
    iget-object v8, v4, LX/J5g;->d:LX/0tI;

    invoke-virtual {v8, v7}, LX/0tI;->a(LX/0gW;)V

    .line 2644199
    iget-object v8, v7, LX/0gW;->e:LX/0w7;

    move-object v7, v8

    .line 2644200
    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2644201
    iget-object v7, v4, LX/J5g;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    move-object v4, v6

    .line 2644202
    new-instance v5, LX/J4n;

    invoke-direct {v5, v3}, LX/J4n;-><init>(LX/J4j;)V

    iget-object v6, v0, LX/J4p;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2644203
    :goto_0
    move-object v0, v4

    .line 2644204
    return-object v0

    :cond_0
    iget-object v5, v0, LX/J4p;->a:LX/J5g;

    .line 2644205
    iget-object v6, v3, LX/J4j;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2644206
    new-instance v7, LX/J4s;

    invoke-direct {v7}, LX/J4s;-><init>()V

    move-object v7, v7

    .line 2644207
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    .line 2644208
    new-instance v8, LX/J4s;

    invoke-direct {v8}, LX/J4s;-><init>()V

    const-string p0, "first_count"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, p0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string p0, "after_cursor"

    invoke-virtual {v8, p0, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string p0, "checkup_type"

    invoke-virtual {v8, p0, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    .line 2644209
    iget-object p0, v5, LX/J5g;->b:LX/0se;

    invoke-virtual {p0, v8}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 2644210
    iget-object p0, v5, LX/J5g;->c:LX/0tG;

    invoke-virtual {p0, v8}, LX/0tG;->a(LX/0gW;)V

    .line 2644211
    iget-object p0, v5, LX/J5g;->d:LX/0tI;

    invoke-virtual {p0, v8}, LX/0tI;->a(LX/0gW;)V

    .line 2644212
    iget-object p0, v8, LX/0gW;->e:LX/0w7;

    move-object v8, p0

    .line 2644213
    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2644214
    iget-object v8, v5, LX/J5g;->a:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    move-object v4, v7

    .line 2644215
    new-instance v5, LX/J4m;

    invoke-direct {v5, v3}, LX/J4m;-><init>(LX/J4j;)V

    iget-object v6, v0, LX/J4p;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_0
.end method
