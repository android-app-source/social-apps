.class public final LX/I7L;
.super LX/8wT;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538752
    iput-object p1, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/8wT;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2538753
    check-cast p1, LX/8wS;

    .line 2538754
    iget-object v0, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aH:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;

    if-eqz v0, :cond_0

    .line 2538755
    iget-object v0, p1, LX/8wS;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2538756
    iget-object v1, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aH:Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;

    invoke-virtual {v1}, Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel$BoostableStoryModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538757
    iget-object v0, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    new-instance v1, LX/7qN;

    invoke-direct {v1}, LX/7qN;-><init>()V

    iget-object v1, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2538758
    new-instance v3, LX/7qN;

    invoke-direct {v3}, LX/7qN;-><init>()V

    .line 2538759
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->t()Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 2538760
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ak()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    .line 2538761
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->al()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$AlbumModel;

    .line 2538762
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    .line 2538763
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->v()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->e:Z

    .line 2538764
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->w()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->f:Z

    .line 2538765
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->an()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->g:Z

    .line 2538766
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->k()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->h:Z

    .line 2538767
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->x()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->i:Z

    .line 2538768
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->j:Z

    .line 2538769
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->k:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2538770
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ao()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    .line 2538771
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ap()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    .line 2538772
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->z()J

    move-result-wide v5

    iput-wide v5, v3, LX/7qN;->n:J

    .line 2538773
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->b()J

    move-result-wide v5

    iput-wide v5, v3, LX/7qN;->o:J

    .line 2538774
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aq()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    .line 2538775
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ar()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2538776
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->B()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->r:Ljava/lang/String;

    .line 2538777
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->as()I

    move-result v4

    iput v4, v3, LX/7qN;->s:I

    .line 2538778
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->t:Ljava/lang/String;

    .line 2538779
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->at()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->u:Ljava/lang/String;

    .line 2538780
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->au()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCategoryInfoFragmentModel$EventCategoryInfoModel;

    .line 2538781
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->av()Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->w:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    .line 2538782
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->x:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    .line 2538783
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ax()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 2538784
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    .line 2538785
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->A:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    .line 2538786
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    .line 2538787
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    .line 2538788
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aC()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    .line 2538789
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->E:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    .line 2538790
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->F:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2538791
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->G:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    .line 2538792
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->H:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    .line 2538793
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aG()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->I:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 2538794
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->J()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->J:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2538795
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->K:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 2538796
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->L:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    .line 2538797
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->M:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    .line 2538798
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->N:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    .line 2538799
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->O:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    .line 2538800
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ad()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->P:Ljava/lang/String;

    .line 2538801
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->L()Lcom/facebook/graphql/enums/GraphQLEventType;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->Q:Lcom/facebook/graphql/enums/GraphQLEventType;

    .line 2538802
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ae()I

    move-result v4

    iput v4, v3, LX/7qN;->R:I

    .line 2538803
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->af()I

    move-result v4

    iput v4, v3, LX/7qN;->S:I

    .line 2538804
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->T:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    .line 2538805
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->N()Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->U:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    .line 2538806
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aM()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->V:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventWatchersModel;

    .line 2538807
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aN()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->W:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$FriendEventInviteesFirst5Model;

    .line 2538808
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aO()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->X:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 2538809
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aP()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->Y:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 2538810
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aQ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->Z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 2538811
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->aa:Ljava/lang/String;

    .line 2538812
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eQ_()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->ab:Z

    .line 2538813
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->R()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->ac:Z

    .line 2538814
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->n()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->ad:Z

    .line 2538815
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aR()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->ae:Z

    .line 2538816
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->S()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->af:Z

    .line 2538817
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aS()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ag:Ljava/lang/String;

    .line 2538818
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aT()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ah:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MaxTicketPriceModel;

    .line 2538819
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aU()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ai:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$MinTicketPriceModel;

    .line 2538820
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->aj:Ljava/lang/String;

    .line 2538821
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aV()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ak:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    .line 2538822
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aW()I

    move-result v4

    iput v4, v3, LX/7qN;->al:I

    .line 2538823
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->U()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->am:Z

    .line 2538824
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aX()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2538825
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aY()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ao:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    .line 2538826
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->o()J

    move-result-wide v5

    iput-wide v5, v3, LX/7qN;->ap:J

    .line 2538827
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->j()J

    move-result-wide v5

    iput-wide v5, v3, LX/7qN;->aq:J

    .line 2538828
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aZ()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ar:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 2538829
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ba()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->as:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$SupportedTaggableActivitiesModel;

    .line 2538830
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ai()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->at:Z

    .line 2538831
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->au:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    .line 2538832
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->p()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->av:Ljava/lang/String;

    .line 2538833
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->X()I

    move-result v4

    iput v4, v3, LX/7qN;->aw:I

    .line 2538834
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->ax:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2538835
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r()Z

    move-result v4

    iput-boolean v4, v3, LX/7qN;->ay:Z

    .line 2538836
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Y()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->az:LX/0Px;

    .line 2538837
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->aA:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2538838
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    iput-object v4, v3, LX/7qN;->aB:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2538839
    move-object v1, v3

    .line 2538840
    iget-object v2, p1, LX/8wS;->b:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-object v2, v2

    .line 2538841
    iput-object v2, v1, LX/7qN;->K:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    .line 2538842
    move-object v1, v1

    .line 2538843
    invoke-virtual {v1}, LX/7qN;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2538844
    iget-object v0, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v1

    .line 2538845
    iput-object v1, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    .line 2538846
    iget-object v0, p0, LX/I7L;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->c(Lcom/facebook/events/permalink/EventPermalinkFragment;Z)V

    .line 2538847
    :cond_0
    return-void
.end method
