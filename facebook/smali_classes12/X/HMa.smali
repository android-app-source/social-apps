.class public LX/HMa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/17Y;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457367
    iput-object p1, p0, LX/HMa;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2457368
    iput-object p2, p0, LX/HMa;->b:LX/17Y;

    .line 2457369
    return-void
.end method

.method public static a(LX/0QB;)LX/HMa;
    .locals 1

    .prologue
    .line 2457370
    invoke-static {p0}, LX/HMa;->b(LX/0QB;)LX/HMa;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/HMa;
    .locals 3

    .prologue
    .line 2457371
    new-instance v2, LX/HMa;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-direct {v2, v0, v1}, LX/HMa;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 2457372
    return-object v2
.end method

.method private static b(LX/HMa;Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2457373
    iget-object v0, p0, LX/HMa;->b:LX/17Y;

    sget-object v1, LX/0ax;->az:Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p5}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2457374
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2457375
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2457376
    const-string v2, "profile_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457377
    const-string v2, "extra_page_presence_tab_type"

    invoke-interface {p5}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457378
    const-string v2, "extra_page_presence_tab_content_type"

    invoke-interface {p5}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457379
    const-string v2, "extra_page_presence_tab_reaction_surface"

    invoke-interface {p5}, LX/9Y7;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457380
    const-string v2, "page_fragment_uuid"

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-direct {v3, p0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2457381
    invoke-interface {p5}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->REACTION_SURFACE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-ne v2, v3, :cond_1

    .line 2457382
    const-string v2, "arg_pages_surface_reaction_surface"

    invoke-static {p5}, LX/HQB;->a(LX/9Y7;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2457383
    invoke-interface {p5}, LX/9Y7;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2457384
    const-string v2, "source_name"

    invoke-interface {p5}, LX/9Y7;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457385
    :cond_0
    :goto_0
    move-object v1, v1

    .line 2457386
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2457387
    return-object v0

    .line 2457388
    :cond_1
    sget-object v2, LX/HMZ;->a:[I

    invoke-interface {p5}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2457389
    :pswitch_0
    const-string v2, "extra_ref_module"

    const-string v3, "pages_public_view"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457390
    const-string v2, "event_ref_mechanism"

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_EDIT_PAGE:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2457391
    :pswitch_1
    invoke-interface {p5}, LX/9Y7;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2457392
    const-string v2, "source_name"

    invoke-interface {p5}, LX/9Y7;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)V
    .locals 4

    .prologue
    .line 2457393
    iget-object v0, p0, LX/HMa;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p5}, LX/HMa;->b(LX/HMa;Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x2783

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2457394
    return-void
.end method

.method public final a(Landroid/app/Activity;JLjava/lang/String;LX/9Y7;Ljava/lang/String;Z)V
    .locals 4
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2457395
    invoke-static/range {p0 .. p5}, LX/HMa;->b(LX/HMa;Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)Landroid/content/Intent;

    move-result-object v0

    .line 2457396
    const-string v1, "extra_launched_from_deeplink"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2457397
    const-string v1, "extra_is_admin"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2457398
    if-eqz p6, :cond_0

    .line 2457399
    const-string v1, "page_profile_pic_url_extra"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2457400
    :cond_0
    iget-object v1, p0, LX/HMa;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2457401
    return-void
.end method
