.class public final LX/JIL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;",
        ">;",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

.field public final synthetic b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)V
    .locals 0

    .prologue
    .line 2677788
    iput-object p1, p0, LX/JIL;->b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    iput-object p2, p0, LX/JIL;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2677789
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2677790
    iget-object v0, p0, LX/JIL;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v0}, LX/FT2;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/FT2;

    move-result-object v2

    if-nez p1, :cond_0

    move-object v0, v1

    .line 2677791
    :goto_0
    iput-object v0, v2, LX/FT2;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2677792
    move-object v2, v2

    .line 2677793
    if-nez p1, :cond_1

    .line 2677794
    :goto_1
    iput-object v1, v2, LX/FT2;->f:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    .line 2677795
    move-object v0, v2

    .line 2677796
    invoke-virtual {v0}, LX/FT2;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v0

    return-object v0

    .line 2677797
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677798
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    goto :goto_0

    .line 2677799
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677800
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;->j()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$SelectedTagsCountFragmentModel$SelectedTagsModel;

    move-result-object v1

    goto :goto_1
.end method
