.class public final LX/HSi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/HSk;


# direct methods
.method public constructor <init>(LX/HSk;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2467444
    iput-object p1, p0, LX/HSi;->b:LX/HSk;

    iput-object p2, p0, LX/HSi;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2467445
    new-instance v0, LX/7oI;

    invoke-direct {v0}, LX/7oI;-><init>()V

    .line 2467446
    const-string v1, "event_id"

    iget-object v2, p0, LX/HSi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2467447
    const-string v1, "profile_image_size"

    iget-object v2, p0, LX/HSi;->b:LX/HSk;

    iget v2, v2, LX/HSk;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2467448
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2467449
    iget-object v1, p0, LX/HSi;->b:LX/HSk;

    iget-object v1, v1, LX/HSk;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
