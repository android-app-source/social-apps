.class public final LX/Hrn;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/PostCompositionView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/PostCompositionView;)V
    .locals 0

    .prologue
    .line 2513516
    iput-object p1, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/composer/activity/PostCompositionView;B)V
    .locals 0

    .prologue
    .line 2513517
    invoke-direct {p0, p1}, LX/Hrn;-><init>(Lcom/facebook/composer/activity/PostCompositionView;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 2

    .prologue
    .line 2513518
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2513519
    iget-object v1, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    iget-boolean v1, v1, Lcom/facebook/composer/activity/PostCompositionView;->e:Z

    if-eqz v1, :cond_0

    .line 2513520
    iget-object v1, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/activity/PostCompositionView;->setAlpha(F)V

    .line 2513521
    :cond_0
    iget-object v0, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    iget-object v1, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    iget v1, v1, Lcom/facebook/composer/activity/PostCompositionView;->f:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/activity/PostCompositionView;->setY(F)V

    .line 2513522
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 2513523
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2513524
    iget-object v0, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/composer/activity/PostCompositionView;->setVisibility(I)V

    .line 2513525
    :cond_0
    iget-object v0, p0, LX/Hrn;->a:Lcom/facebook/composer/activity/PostCompositionView;

    const/4 v1, 0x0

    .line 2513526
    iput-boolean v1, v0, Lcom/facebook/composer/activity/PostCompositionView;->e:Z

    .line 2513527
    return-void
.end method
