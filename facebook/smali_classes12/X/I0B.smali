.class public LX/I0B;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation


# instance fields
.field private a:LX/0Uh;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/0Or;)V
    .locals 3
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2526612
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2526613
    iput-object p1, p0, LX/I0B;->a:LX/0Uh;

    .line 2526614
    iput-object p2, p0, LX/I0B;->b:LX/0Or;

    .line 2526615
    const-string v0, "upcoming_birthdays?start_date=%s"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{birthday_view_start_date unset}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/I0A;

    invoke-direct {v1, p0}, LX/I0A;-><init>(LX/I0B;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2526616
    return-void
.end method

.method public static b(LX/0QB;)LX/I0B;
    .locals 3

    .prologue
    .line 2526617
    new-instance v1, LX/I0B;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v2, 0xc

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/I0B;-><init>(LX/0Uh;LX/0Or;)V

    .line 2526618
    return-object v1
.end method
