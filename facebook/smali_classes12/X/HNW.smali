.class public LX/HNW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0tX;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458589
    iput-object p1, p0, LX/HNW;->a:Landroid/content/Context;

    .line 2458590
    iput-object p2, p0, LX/HNW;->b:LX/0tX;

    .line 2458591
    return-void
.end method

.method public static a(LX/0QB;)LX/HNW;
    .locals 5

    .prologue
    .line 2458538
    const-class v1, LX/HNW;

    monitor-enter v1

    .line 2458539
    :try_start_0
    sget-object v0, LX/HNW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2458540
    sput-object v2, LX/HNW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2458541
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2458542
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2458543
    new-instance p0, LX/HNW;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/HNW;-><init>(Landroid/content/Context;LX/0tX;)V

    .line 2458544
    move-object v0, p0

    .line 2458545
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2458546
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HNW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2458547
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2458548
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/CYE;",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelAddToOrderingMutationModels$PageActionChannelAddToOrderingMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458574
    new-instance v0, LX/4Hc;

    invoke-direct {v0}, LX/4Hc;-><init>()V

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v0

    .line 2458575
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2458576
    invoke-virtual {v0, p5}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2458577
    :cond_0
    new-instance v1, LX/4HX;

    invoke-direct {v1}, LX/4HX;-><init>()V

    .line 2458578
    iget-object v2, p3, LX/CYE;->mActionChannelType:Ljava/lang/String;

    move-object v2, v2

    .line 2458579
    invoke-virtual {v1, v2}, LX/4HX;->b(Ljava/lang/String;)LX/4HX;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4HX;->a(LX/4Hc;)LX/4HX;

    move-result-object v0

    .line 2458580
    iget v1, p3, LX/CYE;->mActionPosition:I

    move v1, v1

    .line 2458581
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2458582
    const-string v2, "position"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2458583
    move-object v0, v0

    .line 2458584
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4HX;->a(Ljava/lang/String;)LX/4HX;

    move-result-object v0

    .line 2458585
    invoke-static {}, LX/9XZ;->a()LX/9XY;

    move-result-object v1

    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9XY;

    .line 2458586
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2458587
    iget-object v1, p0, LX/HNW;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/CYE;",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionChannelReplaceInOrderingMutationModels$PageActionChannelReplaceInOrderingMutationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458549
    new-instance v0, LX/4Hc;

    invoke-direct {v0}, LX/4Hc;-><init>()V

    .line 2458550
    iget-object v1, p3, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v1, v1

    .line 2458551
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v0

    .line 2458552
    iget-object v1, p3, LX/CYE;->mActionId:Ljava/lang/String;

    move-object v1, v1

    .line 2458553
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2458554
    iget-object v1, p3, LX/CYE;->mActionId:Ljava/lang/String;

    move-object v1, v1

    .line 2458555
    invoke-virtual {v0, v1}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2458556
    :cond_0
    new-instance v1, LX/4Hc;

    invoke-direct {v1}, LX/4Hc;-><init>()V

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    move-result-object v1

    .line 2458557
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2458558
    invoke-virtual {v1, p5}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2458559
    :cond_1
    new-instance v2, LX/4Hb;

    invoke-direct {v2}, LX/4Hb;-><init>()V

    .line 2458560
    iget-object v3, p3, LX/CYE;->mActionChannelType:Ljava/lang/String;

    move-object v3, v3

    .line 2458561
    const-string v4, "channel_type"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458562
    move-object v2, v2

    .line 2458563
    const-string v3, "action_to_remove"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2458564
    move-object v0, v2

    .line 2458565
    const-string v2, "action_to_add"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2458566
    move-object v0, v0

    .line 2458567
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2458568
    const-string v2, "page_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458569
    move-object v0, v0

    .line 2458570
    new-instance v1, LX/9Xw;

    invoke-direct {v1}, LX/9Xw;-><init>()V

    move-object v1, v1

    .line 2458571
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9Xw;

    .line 2458572
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2458573
    iget-object v1, p0, LX/HNW;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
