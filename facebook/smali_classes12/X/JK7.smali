.class public LX/JK7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/342;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/01U;


# direct methods
.method public constructor <init>(LX/0ad;LX/01U;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680229
    iput-object p1, p0, LX/JK7;->b:LX/0ad;

    .line 2680230
    iput-object p2, p0, LX/JK7;->c:LX/01U;

    .line 2680231
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/react/bridge/WritableNativeMap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2680232
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2680233
    const-string v2, "GCTimers"

    iget-object v3, p0, LX/JK7;->b:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-short v5, LX/347;->F:S

    invoke-interface {v3, v4, v5, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/react/bridge/WritableNativeMap;->putBoolean(Ljava/lang/String;Z)V

    .line 2680234
    const-string v2, "CacheBytecode"

    iget-object v3, p0, LX/JK7;->b:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-short v5, LX/347;->B:S

    invoke-interface {v3, v4, v5, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/react/bridge/WritableNativeMap;->putBoolean(Ljava/lang/String;Z)V

    .line 2680235
    const-string v2, "VerifyBytecode"

    .line 2680236
    sget-boolean v3, LX/007;->i:Z

    move v3, v3

    .line 2680237
    if-eqz v3, :cond_0

    iget-object v3, p0, LX/JK7;->c:LX/01U;

    sget-object v4, LX/01U;->DEBUG:LX/01U;

    if-ne v3, v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/react/bridge/WritableNativeMap;->putBoolean(Ljava/lang/String;Z)V

    .line 2680238
    return-object v1
.end method
