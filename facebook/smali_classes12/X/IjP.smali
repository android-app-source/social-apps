.class public LX/IjP;
.super LX/6E7;
.source ""


# instance fields
.field public a:LX/73s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public e:Landroid/widget/ImageView;

.field public f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2605707
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 2605708
    const-class v0, LX/IjP;

    invoke-static {v0, p0}, LX/IjP;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605709
    const v0, 0x7f030afe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605710
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/IjP;->setOrientation(I)V

    .line 2605711
    const v0, 0x7f0d1bcc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2605712
    const v0, 0x7f0d248b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/IjP;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2605713
    const v0, 0x7f0d248c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/IjP;->e:Landroid/widget/ImageView;

    .line 2605714
    const v0, 0x7f0d248d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IjP;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2605715
    invoke-virtual {p0}, LX/IjP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2605716
    iget-object v2, p0, LX/IjP;->e:Landroid/widget/ImageView;

    iget-object v3, p0, LX/IjP;->b:LX/0wM;

    invoke-virtual {p0}, LX/IjP;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a019f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v1, v4}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2605717
    iget-object v1, p0, LX/IjP;->a:LX/73s;

    const v2, 0x7f082c3c

    const-string v3, "[[contact_us_link]]"

    invoke-virtual {p0}, LX/IjP;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082c3d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/IjP;->f:Lcom/facebook/resources/ui/FbTextView;

    const-string v6, "https://m.facebook.com/help/contact/223254857690713"

    invoke-virtual/range {v1 .. v6}, LX/73s;->a(ILjava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2605718
    return-void
.end method

.method public static a(LX/IjP;Lcom/facebook/resources/ui/FbTextView;I)V
    .locals 4
    .param p1    # Lcom/facebook/resources/ui/FbTextView;
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 2605719
    invoke-virtual {p0}, LX/IjP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2605720
    iget-object v1, p0, LX/IjP;->b:LX/0wM;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2605721
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IjP;

    invoke-static {p0}, LX/73s;->b(LX/0QB;)LX/73s;

    move-result-object v1

    check-cast v1, LX/73s;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object v1, p1, LX/IjP;->a:LX/73s;

    iput-object p0, p1, LX/IjP;->b:LX/0wM;

    return-void
.end method


# virtual methods
.method public setVisibilityOfIsPrimaryCardTextView(I)V
    .locals 1

    .prologue
    .line 2605722
    iget-object v0, p0, LX/IjP;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2605723
    return-void
.end method
