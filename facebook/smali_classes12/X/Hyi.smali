.class public final LX/Hyi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hyh;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524458
    iput-object p1, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2524459
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    if-eqz v0, :cond_0

    .line 2524460
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    .line 2524461
    iput-boolean v2, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->Q:Z

    .line 2524462
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-object v1, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H:Ljava/lang/String;

    .line 2524463
    iget-object p2, v0, LX/Hz2;->g:LX/I2o;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524464
    iget-object p2, v0, LX/Hz2;->j:LX/I2Z;

    invoke-virtual {p2, p1, v1}, LX/I2Z;->b(Ljava/util/List;Ljava/lang/String;)V

    .line 2524465
    invoke-static {v0}, LX/Hz2;->k(LX/Hz2;)V

    .line 2524466
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-virtual {v0, v2}, LX/Hz2;->b(Z)V

    .line 2524467
    :goto_0
    return-void

    .line 2524468
    :cond_0
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-object v1, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/Hz2;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 2524469
    iget-object v0, p0, LX/Hyi;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->G:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0
.end method
