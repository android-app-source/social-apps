.class public LX/JCW;
.super LX/B0V;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/0ad;

.field private final c:Z

.field private final d:Z

.field private final e:LX/J90;

.field private final f:LX/JCx;

.field public final g:LX/J8z;

.field private final h:LX/JCV;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0ad;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/J90;LX/JCx;LX/J8z;LX/JCV;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsTheWhoEnabled;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662387
    invoke-direct {p0}, LX/B0V;-><init>()V

    .line 2662388
    iput-object p1, p0, LX/JCW;->a:Ljava/lang/String;

    .line 2662389
    iput-object p2, p0, LX/JCW;->b:LX/0ad;

    .line 2662390
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/JCW;->c:Z

    .line 2662391
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/JCW;->d:Z

    .line 2662392
    iput-object p5, p0, LX/JCW;->e:LX/J90;

    .line 2662393
    iput-object p6, p0, LX/JCW;->f:LX/JCx;

    .line 2662394
    iput-object p7, p0, LX/JCW;->g:LX/J8z;

    .line 2662395
    iput-object p8, p0, LX/JCW;->h:LX/JCV;

    .line 2662396
    return-void
.end method


# virtual methods
.method public final a(LX/B0d;)LX/0zO;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2662397
    iget-object v0, p1, LX/B0d;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2662398
    iget-object v0, p0, LX/JCW;->b:LX/0ad;

    sget-short v2, LX/0wf;->aj:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2662399
    if-eqz v0, :cond_1

    .line 2662400
    iget-object v0, p0, LX/JCW;->e:LX/J90;

    iget-object v1, p0, LX/JCW;->a:Ljava/lang/String;

    iget-object v2, p1, LX/B0d;->b:Ljava/lang/String;

    .line 2662401
    new-instance v3, LX/JA1;

    invoke-direct {v3}, LX/JA1;-><init>()V

    move-object v3, v3

    .line 2662402
    const-string v4, "profile_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string p0, "num_mutual_friends"

    const-string p1, "4"

    invoke-virtual {v4, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string p0, "facepile_image_size"

    iget-object p1, v0, LX/J90;->c:LX/JAU;

    invoke-virtual {p1}, LX/JAU;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string p0, "profile_field_sections_after"

    invoke-virtual {v4, p0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662403
    move-object v0, v3

    .line 2662404
    const-string v1, "param"

    const-string v2, "profile_field_sections_after"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662405
    const-string v1, "import"

    const-string v2, "profile_field_sections_end_cursor"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662406
    const-string v1, "max_runs"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2662407
    :goto_1
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 2662408
    goto :goto_0

    .line 2662409
    :cond_1
    iget-object v0, p0, LX/JCW;->e:LX/J90;

    iget-object v1, p0, LX/JCW;->a:Ljava/lang/String;

    const/4 v2, 0x4

    iget-boolean v3, p0, LX/JCW;->c:Z

    iget-object v4, p1, LX/B0d;->b:Ljava/lang/String;

    .line 2662410
    new-instance v5, LX/JBH;

    invoke-direct {v5}, LX/JBH;-><init>()V

    move-object v6, v5

    .line 2662411
    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v10

    move-object v5, v0

    move-object v7, v1

    move v8, v2

    move v9, v3

    invoke-static/range {v5 .. v10}, LX/J90;->a(LX/J90;LX/0gW;Ljava/lang/String;IZLX/0am;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/JBH;

    move-object v0, v5

    .line 2662412
    goto :goto_1
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 23

    .prologue
    .line 2662413
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v5

    .line 2662414
    instance-of v4, v5, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    if-eqz v4, :cond_7

    .line 2662415
    check-cast v5, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    .line 2662416
    new-instance v6, LX/9JH;

    invoke-direct {v6}, LX/9JH;-><init>()V

    .line 2662417
    move-object/from16 v0, p1

    iget-object v4, v0, LX/B0d;->b:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 2662418
    invoke-virtual {v5}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v4

    invoke-static {v5}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, LX/9JH;->a(ILX/0w5;)V

    .line 2662419
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v4

    .line 2662420
    if-nez v4, :cond_1

    .line 2662421
    new-instance v4, LX/B0e;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, LX/B0e;-><init>(LX/B0d;)V

    .line 2662422
    :goto_0
    return-object v4

    .line 2662423
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    const/4 v4, 0x0

    move v9, v4

    :goto_1
    if-ge v9, v11, :cond_6

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;

    .line 2662424
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    .line 2662425
    new-instance v12, LX/9JH;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    invoke-direct {v12, v7}, LX/9JH;-><init>(I)V

    .line 2662426
    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;->a()LX/0Px;

    move-result-object v13

    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v14

    const/4 v7, 0x0

    move v8, v7

    :goto_2
    if-ge v8, v14, :cond_3

    invoke-virtual {v13, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2662427
    invoke-static {v7}, LX/JCU;->a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 2662428
    invoke-virtual {v7}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v15

    invoke-static {v7}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v7

    invoke-virtual {v12, v15, v7}, LX/9JH;->a(ILX/0w5;)V

    .line 2662429
    :cond_2
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 2662430
    :cond_3
    invoke-virtual {v12}, LX/9JH;->b()Z

    move-result v7

    if-nez v7, :cond_5

    .line 2662431
    invoke-virtual {v4}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v7

    invoke-static {v4}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v4

    const/4 v8, 0x0

    const/4 v13, 0x2

    invoke-virtual {v6, v7, v4, v8, v13}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662432
    const/4 v4, 0x0

    :goto_3
    invoke-virtual {v12}, LX/9JH;->a()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v4, v7, :cond_4

    .line 2662433
    invoke-virtual {v12, v4}, LX/9JH;->g(I)I

    move-result v7

    invoke-virtual {v12, v4}, LX/9JH;->b(I)LX/0w5;

    move-result-object v8

    invoke-virtual {v12, v4}, LX/9JH;->d(I)Ljava/util/Collection;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v6, v7, v8, v13, v14}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662434
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2662435
    :cond_4
    invoke-virtual {v12}, LX/9JH;->a()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v12, v4}, LX/9JH;->g(I)I

    move-result v4

    invoke-virtual {v12}, LX/9JH;->a()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v12, v7}, LX/9JH;->b(I)LX/0w5;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v13, 0x8

    invoke-virtual {v6, v4, v7, v8, v13}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662436
    invoke-virtual {v12}, LX/9JH;->a()I

    .line 2662437
    :cond_5
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto/16 :goto_1

    .line 2662438
    :cond_6
    new-instance v4, LX/B0i;

    const-string v8, ""

    const/4 v9, 0x1

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v9}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2662439
    :cond_7
    instance-of v4, v5, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    if-eqz v4, :cond_1a

    .line 2662440
    check-cast v5, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    .line 2662441
    new-instance v6, LX/9JH;

    invoke-direct {v6}, LX/9JH;-><init>()V

    .line 2662442
    move-object/from16 v0, p1

    iget-object v4, v0, LX/B0d;->b:Ljava/lang/String;

    if-nez v4, :cond_8

    .line 2662443
    invoke-virtual {v5}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v4

    invoke-static {v5}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, LX/9JH;->a(ILX/0w5;)V

    .line 2662444
    :cond_8
    invoke-virtual {v5}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v14

    .line 2662445
    if-nez v14, :cond_9

    .line 2662446
    const-class v4, LX/JCW;

    const-string v5, "Missing connection"

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2662447
    new-instance v4, LX/B0f;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, LX/B0f;-><init>(LX/B0d;)V

    goto/16 :goto_0

    .line 2662448
    :cond_9
    invoke-virtual {v14}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;->a()LX/0Px;

    move-result-object v15

    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v16

    const/4 v4, 0x0

    move v13, v4

    :goto_4
    move/from16 v0, v16

    if-ge v13, v0, :cond_17

    invoke-virtual {v15, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    .line 2662449
    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_a

    .line 2662450
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->CONTACT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 2662451
    move-object/from16 v0, p0

    iget-object v7, v0, LX/JCW;->h:LX/JCV;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/JCW;->f:LX/JCx;

    invoke-virtual {v7, v8, v4}, LX/JCV;->a(LX/JCx;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2662452
    invoke-virtual {v4}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v7

    invoke-static {v4}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v8

    invoke-static {v4}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v4, v9}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662453
    :cond_a
    :goto_5
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    goto :goto_4

    .line 2662454
    :cond_b
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const/4 v7, 0x1

    move v12, v7

    .line 2662455
    :goto_6
    if-nez v12, :cond_e

    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/JCW;->d:Z

    if-nez v7, :cond_e

    const/4 v7, 0x1

    move v9, v7

    .line 2662456
    :goto_7
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    const/16 v7, 0x10

    move v11, v7

    .line 2662457
    :goto_8
    if-eqz v12, :cond_c

    .line 2662458
    invoke-virtual {v4}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v7

    invoke-static {v4}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v8

    const/4 v10, 0x0

    or-int/lit8 v17, v11, 0x2

    move/from16 v0, v17

    invoke-virtual {v6, v7, v8, v10, v0}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662459
    :cond_c
    new-instance v17, LX/9JH;

    invoke-direct/range {v17 .. v17}, LX/9JH;-><init>()V

    .line 2662460
    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->p()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    .line 2662461
    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v8

    if-eqz v8, :cond_11

    .line 2662462
    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, LX/0Px;->size()I

    move-result v19

    const/4 v8, 0x0

    move v10, v8

    :goto_9
    move/from16 v0, v19

    if-ge v10, v0, :cond_11

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2662463
    invoke-virtual {v8}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v20

    invoke-static {v8}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v21

    invoke-virtual {v8}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_10

    invoke-virtual {v8}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    :goto_a
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move-object/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v8, v3}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662464
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_9

    .line 2662465
    :cond_d
    const/4 v7, 0x0

    move v12, v7

    goto/16 :goto_6

    .line 2662466
    :cond_e
    const/4 v7, 0x0

    move v9, v7

    goto/16 :goto_7

    .line 2662467
    :cond_f
    const/4 v7, 0x0

    move v11, v7

    goto :goto_8

    .line 2662468
    :cond_10
    const/4 v8, 0x0

    goto :goto_a

    .line 2662469
    :cond_11
    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->n()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;

    move-result-object v8

    if-eqz v8, :cond_13

    .line 2662470
    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->n()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v18

    .line 2662471
    const/4 v7, 0x0

    move v8, v7

    :goto_b
    move/from16 v0, v18

    if-ge v8, v0, :cond_13

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    .line 2662472
    sget-object v19, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->FILLED:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->e()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_12

    .line 2662473
    invoke-virtual {v7}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v19

    invoke-static {v7}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v20

    invoke-virtual {v7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move-object/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v7, v3}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662474
    :cond_12
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_b

    .line 2662475
    :cond_13
    const/4 v7, 0x0

    :goto_c
    invoke-virtual/range {v17 .. v17}, LX/9JH;->a()I

    move-result v8

    if-ge v7, v8, :cond_16

    .line 2662476
    if-nez v7, :cond_14

    if-nez v12, :cond_14

    .line 2662477
    or-int/lit8 v8, v11, 0x2

    .line 2662478
    :goto_d
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, LX/9JH;->g(I)I

    move-result v10

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, LX/9JH;->b(I)LX/0w5;

    move-result-object v18

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, LX/9JH;->d(I)Ljava/util/Collection;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v10, v0, v1, v8}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662479
    add-int/lit8 v7, v7, 0x1

    goto :goto_c

    .line 2662480
    :cond_14
    invoke-virtual/range {v17 .. v17}, LX/9JH;->a()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v7, v8, :cond_15

    if-nez v9, :cond_15

    .line 2662481
    or-int/lit8 v8, v11, 0x8

    goto :goto_d

    .line 2662482
    :cond_15
    or-int/lit8 v8, v11, 0x4

    goto :goto_d

    .line 2662483
    :cond_16
    if-eqz v9, :cond_a

    .line 2662484
    invoke-virtual {v4}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v7

    invoke-static {v4}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v4

    const/4 v8, 0x0

    or-int/lit8 v9, v11, 0x8

    invoke-virtual {v6, v7, v4, v8, v9}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    goto/16 :goto_5

    .line 2662485
    :cond_17
    const/4 v8, 0x0

    .line 2662486
    const/4 v9, 0x0

    .line 2662487
    invoke-virtual {v14}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    sget-object v10, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2662488
    if-nez v4, :cond_18

    .line 2662489
    const-class v4, LX/JCW;

    const-string v7, "Missing page info"

    invoke-static {v4, v7}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2662490
    :goto_e
    new-instance v4, LX/B0i;

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v9}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2662491
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 2662492
    :cond_18
    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    .line 2662493
    const/4 v9, 0x1

    invoke-virtual {v7, v4, v9}, LX/15i;->h(II)Z

    move-result v4

    if-eqz v4, :cond_19

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_19

    const/4 v4, 0x1

    :goto_f
    move v9, v4

    goto :goto_e

    :cond_19
    const/4 v4, 0x0

    goto :goto_f

    .line 2662494
    :cond_1a
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    throw v4
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2662495
    const-string v0, "CollectionsSummaryInitial"

    return-object v0
.end method
