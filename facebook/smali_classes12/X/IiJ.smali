.class public LX/IiJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603837
    iput-object p1, p0, LX/IiJ;->a:LX/0tX;

    .line 2603838
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2603839
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2603840
    new-instance v2, LX/IiK;

    invoke-direct {v2}, LX/IiK;-><init>()V

    move-object v2, v2

    .line 2603841
    const-string v3, "count"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2603842
    const-string v3, "searchQuery"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2603843
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v2, v2

    .line 2603844
    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 2603845
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    .line 2603846
    iget-object v3, p0, LX/IiJ;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2603847
    new-instance v3, LX/IiI;

    invoke-direct {v3, p0}, LX/IiI;-><init>(LX/IiJ;)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2603848
    monitor-exit p0

    return-object v0

    .line 2603849
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
