.class public LX/HVD;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:[Landroid/view/View;

.field public c:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2474850
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/HVD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474851
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 2474852
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474853
    const-class v0, LX/HVD;

    invoke-static {v0, p0}, LX/HVD;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2474854
    const v0, 0x7f0315aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2474855
    const v0, 0x7f081809

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/HVD;->d:Ljava/lang/String;

    .line 2474856
    const v0, 0x7f0d30dc

    invoke-virtual {p0, v0}, LX/HVD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/HVD;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2474857
    new-array v2, v5, [I

    const v0, 0x7f0d30dd

    aput v0, v2, v1

    const/4 v0, 0x1

    const v3, 0x7f0d30de

    aput v3, v2, v0

    const/4 v0, 0x2

    const v3, 0x7f0d30df

    aput v3, v2, v0

    .line 2474858
    new-array v0, v5, [Landroid/view/View;

    iput-object v0, p0, LX/HVD;->b:[Landroid/view/View;

    move v0, v1

    .line 2474859
    :goto_0
    if-ge v0, v5, :cond_0

    .line 2474860
    iget-object v3, p0, LX/HVD;->b:[Landroid/view/View;

    aget v4, v2, v0

    invoke-virtual {p0, v4}, LX/HVD;->findViewById(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v0

    .line 2474861
    iget-object v3, p0, LX/HVD;->b:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2474862
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2474863
    :cond_0
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2474864
    const/16 v0, 0x14

    if-gt p0, v0, :cond_0

    .line 2474865
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2474866
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "20"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HVD;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object p0, p1, LX/HVD;->a:LX/17W;

    return-void
.end method
