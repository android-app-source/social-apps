.class public LX/JJk;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBCommunityCommerceComposerModule"
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JJj;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/F68;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;LX/0Ot;LX/0Ot;LX/F68;LX/0Ot;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JJj;",
            ">;",
            "LX/F68;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679765
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679766
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5on;)V

    .line 2679767
    iput-object p2, p0, LX/JJk;->a:LX/0Ot;

    .line 2679768
    iput-object p3, p0, LX/JJk;->b:LX/0Ot;

    .line 2679769
    iput-object p4, p0, LX/JJk;->c:LX/F68;

    .line 2679770
    iput-object p5, p0, LX/JJk;->d:LX/0Ot;

    .line 2679771
    return-void
.end method

.method private static a(Ljava/lang/String;)LX/21D;
    .locals 1

    .prologue
    .line 2679759
    const-string v0, "buy_sell_bookmark"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2679760
    sget-object v0, LX/21D;->BUY_SELL_BOOKMARK:LX/21D;

    .line 2679761
    :goto_0
    return-object v0

    .line 2679762
    :cond_0
    const-string v0, "inventory_management"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2679763
    sget-object v0, LX/21D;->INVENTORY_MANAGEMENT:LX/21D;

    goto :goto_0

    .line 2679764
    :cond_1
    sget-object v0, LX/21D;->INVALID:LX/21D;

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679756
    :try_start_0
    iget-object v0, p0, LX/JJk;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 2679757
    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2679758
    :goto_0
    return-object v0

    :catch_0
    const-string v0, "USD"

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2679739
    packed-switch p1, :pswitch_data_0

    .line 2679740
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2679741
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2679742
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679743
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v1, "onCommerceComposerPostBegin"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2679744
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2679745
    iget-object v1, p0, LX/JJk;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JJj;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 2679746
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2679747
    iput-object v0, v1, LX/JJj;->c:Ljava/lang/String;

    .line 2679748
    iput-object v2, v1, LX/JJj;->d:LX/5pY;

    .line 2679749
    iget-object p0, v1, LX/JJj;->b:LX/0Yb;

    if-nez p0, :cond_1

    .line 2679750
    iget-object p0, v1, LX/JJj;->a:LX/0Xl;

    invoke-interface {p0}, LX/0Xl;->a()LX/0YX;

    move-result-object p0

    const-string p1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance p2, LX/JJi;

    invoke-direct {p2, v1}, LX/JJi;-><init>(LX/JJj;)V

    invoke-interface {p0, p1, p2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object p0

    invoke-interface {p0}, LX/0YX;->a()LX/0Yb;

    move-result-object p0

    iput-object p0, v1, LX/JJj;->b:LX/0Yb;

    .line 2679751
    :cond_1
    iget-object p0, v1, LX/JJj;->b:LX/0Yb;

    invoke-virtual {p0}, LX/0Yb;->b()V

    .line 2679752
    goto :goto_0

    .line 2679753
    :pswitch_2
    iget-object v0, p0, LX/JJk;->c:LX/F68;

    .line 2679754
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2679755
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F68;->a(LX/5pX;Landroid/app/Activity;)LX/F67;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/F67;->a(ILandroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public editPost(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679735
    iget-object v0, p0, LX/JJk;->c:LX/F68;

    .line 2679736
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2679737
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F68;->a(LX/5pX;Landroid/app/Activity;)LX/F67;

    move-result-object v0

    invoke-static {p2}, LX/JJk;->a(Ljava/lang/String;)LX/21D;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/F67;->a(Ljava/lang/String;LX/21D;)V

    .line 2679738
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679734
    const-string v0, "FBCommunityCommerceComposerModule"

    return-object v0
.end method

.method public launchComposer(ZLjava/lang/String;I)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2679728
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679729
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2679730
    :goto_0
    return-void

    .line 2679731
    :cond_0
    invoke-static {p2}, LX/JJk;->a(Ljava/lang/String;)LX/21D;

    move-result-object v0

    const-string v1, "FBCommunityCommerceComposerJavaModule"

    invoke-direct {p0}, LX/JJk;->h()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/88g;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0, v1, v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2679732
    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "commerce_composer"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v2, "ANDROID_COMPOSER"

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2679733
    iget-object v0, p0, LX/JJk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v2, 0x6dc

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v0, v4, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method
