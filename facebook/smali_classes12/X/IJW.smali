.class public final LX/IJW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;LX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 2563567
    iput-object p1, p0, LX/IJW;->c:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;

    iput-object p2, p0, LX/IJW;->a:LX/0Px;

    iput-object p3, p0, LX/IJW;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2563568
    iget-object v0, p0, LX/IJW;->c:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;

    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->n:LX/IID;

    iget-object v0, p0, LX/IJW;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2563569
    const-string v2, "friends_nearby_ping_open_in_app"

    invoke-static {v1, v2}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2563570
    const-string p1, "app"

    invoke-virtual {v2, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2563571
    iget-object p1, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563572
    iget-object v1, p0, LX/IJW;->c:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;

    iget-object v0, p0, LX/IJW;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2563573
    iget-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment$PingActionFragment;->l:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2563574
    return-void
.end method
