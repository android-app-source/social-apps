.class public final LX/Iwm;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V
    .locals 0

    .prologue
    .line 2630904
    iput-object p1, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2630918
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v1, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    .line 2630919
    iget-object v2, v0, LX/Iw6;->a:LX/0Zb;

    const-string v3, "pages_browser_category_page_load_error"

    invoke-static {v3}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "page_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630920
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->i:LX/03V;

    const-string v1, "page_identity_category_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2630921
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2630905
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 2630906
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v2, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v2, v2, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    .line 2630907
    iget-object v3, v0, LX/Iw6;->a:LX/0Zb;

    const-string v4, "pages_browser_category_page_load_successful"

    invoke-static {v4}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "page_id"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630908
    iget-object v2, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    .line 2630909
    iput-object v0, v2, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    .line 2630910
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2630911
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630912
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2630913
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2630914
    iget-object v4, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v4, v4, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->c:LX/Iwr;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/Iwr;->a(Ljava/lang/String;)V

    .line 2630915
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2630916
    :cond_1
    iget-object v0, p0, LX/Iwm;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-static {v0}, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->c(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V

    .line 2630917
    return-void
.end method
