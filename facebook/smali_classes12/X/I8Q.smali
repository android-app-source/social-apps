.class public final enum LX/I8Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I8Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I8Q;

.field public static final enum BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

.field public static final enum CANCEL_EVENT_BANNER:LX/I8Q;

.field public static final enum CHECKIN_CALL_TO_ACTION:LX/I8Q;

.field public static final enum COPY_EVENT_INVITES:LX/I8Q;

.field public static final enum DETAILS_VIEW:LX/I8Q;

.field public static final enum DRAFT_EVENT_BANNER:LX/I8Q;

.field public static final enum EVENT_COVER:LX/I8Q;

.field public static final enum GUEST_LIST:LX/I8Q;

.field public static final enum HEADER_SHADOW:LX/I8Q;

.field public static final enum INVITE_FRIENDS:LX/I8Q;

.field public static final enum PURCHASED_TICKETS_SUMMARY:LX/I8Q;

.field public static final enum SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

.field public static final enum SUMMARY_LINES:LX/I8Q;

.field public static final enum TOXICLE_ACTION_BAR:LX/I8Q;

.field public static final enum TOXICLE_BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

.field public static final enum TOXICLE_CHECKIN_CALL_TO_ACTION:LX/I8Q;

.field public static final enum TOXICLE_DETAILS_VIEW:LX/I8Q;

.field public static final enum TOXICLE_EVENT_COVER:LX/I8Q;

.field public static final enum TOXICLE_INVITED_BY_BAR:LX/I8Q;

.field public static final enum TOXICLE_MESSAGE_INVITER_BAR:LX/I8Q;

.field public static final enum TOXICLE_SAY_THANKS_CALL_TO_ACTION:LX/I8Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2541103
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_EVENT_COVER"

    invoke-direct {v0, v1, v3}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_EVENT_COVER:LX/I8Q;

    .line 2541104
    new-instance v0, LX/I8Q;

    const-string v1, "EVENT_COVER"

    invoke-direct {v0, v1, v4}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->EVENT_COVER:LX/I8Q;

    .line 2541105
    new-instance v0, LX/I8Q;

    const-string v1, "CANCEL_EVENT_BANNER"

    invoke-direct {v0, v1, v5}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->CANCEL_EVENT_BANNER:LX/I8Q;

    .line 2541106
    new-instance v0, LX/I8Q;

    const-string v1, "DRAFT_EVENT_BANNER"

    invoke-direct {v0, v1, v6}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->DRAFT_EVENT_BANNER:LX/I8Q;

    .line 2541107
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_BUY_TICKETS_CALL_TO_ACTION"

    invoke-direct {v0, v1, v7}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    .line 2541108
    new-instance v0, LX/I8Q;

    const-string v1, "BUY_TICKETS_CALL_TO_ACTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    .line 2541109
    new-instance v0, LX/I8Q;

    const-string v1, "PURCHASED_TICKETS_SUMMARY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->PURCHASED_TICKETS_SUMMARY:LX/I8Q;

    .line 2541110
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_CHECKIN_CALL_TO_ACTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_CHECKIN_CALL_TO_ACTION:LX/I8Q;

    .line 2541111
    new-instance v0, LX/I8Q;

    const-string v1, "CHECKIN_CALL_TO_ACTION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->CHECKIN_CALL_TO_ACTION:LX/I8Q;

    .line 2541112
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_INVITED_BY_BAR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_INVITED_BY_BAR:LX/I8Q;

    .line 2541113
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_MESSAGE_INVITER_BAR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_MESSAGE_INVITER_BAR:LX/I8Q;

    .line 2541114
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_ACTION_BAR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_ACTION_BAR:LX/I8Q;

    .line 2541115
    new-instance v0, LX/I8Q;

    const-string v1, "SUMMARY_LINES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->SUMMARY_LINES:LX/I8Q;

    .line 2541116
    new-instance v0, LX/I8Q;

    const-string v1, "HEADER_SHADOW"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->HEADER_SHADOW:LX/I8Q;

    .line 2541117
    new-instance v0, LX/I8Q;

    const-string v1, "GUEST_LIST"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->GUEST_LIST:LX/I8Q;

    .line 2541118
    new-instance v0, LX/I8Q;

    const-string v1, "INVITE_FRIENDS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->INVITE_FRIENDS:LX/I8Q;

    .line 2541119
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_DETAILS_VIEW"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_DETAILS_VIEW:LX/I8Q;

    .line 2541120
    new-instance v0, LX/I8Q;

    const-string v1, "DETAILS_VIEW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->DETAILS_VIEW:LX/I8Q;

    .line 2541121
    new-instance v0, LX/I8Q;

    const-string v1, "TOXICLE_SAY_THANKS_CALL_TO_ACTION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->TOXICLE_SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    .line 2541122
    new-instance v0, LX/I8Q;

    const-string v1, "SAY_THANKS_CALL_TO_ACTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    .line 2541123
    new-instance v0, LX/I8Q;

    const-string v1, "COPY_EVENT_INVITES"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/I8Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8Q;->COPY_EVENT_INVITES:LX/I8Q;

    .line 2541124
    const/16 v0, 0x15

    new-array v0, v0, [LX/I8Q;

    sget-object v1, LX/I8Q;->TOXICLE_EVENT_COVER:LX/I8Q;

    aput-object v1, v0, v3

    sget-object v1, LX/I8Q;->EVENT_COVER:LX/I8Q;

    aput-object v1, v0, v4

    sget-object v1, LX/I8Q;->CANCEL_EVENT_BANNER:LX/I8Q;

    aput-object v1, v0, v5

    sget-object v1, LX/I8Q;->DRAFT_EVENT_BANNER:LX/I8Q;

    aput-object v1, v0, v6

    sget-object v1, LX/I8Q;->TOXICLE_BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/I8Q;->BUY_TICKETS_CALL_TO_ACTION:LX/I8Q;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/I8Q;->PURCHASED_TICKETS_SUMMARY:LX/I8Q;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/I8Q;->TOXICLE_CHECKIN_CALL_TO_ACTION:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/I8Q;->CHECKIN_CALL_TO_ACTION:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/I8Q;->TOXICLE_INVITED_BY_BAR:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/I8Q;->TOXICLE_MESSAGE_INVITER_BAR:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/I8Q;->TOXICLE_ACTION_BAR:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/I8Q;->SUMMARY_LINES:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/I8Q;->HEADER_SHADOW:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/I8Q;->GUEST_LIST:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/I8Q;->INVITE_FRIENDS:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/I8Q;->TOXICLE_DETAILS_VIEW:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/I8Q;->DETAILS_VIEW:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/I8Q;->TOXICLE_SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/I8Q;->SAY_THANKS_CALL_TO_ACTION:LX/I8Q;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/I8Q;->COPY_EVENT_INVITES:LX/I8Q;

    aput-object v2, v0, v1

    sput-object v0, LX/I8Q;->$VALUES:[LX/I8Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2541125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I8Q;
    .locals 1

    .prologue
    .line 2541126
    const-class v0, LX/I8Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I8Q;

    return-object v0
.end method

.method public static values()[LX/I8Q;
    .locals 1

    .prologue
    .line 2541127
    sget-object v0, LX/I8Q;->$VALUES:[LX/I8Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I8Q;

    return-object v0
.end method
