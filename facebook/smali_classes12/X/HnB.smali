.class public final LX/HnB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501418
    iput-object p1, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2501419
    iget-object v0, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->t:LX/Hm2;

    invoke-virtual {v1}, LX/Hm2;->a()Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-result-object v1

    .line 2501420
    iput-object v1, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501421
    iget-object v0, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v0}, LX/Hm1;->a()Ljava/lang/String;

    .line 2501422
    iget-object v0, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    iget-object v1, p0, LX/HnB;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501423
    iget-object v2, v0, LX/Hmz;->a:LX/0TD;

    new-instance p0, LX/Hms;

    invoke-direct {p0, v0, v1}, LX/Hms;-><init>(LX/Hmz;Lcom/facebook/beam/protocol/BeamPreflightInfo;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2501424
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501425
    invoke-direct {p0}, LX/HnB;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
