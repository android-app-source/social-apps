.class public LX/Isi;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Isl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/support/v7/widget/RecyclerView;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2621247
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2621248
    const-class v0, LX/Isi;

    invoke-static {v0, p0}, LX/Isi;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2621249
    const v0, 0x7f030cda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2621250
    const v0, 0x7f0d0b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/Isi;->b:Landroid/support/v7/widget/RecyclerView;

    .line 2621251
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/Isi;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2621252
    const/4 p1, 0x0

    invoke-virtual {v0, p1}, LX/1P1;->b(I)V

    .line 2621253
    iget-object p1, p0, LX/Isi;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2621254
    iget-object v0, p0, LX/Isi;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object p1, p0, LX/Isi;->a:LX/Isl;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2621255
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Isi;

    new-instance v0, LX/Isl;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object p0

    check-cast p0, Landroid/view/LayoutInflater;

    invoke-direct {v0, p0}, LX/Isl;-><init>(Landroid/view/LayoutInflater;)V

    move-object v1, v0

    check-cast v1, LX/Isl;

    iput-object v1, p1, LX/Isi;->a:LX/Isl;

    return-void
.end method
