.class public final LX/HY7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWL;


# instance fields
.field public final synthetic a:LX/HY9;


# direct methods
.method public constructor <init>(LX/HY9;)V
    .locals 0

    .prologue
    .line 2480204
    iput-object p1, p0, LX/HY7;->a:LX/HY9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 2480205
    iget-object v0, p0, LX/HY7;->a:LX/HY9;

    iget-object v0, v0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    .line 2480206
    if-nez v0, :cond_1

    .line 2480207
    :cond_0
    :goto_0
    return-void

    .line 2480208
    :cond_1
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "undefined"

    invoke-virtual {p4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2480209
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2480210
    const-string v1, "method"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2480211
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2480212
    iget-object v1, p0, LX/HY7;->a:LX/HY9;

    iget-object v1, v1, LX/HY9;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYA;

    .line 2480213
    invoke-virtual {v1}, LX/HYA;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2480214
    const-string v2, "params"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2480215
    if-nez v2, :cond_3

    .line 2480216
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2480217
    :cond_3
    invoke-virtual {v1, v2}, LX/HYA;->a(Lorg/json/JSONObject;)LX/HYB;

    move-result-object v1

    .line 2480218
    iput-object v1, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n:LX/HYB;

    .line 2480219
    invoke-virtual {v1, v0, v0}, LX/HYB;->a(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2480220
    goto :goto_1

    .line 2480221
    :catch_0
    move-exception v0

    .line 2480222
    sget-object v1, LX/HY9;->a:Ljava/lang/Class;

    const-string v2, "Error in parsing de-queued JSON : %1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2480223
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/HY7;->a:LX/HY9;

    invoke-static {v0, p1}, LX/HY9;->d(LX/HY9;Lcom/facebook/webview/FacebookWebView;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
