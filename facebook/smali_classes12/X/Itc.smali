.class public LX/Itc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/03V;

.field public final c:LX/2Hv;

.field private final d:LX/7GV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2623572
    const-class v0, LX/Itc;

    sput-object v0, LX/Itc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/2Hv;LX/7GV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2623567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2623568
    iput-object p1, p0, LX/Itc;->b:LX/03V;

    .line 2623569
    iput-object p2, p0, LX/Itc;->c:LX/2Hv;

    .line 2623570
    iput-object p3, p0, LX/Itc;->d:LX/7GV;

    .line 2623571
    return-void
.end method

.method public static synthetic a(LX/Itc;[B)LX/1su;
    .locals 4

    .prologue
    .line 2623573
    invoke-static {p1}, LX/7GV;->a([B)LX/7GU;

    move-result-object v0

    .line 2623574
    iget v0, v0, LX/7GU;->b:I

    .line 2623575
    new-instance v1, LX/1sp;

    invoke-direct {v1}, LX/1sp;-><init>()V

    .line 2623576
    new-instance v2, LX/1sr;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    array-length p0, p1

    sub-int/2addr p0, v0

    invoke-direct {v3, p1, v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-direct {v2, v3}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v1, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    move-object v0, v0

    .line 2623577
    return-object v0
.end method

.method public static a$redex0(LX/Itc;LX/6md;)LX/765;
    .locals 9

    .prologue
    .line 2623551
    iget-object v0, p1, LX/6md;->sendSucceeded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2623552
    iget-object v1, p1, LX/6md;->errno:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/6md;->errno:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2623553
    :goto_0
    iget-object v1, p1, LX/6md;->isRetryable:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p1, LX/6md;->isRetryable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2623554
    :goto_1
    iget-object v1, p1, LX/6md;->errStr:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v4, p1, LX/6md;->errStr:Ljava/lang/String;

    .line 2623555
    :goto_2
    iget-object v1, p1, LX/6md;->fbTraceMeta:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v5, p1, LX/6md;->fbTraceMeta:Ljava/lang/String;

    .line 2623556
    :goto_3
    iget-object v1, p1, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v1, :cond_5

    iget-object v1, p1, LX/6md;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2623557
    :goto_4
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2623558
    sget-object v1, LX/Itc;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v8, "Empty errStr for mqtt NO_RETRY error"

    invoke-static {v1, v8}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 2623559
    iget-object v8, p0, LX/Itc;->b:LX/03V;

    invoke-virtual {v8, v1}, LX/03V;->a(LX/0VG;)V

    .line 2623560
    :cond_0
    if-eqz v0, :cond_6

    new-instance v0, LX/765;

    const/4 v1, 0x1

    sget-object v2, LX/Itv;->NONE:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    const/4 v3, 0x0

    const-string v4, ""

    invoke-direct/range {v0 .. v7}, LX/765;-><init>(ZIZLjava/lang/String;Ljava/lang/String;J)V

    :goto_5
    return-object v0

    .line 2623561
    :cond_1
    sget-object v1, LX/Itv;->NONE:LX/Itv;

    iget v2, v1, LX/Itv;->errorCode:I

    goto :goto_0

    .line 2623562
    :cond_2
    const/4 v3, 0x1

    goto :goto_1

    .line 2623563
    :cond_3
    const-string v4, ""

    goto :goto_2

    .line 2623564
    :cond_4
    const-string v5, ""

    goto :goto_3

    .line 2623565
    :cond_5
    const-wide/16 v6, -0x1

    goto :goto_4

    .line 2623566
    :cond_6
    new-instance v0, LX/765;

    const/4 v1, 0x0

    invoke-direct/range {v0 .. v7}, LX/765;-><init>(ZIZLjava/lang/String;Ljava/lang/String;J)V

    goto :goto_5
.end method


# virtual methods
.method public final a(JLX/ItY;)LX/76J;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/ItY;",
            ")",
            "LX/76J",
            "<",
            "Ljava/util/List",
            "<",
            "LX/765;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2623547
    sget-object v0, LX/ItY;->THRIFT_BATCH:LX/ItY;

    if-eq p3, v0, :cond_0

    .line 2623548
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2623549
    :cond_0
    new-instance v0, LX/ItZ;

    invoke-direct {v0, p0, p1, p2}, LX/ItZ;-><init>(LX/Itc;J)V

    .line 2623550
    iget-object v1, p0, LX/Itc;->c:LX/2Hv;

    sget-object v2, LX/ItY;->THRIFT_BATCH:LX/ItY;

    invoke-virtual {v2}, LX/ItY;->getResponseTopic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3, v3}, LX/2Hv;->a(Ljava/lang/String;LX/6cX;LX/2gV;LX/03V;)LX/76J;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLX/ItY;LX/2gV;LX/03V;)LX/76J;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/ItY;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClient;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")",
            "LX/76J",
            "<",
            "LX/765;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2623542
    sget-object v0, LX/Itb;->a:[I

    invoke-virtual {p3}, LX/ItY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2623543
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2623544
    :pswitch_0
    new-instance v0, LX/Ita;

    invoke-direct {v0, p0, p1, p2}, LX/Ita;-><init>(LX/Itc;J)V

    .line 2623545
    iget-object v1, p0, LX/Itc;->c:LX/2Hv;

    sget-object v2, LX/ItY;->THRIFT:LX/ItY;

    invoke-virtual {v2}, LX/ItY;->getResponseTopic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p4, p5}, LX/2Hv;->a(Ljava/lang/String;LX/6cX;LX/2gV;LX/03V;)LX/76J;

    move-result-object v0

    move-object v0, v0

    .line 2623546
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
