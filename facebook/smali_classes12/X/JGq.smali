.class public final LX/JGq;
.super LX/5rl;
.source ""


# static fields
.field public static final a:[I


# instance fields
.field public final b:LX/JGd;

.field private final c:LX/JGj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2669082
    const/4 v0, 0x4

    new-array v0, v0, [I

    sput-object v0, LX/JGq;->a:[I

    return-void
.end method

.method public constructor <init>(LX/5pY;LX/JGd;)V
    .locals 2

    .prologue
    .line 2669086
    invoke-direct {p0, p1, p2}, LX/5rl;-><init>(LX/5pY;LX/5qw;)V

    .line 2669087
    new-instance v0, LX/JGj;

    invoke-direct {v0, p0}, LX/JGj;-><init>(LX/JGq;)V

    iput-object v0, p0, LX/JGq;->c:LX/JGj;

    .line 2669088
    iput-object p2, p0, LX/JGq;->b:LX/JGd;

    .line 2669089
    return-void
.end method


# virtual methods
.method public final a(IIIII)LX/JGn;
    .locals 8

    .prologue
    .line 2669085
    new-instance v0, LX/JGn;

    const/4 v7, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, LX/JGn;-><init>(LX/JGq;IIIIIB)V

    return-object v0
.end method

.method public final a(IFFFFZLcom/facebook/react/bridge/Callback;)V
    .locals 10

    .prologue
    .line 2669083
    new-instance v0, LX/JGi;

    const/4 v9, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v9}, LX/JGi;-><init>(LX/JGq;IFFFFZLcom/facebook/react/bridge/Callback;B)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669084
    return-void
.end method

.method public final a(IFFLcom/facebook/react/bridge/Callback;)V
    .locals 7

    .prologue
    .line 2669080
    new-instance v0, LX/JGh;

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/JGh;-><init>(LX/JGq;IFFLcom/facebook/react/bridge/Callback;B)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669081
    return-void
.end method

.method public final a(I[I[I)V
    .locals 6

    .prologue
    .line 2669078
    new-instance v0, LX/JGo;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, LX/JGo;-><init>(LX/JGq;I[I[I)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669079
    return-void
.end method

.method public final a(I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V
    .locals 13
    .param p2    # [LX/JGM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # [LX/JGQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [LX/JGu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669090
    new-instance v0, LX/JGl;

    const/4 v12, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v0 .. v12}, LX/JGl;-><init>(LX/JGq;I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZB)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669091
    return-void
.end method

.method public final a(I[LX/JGM;[LX/JGQ;[LX/JGu;)V
    .locals 7
    .param p2    # [LX/JGM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [LX/JGQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [LX/JGu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669076
    new-instance v0, LX/JGm;

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/JGm;-><init>(LX/JGq;I[LX/JGM;[LX/JGQ;[LX/JGu;B)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669077
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2669074
    new-instance v0, LX/JGg;

    invoke-direct {v0, p0, p1, p2}, LX/JGg;-><init>(LX/JGq;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669075
    return-void
.end method

.method public final b(IILX/5pC;)LX/JGp;
    .locals 1
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2669066
    new-instance v0, LX/JGp;

    invoke-direct {v0, p0, p1, p2, p3}, LX/JGp;-><init>(LX/JGq;IILX/5pC;)V

    return-object v0
.end method

.method public final b(IIIII)V
    .locals 8

    .prologue
    .line 2669072
    new-instance v0, LX/JGk;

    const/4 v7, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, LX/JGk;-><init>(LX/JGq;IIIIIB)V

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669073
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2669070
    iget-object v0, p0, LX/JGq;->c:LX/JGj;

    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669071
    return-void
.end method

.method public final g()LX/JGf;
    .locals 1

    .prologue
    .line 2669067
    new-instance v0, LX/JGf;

    invoke-direct {v0, p0}, LX/JGf;-><init>(LX/JGq;)V

    .line 2669068
    invoke-virtual {p0, v0}, LX/5rl;->a(LX/5rT;)V

    .line 2669069
    return-object v0
.end method
