.class public final LX/HWR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V
    .locals 0

    .prologue
    .line 2476089
    iput-object p1, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2476090
    iget-object v0, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2476091
    new-instance v1, LX/9ZZ;

    invoke-direct {v1}, LX/9ZZ;-><init>()V

    move-object v1, v1

    .line 2476092
    const-string v2, "page_id"

    iget-object v3, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-wide v4, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "num_of_featured_admins"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "featured_admin_profile_image_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/9ZZ;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2476093
    iget-object v1, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v2, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2476094
    iput-object v0, v1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2476095
    iget-object v0, p0, LX/HWR;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method
