.class public final LX/I2d;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/I2e;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/I2j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/I2e;


# direct methods
.method public constructor <init>(LX/I2e;)V
    .locals 1

    .prologue
    .line 2530583
    iput-object p1, p0, LX/I2d;->b:LX/I2e;

    .line 2530584
    move-object v0, p1

    .line 2530585
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2530586
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2530587
    const-string v0, "EventsDashboardViewAllEventsRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2530588
    if-ne p0, p1, :cond_1

    .line 2530589
    :cond_0
    :goto_0
    return v0

    .line 2530590
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2530591
    goto :goto_0

    .line 2530592
    :cond_3
    check-cast p1, LX/I2d;

    .line 2530593
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2530594
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2530595
    if-eq v2, v3, :cond_0

    .line 2530596
    iget-object v2, p0, LX/I2d;->a:LX/I2j;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/I2d;->a:LX/I2j;

    iget-object v3, p1, LX/I2d;->a:LX/I2j;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2530597
    goto :goto_0

    .line 2530598
    :cond_4
    iget-object v2, p1, LX/I2d;->a:LX/I2j;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
