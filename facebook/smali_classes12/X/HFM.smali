.class public final LX/HFM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443461
    iput-object p1, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x3b03402c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443462
    iget-object v1, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->f:LX/HF2;

    .line 2443463
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, LX/HF2;->e(I)LX/HF0;

    move-result-object v1

    .line 2443464
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443465
    iget-object v2, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    .line 2443466
    iget-object v3, v1, LX/HF0;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2443467
    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    .line 2443468
    iget-object v2, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443469
    iget-object v3, v1, LX/HF0;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2443470
    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443471
    iget-object v1, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2443472
    iget-object v1, p0, LX/HFM;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2443473
    const v1, -0x2a2e29e0

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
