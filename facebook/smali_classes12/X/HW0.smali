.class public LX/HW0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475516
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    .line 2475517
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2475518
    const-string v2, "profile_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2475519
    const-string v3, "event_id"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2475520
    const-string v4, "extra_ref_module"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2475521
    const-string v5, "event_ref_mechanism"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2475522
    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(JLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    move-result-object v0

    return-object v0
.end method
