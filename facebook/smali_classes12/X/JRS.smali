.class public LX/JRS;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mW",
        "<",
        "LX/3hq;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/1Pm;

.field private final d:LX/JRh;

.field private final e:LX/JRZ;

.field private final f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;LX/JRV;LX/JRh;LX/JRZ;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/JRV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/3hq;",
            ">;",
            "LX/1Pm;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/25M;",
            "LX/JRV;",
            "LX/JRh;",
            "LX/JRZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693339
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2693340
    iput-object p3, p0, LX/JRS;->c:LX/1Pm;

    .line 2693341
    iput-object p7, p0, LX/JRS;->d:LX/JRh;

    .line 2693342
    iput-object p8, p0, LX/JRS;->e:LX/JRZ;

    .line 2693343
    iput-object p4, p0, LX/JRS;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693344
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2693345
    iget-object v0, p0, LX/JRS;->e:LX/JRZ;

    const/4 v1, 0x0

    .line 2693346
    new-instance v2, LX/JRY;

    invoke-direct {v2, v0}, LX/JRY;-><init>(LX/JRZ;)V

    .line 2693347
    sget-object v3, LX/JRZ;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JRX;

    .line 2693348
    if-nez v3, :cond_0

    .line 2693349
    new-instance v3, LX/JRX;

    invoke-direct {v3}, LX/JRX;-><init>()V

    .line 2693350
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JRX;->a$redex0(LX/JRX;LX/1De;IILX/JRY;)V

    .line 2693351
    move-object v2, v3

    .line 2693352
    move-object v1, v2

    .line 2693353
    move-object v0, v1

    .line 2693354
    iget-object v1, p0, LX/JRS;->c:LX/1Pm;

    .line 2693355
    iget-object v2, v0, LX/JRX;->a:LX/JRY;

    iput-object v1, v2, LX/JRY;->b:LX/1Pm;

    .line 2693356
    iget-object v2, v0, LX/JRX;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2693357
    move-object v0, v0

    .line 2693358
    iget-object v1, p0, LX/JRS;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693359
    iget-object v2, v0, LX/JRX;->a:LX/JRY;

    iput-object v1, v2, LX/JRY;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693360
    iget-object v2, v0, LX/JRX;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2693361
    move-object v0, v0

    .line 2693362
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2693363
    check-cast p2, LX/3hq;

    .line 2693364
    iget-object v0, p0, LX/JRS;->d:LX/JRh;

    const/4 v1, 0x0

    .line 2693365
    new-instance v2, LX/JRg;

    invoke-direct {v2, v0}, LX/JRg;-><init>(LX/JRh;)V

    .line 2693366
    sget-object v3, LX/JRh;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JRf;

    .line 2693367
    if-nez v3, :cond_0

    .line 2693368
    new-instance v3, LX/JRf;

    invoke-direct {v3}, LX/JRf;-><init>()V

    .line 2693369
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JRf;->a$redex0(LX/JRf;LX/1De;IILX/JRg;)V

    .line 2693370
    move-object v2, v3

    .line 2693371
    move-object v1, v2

    .line 2693372
    move-object v0, v1

    .line 2693373
    iget-object v1, p0, LX/JRS;->c:LX/1Pm;

    .line 2693374
    iget-object v2, v0, LX/JRf;->a:LX/JRg;

    iput-object v1, v2, LX/JRg;->b:LX/1Pm;

    .line 2693375
    iget-object v2, v0, LX/JRf;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2693376
    move-object v0, v0

    .line 2693377
    iget-object v1, p2, LX/3hq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2693378
    iget-object v2, v0, LX/JRf;->a:LX/JRg;

    iput-object v1, v2, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693379
    iget-object v2, v0, LX/JRf;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2693380
    move-object v0, v0

    .line 2693381
    iget v1, p2, LX/3hq;->d:I

    move v1, v1

    .line 2693382
    iget-object v2, v0, LX/JRf;->a:LX/JRg;

    iput v1, v2, LX/JRg;->c:I

    .line 2693383
    iget-object v2, v0, LX/JRf;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2693384
    move-object v0, v0

    .line 2693385
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2693386
    const/4 v0, 0x0

    return v0
.end method
