.class public final LX/HW4;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 2475658
    iput-object p1, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    .line 2475659
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2475660
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v1, -0x2

    .line 2475568
    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    if-ne p1, v2, :cond_1

    .line 2475569
    :cond_0
    :goto_0
    return v0

    .line 2475570
    :cond_1
    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->p:I

    if-lez v2, :cond_2

    .line 2475571
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    .line 2475572
    iget v2, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->p:I

    add-int/lit8 v3, v2, -0x1

    iput v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->p:I

    .line 2475573
    move v0, v1

    .line 2475574
    goto :goto_0

    .line 2475575
    :cond_2
    instance-of v2, p1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    if-nez v2, :cond_0

    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2475576
    goto :goto_0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 11

    .prologue
    .line 2475591
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    .line 2475592
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2475593
    const-string v1, "extra_in_admin_container_frag"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475594
    packed-switch p1, :pswitch_data_0

    .line 2475595
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment index out of bounds: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2475596
    :pswitch_0
    iget-object v1, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2475597
    :goto_0
    return-object v0

    .line 2475598
    :pswitch_1
    iget-object v1, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0

    .line 2475599
    :pswitch_2
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2475600
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    if-eqz v3, :cond_0

    .line 2475601
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    .line 2475602
    :goto_1
    move-object v0, v3

    .line 2475603
    goto :goto_0

    .line 2475604
    :cond_0
    sget-object v3, LX/HW3;->a:[I

    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 2475605
    sget-object v3, LX/8Dq;->a:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    iget-wide v5, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2475606
    :goto_2
    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->H:LX/0ad;

    sget-short v5, LX/8Dn;->k:S

    invoke-interface {v4, v5, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2475607
    iget-wide v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    .line 2475608
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2475609
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2475610
    new-instance v6, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;

    invoke-direct {v6}, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;-><init>()V

    .line 2475611
    const-string v7, "com.facebook.katana.profile.id"

    invoke-virtual {v5, v7, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475612
    const-string v7, "ptr_enabled"

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475613
    invoke-virtual {v6, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475614
    move-object v3, v6

    .line 2475615
    iput-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    .line 2475616
    :goto_3
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    goto :goto_1

    .line 2475617
    :pswitch_3
    sget-object v3, LX/8Dq;->z:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    iget-wide v5, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2475618
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    iput-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    goto :goto_2

    .line 2475619
    :pswitch_4
    sget-object v3, LX/8Dq;->A:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    iget-wide v5, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->j:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2475620
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    iput-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->K:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    goto :goto_2

    .line 2475621
    :cond_1
    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->w:LX/17Y;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2475622
    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->v:LX/44t;

    .line 2475623
    invoke-static {v3}, LX/44t;->c(Landroid/content/Intent;)I

    move-result v5

    .line 2475624
    const-string v6, "target_fragment"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2475625
    iget-object v6, v4, LX/44t;->a:LX/0jo;

    invoke-interface {v6, v5}, LX/0jo;->a(I)LX/0jq;

    move-result-object v5

    move-object v5, v5

    .line 2475626
    const-string v6, "Undefined content fragment factory identifier %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, LX/44t;->c(Landroid/content/Intent;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0jq;

    move-object v5, v5

    .line 2475627
    invoke-interface {v5, v3}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    .line 2475628
    const-string v6, "Factory could not generate fragment for intent: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v8}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 2475629
    move-object v3, v5

    .line 2475630
    iput-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    .line 2475631
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->c:Landroid/support/v4/app/Fragment;

    .line 2475632
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v4

    .line 2475633
    const-string v4, "parent_control_title_bar"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475634
    const-string v4, "no_title"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475635
    const-string v4, "hide_drop_shadow"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2475636
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    .line 2475637
    if-nez p2, :cond_2

    .line 2475638
    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    move-object v0, v1

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    iput-object v0, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2475639
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->f:Landroid/view/ViewGroup;

    .line 2475640
    iput-object v2, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->t:Landroid/view/ViewGroup;

    .line 2475641
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    if-eqz v3, :cond_0

    .line 2475642
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->D:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1PJ;

    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->t:Landroid/view/ViewGroup;

    new-instance p1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_STORY:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {p1, p2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v3, v4, p1}, LX/1PJ;->a(Landroid/view/ViewGroup;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 2475643
    invoke-static {v0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->k(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V

    .line 2475644
    :cond_0
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    check-cast v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2475645
    iput-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ad:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2475646
    :cond_1
    :goto_0
    return-object v1

    .line 2475647
    :cond_2
    if-ne p2, v2, :cond_3

    .line 2475648
    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    move-object v0, v1

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iput-object v0, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    .line 2475649
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    if-eqz v0, :cond_1

    .line 2475650
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->b:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v2, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->m:LX/0Px;

    .line 2475651
    iput-object v2, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    .line 2475652
    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->d(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    .line 2475653
    goto :goto_0

    .line 2475654
    :cond_3
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 2475655
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    .line 2475656
    iput-boolean v2, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->o:Z

    .line 2475657
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment$PageIdentityPagerAdapter$1;

    invoke-direct {v2, p0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment$PageIdentityPagerAdapter$1;-><init>(LX/HW4;)V

    const v3, -0x1e593fd3

    invoke-static {v0, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2475586
    invoke-super {p0, p1, p2, p3}, LX/2s5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2475587
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2475588
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    const/4 v1, 0x0

    .line 2475589
    iput-boolean v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->o:Z

    .line 2475590
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2475583
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->q:Z

    if-eqz v0, :cond_0

    .line 2475584
    const/4 v0, 0x3

    .line 2475585
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 2475577
    invoke-super {p0, p1}, LX/2s5;->b(Landroid/view/ViewGroup;)V

    .line 2475578
    iget-object v0, p0, LX/HW4;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    .line 2475579
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2475580
    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->a:Lcom/facebook/base/fragment/FbFragment;

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HW5;

    iget p0, v1, LX/HW5;->a:I

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HW5;

    iget p1, v1, LX/HW5;->b:I

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HW5;

    iget-object v1, v1, LX/HW5;->c:Landroid/content/Intent;

    invoke-virtual {v2, p0, p1, v1}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2475581
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->r:LX/0am;

    .line 2475582
    :cond_0
    return-void
.end method
