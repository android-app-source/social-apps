.class public final LX/HP3;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:LX/HMI;

.field public final synthetic c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;LX/4At;LX/HMI;)V
    .locals 0

    .prologue
    .line 2461248
    iput-object p1, p0, LX/HP3;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iput-object p2, p0, LX/HP3;->a:LX/4At;

    iput-object p3, p0, LX/HP3;->b:LX/HMI;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 2461249
    invoke-super {p0, p1}, LX/2h0;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 2461250
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2461251
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2461252
    :cond_0
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2461253
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2461254
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2461255
    :cond_0
    iget-object v0, p0, LX/HP3;->b:LX/HMI;

    invoke-interface {v0, p1}, LX/HMI;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2461256
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2461257
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2461258
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    if-eqz v0, :cond_0

    .line 2461259
    iget-object v0, p0, LX/HP3;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2461260
    :cond_0
    iget-object v0, p0, LX/HP3;->b:LX/HMI;

    invoke-interface {v0}, LX/HMI;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2461261
    iget-object v0, p0, LX/HP3;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    const-class v1, LX/HQw;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQw;

    .line 2461262
    if-eqz v0, :cond_2

    .line 2461263
    invoke-interface {v0}, LX/HQw;->c()V

    .line 2461264
    :cond_1
    :goto_0
    iget-object v0, p0, LX/HP3;->b:LX/HMI;

    invoke-interface {v0, p1}, LX/HMI;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2461265
    return-void

    .line 2461266
    :cond_2
    iget-object v0, p0, LX/HP3;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->g()V

    goto :goto_0
.end method
