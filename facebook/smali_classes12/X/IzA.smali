.class public final LX/IzA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Iys;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Iys;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2634218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634219
    iput-object p1, p0, LX/IzA;->a:LX/0QB;

    .line 2634220
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2634230
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/IzA;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2634222
    packed-switch p2, :pswitch_data_0

    .line 2634223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2634224
    :pswitch_0
    new-instance v1, LX/Iyt;

    .line 2634225
    new-instance p0, LX/Iz9;

    invoke-static {p1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-direct {p0, v0}, LX/Iz9;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    .line 2634226
    move-object v0, p0

    .line 2634227
    check-cast v0, LX/Iz9;

    const/16 p0, 0x2d27

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v1, v0, p0}, LX/Iyt;-><init>(LX/Iz9;LX/0Ot;)V

    .line 2634228
    move-object v0, v1

    .line 2634229
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2634221
    const/4 v0, 0x1

    return v0
.end method
