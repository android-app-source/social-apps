.class public LX/If1;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Dct;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IfL;",
        ">;",
        "LX/Dct",
        "<",
        "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/IfB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/Iez;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599268
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2599269
    new-instance v0, LX/If0;

    invoke-direct {v0, p0}, LX/If0;-><init>(LX/If1;)V

    iput-object v0, p0, LX/If1;->e:LX/Iez;

    .line 2599270
    iput-object p1, p0, LX/If1;->a:Landroid/view/LayoutInflater;

    .line 2599271
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2599272
    iget-object v0, p0, LX/If1;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030377

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2599273
    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;

    iget-object v2, p0, LX/If1;->e:LX/Iez;

    .line 2599274
    iput-object v2, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->b:LX/Iez;

    .line 2599275
    new-instance v0, LX/IfL;

    invoke-direct {v0, v1}, LX/IfL;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;
    .locals 6

    .prologue
    .line 2599276
    iget-object v0, p0, LX/If1;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/If1;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599277
    iget-object v3, v0, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-object v3, v3

    .line 2599278
    iget-object v3, v3, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599279
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2599280
    iget-object v4, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599281
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2599282
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2599283
    :goto_1
    return-object v0

    .line 2599284
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2599285
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2599286
    check-cast p1, LX/IfL;

    .line 2599287
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;

    .line 2599288
    iget-object v1, p0, LX/If1;->b:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599289
    iget-object v2, v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-object v1, v2

    .line 2599290
    iget-object v2, p0, LX/If1;->d:Ljava/util/Set;

    iget-object v3, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599291
    iget-object p0, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, p0

    .line 2599292
    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599293
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->setTag(Ljava/lang/Object;)V

    .line 2599294
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2599295
    iget-object v0, p0, LX/If1;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
