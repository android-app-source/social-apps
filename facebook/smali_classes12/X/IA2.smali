.class public final LX/IA2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic b:LX/IA4;


# direct methods
.method public constructor <init>(LX/IA4;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2544424
    iput-object p1, p0, LX/IA2;->b:LX/IA4;

    iput-object p2, p0, LX/IA2;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x7586a205

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544425
    iget-object v1, p0, LX/IA2;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->C:Lcom/facebook/friends/ui/SmartButtonLite;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friends/ui/SmartButtonLite;->setEnabled(Z)V

    .line 2544426
    iget-object v1, p0, LX/IA2;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->l:LX/2hX;

    iget-object v2, p0, LX/IA2;->b:LX/IA4;

    iget-object v2, v2, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544427
    iget-object v3, v2, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2544428
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/IA2;->b:LX/IA4;

    iget-object v4, v4, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544429
    iget-object v5, v4, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v4, v5

    .line 2544430
    sget-object v5, LX/2h7;->EVENT_GYMK:LX/2h7;

    iget-object v6, p0, LX/IA2;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual/range {v1 .. v6}, LX/2hX;->a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2544431
    iget-object v1, p0, LX/IA2;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/IA2;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    if-eqz v1, :cond_0

    .line 2544432
    iget-object v1, p0, LX/IA2;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2544433
    iget v2, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->l:I

    .line 2544434
    :cond_0
    const v1, -0x778a3fd0

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
