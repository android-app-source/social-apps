.class public final LX/IpR;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IpT;


# direct methods
.method public constructor <init>(LX/IpT;)V
    .locals 0

    .prologue
    .line 2612923
    iput-object p1, p0, LX/IpR;->a:LX/IpT;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2612924
    iget-object v0, p0, LX/IpR;->a:LX/IpT;

    .line 2612925
    iget-object v1, v0, LX/IpT;->g:LX/IpQ;

    const-string v2, "p2p_request_fail"

    iget-object v3, v0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v1, v2, v3}, LX/IpQ;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2612926
    iget-object v1, v0, LX/IpT;->j:LX/Io2;

    invoke-virtual {v1}, LX/Io2;->a()V

    .line 2612927
    const-string v1, "OrionRequestMessengerPaySender"

    const-string v2, "Failed to create a request"

    invoke-static {v1, v2, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2612928
    iget-object v1, v0, LX/IpT;->e:LX/03V;

    const-string v2, "OrionRequestMessengerPaySender"

    const-string v3, "Attempted to create a request, but received a response with an error"

    invoke-virtual {v1, v2, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2612929
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2612930
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v2, :cond_0

    .line 2612931
    iget-object v1, v0, LX/IpT;->f:Landroid/content/Context;

    invoke-static {v1, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2612932
    :goto_0
    return-void

    .line 2612933
    :cond_0
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2612934
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2612935
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2612936
    iget-object v2, v0, LX/IpT;->f:Landroid/content/Context;

    iget-object v3, v0, LX/IpT;->f:Landroid/content/Context;

    const v4, 0x7f082d2a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/IpT;->f:Landroid/content/Context;

    const p0, 0x7f080016

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance p0, LX/IpS;

    invoke-direct {p0, v0}, LX/IpS;-><init>(LX/IpT;)V

    invoke-static {v2, v3, v1, v4, p0}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2612937
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2612938
    iget-object v0, p0, LX/IpR;->a:LX/IpT;

    .line 2612939
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v1, v1

    .line 2612940
    iget-object v2, v0, LX/IpT;->g:LX/IpQ;

    const-string p0, "p2p_request_success"

    iget-object p1, v0, LX/IpT;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v2, p0, p1, v1}, LX/IpQ;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Ljava/lang/String;)V

    .line 2612941
    iget-object v2, v0, LX/IpT;->j:LX/Io2;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/Io2;->a(Landroid/content/Intent;)V

    .line 2612942
    return-void
.end method
