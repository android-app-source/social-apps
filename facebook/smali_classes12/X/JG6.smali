.class public LX/JG6;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AccessibilityInfo"
.end annotation


# instance fields
.field private a:Landroid/view/accessibility/AccessibilityManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/JG5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2666802
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2666803
    iput-boolean v2, p0, LX/JG6;->c:Z

    .line 2666804
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666805
    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, LX/5pY;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 2666806
    iget-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    iput-boolean v0, p0, LX/JG6;->c:Z

    .line 2666807
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2666808
    new-instance v0, LX/JG5;

    invoke-direct {v0, p0}, LX/JG5;-><init>(LX/JG6;)V

    iput-object v0, p0, LX/JG6;->b:LX/JG5;

    .line 2666809
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/JG6;Z)V
    .locals 3

    .prologue
    .line 2666810
    iget-boolean v0, p0, LX/JG6;->c:Z

    if-eq v0, p1, :cond_0

    .line 2666811
    iput-boolean p1, p0, LX/JG6;->c:Z

    .line 2666812
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666813
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "touchExplorationDidChange"

    iget-boolean v2, p0, LX/JG6;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2666814
    :cond_0
    return-void
.end method


# virtual methods
.method public final bM_()V
    .locals 2

    .prologue
    .line 2666815
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2666816
    iget-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, LX/JG6;->b:LX/JG5;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addTouchExplorationStateChangeListener(Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;)Z

    .line 2666817
    :cond_0
    iget-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    invoke-static {p0, v0}, LX/JG6;->a$redex0(LX/JG6;Z)V

    .line 2666818
    return-void
.end method

.method public final bN_()V
    .locals 2

    .prologue
    .line 2666819
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2666820
    iget-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, LX/JG6;->b:LX/JG5;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->removeTouchExplorationStateChangeListener(Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;)Z

    .line 2666821
    :cond_0
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2666822
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2666823
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666824
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2666825
    iget-object v0, p0, LX/JG6;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    invoke-static {p0, v0}, LX/JG6;->a$redex0(LX/JG6;Z)V

    .line 2666826
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2666827
    const-string v0, "AccessibilityInfo"

    return-object v0
.end method

.method public isTouchExplorationEnabled(Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666828
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/JG6;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2666829
    return-void
.end method
