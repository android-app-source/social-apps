.class public final LX/Ixb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ixc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/IxU;

.field public final synthetic b:LX/Ixc;


# direct methods
.method public constructor <init>(LX/Ixc;)V
    .locals 1

    .prologue
    .line 2631947
    iput-object p1, p0, LX/Ixb;->b:LX/Ixc;

    .line 2631948
    move-object v0, p1

    .line 2631949
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2631950
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2631962
    const-string v0, "DiscoverPageProfileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2631951
    if-ne p0, p1, :cond_1

    .line 2631952
    :cond_0
    :goto_0
    return v0

    .line 2631953
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2631954
    goto :goto_0

    .line 2631955
    :cond_3
    check-cast p1, LX/Ixb;

    .line 2631956
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2631957
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2631958
    if-eq v2, v3, :cond_0

    .line 2631959
    iget-object v2, p0, LX/Ixb;->a:LX/IxU;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Ixb;->a:LX/IxU;

    iget-object v3, p1, LX/Ixb;->a:LX/IxU;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2631960
    goto :goto_0

    .line 2631961
    :cond_4
    iget-object v2, p1, LX/Ixb;->a:LX/IxU;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
