.class public final LX/IDA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;",
        ">;",
        "LX/IDG;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IDE;


# direct methods
.method public constructor <init>(LX/IDE;)V
    .locals 0

    .prologue
    .line 2550404
    iput-object p1, p0, LX/IDA;->a:LX/IDE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2550405
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2550406
    if-eqz p1, :cond_0

    .line 2550407
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550408
    if-eqz v0, :cond_0

    .line 2550409
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550410
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2550411
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550412
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2550413
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2550414
    sget-object v1, LX/IDE;->a:LX/4a7;

    invoke-virtual {v1}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {v0, v1}, LX/IDE;->b(Ljava/util/List;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)LX/IDG;

    move-result-object v0

    .line 2550415
    :goto_0
    return-object v0

    .line 2550416
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550417
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    .line 2550418
    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-static {v1, v0}, LX/IDE;->b(Ljava/util/List;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)LX/IDG;

    move-result-object v0

    goto :goto_0
.end method
