.class public final LX/Hf4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Hf5;

.field private b:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(LX/Hf5;)V
    .locals 0

    .prologue
    .line 2491114
    iput-object p1, p0, LX/Hf4;->a:LX/Hf5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2491115
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 2491116
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v3, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v3, v3, LX/Hf5;->a:Landroid/content/Context;

    iget-object v4, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v4, v4, LX/Hf5;->c:LX/Hf3;

    invoke-direct {v0, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/Hf4;->b:Landroid/view/GestureDetector;

    .line 2491117
    iget-object v0, p0, LX/Hf4;->b:Landroid/view/GestureDetector;

    invoke-virtual {v0, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 2491118
    iget-object v0, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->n:LX/Hep;

    invoke-virtual {v0, v2}, LX/Hep;->setVisibility(I)V

    .line 2491119
    iget-object v0, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->n:LX/Hep;

    .line 2491120
    iget-object v5, v0, LX/Hep;->f:LX/0wd;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v5, v7, v8}, LX/0wd;->b(D)LX/0wd;

    .line 2491121
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v0, v3

    .line 2491122
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    .line 2491123
    invoke-virtual {p2, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2491124
    iget-object v0, p0, LX/Hf4;->b:Landroid/view/GestureDetector;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/Hf4;->b:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 2491125
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_4

    .line 2491126
    :cond_1
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v1, v1, LX/Hf5;->n:LX/Hep;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    iget-boolean v1, v1, LX/Hf5;->z:Z

    if-nez v1, :cond_2

    .line 2491127
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    iget-object v1, v1, LX/Hf5;->n:LX/Hep;

    invoke-virtual {v1}, LX/Hep;->b()V

    .line 2491128
    :cond_2
    if-nez v0, :cond_3

    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    iget-boolean v1, v1, LX/Hf5;->z:Z

    if-nez v1, :cond_3

    .line 2491129
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 2491130
    iget-object v5, v1, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v5, v5, 0x2

    .line 2491131
    iget-object v6, v1, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v6, v6, 0x2

    .line 2491132
    if-ge v3, v5, :cond_6

    if-ge v4, v6, :cond_6

    .line 2491133
    sget-object v5, LX/Her;->TOP_LEFT:LX/Her;

    .line 2491134
    :goto_1
    move-object v5, v5

    .line 2491135
    const/4 v6, 0x0

    invoke-static {v1, v5, v6}, LX/Hf5;->a(LX/Hf5;LX/Her;Z)V

    .line 2491136
    iget-object v5, v1, LX/Hf5;->g:LX/378;

    .line 2491137
    sget-object v6, LX/7Rp;->PLAYER_DOCKED:LX/7Rp;

    invoke-static {v5, v6}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v6

    invoke-static {v6}, LX/378;->a(LX/0oG;)V

    .line 2491138
    :cond_3
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    iget-boolean v1, v1, LX/Hf5;->z:Z

    if-eqz v1, :cond_4

    .line 2491139
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    .line 2491140
    iput-boolean v2, v1, LX/Hf5;->z:Z

    .line 2491141
    iget-object v1, p0, LX/Hf4;->a:LX/Hf5;

    invoke-virtual {v1}, LX/Hf5;->f()V

    .line 2491142
    :cond_4
    return v0

    :cond_5
    move v0, v2

    goto :goto_0

    .line 2491143
    :cond_6
    if-ge v4, v6, :cond_7

    .line 2491144
    sget-object v5, LX/Her;->TOP_RIGHT:LX/Her;

    goto :goto_1

    .line 2491145
    :cond_7
    if-ge v3, v5, :cond_8

    .line 2491146
    sget-object v5, LX/Her;->BOTTOM_LEFT:LX/Her;

    goto :goto_1

    .line 2491147
    :cond_8
    sget-object v5, LX/Her;->BOTTOM_RIGHT:LX/Her;

    goto :goto_1
.end method
