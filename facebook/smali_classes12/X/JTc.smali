.class public LX/JTc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/JTa;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2696862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696863
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/JTc;->a:Ljava/util/HashMap;

    .line 2696864
    new-instance v0, LX/JTU;

    invoke-direct {v0, p1}, LX/JTU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2696865
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->SONG_ID:LX/JTa;

    .line 2696866
    iget-object v3, v0, LX/JTU;->d:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2696867
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696868
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->STORY_ID:LX/JTa;

    .line 2696869
    iget-object v3, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 2696870
    :cond_0
    const/4 v3, 0x0

    .line 2696871
    :cond_1
    :goto_0
    move-object v3, v3

    .line 2696872
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696873
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->PROVIDER:LX/JTa;

    invoke-virtual {v0}, LX/JTU;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696874
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->TRACKING_CODES:LX/JTa;

    .line 2696875
    iget-object v3, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v3, :cond_4

    .line 2696876
    const/4 v3, 0x0

    .line 2696877
    :goto_1
    move-object v3, v3

    .line 2696878
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696879
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->ALBUM:LX/JTa;

    invoke-virtual {v0}, LX/JTU;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696880
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->SONG:LX/JTa;

    invoke-virtual {v0}, LX/JTU;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696881
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->MUSICIAN:LX/JTa;

    invoke-virtual {v0}, LX/JTU;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696882
    iget-object v1, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v2, LX/JTa;->MINUTIAE_ID:LX/JTa;

    const/4 v3, 0x0

    .line 2696883
    iget-object v4, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "og_action_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2696884
    :cond_2
    :goto_2
    move-object v0, v3

    .line 2696885
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696886
    iget-object v0, p0, LX/JTc;->a:Ljava/util/HashMap;

    sget-object v1, LX/JTa;->STORY_TYPE:LX/JTa;

    sget-object v2, LX/JTb;->single:LX/JTb;

    invoke-virtual {v2}, LX/JTb;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696887
    return-void

    .line 2696888
    :cond_3
    iget-object v3, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v3

    .line 2696889
    const-string p1, "_"

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2696890
    const/16 p1, 0x5f

    invoke-virtual {v3, p1}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v3, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_4
    iget-object v3, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2696891
    :cond_5
    iget-object v4, v0, LX/JTU;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    .line 2696892
    new-instance v5, Ljava/util/StringTokenizer;

    const-string p1, "\"{}:"

    invoke-direct {v5, v4, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696893
    :cond_6
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2696894
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 2696895
    const-string p1, "og_action_id"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2696896
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method
