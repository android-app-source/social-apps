.class public final LX/JVM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JVN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLProductItem;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

.field public final synthetic d:LX/JVN;


# direct methods
.method public constructor <init>(LX/JVN;)V
    .locals 1

    .prologue
    .line 2700561
    iput-object p1, p0, LX/JVM;->d:LX/JVN;

    .line 2700562
    move-object v0, p1

    .line 2700563
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2700564
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700560
    const-string v0, "ProductsDealsForYouProductCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2700543
    if-ne p0, p1, :cond_1

    .line 2700544
    :cond_0
    :goto_0
    return v0

    .line 2700545
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2700546
    goto :goto_0

    .line 2700547
    :cond_3
    check-cast p1, LX/JVM;

    .line 2700548
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2700549
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2700550
    if-eq v2, v3, :cond_0

    .line 2700551
    iget-object v2, p0, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    iget-object v3, p1, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2700552
    goto :goto_0

    .line 2700553
    :cond_5
    iget-object v2, p1, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    if-nez v2, :cond_4

    .line 2700554
    :cond_6
    iget-object v2, p0, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2700555
    goto :goto_0

    .line 2700556
    :cond_8
    iget-object v2, p1, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2700557
    :cond_9
    iget-object v2, p0, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    iget-object v3, p1, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2700558
    goto :goto_0

    .line 2700559
    :cond_a
    iget-object v2, p1, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
