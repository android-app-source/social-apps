.class public final LX/I8H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/I8J;


# direct methods
.method public constructor <init>(LX/I8J;)V
    .locals 0

    .prologue
    .line 2540793
    iput-object p1, p0, LX/I8H;->a:LX/I8J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2540794
    iget-object v0, p0, LX/I8H;->a:LX/I8J;

    iget-object v0, v0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8H;->a:LX/I8J;

    iget-object v0, v0, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540795
    iget-object v1, v0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v0, v1

    .line 2540796
    if-nez v0, :cond_1

    .line 2540797
    :cond_0
    :goto_0
    return v5

    .line 2540798
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2540799
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2540800
    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, LX/I8H;->a:LX/I8J;

    iget-object v2, v2, LX/I8J;->a:Lcom/facebook/events/model/Event;

    .line 2540801
    iget-object v3, v2, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v2, v3

    .line 2540802
    const-string v3, "ti"

    const-string v4, "as"

    invoke-static {v2, v3, v4}, LX/1H1;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2540803
    iget-object v1, p0, LX/I8H;->a:LX/I8J;

    invoke-static {v1}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081eed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 2540804
    iget-object v1, p0, LX/I8H;->a:LX/I8J;

    iget-object v1, v1, LX/I8J;->u:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/I8H;->a:LX/I8J;

    invoke-static {v2}, LX/I8J;->c(LX/I8J;)Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
