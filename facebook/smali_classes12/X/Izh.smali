.class public LX/Izh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Izh;


# instance fields
.field private final a:LX/IzM;

.field public final b:LX/Izp;

.field private final c:LX/IzO;

.field public final d:LX/Izt;

.field public final e:LX/Izs;

.field public final f:LX/Izr;

.field private final g:LX/03V;


# direct methods
.method public constructor <init>(LX/IzM;LX/Izp;LX/IzO;LX/Izt;LX/Izs;LX/Izr;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634979
    iput-object p1, p0, LX/Izh;->a:LX/IzM;

    .line 2634980
    iput-object p2, p0, LX/Izh;->b:LX/Izp;

    .line 2634981
    iput-object p3, p0, LX/Izh;->c:LX/IzO;

    .line 2634982
    iput-object p4, p0, LX/Izh;->d:LX/Izt;

    .line 2634983
    iput-object p5, p0, LX/Izh;->e:LX/Izs;

    .line 2634984
    iput-object p6, p0, LX/Izh;->f:LX/Izr;

    .line 2634985
    iput-object p7, p0, LX/Izh;->g:LX/03V;

    .line 2634986
    return-void
.end method

.method public static a(LX/0QB;)LX/Izh;
    .locals 11

    .prologue
    .line 2634987
    sget-object v0, LX/Izh;->h:LX/Izh;

    if-nez v0, :cond_1

    .line 2634988
    const-class v1, LX/Izh;

    monitor-enter v1

    .line 2634989
    :try_start_0
    sget-object v0, LX/Izh;->h:LX/Izh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634990
    if-eqz v2, :cond_0

    .line 2634991
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2634992
    new-instance v3, LX/Izh;

    invoke-static {v0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v4

    check-cast v4, LX/IzM;

    invoke-static {v0}, LX/Izp;->a(LX/0QB;)LX/Izp;

    move-result-object v5

    check-cast v5, LX/Izp;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v6

    check-cast v6, LX/IzO;

    invoke-static {v0}, LX/Izt;->b(LX/0QB;)LX/Izt;

    move-result-object v7

    check-cast v7, LX/Izt;

    invoke-static {v0}, LX/Izs;->b(LX/0QB;)LX/Izs;

    move-result-object v8

    check-cast v8, LX/Izs;

    invoke-static {v0}, LX/Izr;->b(LX/0QB;)LX/Izr;

    move-result-object v9

    check-cast v9, LX/Izr;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-direct/range {v3 .. v10}, LX/Izh;-><init>(LX/IzM;LX/Izp;LX/IzO;LX/Izt;LX/Izs;LX/Izr;LX/03V;)V

    .line 2634993
    move-object v0, v3

    .line 2634994
    sput-object v0, LX/Izh;->h:LX/Izh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634995
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634996
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634997
    :cond_1
    sget-object v0, LX/Izh;->h:LX/Izh;

    return-object v0

    .line 2634998
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)Lcom/facebook/payments/p2p/model/PaymentTransaction;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2635000
    const-string v0, "getPaymentTransaction"

    const v1, -0x68fbd5f1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635001
    :try_start_0
    iget-object v0, p0, LX/Izh;->c:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2635002
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/IzZ;->a:LX/0U1;

    .line 2635003
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635004
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2635005
    const-string v1, "transactions"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2635006
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 2635007
    iget-object v0, p0, LX/Izh;->g:LX/03V;

    const-string v2, "DbFetchPaymentTransactionsHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Transactions table should only have one row for a given transaction ID, but it has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635008
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635009
    const v0, 0x14cea26b

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 2635010
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2635011
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2635012
    const v0, 0x71be3d03

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2635013
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2635014
    sget-object v0, LX/IzZ;->b:LX/0U1;

    .line 2635015
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 2635016
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2635017
    iget-object v2, p0, LX/Izh;->b:LX/Izp;

    invoke-virtual {v2, v0}, LX/Izp;->b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v3

    .line 2635018
    if-nez v3, :cond_4

    .line 2635019
    const/4 v2, 0x0

    .line 2635020
    :goto_1
    move-object v2, v2

    .line 2635021
    sget-object v0, LX/IzZ;->c:LX/0U1;

    .line 2635022
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 2635023
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2635024
    iget-object v3, p0, LX/Izh;->b:LX/Izp;

    invoke-virtual {v3, v0}, LX/Izp;->b(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v4

    .line 2635025
    if-nez v4, :cond_5

    .line 2635026
    const/4 v3, 0x0

    .line 2635027
    :goto_2
    move-object v3, v3

    .line 2635028
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 2635029
    :cond_2
    const/4 v0, 0x0

    .line 2635030
    :goto_3
    move-object v0, v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2635031
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2635032
    const v1, 0x3956e6a2

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635033
    :catch_0
    move-exception v0

    .line 2635034
    :try_start_7
    iget-object v2, p0, LX/Izh;->g:LX/03V;

    const-string v3, "DbFetchPaymentTransactionsHandler"

    const-string v4, "Reading the transaction from the database threw an exception."

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2635035
    :try_start_8
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2635036
    const v0, 0x27761f35

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2635037
    :catchall_0
    move-exception v0

    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2635038
    :catchall_1
    move-exception v0

    const v1, -0x2646f7be

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635039
    :cond_3
    new-instance v4, Lcom/facebook/payments/p2p/model/Amount;

    sget-object v0, LX/IzZ;->j:LX/0U1;

    .line 2635040
    iget-object v5, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v5

    .line 2635041
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v5, LX/IzZ;->i:LX/0U1;

    .line 2635042
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2635043
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    sget-object v6, LX/IzZ;->h:LX/0U1;

    .line 2635044
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2635045
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-direct {v4, v0, v5, v6}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2635046
    new-instance v5, Lcom/facebook/payments/p2p/model/Amount;

    sget-object v0, LX/IzZ;->j:LX/0U1;

    .line 2635047
    iget-object v6, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v6

    .line 2635048
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v6, LX/IzZ;->i:LX/0U1;

    .line 2635049
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2635050
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    sget-object v7, LX/IzZ;->k:LX/0U1;

    .line 2635051
    iget-object p1, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, p1

    .line 2635052
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-direct {v5, v0, v6, v7}, Lcom/facebook/payments/p2p/model/Amount;-><init>(Ljava/lang/String;II)V

    .line 2635053
    new-instance v0, LX/Dtx;

    invoke-direct {v0}, LX/Dtx;-><init>()V

    sget-object v6, LX/IzZ;->l:LX/0U1;

    .line 2635054
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2635055
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2635056
    iput-object v6, v0, LX/Dtx;->a:Ljava/lang/String;

    .line 2635057
    move-object v6, v0

    .line 2635058
    iget-object v0, p0, LX/Izh;->d:LX/Izt;

    sget-object v7, LX/IzZ;->m:LX/0U1;

    .line 2635059
    iget-object p1, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, p1

    .line 2635060
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/Izt;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2635061
    iput-object v0, v6, LX/Dtx;->b:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2635062
    move-object v0, v6

    .line 2635063
    invoke-virtual {v0}, LX/Dtx;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    move-result-object v0

    .line 2635064
    invoke-static {}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->newBuilder()LX/DtJ;

    move-result-object v6

    sget-object v7, LX/IzZ;->a:LX/0U1;

    .line 2635065
    iget-object p1, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, p1

    .line 2635066
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2635067
    iput-object v7, v6, LX/DtJ;->a:Ljava/lang/String;

    .line 2635068
    move-object v6, v6

    .line 2635069
    iput-object v2, v6, LX/DtJ;->c:Lcom/facebook/payments/p2p/model/Sender;

    .line 2635070
    move-object v2, v6

    .line 2635071
    iput-object v3, v2, LX/DtJ;->d:Lcom/facebook/payments/p2p/model/Receiver;

    .line 2635072
    move-object v2, v2

    .line 2635073
    sget-object v3, LX/IzZ;->e:LX/0U1;

    .line 2635074
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 2635075
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2635076
    iput-object v3, v2, LX/DtJ;->e:Ljava/lang/String;

    .line 2635077
    move-object v2, v2

    .line 2635078
    sget-object v3, LX/IzZ;->f:LX/0U1;

    .line 2635079
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 2635080
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2635081
    iput-object v3, v2, LX/DtJ;->h:Ljava/lang/String;

    .line 2635082
    move-object v2, v2

    .line 2635083
    sget-object v3, LX/IzZ;->g:LX/0U1;

    .line 2635084
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 2635085
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2635086
    iput-object v3, v2, LX/DtJ;->g:Ljava/lang/String;

    .line 2635087
    move-object v2, v2

    .line 2635088
    sget-object v3, LX/IzZ;->d:LX/0U1;

    .line 2635089
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 2635090
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v3

    .line 2635091
    iput-object v3, v2, LX/DtJ;->f:LX/DtQ;

    .line 2635092
    move-object v2, v2

    .line 2635093
    iput-object v4, v2, LX/DtJ;->i:Lcom/facebook/payments/p2p/model/Amount;

    .line 2635094
    move-object v2, v2

    .line 2635095
    iput-object v5, v2, LX/DtJ;->j:Lcom/facebook/payments/p2p/model/Amount;

    .line 2635096
    move-object v2, v2

    .line 2635097
    iput-object v0, v2, LX/DtJ;->k:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$TransferContextModel;

    .line 2635098
    move-object v0, v2

    .line 2635099
    iget-object v2, p0, LX/Izh;->e:LX/Izs;

    sget-object v3, LX/IzZ;->n:LX/0U1;

    .line 2635100
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635101
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Izs;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    move-result-object v2

    .line 2635102
    iput-object v2, v0, LX/DtJ;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PlatformItemModel;

    .line 2635103
    move-object v0, v0

    .line 2635104
    iget-object v2, p0, LX/Izh;->f:LX/Izr;

    sget-object v3, LX/IzZ;->o:LX/0U1;

    .line 2635105
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635106
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Izr;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-result-object v2

    .line 2635107
    iput-object v2, v0, LX/DtJ;->m:Lcom/facebook/payments/p2p/model/CommerceOrder;

    .line 2635108
    move-object v0, v0

    .line 2635109
    invoke-virtual {v0}, LX/DtJ;->o()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    goto/16 :goto_3

    :cond_4
    new-instance v2, Lcom/facebook/payments/p2p/model/Sender;

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v3

    invoke-direct {v2, v0, v4, v3}, Lcom/facebook/payments/p2p/model/Sender;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_5
    new-instance v3, Lcom/facebook/payments/p2p/model/Receiver;

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v4

    invoke-direct {v3, v0, v5, v4}, Lcom/facebook/payments/p2p/model/Receiver;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_2
.end method
