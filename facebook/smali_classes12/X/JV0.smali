.class public LX/JV0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUy;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JV1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700056
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JV0;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JV1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700057
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700058
    iput-object p1, p0, LX/JV0;->b:LX/0Ot;

    .line 2700059
    return-void
.end method

.method public static a(LX/0QB;)LX/JV0;
    .locals 4

    .prologue
    .line 2700045
    const-class v1, LX/JV0;

    monitor-enter v1

    .line 2700046
    :try_start_0
    sget-object v0, LX/JV0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700047
    sput-object v2, LX/JV0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700048
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700049
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700050
    new-instance v3, LX/JV0;

    const/16 p0, 0x2089

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JV0;-><init>(LX/0Ot;)V

    .line 2700051
    move-object v0, v3

    .line 2700052
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700053
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JV0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700054
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700055
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2700025
    iget-object v0, p0, LX/JV0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JV1;

    .line 2700026
    iget-object p0, v0, LX/JV1;->b:LX/2g9;

    invoke-virtual {p0, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object p0

    const/16 p2, 0x44

    invoke-virtual {p0, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object p0

    const p2, 0x7f081857

    invoke-virtual {p0, p2}, LX/2gA;->i(I)LX/2gA;

    move-result-object p0

    const p2, 0x7f0209c7

    invoke-virtual {p0, p2}, LX/2gA;->k(I)LX/2gA;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 2700027
    const p2, -0x5d4c3768

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2700028
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 2700029
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2700030
    invoke-static {}, LX/1dS;->b()V

    .line 2700031
    iget v0, p1, LX/1dQ;->b:I

    .line 2700032
    packed-switch v0, :pswitch_data_0

    .line 2700033
    :goto_0
    return-object v2

    .line 2700034
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700035
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2700036
    check-cast v1, LX/JUz;

    .line 2700037
    iget-object v3, p0, LX/JV0;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JV1;

    iget-object v4, v1, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    iget-object v5, v1, LX/JUz;->b:LX/JVO;

    .line 2700038
    iget-object v6, v3, LX/JV1;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Kf;

    const/4 p1, 0x0

    sget-object v7, LX/21D;->NEWSFEED:LX/21D;

    const-string p2, "pdfyShareButton"

    invoke-static {v4}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object p0

    invoke-virtual {p0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p0

    invoke-static {v7, p2, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p2

    const/16 p0, 0x6dc

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    const-class v1, Landroid/app/Activity;

    invoke-static {v7, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-interface {v6, p1, p2, p0, v7}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2700039
    if-eqz v5, :cond_0

    .line 2700040
    iget-object v7, v5, LX/JVO;->d:Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v6, v5, LX/JVO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700041
    iget-object p1, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, p1

    .line 2700042
    check-cast v6, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    iget-object p1, v5, LX/JVO;->b:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    const-string p2, "pdfy_share"

    invoke-static {v7, v6, p1, p2}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a$redex0(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Ljava/lang/String;)V

    .line 2700043
    iget-object v6, v5, LX/JVO;->d:Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v6, v6, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->g:LX/1g6;

    sget-object v7, LX/7iQ;->PDFY_PRODUCT_SHARE_CLICK:LX/7iQ;

    iget-object p1, v5, LX/JVO;->c:Lcom/facebook/graphql/model/GraphQLProductItem;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, LX/1g6;->a(LX/7iQ;Ljava/lang/String;)V

    .line 2700044
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5d4c3768
        :pswitch_0
    .end packed-switch
.end method
