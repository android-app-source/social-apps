.class public final LX/I5A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V
    .locals 0

    .prologue
    .line 2535093
    iput-object p1, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2535100
    iget-object v0, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2535101
    iget-object v0, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2535102
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2535103
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2535094
    check-cast p1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2535095
    iget-object v0, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2535096
    if-eqz p1, :cond_0

    .line 2535097
    iget-object v0, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iput-object p1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2535098
    iget-object v0, p0, LX/I5A;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    const v1, 0x223a4879

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2535099
    :cond_0
    return-void
.end method
