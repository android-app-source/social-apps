.class public LX/IOj;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IOj;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2572906
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2572907
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2572908
    const-string v1, "groups/addtogroups/{%s}?profile_name={%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.facebook.katana.profile.id"

    const-string v3, "profile_name"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUPS_ADD_TO_GROUPS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2572909
    return-void
.end method

.method public static a(LX/0QB;)LX/IOj;
    .locals 3

    .prologue
    .line 2572910
    sget-object v0, LX/IOj;->a:LX/IOj;

    if-nez v0, :cond_1

    .line 2572911
    const-class v1, LX/IOj;

    monitor-enter v1

    .line 2572912
    :try_start_0
    sget-object v0, LX/IOj;->a:LX/IOj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2572913
    if-eqz v2, :cond_0

    .line 2572914
    :try_start_1
    new-instance v0, LX/IOj;

    invoke-direct {v0}, LX/IOj;-><init>()V

    .line 2572915
    move-object v0, v0

    .line 2572916
    sput-object v0, LX/IOj;->a:LX/IOj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2572917
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2572918
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2572919
    :cond_1
    sget-object v0, LX/IOj;->a:LX/IOj;

    return-object v0

    .line 2572920
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2572921
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
