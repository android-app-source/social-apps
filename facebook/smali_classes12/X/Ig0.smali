.class public LX/Ig0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ifr;
.implements LX/Ifs;
.implements LX/Ift;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final d:LX/0lC;

.field public e:LX/Ifv;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600412
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    iput-object v0, p0, LX/Ig0;->d:LX/0lC;

    .line 2600413
    return-void
.end method

.method public static d(LX/Ig0;LX/Ig6;)V
    .locals 8

    .prologue
    .line 2600382
    iget-object v0, p0, LX/Ig0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600383
    sget-object v1, LX/Ig2;->b:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600384
    iget-object v1, p1, LX/Ig6;->d:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2600385
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600386
    iget-object v1, p1, LX/Ig6;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2600387
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600388
    iget-object v1, p0, LX/Ig0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2600389
    invoke-virtual {p1}, LX/Ig6;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2600390
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2600391
    const-string v3, "userKey"

    .line 2600392
    iget-object v4, p1, LX/Ig6;->d:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2600393
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600394
    const-string v3, "threadKey"

    .line 2600395
    iget-object v4, p1, LX/Ig6;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v4, v4

    .line 2600396
    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600397
    const-string v3, "expireTimeMillis"

    .line 2600398
    iget-wide v6, p1, LX/Ig6;->f:J

    move-wide v4, v6

    .line 2600399
    invoke-virtual {v2, v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2600400
    const-string v3, "offlineThreadingId"

    .line 2600401
    iget-object v4, p1, LX/Ig6;->h:Ljava/lang/String;

    move-object v4, v4

    .line 2600402
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600403
    const-string v3, "attachmentId"

    .line 2600404
    iget-object v4, p1, LX/Ig6;->i:Ljava/lang/String;

    move-object v4, v4

    .line 2600405
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600406
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2600407
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2600408
    :goto_0
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2600409
    return-void

    .line 2600410
    :cond_0
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ifv;)V
    .locals 13

    .prologue
    .line 2600357
    iput-object p1, p0, LX/Ig0;->e:LX/Ifv;

    .line 2600358
    iget-object v0, p0, LX/Ig0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600359
    iget-object v1, p0, LX/Ig0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Ig2;->b:LX/0Tn;

    invoke-virtual {v2, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 2600360
    iget-object v1, p0, LX/Ig0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2600361
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600362
    const/4 v5, 0x0

    .line 2600363
    iget-object v4, p0, LX/Ig0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v0, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2600364
    :try_start_0
    iget-object v6, p0, LX/Ig0;->d:LX/0lC;

    invoke-virtual {v6, v4}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    check-cast v4, LX/0m9;

    .line 2600365
    const-string v6, "userKey"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v6

    .line 2600366
    const-string v7, "threadKey"

    invoke-virtual {v4, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-virtual {v7}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v7

    .line 2600367
    const-string v8, "expireTimeMillis"

    invoke-virtual {v4, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->D()J

    move-result-wide v8

    .line 2600368
    const-string v10, "offlineThreadingId"

    invoke-virtual {v4, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    invoke-virtual {v10}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v10

    .line 2600369
    const-string v11, "attachmentId"

    invoke-virtual {v4, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v11

    .line 2600370
    iget-object v4, p0, LX/Ig0;->e:LX/Ifv;

    invoke-virtual {v4, v6, v7}, LX/Ifv;->a(Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Ig6;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2600371
    :try_start_1
    invoke-virtual {v4, v10}, LX/Ig6;->a(Ljava/lang/String;)V

    .line 2600372
    invoke-virtual {v4, v11}, LX/Ig6;->b(Ljava/lang/String;)V

    .line 2600373
    invoke-virtual {v4, v8, v9}, LX/Ig6;->a(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2600374
    :goto_1
    move-object v3, v4

    .line 2600375
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/Ig6;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2600376
    :cond_1
    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    goto :goto_0

    .line 2600377
    :cond_2
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2600378
    return-void

    .line 2600379
    :catch_0
    move-exception v4

    move-object v12, v4

    move-object v4, v5

    move-object v5, v12

    .line 2600380
    :goto_2
    iget-object v6, p0, LX/Ig0;->c:LX/03V;

    const-string v7, "Error reading object from persistent storage."

    invoke-virtual {v6, v7, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2600381
    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method public final a(LX/Ig6;)V
    .locals 0

    .prologue
    .line 2600355
    invoke-static {p0, p1}, LX/Ig0;->d(LX/Ig0;LX/Ig6;)V

    .line 2600356
    return-void
.end method

.method public final b(LX/Ig6;)V
    .locals 0

    .prologue
    .line 2600353
    invoke-static {p0, p1}, LX/Ig0;->d(LX/Ig0;LX/Ig6;)V

    .line 2600354
    return-void
.end method

.method public final c(LX/Ig6;)V
    .locals 0

    .prologue
    .line 2600351
    invoke-static {p0, p1}, LX/Ig0;->d(LX/Ig0;LX/Ig6;)V

    .line 2600352
    return-void
.end method
