.class public final LX/Hvc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/1c9",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Hvj;


# direct methods
.method public constructor <init>(LX/Hvj;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2519370
    iput-object p1, p0, LX/Hvc;->b:LX/Hvj;

    iput-object p2, p0, LX/Hvc;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2519371
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->g:LX/IF5;

    iget-object v1, p0, LX/Hvc;->a:Ljava/lang/String;

    .line 2519372
    iget-object v2, v0, LX/IF5;->a:LX/0Zb;

    sget-object p1, LX/IF3;->FETCH_STYLE_FAILED:LX/IF3;

    invoke-virtual {p1}, LX/IF3;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519373
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->h:LX/IF6;

    iget-object v1, p0, LX/Hvc;->a:Ljava/lang/String;

    .line 2519374
    iget-object v2, v0, LX/IF6;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xe000c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/4 p1, 0x3

    invoke-interface {v2, v3, v4, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2519375
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    invoke-static {v0}, LX/Hvj;->e(LX/Hvj;)V

    .line 2519376
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2519377
    check-cast p1, LX/1c9;

    .line 2519378
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->h:LX/IF6;

    iget-object v1, p0, LX/Hvc;->a:Ljava/lang/String;

    .line 2519379
    iget-object v2, v0, LX/IF6;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xe000c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/4 v5, 0x2

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2519380
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1c9;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2519381
    :cond_0
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->g:LX/IF5;

    iget-object v1, p0, LX/Hvc;->a:Ljava/lang/String;

    .line 2519382
    iget-object v2, v0, LX/IF5;->a:LX/0Zb;

    sget-object v3, LX/IF3;->FETCH_STYLE_EMPTY:LX/IF3;

    invoke-virtual {v3}, LX/IF3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519383
    :goto_0
    return-void

    .line 2519384
    :cond_1
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->g:LX/IF5;

    iget-object v1, p0, LX/Hvc;->a:Ljava/lang/String;

    .line 2519385
    iget-object v2, v0, LX/IF5;->a:LX/0Zb;

    sget-object v3, LX/IF3;->FETCH_STYLE_SUCCESS:LX/IF3;

    invoke-virtual {v3}, LX/IF3;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/IF5;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2519386
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    .line 2519387
    iput-object p1, v0, LX/Hvj;->A:LX/1c9;

    .line 2519388
    iget-object v0, p0, LX/Hvc;->b:LX/Hvj;

    invoke-static {v0}, LX/Hvj;->f$redex0(LX/Hvj;)V

    goto :goto_0
.end method
