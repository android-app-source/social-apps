.class public final LX/IFu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "LX/IFN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIQ;

.field public final synthetic b:LX/IFv;

.field private c:Z


# direct methods
.method public constructor <init>(LX/IFv;LX/IIQ;)V
    .locals 1

    .prologue
    .line 2554982
    iput-object p1, p0, LX/IFu;->b:LX/IFv;

    iput-object p2, p0, LX/IFu;->a:LX/IIQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554983
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IFu;->c:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2554973
    iget-object v0, p0, LX/IFu;->a:LX/IIQ;

    .line 2554974
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v2, LX/IG6;->DASHBOARD_FETCH_DATA1:LX/IG6;

    invoke-virtual {v1, v2}, LX/IG7;->b(LX/IG6;)V

    .line 2554975
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->o:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    .line 2554976
    iput-wide v3, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ad:J

    .line 2554977
    iget-boolean v1, v0, LX/IIQ;->d:Z

    if-eqz v1, :cond_1

    .line 2554978
    iget-object v1, v0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v1}, LX/IIm;->i()V

    .line 2554979
    :cond_0
    :goto_0
    return-void

    .line 2554980
    :cond_1
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    iget-object v2, v0, LX/IIQ;->c:LX/IIm;

    if-ne v1, v2, :cond_0

    .line 2554981
    iget-object v1, v0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v1}, LX/IIm;->k()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2554941
    check-cast p1, LX/IFN;

    .line 2554942
    iget-boolean v0, p0, LX/IFu;->c:Z

    if-eqz v0, :cond_1

    .line 2554943
    iget-object v0, p0, LX/IFu;->a:LX/IIQ;

    .line 2554944
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2554945
    iget-object v2, v1, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2554946
    invoke-static {v1}, LX/IFX;->r(LX/IFX;)V

    .line 2554947
    invoke-static {v1, p1}, LX/IFX;->c(LX/IFX;LX/IFN;)V

    .line 2554948
    invoke-static {v1}, LX/IFX;->o(LX/IFX;)V

    .line 2554949
    invoke-static {v1}, LX/IFX;->t(LX/IFX;)V

    .line 2554950
    iget-object v1, p1, LX/IFN;->a:LX/0am;

    .line 2554951
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->d()LX/1Fb;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2554952
    :cond_0
    :goto_0
    iget-object v1, p1, LX/IFN;->d:LX/0am;

    invoke-static {v0, v1}, LX/IIQ;->b(LX/IIQ;LX/0am;)V

    .line 2554953
    iget-object v1, p1, LX/IFN;->b:LX/0am;

    iget-object v2, p1, LX/IFN;->c:LX/0am;

    invoke-static {v0, v1, v2}, LX/IIQ;->a(LX/IIQ;LX/0am;LX/0am;)V

    .line 2554954
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IFu;->c:Z

    .line 2554955
    return-void

    .line 2554956
    :cond_1
    iget-object v0, p0, LX/IFu;->a:LX/IIQ;

    .line 2554957
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2554958
    iget-object v2, v1, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2554959
    invoke-static {v1, p1}, LX/IFX;->c(LX/IFX;LX/IFN;)V

    .line 2554960
    invoke-static {v1}, LX/IFX;->o(LX/IFX;)V

    .line 2554961
    invoke-static {v1}, LX/IFX;->t(LX/IFX;)V

    .line 2554962
    iget-object v1, p1, LX/IFN;->d:LX/0am;

    invoke-static {v0, v1}, LX/IIQ;->b(LX/IIQ;LX/0am;)V

    .line 2554963
    iget-object v1, p1, LX/IFN;->b:LX/0am;

    iget-object v2, p1, LX/IFN;->c:LX/0am;

    invoke-static {v0, v1, v2}, LX/IIQ;->a(LX/IIQ;LX/0am;LX/0am;)V

    .line 2554964
    goto :goto_1

    .line 2554965
    :cond_2
    iget-object v2, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v3, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 2554966
    iput-object v2, v3, LX/IFX;->B:Ljava/lang/String;

    .line 2554967
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2554968
    iget-object v0, p0, LX/IFu;->a:LX/IIQ;

    .line 2554969
    iget-object v1, v0, LX/IIQ;->a:LX/IIm;

    invoke-virtual {v1}, LX/IIm;->k()V

    .line 2554970
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->j:LX/03V;

    const-string p0, "friends_nearby_fetch_failed"

    invoke-virtual {v1, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2554971
    iget-object v1, v0, LX/IIQ;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_FETCH_DATA1:LX/IG6;

    invoke-virtual {v1, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2554972
    return-void
.end method
