.class public final LX/Hfm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hfn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:F

.field public c:LX/BcO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcO",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2492309
    invoke-static {}, LX/Hfn;->q()LX/Hfn;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2492310
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2492311
    const-string v0, "GroupDiscoveryHScrollCollectionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2492312
    if-ne p0, p1, :cond_1

    .line 2492313
    :cond_0
    :goto_0
    return v0

    .line 2492314
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2492315
    goto :goto_0

    .line 2492316
    :cond_3
    check-cast p1, LX/Hfm;

    .line 2492317
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2492318
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2492319
    if-eq v2, v3, :cond_0

    .line 2492320
    iget v2, p0, LX/Hfm;->a:I

    iget v3, p1, LX/Hfm;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2492321
    goto :goto_0

    .line 2492322
    :cond_4
    iget v2, p0, LX/Hfm;->b:F

    iget v3, p1, LX/Hfm;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 2492323
    goto :goto_0

    .line 2492324
    :cond_5
    iget-object v2, p0, LX/Hfm;->c:LX/BcO;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Hfm;->c:LX/BcO;

    iget-object v3, p1, LX/Hfm;->c:LX/BcO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2492325
    goto :goto_0

    .line 2492326
    :cond_6
    iget-object v2, p1, LX/Hfm;->c:LX/BcO;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
