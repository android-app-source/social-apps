.class public final LX/HN6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HMl;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:J

.field public final synthetic d:LX/HN7;


# direct methods
.method public constructor <init>(LX/HN7;LX/HMl;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2458124
    iput-object p1, p0, LX/HN6;->d:LX/HN7;

    iput-object p2, p0, LX/HN6;->a:LX/HMl;

    iput-object p3, p0, LX/HN6;->b:Ljava/lang/String;

    iput-wide p4, p0, LX/HN6;->c:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2458125
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v1, p0, LX/HN6;->b:Ljava/lang/String;

    .line 2458126
    iput-object v1, v0, LX/HN7;->e:Ljava/lang/String;

    .line 2458127
    iget-object v0, p0, LX/HN6;->a:LX/HMl;

    invoke-interface {v0}, LX/HMl;->a()V

    .line 2458128
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v0, v0, LX/HN7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817cb

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2458129
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v0, v0, LX/HN7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    iget-wide v2, p0, LX/HN6;->c:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v2, v3, v1}, LX/HPs;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2458130
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2458131
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2458132
    if-eqz p1, :cond_2

    .line 2458133
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2458134
    if-eqz v0, :cond_2

    .line 2458135
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2458136
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2458137
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 2458138
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2458139
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2458140
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_4

    .line 2458141
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2458142
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2458143
    iget-object v3, p0, LX/HN6;->d:LX/HN7;

    iget-object v3, v3, LX/HN7;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2458144
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2458145
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesMutationsModels$ServicesSectionUpdateMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HN6;->d:LX/HN7;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2458146
    iput-object v0, v3, LX/HN7;->e:Ljava/lang/String;

    .line 2458147
    iget-object v0, p0, LX/HN6;->a:LX/HMl;

    invoke-interface {v0}, LX/HMl;->a()V

    .line 2458148
    :cond_0
    :goto_2
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v0, v0, LX/HN7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    iget-wide v2, p0, LX/HN6;->c:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v2, v3, v1}, LX/HPs;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2458149
    return-void

    :cond_1
    move v0, v1

    .line 2458150
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2458151
    :cond_4
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v1, p0, LX/HN6;->b:Ljava/lang/String;

    .line 2458152
    iput-object v1, v0, LX/HN7;->e:Ljava/lang/String;

    .line 2458153
    iget-object v0, p0, LX/HN6;->a:LX/HMl;

    invoke-interface {v0}, LX/HMl;->a()V

    .line 2458154
    iget-object v0, p0, LX/HN6;->d:LX/HN7;

    iget-object v0, v0, LX/HN7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817cb

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_2
.end method
