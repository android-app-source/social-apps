.class public final LX/HqH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Rm;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 1

    .prologue
    .line 2510549
    iput-object p1, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510550
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HqH;->b:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2510551
    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_OPEN_PRIVACY:LX/0ge;

    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v4, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/0gd;->a(LX/0ge;Ljava/lang/String;JLX/2rt;)V

    .line 2510552
    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    sget-object v1, LX/ASW;->INTERSTITIAL_NUX:LX/ASW;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/ASX;->a(LX/ASW;Z)Z

    .line 2510553
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2510554
    iget-boolean v0, p0, LX/HqH;->b:Z

    if-nez v0, :cond_0

    .line 2510555
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HqH;->b:Z

    .line 2510556
    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_PICKS_PRIVACY_FROM_TYPEAHEAD_FILTER:LX/0ge;

    iget-object v0, p0, LX/HqH;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510557
    :cond_0
    return-void
.end method
