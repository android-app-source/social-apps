.class public LX/I10;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/1Ad;

.field public final c:LX/0wM;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Ad;LX/0wM;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2528425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2528426
    iput-object p1, p0, LX/I10;->a:Landroid/content/Context;

    .line 2528427
    iput-object p2, p0, LX/I10;->b:LX/1Ad;

    .line 2528428
    iput-object p3, p0, LX/I10;->c:LX/0wM;

    .line 2528429
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/I10;->d:I

    .line 2528430
    return-void
.end method

.method public static a(LX/7oW;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2528422
    invoke-interface {p0}, LX/7oW;->eP_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2528423
    invoke-interface {p0}, LX/7oW;->eP_()Ljava/lang/String;

    move-result-object v0

    .line 2528424
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/7oW;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/I10;LX/7oa;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7oa;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/1aZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2528374
    invoke-static {p1}, LX/I10;->s(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2528375
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2528376
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2528377
    :goto_0
    move-object v0, v0

    .line 2528378
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2528379
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    .line 2528380
    invoke-interface {v0}, LX/7ob;->e()LX/1Fb;

    move-result-object v0

    .line 2528381
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2528382
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v3, LX/1o9;

    iget v4, p0, LX/I10;->d:I

    iget v5, p0, LX/I10;->d:I

    invoke-direct {v3, v4, v5}, LX/1o9;-><init>(II)V

    .line 2528383
    iput-object v3, v0, LX/1bX;->c:LX/1o9;

    .line 2528384
    move-object v0, v0

    .line 2528385
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2528386
    iget-object v3, p0, LX/I10;->b:LX/1Ad;

    invoke-virtual {v3, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2528387
    :cond_1
    const/4 v6, 0x2

    const/4 v0, 0x0

    .line 2528388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2528389
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p1}, LX/I10;->u(LX/7oa;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_2
    move-object v0, v1

    .line 2528390
    :goto_3
    move-object v0, v0

    .line 2528391
    goto :goto_1

    .line 2528392
    :cond_3
    return-object v1

    .line 2528393
    :cond_4
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2528394
    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v3

    .line 2528395
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_4
    if-ge v2, v4, :cond_7

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    .line 2528396
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2528397
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2528398
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v5, 0x2

    if-eq v0, v5, :cond_7

    .line 2528399
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2528400
    :cond_6
    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2528401
    invoke-static {p1}, LX/I10;->e(LX/7oa;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 2528402
    goto/16 :goto_0

    .line 2528403
    :cond_8
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2528404
    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v3

    .line 2528405
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_5
    if-ge v2, v4, :cond_f

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    .line 2528406
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 2528407
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2528408
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v0, v6, :cond_f

    .line 2528409
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2528410
    :cond_a
    invoke-static {p1}, LX/I10;->x(LX/7oa;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2528411
    invoke-interface {p1}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v3

    .line 2528412
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_6
    if-ge v2, v4, :cond_f

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;

    .line 2528413
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 2528414
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->a()LX/7ob;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2528415
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v0, v6, :cond_f

    .line 2528416
    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 2528417
    :cond_c
    invoke-static {p1}, LX/I10;->y(LX/7oa;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-static {p1}, LX/I10;->z(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    move-object v0, v1

    .line 2528418
    goto/16 :goto_3

    .line 2528419
    :cond_e
    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2528420
    invoke-static {p1}, LX/I10;->e(LX/7oa;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_3

    :cond_f
    move-object v0, v1

    .line 2528421
    goto/16 :goto_3
.end method

.method public static e(LX/7oa;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7oa;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7ob;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2528370
    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    .line 2528371
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2528372
    const/4 v0, 0x1

    new-array v1, v0, [LX/7ob;

    invoke-interface {p0}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7ob;

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 2528373
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public static j(LX/I10;LX/7oa;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2528340
    invoke-static {p1}, LX/I10;->t(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2528341
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0837da

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v3

    invoke-interface {v3}, LX/7oe;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-interface {p1}, LX/7oa;->H()LX/7od;

    move-result-object v3

    invoke-interface {v3}, LX/7od;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2528342
    :goto_0
    return-object v0

    .line 2528343
    :cond_0
    invoke-static {p1}, LX/I10;->u(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2528344
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0172

    invoke-interface {p1}, LX/7oa;->X()I

    move-result v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->X()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2528345
    :cond_1
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2528346
    invoke-static {p0, p1}, LX/I10;->n(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2528347
    :cond_2
    invoke-static {p1}, LX/I10;->x(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2528348
    invoke-interface {p1}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 2528349
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2528350
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;

    .line 2528351
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2528352
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2528353
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2528354
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->b()I

    move-result v0

    const/4 v5, 0x2

    const/4 p1, 0x1

    const/4 v6, 0x0

    .line 2528355
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ne v2, p1, :cond_9

    .line 2528356
    iget-object v3, p0, LX/I10;->a:Landroid/content/Context;

    const v4, 0x7f0837d1

    new-array v5, p1, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2528357
    :goto_2
    move-object v0, v2

    .line 2528358
    move-object v0, v0

    .line 2528359
    goto/16 :goto_0

    .line 2528360
    :cond_5
    invoke-static {p1}, LX/I10;->y(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2528361
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0173

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v2

    invoke-interface {v2}, LX/7oe;->a()I

    move-result v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->I()LX/7oe;

    move-result-object v4

    invoke-interface {v4}, LX/7oe;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2528362
    :cond_6
    invoke-static {p1}, LX/I10;->z(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2528363
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0174

    invoke-interface {p1}, LX/7oa;->H()LX/7od;

    move-result-object v2

    invoke-interface {v2}, LX/7od;->a()I

    move-result v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/7oa;->H()LX/7od;

    move-result-object v4

    invoke-interface {v4}, LX/7od;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2528364
    :cond_7
    invoke-interface {p1}, LX/7oa;->r()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, LX/7oa;->Y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2528365
    invoke-static {p0, p1}, LX/I10;->o(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2528366
    :cond_8
    invoke-static {p0, p1}, LX/I10;->q(LX/I10;LX/7oa;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2528367
    :cond_9
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v5, :cond_a

    .line 2528368
    iget-object v3, p0, LX/I10;->a:Landroid/content/Context;

    const v4, 0x7f0837d2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 2528369
    :cond_a
    iget-object v2, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837d3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2
.end method

.method public static n(LX/I10;LX/7oa;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2528326
    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 2528327
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2528328
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;

    .line 2528329
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2528330
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2528331
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2528332
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v0

    const/4 v5, 0x2

    const/4 p1, 0x1

    const/4 v6, 0x0

    .line 2528333
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 2528334
    iget-object v3, p0, LX/I10;->a:Landroid/content/Context;

    const v4, 0x7f0837ce

    new-array v5, p1, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2528335
    :goto_1
    move-object v0, v2

    .line 2528336
    return-object v0

    .line 2528337
    :cond_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 2528338
    iget-object v3, p0, LX/I10;->a:Landroid/content/Context;

    const v4, 0x7f0837cf

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2528339
    :cond_3
    iget-object v2, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837d0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7oW;

    invoke-static {v2}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static o(LX/I10;LX/7oa;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2528324
    invoke-interface {p1}, LX/7oa;->Y()LX/0Px;

    move-result-object v2

    .line 2528325
    iget-object v3, p0, LX/I10;->a:Landroid/content/Context;

    invoke-static {p1}, LX/I10;->s(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0837d4

    move v1, v0

    :goto_0
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oW;

    invoke-static {v0}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0837d5

    move v1, v0

    goto :goto_0
.end method

.method public static q(LX/I10;LX/7oa;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2528322
    invoke-interface {p1}, LX/7oa;->C()LX/7oW;

    move-result-object v0

    .line 2528323
    iget-object v1, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837d8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, LX/I10;->a(LX/7oW;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static r(LX/I10;LX/7oa;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2528319
    invoke-static {p1}, LX/I10;->w(LX/7oa;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2528320
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0177

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2528321
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I10;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0837dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static s(LX/7oa;)Z
    .locals 2

    .prologue
    .line 2528318
    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->RSVP:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528311
    invoke-interface {p0}, LX/7oa;->M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->M()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528317
    invoke-interface {p0}, LX/7oa;->X()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(LX/7oa;)Z
    .locals 2

    .prologue
    .line 2528316
    invoke-interface {p0}, LX/7oa;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528315
    invoke-interface {p0}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->P()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528314
    invoke-interface {p0}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->Q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static y(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528313
    invoke-interface {p0}, LX/7oa;->I()LX/7oe;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->I()LX/7oe;

    move-result-object v0

    invoke-interface {v0}, LX/7oe;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static z(LX/7oa;)Z
    .locals 1

    .prologue
    .line 2528312
    invoke-interface {p0}, LX/7oa;->H()LX/7od;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/7oa;->H()LX/7od;

    move-result-object v0

    invoke-interface {v0}, LX/7od;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
