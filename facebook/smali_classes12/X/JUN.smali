.class public LX/JUN;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private a:I

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2698161
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2698162
    iput p1, p0, LX/JUN;->a:I

    .line 2698163
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/JUN;->b:Landroid/graphics/Paint;

    .line 2698164
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JUN;->c:Landroid/graphics/Path;

    .line 2698165
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2698150
    invoke-virtual {p0}, LX/JUN;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 2698151
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 2698152
    iget-object v2, p0, LX/JUN;->b:Landroid/graphics/Paint;

    iget v3, p0, LX/JUN;->a:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2698153
    iget-object v2, p0, LX/JUN;->b:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2698154
    iget-object v2, p0, LX/JUN;->c:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 2698155
    iget-object v2, p0, LX/JUN;->c:Landroid/graphics/Path;

    div-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    int-to-float v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2698156
    iget-object v2, p0, LX/JUN;->c:Landroid/graphics/Path;

    div-int/lit8 v3, v1, 0x2

    add-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2698157
    iget-object v2, p0, LX/JUN;->c:Landroid/graphics/Path;

    div-int/lit8 v1, v1, 0x2

    sub-int v0, v1, v0

    int-to-float v0, v0

    invoke-virtual {v2, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2698158
    iget-object v0, p0, LX/JUN;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 2698159
    iget-object v0, p0, LX/JUN;->c:Landroid/graphics/Path;

    iget-object v1, p0, LX/JUN;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2698160
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2698149
    const/4 v0, -0x3

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 2698145
    iget-object v0, p0, LX/JUN;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2698146
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 2698147
    iget-object v0, p0, LX/JUN;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2698148
    return-void
.end method
