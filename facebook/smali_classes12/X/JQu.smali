.class public LX/JQu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/JQt;

.field private final b:LX/3mL;


# direct methods
.method public constructor <init>(LX/3mL;LX/JQt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692364
    iput-object p1, p0, LX/JQu;->b:LX/3mL;

    .line 2692365
    iput-object p2, p0, LX/JQu;->a:LX/JQt;

    .line 2692366
    return-void
.end method

.method public static a(LX/0QB;)LX/JQu;
    .locals 5

    .prologue
    .line 2692367
    const-class v1, LX/JQu;

    monitor-enter v1

    .line 2692368
    :try_start_0
    sget-object v0, LX/JQu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692369
    sput-object v2, LX/JQu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692370
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692371
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692372
    new-instance p0, LX/JQu;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/JQt;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JQt;

    invoke-direct {p0, v3, v4}, LX/JQu;-><init>(LX/3mL;LX/JQt;)V

    .line 2692373
    move-object v0, p0

    .line 2692374
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692375
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692376
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pm;LX/0Px;Ljava/lang/String;Z)LX/1Dg;
    .locals 8
    .param p2    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Pm;",
            "LX/0Px",
            "<",
            "LX/JQk;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2692378
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2692379
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v0

    const v2, 0x7f0b25a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 2692380
    iput v2, v0, LX/3mP;->b:I

    .line 2692381
    move-object v0, v0

    .line 2692382
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v3

    .line 2692383
    iget-object v0, p0, LX/JQu;->a:LX/JQt;

    const v2, 0x7f0b25a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, LX/JQt;->a(Landroid/content/Context;LX/0Px;LX/25M;LX/1Pm;ILjava/lang/String;Z)LX/JQs;

    move-result-object v0

    .line 2692384
    iget-object v1, p0, LX/JQu;->b:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
