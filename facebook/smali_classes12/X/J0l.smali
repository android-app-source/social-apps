.class public LX/J0l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;",
        "Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lB;


# direct methods
.method public constructor <init>(LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637786
    iput-object p1, p0, LX/J0l;->a:LX/0lB;

    .line 2637787
    return-void
.end method

.method public static a(LX/0QB;)LX/J0l;
    .locals 2

    .prologue
    .line 2637788
    new-instance v1, LX/J0l;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lB;

    invoke-direct {v1, v0}, LX/J0l;-><init>(LX/0lB;)V

    .line 2637789
    move-object v0, v1

    .line 2637790
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2637791
    check-cast p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;

    .line 2637792
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2637793
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "transfer_id"

    .line 2637794
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2637795
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637796
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2637797
    if-eqz v0, :cond_0

    .line 2637798
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "submitted_screen"

    .line 2637799
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2637800
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637801
    :cond_0
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->d:Lcom/facebook/payments/p2p/model/verification/UserInput;

    move-object v0, v0

    .line 2637802
    if-eqz v0, :cond_1

    .line 2637803
    iget-object v0, p0, LX/J0l;->a:LX/0lB;

    .line 2637804
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->d:Lcom/facebook/payments/p2p/model/verification/UserInput;

    move-object v2, v2

    .line 2637805
    invoke-virtual {v0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2637806
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "user_input"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637807
    :cond_1
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2637808
    if-eqz v0, :cond_2

    .line 2637809
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "csc"

    .line 2637810
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2637811
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637812
    :cond_2
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2637813
    if-eqz v0, :cond_3

    .line 2637814
    const-string v0, "/%d/p2p_verification_flows"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2637815
    iget-object v4, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2637816
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    .line 2637817
    :goto_0
    const-string v2, "p2p_verification"

    .line 2637818
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2637819
    move-object v0, v0

    .line 2637820
    const-string v2, "POST"

    .line 2637821
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2637822
    move-object v0, v0

    .line 2637823
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2637824
    move-object v0, v0

    .line 2637825
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637826
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637827
    move-object v0, v0

    .line 2637828
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2637829
    :cond_3
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "/%d/p2p_verification_flows"

    .line 2637830
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2637831
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2637832
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 2637833
    move-object v0, v0

    .line 2637834
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2637835
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637836
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2637837
    iget-object v1, p0, LX/J0l;->a:LX/0lB;

    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/J0l;->a:LX/0lB;

    .line 2637838
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 2637839
    const-class v3, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;

    .line 2637840
    return-object v0
.end method
