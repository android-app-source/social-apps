.class public final enum LX/I0p;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I0p;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I0p;

.field public static final enum FETCH_CALENDARABLE_ITEMS:LX/I0p;

.field public static final enum FETCH_PRIVATE_UNCONNECTED_UPCOMING_EVENT:LX/I0p;

.field public static final enum FETCH_SINGLE_EVENT:LX/I0p;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2527939
    new-instance v0, LX/I0p;

    const-string v1, "FETCH_PRIVATE_UNCONNECTED_UPCOMING_EVENT"

    invoke-direct {v0, v1, v2}, LX/I0p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0p;->FETCH_PRIVATE_UNCONNECTED_UPCOMING_EVENT:LX/I0p;

    .line 2527940
    new-instance v0, LX/I0p;

    const-string v1, "FETCH_CALENDARABLE_ITEMS"

    invoke-direct {v0, v1, v3}, LX/I0p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0p;->FETCH_CALENDARABLE_ITEMS:LX/I0p;

    .line 2527941
    new-instance v0, LX/I0p;

    const-string v1, "FETCH_SINGLE_EVENT"

    invoke-direct {v0, v1, v4}, LX/I0p;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0p;->FETCH_SINGLE_EVENT:LX/I0p;

    .line 2527942
    const/4 v0, 0x3

    new-array v0, v0, [LX/I0p;

    sget-object v1, LX/I0p;->FETCH_PRIVATE_UNCONNECTED_UPCOMING_EVENT:LX/I0p;

    aput-object v1, v0, v2

    sget-object v1, LX/I0p;->FETCH_CALENDARABLE_ITEMS:LX/I0p;

    aput-object v1, v0, v3

    sget-object v1, LX/I0p;->FETCH_SINGLE_EVENT:LX/I0p;

    aput-object v1, v0, v4

    sput-object v0, LX/I0p;->$VALUES:[LX/I0p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2527938
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I0p;
    .locals 1

    .prologue
    .line 2527943
    const-class v0, LX/I0p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I0p;

    return-object v0
.end method

.method public static values()[LX/I0p;
    .locals 1

    .prologue
    .line 2527937
    sget-object v0, LX/I0p;->$VALUES:[LX/I0p;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I0p;

    return-object v0
.end method
