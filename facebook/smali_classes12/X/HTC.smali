.class public final LX/HTC;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2469054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2469055
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2469056
    :goto_0
    return v1

    .line 2469057
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2469058
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_7

    .line 2469059
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2469060
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2469061
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2469062
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2469063
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2469064
    :cond_2
    const-string v6, "photos"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2469065
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2469066
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 2469067
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2469068
    invoke-static {p0, p1}, LX/HT7;->b(LX/15w;LX/186;)I

    move-result v5

    .line 2469069
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2469070
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2469071
    goto :goto_1

    .line 2469072
    :cond_4
    const-string v6, "root_share_story"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2469073
    invoke-static {p0, p1}, LX/HT8;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2469074
    :cond_5
    const-string v6, "videos"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2469075
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2469076
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_6

    .line 2469077
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2469078
    invoke-static {p0, p1}, LX/HTB;->b(LX/15w;LX/186;)I

    move-result v5

    .line 2469079
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2469080
    :cond_6
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2469081
    goto/16 :goto_1

    .line 2469082
    :cond_7
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2469083
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2469084
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2469085
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2469086
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2469087
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2469088
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2469089
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2469090
    if-eqz v0, :cond_0

    .line 2469091
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469092
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2469093
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2469094
    if-eqz v0, :cond_2

    .line 2469095
    const-string v1, "photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469096
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2469097
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2469098
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/HT7;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2469099
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2469100
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2469101
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2469102
    if-eqz v0, :cond_3

    .line 2469103
    const-string v1, "root_share_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469104
    invoke-static {p0, v0, p2}, LX/HT8;->a(LX/15i;ILX/0nX;)V

    .line 2469105
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2469106
    if-eqz v0, :cond_5

    .line 2469107
    const-string v1, "videos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469108
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2469109
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2469110
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/HTB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2469111
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2469112
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2469113
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2469114
    return-void
.end method
