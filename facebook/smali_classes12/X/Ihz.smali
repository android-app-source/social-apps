.class public LX/Ihz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 2603175
    const-string v0, "image/tiff"

    const-string v1, "image/x-photoshop"

    const-string v2, "image/x-coreldraw"

    const-string v3, "image/svg+xml"

    const-string v4, "image/vnd.djvu"

    const-string v5, "image/nef"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Ihz;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603177
    iput-object p1, p0, LX/Ihz;->a:Landroid/content/Context;

    .line 2603178
    return-void
.end method

.method public static b(LX/0QB;)LX/Ihz;
    .locals 2

    .prologue
    .line 2603179
    new-instance v1, LX/Ihz;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Ihz;-><init>(Landroid/content/Context;)V

    .line 2603180
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 2603181
    new-instance v0, LX/Ihy;

    invoke-direct {v0, p0}, LX/Ihy;-><init>(LX/Ihz;)V

    .line 2603182
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603183
    sget-object v1, LX/Ihz;->b:LX/0Rf;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2603184
    if-nez v1, :cond_0

    .line 2603185
    const/4 p1, 0x0

    .line 2603186
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603187
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/Ihz;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083a18

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/Ihz;->a:Landroid/content/Context;

    const v3, 0x7f083a19

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/Ihz;->a:Landroid/content/Context;

    const v6, 0x7f080011

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2603188
    const/4 v1, 0x0

    .line 2603189
    :goto_1
    move v0, v1

    .line 2603190
    return v0

    :cond_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
