.class public final LX/JUd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUe;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

.field public c:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/JUe;


# direct methods
.method public constructor <init>(LX/JUe;)V
    .locals 1

    .prologue
    .line 2699412
    iput-object p1, p0, LX/JUd;->d:LX/JUe;

    .line 2699413
    move-object v0, p1

    .line 2699414
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699415
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699411
    const-string v0, "PageContextualRecommendationsItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699394
    if-ne p0, p1, :cond_1

    .line 2699395
    :cond_0
    :goto_0
    return v0

    .line 2699396
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699397
    goto :goto_0

    .line 2699398
    :cond_3
    check-cast p1, LX/JUd;

    .line 2699399
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699400
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699401
    if-eq v2, v3, :cond_0

    .line 2699402
    iget-object v2, p0, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2699403
    goto :goto_0

    .line 2699404
    :cond_5
    iget-object v2, p1, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2699405
    :cond_6
    iget-object v2, p0, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    iget-object v3, p1, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2699406
    goto :goto_0

    .line 2699407
    :cond_8
    iget-object v2, p1, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    if-nez v2, :cond_7

    .line 2699408
    :cond_9
    iget-object v2, p0, LX/JUd;->c:LX/1Pr;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JUd;->c:LX/1Pr;

    iget-object v3, p1, LX/JUd;->c:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699409
    goto :goto_0

    .line 2699410
    :cond_a
    iget-object v2, p1, LX/JUd;->c:LX/1Pr;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
