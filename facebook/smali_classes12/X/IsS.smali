.class public final LX/IsS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;",
        ">;",
        "Lcom/facebook/messaging/service/model/SearchUserResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/SearchUserParams;

.field public final synthetic b:LX/3Mu;


# direct methods
.method public constructor <init>(LX/3Mu;Lcom/facebook/messaging/service/model/SearchUserParams;)V
    .locals 0

    .prologue
    .line 2619719
    iput-object p1, p0, LX/IsS;->b:LX/3Mu;

    iput-object p2, p0, LX/IsS;->a:Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2619720
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2619721
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2619722
    iget-object v1, p0, LX/IsS;->a:Lcom/facebook/messaging/service/model/SearchUserParams;

    .line 2619723
    invoke-static {v1, p1}, LX/3Mu;->a(Lcom/facebook/messaging/service/model/SearchUserParams;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/service/model/SearchUserResult;

    move-result-object p0

    move-object v0, p0

    .line 2619724
    return-object v0
.end method
