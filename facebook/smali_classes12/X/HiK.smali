.class public LX/HiK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497262
    iput-object p1, p0, LX/HiK;->a:LX/0Zb;

    .line 2497263
    return-void
.end method

.method public static a(LX/HiK;Ljava/lang/String;)LX/0oG;
    .locals 2

    .prologue
    .line 2497264
    iget-object v0, p0, LX/HiK;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/location/Location;Landroid/location/Address;)V
    .locals 4
    .param p6    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/location/Address;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2497265
    const-string v0, "address_typeahead_select"

    invoke-static {p0, v0}, LX/HiK;->a(LX/HiK;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    .line 2497266
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2497267
    :goto_0
    return-void

    .line 2497268
    :cond_0
    const-string v1, "input_string"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "selection_index"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v1

    const-string v2, "selection"

    invoke-virtual {v1, v2, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "product_tag"

    invoke-virtual {v1, v2, p5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "ta_provider"

    invoke-virtual {v1, v2, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2497269
    if-eqz p6, :cond_1

    .line 2497270
    const-string v1, "input_latitude"

    invoke-virtual {p6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2497271
    const-string v1, "input_longitude"

    invoke-virtual {p6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2497272
    :cond_1
    if-eqz p7, :cond_2

    .line 2497273
    const-string v1, "select_latitude"

    invoke-virtual {p7}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2497274
    const-string v1, "select_longitude"

    invoke-virtual {p7}, Landroid/location/Address;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2497275
    :cond_2
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
