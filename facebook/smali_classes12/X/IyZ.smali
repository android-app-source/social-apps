.class public LX/IyZ;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;",
        "Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:LX/Iyo;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/Iyo;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633606
    const-class v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    invoke-direct {p0, p2, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2633607
    iput-object p1, p0, LX/IyZ;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633608
    iput-object p3, p0, LX/IyZ;->d:LX/Iyo;

    .line 2633609
    return-void
.end method

.method private a(LX/1pN;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;
    .locals 2

    .prologue
    .line 2633605
    iget-object v0, p0, LX/IyZ;->d:LX/Iyo;

    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Iyo;->a(LX/0lF;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IyZ;
    .locals 4

    .prologue
    .line 2633603
    new-instance v3, LX/IyZ;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/Iyo;->b(LX/0QB;)LX/Iyo;

    move-result-object v2

    check-cast v2, LX/Iyo;

    invoke-direct {v3, v0, v1, v2}, LX/IyZ;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/Iyo;)V

    .line 2633604
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2633578
    check-cast p1, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;

    .line 2633579
    iget-object v0, p0, LX/IyZ;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633580
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2633581
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2633582
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2633583
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "buyer_id"

    iget-wide v4, p1, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633584
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "seller_id"

    iget-object v3, p0, LX/IyZ;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633585
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2633586
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633587
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "client"

    iget-object v3, p1, Lcom/facebook/payments/invoice/protocol/InvoiceConfigParams;->b:LX/6xh;

    invoke-virtual {v3}, LX/6xh;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633588
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-class v2, LX/IyZ;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 2633589
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2633590
    move-object v1, v1

    .line 2633591
    const-string v2, "GET"

    .line 2633592
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2633593
    move-object v1, v1

    .line 2633594
    const-string v2, "payments/invoice_configs"

    .line 2633595
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2633596
    move-object v1, v1

    .line 2633597
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2633598
    move-object v0, v1

    .line 2633599
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2633600
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2633601
    move-object v0, v0

    .line 2633602
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2633575
    invoke-direct {p0, p2}, LX/IyZ;->a(LX/1pN;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2633577
    invoke-direct {p0, p2}, LX/IyZ;->a(LX/1pN;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2633576
    const-string v0, "get_invoice_config"

    return-object v0
.end method
