.class public LX/Hjk;
.super LX/Hjj;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:LX/Hjo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LX/Hjj;-><init>()V

    return-void
.end method

.method private final s()Z
    .locals 1

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    const-wide/16 v5, 0x0

    if-nez p1, :cond_1

    iget-wide v1, v0, LX/Hjo;->L:J

    cmp-long v1, v1, v5

    if-lez v1, :cond_1

    iget-object v1, v0, LX/Hjo;->M:LX/Hkf;

    if-eqz v1, :cond_1

    iget-wide v1, v0, LX/Hjo;->L:J

    iget-object v3, v0, LX/Hjo;->M:LX/Hkf;

    iget-object v4, v0, LX/Hjo;->D:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, LX/Hkg;->a(JLX/Hkf;Ljava/lang/String;)LX/Hkg;

    move-result-object v1

    invoke-static {v1}, LX/Hkh;->a(LX/Hkg;)V

    iput-wide v5, v0, LX/Hjo;->L:J

    const/4 v1, 0x0

    iput-object v1, v0, LX/Hjo;->M:LX/Hkf;

    :cond_1
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;LX/Hjq;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/Hjq;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, LX/Hjo;->a(Lorg/json/JSONObject;)LX/Hjo;

    move-result-object v0

    iput-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iput-object p1, p0, LX/Hjk;->a:Landroid/content/Context;

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    invoke-static {p1, v0}, LX/Hkl;->a(Landroid/content/Context;LX/HjW;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p2, p0}, LX/Hjq;->b(LX/Hjj;)V

    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {p2, p0}, LX/Hjq;->a(LX/Hjj;)V

    :cond_2
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->D:Ljava/lang/String;

    move-object v0, p0

    sput-object v0, LX/Hkg;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    const/4 v6, 0x1

    iget-boolean v1, v0, LX/Hjo;->I:Z

    if-nez v1, :cond_6

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    if-eqz p1, :cond_1

    invoke-static {v1, p1}, LX/Hjo;->a(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {v1, p1}, LX/Hjo;->b(Ljava/util/Map;Ljava/util/Map;)V

    :cond_1
    new-instance v2, LX/Hkv;

    invoke-direct {v2, v1}, LX/Hkv;-><init>(Ljava/util/Map;)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v0, LX/Hjo;->k:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, LX/Hkv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-boolean v2, v0, LX/Hjo;->s:Z

    move v2, v2

    if-nez v2, :cond_2

    iget-boolean v2, v0, LX/Hjo;->t:Z

    move v2, v2

    if-eqz v2, :cond_5

    :cond_2
    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v3, "view"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "view"

    const-string v4, "view"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    const-string v3, "snapshot"

    invoke-interface {p1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "snapshot"

    const-string v4, "snapshot"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object v2, v2

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/facebook/ads/internal/adapters/n$1;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/ads/internal/adapters/n$1;-><init>(LX/Hjo;Ljava/util/Map;Ljava/util/Map;)V

    iget v1, v0, LX/Hjo;->u:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    const v5, 0x1b034b

    invoke-static {v3, v4, v1, v2, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    iget-object v1, v0, LX/Hjo;->E:LX/Hjn;

    const-string v2, "impression"

    invoke-virtual {v1, v2}, LX/Hjn;->a(Ljava/lang/String;)V

    iput-boolean v6, v0, LX/Hjo;->I:Z

    :cond_6
    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object v1, p0, LX/Hjk;->a:Landroid/content/Context;

    const/4 v6, 0x1

    iget-boolean v2, v0, LX/Hjo;->J:Z

    if-nez v2, :cond_2

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    if-eqz p1, :cond_1

    invoke-static {v2, p1}, LX/Hjo;->a(Ljava/util/Map;Ljava/util/Map;)V

    invoke-static {v2, p1}, LX/Hjo;->b(Ljava/util/Map;Ljava/util/Map;)V

    const-string v3, "touch"

    invoke-static {p1}, LX/Hko;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    new-instance v3, LX/Hkv;

    invoke-direct {v3, v2}, LX/Hkv;-><init>(Ljava/util/Map;)V

    new-array v2, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v0, LX/Hjo;->m:Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v3, v2}, LX/Hkv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v2, v0, LX/Hjo;->E:LX/Hjn;

    const-string v3, "click"

    invoke-virtual {v2, v3}, LX/Hjn;->a(Ljava/lang/String;)V

    iput-boolean v6, v0, LX/Hjo;->J:Z

    const-string v2, "Click logged"

    invoke-static {v1, v2}, LX/Hko;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    iget-object v2, v0, LX/Hjo;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/HjQ;->a(Landroid/content/Context;Landroid/net/Uri;)LX/HjP;

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v0, LX/Hjo;->L:J

    invoke-virtual {v2}, LX/HjP;->a()LX/Hkf;

    move-result-object v3

    iput-object v3, v0, LX/Hjo;->M:LX/Hkf;

    invoke-virtual {v2}, LX/HjP;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v3, LX/Hjo;->a:Ljava/lang/String;

    const-string v4, "Error executing action"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-boolean p0, v0, LX/Hjo;->s:Z

    move v0, p0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-boolean p0, v0, LX/Hjo;->t:Z

    move v0, p0

    return v0
.end method

.method public final e()I
    .locals 2

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget v1, v0, LX/Hjo;->v:I

    if-ltz v1, :cond_0

    iget v1, v0, LX/Hjo;->v:I

    const/16 p0, 0x64

    if-le v1, p0, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move v0, v1

    return v0

    :cond_1
    iget v1, v0, LX/Hjo;->v:I

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget p0, v0, LX/Hjo;->w:I

    move v0, p0

    return v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget p0, v0, LX/Hjo;->x:I

    move v0, p0

    return v0
.end method

.method public final h()LX/Hj9;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->h:LX/Hj9;

    move-object v0, p0

    goto :goto_0
.end method

.method public final i()LX/Hj9;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->i:LX/Hj9;

    move-object v0, p0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    invoke-static {v0}, LX/Hjo;->u(LX/Hjo;)V

    iget-object p0, v0, LX/Hjo;->c:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    invoke-static {v0}, LX/Hjo;->u(LX/Hjo;)V

    iget-object p0, v0, LX/Hjo;->d:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    invoke-static {v0}, LX/Hjo;->u(LX/Hjo;)V

    iget-object p0, v0, LX/Hjo;->e:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    invoke-static {v0}, LX/Hjo;->u(LX/Hjo;)V

    iget-object p0, v0, LX/Hjo;->g:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->H:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->y:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->z:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->A:Ljava/lang/String;

    move-object v0, p0

    goto :goto_0
.end method

.method public final r()LX/Hk3;
    .locals 1

    invoke-direct {p0}, LX/Hjk;->s()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Hjk;->b:LX/Hjo;

    iget-object p0, v0, LX/Hjo;->F:LX/Hk3;

    move-object v0, p0

    goto :goto_0
.end method
