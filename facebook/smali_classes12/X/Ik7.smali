.class public LX/Ik7;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2JT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/InT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IkC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IkI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IkM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

.field private l:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field private m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field private o:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private p:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private q:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private r:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2606433
    const-class v0, LX/Ik7;

    sput-object v0, LX/Ik7;->j:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2606412
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2606413
    invoke-direct {p0}, LX/Ik7;->a()V

    .line 2606414
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606430
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606431
    invoke-direct {p0}, LX/Ik7;->a()V

    .line 2606432
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606427
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606428
    invoke-direct {p0}, LX/Ik7;->a()V

    .line 2606429
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2606416
    const-class v0, LX/Ik7;

    invoke-static {v0, p0}, LX/Ik7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606417
    const v0, 0x7f030ded

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606418
    const v0, 0x7f0d1c3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    iput-object v0, p0, LX/Ik7;->k:Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    .line 2606419
    const v0, 0x7f0d1c3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ik7;->l:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2606420
    const v0, 0x7f0d2219

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik7;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2606421
    const v0, 0x7f0d2218

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ik7;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2606422
    const v0, 0x7f0d2217

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik7;->o:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606423
    const v0, 0x7f0d2125

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik7;->p:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606424
    const v0, 0x7f0d1c39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/Ik7;->q:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2606425
    const v0, 0x7f0d1c3a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    iput-object v0, p0, LX/Ik7;->r:Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    .line 2606426
    return-void
.end method

.method private static a(LX/Ik7;Landroid/content/Context;LX/2JT;LX/InT;LX/IkC;LX/IkI;LX/IkM;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2606415
    iput-object p1, p0, LX/Ik7;->a:Landroid/content/Context;

    iput-object p2, p0, LX/Ik7;->b:LX/2JT;

    iput-object p3, p0, LX/Ik7;->c:LX/InT;

    iput-object p4, p0, LX/Ik7;->d:LX/IkC;

    iput-object p5, p0, LX/Ik7;->e:LX/IkI;

    iput-object p6, p0, LX/Ik7;->f:LX/IkM;

    iput-object p7, p0, LX/Ik7;->g:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object p8, p0, LX/Ik7;->h:Ljava/util/concurrent/Executor;

    iput-object p9, p0, LX/Ik7;->i:LX/0Zb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Ik7;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, LX/Ik7;

    const-class v1, Landroid/content/Context;

    invoke-interface {v9, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v9}, LX/2JT;->a(LX/0QB;)LX/2JT;

    move-result-object v2

    check-cast v2, LX/2JT;

    invoke-static {v9}, LX/InT;->b(LX/0QB;)LX/InT;

    move-result-object v3

    check-cast v3, LX/InT;

    invoke-static {v9}, LX/IkC;->a(LX/0QB;)LX/IkC;

    move-result-object v4

    check-cast v4, LX/IkC;

    invoke-static {v9}, LX/IkI;->a(LX/0QB;)LX/IkI;

    move-result-object v5

    check-cast v5, LX/IkI;

    invoke-static {v9}, LX/IkM;->a(LX/0QB;)LX/IkM;

    move-result-object v6

    check-cast v6, LX/IkM;

    invoke-static {v9}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v7

    check-cast v7, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {v9}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v9}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static/range {v0 .. v9}, LX/Ik7;->a(LX/Ik7;Landroid/content/Context;LX/2JT;LX/InT;LX/IkC;LX/IkI;LX/IkM;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;LX/0Zb;)V

    return-void
.end method
