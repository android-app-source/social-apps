.class public final LX/IB1;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;)V
    .locals 0

    .prologue
    .line 2546450
    iput-object p1, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 2546451
    iget-object v0, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v0}, LX/IB5;->d()Ljava/util/Set;

    move-result-object v7

    .line 2546452
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2546453
    iget-object v0, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546454
    iget-object v1, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->c:LX/1nQ;

    .line 2546455
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2546456
    const-string v3, "EVENT_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    invoke-virtual {v3}, LX/IB5;->d()Ljava/util/Set;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->d:LX/IB5;

    .line 2546457
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 2546458
    iget-object v8, v4, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    array-length v9, v8

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v9, :cond_1

    aget-object p1, v8, v5

    .line 2546459
    if-eqz p1, :cond_0

    .line 2546460
    invoke-virtual {p1}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->e()LX/Blc;

    move-result-object v0

    invoke-virtual {v0}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v6, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546461
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2546462
    :cond_1
    move-object v4, v6

    .line 2546463
    const/4 v5, 0x0

    .line 2546464
    iget-object v6, v1, LX/1nQ;->i:LX/0Zb;

    const-string v8, "message_event_guests"

    const/4 v9, 0x0

    invoke-interface {v6, v8, v9}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2546465
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2546466
    const-string v8, "event_message_guests"

    invoke-virtual {v6, v8}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    iget-object v8, v1, LX/1nQ;->j:LX/0kv;

    iget-object v9, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v8, v9}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v8, "Event"

    invoke-virtual {v6, v8}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v8, "selected_ids"

    invoke-virtual {v6, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    move-result-object v6

    const-string v8, "send_as_group_message"

    invoke-virtual {v6, v8, v5}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2546467
    :cond_2
    iget-object v0, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->b:LX/B9y;

    iget-object v1, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546468
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2546469
    const-string v3, "EVENT_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546470
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v4

    .line 2546471
    const-string v4, "EVENT_NAME"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546472
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v5

    .line 2546473
    const-string v5, "EVENT_PHOTO_URL"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546474
    iget-object v6, v5, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v6

    .line 2546475
    const-string v6, "EVENT_LOCATION"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/IB1;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    .line 2546476
    iget-object v8, v6, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v6, v8

    .line 2546477
    const-string v8, "EVENT_TIME"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "event"

    const/16 v9, 0x64

    invoke-virtual/range {v0 .. v9}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;I)V

    .line 2546478
    :cond_3
    return-void
.end method
