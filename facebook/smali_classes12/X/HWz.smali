.class public final LX/HWz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HBQ;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/8A4;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 1

    .prologue
    .line 2477170
    iput-object p1, p0, LX/HWz;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2477171
    const/4 v0, 0x0

    iput-object v0, p0, LX/HWz;->b:LX/0Px;

    return-void
.end method


# virtual methods
.method public final a()LX/8A4;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2477172
    iget-object v0, p0, LX/HWz;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HWz;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477173
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2477174
    if-nez v0, :cond_1

    .line 2477175
    :cond_0
    const/4 v0, 0x0

    .line 2477176
    :goto_0
    return-object v0

    .line 2477177
    :cond_1
    iget-object v0, p0, LX/HWz;->b:LX/0Px;

    iget-object v1, p0, LX/HWz;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477178
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2477179
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 2477180
    iget-object v0, p0, LX/HWz;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477181
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2477182
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HWz;->b:LX/0Px;

    .line 2477183
    new-instance v0, LX/8A4;

    iget-object v1, p0, LX/HWz;->b:LX/0Px;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    iput-object v0, p0, LX/HWz;->c:LX/8A4;

    .line 2477184
    :cond_2
    iget-object v0, p0, LX/HWz;->c:LX/8A4;

    goto :goto_0
.end method
