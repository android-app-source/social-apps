.class public LX/I77;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/I77;


# instance fields
.field private final a:LX/IBS;


# direct methods
.method public constructor <init>(LX/IBS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2538582
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2538583
    iput-object p1, p0, LX/I77;->a:LX/IBS;

    .line 2538584
    return-void
.end method

.method public static a(LX/0QB;)LX/I77;
    .locals 4

    .prologue
    .line 2538557
    sget-object v0, LX/I77;->b:LX/I77;

    if-nez v0, :cond_1

    .line 2538558
    const-class v1, LX/I77;

    monitor-enter v1

    .line 2538559
    :try_start_0
    sget-object v0, LX/I77;->b:LX/I77;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2538560
    if-eqz v2, :cond_0

    .line 2538561
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2538562
    new-instance p0, LX/I77;

    invoke-static {v0}, LX/IBS;->b(LX/0QB;)LX/IBS;

    move-result-object v3

    check-cast v3, LX/IBS;

    invoke-direct {p0, v3}, LX/I77;-><init>(LX/IBS;)V

    .line 2538563
    move-object v0, p0

    .line 2538564
    sput-object v0, LX/I77;->b:LX/I77;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2538565
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2538566
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2538567
    :cond_1
    sget-object v0, LX/I77;->b:LX/I77;

    return-object v0

    .line 2538568
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2538569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2538570
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2538571
    if-nez v1, :cond_1

    .line 2538572
    :cond_0
    :goto_0
    return-object v0

    .line 2538573
    :cond_1
    const-string v2, "event_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2538574
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2538575
    iget-object v0, p0, LX/I77;->a:LX/IBS;

    invoke-virtual {v0, p1, v1}, LX/IBS;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2538576
    new-instance v0, LX/98g;

    invoke-direct {v0, v1, v2}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2538577
    check-cast p1, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2538578
    if-eqz p1, :cond_0

    .line 2538579
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2538580
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2538581
    const/4 v0, 0x1

    return v0
.end method
