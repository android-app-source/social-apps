.class public LX/JNl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JNm;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JNm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686450
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686451
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNl;->b:LX/0Zi;

    .line 2686452
    iput-object p1, p0, LX/JNl;->a:LX/0Ot;

    .line 2686453
    return-void
.end method

.method public static a(LX/0QB;)LX/JNl;
    .locals 4

    .prologue
    .line 2686439
    const-class v1, LX/JNl;

    monitor-enter v1

    .line 2686440
    :try_start_0
    sget-object v0, LX/JNl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686441
    sput-object v2, LX/JNl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686442
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686443
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686444
    new-instance v3, LX/JNl;

    const/16 p0, 0x1f3f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNl;-><init>(LX/0Ot;)V

    .line 2686445
    move-object v0, v3

    .line 2686446
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686447
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686448
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2686404
    check-cast p2, LX/JNk;

    .line 2686405
    iget-object v0, p0, LX/JNl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNm;

    iget-object v1, p2, LX/JNk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JNk;->b:LX/1Pb;

    .line 2686406
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v3, v0, LX/JNm;->a:LX/JO3;

    const/4 v5, 0x0

    .line 2686407
    new-instance p0, LX/JO2;

    invoke-direct {p0, v3}, LX/JO2;-><init>(LX/JO3;)V

    .line 2686408
    iget-object p2, v3, LX/JO3;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JO1;

    .line 2686409
    if-nez p2, :cond_0

    .line 2686410
    new-instance p2, LX/JO1;

    invoke-direct {p2, v3}, LX/JO1;-><init>(LX/JO3;)V

    .line 2686411
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/JO1;->a$redex0(LX/JO1;LX/1De;IILX/JO2;)V

    .line 2686412
    move-object p0, p2

    .line 2686413
    move-object v5, p0

    .line 2686414
    move-object v3, v5

    .line 2686415
    iget-object v5, v3, LX/JO1;->a:LX/JO2;

    iput-object v1, v5, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686416
    iget-object v5, v3, LX/JO1;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2686417
    move-object v5, v3

    .line 2686418
    move-object v3, v2

    check-cast v3, LX/1Pq;

    .line 2686419
    iget-object p0, v5, LX/JO1;->a:LX/JO2;

    iput-object v3, p0, LX/JO2;->b:LX/1Pq;

    .line 2686420
    iget-object p0, v5, LX/JO1;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2686421
    move-object v3, v5

    .line 2686422
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JNm;->b:LX/JNy;

    const/4 v5, 0x0

    .line 2686423
    new-instance p0, LX/JNx;

    invoke-direct {p0, v4}, LX/JNx;-><init>(LX/JNy;)V

    .line 2686424
    iget-object p2, v4, LX/JNy;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNw;

    .line 2686425
    if-nez p2, :cond_1

    .line 2686426
    new-instance p2, LX/JNw;

    invoke-direct {p2, v4}, LX/JNw;-><init>(LX/JNy;)V

    .line 2686427
    :cond_1
    invoke-static {p2, p1, v5, v5, p0}, LX/JNw;->a$redex0(LX/JNw;LX/1De;IILX/JNx;)V

    .line 2686428
    move-object p0, p2

    .line 2686429
    move-object v5, p0

    .line 2686430
    move-object v4, v5

    .line 2686431
    iget-object v5, v4, LX/JNw;->a:LX/JNx;

    iput-object v1, v5, LX/JNx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686432
    iget-object v5, v4, LX/JNw;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2686433
    move-object v4, v4

    .line 2686434
    iget-object v5, v4, LX/JNw;->a:LX/JNx;

    iput-object v2, v5, LX/JNx;->a:LX/1Pb;

    .line 2686435
    iget-object v5, v4, LX/JNw;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2686436
    move-object v4, v4

    .line 2686437
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2686438
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2686402
    invoke-static {}, LX/1dS;->b()V

    .line 2686403
    const/4 v0, 0x0

    return-object v0
.end method
