.class public LX/JQR;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mX",
        "<",
        "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JQi;

.field private d:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

.field private e:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;LX/1Pn;LX/25M;LX/JQi;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;",
            "TE;",
            "LX/25M;",
            "LX/JQi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691787
    move-object v0, p4

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2691788
    iput-object p3, p0, LX/JQR;->d:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 2691789
    iput-object p4, p0, LX/JQR;->e:LX/1Pn;

    .line 2691790
    iput-object p6, p0, LX/JQR;->c:LX/JQi;

    .line 2691791
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2691792
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 3

    .prologue
    .line 2691793
    check-cast p2, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    .line 2691794
    new-instance v0, LX/JQj;

    iget-object v1, p0, LX/JQR;->d:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    const-string v2, "job_carousel"

    invoke-direct {v0, v1, p2, v2}, LX/JQj;-><init>(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;Ljava/lang/String;)V

    .line 2691795
    iget-object v1, p0, LX/JQR;->c:LX/JQi;

    invoke-virtual {v1, p1}, LX/JQi;->c(LX/1De;)LX/JQg;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/JQg;->a(LX/JQj;)LX/JQg;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2691796
    const/4 v0, 0x0

    return v0
.end method
