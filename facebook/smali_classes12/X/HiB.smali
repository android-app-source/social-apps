.class public final LX/HiB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/HiC;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I


# direct methods
.method public constructor <init>(LX/HiC;II)V
    .locals 0

    .prologue
    .line 2496914
    iput-object p1, p0, LX/HiB;->a:LX/HiC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2496915
    iput p2, p0, LX/HiB;->b:I

    .line 2496916
    iput p3, p0, LX/HiB;->c:I

    .line 2496917
    invoke-direct {p0}, LX/HiB;->f()V

    .line 2496918
    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 2496919
    const/16 v0, 0xff

    iput v0, p0, LX/HiB;->h:I

    iput v0, p0, LX/HiB;->f:I

    iput v0, p0, LX/HiB;->d:I

    .line 2496920
    const/4 v0, 0x0

    iput v0, p0, LX/HiB;->i:I

    iput v0, p0, LX/HiB;->g:I

    iput v0, p0, LX/HiB;->e:I

    .line 2496921
    iget v0, p0, LX/HiB;->b:I

    :goto_0
    iget v1, p0, LX/HiB;->c:I

    if-gt v0, v1, :cond_6

    .line 2496922
    iget-object v1, p0, LX/HiB;->a:LX/HiC;

    iget-object v1, v1, LX/HiC;->c:[I

    aget v1, v1, v0

    .line 2496923
    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 2496924
    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v3

    .line 2496925
    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    .line 2496926
    iget v4, p0, LX/HiB;->e:I

    if-le v2, v4, :cond_0

    .line 2496927
    iput v2, p0, LX/HiB;->e:I

    .line 2496928
    :cond_0
    iget v4, p0, LX/HiB;->d:I

    if-ge v2, v4, :cond_1

    .line 2496929
    iput v2, p0, LX/HiB;->d:I

    .line 2496930
    :cond_1
    iget v2, p0, LX/HiB;->g:I

    if-le v3, v2, :cond_2

    .line 2496931
    iput v3, p0, LX/HiB;->g:I

    .line 2496932
    :cond_2
    iget v2, p0, LX/HiB;->f:I

    if-ge v3, v2, :cond_3

    .line 2496933
    iput v3, p0, LX/HiB;->f:I

    .line 2496934
    :cond_3
    iget v2, p0, LX/HiB;->i:I

    if-le v1, v2, :cond_4

    .line 2496935
    iput v1, p0, LX/HiB;->i:I

    .line 2496936
    :cond_4
    iget v2, p0, LX/HiB;->h:I

    if-ge v1, v2, :cond_5

    .line 2496937
    iput v1, p0, LX/HiB;->h:I

    .line 2496938
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2496939
    :cond_6
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2496940
    iget v0, p0, LX/HiB;->e:I

    iget v1, p0, LX/HiB;->d:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/HiB;->g:I

    iget v2, p0, LX/HiB;->f:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, LX/HiB;->i:I

    iget v2, p0, LX/HiB;->h:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2496941
    iget v1, p0, LX/HiB;->c:I

    iget v2, p0, LX/HiB;->b:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    move v1, v1

    .line 2496942
    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/HiB;
    .locals 5

    .prologue
    .line 2496943
    invoke-virtual {p0}, LX/HiB;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2496944
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not split a box with only 1 color"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2496945
    :cond_0
    iget v0, p0, LX/HiB;->e:I

    iget v1, p0, LX/HiB;->d:I

    sub-int/2addr v0, v1

    .line 2496946
    iget v1, p0, LX/HiB;->g:I

    iget v2, p0, LX/HiB;->f:I

    sub-int/2addr v1, v2

    .line 2496947
    iget v2, p0, LX/HiB;->i:I

    iget v3, p0, LX/HiB;->h:I

    sub-int/2addr v2, v3

    .line 2496948
    if-lt v0, v1, :cond_3

    if-lt v0, v2, :cond_3

    .line 2496949
    const/4 v0, -0x3

    .line 2496950
    :goto_0
    move v1, v0

    .line 2496951
    iget-object v0, p0, LX/HiB;->a:LX/HiC;

    iget v2, p0, LX/HiB;->b:I

    iget v3, p0, LX/HiB;->c:I

    invoke-static {v0, v1, v2, v3}, LX/HiC;->a$redex0(LX/HiC;III)V

    .line 2496952
    iget-object v0, p0, LX/HiB;->a:LX/HiC;

    iget-object v0, v0, LX/HiC;->c:[I

    iget v2, p0, LX/HiB;->b:I

    iget v3, p0, LX/HiB;->c:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->sort([III)V

    .line 2496953
    iget-object v0, p0, LX/HiB;->a:LX/HiC;

    iget v2, p0, LX/HiB;->b:I

    iget v3, p0, LX/HiB;->c:I

    invoke-static {v0, v1, v2, v3}, LX/HiC;->a$redex0(LX/HiC;III)V

    .line 2496954
    packed-switch v1, :pswitch_data_0

    .line 2496955
    iget v0, p0, LX/HiB;->d:I

    iget v2, p0, LX/HiB;->e:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 2496956
    :goto_1
    move v2, v0

    .line 2496957
    iget v0, p0, LX/HiB;->b:I

    :goto_2
    iget v3, p0, LX/HiB;->c:I

    if-gt v0, v3, :cond_2

    .line 2496958
    iget-object v3, p0, LX/HiB;->a:LX/HiC;

    iget-object v3, v3, LX/HiC;->c:[I

    aget v3, v3, v0

    .line 2496959
    packed-switch v1, :pswitch_data_1

    .line 2496960
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2496961
    :pswitch_0
    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    if-lt v3, v2, :cond_1

    .line 2496962
    :goto_3
    move v0, v0

    .line 2496963
    new-instance v1, LX/HiB;

    iget-object v2, p0, LX/HiB;->a:LX/HiC;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, LX/HiB;->c:I

    invoke-direct {v1, v2, v3, v4}, LX/HiB;-><init>(LX/HiC;II)V

    .line 2496964
    iput v0, p0, LX/HiB;->c:I

    .line 2496965
    invoke-direct {p0}, LX/HiB;->f()V

    .line 2496966
    return-object v1

    .line 2496967
    :pswitch_1
    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    if-lt v3, v2, :cond_1

    goto :goto_3

    .line 2496968
    :pswitch_2
    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    if-le v3, v2, :cond_1

    goto :goto_3

    .line 2496969
    :cond_2
    iget v0, p0, LX/HiB;->b:I

    goto :goto_3

    .line 2496970
    :cond_3
    if-lt v1, v0, :cond_4

    if-lt v1, v2, :cond_4

    .line 2496971
    const/4 v0, -0x2

    goto :goto_0

    .line 2496972
    :cond_4
    const/4 v0, -0x1

    goto :goto_0

    .line 2496973
    :pswitch_3
    iget v0, p0, LX/HiB;->f:I

    iget v2, p0, LX/HiB;->g:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 2496974
    :pswitch_4
    iget v0, p0, LX/HiB;->h:I

    iget v2, p0, LX/HiB;->i:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
