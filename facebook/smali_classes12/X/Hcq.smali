.class public LX/Hcq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/components/TopicSocialContextComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2487553
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hcq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/components/TopicSocialContextComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487533
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2487534
    iput-object p1, p0, LX/Hcq;->b:LX/0Ot;

    .line 2487535
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2487538
    check-cast p2, LX/Hcp;

    .line 2487539
    iget-object v0, p0, LX/Hcq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;

    iget-object v1, p2, LX/Hcp;->a:Ljava/util/List;

    const/4 p2, 0x1

    .line 2487540
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const-string p0, "Followers"

    invoke-virtual {v3, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0050

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 p0, 0x2c

    invoke-interface {v3, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->b:LX/GgK;

    invoke-virtual {v3, p1}, LX/GgK;->c(LX/1De;)LX/GgJ;

    move-result-object v3

    const p0, 0x7f020882

    invoke-virtual {v3, p0}, LX/GgJ;->h(I)LX/GgJ;

    move-result-object v3

    .line 2487541
    const-string p0, "1,636 people follow this topic"

    move-object p0, p0

    .line 2487542
    invoke-virtual {v3, p0}, LX/GgJ;->b(Ljava/lang/String;)LX/GgJ;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->b:LX/GgK;

    invoke-virtual {v3, p1}, LX/GgK;->c(LX/1De;)LX/GgJ;

    move-result-object v3

    const p0, 0x7f020892

    invoke-virtual {v3, p0}, LX/GgJ;->h(I)LX/GgJ;

    move-result-object v3

    .line 2487543
    const-string p0, "Jeremy Friedland, Hanoch Yeung, Martin Gartz and 15 other friends follow this topic."

    move-object p0, p0

    .line 2487544
    invoke-virtual {v3, p0}, LX/GgJ;->b(Ljava/lang/String;)LX/GgJ;

    move-result-object v3

    .line 2487545
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p0

    invoke-direct {p2, p0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2487546
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2487547
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2487548
    :cond_0
    move-object p0, p2

    .line 2487549
    iget-object p2, v3, LX/GgJ;->a:Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;

    iput-object p0, p2, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    .line 2487550
    move-object v3, v3

    .line 2487551
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2487552
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2487536
    invoke-static {}, LX/1dS;->b()V

    .line 2487537
    const/4 v0, 0x0

    return-object v0
.end method
