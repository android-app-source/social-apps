.class public final LX/ISb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 0

    .prologue
    .line 2578073
    iput-object p1, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    .line 2578074
    iget-object v0, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    iget-object v0, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2578075
    const-string v5, "groups_seeds_composer_create_event"

    invoke-static {v5}, LX/ISn;->a(Ljava/lang/String;)V

    .line 2578076
    const-string v5, "group_inline_composer"

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v5, LX/ISn;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2578077
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v7

    .line 2578078
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    sget-object v8, Lcom/facebook/events/common/ActionMechanism;->GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    move-object v5, v4

    move-object v9, v1

    move-object v10, v2

    move-object v11, v3

    move-object v12, v0

    invoke-static/range {v5 .. v12}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2578079
    sget-object v6, LX/ISn;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v6, v5, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578080
    const/4 v0, 0x1

    return v0

    .line 2578081
    :cond_0
    iget-object v0, p0, LX/ISb;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->v:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
