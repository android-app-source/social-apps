.class public LX/It1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/DgE;

.field private final c:LX/FDz;

.field private final d:LX/It0;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Uh;

.field private final j:LX/2Mv;

.field private final k:LX/2Uq;

.field public final l:LX/1rd;

.field public final m:LX/FN6;


# direct methods
.method public constructor <init>(LX/0SG;LX/DgE;LX/FDz;LX/It0;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/2Mv;LX/2Uq;LX/1rd;LX/FN6;)V
    .locals 0
    .param p1    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/DgE;",
            "LX/FDz;",
            "LX/It0;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2Mv;",
            "LX/2Uq;",
            "LX/1rd;",
            "LX/FN6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2621843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621844
    iput-object p1, p0, LX/It1;->a:LX/0SG;

    .line 2621845
    iput-object p2, p0, LX/It1;->b:LX/DgE;

    .line 2621846
    iput-object p3, p0, LX/It1;->c:LX/FDz;

    .line 2621847
    iput-object p4, p0, LX/It1;->d:LX/It0;

    .line 2621848
    iput-object p5, p0, LX/It1;->e:LX/0Or;

    .line 2621849
    iput-object p6, p0, LX/It1;->f:LX/0Or;

    .line 2621850
    iput-object p7, p0, LX/It1;->g:LX/0Or;

    .line 2621851
    iput-object p8, p0, LX/It1;->h:LX/0Or;

    .line 2621852
    iput-object p9, p0, LX/It1;->i:LX/0Uh;

    .line 2621853
    iput-object p10, p0, LX/It1;->j:LX/2Mv;

    .line 2621854
    iput-object p11, p0, LX/It1;->k:LX/2Uq;

    .line 2621855
    iput-object p12, p0, LX/It1;->l:LX/1rd;

    .line 2621856
    iput-object p13, p0, LX/It1;->m:LX/FN6;

    .line 2621857
    return-void
.end method

.method public static a(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;
    .locals 2

    .prologue
    .line 2621842
    iget-object v0, p0, LX/It1;->d:LX/It0;

    invoke-virtual {v0}, LX/It0;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/It1;->c(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/It1;
    .locals 3

    .prologue
    .line 2621834
    const-class v1, LX/It1;

    monitor-enter v1

    .line 2621835
    :try_start_0
    sget-object v0, LX/It1;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2621836
    sput-object v2, LX/It1;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2621837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2621838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/It1;->b(LX/0QB;)LX/It1;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2621839
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/It1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2621840
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2621841
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/It1;
    .locals 14

    .prologue
    .line 2621825
    new-instance v0, LX/It1;

    invoke-static {p0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v1

    check-cast v1, LX/0SG;

    .line 2621826
    new-instance v3, LX/DgE;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v2}, LX/DgE;-><init>(Landroid/content/res/Resources;)V

    .line 2621827
    move-object v2, v3

    .line 2621828
    check-cast v2, LX/DgE;

    .line 2621829
    new-instance v3, LX/FDz;

    invoke-direct {v3}, LX/FDz;-><init>()V

    .line 2621830
    move-object v3, v3

    .line 2621831
    move-object v3, v3

    .line 2621832
    check-cast v3, LX/FDz;

    invoke-static {p0}, LX/It0;->a(LX/0QB;)LX/It0;

    move-result-object v4

    check-cast v4, LX/It0;

    const/16 v5, 0xce8

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x12cb

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x19e

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x38d9

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v10

    check-cast v10, LX/2Mv;

    invoke-static {p0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v11

    check-cast v11, LX/2Uq;

    invoke-static {p0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v12

    check-cast v12, LX/1rd;

    invoke-static {p0}, LX/FN6;->b(LX/0QB;)LX/FN6;

    move-result-object v13

    check-cast v13, LX/FN6;

    invoke-direct/range {v0 .. v13}, LX/It1;-><init>(LX/0SG;LX/DgE;LX/FDz;LX/It0;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Uh;LX/2Mv;LX/2Uq;LX/1rd;LX/FN6;)V

    .line 2621833
    return-object v0
.end method

.method public static c(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;
    .locals 11

    .prologue
    .line 2621756
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sent."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2621757
    iget-object v1, p0, LX/It1;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2621758
    invoke-static {v2, v3}, LX/6fa;->a(J)J

    move-result-wide v4

    .line 2621759
    iget-object v1, p0, LX/It1;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2621760
    const-string v6, "Can\'t create a sent message without a viewer"

    invoke-static {v1, v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621761
    const-string v6, ""

    .line 2621762
    iget-boolean v7, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v7, v7

    .line 2621763
    if-nez v7, :cond_0

    .line 2621764
    iget-object v6, p0, LX/It1;->f:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    .line 2621765
    invoke-virtual {v6}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v6

    .line 2621766
    :cond_0
    new-instance v7, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v8, Lcom/facebook/user/model/UserKey;

    sget-object v9, LX/0XG;->FACEBOOK:LX/0XG;

    .line 2621767
    iget-object v10, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2621768
    invoke-direct {v8, v9, v10}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2621769
    iget-object v10, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v10

    .line 2621770
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, "@facebook.com"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v7, v8, v6, v1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v7

    .line 2621771
    iget-object v6, p0, LX/It1;->e:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2Og;

    invoke-virtual {v6, p1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v6

    .line 2621772
    if-eqz v6, :cond_3

    iget v6, v6, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :goto_0
    move-object v6, v6

    .line 2621773
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v7

    sget-object v8, LX/2uW;->PENDING_SEND:LX/2uW;

    .line 2621774
    iput-object v8, v7, LX/6f7;->l:LX/2uW;

    .line 2621775
    move-object v7, v7

    .line 2621776
    invoke-virtual {v7, v0}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v0

    .line 2621777
    iput-object p1, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2621778
    move-object v0, v0

    .line 2621779
    iput-object p2, v0, LX/6f7;->n:Ljava/lang/String;

    .line 2621780
    move-object v0, v0

    .line 2621781
    iput-wide v2, v0, LX/6f7;->c:J

    .line 2621782
    move-object v0, v0

    .line 2621783
    iput-wide v2, v0, LX/6f7;->d:J

    .line 2621784
    move-object v0, v0

    .line 2621785
    iput-wide v4, v0, LX/6f7;->g:J

    .line 2621786
    move-object v0, v0

    .line 2621787
    iput-object v1, v0, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2621788
    move-object v0, v0

    .line 2621789
    const/4 v1, 0x1

    .line 2621790
    iput-boolean v1, v0, LX/6f7;->o:Z

    .line 2621791
    move-object v0, v0

    .line 2621792
    sget-object v1, LX/6f2;->SEND:LX/6f2;

    .line 2621793
    iput-object v1, v0, LX/6f7;->q:LX/6f2;

    .line 2621794
    move-object v0, v0

    .line 2621795
    const-string v1, "mobile"

    .line 2621796
    iput-object v1, v0, LX/6f7;->p:Ljava/lang/String;

    .line 2621797
    move-object v0, v0

    .line 2621798
    sget-object v1, Lcom/facebook/messaging/model/messages/Publicity;->b:Lcom/facebook/messaging/model/messages/Publicity;

    .line 2621799
    iput-object v1, v0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 2621800
    move-object v0, v0

    .line 2621801
    iput-object v6, v0, LX/6f7;->J:Ljava/lang/Integer;

    .line 2621802
    move-object v0, v0

    .line 2621803
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/It1;->m:LX/FN6;

    invoke-virtual {v1}, LX/FN6;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2621804
    iget-object v1, p0, LX/It1;->l:LX/1rd;

    iget-object v2, p0, LX/It1;->m:LX/FN6;

    .line 2621805
    if-nez p1, :cond_4

    .line 2621806
    sget-object v3, Lcom/facebook/messaging/model/threads/DualSimSetting;->a:Lcom/facebook/messaging/model/threads/DualSimSetting;

    .line 2621807
    :goto_1
    move-object v2, v3

    .line 2621808
    iget v2, v2, Lcom/facebook/messaging/model/threads/DualSimSetting;->b:I

    iget-object v3, p0, LX/It1;->m:LX/FN6;

    invoke-virtual {v3}, LX/FN6;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/1rd;->a(II)I

    move-result v1

    .line 2621809
    iput v1, v0, LX/6f7;->U:I

    .line 2621810
    :cond_1
    iget-object v1, p0, LX/It1;->i:LX/0Uh;

    const/16 v2, 0x52f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2621811
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2621812
    iget-object v2, p0, LX/It1;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    .line 2621813
    goto :goto_2

    .line 2621814
    :goto_2
    move-object v1, v1

    .line 2621815
    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/util/Map;)LX/6f7;

    .line 2621816
    :cond_2
    return-object v0

    :cond_3
    const/4 v6, 0x0

    goto :goto_0

    .line 2621817
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2621818
    sget-object v4, LX/0db;->aQ:LX/0Tn;

    invoke-virtual {v4, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    const-string v4, "/sub_id"

    invoke-virtual {v3, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v3

    check-cast v3, LX/0Tn;

    move-object v3, v3

    .line 2621819
    iget-object v4, v2, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2621820
    iget-object v4, v2, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v5, -0x1

    invoke-interface {v4, v3, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 2621821
    new-instance v4, Lcom/facebook/messaging/model/threads/DualSimSetting;

    invoke-direct {v4, v3}, Lcom/facebook/messaging/model/threads/DualSimSetting;-><init>(I)V

    move-object v3, v4

    .line 2621822
    goto :goto_1

    .line 2621823
    :cond_5
    sget-object v3, Lcom/facebook/messaging/model/threads/DualSimSetting;->a:Lcom/facebook/messaging/model/threads/DualSimSetting;

    goto :goto_1
    .line 2621824
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/FFq;)Lcom/facebook/messaging/model/messages/Message;
    .locals 5

    .prologue
    .line 2621728
    invoke-virtual {p2}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2621729
    const-string v1, ""

    .line 2621730
    const-string v2, ""

    .line 2621731
    new-instance v3, LX/5Xq;

    invoke-direct {v3}, LX/5Xq;-><init>()V

    .line 2621732
    const-string v4, "INITIATED"

    .line 2621733
    iput-object v4, v3, LX/5Xq;->aR:Ljava/lang/String;

    .line 2621734
    iput-object v0, v3, LX/5Xq;->aS:Ljava/lang/String;

    .line 2621735
    invoke-virtual {v3}, LX/5Xq;->a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v3

    .line 2621736
    new-instance v4, LX/5Zg;

    invoke-direct {v4}, LX/5Zg;-><init>()V

    .line 2621737
    iput-object v3, v4, LX/5Zg;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    .line 2621738
    move-object v4, v4

    .line 2621739
    iput-object v1, v4, LX/5Zg;->l:Ljava/lang/String;

    .line 2621740
    move-object v4, v4

    .line 2621741
    iput-object v2, v4, LX/5Zg;->j:Ljava/lang/String;

    .line 2621742
    move-object v4, v4

    .line 2621743
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->LIGHTWEIGHT_ACTION:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {p2, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    .line 2621744
    iput-object p2, v4, LX/5Zg;->h:LX/0Px;

    .line 2621745
    move-object v4, v4

    .line 2621746
    invoke-virtual {v4}, LX/5Zg;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v4

    move-object v1, v4

    .line 2621747
    new-instance v2, LX/5Zl;

    invoke-direct {v2}, LX/5Zl;-><init>()V

    .line 2621748
    iput-object v1, v2, LX/5Zl;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 2621749
    move-object v2, v2

    .line 2621750
    invoke-virtual {v2}, LX/5Zl;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v2

    move-object v1, v2

    .line 2621751
    move-object v0, v1

    .line 2621752
    invoke-static {p0, p1}, LX/It1;->a(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    move-result-object v1

    .line 2621753
    iput-object v0, v1, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 2621754
    move-object v0, v1

    .line 2621755
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 2621722
    iget-object v0, p0, LX/It1;->d:LX/It0;

    invoke-virtual {v0}, LX/It0;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 2621723
    invoke-static {p0, p1, v0}, LX/It1;->c(LX/It1;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)LX/6f7;

    move-result-object v1

    .line 2621724
    iput-object p2, v1, LX/6f7;->f:Ljava/lang/String;

    .line 2621725
    move-object v1, v1

    .line 2621726
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    move-object v0, v1

    .line 2621727
    return-object v0
.end method
