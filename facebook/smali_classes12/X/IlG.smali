.class public LX/IlG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:LX/InN;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607615
    new-instance v0, LX/InN;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/InN;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IlG;->a:LX/InN;

    .line 2607616
    iget-object v0, p0, LX/IlG;->a:LX/InN;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, LX/InN;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2607617
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1eaa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2607618
    iget-object v1, p0, LX/IlG;->a:LX/InN;

    const v2, 0x7f02140f

    invoke-virtual {v1, v2}, LX/InN;->setBackgroundResource(I)V

    .line 2607619
    iget-object v1, p0, LX/IlG;->a:LX/InN;

    invoke-virtual {v1, v0, v0, v0, v0}, LX/InN;->setPadding(IIII)V

    .line 2607620
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607621
    iget-object v0, p0, LX/IlG;->a:LX/InN;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 2

    .prologue
    .line 2607622
    iget-object v0, p1, LX/Il0;->c:LX/0am;

    move-object v0, v0

    .line 2607623
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2607624
    iget-object v1, p0, LX/IlG;->a:LX/InN;

    .line 2607625
    iget-object v0, p1, LX/Il0;->c:LX/0am;

    move-object v0, v0

    .line 2607626
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/InQ;

    invoke-virtual {v1, v0}, LX/InN;->setViewParams(LX/InQ;)V

    .line 2607627
    return-void
.end method
