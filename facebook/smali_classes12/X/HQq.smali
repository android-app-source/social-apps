.class public LX/HQq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1Ck;

.field public final c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/HQo;

.field private final g:Landroid/os/ParcelUuid;

.field public final h:LX/CXj;

.field public final i:LX/CXo;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;Landroid/os/ParcelUuid;LX/CXj;LX/0Ot;LX/1Ck;LX/0Ot;LX/HQo;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/os/ParcelUuid;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesDataListener;",
            "Landroid/os/ParcelUuid;",
            "LX/CXj;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/HQo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2464039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2464040
    iput-object p1, p0, LX/HQq;->d:Ljava/lang/String;

    .line 2464041
    iput-object p2, p0, LX/HQq;->c:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    .line 2464042
    iput-object p3, p0, LX/HQq;->g:Landroid/os/ParcelUuid;

    .line 2464043
    iput-object p4, p0, LX/HQq;->h:LX/CXj;

    .line 2464044
    iput-object p5, p0, LX/HQq;->a:LX/0Ot;

    .line 2464045
    iput-object p6, p0, LX/HQq;->b:LX/1Ck;

    .line 2464046
    iput-object p7, p0, LX/HQq;->e:LX/0Ot;

    .line 2464047
    iput-object p8, p0, LX/HQq;->f:LX/HQo;

    .line 2464048
    new-instance v0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;

    iget-object v1, p0, LX/HQq;->g:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;-><init>(LX/HQq;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, LX/HQq;->i:LX/CXo;

    .line 2464049
    return-void
.end method
