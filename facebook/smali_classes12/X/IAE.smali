.class public LX/IAE;
.super LX/IA6;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/IA5;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545176
    invoke-direct {p0, p1, p2}, LX/IA6;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2545177
    return-void
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/IA6;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545146
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    .line 2545147
    instance-of v2, v0, LX/IAE;

    if-eqz v2, :cond_0

    .line 2545148
    check-cast v0, LX/IAE;

    invoke-direct {v0, p0}, LX/IAE;->c(Ljava/util/List;)V

    goto :goto_0

    .line 2545149
    :cond_1
    return-void
.end method

.method private c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545150
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2545151
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545152
    iget-object p1, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, p1

    .line 2545153
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2545154
    :cond_0
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2545155
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545156
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2545157
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2545158
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 2545159
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/EventUser;)V
    .locals 1

    .prologue
    .line 2545160
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545161
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 3

    .prologue
    .line 2545162
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2545163
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545164
    iget-object v2, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, v2

    .line 2545165
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545166
    new-instance v2, LX/7vI;

    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    invoke-direct {v2, v0}, LX/7vI;-><init>(Lcom/facebook/events/model/EventUser;)V

    .line 2545167
    iput-object p2, v2, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2545168
    move-object v0, v2

    .line 2545169
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    .line 2545170
    iget-object v2, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2545171
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2545172
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545173
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2545174
    return-void
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2545175
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    return-object v0
.end method
