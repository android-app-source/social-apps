.class public LX/INd;
.super LX/INP;
.source ""


# instance fields
.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2570980
    invoke-direct {p0, p1}, LX/INP;-><init>(Landroid/content/Context;)V

    .line 2570981
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/INd;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v2

    check-cast v2, LX/0hy;

    const/16 p0, 0xc

    invoke-static {v3, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    iput-object v1, p1, LX/INd;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v2, p1, LX/INd;->e:LX/0hy;

    iput-object p0, p1, LX/INd;->f:LX/0Or;

    iput-object v3, p1, LX/INd;->g:LX/0Zb;

    return-void
.end method

.method public static a$redex0(LX/INd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2570982
    iget-object v0, p0, LX/INd;->g:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2570983
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2570984
    const-string v1, "community_trending_stories"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "community_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2570985
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2570986
    invoke-super {p0}, LX/INP;->a()V

    .line 2570987
    const-class v0, LX/INd;

    invoke-static {v0, p0}, LX/INd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2570988
    invoke-virtual {p0}, LX/INd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2570989
    invoke-virtual {p0, v2, v0, v2, v0}, LX/INd;->setPadding(IIII)V

    .line 2570990
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;Landroid/view/View$OnClickListener;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2570991
    const/4 v1, 0x0

    invoke-super {p0, v1}, LX/INP;->a(Landroid/view/View$OnClickListener;)V

    .line 2570992
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2570993
    :cond_0
    :goto_0
    return-void

    .line 2570994
    :cond_1
    iget-object v1, p0, LX/INP;->b:Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {v1, p1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2570995
    invoke-virtual {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    .line 2570996
    iget-object v1, p0, LX/INP;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/footer/FigFooter;->setVisibility(I)V

    .line 2570997
    iget-object v1, p0, LX/INP;->c:Lcom/facebook/fig/footer/FigFooter;

    new-instance v2, LX/INb;

    invoke-direct {v2, p0, p2, p4}, LX/INb;-><init>(LX/INd;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570998
    :goto_1
    iget-object v1, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 2570999
    invoke-virtual {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;

    .line 2571000
    invoke-static {v0}, LX/DK9;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2571001
    new-instance v5, LX/IOD;

    invoke-virtual {p0}, LX/INd;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, LX/IOD;-><init>(Landroid/content/Context;)V

    .line 2571002
    if-nez v4, :cond_4

    .line 2571003
    :goto_3
    new-instance v4, LX/INc;

    invoke-direct {v4, p0, p2, v0}, LX/INc;-><init>(LX/INd;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCondensedTrendingStoryFieldsModel;)V

    invoke-virtual {v5, v4}, LX/IOD;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571004
    iget-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2571005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2571006
    :cond_2
    iget-object v1, p0, LX/INP;->c:Lcom/facebook/fig/footer/FigFooter;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fig/footer/FigFooter;->setVisibility(I)V

    goto :goto_1

    .line 2571007
    :cond_3
    const-string v0, "community_trending_stories_unit_shown"

    invoke-static {p0, v0, p2}, LX/INd;->a$redex0(LX/INd;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2571008
    :cond_4
    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2571009
    iget-object v7, v5, LX/IOD;->d:LX/1xv;

    invoke-virtual {v7, v6}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v7

    invoke-virtual {v7}, LX/1y2;->a()LX/1y2;

    move-result-object v7

    invoke-virtual {v7}, LX/1y2;->c()LX/1y2;

    move-result-object v7

    invoke-virtual {v7}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v7

    .line 2571010
    iget-object v8, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v8, v7}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryHeaderText(Ljava/lang/CharSequence;)V

    .line 2571011
    iget-object v7, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    const v8, 0x7f0e0adc

    invoke-virtual {v7, v8}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryHeaderTextAppearance(I)V

    .line 2571012
    iget-object v7, v5, LX/IOD;->e:LX/1xe;

    invoke-virtual {v7, v6}, LX/1xe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v7

    .line 2571013
    iget-object v8, v5, LX/IOD;->f:LX/1xg;

    invoke-virtual {v8, v6, v7}, LX/1xg;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 2571014
    iget-object v7, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v7, v6}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setTimestampText(Ljava/lang/CharSequence;)V

    .line 2571015
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    if-eqz v6, :cond_d

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->S()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2571016
    const/4 v6, 0x0

    .line 2571017
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object p1

    move v8, v6

    move v7, v6

    .line 2571018
    :goto_4
    const/4 v6, 0x2

    if-ge v8, v6, :cond_a

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_a

    .line 2571019
    invoke-virtual {p1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2571020
    if-eqz v6, :cond_9

    .line 2571021
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object p3

    .line 2571022
    sget-object p4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p3, p4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-nez p4, :cond_5

    sget-object p4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p3, p4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_6

    .line 2571023
    :cond_5
    invoke-static {v5, v6}, LX/IOD;->setThumnail(LX/IOD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    move v6, v7

    .line 2571024
    :goto_5
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v6

    goto :goto_4

    .line 2571025
    :cond_6
    sget-object p4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p3, p4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-nez p4, :cond_7

    sget-object p4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP_SELL_PRODUCT_ITEM_MARK_AS_SOLD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p3, p4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_9

    .line 2571026
    :cond_7
    const/4 v7, 0x1

    .line 2571027
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p3

    if-eqz p3, :cond_8

    .line 2571028
    iget-object p3, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryMessageText(Ljava/lang/CharSequence;)V

    move v6, v7

    goto :goto_5

    .line 2571029
    :cond_8
    iget-object p3, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryMessageText(Ljava/lang/CharSequence;)V

    :cond_9
    move v6, v7

    goto :goto_5

    .line 2571030
    :cond_a
    if-nez v7, :cond_b

    invoke-static {v4}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 2571031
    iget-object v6, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-static {v4}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryMessageText(Ljava/lang/CharSequence;)V

    .line 2571032
    :cond_b
    :goto_6
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    .line 2571033
    if-eqz v6, :cond_c

    .line 2571034
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, v5, LX/IOD;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {v6, v7, v8}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 2571035
    iget-object v7, v5, LX/IOD;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    new-instance v8, LX/IOC;

    invoke-direct {v8, v5, v6}, LX/IOC;-><init>(LX/IOD;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v7, v8}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571036
    :cond_c
    goto/16 :goto_3

    .line 2571037
    :cond_d
    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2571038
    new-instance v7, Landroid/text/SpannableStringBuilder;

    iget-object v8, v5, LX/IOD;->c:LX/1nA;

    const/4 p1, 0x1

    invoke-virtual {v8, v6, p1}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v7, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2571039
    iget-object v6, v5, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v6, v7}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryMessageText(Ljava/lang/CharSequence;)V

    .line 2571040
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_b

    .line 2571041
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2571042
    invoke-static {v5, v6}, LX/IOD;->setThumnail(LX/IOD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    goto :goto_6
.end method

.method public getHeaderTextResId()I
    .locals 1

    .prologue
    .line 2571043
    const v0, 0x7f08301b

    return v0
.end method
