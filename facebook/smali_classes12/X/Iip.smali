.class public LX/Iip;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iio;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pGroupCommerceEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605096
    iput-object p1, p0, LX/Iip;->a:LX/0Or;

    .line 2605097
    iput-object p2, p0, LX/Iip;->b:LX/0ad;

    .line 2605098
    return-void
.end method


# virtual methods
.method public final a()LX/0Tn;
    .locals 1

    .prologue
    .line 2605099
    sget-object v0, LX/Iiw;->c:LX/0Tn;

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2605100
    iget-object v0, p0, LX/Iip;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Iip;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/Iie;->b:S

    invoke-interface {v0, v2, v3, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2605101
    const/4 v0, 0x2

    return v0
.end method
