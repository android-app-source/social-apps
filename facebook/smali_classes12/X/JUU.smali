.class public LX/JUU;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUU",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699129
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699130
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JUU;->b:LX/0Zi;

    .line 2699131
    iput-object p1, p0, LX/JUU;->a:LX/0Ot;

    .line 2699132
    return-void
.end method

.method public static a(LX/0QB;)LX/JUU;
    .locals 4

    .prologue
    .line 2699133
    const-class v1, LX/JUU;

    monitor-enter v1

    .line 2699134
    :try_start_0
    sget-object v0, LX/JUU;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699135
    sput-object v2, LX/JUU;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699138
    new-instance v3, LX/JUU;

    const/16 p0, 0x207a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUU;-><init>(LX/0Ot;)V

    .line 2699139
    move-object v0, v3

    .line 2699140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 7

    .prologue
    .line 2699144
    check-cast p2, LX/JUT;

    .line 2699145
    iget-object v0, p0, LX/JUU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;

    iget-object v2, p2, LX/JUT;->d:LX/JUq;

    iget-object v3, p2, LX/JUT;->f:LX/1Pr;

    iget-object v4, p2, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p2, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v6, p2, LX/JUT;->h:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->a(Landroid/view/View;LX/JUq;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V

    .line 2699146
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2699147
    check-cast p2, LX/JUT;

    .line 2699148
    iget-object v0, p0, LX/JUU;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;

    iget-object v2, p2, LX/JUT;->a:Ljava/lang/String;

    iget-object v3, p2, LX/JUT;->b:Ljava/lang/String;

    iget-object v4, p2, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v5, p2, LX/JUT;->d:LX/JUq;

    iget-object v6, p2, LX/JUT;->e:Ljava/lang/String;

    move-object v1, p1

    const/4 p2, 0x7

    const/high16 p1, 0x3f800000    # 1.0f

    const/4 p0, 0x2

    const/4 v12, 0x1

    .line 2699149
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v7, v8, :cond_1

    .line 2699150
    iget-boolean v7, v5, LX/JUq;->a:Z

    move v7, v7

    .line 2699151
    if-eqz v7, :cond_0

    const v7, 0x7f08178a

    invoke-virtual {v1, v7}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2699152
    :goto_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b00a2

    invoke-interface {v8, v9}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b25c1

    invoke-interface {v8, v9}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    .line 2699153
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2699154
    const/4 v9, 0x0

    .line 2699155
    :goto_1
    move-object v9, v9

    .line 2699156
    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v9, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a0099

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b0050

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v12}, LX/1ne;->t(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v12}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a00a4

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b004e

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v12}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    const/16 v10, 0x8

    const v11, 0x7f0b0060

    invoke-interface {v9, v10, v11}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v12}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v12}, LX/1ne;->t(I)LX/1ne;

    move-result-object v7

    const v9, 0x7f0a00a4

    invoke-virtual {v7, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const v9, 0x7f0b004e

    invoke-virtual {v7, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v9, 0x7f0b0060

    invoke-interface {v7, p2, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    const v9, 0x7f0b0060

    invoke-interface {v7, p0, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    const v9, 0x7f02079d

    invoke-interface {v7, v9}, LX/1Di;->x(I)LX/1Di;

    move-result-object v7

    const/4 v9, 0x6

    const v10, 0x7f0b25c4

    invoke-interface {v7, v9, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const v9, 0x7f0b25c5

    invoke-interface {v7, p2, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p1}, LX/1Di;->c(F)LX/1Di;

    move-result-object v7

    .line 2699157
    const v9, 0x1a587a1c

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2699158
    invoke-interface {v7, v9}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 2699159
    return-object v0

    .line 2699160
    :cond_0
    const v7, 0x7f081788

    invoke-virtual {v1, v7}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 2699161
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    :cond_2
    iget-object v9, v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->b:LX/1nu;

    invoke-virtual {v9, v1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v9

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v9

    sget-object v10, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v10}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v9

    sget-object v10, LX/1Up;->f:LX/1Up;

    invoke-virtual {v9, v10}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v9

    const v10, 0x7f0a010a

    invoke-virtual {v9, v10}, LX/1nw;->h(I)LX/1nw;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const v10, 0x7f0102a9

    invoke-interface {v9, v10}, LX/1Di;->h(I)LX/1Di;

    move-result-object v9

    const v10, 0x7f0102a9

    invoke-interface {v9, v10}, LX/1Di;->p(I)LX/1Di;

    move-result-object v9

    const/4 v10, 0x7

    const v11, 0x7f0b0060

    invoke-interface {v9, v10, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    const/4 v10, 0x0

    const v11, 0x7f0b0060

    invoke-interface {v9, v10, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    const v10, 0x7f021388

    invoke-interface {v9, v10}, LX/1Di;->z(I)LX/1Di;

    move-result-object v9

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2699162
    invoke-static {}, LX/1dS;->b()V

    .line 2699163
    iget v0, p1, LX/1dQ;->b:I

    .line 2699164
    packed-switch v0, :pswitch_data_0

    .line 2699165
    :goto_0
    return-object v2

    .line 2699166
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2699167
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JUU;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1a587a1c
        :pswitch_0
    .end packed-switch
.end method
