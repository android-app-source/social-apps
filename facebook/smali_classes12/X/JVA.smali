.class public LX/JVA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JV8;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700288
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JVA;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700289
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700290
    iput-object p1, p0, LX/JVA;->b:LX/0Ot;

    .line 2700291
    return-void
.end method

.method public static a(LX/0QB;)LX/JVA;
    .locals 4

    .prologue
    .line 2700292
    const-class v1, LX/JVA;

    monitor-enter v1

    .line 2700293
    :try_start_0
    sget-object v0, LX/JVA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700294
    sput-object v2, LX/JVA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700297
    new-instance v3, LX/JVA;

    const/16 p0, 0x208e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVA;-><init>(LX/0Ot;)V

    .line 2700298
    move-object v0, v3

    .line 2700299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2700303
    const v0, 0x33bede9d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2700304
    iget-object v0, p0, LX/JVA;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p2, 0x2

    .line 2700305
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    const p0, 0x7f0e0123

    invoke-static {p1, v1, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    const p0, 0x7f08184f

    invoke-virtual {v1, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    const p0, 0x7f0b0917

    invoke-interface {v0, v1, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b0950

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    .line 2700306
    const v1, 0x33bede9d

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2700307
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2700308
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2700309
    invoke-static {}, LX/1dS;->b()V

    .line 2700310
    iget v0, p1, LX/1dQ;->b:I

    .line 2700311
    packed-switch v0, :pswitch_data_0

    .line 2700312
    :goto_0
    return-object v2

    .line 2700313
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700314
    iget-object v3, p0, LX/JVA;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JVB;

    .line 2700315
    iget-object p1, v3, LX/JVB;->a:LX/7j6;

    const/4 p2, 0x0

    sget-object p0, LX/7iP;->SHARE:LX/7iP;

    .line 2700316
    sget-object v1, LX/0ax;->fW:Ljava/lang/String;

    iget-object v3, p0, LX/7iP;->value:Ljava/lang/String;

    invoke-static {v1, p2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2700317
    iget-object v3, p1, LX/7j6;->c:LX/17Y;

    iget-object v0, p1, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v3, v0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2700318
    if-nez v1, :cond_0

    .line 2700319
    :goto_1
    goto :goto_0

    .line 2700320
    :cond_0
    iget-object v3, p1, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p1, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {v3, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x33bede9d
        :pswitch_0
    .end packed-switch
.end method
