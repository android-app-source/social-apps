.class public LX/ImN;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final b:LX/J0B;

.field private final c:LX/2JT;

.field private final d:LX/Izh;

.field private final e:LX/Izg;

.field private final f:LX/Izk;

.field private final g:LX/2JS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609650
    const-class v0, LX/ImN;

    sput-object v0, LX/ImN;->a:Ljava/lang/Class;

    .line 2609651
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImN;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/J0B;LX/2JT;LX/Izh;LX/Izg;LX/Izk;LX/2JS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609642
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609643
    iput-object p1, p0, LX/ImN;->b:LX/J0B;

    .line 2609644
    iput-object p2, p0, LX/ImN;->c:LX/2JT;

    .line 2609645
    iput-object p3, p0, LX/ImN;->d:LX/Izh;

    .line 2609646
    iput-object p4, p0, LX/ImN;->e:LX/Izg;

    .line 2609647
    iput-object p5, p0, LX/ImN;->f:LX/Izk;

    .line 2609648
    iput-object p6, p0, LX/ImN;->g:LX/2JS;

    .line 2609649
    return-void
.end method

.method public static a(LX/0QB;)LX/ImN;
    .locals 14

    .prologue
    .line 2609563
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609564
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609565
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609566
    if-nez v1, :cond_0

    .line 2609567
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609568
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609569
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609570
    sget-object v1, LX/ImN;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609571
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609572
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609573
    :cond_1
    if-nez v1, :cond_4

    .line 2609574
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609575
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609576
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609577
    new-instance v7, LX/ImN;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v8

    check-cast v8, LX/J0B;

    invoke-static {v0}, LX/2JT;->a(LX/0QB;)LX/2JT;

    move-result-object v9

    check-cast v9, LX/2JT;

    invoke-static {v0}, LX/Izh;->a(LX/0QB;)LX/Izh;

    move-result-object v10

    check-cast v10, LX/Izh;

    invoke-static {v0}, LX/Izg;->a(LX/0QB;)LX/Izg;

    move-result-object v11

    check-cast v11, LX/Izg;

    invoke-static {v0}, LX/Izk;->a(LX/0QB;)LX/Izk;

    move-result-object v12

    check-cast v12, LX/Izk;

    invoke-static {v0}, LX/2JS;->a(LX/0QB;)LX/2JS;

    move-result-object v13

    check-cast v13, LX/2JS;

    invoke-direct/range {v7 .. v13}, LX/ImN;-><init>(LX/J0B;LX/2JT;LX/Izh;LX/Izg;LX/Izk;LX/2JS;)V

    .line 2609578
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609579
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609580
    if-nez v1, :cond_2

    .line 2609581
    sget-object v0, LX/ImN;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImN;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609582
    :goto_1
    if-eqz v0, :cond_3

    .line 2609583
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609584
    :goto_3
    check-cast v0, LX/ImN;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609585
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609586
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609587
    :catchall_1
    move-exception v0

    .line 2609588
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609589
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609590
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609591
    :cond_2
    :try_start_8
    sget-object v0, LX/ImN;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImN;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609597
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->i()LX/Ips;

    move-result-object v1

    .line 2609598
    iget-object v0, p0, LX/ImN;->e:LX/Izg;

    iget-object v2, v1, LX/Ips;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/Izg;->a(J)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v2

    .line 2609599
    sget-object v0, LX/Ipz;->b:Ljava/util/Map;

    iget-object v3, v1, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v0

    .line 2609600
    new-instance v3, LX/Dtm;

    invoke-direct {v3}, LX/Dtm;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 2609601
    iput-object v4, v3, LX/Dtm;->d:Ljava/lang/String;

    .line 2609602
    move-object v3, v3

    .line 2609603
    iput-object v0, v3, LX/Dtm;->f:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 2609604
    move-object v3, v3

    .line 2609605
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2609606
    iput-object v0, v3, LX/Dtm;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2609607
    move-object v0, v3

    .line 2609608
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->c()J

    move-result-wide v4

    .line 2609609
    iput-wide v4, v0, LX/Dtm;->b:J

    .line 2609610
    move-object v0, v0

    .line 2609611
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 2609612
    iput-object v3, v0, LX/Dtm;->c:Ljava/lang/String;

    .line 2609613
    move-object v0, v0

    .line 2609614
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->kg_()Ljava/lang/String;

    move-result-object v3

    .line 2609615
    iput-object v3, v0, LX/Dtm;->e:Ljava/lang/String;

    .line 2609616
    move-object v3, v0

    .line 2609617
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2609618
    iput-object v0, v3, LX/Dtm;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2609619
    move-object v3, v3

    .line 2609620
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->k()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609621
    iput-object v0, v3, LX/Dtm;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609622
    move-object v3, v3

    .line 2609623
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609624
    iput-object v0, v3, LX/Dtm;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2609625
    move-object v0, v3

    .line 2609626
    iget-object v2, v1, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 2609627
    iget-object v2, p0, LX/ImN;->d:LX/Izh;

    iget-object v3, v1, LX/Ips;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LX/Izh;->a(J)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v2

    .line 2609628
    if-eqz v2, :cond_0

    .line 2609629
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    move-result-object v2

    .line 2609630
    iput-object v2, v0, LX/Dtm;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2609631
    :cond_0
    iget-object v2, v1, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 2609632
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, v1, LX/Ips;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 2609633
    iput-wide v2, v0, LX/Dtm;->k:J

    .line 2609634
    :cond_1
    invoke-virtual {v0}, LX/Dtm;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    .line 2609635
    iget-object v1, p0, LX/ImN;->c:LX/2JT;

    invoke-virtual {v1, v0}, LX/2JT;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2609636
    iget-object v1, p0, LX/ImN;->f:LX/Izk;

    invoke-virtual {v1, v0}, LX/Izk;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2609637
    :cond_2
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2609638
    const-string v2, "payment_request"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2609639
    return-object v1

    .line 2609640
    :cond_3
    iget-object v1, p0, LX/ImN;->c:LX/2JT;

    invoke-virtual {v1, v0}, LX/2JT;->c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2609641
    iget-object v1, p0, LX/ImN;->f:LX/Izk;

    invoke-virtual {v1, v0}, LX/Izk;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609592
    const-string v0, "payment_request"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2609593
    if-eqz v0, :cond_0

    .line 2609594
    iget-object v1, p0, LX/ImN;->g:LX/2JS;

    invoke-virtual {v1, v0}, LX/2JS;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2609595
    iget-object v1, p0, LX/ImN;->b:LX/J0B;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/J0B;->a(Ljava/lang/String;)V

    .line 2609596
    :cond_0
    return-void
.end method
