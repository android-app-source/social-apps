.class public final LX/Ibx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IcK;

.field public final synthetic b:LX/Ic4;


# direct methods
.method public constructor <init>(LX/Ic4;LX/IcK;)V
    .locals 0

    .prologue
    .line 2594947
    iput-object p1, p0, LX/Ibx;->b:LX/Ic4;

    iput-object p2, p0, LX/Ibx;->a:LX/IcK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2595003
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    invoke-static {v0}, LX/Ic4;->b(LX/Ic4;)V

    .line 2595004
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    const/4 v1, 0x0

    .line 2595005
    iput-object v1, v0, LX/Ic4;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2595006
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2595007
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->c:LX/03V;

    sget-object v1, LX/Ic4;->a:Ljava/lang/String;

    const-string v2, "Can\'t get request mutation result"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595008
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2594948
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2594949
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    invoke-static {v0}, LX/Ic4;->b(LX/Ic4;)V

    .line 2594950
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    const/4 v1, 0x0

    .line 2594951
    iput-object v1, v0, LX/Ic4;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2594952
    if-eqz p1, :cond_0

    .line 2594953
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594954
    if-eqz v0, :cond_0

    .line 2594955
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594956
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->k()Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2594957
    :cond_0
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->c:LX/03V;

    sget-object v1, LX/Ic4;->a:Ljava/lang/String;

    const-string v2, "Get wrong ride request mutation result"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594958
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2594959
    :cond_1
    :goto_0
    return-void

    .line 2594960
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594961
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->k()Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    move-result-object v0

    .line 2594962
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-ne v0, v1, :cond_3

    .line 2594963
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594964
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->l()LX/1vs;

    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->m:LX/IdG;

    const/4 v3, 0x1

    .line 2594965
    iget-object v1, v0, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v2, "success_request_ride"

    .line 2594966
    invoke-virtual {v1, v2}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2594967
    iget-object v4, v1, LX/Ib2;->b:LX/0if;

    sget-object p0, LX/0ig;->o:LX/0ih;

    invoke-virtual {v4, p0}, LX/0if;->c(LX/0ih;)V

    .line 2594968
    iget-object v1, v0, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    .line 2594969
    iput-boolean v3, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->v:Z

    .line 2594970
    iget-object v1, v0, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v1, v3}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Z)V

    .line 2594971
    iget-object v1, v0, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2594972
    iget-object v1, v0, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->onBackPressed()V

    .line 2594973
    goto :goto_0

    .line 2594974
    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SURGE_ACCEPTANCE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-ne v0, v1, :cond_4

    .line 2594975
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594976
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v3, p0, LX/Ibx;->a:LX/IcK;

    invoke-virtual {v2, v1, v0, v3}, LX/Ic4;->a(LX/15i;ILX/IcK;)V

    goto :goto_0

    .line 2594977
    :cond_4
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->PRESET_PRICE_FLOW_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-ne v0, v1, :cond_6

    .line 2594978
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594979
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 2594980
    iget-object v3, p0, LX/Ibx;->b:LX/Ic4;

    .line 2594981
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594982
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LX/Ibx;->a:LX/IcK;

    .line 2594983
    if-eqz v2, :cond_5

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2594984
    :cond_5
    iget-object v5, v3, LX/Ic4;->g:LX/IZK;

    invoke-virtual {v5}, LX/IZK;->a()V

    .line 2594985
    :goto_1
    goto/16 :goto_0

    .line 2594986
    :cond_6
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ADDING_PAYMENT_NEEDED:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_INVALID_PAYMENT_INFORMATION:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->ERROR_UPDATE_PAYMENT:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    if-ne v0, v1, :cond_9

    .line 2594987
    :cond_7
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v1, v0, LX/Ic4;->m:LX/IdG;

    .line 2594988
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594989
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->b:Landroid/content/Context;

    const v2, 0x7f082d90

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2594990
    :goto_2
    iget-object v2, v1, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    .line 2594991
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v4, "show_payment_error_dialog"

    invoke-virtual {v3, v4}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2594992
    new-instance v3, LX/31Y;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08001a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Id7;

    invoke-direct {v5, v2}, LX/Id7;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Id6;

    invoke-direct {v5, v2}, LX/Id6;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    .line 2594993
    iget-object v2, v1, LX/IdG;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Z)V

    .line 2594994
    goto/16 :goto_0

    .line 2594995
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594996
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2594997
    :cond_9
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594998
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2594999
    iget-object v0, p0, LX/Ibx;->b:LX/Ic4;

    iget-object v1, v0, LX/Ic4;->g:LX/IZK;

    .line 2595000
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595001
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/IZK;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2595002
    :cond_a
    new-instance v5, LX/31Y;

    iget-object p0, v3, LX/Ic4;->b:Landroid/content/Context;

    invoke-direct {v5, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    iget-object p0, v3, LX/Ic4;->b:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f082d75

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/Ibz;

    invoke-direct {p1, v3, v4, v1, v2}, LX/Ibz;-><init>(LX/Ic4;LX/IcK;LX/15i;I)V

    invoke-virtual {v5, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_1
.end method
