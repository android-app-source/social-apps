.class public final enum LX/HjA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HjA;",
        ">;"
    }
.end annotation


# static fields
.field public static final ALL:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/HjA;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ICON:LX/HjA;

.field public static final enum IMAGE:LX/HjA;

.field public static final enum NONE:LX/HjA;

.field private static final synthetic b:[LX/HjA;


# instance fields
.field private final a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, LX/HjA;

    const-string v1, "NONE"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, LX/HjA;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/HjA;->NONE:LX/HjA;

    new-instance v0, LX/HjA;

    const-string v1, "ICON"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, LX/HjA;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/HjA;->ICON:LX/HjA;

    new-instance v0, LX/HjA;

    const-string v1, "IMAGE"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, LX/HjA;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/HjA;->IMAGE:LX/HjA;

    const/4 v0, 0x3

    new-array v0, v0, [LX/HjA;

    sget-object v1, LX/HjA;->NONE:LX/HjA;

    aput-object v1, v0, v4

    sget-object v1, LX/HjA;->ICON:LX/HjA;

    aput-object v1, v0, v5

    sget-object v1, LX/HjA;->IMAGE:LX/HjA;

    aput-object v1, v0, v6

    sput-object v0, LX/HjA;->b:[LX/HjA;

    const-class v0, LX/HjA;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/HjA;->ALL:Ljava/util/EnumSet;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, LX/HjA;->a:J

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HjA;
    .locals 1

    const-class v0, LX/HjA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HjA;

    return-object v0
.end method

.method public static values()[LX/HjA;
    .locals 1

    sget-object v0, LX/HjA;->b:[LX/HjA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HjA;

    return-object v0
.end method


# virtual methods
.method public final getCacheFlagValue()J
    .locals 2

    iget-wide v0, p0, LX/HjA;->a:J

    return-wide v0
.end method
