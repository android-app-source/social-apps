.class public final LX/HXE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HXF;


# direct methods
.method public constructor <init>(LX/HXF;)V
    .locals 0

    .prologue
    .line 2478591
    iput-object p1, p0, LX/HXE;->a:LX/HXF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x3118be2b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478592
    iget-object v1, p0, LX/HXE;->a:LX/HXF;

    iget-object v1, v1, LX/HXF;->d:LX/9XE;

    iget-object v2, p0, LX/HXE;->a:LX/HXF;

    iget-wide v2, v2, LX/HXF;->a:J

    sget-object v4, LX/9XI;->EVENT_TAPPED_TIMELINE_FOOTER_CREATE_PAGE:LX/9XI;

    invoke-virtual {v4}, LX/9XI;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2478593
    iget-object v6, v1, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9XI;->EVENT_TAPPED_FB_EVENT:LX/9XI;

    invoke-static {v7, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "event_id"

    invoke-virtual {v7, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2478594
    iget-object v1, p0, LX/HXE;->a:LX/HXF;

    iget-object v1, v1, LX/HXF;->c:LX/17W;

    iget-object v2, p0, LX/HXE;->a:LX/HXF;

    iget-object v2, v2, LX/HXF;->b:Landroid/content/Context;

    sget-object v3, LX/0ax;->bk:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2478595
    const v1, 0x3d709ad5

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
