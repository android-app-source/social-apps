.class public LX/It3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final a:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

.field private final b:LX/0So;

.field public final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public g:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public i:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public j:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V
    .locals 2

    .prologue
    .line 2621910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621911
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    .line 2621912
    iput-object p1, p0, LX/It3;->b:LX/0So;

    .line 2621913
    iput-object p2, p0, LX/It3;->a:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2621914
    iget-object v0, p0, LX/It3;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/It3;->d:J

    .line 2621915
    return-void
.end method

.method public static declared-synchronized o(LX/It3;)V
    .locals 2

    .prologue
    .line 2621905
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/It3;->f:Ljava/lang/String;

    .line 2621906
    const/4 v0, 0x0

    iput v0, p0, LX/It3;->g:I

    .line 2621907
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/It3;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621908
    monitor-exit p0

    return-void

    .line 2621909
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 2621870
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621871
    monitor-exit p0

    return-void

    .line 2621872
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2621904
    iget-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2621896
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2621897
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2621898
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2621899
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2621900
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621901
    const/4 v0, 0x1

    .line 2621902
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2621903
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2621893
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621894
    monitor-exit p0

    return-void

    .line 2621895
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2621886
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, p0, LX/It3;->a:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2621887
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v1, p0, LX/It3;->f:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2621888
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iput-object v0, p0, LX/It3;->f:Ljava/lang/String;

    .line 2621889
    iget-object v0, p0, LX/It3;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/It3;->h:J

    .line 2621890
    const/4 v0, 0x0

    iput v0, p0, LX/It3;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621891
    :cond_0
    monitor-exit p0

    return-void

    .line 2621892
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 2621882
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/It3;->e:Z

    .line 2621883
    iget v0, p0, LX/It3;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/It3;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621884
    monitor-exit p0

    return-void

    .line 2621885
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 1

    .prologue
    .line 2621878
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/It3;->e:Z

    .line 2621879
    invoke-static {p0}, LX/It3;->o(LX/It3;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621880
    monitor-exit p0

    return-void

    .line 2621881
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 2621874
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/It3;->e:Z

    .line 2621875
    invoke-static {p0}, LX/It3;->o(LX/It3;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621876
    monitor-exit p0

    return-void

    .line 2621877
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized n()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2621873
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/It3;->c:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
