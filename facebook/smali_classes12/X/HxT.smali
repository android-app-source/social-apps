.class public final LX/HxT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

.field private b:LX/Hx6;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFilterView;LX/Hx6;)V
    .locals 0

    .prologue
    .line 2522590
    iput-object p1, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522591
    iput-object p2, p0, LX/HxT;->b:LX/Hx6;

    .line 2522592
    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2522593
    iget-object v0, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->l:LX/HxU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    iget-object v1, p0, LX/HxT;->b:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2522594
    iget-object v0, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v1, p0, LX/HxT;->b:LX/Hx6;

    invoke-virtual {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->setDashboardFilterType(LX/Hx6;)V

    .line 2522595
    iget-object v0, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->b:LX/1nQ;

    iget-object v1, p0, LX/HxT;->b:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nQ;->c(Ljava/lang/String;)V

    .line 2522596
    iget-object v0, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->l:LX/HxU;

    iget-object v1, p0, LX/HxT;->a:Lcom/facebook/events/dashboard/EventsDashboardFilterView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardFilterView;->k:LX/Hx6;

    invoke-interface {v0, v1}, LX/HxU;->a(LX/Hx6;)V

    .line 2522597
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
