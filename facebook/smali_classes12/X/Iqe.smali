.class public final enum LX/Iqe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iqe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iqe;

.field public static final enum DISABLED:LX/Iqe;

.field public static final enum DOODLE:LX/Iqe;

.field public static final enum DOODLING:LX/Iqe;

.field public static final enum IDLE:LX/Iqe;

.field public static final enum STICKER:LX/Iqe;

.field public static final enum STICKER_COLLAPSED:LX/Iqe;

.field public static final enum TEXT:LX/Iqe;

.field public static final enum TRANSFORMING:LX/Iqe;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2617106
    new-instance v0, LX/Iqe;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->IDLE:LX/Iqe;

    .line 2617107
    new-instance v0, LX/Iqe;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->DISABLED:LX/Iqe;

    .line 2617108
    new-instance v0, LX/Iqe;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v5}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->TEXT:LX/Iqe;

    .line 2617109
    new-instance v0, LX/Iqe;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v6}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->STICKER:LX/Iqe;

    .line 2617110
    new-instance v0, LX/Iqe;

    const-string v1, "STICKER_COLLAPSED"

    invoke-direct {v0, v1, v7}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    .line 2617111
    new-instance v0, LX/Iqe;

    const-string v1, "DOODLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->DOODLE:LX/Iqe;

    .line 2617112
    new-instance v0, LX/Iqe;

    const-string v1, "DOODLING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->DOODLING:LX/Iqe;

    .line 2617113
    new-instance v0, LX/Iqe;

    const-string v1, "TRANSFORMING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Iqe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqe;->TRANSFORMING:LX/Iqe;

    .line 2617114
    const/16 v0, 0x8

    new-array v0, v0, [LX/Iqe;

    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    aput-object v1, v0, v3

    sget-object v1, LX/Iqe;->DISABLED:LX/Iqe;

    aput-object v1, v0, v4

    sget-object v1, LX/Iqe;->TEXT:LX/Iqe;

    aput-object v1, v0, v5

    sget-object v1, LX/Iqe;->STICKER:LX/Iqe;

    aput-object v1, v0, v6

    sget-object v1, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Iqe;->DOODLE:LX/Iqe;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Iqe;->DOODLING:LX/Iqe;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Iqe;->TRANSFORMING:LX/Iqe;

    aput-object v2, v0, v1

    sput-object v0, LX/Iqe;->$VALUES:[LX/Iqe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2617105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isUserInteracting(LX/Iqe;)Z
    .locals 1

    .prologue
    .line 2617104
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/Iqe;->isUserInteracting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iqe;
    .locals 1

    .prologue
    .line 2617115
    const-class v0, LX/Iqe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iqe;

    return-object v0
.end method

.method public static values()[LX/Iqe;
    .locals 1

    .prologue
    .line 2617103
    sget-object v0, LX/Iqe;->$VALUES:[LX/Iqe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iqe;

    return-object v0
.end method


# virtual methods
.method public final isFullscreen()Z
    .locals 3

    .prologue
    .line 2617102
    const/4 v0, 0x2

    new-array v0, v0, [LX/Iqe;

    const/4 v1, 0x0

    sget-object v2, LX/Iqe;->TEXT:LX/Iqe;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/Iqe;->STICKER:LX/Iqe;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/Iqe;->isOneOf([LX/Iqe;)Z

    move-result v0

    return v0
.end method

.method public final varargs isOneOf([LX/Iqe;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2617096
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617097
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 2617098
    if-ne p0, v3, :cond_1

    .line 2617099
    const/4 v0, 0x1

    .line 2617100
    :cond_0
    return v0

    .line 2617101
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final isUserInteracting()Z
    .locals 3

    .prologue
    .line 2617095
    const/4 v0, 0x2

    new-array v0, v0, [LX/Iqe;

    const/4 v1, 0x0

    sget-object v2, LX/Iqe;->DOODLING:LX/Iqe;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/Iqe;->TRANSFORMING:LX/Iqe;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/Iqe;->isOneOf([LX/Iqe;)Z

    move-result v0

    return v0
.end method
