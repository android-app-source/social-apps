.class public final LX/J1r;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V
    .locals 0

    .prologue
    .line 2639472
    iput-object p1, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 1

    .prologue
    .line 2639461
    iget-object v0, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a(LX/73T;)V

    .line 2639462
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2639470
    iget-object v0, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    iget-object v0, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2639471
    return-void
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 2639473
    iget-object v0, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_dialog_fragment"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2639474
    return-void
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 2

    .prologue
    .line 2639465
    iget-object v0, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639466
    iget-object v1, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    .line 2639467
    :cond_0
    :goto_0
    return-void

    .line 2639468
    :cond_1
    iget-object v1, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    invoke-interface {v1}, LX/J1v;->a()V

    .line 2639469
    new-instance v1, LX/J1s;

    invoke-direct {v1, v0}, LX/J1s;-><init>(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V

    iget-object p0, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v1, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2639463
    iget-object v0, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    iget-object v0, v0, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/J1r;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2639464
    return-void
.end method
