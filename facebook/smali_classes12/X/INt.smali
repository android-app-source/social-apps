.class public final LX/INt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;

.field public final synthetic b:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/views/CommunityForSalePostItemView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;)V
    .locals 0

    .prologue
    .line 2571251
    iput-object p1, p0, LX/INt;->b:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    iput-object p2, p0, LX/INt;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x383f0299

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571252
    iget-object v1, p0, LX/INt;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;

    move-result-object v1

    .line 2571253
    iget-object v2, p0, LX/INt;->b:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    iget-object v2, v2, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2571254
    iput-object v4, v3, LX/23u;->N:Ljava/lang/String;

    .line 2571255
    move-object v3, v3

    .line 2571256
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel$EdgesModel$NodeModel$AttachmentsModel$TargetModel$StoryModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2571257
    iput-object v1, v3, LX/23u;->aP:Ljava/lang/String;

    .line 2571258
    move-object v1, v3

    .line 2571259
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    .line 2571260
    iget-object v2, p0, LX/INt;->b:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    iget-object v2, v2, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/INt;->b:Lcom/facebook/groups/community/views/CommunityForSalePostItemView;

    iget-object v3, v3, Lcom/facebook/groups/community/views/CommunityForSalePostItemView;->d:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2571261
    const v1, -0x371217b7

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
