.class public LX/Ifz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ifx;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final d:LX/0lC;

.field public e:LX/Ifv;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600316
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    iput-object v0, p0, LX/Ifz;->d:LX/0lC;

    .line 2600317
    return-void
.end method

.method public static d(LX/Ifz;)V
    .locals 5

    .prologue
    .line 2600318
    iget-object v0, p0, LX/Ifz;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600319
    sget-object v1, LX/Ig2;->c:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600320
    iget-object v1, p0, LX/Ifz;->e:LX/Ifv;

    .line 2600321
    iget v2, v1, LX/Ifv;->m:I

    move v1, v2

    .line 2600322
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2600323
    const-string v3, "updateInterval"

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2600324
    const-string v1, "lastCancelledMessage"

    iget-object v3, p0, LX/Ifz;->e:LX/Ifv;

    .line 2600325
    iget-object v4, v3, LX/Ifv;->l:Ljava/lang/String;

    move-object v3, v4

    .line 2600326
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2600327
    iget-object v1, p0, LX/Ifz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2600328
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2600329
    invoke-static {p0}, LX/Ifz;->d(LX/Ifz;)V

    .line 2600330
    return-void
.end method

.method public final a(LX/Ifv;)V
    .locals 4

    .prologue
    .line 2600331
    iput-object p1, p0, LX/Ifz;->e:LX/Ifv;

    .line 2600332
    iget-object v0, p0, LX/Ifz;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    .line 2600333
    sget-object v1, LX/Ig2;->c:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2600334
    iget-object v1, p0, LX/Ifz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2600335
    :try_start_0
    iget-object v1, p0, LX/Ifz;->d:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    .line 2600336
    const-string v1, "updateInterval"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    sget v2, LX/Ifv;->a:I

    invoke-virtual {v1, v2}, LX/0lF;->b(I)I

    move-result v1

    .line 2600337
    iget-object v2, p0, LX/Ifz;->e:LX/Ifv;

    .line 2600338
    iget v3, v2, LX/Ifv;->m:I

    if-ne v3, v1, :cond_0

    .line 2600339
    :goto_0
    const-string v1, "lastCancelledMessage"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2600340
    iget-object v1, p0, LX/Ifz;->e:LX/Ifv;

    invoke-virtual {v1, v0}, LX/Ifv;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2600341
    :goto_1
    return-void

    .line 2600342
    :catch_0
    move-exception v0

    .line 2600343
    iget-object v1, p0, LX/Ifz;->c:LX/03V;

    const-string v2, "Error reading object from persistent storage."

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2600344
    :cond_0
    iput v1, v2, LX/Ifv;->m:I

    .line 2600345
    iget-object v3, v2, LX/Ifv;->d:LX/0mY;

    invoke-virtual {v3}, LX/0mY;->a()Ljava/util/List;

    move-result-object v1

    .line 2600346
    const/4 v3, 0x0

    move p1, v3

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge p1, v3, :cond_1

    .line 2600347
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ifx;

    invoke-interface {v3}, LX/Ifx;->a()V

    .line 2600348
    add-int/lit8 v3, p1, 0x1

    move p1, v3

    goto :goto_2

    .line 2600349
    :cond_1
    iget-object v3, v2, LX/Ifv;->d:LX/0mY;

    invoke-virtual {v3}, LX/0mY;->b()V

    .line 2600350
    goto :goto_0
.end method
