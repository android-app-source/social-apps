.class public LX/HgK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2492925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492926
    iput p1, p0, LX/HgK;->a:I

    .line 2492927
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2492917
    new-instance v0, LX/HgV;

    invoke-direct {v0}, LX/HgV;-><init>()V

    move-object v0, v0

    .line 2492918
    const-string v1, "member_count"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2492919
    const-string v1, "item_count"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2492920
    const-string v1, "from_page_cursor"

    .line 2492921
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2492922
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2492923
    const-string v1, "profile_image_size"

    iget v2, p0, LX/HgK;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2492924
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2492906
    if-eqz p1, :cond_2

    .line 2492907
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2492908
    if-eqz v0, :cond_2

    .line 2492909
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2492910
    check-cast v0, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel;->a()Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;

    move-result-object v0

    .line 2492911
    :goto_0
    move-object v1, v0

    .line 2492912
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2492913
    :cond_0
    const/4 v0, 0x0

    .line 2492914
    :goto_1
    return-object v0

    .line 2492915
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 2492916
    new-instance v0, LX/5Mb;

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$FetchWorkSuggestedGroupsQueryModel$GroupsYouShouldJoinModel;->a()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, v2, v3}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
