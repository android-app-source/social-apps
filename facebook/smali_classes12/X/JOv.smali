.class public LX/JOv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0SI;

.field private final b:LX/CSL;

.field private final c:LX/1Kf;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/17Y;

.field public final f:LX/1vg;

.field private final g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SI;LX/CSL;LX/1Kf;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1vg;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SI;",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            "LX/1Kf;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/1vg;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688493
    iput-object p1, p0, LX/JOv;->a:LX/0SI;

    .line 2688494
    iput-object p2, p0, LX/JOv;->b:LX/CSL;

    .line 2688495
    iput-object p3, p0, LX/JOv;->c:LX/1Kf;

    .line 2688496
    iput-object p4, p0, LX/JOv;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2688497
    iput-object p5, p0, LX/JOv;->e:LX/17Y;

    .line 2688498
    iput-object p6, p0, LX/JOv;->f:LX/1vg;

    .line 2688499
    iput-object p7, p0, LX/JOv;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2688500
    iput-object p8, p0, LX/JOv;->h:LX/0Ot;

    .line 2688501
    return-void
.end method

.method public static a(LX/0QB;)LX/JOv;
    .locals 12

    .prologue
    .line 2688481
    const-class v1, LX/JOv;

    monitor-enter v1

    .line 2688482
    :try_start_0
    sget-object v0, LX/JOv;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688483
    sput-object v2, LX/JOv;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688484
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688485
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688486
    new-instance v3, LX/JOv;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    invoke-static {v0}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v5

    check-cast v5, LX/CSL;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v6

    check-cast v6, LX/1Kf;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v9

    check-cast v9, LX/1vg;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v10

    check-cast v10, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const/16 v11, 0x1fdc

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/JOv;-><init>(LX/0SI;LX/CSL;LX/1Kf;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1vg;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Ot;)V

    .line 2688487
    move-object v0, v3

    .line 2688488
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688489
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688490
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/auth/viewercontext/ViewerContext;JLjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 8

    .prologue
    .line 2688457
    iget-object v0, p0, LX/JOv;->c:LX/1Kf;

    const/4 v7, 0x0

    iget-object v1, p0, LX/JOv;->b:LX/CSL;

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, LX/CSL;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v2, 0x6dc

    invoke-interface {v0, v7, v1, v2, p6}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2688458
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V
    .locals 7
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2688459
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    .line 2688460
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2688461
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2688462
    iget-object v1, p0, LX/JOv;->a:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2688463
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v5, 0x0

    .line 2688464
    :goto_0
    sget-object v3, LX/JOu;->a:[I

    invoke-static {v2}, LX/JPN;->a(Ljava/lang/String;)LX/JPM;

    move-result-object v2

    invoke-virtual {v2}, LX/JPM;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2688465
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal hpp card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2688466
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 2688467
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2688468
    sget-object v2, LX/0ax;->bo:Ljava/lang/String;

    .line 2688469
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    .line 2688470
    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2688471
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2688472
    iget-object v0, p0, LX/JOv;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2688473
    :cond_1
    :goto_1
    :pswitch_1
    iget-object v0, p0, LX/JOv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JPN;

    invoke-virtual {v0, p2, p3}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2688474
    return-void

    .line 2688475
    :pswitch_2
    iget-object v1, p0, LX/JOv;->e:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/app/Activity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    sget-object v2, LX/0ax;->bb:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2688476
    if-eqz v1, :cond_1

    .line 2688477
    iget-object v2, p0, LX/JOv;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2688478
    :pswitch_3
    iget-object v0, p0, LX/JOv;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2688479
    iget-object v0, p0, LX/JOv;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JPN;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " action uri can\'t be handled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, p3, v1}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_1

    .line 2688480
    :pswitch_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Landroid/app/Activity;

    invoke-static {v0, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/JOv;->a(Lcom/facebook/auth/viewercontext/ViewerContext;JLjava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
