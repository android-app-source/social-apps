.class public LX/JF3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/JFJ;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/2vn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vn",
            "<",
            "LX/JFV;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/JF5;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/2vs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2vs",
            "<",
            "LX/JF5;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/JEt;

.field public static final f:LX/JEw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/JF3;->a:LX/2vn;

    new-instance v0, LX/2vn;

    invoke-direct {v0}, LX/2vn;-><init>()V

    sput-object v0, LX/JF3;->b:LX/2vn;

    new-instance v0, LX/2vs;

    const-string v1, "Places.GEO_DATA_API"

    new-instance v2, LX/JFI;

    invoke-direct {v2}, LX/JFI;-><init>()V

    sget-object v3, LX/JF3;->a:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/JF3;->c:LX/2vs;

    new-instance v0, LX/2vs;

    const-string v1, "Places.PLACE_DETECTION_API"

    new-instance v2, LX/JFU;

    invoke-direct {v2}, LX/JFU;-><init>()V

    sget-object v3, LX/JF3;->b:LX/2vn;

    invoke-direct {v0, v1, v2, v3}, LX/2vs;-><init>(Ljava/lang/String;LX/2vq;LX/2vn;)V

    sput-object v0, LX/JF3;->d:LX/2vs;

    new-instance v0, LX/JFH;

    invoke-direct {v0}, LX/JFH;-><init>()V

    sput-object v0, LX/JF3;->e:LX/JEt;

    new-instance v0, LX/JFT;

    invoke-direct {v0}, LX/JFT;-><init>()V

    sput-object v0, LX/JF3;->f:LX/JEw;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
