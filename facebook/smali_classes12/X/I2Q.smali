.class public LX/I2Q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/I2Q;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530293
    iput-object p1, p0, LX/I2Q;->a:LX/0Or;

    .line 2530294
    iput-object p2, p0, LX/I2Q;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2530295
    return-void
.end method

.method public static a(LX/0QB;)LX/I2Q;
    .locals 5

    .prologue
    .line 2530296
    sget-object v0, LX/I2Q;->d:LX/I2Q;

    if-nez v0, :cond_1

    .line 2530297
    const-class v1, LX/I2Q;

    monitor-enter v1

    .line 2530298
    :try_start_0
    sget-object v0, LX/I2Q;->d:LX/I2Q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2530299
    if-eqz v2, :cond_0

    .line 2530300
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2530301
    new-instance v4, LX/I2Q;

    const/16 v3, 0xc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v4, p0, v3}, LX/I2Q;-><init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 2530302
    move-object v0, v4

    .line 2530303
    sput-object v0, LX/I2Q;->d:LX/I2Q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530304
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2530305
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2530306
    :cond_1
    sget-object v0, LX/I2Q;->d:LX/I2Q;

    return-object v0

    .line 2530307
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2530308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
