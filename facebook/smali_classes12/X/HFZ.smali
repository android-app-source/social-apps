.class public final LX/HFZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "LX/HEu;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HFc;


# direct methods
.method public constructor <init>(LX/HFc;)V
    .locals 0

    .prologue
    .line 2443893
    iput-object p1, p0, LX/HFZ;->a:LX/HFc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2443894
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2443895
    if-eqz p1, :cond_0

    .line 2443896
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443897
    if-eqz v0, :cond_0

    .line 2443898
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443899
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;->a()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2443900
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443901
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443902
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2443903
    :goto_0
    return-object v0

    .line 2443904
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2443905
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2443906
    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;->a()LX/2uF;

    move-result-object v0

    .line 2443907
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2443908
    if-eqz v2, :cond_2

    .line 2443909
    new-instance v4, LX/HEu;

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, LX/HEu;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443910
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2443911
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
