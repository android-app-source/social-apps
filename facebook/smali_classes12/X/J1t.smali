.class public final LX/J1t;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/payments/receipt/graphql/ReceiptQueriesInterfaces$FBPaymentsReceiptView;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/receipt/components/ReceiptComponentController;)V
    .locals 0

    .prologue
    .line 2639480
    iput-object p1, p0, LX/J1t;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2639481
    iget-object v0, p0, LX/J1t;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a(Ljava/lang/Throwable;)V

    .line 2639482
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2639483
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2639484
    if-eqz p1, :cond_0

    .line 2639485
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2639486
    if-nez v0, :cond_1

    .line 2639487
    :cond_0
    iget-object v0, p0, LX/J1t;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->a(Ljava/lang/Throwable;)V

    .line 2639488
    :goto_0
    return-void

    .line 2639489
    :cond_1
    iget-object v1, p0, LX/J1t;->a:Lcom/facebook/payments/receipt/components/ReceiptComponentController;

    .line 2639490
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2639491
    check-cast v0, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;

    .line 2639492
    iget-object v2, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    if-eqz v2, :cond_9

    .line 2639493
    iget-object v2, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    invoke-interface {v2}, LX/J1v;->b()V

    .line 2639494
    iget-object v2, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->i:LX/J1v;

    iget-object v3, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->e:LX/J22;

    iget-object v4, v1, Lcom/facebook/payments/receipt/components/ReceiptComponentController;->k:Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    .line 2639495
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2639496
    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->d()LX/0Px;

    move-result-object v8

    .line 2639497
    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->c()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;

    move-result-object v5

    .line 2639498
    if-nez v5, :cond_a

    .line 2639499
    :goto_1
    const/4 v5, 0x0

    move v6, v5

    :goto_2
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_8

    .line 2639500
    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;

    .line 2639501
    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;->b()LX/0Px;

    move-result-object p0

    .line 2639502
    const/4 v9, 0x0

    move v10, v9

    :goto_3
    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_4

    .line 2639503
    if-eqz v10, :cond_2

    .line 2639504
    sget-object v9, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    invoke-static {v9}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639505
    :cond_2
    invoke-virtual {p0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;

    .line 2639506
    new-instance p1, LX/J2q;

    invoke-direct {p1}, LX/J2q;-><init>()V

    move-object p1, p1

    .line 2639507
    invoke-virtual {v9}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2639508
    iput-object v1, p1, LX/J2q;->a:Ljava/lang/String;

    .line 2639509
    move-object p1, p1

    .line 2639510
    invoke-virtual {v9}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2639511
    iput-object v1, p1, LX/J2q;->b:Ljava/lang/String;

    .line 2639512
    move-object p1, p1

    .line 2639513
    invoke-virtual {v9}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2639514
    invoke-virtual {v9}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ComponentsModel;->a()Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;

    move-result-object v9

    invoke-static {v3, v4, v9, p1}, LX/J22;->a(LX/J22;Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$ExtensionModel;LX/J2q;)V

    .line 2639515
    :cond_3
    new-instance v9, LX/J2r;

    invoke-direct {v9, p1}, LX/J2r;-><init>(LX/J2q;)V

    move-object v9, v9

    .line 2639516
    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639517
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_3

    .line 2639518
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;->a()LX/3Ab;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 2639519
    sget-object v9, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    invoke-static {v9}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v9

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639520
    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$SectionsModel;->a()LX/3Ab;

    move-result-object v5

    invoke-static {v5, v7}, LX/J22;->a(LX/3Ab;LX/0Pz;)V

    .line 2639521
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v6, v5, :cond_5

    .line 2639522
    sget-object v5, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    invoke-static {v5}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639523
    :cond_5
    :goto_4
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto/16 :goto_2

    .line 2639524
    :cond_6
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-eq v6, v5, :cond_7

    .line 2639525
    sget-object v5, LX/J2w;->SPACED_DOUBLE_DIVIDER:LX/J2w;

    invoke-static {v5}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2639526
    :cond_7
    sget-object v5, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    invoke-static {v5}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2639527
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel;->b()LX/3Ab;

    move-result-object v5

    invoke-static {v5, v7}, LX/J22;->a(LX/3Ab;LX/0Pz;)V

    .line 2639528
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v3, v5

    .line 2639529
    invoke-interface {v2, v3}, LX/J1v;->setData(LX/0Px;)V

    .line 2639530
    :cond_9
    goto/16 :goto_0

    .line 2639531
    :cond_a
    new-instance v6, LX/J2j;

    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lcom/facebook/payments/receipt/graphql/ReceiptQueriesModels$FBPaymentsReceiptViewModel$OtherParticipantModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v6, v9, v10, p0}, LX/J2j;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2639532
    sget-object v6, LX/J2w;->SPACED_DOUBLE_DIVIDER:LX/J2w;

    invoke-static {v6}, LX/J22;->a(LX/J2w;)LX/J20;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1
.end method
