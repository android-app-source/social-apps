.class public final LX/HPD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V
    .locals 0

    .prologue
    .line 2461712
    iput-object p1, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2461713
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461714
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPageMetaData"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2461715
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2461716
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2461717
    if-eqz p1, :cond_0

    .line 2461718
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2461719
    if-nez v0, :cond_1

    .line 2461720
    :cond_0
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461721
    :goto_0
    return-void

    .line 2461722
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;

    .line 2461723
    iget-object v1, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v1, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;)V

    .line 2461724
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2461725
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->s:LX/3iH;

    iget-object v1, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2461726
    iget-object v1, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->v:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pages_photos_reaction_fetch_viewer_context"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/HPC;

    invoke-direct {v3, p0}, LX/HPC;-><init>(LX/HPD;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2461727
    :cond_2
    iget-object v0, p0, LX/HPD;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->r(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    goto :goto_0
.end method
