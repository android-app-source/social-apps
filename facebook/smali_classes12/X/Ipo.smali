.class public LX/Ipo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final paymentMethodId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x1

    .line 2614555
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentMethodPrimary"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipo;->b:LX/1sv;

    .line 2614556
    new-instance v0, LX/1sw;

    const-string v1, "paymentMethodId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipo;->c:LX/1sw;

    .line 2614557
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipo;->d:LX/1sw;

    .line 2614558
    sput-boolean v3, LX/Ipo;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2614551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614552
    iput-object p1, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    .line 2614553
    iput-object p2, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    .line 2614554
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2614519
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2614520
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 2614521
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 2614522
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPaymentMethodPrimary"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614523
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614524
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614525
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614526
    const/4 v1, 0x1

    .line 2614527
    iget-object v5, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 2614528
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614529
    const-string v1, "paymentMethodId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614530
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614531
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614532
    iget-object v1, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-nez v1, :cond_6

    .line 2614533
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614534
    :goto_3
    const/4 v1, 0x0

    .line 2614535
    :cond_0
    iget-object v5, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 2614536
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614537
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614538
    const-string v1, "irisSeqId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614539
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614540
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614541
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 2614542
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614543
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614544
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614545
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614546
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2614547
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2614548
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 2614549
    :cond_6
    iget-object v1, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2614550
    :cond_7
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2614507
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2614508
    iget-object v0, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2614509
    iget-object v0, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2614510
    sget-object v0, LX/Ipo;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614511
    iget-object v0, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614512
    :cond_0
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614513
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614514
    sget-object v0, LX/Ipo;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614515
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614516
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2614517
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2614518
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2614481
    if-nez p1, :cond_1

    .line 2614482
    :cond_0
    :goto_0
    return v0

    .line 2614483
    :cond_1
    instance-of v1, p1, LX/Ipo;

    if-eqz v1, :cond_0

    .line 2614484
    check-cast p1, LX/Ipo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2614485
    if-nez p1, :cond_3

    .line 2614486
    :cond_2
    :goto_1
    move v0, v2

    .line 2614487
    goto :goto_0

    .line 2614488
    :cond_3
    iget-object v0, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2614489
    :goto_2
    iget-object v3, p1, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2614490
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2614491
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614492
    iget-object v0, p0, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipo;->paymentMethodId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614493
    :cond_5
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2614494
    :goto_4
    iget-object v3, p1, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2614495
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2614496
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614497
    iget-object v0, p0, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipo;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2614498
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2614499
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2614500
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2614501
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2614502
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2614503
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2614504
    sget-boolean v0, LX/Ipo;->a:Z

    .line 2614505
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipo;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2614506
    return-object v0
.end method
