.class public LX/IVR;
.super LX/3mU;
.source ""


# instance fields
.field private final a:LX/AjG;

.field public final b:LX/0bH;

.field private final c:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(LX/AjG;LX/0bH;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581919
    invoke-direct {p0}, LX/3mU;-><init>()V

    .line 2581920
    iput-object p1, p0, LX/IVR;->a:LX/AjG;

    .line 2581921
    iput-object p2, p0, LX/IVR;->b:LX/0bH;

    .line 2581922
    iput-object p3, p0, LX/IVR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2581923
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2581924
    iget-object v0, p0, LX/IVR;->a:LX/AjG;

    iget-object v1, p0, LX/IVR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x4

    new-instance v3, LX/IVQ;

    invoke-direct {v3, p0}, LX/IVQ;-><init>(LX/IVR;)V

    invoke-virtual {v0, v1, v2, v3}, LX/AjG;->a(Lcom/facebook/graphql/model/GraphQLStory;ILX/0TF;)V

    .line 2581925
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2581926
    iget-object v0, p0, LX/IVR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVR;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->l()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
