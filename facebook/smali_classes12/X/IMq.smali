.class public final LX/IMq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/search/CommunitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V
    .locals 0

    .prologue
    .line 2570236
    iput-object p1, p0, LX/IMq;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x18f4ca1a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2570237
    iget-object v1, p0, LX/IMq;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    if-nez v1, :cond_0

    .line 2570238
    const v1, -0xdccefcb

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2570239
    :goto_0
    return-void

    .line 2570240
    :cond_0
    check-cast p1, Landroid/widget/TextView;

    .line 2570241
    iget-object v1, p0, LX/IMq;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2570242
    const v1, 0x4533ba0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
