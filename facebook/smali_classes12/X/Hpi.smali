.class public final LX/Hpi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hpj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:LX/5KI;

.field public final synthetic c:LX/Hpj;


# direct methods
.method public constructor <init>(LX/Hpj;)V
    .locals 1

    .prologue
    .line 2509780
    iput-object p1, p0, LX/Hpi;->c:LX/Hpj;

    .line 2509781
    move-object v0, p1

    .line 2509782
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2509783
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2509765
    const-string v0, "CSCollection"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2509766
    if-ne p0, p1, :cond_1

    .line 2509767
    :cond_0
    :goto_0
    return v0

    .line 2509768
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2509769
    goto :goto_0

    .line 2509770
    :cond_3
    check-cast p1, LX/Hpi;

    .line 2509771
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2509772
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2509773
    if-eq v2, v3, :cond_0

    .line 2509774
    iget-object v2, p0, LX/Hpi;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hpi;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/Hpi;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2509775
    goto :goto_0

    .line 2509776
    :cond_5
    iget-object v2, p1, LX/Hpi;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 2509777
    :cond_6
    iget-object v2, p0, LX/Hpi;->b:LX/5KI;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Hpi;->b:LX/5KI;

    iget-object v3, p1, LX/Hpi;->b:LX/5KI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2509778
    goto :goto_0

    .line 2509779
    :cond_7
    iget-object v2, p1, LX/Hpi;->b:LX/5KI;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
