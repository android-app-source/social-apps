.class public final LX/J9g;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V
    .locals 0

    .prologue
    .line 2653160
    iput-object p1, p0, LX/J9g;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2653161
    iget-object v0, p0, LX/J9g;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Ljava/lang/Throwable;)V

    .line 2653162
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2653163
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2653164
    iget-object v0, p0, LX/J9g;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    .line 2653165
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    invoke-static {p0}, LX/0PB;->checkState(Z)V

    .line 2653166
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {p0, p1}, LX/J9W;->a(LX/JBL;)V

    .line 2653167
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    invoke-virtual {p0}, LX/J8y;->a()V

    .line 2653168
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->k(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    .line 2653169
    return-void

    .line 2653170
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
