.class public final LX/JUT;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

.field public d:LX/JUq;

.field public e:Ljava/lang/String;

.field public f:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public final synthetic i:LX/JUU;


# direct methods
.method public constructor <init>(LX/JUU;)V
    .locals 1

    .prologue
    .line 2699092
    iput-object p1, p0, LX/JUT;->i:LX/JUU;

    .line 2699093
    move-object v0, p1

    .line 2699094
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699095
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699096
    const-string v0, "PageContextualRecommendationsFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699097
    if-ne p0, p1, :cond_1

    .line 2699098
    :cond_0
    :goto_0
    return v0

    .line 2699099
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699100
    goto :goto_0

    .line 2699101
    :cond_3
    check-cast p1, LX/JUT;

    .line 2699102
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699103
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699104
    if-eq v2, v3, :cond_0

    .line 2699105
    iget-object v2, p0, LX/JUT;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUT;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JUT;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2699106
    goto :goto_0

    .line 2699107
    :cond_5
    iget-object v2, p1, LX/JUT;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2699108
    :cond_6
    iget-object v2, p0, LX/JUT;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JUT;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JUT;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2699109
    goto :goto_0

    .line 2699110
    :cond_8
    iget-object v2, p1, LX/JUT;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2699111
    :cond_9
    iget-object v2, p0, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v3, p1, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2699112
    goto :goto_0

    .line 2699113
    :cond_b
    iget-object v2, p1, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-nez v2, :cond_a

    .line 2699114
    :cond_c
    iget-object v2, p0, LX/JUT;->d:LX/JUq;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JUT;->d:LX/JUq;

    iget-object v3, p1, LX/JUT;->d:LX/JUq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2699115
    goto :goto_0

    .line 2699116
    :cond_e
    iget-object v2, p1, LX/JUT;->d:LX/JUq;

    if-nez v2, :cond_d

    .line 2699117
    :cond_f
    iget-object v2, p0, LX/JUT;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/JUT;->e:Ljava/lang/String;

    iget-object v3, p1, LX/JUT;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2699118
    goto :goto_0

    .line 2699119
    :cond_11
    iget-object v2, p1, LX/JUT;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2699120
    :cond_12
    iget-object v2, p0, LX/JUT;->f:LX/1Pr;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/JUT;->f:LX/1Pr;

    iget-object v3, p1, LX/JUT;->f:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2699121
    goto/16 :goto_0

    .line 2699122
    :cond_14
    iget-object v2, p1, LX/JUT;->f:LX/1Pr;

    if-nez v2, :cond_13

    .line 2699123
    :cond_15
    iget-object v2, p0, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 2699124
    goto/16 :goto_0

    .line 2699125
    :cond_17
    iget-object v2, p1, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_16

    .line 2699126
    :cond_18
    iget-object v2, p0, LX/JUT;->h:Ljava/lang/String;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/JUT;->h:Ljava/lang/String;

    iget-object v3, p1, LX/JUT;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699127
    goto/16 :goto_0

    .line 2699128
    :cond_19
    iget-object v2, p1, LX/JUT;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
