.class public final LX/IRy;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/INf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;LX/DML;)V
    .locals 0

    .prologue
    .line 2577371
    iput-object p1, p0, LX/IRy;->a:LX/ISI;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2577372
    check-cast p1, LX/INf;

    .line 2577373
    iget-object v0, p0, LX/IRy;->a:LX/ISI;

    iget-object v0, v0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/IRy;->a:LX/ISI;

    sget-object v2, LX/5QS;->VIEWER_CHILD_GROUPS:LX/5QS;

    iget-object v3, p0, LX/IRy;->a:LX/ISI;

    iget-object v3, v3, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1, v2, v3}, LX/ISI;->a$redex0(LX/ISI;LX/5QS;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 2577374
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2577375
    :cond_0
    :goto_0
    return-void

    .line 2577376
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    move-result-object v2

    .line 2577377
    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2577378
    iget-object v3, p1, LX/INf;->c:Lcom/facebook/fig/footer/FigFooter;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Lcom/facebook/fig/footer/FigFooter;->setVisibility(I)V

    .line 2577379
    iget-object v3, p1, LX/INf;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577380
    :cond_2
    iget-object v3, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 2577381
    iput-object v2, v3, LX/IL3;->a:LX/0Px;

    .line 2577382
    iget-object v2, p1, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2577383
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v2, v3

    .line 2577384
    if-nez v2, :cond_3

    .line 2577385
    iget-object v2, p1, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2577386
    :cond_3
    iget-object v2, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method
