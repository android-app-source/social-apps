.class public final LX/IAj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

.field public final synthetic b:LX/IAk;


# direct methods
.method public constructor <init>(LX/IAk;Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V
    .locals 0

    .prologue
    .line 2546046
    iput-object p1, p0, LX/IAj;->b:LX/IAk;

    iput-object p2, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2546078
    iget-object v0, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b()V

    .line 2546079
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2546047
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2546048
    if-nez p1, :cond_0

    .line 2546049
    iget-object v0, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->b()V

    .line 2546050
    :goto_0
    return-void

    .line 2546051
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2546052
    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;

    .line 2546053
    if-nez v0, :cond_1

    .line 2546054
    iget-object v0, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a()V

    goto :goto_0

    .line 2546055
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2546056
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2546057
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2546058
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2546059
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v3

    if-nez v3, :cond_4

    .line 2546060
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2546061
    :goto_3
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-nez v4, :cond_5

    .line 2546062
    iget-object v0, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->a()V

    goto :goto_0

    .line 2546063
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->j()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsGoingModel;->a()LX/0Px;

    move-result-object v1

    goto :goto_1

    .line 2546064
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->k()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInterestedModel;->a()LX/0Px;

    move-result-object v2

    goto :goto_2

    .line 2546065
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel;->l()Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$EventFriendsQueryModel$FriendsInvitedModel;->a()LX/0Px;

    move-result-object v3

    goto :goto_3

    .line 2546066
    :cond_5
    iget-object v4, p0, LX/IAj;->a:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    .line 2546067
    iget-object v5, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->i:Landroid/widget/ProgressBar;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2546068
    iget-object v5, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2546069
    iput-object v0, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->h:LX/7nK;

    .line 2546070
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2546071
    new-instance v6, LX/IAh;

    new-instance v7, LX/IAg;

    sget-object v8, LX/Blc;->PUBLIC_GOING:LX/Blc;

    invoke-direct {v7, v5, v1, v8}, LX/IAg;-><init>(Landroid/content/Context;LX/0Px;LX/Blc;)V

    new-instance v8, LX/IAg;

    sget-object p0, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    invoke-direct {v8, v5, v2, p0}, LX/IAg;-><init>(Landroid/content/Context;LX/0Px;LX/Blc;)V

    new-instance p0, LX/IAg;

    sget-object p1, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    invoke-direct {p0, v5, v3, p1}, LX/IAg;-><init>(Landroid/content/Context;LX/0Px;LX/Blc;)V

    invoke-static {v7, v8, p0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-direct {v6, v7}, LX/IAh;-><init>(LX/0Px;)V

    iput-object v6, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->l:LX/IAh;

    .line 2546072
    iget-object v6, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->l:LX/IAh;

    .line 2546073
    iput-object v4, v6, LX/IAh;->b:Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;

    .line 2546074
    iget-object v6, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v7, LX/1P1;

    invoke-direct {v7, v5}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2546075
    iget-object v5, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v6, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->l:LX/IAh;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2546076
    iget-object v5, v4, Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v6, LX/IAm;

    invoke-direct {v6, v4}, LX/IAm;-><init>(Lcom/facebook/events/permalink/messagefriends/EventMessageFriendsFragment;)V

    invoke-virtual {v5, v6}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2546077
    goto/16 :goto_0
.end method
