.class public LX/I8s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1nQ;

.field private final b:LX/I7W;

.field public final c:LX/1nP;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Lcom/facebook/events/model/Event;

.field private f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field private g:Z


# direct methods
.method public constructor <init>(LX/1nQ;LX/I7W;LX/1nP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542101
    iput-object p1, p0, LX/I8s;->a:LX/1nQ;

    .line 2542102
    iput-object p2, p0, LX/I8s;->b:LX/I7W;

    .line 2542103
    iput-object p3, p0, LX/I8s;->c:LX/1nP;

    .line 2542104
    return-void
.end method

.method public static b(LX/0QB;)LX/I8s;
    .locals 4

    .prologue
    .line 2542098
    new-instance v3, LX/I8s;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v0

    check-cast v0, LX/1nQ;

    invoke-static {p0}, LX/I7W;->a(LX/0QB;)LX/I7W;

    move-result-object v1

    check-cast v1, LX/I7W;

    invoke-static {p0}, LX/1nP;->a(LX/0QB;)LX/1nP;

    move-result-object v2

    check-cast v2, LX/1nP;

    invoke-direct {v3, v0, v1, v2}, LX/I8s;-><init>(LX/1nQ;LX/I7W;LX/1nP;)V

    .line 2542099
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Landroid/widget/TextView;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 2

    .prologue
    .line 2542079
    iput-object p1, p0, LX/I8s;->e:Lcom/facebook/events/model/Event;

    .line 2542080
    iput-object p2, p0, LX/I8s;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2542081
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->bb()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    .line 2542082
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2542083
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2542084
    invoke-virtual {p3}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081ee2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542085
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542086
    :goto_0
    return-void

    .line 2542087
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2542088
    invoke-virtual {p3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081edf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2542089
    iget-object v0, p0, LX/I8s;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 2542090
    iget-object v0, p0, LX/I8s;->d:Landroid/view/View$OnClickListener;

    .line 2542091
    :goto_1
    move-object v0, v0

    .line 2542092
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542093
    iget-object v0, p0, LX/I8s;->e:Lcom/facebook/events/model/Event;

    .line 2542094
    iget-object v1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2542095
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_CTA:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {p0, v0, v1}, LX/I8s;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/I8s;->c:LX/1nP;

    iget-object v1, p0, LX/I8s;->e:Lcom/facebook/events/model/Event;

    .line 2542096
    iget-object p1, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, p1

    .line 2542097
    sget-object p1, Lcom/facebook/events/common/ActionMechanism;->BUY_TICKETS_CTA:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, p1, p4}, LX/1nP;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, LX/I8s;->d:Landroid/view/View$OnClickListener;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 3

    .prologue
    .line 2542072
    iget-boolean v0, p0, LX/I8s;->g:Z

    if-nez v0, :cond_0

    .line 2542073
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I8s;->g:Z

    .line 2542074
    iget-object v0, p0, LX/I8s;->a:LX/1nQ;

    .line 2542075
    iget-object v1, v0, LX/1nQ;->i:LX/0Zb;

    const-string v2, "event_buy_tickets_button_impression"

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2542076
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2542077
    const-string v2, "event_permalink"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    iget-object v2, v0, LX/1nQ;->j:LX/0kv;

    iget-object p0, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v2, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "Event"

    invoke-virtual {v1, v2}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "mechanism"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v1

    const-string v2, "event_id"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2542078
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 7
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2542067
    invoke-virtual {p0, p1, p2}, LX/I8s;->b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    .line 2542068
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel$NodesModel;->c()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    .line 2542069
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2542070
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2542071
    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2542066
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/I8s;->b:LX/I7W;

    invoke-virtual {v0, p1}, LX/I7W;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aj()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierPermalinkFragmentModel$TicketTiersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
