.class public LX/HiO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2N8;

.field public final b:Ljava/util/Locale;


# direct methods
.method public constructor <init>(LX/2N8;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497322
    iput-object p1, p0, LX/HiO;->a:LX/2N8;

    .line 2497323
    iput-object p2, p0, LX/HiO;->b:Ljava/util/Locale;

    .line 2497324
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/location/Address;I)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2497325
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v2

    .line 2497326
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2497327
    const-string v7, "title"

    const/4 v8, 0x0

    invoke-virtual {p2, v8}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2497328
    const-string v7, "subtitle"

    invoke-virtual {p2}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2497329
    const-string v7, "latitude"

    invoke-virtual {p2}, Landroid/location/Address;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2497330
    const-string v7, "longitude"

    invoke-virtual {p2}, Landroid/location/Address;->getLongitude()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2497331
    move-object v3, v6

    .line 2497332
    invoke-virtual {v2, v3}, LX/162;->a(LX/0lF;)LX/162;

    .line 2497333
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2497334
    const/4 v1, 0x0

    .line 2497335
    const/4 v0, 0x1

    .line 2497336
    :try_start_0
    iget-object v4, p0, LX/HiO;->a:LX/2N8;

    invoke-virtual {v4, p1}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/462; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2497337
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2497338
    invoke-virtual {v1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2497339
    const-string v5, "title"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v3, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    move v5, v5

    .line 2497340
    if-nez v5, :cond_0

    .line 2497341
    add-int/lit8 v1, v1, 0x1

    .line 2497342
    if-gt v1, p3, :cond_1

    .line 2497343
    invoke-virtual {v2, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_1

    .line 2497344
    :cond_1
    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_0
.end method
