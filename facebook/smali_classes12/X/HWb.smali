.class public final LX/HWb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 0

    .prologue
    .line 2476833
    iput-object p1, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iput-object p2, p0, LX/HWb;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2476834
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->x:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2476835
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    new-instance v1, LX/CXw;

    iget-object v2, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    iget-object v3, p0, LX/HWb;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {v1, v2, v3, v4, v4}, LX/CXw;-><init>(Landroid/os/ParcelUuid;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2476836
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fail to get additional tab data for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HWb;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2476837
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2476838
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2476839
    if-eqz p1, :cond_1

    .line 2476840
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476841
    if-eqz v0, :cond_1

    .line 2476842
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476843
    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476844
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476845
    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476846
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    const/4 v1, 0x1

    .line 2476847
    iput-boolean v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->v:Z

    .line 2476848
    iget-object v1, p0, LX/HWb;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2476849
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476850
    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-static {v1, v0}, LX/HNE;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476851
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    .line 2476852
    iget-boolean v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->v:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->C:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-nez v1, :cond_2

    const-string v1, "admin_publish_services"

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "admin_publish_services_edit_flow"

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2476853
    if-eqz v1, :cond_1

    .line 2476854
    const v1, 0x7f0d2458

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2476855
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->C:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    .line 2476856
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    if-nez v1, :cond_0

    .line 2476857
    new-instance v1, LX/HWe;

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    invoke-direct {v1, v0, v2}, LX/HWe;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V

    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    .line 2476858
    :cond_0
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    move-object v1, v1

    .line 2476859
    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    .line 2476860
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2476861
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    if-eqz v1, :cond_1

    .line 2476862
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    invoke-virtual {v1}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->d()V

    .line 2476863
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    invoke-virtual {v1}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->invalidate()V

    .line 2476864
    :cond_1
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->x:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2476865
    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    new-instance v2, LX/CXw;

    iget-object v0, p0, LX/HWb;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    iget-object v4, p0, LX/HWb;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2476866
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2476867
    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    .line 2476868
    iget-object v5, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v5, v5

    .line 2476869
    invoke-direct {v2, v3, v4, v0, v5}, LX/CXw;-><init>(Landroid/os/ParcelUuid;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2476870
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
