.class public LX/J7J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650663
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2650664
    check-cast p1, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;

    .line 2650665
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2650666
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "field_types"

    iget-object v3, p1, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->b:Ljava/util/List;

    .line 2650667
    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "[\""

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string p0, "\",\""

    invoke-static {p0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object p0

    invoke-virtual {p0}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, "\"]"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2650668
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650669
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    iget-object v3, p1, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650670
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "location"

    const-string v3, "ANDROID_ABOUT_PAGE"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650671
    const-string v1, "%s/info_requests"

    iget-object v2, p1, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2650672
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "ask_info_request"

    .line 2650673
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2650674
    move-object v2, v2

    .line 2650675
    const-string v3, "POST"

    .line 2650676
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2650677
    move-object v2, v2

    .line 2650678
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 2650679
    move-object v1, v2

    .line 2650680
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2650681
    move-object v0, v1

    .line 2650682
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2650683
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2650684
    move-object v0, v0

    .line 2650685
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2650686
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2650687
    const/4 v0, 0x0

    return-object v0
.end method
