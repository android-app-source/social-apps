.class public final LX/Hbc;
.super LX/25X;
.source ""


# instance fields
.field public final synthetic a:LX/Hbd;

.field private final g:F

.field private final h:F

.field private final i:I


# direct methods
.method public constructor <init>(LX/Hbd;Landroid/content/Context;III)V
    .locals 1

    .prologue
    .line 2485929
    iput-object p1, p0, LX/Hbc;->a:LX/Hbd;

    .line 2485930
    invoke-direct {p0, p2}, LX/25X;-><init>(Landroid/content/Context;)V

    .line 2485931
    int-to-float v0, p3

    iput v0, p0, LX/Hbc;->g:F

    .line 2485932
    int-to-float v0, p4

    iput v0, p0, LX/Hbc;->h:F

    .line 2485933
    iput p5, p0, LX/Hbc;->i:I

    .line 2485934
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 2485935
    iget-object v0, p0, LX/Hbc;->a:LX/Hbd;

    invoke-virtual {v0, p1}, LX/1P1;->d(I)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 2485936
    int-to-float v0, p1

    iget v1, p0, LX/Hbc;->g:F

    div-float/2addr v0, v1

    .line 2485937
    iget v1, p0, LX/Hbc;->h:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2485938
    iget v0, p0, LX/Hbc;->i:I

    return v0
.end method
