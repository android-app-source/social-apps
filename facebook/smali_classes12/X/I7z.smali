.class public LX/I7z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/model/Event;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:LX/2fj;

.field private e:LX/1Ck;

.field public f:LX/Bl6;


# direct methods
.method public constructor <init>(LX/2fj;LX/1Ck;LX/Bl6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540424
    iput-object p1, p0, LX/I7z;->d:LX/2fj;

    .line 2540425
    iput-object p2, p0, LX/I7z;->e:LX/1Ck;

    .line 2540426
    iput-object p3, p0, LX/I7z;->f:LX/Bl6;

    .line 2540427
    return-void
.end method

.method public static a(LX/0QB;)LX/I7z;
    .locals 4

    .prologue
    .line 2540428
    new-instance v3, LX/I7z;

    invoke-static {p0}, LX/2fj;->b(LX/0QB;)LX/2fj;

    move-result-object v0

    check-cast v0, LX/2fj;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v2

    check-cast v2, LX/Bl6;

    invoke-direct {v3, v0, v1, v2}, LX/I7z;-><init>(LX/2fj;LX/1Ck;LX/Bl6;)V

    .line 2540429
    move-object v0, v3

    .line 2540430
    return-object v0
.end method

.method public static a(LX/I7z;LX/5vL;)V
    .locals 5

    .prologue
    .line 2540431
    iget-object v0, p0, LX/I7z;->a:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7z;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I7z;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2540432
    :cond_0
    :goto_0
    return-void

    .line 2540433
    :cond_1
    new-instance v0, LX/5vM;

    invoke-direct {v0}, LX/5vM;-><init>()V

    iget-object v1, p0, LX/I7z;->b:Ljava/lang/String;

    .line 2540434
    iput-object v1, v0, LX/5vM;->a:Ljava/lang/String;

    .line 2540435
    move-object v0, v0

    .line 2540436
    iget-object v1, p0, LX/I7z;->a:Lcom/facebook/events/model/Event;

    .line 2540437
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540438
    iput-object v1, v0, LX/5vM;->b:Ljava/lang/String;

    .line 2540439
    move-object v0, v0

    .line 2540440
    iput-object p1, v0, LX/5vM;->c:LX/5vL;

    .line 2540441
    move-object v0, v0

    .line 2540442
    const-string v1, "native_event"

    invoke-virtual {v0, v1}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    const-string v1, "toggle_button"

    invoke-virtual {v0, v1}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    iget-object v1, p0, LX/I7z;->c:Ljava/lang/String;

    .line 2540443
    iput-object v1, v0, LX/5vM;->g:Ljava/lang/String;

    .line 2540444
    move-object v0, v0

    .line 2540445
    const/4 v1, 0x1

    .line 2540446
    iput-boolean v1, v0, LX/5vM;->j:Z

    .line 2540447
    move-object v0, v0

    .line 2540448
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 2540449
    iget-object v1, p0, LX/I7z;->d:LX/2fj;

    invoke-virtual {v1, v0}, LX/2fj;->a(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2540450
    new-instance v2, LX/I7x;

    invoke-direct {v2, p0}, LX/I7x;-><init>(LX/I7z;)V

    .line 2540451
    const/4 v0, 0x0

    .line 2540452
    sget-object v3, LX/I7y;->a:[I

    invoke-virtual {p1}, LX/5vL;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2540453
    :goto_1
    iget-object v3, p0, LX/I7z;->e:LX/1Ck;

    invoke-virtual {v3, v0, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2540454
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "tasks-saveEvent:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/I7z;->a:Lcom/facebook/events/model/Event;

    .line 2540455
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2540456
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2540457
    iget-object v3, p0, LX/I7z;->f:LX/Bl6;

    new-instance v4, LX/BlG;

    invoke-direct {v4}, LX/BlG;-><init>()V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 2540458
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "tasks-removeEvent:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/I7z;->a:Lcom/facebook/events/model/Event;

    .line 2540459
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2540460
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2540461
    iget-object v3, p0, LX/I7z;->f:LX/Bl6;

    new-instance v4, LX/BlJ;

    invoke-direct {v4}, LX/BlJ;-><init>()V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
