.class public LX/J1k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5fv;


# direct methods
.method public constructor <init>(LX/5fv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639196
    iput-object p1, p0, LX/J1k;->a:LX/5fv;

    .line 2639197
    return-void
.end method

.method public static b(LX/0QB;)LX/J1k;
    .locals 2

    .prologue
    .line 2639198
    new-instance v1, LX/J1k;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    invoke-direct {v1, v0}, LX/J1k;-><init>(LX/5fv;)V

    .line 2639199
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)Z
    .locals 4

    .prologue
    .line 2639200
    iget-object v0, p0, LX/J1k;->a:LX/5fv;

    .line 2639201
    iget-object v1, p1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2639202
    iget-object v2, p1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2639203
    invoke-static {v1}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    .line 2639204
    const/4 p0, 0x0

    .line 2639205
    :try_start_0
    iget-object p1, v0, LX/5fv;->a:LX/0W9;

    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1, v3, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;Ljava/util/Currency;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object p1

    .line 2639206
    iget-object v1, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object p1, v1

    .line 2639207
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    if-lez p1, :cond_0

    const/4 p0, 0x1

    .line 2639208
    :cond_0
    :goto_0
    move v3, p0

    .line 2639209
    move v0, v3

    .line 2639210
    return v0

    :catch_0
    goto :goto_0
.end method
