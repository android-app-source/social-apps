.class public final LX/IE4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IDH;

.field public final synthetic b:LX/IE9;

.field public final synthetic c:LX/IE7;


# direct methods
.method public constructor <init>(LX/IE7;LX/IDH;LX/IE9;)V
    .locals 0

    .prologue
    .line 2551521
    iput-object p1, p0, LX/IE4;->c:LX/IE7;

    iput-object p2, p0, LX/IE4;->a:LX/IDH;

    iput-object p3, p0, LX/IE4;->b:LX/IE9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, 0xe34eaeb

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2551522
    iget-object v1, p0, LX/IE4;->c:LX/IE7;

    iget-object v1, v1, LX/IE7;->c:LX/IDs;

    iget-object v2, p0, LX/IE4;->c:LX/IE7;

    iget-object v2, v2, LX/IE7;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/IE4;->a:LX/IDH;

    invoke-interface {v4}, LX/2lr;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/IE4;->a:LX/IDH;

    invoke-interface {v6}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/IE4;->a:LX/IDH;

    invoke-interface {v7}, LX/83W;->g()LX/2h7;

    move-result-object v7

    iget-object v8, p0, LX/IE4;->a:LX/IDH;

    invoke-interface {v8}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    iget-object v9, p0, LX/IE4;->b:LX/IE9;

    .line 2551523
    iget-object p0, v9, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    move-object v9, p0

    .line 2551524
    sget-object v11, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v11, v8}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2551525
    invoke-static/range {v1 .. v8}, LX/IDs;->a(LX/IDs;JJLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/5OM;

    move-result-object v11

    .line 2551526
    invoke-virtual {v11, v9}, LX/0ht;->f(Landroid/view/View;)V

    .line 2551527
    :goto_0
    const v1, -0x4f179bb2

    invoke-static {v10, v10, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2551528
    :cond_0
    invoke-virtual {v1, v4, v5, v7, v8}, LX/2hX;->b(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_0
.end method
