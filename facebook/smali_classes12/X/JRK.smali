.class public final LX/JRK;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JRM;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JRL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JRM",
            "<TE;>.MessengerGenericPromotionHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JRM;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JRM;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2693177
    iput-object p1, p0, LX/JRK;->b:LX/JRM;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2693178
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "headerText"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JRK;->c:[Ljava/lang/String;

    .line 2693179
    iput v3, p0, LX/JRK;->d:I

    .line 2693180
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JRK;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JRK;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JRK;LX/1De;IILX/JRL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JRM",
            "<TE;>.MessengerGenericPromotionHeaderComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2693181
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2693182
    iput-object p4, p0, LX/JRK;->a:LX/JRL;

    .line 2693183
    iget-object v0, p0, LX/JRK;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2693184
    return-void
.end method


# virtual methods
.method public final a(LX/1Pk;)LX/JRK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/JRM",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2693185
    iget-object v0, p0, LX/JRK;->a:LX/JRL;

    iput-object p1, v0, LX/JRL;->b:LX/1Pk;

    .line 2693186
    iget-object v0, p0, LX/JRK;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693187
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)",
            "LX/JRM",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2693188
    iget-object v0, p0, LX/JRK;->a:LX/JRL;

    iput-object p1, v0, LX/JRL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693189
    iget-object v0, p0, LX/JRK;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693190
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2693191
    invoke-super {p0}, LX/1X5;->a()V

    .line 2693192
    const/4 v0, 0x0

    iput-object v0, p0, LX/JRK;->a:LX/JRL;

    .line 2693193
    iget-object v0, p0, LX/JRK;->b:LX/JRM;

    iget-object v0, v0, LX/JRM;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2693194
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/JRK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/JRM",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2693195
    iget-object v0, p0, LX/JRK;->a:LX/JRL;

    iput-object p1, v0, LX/JRL;->c:Ljava/lang/String;

    .line 2693196
    iget-object v0, p0, LX/JRK;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2693197
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JRM;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2693198
    iget-object v1, p0, LX/JRK;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JRK;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JRK;->d:I

    if-ge v1, v2, :cond_2

    .line 2693199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2693200
    :goto_0
    iget v2, p0, LX/JRK;->d:I

    if-ge v0, v2, :cond_1

    .line 2693201
    iget-object v2, p0, LX/JRK;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2693202
    iget-object v2, p0, LX/JRK;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2693203
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2693204
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2693205
    :cond_2
    iget-object v0, p0, LX/JRK;->a:LX/JRL;

    .line 2693206
    invoke-virtual {p0}, LX/JRK;->a()V

    .line 2693207
    return-object v0
.end method
