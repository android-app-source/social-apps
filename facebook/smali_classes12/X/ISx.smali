.class public LX/ISx;
.super LX/1Cv;
.source ""


# instance fields
.field private a:LX/11R;

.field private b:Landroid/content/res/Resources;

.field public c:LX/DTa;

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CGV;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/DTZ;

.field public g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

.field public h:LX/DSf;


# direct methods
.method private constructor <init>(LX/11R;Landroid/content/res/Resources;LX/DTa;LX/0Ot;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11R;",
            "Landroid/content/res/Resources;",
            "LX/DTa;",
            "LX/0Ot",
            "<",
            "LX/CGV;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2578625
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2578626
    iput-object p2, p0, LX/ISx;->b:Landroid/content/res/Resources;

    .line 2578627
    iput-object p1, p0, LX/ISx;->a:LX/11R;

    .line 2578628
    iput-object p3, p0, LX/ISx;->c:LX/DTa;

    .line 2578629
    iput-object p4, p0, LX/ISx;->d:LX/0Ot;

    .line 2578630
    iput-object p5, p0, LX/ISx;->e:LX/0Or;

    .line 2578631
    return-void
.end method

.method public static b(LX/0QB;)LX/ISx;
    .locals 6

    .prologue
    .line 2578538
    new-instance v0, LX/ISx;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11R;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const-class v3, LX/DTa;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DTa;

    const/16 v4, 0x238d

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/ISx;-><init>(LX/11R;Landroid/content/res/Resources;LX/DTa;LX/0Ot;LX/0Or;)V

    .line 2578539
    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2578617
    iget-object v5, p0, LX/ISx;->h:LX/DSf;

    if-eqz v5, :cond_0

    .line 2578618
    iget-object v5, p0, LX/ISx;->h:LX/DSf;

    .line 2578619
    iget-object v6, v5, LX/DSf;->e:LX/DS2;

    move-object v5, v6

    .line 2578620
    iget-wide v9, v5, LX/DS2;->c:J

    move-wide v5, v9

    .line 2578621
    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    .line 2578622
    :goto_0
    move-wide v0, v5

    .line 2578623
    iget-object v2, p0, LX/ISx;->a:LX/11R;

    sget-object v3, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    invoke-virtual {v2, v3, v0, v1}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2578624
    iget-object v1, p0, LX/ISx;->b:Landroid/content/res/Resources;

    const v2, 0x7f081b7c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v5, 0x0

    goto :goto_0
.end method

.method public static f(LX/ISx;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2578594
    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2578595
    :cond_0
    :goto_0
    return-void

    .line 2578596
    :cond_1
    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    .line 2578597
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2578598
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;

    .line 2578599
    sget-object v7, LX/DSe;->FRIEND:LX/DSe;

    move-object v8, v0

    .line 2578600
    :goto_1
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2578601
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2578602
    sget-object v5, LX/DSd;->NONE:LX/DSd;

    .line 2578603
    :goto_2
    sget-object v0, LX/ISv;->a:[I

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2578604
    sget-object v2, LX/DSb;->NOT_ADMIN:LX/DSb;

    .line 2578605
    :goto_3
    new-instance v0, LX/DSf;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v1

    sget-object v3, LX/DSc;->NOT_BLOCKED:LX/DSc;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v6

    if-nez v6, :cond_4

    :goto_4
    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel$NodeModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v6

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/DSf;-><init>(LX/DUV;LX/DSb;LX/DSc;LX/DS2;LX/DSd;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;LX/DSe;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesBioStoryFieldsModel$BioStoryModel;)V

    iput-object v0, p0, LX/ISx;->h:LX/DSf;

    goto/16 :goto_0

    .line 2578606
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2578607
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;

    .line 2578608
    sget-object v7, LX/DSe;->NON_FRIEND_MEMBER:LX/DSe;

    move-object v8, v0

    goto/16 :goto_1

    .line 2578609
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2578610
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;

    .line 2578611
    sget-object v7, LX/DSe;->INVITES:LX/DSe;

    move-object v8, v0

    goto/16 :goto_1

    .line 2578612
    :sswitch_0
    sget-object v5, LX/DSd;->EMAIL_INVITE:LX/DSd;

    goto/16 :goto_2

    .line 2578613
    :sswitch_1
    sget-object v5, LX/DSd;->ONSITE_INVITE:LX/DSd;

    goto/16 :goto_2

    .line 2578614
    :pswitch_0
    sget-object v2, LX/DSb;->ADMIN:LX/DSb;

    goto/16 :goto_3

    .line 2578615
    :pswitch_1
    sget-object v2, LX/DSb;->MODERATOR:LX/DSb;

    goto/16 :goto_3

    .line 2578616
    :cond_4
    new-instance v4, LX/DS2;

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel$EdgesModel;->j()J

    move-result-wide v10

    invoke-direct {v4, v6, v9, v10, v11}, LX/DS2;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_4

    :cond_5
    move-object v7, v4

    move-object v8, v4

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x58017a73 -> :sswitch_1
        0x7f489e48 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2578588
    sget-object v0, LX/ISw;->MEMBER_BIO_HEADER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2578589
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2578590
    :goto_0
    return-object v0

    .line 2578591
    :cond_0
    sget-object v0, LX/ISw;->MEMBER_BIO_COMPOSER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2578592
    new-instance v0, Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2578593
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2578583
    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    if-eqz v0, :cond_0

    .line 2578584
    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    .line 2578585
    iget-object p0, v0, LX/DSf;->d:LX/DUV;

    move-object v0, p0

    .line 2578586
    invoke-interface {v0}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v0

    .line 2578587
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2578554
    sget-object v0, LX/ISw;->MEMBER_BIO_HEADER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_3

    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    if-eqz v0, :cond_3

    .line 2578555
    check-cast p3, Lcom/facebook/fig/listitem/FigListItem;

    .line 2578556
    const/4 v0, 0x3

    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 2578557
    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    if-eqz v0, :cond_5

    .line 2578558
    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    .line 2578559
    iget-object v1, v0, LX/DSf;->d:LX/DUV;

    move-object v0, v1

    .line 2578560
    invoke-interface {v0}, LX/DUV;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2578561
    :goto_0
    move-object v0, v0

    .line 2578562
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2578563
    invoke-virtual {p0}, LX/ISx;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2578564
    invoke-direct {p0}, LX/ISx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2578565
    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2578566
    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    .line 2578567
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_2

    .line 2578568
    :cond_0
    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2578569
    :cond_1
    :goto_1
    return-void

    .line 2578570
    :cond_2
    invoke-virtual {p3, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2578571
    goto :goto_1

    .line 2578572
    :cond_3
    sget-object v0, LX/ISw;->MEMBER_BIO_COMPOSER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_1

    .line 2578573
    check-cast p3, Lcom/facebook/fig/footer/FigFooter;

    .line 2578574
    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2578575
    const v0, 0x7f082971

    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(I)V

    .line 2578576
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setTopDivider(Z)V

    .line 2578577
    const v0, 0x7f02080f

    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setActionDrawable(I)V

    .line 2578578
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setActionType(I)V

    .line 2578579
    iget-object v0, p0, LX/ISx;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CGV;

    iget-object v1, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->n()Ljava/lang/String;

    move-result-object v2

    const-string v3, "member_info_page"

    .line 2578580
    new-instance p0, LX/CGU;

    invoke-direct {p0, v0, v1, v3, v2}, LX/CGU;-><init>(LX/CGV;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    .line 2578581
    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2578582
    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Lcom/facebook/fig/footer/FigFooter;->setVisibility(I)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2578546
    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ISx;->h:LX/DSf;

    invoke-virtual {v0}, LX/DSf;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ISx;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2578547
    iget-object v1, p0, LX/ISx;->h:LX/DSf;

    if-eqz v1, :cond_2

    .line 2578548
    iget-object v1, p0, LX/ISx;->h:LX/DSf;

    .line 2578549
    iget-object p0, v1, LX/DSf;->d:LX/DUV;

    move-object v1, p0

    .line 2578550
    invoke-interface {v1}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v1

    .line 2578551
    :goto_0
    move-object v1, v1

    .line 2578552
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2578553
    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2578545
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2578544
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 2578540
    packed-switch p1, :pswitch_data_0

    .line 2578541
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GroupsMemberBioHeaderAdapter::getItemViewType Invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2578542
    :pswitch_0
    sget-object v0, LX/ISw;->MEMBER_BIO_HEADER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    .line 2578543
    :goto_0
    return v0

    :pswitch_1
    sget-object v0, LX/ISw;->MEMBER_BIO_COMPOSER:LX/ISw;

    invoke-virtual {v0}, LX/ISw;->ordinal()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
