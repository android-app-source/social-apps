.class public final enum LX/HyN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HyN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HyN;

.field public static final enum CREATE_VIEW:LX/HyN;

.field public static final enum DB_FETCH:LX/HyN;

.field public static final enum DB_UPDATE:LX/HyN;

.field public static final enum NETWORK_FETCH:LX/HyN;

.field public static final enum RENDERING:LX/HyN;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2524121
    new-instance v0, LX/HyN;

    const-string v1, "CREATE_VIEW"

    invoke-direct {v0, v1, v2}, LX/HyN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HyN;->CREATE_VIEW:LX/HyN;

    .line 2524122
    new-instance v0, LX/HyN;

    const-string v1, "NETWORK_FETCH"

    invoke-direct {v0, v1, v3}, LX/HyN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HyN;->NETWORK_FETCH:LX/HyN;

    .line 2524123
    new-instance v0, LX/HyN;

    const-string v1, "DB_UPDATE"

    invoke-direct {v0, v1, v4}, LX/HyN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HyN;->DB_UPDATE:LX/HyN;

    .line 2524124
    new-instance v0, LX/HyN;

    const-string v1, "DB_FETCH"

    invoke-direct {v0, v1, v5}, LX/HyN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HyN;->DB_FETCH:LX/HyN;

    .line 2524125
    new-instance v0, LX/HyN;

    const-string v1, "RENDERING"

    invoke-direct {v0, v1, v6}, LX/HyN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HyN;->RENDERING:LX/HyN;

    .line 2524126
    const/4 v0, 0x5

    new-array v0, v0, [LX/HyN;

    sget-object v1, LX/HyN;->CREATE_VIEW:LX/HyN;

    aput-object v1, v0, v2

    sget-object v1, LX/HyN;->NETWORK_FETCH:LX/HyN;

    aput-object v1, v0, v3

    sget-object v1, LX/HyN;->DB_UPDATE:LX/HyN;

    aput-object v1, v0, v4

    sget-object v1, LX/HyN;->DB_FETCH:LX/HyN;

    aput-object v1, v0, v5

    sget-object v1, LX/HyN;->RENDERING:LX/HyN;

    aput-object v1, v0, v6

    sput-object v0, LX/HyN;->$VALUES:[LX/HyN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2524118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HyN;
    .locals 1

    .prologue
    .line 2524119
    const-class v0, LX/HyN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HyN;

    return-object v0
.end method

.method public static values()[LX/HyN;
    .locals 1

    .prologue
    .line 2524120
    sget-object v0, LX/HyN;->$VALUES:[LX/HyN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HyN;

    return-object v0
.end method
