.class public LX/HaF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/HaD;

.field public final c:LX/HaE;

.field public final d:LX/Ha9;

.field public final e:LX/HaA;


# direct methods
.method public constructor <init>(LX/0Or;LX/HaD;LX/HaE;LX/Ha9;LX/HaA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/HaD;",
            "LX/HaE;",
            "LX/Ha9;",
            "LX/HaA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483629
    iput-object p1, p0, LX/HaF;->a:LX/0Or;

    .line 2483630
    iput-object p2, p0, LX/HaF;->b:LX/HaD;

    .line 2483631
    iput-object p3, p0, LX/HaF;->c:LX/HaE;

    .line 2483632
    iput-object p4, p0, LX/HaF;->d:LX/Ha9;

    .line 2483633
    iput-object p5, p0, LX/HaF;->e:LX/HaA;

    .line 2483634
    return-void
.end method

.method public static a(LX/0QB;)LX/HaF;
    .locals 10

    .prologue
    .line 2483635
    const-class v1, LX/HaF;

    monitor-enter v1

    .line 2483636
    :try_start_0
    sget-object v0, LX/HaF;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2483637
    sput-object v2, LX/HaF;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2483638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2483640
    new-instance v3, LX/HaF;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 2483641
    new-instance v9, LX/HaD;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lB;

    invoke-static {v0}, LX/HYW;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v7

    check-cast v7, LX/0dC;

    invoke-static {v0}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v8

    check-cast v8, LX/0hw;

    invoke-direct {v9, v5, v6, v7, v8}, LX/HaD;-><init>(LX/0lB;Ljava/lang/String;LX/0dC;LX/0hw;)V

    .line 2483642
    move-object v5, v9

    .line 2483643
    check-cast v5, LX/HaD;

    .line 2483644
    new-instance v7, LX/HaE;

    invoke-static {v0}, LX/HYW;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v7, v6}, LX/HaE;-><init>(Ljava/lang/String;)V

    .line 2483645
    move-object v6, v7

    .line 2483646
    check-cast v6, LX/HaE;

    .line 2483647
    new-instance v9, LX/Ha9;

    invoke-static {v0}, LX/HYW;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v8

    check-cast v8, LX/0dC;

    invoke-direct {v9, v7, v8}, LX/Ha9;-><init>(Ljava/lang/String;LX/0dC;)V

    .line 2483648
    move-object v7, v9

    .line 2483649
    check-cast v7, LX/Ha9;

    .line 2483650
    new-instance v9, LX/HaA;

    invoke-static {v0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v8

    check-cast v8, LX/0dC;

    invoke-direct {v9, v8}, LX/HaA;-><init>(LX/0dC;)V

    .line 2483651
    move-object v8, v9

    .line 2483652
    check-cast v8, LX/HaA;

    invoke-direct/range {v3 .. v8}, LX/HaF;-><init>(LX/0Or;LX/HaD;LX/HaE;LX/Ha9;LX/HaA;)V

    .line 2483653
    move-object v0, v3

    .line 2483654
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2483655
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HaF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2483656
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2483657
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2483658
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2483659
    const-string v1, "registration_register_account"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2483660
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2483661
    const-string v1, "registrationRegisterAccountParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/RegistrationFormData;

    .line 2483662
    iget-object v1, p0, LX/HaF;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/HaF;->b:LX/HaD;

    .line 2483663
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2483664
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2483665
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2483666
    :goto_0
    return-object v0

    .line 2483667
    :cond_0
    const-string v1, "registration_validate_registration_data"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2483668
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2483669
    const-string v1, "registrationValidateRegistrationDataParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/RegistrationFormData;

    .line 2483670
    iget-object v1, p0, LX/HaF;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/HaF;->c:LX/HaE;

    .line 2483671
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2483672
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2483673
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2483674
    move-object v0, v0

    .line 2483675
    goto :goto_0

    .line 2483676
    :cond_1
    const-string v1, "registration_contact_point_suggestions"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2483677
    iget-object v0, p0, LX/HaF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/HaF;->d:LX/Ha9;

    const/4 v2, 0x0

    .line 2483678
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2483679
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    .line 2483680
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2483681
    goto :goto_0

    .line 2483682
    :cond_2
    const-string v1, "registration_header_prefill_kickoff"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2483683
    iget-object v0, p0, LX/HaF;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/HaF;->e:LX/HaA;

    const/4 v2, 0x0

    .line 2483684
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2483685
    invoke-virtual {v0, v1, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2483686
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2483687
    move-object v0, v0

    .line 2483688
    goto :goto_0

    .line 2483689
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
