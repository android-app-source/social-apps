.class public final LX/JUz;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JV0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLEntity;

.field public b:LX/JVO;

.field public final synthetic c:LX/JV0;


# direct methods
.method public constructor <init>(LX/JV0;)V
    .locals 1

    .prologue
    .line 2700006
    iput-object p1, p0, LX/JUz;->c:LX/JV0;

    .line 2700007
    move-object v0, p1

    .line 2700008
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2700009
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700010
    const-string v0, "CommerceShareButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2700011
    if-ne p0, p1, :cond_1

    .line 2700012
    :cond_0
    :goto_0
    return v0

    .line 2700013
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2700014
    goto :goto_0

    .line 2700015
    :cond_3
    check-cast p1, LX/JUz;

    .line 2700016
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2700017
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2700018
    if-eq v2, v3, :cond_0

    .line 2700019
    iget-object v2, p0, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    iget-object v3, p1, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2700020
    goto :goto_0

    .line 2700021
    :cond_5
    iget-object v2, p1, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    if-nez v2, :cond_4

    .line 2700022
    :cond_6
    iget-object v2, p0, LX/JUz;->b:LX/JVO;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JUz;->b:LX/JVO;

    iget-object v3, p1, LX/JUz;->b:LX/JVO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2700023
    goto :goto_0

    .line 2700024
    :cond_7
    iget-object v2, p1, LX/JUz;->b:LX/JVO;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
