.class public LX/In4;
.super LX/6LI;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/5fv;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:Landroid/view/LayoutInflater;

.field public f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2610234
    const-class v0, LX/In4;

    sput-object v0, LX/In4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/5fv;Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610235
    sget-object v0, LX/In4;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2610236
    iput-object p1, p0, LX/In4;->b:LX/5fv;

    .line 2610237
    iput-object p2, p0, LX/In4;->c:Landroid/content/Context;

    .line 2610238
    iput-object p3, p0, LX/In4;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2610239
    iput-object p4, p0, LX/In4;->e:Landroid/view/LayoutInflater;

    .line 2610240
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2610241
    iget-object v0, p0, LX/In4;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2610242
    iget-object v0, p0, LX/In4;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f03018f

    invoke-virtual {v0, v1, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/banner/BasicBannerNotificationView;

    .line 2610243
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p0, LX/In4;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/In4;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a()I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 2610244
    iget-object v2, p0, LX/In4;->b:LX/5fv;

    sget-object v3, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v2, v1, v3}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v1

    .line 2610245
    new-instance v2, LX/2M6;

    invoke-direct {v2}, LX/2M6;-><init>()V

    iget-object v3, p0, LX/In4;->c:Landroid/content/Context;

    const v4, 0x7f082d33

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/In4;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    invoke-virtual {v6}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2610246
    iput-object v1, v2, LX/2M6;->a:Ljava/lang/CharSequence;

    .line 2610247
    move-object v1, v2

    .line 2610248
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, LX/In4;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a01fc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2610249
    iput-object v2, v1, LX/2M6;->c:Landroid/graphics/drawable/Drawable;

    .line 2610250
    move-object v1, v1

    .line 2610251
    iget-object v2, p0, LX/In4;->c:Landroid/content/Context;

    const v3, 0x7f082d32

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2M6;->a(Ljava/lang/String;)LX/2M6;

    move-result-object v1

    .line 2610252
    invoke-virtual {v1}, LX/2M6;->a()LX/6LR;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/banner/BasicBannerNotificationView;->setParams(LX/6LR;)V

    .line 2610253
    new-instance v1, LX/In3;

    invoke-direct {v1, p0}, LX/In3;-><init>(LX/In4;)V

    .line 2610254
    iput-object v1, v0, Lcom/facebook/common/banner/BasicBannerNotificationView;->a:LX/6LQ;

    .line 2610255
    return-object v0
.end method
