.class public LX/Ij4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iif;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605243
    iput-object p1, p0, LX/Ij4;->a:Landroid/content/Context;

    .line 2605244
    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Iik;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605245
    new-instance v0, LX/Ij2;

    iget-object v1, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ij2;-><init>(Landroid/content/Context;)V

    .line 2605246
    new-instance v1, LX/Ij6;

    invoke-direct {v1}, LX/Ij6;-><init>()V

    move-object v1, v1

    .line 2605247
    iput p1, v1, LX/Ij6;->e:I

    .line 2605248
    move-object v1, v1

    .line 2605249
    iput-object p2, v1, LX/Ij6;->d:Ljava/lang/String;

    .line 2605250
    move-object v1, v1

    .line 2605251
    iput-object p3, v1, LX/Ij6;->c:Ljava/lang/String;

    .line 2605252
    move-object v1, v1

    .line 2605253
    iput-object p4, v1, LX/Ij6;->a:Ljava/lang/String;

    .line 2605254
    move-object v1, v1

    .line 2605255
    iput-object p5, v1, LX/Ij6;->b:Ljava/lang/String;

    .line 2605256
    move-object v1, v1

    .line 2605257
    new-instance p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;

    invoke-direct {p0, v1}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;-><init>(LX/Ij6;)V

    move-object v1, p0

    .line 2605258
    iget p0, v1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    move p0, p0

    .line 2605259
    if-lez p0, :cond_0

    .line 2605260
    iget-object p1, v0, LX/Ij2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p2, v0, LX/Ij2;->b:Landroid/content/Context;

    invoke-static {p2, p0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2605261
    iget-object p1, v0, LX/Ij2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2605262
    :goto_0
    iget-object p0, v0, LX/Ij2;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2605263
    iget-object p1, v1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    move-object p1, p1

    .line 2605264
    invoke-static {p0, p1}, LX/Ij2;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2605265
    iget-object p0, v0, LX/Ij2;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2605266
    iget-object p1, v1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    move-object p1, p1

    .line 2605267
    invoke-static {p0, p1}, LX/Ij2;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2605268
    iget-object p0, v0, LX/Ij2;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2605269
    iget-object p1, v1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2605270
    invoke-static {p0, p1}, LX/Ij2;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2605271
    iget-object p0, v0, LX/Ij2;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2605272
    iget-object p1, v1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    move-object p1, p1

    .line 2605273
    invoke-static {p0, p1}, LX/Ij2;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2605274
    return-object v0

    .line 2605275
    :cond_0
    iget-object p1, v0, LX/Ij2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Iiv;)LX/Iik;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2605276
    sget-object v0, LX/Ij3;->a:[I

    invoke-virtual {p1}, LX/Iiv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2605277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid PaymentAwarenessMode provided: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2605278
    :pswitch_0
    const v1, 0x7f0203eb

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0839f8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0839f9

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0839fa

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Ij4;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Iik;

    move-result-object v0

    .line 2605279
    :goto_0
    return-object v0

    .line 2605280
    :pswitch_1
    const v1, 0x7f0203eb

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0839f5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0839f6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0839f7

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Ij4;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Iik;

    move-result-object v0

    goto :goto_0

    .line 2605281
    :pswitch_2
    const v1, 0x7f0203eb

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0839f2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0839f3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0839f4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Ij4;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Iik;

    move-result-object v0

    goto :goto_0

    .line 2605282
    :pswitch_3
    const v1, 0x7f0203eb

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0839ef

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0839f0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Ij4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0839f1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/Ij4;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Iik;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
