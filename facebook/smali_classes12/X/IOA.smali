.class public final LX/IOA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

.field public final synthetic b:Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;)V
    .locals 0

    .prologue
    .line 2571485
    iput-object p1, p0, LX/IOA;->b:Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    iput-object p2, p0, LX/IOA;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x16055393

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571486
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2571487
    iget-object v2, p0, LX/IOA;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2571488
    iget-object v2, p0, LX/IOA;->b:Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    .line 2571489
    iget-object p0, v2, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->a:Landroid/content/pm/PackageManager;

    const/high16 p1, 0x10000

    invoke-virtual {p0, v1, p1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p0

    .line 2571490
    if-eqz p0, :cond_0

    .line 2571491
    iget-object p0, v2, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v2}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {p0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2571492
    :cond_0
    const v1, 0x48809b18    # 263384.75f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
