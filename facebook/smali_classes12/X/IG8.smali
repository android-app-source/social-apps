.class public LX/IG8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IG8;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IGC;

.field public final c:LX/IG9;


# direct methods
.method public constructor <init>(LX/0Or;LX/IGC;LX/IG9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/IGC;",
            "LX/IG9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2555309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555310
    iput-object p1, p0, LX/IG8;->a:LX/0Or;

    .line 2555311
    iput-object p2, p0, LX/IG8;->b:LX/IGC;

    .line 2555312
    iput-object p3, p0, LX/IG8;->c:LX/IG9;

    .line 2555313
    return-void
.end method

.method public static a(LX/0QB;)LX/IG8;
    .locals 6

    .prologue
    .line 2555314
    sget-object v0, LX/IG8;->d:LX/IG8;

    if-nez v0, :cond_1

    .line 2555315
    const-class v1, LX/IG8;

    monitor-enter v1

    .line 2555316
    :try_start_0
    sget-object v0, LX/IG8;->d:LX/IG8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2555317
    if-eqz v2, :cond_0

    .line 2555318
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2555319
    new-instance v5, LX/IG8;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2555320
    new-instance v4, LX/IGC;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {v4, v3}, LX/IGC;-><init>(LX/0SG;)V

    .line 2555321
    move-object v3, v4

    .line 2555322
    check-cast v3, LX/IGC;

    .line 2555323
    new-instance v4, LX/IG9;

    invoke-direct {v4}, LX/IG9;-><init>()V

    .line 2555324
    move-object v4, v4

    .line 2555325
    move-object v4, v4

    .line 2555326
    check-cast v4, LX/IG9;

    invoke-direct {v5, p0, v3, v4}, LX/IG8;-><init>(LX/0Or;LX/IGC;LX/IG9;)V

    .line 2555327
    move-object v0, v5

    .line 2555328
    sput-object v0, LX/IG8;->d:LX/IG8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2555329
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2555330
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2555331
    :cond_1
    sget-object v0, LX/IG8;->d:LX/IG8;

    return-object v0

    .line 2555332
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2555333
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2555334
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2555335
    const-string v1, "location_ping"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2555336
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2555337
    const-string v1, "locationPingParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2555338
    iget-object v1, p0, LX/IG8;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/IG8;->b:LX/IGC;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2555339
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2555340
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2555341
    :goto_0
    move-object v0, v0

    .line 2555342
    :goto_1
    return-object v0

    .line 2555343
    :cond_0
    const-string v1, "delete_ping"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2555344
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2555345
    const-string v1, "locationPingDeleteParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/LocationPingDeleteParams;

    .line 2555346
    iget-object v1, p0, LX/IG8;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/IG8;->c:LX/IG9;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2555347
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2555348
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2555349
    :goto_2
    move-object v0, v0

    .line 2555350
    goto :goto_1

    .line 2555351
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2
.end method
