.class public final LX/Inu;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611113
    iput-object p1, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2611114
    iget-object v0, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    const-string v1, "p2p_decline_fail"

    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;Ljava/lang/String;)V

    .line 2611115
    iget-object v0, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b:LX/03V;

    const-string v1, "EnterPaymentValueFragment"

    const-string v2, "Failed to decline a request."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2611116
    iget-object v0, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F:LX/Ini;

    invoke-virtual {v0}, LX/Ini;->a()V

    .line 2611117
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2611118
    iget-object v0, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    const-string v1, "p2p_decline_success"

    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;Ljava/lang/String;)V

    .line 2611119
    iget-object v0, p0, LX/Inu;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->x(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611120
    return-void
.end method
