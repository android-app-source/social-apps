.class public LX/Izd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/IzB;

.field public final b:LX/Izj;


# direct methods
.method public constructor <init>(LX/IzB;LX/Izj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634724
    iput-object p1, p0, LX/Izd;->a:LX/IzB;

    .line 2634725
    iput-object p2, p0, LX/Izd;->b:LX/Izj;

    .line 2634726
    return-void
.end method

.method public static a(LX/0QB;)LX/Izd;
    .locals 1

    .prologue
    .line 2634722
    invoke-static {p0}, LX/Izd;->b(LX/0QB;)LX/Izd;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Izd;
    .locals 3

    .prologue
    .line 2634741
    new-instance v2, LX/Izd;

    invoke-static {p0}, LX/IzB;->a(LX/0QB;)LX/IzB;

    move-result-object v0

    check-cast v0, LX/IzB;

    invoke-static {p0}, LX/Izj;->a(LX/0QB;)LX/Izj;

    move-result-object v1

    check-cast v1, LX/Izj;

    invoke-direct {v2, v0, v1}, LX/Izd;-><init>(LX/IzB;LX/Izj;)V

    .line 2634742
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2634743
    iget-object v0, p0, LX/Izd;->b:LX/Izj;

    invoke-virtual {v0}, LX/Izj;->b()V

    .line 2634744
    iget-object v0, p0, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0}, LX/IzB;->c()V

    .line 2634745
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2634732
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2634733
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2634734
    iget-wide v6, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v4, v6

    .line 2634735
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634736
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2634737
    :cond_0
    iget-object v0, p0, LX/Izd;->b:LX/Izj;

    invoke-virtual {v0}, LX/Izj;->a()V

    .line 2634738
    iget-object v0, p0, LX/Izd;->b:LX/Izj;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Izj;->a(LX/0Px;)V

    .line 2634739
    iget-object v0, p0, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0, p1}, LX/IzB;->a(LX/0Px;)V

    .line 2634740
    return-void
.end method

.method public final a(Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 6

    .prologue
    .line 2634727
    iget-object v0, p0, LX/Izd;->b:LX/Izj;

    .line 2634728
    iget-wide v4, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v2, v4

    .line 2634729
    invoke-virtual {v0, v2, v3}, LX/Izj;->a(J)V

    .line 2634730
    iget-object v0, p0, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0, p1}, LX/IzB;->a(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2634731
    return-void
.end method
