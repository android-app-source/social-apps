.class public LX/Iy7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5fv;

.field public final b:LX/IyJ;

.field public c:LX/6qh;


# direct methods
.method public constructor <init>(LX/5fv;LX/IyJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2632807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2632808
    iput-object p1, p0, LX/Iy7;->a:LX/5fv;

    .line 2632809
    iput-object p2, p0, LX/Iy7;->b:LX/IyJ;

    .line 2632810
    return-void
.end method

.method public static a$redex0(LX/Iy7;Lcom/facebook/payments/cart/model/SimpleCartItem;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2632811
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2632812
    const-string v1, "extra_user_action"

    .line 2632813
    iget-object v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2632814
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2632815
    const-string v1, "view_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2632816
    iget-object v1, p0, LX/Iy7;->c:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v2, v3, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 2632817
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/cart/model/SimpleCartItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2632818
    sget-object v0, LX/Iy6;->a:[I

    .line 2632819
    iget-object v1, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    move-object v1, v1

    .line 2632820
    invoke-virtual {v1}, LX/IyK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2632821
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2632822
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2632823
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2632824
    :goto_0
    if-nez p2, :cond_1

    new-instance p2, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/facebook/payments/cart/ui/CartSearchItemView;-><init>(Landroid/content/Context;)V

    .line 2632825
    :goto_1
    invoke-virtual {p2, p1, v0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Ljava/lang/String;)V

    .line 2632826
    move-object v0, p2

    .line 2632827
    :goto_2
    return-object v0

    .line 2632828
    :pswitch_1
    if-nez p2, :cond_2

    new-instance p2, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;-><init>(Landroid/content/Context;)V

    .line 2632829
    :goto_3
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/facebook/payments/cart/ui/CartSearchItemView;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Ljava/lang/String;)V

    .line 2632830
    move-object v0, p2

    .line 2632831
    goto :goto_2

    .line 2632832
    :pswitch_2
    if-nez p2, :cond_3

    new-instance p2, Lcom/facebook/payments/ui/MediaGridTextLayout;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/facebook/payments/ui/MediaGridTextLayout;-><init>(Landroid/content/Context;)V

    .line 2632833
    :goto_4
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f017f

    .line 2632834
    iget v2, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    move v2, v2

    .line 2632835
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2632836
    iget v5, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->g:I

    move v5, v5

    .line 2632837
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/Iy7;->a:LX/5fv;

    .line 2632838
    iget-object v6, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v6, v6

    .line 2632839
    invoke-virtual {v5, v6}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2632840
    iget-object v1, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2632841
    invoke-static {v1}, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a(Ljava/lang/String;)LX/73R;

    move-result-object v1

    .line 2632842
    iput-object v0, v1, LX/73R;->d:Ljava/lang/String;

    .line 2632843
    move-object v0, v1

    .line 2632844
    iget-object v1, p0, LX/Iy7;->a:LX/5fv;

    invoke-virtual {p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->f()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    .line 2632845
    iput-object v1, v0, LX/73R;->b:Ljava/lang/String;

    .line 2632846
    move-object v0, v0

    .line 2632847
    invoke-virtual {p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/73R;->b(Ljava/lang/String;)LX/73R;

    move-result-object v0

    invoke-virtual {v0}, LX/73R;->a()Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/payments/ui/MediaGridTextLayout;->setViewParams(Lcom/facebook/payments/ui/MediaGridTextLayoutParams;)V

    .line 2632848
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Iy4;

    invoke-direct {v1, p0, p1}, LX/Iy4;-><init>(LX/Iy7;Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    invoke-virtual {p2, v0, v1}, Lcom/facebook/payments/ui/MediaGridTextLayout;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2632849
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Iy5;

    invoke-direct {v1, p0, p1}, LX/Iy5;-><init>(LX/Iy7;Lcom/facebook/payments/cart/model/SimpleCartItem;)V

    invoke-virtual {p2, v0, v1}, Lcom/facebook/payments/ui/MediaGridTextLayout;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2632850
    move-object v0, p2

    .line 2632851
    goto/16 :goto_2

    .line 2632852
    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0838d0    # 1.8107E38f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2632853
    iget-object p0, p1, Lcom/facebook/payments/cart/model/SimpleCartItem;->c:Ljava/lang/String;

    move-object p0, p0

    .line 2632854
    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2632855
    :cond_1
    check-cast p2, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    goto/16 :goto_1

    .line 2632856
    :cond_2
    check-cast p2, Lcom/facebook/payments/cart/ui/CartSearchItemView;

    goto/16 :goto_3

    .line 2632857
    :cond_3
    check-cast p2, Lcom/facebook/payments/ui/MediaGridTextLayout;

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
