.class public final LX/HNX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HNc;


# direct methods
.method public constructor <init>(LX/HNc;)V
    .locals 0

    .prologue
    .line 2458592
    iput-object p1, p0, LX/HNX;->a:LX/HNc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2458593
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->d:LX/HNb;

    if-eqz v0, :cond_0

    .line 2458594
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->d:LX/HNb;

    invoke-interface {v0, p1}, LX/HNb;->a(Ljava/lang/Throwable;)V

    .line 2458595
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2458596
    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;

    .line 2458597
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->i:LX/CYE;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->i:LX/CYE;

    .line 2458598
    iget-boolean v1, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v1

    .line 2458599
    if-eqz v0, :cond_4

    .line 2458600
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2458601
    :cond_0
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->d:LX/HNb;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-interface {v0, v1}, LX/HNb;->a(Ljava/lang/Throwable;)V

    .line 2458602
    :cond_1
    :goto_0
    return-void

    .line 2458603
    :cond_2
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->i:LX/CYE;

    .line 2458604
    iget-object v1, v0, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v0, v1

    .line 2458605
    if-eqz v0, :cond_3

    .line 2458606
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;->j()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2458607
    invoke-static {v1}, LX/HNc;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v8

    .line 2458608
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v8, v3, :cond_5

    .line 2458609
    iget-object v3, v0, LX/HNc;->a:LX/1Ck;

    const-string v10, "replace_cta_action_mutation"

    iget-object v4, v0, LX/HNc;->c:LX/HNW;

    iget-object v5, v0, LX/HNc;->f:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    iget-object v7, v0, LX/HNc;->i:LX/CYE;

    move-object v9, v2

    invoke-virtual/range {v4 .. v9}, LX/HNW;->b(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HNY;

    invoke-direct {v5, v0}, LX/HNY;-><init>(LX/HNc;)V

    invoke-virtual {v3, v10, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2458610
    :goto_1
    goto :goto_0

    .line 2458611
    :cond_3
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;->j()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreCreateMutationFieldsModel$PageCallToActionModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2458612
    invoke-static {v1}, LX/HNc;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v8

    .line 2458613
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v8, v3, :cond_6

    .line 2458614
    iget-object v3, v0, LX/HNc;->a:LX/1Ck;

    const-string v10, "add_cta_action_mutation"

    iget-object v4, v0, LX/HNc;->c:LX/HNW;

    iget-object v5, v0, LX/HNc;->f:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    iget-object v7, v0, LX/HNc;->i:LX/CYE;

    move-object v9, v2

    invoke-virtual/range {v4 .. v9}, LX/HNW;->a(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HNZ;

    invoke-direct {v5, v0}, LX/HNZ;-><init>(LX/HNc;)V

    invoke-virtual {v3, v10, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2458615
    :goto_2
    goto :goto_0

    .line 2458616
    :cond_4
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->d:LX/HNb;

    if-eqz v0, :cond_1

    .line 2458617
    iget-object v0, p0, LX/HNX;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->d:LX/HNb;

    invoke-interface {v0}, LX/HNb;->a()V

    goto/16 :goto_0

    .line 2458618
    :cond_5
    iget-object v3, v0, LX/HNc;->d:LX/HNb;

    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "No page action exists that matches the call-to-action type"

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, LX/HNb;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2458619
    :cond_6
    iget-object v3, v0, LX/HNc;->d:LX/HNb;

    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "No page action exists that matches the call-to-action type"

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, LX/HNb;->a(Ljava/lang/Throwable;)V

    goto :goto_2
.end method
