.class public LX/IE9;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# instance fields
.field public j:Landroid/view/View;

.field public k:Lcom/facebook/friends/ui/SmartButtonLite;

.field public l:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2551613
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2551614
    const/4 p1, 0x0

    .line 2551615
    invoke-virtual {p0}, LX/IE9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2551616
    const v1, 0x7f031055

    invoke-virtual {v0, v1, p0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/IE9;->j:Landroid/view/View;

    .line 2551617
    iget-object v0, p0, LX/IE9;->j:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/IE9;->addView(Landroid/view/View;)V

    .line 2551618
    const v0, 0x7f0d272a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2551619
    const v0, 0x7f0d272b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/IE9;->l:Landroid/widget/ImageView;

    .line 2551620
    invoke-virtual {p0}, LX/IE9;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, LX/IE9;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/IE9;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, v1, p1, v2}, LX/IE9;->setPadding(IIII)V

    .line 2551621
    const v0, 0x7f0e0bce

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaTextAppearance(I)V

    .line 2551622
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2551611
    iget-object v0, p0, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/friends/ui/SmartButtonLite;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2551612
    return-void
.end method

.method public setActionButtonContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2551623
    iget-object v0, p0, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2551624
    return-void
.end method

.method public setActionButtonStyle(I)V
    .locals 1

    .prologue
    .line 2551609
    iget-object v0, p0, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v0, p1}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 2551610
    return-void
.end method

.method public setActionButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2551607
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/IE9;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2551608
    return-void
.end method

.method public setShowButtonContainer(Z)V
    .locals 2

    .prologue
    .line 2551604
    iget-object v1, p0, LX/IE9;->j:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2551605
    return-void

    .line 2551606
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
