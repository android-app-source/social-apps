.class public final LX/Hbk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)V
    .locals 0

    .prologue
    .line 2486161
    iput-object p1, p0, LX/Hbk;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2486162
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2486163
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2486164
    iget-object v0, p0, LX/Hbk;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-static {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->n(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2486165
    iget-object v0, p0, LX/Hbk;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v0, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b()V

    .line 2486166
    :goto_0
    return-void

    .line 2486167
    :cond_0
    iget-object v0, p0, LX/Hbk;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    iget-object v0, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c()V

    goto :goto_0
.end method
