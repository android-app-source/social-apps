.class public final LX/HYI;
.super LX/HYB;
.source ""


# instance fields
.field public a:LX/4hz;


# direct methods
.method public constructor <init>(LX/4hz;Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 2480443
    invoke-direct {p0, p2}, LX/HYB;-><init>(Lorg/json/JSONObject;)V

    .line 2480444
    iput-object p1, p0, LX/HYI;->a:LX/4hz;

    .line 2480445
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2480412
    iget-object v1, p0, LX/HYI;->a:LX/4hz;

    .line 2480413
    iget-object v2, p0, LX/HYB;->a:Lorg/json/JSONObject;

    move-object v2, v2

    .line 2480414
    invoke-virtual {v1, v2}, LX/4hz;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    .line 2480415
    iget-object v2, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    move-object v2, v2

    .line 2480416
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2480417
    const-string v3, "method"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2480418
    const-string v4, "share"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2480419
    const-string v4, "com.facebook.platform.action.request.FEED_DIALOG"

    .line 2480420
    :goto_0
    move-object v3, v4

    .line 2480421
    const-string v4, "methodArgs"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2480422
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    .line 2480423
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2480424
    const-string v5, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    sget-object v6, LX/25y;->a:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2480425
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    invoke-virtual {v4, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2480426
    const-string v0, "com.facebook.platform.extra.APPLICATION_ID"

    .line 2480427
    iget-object v3, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2480428
    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2480429
    const-string v0, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2480430
    const-string v0, "com.facebook.platform.protocol.BRIDGE_ARGS"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2480431
    iput-object p0, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->o:LX/HYI;

    .line 2480432
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2480433
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/platform/common/activity/PlatformWrapperActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2480434
    invoke-virtual {v1, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2480435
    const-string v2, "calling_package_key"

    iget-object v3, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2480436
    iget-object v4, v3, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2480437
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2480438
    iget-object v2, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->e:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x64

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2480439
    const/4 v0, 0x1

    .line 2480440
    :cond_0
    if-nez v0, :cond_1

    .line 2480441
    const/4 v0, 0x0

    invoke-virtual {p2, p0, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a(LX/HYB;Landroid/os/Bundle;)V

    .line 2480442
    :cond_1
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
