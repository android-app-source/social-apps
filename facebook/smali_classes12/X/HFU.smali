.class public final LX/HFU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443588
    iput-object p1, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x68abf3eb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2443589
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v1, 0x7f0d22d5

    .line 2443590
    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2443591
    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443592
    iget-object v1, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v2, 0x7f0d22d9

    .line 2443593
    invoke-virtual {v1, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    move-object v1, v3

    .line 2443594
    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443595
    iget-object v2, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v3, 0x7f0d22da

    .line 2443596
    invoke-virtual {v2, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    move-object v2, v4

    .line 2443597
    move-object v3, v2

    check-cast v3, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443598
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2443599
    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2443600
    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2443601
    iget-object v1, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443602
    iget-object v1, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2443603
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2443604
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "next"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HFf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443605
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b()V

    .line 2443606
    :goto_0
    const v0, -0x237ae8b5

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2443607
    :cond_0
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443608
    iget-object v1, v0, LX/HFJ;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2443609
    if-eqz v0, :cond_5

    .line 2443610
    :cond_1
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443611
    iget-object v1, v0, LX/HFJ;->g:Ljava/lang/String;

    move-object v3, v1

    .line 2443612
    :goto_1
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443613
    iget-object v1, v0, LX/HFJ;->f:Ljava/lang/String;

    move-object v0, v1

    .line 2443614
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443615
    iget-object v1, v0, LX/HFJ;->g:Ljava/lang/String;

    move-object v0, v1

    .line 2443616
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443617
    iget-object v1, v0, LX/HFJ;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2443618
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443619
    iget-object v1, v0, LX/HFJ;->j:Ljava/lang/String;

    move-object v0, v1

    .line 2443620
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2443621
    :cond_2
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v7}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443622
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443623
    iget-object v7, v1, LX/HFJ;->a:Ljava/lang/String;

    move-object v1, v7

    .line 2443624
    iget-object p0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string p1, "save_address_gql_task_key"

    iget-object v7, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b:LX/HFc;

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    .line 2443625
    new-instance v1, LX/4Hd;

    invoke-direct {v1}, LX/4Hd;-><init>()V

    .line 2443626
    const-string v2, "pageid"

    invoke-virtual {v1, v2, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443627
    move-object v1, v1

    .line 2443628
    const-string v2, "street"

    invoke-virtual {v1, v2, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443629
    move-object v1, v1

    .line 2443630
    const-string v2, "city_id"

    invoke-virtual {v1, v2, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443631
    move-object v1, v1

    .line 2443632
    const-string v2, "zipcode"

    invoke-virtual {v1, v2, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443633
    move-object v1, v1

    .line 2443634
    const-string v2, "phone"

    invoke-virtual {v1, v2, v12}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443635
    move-object v1, v1

    .line 2443636
    new-instance v2, LX/HGD;

    invoke-direct {v2}, LX/HGD;-><init>()V

    move-object v2, v2

    .line 2443637
    const-string v4, "input"

    invoke-virtual {v2, v4, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/HGD;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2443638
    iget-object v2, v7, LX/HFc;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {v1}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v7, v1

    .line 2443639
    new-instance v8, LX/HFO;

    invoke-direct {v8, v0, v3}, LX/HFO;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v7, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443640
    goto/16 :goto_0

    .line 2443641
    :cond_3
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    goto/16 :goto_1

    .line 2443642
    :cond_4
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b()V

    goto/16 :goto_0

    .line 2443643
    :cond_5
    iget-object v0, p0, LX/HFU;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v1, 0x7f0d22d2

    .line 2443644
    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2443645
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443646
    invoke-virtual {v0, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443647
    const v1, 0x7f082211

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto/16 :goto_0
.end method
