.class public final LX/IvC;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/neko/util/MainActivityFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/neko/util/MainActivityFragment;)V
    .locals 1

    .prologue
    .line 2628035
    iput-object p1, p0, LX/IvC;->a:Lcom/facebook/neko/util/MainActivityFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    .line 2628036
    const/4 v0, 0x0

    iput v0, p0, LX/IvC;->b:I

    return-void
.end method


# virtual methods
.method public final b(I)V
    .locals 5

    .prologue
    .line 2628019
    if-nez p1, :cond_1

    .line 2628020
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2628021
    iget-object v1, p0, LX/IvC;->a:Lcom/facebook/neko/util/MainActivityFragment;

    iget-object v1, v1, Lcom/facebook/neko/util/MainActivityFragment;->e:Lcom/facebook/neko/util/AppCardPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 2628022
    iget-object v2, p0, LX/IvC;->a:Lcom/facebook/neko/util/MainActivityFragment;

    iget-object v2, v2, Lcom/facebook/neko/util/MainActivityFragment;->f:LX/Iux;

    .line 2628023
    if-ltz v1, :cond_2

    iget-object v3, v2, LX/Iux;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 2628024
    iget-object v3, v2, LX/Iux;->e:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HjF;

    .line 2628025
    :goto_0
    move-object v2, v3

    .line 2628026
    iget v3, p0, LX/IvC;->b:I

    sub-int v3, v1, v3

    .line 2628027
    iput v1, p0, LX/IvC;->b:I

    .line 2628028
    const-string v4, "position"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2628029
    const-string v1, "direction"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2628030
    if-eqz v2, :cond_0

    .line 2628031
    const-string v1, "adID"

    invoke-static {v2}, LX/HjF;->m(LX/HjF;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x0

    :goto_1
    move-object v2, v3

    .line 2628032
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2628033
    :cond_0
    iget-object v1, p0, LX/IvC;->a:Lcom/facebook/neko/util/MainActivityFragment;

    iget-object v1, v1, Lcom/facebook/neko/util/MainActivityFragment;->g:LX/Iup;

    invoke-interface {v1, v0}, LX/Iup;->a(Ljava/util/Map;)V

    .line 2628034
    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    iget-object v3, v2, LX/HjF;->j:LX/Hjj;

    invoke-virtual {v3}, LX/Hjj;->n()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
