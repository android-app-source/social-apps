.class public final LX/IjQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:LX/6y8;

.field public final synthetic c:LX/IjT;


# direct methods
.method public constructor <init>(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V
    .locals 0

    .prologue
    .line 2605724
    iput-object p1, p0, LX/IjQ;->c:LX/IjT;

    iput-object p2, p0, LX/IjQ;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object p3, p0, LX/IjQ;->b:LX/6y8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2605725
    iget-object v0, p0, LX/IjQ;->c:LX/IjT;

    iget-object v0, v0, LX/IjT;->c:LX/6ye;

    iget-object v1, p0, LX/IjQ;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, p0, LX/IjQ;->c:LX/IjT;

    iget-object v2, v2, LX/IjT;->b:Landroid/content/Context;

    const v3, 0x7f082c2b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2605726
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2605727
    iget-object v0, p0, LX/IjQ;->c:LX/IjT;

    iget-object v1, p0, LX/IjQ;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/IjQ;->b:LX/6y8;

    invoke-static {v0, v1, v2}, LX/IjT;->b(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V

    .line 2605728
    return-void
.end method
