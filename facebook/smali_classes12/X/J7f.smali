.class public LX/J7f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field private final b:LX/1Ck;

.field public final c:Ljava/util/concurrent/Executor;

.field public d:Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2651098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2651099
    iput-object p1, p0, LX/J7f;->a:LX/0tX;

    .line 2651100
    iput-object p2, p0, LX/J7f;->b:LX/1Ck;

    .line 2651101
    iput-object p3, p0, LX/J7f;->c:Ljava/util/concurrent/Executor;

    .line 2651102
    return-void
.end method

.method public static b(LX/0QB;)LX/J7f;
    .locals 4

    .prologue
    .line 2651103
    new-instance v3, LX/J7f;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/J7f;-><init>(LX/0tX;LX/1Ck;Ljava/util/concurrent/ExecutorService;)V

    .line 2651104
    return-object v3
.end method


# virtual methods
.method public final a(LX/0TF;I)V
    .locals 4

    .prologue
    .line 2651105
    if-nez p1, :cond_0

    .line 2651106
    :goto_0
    return-void

    .line 2651107
    :cond_0
    iget-object v0, p0, LX/J7f;->b:LX/1Ck;

    const/4 v1, 0x0

    new-instance v2, LX/J7c;

    invoke-direct {v2, p0, p2}, LX/J7c;-><init>(LX/J7f;I)V

    new-instance v3, LX/J7d;

    invoke-direct {v3, p0, p1}, LX/J7d;-><init>(LX/J7f;LX/0TF;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
