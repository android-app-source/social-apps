.class public final LX/Hzj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525914
    iput-object p1, p0, LX/Hzj;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2525915
    iget-object v0, p0, LX/Hzj;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    iget-object v2, p0, LX/Hzj;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-boolean v2, v2, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    const/16 v3, 0xe

    iget-object v4, p0, LX/Hzj;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0f74

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "PUBLISHED"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "CANCELED"

    aput-object v7, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
