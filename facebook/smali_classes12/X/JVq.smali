.class public LX/JVq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/JVs;

.field public final b:LX/BcE;


# direct methods
.method public constructor <init>(LX/JVs;LX/BcE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701701
    iput-object p1, p0, LX/JVq;->a:LX/JVs;

    .line 2701702
    iput-object p2, p0, LX/JVq;->b:LX/BcE;

    .line 2701703
    return-void
.end method

.method public static a(LX/0QB;)LX/JVq;
    .locals 5

    .prologue
    .line 2701704
    const-class v1, LX/JVq;

    monitor-enter v1

    .line 2701705
    :try_start_0
    sget-object v0, LX/JVq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701706
    sput-object v2, LX/JVq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701707
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701708
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701709
    new-instance p0, LX/JVq;

    const-class v3, LX/JVs;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JVs;

    invoke-static {v0}, LX/BcE;->a(LX/0QB;)LX/BcE;

    move-result-object v4

    check-cast v4, LX/BcE;

    invoke-direct {p0, v3, v4}, LX/JVq;-><init>(LX/JVs;LX/BcE;)V

    .line 2701710
    move-object v0, p0

    .line 2701711
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701712
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701713
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701714
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
