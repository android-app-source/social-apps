.class public LX/HYW;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480775
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2480762
    sget-object v0, LX/HYW;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2480763
    const-class v1, LX/HYW;

    monitor-enter v1

    .line 2480764
    :try_start_0
    sget-object v0, LX/HYW;->a:Ljava/lang/String;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480765
    if-eqz v2, :cond_0

    .line 2480766
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480767
    invoke-static {v0}, LX/HYg;->b(LX/0QB;)LX/HYg;

    move-result-object p0

    check-cast p0, LX/HYg;

    invoke-static {p0}, LX/HYV;->a(LX/HYg;)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 2480768
    sput-object v0, LX/HYW;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480769
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480770
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480771
    :cond_1
    sget-object v0, LX/HYW;->a:Ljava/lang/String;

    return-object v0

    .line 2480772
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2480774
    invoke-static {p0}, LX/HYg;->b(LX/0QB;)LX/HYg;

    move-result-object v0

    check-cast v0, LX/HYg;

    invoke-static {v0}, LX/HYV;->a(LX/HYg;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
