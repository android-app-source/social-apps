.class public LX/Izi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Izi;


# instance fields
.field private final a:LX/IzO;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/IzO;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635150
    iput-object p1, p0, LX/Izi;->a:LX/IzO;

    .line 2635151
    iput-object p2, p0, LX/Izi;->b:LX/03V;

    .line 2635152
    return-void
.end method

.method public static a(LX/0QB;)LX/Izi;
    .locals 5

    .prologue
    .line 2635136
    sget-object v0, LX/Izi;->c:LX/Izi;

    if-nez v0, :cond_1

    .line 2635137
    const-class v1, LX/Izi;

    monitor-enter v1

    .line 2635138
    :try_start_0
    sget-object v0, LX/Izi;->c:LX/Izi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635139
    if-eqz v2, :cond_0

    .line 2635140
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635141
    new-instance p0, LX/Izi;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/Izi;-><init>(LX/IzO;LX/03V;)V

    .line 2635142
    move-object v0, p0

    .line 2635143
    sput-object v0, LX/Izi;->c:LX/Izi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635144
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635145
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635146
    :cond_1
    sget-object v0, LX/Izi;->c:LX/Izi;

    return-object v0

    .line 2635147
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635148
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)Ljava/lang/Long;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2635110
    const-string v0, "getPaymentCardIdForTransaction"

    const v1, 0x1991293a

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635111
    :try_start_0
    iget-object v0, p0, LX/Izi;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2635112
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v3, LX/IzY;->a:LX/0U1;

    .line 2635113
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635114
    aput-object v3, v2, v1

    const/4 v1, 0x1

    sget-object v3, LX/IzY;->b:LX/0U1;

    .line 2635115
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635116
    aput-object v3, v2, v1

    .line 2635117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/IzY;->a:LX/0U1;

    .line 2635118
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2635119
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2635120
    const-string v1, "transaction_payment_card_id"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2635121
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v9, :cond_0

    .line 2635122
    iget-object v0, p0, LX/Izi;->b:LX/03V;

    const-string v2, "DbFetchTransactionPaymentCardIdHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TransactionPaymentCardId table should only have one row for a given transactionID, but it has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635123
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635124
    const v0, 0x20a573c8

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 2635125
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2635126
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2635127
    const v0, -0x1f6b3c4f

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2635128
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2635129
    sget-object v0, LX/IzY;->b:LX/0U1;

    .line 2635130
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 2635131
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2635132
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2635133
    const v1, -0x218cd4f3

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635134
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2635135
    :catchall_1
    move-exception v0

    const v1, 0x49c5ff7b    # 1621999.4f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
