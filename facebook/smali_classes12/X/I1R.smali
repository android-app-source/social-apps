.class public LX/I1R;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private c:Lcom/facebook/events/common/EventAnalyticsParams;

.field private d:LX/I1V;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2528968
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f0d01a7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d01a9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/I1R;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;LX/I1V;)V
    .locals 0
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528964
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2528965
    iput-object p1, p0, LX/I1R;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528966
    iput-object p2, p0, LX/I1R;->d:LX/I1V;

    .line 2528967
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 11

    .prologue
    .line 2528952
    const v0, 0x7f0d01a7

    if-ne p2, v0, :cond_0

    .line 2528953
    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2528954
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2528955
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b15db

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2528956
    new-instance v0, LX/I1Q;

    invoke-direct {v0, v1}, LX/I1Q;-><init>(Landroid/view/View;)V

    .line 2528957
    :goto_0
    return-object v0

    .line 2528958
    :cond_0
    const v0, 0x7f0d01a9

    if-ne p2, v0, :cond_1

    .line 2528959
    iget-object v0, p0, LX/I1R;->d:LX/I1V;

    new-instance v1, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/I1R;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528960
    new-instance v5, LX/I1U;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v9

    check-cast v9, LX/6RZ;

    invoke-static {v0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v10

    check-cast v10, LX/Blh;

    move-object v6, v1

    move-object v7, v2

    invoke-direct/range {v5 .. v10}, LX/I1U;-><init>(Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/6RZ;LX/Blh;)V

    .line 2528961
    move-object v0, v5

    .line 2528962
    goto :goto_0

    .line 2528963
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2528969
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2528970
    const v1, 0x7f0d01a9

    if-ne v0, v1, :cond_0

    .line 2528971
    check-cast p1, LX/I1U;

    .line 2528972
    iget-object v0, p0, LX/I1R;->b:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {p1, v0}, LX/I1U;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    .line 2528973
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V
    .locals 0
    .param p1    # Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2528949
    iput-object p1, p0, LX/I1R;->b:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2528950
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2528951
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2528948
    if-nez p1, :cond_0

    const v0, 0x7f0d01a7

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d01a9

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2528947
    iget-object v0, p0, LX/I1R;->b:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I1R;->b:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
