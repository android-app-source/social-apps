.class public final LX/Iuz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/neko/util/AppShimmerFrameLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/neko/util/AppShimmerFrameLayout;)V
    .locals 0

    .prologue
    .line 2627544
    iput-object p1, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2627545
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2627546
    iget-object v1, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v2, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v2, v2, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    iget v2, v2, LX/Iv4;->a:I

    int-to-float v2, v2

    sub-float v3, v4, v0

    mul-float/2addr v2, v3

    iget-object v3, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v3, v3, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    iget v3, v3, LX/Iv4;->c:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setMaskOffsetX(Lcom/facebook/neko/util/AppShimmerFrameLayout;I)V

    .line 2627547
    iget-object v1, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v2, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v2, v2, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    iget v2, v2, LX/Iv4;->b:I

    int-to-float v2, v2

    sub-float v3, v4, v0

    mul-float/2addr v2, v3

    iget-object v3, p0, LX/Iuz;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iget-object v3, v3, Lcom/facebook/neko/util/AppShimmerFrameLayout;->g:LX/Iv4;

    iget v3, v3, LX/Iv4;->d:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    invoke-static {v1, v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setMaskOffsetY(Lcom/facebook/neko/util/AppShimmerFrameLayout;I)V

    .line 2627548
    return-void
.end method
