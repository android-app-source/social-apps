.class public final LX/IRd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1CH;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576585
    iput-object p1, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2576586
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l()V

    .line 2576587
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "groups_optimistic_post_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to post to profile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576588
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 2576589
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l()V

    .line 2576590
    invoke-static {p1}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2576591
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    .line 2576592
    const/4 p0, 0x1

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;I)V

    .line 2576593
    :cond_0
    return-void
.end method

.method public final a(JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 2576594
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2576595
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->mJ_()V

    .line 2576596
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 2576597
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Y(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2576598
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2576599
    iget-object v1, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->v:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, v0}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2576600
    :goto_0
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    .line 2576601
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    .line 2576602
    invoke-static {p1}, LX/16z;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->M(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2576603
    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Z(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576604
    :cond_0
    :goto_1
    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Y(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2576605
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2576606
    :cond_1
    return-void

    .line 2576607
    :cond_2
    iget-object v0, p0, LX/IRd;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l()V

    goto :goto_0

    .line 2576608
    :cond_3
    iget-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aU:Z

    if-eqz v1, :cond_0

    .line 2576609
    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->Z(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576610
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aU:Z

    goto :goto_1
.end method
