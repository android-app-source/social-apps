.class public final LX/IEI;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2551766
    const-class v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchPYMKFriendListQueryModel;

    const v0, 0x3bd56a66

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchPYMKFriendListQuery"

    const-string v6, "68940c60ba1496c31315bb1b1eebb15e"

    const-string v7, "viewer"

    const-string v8, "10155192631766729"

    const-string v9, "10155259087896729"

    .line 2551767
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2551768
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2551769
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2551770
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2551771
    sparse-switch v0, :sswitch_data_0

    .line 2551772
    :goto_0
    return-object p1

    .line 2551773
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2551774
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2551775
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2551776
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2551777
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2551778
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x76d1794f -> :sswitch_5
        -0x295975c2 -> :sswitch_1
        0x818bf83 -> :sswitch_2
        0xa174e6b -> :sswitch_3
        0x21beac6a -> :sswitch_0
        0x6c6f579a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2551779
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2551780
    :goto_1
    return v0

    .line 2551781
    :pswitch_1
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 2551782
    :pswitch_4
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2551783
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2551784
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
