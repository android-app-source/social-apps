.class public LX/HXP;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/Ft9;


# instance fields
.field private final n:LX/1PT;

.field private final o:LX/HXk;

.field private final p:LX/5SB;

.field private final q:LX/Ft5;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/5SB;Ljava/lang/Runnable;LX/1PY;LX/HXk;LX/Ft5;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2479056
    invoke-direct {p0, p1, p4, p5}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2479057
    iput-object p2, p0, LX/HXP;->n:LX/1PT;

    .line 2479058
    iput-object p6, p0, LX/HXP;->o:LX/HXk;

    .line 2479059
    iput-object p3, p0, LX/HXP;->p:LX/5SB;

    .line 2479060
    iput-object p7, p0, LX/HXP;->q:LX/Ft5;

    .line 2479061
    return-void
.end method


# virtual methods
.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2479062
    iget-object v0, p0, LX/HXP;->n:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 4

    .prologue
    .line 2479049
    iget-object v0, p0, LX/HXP;->o:LX/HXk;

    iget-object v1, p0, LX/HXP;->n:LX/1PT;

    .line 2479050
    instance-of v2, v1, LX/FuQ;

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2479051
    check-cast v1, LX/FuQ;

    .line 2479052
    iget-object v2, v0, LX/HXk;->a:LX/HXj;

    .line 2479053
    iget-object v3, v1, LX/FuQ;->a:LX/5SB;

    move-object v3, v3

    .line 2479054
    invoke-virtual {v2, v3, p0}, LX/HXj;->a(LX/5SB;LX/1Qj;)LX/HXi;

    move-result-object v2

    move-object v0, v2

    .line 2479055
    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2479048
    iget-object v0, p0, LX/HXP;->q:LX/Ft5;

    invoke-virtual {v0}, LX/Ft5;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
