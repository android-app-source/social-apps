.class public LX/IlI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:Lcom/facebook/payments/ui/SingleItemInfoView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607631
    new-instance v0, Lcom/facebook/payments/ui/SingleItemInfoView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/ui/SingleItemInfoView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    .line 2607632
    iget-object v0, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/SingleItemInfoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2607633
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1eaa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2607634
    iget-object v1, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    const v2, 0x7f02140f

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/SingleItemInfoView;->setBackgroundResource(I)V

    .line 2607635
    iget-object v1, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/facebook/payments/ui/SingleItemInfoView;->setPadding(IIII)V

    .line 2607636
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607637
    iget-object v0, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 2

    .prologue
    .line 2607638
    iget-object v0, p1, LX/Il0;->b:LX/0am;

    move-object v0, v0

    .line 2607639
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2607640
    iget-object v1, p0, LX/IlI;->a:Lcom/facebook/payments/ui/SingleItemInfoView;

    .line 2607641
    iget-object v0, p1, LX/Il0;->b:LX/0am;

    move-object v0, v0

    .line 2607642
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73f;

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/SingleItemInfoView;->setViewParams(LX/73f;)V

    .line 2607643
    return-void
.end method
