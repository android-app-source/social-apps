.class public LX/HVQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/HVQ;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475034
    iput-object p1, p0, LX/HVQ;->a:Landroid/content/Context;

    .line 2475035
    iput-object p2, p0, LX/HVQ;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2475036
    return-void
.end method

.method public static a(LX/0QB;)LX/HVQ;
    .locals 5

    .prologue
    .line 2475037
    sget-object v0, LX/HVQ;->c:LX/HVQ;

    if-nez v0, :cond_1

    .line 2475038
    const-class v1, LX/HVQ;

    monitor-enter v1

    .line 2475039
    :try_start_0
    sget-object v0, LX/HVQ;->c:LX/HVQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2475040
    if-eqz v2, :cond_0

    .line 2475041
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2475042
    new-instance p0, LX/HVQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, LX/HVQ;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 2475043
    move-object v0, p0

    .line 2475044
    sput-object v0, LX/HVQ;->c:LX/HVQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2475045
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2475046
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2475047
    :cond_1
    sget-object v0, LX/HVQ;->c:LX/HVQ;

    return-object v0

    .line 2475048
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2475049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
