.class public final LX/IHz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IFS;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

.field public final synthetic c:LX/IIB;


# direct methods
.method public constructor <init>(LX/IIB;LX/IFS;Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;)V
    .locals 0

    .prologue
    .line 2561320
    iput-object p1, p0, LX/IHz;->c:LX/IIB;

    iput-object p2, p0, LX/IHz;->a:LX/IFS;

    iput-object p3, p0, LX/IHz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2561321
    iget-object v0, p0, LX/IHz;->a:LX/IFS;

    sget-object v1, LX/IFQ;->NOT_INVITED:LX/IFQ;

    .line 2561322
    iput-object v1, v0, LX/IFS;->e:LX/IFQ;

    .line 2561323
    iget-object v0, p0, LX/IHz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->e()V

    .line 2561324
    iget-object v0, p0, LX/IHz;->c:LX/IIB;

    iget-object v0, v0, LX/IIB;->j:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/IHz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08385d

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/IHz;->a:LX/IFS;

    .line 2561325
    iget-object p0, v6, LX/IFS;->d:Ljava/lang/String;

    move-object v6, p0

    .line 2561326
    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2561327
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2561328
    iget-object v0, p0, LX/IHz;->a:LX/IFS;

    sget-object v1, LX/IFQ;->INVITED:LX/IFQ;

    .line 2561329
    iput-object v1, v0, LX/IFS;->e:LX/IFQ;

    .line 2561330
    iget-object v0, p0, LX/IHz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->e()V

    .line 2561331
    return-void
.end method
