.class public LX/HR0;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 2464112
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2464113
    iput-object p1, p0, LX/HR0;->a:Landroid/view/View;

    .line 2464114
    iput p2, p0, LX/HR0;->b:I

    .line 2464115
    iput p3, p0, LX/HR0;->c:I

    .line 2464116
    return-void
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 2464117
    iget v0, p0, LX/HR0;->b:I

    iget v1, p0, LX/HR0;->c:I

    iget v2, p0, LX/HR0;->b:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 2464118
    iget-object v1, p0, LX/HR0;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2464119
    iget-object v0, p0, LX/HR0;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2464120
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 2464121
    const/4 v0, 0x1

    return v0
.end method
