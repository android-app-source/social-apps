.class public final LX/IDR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/fragment/FriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 0

    .prologue
    .line 2550868
    iput-object p1, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    .line 2550852
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2550853
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 2550854
    :cond_0
    if-lez p3, :cond_3

    .line 2550855
    add-int v0, p2, p3

    .line 2550856
    if-ne v0, p4, :cond_1

    .line 2550857
    iget-object v1, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v1, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getFooterViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2550858
    :cond_1
    iget-object v1, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v2, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget v2, v2, Lcom/facebook/friendlist/fragment/FriendListFragment;->H:I

    iget-object v3, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v3, v3, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    const/4 v4, 0x0

    .line 2550859
    iget-boolean v5, v3, LX/IE2;->c:Z

    if-eqz v5, :cond_2

    invoke-virtual {v3}, LX/IE2;->d()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2550860
    invoke-virtual {v3, v0}, LX/IE2;->getSectionForPosition(I)I

    move-result v5

    iget-boolean p1, v3, LX/IE2;->d:Z

    if-eqz p1, :cond_5

    :goto_0
    add-int/2addr v4, v5

    .line 2550861
    :cond_2
    move v3, v4

    .line 2550862
    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2550863
    iput v0, v1, Lcom/facebook/friendlist/fragment/FriendListFragment;->H:I

    .line 2550864
    :cond_3
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-virtual {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x8

    if-lt v0, p4, :cond_4

    .line 2550865
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-static {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->m(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    .line 2550866
    :cond_4
    return-void

    .line 2550867
    :cond_5
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 2550848
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2550849
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    iget-object v0, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 2550850
    :cond_0
    iget-object v0, p0, LX/IDR;->a:Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-static {v0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->r(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    .line 2550851
    return-void
.end method
