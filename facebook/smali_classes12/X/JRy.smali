.class public final enum LX/JRy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JRy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JRy;

.field public static final enum BUFFERING:LX/JRy;

.field public static final enum PAUSED:LX/JRy;

.field public static final enum PLAYING:LX/JRy;

.field public static final enum STOPPED:LX/JRy;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2694432
    new-instance v0, LX/JRy;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, LX/JRy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JRy;->PLAYING:LX/JRy;

    .line 2694433
    new-instance v0, LX/JRy;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/JRy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JRy;->PAUSED:LX/JRy;

    .line 2694434
    new-instance v0, LX/JRy;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, LX/JRy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JRy;->STOPPED:LX/JRy;

    .line 2694435
    new-instance v0, LX/JRy;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v5}, LX/JRy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JRy;->BUFFERING:LX/JRy;

    .line 2694436
    const/4 v0, 0x4

    new-array v0, v0, [LX/JRy;

    sget-object v1, LX/JRy;->PLAYING:LX/JRy;

    aput-object v1, v0, v2

    sget-object v1, LX/JRy;->PAUSED:LX/JRy;

    aput-object v1, v0, v3

    sget-object v1, LX/JRy;->STOPPED:LX/JRy;

    aput-object v1, v0, v4

    sget-object v1, LX/JRy;->BUFFERING:LX/JRy;

    aput-object v1, v0, v5

    sput-object v0, LX/JRy;->$VALUES:[LX/JRy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2694437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JRy;
    .locals 1

    .prologue
    .line 2694438
    const-class v0, LX/JRy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JRy;

    return-object v0
.end method

.method public static values()[LX/JRy;
    .locals 1

    .prologue
    .line 2694439
    sget-object v0, LX/JRy;->$VALUES:[LX/JRy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JRy;

    return-object v0
.end method
