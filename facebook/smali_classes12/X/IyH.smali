.class public LX/IyH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ixz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/cart/CartDataFetcher$DataFetchListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633098
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IyH;->a:Ljava/util/List;

    .line 2633099
    return-void
.end method

.method public static a(LX/0QB;)LX/IyH;
    .locals 3

    .prologue
    .line 2633079
    const-class v1, LX/IyH;

    monitor-enter v1

    .line 2633080
    :try_start_0
    sget-object v0, LX/IyH;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2633081
    sput-object v2, LX/IyH;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2633082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2633083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2633084
    new-instance v0, LX/IyH;

    invoke-direct {v0}, LX/IyH;-><init>()V

    .line 2633085
    move-object v0, v0

    .line 2633086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2633087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IyH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2633088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2633089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Iy3;)V
    .locals 1

    .prologue
    .line 2633095
    iget-object v0, p0, LX/IyH;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633096
    return-void
.end method

.method public final a(Lcom/facebook/payments/cart/model/PaymentsCartParams;)V
    .locals 1

    .prologue
    .line 2633094
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/cart/model/PaymentsCartParams;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2633093
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2633092
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/Iy3;)V
    .locals 1

    .prologue
    .line 2633090
    iget-object v0, p0, LX/IyH;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2633091
    return-void
.end method
