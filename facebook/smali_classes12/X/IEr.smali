.class public LX/IEr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/stickers/search/StickerSearchLoader;

.field public c:LX/IEq;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerLoader$BirthdayStickerLoaderListener;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/stickers/search/StickerSearchLoader;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553412
    iput-object p1, p0, LX/IEr;->b:Lcom/facebook/stickers/search/StickerSearchLoader;

    .line 2553413
    sget-object v0, LX/IEq;->INIT:LX/IEq;

    iput-object v0, p0, LX/IEr;->c:LX/IEq;

    .line 2553414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IEr;->d:Ljava/util/List;

    .line 2553415
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2553416
    iput-object v0, p0, LX/IEr;->e:LX/0Px;

    .line 2553417
    if-nez p2, :cond_0

    const-string p2, "bday"

    :cond_0
    iput-object p2, p0, LX/IEr;->a:Ljava/lang/String;

    .line 2553418
    return-void
.end method
