.class public LX/Hq7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0jF;",
        ":",
        "LX/0iz;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsCustomPublishModeSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/composer/actionitem/ActionItemController;"
    }
.end annotation


# instance fields
.field private final a:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public final b:LX/HrE;

.field private final c:LX/Hq6;

.field private final d:LX/Hvy;


# direct methods
.method public constructor <init>(LX/0il;LX/HrE;LX/Hq6;LX/Hvy;)V
    .locals 0
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HrE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/actionitem/ActionItemController$Delegate;",
            "LX/Hq6;",
            "LX/Hvy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510254
    iput-object p1, p0, LX/Hq7;->a:LX/0il;

    .line 2510255
    iput-object p2, p0, LX/Hq7;->b:LX/HrE;

    .line 2510256
    iput-object p3, p0, LX/Hq7;->c:LX/Hq6;

    .line 2510257
    iput-object p4, p0, LX/Hq7;->d:LX/Hvy;

    .line 2510258
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbui/widget/contentview/ContentView;)V
    .locals 4

    .prologue
    .line 2510259
    const v0, 0x7f081497

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2510260
    iget-object v0, p0, LX/Hq7;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jF;

    invoke-interface {v0}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v0

    .line 2510261
    sget-object v1, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v0, v1, :cond_1

    .line 2510262
    iget-object v1, p0, LX/Hq7;->d:LX/Hvy;

    iget-object v0, p0, LX/Hq7;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jF;

    check-cast v0, LX/0iz;

    invoke-interface {v0}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/Hvy;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 2510263
    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2510264
    iget-object v0, p0, LX/Hq7;->c:LX/Hq6;

    const v1, 0x7f0207fd

    const/4 v2, 0x1

    .line 2510265
    iget-boolean v3, v0, LX/Hq6;->c:Z

    if-nez v3, :cond_0

    .line 2510266
    iget-object v3, v0, LX/Hq6;->a:Landroid/content/res/Resources;

    const p0, 0x7f0a0512

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v0, LX/Hq6;->d:I

    .line 2510267
    iget-object v3, v0, LX/Hq6;->a:Landroid/content/res/Resources;

    const p0, 0x7f0a0513

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v0, LX/Hq6;->e:I

    .line 2510268
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/Hq6;->c:Z

    .line 2510269
    :cond_0
    if-eqz v2, :cond_2

    iget v3, v0, LX/Hq6;->d:I

    .line 2510270
    :goto_1
    iget-object p0, v0, LX/Hq6;->b:LX/0wM;

    invoke-virtual {p0, v1, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2510271
    return-void

    .line 2510272
    :cond_1
    iget-object v1, p0, LX/Hq7;->d:LX/Hvy;

    iget-object v0, p0, LX/Hq7;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jF;

    invoke-interface {v0}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Hvy;->a(LX/5Rn;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2510273
    :cond_2
    iget v3, v0, LX/Hq6;->e:I

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2510274
    iget-object v0, p0, LX/Hq7;->a:LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->j()Z

    move-result v0

    return v0
.end method
