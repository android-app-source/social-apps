.class public LX/HPW;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/8E5;

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/8E7;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/2iz;

.field public final f:LX/8E2;

.field private final g:LX/8Do;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/String;

.field public final j:LX/HP0;

.field public final k:LX/8EI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:LX/2jY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field private o:Z

.field public final p:LX/CfG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2462213
    sget-object v0, LX/8E5;->INTERACTIVE:LX/8E5;

    sput-object v0, LX/HPW;->a:LX/8E5;

    .line 2462214
    sget-object v0, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sget-object v1, LX/8E7;->PAGES_FIRST_CARD_LOADING_TIMER:LX/8E7;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/HPW;->b:LX/0Rf;

    .line 2462215
    sget-object v0, LX/8E7;->PAGES_DEFAULT_TAB_FRAGMENT_LOADED:LX/8E7;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/HPW;->c:LX/0Rf;

    .line 2462216
    sget-object v0, LX/8E7;->PAGES_FIRST_CARD_DATA_LOADED:LX/8E7;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/HPW;->d:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/HP0;LX/8EI;LX/2iz;LX/8E2;LX/8Do;LX/0Ot;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/HP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/8EI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/pages/common/surface/fragments/reaction/PagesReactionSessionEarlyFetchController$ReactionSessionResultListener;",
            "Lcom/facebook/pages/common/sequencelogger/PagesFirstCardPerfLogger;",
            "LX/2iz;",
            "LX/8E2;",
            "LX/8Do;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2462177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462178
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HPW;->n:Z

    .line 2462179
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HPW;->o:Z

    .line 2462180
    new-instance v0, LX/HPV;

    invoke-direct {v0, p0}, LX/HPV;-><init>(LX/HPW;)V

    iput-object v0, p0, LX/HPW;->p:LX/CfG;

    .line 2462181
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/HPW;->i:Ljava/lang/String;

    .line 2462182
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HP0;

    iput-object v0, p0, LX/HPW;->j:LX/HP0;

    .line 2462183
    iput-object p5, p0, LX/HPW;->k:LX/8EI;

    .line 2462184
    invoke-virtual {p6, p2}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, LX/HPW;->m:LX/2jY;

    .line 2462185
    invoke-virtual {p6, p3}, LX/2iz;->b(Ljava/lang/String;)LX/2jY;

    move-result-object v0

    iput-object v0, p0, LX/HPW;->l:LX/2jY;

    .line 2462186
    iput-object p6, p0, LX/HPW;->e:LX/2iz;

    .line 2462187
    iput-object p7, p0, LX/HPW;->f:LX/8E2;

    .line 2462188
    iput-object p8, p0, LX/HPW;->g:LX/8Do;

    .line 2462189
    iput-object p9, p0, LX/HPW;->h:LX/0Ot;

    .line 2462190
    return-void
.end method

.method public static a(LX/2jY;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2jY;",
            ")",
            "LX/0Px",
            "<",
            "LX/9qT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2462207
    invoke-virtual {p0}, LX/2jY;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2462208
    invoke-virtual {p0}, LX/2jY;->o()LX/0Px;

    move-result-object v0

    .line 2462209
    invoke-virtual {p0}, LX/2jY;->a()V

    .line 2462210
    const/4 v1, 0x1

    .line 2462211
    iput-boolean v1, p0, LX/2jY;->n:Z

    .line 2462212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/HPW;Ljava/util/concurrent/Callable;)LX/8E6;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "LX/8E6;"
        }
    .end annotation

    .prologue
    .line 2462195
    new-instance v0, LX/8E3;

    invoke-direct {v0}, LX/8E3;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/8E3;->a(Ljava/util/concurrent/Callable;Z)LX/8E3;

    move-result-object v0

    sget-object v1, LX/HPW;->a:LX/8E5;

    .line 2462196
    iput-object v1, v0, LX/8E3;->a:LX/8E5;

    .line 2462197
    move-object v0, v0

    .line 2462198
    iget-object v1, p0, LX/HPW;->i:Ljava/lang/String;

    .line 2462199
    iput-object v1, v0, LX/8E3;->b:Ljava/lang/String;

    .line 2462200
    move-object v0, v0

    .line 2462201
    sget-object v1, LX/HPW;->d:LX/0Rf;

    invoke-virtual {v0, v1}, LX/8E3;->b(LX/0Rf;)LX/8E3;

    move-result-object v1

    .line 2462202
    iget-object v0, p0, LX/HPW;->g:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2462203
    sget-object v0, LX/HPW;->c:LX/0Rf;

    invoke-virtual {v1, v0}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    .line 2462204
    :cond_0
    invoke-virtual {v1}, LX/8E3;->a()LX/8E6;

    move-result-object v0

    return-object v0

    .line 2462205
    :cond_1
    sget-object v0, LX/HPW;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    .line 2462206
    invoke-virtual {v1, v0}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    goto :goto_0
.end method

.method public static d(LX/HPW;)V
    .locals 1

    .prologue
    .line 2462191
    iget-boolean v0, p0, LX/HPW;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HPW;->k:LX/8EI;

    if-eqz v0, :cond_0

    .line 2462192
    iget-object v0, p0, LX/HPW;->k:LX/8EI;

    invoke-virtual {v0}, LX/8EI;->d()V

    .line 2462193
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HPW;->o:Z

    .line 2462194
    :cond_0
    return-void
.end method
