.class public final LX/J7c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/storygallerysurvey/protocol/FetchStoryGallerySurveyWithStoryGraphQLModels$FetchStoryGallerySurveyWithStoryQueryModel$StoryGallerySurveyModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/J7f;


# direct methods
.method public constructor <init>(LX/J7f;I)V
    .locals 0

    .prologue
    .line 2651077
    iput-object p1, p0, LX/J7c;->b:LX/J7f;

    iput p2, p0, LX/J7c;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2651078
    iget-object v0, p0, LX/J7c;->b:LX/J7f;

    iget v1, p0, LX/J7c;->a:I

    .line 2651079
    new-instance v2, LX/J85;

    invoke-direct {v2}, LX/J85;-><init>()V

    move-object v2, v2

    .line 2651080
    const-string v3, "survey_query_type"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2651081
    iget-object v3, v0, LX/J7f;->a:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2651082
    new-instance v3, LX/J7e;

    invoke-direct {v3, v0}, LX/J7e;-><init>(LX/J7f;)V

    iget-object p0, v0, LX/J7f;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2651083
    return-object v0
.end method
