.class public LX/I9l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/I9S;

.field public c:LX/Blc;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IA6;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/I9k;

.field private f:I

.field private g:I

.field public h:LX/IA6;

.field private i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0tX;

.field private final k:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final l:LX/0Uh;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Landroid/content/Context;

.field public p:Lcom/facebook/user/model/User;

.field public q:J

.field public r:Z


# direct methods
.method public constructor <init>(LX/0Or;LX/0Uh;LX/0tX;LX/0Tf;Landroid/content/Context;LX/0SG;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p4    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0tX;",
            "LX/0Tf;",
            "Landroid/content/Context;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2543748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2543749
    sget-object v0, LX/I9k;->INITIAL:LX/I9k;

    iput-object v0, p0, LX/I9l;->e:LX/I9k;

    .line 2543750
    iput-object v1, p0, LX/I9l;->a:Ljava/lang/String;

    .line 2543751
    iput-object v1, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2543752
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    .line 2543753
    iput-object p2, p0, LX/I9l;->l:LX/0Uh;

    .line 2543754
    iput-object p3, p0, LX/I9l;->j:LX/0tX;

    .line 2543755
    iput-object p4, p0, LX/I9l;->k:LX/0Tf;

    .line 2543756
    iput-object p5, p0, LX/I9l;->o:Landroid/content/Context;

    .line 2543757
    invoke-interface {p6}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/I9l;->q:J

    .line 2543758
    return-void
.end method

.method private a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/IA6;",
            ">;",
            "Ljava/lang/String;",
            "LX/IA5;",
            ")",
            "LX/IA6;"
        }
    .end annotation

    .prologue
    .line 2543759
    iget-boolean v0, p0, LX/I9l;->r:Z

    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, LX/I9l;->a(LX/0Pz;ZLjava/lang/String;Ljava/util/List;)LX/IA6;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/0Pz;ZLjava/lang/String;Ljava/util/List;)LX/IA6;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/IA6;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/IA5;",
            ">;)",
            "LX/IA6;"
        }
    .end annotation

    .prologue
    .line 2543760
    if-eqz p1, :cond_0

    new-instance v0, LX/IAE;

    invoke-direct {v0, p2, p3}, LX/IAE;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2543761
    :goto_0
    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543762
    return-object v0

    .line 2543763
    :cond_0
    new-instance v0, LX/IAD;

    invoke-direct {v0, p2, p3}, LX/IAD;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static a(LX/I9l;Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 2543764
    if-nez p1, :cond_1

    .line 2543765
    :cond_0
    :goto_0
    return-object v0

    .line 2543766
    :cond_1
    iget-object v1, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    .line 2543767
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2543768
    iget-wide v2, p0, LX/I9l;->q:J

    .line 2543769
    new-instance v6, LX/4yA;

    invoke-direct {v6}, LX/4yA;-><init>()V

    .line 2543770
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_3

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel$EdgesModel;

    .line 2543771
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v9

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v10

    invoke-static {v9, v10}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v9

    .line 2543772
    iget-object v10, v9, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2543773
    invoke-static {v10, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 2543774
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel$EdgesModel;->j()J

    move-result-wide v10

    invoke-static {v10, v11, v2, v3}, LX/Bm1;->a(JJ)LX/Bm0;

    move-result-object v4

    invoke-virtual {v6, v4, v9}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4yA;

    .line 2543775
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 2543776
    :cond_3
    invoke-virtual {v6}, LX/4yA;->a()LX/3CE;

    move-result-object v4

    move-object v1, v4

    .line 2543777
    invoke-static {p0, v1}, LX/I9l;->a(LX/I9l;LX/3CE;)V

    .line 2543778
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    .line 2543779
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 2543780
    if-nez p1, :cond_1

    .line 2543781
    :cond_0
    :goto_0
    return-object v0

    .line 2543782
    :cond_1
    iget-object v1, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    .line 2543783
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2543784
    iget-wide v2, p0, LX/I9l;->q:J

    .line 2543785
    new-instance v6, LX/4yA;

    invoke-direct {v6}, LX/4yA;-><init>()V

    .line 2543786
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v8, :cond_3

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;

    .line 2543787
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v9

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v10

    invoke-static {v9, v10}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v9

    .line 2543788
    iget-object v10, v9, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2543789
    invoke-static {v10, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 2543790
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel$EdgesModel;->j()J

    move-result-wide v10

    invoke-static {v10, v11, v2, v3}, LX/Bm1;->a(JJ)LX/Bm0;

    move-result-object v4

    invoke-virtual {v6, v4, v9}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4yA;

    .line 2543791
    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 2543792
    :cond_3
    invoke-virtual {v6}, LX/4yA;->a()LX/3CE;

    move-result-object v4

    move-object v1, v4

    .line 2543793
    invoke-static {p0, v1}, LX/I9l;->a(LX/I9l;LX/3CE;)V

    .line 2543794
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    .line 2543795
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2543796
    if-nez p1, :cond_0

    .line 2543797
    :goto_0
    return-object v0

    .line 2543798
    :cond_0
    sget-object v1, LX/I9j;->b:[I

    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v2

    invoke-virtual {v2}, LX/IA5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2543799
    :pswitch_0
    sget-object v1, LX/I9j;->a:[I

    .line 2543800
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543801
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 2543802
    :pswitch_1
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel;

    .line 2543803
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543804
    if-nez v0, :cond_14

    .line 2543805
    :cond_1
    :goto_1
    move-object v0, v1

    .line 2543806
    goto :goto_0

    .line 2543807
    :pswitch_2
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;

    .line 2543808
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543809
    if-nez v0, :cond_18

    .line 2543810
    :cond_2
    :goto_2
    move-object v0, v1

    .line 2543811
    goto :goto_0

    .line 2543812
    :pswitch_3
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel;

    .line 2543813
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543814
    if-nez v0, :cond_1c

    .line 2543815
    :cond_3
    :goto_3
    move-object v0, v1

    .line 2543816
    goto :goto_0

    .line 2543817
    :pswitch_4
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel;

    .line 2543818
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543819
    if-nez v0, :cond_20

    .line 2543820
    :cond_4
    :goto_4
    move-object v0, v1

    .line 2543821
    goto :goto_0

    .line 2543822
    :pswitch_5
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel;

    .line 2543823
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543824
    if-nez v0, :cond_24

    .line 2543825
    :cond_5
    :goto_5
    move-object v0, v1

    .line 2543826
    goto :goto_0

    .line 2543827
    :pswitch_6
    sget-object v1, LX/I9j;->a:[I

    .line 2543828
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543829
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    .line 2543830
    :pswitch_7
    sget-object v1, LX/I9j;->a:[I

    .line 2543831
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543832
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    .line 2543833
    :pswitch_8
    sget-object v1, LX/I9j;->a:[I

    .line 2543834
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543835
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4

    .line 2543836
    :pswitch_9
    sget-object v1, LX/I9j;->a:[I

    .line 2543837
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543838
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    .line 2543839
    :sswitch_0
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;

    .line 2543840
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;

    move-result-object v0

    invoke-static {p0, v0}, LX/I9l;->a(LX/I9l;Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMembersQueryModel$AllEventMembersModel;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2543841
    :pswitch_a
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel;

    .line 2543842
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel;

    move-result-object v0

    .line 2543843
    const/4 v1, 0x0

    .line 2543844
    if-nez v0, :cond_28

    .line 2543845
    :cond_6
    :goto_6
    move-object v0, v1

    .line 2543846
    goto/16 :goto_0

    .line 2543847
    :pswitch_b
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel;

    .line 2543848
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel;

    move-result-object v0

    .line 2543849
    const/4 v1, 0x0

    .line 2543850
    if-nez v0, :cond_2c

    .line 2543851
    :cond_7
    :goto_7
    move-object v0, v1

    .line 2543852
    goto/16 :goto_0

    .line 2543853
    :pswitch_c
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;

    .line 2543854
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;

    move-result-object v0

    const/4 v1, 0x0

    .line 2543855
    if-nez v0, :cond_30

    .line 2543856
    :cond_8
    :goto_8
    move-object v0, v1

    .line 2543857
    goto/16 :goto_0

    .line 2543858
    :pswitch_d
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel;

    .line 2543859
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel;

    move-result-object v0

    .line 2543860
    const/4 v1, 0x0

    .line 2543861
    if-nez v0, :cond_34

    .line 2543862
    :cond_9
    :goto_9
    move-object v0, v1

    .line 2543863
    goto/16 :goto_0

    .line 2543864
    :pswitch_e
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;

    .line 2543865
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;

    move-result-object v0

    .line 2543866
    const/4 v1, 0x0

    .line 2543867
    if-nez v0, :cond_38

    .line 2543868
    :cond_a
    :goto_a
    move-object v0, v1

    .line 2543869
    goto/16 :goto_0

    .line 2543870
    :pswitch_f
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;

    .line 2543871
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;

    move-result-object v0

    .line 2543872
    const/4 v3, 0x0

    .line 2543873
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;->a()I

    move-result v1

    if-nez v1, :cond_3c

    .line 2543874
    :cond_b
    :goto_b
    move-object v0, v3

    .line 2543875
    goto/16 :goto_0

    .line 2543876
    :pswitch_10
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;

    .line 2543877
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;

    move-result-object v0

    .line 2543878
    const/4 v3, 0x0

    .line 2543879
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;->a()I

    move-result v1

    if-nez v1, :cond_40

    .line 2543880
    :cond_c
    :goto_c
    move-object v0, v3

    .line 2543881
    goto/16 :goto_0

    .line 2543882
    :pswitch_11
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;

    .line 2543883
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2543884
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2543885
    const/4 v4, 0x0

    .line 2543886
    if-eqz v0, :cond_d

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v2

    if-nez v2, :cond_44

    .line 2543887
    :cond_d
    :goto_d
    move-object v0, v4

    .line 2543888
    goto/16 :goto_0

    .line 2543889
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2543890
    :pswitch_12
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel;

    .line 2543891
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;

    move-result-object v0

    .line 2543892
    const/4 v3, 0x0

    .line 2543893
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->a()I

    move-result v1

    if-nez v1, :cond_4a

    .line 2543894
    :cond_e
    :goto_e
    move-object v0, v3

    .line 2543895
    goto/16 :goto_0

    .line 2543896
    :pswitch_13
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;

    .line 2543897
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;

    move-result-object v0

    .line 2543898
    const/4 v3, 0x0

    .line 2543899
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;->a()I

    move-result v1

    if-nez v1, :cond_4e

    .line 2543900
    :cond_f
    :goto_f
    move-object v0, v3

    .line 2543901
    goto/16 :goto_0

    .line 2543902
    :pswitch_14
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;

    .line 2543903
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$EventSmsAssociatesModel;

    move-result-object v0

    .line 2543904
    const/4 v3, 0x0

    .line 2543905
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$EventSmsAssociatesModel;->a()I

    move-result v1

    if-nez v1, :cond_52

    .line 2543906
    :cond_10
    :goto_10
    move-object v0, v3

    .line 2543907
    goto/16 :goto_0

    .line 2543908
    :pswitch_15
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;

    .line 2543909
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2543910
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2543911
    const/4 v4, 0x0

    .line 2543912
    if-eqz v0, :cond_11

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v2

    if-nez v2, :cond_56

    .line 2543913
    :cond_11
    :goto_11
    move-object v0, v4

    .line 2543914
    goto/16 :goto_0

    .line 2543915
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2543916
    :pswitch_16
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel;

    .line 2543917
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel$EventSmsDeclinesModel;

    move-result-object v0

    .line 2543918
    const/4 v3, 0x0

    .line 2543919
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel$EventSmsDeclinesModel;->a()I

    move-result v1

    if-nez v1, :cond_5c

    .line 2543920
    :cond_12
    :goto_12
    move-object v0, v3

    .line 2543921
    goto/16 :goto_0

    .line 2543922
    :sswitch_1
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;

    .line 2543923
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/I9l;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllMaybesQueryModel$AllEventMaybesModel;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2543924
    :sswitch_2
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;

    .line 2543925
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;

    move-result-object v0

    const/4 v3, 0x0

    .line 2543926
    if-nez v0, :cond_60

    .line 2543927
    :cond_13
    :goto_13
    move-object v0, v3

    .line 2543928
    goto/16 :goto_0

    .line 2543929
    :cond_14
    if-nez v0, :cond_16

    .line 2543930
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543931
    :goto_14
    move-object v2, v2

    .line 2543932
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_15

    .line 2543933
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543934
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543935
    :cond_15
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2543936
    if-eqz v2, :cond_1

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 2543937
    :cond_16
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543938
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_15
    if-ge v3, v6, :cond_17

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel$EdgesModel;

    .line 2543939
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMembersQueryModel$FriendEventMembersModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543940
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_15

    .line 2543941
    :cond_17
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_14

    .line 2543942
    :cond_18
    if-nez v0, :cond_1a

    .line 2543943
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543944
    :goto_16
    move-object v2, v2

    .line 2543945
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_19

    .line 2543946
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543947
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543948
    :cond_19
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2543949
    if-eqz v2, :cond_2

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2543950
    :cond_1a
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543951
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_17
    if-ge v3, v6, :cond_1b

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;

    .line 2543952
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendMaybesQueryModel$FriendEventMaybesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543953
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_17

    .line 2543954
    :cond_1b
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_16

    .line 2543955
    :cond_1c
    if-nez v0, :cond_1e

    .line 2543956
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543957
    :goto_18
    move-object v2, v2

    .line 2543958
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_1d

    .line 2543959
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543960
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543961
    :cond_1d
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2543962
    if-eqz v2, :cond_3

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    .line 2543963
    :cond_1e
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543964
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_19
    if-ge v3, v6, :cond_1f

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;

    .line 2543965
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendWatchersQueryModel$FriendEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    const/4 p1, 0x0

    invoke-static {v2, p1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543966
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_19

    .line 2543967
    :cond_1f
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_18

    .line 2543968
    :cond_20
    if-nez v0, :cond_22

    .line 2543969
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543970
    :goto_1a
    move-object v2, v2

    .line 2543971
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_21

    .line 2543972
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543973
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543974
    :cond_21
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2543975
    if-eqz v2, :cond_4

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 2543976
    :cond_22
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543977
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1b
    if-ge v3, v6, :cond_23

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel$EdgesModel;

    .line 2543978
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendInviteesQueryModel$FriendEventInviteesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543979
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1b

    .line 2543980
    :cond_23
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1a

    .line 2543981
    :cond_24
    if-nez v0, :cond_26

    .line 2543982
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543983
    :goto_1c
    move-object v2, v2

    .line 2543984
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_25

    .line 2543985
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543986
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543987
    :cond_25
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2543988
    if-eqz v2, :cond_5

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    .line 2543989
    :cond_26
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2543990
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1d
    if-ge v3, v6, :cond_27

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;

    .line 2543991
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventFriendDeclinesQueryModel$FriendEventDeclinesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543992
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1d

    .line 2543993
    :cond_27
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1c

    .line 2543994
    :cond_28
    if-nez v0, :cond_2a

    .line 2543995
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2543996
    :goto_1e
    move-object v2, v2

    .line 2543997
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_29

    .line 2543998
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2543999
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544000
    :cond_29
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2544001
    if-eqz v2, :cond_6

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 2544002
    :cond_2a
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2544003
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1f
    if-ge v3, v6, :cond_2b

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;

    .line 2544004
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMembersQueryModel$OtherEventMembersModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544005
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1f

    .line 2544006
    :cond_2b
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1e

    .line 2544007
    :cond_2c
    if-nez v0, :cond_2e

    .line 2544008
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544009
    :goto_20
    move-object v2, v2

    .line 2544010
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_2d

    .line 2544011
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544012
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544013
    :cond_2d
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2544014
    if-eqz v2, :cond_7

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_7

    .line 2544015
    :cond_2e
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2544016
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_21
    if-ge v3, v6, :cond_2f

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel$EdgesModel;

    .line 2544017
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherMaybesQueryModel$OtherEventMaybesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544018
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_21

    .line 2544019
    :cond_2f
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_20

    .line 2544020
    :cond_30
    if-nez v0, :cond_32

    .line 2544021
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544022
    :goto_22
    move-object v2, v2

    .line 2544023
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_31

    .line 2544024
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544025
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544026
    :cond_31
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2544027
    if-eqz v2, :cond_8

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_8

    .line 2544028
    :cond_32
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2544029
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_23
    if-ge v3, v6, :cond_33

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel$EdgesModel;

    .line 2544030
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherWatchersQueryModel$OtherEventWatchersModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object v2

    const/4 p1, 0x0

    invoke-static {v2, p1}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544031
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_23

    .line 2544032
    :cond_33
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_22

    .line 2544033
    :cond_34
    if-nez v0, :cond_36

    .line 2544034
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544035
    :goto_24
    move-object v2, v2

    .line 2544036
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_35

    .line 2544037
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544038
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544039
    :cond_35
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2544040
    if-eqz v2, :cond_9

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_9

    .line 2544041
    :cond_36
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2544042
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_25
    if-ge v3, v6, :cond_37

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel$EdgesModel;

    .line 2544043
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherInviteesQueryModel$OtherEventInviteesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544044
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_25

    .line 2544045
    :cond_37
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_24

    .line 2544046
    :cond_38
    if-nez v0, :cond_3a

    .line 2544047
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544048
    :goto_26
    move-object v2, v2

    .line 2544049
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_39

    .line 2544050
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544051
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544052
    :cond_39
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2544053
    if-eqz v2, :cond_a

    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a

    .line 2544054
    :cond_3a
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2544055
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_27
    if-ge v3, v6, :cond_3b

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel$EdgesModel;

    .line 2544056
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventOtherDeclinesQueryModel$OtherEventDeclinesModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v2

    invoke-static {p1, v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventWithMutualFriendsFragmentModel;Lcom/facebook/graphql/enums/GraphQLEventSeenState;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544057
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_27

    .line 2544058
    :cond_3b
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_26

    .line 2544059
    :cond_3c
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544060
    if-nez v0, :cond_3d

    .line 2544061
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544062
    :goto_28
    move-object v1, v2

    .line 2544063
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 2544064
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544065
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_b

    .line 2544066
    :cond_3d
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544067
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 2544068
    :goto_29
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_3f

    .line 2544069
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "email_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544070
    add-int/lit8 v2, v2, 0x1

    goto :goto_29

    .line 2544071
    :cond_3e
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMembersQueryModel$EventEmailMembersModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v4, v2

    :goto_2a
    if-ge v4, p1, :cond_3f

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    .line 2544072
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544073
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2a

    .line 2544074
    :cond_3f
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_28

    .line 2544075
    :cond_40
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544076
    if-nez v0, :cond_41

    .line 2544077
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544078
    :goto_2b
    move-object v1, v2

    .line 2544079
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 2544080
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544081
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_c

    .line 2544082
    :cond_41
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544083
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_42

    .line 2544084
    :goto_2c
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_43

    .line 2544085
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "email_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544086
    add-int/lit8 v2, v2, 0x1

    goto :goto_2c

    .line 2544087
    :cond_42
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailMaybesQueryModel$EventEmailAssociatesModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    move v4, v2

    :goto_2d
    if-ge v4, p1, :cond_43

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    .line 2544088
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544089
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2d

    .line 2544090
    :cond_43
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_2b

    .line 2544091
    :cond_44
    iget-object v2, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2544092
    if-nez v0, :cond_45

    .line 2544093
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544094
    :goto_2e
    move-object v2, v3

    .line 2544095
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 2544096
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544097
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_d

    .line 2544098
    :cond_45
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2544099
    const-class v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    invoke-virtual {v1, v0, v6, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_46

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    :goto_2f
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_47

    move v3, v5

    .line 2544100
    :goto_30
    invoke-virtual {v1, v0, v5}, LX/15i;->j(II)I

    move-result v6

    if-ge v3, v6, :cond_49

    .line 2544101
    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "email_guest_"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object p1, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    invoke-static {v6, p1, v2}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544102
    add-int/lit8 v3, v3, 0x1

    goto :goto_30

    .line 2544103
    :cond_46
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544104
    goto :goto_2f

    .line 2544105
    :cond_47
    const-class v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    invoke-virtual {v1, v0, v6, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_48

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v6, v3

    :goto_31
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    :goto_32
    if-ge v5, p1, :cond_49

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    .line 2544106
    invoke-static {v3}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544107
    add-int/lit8 v5, v5, 0x1

    goto :goto_32

    .line 2544108
    :cond_48
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544109
    move-object v6, v3

    goto :goto_31

    .line 2544110
    :cond_49
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto :goto_2e

    .line 2544111
    :cond_4a
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544112
    if-nez v0, :cond_4b

    .line 2544113
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544114
    :goto_33
    move-object v1, v2

    .line 2544115
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 2544116
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544117
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_e

    .line 2544118
    :cond_4b
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544119
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4c

    .line 2544120
    :goto_34
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_4d

    .line 2544121
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "email_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->EMAIL_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544122
    add-int/lit8 v2, v2, 0x1

    goto :goto_34

    .line 2544123
    :cond_4c
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailDeclinesQueryModel$EventEmailDeclinesModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_35
    if-ge v4, v7, :cond_4d

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;

    .line 2544124
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventEmailInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544125
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_35

    .line 2544126
    :cond_4d
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_33

    .line 2544127
    :cond_4e
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544128
    if-nez v0, :cond_4f

    .line 2544129
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544130
    :goto_36
    move-object v1, v2

    .line 2544131
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 2544132
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544133
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_f

    .line 2544134
    :cond_4f
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544135
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_50

    .line 2544136
    :goto_37
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_51

    .line 2544137
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "sms_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544138
    add-int/lit8 v2, v2, 0x1

    goto :goto_37

    .line 2544139
    :cond_50
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMembersQueryModel$EventSmsMembersModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_38
    if-ge v4, v7, :cond_51

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    .line 2544140
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544141
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_38

    .line 2544142
    :cond_51
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_36

    .line 2544143
    :cond_52
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544144
    if-nez v0, :cond_53

    .line 2544145
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544146
    :goto_39
    move-object v1, v2

    .line 2544147
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 2544148
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544149
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_10

    .line 2544150
    :cond_53
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544151
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$EventSmsAssociatesModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_54

    .line 2544152
    :goto_3a
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$EventSmsAssociatesModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_55

    .line 2544153
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "sms_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544154
    add-int/lit8 v2, v2, 0x1

    goto :goto_3a

    .line 2544155
    :cond_54
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSMaybesQueryModel$EventSmsAssociatesModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_3b
    if-ge v4, v7, :cond_55

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    .line 2544156
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544157
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3b

    .line 2544158
    :cond_55
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_39

    .line 2544159
    :cond_56
    iget-object v2, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2544160
    if-nez v0, :cond_57

    .line 2544161
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544162
    :goto_3c
    move-object v2, v3

    .line 2544163
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    if-eqz v3, :cond_11

    .line 2544164
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544165
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_11

    .line 2544166
    :cond_57
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2544167
    const-class v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    invoke-virtual {v1, v0, v6, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_58

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    :goto_3d
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_59

    move v3, v5

    .line 2544168
    :goto_3e
    invoke-virtual {v1, v0, v5}, LX/15i;->j(II)I

    move-result v6

    if-ge v3, v6, :cond_5b

    .line 2544169
    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "sms_guest_"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object p1, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    invoke-static {v6, p1, v2}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544170
    add-int/lit8 v3, v3, 0x1

    goto :goto_3e

    .line 2544171
    :cond_58
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544172
    goto :goto_3d

    .line 2544173
    :cond_59
    const-class v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    invoke-virtual {v1, v0, v6, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_5a

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v6, v3

    :goto_3f
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    :goto_40
    if-ge v5, p1, :cond_5b

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    .line 2544174
    invoke-static {v3}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544175
    add-int/lit8 v5, v5, 0x1

    goto :goto_40

    .line 2544176
    :cond_5a
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2544177
    move-object v6, v3

    goto :goto_3f

    .line 2544178
    :cond_5b
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto :goto_3c

    .line 2544179
    :cond_5c
    iget-object v1, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    .line 2544180
    if-nez v0, :cond_5d

    .line 2544181
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2544182
    :goto_41
    move-object v1, v2

    .line 2544183
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 2544184
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/IA6;->a(Ljava/util/List;)V

    .line 2544185
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    goto/16 :goto_12

    .line 2544186
    :cond_5d
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2544187
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel$EventSmsDeclinesModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5e

    .line 2544188
    :goto_42
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel$EventSmsDeclinesModel;->a()I

    move-result v4

    if-ge v2, v4, :cond_5f

    .line 2544189
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "sms_guest_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v6, LX/7vJ;->SMS_SYNTHETIC:LX/7vJ;

    invoke-static {v4, v6, v1}, LX/Bm1;->a(Ljava/lang/String;LX/7vJ;Landroid/content/res/Resources;)Lcom/facebook/events/model/EventUser;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544190
    add-int/lit8 v2, v2, 0x1

    goto :goto_42

    .line 2544191
    :cond_5e
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSDeclinesQueryModel$EventSmsDeclinesModel;->j()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    :goto_43
    if-ge v4, v7, :cond_5f

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;

    .line 2544192
    invoke-static {v2}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventSMSInviteeFragmentModel;)Lcom/facebook/events/model/EventUser;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2544193
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_43

    .line 2544194
    :cond_5f
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_41

    .line 2544195
    :cond_60
    iget-object v4, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    .line 2544196
    iget-object v5, v4, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2544197
    iget-wide v5, p0, LX/I9l;->q:J

    invoke-static {v0, v4, v5, v6}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;Ljava/lang/String;J)LX/3CE;

    move-result-object v4

    .line 2544198
    invoke-static {p0, v4}, LX/I9l;->a(LX/I9l;LX/3CE;)V

    .line 2544199
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventAllDeclinesQueryModel$AllEventDeclinesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v4

    .line 2544200
    if-eqz v4, :cond_13

    invoke-interface {v4}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_13

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_f
        :pswitch_8
        :pswitch_10
        :pswitch_8
        :pswitch_11
        :pswitch_8
        :pswitch_12
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_13
        :pswitch_9
        :pswitch_14
        :pswitch_9
        :pswitch_15
        :pswitch_9
        :pswitch_16
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method

.method private a(LX/0Pz;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/IA6;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2544201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2544202
    if-eqz p2, :cond_0

    .line 2544203
    sget-object v1, LX/IA5;->EMAILS:LX/IA5;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2544204
    :cond_0
    if-eqz p3, :cond_1

    .line 2544205
    sget-object v1, LX/IA5;->SMS:LX/IA5;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2544206
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2544207
    const/4 v1, 0x0

    iget-object v2, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081f23

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2, v0}, LX/I9l;->a(LX/0Pz;ZLjava/lang/String;Ljava/util/List;)LX/IA6;

    .line 2544208
    :cond_2
    return-void
.end method

.method public static a(LX/I9l;LX/3CE;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3CE",
            "<",
            "LX/Bm0;",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2544209
    sget-object v0, LX/I9j;->b:[I

    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v1

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2544210
    :goto_0
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2544211
    return-void

    .line 2544212
    :pswitch_0
    sget-object v0, LX/IA5;->TODAY:LX/IA5;

    sget-object v1, LX/Bm0;->TODAY:LX/Bm0;

    invoke-virtual {p1, v1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/I9l;->a(LX/IA5;LX/0Px;)V

    .line 2544213
    :pswitch_1
    sget-object v0, LX/IA5;->YESTERDAY:LX/IA5;

    sget-object v1, LX/Bm0;->YESTERDAY:LX/Bm0;

    invoke-virtual {p1, v1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/I9l;->a(LX/IA5;LX/0Px;)V

    .line 2544214
    :pswitch_2
    sget-object v0, LX/IA5;->EARLIER:LX/IA5;

    sget-object v1, LX/Bm0;->EARLIER:LX/Bm0;

    invoke-virtual {p1, v1}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/I9l;->a(LX/IA5;LX/0Px;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/I9l;ZZZZ)V
    .locals 5

    .prologue
    .line 2543725
    iget-object v0, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2543726
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2543727
    if-eqz p1, :cond_0

    .line 2543728
    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, LX/IA5;->SELF:LX/IA5;

    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/I9l;->a(LX/0Pz;ZLjava/lang/String;Ljava/util/List;)LX/IA6;

    .line 2543729
    :cond_0
    if-eqz p2, :cond_1

    .line 2543730
    const v2, 0x7f081f24

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/IA5;->TODAY:LX/IA5;

    invoke-direct {p0, v1, v2, v3}, LX/I9l;->a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;

    move-result-object v2

    iput-object v2, p0, LX/I9l;->h:LX/IA6;

    .line 2543731
    const v2, 0x7f081f25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/IA5;->YESTERDAY:LX/IA5;

    invoke-direct {p0, v1, v2, v3}, LX/I9l;->a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;

    .line 2543732
    const v2, 0x7f081f26

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/IA5;->EARLIER:LX/IA5;

    invoke-direct {p0, v1, v0, v2}, LX/I9l;->a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;

    .line 2543733
    invoke-direct {p0, v1, p3, p4}, LX/I9l;->a(LX/0Pz;ZZ)V

    .line 2543734
    :goto_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I9l;->d:LX/0Px;

    .line 2543735
    return-void

    .line 2543736
    :cond_1
    const v2, 0x7f081f21

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/IA5;->FRIENDS:LX/IA5;

    invoke-direct {p0, v1, v2, v3}, LX/I9l;->a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;

    .line 2543737
    const v2, 0x7f081f22

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, LX/IA5;->NON_FRIENDS:LX/IA5;

    invoke-direct {p0, v1, v0, v2}, LX/I9l;->a(LX/0Pz;Ljava/lang/String;LX/IA5;)LX/IA6;

    .line 2543738
    invoke-direct {p0, v1, p3, p4}, LX/I9l;->a(LX/0Pz;ZZ)V

    goto :goto_0
.end method

.method private a(LX/IA5;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IA5;",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2543739
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2543740
    :goto_0
    return-void

    .line 2543741
    :cond_0
    invoke-direct {p0}, LX/I9l;->g()V

    .line 2543742
    :cond_1
    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v0

    .line 2543743
    if-eq v0, p1, :cond_2

    .line 2543744
    sget-object v1, LX/IA5;->TODAY:LX/IA5;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/IA5;->YESTERDAY:LX/IA5;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/IA5;->EARLIER:LX/IA5;

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 2543745
    :cond_2
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/IA6;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/I9l;
    .locals 7

    .prologue
    .line 2543746
    new-instance v0, LX/I9l;

    const/16 v1, 0x12cb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, LX/0Tf;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct/range {v0 .. v6}, LX/I9l;-><init>(LX/0Or;LX/0Uh;LX/0tX;LX/0Tf;Landroid/content/Context;LX/0SG;)V

    .line 2543747
    return-object v0
.end method

.method public static e(LX/I9l;)LX/IA6;
    .locals 2

    .prologue
    .line 2543440
    iget v0, p0, LX/I9l;->f:I

    iget-object v1, p0, LX/I9l;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/I9l;->d:LX/0Px;

    iget v1, p0, LX/I9l;->f:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/I9l;)LX/IA5;
    .locals 2

    .prologue
    .line 2543436
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v0

    .line 2543437
    if-eqz v0, :cond_0

    iget v1, p0, LX/I9l;->g:I

    .line 2543438
    iget-object p0, v0, LX/IA6;->b:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ge v1, p0, :cond_1

    iget-object p0, v0, LX/IA6;->b:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/IA5;

    :goto_0
    move-object v0, p0

    .line 2543439
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 2543441
    iget v0, p0, LX/I9l;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/I9l;->g:I

    .line 2543442
    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2543443
    const/4 v0, 0x0

    iput v0, p0, LX/I9l;->g:I

    .line 2543444
    iget v0, p0, LX/I9l;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/I9l;->f:I

    .line 2543445
    :cond_0
    return-void
.end method

.method private h()LX/0gW;
    .locals 2

    .prologue
    .line 2543446
    sget-object v0, LX/I9j;->b:[I

    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v1

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2543447
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2543448
    :pswitch_0
    sget-object v0, LX/I9j;->a:[I

    .line 2543449
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543450
    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2543451
    :pswitch_1
    new-instance v0, LX/7nr;

    invoke-direct {v0}, LX/7nr;-><init>()V

    move-object v0, v0

    .line 2543452
    goto :goto_1

    .line 2543453
    :pswitch_2
    new-instance v0, LX/7nq;

    invoke-direct {v0}, LX/7nq;-><init>()V

    move-object v0, v0

    .line 2543454
    goto :goto_1

    .line 2543455
    :pswitch_3
    new-instance v0, LX/7ns;

    invoke-direct {v0}, LX/7ns;-><init>()V

    move-object v0, v0

    .line 2543456
    goto :goto_1

    .line 2543457
    :pswitch_4
    new-instance v0, LX/7np;

    invoke-direct {v0}, LX/7np;-><init>()V

    move-object v0, v0

    .line 2543458
    goto :goto_1

    .line 2543459
    :pswitch_5
    new-instance v0, LX/7no;

    invoke-direct {v0}, LX/7no;-><init>()V

    move-object v0, v0

    .line 2543460
    goto :goto_1

    .line 2543461
    :pswitch_6
    sget-object v0, LX/I9j;->a:[I

    .line 2543462
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543463
    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 2543464
    :pswitch_7
    new-instance v0, LX/7o0;

    invoke-direct {v0}, LX/7o0;-><init>()V

    move-object v0, v0

    .line 2543465
    goto :goto_1

    .line 2543466
    :pswitch_8
    new-instance v0, LX/7nz;

    invoke-direct {v0}, LX/7nz;-><init>()V

    move-object v0, v0

    .line 2543467
    goto :goto_1

    .line 2543468
    :pswitch_9
    new-instance v0, LX/7o1;

    invoke-direct {v0}, LX/7o1;-><init>()V

    move-object v0, v0

    .line 2543469
    goto :goto_1

    .line 2543470
    :pswitch_a
    new-instance v0, LX/7ny;

    invoke-direct {v0}, LX/7ny;-><init>()V

    move-object v0, v0

    .line 2543471
    goto :goto_1

    .line 2543472
    :pswitch_b
    new-instance v0, LX/7nx;

    invoke-direct {v0}, LX/7nx;-><init>()V

    move-object v0, v0

    .line 2543473
    goto :goto_1

    .line 2543474
    :pswitch_c
    sget-object v0, LX/I9j;->a:[I

    .line 2543475
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543476
    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    :pswitch_d
    goto :goto_0

    .line 2543477
    :pswitch_e
    new-instance v0, LX/7nn;

    invoke-direct {v0}, LX/7nn;-><init>()V

    move-object v0, v0

    .line 2543478
    goto :goto_1

    .line 2543479
    :pswitch_f
    new-instance v0, LX/7nm;

    invoke-direct {v0}, LX/7nm;-><init>()V

    move-object v0, v0

    .line 2543480
    goto/16 :goto_1

    .line 2543481
    :pswitch_10
    new-instance v0, LX/7nl;

    invoke-direct {v0}, LX/7nl;-><init>()V

    move-object v0, v0

    .line 2543482
    goto/16 :goto_1

    .line 2543483
    :pswitch_11
    new-instance v0, LX/7nk;

    invoke-direct {v0}, LX/7nk;-><init>()V

    move-object v0, v0

    .line 2543484
    goto/16 :goto_1

    .line 2543485
    :pswitch_12
    sget-object v0, LX/I9j;->a:[I

    .line 2543486
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543487
    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    :pswitch_13
    goto/16 :goto_0

    .line 2543488
    :pswitch_14
    new-instance v0, LX/7o7;

    invoke-direct {v0}, LX/7o7;-><init>()V

    move-object v0, v0

    .line 2543489
    goto/16 :goto_1

    .line 2543490
    :pswitch_15
    new-instance v0, LX/7o6;

    invoke-direct {v0}, LX/7o6;-><init>()V

    move-object v0, v0

    .line 2543491
    goto/16 :goto_1

    .line 2543492
    :pswitch_16
    new-instance v0, LX/7o5;

    invoke-direct {v0}, LX/7o5;-><init>()V

    move-object v0, v0

    .line 2543493
    goto/16 :goto_1

    .line 2543494
    :pswitch_17
    new-instance v0, LX/7o4;

    invoke-direct {v0}, LX/7o4;-><init>()V

    move-object v0, v0

    .line 2543495
    goto/16 :goto_1

    .line 2543496
    :pswitch_18
    sget-object v0, LX/I9j;->a:[I

    .line 2543497
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543498
    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 2543499
    :sswitch_0
    new-instance v0, LX/7nj;

    invoke-direct {v0}, LX/7nj;-><init>()V

    move-object v0, v0

    .line 2543500
    goto/16 :goto_1

    .line 2543501
    :sswitch_1
    new-instance v0, LX/7ni;

    invoke-direct {v0}, LX/7ni;-><init>()V

    move-object v0, v0

    .line 2543502
    goto/16 :goto_1

    .line 2543503
    :sswitch_2
    new-instance v0, LX/7nh;

    invoke-direct {v0}, LX/7nh;-><init>()V

    move-object v0, v0

    .line 2543504
    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_c
        :pswitch_12
        :pswitch_18
        :pswitch_18
        :pswitch_18
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_f
        :pswitch_d
        :pswitch_10
        :pswitch_d
        :pswitch_11
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_15
        :pswitch_13
        :pswitch_16
        :pswitch_13
        :pswitch_17
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method

.method public static j(LX/I9l;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2543505
    iget-object v0, p0, LX/I9l;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2543506
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static l(LX/I9l;)V
    .locals 5

    .prologue
    .line 2543507
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2543508
    iget-object v0, p0, LX/I9l;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/I9l;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IA6;

    .line 2543509
    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2543510
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543511
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2543512
    :cond_1
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/I9S;->a(LX/0Px;)V

    .line 2543513
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    invoke-interface {v0}, LX/I9S;->c()V

    .line 2543514
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 4

    .prologue
    .line 2543515
    iget-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2543516
    :goto_0
    return-void

    .line 2543517
    :cond_0
    iget-object v0, p0, LX/I9l;->e:LX/I9k;

    sget-object v1, LX/I9k;->COMPLETE:LX/I9k;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/I9l;->e:LX/I9k;

    sget-object v1, LX/I9k;->ERROR:LX/I9k;

    if-ne v0, v1, :cond_2

    .line 2543518
    :cond_1
    invoke-direct {p0}, LX/I9l;->g()V

    .line 2543519
    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2543520
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/I9S;->a(Z)V

    .line 2543521
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    .line 2543522
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543523
    invoke-interface {v0, v1}, LX/I9S;->a(LX/Blc;)V

    goto :goto_0

    .line 2543524
    :cond_2
    invoke-direct {p0}, LX/I9l;->h()LX/0gW;

    move-result-object v0

    .line 2543525
    if-eqz v0, :cond_3

    .line 2543526
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/I9S;->a(Z)V

    .line 2543527
    invoke-direct {p0}, LX/I9l;->h()LX/0gW;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2543528
    sget-object v1, LX/I9j;->b:[I

    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v2

    invoke-virtual {v2}, LX/IA5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2543529
    :goto_1
    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    .line 2543530
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2543531
    iget-object v1, p0, LX/I9l;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2543532
    iget-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/I9l;->k:LX/0Tf;

    invoke-static {v0, p0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2543533
    :cond_3
    invoke-static {p0}, LX/I9l;->f(LX/I9l;)LX/IA5;

    move-result-object v0

    sget-object v1, LX/IA5;->SELF:LX/IA5;

    if-ne v0, v1, :cond_4

    .line 2543534
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2543535
    new-instance v0, LX/7vI;

    invoke-direct {v0}, LX/7vI;-><init>()V

    sget-object v1, LX/7vJ;->USER:LX/7vJ;

    .line 2543536
    iput-object v1, v0, LX/7vI;->a:LX/7vJ;

    .line 2543537
    move-object v0, v0

    .line 2543538
    iget-object v1, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    .line 2543539
    iget-object v2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2543540
    iput-object v1, v0, LX/7vI;->c:Ljava/lang/String;

    .line 2543541
    move-object v0, v0

    .line 2543542
    iget-object v1, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    .line 2543543
    iput-object v1, v0, LX/7vI;->b:Ljava/lang/String;

    .line 2543544
    move-object v0, v0

    .line 2543545
    iget-object v1, p0, LX/I9l;->p:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    .line 2543546
    iput-object v1, v0, LX/7vI;->d:Ljava/lang/String;

    .line 2543547
    move-object v0, v0

    .line 2543548
    const/4 v1, 0x0

    .line 2543549
    iput-object v1, v0, LX/7vI;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2543550
    move-object v0, v0

    .line 2543551
    invoke-virtual {v0}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v0

    .line 2543552
    invoke-static {p0}, LX/I9l;->e(LX/I9l;)LX/IA6;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/IA6;->a(Lcom/facebook/events/model/EventUser;)V

    .line 2543553
    invoke-static {p0}, LX/I9l;->l(LX/I9l;)V

    .line 2543554
    :cond_4
    sget-object v0, LX/I9k;->COMPLETE:LX/I9k;

    iput-object v0, p0, LX/I9l;->e:LX/I9k;

    .line 2543555
    invoke-virtual {p0}, LX/I9l;->c()V

    goto/16 :goto_0

    .line 2543556
    :pswitch_0
    sget-object v1, LX/I9j;->a:[I

    .line 2543557
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543558
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 2543559
    :pswitch_1
    new-instance v1, LX/7nr;

    invoke-direct {v1}, LX/7nr;-><init>()V

    .line 2543560
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543561
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543562
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 2543563
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543564
    :cond_5
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543565
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543566
    goto/16 :goto_2

    .line 2543567
    :pswitch_2
    new-instance v1, LX/7nq;

    invoke-direct {v1}, LX/7nq;-><init>()V

    .line 2543568
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543569
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543570
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2543571
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543572
    :cond_6
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543573
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543574
    goto/16 :goto_2

    .line 2543575
    :pswitch_3
    new-instance v1, LX/7ns;

    invoke-direct {v1}, LX/7ns;-><init>()V

    .line 2543576
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543577
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543578
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 2543579
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543580
    :cond_7
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543581
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543582
    goto/16 :goto_2

    .line 2543583
    :pswitch_4
    new-instance v1, LX/7np;

    invoke-direct {v1}, LX/7np;-><init>()V

    .line 2543584
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543585
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543586
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 2543587
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543588
    :cond_8
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543589
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543590
    goto/16 :goto_2

    .line 2543591
    :pswitch_5
    new-instance v1, LX/7no;

    invoke-direct {v1}, LX/7no;-><init>()V

    .line 2543592
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543593
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543594
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 2543595
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543596
    :cond_9
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543597
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543598
    goto/16 :goto_2

    .line 2543599
    :pswitch_6
    sget-object v1, LX/I9j;->a:[I

    .line 2543600
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543601
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto/16 :goto_1

    .line 2543602
    :pswitch_7
    new-instance v1, LX/7o0;

    invoke-direct {v1}, LX/7o0;-><init>()V

    .line 2543603
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543604
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543605
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 2543606
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543607
    :cond_a
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543608
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543609
    goto/16 :goto_2

    .line 2543610
    :pswitch_8
    new-instance v1, LX/7nz;

    invoke-direct {v1}, LX/7nz;-><init>()V

    .line 2543611
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543612
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543613
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 2543614
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543615
    :cond_b
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543616
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543617
    goto/16 :goto_2

    .line 2543618
    :pswitch_9
    new-instance v1, LX/7o1;

    invoke-direct {v1}, LX/7o1;-><init>()V

    .line 2543619
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543620
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543621
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 2543622
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543623
    :cond_c
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543624
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543625
    goto/16 :goto_2

    .line 2543626
    :pswitch_a
    new-instance v1, LX/7ny;

    invoke-direct {v1}, LX/7ny;-><init>()V

    .line 2543627
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543628
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543629
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_d

    .line 2543630
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543631
    :cond_d
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543632
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543633
    goto/16 :goto_2

    .line 2543634
    :pswitch_b
    new-instance v1, LX/7nx;

    invoke-direct {v1}, LX/7nx;-><init>()V

    .line 2543635
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543636
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543637
    iget-object v2, p0, LX/I9l;->a:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 2543638
    const-string v2, "after_cursor"

    iget-object v3, p0, LX/I9l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543639
    :cond_e
    const-string v2, "first_count"

    const-string v3, "20"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543640
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543641
    goto/16 :goto_2

    .line 2543642
    :pswitch_c
    sget-object v1, LX/I9j;->a:[I

    .line 2543643
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543644
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3

    :pswitch_d
    goto/16 :goto_1

    .line 2543645
    :pswitch_e
    new-instance v1, LX/7nn;

    invoke-direct {v1}, LX/7nn;-><init>()V

    .line 2543646
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543647
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543648
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543649
    goto/16 :goto_2

    .line 2543650
    :pswitch_f
    new-instance v1, LX/7nm;

    invoke-direct {v1}, LX/7nm;-><init>()V

    .line 2543651
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543652
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543653
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543654
    goto/16 :goto_2

    .line 2543655
    :pswitch_10
    new-instance v1, LX/7nl;

    invoke-direct {v1}, LX/7nl;-><init>()V

    .line 2543656
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543657
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543658
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543659
    goto/16 :goto_2

    .line 2543660
    :pswitch_11
    new-instance v1, LX/7nk;

    invoke-direct {v1}, LX/7nk;-><init>()V

    .line 2543661
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543662
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543663
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543664
    goto/16 :goto_2

    .line 2543665
    :pswitch_12
    sget-object v1, LX/I9j;->a:[I

    .line 2543666
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543667
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4

    :pswitch_13
    goto/16 :goto_1

    .line 2543668
    :pswitch_14
    new-instance v1, LX/7o7;

    invoke-direct {v1}, LX/7o7;-><init>()V

    .line 2543669
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543670
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543671
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543672
    goto/16 :goto_2

    .line 2543673
    :pswitch_15
    new-instance v1, LX/7o6;

    invoke-direct {v1}, LX/7o6;-><init>()V

    .line 2543674
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543675
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543676
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543677
    goto/16 :goto_2

    .line 2543678
    :pswitch_16
    new-instance v1, LX/7o5;

    invoke-direct {v1}, LX/7o5;-><init>()V

    .line 2543679
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543680
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543681
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543682
    goto/16 :goto_2

    .line 2543683
    :pswitch_17
    new-instance v1, LX/7o4;

    invoke-direct {v1}, LX/7o4;-><init>()V

    .line 2543684
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543685
    const-string v2, "profile_pic_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543686
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543687
    goto/16 :goto_2

    .line 2543688
    :pswitch_18
    sget-object v1, LX/I9j;->a:[I

    .line 2543689
    iget-object v2, p0, LX/I9l;->c:LX/Blc;

    move-object v2, v2

    .line 2543690
    invoke-virtual {v2}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_1

    .line 2543691
    :sswitch_0
    new-instance v1, LX/7nj;

    invoke-direct {v1}, LX/7nj;-><init>()V

    .line 2543692
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543693
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543694
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543695
    goto/16 :goto_2

    .line 2543696
    :sswitch_1
    new-instance v1, LX/7ni;

    invoke-direct {v1}, LX/7ni;-><init>()V

    .line 2543697
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543698
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543699
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543700
    goto/16 :goto_2

    .line 2543701
    :sswitch_2
    new-instance v1, LX/7nh;

    invoke-direct {v1}, LX/7nh;-><init>()V

    .line 2543702
    const-string v2, "event_id"

    iget-object v3, p0, LX/I9l;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543703
    const-string v2, "profile_image_size"

    invoke-static {p0}, LX/I9l;->j(LX/I9l;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2543704
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2543705
    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_c
        :pswitch_12
        :pswitch_18
        :pswitch_18
        :pswitch_18
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_f
        :pswitch_d
        :pswitch_10
        :pswitch_d
        :pswitch_11
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_15
        :pswitch_13
        :pswitch_16
        :pswitch_13
        :pswitch_17
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x7 -> :sswitch_2
    .end sparse-switch
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2543706
    iget-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2543707
    iget-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2543708
    :cond_0
    return-void
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2543709
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error while fetching event guest list "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2543710
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/I9S;->a(Z)V

    .line 2543711
    sget-object v0, LX/I9k;->ERROR:LX/I9k;

    iput-object v0, p0, LX/I9l;->e:LX/I9k;

    .line 2543712
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2543713
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2543714
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2543715
    invoke-direct {p0, v0}, LX/I9l;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I9l;->a:Ljava/lang/String;

    .line 2543716
    iget-object v0, p0, LX/I9l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, LX/I9k;->COMPLETE:LX/I9k;

    :goto_0
    iput-object v0, p0, LX/I9l;->e:LX/I9k;

    .line 2543717
    const/4 v0, 0x0

    iput-object v0, p0, LX/I9l;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2543718
    iget-object v0, p0, LX/I9l;->e:LX/I9k;

    sget-object v1, LX/I9k;->COMPLETE:LX/I9k;

    if-ne v0, v1, :cond_1

    .line 2543719
    invoke-virtual {p0}, LX/I9l;->c()V

    .line 2543720
    :goto_1
    return-void

    .line 2543721
    :cond_0
    sget-object v0, LX/I9k;->PAGING:LX/I9k;

    goto :goto_0

    .line 2543722
    :cond_1
    iget-object v0, p0, LX/I9l;->b:LX/I9S;

    .line 2543723
    iget-object v1, p0, LX/I9l;->c:LX/Blc;

    move-object v1, v1

    .line 2543724
    invoke-interface {v0, v1}, LX/I9S;->a(LX/Blc;)V

    goto :goto_1
.end method
