.class public final LX/J75;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Landroid/widget/Button;

.field public final synthetic b:Lcom/facebook/profile/inforequest/InfoRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 2650502
    iput-object p1, p0, LX/J75;->b:Lcom/facebook/profile/inforequest/InfoRequestFragment;

    iput-object p2, p0, LX/J75;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2650503
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2650504
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2650505
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2650506
    :goto_0
    iget-object v1, p0, LX/J75;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2650507
    return-void

    .line 2650508
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
