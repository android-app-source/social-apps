.class public final LX/IsM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;",
        "LX/0Px",
        "<",
        "LX/IsP;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IsN;


# direct methods
.method public constructor <init>(LX/IsN;)V
    .locals 0

    .prologue
    .line 2619654
    iput-object p1, p0, LX/IsM;->a:LX/IsN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2619655
    check-cast p1, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;

    .line 2619656
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2619657
    iget-object v0, p0, LX/IsM;->a:LX/IsN;

    .line 2619658
    invoke-virtual {p1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;->a()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2619659
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2619660
    :goto_0
    move-object v0, v1

    .line 2619661
    return-object v0

    .line 2619662
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel;->a()Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    .line 2619663
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2619664
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;

    .line 2619665
    iget-object v6, v0, LX/IsN;->d:LX/3Mv;

    .line 2619666
    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 2619667
    const/4 v7, 0x0

    .line 2619668
    :goto_2
    move-object v1, v7

    .line 2619669
    if-eqz v1, :cond_1

    .line 2619670
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2619671
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2619672
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 2619673
    :sswitch_0
    new-instance v7, LX/IsP;

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2619674
    iget-object v10, v6, LX/3Mv;->a:LX/3Mw;

    iget-object p0, v6, LX/3Mv;->a:LX/3Mw;

    iget-object v9, v6, LX/3Mv;->b:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    invoke-virtual {p0, v1, v9}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object p0

    const/4 p1, 0x0

    iget-object v9, v6, LX/3Mv;->b:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/user/model/User;

    invoke-virtual {v10, p0, v1, p1, v9}, LX/3Mw;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/5Vt;LX/0P1;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v9

    move-object v9, v9

    .line 2619675
    invoke-direct {v7, v8, v9}, LX/IsP;-><init>(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)V

    goto :goto_2

    .line 2619676
    :sswitch_1
    new-instance v7, LX/IsP;

    invoke-virtual {v1}, Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-static {v1}, LX/3Mv;->b(Lcom/facebook/messaging/search/graphql/MessengerSearchQueriesModels$SearchEntitiesNamedQueryModel$SearchResultsModel$NodesModel;)Lcom/facebook/user/model/User;

    move-result-object v9

    invoke-direct {v7, v8, v9}, LX/IsP;-><init>(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2c24372f -> :sswitch_0
        0x285feb -> :sswitch_1
    .end sparse-switch
.end method
