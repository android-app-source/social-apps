.class public final LX/JEd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0yJ;


# instance fields
.field public final synthetic a:LX/JEe;


# direct methods
.method public constructor <init>(LX/JEe;)V
    .locals 0

    .prologue
    .line 2666139
    iput-object p1, p0, LX/JEd;->a:LX/JEe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2666140
    iget-object v1, p0, LX/JEd;->a:LX/JEe;

    iget-object v1, v1, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v1, v1, Lcom/facebook/zero/settings/FlexSettingsActivity;->q:LX/0yP;

    invoke-virtual {v1, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 2666141
    iget-object v1, p0, LX/JEd;->a:LX/JEe;

    iget-object v1, v1, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v1, v1, Lcom/facebook/zero/settings/FlexSettingsActivity;->r:LX/0yI;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Lcom/facebook/zero/sdk/token/ZeroToken;LX/0yi;)V

    .line 2666142
    iget-object v1, p0, LX/JEd;->a:LX/JEe;

    iget-boolean v1, v1, LX/JEe;->a:Z

    if-nez v1, :cond_1

    iget-object v1, p0, LX/JEd;->a:LX/JEe;

    iget-object v1, v1, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-boolean v1, v1, Lcom/facebook/zero/settings/FlexSettingsActivity;->E:Z

    if-ne v1, v0, :cond_1

    .line 2666143
    :goto_0
    iget-object v1, p0, LX/JEd;->a:LX/JEe;

    iget-object v1, v1, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v2, p0, LX/JEd;->a:LX/JEe;

    iget-boolean v2, v2, LX/JEe;->a:Z

    .line 2666144
    iput-boolean v2, v1, Lcom/facebook/zero/settings/FlexSettingsActivity;->E:Z

    .line 2666145
    if-eqz v0, :cond_0

    .line 2666146
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->v:LX/0Sh;

    new-instance v1, Lcom/facebook/zero/settings/FlexSettingsActivity$7$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/zero/settings/FlexSettingsActivity$7$1$1;-><init>(LX/JEd;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2666147
    :cond_0
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    invoke-static {v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->n(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    .line 2666148
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    invoke-static {v0}, Lcom/facebook/zero/settings/FlexSettingsActivity;->o(Lcom/facebook/zero/settings/FlexSettingsActivity;)V

    .line 2666149
    return-void

    .line 2666150
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;LX/0yi;)V
    .locals 2

    .prologue
    .line 2666151
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->z:LX/120;

    const/4 v1, 0x0

    .line 2666152
    iput-boolean v1, v0, LX/120;->y:Z

    .line 2666153
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->q:LX/0yP;

    invoke-virtual {v0, p0}, LX/0yP;->b(LX/0yJ;)V

    .line 2666154
    iget-object v0, p0, LX/JEd;->a:LX/JEe;

    iget-object v0, v0, LX/JEe;->b:Lcom/facebook/zero/settings/FlexSettingsActivity;

    iget-object v0, v0, Lcom/facebook/zero/settings/FlexSettingsActivity;->r:LX/0yI;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/zero/sdk/token/ZeroTokenManagerBase;->a(Ljava/lang/Throwable;LX/0yi;)V

    .line 2666155
    return-void
.end method
