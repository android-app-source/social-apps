.class public final LX/HeE;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/uberbar/ui/UberbarResultView;


# direct methods
.method public constructor <init>(Lcom/facebook/uberbar/ui/UberbarResultView;)V
    .locals 0

    .prologue
    .line 2489821
    iput-object p1, p0, LX/HeE;->a:Lcom/facebook/uberbar/ui/UberbarResultView;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 9

    .prologue
    .line 2489822
    check-cast p1, LX/2f2;

    .line 2489823
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/HeE;->a:Lcom/facebook/uberbar/ui/UberbarResultView;

    iget-object v0, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->l:Lcom/facebook/search/api/SearchTypeaheadResult;

    if-eqz v0, :cond_0

    iget-wide v0, p1, LX/2f2;->a:J

    iget-object v2, p0, LX/HeE;->a:Lcom/facebook/uberbar/ui/UberbarResultView;

    iget-object v2, v2, Lcom/facebook/uberbar/ui/UberbarResultView;->l:Lcom/facebook/search/api/SearchTypeaheadResult;

    iget-wide v2, v2, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2489824
    :cond_0
    :goto_0
    return-void

    .line 2489825
    :cond_1
    iget-object v0, p0, LX/HeE;->a:Lcom/facebook/uberbar/ui/UberbarResultView;

    iget-object v0, v0, Lcom/facebook/uberbar/ui/UberbarResultView;->l:Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2489826
    new-instance v4, LX/7BL;

    invoke-direct {v4}, LX/7BL;-><init>()V

    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    .line 2489827
    iput-object v5, v4, LX/7BL;->a:Ljava/lang/String;

    .line 2489828
    move-object v4, v4

    .line 2489829
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    .line 2489830
    iput-object v5, v4, LX/7BL;->c:Landroid/net/Uri;

    .line 2489831
    move-object v4, v4

    .line 2489832
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2489833
    iput-object v5, v4, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2489834
    move-object v4, v4

    .line 2489835
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    .line 2489836
    iput-object v5, v4, LX/7BL;->d:Landroid/net/Uri;

    .line 2489837
    move-object v4, v4

    .line 2489838
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    .line 2489839
    iput-object v5, v4, LX/7BL;->e:Landroid/net/Uri;

    .line 2489840
    move-object v4, v4

    .line 2489841
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    .line 2489842
    iput-object v5, v4, LX/7BL;->f:Landroid/net/Uri;

    .line 2489843
    move-object v4, v4

    .line 2489844
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 2489845
    iput-object v5, v4, LX/7BL;->g:Ljava/lang/String;

    .line 2489846
    move-object v4, v4

    .line 2489847
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    .line 2489848
    iput-object v5, v4, LX/7BL;->h:Ljava/lang/String;

    .line 2489849
    move-object v4, v4

    .line 2489850
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    .line 2489851
    iput-object v5, v4, LX/7BL;->i:Ljava/lang/String;

    .line 2489852
    move-object v4, v4

    .line 2489853
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    .line 2489854
    iput-object v5, v4, LX/7BL;->j:Ljava/lang/String;

    .line 2489855
    move-object v4, v4

    .line 2489856
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2489857
    iput-object v5, v4, LX/7BL;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2489858
    move-object v4, v4

    .line 2489859
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2489860
    iput-object v5, v4, LX/7BL;->l:Ljava/lang/String;

    .line 2489861
    move-object v4, v4

    .line 2489862
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    .line 2489863
    iput-object v5, v4, LX/7BL;->m:LX/7BK;

    .line 2489864
    move-object v4, v4

    .line 2489865
    iget-wide v6, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    .line 2489866
    iput-wide v6, v4, LX/7BL;->n:J

    .line 2489867
    move-object v4, v4

    .line 2489868
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    .line 2489869
    iput-object v5, v4, LX/7BL;->r:Ljava/util/List;

    .line 2489870
    move-object v4, v4

    .line 2489871
    iget-boolean v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    .line 2489872
    iput-boolean v5, v4, LX/7BL;->o:Z

    .line 2489873
    move-object v4, v4

    .line 2489874
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2489875
    iput-object v5, v4, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2489876
    move-object v4, v4

    .line 2489877
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->s:LX/0Px;

    .line 2489878
    iput-object v5, v4, LX/7BL;->s:LX/0Px;

    .line 2489879
    move-object v4, v4

    .line 2489880
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    .line 2489881
    iput-object v5, v4, LX/7BL;->t:Ljava/lang/String;

    .line 2489882
    move-object v4, v4

    .line 2489883
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->u:LX/0Px;

    .line 2489884
    iput-object v5, v4, LX/7BL;->u:LX/0Px;

    .line 2489885
    move-object v4, v4

    .line 2489886
    iget-boolean v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7BL;->a(Ljava/lang/Boolean;)LX/7BL;

    move-result-object v4

    iget v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->v:I

    .line 2489887
    iput v5, v4, LX/7BL;->v:I

    .line 2489888
    move-object v4, v4

    .line 2489889
    iget v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->w:I

    .line 2489890
    iput v5, v4, LX/7BL;->w:I

    .line 2489891
    move-object v4, v4

    .line 2489892
    iget-boolean v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    .line 2489893
    iput-boolean v5, v4, LX/7BL;->x:Z

    .line 2489894
    move-object v4, v4

    .line 2489895
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    .line 2489896
    iput-object v5, v4, LX/7BL;->y:Ljava/util/Map;

    .line 2489897
    move-object v4, v4

    .line 2489898
    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    .line 2489899
    iput-object v5, v4, LX/7BL;->z:Ljava/lang/String;

    .line 2489900
    move-object v4, v4

    .line 2489901
    iget-boolean v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    .line 2489902
    iput-boolean v5, v4, LX/7BL;->A:Z

    .line 2489903
    move-object v4, v4

    .line 2489904
    iget-boolean v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    .line 2489905
    iput-boolean v5, v4, LX/7BL;->B:Z

    .line 2489906
    move-object v4, v4

    .line 2489907
    move-object v0, v4

    .line 2489908
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2489909
    iput-object v1, v0, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2489910
    move-object v0, v0

    .line 2489911
    invoke-virtual {v0}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v0

    .line 2489912
    iget-object v1, p0, LX/HeE;->a:Lcom/facebook/uberbar/ui/UberbarResultView;

    invoke-virtual {v1, v0}, Lcom/facebook/uberbar/ui/UberbarResultView;->a(Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/uberbar/ui/UberbarResultView;

    goto/16 :goto_0
.end method
