.class public final LX/J66;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V
    .locals 0

    .prologue
    .line 2649236
    iput-object p1, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2649237
    iget-object v0, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2649238
    :goto_0
    return-void

    .line 2649239
    :cond_0
    iget-object v0, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2649240
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2649241
    iget-object v0, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2649242
    iget-object v1, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2471

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2649243
    iget-object v2, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2470

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 2649244
    iget-object v3, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget-object v4, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v4, v4, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->F:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v3, v4

    .line 2649245
    iget-object v4, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v4, v4, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v3, v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v4, v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2649246
    iget-object v0, p0, LX/J66;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->I:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
