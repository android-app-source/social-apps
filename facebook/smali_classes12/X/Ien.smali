.class public LX/Ien;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598983
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598984
    iput-object p1, p0, LX/Ien;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2598985
    iput-object p2, p0, LX/Ien;->b:Landroid/content/Context;

    .line 2598986
    return-void
.end method

.method public static b(LX/0QB;)LX/Ien;
    .locals 3

    .prologue
    .line 2598987
    new-instance v2, LX/Ien;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/Ien;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    .line 2598988
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2598989
    iget-object v0, p0, LX/Ien;->a:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/Ien;->b:Landroid/content/Context;

    const-class v3, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, LX/Ien;->b:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2598990
    return-void
.end method
