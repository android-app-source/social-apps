.class public final LX/IO5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IO6;


# direct methods
.method public constructor <init>(LX/IO6;)V
    .locals 0

    .prologue
    .line 2571440
    iput-object p1, p0, LX/IO5;->a:LX/IO6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2571441
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    sget-object v1, LX/IOE;->NOT_INVITED:LX/IOE;

    invoke-static {v0, v1}, LX/IO9;->setInviteButtonState(LX/IO9;LX/IOE;)V

    .line 2571442
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    iget-object v0, v0, LX/IO9;->e:LX/IOF;

    iget-object v1, p0, LX/IO5;->a:LX/IO6;

    iget-object v1, v1, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/IOE;->NOT_INVITED:LX/IOE;

    invoke-virtual {v0, v1, v2}, LX/IOF;->a(Ljava/lang/String;LX/IOE;)V

    .line 2571443
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    const-string v1, "community_invite_friend_server_exception"

    iget-object v2, p0, LX/IO5;->a:LX/IO6;

    iget-object v2, v2, LX/IO6;->b:Ljava/lang/String;

    iget-object v3, p0, LX/IO5;->a:LX/IO6;

    iget-object v3, v3, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/IO9;->a$redex0(LX/IO9;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2571444
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    iget-object v0, v0, LX/IO9;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083026

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2571445
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2571446
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2571447
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2571448
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;->m()LX/0Px;

    move-result-object v0

    .line 2571449
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2571450
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    sget-object v1, LX/IOE;->NOT_INVITED:LX/IOE;

    invoke-static {v0, v1}, LX/IO9;->setInviteButtonState(LX/IO9;LX/IOE;)V

    .line 2571451
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    iget-object v0, v0, LX/IO9;->e:LX/IOF;

    iget-object v1, p0, LX/IO5;->a:LX/IO6;

    iget-object v1, v1, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/IOE;->NOT_INVITED:LX/IOE;

    invoke-virtual {v0, v1, v2}, LX/IOF;->a(Ljava/lang/String;LX/IOE;)V

    .line 2571452
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    const-string v1, "community_invite_friend_failed"

    iget-object v2, p0, LX/IO5;->a:LX/IO6;

    iget-object v2, v2, LX/IO6;->b:Ljava/lang/String;

    iget-object v3, p0, LX/IO5;->a:LX/IO6;

    iget-object v3, v3, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/IO9;->a$redex0(LX/IO9;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2571453
    iget-object v0, p0, LX/IO5;->a:LX/IO6;

    iget-object v0, v0, LX/IO6;->c:LX/IO9;

    iget-object v0, v0, LX/IO9;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083026

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2571454
    :cond_0
    return-void
.end method
