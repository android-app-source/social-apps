.class public final LX/Hu5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/9jN;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Hu6;


# direct methods
.method public constructor <init>(LX/Hu6;Z)V
    .locals 0

    .prologue
    .line 2517094
    iput-object p1, p0, LX/Hu5;->b:LX/Hu6;

    iput-boolean p2, p0, LX/Hu5;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2517095
    iget-object v0, p0, LX/Hu5;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->e:LX/7kl;

    iget-boolean v1, p0, LX/Hu5;->a:Z

    sget-object v2, LX/2tG;->FAILED:LX/2tG;

    invoke-virtual {v0, v1, v2}, LX/7kl;->a(ZLX/2tG;)V

    .line 2517096
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2517097
    check-cast p1, LX/9jN;

    .line 2517098
    iget-object v0, p0, LX/Hu5;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->e:LX/7kl;

    iget-boolean v1, p0, LX/Hu5;->a:Z

    sget-object v2, LX/2tG;->SUCCEEDED:LX/2tG;

    invoke-virtual {v0, v1, v2}, LX/7kl;->a(ZLX/2tG;)V

    .line 2517099
    iget-object v0, p0, LX/Hu5;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->i:LX/HqN;

    .line 2517100
    if-nez p1, :cond_0

    .line 2517101
    :goto_0
    return-void

    .line 2517102
    :cond_0
    iget-object v3, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v3

    .line 2517103
    iget-object v4, p1, LX/9jN;->b:Ljava/util/List;

    move-object v4, v4

    .line 2517104
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2517105
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2517106
    invoke-static {v5}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2517107
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, v3, LX/5RO;->g:LX/0Px;

    .line 2517108
    move-object v3, v3

    .line 2517109
    iget-object v4, p1, LX/9jN;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2517110
    iput-object v4, v3, LX/5RO;->i:Ljava/lang/String;

    .line 2517111
    move-object v4, v3

    .line 2517112
    iget-object v3, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    .line 2517113
    iget-object v5, v3, LX/Hu6;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/Hu6;->b:LX/0Tn;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v5

    move v3, v5

    .line 2517114
    if-eqz v3, :cond_2

    iget-object v3, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->e()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2517115
    iget-object v3, p1, LX/9jN;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;

    move-object v5, v3

    .line 2517116
    if-eqz v5, :cond_2

    .line 2517117
    iget-object v3, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2517118
    invoke-virtual {v5}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinSearchQueryModel$ClosestCityModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v3, v5, v6}, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->a(Ljava/lang/String;J)LX/5S2;

    move-result-object v3

    invoke-virtual {v3}, LX/5S2;->a()Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v3

    .line 2517119
    iput-object v3, v4, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2517120
    :cond_2
    iget-object v3, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v3, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v5, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v5}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v4}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3}, LX/0jL;->a()V

    goto/16 :goto_0
.end method
