.class public final LX/I15;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V
    .locals 0

    .prologue
    .line 2528493
    iput-object p1, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2528494
    iget-object v0, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1f;->a(LX/0m9;)V

    .line 2528495
    iget-object v0, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V

    .line 2528496
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2528497
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2528498
    if-eqz p1, :cond_0

    .line 2528499
    iget-object v0, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    .line 2528500
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v2

    .line 2528501
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2528502
    const-string v4, "latitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2528503
    const-string v4, "longitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2528504
    move-object v1, v3

    .line 2528505
    iput-object v1, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->n:LX/0m9;

    .line 2528506
    :cond_0
    iget-object v0, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    iget-object v1, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v1, v1, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->n:LX/0m9;

    invoke-virtual {v0, v1}, LX/I1f;->a(LX/0m9;)V

    .line 2528507
    iget-object v0, p0, LX/I15;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->a$redex0(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V

    .line 2528508
    return-void
.end method
