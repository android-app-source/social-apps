.class public LX/JOt;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JOv;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOt",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JOv;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688408
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2688409
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOt;->b:LX/0Zi;

    .line 2688410
    iput-object p1, p0, LX/JOt;->a:LX/0Ot;

    .line 2688411
    return-void
.end method

.method public static a(LX/0QB;)LX/JOt;
    .locals 4

    .prologue
    .line 2688412
    const-class v1, LX/JOt;

    monitor-enter v1

    .line 2688413
    :try_start_0
    sget-object v0, LX/JOt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688414
    sput-object v2, LX/JOt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688415
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688416
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688417
    new-instance v3, LX/JOt;

    const/16 p0, 0x1fd0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOt;-><init>(LX/0Ot;)V

    .line 2688418
    move-object v0, v3

    .line 2688419
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688420
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688421
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688422
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2688423
    check-cast p2, LX/JOr;

    .line 2688424
    iget-object v0, p0, LX/JOt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JOv;

    iget-object v1, p2, LX/JOr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688425
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b257f

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b258d

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/JPK;->a(LX/1De;)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 p2, 0x7

    const/4 p0, 0x1

    const/4 v8, 0x2

    .line 2688426
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/JPN;->a(Ljava/lang/String;)LX/JPM;

    move-result-object v5

    .line 2688427
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v4, ""

    .line 2688428
    :goto_0
    sget-object v6, LX/JPM;->POST_ENGAGEMENT:LX/JPM;

    if-eq v5, v6, :cond_0

    sget-object v6, LX/JPM;->VIDEO_INSIGHTS:LX/JPM;

    if-eq v5, v6, :cond_0

    sget-object v6, LX/JPM;->VIEW_INSIGHTS:LX/JPM;

    if-ne v5, v6, :cond_2

    .line 2688429
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const v7, 0x7f02080f

    .line 2688430
    iget-object v2, v0, LX/JOv;->f:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v2

    const v1, 0x7f0a010e

    invoke-virtual {v2, v1}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    move-object v7, v2

    .line 2688431
    invoke-virtual {v6, v7}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b258f

    invoke-interface {v6, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b258f

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b2571

    invoke-interface {v6, v8, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a010e

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b2582

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b2571

    invoke-interface {v4, p0, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b2571

    invoke-interface {v4, p2, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 2688432
    :goto_1
    move-object v4, v4

    .line 2688433
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2688434
    const v4, 0x197b323f

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2688435
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2688436
    return-object v0

    .line 2688437
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2688438
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a010e

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b2582

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b2571

    invoke-interface {v4, p2, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2688439
    invoke-static {}, LX/1dS;->b()V

    .line 2688440
    iget v0, p1, LX/1dQ;->b:I

    .line 2688441
    packed-switch v0, :pswitch_data_0

    .line 2688442
    :goto_0
    return-object v2

    .line 2688443
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2688444
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2688445
    check-cast v1, LX/JOr;

    .line 2688446
    iget-object v3, p0, LX/JOt;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOv;

    iget-object v4, v1, LX/JOr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JOr;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v3, v0, v4, p1}, LX/JOv;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2688447
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x197b323f
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/JOs;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/JOt",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2688448
    new-instance v1, LX/JOr;

    invoke-direct {v1, p0}, LX/JOr;-><init>(LX/JOt;)V

    .line 2688449
    iget-object v2, p0, LX/JOt;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JOs;

    .line 2688450
    if-nez v2, :cond_0

    .line 2688451
    new-instance v2, LX/JOs;

    invoke-direct {v2, p0}, LX/JOs;-><init>(LX/JOt;)V

    .line 2688452
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JOs;->a$redex0(LX/JOs;LX/1De;IILX/JOr;)V

    .line 2688453
    move-object v1, v2

    .line 2688454
    move-object v0, v1

    .line 2688455
    return-object v0
.end method
