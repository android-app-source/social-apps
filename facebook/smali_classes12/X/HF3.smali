.class public LX/HF3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HF3;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/HFJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442981
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HF3;->a:Ljava/util/Map;

    .line 2442982
    return-void
.end method

.method public static a(LX/0QB;)LX/HF3;
    .locals 3

    .prologue
    .line 2442983
    sget-object v0, LX/HF3;->b:LX/HF3;

    if-nez v0, :cond_1

    .line 2442984
    const-class v1, LX/HF3;

    monitor-enter v1

    .line 2442985
    :try_start_0
    sget-object v0, LX/HF3;->b:LX/HF3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2442986
    if-eqz v2, :cond_0

    .line 2442987
    :try_start_1
    new-instance v0, LX/HF3;

    invoke-direct {v0}, LX/HF3;-><init>()V

    .line 2442988
    move-object v0, v0

    .line 2442989
    sput-object v0, LX/HF3;->b:LX/HF3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2442990
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2442991
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2442992
    :cond_1
    sget-object v0, LX/HF3;->b:LX/HF3;

    return-object v0

    .line 2442993
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2442994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/HFJ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442995
    iget-object v0, p0, LX/HF3;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HFJ;

    return-object v0
.end method
