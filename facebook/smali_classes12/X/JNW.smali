.class public LX/JNW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field public final b:LX/I53;

.field public final c:LX/0kx;


# direct methods
.method public constructor <init>(LX/23P;LX/I53;LX/0kx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686049
    iput-object p1, p0, LX/JNW;->a:LX/23P;

    .line 2686050
    iput-object p2, p0, LX/JNW;->b:LX/I53;

    .line 2686051
    iput-object p3, p0, LX/JNW;->c:LX/0kx;

    .line 2686052
    return-void
.end method

.method public static a(LX/0QB;)LX/JNW;
    .locals 6

    .prologue
    .line 2686053
    const-class v1, LX/JNW;

    monitor-enter v1

    .line 2686054
    :try_start_0
    sget-object v0, LX/JNW;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686055
    sput-object v2, LX/JNW;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686056
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686057
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686058
    new-instance p0, LX/JNW;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/I53;->b(LX/0QB;)LX/I53;

    move-result-object v4

    check-cast v4, LX/I53;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v5

    check-cast v5, LX/0kx;

    invoke-direct {p0, v3, v4, v5}, LX/JNW;-><init>(LX/23P;LX/I53;LX/0kx;)V

    .line 2686059
    move-object v0, p0

    .line 2686060
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686061
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686062
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
