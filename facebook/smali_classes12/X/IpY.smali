.class public LX/IpY;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/IoU;


# instance fields
.field public a:LX/J1j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IoY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public final d:Lcom/facebook/messaging/payment/value/input/MemoInputView;

.field public e:Landroid/support/v4/view/ViewPager;

.field public f:LX/Io1;

.field public g:LX/J1i;

.field public h:LX/Ios;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2613039
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IpY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2613040
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2612999
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IpY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2613000
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2613030
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2613031
    const-class v0, LX/IpY;

    invoke-static {v0, p0}, LX/IpY;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2613032
    const v0, 0x7f030dea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2613033
    const v0, 0x7f0d220d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/IpY;->e:Landroid/support/v4/view/ViewPager;

    .line 2613034
    const v0, 0x7f0d2202

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/IpY;->c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2613035
    const v0, 0x7f0d220b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iput-object v0, p0, LX/IpY;->d:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    .line 2613036
    iget-object v0, p0, LX/IpY;->c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2613037
    iget-object v0, p0, LX/IpY;->c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setLongClickable(Z)V

    .line 2613038
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IpY;

    const-class v1, LX/J1j;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/J1j;

    invoke-static {p0}, LX/IoY;->b(LX/0QB;)LX/IoY;

    move-result-object p0

    check-cast p0, LX/IoY;

    iput-object v1, p1, LX/IpY;->a:LX/J1j;

    iput-object p0, p1, LX/IpY;->b:LX/IoY;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2613028
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    invoke-virtual {v0}, LX/J1i;->a()V

    .line 2613029
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 6
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2613041
    iget-object v0, p0, LX/IpY;->b:LX/IoY;

    const/4 v2, 0x0

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    iget-object v3, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    iget-object v4, v1, LX/Ios;->a:LX/IoT;

    const/4 v5, 0x1

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/IoY;->a(Landroid/view/MenuItem;LX/0am;Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;LX/IoT;Z)V

    .line 2613042
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2613027
    return-void
.end method

.method public getImmediateFocusView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2613026
    iget-object v0, p0, LX/IpY;->c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    return-object v0
.end method

.method public setListener(LX/Io1;)V
    .locals 2

    .prologue
    .line 2613022
    iput-object p1, p0, LX/IpY;->f:LX/Io1;

    .line 2613023
    iget-object v0, p0, LX/IpY;->d:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    new-instance v1, LX/IpU;

    invoke-direct {v1, p0}, LX/IpU;-><init>(LX/IpY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->setListener(LX/IoG;)V

    .line 2613024
    iget-object v0, p0, LX/IpY;->e:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/IpV;

    invoke-direct {v1, p0}, LX/IpV;-><init>(LX/IpY;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2613025
    return-void
.end method

.method public setMessengerPayViewParams(LX/Ios;)V
    .locals 4

    .prologue
    .line 2613001
    check-cast p1, LX/Ios;

    iput-object p1, p0, LX/IpY;->h:LX/Ios;

    .line 2613002
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    if-eqz v0, :cond_0

    .line 2613003
    :goto_0
    const/4 v2, 0x0

    .line 2613004
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    iget-object v1, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2613005
    iget-object v3, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2613006
    invoke-virtual {v0, v1, v2}, LX/J1i;->a(Ljava/lang/String;Z)V

    .line 2613007
    sget-object v0, LX/IpX;->a:[I

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    .line 2613008
    iget-object v3, v1, LX/Ios;->a:LX/IoT;

    move-object v1, v3

    .line 2613009
    invoke-virtual {v1}, LX/IoT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2613010
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    invoke-virtual {v0, v2}, LX/J1i;->a(Z)V

    .line 2613011
    :goto_1
    iget-object v0, p0, LX/IpY;->d:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    iget-object v1, v1, LX/Ios;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->setMemoText(Ljava/lang/String;)V

    .line 2613012
    iget-object v0, p0, LX/IpY;->h:LX/Ios;

    iget-object v0, v0, LX/Ios;->h:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2613013
    :goto_2
    return-void

    .line 2613014
    :cond_0
    iget-object v0, p0, LX/IpY;->a:LX/J1j;

    new-instance v1, LX/IpW;

    invoke-direct {v1, p0}, LX/IpW;-><init>(LX/IpY;)V

    const/4 v2, 0x1

    iget-object v3, p0, LX/IpY;->h:LX/Ios;

    iget-object v3, v3, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2613015
    iget-object p1, v3, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2613016
    invoke-virtual {v0, v1, v2, v3}, LX/J1j;->a(LX/Ioo;ZLjava/lang/String;)LX/J1i;

    move-result-object v0

    iput-object v0, p0, LX/IpY;->g:LX/J1i;

    .line 2613017
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    iget-object v1, p0, LX/IpY;->c:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, v1}, LX/J1i;->a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V

    goto :goto_0

    .line 2613018
    :pswitch_0
    iget-object v0, p0, LX/IpY;->g:LX/J1i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    goto :goto_1

    .line 2613019
    :cond_1
    iget-object v0, p0, LX/IpY;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2613020
    iget-object v0, p0, LX/IpY;->e:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;

    iget-object v2, p0, LX/IpY;->h:LX/Ios;

    iget-object v2, v2, LX/Ios;->h:Ljava/util/List;

    invoke-virtual {p0}, LX/IpY;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    goto :goto_2

    .line 2613021
    :cond_2
    iget-object v0, p0, LX/IpY;->e:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/IpY;->h:LX/Ios;

    iget-object v1, v1, LX/Ios;->h:Ljava/util/List;

    iget-object v2, p0, LX/IpY;->h:LX/Ios;

    iget-object v2, v2, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v1, v2}, LX/Ipi;->a(Ljava/util/List;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
