.class public final LX/HF6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;)V
    .locals 0

    .prologue
    .line 2443021
    iput-object p1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x79e33b98

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443022
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443023
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    if-nez v1, :cond_0

    .line 2443024
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b()V

    .line 2443025
    :goto_0
    const v1, 0x6613f767

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2443026
    :cond_0
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443027
    iget-object v2, v1, LX/HFJ;->d:LX/HEu;

    move-object v1, v2

    .line 2443028
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    .line 2443029
    iget-object v2, v1, LX/HEu;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2443030
    iget-object v2, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443031
    iget-object p1, v2, LX/HFJ;->d:LX/HEu;

    move-object v2, p1

    .line 2443032
    iget-object p1, v2, LX/HEu;->b:Ljava/lang/String;

    move-object v2, p1

    .line 2443033
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2443034
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->c()V

    goto :goto_0

    .line 2443035
    :cond_1
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443036
    iget-object v2, v1, LX/HFJ;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2443037
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2443038
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443039
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    .line 2443040
    iget-object v3, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->f:LX/1Ck;

    const-string v4, "create_page_gql_task_key"

    iget-object v5, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->d:LX/HFc;

    iget-object v6, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443041
    iget-object v7, v6, LX/HFJ;->b:Ljava/lang/String;

    move-object v6, v7

    .line 2443042
    new-instance v7, LX/4Hl;

    invoke-direct {v7}, LX/4Hl;-><init>()V

    .line 2443043
    const-string p0, "name"

    invoke-virtual {v7, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443044
    move-object v7, v7

    .line 2443045
    iget-object p0, v2, LX/HEu;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2443046
    const-string p1, "category"

    invoke-virtual {v7, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443047
    move-object v7, v7

    .line 2443048
    new-instance p0, LX/HGE;

    invoke-direct {p0}, LX/HGE;-><init>()V

    move-object p0, p0

    .line 2443049
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/HGE;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2443050
    iget-object p0, v5, LX/HFc;->a:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v5, v7

    .line 2443051
    new-instance v6, LX/HF7;

    invoke-direct {v6, v1, v2}, LX/HF7;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEu;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443052
    goto/16 :goto_0

    .line 2443053
    :cond_2
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443054
    iget-object v1, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, p0, LX/HF6;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    .line 2443055
    iget-object v3, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->f:LX/1Ck;

    const-string v4, "update_cat_gql_task_key"

    iget-object v5, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->d:LX/HFc;

    iget-object v6, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443056
    iget-object v7, v6, LX/HFJ;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2443057
    new-instance v7, LX/4EE;

    invoke-direct {v7}, LX/4EE;-><init>()V

    .line 2443058
    const-string p0, "page_id"

    invoke-virtual {v7, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443059
    move-object v7, v7

    .line 2443060
    iget-object p0, v2, LX/HEu;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2443061
    const-string p1, "page_category"

    invoke-virtual {v7, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443062
    move-object v7, v7

    .line 2443063
    new-instance p0, LX/HGG;

    invoke-direct {p0}, LX/HGG;-><init>()V

    move-object p0, p0

    .line 2443064
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/HGG;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2443065
    iget-object p0, v5, LX/HFc;->a:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v5, v7

    .line 2443066
    new-instance v6, LX/HFA;

    invoke-direct {v6, v1, v2}, LX/HFA;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEu;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443067
    goto/16 :goto_0
.end method
