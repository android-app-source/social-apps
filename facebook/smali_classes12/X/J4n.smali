.class public final LX/J4n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/J4j;


# direct methods
.method public constructor <init>(LX/J4j;)V
    .locals 0

    .prologue
    .line 2644491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2644492
    iput-object p1, p0, LX/J4n;->a:LX/J4j;

    .line 2644493
    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644483
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2644484
    if-eqz p1, :cond_0

    .line 2644485
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2644486
    if-nez v0, :cond_1

    .line 2644487
    :cond_0
    :goto_0
    return-object v2

    .line 2644488
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2644489
    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;

    .line 2644490
    iget-object v1, p0, LX/J4n;->a:LX/J4j;

    invoke-static {v0, v1}, LX/J4p;->a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;LX/J4j;)V

    goto :goto_0
.end method
