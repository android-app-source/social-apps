.class public LX/I2f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/I2j;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/0hL;

.field public final c:LX/1nQ;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/1vg;LX/0hL;LX/1nQ;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0hL;",
            "LX/1nQ;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530652
    iput-object p1, p0, LX/I2f;->a:LX/1vg;

    .line 2530653
    iput-object p2, p0, LX/I2f;->b:LX/0hL;

    .line 2530654
    iput-object p3, p0, LX/I2f;->c:LX/1nQ;

    .line 2530655
    iput-object p4, p0, LX/I2f;->d:LX/0Or;

    .line 2530656
    iput-object p5, p0, LX/I2f;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2530657
    return-void
.end method

.method public static a(LX/0QB;)LX/I2f;
    .locals 9

    .prologue
    .line 2530640
    const-class v1, LX/I2f;

    monitor-enter v1

    .line 2530641
    :try_start_0
    sget-object v0, LX/I2f;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2530642
    sput-object v2, LX/I2f;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2530643
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530644
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2530645
    new-instance v3, LX/I2f;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v5

    check-cast v5, LX/0hL;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v6

    check-cast v6, LX/1nQ;

    const/16 v7, 0xc

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v8}, LX/I2f;-><init>(LX/1vg;LX/0hL;LX/1nQ;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 2530646
    move-object v0, v3

    .line 2530647
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2530648
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/I2f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2530649
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2530650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
