.class public LX/IND;
.super LX/7HT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HT",
        "<",
        "LX/INB;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;

.field private final b:LX/IN4;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;LX/1Ck;LX/03V;LX/IN4;Ljava/util/concurrent/ExecutorService;LX/7Hq;)V
    .locals 0
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570654
    invoke-direct {p0, p2, p6}, LX/7HT;-><init>(LX/1Ck;LX/7Hq;)V

    .line 2570655
    iput-object p3, p0, LX/IND;->a:LX/03V;

    .line 2570656
    iput-object p4, p0, LX/IND;->b:LX/IN4;

    .line 2570657
    iput-object p5, p0, LX/IND;->c:Ljava/util/concurrent/ExecutorService;

    .line 2570658
    iput-object p1, p0, LX/IND;->d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2570659
    return-void
.end method


# virtual methods
.method public final a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "LX/INB;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2570660
    iget-object v1, p0, LX/IND;->b:LX/IN4;

    move-object v0, p1

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    const/16 v2, 0xa

    .line 2570661
    iget-object v3, v1, LX/IN4;->e:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v3, v0}, Lcom/facebook/search/api/GraphSearchQuery;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2570662
    const/4 v3, 0x0

    iput-object v3, v1, LX/IN4;->d:Ljava/lang/String;

    .line 2570663
    :cond_0
    new-instance v3, LX/4FT;

    invoke-direct {v3}, LX/4FT;-><init>()V

    .line 2570664
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2570665
    const-string v5, "ENTITY_GROUPS"

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570666
    const-string v5, "DISCOVERY_ENTITY_GROUPS"

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2570667
    new-instance v5, LX/4FQ;

    invoke-direct {v5}, LX/4FQ;-><init>()V

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/4FQ;->d(Ljava/util/List;)LX/4FQ;

    move-result-object v4

    .line 2570668
    iget-object v5, v0, LX/7B6;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2570669
    invoke-virtual {v3, v5}, LX/4FT;->a(Ljava/lang/String;)LX/4FT;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/4FT;->a(LX/4FQ;)LX/4FT;

    .line 2570670
    new-instance v4, LX/IMP;

    invoke-direct {v4}, LX/IMP;-><init>()V

    move-object v4, v4

    .line 2570671
    const-string v5, "search_query"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    const-string v4, "first_count"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "cover_photo_height"

    iget-object v5, v1, LX/IN4;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "cover_photo_width"

    iget-object v5, v1, LX/IN4;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "first_count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "after_cursor"

    iget-object v5, v1, LX/IN4;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/IMP;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2570672
    iget-object v4, v1, LX/IN4;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2570673
    new-instance v4, LX/IN3;

    invoke-direct {v4, v1, v0}, LX/IN3;-><init>(LX/IN4;Lcom/facebook/search/api/GraphSearchQuery;)V

    iget-object v5, v1, LX/IN4;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2570674
    new-instance v1, LX/INC;

    invoke-direct {v1, p0, p1}, LX/INC;-><init>(LX/IND;LX/7B6;)V

    iget-object v2, p0, LX/IND;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2570675
    const-string v0, "fetch_groups_community_groups_typeahead"

    return-object v0
.end method

.method public final a(LX/7B6;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2570676
    iget-object v0, p0, LX/IND;->a:LX/03V;

    const-string v1, "search_remote_failure"

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2570677
    return-void
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2570678
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    return-object v0
.end method
