.class public LX/Iu3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/3GK;

.field public final d:LX/3RA;

.field public final e:LX/3Ec;

.field private final f:LX/3Kn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/3GK;LX/3RA;LX/3Ec;LX/3Kn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625224
    iput-object p1, p0, LX/Iu3;->a:Landroid/content/Context;

    .line 2625225
    iput-object p2, p0, LX/Iu3;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2625226
    iput-object p3, p0, LX/Iu3;->c:LX/3GK;

    .line 2625227
    iput-object p4, p0, LX/Iu3;->d:LX/3RA;

    .line 2625228
    iput-object p5, p0, LX/Iu3;->e:LX/3Ec;

    .line 2625229
    iput-object p6, p0, LX/Iu3;->f:LX/3Kn;

    .line 2625230
    return-void
.end method

.method public static a(LX/Iu3;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2625231
    iget-object v0, p0, LX/Iu3;->c:LX/3GK;

    invoke-interface {v0, p1}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v0

    .line 2625232
    const-string v1, "modify_backstack_override"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2625233
    return-object v0
.end method

.method public static b(LX/0QB;)LX/Iu3;
    .locals 7

    .prologue
    .line 2625234
    new-instance v0, LX/Iu3;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/35f;->a(LX/0QB;)LX/35f;

    move-result-object v3

    check-cast v3, LX/3GK;

    invoke-static {p0}, LX/3RA;->a(LX/0QB;)LX/3RA;

    move-result-object v4

    check-cast v4, LX/3RA;

    invoke-static {p0}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v5

    check-cast v5, LX/3Ec;

    invoke-static {p0}, LX/3Kn;->a(LX/0QB;)LX/3Kn;

    move-result-object v6

    check-cast v6, LX/3Kn;

    invoke-direct/range {v0 .. v6}, LX/Iu3;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/3GK;LX/3RA;LX/3Ec;LX/3Kn;)V

    .line 2625235
    return-object v0
.end method
