.class public LX/J1z;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/J20;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/J29;

.field public b:Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639671
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2639672
    return-void
.end method


# virtual methods
.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2639673
    invoke-virtual {p0, p1}, LX/J1z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J20;

    invoke-interface {v0}, LX/J20;->a()LX/J2w;

    move-result-object v0

    invoke-virtual {v0}, LX/J2w;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2639674
    iget-object v1, p0, LX/J1z;->b:Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;

    iget-object v2, p0, LX/J1z;->a:LX/J29;

    invoke-virtual {p0, p1}, LX/J1z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J20;

    invoke-virtual {v1, v2, v0, p2, p3}, Lcom/facebook/payments/receipt/components/SimpleReceiptViewFactory;->a(LX/J29;LX/J20;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2639675
    invoke-static {}, LX/J2w;->values()[LX/J2w;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
