.class public abstract LX/J8N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J8M;


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2652040
    const-class v0, LX/J8N;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/J8N;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2652039
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 2652038
    xor-int/lit8 v0, p1, -0x1

    and-int/2addr v0, p0

    return v0
.end method

.method public static final a(Landroid/os/StrictMode$VmPolicy;)Landroid/os/StrictMode$VmPolicy$Builder;
    .locals 4

    .prologue
    .line 2652020
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 2652021
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0, p0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>(Landroid/os/StrictMode$VmPolicy;)V

    move-object v0, v0

    .line 2652022
    :goto_0
    return-object v0

    .line 2652023
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 2652024
    new-instance v1, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v1}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    .line 2652025
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2652026
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 2652027
    const-string v3, "mask"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2652028
    const-string v3, "mMask"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 2652029
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2652030
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2652031
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 2652032
    invoke-virtual {v2, v1, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2652033
    :goto_1
    move-object v0, v1

    .line 2652034
    goto :goto_0

    .line 2652035
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2652036
    :catch_0
    move-exception v0

    .line 2652037
    sget-object v2, LX/J8N;->a:Ljava/lang/String;

    const-string v3, "Failed to inherit existing VmPolicy mask to VmPolicy.Builder."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 2652012
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 2652013
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2652014
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2652015
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2652016
    :goto_0
    return-object v0

    .line 2652017
    :catch_0
    move-exception v0

    .line 2652018
    sget-object v2, LX/J8N;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to get penalty mask in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2652019
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 2652005
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 2652006
    :try_start_0
    invoke-virtual {v1, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2652007
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2652008
    invoke-virtual {v0, p0, p2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2652009
    :goto_0
    return-void

    .line 2652010
    :catch_0
    move-exception v0

    .line 2652011
    sget-object v2, LX/J8N;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to set penalty mask in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2652041
    invoke-static {p0, p1}, LX/J8N;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 2652042
    if-nez v1, :cond_1

    .line 2652043
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/StrictMode$ThreadPolicy$Builder;)Landroid/os/StrictMode$ThreadPolicy;
    .locals 1

    .prologue
    .line 2652004
    invoke-virtual {p1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/StrictMode$VmPolicy$Builder;)Landroid/os/StrictMode$VmPolicy;
    .locals 1

    .prologue
    .line 2651977
    invoke-virtual {p1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2652003
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/StrictMode$ThreadPolicy;)Z
    .locals 2

    .prologue
    .line 2652002
    const-string v0, "mask"

    const/16 v1, 0x10

    invoke-static {p1, v0, v1}, LX/J8N;->b(Ljava/lang/Object;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2652001
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/os/StrictMode$VmPolicy;)Z
    .locals 2

    .prologue
    .line 2652000
    const-string v0, "mask"

    const/16 v1, 0x10

    invoke-static {p1, v0, v1}, LX/J8N;->b(Ljava/lang/Object;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final set()V
    .locals 3

    .prologue
    .line 2651978
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2651979
    const/16 v1, 0x9

    move v1, v1

    .line 2651980
    if-lt v0, v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_2

    .line 2651981
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 2651982
    invoke-virtual {p0, v0}, LX/J8N;->a(Landroid/os/StrictMode$ThreadPolicy;)Z

    move-result v1

    .line 2651983
    new-instance v2, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v2, v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 2651984
    invoke-virtual {p0, v2}, LX/J8N;->a(Landroid/os/StrictMode$ThreadPolicy$Builder;)Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 2651985
    if-nez v1, :cond_0

    invoke-virtual {p0}, LX/J8N;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2651986
    const-string v1, "mask"

    invoke-static {v0, v1}, LX/J8N;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2651987
    const/16 v2, 0x10

    invoke-static {v1, v2}, LX/J8N;->a(II)I

    move-result v1

    .line 2651988
    const-string v2, "mask"

    invoke-static {v0, v2, v1}, LX/J8N;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    .line 2651989
    :cond_0
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 2651990
    invoke-static {}, Landroid/os/StrictMode;->getVmPolicy()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    .line 2651991
    invoke-virtual {p0, v0}, LX/J8N;->b(Landroid/os/StrictMode$VmPolicy;)Z

    move-result v1

    .line 2651992
    invoke-static {v0}, LX/J8N;->a(Landroid/os/StrictMode$VmPolicy;)Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    .line 2651993
    invoke-virtual {p0, v0}, LX/J8N;->a(Landroid/os/StrictMode$VmPolicy$Builder;)Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    .line 2651994
    if-nez v1, :cond_1

    invoke-virtual {p0}, LX/J8N;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2651995
    const-string v1, "mask"

    invoke-static {v0, v1}, LX/J8N;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2651996
    const/16 p0, 0x10

    invoke-static {v1, p0}, LX/J8N;->a(II)I

    move-result v1

    .line 2651997
    const-string p0, "mask"

    invoke-static {v0, p0, v1}, LX/J8N;->a(Ljava/lang/Object;Ljava/lang/String;I)V

    .line 2651998
    :cond_1
    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 2651999
    :cond_2
    return-void
.end method
