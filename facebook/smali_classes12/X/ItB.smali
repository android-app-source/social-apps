.class public LX/ItB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final n:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Uo;

.field public final b:LX/Di5;

.field private final c:LX/0Sh;

.field private final d:LX/0Zb;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/2EL;

.field public final g:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final h:LX/0SF;

.field private final i:LX/2Hu;

.field private final j:LX/1fX;

.field public final k:LX/0V8;

.field public final l:LX/2Og;

.field private final m:LX/2Mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2622187
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ItB;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Di5;LX/0Sh;LX/0Zb;LX/2EL;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SF;LX/0Uo;LX/2Hu;LX/1fX;LX/0V8;LX/2Og;LX/2Mv;)V
    .locals 1
    .param p9    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2622172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2622173
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/ItB;->e:Ljava/util/Set;

    .line 2622174
    iput-object p1, p0, LX/ItB;->b:LX/Di5;

    .line 2622175
    iput-object p2, p0, LX/ItB;->c:LX/0Sh;

    .line 2622176
    iput-object p3, p0, LX/ItB;->d:LX/0Zb;

    .line 2622177
    iput-object p4, p0, LX/ItB;->f:LX/2EL;

    .line 2622178
    iput-object p5, p0, LX/ItB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2622179
    iput-object p6, p0, LX/ItB;->h:LX/0SF;

    .line 2622180
    iput-object p7, p0, LX/ItB;->a:LX/0Uo;

    .line 2622181
    iput-object p8, p0, LX/ItB;->i:LX/2Hu;

    .line 2622182
    iput-object p9, p0, LX/ItB;->j:LX/1fX;

    .line 2622183
    iput-object p10, p0, LX/ItB;->k:LX/0V8;

    .line 2622184
    iput-object p11, p0, LX/ItB;->l:LX/2Og;

    .line 2622185
    iput-object p12, p0, LX/ItB;->m:LX/2Mv;

    .line 2622186
    return-void
.end method

.method public static a(LX/0QB;)LX/ItB;
    .locals 7

    .prologue
    .line 2622145
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2622146
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2622147
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2622148
    if-nez v1, :cond_0

    .line 2622149
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2622150
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2622151
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2622152
    sget-object v1, LX/ItB;->n:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2622153
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2622154
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2622155
    :cond_1
    if-nez v1, :cond_4

    .line 2622156
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2622157
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2622158
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ItB;->b(LX/0QB;)LX/ItB;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2622159
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2622160
    if-nez v1, :cond_2

    .line 2622161
    sget-object v0, LX/ItB;->n:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItB;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2622162
    :goto_1
    if-eqz v0, :cond_3

    .line 2622163
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2622164
    :goto_3
    check-cast v0, LX/ItB;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2622165
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2622166
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2622167
    :catchall_1
    move-exception v0

    .line 2622168
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2622169
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2622170
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2622171
    :cond_2
    :try_start_8
    sget-object v0, LX/ItB;->n:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItB;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static declared-synchronized a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V
    .locals 4
    .param p2    # LX/ItA;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2622139
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2622140
    iget-object v1, p0, LX/ItB;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 2622141
    if-eqz v0, :cond_0

    .line 2622142
    iget-object v0, p0, LX/ItB;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/messaging/send/client/SendFailureNotifier$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/messaging/send/client/SendFailureNotifier$2;-><init>(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622143
    :cond_0
    monitor-exit p0

    return-void

    .line 2622144
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/ItB;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 6

    .prologue
    .line 2622133
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "spurious_send_failure"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2622134
    iget-object v2, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->GROUP:LX/5e9;

    if-ne v2, v3, :cond_0

    .line 2622135
    const-string v2, "thread_fbid"

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622136
    :goto_0
    iget-object v1, p0, LX/ItB;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2622137
    return-void

    .line 2622138
    :cond_0
    const-string v2, "other_user_id"

    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v0, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2622100
    monitor-enter p0

    .line 2622101
    :try_start_0
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2622102
    iget-object v0, p0, LX/ItB;->e:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2622103
    :goto_0
    monitor-exit p0

    return v0

    .line 2622104
    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622105
    iget-object v3, p0, LX/ItB;->l:LX/2Og;

    invoke-virtual {v3, v0}, LX/2Og;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 2622106
    if-nez v0, :cond_2

    .line 2622107
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2622108
    :goto_1
    new-instance v3, LX/It8;

    invoke-direct {v3, p0, v2}, LX/It8;-><init>(LX/ItB;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {v0, v3, v2}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622109
    if-eqz v0, :cond_4

    .line 2622110
    sget-object v1, LX/ItA;->PERMANENT:LX/ItA;

    if-ne p2, v1, :cond_3

    .line 2622111
    const/4 v1, 0x0

    .line 2622112
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622113
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object p2, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v3, p2, :cond_5

    .line 2622114
    iget-object v3, p0, LX/ItB;->l:LX/2Og;

    invoke-virtual {v3, v2}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2622115
    if-eqz v2, :cond_1

    .line 2622116
    iget-object v1, p0, LX/ItB;->b:LX/Di5;

    new-instance v3, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object p2, LX/Dhu;->UNKNOWN:LX/Dhu;

    invoke-direct {v3, v2, p2}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v1, v3}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    .line 2622117
    const/4 v1, 0x1

    .line 2622118
    :cond_1
    :goto_2
    move v0, v1

    .line 2622119
    :goto_3
    invoke-virtual {p0, p1}, LX/ItB;->a(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2622120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2622121
    :cond_2
    :try_start_2
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v0, v3

    .line 2622122
    goto :goto_1

    .line 2622123
    :cond_3
    const/4 v1, 0x1

    .line 2622124
    sget-object v2, LX/It9;->a:[I

    invoke-virtual {p2}, LX/ItA;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2622125
    const/4 v1, 0x0

    :goto_4
    move v0, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2622126
    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_3

    .line 2622127
    :cond_5
    :try_start_3
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object p2, LX/2uW;->REGULAR:LX/2uW;

    if-ne v3, p2, :cond_1

    .line 2622128
    invoke-static {p0, v2}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2622129
    :pswitch_0
    iget-object v2, p0, LX/ItB;->b:LX/Di5;

    new-instance v3, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v5, LX/Dhu;->CAPTIVE_PORTAL:LX/Dhu;

    invoke-direct {v3, v4, v5}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v2, v3}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    goto :goto_4

    .line 2622130
    :pswitch_1
    iget-object v2, p0, LX/ItB;->b:LX/Di5;

    new-instance v3, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v5, LX/Dhu;->RESTRICTED_BACKGROUND_MODE:LX/Dhu;

    invoke-direct {v3, v4, v5}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v2, v3}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    goto :goto_4

    .line 2622131
    :pswitch_2
    iget-object v2, p0, LX/ItB;->b:LX/Di5;

    new-instance v3, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v5, LX/Dhu;->LONG_QUEUE_TIME:LX/Dhu;

    invoke-direct {v3, v4, v5}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v2, v3}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    goto :goto_4

    .line 2622132
    :pswitch_3
    invoke-static {p0, v0}, LX/ItB;->i(LX/ItB;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/ItB;
    .locals 13

    .prologue
    .line 2622188
    new-instance v0, LX/ItB;

    invoke-static {p0}, LX/Di5;->a(LX/0QB;)LX/Di5;

    move-result-object v1

    check-cast v1, LX/Di5;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/2EL;->a(LX/0QB;)LX/2EL;

    move-result-object v4

    check-cast v4, LX/2EL;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SF;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v7

    check-cast v7, LX/0Uo;

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v8

    check-cast v8, LX/2Hu;

    invoke-static {p0}, LX/1fV;->b(LX/0QB;)LX/1fW;

    move-result-object v9

    check-cast v9, LX/1fX;

    invoke-static {p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v10

    check-cast v10, LX/0V8;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v11

    check-cast v11, LX/2Og;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v12

    check-cast v12, LX/2Mv;

    invoke-direct/range {v0 .. v12}, LX/ItB;-><init>(LX/Di5;LX/0Sh;LX/0Zb;LX/2EL;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SF;LX/0Uo;LX/2Hu;LX/1fX;LX/0V8;LX/2Og;LX/2Mv;)V

    .line 2622189
    return-object v0
.end method

.method public static b(LX/ItB;LX/0Tn;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 2622097
    iget-object v1, p0, LX/ItB;->h:LX/0SF;

    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/ItB;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, p1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 2622098
    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    .line 2622099
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-wide/32 v4, 0x36ee80

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b$redex0(LX/ItB;)Z
    .locals 3

    .prologue
    .line 2622086
    iget-object v0, p0, LX/ItB;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2622087
    const/4 v0, 0x0

    .line 2622088
    :cond_0
    :goto_0
    return v0

    .line 2622089
    :cond_1
    const/4 v1, 0x0

    .line 2622090
    :try_start_0
    iget-object v0, p0, LX/ItB;->i:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 2622091
    invoke-virtual {v1}, LX/2gV;->e()Ljava/lang/String;

    move-result-object v0

    .line 2622092
    sget-object v2, LX/1tE;->YES:LX/1tE;

    invoke-virtual {v2}, LX/1tE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2622093
    if-eqz v1, :cond_0

    .line 2622094
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    .line 2622095
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2622096
    invoke-virtual {v1}, LX/2gV;->f()V

    :cond_2
    throw v0
.end method

.method public static i(LX/ItB;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 12

    .prologue
    .line 2622075
    const-wide/16 v10, 0x2800

    const-wide/16 v8, 0x0

    const/4 v4, 0x1

    .line 2622076
    iget-object v5, p0, LX/ItB;->k:LX/0V8;

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v5, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    iget-object v5, p0, LX/ItB;->k:LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v5, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    .line 2622077
    :cond_0
    iget-object v5, p0, LX/ItB;->k:LX/0V8;

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v5, v6, v10, v11}, LX/0V8;->a(LX/0VA;J)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2622078
    :cond_1
    :goto_0
    move v0, v4

    .line 2622079
    if-nez v0, :cond_2

    .line 2622080
    const/4 v0, 0x0

    .line 2622081
    :goto_1
    return v0

    .line 2622082
    :cond_2
    iget-object v0, p0, LX/ItB;->b:LX/Di5;

    new-instance v1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v3, LX/Dhu;->MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE:LX/Dhu;

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    .line 2622083
    const/4 v0, 0x1

    goto :goto_1

    .line 2622084
    :cond_3
    iget-object v5, p0, LX/ItB;->k:LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v5, v6, v10, v11}, LX/0V8;->a(LX/0VA;J)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2622085
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2622072
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ItB;->e:Ljava/util/Set;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622073
    monitor-exit p0

    return-void

    .line 2622074
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2622047
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ItB;->m:LX/2Mv;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2622048
    :goto_0
    monitor-exit p0

    return-void

    .line 2622049
    :cond_0
    :try_start_1
    const/4 v0, 0x0

    .line 2622050
    instance-of v1, p2, LX/FKG;

    if-nez v1, :cond_3

    .line 2622051
    :cond_1
    :goto_1
    move v0, v0

    .line 2622052
    if-nez v0, :cond_2

    .line 2622053
    sget-object v0, LX/ItA;->PERMANENT:LX/ItA;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2622054
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2622055
    :cond_2
    :try_start_2
    sget-object v0, LX/ItA;->MEDIA_UPLOAD_FILE_NOT_FOUND_LOW_DISK_SPACE:LX/ItA;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2622056
    :cond_3
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 2622057
    instance-of v1, v1, Ljava/io/FileNotFoundException;

    if-eqz v1, :cond_1

    .line 2622058
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final declared-synchronized b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622068
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ItB;->m:LX/2Mv;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2622069
    :goto_0
    monitor-exit p0

    return-void

    .line 2622070
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/ItB;->j:LX/1fX;

    new-instance v1, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;-><init>(LX/ItB;Lcom/facebook/messaging/model/messages/Message;)V

    const v2, 0x674a62a7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2622071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    .line 2622059
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/ItB;->m:LX/2Mv;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2622060
    :goto_0
    monitor-exit p0

    return-void

    .line 2622061
    :cond_0
    :try_start_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622062
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v2, v3, :cond_2

    .line 2622063
    iget-object v0, p0, LX/ItB;->b:LX/Di5;

    new-instance v2, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    sget-object v3, LX/Dhu;->UNKNOWN:LX/Dhu;

    invoke-direct {v2, v1, v3}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v0, v2}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622064
    :cond_1
    :goto_1
    goto :goto_0

    .line 2622065
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2622066
    :cond_2
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->REGULAR:LX/2uW;

    if-ne v2, v3, :cond_1

    .line 2622067
    invoke-static {p0, v1}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_1
.end method
