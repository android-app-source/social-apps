.class public final enum LX/Ikj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ikj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ikj;

.field public static final enum UPLOAD_RECEIPT:LX/Ikj;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2606982
    new-instance v0, LX/Ikj;

    const-string v1, "UPLOAD_RECEIPT"

    invoke-direct {v0, v1, v2}, LX/Ikj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ikj;->UPLOAD_RECEIPT:LX/Ikj;

    .line 2606983
    const/4 v0, 0x1

    new-array v0, v0, [LX/Ikj;

    sget-object v1, LX/Ikj;->UPLOAD_RECEIPT:LX/Ikj;

    aput-object v1, v0, v2

    sput-object v0, LX/Ikj;->$VALUES:[LX/Ikj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2606981
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ikj;
    .locals 1

    .prologue
    .line 2606984
    const-class v0, LX/Ikj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ikj;

    return-object v0
.end method

.method public static values()[LX/Ikj;
    .locals 1

    .prologue
    .line 2606980
    sget-object v0, LX/Ikj;->$VALUES:[LX/Ikj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ikj;

    return-object v0
.end method
