.class public final LX/Ho7;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;)V
    .locals 0

    .prologue
    .line 2505223
    iput-object p1, p0, LX/Ho7;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 4

    .prologue
    .line 2505224
    iget-object v0, p0, LX/Ho7;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v1, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->s:LX/Hnj;

    iget-object v0, p0, LX/Ho7;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->z:Ljava/util/ArrayList;

    iget-object v2, p0, LX/Ho7;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    iget v2, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->G:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HoQ;

    iget-object v0, v0, LX/HoQ;->a:Ljava/lang/String;

    .line 2505225
    const-string v2, "election_hub_tab_switched"

    invoke-static {v1, v2}, LX/Hnj;->b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2505226
    const-string v3, "previous_tab"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2505227
    iget-object v3, v1, LX/Hnj;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2505228
    iget-object v0, p0, LX/Ho7;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;

    .line 2505229
    iput p1, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubActivity;->G:I

    .line 2505230
    return-void
.end method
