.class public final LX/HpV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2509540
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2509541
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2509542
    :goto_0
    return v1

    .line 2509543
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 2509544
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2509545
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2509546
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 2509547
    const-string v9, "can_viewer_edit"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2509548
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 2509549
    :cond_1
    const-string v9, "icon_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2509550
    invoke-static {p0, p1}, LX/HpU;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2509551
    :cond_2
    const-string v9, "label"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2509552
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2509553
    :cond_3
    const-string v9, "privacy_options"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2509554
    invoke-static {p0, p1}, LX/HpC;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2509555
    :cond_4
    const-string v9, "selectedPrivacyOption"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2509556
    invoke-static {p0, p1}, LX/HpF;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2509557
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2509558
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2509559
    if-eqz v0, :cond_7

    .line 2509560
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2509561
    :cond_7
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2509562
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2509563
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2509564
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2509565
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2509566
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2509567
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2509568
    if-eqz v0, :cond_0

    .line 2509569
    const-string v1, "can_viewer_edit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509570
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2509571
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509572
    if-eqz v0, :cond_1

    .line 2509573
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509574
    invoke-static {p0, v0, p2}, LX/HpU;->a(LX/15i;ILX/0nX;)V

    .line 2509575
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2509576
    if-eqz v0, :cond_2

    .line 2509577
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509578
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2509579
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509580
    if-eqz v0, :cond_3

    .line 2509581
    const-string v1, "privacy_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509582
    invoke-static {p0, v0, p2, p3}, LX/HpC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2509583
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509584
    if-eqz v0, :cond_4

    .line 2509585
    const-string v1, "selectedPrivacyOption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509586
    invoke-static {p0, v0, p2, p3}, LX/HpF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2509587
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2509588
    return-void
.end method
