.class public final LX/Ivc;
.super Landroid/telephony/PhoneStateListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

.field private b:LX/03R;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 1

    .prologue
    .line 2628459
    iput-object p1, p0, LX/Ivc;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 2628460
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/Ivc;->b:LX/03R;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628448
    invoke-direct {p0, p1}, LX/Ivc;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 2628456
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/Ivc;->b:LX/03R;

    .line 2628457
    return-void

    .line 2628458
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2628453
    iget-object v0, p0, LX/Ivc;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2628454
    iget-object v0, p0, LX/Ivc;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->t:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    invoke-direct {p0, v0}, LX/Ivc;->a(I)V

    .line 2628455
    :cond_0
    iget-object v0, p0, LX/Ivc;->b:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    return v0
.end method

.method public final onCallStateChanged(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2628449
    invoke-direct {p0, p1}, LX/Ivc;->a(I)V

    .line 2628450
    invoke-virtual {p0}, LX/Ivc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2628451
    iget-object v0, p0, LX/Ivc;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->stopSelf()V

    .line 2628452
    :cond_0
    return-void
.end method
