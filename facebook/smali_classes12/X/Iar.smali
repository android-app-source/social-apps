.class public final LX/Iar;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V
    .locals 0

    .prologue
    .line 2591416
    iput-object p1, p0, LX/Iar;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final i(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    .line 2591417
    iget-object v0, p0, LX/Iar;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    const-string v1, "back_button_pressed"

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)V

    .line 2591418
    iget-object v0, p0, LX/Iar;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    .line 2591419
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IZp;

    .line 2591420
    iget-object p0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591421
    iget-object p1, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->b:Ljava/lang/String;

    move-object p0, p1

    .line 2591422
    invoke-interface {v1}, LX/IZp;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    invoke-interface {v1, p0}, LX/IZp;->b(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2591423
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
