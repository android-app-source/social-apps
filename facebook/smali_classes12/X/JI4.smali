.class public LX/JI4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Enm;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/JI8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/JI9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/JI7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/JI6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2677325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677326
    return-void
.end method

.method public static a(LX/0QB;)LX/JI4;
    .locals 7

    .prologue
    .line 2677327
    const-class v1, LX/JI4;

    monitor-enter v1

    .line 2677328
    :try_start_0
    sget-object v0, LX/JI4;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677329
    sput-object v2, LX/JI4;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677330
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677331
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677332
    new-instance p0, LX/JI4;

    invoke-direct {p0}, LX/JI4;-><init>()V

    .line 2677333
    invoke-static {v0}, LX/JI8;->a(LX/0QB;)LX/JI8;

    move-result-object v3

    check-cast v3, LX/JI8;

    invoke-static {v0}, LX/JI9;->a(LX/0QB;)LX/JI9;

    move-result-object v4

    check-cast v4, LX/JI9;

    invoke-static {v0}, LX/JI7;->a(LX/0QB;)LX/JI7;

    move-result-object v5

    check-cast v5, LX/JI7;

    invoke-static {v0}, LX/JI6;->a(LX/0QB;)LX/JI6;

    move-result-object v6

    check-cast v6, LX/JI6;

    .line 2677334
    iput-object v3, p0, LX/JI4;->a:LX/JI8;

    iput-object v4, p0, LX/JI4;->b:LX/JI9;

    iput-object v5, p0, LX/JI4;->c:LX/JI7;

    iput-object v6, p0, LX/JI4;->d:LX/JI6;

    .line 2677335
    move-object v0, p0

    .line 2677336
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677337
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JI4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677338
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2677340
    iget-object v0, p0, LX/JI4;->a:LX/JI8;

    iget-object v1, p0, LX/JI4;->b:LX/JI9;

    iget-object v2, p0, LX/JI4;->c:LX/JI7;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 2677341
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-class v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p0, LX/JI4;->d:LX/JI6;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2677342
    const/4 v0, 0x0

    return v0
.end method
