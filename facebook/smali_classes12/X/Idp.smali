.class public final LX/Idp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/Idr;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2597984
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2597985
    check-cast p1, LX/Idr;

    check-cast p2, LX/Idr;

    .line 2597986
    iget-wide v0, p1, LX/Idr;->b:J

    iget-wide v2, p2, LX/Idr;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2597987
    iget-wide v0, p1, LX/Idr;->b:J

    iget-wide v2, p2, LX/Idr;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    .line 2597988
    :goto_0
    return v0

    .line 2597989
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2597990
    :cond_1
    iget v0, p1, LX/Idr;->a:I

    iget v1, p2, LX/Idr;->a:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
