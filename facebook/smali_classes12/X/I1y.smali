.class public final LX/I1y;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V
    .locals 0

    .prologue
    .line 2529561
    iput-object p1, p0, LX/I1y;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 2529562
    if-ltz p3, :cond_0

    iget-object v0, p0, LX/I1y;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->i:Z

    if-eqz v0, :cond_0

    .line 2529563
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2529564
    iget-object v1, p0, LX/I1y;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    .line 2529565
    add-int/lit8 v0, v0, 0x3

    if-le v0, v1, :cond_0

    .line 2529566
    iget-object v0, p0, LX/I1y;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->j:LX/I1o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/I1o;->b(Z)V

    .line 2529567
    iget-object v0, p0, LX/I1y;->a:Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    .line 2529568
    iget-object v1, v0, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->c:LX/1Ck;

    sget-object p0, LX/Hy9;->FETCH_PAST_EVENTS:LX/Hy9;

    invoke-virtual {v1, p0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2529569
    :cond_0
    :goto_0
    return-void

    .line 2529570
    :cond_1
    invoke-static {v0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->e(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    .line 2529571
    invoke-static {v0}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;->b(Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;)V

    goto :goto_0
.end method
