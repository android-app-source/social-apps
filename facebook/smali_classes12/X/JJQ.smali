.class public final LX/JJQ;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/text/style/URLSpan;

.field public final synthetic b:LX/JJR;


# direct methods
.method public constructor <init>(LX/JJR;Landroid/text/style/URLSpan;)V
    .locals 0

    .prologue
    .line 2679391
    iput-object p1, p0, LX/JJQ;->b:LX/JJR;

    iput-object p2, p0, LX/JJQ;->a:Landroid/text/style/URLSpan;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2679392
    iget-object v0, p0, LX/JJQ;->b:LX/JJR;

    iget-object v0, v0, LX/JJR;->b:LX/17W;

    iget-object v1, p0, LX/JJQ;->b:LX/JJR;

    invoke-virtual {v1}, LX/JJR;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/JJQ;->a:Landroid/text/style/URLSpan;

    invoke-virtual {v2}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    .line 2679393
    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, p0

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2679394
    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2679395
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2679396
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2679397
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2679398
    iget-object v0, p0, LX/JJQ;->b:LX/JJR;

    invoke-virtual {v0}, LX/JJR;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2679399
    return-void
.end method
