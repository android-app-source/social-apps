.class public final LX/JK0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JJy;

.field public final synthetic b:LX/JK3;


# direct methods
.method public constructor <init>(LX/JK3;LX/JJy;)V
    .locals 0

    .prologue
    .line 2680009
    iput-object p1, p0, LX/JK0;->b:LX/JK3;

    iput-object p2, p0, LX/JK0;->a:LX/JJy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2680010
    iget-object v0, p0, LX/JK0;->a:LX/JJy;

    invoke-virtual {v0}, LX/JJy;->a()V

    .line 2680011
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2680012
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2680013
    if-eqz p1, :cond_0

    .line 2680014
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2680015
    if-nez v0, :cond_1

    .line 2680016
    :cond_0
    iget-object v0, p0, LX/JK0;->a:LX/JJy;

    invoke-virtual {v0}, LX/JJy;->a()V

    .line 2680017
    :goto_0
    return-void

    .line 2680018
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2680019
    check-cast v0, LX/7oa;

    .line 2680020
    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2680021
    iget-object v1, p0, LX/JK0;->a:LX/JJy;

    invoke-virtual {v1, v0}, LX/JJy;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_0
.end method
