.class public final LX/HKP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/HKU;

.field public b:Z

.field public c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public d:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/HKQ;


# direct methods
.method public constructor <init>(LX/HKQ;)V
    .locals 1

    .prologue
    .line 2454374
    iput-object p1, p0, LX/HKP;->e:LX/HKQ;

    .line 2454375
    move-object v0, p1

    .line 2454376
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2454377
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2454378
    const-string v0, "PagePhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2454379
    if-ne p0, p1, :cond_1

    .line 2454380
    :cond_0
    :goto_0
    return v0

    .line 2454381
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2454382
    goto :goto_0

    .line 2454383
    :cond_3
    check-cast p1, LX/HKP;

    .line 2454384
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2454385
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2454386
    if-eq v2, v3, :cond_0

    .line 2454387
    iget-object v2, p0, LX/HKP;->a:LX/HKU;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKP;->a:LX/HKU;

    iget-object v3, p1, LX/HKP;->a:LX/HKU;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2454388
    goto :goto_0

    .line 2454389
    :cond_5
    iget-object v2, p1, LX/HKP;->a:LX/HKU;

    if-nez v2, :cond_4

    .line 2454390
    :cond_6
    iget-boolean v2, p0, LX/HKP;->b:Z

    iget-boolean v3, p1, LX/HKP;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2454391
    goto :goto_0

    .line 2454392
    :cond_7
    iget-object v2, p0, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2454393
    goto :goto_0

    .line 2454394
    :cond_9
    iget-object v2, p1, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_8

    .line 2454395
    :cond_a
    iget-object v2, p0, LX/HKP;->d:LX/2km;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/HKP;->d:LX/2km;

    iget-object v3, p1, LX/HKP;->d:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2454396
    goto :goto_0

    .line 2454397
    :cond_b
    iget-object v2, p1, LX/HKP;->d:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
