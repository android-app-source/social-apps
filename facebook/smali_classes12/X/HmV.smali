.class public LX/HmV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0TD;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/Hm6;

.field public final f:LX/Hmc;

.field private final g:LX/Hma;

.field public final h:LX/03V;

.field public final i:LX/Hm2;

.field public j:LX/Hm4;

.field public k:LX/HmU;

.field public l:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/Hm3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/beam/protocol/BeamPreflightInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/beam/protocol/BeamPreflightInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/Hm6;LX/Hmc;LX/Hma;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/Hm2;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2500789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500790
    sget-object v0, LX/HmU;->WAIT_INITIALIZE:LX/HmU;

    iput-object v0, p0, LX/HmV;->k:LX/HmU;

    .line 2500791
    iput-object p1, p0, LX/HmV;->a:Landroid/content/Context;

    .line 2500792
    invoke-static {p2}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/HmV;->b:LX/0TD;

    .line 2500793
    iput-object p3, p0, LX/HmV;->c:Ljava/util/concurrent/Executor;

    .line 2500794
    iput-object p4, p0, LX/HmV;->e:LX/Hm6;

    .line 2500795
    iput-object p5, p0, LX/HmV;->f:LX/Hmc;

    .line 2500796
    iput-object p6, p0, LX/HmV;->g:LX/Hma;

    .line 2500797
    iput-object p7, p0, LX/HmV;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2500798
    iput-object p8, p0, LX/HmV;->h:LX/03V;

    .line 2500799
    iput-object p9, p0, LX/HmV;->i:LX/Hm2;

    .line 2500800
    return-void
.end method

.method public static a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/HmU;",
            ">;",
            "LX/0TF",
            "<",
            "LX/HmU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2500817
    iget-object v0, p0, LX/HmV;->c:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2500818
    return-void
.end method

.method public static a$redex0(LX/HmV;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2500801
    const-string v0, "BeamReceiver"

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2500802
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500803
    sget-object v1, LX/Hmb;->RECEIVER_ERROR:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500804
    if-eqz p2, :cond_0

    .line 2500805
    sget-object v1, LX/Hmb;->WRITE_ERROR_TO_SENDER:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500806
    :cond_0
    iput-object p1, p0, LX/HmV;->s:Ljava/lang/String;

    .line 2500807
    if-eqz p2, :cond_1

    .line 2500808
    :try_start_0
    iget-object v0, p0, LX/HmV;->j:LX/Hm4;

    .line 2500809
    iget-object v1, v0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    sget-object p2, LX/Hm3;->ERROR:LX/Hm3;

    invoke-virtual {p2}, LX/Hm3;->getInt()I

    move-result p2

    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2500810
    iget-object v1, v0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 2500811
    iget-object v1, v0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500812
    :cond_1
    :goto_0
    invoke-virtual {p0}, LX/HmV;->h()V

    .line 2500813
    sget-object v0, LX/HmU;->RECEIVER_ERROR:LX/HmU;

    iput-object v0, p0, LX/HmV;->k:LX/HmU;

    .line 2500814
    return-void

    .line 2500815
    :catch_0
    move-exception v0

    .line 2500816
    const-string v1, "BeamReceiver"

    invoke-static {v1, p1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/HmV;
    .locals 10

    .prologue
    .line 2500780
    new-instance v0, LX/HmV;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    .line 2500781
    new-instance v5, LX/Hm6;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v4}, LX/Hm6;-><init>(Landroid/content/Context;)V

    .line 2500782
    move-object v4, v5

    .line 2500783
    check-cast v4, LX/Hm6;

    invoke-static {p0}, LX/Hmc;->a(LX/0QB;)LX/Hmc;

    move-result-object v5

    check-cast v5, LX/Hmc;

    invoke-static {p0}, LX/Hma;->a(LX/0QB;)LX/Hma;

    move-result-object v6

    check-cast v6, LX/Hma;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/Hm2;->b(LX/0QB;)LX/Hm2;

    move-result-object v9

    check-cast v9, LX/Hm2;

    invoke-direct/range {v0 .. v9}, LX/HmV;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/Hm6;LX/Hmc;LX/Hma;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/Hm2;)V

    .line 2500784
    return-object v0
.end method

.method public static i$redex0(LX/HmV;)V
    .locals 2

    .prologue
    .line 2500785
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500786
    sget-object v1, LX/Hmb;->SOCKET_IO_ERROR:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500787
    iget-object v0, p0, LX/HmV;->a:Landroid/content/Context;

    const v1, 0x7f083944

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/HmV;->a$redex0(LX/HmV;Ljava/lang/String;Z)V

    .line 2500788
    return-void
.end method

.method public static j(LX/HmV;)V
    .locals 2

    .prologue
    .line 2500776
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500777
    sget-object v1, LX/Hmb;->COMMUNICATION_ERROR:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500778
    iget-object v0, p0, LX/HmV;->a:Landroid/content/Context;

    const v1, 0x7f083943

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/HmV;->a$redex0(LX/HmV;Ljava/lang/String;Z)V

    .line 2500779
    return-void
.end method

.method public static k(LX/HmV;)V
    .locals 2

    .prologue
    .line 2500766
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500767
    sget-object v1, LX/Hmb;->SENDER_ERROR:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500768
    :try_start_0
    iget-object v0, p0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/HmV;->m:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500769
    sget-object v0, LX/Hm3;->ERROR:LX/Hm3;

    iput-object v0, p0, LX/HmV;->n:LX/Hm3;

    .line 2500770
    sget-object v0, LX/HmU;->SENDER_ERROR:LX/HmU;

    iput-object v0, p0, LX/HmV;->k:LX/HmU;

    .line 2500771
    iget-object v0, p0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->b()Z

    .line 2500772
    :goto_0
    return-void

    .line 2500773
    :catch_0
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500774
    sget-object v1, LX/Hmb;->FAILED_TO_READ_SENDER_ERROR:LX/Hmb;

    invoke-static {v0, v1}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500775
    invoke-static {p0}, LX/HmV;->i$redex0(LX/HmV;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2500753
    iget-object v0, p0, LX/HmV;->f:LX/Hmc;

    .line 2500754
    sget-object v3, LX/Hmb;->INSTALL_APK:LX/Hmb;

    invoke-static {v0, v3}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500755
    iget-object v0, p0, LX/HmV;->k:LX/HmU;

    sget-object v3, LX/HmU;->WAIT_INSTALL_APK:LX/HmU;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 2500756
    iget-object v0, p0, LX/HmV;->r:Ljava/io/File;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 2500757
    iget-object v0, p0, LX/HmV;->a:Landroid/content/Context;

    iget-object v3, p0, LX/HmV;->r:Ljava/io/File;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v0, v3}, LX/1sY;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2500758
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-lt v3, v4, :cond_0

    .line 2500759
    iget-object v3, p0, LX/HmV;->r:Ljava/io/File;

    invoke-virtual {v3, v1, v2}, Ljava/io/File;->setReadable(ZZ)Z

    .line 2500760
    :cond_0
    iget-object v1, p0, LX/HmV;->g:LX/Hma;

    iget-object v2, p0, LX/HmV;->p:Ljava/lang/String;

    .line 2500761
    iget-object v3, v1, LX/Hma;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/Hma;->d:LX/0Tn;

    iget-object v5, v1, LX/Hma;->b:Landroid/content/pm/PackageInfo;

    iget v5, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-interface {v3, v4, v5}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v3

    sget-object v4, LX/Hma;->e:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2500762
    iget-object v1, p0, LX/HmV;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HmV;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2500763
    return-void

    :cond_1
    move v0, v2

    .line 2500764
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2500765
    goto :goto_1
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 2500742
    iget-object v0, p0, LX/HmV;->j:LX/Hm4;

    if-eqz v0, :cond_0

    .line 2500743
    :try_start_0
    iget-object v0, p0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->b()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500744
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/HmV;->e:LX/Hm6;

    invoke-virtual {v0}, LX/Hm6;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2500745
    :goto_1
    :try_start_2
    iget-object v0, p0, LX/HmV;->g:LX/Hma;

    invoke-virtual {v0}, LX/Hma;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2500746
    :goto_2
    return-void

    .line 2500747
    :catch_0
    move-exception v0

    .line 2500748
    const-string v1, "BeamReceiver"

    const-string v2, "Failed to close socket during cleanup"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2500749
    :catch_1
    move-exception v0

    .line 2500750
    const-string v1, "BeamReceiver"

    const-string v2, "Failed to delete orphaned files during cleanup"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2500751
    :catch_2
    move-exception v0

    .line 2500752
    const-string v1, "BeamReceiver"

    const-string v2, "Failed to remove cleanup pending in BeamSharedPreferences"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
