.class public final LX/J9D;
.super LX/1PF;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2652573
    iput-object p1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-direct {p0, p2}, LX/1PF;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2652560
    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    if-eq p0, v0, :cond_1

    .line 2652561
    :cond_0
    :goto_0
    return-void

    .line 2652562
    :cond_1
    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 2652563
    iget-object v1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v1, :cond_2

    .line 2652564
    if-eqz v0, :cond_3

    .line 2652565
    iget-object v1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v1}, LX/4nk;->a()V

    .line 2652566
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->m:Z

    if-nez v0, :cond_0

    .line 2652567
    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    const/4 v1, 0x0

    .line 2652568
    iput-boolean v1, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l:Z

    .line 2652569
    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    const/4 v1, 0x1

    .line 2652570
    iput-boolean v1, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->m:Z

    .line 2652571
    iget-object v0, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->mJ_()V

    goto :goto_0

    .line 2652572
    :cond_3
    iget-object v1, p0, LX/J9D;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v2, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v1, v2}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_1
.end method
