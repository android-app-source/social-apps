.class public LX/Ixs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1vq",
        "<",
        "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2632500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2632501
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2632486
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2632487
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2632488
    iget-object v0, p0, LX/Ixs;->a:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    sget-object v2, LX/IxE;->i:Ljava/lang/String;

    .line 2632489
    iget-object v3, p1, LX/2nj;->c:LX/2nk;

    move-object v3, v3

    .line 2632490
    invoke-virtual {v3}, LX/2nk;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2632491
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2632492
    iget-object v0, p0, LX/Ixs;->a:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    sget-object v2, LX/IxE;->k:Ljava/lang/String;

    .line 2632493
    iget-object v3, p1, LX/2nj;->c:LX/2nk;

    move-object v3, v3

    .line 2632494
    invoke-virtual {v3}, LX/2nk;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2632495
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2632496
    iget-object v0, p0, LX/Ixs;->a:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    sget-object v2, LX/IxE;->j:Ljava/lang/String;

    .line 2632497
    iget-object v3, p1, LX/2nj;->c:LX/2nk;

    move-object v3, v3

    .line 2632498
    invoke-virtual {v3}, LX/2nk;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2632499
    return-void
.end method
