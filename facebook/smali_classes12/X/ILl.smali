.class public final LX/ILl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;)V
    .locals 0

    .prologue
    .line 2568277
    iput-object p1, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const v9, 0x104000a

    const/4 v8, 0x0

    const/4 v4, 0x2

    const/4 v6, 0x1

    const v0, 0x792ff8e3

    invoke-static {v4, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2568278
    iget-object v1, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2568279
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 2568280
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2568281
    const/4 v2, 0x0

    .line 2568282
    :goto_0
    move v2, v2

    .line 2568283
    if-nez v2, :cond_0

    .line 2568284
    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    const-string v3, "group_email_submit_invalid_email"

    invoke-static {v2, v3, v1}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->a$redex0(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2568285
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083027

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f083028

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    new-instance v2, LX/ILj;

    invoke-direct {v2, p0}, LX/ILj;-><init>(LX/ILl;)V

    invoke-virtual {v1, v9, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2568286
    const v1, 0x3b5a2501

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2568287
    :goto_1
    return-void

    .line 2568288
    :cond_0
    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v3, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v3, v3, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    const/4 v5, 0x1

    .line 2568289
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 2568290
    :cond_1
    iget-object v4, v2, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->c:LX/03V;

    sget-object v7, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->h:Ljava/lang/String;

    const-string p1, "Email domains should not be empty."

    invoke-virtual {v4, v7, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 2568291
    :goto_2
    move v2, v4

    .line 2568292
    if-nez v2, :cond_2

    .line 2568293
    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    const-string v3, "group_email_submit_invalid_email_domain"

    invoke-static {v2, v3, v1}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->a$redex0(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2568294
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f083029

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08302a

    new-array v4, v6, [Ljava/lang/Object;

    const-string v5, " or "

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    iget-object v7, v7, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->r:Ljava/util/ArrayList;

    aput-object v7, v6, v8

    invoke-static {v5, v6}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/ILk;

    invoke-direct {v2, p0}, LX/ILk;-><init>(LX/ILl;)V

    invoke-virtual {v1, v9, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2568295
    const v1, 0x72748c2f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 2568296
    :cond_2
    iget-object v2, p0, LX/ILl;->a:Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;

    invoke-static {v2, v1}, Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;->b(Lcom/facebook/groups/community/fragments/SchoolEmailVerificationFragment;Ljava/lang/String;)V

    .line 2568297
    const v1, 0x638fd0ce

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 2568298
    :cond_3
    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 2568299
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    goto/16 :goto_0

    .line 2568300
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 2568301
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2568302
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 2568303
    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, v5

    .line 2568304
    goto/16 :goto_2

    .line 2568305
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2
.end method
