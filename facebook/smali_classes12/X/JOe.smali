.class public LX/JOe;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JOf;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOe",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JOf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687938
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2687939
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOe;->b:LX/0Zi;

    .line 2687940
    iput-object p1, p0, LX/JOe;->a:LX/0Ot;

    .line 2687941
    return-void
.end method

.method public static a(LX/0QB;)LX/JOe;
    .locals 4

    .prologue
    .line 2687942
    const-class v1, LX/JOe;

    monitor-enter v1

    .line 2687943
    :try_start_0
    sget-object v0, LX/JOe;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687944
    sput-object v2, LX/JOe;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687945
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687946
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687947
    new-instance v3, LX/JOe;

    const/16 p0, 0x1fc8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOe;-><init>(LX/0Ot;)V

    .line 2687948
    move-object v0, v3

    .line 2687949
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687950
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687951
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687952
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2687953
    check-cast p2, LX/JOc;

    .line 2687954
    iget-object v0, p0, LX/JOe;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JOf;

    iget-object v1, p2, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p2, LX/JOc;->c:LX/1Pn;

    .line 2687955
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->y()LX/0Px;

    move-result-object v4

    .line 2687956
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257f

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b2580

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3d

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    .line 2687957
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b258c

    invoke-interface {v6, v7}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    .line 2687958
    const/4 v6, 0x3

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 2687959
    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_2

    .line 2687960
    iget-object v6, v0, LX/JOf;->a:LX/JOq;

    const/4 v10, 0x0

    .line 2687961
    new-instance p0, LX/JOo;

    invoke-direct {p0, v6}, LX/JOo;-><init>(LX/JOq;)V

    .line 2687962
    iget-object p2, v6, LX/JOq;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JOp;

    .line 2687963
    if-nez p2, :cond_0

    .line 2687964
    new-instance p2, LX/JOp;

    invoke-direct {p2, v6}, LX/JOp;-><init>(LX/JOq;)V

    .line 2687965
    :cond_0
    invoke-static {p2, p1, v10, v10, p0}, LX/JOp;->a$redex0(LX/JOp;LX/1De;IILX/JOo;)V

    .line 2687966
    move-object p0, p2

    .line 2687967
    move-object v10, p0

    .line 2687968
    move-object v6, v10

    .line 2687969
    iget-object v10, v6, LX/JOp;->a:LX/JOo;

    iput-object v1, v10, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687970
    iget-object v10, v6, LX/JOp;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2687971
    move-object v6, v6

    .line 2687972
    iget-object v10, v6, LX/JOp;->a:LX/JOo;

    iput-object v2, v10, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2687973
    iget-object v10, v6, LX/JOp;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2687974
    move-object v10, v6

    .line 2687975
    invoke-virtual {v4, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 2687976
    iget-object p0, v10, LX/JOp;->a:LX/JOo;

    iput-object v6, p0, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    .line 2687977
    iget-object p0, v10, LX/JOp;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2687978
    move-object v6, v10

    .line 2687979
    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2687980
    add-int/lit8 v6, v9, -0x1

    if-eq v7, v6, :cond_1

    .line 2687981
    invoke-static {p1}, LX/JPK;->b(LX/1De;)LX/1Di;

    move-result-object v6

    const/4 v10, 0x6

    const p0, 0x7f0b2571

    invoke-interface {v6, v10, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2687982
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 2687983
    :cond_2
    move-object v4, v8

    .line 2687984
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JOf;->b:LX/JOt;

    invoke-virtual {v5, p1}, LX/JOt;->c(LX/1De;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/JOs;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/JOs;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/JOs;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/JOs;->a(LX/1Pn;)LX/JOs;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const v6, 0x7f0b2581

    invoke-interface {v4, v5, v6}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2687985
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2687986
    invoke-static {}, LX/1dS;->b()V

    .line 2687987
    const/4 v0, 0x0

    return-object v0
.end method
