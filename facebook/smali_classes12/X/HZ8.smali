.class public final LX/HZ8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/model/Birthday;

.field public final synthetic b:Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;Lcom/facebook/growth/model/Birthday;)V
    .locals 0

    .prologue
    .line 2481485
    iput-object p1, p0, LX/HZ8;->b:Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    iput-object p2, p0, LX/HZ8;->a:Lcom/facebook/growth/model/Birthday;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x43258411

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2481486
    new-instance v0, LX/D97;

    iget-object v1, p0, LX/HZ8;->b:Lcom/facebook/registration/fragment/RegistrationBirthdayFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0b76

    new-instance v3, LX/HZ6;

    invoke-direct {v3, p0}, LX/HZ6;-><init>(LX/HZ8;)V

    iget-object v4, p0, LX/HZ8;->a:Lcom/facebook/growth/model/Birthday;

    iget v4, v4, Lcom/facebook/growth/model/Birthday;->c:I

    iget-object v5, p0, LX/HZ8;->a:Lcom/facebook/growth/model/Birthday;

    iget v5, v5, Lcom/facebook/growth/model/Birthday;->b:I

    iget-object v6, p0, LX/HZ8;->a:Lcom/facebook/growth/model/Birthday;

    iget v6, v6, Lcom/facebook/growth/model/Birthday;->a:I

    invoke-direct/range {v0 .. v6}, LX/D97;-><init>(Landroid/content/Context;ILandroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2481487
    new-instance v1, LX/HZ7;

    invoke-direct {v1, p0}, LX/HZ7;-><init>(LX/HZ8;)V

    invoke-virtual {v0, v1}, LX/D97;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2481488
    invoke-virtual {v0}, LX/D97;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 2481489
    invoke-virtual {v0}, LX/D97;->show()V

    .line 2481490
    const v0, 0x6abb1f41

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
