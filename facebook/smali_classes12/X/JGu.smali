.class public LX/JGu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[LX/JGu;

.field public static final b:LX/JGu;


# instance fields
.field public final c:I

.field public final d:Z

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2669443
    new-array v0, v6, [LX/JGu;

    sput-object v0, LX/JGu;->a:[LX/JGu;

    .line 2669444
    new-instance v0, LX/JGu;

    const/4 v5, -0x1

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, LX/JGu;-><init>(FFFFIZ)V

    sput-object v0, LX/JGu;->b:LX/JGu;

    return-void
.end method

.method public constructor <init>(FFFFIZ)V
    .locals 0

    .prologue
    .line 2669445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2669446
    iput p1, p0, LX/JGu;->e:F

    .line 2669447
    iput p2, p0, LX/JGu;->f:F

    .line 2669448
    iput p3, p0, LX/JGu;->g:F

    .line 2669449
    iput p4, p0, LX/JGu;->h:F

    .line 2669450
    iput p5, p0, LX/JGu;->c:I

    .line 2669451
    iput-boolean p6, p0, LX/JGu;->d:Z

    .line 2669452
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 2669453
    iget v0, p0, LX/JGu;->e:F

    move v0, v0

    .line 2669454
    return v0
.end method

.method public a(FF)Z
    .locals 1

    .prologue
    .line 2669455
    iget v0, p0, LX/JGu;->e:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget v0, p0, LX/JGu;->g:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, LX/JGu;->f:F

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    iget v0, p0, LX/JGu;->h:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FFFFZ)Z
    .locals 1

    .prologue
    .line 2669456
    iget v0, p0, LX/JGu;->e:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGu;->f:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGu;->g:F

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    iget v0, p0, LX/JGu;->h:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/JGu;->d:Z

    if-ne p5, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 2669457
    iget v0, p0, LX/JGu;->c:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 2669458
    iget v0, p0, LX/JGu;->f:F

    move v0, v0

    .line 2669459
    return v0
.end method

.method public b(FF)I
    .locals 1

    .prologue
    .line 2669460
    iget v0, p0, LX/JGu;->c:I

    return v0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 2669461
    iget v0, p0, LX/JGu;->g:F

    move v0, v0

    .line 2669462
    return v0
.end method

.method public d()F
    .locals 1

    .prologue
    .line 2669463
    iget v0, p0, LX/JGu;->h:F

    move v0, v0

    .line 2669464
    return v0
.end method
