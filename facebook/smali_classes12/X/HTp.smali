.class public final LX/HTp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 2470975
    const/4 v5, 0x0

    .line 2470976
    const-wide/16 v6, 0x0

    .line 2470977
    const/4 v4, 0x0

    .line 2470978
    const/4 v3, 0x0

    .line 2470979
    const/4 v2, 0x0

    .line 2470980
    const/4 v1, 0x0

    .line 2470981
    const/4 v0, 0x0

    .line 2470982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 2470983
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2470984
    const/4 v0, 0x0

    .line 2470985
    :goto_0
    return v0

    .line 2470986
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v5, :cond_8

    .line 2470987
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 2470988
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2470989
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v1, :cond_0

    .line 2470990
    const-string v5, "actors"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2470991
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2470992
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_1

    .line 2470993
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_1

    .line 2470994
    invoke-static {p0, p1}, LX/HTl;->b(LX/15w;LX/186;)I

    move-result v4

    .line 2470995
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2470996
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2470997
    move v4, v1

    goto :goto_1

    .line 2470998
    :cond_2
    const-string v5, "creation_time"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2470999
    const/4 v0, 0x1

    .line 2471000
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 2471001
    :cond_3
    const-string v5, "id"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2471002
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 2471003
    :cond_4
    const-string v5, "implicit_place"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2471004
    invoke-static {p0, p1}, LX/HTm;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 2471005
    :cond_5
    const-string v5, "message"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2471006
    invoke-static {p0, p1}, LX/HTn;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 2471007
    :cond_6
    const-string v5, "privacy_scope"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2471008
    invoke-static {p0, p1}, LX/HTo;->a(LX/15w;LX/186;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 2471009
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2471010
    :cond_8
    const/4 v1, 0x6

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2471011
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2471012
    if-eqz v0, :cond_9

    .line 2471013
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2471014
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2471015
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2471016
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2471017
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2471018
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v8, v3

    move v9, v4

    move v4, v5

    move v11, v2

    move-wide v2, v6

    move v7, v11

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 2471019
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2471020
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471021
    if-eqz v0, :cond_1

    .line 2471022
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471023
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2471024
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2471025
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/HTl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471026
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2471027
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2471028
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2471029
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 2471030
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471031
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2471032
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471033
    if-eqz v0, :cond_3

    .line 2471034
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471035
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471036
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471037
    if-eqz v0, :cond_4

    .line 2471038
    const-string v1, "implicit_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471039
    invoke-static {p0, v0, p2}, LX/HTm;->a(LX/15i;ILX/0nX;)V

    .line 2471040
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471041
    if-eqz v0, :cond_5

    .line 2471042
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471043
    invoke-static {p0, v0, p2}, LX/HTn;->a(LX/15i;ILX/0nX;)V

    .line 2471044
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471045
    if-eqz v0, :cond_6

    .line 2471046
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471047
    invoke-static {p0, v0, p2, p3}, LX/HTo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471048
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2471049
    return-void
.end method
