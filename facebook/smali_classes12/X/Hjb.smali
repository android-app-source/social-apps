.class public LX/Hjb;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Hjd;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/HkF;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/Hjb;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/Hjb;->b:Ljava/util/Map;

    invoke-static {}, LX/Hjd;->values()[LX/Hjd;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    const/4 v0, 0x0

    sget-object v1, LX/Hja;->a:[I

    iget-object v6, v5, LX/Hjd;->g:LX/HkF;

    invoke-virtual {v6}, LX/HkF;->ordinal()I

    move-result v6

    aget v1, v1, v6

    packed-switch v1, :pswitch_data_0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_1

    iget-object v0, v5, LX/Hjd;->d:Ljava/lang/Class;

    if-nez v0, :cond_0

    :try_start_0
    iget-object v6, v5, LX/Hjd;->e:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/Hjb;->a:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :pswitch_0
    const-class v0, LX/HjU;

    move-object v1, v0

    goto :goto_1

    :pswitch_1
    const-class v0, LX/HjV;

    move-object v1, v0

    goto :goto_1

    :pswitch_2
    const-class v0, LX/Hjj;

    move-object v1, v0

    goto :goto_1

    :cond_2
    return-void

    :catch_0
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;LX/HkF;)LX/HjT;
    .locals 4

    invoke-static {p0}, LX/Hjc;->a(Ljava/lang/String;)LX/Hjc;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    sget-object v1, LX/Hjb;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hjd;

    iget-object p0, v1, LX/Hjd;->f:LX/Hjc;

    if-ne p0, v0, :cond_0

    iget-object p0, v1, LX/Hjd;->g:LX/HkF;

    if-ne p0, p1, :cond_0

    :goto_0
    move-object v3, v1

    if-eqz v3, :cond_2

    sget-object v1, LX/Hjb;->a:Ljava/util/Set;

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v3, LX/Hjd;->d:Ljava/lang/Class;

    if-nez v1, :cond_1

    iget-object v1, v3, LX/Hjd;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HjT;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v0, v1

    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
