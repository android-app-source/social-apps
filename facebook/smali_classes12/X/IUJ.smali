.class public final LX/IUJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1CH;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 0

    .prologue
    .line 2579860
    iput-object p1, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2579861
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l()V

    .line 2579862
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "groups_optimistic_post_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to post to profile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2579863
    iget-object p0, v3, LX/IW5;->c:Ljava/lang/String;

    move-object v3, p0

    .line 2579864
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579865
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 2579866
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l()V

    .line 2579867
    invoke-static {p1}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2579868
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    .line 2579869
    const/4 p0, 0x1

    invoke-static {v0, p0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;I)V

    .line 2579870
    :cond_0
    return-void
.end method

.method public final a(JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 2579871
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2579872
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->mJ_()V

    .line 2579873
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 2579874
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v0}, LX/IQT;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2579875
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2579876
    iget-object v1, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->o:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, v0}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2579877
    :goto_0
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    .line 2579878
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    .line 2579879
    invoke-static {p1}, LX/16z;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->E(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2579880
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v1}, LX/IQT;->e()V

    .line 2579881
    :cond_0
    :goto_1
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v1}, LX/IQT;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2579882
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ah:LX/ISU;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/ISU;->c(Z)V

    .line 2579883
    :cond_1
    return-void

    .line 2579884
    :cond_2
    iget-object v0, p0, LX/IUJ;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->l()V

    goto :goto_0

    .line 2579885
    :cond_3
    iget-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->am:Z

    if-eqz v1, :cond_0

    .line 2579886
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v1}, LX/IQT;->e()V

    .line 2579887
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->am:Z

    goto :goto_1
.end method
