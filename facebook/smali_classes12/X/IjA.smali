.class public LX/IjA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6y0;


# instance fields
.field private final a:LX/6yY;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/6yY;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605397
    iput-object p1, p0, LX/IjA;->a:LX/6yY;

    .line 2605398
    iput-object p2, p0, LX/IjA;->b:Landroid/content/Context;

    .line 2605399
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2605368
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IjA;->b:Landroid/content/Context;

    const v1, 0x7f082c1f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 2

    .prologue
    .line 2605390
    move-object v0, p2

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    .line 2605391
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1, p2}, LX/6yY;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    .line 2605392
    :goto_0
    return v0

    .line 2605393
    :cond_0
    sget-object v0, LX/Ij9;->a:[I

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2605394
    const/4 v0, 0x0

    goto :goto_0

    .line 2605395
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;
    .locals 4

    .prologue
    .line 2605378
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    .line 2605379
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 2605380
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/6dy;

    iget-object v1, p0, LX/IjA;->b:Landroid/content/Context;

    const v2, 0x7f082c25

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/IjA;->b:Landroid/content/Context;

    const v3, 0x7f082d03

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, LX/IjA;->b:Landroid/content/Context;

    const v2, 0x7f082c26

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2605381
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 2605382
    move-object v0, v0

    .line 2605383
    iget-object v1, p0, LX/IjA;->b:Landroid/content/Context;

    const v2, 0x7f082c27

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2605384
    iput-object v1, v0, LX/6dy;->c:Ljava/lang/String;

    .line 2605385
    move-object v0, v0

    .line 2605386
    const/4 v1, 0x1

    .line 2605387
    iput-boolean v1, v0, LX/6dy;->f:Z

    .line 2605388
    move-object v0, v0

    .line 2605389
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2605375
    move-object v0, p1

    check-cast v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    iget-boolean v0, v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    if-eqz v0, :cond_0

    .line 2605376
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2605377
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "https://m.facebook.com/help/messenger-app/android/1528535330720775"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605374
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605373
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605372
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605371
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605370
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method

.method public final i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z
    .locals 1

    .prologue
    .line 2605369
    iget-object v0, p0, LX/IjA;->a:LX/6yY;

    invoke-virtual {v0, p1}, LX/6yY;->i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    return v0
.end method
