.class public final LX/I6q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/I6r;


# direct methods
.method public constructor <init>(LX/I6r;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2538001
    iput-object p1, p0, LX/I6q;->b:LX/I6r;

    iput-object p2, p0, LX/I6q;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2537979
    iget-object v0, p0, LX/I6q;->b:LX/I6r;

    iget-object v0, v0, LX/I6r;->d:LX/I6m;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/I6m;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    .line 2537980
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2537981
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2537982
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537983
    if-eqz v0, :cond_2

    .line 2537984
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537985
    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;

    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2537986
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 2537987
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537988
    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;

    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2537989
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2537990
    const-class v3, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2537991
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2537992
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537993
    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;

    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;->k()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v0

    .line 2537994
    iget-object v2, p0, LX/I6q;->b:LX/I6r;

    iget-object v2, v2, LX/I6r;->d:LX/I6m;

    invoke-virtual {v2, v1, v0}, LX/I6m;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    .line 2537995
    iget-object v0, p0, LX/I6q;->b:LX/I6r;

    iget-object v0, v0, LX/I6r;->a:LX/1My;

    iget-object v1, p0, LX/I6q;->b:LX/I6r;

    iget-object v1, v1, LX/I6r;->e:LX/0TF;

    iget-object v2, p0, LX/I6q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2537996
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2537997
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2537998
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2537999
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2538000
    move-object v1, v0

    goto :goto_1
.end method
