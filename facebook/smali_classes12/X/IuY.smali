.class public LX/IuY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2626876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626877
    const/16 v0, 0x64

    iput v0, p0, LX/IuY;->a:I

    .line 2626878
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x65

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Ljava/util/HashSet;-><init>(IF)V

    iput-object v0, p0, LX/IuY;->b:Ljava/util/Set;

    .line 2626879
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/IuY;->c:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 2626880
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 2626881
    iget-object v1, p0, LX/IuY;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2626882
    iget-object v1, p0, LX/IuY;->c:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2626883
    iget-object v0, p0, LX/IuY;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 2626884
    iget-object v0, p0, LX/IuY;->b:Ljava/util/Set;

    iget-object v1, p0, LX/IuY;->c:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2626885
    :cond_0
    const/4 v0, 0x0

    .line 2626886
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2626887
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
