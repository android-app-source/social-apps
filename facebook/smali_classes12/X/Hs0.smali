.class public final LX/Hs0;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/Hs1;


# direct methods
.method public constructor <init>(LX/Hs1;)V
    .locals 0

    .prologue
    .line 2513836
    iput-object p1, p0, LX/Hs0;->a:LX/Hs1;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2513837
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2513838
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2513839
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2513840
    if-nez v0, :cond_0

    .line 2513841
    iget-object v0, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v0, v0, LX/Hs1;->h:LX/03V;

    const-string v1, "albums_no_result_data_parcelable"

    const-string v2, "Composer result data parcelable is null."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513842
    :goto_0
    return-void

    .line 2513843
    :cond_0
    iget-object v1, p0, LX/Hs0;->a:LX/Hs1;

    sget-object v2, LX/9b0;->DEFAULT:LX/9b0;

    invoke-static {v2}, LX/9b2;->a(LX/9b0;)LX/9b1;

    move-result-object v2

    .line 2513844
    iput-object v2, v1, LX/Hs1;->k:LX/9b1;

    .line 2513845
    iget-object v1, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v1, v1, LX/Hs1;->a:Landroid/widget/ListView;

    iget-object v2, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v2, v2, LX/Hs1;->k:LX/9b1;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2513846
    iget-object v1, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v1, v1, LX/Hs1;->k:LX/9b1;

    invoke-virtual {v1, v0}, LX/9b1;->a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V

    .line 2513847
    iget-object v0, p0, LX/Hs0;->a:LX/Hs1;

    iget-boolean v0, v0, LX/Hs1;->j:Z

    if-eqz v0, :cond_1

    .line 2513848
    iget-object v0, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v0, v0, LX/Hs1;->k:LX/9b1;

    invoke-virtual {v0}, LX/9b1;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 2513849
    :cond_1
    iget-object v0, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v0, v0, LX/Hs1;->f:LX/Hrw;

    .line 2513850
    iget-object v1, v0, LX/Hrw;->b:LX/Hry;

    iget-object v1, v1, LX/Hry;->k:LX/Hro;

    invoke-virtual {v1}, LX/Hro;->b()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2513851
    iget-object v1, v0, LX/Hrw;->b:LX/Hry;

    iget-object v1, v1, LX/Hry;->j:LX/Hs1;

    iget-object v2, v0, LX/Hrw;->b:LX/Hry;

    iget-object v2, v2, LX/Hry;->k:LX/Hro;

    invoke-virtual {v2}, LX/Hro;->b()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    .line 2513852
    if-nez v2, :cond_3

    .line 2513853
    :cond_2
    :goto_1
    iget-object v1, v0, LX/Hrw;->a:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2513854
    iget-object v0, p0, LX/Hs0;->a:LX/Hs1;

    iget-object v0, v0, LX/Hs1;->g:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xe000b

    const-string v2, "perf_albums_list_fetch"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    goto :goto_0

    .line 2513855
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object p1

    .line 2513856
    iget-object v2, v1, LX/Hs1;->k:LX/9b1;

    if-eqz v2, :cond_4

    .line 2513857
    iget-object v2, v1, LX/Hs1;->k:LX/9b1;

    invoke-virtual {v2, p1}, LX/9b1;->a(Ljava/lang/String;)V

    .line 2513858
    :cond_4
    goto :goto_1
.end method
