.class public final LX/IzT;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field private static final c:LX/0sv;

.field private static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2634616
    new-instance v0, LX/0U1;

    const-string v1, "creation_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzT;->a:LX/0U1;

    .line 2634617
    new-instance v0, LX/0U1;

    const-string v1, "transaction_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzT;->b:LX/0U1;

    .line 2634618
    new-instance v0, LX/0su;

    sget-object v1, LX/IzT;->b:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/IzT;->c:LX/0sv;

    .line 2634619
    sget-object v0, LX/IzT;->a:LX/0U1;

    sget-object v1, LX/IzT;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IzT;->d:LX/0Px;

    .line 2634620
    const-string v0, "recent_all_transactions"

    const-string v1, "all_transactions_creation_time_index"

    sget-object v2, LX/IzT;->a:LX/0U1;

    invoke-virtual {v2}, LX/0U1;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->b(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IzT;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2634621
    const-string v0, "recent_all_transactions"

    sget-object v1, LX/IzT;->d:LX/0Px;

    sget-object v2, LX/IzT;->c:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2634622
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 2634613
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2634614
    sget-object v0, LX/IzT;->e:Ljava/lang/String;

    const v1, -0x615684c

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x28e0d7f0

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2634615
    return-void
.end method
