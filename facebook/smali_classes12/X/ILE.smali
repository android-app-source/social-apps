.class public final LX/ILE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;",
        ">;",
        "LX/ILO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ILF;


# direct methods
.method public constructor <init>(LX/ILF;)V
    .locals 0

    .prologue
    .line 2567751
    iput-object p1, p0, LX/ILE;->a:LX/ILF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2567752
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2567753
    if-eqz p1, :cond_0

    .line 2567754
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567755
    if-eqz v0, :cond_0

    .line 2567756
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567757
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2567758
    :cond_0
    const/4 v0, 0x0

    .line 2567759
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/ILO;

    .line 2567760
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567761
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 2567762
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567763
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2567764
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567765
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchAllSubGroupsGraphQLModels$FetchAllSubGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, LX/ILO;-><init>(LX/0Px;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0
.end method
