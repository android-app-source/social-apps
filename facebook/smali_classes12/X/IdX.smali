.class public LX/IdX;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static c:LX/IdX;


# instance fields
.field private final b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Thread;",
            "LX/IdW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2596898
    const-class v0, LX/IdX;

    sput-object v0, LX/IdX;->a:Ljava/lang/Class;

    .line 2596899
    const/4 v0, 0x0

    sput-object v0, LX/IdX;->c:LX/IdX;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2596900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2596901
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/IdX;->b:Ljava/util/WeakHashMap;

    .line 2596902
    return-void
.end method

.method private static declared-synchronized a(LX/IdX;Ljava/lang/Thread;)LX/IdW;
    .locals 2

    .prologue
    .line 2596903
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IdX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdW;

    .line 2596904
    if-nez v0, :cond_0

    .line 2596905
    new-instance v0, LX/IdW;

    invoke-direct {v0}, LX/IdW;-><init>()V

    .line 2596906
    iget-object v1, p0, LX/IdX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2596907
    :cond_0
    monitor-exit p0

    return-object v0

    .line 2596908
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a()LX/IdX;
    .locals 2

    .prologue
    .line 2596909
    const-class v1, LX/IdX;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/IdX;->c:LX/IdX;

    if-nez v0, :cond_0

    .line 2596910
    new-instance v0, LX/IdX;

    invoke-direct {v0}, LX/IdX;-><init>()V

    sput-object v0, LX/IdX;->c:LX/IdX;

    .line 2596911
    :cond_0
    sget-object v0, LX/IdX;->c:LX/IdX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2596912
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/Thread;Landroid/graphics/BitmapFactory$Options;)V
    .locals 1

    .prologue
    .line 2596913
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/IdX;->a(LX/IdX;Ljava/lang/Thread;)LX/IdW;

    move-result-object v0

    iput-object p2, v0, LX/IdW;->b:Landroid/graphics/BitmapFactory$Options;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2596914
    monitor-exit p0

    return-void

    .line 2596915
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 2596916
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IdX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdW;

    .line 2596917
    const/4 v1, 0x0

    iput-object v1, v0, LX/IdW;->b:Landroid/graphics/BitmapFactory$Options;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2596918
    monitor-exit p0

    return-void

    .line 2596919
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/Thread;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2596920
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IdX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2596921
    if-nez v0, :cond_0

    .line 2596922
    :goto_0
    monitor-exit p0

    return v1

    .line 2596923
    :cond_0
    :try_start_1
    iget-object v0, v0, LX/IdW;->a:LX/IdV;

    sget-object v2, LX/IdV;->CANCEL:LX/IdV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v2, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    .line 2596924
    goto :goto_0

    .line 2596925
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2596926
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/FileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2596927
    iget-boolean v1, p2, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-eqz v1, :cond_0

    .line 2596928
    :goto_0
    return-object v0

    .line 2596929
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 2596930
    invoke-direct {p0, v1}, LX/IdX;->c(Ljava/lang/Thread;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2596931
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Thread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not allowed to decode."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2596932
    :cond_1
    invoke-direct {p0, v1, p2}, LX/IdX;->a(Ljava/lang/Thread;Landroid/graphics/BitmapFactory$Options;)V

    .line 2596933
    invoke-static {p1, v0, p2}, LX/436;->a(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2596934
    invoke-direct {p0, v1}, LX/IdX;->b(Ljava/lang/Thread;)V

    goto :goto_0
.end method
