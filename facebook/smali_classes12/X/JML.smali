.class public LX/JML;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/JML;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JML;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:F

.field private b:F

.field private c:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2683666
    new-instance v0, LX/0Zi;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JML;->d:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2683648
    invoke-direct {p0}, LX/5r0;-><init>()V

    .line 2683649
    return-void
.end method

.method private static a(LX/JML;)LX/5pH;
    .locals 4

    .prologue
    .line 2683661
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2683662
    const-string v1, "scaleFactor"

    iget v2, p0, LX/JML;->a:F

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2683663
    const-string v1, "translateX"

    iget v2, p0, LX/JML;->b:F

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2683664
    const-string v1, "translateY"

    iget v2, p0, LX/JML;->c:F

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2683665
    return-object v0
.end method

.method public static a(IFFF)LX/JML;
    .locals 1

    .prologue
    .line 2683656
    sget-object v0, LX/JML;->d:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JML;

    .line 2683657
    if-nez v0, :cond_0

    .line 2683658
    new-instance v0, LX/JML;

    invoke-direct {v0}, LX/JML;-><init>()V

    .line 2683659
    :cond_0
    invoke-direct {v0, p0, p1, p2, p3}, LX/JML;->b(IFFF)V

    .line 2683660
    return-object v0
.end method

.method private b(IFFF)V
    .locals 0

    .prologue
    .line 2683667
    invoke-super {p0, p1}, LX/5r0;->a(I)V

    .line 2683668
    iput p2, p0, LX/JML;->a:F

    .line 2683669
    iput p3, p0, LX/JML;->b:F

    .line 2683670
    iput p4, p0, LX/JML;->c:F

    .line 2683671
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2683654
    sget-object v0, LX/JML;->d:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2683655
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2683651
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2683652
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, LX/JML;->a(LX/JML;)LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2683653
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683650
    const-string v0, "topZoom"

    return-object v0
.end method
