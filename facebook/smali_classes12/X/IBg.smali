.class public LX/IBg;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/IBX;


# instance fields
.field public a:LX/0s6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6Zi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/CK5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/events/model/Event;

.field public l:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 2547379
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547380
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IBg;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v3

    check-cast v3, LX/0s6;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v5

    check-cast v5, LX/1nQ;

    invoke-static {v0}, LX/6Zi;->a(LX/0QB;)LX/6Zi;

    move-result-object v6

    check-cast v6, LX/6Zi;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v7

    check-cast v7, LX/1nG;

    const/16 v8, 0xbc6

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object p1

    check-cast p1, LX/IBl;

    invoke-static {v0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v0

    check-cast v0, LX/CK5;

    iput-object v3, v2, LX/IBg;->a:LX/0s6;

    iput-object v4, v2, LX/IBg;->b:LX/0Zb;

    iput-object v5, v2, LX/IBg;->c:LX/1nQ;

    iput-object v6, v2, LX/IBg;->d:LX/6Zi;

    iput-object v7, v2, LX/IBg;->e:LX/1nG;

    iput-object v8, v2, LX/IBg;->f:LX/0Or;

    iput-object v9, v2, LX/IBg;->g:LX/0ad;

    iput-object v10, v2, LX/IBg;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v2, LX/IBg;->i:LX/IBl;

    iput-object v0, v2, LX/IBg;->j:LX/CK5;

    .line 2547381
    invoke-virtual {p0, p0}, LX/IBg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547382
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 4
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547383
    iput-object p1, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547384
    iput-object p3, p0, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2547385
    iget-object v0, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547386
    iget-object v1, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v1

    .line 2547387
    iget-object v1, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547388
    iget-object v2, v1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v1, v2

    .line 2547389
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2547390
    const/4 v2, 0x0

    .line 2547391
    :goto_0
    move-object v1, v2

    .line 2547392
    iget-object v2, p0, LX/IBg;->i:LX/IBl;

    invoke-virtual {v2, p0, v0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547393
    iget-object v0, p0, LX/IBg;->i:LX/IBl;

    const v1, 0x7f020339

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547394
    return-void

    .line 2547395
    :cond_0
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081f16

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string p3, ""

    aput-object p3, p1, p2

    const/4 p2, 0x1

    const-string p3, ""

    aput-object p3, p1, p2

    invoke-virtual {v2, v3, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2547396
    const-string v3, "\n"

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547397
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v0

    .line 2547398
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x4a57a535    # 3533133.2f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2547399
    new-instance v1, LX/4mb;

    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4mb;-><init>(Landroid/content/Context;)V

    .line 2547400
    iget-object v2, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547401
    iget-object v3, v2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v3

    .line 2547402
    iget-object v3, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547403
    iget-object v4, v3, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v3, v4

    .line 2547404
    iget-object v4, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547405
    iget v5, v4, Lcom/facebook/events/model/Event;->T:I

    move v4, v5

    .line 2547406
    const v5, 0x25d6af

    if-ne v4, v5, :cond_0

    .line 2547407
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f081ee7

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/IBa;

    invoke-direct {v6, p0, v4}, LX/IBa;-><init>(LX/IBg;I)V

    invoke-virtual {v1, v5, v6}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2547408
    :cond_0
    iget-object v4, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    invoke-virtual {v4}, Lcom/facebook/events/model/Event;->R()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2547409
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081f65

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547410
    new-instance v6, LX/IBe;

    invoke-direct {v6, p0, v5}, LX/IBe;-><init>(LX/IBg;Lcom/facebook/events/model/Event;)V

    move-object v5, v6

    .line 2547411
    invoke-virtual {v1, v4, v5}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2547412
    iget-object v4, p0, LX/IBg;->j:LX/CK5;

    invoke-virtual {v4}, LX/CK5;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2547413
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081f67

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547414
    new-instance v6, LX/IBf;

    invoke-direct {v6, p0, v5}, LX/IBf;-><init>(LX/IBg;Lcom/facebook/events/model/Event;)V

    move-object v5, v6

    .line 2547415
    invoke-virtual {v1, v4, v5}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2547416
    :cond_1
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2547417
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f081f63

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/IBb;

    invoke-direct {v4, p0, v3}, LX/IBb;-><init>(LX/IBg;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v4}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2547418
    :cond_2
    :goto_0
    new-instance v2, LX/IBd;

    invoke-direct {v2, p0}, LX/IBd;-><init>(LX/IBg;)V

    invoke-virtual {v1, v2}, LX/4mb;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 2547419
    invoke-virtual {v1}, LX/4mb;->show()Landroid/app/AlertDialog;

    .line 2547420
    const v1, 0x421ff621

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2547421
    :cond_3
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2547422
    invoke-virtual {p0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f081f64

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/IBc;

    invoke-direct {v4, p0, v2}, LX/IBc;-><init>(LX/IBg;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    goto :goto_0
.end method
