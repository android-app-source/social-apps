.class public LX/IKn;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:Lcom/facebook/fig/listitem/FigListItem;

.field public final m:LX/0wM;

.field public final n:LX/IK6;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/IK6;LX/0wM;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2567332
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2567333
    iput-object p3, p0, LX/IKn;->m:LX/0wM;

    .line 2567334
    iput-object p2, p0, LX/IKn;->n:LX/IK6;

    .line 2567335
    const v0, 0x7f0d14f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2567336
    iget-object v0, p0, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setThumbnailSizeType(I)V

    .line 2567337
    iget-object v0, p0, LX/IKn;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/IKn;->m:LX/0wM;

    const p2, 0x7f020970

    const p3, -0x4d4d4e

    invoke-virtual {v1, p2, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2567338
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2567339
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4a0d5b02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2567340
    iget-object v1, p0, LX/IKn;->n:LX/IK6;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/IK6;->a(Landroid/content/Context;)V

    .line 2567341
    const v1, -0xcc6ca86

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
