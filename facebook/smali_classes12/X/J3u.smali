.class public final LX/J3u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/J45;


# direct methods
.method public constructor <init>(LX/J45;Z)V
    .locals 0

    .prologue
    .line 2643388
    iput-object p1, p0, LX/J3u;->b:LX/J45;

    iput-boolean p2, p0, LX/J3u;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2643389
    iget-object v0, p0, LX/J3u;->b:LX/J45;

    iget-object v1, v0, LX/J45;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-boolean v0, p0, LX/J3u;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    goto :goto_0
.end method
