.class public final LX/J4k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:LX/J4p;


# direct methods
.method public constructor <init>(LX/J4p;LX/0Pz;)V
    .locals 0

    .prologue
    .line 2644467
    iput-object p1, p0, LX/J4k;->b:LX/J4p;

    iput-object p2, p0, LX/J4k;->a:LX/0Pz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2644468
    iget-object v0, p0, LX/J4k;->b:LX/J4p;

    iget-object v0, v0, LX/J4p;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    const-string v1, "none"

    iget-object v2, p0, LX/J4k;->a:LX/0Pz;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;LX/0Px;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
