.class public LX/JIF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JID;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2677605
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JIF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677606
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2677607
    iput-object p1, p0, LX/JIF;->b:LX/0Ot;

    .line 2677608
    return-void
.end method

.method public static a(LX/0QB;)LX/JIF;
    .locals 4

    .prologue
    .line 2677609
    const-class v1, LX/JIF;

    monitor-enter v1

    .line 2677610
    :try_start_0
    sget-object v0, LX/JIF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677611
    sput-object v2, LX/JIF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677614
    new-instance v3, LX/JIF;

    const/16 p0, 0x1acc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JIF;-><init>(LX/0Ot;)V

    .line 2677615
    move-object v0, v3

    .line 2677616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JIF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 11

    .prologue
    .line 2677620
    check-cast p2, LX/JIE;

    .line 2677621
    iget-object v0, p0, LX/JIF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    iget-object v1, p2, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p2, LX/JIE;->c:LX/Emj;

    .line 2677622
    iget-object v3, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JIy;

    .line 2677623
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v10

    .line 2677624
    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->b()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->b()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 2677625
    :cond_0
    :goto_0
    return-void

    .line 2677626
    :cond_1
    sget-object v5, LX/Emo;->COVER_PHOTO:LX/Emo;

    invoke-interface {v10}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v8

    move-object v4, v2

    move-object v9, v1

    invoke-interface/range {v4 .. v9}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2677627
    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->b()LX/1Fb;

    move-result-object v7

    .line 2677628
    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->e()Ljava/lang/String;

    move-result-object v6

    .line 2677629
    invoke-interface {v10}, LX/FSx;->t()LX/FSv;

    move-result-object v4

    invoke-interface {v4}, LX/FSv;->c()LX/FSu;

    move-result-object v4

    invoke-interface {v4}, LX/FSu;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2677630
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v8

    sget-object v9, LX/74S;->TIMELINE_COVER_PHOTO:LX/74S;

    move-object v4, v3

    invoke-static/range {v4 .. v9}, LX/JIy;->a(LX/JIy;Landroid/content/Context;Ljava/lang/String;LX/1Fb;LX/9hE;LX/74S;)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 12

    .prologue
    .line 2677631
    check-cast p2, LX/JIE;

    .line 2677632
    iget-object v0, p0, LX/JIF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    iget-object v1, p2, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p2, LX/JIE;->c:LX/Emj;

    .line 2677633
    iget-object v3, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JIy;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2677634
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v11

    .line 2677635
    invoke-interface {v11}, LX/FSx;->E()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v11}, LX/FSx;->E()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel$AssociatedVideoModel;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v11}, LX/FSx;->E()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel$AssociatedVideoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel$AssociatedVideoModel;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    move v10, v4

    .line 2677636
    :goto_0
    invoke-interface {v11}, LX/FSx;->C()LX/1Fb;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v11}, LX/FSx;->C()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v11}, LX/FSx;->D()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2677637
    :goto_1
    if-nez v10, :cond_3

    if-nez v4, :cond_3

    .line 2677638
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v10, v5

    .line 2677639
    goto :goto_0

    :cond_2
    move v4, v5

    .line 2677640
    goto :goto_1

    .line 2677641
    :cond_3
    sget-object v5, LX/Emo;->PROFILE_PICTURE:LX/Emo;

    invoke-interface {v11}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v8

    move-object v4, v2

    move-object v9, v1

    invoke-interface/range {v4 .. v9}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2677642
    if-eqz v10, :cond_4

    .line 2677643
    iget-object v4, v3, LX/JIy;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/D2M;

    .line 2677644
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v11}, LX/FSx;->E()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel$AssociatedVideoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel$AssociatedVideoModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/D2M;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    .line 2677645
    :cond_4
    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    invoke-interface {v4}, LX/FSt;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    invoke-interface {v4}, LX/FSt;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    invoke-interface {v4}, LX/FSt;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2677646
    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    invoke-interface {v4}, LX/FSt;->e()Ljava/lang/String;

    move-result-object v6

    .line 2677647
    invoke-interface {v11}, LX/FSx;->w()LX/FSt;

    move-result-object v4

    invoke-interface {v4}, LX/FSt;->d()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2677648
    invoke-static {v4}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v8

    .line 2677649
    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v11}, LX/FSx;->C()LX/1Fb;

    move-result-object v7

    sget-object v9, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    move-object v4, v3

    invoke-static/range {v4 .. v9}, LX/JIy;->a(LX/JIy;Landroid/content/Context;Ljava/lang/String;LX/1Fb;LX/9hE;LX/74S;)V

    goto/16 :goto_2

    .line 2677650
    :cond_5
    invoke-interface {v11}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2677651
    invoke-interface {v11}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v6

    .line 2677652
    invoke-static {v6}, LX/9hF;->f(Ljava/lang/String;)LX/9hE;

    move-result-object v8

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2677653
    check-cast p2, LX/JIE;

    .line 2677654
    iget-object v0, p0, LX/JIF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    iget-object v1, p2, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v2, p2, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    iget-object v3, p2, LX/JIE;->c:LX/Emj;

    .line 2677655
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v5

    .line 2677656
    iget-object v4, v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    .line 2677657
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x0

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    const p2, 0x7f0a00d5

    invoke-static {p1, p2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p2

    invoke-interface {p0, p2}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object p0

    invoke-static {p1, v5, v2, v4, v3}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a(LX/1De;LX/FSx;Landroid/graphics/drawable/Drawable;LX/1Ad;LX/Emj;)LX/1Di;

    move-result-object p2

    invoke-interface {p0, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {p1, v5, v4, v3}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a(LX/1De;LX/FSx;LX/1Ad;LX/Emj;)LX/1Di;

    move-result-object v4

    invoke-interface {p0, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-interface {v5}, LX/FSx;->ce_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const p0, 0x7f0b0058

    invoke-virtual {v5, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const p0, 0x7f0a00aa

    invoke-virtual {v5, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 p0, 0x7

    const p2, 0x7f0b2544

    invoke-interface {v5, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/4 p0, 0x2

    invoke-interface {v5, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2677658
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2677659
    invoke-static {}, LX/1dS;->b()V

    .line 2677660
    iget v0, p1, LX/1dQ;->b:I

    .line 2677661
    sparse-switch v0, :sswitch_data_0

    .line 2677662
    :goto_0
    return-object v2

    .line 2677663
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2677664
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JIF;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2677665
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2677666
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JIF;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x10055456 -> :sswitch_0
        -0x10053195 -> :sswitch_1
    .end sparse-switch
.end method
