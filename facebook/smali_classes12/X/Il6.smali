.class public LX/Il6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/5fv;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/5fv;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/5fv;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607389
    iput-object p1, p0, LX/Il6;->a:Landroid/content/res/Resources;

    .line 2607390
    iput-object p2, p0, LX/Il6;->b:LX/5fv;

    .line 2607391
    iput-object p3, p0, LX/Il6;->c:LX/0Or;

    .line 2607392
    return-void
.end method

.method private a(LX/Ikz;Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;)V
    .locals 7
    .param p2    # Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v4, -0x390684db

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2607393
    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 2607394
    :goto_2
    return-void

    .line 2607395
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2607396
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2607397
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2607398
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2607399
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2607400
    new-instance v5, LX/73g;

    invoke-direct {v5}, LX/73g;-><init>()V

    .line 2607401
    invoke-virtual {v3, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2607402
    iput-object v0, v5, LX/73g;->c:Ljava/lang/String;

    .line 2607403
    invoke-virtual {v3, v4, v6}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v3, v4, v6}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 2607404
    invoke-virtual {v3, v4, v6}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2607405
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    :goto_6
    if-eqz v1, :cond_5

    .line 2607406
    invoke-virtual {v3, v4, v6}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2607407
    iput-object v0, v5, LX/73g;->a:Ljava/lang/String;

    .line 2607408
    iget-object v0, p0, LX/Il6;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b0783

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 2607409
    iput v0, v5, LX/73g;->b:F

    .line 2607410
    :cond_5
    invoke-virtual {v5}, LX/73g;->f()LX/73f;

    move-result-object v0

    .line 2607411
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p1, LX/Ikz;->b:LX/0am;

    .line 2607412
    const/4 v0, 0x4

    invoke-virtual {v3, v4, v0}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2607413
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p1, LX/Ikz;->k:LX/0am;

    .line 2607414
    goto/16 :goto_2

    .line 2607415
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move v0, v2

    .line 2607416
    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    move v1, v2

    goto :goto_6

    :cond_a
    move v1, v2

    goto :goto_6
.end method

.method public static b(LX/0QB;)LX/Il6;
    .locals 4

    .prologue
    .line 2607417
    new-instance v2, LX/Il6;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v1

    check-cast v1, LX/5fv;

    const/16 v3, 0x19e

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/Il6;-><init>(Landroid/content/res/Resources;LX/5fv;LX/0Or;)V

    .line 2607418
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;)LX/Il0;
    .locals 11
    .param p1    # Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2607419
    if-nez p1, :cond_0

    .line 2607420
    const/4 v0, 0x0

    .line 2607421
    :goto_0
    return-object v0

    .line 2607422
    :cond_0
    new-instance v3, LX/Ikz;

    invoke-direct {v3}, LX/Ikz;-><init>()V

    .line 2607423
    invoke-direct {p0, v3, p1}, LX/Il6;->a(LX/Ikz;Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;)V

    .line 2607424
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2607425
    if-nez p1, :cond_b

    .line 2607426
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2607427
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->a:LX/0am;

    .line 2607428
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 2607429
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->h:LX/0am;

    .line 2607430
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v0

    .line 2607431
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->n:LX/0am;

    .line 2607432
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->nb_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->nb_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;->a()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->nb_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;->a()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2607433
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->nb_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel;->a()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$ReceiptImageModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2607434
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->i:LX/0am;

    .line 2607435
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->na_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionPaymentOptionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2607436
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->na_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionPaymentOptionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionPaymentOptionFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2607437
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->g:LX/0am;

    .line 2607438
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->na_()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionPaymentOptionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionPaymentOptionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2607439
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->m:LX/0am;

    .line 2607440
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2607441
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2607442
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 2607443
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2607444
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->e:LX/0am;

    .line 2607445
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2607446
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2607447
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->d:LX/0am;

    .line 2607448
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2607449
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2607450
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->q:LX/0am;

    .line 2607451
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2607452
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2607453
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iput-object v4, v3, LX/Ikz;->f:LX/0am;

    .line 2607454
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    if-eq v0, v4, :cond_9

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_EXPIRED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    if-eq v0, v4, :cond_9

    move v0, v1

    .line 2607455
    :goto_3
    iput-boolean v0, v3, LX/Ikz;->r:Z

    .line 2607456
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v0

    .line 2607457
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    if-eq v0, v4, :cond_f

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    if-eq v0, v4, :cond_f

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PENDING_APPROVAL:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    if-eq v0, v4, :cond_f

    const/4 v4, 0x1

    :goto_4
    move v0, v4

    .line 2607458
    if-nez v0, :cond_a

    iget-object v0, p0, LX/Il6;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2607459
    iget-boolean v4, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v4

    .line 2607460
    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->e()Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/Il6;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2607461
    iget-object v5, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v5

    .line 2607462
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2607463
    :goto_5
    iput-boolean v1, v3, LX/Ikz;->s:Z

    .line 2607464
    new-instance v0, LX/Il0;

    invoke-direct {v0, v3}, LX/Il0;-><init>(LX/Ikz;)V

    move-object v0, v0

    .line 2607465
    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 2607466
    goto/16 :goto_2

    :cond_8
    move v0, v2

    goto/16 :goto_2

    :cond_9
    move v0, v2

    .line 2607467
    goto :goto_3

    :cond_a
    move v1, v2

    .line 2607468
    goto :goto_5

    .line 2607469
    :cond_b
    new-instance v8, LX/InR;

    invoke-direct {v8}, LX/InR;-><init>()V

    .line 2607470
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->n()I

    move-result v9

    invoke-static {v5, v9}, LX/7j4;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 2607471
    iput-object v5, v8, LX/InR;->a:Ljava/lang/String;

    .line 2607472
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->o()I

    move-result v9

    invoke-static {v5, v9}, LX/7j4;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 2607473
    iput-object v5, v8, LX/InR;->d:Ljava/lang/String;

    .line 2607474
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_e

    .line 2607475
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2607476
    invoke-virtual {v9, v5, v6}, LX/15i;->g(II)I

    move-result v5

    if-eqz v5, :cond_d

    move v5, v6

    :goto_6
    if-eqz v5, :cond_c

    .line 2607477
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->p()LX/1vs;

    move-result-object v5

    iget-object v9, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v9, v5, v6}, LX/15i;->g(II)I

    move-result v5

    invoke-virtual {v9, v5, v7}, LX/15i;->j(II)I

    move-result v5

    .line 2607478
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, LX/7j4;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 2607479
    iput-object v5, v8, LX/InR;->c:Ljava/lang/String;

    .line 2607480
    :cond_c
    new-instance v5, LX/InQ;

    invoke-direct {v5, v8}, LX/InQ;-><init>(LX/InR;)V

    move-object v5, v5

    .line 2607481
    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    iput-object v6, v3, LX/Ikz;->c:LX/0am;

    .line 2607482
    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2607483
    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    iput-object v6, v3, LX/Ikz;->o:LX/0am;

    .line 2607484
    iget-object v5, p0, LX/Il6;->b:LX/5fv;

    new-instance v6, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceQueryModels$TransactionInvoiceFieldsModel;->o()I

    move-result v8

    int-to-long v9, v8

    invoke-direct {v6, v7, v9, v10}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sget-object v7, LX/5fu;->NO_CURRENCY_SYMBOL:LX/5fu;

    invoke-virtual {v5, v6, v7}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v5

    .line 2607485
    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    iput-object v6, v3, LX/Ikz;->p:LX/0am;

    .line 2607486
    goto/16 :goto_1

    :cond_d
    move v5, v7

    .line 2607487
    goto :goto_6

    :cond_e
    move v5, v7

    goto :goto_6

    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_4
.end method
