.class public final enum LX/IM5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IM5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IM5;

.field public static final enum TASK_ANSWER_A_QUESTION:LX/IM5;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2569018
    new-instance v0, LX/IM5;

    const-string v1, "TASK_ANSWER_A_QUESTION"

    invoke-direct {v0, v1, v2}, LX/IM5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IM5;->TASK_ANSWER_A_QUESTION:LX/IM5;

    .line 2569019
    const/4 v0, 0x1

    new-array v0, v0, [LX/IM5;

    sget-object v1, LX/IM5;->TASK_ANSWER_A_QUESTION:LX/IM5;

    aput-object v1, v0, v2

    sput-object v0, LX/IM5;->$VALUES:[LX/IM5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2569020
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IM5;
    .locals 1

    .prologue
    .line 2569021
    const-class v0, LX/IM5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IM5;

    return-object v0
.end method

.method public static values()[LX/IM5;
    .locals 1

    .prologue
    .line 2569022
    sget-object v0, LX/IM5;->$VALUES:[LX/IM5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IM5;

    return-object v0
.end method
