.class public LX/IfR;
.super LX/6Lb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Iey;

.field public final b:LX/Ieu;

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/Iey;LX/Ieu;LX/0SG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599685
    invoke-direct {p0, p1}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2599686
    iput-object p2, p0, LX/IfR;->a:LX/Iey;

    .line 2599687
    iput-object p3, p0, LX/IfR;->b:LX/Ieu;

    .line 2599688
    iput-object p4, p0, LX/IfR;->c:LX/0SG;

    .line 2599689
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599690
    check-cast p1, Ljava/lang/String;

    .line 2599691
    iget-object v0, p0, LX/IfR;->a:LX/Iey;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2599692
    invoke-static {v1}, LX/Iey;->b(Ljava/lang/String;)LX/0zO;

    move-result-object v2

    .line 2599693
    iget-object p2, v0, LX/Iey;->b:LX/0tX;

    invoke-virtual {p2, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2599694
    new-instance p2, LX/Iex;

    invoke-direct {p2, v0}, LX/Iex;-><init>(LX/Iey;)V

    invoke-static {v2, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2599695
    new-instance v1, LX/IfQ;

    invoke-direct {v1, p0, p1}, LX/IfQ;-><init>(LX/IfR;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599696
    check-cast p1, Ljava/lang/String;

    .line 2599697
    iget-object v0, p0, LX/IfR;->b:LX/Ieu;

    invoke-virtual {v0, p1}, LX/Ieu;->a(Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    move-result-object v0

    .line 2599698
    if-eqz v0, :cond_1

    .line 2599699
    iget-object v2, p0, LX/IfR;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2599700
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/6LZ;->a(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    .line 2599701
    :goto_1
    return-object v0

    .line 2599702
    :cond_0
    invoke-static {v0}, LX/6LZ;->b(Ljava/lang/Object;)LX/6LZ;

    move-result-object v0

    goto :goto_1

    .line 2599703
    :cond_1
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
