.class public final LX/HIE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2450733
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2450734
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450735
    :goto_0
    return v1

    .line 2450736
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450737
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2450738
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2450739
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450740
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2450741
    const-string v4, "cta_admin_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2450742
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2450743
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_9

    .line 2450744
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450745
    :goto_2
    move v2, v3

    .line 2450746
    goto :goto_1

    .line 2450747
    :cond_2
    const-string v4, "cta_type"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2450748
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 2450749
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2450750
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2450751
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2450752
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2450753
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 2450754
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2450755
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450756
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_5

    if-eqz v6, :cond_5

    .line 2450757
    const-string v7, "can_see_new_cta"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2450758
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v5, v2

    move v2, v4

    goto :goto_3

    .line 2450759
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2450760
    :cond_7
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2450761
    if-eqz v2, :cond_8

    .line 2450762
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 2450763
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    move v5, v3

    goto :goto_3
.end method
