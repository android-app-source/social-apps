.class public LX/IDs;
.super LX/2hX;
.source ""


# instance fields
.field public c:Landroid/content/Context;

.field private d:LX/DHs;

.field public e:LX/DHr;

.field public f:LX/2do;

.field public g:LX/0Sh;

.field public h:LX/17W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DHs;LX/DHr;LX/2dj;LX/2do;LX/0Sh;LX/2hZ;LX/17W;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/DHs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DHr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551262
    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, LX/2hX;-><init>(Landroid/content/Context;LX/2dj;LX/2do;LX/0Sh;LX/2hZ;)V

    .line 2551263
    iput-object p1, p0, LX/IDs;->c:Landroid/content/Context;

    .line 2551264
    iput-object p2, p0, LX/IDs;->d:LX/DHs;

    .line 2551265
    iput-object p3, p0, LX/IDs;->e:LX/DHr;

    .line 2551266
    iput-object p5, p0, LX/IDs;->f:LX/2do;

    .line 2551267
    iput-object p6, p0, LX/IDs;->g:LX/0Sh;

    .line 2551268
    iput-object p8, p0, LX/IDs;->h:LX/17W;

    .line 2551269
    return-void
.end method

.method public static a(LX/IDs;JJLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/5OM;
    .locals 13

    .prologue
    .line 2551217
    new-instance v10, LX/6WS;

    iget-object v1, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-direct {v10, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2551218
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, LX/5OM;->a(Z)V

    .line 2551219
    new-instance v1, LX/5OG;

    iget-object v2, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2551220
    invoke-virtual {v10, v1}, LX/5OM;->a(LX/5OG;)V

    .line 2551221
    const v2, 0x7f0d021f

    const/4 v3, 0x0

    iget-object v4, p0, LX/IDs;->c:Landroid/content/Context;

    const v5, 0x7f083833

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f02089e

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2551222
    const v2, 0x7f0d0218

    const/4 v3, 0x0

    iget-object v4, p0, LX/IDs;->c:Landroid/content/Context;

    const v5, 0x7f08382a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    const v2, 0x7f08382b

    move-object/from16 v0, p5

    invoke-static {p0, v2, v0}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020888

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2551223
    new-instance v1, LX/IDl;

    move-object v2, p0

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-wide v8, p1

    invoke-direct/range {v1 .. v9}, LX/IDl;-><init>(LX/IDs;JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;J)V

    invoke-virtual {v10, v1}, LX/5OM;->a(LX/5OO;)V

    .line 2551224
    return-object v10
.end method

.method public static a(LX/IDs;JJLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/5OM;
    .locals 13

    .prologue
    .line 2551225
    new-instance v11, LX/6WS;

    iget-object v2, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-direct {v11, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2551226
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, LX/5OM;->a(Z)V

    .line 2551227
    new-instance v2, LX/5OG;

    iget-object v3, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2551228
    invoke-virtual {v11, v2}, LX/5OM;->a(LX/5OG;)V

    .line 2551229
    sget-object v3, LX/IDr;->b:[I

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2551230
    :goto_0
    new-instance v2, LX/IDm;

    move-object v3, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-direct/range {v2 .. v10}, LX/IDm;-><init>(LX/IDs;JJLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v11, v2}, LX/5OM;->a(LX/5OO;)V

    .line 2551231
    return-object v11

    .line 2551232
    :pswitch_0
    const v3, 0x7f0d021c

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f083830

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    const v4, 0x7f020897

    invoke-virtual {v3, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2551233
    const v3, 0x7f0d021a

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f08382e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    const v4, 0x7f020740

    invoke-virtual {v3, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2551234
    sget-object v3, LX/IDr;->a:[I

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 2551235
    :pswitch_1
    const v3, 0x7f0d021e

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f083831

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f083832

    move-object/from16 v0, p5

    invoke-static {p0, v3, v0}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020a1d

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 2551236
    :pswitch_2
    const v3, 0x7f0d0219

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f08382c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f08382d

    move-object/from16 v0, p5

    invoke-static {p0, v3, v0}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f02087f

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2551237
    :pswitch_3
    const v3, 0x7f0d021c

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f083830

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    const v4, 0x7f020897

    invoke-virtual {v3, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2551238
    sget-object v3, LX/DHs;->PYMK:LX/DHs;

    iget-object v4, p0, LX/IDs;->d:LX/DHs;

    invoke-virtual {v3, v4}, LX/DHs;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, LX/DHs;->SUGGESTIONS:LX/DHs;

    iget-object v4, p0, LX/IDs;->d:LX/DHs;

    invoke-virtual {v3, v4}, LX/DHs;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2551239
    :cond_0
    const v3, 0x7f0d021b

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f08382f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    const v4, 0x7f02089e

    invoke-virtual {v3, v4}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2551240
    :cond_1
    const v3, 0x7f0d0218

    const/4 v4, 0x0

    iget-object v5, p0, LX/IDs;->c:Landroid/content/Context;

    const v6, 0x7f08382a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    const v3, 0x7f08382b

    move-object/from16 v0, p5

    invoke-static {p0, v3, v0}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020888

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2551241
    iget-object v0, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/IDs;JJLjava/lang/String;)V
    .locals 10

    .prologue
    .line 2551242
    new-instance v3, LX/IDn;

    move-object v4, p0

    move-wide v5, p1

    move-wide v7, p3

    invoke-direct/range {v3 .. v8}, LX/IDn;-><init>(LX/IDs;JJ)V

    .line 2551243
    new-instance v4, LX/IDo;

    invoke-direct {v4, p0}, LX/IDo;-><init>(LX/IDs;)V

    .line 2551244
    new-instance v5, LX/0ju;

    iget-object v6, p0, LX/IDs;->c:Landroid/content/Context;

    invoke-direct {v5, v6}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v6, 0x7f08154b

    invoke-virtual {v5, v6, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v5, 0x7f080017

    invoke-virtual {v3, v5, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    move-object v0, v3

    .line 2551245
    const v1, 0x7f083834

    invoke-static {p0, v1, p5}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2551246
    const v1, 0x7f083835

    invoke-static {p0, v1, p5}, LX/IDs;->a(LX/IDs;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2551247
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2551248
    return-void
.end method

.method public static a$redex0(LX/IDs;JZ)V
    .locals 11

    .prologue
    .line 2551249
    if-eqz p3, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v8, v0

    .line 2551250
    :goto_0
    if-eqz p3, :cond_1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2551251
    :goto_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2551252
    if-eqz p3, :cond_2

    .line 2551253
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PROFILE_FRIENDS_PAGE"

    invoke-virtual {v0, v1, v2}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v7, v0

    .line 2551254
    :goto_2
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, LX/IDs;->b$redex0(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2551255
    iget-object v9, p0, LX/IDs;->g:LX/0Sh;

    new-instance v0, LX/IDp;

    move-object v1, p0

    move-wide v2, p1

    move-object v6, v8

    invoke-direct/range {v0 .. v6}, LX/IDp;-><init>(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v9, v7, v0}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2551256
    return-void

    .line 2551257
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v8, v0

    goto :goto_0

    .line 2551258
    :cond_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_1

    .line 2551259
    :cond_2
    iget-object v0, p0, LX/2hY;->a:LX/2dj;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PROFILE_FRIENDS_PAGE"

    invoke-virtual {v0, v1, v2}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v7, v0

    goto :goto_2
.end method

.method public static b$redex0(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V
    .locals 9

    .prologue
    .line 2551260
    iget-object v7, p0, LX/IDs;->g:LX/0Sh;

    new-instance v0, Lcom/facebook/friendlist/listadapter/ActionButtonsController$7;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/friendlist/listadapter/ActionButtonsController$7;-><init>(LX/IDs;JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v7, v0}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2551261
    return-void
.end method
