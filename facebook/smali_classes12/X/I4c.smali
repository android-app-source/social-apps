.class public final LX/I4c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I4d;


# direct methods
.method public constructor <init>(LX/I4d;)V
    .locals 0

    .prologue
    .line 2534260
    iput-object p1, p0, LX/I4c;->a:LX/I4d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x5123f633    # -1.0006183E-10f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2534261
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/I4c;->a:LX/I4d;

    iget-object v0, v0, LX/I4d;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 2534262
    const-string v0, "target_fragment"

    sget-object v1, LX/0cQ;->EVENTS_DISCOVERY_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534263
    iget-object v0, p0, LX/I4c;->a:LX/I4d;

    iget-object v0, v0, LX/I4d;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_0

    .line 2534264
    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    const-string v1, "unknown"

    const-string v2, "unknown"

    const-string v3, "event_collection"

    const-string v4, "event_collection"

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2534265
    :goto_0
    const-string v1, "extra_reaction_analytics_params"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2534266
    iget-object v0, p0, LX/I4c;->a:LX/I4d;

    iget-object v0, v0, LX/I4d;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v7, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2534267
    const v0, -0x18e117da

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2534268
    :cond_0
    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, LX/I4c;->a:LX/I4d;

    iget-object v1, v1, LX/I4d;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I4c;->a:LX/I4d;

    iget-object v2, v2, LX/I4d;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    const-string v3, "event_collection"

    const-string v4, "event_collection"

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method
