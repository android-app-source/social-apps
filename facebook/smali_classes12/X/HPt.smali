.class public final LX/HPt;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2462621
    const-class v1, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;

    const v0, -0xe7ff8a3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PageReorderTabQuery"

    const-string v6, "c97488f6fcb5fac10f68660cccc36079"

    const-string v7, "node"

    const-string v8, "10155261262196729"

    const-string v9, "10155263176671729"

    .line 2462622
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2462623
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2462624
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2462625
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2462626
    sparse-switch v0, :sswitch_data_0

    .line 2462627
    :goto_0
    return-object p1

    .line 2462628
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2462629
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2462630
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2462631
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2462632
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x71e86c6d -> :sswitch_3
        -0x2fe52f35 -> :sswitch_1
        0x50f8fde2 -> :sswitch_0
        0x57a06f49 -> :sswitch_2
        0x78326898 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2462633
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2462634
    :goto_1
    return v0

    .line 2462635
    :pswitch_1
    const-string v3, "3"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v3, "4"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :pswitch_3
    const-string v3, "0"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 2462636
    :pswitch_4
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2462637
    :pswitch_5
    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2462638
    :pswitch_6
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
