.class public LX/JSe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jW;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Landroid/net/Uri;

.field public final g:Landroid/net/Uri;

.field public final h:Landroid/net/Uri;

.field public final i:Landroid/net/Uri;

.field public final j:Landroid/net/Uri;

.field public final k:Ljava/lang/String;

.field public final l:Landroid/net/Uri;


# direct methods
.method public constructor <init>(LX/JSd;)V
    .locals 1

    .prologue
    .line 2695760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695761
    iget-object v0, p1, LX/JSd;->b:Ljava/lang/String;

    iput-object v0, p0, LX/JSe;->a:Ljava/lang/String;

    .line 2695762
    iget-object v0, p1, LX/JSd;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, LX/JSe;->b:Ljava/lang/String;

    .line 2695763
    iget-object v0, p1, LX/JSd;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    iput-object v0, p0, LX/JSe;->c:Ljava/lang/String;

    .line 2695764
    iget-object v0, p1, LX/JSd;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    iput-object v0, p0, LX/JSe;->d:Ljava/lang/String;

    .line 2695765
    iget-object v0, p1, LX/JSd;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    iput-object v0, p0, LX/JSe;->e:Ljava/lang/String;

    .line 2695766
    iget-object v0, p1, LX/JSd;->g:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->f:Landroid/net/Uri;

    .line 2695767
    iget-object v0, p1, LX/JSd;->k:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->g:Landroid/net/Uri;

    .line 2695768
    iget-object v0, p1, LX/JSd;->a:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->h:Landroid/net/Uri;

    .line 2695769
    iget-object v0, p1, LX/JSd;->h:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->i:Landroid/net/Uri;

    .line 2695770
    iget-object v0, p1, LX/JSd;->i:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->j:Landroid/net/Uri;

    .line 2695771
    iget-object v0, p1, LX/JSd;->j:Ljava/lang/String;

    iput-object v0, p0, LX/JSe;->k:Ljava/lang/String;

    .line 2695772
    iget-object v0, p1, LX/JSd;->l:Landroid/net/Uri;

    iput-object v0, p0, LX/JSe;->l:Landroid/net/Uri;

    .line 2695773
    return-void

    .line 2695774
    :cond_0
    iget-object v0, p1, LX/JSd;->c:Ljava/lang/String;

    goto :goto_0

    .line 2695775
    :cond_1
    iget-object v0, p1, LX/JSd;->d:Ljava/lang/String;

    goto :goto_1

    .line 2695776
    :cond_2
    iget-object v0, p1, LX/JSd;->e:Ljava/lang/String;

    goto :goto_2

    .line 2695777
    :cond_3
    iget-object v0, p1, LX/JSd;->f:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2695778
    iget-object v0, p0, LX/JSe;->f:Landroid/net/Uri;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2695779
    iget-object v0, p0, LX/JSe;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JSe;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/JSe;->h:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
