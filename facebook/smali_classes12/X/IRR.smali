.class public final LX/IRR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IRS;


# direct methods
.method public constructor <init>(LX/IRS;)V
    .locals 0

    .prologue
    .line 2576417
    iput-object p1, p0, LX/IRR;->a:LX/IRS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2576418
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2576419
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2576420
    if-eqz p1, :cond_0

    .line 2576421
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576422
    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    if-eqz v0, :cond_8

    .line 2576423
    :goto_3
    return-void

    .line 2576424
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576425
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2576426
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2576427
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576428
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2576429
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    if-nez v0, :cond_5

    move v0, v2

    goto :goto_1

    .line 2576430
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2576431
    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_1

    .line 2576432
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576433
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2576434
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    .line 2576435
    :cond_8
    iget-object v3, p0, LX/IRR;->a:LX/IRS;

    .line 2576436
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576437
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->a()Z

    move-result v0

    .line 2576438
    iput-boolean v0, v3, LX/IRS;->s:Z

    .line 2576439
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576440
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2576441
    iget-object v4, p0, LX/IRR;->a:LX/IRS;

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2576442
    iput-object v5, v4, LX/IRS;->q:Ljava/lang/String;

    .line 2576443
    iget-object v4, p0, LX/IRR;->a:LX/IRS;

    invoke-virtual {v3, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/IRR;->a:LX/IRS;

    iget-object v0, v0, LX/IRS;->q:Ljava/lang/String;

    if-nez v0, :cond_a

    :cond_9
    move v0, v2

    .line 2576444
    :goto_5
    iput-boolean v0, v4, LX/IRS;->o:Z

    .line 2576445
    iget-object v0, p0, LX/IRR;->a:LX/IRS;

    iget-object v0, v0, LX/IRS;->p:LX/0Px;

    if-nez v0, :cond_c

    .line 2576446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576447
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v2, p0, LX/IRR;->a:LX/IRS;

    if-eqz v0, :cond_b

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2576448
    :goto_6
    iput-object v0, v2, LX/IRS;->p:LX/0Px;

    .line 2576449
    :goto_7
    iget-object v0, p0, LX/IRR;->a:LX/IRS;

    iget-object v0, v0, LX/IRS;->j:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    iget-object v2, p0, LX/IRR;->a:LX/IRS;

    iget-object v2, v2, LX/IRS;->p:LX/0Px;

    .line 2576450
    iput-object v2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->b:LX/0Px;

    .line 2576451
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2576452
    iget-object v0, p0, LX/IRR;->a:LX/IRS;

    .line 2576453
    iput-boolean v1, v0, LX/IRS;->n:Z

    .line 2576454
    goto/16 :goto_3

    .line 2576455
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    move v0, v1

    .line 2576456
    goto :goto_5

    .line 2576457
    :cond_b
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2576458
    goto :goto_6

    .line 2576459
    :cond_c
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2576460
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2576461
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2576462
    iget-object v3, p0, LX/IRR;->a:LX/IRS;

    iget-object v4, p0, LX/IRR;->a:LX/IRS;

    iget-object v4, v4, LX/IRS;->p:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    if-eqz v0, :cond_d

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_8
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2576463
    iput-object v0, v3, LX/IRS;->p:LX/0Px;

    .line 2576464
    goto :goto_7

    .line 2576465
    :cond_d
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2576466
    goto :goto_8
.end method
