.class public LX/Hpb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hpa;


# instance fields
.field private a:Lcom/facebook/common/callercontext/CallerContext;

.field private b:Ljava/lang/String;

.field public c:LX/1QO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2509708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2509709
    iput-object p1, p0, LX/Hpb;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2509710
    iput-object p2, p0, LX/Hpb;->b:Ljava/lang/String;

    .line 2509711
    return-void
.end method


# virtual methods
.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
    .end annotation

    .prologue
    .line 2509712
    iget-object v0, p0, LX/Hpb;->c:LX/1QO;

    return-object v0
.end method

.method public getImageDownloader()LX/1Ad;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "imageDownloader"
    .end annotation

    .prologue
    .line 2509713
    iget-object v0, p0, LX/Hpb;->d:LX/1Ad;

    return-object v0
.end method

.method public getScenePath()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "scenePath"
    .end annotation

    .prologue
    .line 2509714
    iget-object v0, p0, LX/Hpb;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "sessionId"
    .end annotation

    .prologue
    .line 2509715
    iget-object v0, p0, LX/Hpb;->b:Ljava/lang/String;

    return-object v0
.end method
