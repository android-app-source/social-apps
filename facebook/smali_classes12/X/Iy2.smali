.class public LX/Iy2;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/payments/cart/model/CartItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/Iy7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Iy7;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2632730
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2632731
    iput-object p2, p0, LX/Iy2;->a:LX/Iy7;

    .line 2632732
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2632726
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2632727
    invoke-virtual {p0, p1}, LX/Iy2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2632728
    iget-object p0, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    move-object v0, p0

    .line 2632729
    invoke-virtual {v0}, LX/IyK;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2632725
    iget-object v1, p0, LX/Iy2;->a:LX/Iy7;

    invoke-virtual {p0, p1}, LX/Iy2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    invoke-virtual {v1, v0, p2, p3}, LX/Iy7;->a(Lcom/facebook/payments/cart/model/SimpleCartItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2632724
    invoke-static {}, LX/IyK;->values()[LX/IyK;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2632721
    invoke-virtual {p0, p1}, LX/Iy2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/cart/model/SimpleCartItem;

    .line 2632722
    iget-object p0, v0, Lcom/facebook/payments/cart/model/SimpleCartItem;->b:LX/IyK;

    move-object v0, p0

    .line 2632723
    invoke-virtual {v0}, LX/IyK;->isSelectable()Z

    move-result v0

    return v0
.end method
