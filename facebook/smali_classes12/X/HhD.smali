.class public LX/HhD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2495147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2495148
    return-void
.end method

.method public static a(Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Z)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2495120
    new-instance v4, LX/0cA;

    invoke-direct {v4}, LX/0cA;-><init>()V

    .line 2495121
    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    iget-object v1, p0, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2495122
    invoke-static {v1}, LX/6YB;->fromString(Ljava/lang/String;)LX/6YB;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2495123
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2495124
    :cond_0
    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    move-object v1, v1

    .line 2495125
    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YB;

    .line 2495126
    sget-object v5, LX/HhC;->a:[I

    invoke-virtual {v0}, LX/6YB;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_2
    move v1, v0

    .line 2495127
    goto :goto_1

    :pswitch_0
    move v2, v3

    .line 2495128
    goto :goto_1

    :pswitch_1
    move v0, v3

    .line 2495129
    goto :goto_2

    .line 2495130
    :cond_1
    if-eqz p1, :cond_4

    .line 2495131
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 2495132
    const v0, 0x7f02029b

    .line 2495133
    :goto_3
    return v0

    .line 2495134
    :cond_2
    if-eqz v1, :cond_3

    .line 2495135
    const v0, 0x7f02143d

    goto :goto_3

    .line 2495136
    :cond_3
    const v0, 0x7f02039d

    goto :goto_3

    .line 2495137
    :cond_4
    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    .line 2495138
    const v0, 0x7f02029c

    goto :goto_3

    .line 2495139
    :cond_5
    if-eqz v1, :cond_6

    .line 2495140
    const v0, 0x7f02143e

    goto :goto_3

    .line 2495141
    :cond_6
    const v0, 0x7f0203a5

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2495142
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2495143
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2495144
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const v4, 0x7f0b0034

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2495145
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x7f0a0948

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2495146
    return-object v1
.end method
