.class public LX/Ie2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0gh;

.field public final c:LX/2MV;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0TD;

.field public final f:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0gh;LX/2MV;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/03V;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598263
    iput-object p1, p0, LX/Ie2;->a:Landroid/content/Context;

    .line 2598264
    iput-object p2, p0, LX/Ie2;->b:LX/0gh;

    .line 2598265
    iput-object p3, p0, LX/Ie2;->c:LX/2MV;

    .line 2598266
    iput-object p4, p0, LX/Ie2;->d:Ljava/util/concurrent/ExecutorService;

    .line 2598267
    iput-object p5, p0, LX/Ie2;->e:LX/0TD;

    .line 2598268
    iput-object p6, p0, LX/Ie2;->f:LX/03V;

    .line 2598269
    return-void
.end method

.method public static a$redex0(LX/Ie2;Ljava/lang/Long;Ljava/lang/String;LX/0QK;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "LX/0QK",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2598254
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2598255
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2598256
    if-eqz v0, :cond_0

    .line 2598257
    invoke-static {p3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2598258
    iget-object v1, p0, LX/Ie2;->a:Landroid/content/Context;

    const v2, 0x7f083a12

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, LX/Ie2;->a:Landroid/content/Context;

    const v2, 0x7f083a13

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p3, "messenger_video_too_short_dialog"

    move-object v1, p0

    move-object v2, p2

    move-object v3, v0

    .line 2598259
    new-instance p0, LX/31Y;

    iget-object p2, v1, LX/Ie2;->a:Landroid/content/Context;

    invoke-direct {p0, p2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object p0

    const p2, 0x104000a

    new-instance v0, LX/Idz;

    invoke-direct {v0, v1, v3}, LX/Idz;-><init>(LX/Ie2;LX/0am;)V

    invoke-virtual {p0, p2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    new-instance p2, LX/Idy;

    invoke-direct {p2, v1, v3}, LX/Idy;-><init>(LX/Ie2;LX/0am;)V

    invoke-virtual {p0, p2}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 2598260
    iget-object p0, v1, LX/Ie2;->b:LX/0gh;

    const/4 p2, 0x1

    const-string v0, "from_module"

    invoke-static {v0, v2}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, p3, p2, v0}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 2598261
    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
