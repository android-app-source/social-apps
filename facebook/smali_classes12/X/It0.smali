.class public LX/It0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/It0;


# instance fields
.field private final a:LX/3Ed;


# direct methods
.method public constructor <init>(LX/3Ed;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2621705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621706
    iput-object p1, p0, LX/It0;->a:LX/3Ed;

    .line 2621707
    return-void
.end method

.method public static a(LX/0QB;)LX/It0;
    .locals 4

    .prologue
    .line 2621708
    sget-object v0, LX/It0;->b:LX/It0;

    if-nez v0, :cond_1

    .line 2621709
    const-class v1, LX/It0;

    monitor-enter v1

    .line 2621710
    :try_start_0
    sget-object v0, LX/It0;->b:LX/It0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2621711
    if-eqz v2, :cond_0

    .line 2621712
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2621713
    new-instance p0, LX/It0;

    invoke-static {v0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v3

    check-cast v3, LX/3Ed;

    invoke-direct {p0, v3}, LX/It0;-><init>(LX/3Ed;)V

    .line 2621714
    move-object v0, p0

    .line 2621715
    sput-object v0, LX/It0;->b:LX/It0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2621716
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2621717
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2621718
    :cond_1
    sget-object v0, LX/It0;->b:LX/It0;

    return-object v0

    .line 2621719
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2621720
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2621721
    iget-object v0, p0, LX/It0;->a:LX/3Ed;

    invoke-virtual {v0}, LX/3Ed;->a()J

    move-result-wide v0

    return-wide v0
.end method
