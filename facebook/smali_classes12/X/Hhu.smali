.class public final LX/Hhu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2496378
    iput-object p1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3f8fb698

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496379
    iget-object v1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->c:LX/0if;

    sget-object v2, LX/0ig;->X:LX/0ih;

    const-string v3, "confirmation_close"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2496380
    iget-object v1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->d:LX/Hht;

    iget-object v2, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->j:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Hht;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->b:LX/Gm9;

    .line 2496381
    iget-boolean v2, v1, LX/Gm9;->f:Z

    move v1, v2

    .line 2496382
    if-eqz v1, :cond_0

    .line 2496383
    iget-object v1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;->j:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v2, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083759

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setError(Ljava/lang/CharSequence;)V

    .line 2496384
    const v1, -0x1581d5d3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2496385
    :goto_0
    return-void

    .line 2496386
    :cond_0
    iget-object v1, p0, LX/Hhu;->a:Lcom/facebook/zero/onboarding/fragment/FreeFacebookConfirmationFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2496387
    const v1, -0x1ee36aec

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
