.class public final LX/Ivi;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 0

    .prologue
    .line 2628534
    iput-object p1, p0, LX/Ivi;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628535
    invoke-direct {p0, p1}, LX/Ivi;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2628536
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2628537
    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 2628538
    iget-object v1, p0, LX/Ivi;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    iget-object v2, p0, LX/Ivi;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0a60

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    iget-object v3, p0, LX/Ivi;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0a59

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-virtual {v1, v2, v6, v0, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 2628539
    :cond_0
    return-void
.end method
