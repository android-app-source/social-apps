.class public final LX/HiG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HiH;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/graphics/Bitmap;

.field private c:I

.field public d:I

.field private e:LX/HiE;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 2497136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497137
    const/16 v0, 0x10

    iput v0, p0, LX/HiG;->c:I

    .line 2497138
    const/16 v0, 0xc0

    iput v0, p0, LX/HiG;->d:I

    .line 2497139
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2497140
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bitmap is not valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497141
    :cond_1
    iput-object p1, p0, LX/HiG;->b:Landroid/graphics/Bitmap;

    .line 2497142
    return-void
.end method


# virtual methods
.method public final a()LX/HiI;
    .locals 12

    .prologue
    .line 2497143
    iget-object v0, p0, LX/HiG;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 2497144
    iget v0, p0, LX/HiG;->d:I

    if-gtz v0, :cond_0

    .line 2497145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Minimum dimension size for resizing should should be >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497146
    :cond_0
    iget-object v0, p0, LX/HiG;->b:Landroid/graphics/Bitmap;

    iget v1, p0, LX/HiG;->d:I

    .line 2497147
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2497148
    if-gt v2, v1, :cond_4

    .line 2497149
    :goto_0
    move-object v0, v0

    .line 2497150
    iget v1, p0, LX/HiG;->c:I

    const/4 v6, 0x0

    .line 2497151
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 2497152
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    .line 2497153
    mul-int v4, v7, v11

    new-array v5, v4, [I

    move-object v4, v0

    move v8, v6

    move v9, v6

    move v10, v7

    .line 2497154
    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 2497155
    new-instance v4, LX/HiC;

    new-instance v6, LX/HiD;

    invoke-direct {v6, v5}, LX/HiD;-><init>([I)V

    invoke-direct {v4, v6, v1}, LX/HiC;-><init>(LX/HiD;I)V

    move-object v1, v4

    .line 2497156
    iget-object v2, p0, LX/HiG;->b:Landroid/graphics/Bitmap;

    if-eq v0, v2, :cond_1

    .line 2497157
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2497158
    :cond_1
    iget-object v0, v1, LX/HiC;->e:Ljava/util/List;

    move-object v0, v0

    .line 2497159
    :goto_1
    iget-object v1, p0, LX/HiG;->e:LX/HiE;

    if-nez v1, :cond_2

    .line 2497160
    new-instance v1, LX/HiF;

    invoke-direct {v1}, LX/HiF;-><init>()V

    iput-object v1, p0, LX/HiG;->e:LX/HiE;

    .line 2497161
    :cond_2
    iget-object v1, p0, LX/HiG;->e:LX/HiE;

    invoke-virtual {v1, v0}, LX/HiE;->a(Ljava/util/List;)V

    .line 2497162
    new-instance v1, LX/HiI;

    iget-object v2, p0, LX/HiG;->e:LX/HiE;

    invoke-direct {v1, v0, v2}, LX/HiI;-><init>(Ljava/util/List;LX/HiE;)V

    .line 2497163
    return-object v1

    .line 2497164
    :cond_3
    iget-object v0, p0, LX/HiG;->a:Ljava/util/List;

    goto :goto_1

    .line 2497165
    :cond_4
    int-to-float v3, v1

    int-to-float v2, v2

    div-float v2, v3, v2

    .line 2497166
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/4 v4, 0x0

    invoke-static {v0, v3, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
