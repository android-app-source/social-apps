.class public final LX/JIb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2678064
    iput-object p1, p0, LX/JIb;->b:LX/JIe;

    iput-object p2, p0, LX/JIb;->a:Landroid/view/View;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678065
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678066
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678067
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    invoke-virtual {v0}, LX/JJF;->f()V

    .line 2678068
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678069
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678070
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    invoke-virtual {v0}, LX/JJF;->h()V

    .line 2678071
    iget-object v0, p0, LX/JIb;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/JIb;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a6d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678072
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2678073
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2678074
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678075
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678076
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678077
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    .line 2678078
    iget-object v1, v0, LX/JJF;->j:LX/JJE;

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/JJE;->setEnabled(Z)V

    .line 2678079
    iget-object v1, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    iget-object p1, v0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {p1}, Lcom/facebook/widget/FlowLayout;->getChildCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v1, p1}, Lcom/facebook/widget/FlowLayout;->removeViewAt(I)V

    .line 2678080
    iget-object v1, v0, LX/JJF;->l:Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2678081
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678082
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678083
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    invoke-virtual {v0}, LX/JJF;->f()V

    .line 2678084
    :goto_0
    return-void

    .line 2678085
    :cond_0
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678086
    iget-object v1, v0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v1}, LX/FT2;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)LX/FT2;

    move-result-object v1

    new-instance v2, LX/FTB;

    invoke-direct {v2}, LX/FTB;-><init>()V

    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    iget-object v4, v0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2678087
    iput-object v3, v2, LX/FTB;->a:LX/0Px;

    .line 2678088
    move-object v2, v2

    .line 2678089
    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    .line 2678090
    iput-object v3, v2, LX/FTB;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2678091
    move-object v2, v2

    .line 2678092
    invoke-virtual {v2}, LX/FTB;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v2

    .line 2678093
    iput-object v2, v1, LX/FT2;->e:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    .line 2678094
    move-object v1, v1

    .line 2678095
    invoke-virtual {v1}, LX/FT2;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-result-object v1

    iput-object v1, v0, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678096
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678097
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678098
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    invoke-virtual {v0}, LX/JJF;->f()V

    .line 2678099
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/JIe;->a$redex0(LX/JIe;Ljava/util/List;)V

    .line 2678100
    iget-object v0, p0, LX/JIb;->b:LX/JIe;

    .line 2678101
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678102
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    invoke-virtual {v0}, LX/JJF;->h()V

    goto :goto_0
.end method
