.class public final enum LX/HPl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HPl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HPl;

.field public static final enum AFTER_CACHE_HIT:LX/HPl;

.field public static final enum FORCED_BY_USER:LX/HPl;

.field public static final enum INSTEAD_OF_CACHE_HIT:LX/HPl;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2462401
    new-instance v0, LX/HPl;

    const-string v1, "AFTER_CACHE_HIT"

    invoke-direct {v0, v1, v2}, LX/HPl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPl;->AFTER_CACHE_HIT:LX/HPl;

    .line 2462402
    new-instance v0, LX/HPl;

    const-string v1, "INSTEAD_OF_CACHE_HIT"

    invoke-direct {v0, v1, v3}, LX/HPl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPl;->INSTEAD_OF_CACHE_HIT:LX/HPl;

    .line 2462403
    new-instance v0, LX/HPl;

    const-string v1, "FORCED_BY_USER"

    invoke-direct {v0, v1, v4}, LX/HPl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HPl;->FORCED_BY_USER:LX/HPl;

    .line 2462404
    const/4 v0, 0x3

    new-array v0, v0, [LX/HPl;

    sget-object v1, LX/HPl;->AFTER_CACHE_HIT:LX/HPl;

    aput-object v1, v0, v2

    sget-object v1, LX/HPl;->INSTEAD_OF_CACHE_HIT:LX/HPl;

    aput-object v1, v0, v3

    sget-object v1, LX/HPl;->FORCED_BY_USER:LX/HPl;

    aput-object v1, v0, v4

    sput-object v0, LX/HPl;->$VALUES:[LX/HPl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2462398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HPl;
    .locals 1

    .prologue
    .line 2462400
    const-class v0, LX/HPl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HPl;

    return-object v0
.end method

.method public static values()[LX/HPl;
    .locals 1

    .prologue
    .line 2462399
    sget-object v0, LX/HPl;->$VALUES:[LX/HPl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HPl;

    return-object v0
.end method
