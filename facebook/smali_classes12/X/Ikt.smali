.class public final LX/Ikt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Il0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ikv;


# direct methods
.method public constructor <init>(LX/Ikv;)V
    .locals 0

    .prologue
    .line 2607182
    iput-object p1, p0, LX/Ikt;->a:LX/Ikv;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2607183
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    invoke-static {v0}, LX/Ikv;->c(LX/Ikv;)V

    .line 2607184
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2607185
    check-cast p1, LX/Il0;

    .line 2607186
    if-nez p1, :cond_1

    .line 2607187
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    invoke-static {v0}, LX/Ikv;->c(LX/Ikv;)V

    .line 2607188
    :cond_0
    :goto_0
    return-void

    .line 2607189
    :cond_1
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    iget-object v0, v0, LX/Ikv;->b:LX/Ikn;

    .line 2607190
    iput-object p1, v0, LX/Ikn;->d:LX/Il0;

    .line 2607191
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2607192
    iget-object v2, p1, LX/Il0;->b:LX/0am;

    move-object v2, v2

    .line 2607193
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2607194
    sget-object v2, LX/Il1;->PRODUCT_ITEM:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607195
    :cond_2
    iget-object v2, p1, LX/Il0;->k:LX/0am;

    move-object v2, v2

    .line 2607196
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2607197
    sget-object v2, LX/Il1;->QUANTITY:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607198
    :cond_3
    iget-object v2, p1, LX/Il0;->c:LX/0am;

    move-object v2, v2

    .line 2607199
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2607200
    sget-object v2, LX/Il1;->PRICE_BREAKDOWN:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607201
    :cond_4
    iget-object v2, p1, LX/Il0;->d:LX/0am;

    move-object v2, v2

    .line 2607202
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2607203
    sget-object v2, LX/Il1;->SHIPPING_ADDRESS:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607204
    :cond_5
    iget-object v2, p1, LX/Il0;->e:LX/0am;

    move-object v2, v2

    .line 2607205
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2607206
    sget-object v2, LX/Il1;->SHIPPING_METHOD:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607207
    :cond_6
    iget-object v2, p1, LX/Il0;->q:LX/0am;

    move-object v2, v2

    .line 2607208
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2607209
    sget-object v2, LX/Il1;->PAYMENT_STATUS_WITH_ATTACHMENT:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607210
    :cond_7
    iget-object v2, p1, LX/Il0;->f:LX/0am;

    move-object v2, v2

    .line 2607211
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2607212
    sget-object v2, LX/Il1;->SHIPPING_FULFILLMENT:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607213
    :cond_8
    iget-boolean v2, p1, LX/Il0;->s:Z

    move v2, v2

    .line 2607214
    if-eqz v2, :cond_9

    .line 2607215
    sget-object v2, LX/Il1;->ACTION_BUTTON:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607216
    :cond_9
    iget-object v2, p1, LX/Il0;->a:LX/0am;

    move-object v2, v2

    .line 2607217
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2607218
    sget-object v2, LX/Il1;->ORDER_ID:LX/Il1;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2607219
    :cond_a
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2607220
    iput-object v1, v0, LX/Ikn;->e:LX/0Px;

    .line 2607221
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2607222
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    iget-object v0, v0, LX/Ikv;->e:LX/Ikx;

    invoke-virtual {v0}, LX/Ikx;->b()V

    .line 2607223
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    iget-object v0, v0, LX/Ikv;->g:LX/IZ9;

    if-eqz v0, :cond_0

    .line 2607224
    iget-object v0, p0, LX/Ikt;->a:LX/Ikv;

    iget-object v0, v0, LX/Ikv;->g:LX/IZ9;

    invoke-interface {v0}, LX/IZ9;->a()V

    goto/16 :goto_0
.end method
