.class public LX/I26;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HzD;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/Hyx;

.field public final c:LX/1Ck;

.field private d:LX/I2G;

.field private e:LX/I2C;

.field private f:LX/1nQ;

.field public g:LX/I2I;

.field private h:Lcom/facebook/events/common/EventAnalyticsParams;

.field public i:LX/HzH;

.field public j:LX/HzI;

.field private k:Z

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public n:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final o:LX/Hyh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Hyx;LX/I2G;LX/I2C;LX/1Ck;LX/1nQ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2529811
    new-instance v0, LX/I22;

    invoke-direct {v0, p0}, LX/I22;-><init>(LX/I26;)V

    iput-object v0, p0, LX/I26;->o:LX/Hyh;

    .line 2529812
    iput-object p1, p0, LX/I26;->a:Landroid/content/Context;

    .line 2529813
    iput-object p6, p0, LX/I26;->f:LX/1nQ;

    .line 2529814
    iput-object p2, p0, LX/I26;->b:LX/Hyx;

    .line 2529815
    iput-object p3, p0, LX/I26;->d:LX/I2G;

    .line 2529816
    iput-object p4, p0, LX/I26;->e:LX/I2C;

    .line 2529817
    iput-object p5, p0, LX/I26;->c:LX/1Ck;

    .line 2529818
    return-void
.end method

.method public static a$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529819
    if-nez p1, :cond_1

    .line 2529820
    :cond_0
    :goto_0
    return-void

    .line 2529821
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529822
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2529823
    if-eqz v0, :cond_0

    .line 2529824
    iget-object v1, p0, LX/I26;->b:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2529825
    iget-object v2, p0, LX/I26;->g:LX/I2I;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    sget-object v3, LX/I2H;->PUBLISHED:LX/I2H;

    invoke-virtual {v2, v1, v0, v3}, LX/I2I;->a(Ljava/util/List;ZLX/I2H;)V

    goto :goto_0
.end method

.method public static b$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529826
    if-nez p1, :cond_1

    .line 2529827
    :cond_0
    :goto_0
    return-void

    .line 2529828
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529829
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2529830
    if-eqz v0, :cond_0

    .line 2529831
    iget-object v1, p0, LX/I26;->b:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2529832
    iget-object v2, p0, LX/I26;->g:LX/I2I;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    sget-object v3, LX/I2H;->DRAFT:LX/I2H;

    invoke-virtual {v2, v1, v0, v3}, LX/I2I;->a(Ljava/util/List;ZLX/I2H;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529839
    if-nez p1, :cond_1

    .line 2529840
    :cond_0
    :goto_0
    return-void

    .line 2529841
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529842
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;

    move-result-object v0

    .line 2529843
    if-eqz v0, :cond_0

    .line 2529844
    iget-object v1, p0, LX/I26;->b:LX/Hyx;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2529845
    iget-object v2, p0, LX/I26;->g:LX/I2I;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPastEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v0

    sget-object v3, LX/I2H;->PAST:LX/I2H;

    invoke-virtual {v2, v1, v0, v3}, LX/I2I;->a(Ljava/util/List;ZLX/I2H;)V

    goto :goto_0
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v3, 0x6

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 2529833
    iget-object v0, p0, LX/I26;->b:LX/Hyx;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    iget-object v4, p0, LX/I26;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0f74

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "PUBLISHED"

    aput-object v6, v5, v2

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "HOST"

    aput-object v7, v6, v2

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2529834
    iget-object v0, p0, LX/I26;->b:LX/Hyx;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    iget-object v4, p0, LX/I26;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0f74

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "DRAFT"

    aput-object v6, v5, v2

    const-string v6, "SCHEDULED_DRAFT_FOR_PUBLICATION"

    aput-object v6, v5, v8

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "HOST"

    aput-object v7, v6, v2

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Hyx;->a(LX/Hx6;ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2529835
    iget-object v1, p0, LX/I26;->b:LX/Hyx;

    iget-object v0, p0, LX/I26;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0f74

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    new-array v0, v8, [Ljava/lang/String;

    const-string v5, "PUBLISHED"

    aput-object v5, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-array v0, v8, [Ljava/lang/String;

    const-string v6, "HOST"

    aput-object v6, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/Hyx;->a(ZIILjava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2529836
    iget-object v0, p0, LX/I26;->b:LX/Hyx;

    invoke-virtual {v0}, LX/Hyx;->a()V

    .line 2529837
    iget-object v0, p0, LX/I26;->b:LX/Hyx;

    iget-object v1, p0, LX/I26;->o:LX/Hyh;

    invoke-virtual {v0, v2, v1, v3}, LX/Hyx;->a(ZLX/Hyh;I)V

    .line 2529838
    return-void
.end method


# virtual methods
.method public final a()LX/1OM;
    .locals 1

    .prologue
    .line 2529846
    iget-object v0, p0, LX/I26;->g:LX/I2I;

    return-object v0
.end method

.method public final a(LX/HzH;)V
    .locals 0

    .prologue
    .line 2529806
    iput-object p1, p0, LX/I26;->i:LX/HzH;

    .line 2529807
    return-void
.end method

.method public final a(LX/HzI;)V
    .locals 0

    .prologue
    .line 2529808
    iput-object p1, p0, LX/I26;->j:LX/HzI;

    .line 2529809
    return-void
.end method

.method public final a(Lcom/facebook/events/common/EventAnalyticsParams;LX/HzV;)V
    .locals 11

    .prologue
    .line 2529799
    iput-object p1, p0, LX/I26;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529800
    new-instance v0, LX/I2I;

    iget-object v1, p0, LX/I26;->d:LX/I2G;

    sget-object v2, LX/I2H;->PUBLISHED:LX/I2H;

    invoke-virtual {v1, p1, v2}, LX/I2G;->a(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;)LX/I2F;

    move-result-object v1

    iget-object v2, p0, LX/I26;->d:LX/I2G;

    sget-object v3, LX/I2H;->DRAFT:LX/I2H;

    invoke-virtual {v2, p1, v3}, LX/I2G;->a(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;)LX/I2F;

    move-result-object v2

    iget-object v3, p0, LX/I26;->d:LX/I2G;

    sget-object v4, LX/I2H;->PAST:LX/I2H;

    invoke-virtual {v3, p1, v4}, LX/I2G;->a(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;)LX/I2F;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/I26;->e:LX/I2C;

    .line 2529801
    new-instance v5, LX/I2B;

    const-class v6, Landroid/content/Context;

    invoke-interface {v2, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v2}, LX/I21;->b(LX/0QB;)LX/I21;

    move-result-object v8

    check-cast v8, LX/I21;

    const/16 v6, 0xc

    invoke-static {v2, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    move-object v6, p1

    invoke-direct/range {v5 .. v10}, LX/I2B;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I21;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 2529802
    move-object v2, v5

    .line 2529803
    invoke-direct {v0, v1, v2}, LX/I2I;-><init>(LX/0Px;LX/I2B;)V

    iput-object v0, p0, LX/I26;->g:LX/I2I;

    .line 2529804
    invoke-direct {p0}, LX/I26;->e()V

    .line 2529805
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 2

    .prologue
    .line 2529785
    if-eqz p1, :cond_0

    .line 2529786
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v0

    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2529787
    iget-object v1, p0, LX/I26;->g:LX/I2I;

    .line 2529788
    iget-boolean p0, v0, Lcom/facebook/events/model/Event;->z:Z

    move p0, p0

    .line 2529789
    if-eqz p0, :cond_1

    .line 2529790
    iget-object p0, v1, LX/I2I;->c:Ljava/util/HashMap;

    sget-object p1, LX/I2H;->DRAFT:LX/I2H;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2529791
    iget-object p0, v1, LX/I2I;->c:Ljava/util/HashMap;

    sget-object p1, LX/I2H;->DRAFT:LX/I2H;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 2529792
    iget-object p1, v1, LX/I2I;->a:LX/0Px;

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/I2F;

    .line 2529793
    invoke-virtual {p0, v0}, LX/I2F;->a(Lcom/facebook/events/model/Event;)V

    .line 2529794
    :cond_0
    :goto_0
    return-void

    .line 2529795
    :cond_1
    iget-object p0, v1, LX/I2I;->c:Ljava/util/HashMap;

    sget-object p1, LX/I2H;->PUBLISHED:LX/I2H;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2529796
    iget-object p0, v1, LX/I2I;->c:Ljava/util/HashMap;

    sget-object p1, LX/I2H;->PUBLISHED:LX/I2H;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 2529797
    iget-object p1, v1, LX/I2I;->a:LX/0Px;

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/I2F;

    .line 2529798
    invoke-virtual {p0, v0}, LX/I2F;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2529772
    iget-object v0, p0, LX/I26;->g:LX/I2I;

    .line 2529773
    iget-object v1, v0, LX/I2I;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v1, v0, LX/I2I;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/I2F;

    .line 2529774
    iget-object v4, v1, LX/I2F;->j:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2529775
    iget-object v4, v1, LX/I2F;->j:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2529776
    iget-object p0, v1, LX/I2F;->i:Ljava/util/List;

    invoke-interface {p0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2529777
    iget-boolean v4, v1, LX/I2F;->k:Z

    if-eqz v4, :cond_0

    .line 2529778
    iget-boolean v4, v1, LX/I2F;->k:Z

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    iput-boolean v4, v1, LX/I2F;->k:Z

    .line 2529779
    :cond_0
    invoke-static {v1}, LX/I2F;->g(LX/I2F;)V

    .line 2529780
    invoke-static {v1}, LX/I2F;->e(LX/I2F;)V

    .line 2529781
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2529782
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2529783
    :cond_2
    return-void

    .line 2529784
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(ZII)V
    .locals 4

    .prologue
    .line 2529768
    iget-boolean v0, p0, LX/I26;->k:Z

    if-nez v0, :cond_0

    .line 2529769
    iget-object v0, p0, LX/I26;->f:LX/1nQ;

    iget-object v1, p0, LX/I26;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I26;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB_SCROLL_VIEW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, LX/1nQ;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2529770
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I26;->k:Z

    .line 2529771
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2529740
    const/4 v4, 0x0

    .line 2529741
    iget-object v0, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_3

    .line 2529742
    :cond_0
    :goto_0
    const/4 v4, 0x0

    .line 2529743
    iget-object v0, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_5

    .line 2529744
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 2529745
    iget-object v0, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_7

    .line 2529746
    :cond_2
    :goto_2
    return-void

    .line 2529747
    :cond_3
    iget-object v0, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2529748
    iget-object v0, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529749
    invoke-static {p0, v0}, LX/I26;->a$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529750
    iput-object v4, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2529751
    :cond_4
    iget-object v0, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2529752
    iget-object v0, p0, LX/I26;->c:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_HOSTING_EVENTS:LX/Hy9;

    iget-object v2, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I23;

    invoke-direct {v3, p0}, LX/I23;-><init>(LX/I26;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2529753
    iput-object v4, p0, LX/I26;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2529754
    :cond_5
    iget-object v0, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2529755
    iget-object v0, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529756
    invoke-static {p0, v0}, LX/I26;->b$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529757
    iput-object v4, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2529758
    :cond_6
    iget-object v0, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2529759
    iget-object v0, p0, LX/I26;->c:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_DRAFT_EVENTS:LX/Hy9;

    iget-object v2, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I24;

    invoke-direct {v3, p0}, LX/I24;-><init>(LX/I26;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2529760
    iput-object v4, p0, LX/I26;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2529761
    :cond_7
    iget-object v0, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2529762
    iget-object v0, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529763
    invoke-static {p0, v0}, LX/I26;->c$redex0(LX/I26;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2529764
    iput-object v4, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2

    .line 2529765
    :cond_8
    iget-object v0, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2529766
    iget-object v0, p0, LX/I26;->c:LX/1Ck;

    sget-object v1, LX/Hy9;->FETCH_PAST_EVENTS:LX/Hy9;

    iget-object v2, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/I25;

    invoke-direct {v3, p0}, LX/I25;-><init>(LX/I26;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2529767
    iput-object v4, p0, LX/I26;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_2
.end method

.method public final b(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V
    .locals 5

    .prologue
    .line 2529726
    if-eqz p1, :cond_1

    .line 2529727
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v0

    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2529728
    iget-object v1, p0, LX/I26;->g:LX/I2I;

    .line 2529729
    iget-object v2, v1, LX/I2I;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v2, v1, LX/I2I;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/I2F;

    .line 2529730
    iget-object p0, v2, LX/I2F;->j:Ljava/util/HashMap;

    .line 2529731
    iget-object p1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2529732
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2529733
    iget-object p0, v2, LX/I2F;->j:Ljava/util/HashMap;

    .line 2529734
    iget-object p1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2529735
    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 2529736
    iget-object p1, v2, LX/I2F;->i:Ljava/util/List;

    invoke-interface {p1, p0, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2529737
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2529738
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2529739
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2529722
    iget-object v0, p0, LX/I26;->c:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2529723
    iget-object v0, p0, LX/I26;->b:LX/Hyx;

    .line 2529724
    iget-object p0, v0, LX/Hyx;->q:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 2529725
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2529719
    invoke-direct {p0}, LX/I26;->e()V

    .line 2529720
    invoke-virtual {p0}, LX/I26;->b()V

    .line 2529721
    return-void
.end method
