.class public final LX/Hlw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:D

.field public final b:D

.field public final c:F

.field public final d:J


# direct methods
.method public constructor <init>(DDFJ)V
    .locals 1

    .prologue
    .line 2499593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499594
    iput-wide p1, p0, LX/Hlw;->a:D

    .line 2499595
    iput-wide p3, p0, LX/Hlw;->b:D

    .line 2499596
    iput p5, p0, LX/Hlw;->c:F

    .line 2499597
    iput-wide p6, p0, LX/Hlw;->d:J

    .line 2499598
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2499599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2499600
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2499601
    const-string v1, "latitude_degrees"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, LX/Hlw;->a:D

    .line 2499602
    const-string v1, "longitude_degrees"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, LX/Hlw;->b:D

    .line 2499603
    const-string v1, "speed_meters_per_second"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, LX/Hlw;->c:F

    .line 2499604
    const-string v1, "timestamp_ms_since_boot"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Hlw;->d:J

    .line 2499605
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2499606
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2499607
    const-string v1, "latitude_degrees"

    iget-wide v2, p0, LX/Hlw;->a:D

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2499608
    const-string v1, "longitude_degrees"

    iget-wide v2, p0, LX/Hlw;->b:D

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2499609
    const-string v1, "speed_meters_per_second"

    iget v2, p0, LX/Hlw;->c:F

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 2499610
    const-string v1, "timestamp_ms_since_boot"

    iget-wide v2, p0, LX/Hlw;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2499611
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
