.class public LX/HyO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/HyM;

.field private static volatile d:LX/HyO;


# instance fields
.field private final b:LX/11i;

.field private c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2524160
    new-instance v0, LX/HyM;

    invoke-direct {v0}, LX/HyM;-><init>()V

    sput-object v0, LX/HyO;->a:LX/HyM;

    return-void
.end method

.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2524157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2524158
    iput-object p1, p0, LX/HyO;->b:LX/11i;

    .line 2524159
    return-void
.end method

.method public static a(LX/0QB;)LX/HyO;
    .locals 4

    .prologue
    .line 2524144
    sget-object v0, LX/HyO;->d:LX/HyO;

    if-nez v0, :cond_1

    .line 2524145
    const-class v1, LX/HyO;

    monitor-enter v1

    .line 2524146
    :try_start_0
    sget-object v0, LX/HyO;->d:LX/HyO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2524147
    if-eqz v2, :cond_0

    .line 2524148
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2524149
    new-instance p0, LX/HyO;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/HyO;-><init>(LX/11i;)V

    .line 2524150
    move-object v0, p0

    .line 2524151
    sput-object v0, LX/HyO;->d:LX/HyO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2524152
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2524153
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2524154
    :cond_1
    sget-object v0, LX/HyO;->d:LX/HyO;

    return-object v0

    .line 2524155
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2524156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/HyM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2524143
    iget-object v0, p0, LX/HyO;->b:LX/11i;

    sget-object v1, LX/HyO;->a:LX/HyM;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2524140
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HyO;->c:Z

    .line 2524141
    iget-object v0, p0, LX/HyO;->b:LX/11i;

    sget-object v1, LX/HyO;->a:LX/HyM;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2524142
    return-void
.end method

.method public final a(LX/HyN;)V
    .locals 3

    .prologue
    .line 2524127
    iget-boolean v0, p0, LX/HyO;->c:Z

    if-nez v0, :cond_1

    .line 2524128
    :cond_0
    :goto_0
    return-void

    .line 2524129
    :cond_1
    invoke-direct {p0}, LX/HyO;->c()LX/11o;

    move-result-object v0

    .line 2524130
    if-eqz v0, :cond_0

    .line 2524131
    invoke-virtual {p1}, LX/HyN;->name()Ljava/lang/String;

    move-result-object v1

    const v2, 0x31f0f208

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2524137
    iget-boolean v0, p0, LX/HyO;->c:Z

    if-nez v0, :cond_0

    .line 2524138
    :goto_0
    return-void

    .line 2524139
    :cond_0
    iget-object v0, p0, LX/HyO;->b:LX/11i;

    sget-object v1, LX/HyO;->a:LX/HyM;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    goto :goto_0
.end method

.method public final b(LX/HyN;)V
    .locals 3

    .prologue
    .line 2524132
    iget-boolean v0, p0, LX/HyO;->c:Z

    if-nez v0, :cond_1

    .line 2524133
    :cond_0
    :goto_0
    return-void

    .line 2524134
    :cond_1
    invoke-direct {p0}, LX/HyO;->c()LX/11o;

    move-result-object v0

    .line 2524135
    if-eqz v0, :cond_0

    .line 2524136
    invoke-virtual {p1}, LX/HyN;->name()Ljava/lang/String;

    move-result-object v1

    const v2, 0x1fa46900

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method
