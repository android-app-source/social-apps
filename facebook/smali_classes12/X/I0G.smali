.class public final enum LX/I0G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I0G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I0G;

.field public static final enum BIRTHDAY:LX/I0G;

.field public static final enum HEADER:LX/I0G;

.field public static final enum LOADING:LX/I0G;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2526654
    new-instance v0, LX/I0G;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/I0G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0G;->HEADER:LX/I0G;

    .line 2526655
    new-instance v0, LX/I0G;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v3}, LX/I0G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0G;->BIRTHDAY:LX/I0G;

    .line 2526656
    new-instance v0, LX/I0G;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/I0G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0G;->LOADING:LX/I0G;

    .line 2526657
    const/4 v0, 0x3

    new-array v0, v0, [LX/I0G;

    sget-object v1, LX/I0G;->HEADER:LX/I0G;

    aput-object v1, v0, v2

    sget-object v1, LX/I0G;->BIRTHDAY:LX/I0G;

    aput-object v1, v0, v3

    sget-object v1, LX/I0G;->LOADING:LX/I0G;

    aput-object v1, v0, v4

    sput-object v0, LX/I0G;->$VALUES:[LX/I0G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2526658
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I0G;
    .locals 1

    .prologue
    .line 2526659
    const-class v0, LX/I0G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I0G;

    return-object v0
.end method

.method public static values()[LX/I0G;
    .locals 1

    .prologue
    .line 2526660
    sget-object v0, LX/I0G;->$VALUES:[LX/I0G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I0G;

    return-object v0
.end method
