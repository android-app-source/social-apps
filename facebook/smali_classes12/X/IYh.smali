.class public final enum LX/IYh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IYh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IYh;

.field public static final enum LOOK_NOW_PERMALINK_LOAD_FAILURE:LX/IYh;

.field public static final enum LOOK_NOW_PERMALINK_LOAD_SUCCESS:LX/IYh;

.field public static final enum LOOK_NOW_PERMALINK_OPENED:LX/IYh;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2588025
    new-instance v0, LX/IYh;

    const-string v1, "LOOK_NOW_PERMALINK_OPENED"

    const-string v2, "look_now_permalink_opened"

    invoke-direct {v0, v1, v3, v2}, LX/IYh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IYh;->LOOK_NOW_PERMALINK_OPENED:LX/IYh;

    .line 2588026
    new-instance v0, LX/IYh;

    const-string v1, "LOOK_NOW_PERMALINK_LOAD_SUCCESS"

    const-string v2, "look_now_permalink_load_success"

    invoke-direct {v0, v1, v4, v2}, LX/IYh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_SUCCESS:LX/IYh;

    .line 2588027
    new-instance v0, LX/IYh;

    const-string v1, "LOOK_NOW_PERMALINK_LOAD_FAILURE"

    const-string v2, "look_now_permalink_load_failure"

    invoke-direct {v0, v1, v5, v2}, LX/IYh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_FAILURE:LX/IYh;

    .line 2588028
    const/4 v0, 0x3

    new-array v0, v0, [LX/IYh;

    sget-object v1, LX/IYh;->LOOK_NOW_PERMALINK_OPENED:LX/IYh;

    aput-object v1, v0, v3

    sget-object v1, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_SUCCESS:LX/IYh;

    aput-object v1, v0, v4

    sget-object v1, LX/IYh;->LOOK_NOW_PERMALINK_LOAD_FAILURE:LX/IYh;

    aput-object v1, v0, v5

    sput-object v0, LX/IYh;->$VALUES:[LX/IYh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2588022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2588023
    iput-object p3, p0, LX/IYh;->name:Ljava/lang/String;

    .line 2588024
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IYh;
    .locals 1

    .prologue
    .line 2588020
    const-class v0, LX/IYh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IYh;

    return-object v0
.end method

.method public static values()[LX/IYh;
    .locals 1

    .prologue
    .line 2588021
    sget-object v0, LX/IYh;->$VALUES:[LX/IYh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IYh;

    return-object v0
.end method
