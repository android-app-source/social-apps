.class public final LX/JES;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JET;


# direct methods
.method public constructor <init>(LX/JET;)V
    .locals 0

    .prologue
    .line 2666015
    iput-object p1, p0, LX/JES;->a:LX/JET;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const v3, 0x50ebd2fb

    invoke-static {v2, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2666016
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-boolean v3, v3, LX/JET;->h:Z

    if-eqz v3, :cond_0

    .line 2666017
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-object v3, v3, LX/JET;->b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    invoke-virtual {v3, v1}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->setVisibility(I)V

    .line 2666018
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-object v3, v3, LX/JET;->c:Lcom/facebook/video/insight/view/VideoInsightInfoView;

    invoke-virtual {v3, v4}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->setVisibility(I)V

    .line 2666019
    :goto_0
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-object v4, p0, LX/JES;->a:LX/JET;

    iget-boolean v4, v4, LX/JET;->h:Z

    if-nez v4, :cond_1

    .line 2666020
    :goto_1
    iput-boolean v0, v3, LX/JET;->h:Z

    .line 2666021
    const v0, 0x1d48784b

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2666022
    :cond_0
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-object v3, v3, LX/JET;->b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    invoke-virtual {v3, v4}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->setVisibility(I)V

    .line 2666023
    iget-object v3, p0, LX/JES;->a:LX/JET;

    iget-object v3, v3, LX/JET;->c:Lcom/facebook/video/insight/view/VideoInsightInfoView;

    invoke-virtual {v3, v1}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2666024
    goto :goto_1
.end method
