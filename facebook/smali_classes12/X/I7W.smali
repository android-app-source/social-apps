.class public LX/I7W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private a:LX/0SG;

.field private b:J

.field private c:J

.field private d:J

.field private e:J


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2539738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2539739
    iput-object p1, p0, LX/I7W;->a:LX/0SG;

    .line 2539740
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/I7W;->e:J

    .line 2539741
    return-void
.end method

.method public static a(LX/0QB;)LX/I7W;
    .locals 4

    .prologue
    .line 2539727
    const-class v1, LX/I7W;

    monitor-enter v1

    .line 2539728
    :try_start_0
    sget-object v0, LX/I7W;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2539729
    sput-object v2, LX/I7W;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2539730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2539731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2539732
    new-instance p0, LX/I7W;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/I7W;-><init>(LX/0SG;)V

    .line 2539733
    move-object v0, p0

    .line 2539734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2539735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/I7W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2539736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2539737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/I7W;Lcom/facebook/events/model/Event;)I
    .locals 6

    .prologue
    .line 2539742
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    .line 2539743
    iget-wide v2, p0, LX/I7W;->b:J

    sub-long v2, v0, v2

    .line 2539744
    iget-wide v4, p0, LX/I7W;->e:J

    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    .line 2539745
    const/4 v0, 0x0

    .line 2539746
    :goto_0
    return v0

    .line 2539747
    :cond_0
    iget-wide v4, p0, LX/I7W;->d:J

    add-long/2addr v0, v4

    invoke-virtual {p1, v0, v1}, Lcom/facebook/events/model/Event;->a(J)J

    move-result-wide v0

    .line 2539748
    iget-wide v4, p0, LX/I7W;->c:J

    add-long/2addr v0, v4

    .line 2539749
    iget-wide v4, p0, LX/I7W;->e:J

    cmp-long v4, v4, v0

    if-gtz v4, :cond_1

    cmp-long v0, v2, v0

    if-nez v0, :cond_2

    .line 2539750
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 2539751
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(JJJ)V
    .locals 3

    .prologue
    .line 2539722
    iput-wide p1, p0, LX/I7W;->b:J

    .line 2539723
    iput-wide p3, p0, LX/I7W;->c:J

    .line 2539724
    iput-wide p5, p0, LX/I7W;->d:J

    .line 2539725
    iget-object v0, p0, LX/I7W;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/I7W;->e:J

    .line 2539726
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;)Z
    .locals 2

    .prologue
    .line 2539721
    invoke-static {p0, p1}, LX/I7W;->c(LX/I7W;Lcom/facebook/events/model/Event;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
