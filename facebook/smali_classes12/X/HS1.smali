.class public final enum LX/HS1;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HS1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HS1;

.field public static final enum DRAFTS:LX/HS1;

.field public static final enum FOLLOWERS:LX/HS1;

.field public static final enum MESSAGES:LX/HS1;

.field public static final enum NEW_LIKES:LX/HS1;

.field public static final enum NOTIFICATIONS:LX/HS1;

.field public static final enum PAGES_FEED:LX/HS1;

.field public static final enum PAGE_TIPS:LX/HS1;

.field public static final enum RECENT_ACTIVITY:LX/HS1;

.field public static final enum RECENT_CHECK_INS:LX/HS1;

.field public static final enum RECENT_MENTIONS:LX/HS1;

.field public static final enum RECENT_REVIEWS:LX/HS1;

.field public static final enum RECENT_SHARES:LX/HS1;

.field public static final enum SCHEDULED_POSTS:LX/HS1;


# instance fields
.field public final loggingEvent:LX/9X5;

.field public final resId:I

.field public final uri:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2466285
    new-instance v0, LX/HS1;

    const-string v1, "MESSAGES"

    const v3, 0x7f0d226d

    sget-object v4, LX/0ax;->bf:Ljava/lang/String;

    sget-object v5, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_MESSAGE:LX/9X5;

    invoke-direct/range {v0 .. v5}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v0, LX/HS1;->MESSAGES:LX/HS1;

    .line 2466286
    new-instance v3, LX/HS1;

    const-string v4, "NOTIFICATIONS"

    const v6, 0x7f0d226e    # 1.8759992E38f

    sget-object v7, LX/8Dq;->n:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NOTIF:LX/9X5;

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->NOTIFICATIONS:LX/HS1;

    .line 2466287
    new-instance v3, LX/HS1;

    const-string v4, "PAGES_FEED"

    const v6, 0x7f0d226f    # 1.8759994E38f

    sget-object v7, LX/8Dq;->o:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGES_FEED:LX/9X5;

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->PAGES_FEED:LX/HS1;

    .line 2466288
    new-instance v3, LX/HS1;

    const-string v4, "NEW_LIKES"

    const v6, 0x7f0d2271    # 1.8759998E38f

    const-string v7, "http://m.facebook.com/browse/fans/?recentFirst=1&id=%s"

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_NEW_LIKES:LX/9X5;

    move v5, v11

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->NEW_LIKES:LX/HS1;

    .line 2466289
    new-instance v3, LX/HS1;

    const-string v4, "SCHEDULED_POSTS"

    const v6, 0x7f0d2272    # 1.876E38f

    sget-object v7, LX/8Dq;->v:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_SCHEDULED_POST:LX/9X5;

    move v5, v12

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->SCHEDULED_POSTS:LX/HS1;

    .line 2466290
    new-instance v3, LX/HS1;

    const-string v4, "DRAFTS"

    const/4 v5, 0x5

    const v6, 0x7f0d2273    # 1.8760002E38f

    sget-object v7, LX/8Dq;->w:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_DRAFT_POST:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->DRAFTS:LX/HS1;

    .line 2466291
    new-instance v3, LX/HS1;

    const-string v4, "FOLLOWERS"

    const/4 v5, 0x6

    const v6, 0x7f0d2274    # 1.8760004E38f

    const-string v7, "https://m.facebook.com/browse/followers/?id=%s&recentFirst=1"

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_FOLLOWERS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->FOLLOWERS:LX/HS1;

    .line 2466292
    new-instance v3, LX/HS1;

    const-string v4, "RECENT_ACTIVITY"

    const/4 v5, 0x7

    const v6, 0x7f0d2276    # 1.8760008E38f

    sget-object v7, LX/8Dq;->p:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_ACTIVITY:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->RECENT_ACTIVITY:LX/HS1;

    .line 2466293
    new-instance v3, LX/HS1;

    const-string v4, "RECENT_MENTIONS"

    const/16 v5, 0x8

    const v6, 0x7f0d2277    # 1.876001E38f

    sget-object v7, LX/8Dq;->q:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->RECENT_MENTIONS:LX/HS1;

    .line 2466294
    new-instance v3, LX/HS1;

    const-string v4, "RECENT_SHARES"

    const/16 v5, 0x9

    const v6, 0x7f0d2278

    sget-object v7, LX/8Dq;->r:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_MENTIONS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->RECENT_SHARES:LX/HS1;

    .line 2466295
    new-instance v3, LX/HS1;

    const-string v4, "RECENT_REVIEWS"

    const/16 v5, 0xa

    const v6, 0x7f0d2279

    sget-object v7, LX/8Dq;->s:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_REVIEWS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->RECENT_REVIEWS:LX/HS1;

    .line 2466296
    new-instance v3, LX/HS1;

    const-string v4, "RECENT_CHECK_INS"

    const/16 v5, 0xb

    const v6, 0x7f0d227a

    sget-object v7, LX/8Dq;->t:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_RECENT_CHECK_INS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->RECENT_CHECK_INS:LX/HS1;

    .line 2466297
    new-instance v3, LX/HS1;

    const-string v4, "PAGE_TIPS"

    const/16 v5, 0xc

    const v6, 0x7f0d2270    # 1.8759996E38f

    sget-object v7, LX/8Dq;->L:Ljava/lang/String;

    sget-object v8, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_PAGE_TIPS:LX/9X5;

    invoke-direct/range {v3 .. v8}, LX/HS1;-><init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V

    sput-object v3, LX/HS1;->PAGE_TIPS:LX/HS1;

    .line 2466298
    const/16 v0, 0xd

    new-array v0, v0, [LX/HS1;

    sget-object v1, LX/HS1;->MESSAGES:LX/HS1;

    aput-object v1, v0, v2

    sget-object v1, LX/HS1;->NOTIFICATIONS:LX/HS1;

    aput-object v1, v0, v9

    sget-object v1, LX/HS1;->PAGES_FEED:LX/HS1;

    aput-object v1, v0, v10

    sget-object v1, LX/HS1;->NEW_LIKES:LX/HS1;

    aput-object v1, v0, v11

    sget-object v1, LX/HS1;->SCHEDULED_POSTS:LX/HS1;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LX/HS1;->DRAFTS:LX/HS1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HS1;->FOLLOWERS:LX/HS1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HS1;->RECENT_ACTIVITY:LX/HS1;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HS1;->RECENT_MENTIONS:LX/HS1;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HS1;->RECENT_SHARES:LX/HS1;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HS1;->RECENT_REVIEWS:LX/HS1;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HS1;->RECENT_CHECK_INS:LX/HS1;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HS1;->PAGE_TIPS:LX/HS1;

    aput-object v2, v0, v1

    sput-object v0, LX/HS1;->$VALUES:[LX/HS1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;LX/9X5;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "LX/9X5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2466299
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2466300
    iput p3, p0, LX/HS1;->resId:I

    .line 2466301
    iput-object p4, p0, LX/HS1;->uri:Ljava/lang/String;

    .line 2466302
    iput-object p5, p0, LX/HS1;->loggingEvent:LX/9X5;

    .line 2466303
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HS1;
    .locals 1

    .prologue
    .line 2466304
    const-class v0, LX/HS1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HS1;

    return-object v0
.end method

.method public static values()[LX/HS1;
    .locals 1

    .prologue
    .line 2466305
    sget-object v0, LX/HS1;->$VALUES:[LX/HS1;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HS1;

    return-object v0
.end method
