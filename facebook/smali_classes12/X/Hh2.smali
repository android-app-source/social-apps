.class public final LX/Hh2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hh0;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V
    .locals 0

    .prologue
    .line 2494792
    iput-object p1, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2494793
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->y:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494794
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->z:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2494795
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2494796
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget-object v1, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget v1, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->A:I

    if-lt v0, v1, :cond_0

    .line 2494797
    iget-object v1, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v1, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 2494798
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2494799
    :cond_0
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    iget-object v1, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a003d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2494800
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494801
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->z:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2494802
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2494803
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494804
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->z:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2494805
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    iget-object v1, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0947

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2494806
    iget-object v0, p0, LX/Hh2;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->t:Landroid/widget/TextView;

    const v1, 0x7f08374a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2494807
    return-void
.end method
