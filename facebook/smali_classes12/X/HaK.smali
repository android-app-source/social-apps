.class public LX/HaK;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/HaK;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483723
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2483724
    sget-object v0, LX/0ax;->z:Ljava/lang/String;

    const-class v1, Lcom/facebook/registration/activity/AccountRegistrationActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2483725
    return-void
.end method

.method public static a(LX/0QB;)LX/HaK;
    .locals 3

    .prologue
    .line 2483726
    sget-object v0, LX/HaK;->a:LX/HaK;

    if-nez v0, :cond_1

    .line 2483727
    const-class v1, LX/HaK;

    monitor-enter v1

    .line 2483728
    :try_start_0
    sget-object v0, LX/HaK;->a:LX/HaK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2483729
    if-eqz v2, :cond_0

    .line 2483730
    :try_start_1
    new-instance v0, LX/HaK;

    invoke-direct {v0}, LX/HaK;-><init>()V

    .line 2483731
    move-object v0, v0

    .line 2483732
    sput-object v0, LX/HaK;->a:LX/HaK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2483733
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2483734
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2483735
    :cond_1
    sget-object v0, LX/HaK;->a:LX/HaK;

    return-object v0

    .line 2483736
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2483737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
