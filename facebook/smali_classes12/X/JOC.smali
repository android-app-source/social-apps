.class public LX/JOC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687058
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2687059
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOC;->b:LX/0Zi;

    .line 2687060
    iput-object p1, p0, LX/JOC;->a:LX/0Ot;

    .line 2687061
    return-void
.end method

.method public static a(LX/0QB;)LX/JOC;
    .locals 4

    .prologue
    .line 2687062
    const-class v1, LX/JOC;

    monitor-enter v1

    .line 2687063
    :try_start_0
    sget-object v0, LX/JOC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687064
    sput-object v2, LX/JOC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687065
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687066
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687067
    new-instance v3, LX/JOC;

    const/16 p0, 0x1f49

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOC;-><init>(LX/0Ot;)V

    .line 2687068
    move-object v0, v3

    .line 2687069
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687070
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687071
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687072
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2687073
    check-cast p2, LX/JOB;

    .line 2687074
    iget-object v0, p0, LX/JOC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;

    iget-object v1, p2, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget v2, p2, LX/JOB;->b:I

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 2687075
    if-eqz v1, :cond_0

    .line 2687076
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 2687077
    if-eqz v3, :cond_0

    .line 2687078
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->W()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2687079
    if-eqz v3, :cond_0

    .line 2687080
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2687081
    :goto_0
    move-object v3, v3

    .line 2687082
    int-to-float v4, v2

    iget-object v5, v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->d:Landroid/content/res/Resources;

    const v6, 0x7f0b2552

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 2687083
    div-int/lit8 v5, v2, 0x2

    int-to-float v5, v5

    const/high16 v6, 0x3f400000    # 0.75f

    iget-object v7, v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->d:Landroid/content/res/Resources;

    const v8, 0x7f0b2553

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 2687084
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f020a3d

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b2552

    invoke-interface {v6, v7}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b2553

    invoke-interface {v6, v7}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2, v4}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0, v5}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b2556

    invoke-interface {v3, p0, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b2557

    invoke-interface {v3, p2, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b2554

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b2555

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2687085
    const v4, 0x13dfd83e

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2687086
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2687087
    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2687088
    invoke-static {}, LX/1dS;->b()V

    .line 2687089
    iget v0, p1, LX/1dQ;->b:I

    .line 2687090
    packed-switch v0, :pswitch_data_0

    .line 2687091
    :goto_0
    return-object v2

    .line 2687092
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2687093
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2687094
    check-cast v1, LX/JOB;

    .line 2687095
    iget-object v3, p0, LX/JOC;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;

    iget-object p1, v1, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object p2, v1, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2687096
    iget-object p0, v3, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->c:LX/JNi;

    invoke-virtual {p0, p1, p2, v0}, LX/JNi;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;Landroid/view/View;)V

    .line 2687097
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x13dfd83e
        :pswitch_0
    .end packed-switch
.end method
