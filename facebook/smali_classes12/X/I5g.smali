.class public final LX/I5g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/events/feed/data/EventFeedStories;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2536016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2536017
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2536018
    if-nez p1, :cond_0

    .line 2536019
    const/4 v0, 0x0

    .line 2536020
    :goto_0
    return-object v0

    .line 2536021
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 2536022
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v1

    .line 2536023
    new-instance v1, Lcom/facebook/events/feed/data/EventFeedStories;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedHomeStories;->n()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2536024
    iget-object p0, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, p0

    .line 2536025
    invoke-direct {v1, v3, v2, v0}, Lcom/facebook/events/feed/data/EventFeedStories;-><init>(LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0ta;)V

    move-object v0, v1

    goto :goto_0
.end method
