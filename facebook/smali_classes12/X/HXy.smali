.class public final LX/HXy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2479767
    iput-object p1, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    iput-object p2, p0, LX/HXy;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2479768
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HXy;->c:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2479763
    iget-object v0, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    const-string v1, "platform_webview_manifest_refresh_succeeded"

    invoke-static {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2479764
    iget-boolean v0, p0, LX/HXy;->c:Z

    if-nez v0, :cond_0

    .line 2479765
    iget-object v0, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-static {v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->n(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    .line 2479766
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2479769
    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HXy;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2479770
    iget-object v0, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    iget-object v0, v0, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    invoke-virtual {v0, p1, v2, p2}, LX/HY4;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;ZZ)V

    .line 2479771
    iget-object v0, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    const-string v1, "platform_webview_method_refresh_succeeded"

    invoke-static {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2479772
    iput-boolean v2, p0, LX/HXy;->c:Z

    .line 2479773
    iget-object v0, p0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-static {v0, p1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V

    .line 2479774
    :cond_0
    return-void
.end method
