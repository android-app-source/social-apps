.class public final LX/IIz;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562334
    iput-object p1, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method

.method public static u(LX/IIz;)V
    .locals 3

    .prologue
    .line 2562325
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562326
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562327
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aB:LX/IIm;

    .line 2562328
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562329
    if-eqz v2, :cond_0

    .line 2562330
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562331
    :goto_0
    return-void

    .line 2562332
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562333
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 2562323
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562324
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 2562310
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_VIEW_RENDER:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->a(LX/IG6;)V

    .line 2562311
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x1

    .line 2562312
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ai:Z

    .line 2562313
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2562314
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->N$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562315
    :goto_0
    return-void

    .line 2562316
    :cond_0
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->w(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2562317
    invoke-static {p0}, LX/IIz;->u(LX/IIz;)V

    goto :goto_0

    .line 2562318
    :cond_1
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->C:LX/121;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080e44

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IIy;

    invoke-direct {v3, p0}, LX/IIy;-><init>(LX/IIz;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2562319
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->C:LX/121;

    sget-object v1, LX/0yY;->VIEW_MAP_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2562320
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->B$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562321
    iget-object v0, p0, LX/IIz;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    invoke-virtual {v0}, LX/IID;->k()V

    .line 2562322
    return-void
.end method
