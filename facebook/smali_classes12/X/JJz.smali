.class public final LX/JJz;
.super LX/JJy;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/react/bridge/Callback;

.field public final synthetic b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final synthetic c:LX/JK3;


# direct methods
.method public constructor <init>(LX/JK3;Lcom/facebook/react/bridge/Callback;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 1

    .prologue
    .line 2680001
    iput-object p1, p0, LX/JJz;->c:LX/JK3;

    iput-object p2, p0, LX/JJz;->a:Lcom/facebook/react/bridge/Callback;

    iput-object p3, p0, LX/JJz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {p0}, LX/JJy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2680002
    iget-object v0, p0, LX/JJz;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2680003
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2680004
    iget-object v0, p0, LX/JJz;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2680005
    iget-object v0, p0, LX/JJz;->c:LX/JK3;

    iget-object v0, v0, LX/JK3;->g:LX/DBA;

    iget-object v1, p0, LX/JJz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, p1, v1}, LX/DBA;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2680006
    iget-object v0, p0, LX/JJz;->c:LX/JK3;

    iget-object v0, v0, LX/JK3;->g:LX/DBA;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    const/4 p1, 0x1

    .line 2680007
    const/4 p0, 0x0

    invoke-static {v0, v1, p0, p1, p1}, LX/DBA;->a(LX/DBA;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;ZZ)V

    .line 2680008
    return-void
.end method
