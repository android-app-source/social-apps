.class public final LX/IOh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DKm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/DKm",
        "<",
        "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;",
        "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V
    .locals 0

    .prologue
    .line 2572818
    iput-object p1, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/DKn;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;",
            ">;)",
            "LX/DKn",
            "<",
            "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2572819
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2572820
    if-eqz v0, :cond_3

    .line 2572821
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2572822
    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;->j()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2572823
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2572824
    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;->j()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2572825
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2572826
    if-eqz v0, :cond_0

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2572827
    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v4, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2572828
    :goto_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2572829
    check-cast v0, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;->j()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 2572830
    new-instance v0, LX/DKn;

    invoke-direct {v0, v2, v1, v3}, LX/DKn;-><init>(LX/0Px;ZLjava/lang/String;)V

    .line 2572831
    :goto_1
    return-object v0

    .line 2572832
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v1, v2

    .line 2572833
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, v3

    .line 2572834
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2572809
    new-instance v0, LX/IOs;

    invoke-direct {v0}, LX/IOs;-><init>()V

    move-object v1, v0

    .line 2572810
    const-string v2, "inviting_user_id"

    iget-object v0, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572811
    const-string v0, "cover_photo_height"

    const-string v2, "64"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572812
    const-string v0, "cover_photo_width"

    const-string v2, "64"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572813
    const-string v0, "friend_id"

    iget-object v2, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572814
    const-string v0, "add_to_groups_page_cursor"

    invoke-virtual {v1, v0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572815
    const-string v0, "add_to_groups_page_size"

    const-string v2, "20"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2572816
    iget-object v0, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->e:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2572817
    return-object v0
.end method

.method public final a(LX/0Px;Z)V
    .locals 5

    .prologue
    .line 2572806
    iget-object v0, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iput-object p1, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->j:LX/0Px;

    .line 2572807
    iget-object v0, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->o:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->l:Ljava/lang/String;

    iget-object v2, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->j:LX/0Px;

    iget-object v3, p0, LX/IOh;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->a(Ljava/lang/String;LX/0Px;LX/IOb;Z)V

    .line 2572808
    return-void
.end method
