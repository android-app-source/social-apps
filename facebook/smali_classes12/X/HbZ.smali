.class public LX/HbZ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HbY;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2485905
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2485906
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/content/Context;I)LX/HbY;
    .locals 8

    .prologue
    .line 2485907
    new-instance v0, LX/HbY;

    .line 2485908
    new-instance v3, LX/Hbf;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0xc

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v3, v1, v2, v4}, LX/Hbf;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 2485909
    move-object v4, v3

    .line 2485910
    check-cast v4, LX/Hbf;

    const-class v1, LX/HbF;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HbF;

    invoke-static {p0}, LX/HaZ;->a(LX/0QB;)LX/HaZ;

    move-result-object v6

    check-cast v6, LX/HaZ;

    invoke-static {p0}, LX/Hai;->a(LX/0QB;)LX/Hai;

    move-result-object v7

    check-cast v7, LX/Hai;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, LX/HbY;-><init>(Landroid/view/View;Landroid/content/Context;ILX/Hbf;LX/HbF;LX/HaZ;LX/Hai;)V

    .line 2485911
    return-object v0
.end method
