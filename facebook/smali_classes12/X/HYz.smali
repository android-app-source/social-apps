.class public final LX/HYz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/registration/controller/RegistrationFragmentController;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/controller/RegistrationFragmentController;)V
    .locals 0

    .prologue
    .line 2481221
    iput-object p1, p0, LX/HYz;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2481222
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/model/ContactPointSuggestions;

    .line 2481223
    iget-object v1, p0, LX/HYz;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v1, v1, Lcom/facebook/registration/controller/RegistrationFragmentController;->c:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(Lcom/facebook/registration/model/ContactPointSuggestions;)V

    .line 2481224
    iget-object v1, p0, LX/HYz;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v1, v1, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    iget-object v2, v0, Lcom/facebook/registration/model/ContactPointSuggestions;->prefillContactPoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, v0, Lcom/facebook/registration/model/ContactPointSuggestions;->autocompleteContactPoints:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2481225
    iget-object v3, v1, LX/HZt;->a:LX/0Zb;

    sget-object p0, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_SUCCESS:LX/HZu;

    invoke-static {v1, p0}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "prefill"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "autocomplete"

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481226
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2481227
    iget-object v0, p0, LX/HYz;->a:Lcom/facebook/registration/controller/RegistrationFragmentController;

    iget-object v0, v0, Lcom/facebook/registration/controller/RegistrationFragmentController;->d:LX/HZt;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 2481228
    iget-object v2, v0, LX/HZt;->a:LX/0Zb;

    sget-object p0, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_ERROR:LX/HZu;

    invoke-static {v0, p0}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "error_description"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481229
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2481230
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/HYz;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
