.class public final LX/HFh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V
    .locals 0

    .prologue
    .line 2444053
    iput-object p1, p0, LX/HFh;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2444054
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2444055
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2444056
    iget-object v0, p0, LX/HFh;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->j:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444057
    :goto_0
    return-void

    .line 2444058
    :cond_0
    iget-object v0, p0, LX/HFh;->a:Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->j:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2444059
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2444060
    return-void
.end method
