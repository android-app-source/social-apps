.class public LX/Hni;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Hni;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Hnh;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:J

.field public e:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Zb;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0SF;",
            ">;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2503491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2503492
    iput-object p1, p0, LX/Hni;->a:LX/0Or;

    .line 2503493
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Hni;->b:Ljava/util/Map;

    .line 2503494
    iput-object p2, p0, LX/Hni;->e:LX/0Zb;

    .line 2503495
    return-void
.end method

.method public static a(LX/0QB;)LX/Hni;
    .locals 5

    .prologue
    .line 2503471
    sget-object v0, LX/Hni;->f:LX/Hni;

    if-nez v0, :cond_1

    .line 2503472
    const-class v1, LX/Hni;

    monitor-enter v1

    .line 2503473
    :try_start_0
    sget-object v0, LX/Hni;->f:LX/Hni;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2503474
    if-eqz v2, :cond_0

    .line 2503475
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2503476
    new-instance v4, LX/Hni;

    const/16 v3, 0x2e3

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {v4, p0, v3}, LX/Hni;-><init>(LX/0Or;LX/0Zb;)V

    .line 2503477
    move-object v0, v4

    .line 2503478
    sput-object v0, LX/Hni;->f:LX/Hni;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2503479
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2503480
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2503481
    :cond_1
    sget-object v0, LX/Hni;->f:LX/Hni;

    return-object v0

    .line 2503482
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2503483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2503484
    iget-object v0, p0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2503485
    :goto_0
    return-void

    .line 2503486
    :cond_0
    iget-object v0, p0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hnh;

    iput-boolean p2, v0, LX/Hnh;->a:Z

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2503487
    iget-object v0, p0, LX/Hni;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2503488
    iget-object v0, p0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2503489
    :goto_0
    return-void

    .line 2503490
    :cond_0
    iget-object v0, p0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hnh;

    iput-boolean p2, v0, LX/Hnh;->b:Z

    goto :goto_0
.end method
