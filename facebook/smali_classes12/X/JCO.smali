.class public final enum LX/JCO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JCO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JCO;

.field public static final enum FIRST:LX/JCO;

.field public static final enum FULL_CARD:LX/JCO;

.field public static final enum LAST:LX/JCO;

.field public static final enum MIDDLE:LX/JCO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2661898
    new-instance v0, LX/JCO;

    const-string v1, "FULL_CARD"

    invoke-direct {v0, v1, v2}, LX/JCO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCO;->FULL_CARD:LX/JCO;

    .line 2661899
    new-instance v0, LX/JCO;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v3}, LX/JCO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCO;->FIRST:LX/JCO;

    .line 2661900
    new-instance v0, LX/JCO;

    const-string v1, "MIDDLE"

    invoke-direct {v0, v1, v4}, LX/JCO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCO;->MIDDLE:LX/JCO;

    .line 2661901
    new-instance v0, LX/JCO;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v5}, LX/JCO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCO;->LAST:LX/JCO;

    .line 2661902
    const/4 v0, 0x4

    new-array v0, v0, [LX/JCO;

    sget-object v1, LX/JCO;->FULL_CARD:LX/JCO;

    aput-object v1, v0, v2

    sget-object v1, LX/JCO;->FIRST:LX/JCO;

    aput-object v1, v0, v3

    sget-object v1, LX/JCO;->MIDDLE:LX/JCO;

    aput-object v1, v0, v4

    sget-object v1, LX/JCO;->LAST:LX/JCO;

    aput-object v1, v0, v5

    sput-object v0, LX/JCO;->$VALUES:[LX/JCO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2661903
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JCO;
    .locals 1

    .prologue
    .line 2661904
    const-class v0, LX/JCO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JCO;

    return-object v0
.end method

.method public static values()[LX/JCO;
    .locals 1

    .prologue
    .line 2661905
    sget-object v0, LX/JCO;->$VALUES:[LX/JCO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JCO;

    return-object v0
.end method
