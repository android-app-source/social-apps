.class public final LX/JHc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2673931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static A(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2675408
    sub-int v6, p3, p2

    .line 2675409
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2675410
    const/4 v4, 0x0

    .line 2675411
    const/4 v0, 0x0

    .line 2675412
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2675413
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_9

    .line 2675414
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675415
    if-gez v0, :cond_1

    .line 2675416
    const/4 v0, 0x0

    .line 2675417
    :goto_1
    return-object v0

    .line 2675418
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2675419
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675420
    if-lez v1, :cond_3

    .line 2675421
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675422
    add-int/lit8 v1, v1, 0x1

    .line 2675423
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675424
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675425
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2675426
    goto :goto_0

    .line 2675427
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675428
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675429
    :sswitch_0
    const-string v9, "ref"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "code"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "ssid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v9, "timestamp"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x3

    goto :goto_3

    .line 2675430
    :pswitch_0
    or-int/lit8 v2, v5, 0x1

    .line 2675431
    if-nez v3, :cond_4

    .line 2675432
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675433
    :cond_4
    const-string v5, "ref"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675434
    goto :goto_0

    .line 2675435
    :pswitch_1
    or-int/lit8 v2, v5, 0x2

    .line 2675436
    if-nez v3, :cond_5

    .line 2675437
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675438
    :cond_5
    const-string v5, "code"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675439
    goto/16 :goto_0

    .line 2675440
    :pswitch_2
    or-int/lit8 v2, v5, 0x4

    .line 2675441
    if-nez v3, :cond_6

    .line 2675442
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675443
    :cond_6
    const-string v5, "ssid"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675444
    goto/16 :goto_0

    .line 2675445
    :pswitch_3
    invoke-static {v0}, LX/48d;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 2675446
    if-eqz v2, :cond_8

    .line 2675447
    or-int/lit8 v0, v5, 0x8

    .line 2675448
    if-nez v3, :cond_7

    .line 2675449
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675450
    :cond_7
    const-string v5, "timestamp"

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v3, v5, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move v5, v0

    .line 2675451
    goto/16 :goto_0

    .line 2675452
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675453
    :cond_9
    sparse-switch v5, :sswitch_data_1

    .line 2675454
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675455
    :sswitch_4
    const-string v1, "com.facebook.beam.receiver.BeamReceiverActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675456
    :sswitch_5
    const-string v1, "com.facebook.beam.receiver.BeamReceiverActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x1b893 -> :sswitch_0
        0x2eaded -> :sswitch_1
        0x36037b -> :sswitch_2
        0x3492916 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0xe -> :sswitch_5
    .end sparse-switch
.end method

.method private static B(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2675367
    sub-int v6, p3, p2

    .line 2675368
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2675369
    const/4 v4, 0x0

    .line 2675370
    const/4 v0, 0x0

    .line 2675371
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2675372
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_7

    .line 2675373
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675374
    if-gez v0, :cond_1

    .line 2675375
    const/4 v0, 0x0

    .line 2675376
    :goto_1
    return-object v0

    .line 2675377
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2675378
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675379
    if-lez v1, :cond_3

    .line 2675380
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675381
    add-int/lit8 v1, v1, 0x1

    .line 2675382
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675383
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675384
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2675385
    goto :goto_0

    .line 2675386
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675387
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675388
    :sswitch_0
    const-string v9, "webview"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "code"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "ssid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    .line 2675389
    :pswitch_0
    or-int/lit8 v2, v5, 0x1

    .line 2675390
    if-nez v3, :cond_4

    .line 2675391
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675392
    :cond_4
    const-string v5, "redirect_uri"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675393
    goto :goto_0

    .line 2675394
    :pswitch_1
    or-int/lit8 v2, v5, 0x2

    .line 2675395
    if-nez v3, :cond_5

    .line 2675396
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675397
    :cond_5
    const-string v5, "code"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675398
    goto :goto_0

    .line 2675399
    :pswitch_2
    or-int/lit8 v2, v5, 0x4

    .line 2675400
    if-nez v3, :cond_6

    .line 2675401
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2675402
    :cond_6
    const-string v5, "ssid"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2675403
    goto/16 :goto_0

    .line 2675404
    :cond_7
    sparse-switch v5, :sswitch_data_1

    .line 2675405
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675406
    :sswitch_3
    const-string v1, "com.facebook.beam.sender.activity.BeamSenderActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675407
    :sswitch_4
    const-string v1, "com.facebook.beam.sender.activity.BeamSenderActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x2eaded -> :sswitch_1
        0x36037b -> :sswitch_2
        0x48fb3bf9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_4
        0x6 -> :sswitch_3
    .end sparse-switch
.end method

.method private static C(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675332
    sub-int v7, p3, p2

    .line 2675333
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675334
    const/4 v4, 0x0

    .line 2675335
    const/4 v2, 0x0

    .line 2675336
    const/4 v1, 0x0

    .line 2675337
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675338
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2675339
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675340
    if-gez v0, :cond_0

    .line 2675341
    const/4 v0, 0x0

    .line 2675342
    :goto_1
    return-object v0

    .line 2675343
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675344
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675345
    if-lez v1, :cond_2

    .line 2675346
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675347
    add-int/lit8 v1, v1, 0x1

    .line 2675348
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2675349
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675350
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2675351
    goto :goto_0

    .line 2675352
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675353
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675354
    :pswitch_0
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2675355
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675356
    if-nez v3, :cond_3

    .line 2675357
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675358
    :cond_3
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675359
    goto :goto_0

    .line 2675360
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2675361
    goto :goto_0

    .line 2675362
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2675363
    const/4 v0, 0x0

    goto :goto_1

    .line 2675364
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2675365
    const/4 v0, 0x0

    goto :goto_1

    .line 2675366
    :cond_6
    const-string v1, "com.facebook.backgroundlocation.upsell.BackgroundLocationResurrectionActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x356f97e5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static D(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675282
    sub-int v7, p3, p2

    .line 2675283
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675284
    const/4 v4, 0x0

    .line 2675285
    const/4 v2, 0x0

    .line 2675286
    const/4 v1, 0x0

    .line 2675287
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675288
    :goto_0
    if-ge v1, v7, :cond_8

    .line 2675289
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675290
    if-gez v0, :cond_0

    .line 2675291
    const/4 v0, 0x0

    .line 2675292
    :goto_1
    return-object v0

    .line 2675293
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675294
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675295
    if-lez v1, :cond_2

    .line 2675296
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675297
    add-int/lit8 v1, v1, 0x1

    .line 2675298
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675299
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675300
    if-eqz v0, :cond_7

    move-object v4, v0

    .line 2675301
    goto :goto_0

    .line 2675302
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675303
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675304
    :sswitch_0
    const-string v10, "stage"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "page_view_time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "session_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    .line 2675305
    :pswitch_0
    or-int/lit8 v2, v6, 0x8

    .line 2675306
    if-nez v3, :cond_3

    .line 2675307
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675308
    :cond_3
    const-string v6, "stage"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675309
    goto :goto_0

    .line 2675310
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675311
    if-nez v3, :cond_4

    .line 2675312
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675313
    :cond_4
    const-string v6, "page_view_time"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675314
    goto/16 :goto_0

    .line 2675315
    :pswitch_2
    or-int/lit8 v2, v6, 0x4

    .line 2675316
    if-nez v3, :cond_5

    .line 2675317
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675318
    :cond_5
    const-string v6, "session_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675319
    goto/16 :goto_0

    .line 2675320
    :pswitch_3
    or-int/lit8 v2, v6, 0x1

    .line 2675321
    if-nez v3, :cond_6

    .line 2675322
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675323
    :cond_6
    const-string v6, "integration_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675324
    goto/16 :goto_0

    .line 2675325
    :cond_7
    const/4 v0, 0x1

    move v5, v0

    .line 2675326
    goto/16 :goto_0

    .line 2675327
    :cond_8
    packed-switch v6, :pswitch_data_1

    .line 2675328
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675329
    :pswitch_4
    if-eqz v5, :cond_9

    .line 2675330
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675331
    :cond_9
    const-string v1, "com.facebook.browser.liteclient.rapidfeedback.BrowserLiteRapidFeedbackActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0xd1b -> :sswitch_3
        0x68ac2fe -> :sswitch_0
        0x50e06677 -> :sswitch_1
        0x630ddf64 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xf
        :pswitch_4
    .end packed-switch
.end method

.method private static E(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675247
    sub-int v7, p3, p2

    .line 2675248
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675249
    const/4 v4, 0x0

    .line 2675250
    const/4 v2, 0x0

    .line 2675251
    const/4 v1, 0x0

    .line 2675252
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675253
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2675254
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675255
    if-gez v0, :cond_0

    .line 2675256
    const/4 v0, 0x0

    .line 2675257
    :goto_1
    return-object v0

    .line 2675258
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675259
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675260
    if-lez v1, :cond_2

    .line 2675261
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675262
    add-int/lit8 v1, v1, 0x1

    .line 2675263
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2675264
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675265
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2675266
    goto :goto_0

    .line 2675267
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675268
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675269
    :pswitch_0
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2675270
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675271
    if-nez v3, :cond_3

    .line 2675272
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675273
    :cond_3
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675274
    goto :goto_0

    .line 2675275
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2675276
    goto :goto_0

    .line 2675277
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2675278
    const/4 v0, 0x0

    goto :goto_1

    .line 2675279
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2675280
    const/4 v0, 0x0

    goto :goto_1

    .line 2675281
    :cond_6
    const-string v1, "com.facebook.securitycheckup.SecurityCheckupMainActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x356f97e5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static F(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2675233
    if-nez p4, :cond_4

    .line 2675234
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2675235
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "reply_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2675236
    if-nez v0, :cond_0

    .line 2675237
    const/4 v0, 0x0

    .line 2675238
    :goto_1
    return-object v0

    .line 2675239
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2675240
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2675241
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2675242
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2675243
    :cond_2
    const-string v0, "reply_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2675244
    if-gt p2, v1, :cond_3

    .line 2675245
    const-string v3, "com.facebook.audience.direct.app.SnacksReplyThreadActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675246
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static G(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2675211
    if-nez p4, :cond_7

    .line 2675212
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2675213
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2675214
    if-nez v0, :cond_0

    .line 2675215
    const/4 v0, 0x0

    .line 2675216
    :goto_1
    return-object v0

    .line 2675217
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2675218
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2675219
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2675220
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2675221
    :cond_2
    const-string v0, "id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2675222
    if-gt p2, v1, :cond_3

    .line 2675223
    const-string v3, "com.facebook.audience.direct.app.SnacksReplyThreadActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675224
    :cond_3
    if-gt p2, v1, :cond_4

    .line 2675225
    const/4 v0, 0x0

    goto :goto_1

    .line 2675226
    :cond_4
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2675227
    const/16 v2, 0x2f

    if-ne v1, v2, :cond_6

    .line 2675228
    const-string v1, "reply_id/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2675229
    const/4 v0, 0x0

    goto :goto_1

    .line 2675230
    :cond_5
    add-int/lit8 v4, v0, 0x9

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v6, p5

    .line 2675231
    invoke-static/range {v1 .. v6}, LX/JHc;->F(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675232
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    move-object v5, p4

    goto :goto_0
.end method

.method private static H(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675151
    if-nez p4, :cond_f

    .line 2675152
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2675153
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "page_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2675154
    if-nez v0, :cond_0

    .line 2675155
    const/4 v0, 0x0

    .line 2675156
    :goto_1
    return-object v0

    .line 2675157
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2675158
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2675159
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2675160
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2675161
    :cond_2
    const-string v0, "page_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2675162
    if-gt p2, v1, :cond_3

    .line 2675163
    const-string v3, "com.facebook.gametime.ui.GametimeActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object/from16 v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675164
    :cond_3
    if-gt p2, v1, :cond_4

    .line 2675165
    const/4 v0, 0x0

    goto :goto_1

    .line 2675166
    :cond_4
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2675167
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_e

    .line 2675168
    sub-int v7, p2, v0

    .line 2675169
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675170
    const/4 v4, 0x0

    .line 2675171
    const/4 v2, 0x0

    .line 2675172
    const/4 v1, 0x0

    .line 2675173
    const/4 v0, 0x0

    move v6, v2

    move-object v3, v5

    move v5, v0

    .line 2675174
    :goto_2
    if-ge v1, v7, :cond_b

    .line 2675175
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675176
    if-gez v0, :cond_5

    .line 2675177
    const/4 v0, 0x0

    goto :goto_1

    .line 2675178
    :cond_5
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675179
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675180
    if-lez v1, :cond_7

    .line 2675181
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675182
    add-int/lit8 v1, v1, 0x1

    .line 2675183
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_6
    :goto_4
    packed-switch v2, :pswitch_data_0

    .line 2675184
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675185
    if-eqz v0, :cond_a

    move-object v4, v0

    .line 2675186
    goto :goto_2

    .line 2675187
    :cond_7
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675188
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2675189
    :sswitch_0
    const-string v10, "referrer"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x0

    goto :goto_4

    :sswitch_1
    const-string v10, "ref"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x1

    goto :goto_4

    .line 2675190
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2675191
    if-nez v3, :cond_8

    .line 2675192
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675193
    :cond_8
    const-string v6, "referrer"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675194
    goto :goto_2

    .line 2675195
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675196
    if-nez v3, :cond_9

    .line 2675197
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675198
    :cond_9
    const-string v6, "ref"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675199
    goto :goto_2

    .line 2675200
    :cond_a
    const/4 v0, 0x1

    move v5, v0

    .line 2675201
    goto :goto_2

    .line 2675202
    :cond_b
    packed-switch v6, :pswitch_data_1

    .line 2675203
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675204
    :pswitch_2
    if-eqz v5, :cond_c

    .line 2675205
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675206
    :cond_c
    const-string v1, "com.facebook.gametime.ui.GametimeActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675207
    :pswitch_3
    if-eqz v5, :cond_d

    .line 2675208
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675209
    :cond_d
    const-string v1, "com.facebook.gametime.ui.GametimeActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675210
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_f
    move-object v5, p4

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2b1183e1 -> :sswitch_0
        0x1b893 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static I(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674729
    if-nez p4, :cond_f

    .line 2674730
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674731
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "league_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674732
    if-nez v0, :cond_0

    .line 2674733
    const/4 v0, 0x0

    .line 2674734
    :goto_1
    return-object v0

    .line 2674735
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674736
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2674737
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2674738
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674739
    :cond_2
    const-string v0, "league_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674740
    if-gt p2, v1, :cond_3

    .line 2674741
    const-string v3, "com.facebook.gametime.ui.GametimeLeagueActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object/from16 v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2674742
    :cond_3
    if-gt p2, v1, :cond_4

    .line 2674743
    const/4 v0, 0x0

    goto :goto_1

    .line 2674744
    :cond_4
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2674745
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_e

    .line 2674746
    sub-int v7, p2, v0

    .line 2674747
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674748
    const/4 v4, 0x0

    .line 2674749
    const/4 v2, 0x0

    .line 2674750
    const/4 v1, 0x0

    .line 2674751
    const/4 v0, 0x0

    move v6, v2

    move-object v3, v5

    move v5, v0

    .line 2674752
    :goto_2
    if-ge v1, v7, :cond_b

    .line 2674753
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674754
    if-gez v0, :cond_5

    .line 2674755
    const/4 v0, 0x0

    goto :goto_1

    .line 2674756
    :cond_5
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674757
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674758
    if-lez v1, :cond_7

    .line 2674759
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674760
    add-int/lit8 v1, v1, 0x1

    .line 2674761
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_6
    :goto_4
    packed-switch v2, :pswitch_data_0

    .line 2674762
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674763
    if-eqz v0, :cond_a

    move-object v4, v0

    .line 2674764
    goto :goto_2

    .line 2674765
    :cond_7
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674766
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2674767
    :sswitch_0
    const-string v10, "referrer"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x0

    goto :goto_4

    :sswitch_1
    const-string v10, "ref"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x1

    goto :goto_4

    .line 2674768
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2674769
    if-nez v3, :cond_8

    .line 2674770
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674771
    :cond_8
    const-string v6, "referrer"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674772
    goto :goto_2

    .line 2674773
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2674774
    if-nez v3, :cond_9

    .line 2674775
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674776
    :cond_9
    const-string v6, "ref"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674777
    goto :goto_2

    .line 2674778
    :cond_a
    const/4 v0, 0x1

    move v5, v0

    .line 2674779
    goto :goto_2

    .line 2674780
    :cond_b
    packed-switch v6, :pswitch_data_1

    .line 2674781
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674782
    :pswitch_2
    if-eqz v5, :cond_c

    .line 2674783
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674784
    :cond_c
    const-string v1, "com.facebook.gametime.ui.GametimeLeagueActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674785
    :pswitch_3
    if-eqz v5, :cond_d

    .line 2674786
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674787
    :cond_d
    const-string v1, "com.facebook.gametime.ui.GametimeLeagueActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674788
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_f
    move-object v5, p4

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2b1183e1 -> :sswitch_0
        0x1b893 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static J(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2675074
    sub-int v6, p3, p2

    .line 2675075
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2675076
    const/4 v5, 0x0

    .line 2675077
    const/4 v0, 0x0

    .line 2675078
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2675079
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_7

    .line 2675080
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675081
    if-gez v0, :cond_1

    .line 2675082
    const/4 v0, 0x0

    .line 2675083
    :goto_1
    return-object v0

    .line 2675084
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2675085
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675086
    if-lez v1, :cond_3

    .line 2675087
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675088
    add-int/lit8 v1, v1, 0x1

    .line 2675089
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675090
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675091
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2675092
    goto :goto_0

    .line 2675093
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675094
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675095
    :sswitch_0
    const-string v9, "referrer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "ref"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "_bt_"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    .line 2675096
    :pswitch_0
    or-int/lit8 v2, v3, 0x1

    .line 2675097
    if-nez v4, :cond_4

    .line 2675098
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2675099
    :cond_4
    const-string v3, "referrer"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2675100
    goto :goto_0

    .line 2675101
    :pswitch_1
    or-int/lit8 v2, v3, 0x2

    .line 2675102
    if-nez v4, :cond_5

    .line 2675103
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2675104
    :cond_5
    const-string v3, "ref"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2675105
    goto :goto_0

    .line 2675106
    :pswitch_2
    or-int/lit8 v2, v3, 0x4

    .line 2675107
    if-nez v4, :cond_6

    .line 2675108
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2675109
    :cond_6
    const-string v3, "bt"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2675110
    goto/16 :goto_0

    .line 2675111
    :cond_7
    packed-switch v3, :pswitch_data_1

    .line 2675112
    :pswitch_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675113
    :pswitch_4
    const/4 v1, 0x1

    const/16 v2, 0xfd

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675114
    :pswitch_5
    const/4 v1, 0x1

    const/16 v2, 0xfd

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675115
    :pswitch_6
    const/4 v1, 0x1

    const/16 v2, 0xfd

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x2b1183e1 -> :sswitch_0
        0x1b893 -> :sswitch_1
        0x2cad8e -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method private static K(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2675060
    if-nez p4, :cond_4

    .line 2675061
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2675062
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2675063
    if-nez v0, :cond_0

    .line 2675064
    const/4 v0, 0x0

    .line 2675065
    :goto_1
    return-object v0

    .line 2675066
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2675067
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2675068
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2675069
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2675070
    :cond_2
    const-string v0, "id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2675071
    if-gt p2, v1, :cond_3

    .line 2675072
    const-string v3, "com.facebook.storygallerysurvey.activity.StoryGallerySurveyWithStoryActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675073
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static L(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675012
    sub-int v7, p3, p2

    .line 2675013
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675014
    const/4 v4, 0x0

    .line 2675015
    const/4 v2, 0x0

    .line 2675016
    const/4 v1, 0x0

    .line 2675017
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675018
    :goto_0
    if-ge v1, v7, :cond_7

    .line 2675019
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675020
    if-gez v0, :cond_0

    .line 2675021
    const/4 v0, 0x0

    .line 2675022
    :goto_1
    return-object v0

    .line 2675023
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675024
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675025
    if-lez v1, :cond_2

    .line 2675026
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675027
    add-int/lit8 v1, v1, 0x1

    .line 2675028
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675029
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675030
    if-eqz v0, :cond_6

    move-object v4, v0

    .line 2675031
    goto :goto_0

    .line 2675032
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675033
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675034
    :sswitch_0
    const-string v10, "profile_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "short_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "qp_h"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    .line 2675035
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2675036
    if-nez v3, :cond_3

    .line 2675037
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675038
    :cond_3
    const-string v6, "profile_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675039
    goto :goto_0

    .line 2675040
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675041
    if-nez v3, :cond_4

    .line 2675042
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675043
    :cond_4
    const-string v6, "short_name"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675044
    goto :goto_0

    .line 2675045
    :pswitch_2
    or-int/lit8 v2, v6, 0x4

    .line 2675046
    if-nez v3, :cond_5

    .line 2675047
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675048
    :cond_5
    const-string v6, "qp_h"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675049
    goto/16 :goto_0

    .line 2675050
    :cond_6
    const/4 v0, 0x1

    move v5, v0

    .line 2675051
    goto/16 :goto_0

    .line 2675052
    :cond_7
    sparse-switch v6, :sswitch_data_1

    .line 2675053
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675054
    :sswitch_3
    if-eqz v5, :cond_8

    .line 2675055
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675056
    :cond_8
    const-string v1, "com.facebook.wem.sahayta.SahaytaChecklistActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675057
    :sswitch_4
    if-eqz v5, :cond_9

    .line 2675058
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675059
    :cond_9
    const-string v1, "com.facebook.wem.sahayta.SahaytaChecklistActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x41b8e48f -> :sswitch_0
        0x350e48 -> :sswitch_2
        0x5d541c6e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x3 -> :sswitch_4
        0x7 -> :sswitch_3
    .end sparse-switch
.end method

.method private static M(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674972
    sub-int v7, p3, p2

    .line 2674973
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674974
    const/4 v4, 0x0

    .line 2674975
    const/4 v2, 0x0

    .line 2674976
    const/4 v1, 0x0

    .line 2674977
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674978
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2674979
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674980
    if-gez v0, :cond_0

    .line 2674981
    const/4 v0, 0x0

    .line 2674982
    :goto_1
    return-object v0

    .line 2674983
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674984
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674985
    if-lez v1, :cond_2

    .line 2674986
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674987
    add-int/lit8 v1, v1, 0x1

    .line 2674988
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674989
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674990
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2674991
    goto :goto_0

    .line 2674992
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674993
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674994
    :sswitch_0
    const-string v10, "hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "ix_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2674995
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2674996
    if-nez v3, :cond_3

    .line 2674997
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674998
    :cond_3
    const-string v6, "hash"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674999
    goto :goto_0

    .line 2675000
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675001
    if-nez v3, :cond_4

    .line 2675002
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675003
    :cond_4
    const-string v6, "ix_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675004
    goto :goto_0

    .line 2675005
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2675006
    goto :goto_0

    .line 2675007
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2675008
    const/4 v0, 0x0

    goto :goto_1

    .line 2675009
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2675010
    const/4 v0, 0x0

    goto :goto_1

    .line 2675011
    :cond_7
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1846796a -> :sswitch_1
        0x30c10e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static N(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674932
    sub-int v7, p3, p2

    .line 2674933
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674934
    const/4 v4, 0x0

    .line 2674935
    const/4 v2, 0x0

    .line 2674936
    const/4 v1, 0x0

    .line 2674937
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674938
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2674939
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674940
    if-gez v0, :cond_0

    .line 2674941
    const/4 v0, 0x0

    .line 2674942
    :goto_1
    return-object v0

    .line 2674943
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674944
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674945
    if-lez v1, :cond_2

    .line 2674946
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674947
    add-int/lit8 v1, v1, 0x1

    .line 2674948
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674949
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674950
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2674951
    goto :goto_0

    .line 2674952
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674953
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674954
    :sswitch_0
    const-string v10, "hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "ix_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2674955
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2674956
    if-nez v3, :cond_3

    .line 2674957
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674958
    :cond_3
    const-string v6, "hash"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674959
    goto :goto_0

    .line 2674960
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2674961
    if-nez v3, :cond_4

    .line 2674962
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674963
    :cond_4
    const-string v6, "ix_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674964
    goto :goto_0

    .line 2674965
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2674966
    goto :goto_0

    .line 2674967
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2674968
    const/4 v0, 0x0

    goto :goto_1

    .line 2674969
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2674970
    const/4 v0, 0x0

    goto :goto_1

    .line 2674971
    :cond_7
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1846796a -> :sswitch_1
        0x30c10e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static O(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674869
    sub-int v7, p3, p2

    .line 2674870
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674871
    const/4 v4, 0x0

    .line 2674872
    const/4 v2, 0x0

    .line 2674873
    const/4 v1, 0x0

    .line 2674874
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674875
    :goto_0
    if-ge v1, v7, :cond_a

    .line 2674876
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674877
    if-gez v0, :cond_0

    .line 2674878
    const/4 v0, 0x0

    .line 2674879
    :goto_1
    return-object v0

    .line 2674880
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674881
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674882
    if-lez v1, :cond_2

    .line 2674883
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674884
    add-int/lit8 v1, v1, 0x1

    .line 2674885
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674886
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674887
    if-eqz v0, :cond_9

    move-object v4, v0

    .line 2674888
    goto :goto_0

    .line 2674889
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674890
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674891
    :sswitch_0
    const-string v10, "offer_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "notif_trigger"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "notif_medium"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "rule"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v10, "hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v10, "ix_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x5

    goto :goto_3

    .line 2674892
    :pswitch_0
    or-int/lit8 v2, v6, 0x4

    .line 2674893
    if-nez v3, :cond_3

    .line 2674894
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674895
    :cond_3
    const-string v6, "offer_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674896
    goto/16 :goto_0

    .line 2674897
    :pswitch_1
    or-int/lit8 v2, v6, 0x10

    .line 2674898
    if-nez v3, :cond_4

    .line 2674899
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674900
    :cond_4
    const-string v6, "notif_trigger"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674901
    goto/16 :goto_0

    .line 2674902
    :pswitch_2
    or-int/lit8 v2, v6, 0x8

    .line 2674903
    if-nez v3, :cond_5

    .line 2674904
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674905
    :cond_5
    const-string v6, "notif_medium"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674906
    goto/16 :goto_0

    .line 2674907
    :pswitch_3
    or-int/lit8 v2, v6, 0x20

    .line 2674908
    if-nez v3, :cond_6

    .line 2674909
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674910
    :cond_6
    const-string v6, "rule"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674911
    goto/16 :goto_0

    .line 2674912
    :pswitch_4
    or-int/lit8 v2, v6, 0x1

    .line 2674913
    if-nez v3, :cond_7

    .line 2674914
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674915
    :cond_7
    const-string v6, "hash"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674916
    goto/16 :goto_0

    .line 2674917
    :pswitch_5
    or-int/lit8 v2, v6, 0x2

    .line 2674918
    if-nez v3, :cond_8

    .line 2674919
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674920
    :cond_8
    const-string v6, "ix_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674921
    goto/16 :goto_0

    .line 2674922
    :cond_9
    const/4 v0, 0x1

    move v5, v0

    .line 2674923
    goto/16 :goto_0

    .line 2674924
    :cond_a
    sparse-switch v6, :sswitch_data_1

    .line 2674925
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674926
    :sswitch_6
    if-eqz v5, :cond_b

    .line 2674927
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674928
    :cond_b
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674929
    :sswitch_7
    if-eqz v5, :cond_c

    .line 2674930
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674931
    :cond_c
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x2344e457 -> :sswitch_1
        -0x1846796a -> :sswitch_5
        0x30c10e -> :sswitch_4
        0x3596fc -> :sswitch_3
        0x23bdc764 -> :sswitch_2
        0x25c9bcc9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x7 -> :sswitch_6
        0x3f -> :sswitch_7
    .end sparse-switch
.end method

.method private static P(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674829
    sub-int v7, p3, p2

    .line 2674830
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674831
    const/4 v4, 0x0

    .line 2674832
    const/4 v2, 0x0

    .line 2674833
    const/4 v1, 0x0

    .line 2674834
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674835
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2674836
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674837
    if-gez v0, :cond_0

    .line 2674838
    const/4 v0, 0x0

    .line 2674839
    :goto_1
    return-object v0

    .line 2674840
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674841
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674842
    if-lez v1, :cond_2

    .line 2674843
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674844
    add-int/lit8 v1, v1, 0x1

    .line 2674845
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674846
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674847
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2674848
    goto :goto_0

    .line 2674849
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674850
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674851
    :sswitch_0
    const-string v10, "hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "ix_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2674852
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2674853
    if-nez v3, :cond_3

    .line 2674854
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674855
    :cond_3
    const-string v6, "hash"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674856
    goto :goto_0

    .line 2674857
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2674858
    if-nez v3, :cond_4

    .line 2674859
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674860
    :cond_4
    const-string v6, "ix_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674861
    goto :goto_0

    .line 2674862
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2674863
    goto :goto_0

    .line 2674864
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2674865
    const/4 v0, 0x0

    goto :goto_1

    .line 2674866
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2674867
    const/4 v0, 0x0

    goto :goto_1

    .line 2674868
    :cond_7
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1846796a -> :sswitch_1
        0x30c10e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static Q(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674789
    sub-int v7, p3, p2

    .line 2674790
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674791
    const/4 v4, 0x0

    .line 2674792
    const/4 v2, 0x0

    .line 2674793
    const/4 v1, 0x0

    .line 2674794
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674795
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2674796
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674797
    if-gez v0, :cond_0

    .line 2674798
    const/4 v0, 0x0

    .line 2674799
    :goto_1
    return-object v0

    .line 2674800
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674801
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674802
    if-lez v1, :cond_2

    .line 2674803
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674804
    add-int/lit8 v1, v1, 0x1

    .line 2674805
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674806
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674807
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2674808
    goto :goto_0

    .line 2674809
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674810
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674811
    :sswitch_0
    const-string v10, "hash"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "ix_params"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2674812
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2674813
    if-nez v3, :cond_3

    .line 2674814
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674815
    :cond_3
    const-string v6, "hash"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674816
    goto :goto_0

    .line 2674817
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2674818
    if-nez v3, :cond_4

    .line 2674819
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674820
    :cond_4
    const-string v6, "ix_params"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674821
    goto :goto_0

    .line 2674822
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2674823
    goto :goto_0

    .line 2674824
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2674825
    const/4 v0, 0x0

    goto :goto_1

    .line 2674826
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2674827
    const/4 v0, 0x0

    goto :goto_1

    .line 2674828
    :cond_7
    const-string v1, "com.facebook.browserextensions.core.navigation.InstantExperienceUriMapperActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1846796a -> :sswitch_1
        0x30c10e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static R(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2676871
    sub-int v7, p3, p2

    .line 2676872
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2676873
    const/4 v4, 0x0

    .line 2676874
    const/4 v2, 0x0

    .line 2676875
    const/4 v1, 0x0

    .line 2676876
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2676877
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2676878
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676879
    if-gez v0, :cond_0

    .line 2676880
    const/4 v0, 0x0

    .line 2676881
    :goto_1
    return-object v0

    .line 2676882
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2676883
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676884
    if-lez v1, :cond_2

    .line 2676885
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676886
    add-int/lit8 v1, v1, 0x1

    .line 2676887
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2676888
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676889
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2676890
    goto :goto_0

    .line 2676891
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676892
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676893
    :sswitch_0
    const-string v10, "appid"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2676894
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2676895
    if-nez v3, :cond_3

    .line 2676896
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676897
    :cond_3
    const-string v6, "app_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676898
    goto :goto_0

    .line 2676899
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2676900
    if-nez v3, :cond_4

    .line 2676901
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676902
    :cond_4
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676903
    goto :goto_0

    .line 2676904
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2676905
    goto :goto_0

    .line 2676906
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2676907
    const/4 v0, 0x0

    goto :goto_1

    .line 2676908
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2676909
    const/4 v0, 0x0

    goto :goto_1

    .line 2676910
    :cond_7
    const-string v1, "com.facebook.quicksilver.QuicksilverActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x356f97e5 -> :sswitch_1
        0x58b82fc -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static S(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675457
    sub-int v7, p3, p2

    .line 2675458
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675459
    const/4 v4, 0x0

    .line 2675460
    const/4 v2, 0x0

    .line 2675461
    const/4 v1, 0x0

    .line 2675462
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675463
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2675464
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675465
    if-gez v0, :cond_0

    .line 2675466
    const/4 v0, 0x0

    .line 2675467
    :goto_1
    return-object v0

    .line 2675468
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675469
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675470
    if-lez v1, :cond_2

    .line 2675471
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675472
    add-int/lit8 v1, v1, 0x1

    .line 2675473
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2675474
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675475
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2675476
    goto :goto_0

    .line 2675477
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675478
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675479
    :pswitch_0
    const-string v10, "_bt_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2675480
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675481
    if-nez v3, :cond_3

    .line 2675482
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675483
    :cond_3
    const-string v6, "unused"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675484
    goto :goto_0

    .line 2675485
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2675486
    goto :goto_0

    .line 2675487
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2675488
    const/4 v0, 0x0

    goto :goto_1

    .line 2675489
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2675490
    const/4 v0, 0x0

    goto :goto_1

    .line 2675491
    :cond_6
    const-string v1, "com.facebook.work.invitecoworker.InviteCoworkerActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2cad8e
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static T(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2676832
    if-nez p4, :cond_c

    .line 2676833
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2676834
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2676835
    if-nez v0, :cond_0

    .line 2676836
    const/4 v0, 0x0

    .line 2676837
    :goto_1
    return-object v0

    .line 2676838
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2676839
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2676840
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2676841
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2676842
    :cond_2
    const-string v0, "id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2676843
    if-gt p2, v1, :cond_3

    .line 2676844
    const/4 v0, 0x0

    goto :goto_1

    .line 2676845
    :cond_3
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2676846
    const/16 v2, 0x2f

    if-ne v1, v2, :cond_b

    .line 2676847
    if-ge v0, p2, :cond_a

    .line 2676848
    add-int/lit8 v1, v0, 0x1

    aget-char v0, p1, v0

    .line 2676849
    sparse-switch v0, :sswitch_data_0

    .line 2676850
    const/4 v0, 0x0

    goto :goto_1

    .line 2676851
    :sswitch_0
    const-string v0, "rofile_picture"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2676852
    const/4 v0, 0x0

    goto :goto_1

    .line 2676853
    :cond_4
    add-int/lit8 v0, v1, 0xe

    .line 2676854
    if-lt v0, p2, :cond_5

    .line 2676855
    const-string v3, "com.facebook.timeline.legacycontact.MemorialProfilePictureActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2676856
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 2676857
    :sswitch_1
    const-string v0, "over_photo"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2676858
    const/4 v0, 0x0

    goto :goto_1

    .line 2676859
    :cond_6
    add-int/lit8 v0, v1, 0xa

    .line 2676860
    if-lt v0, p2, :cond_7

    .line 2676861
    const-string v3, "com.facebook.timeline.legacycontact.MemorialCoverPhotoActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2676862
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 2676863
    :sswitch_2
    const-string v0, "riend_requests"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2676864
    const/4 v0, 0x0

    goto :goto_1

    .line 2676865
    :cond_8
    add-int/lit8 v0, v1, 0xe

    .line 2676866
    if-lt v0, p2, :cond_9

    .line 2676867
    const-string v3, "com.facebook.timeline.legacycontact.MemorialFriendRequestsActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2676868
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2676869
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2676870
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_c
    move-object v5, p4

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_1
        0x66 -> :sswitch_2
        0x70 -> :sswitch_0
    .end sparse-switch
.end method

.method private static U(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2676789
    sub-int v7, p3, p2

    .line 2676790
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2676791
    const/4 v4, 0x0

    .line 2676792
    const/4 v2, 0x0

    .line 2676793
    const/4 v1, 0x0

    .line 2676794
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2676795
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2676796
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676797
    if-gez v0, :cond_0

    .line 2676798
    const/4 v0, 0x0

    .line 2676799
    :goto_1
    return-object v0

    .line 2676800
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2676801
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676802
    if-lez v1, :cond_2

    .line 2676803
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676804
    add-int/lit8 v1, v1, 0x1

    .line 2676805
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2676806
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676807
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2676808
    goto :goto_0

    .line 2676809
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676810
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676811
    :sswitch_0
    const-string v10, "ad_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "lead_gen_data_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2676812
    :pswitch_0
    or-int/lit8 v2, v6, 0x2

    .line 2676813
    if-nez v3, :cond_3

    .line 2676814
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676815
    :cond_3
    const-string v6, "ad_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676816
    goto :goto_0

    .line 2676817
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2676818
    if-nez v3, :cond_4

    .line 2676819
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676820
    :cond_4
    const-string v6, "lead_gen_data_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676821
    goto :goto_0

    .line 2676822
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2676823
    goto :goto_0

    .line 2676824
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2676825
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2676826
    :pswitch_3
    if-eqz v5, :cond_7

    .line 2676827
    const/4 v0, 0x0

    goto :goto_1

    .line 2676828
    :cond_7
    const-string v1, "com.facebook.leadgen.deeplink.LeadGenActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2676829
    :pswitch_4
    if-eqz v5, :cond_8

    .line 2676830
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2676831
    :cond_8
    const-string v1, "com.facebook.leadgen.deeplink.LeadGenActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x585ceb7 -> :sswitch_0
        0x1939237e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static V(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2676749
    sub-int v7, p3, p2

    .line 2676750
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2676751
    const/4 v5, 0x0

    .line 2676752
    const/4 v2, 0x0

    .line 2676753
    const/4 v1, 0x0

    .line 2676754
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2676755
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2676756
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676757
    if-gez v0, :cond_0

    .line 2676758
    const/4 v0, 0x0

    .line 2676759
    :goto_1
    return-object v0

    .line 2676760
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2676761
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676762
    if-lez v1, :cond_2

    .line 2676763
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676764
    add-int/lit8 v1, v1, 0x1

    .line 2676765
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2676766
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676767
    if-eqz v0, :cond_5

    move-object v5, v0

    .line 2676768
    goto :goto_0

    .line 2676769
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676770
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676771
    :sswitch_0
    const-string v10, "_bt_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2676772
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2676773
    if-nez v4, :cond_3

    .line 2676774
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676775
    :cond_3
    const-string v6, "unused2"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676776
    goto :goto_0

    .line 2676777
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2676778
    if-nez v4, :cond_4

    .line 2676779
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676780
    :cond_4
    const-string v6, "unused"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2676781
    goto :goto_0

    .line 2676782
    :cond_5
    const/4 v0, 0x1

    move v3, v0

    .line 2676783
    goto :goto_0

    .line 2676784
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2676785
    const/4 v0, 0x0

    goto :goto_1

    .line 2676786
    :pswitch_2
    if-eqz v3, :cond_7

    .line 2676787
    const/4 v0, 0x0

    goto :goto_1

    .line 2676788
    :cond_7
    const/4 v1, 0x1

    const/16 v2, 0xff

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x356f97e5 -> :sswitch_1
        0x2cad8e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static W(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2676735
    if-nez p4, :cond_4

    .line 2676736
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2676737
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "topic_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2676738
    if-nez v0, :cond_0

    .line 2676739
    const/4 v0, 0x0

    .line 2676740
    :goto_1
    return-object v0

    .line 2676741
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2676742
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2676743
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2676744
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2676745
    :cond_2
    const-string v0, "topic_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2676746
    if-gt p2, v1, :cond_3

    .line 2676747
    const/4 v2, 0x1

    const/16 v3, 0x13a

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2676748
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static X(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2676697
    sub-int v7, p3, p2

    .line 2676698
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2676699
    const/4 v4, 0x0

    .line 2676700
    const/4 v2, 0x0

    .line 2676701
    const/4 v1, 0x0

    .line 2676702
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2676703
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2676704
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676705
    if-gez v0, :cond_0

    .line 2676706
    const/4 v0, 0x0

    .line 2676707
    :goto_1
    return-object v0

    .line 2676708
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2676709
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676710
    if-lez v1, :cond_2

    .line 2676711
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676712
    add-int/lit8 v1, v1, 0x1

    .line 2676713
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2676714
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676715
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2676716
    goto :goto_0

    .line 2676717
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676718
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676719
    :pswitch_0
    const-string v10, "enable"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2676720
    :pswitch_1
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2676721
    if-eqz v2, :cond_4

    .line 2676722
    or-int/lit8 v0, v6, 0x1

    .line 2676723
    if-nez v3, :cond_3

    .line 2676724
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2676725
    :cond_3
    const-string v6, "enable"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2676726
    goto :goto_0

    .line 2676727
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2676728
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2676729
    goto :goto_0

    .line 2676730
    :cond_6
    packed-switch v6, :pswitch_data_2

    .line 2676731
    const/4 v0, 0x0

    goto :goto_1

    .line 2676732
    :pswitch_2
    if-eqz v5, :cond_7

    .line 2676733
    const/4 v0, 0x0

    goto :goto_1

    .line 2676734
    :cond_7
    const-string v1, "com.facebook.transliteration.TransliterationQPActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x4d6ada7d
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static Y(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2676683
    if-nez p4, :cond_4

    .line 2676684
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2676685
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "collection_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2676686
    if-nez v0, :cond_0

    .line 2676687
    const/4 v0, 0x0

    .line 2676688
    :goto_1
    return-object v0

    .line 2676689
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2676690
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2676691
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2676692
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2676693
    :cond_2
    const-string v0, "collection_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2676694
    if-gt p2, v1, :cond_3

    .line 2676695
    const/4 v2, 0x1

    const/16 v3, 0x13e

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2676696
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static Z(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2676637
    sub-int v6, p3, p2

    .line 2676638
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2676639
    const/4 v5, 0x0

    .line 2676640
    const/4 v0, 0x0

    .line 2676641
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2676642
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_8

    .line 2676643
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676644
    if-gez v0, :cond_1

    .line 2676645
    const/4 v0, 0x0

    .line 2676646
    :goto_1
    return-object v0

    .line 2676647
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2676648
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676649
    if-lez v1, :cond_3

    .line 2676650
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676651
    add-int/lit8 v1, v1, 0x1

    .line 2676652
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2676653
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676654
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2676655
    goto :goto_0

    .line 2676656
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676657
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676658
    :sswitch_0
    const-string v9, "search"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    .line 2676659
    :pswitch_0
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2676660
    if-eqz v2, :cond_5

    .line 2676661
    or-int/lit8 v0, v3, 0x4

    .line 2676662
    if-nez v4, :cond_4

    .line 2676663
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676664
    :cond_4
    const-string v3, "search"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v3, v0

    .line 2676665
    goto :goto_0

    .line 2676666
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 2676667
    :pswitch_1
    or-int/lit8 v2, v3, 0x1

    .line 2676668
    if-nez v4, :cond_6

    .line 2676669
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676670
    :cond_6
    const-string v3, "id"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676671
    goto/16 :goto_0

    .line 2676672
    :pswitch_2
    or-int/lit8 v2, v3, 0x2

    .line 2676673
    if-nez v4, :cond_7

    .line 2676674
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676675
    :cond_7
    const-string v3, "title"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676676
    goto/16 :goto_0

    .line 2676677
    :cond_8
    packed-switch v3, :pswitch_data_1

    .line 2676678
    :pswitch_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2676679
    :pswitch_4
    const/4 v1, 0x1

    const/16 v2, 0x12e

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2676680
    :pswitch_5
    const/4 v1, 0x1

    const/16 v2, 0x12e

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2676681
    :pswitch_6
    const/4 v1, 0x1

    const/16 v2, 0x12e

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2676682
    :pswitch_7
    const/4 v1, 0x1

    const/16 v2, 0x12e

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x36059a58 -> :sswitch_0
        0xd1b -> :sswitch_1
        0x6942258 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LX/17c;)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2676911
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 2676912
    if-gez v5, :cond_1

    move-object v0, v1

    .line 2676913
    :cond_0
    :goto_0
    return-object v0

    .line 2676914
    :cond_1
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    move v0, v3

    .line 2676915
    :goto_1
    if-gt v0, v4, :cond_2

    add-int v7, v5, v0

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x2f

    if-ne v7, v8, :cond_2

    .line 2676916
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2676917
    :cond_2
    add-int/2addr v5, v0

    .line 2676918
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2676919
    :goto_2
    sub-int v7, v0, v5

    .line 2676920
    new-array v7, v7, [C

    .line 2676921
    invoke-virtual {p1, v5, v0, v7, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 2676922
    sget-object v0, LX/007;->a:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2676923
    invoke-static {p0, v7, p2}, LX/JHc;->a(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;

    move-result-object v0

    .line 2676924
    if-nez v0, :cond_0

    .line 2676925
    :cond_3
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_4
    :goto_3
    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 2676926
    goto :goto_0

    .line 2676927
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_2

    .line 2676928
    :sswitch_0
    const-string v3, "fb"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    goto :goto_3

    :sswitch_1
    const-string v2, "fb-messenger"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v3

    goto :goto_3

    :sswitch_2
    const-string v2, "http"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v0, 0x2

    goto :goto_3

    :sswitch_3
    const-string v2, "https"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v4

    goto :goto_3

    .line 2676929
    :pswitch_0
    invoke-static {p0, v7, p2}, LX/JHc;->e(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2676930
    :pswitch_1
    invoke-static {p0, v7, p2}, LX/JHc;->b(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2676931
    :pswitch_2
    invoke-static {p0, v7, p2}, LX/JHc;->c(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2676932
    :pswitch_3
    invoke-static {p0, v7, p2}, LX/JHc;->d(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x545c10be -> :sswitch_1
        0xcbc -> :sswitch_0
        0x310888 -> :sswitch_2
        0x5f008eb -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2676572
    sub-int v6, p3, p2

    .line 2676573
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2676574
    const/4 v5, 0x0

    .line 2676575
    const/4 v0, 0x0

    .line 2676576
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2676577
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_c

    .line 2676578
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2676579
    if-gez v0, :cond_1

    .line 2676580
    const/4 v0, 0x0

    .line 2676581
    :goto_1
    return-object v0

    .line 2676582
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2676583
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2676584
    if-lez v1, :cond_3

    .line 2676585
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2676586
    add-int/lit8 v1, v1, 0x1

    .line 2676587
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2676588
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2676589
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2676590
    goto :goto_0

    .line 2676591
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2676592
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2676593
    :sswitch_0
    const-string v9, "redirect"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "share_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "notif_trigger"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v9, "offer_should_claim"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v9, "notif_medium"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v9, "rule"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x5

    goto :goto_3

    :sswitch_6
    const-string v9, "claim_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x6

    goto :goto_3

    :sswitch_7
    const-string v9, "offer_view_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x7

    goto :goto_3

    .line 2676594
    :pswitch_0
    or-int/lit8 v2, v3, 0x20

    .line 2676595
    if-nez v4, :cond_4

    .line 2676596
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676597
    :cond_4
    const-string v3, "redirect"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676598
    goto/16 :goto_0

    .line 2676599
    :pswitch_1
    or-int/lit16 v2, v3, 0x80

    .line 2676600
    if-nez v4, :cond_5

    .line 2676601
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676602
    :cond_5
    const-string v3, "share_id"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676603
    goto/16 :goto_0

    .line 2676604
    :pswitch_2
    or-int/lit8 v2, v3, 0x4

    .line 2676605
    if-nez v4, :cond_6

    .line 2676606
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676607
    :cond_6
    const-string v3, "notif_trigger"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676608
    goto/16 :goto_0

    .line 2676609
    :pswitch_3
    or-int/lit8 v2, v3, 0x8

    .line 2676610
    if-nez v4, :cond_7

    .line 2676611
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676612
    :cond_7
    const-string v3, "offer_should_claim"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676613
    goto/16 :goto_0

    .line 2676614
    :pswitch_4
    or-int/lit8 v2, v3, 0x2

    .line 2676615
    if-nez v4, :cond_8

    .line 2676616
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676617
    :cond_8
    const-string v3, "notif_medium"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676618
    goto/16 :goto_0

    .line 2676619
    :pswitch_5
    or-int/lit8 v2, v3, 0x40

    .line 2676620
    if-nez v4, :cond_9

    .line 2676621
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676622
    :cond_9
    const-string v3, "rule"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676623
    goto/16 :goto_0

    .line 2676624
    :pswitch_6
    or-int/lit8 v2, v3, 0x1

    .line 2676625
    if-nez v4, :cond_a

    .line 2676626
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676627
    :cond_a
    const-string v3, "claim_type"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676628
    goto/16 :goto_0

    .line 2676629
    :pswitch_7
    or-int/lit8 v2, v3, 0x10

    .line 2676630
    if-nez v4, :cond_b

    .line 2676631
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2676632
    :cond_b
    const-string v3, "offer_view_id"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2676633
    goto/16 :goto_0

    .line 2676634
    :cond_c
    packed-switch v3, :pswitch_data_1

    .line 2676635
    :pswitch_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2676636
    :pswitch_9
    const/4 v1, 0x1

    const/16 v2, 0xc1

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x70792c4d -> :sswitch_3
        -0x2e430824 -> :sswitch_0
        -0x2c889aa3 -> :sswitch_6
        -0x2c54de85 -> :sswitch_1
        -0x2344e457 -> :sswitch_2
        -0x223361ee -> :sswitch_7
        0x3596fc -> :sswitch_5
        0x23bdc764 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x90
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v1, 0x2

    const/16 v5, 0x3f

    const/4 v2, 0x1

    const/16 v8, 0x8

    const/4 v4, 0x0

    .line 2675803
    array-length v3, p1

    .line 2675804
    if-lez v3, :cond_0

    .line 2675805
    const/4 v0, 0x0

    aget-char v0, p1, v0

    .line 2675806
    packed-switch v0, :pswitch_data_0

    .line 2675807
    :cond_0
    :goto_0
    :pswitch_0
    return-object v4

    .line 2675808
    :pswitch_1
    const-string v0, "ffers"

    invoke-static {p1, v2, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675809
    const/4 v0, 0x6

    if-ge v0, v3, :cond_0

    .line 2675810
    const/4 v0, 0x6

    aget-char v0, p1, v0

    .line 2675811
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2675812
    :sswitch_0
    const/4 v0, 0x7

    if-ge v0, v3, :cond_1

    const/4 v0, 0x7

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_0

    .line 2675813
    :cond_1
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2675814
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2675815
    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    .line 2675816
    :sswitch_1
    const-string v0, "arcode_fullscreen"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675817
    const/16 v0, 0x19

    if-ge v0, v3, :cond_0

    .line 2675818
    const/16 v0, 0x19

    const/16 v2, 0x1a

    aget-char v0, p1, v0

    .line 2675819
    sparse-switch v0, :sswitch_data_2

    goto :goto_0

    .line 2675820
    :sswitch_2
    const/16 v0, 0x1a

    if-ge v0, v3, :cond_0

    const/16 v0, 0x1a

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2675821
    const/16 v2, 0x1b

    :sswitch_3
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2675822
    invoke-static/range {v0 .. v5}, LX/JHc;->d(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    .line 2675823
    :sswitch_4
    const-string v0, "etail"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675824
    const/16 v0, 0xd

    if-ge v0, v3, :cond_0

    .line 2675825
    const/16 v0, 0xd

    const/16 v2, 0xe

    aget-char v0, p1, v0

    .line 2675826
    sparse-switch v0, :sswitch_data_3

    goto :goto_0

    .line 2675827
    :sswitch_5
    const/16 v0, 0xe

    if-ge v0, v3, :cond_2

    const/16 v0, 0xe

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_3

    .line 2675828
    :cond_2
    const/16 v0, 0xe

    const-string v1, "view"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675829
    const/16 v0, 0x12

    if-ge v0, v3, :cond_0

    .line 2675830
    const/16 v0, 0x12

    const/16 v2, 0x13

    aget-char v0, p1, v0

    .line 2675831
    sparse-switch v0, :sswitch_data_4

    goto/16 :goto_0

    .line 2675832
    :sswitch_6
    const/16 v0, 0x13

    if-ge v0, v3, :cond_0

    const/16 v0, 0x13

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2675833
    const/16 v2, 0x14

    :sswitch_7
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2675834
    invoke-static/range {v0 .. v5}, LX/JHc;->a(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675835
    :cond_3
    const/16 v2, 0xf

    :sswitch_8
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2675836
    invoke-static/range {v0 .. v5}, LX/JHc;->b(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675837
    :sswitch_9
    const-string v0, "allet"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675838
    const/16 v0, 0xd

    if-ge v0, v3, :cond_4

    .line 2675839
    const/16 v0, 0xd

    const/16 v2, 0xe

    aget-char v0, p1, v0

    .line 2675840
    sparse-switch v0, :sswitch_data_5

    goto/16 :goto_0

    .line 2675841
    :sswitch_a
    const/16 v0, 0xe

    if-ge v0, v3, :cond_0

    const/16 v0, 0xe

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2675842
    const/16 v2, 0xf

    :sswitch_b
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2675843
    invoke-static/range {v0 .. v5}, LX/JHc;->c(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675844
    :cond_4
    const/16 v3, 0xc0

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675845
    :sswitch_c
    const/4 v0, 0x7

    const-string v1, "te/detail/view"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675846
    const/16 v0, 0x15

    if-ge v0, v3, :cond_0

    .line 2675847
    const/16 v0, 0x15

    const/16 v2, 0x16

    aget-char v0, p1, v0

    .line 2675848
    sparse-switch v0, :sswitch_data_6

    goto/16 :goto_0

    .line 2675849
    :sswitch_d
    const/16 v0, 0x16

    if-ge v0, v3, :cond_0

    const/16 v0, 0x16

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2675850
    const/16 v2, 0x17

    :sswitch_e
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2675851
    invoke-static/range {v0 .. v5}, LX/JHc;->e(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675852
    :pswitch_2
    if-ge v2, v3, :cond_0

    .line 2675853
    aget-char v0, p1, v2

    .line 2675854
    sparse-switch v0, :sswitch_data_7

    goto/16 :goto_0

    .line 2675855
    :sswitch_f
    if-ge v1, v3, :cond_0

    .line 2675856
    aget-char v0, p1, v1

    .line 2675857
    sparse-switch v0, :sswitch_data_8

    goto/16 :goto_0

    .line 2675858
    :sswitch_10
    const/4 v0, 0x3

    const-string v1, "e/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675859
    const/4 v0, 0x5

    if-ge v0, v3, :cond_5

    .line 2675860
    const/4 v0, 0x5

    aget-char v0, p1, v0

    .line 2675861
    sparse-switch v0, :sswitch_data_9

    .line 2675862
    :cond_5
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->l(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675863
    :sswitch_11
    const/4 v0, 0x6

    if-ge v0, v3, :cond_6

    .line 2675864
    const/4 v0, 0x6

    aget-char v0, p1, v0

    .line 2675865
    packed-switch v0, :pswitch_data_1

    .line 2675866
    :cond_6
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675867
    :pswitch_3
    const/4 v0, 0x7

    if-ge v0, v3, :cond_7

    .line 2675868
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2675869
    packed-switch v0, :pswitch_data_2

    .line 2675870
    :cond_7
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675871
    :pswitch_4
    if-ge v8, v3, :cond_8

    .line 2675872
    aget-char v0, p1, v8

    .line 2675873
    packed-switch v0, :pswitch_data_3

    .line 2675874
    :cond_8
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675875
    :pswitch_5
    const/16 v0, 0x9

    if-ge v0, v3, :cond_9

    .line 2675876
    const/16 v0, 0x9

    aget-char v0, p1, v0

    .line 2675877
    packed-switch v0, :pswitch_data_4

    .line 2675878
    :cond_9
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675879
    :pswitch_6
    const/16 v0, 0xa

    if-ge v0, v3, :cond_a

    .line 2675880
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2675881
    packed-switch v0, :pswitch_data_5

    .line 2675882
    :cond_a
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675883
    :pswitch_7
    const/16 v0, 0xb

    if-ge v0, v3, :cond_b

    .line 2675884
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2675885
    packed-switch v0, :pswitch_data_6

    .line 2675886
    :cond_b
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675887
    :pswitch_8
    const/16 v0, 0xc

    if-ge v0, v3, :cond_c

    .line 2675888
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2675889
    packed-switch v0, :pswitch_data_7

    .line 2675890
    :cond_c
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675891
    :pswitch_9
    const/16 v0, 0xd

    if-ge v0, v3, :cond_d

    .line 2675892
    const/16 v0, 0xd

    aget-char v0, p1, v0

    .line 2675893
    packed-switch v0, :pswitch_data_8

    .line 2675894
    :cond_d
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675895
    :pswitch_a
    const/16 v0, 0xe

    if-ge v0, v3, :cond_e

    const/16 v0, 0xe

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_d

    .line 2675896
    :cond_e
    const/16 v8, 0xe

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->g(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675897
    :sswitch_12
    const/4 v0, 0x6

    if-ge v0, v3, :cond_f

    .line 2675898
    const/4 v0, 0x6

    aget-char v0, p1, v0

    .line 2675899
    packed-switch v0, :pswitch_data_9

    .line 2675900
    :cond_f
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675901
    :pswitch_b
    const/4 v0, 0x7

    if-ge v0, v3, :cond_10

    .line 2675902
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2675903
    packed-switch v0, :pswitch_data_a

    .line 2675904
    :cond_10
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675905
    :pswitch_c
    if-ge v8, v3, :cond_11

    .line 2675906
    aget-char v0, p1, v8

    .line 2675907
    packed-switch v0, :pswitch_data_b

    .line 2675908
    :cond_11
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675909
    :pswitch_d
    const/16 v0, 0x9

    if-ge v0, v3, :cond_12

    .line 2675910
    const/16 v0, 0x9

    aget-char v0, p1, v0

    .line 2675911
    packed-switch v0, :pswitch_data_c

    .line 2675912
    :cond_12
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675913
    :pswitch_e
    const/16 v0, 0xa

    if-ge v0, v3, :cond_13

    .line 2675914
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2675915
    packed-switch v0, :pswitch_data_d

    .line 2675916
    :cond_13
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675917
    :pswitch_f
    const/16 v0, 0xb

    if-ge v0, v3, :cond_14

    .line 2675918
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2675919
    packed-switch v0, :pswitch_data_e

    .line 2675920
    :cond_14
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675921
    :pswitch_10
    const/16 v0, 0xc

    if-ge v0, v3, :cond_15

    .line 2675922
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2675923
    packed-switch v0, :pswitch_data_f

    .line 2675924
    :cond_15
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675925
    :pswitch_11
    const/16 v0, 0xd

    if-ge v0, v3, :cond_16

    .line 2675926
    const/16 v0, 0xd

    aget-char v0, p1, v0

    .line 2675927
    packed-switch v0, :pswitch_data_10

    .line 2675928
    :cond_16
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675929
    :pswitch_12
    const/16 v0, 0xe

    if-ge v0, v3, :cond_17

    .line 2675930
    const/16 v0, 0xe

    aget-char v0, p1, v0

    .line 2675931
    packed-switch v0, :pswitch_data_11

    .line 2675932
    :cond_17
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675933
    :pswitch_13
    const/16 v0, 0xf

    if-ge v0, v3, :cond_18

    .line 2675934
    const/16 v0, 0xf

    aget-char v0, p1, v0

    .line 2675935
    packed-switch v0, :pswitch_data_12

    .line 2675936
    :cond_18
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675937
    :pswitch_14
    const/16 v0, 0x10

    if-ge v0, v3, :cond_19

    .line 2675938
    const/16 v0, 0x10

    aget-char v0, p1, v0

    .line 2675939
    packed-switch v0, :pswitch_data_13

    .line 2675940
    :cond_19
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675941
    :pswitch_15
    const/16 v0, 0x11

    if-ge v0, v3, :cond_1a

    .line 2675942
    const/16 v0, 0x11

    aget-char v0, p1, v0

    .line 2675943
    packed-switch v0, :pswitch_data_14

    .line 2675944
    :cond_1a
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675945
    :pswitch_16
    const/16 v0, 0x12

    if-ge v0, v3, :cond_1b

    .line 2675946
    const/16 v0, 0x12

    aget-char v0, p1, v0

    .line 2675947
    packed-switch v0, :pswitch_data_15

    .line 2675948
    :cond_1b
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675949
    :pswitch_17
    const/16 v0, 0x13

    if-lt v0, v3, :cond_1c

    .line 2675950
    const/16 v3, 0x139

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675951
    :cond_1c
    const/4 v8, 0x5

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675952
    :sswitch_13
    const/4 v0, 0x3

    const-string v1, "ment_settings"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675953
    const/16 v0, 0x10

    if-lt v0, v3, :cond_0

    .line 2675954
    const-string v3, "com.facebook.payments.picker.PickerScreenActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675955
    :sswitch_14
    const-string v0, "of"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675956
    const/4 v0, 0x4

    if-ge v0, v3, :cond_0

    .line 2675957
    const/4 v0, 0x4

    aget-char v0, p1, v0

    .line 2675958
    sparse-switch v0, :sswitch_data_a

    goto/16 :goto_0

    .line 2675959
    :sswitch_15
    const/4 v0, 0x5

    const-string v1, "ssionalratertool"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675960
    const/16 v0, 0x15

    if-lt v0, v3, :cond_0

    .line 2675961
    const-string v3, "com.facebook.professionalratertool.activity.RatingMainActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675962
    :sswitch_16
    const/4 v0, 0x5

    const-string v1, "le"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2675963
    const/4 v0, 0x7

    if-ge v0, v3, :cond_39

    .line 2675964
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2675965
    sparse-switch v0, :sswitch_data_b

    goto/16 :goto_0

    .line 2675966
    :sswitch_17
    if-ge v8, v3, :cond_1d

    aget-char v0, p1, v8

    if-eq v0, v5, :cond_37

    .line 2675967
    :cond_1d
    if-ge v8, v3, :cond_1e

    .line 2675968
    aget-char v0, p1, v8

    .line 2675969
    sparse-switch v0, :sswitch_data_c

    :cond_1e
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675970
    invoke-static/range {v5 .. v10}, LX/JHc;->o(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675971
    :sswitch_18
    const/16 v0, 0x9

    if-ge v0, v3, :cond_1f

    .line 2675972
    const/16 v0, 0x9

    aget-char v0, p1, v0

    .line 2675973
    packed-switch v0, :pswitch_data_16

    :cond_1f
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675974
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675975
    :pswitch_18
    const/16 v0, 0xa

    if-ge v0, v3, :cond_20

    .line 2675976
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2675977
    packed-switch v0, :pswitch_data_17

    :cond_20
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675978
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675979
    :pswitch_19
    const/16 v0, 0xb

    if-ge v0, v3, :cond_21

    .line 2675980
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2675981
    packed-switch v0, :pswitch_data_18

    :cond_21
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675982
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675983
    :pswitch_1a
    const/16 v0, 0xc

    if-ge v0, v3, :cond_22

    .line 2675984
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2675985
    packed-switch v0, :pswitch_data_19

    :cond_22
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675986
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675987
    :pswitch_1b
    const/16 v0, 0xd

    if-lt v0, v3, :cond_23

    .line 2675988
    const/16 v3, 0x2a

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    :cond_23
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675989
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675990
    :sswitch_19
    const/16 v0, 0x9

    if-ge v0, v3, :cond_24

    .line 2675991
    const/16 v0, 0x9

    aget-char v0, p1, v0

    .line 2675992
    packed-switch v0, :pswitch_data_1a

    :cond_24
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675993
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675994
    :pswitch_1c
    const/16 v0, 0xa

    if-ge v0, v3, :cond_25

    .line 2675995
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2675996
    packed-switch v0, :pswitch_data_1b

    :cond_25
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2675997
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2675998
    :pswitch_1d
    const/16 v0, 0xb

    if-ge v0, v3, :cond_26

    .line 2675999
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2676000
    packed-switch v0, :pswitch_data_1c

    :cond_26
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676001
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676002
    :pswitch_1e
    const/16 v0, 0xc

    if-ge v0, v3, :cond_27

    .line 2676003
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2676004
    packed-switch v0, :pswitch_data_1d

    :cond_27
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676005
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676006
    :pswitch_1f
    const/16 v0, 0xd

    if-ge v0, v3, :cond_28

    .line 2676007
    const/16 v0, 0xd

    aget-char v0, p1, v0

    .line 2676008
    packed-switch v0, :pswitch_data_1e

    :cond_28
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676009
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676010
    :pswitch_20
    const/16 v0, 0xe

    if-ge v0, v3, :cond_29

    .line 2676011
    const/16 v0, 0xe

    aget-char v0, p1, v0

    .line 2676012
    packed-switch v0, :pswitch_data_1f

    :cond_29
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676013
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676014
    :pswitch_21
    const/16 v0, 0xf

    if-ge v0, v3, :cond_2a

    .line 2676015
    const/16 v0, 0xf

    aget-char v0, p1, v0

    .line 2676016
    packed-switch v0, :pswitch_data_20

    :cond_2a
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676017
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676018
    :pswitch_22
    const/16 v0, 0x10

    if-ge v0, v3, :cond_2b

    .line 2676019
    const/16 v0, 0x10

    aget-char v0, p1, v0

    .line 2676020
    packed-switch v0, :pswitch_data_21

    :cond_2b
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676021
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676022
    :pswitch_23
    const/16 v0, 0x11

    if-lt v0, v3, :cond_2c

    .line 2676023
    const-string v3, "com.facebook.timeline.refresher.ProfileRefresherProfileStepActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    :cond_2c
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676024
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676025
    :sswitch_1a
    const/16 v0, 0x9

    if-ge v0, v3, :cond_2d

    .line 2676026
    const/16 v0, 0x9

    aget-char v0, p1, v0

    .line 2676027
    packed-switch v0, :pswitch_data_22

    :cond_2d
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676028
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676029
    :pswitch_24
    const/16 v0, 0xa

    if-ge v0, v3, :cond_2e

    .line 2676030
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2676031
    packed-switch v0, :pswitch_data_23

    :cond_2e
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676032
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676033
    :pswitch_25
    const/16 v0, 0xb

    if-ge v0, v3, :cond_2f

    .line 2676034
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2676035
    packed-switch v0, :pswitch_data_24

    :cond_2f
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676036
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676037
    :pswitch_26
    const/16 v0, 0xc

    if-ge v0, v3, :cond_30

    .line 2676038
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2676039
    packed-switch v0, :pswitch_data_25

    :cond_30
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676040
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676041
    :pswitch_27
    const/16 v0, 0xd

    if-ge v0, v3, :cond_31

    .line 2676042
    const/16 v0, 0xd

    aget-char v0, p1, v0

    .line 2676043
    packed-switch v0, :pswitch_data_26

    :cond_31
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676044
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676045
    :pswitch_28
    const/16 v0, 0xe

    if-ge v0, v3, :cond_32

    .line 2676046
    const/16 v0, 0xe

    aget-char v0, p1, v0

    .line 2676047
    packed-switch v0, :pswitch_data_27

    :cond_32
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676048
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676049
    :pswitch_29
    const/16 v0, 0xf

    if-ge v0, v3, :cond_33

    .line 2676050
    const/16 v0, 0xf

    aget-char v0, p1, v0

    .line 2676051
    packed-switch v0, :pswitch_data_28

    :cond_33
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676052
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676053
    :pswitch_2a
    const/16 v0, 0x10

    if-ge v0, v3, :cond_34

    .line 2676054
    const/16 v0, 0x10

    aget-char v0, p1, v0

    .line 2676055
    packed-switch v0, :pswitch_data_29

    :cond_34
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676056
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676057
    :pswitch_2b
    const/16 v0, 0x11

    if-ge v0, v3, :cond_35

    .line 2676058
    const/16 v0, 0x11

    aget-char v0, p1, v0

    .line 2676059
    packed-switch v0, :pswitch_data_2a

    :cond_35
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676060
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676061
    :pswitch_2c
    const/16 v0, 0x12

    if-lt v0, v3, :cond_36

    .line 2676062
    const-string v3, "com.facebook.composer.lifeevent.type.ComposerLifeEventTypeActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    :cond_36
    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676063
    invoke-static/range {v5 .. v10}, LX/JHc;->m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676064
    :cond_37
    const/16 v2, 0x9

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676065
    invoke-static/range {v0 .. v5}, LX/JHc;->p(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676066
    :sswitch_1b
    const-string v0, "videos/"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676067
    const/16 v8, 0xf

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->q(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676068
    :sswitch_1c
    const-string v0, "iscovery"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676069
    const/16 v0, 0x10

    if-ge v0, v3, :cond_38

    .line 2676070
    const/16 v0, 0x10

    const/16 v2, 0x11

    aget-char v0, p1, v0

    .line 2676071
    sparse-switch v0, :sswitch_data_d

    goto/16 :goto_0

    .line 2676072
    :sswitch_1d
    const/16 v0, 0x11

    if-ge v0, v3, :cond_0

    const/16 v0, 0x11

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676073
    const/16 v2, 0x12

    :sswitch_1e
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676074
    invoke-static/range {v0 .. v5}, LX/JHc;->r(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676075
    :cond_38
    const-string v3, "com.facebook.profile.discovery.DiscoveryDashboardActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    :cond_39
    move-object v1, p0

    move v3, v8

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    .line 2676076
    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676077
    :sswitch_1f
    if-ge v1, v3, :cond_0

    .line 2676078
    aget-char v0, p1, v1

    .line 2676079
    packed-switch v0, :pswitch_data_2b

    goto/16 :goto_0

    .line 2676080
    :pswitch_2d
    const/4 v0, 0x3

    if-ge v0, v3, :cond_0

    .line 2676081
    const/4 v0, 0x3

    aget-char v0, p1, v0

    .line 2676082
    sparse-switch v0, :sswitch_data_e

    goto/16 :goto_0

    .line 2676083
    :sswitch_20
    const/4 v0, 0x4

    const-string v1, "e/creation"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676084
    const/16 v0, 0xe

    if-lt v0, v3, :cond_0

    .line 2676085
    const-string v3, "com.facebook.places.create.NewPlaceCreationActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676086
    :sswitch_21
    const/4 v0, 0x4

    const-string v1, "form_first_party"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676087
    const/16 v0, 0x14

    if-lt v0, v3, :cond_0

    .line 2676088
    const-string v3, "com.facebook.pages.common.platform.activity.PlatformFirstPartyFlowActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676089
    :pswitch_2e
    if-ge v2, v3, :cond_0

    .line 2676090
    aget-char v0, p1, v2

    .line 2676091
    sparse-switch v0, :sswitch_data_f

    goto/16 :goto_0

    .line 2676092
    :sswitch_22
    const-string v0, "ectionHub"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676093
    const/16 v0, 0xb

    if-ge v0, v3, :cond_3b

    .line 2676094
    const/16 v0, 0xb

    const/16 v2, 0xc

    aget-char v0, p1, v0

    .line 2676095
    sparse-switch v0, :sswitch_data_10

    goto/16 :goto_0

    .line 2676096
    :sswitch_23
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    const/16 v0, 0xc

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676097
    const/16 v2, 0xd

    :sswitch_24
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676098
    invoke-static/range {v0 .. v5}, LX/JHc;->t(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676099
    :sswitch_25
    const-string v0, "ent"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676100
    const/4 v0, 0x5

    if-ge v0, v3, :cond_0

    .line 2676101
    const/4 v0, 0x5

    aget-char v0, p1, v0

    .line 2676102
    sparse-switch v0, :sswitch_data_11

    goto/16 :goto_0

    .line 2676103
    :sswitch_26
    const/4 v0, 0x6

    if-ge v0, v3, :cond_3a

    const/4 v0, 0x6

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_0

    .line 2676104
    :cond_3a
    const/4 v0, 0x6

    const-string v1, "order_history/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676105
    const/16 v8, 0x14

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->s(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676106
    :sswitch_27
    const/4 v0, 0x6

    const-string v1, "collection"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676107
    const/16 v0, 0x10

    if-lt v0, v3, :cond_0

    .line 2676108
    const-string v3, "com.facebook.events.eventcollections.EventCollectionsActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676109
    :cond_3b
    const-string v3, "com.facebook.civicengagement.elections.ui.ElectionHubActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676110
    :pswitch_2f
    if-ge v2, v3, :cond_0

    .line 2676111
    aget-char v0, p1, v2

    .line 2676112
    sparse-switch v0, :sswitch_data_12

    goto/16 :goto_0

    .line 2676113
    :sswitch_28
    const-string v0, "ather/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    .line 2676114
    invoke-static/range {v5 .. v10}, LX/JHc;->u(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676115
    :sswitch_29
    const-string v0, "iends"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676116
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676117
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676118
    sparse-switch v0, :sswitch_data_13

    goto/16 :goto_0

    .line 2676119
    :sswitch_2a
    if-ge v8, v3, :cond_3c

    aget-char v0, p1, v8

    if-eq v0, v5, :cond_0

    .line 2676120
    :cond_3c
    const-string v0, "new_user_promotion"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676121
    const/16 v0, 0x1a

    if-lt v0, v3, :cond_0

    .line 2676122
    const-string v3, "com.facebook.friending.newuserpromotion.NewUserPromotionActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676123
    :sswitch_2b
    const-string v0, "ip"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676124
    const/16 v0, 0xa

    if-lt v0, v3, :cond_0

    .line 2676125
    const/16 v3, 0x95

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676126
    :sswitch_2c
    const-string v0, "ndraiser_"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676127
    const/16 v0, 0xb

    if-ge v0, v3, :cond_0

    .line 2676128
    const/16 v0, 0xb

    aget-char v0, p1, v0

    .line 2676129
    packed-switch v0, :pswitch_data_2c

    goto/16 :goto_0

    .line 2676130
    :pswitch_30
    const/16 v0, 0xc

    const-string v1, "eneficiary_"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676131
    const/16 v0, 0x17

    if-ge v0, v3, :cond_0

    .line 2676132
    const/16 v0, 0x17

    aget-char v0, p1, v0

    .line 2676133
    sparse-switch v0, :sswitch_data_14

    goto/16 :goto_0

    .line 2676134
    :sswitch_2d
    const/16 v0, 0x18

    const-string v1, "ther_input"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676135
    const/16 v0, 0x22

    if-lt v0, v3, :cond_0

    .line 2676136
    const/16 v3, 0x13d

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676137
    :pswitch_31
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    .line 2676138
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2676139
    packed-switch v0, :pswitch_data_2d

    :pswitch_32
    goto/16 :goto_0

    .line 2676140
    :pswitch_33
    const/16 v0, 0xd

    const-string v1, "eation_"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676141
    const/16 v0, 0x14

    if-ge v0, v3, :cond_0

    .line 2676142
    const/16 v0, 0x14

    aget-char v0, p1, v0

    .line 2676143
    sparse-switch v0, :sswitch_data_15

    goto/16 :goto_0

    .line 2676144
    :sswitch_2e
    const/16 v0, 0x15

    const-string v1, "harity_search"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676145
    const/16 v0, 0x22

    if-ge v0, v3, :cond_3d

    .line 2676146
    const/16 v0, 0x22

    const/16 v2, 0x23

    aget-char v0, p1, v0

    .line 2676147
    sparse-switch v0, :sswitch_data_16

    goto/16 :goto_0

    .line 2676148
    :sswitch_2f
    const/16 v0, 0x23

    if-ge v0, v3, :cond_0

    const/16 v0, 0x23

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676149
    const/16 v2, 0x24

    :sswitch_30
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676150
    invoke-static/range {v0 .. v5}, LX/JHc;->v(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676151
    :cond_3d
    const-string v3, "com.facebook.socialgood.ui.FundraiserCreationCharitySearchActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676152
    :sswitch_31
    const/16 v0, 0x15

    const-string v1, "uggested_cover_photo"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676153
    const/16 v0, 0x29

    if-lt v0, v3, :cond_0

    .line 2676154
    const/16 v3, 0x11d

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676155
    :pswitch_34
    const/16 v0, 0xd

    const-string v1, "rrency_selector"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676156
    const/16 v0, 0x1c

    if-lt v0, v3, :cond_0

    .line 2676157
    const/16 v3, 0x123

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676158
    :sswitch_32
    const/16 v0, 0x18

    const-string v1, "earch"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676159
    const/16 v0, 0x1d

    if-lt v0, v3, :cond_0

    .line 2676160
    const/16 v3, 0x13b

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676161
    :pswitch_35
    if-ge v2, v3, :cond_0

    .line 2676162
    aget-char v0, p1, v2

    .line 2676163
    packed-switch v0, :pswitch_data_2e

    :pswitch_36
    goto/16 :goto_0

    .line 2676164
    :pswitch_37
    const-string v0, "odfriends/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676165
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    .line 2676166
    const/16 v0, 0xc

    aget-char v0, p1, v0

    .line 2676167
    sparse-switch v0, :sswitch_data_17

    goto/16 :goto_0

    .line 2676168
    :sswitch_33
    const/16 v0, 0xd

    const-string v1, "amera"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676169
    const/16 v0, 0x12

    if-lt v0, v3, :cond_0

    .line 2676170
    const-string v3, "com.facebook.goodfriends.camera.CameraActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676171
    :sswitch_34
    const/16 v0, 0xd

    const-string v1, "ux_flow"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676172
    const/16 v0, 0x14

    if-lt v0, v3, :cond_0

    .line 2676173
    const/16 v3, 0xf7

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676174
    :sswitch_35
    const/16 v0, 0xd

    const-string v1, "elect_audience"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676175
    const/16 v0, 0x1b

    if-lt v0, v3, :cond_0

    .line 2676176
    const/16 v3, 0xf6

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676177
    :pswitch_38
    if-ge v1, v3, :cond_0

    .line 2676178
    aget-char v0, p1, v1

    .line 2676179
    sparse-switch v0, :sswitch_data_18

    goto/16 :goto_0

    .line 2676180
    :sswitch_36
    const/4 v0, 0x3

    const-string v1, "ph_editor"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676181
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    .line 2676182
    const/16 v0, 0xc

    const/16 v2, 0xd

    aget-char v0, p1, v0

    .line 2676183
    sparse-switch v0, :sswitch_data_19

    goto/16 :goto_0

    .line 2676184
    :sswitch_37
    const/16 v0, 0xd

    if-ge v0, v3, :cond_0

    const/16 v0, 0xd

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676185
    const/16 v2, 0xe

    :sswitch_38
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676186
    invoke-static/range {v0 .. v5}, LX/JHc;->w(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676187
    :sswitch_39
    const/4 v0, 0x3

    const-string v1, "ups/gridtab"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676188
    const/16 v0, 0xe

    if-lt v0, v3, :cond_0

    .line 2676189
    const/16 v3, 0x42

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676190
    :pswitch_39
    if-ge v2, v3, :cond_0

    .line 2676191
    aget-char v0, p1, v2

    .line 2676192
    sparse-switch v0, :sswitch_data_1a

    goto/16 :goto_0

    .line 2676193
    :sswitch_3a
    const-string v0, "out"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676194
    const/4 v0, 0x5

    if-lt v0, v3, :cond_0

    .line 2676195
    const-string v3, "com.facebook.about.AboutActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676196
    :sswitch_3b
    const-string v0, "count"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676197
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676198
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676199
    packed-switch v0, :pswitch_data_2f

    :pswitch_3a
    goto/16 :goto_0

    .line 2676200
    :pswitch_3b
    const-string v0, "kit/confirmation_code"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676201
    const/16 v0, 0x1d

    if-ge v0, v3, :cond_0

    .line 2676202
    const/16 v0, 0x1d

    const/16 v2, 0x1e

    aget-char v0, p1, v0

    .line 2676203
    sparse-switch v0, :sswitch_data_1b

    goto/16 :goto_0

    .line 2676204
    :sswitch_3c
    const/16 v0, 0x1e

    if-ge v0, v3, :cond_0

    const/16 v0, 0x1e

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676205
    const/16 v2, 0x1f

    :sswitch_3d
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676206
    invoke-static/range {v0 .. v5}, LX/JHc;->y(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676207
    :pswitch_3c
    if-ge v8, v3, :cond_3e

    aget-char v0, p1, v8

    if-eq v0, v5, :cond_0

    .line 2676208
    :cond_3e
    const-string v0, "recovery"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676209
    const/16 v0, 0x10

    if-ge v0, v3, :cond_3f

    .line 2676210
    const/16 v0, 0x10

    const/16 v2, 0x11

    aget-char v0, p1, v0

    .line 2676211
    sparse-switch v0, :sswitch_data_1c

    goto/16 :goto_0

    .line 2676212
    :sswitch_3e
    const/16 v0, 0x11

    if-ge v0, v3, :cond_0

    const/16 v0, 0x11

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676213
    const/16 v2, 0x12

    :sswitch_3f
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676214
    invoke-static/range {v0 .. v5}, LX/JHc;->x(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676215
    :cond_3f
    const-string v3, "com.facebook.account.recovery.AccountRecoveryActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676216
    :sswitch_40
    const-string v0, "pinvitedialog/launch"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676217
    const/16 v0, 0x16

    if-ge v0, v3, :cond_0

    .line 2676218
    const/16 v0, 0x16

    const/16 v2, 0x17

    aget-char v0, p1, v0

    .line 2676219
    sparse-switch v0, :sswitch_data_1d

    goto/16 :goto_0

    .line 2676220
    :sswitch_41
    const/16 v0, 0x17

    if-ge v0, v3, :cond_0

    const/16 v0, 0x17

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676221
    const/16 v2, 0x18

    :sswitch_42
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676222
    invoke-static/range {v0 .. v5}, LX/JHc;->z(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676223
    :pswitch_3d
    if-ge v2, v3, :cond_0

    .line 2676224
    aget-char v0, p1, v2

    .line 2676225
    sparse-switch v0, :sswitch_data_1e

    goto/16 :goto_0

    .line 2676226
    :sswitch_43
    const-string v0, "ckground_location/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676227
    const/16 v0, 0x14

    if-ge v0, v3, :cond_0

    .line 2676228
    const/16 v0, 0x14

    aget-char v0, p1, v0

    .line 2676229
    packed-switch v0, :pswitch_data_30

    goto/16 :goto_0

    .line 2676230
    :pswitch_3e
    const/16 v0, 0x15

    const-string v1, "esurrection"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676231
    const/16 v0, 0x20

    if-ge v0, v3, :cond_0

    .line 2676232
    const/16 v0, 0x20

    const/16 v2, 0x21

    aget-char v0, p1, v0

    .line 2676233
    sparse-switch v0, :sswitch_data_1f

    goto/16 :goto_0

    .line 2676234
    :sswitch_44
    const/16 v0, 0x21

    if-ge v0, v3, :cond_0

    const/16 v0, 0x21

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676235
    const/16 v2, 0x22

    :sswitch_45
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676236
    invoke-static/range {v0 .. v5}, LX/JHc;->C(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676237
    :sswitch_46
    const-string v0, "am/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676238
    const/4 v0, 0x5

    if-ge v0, v3, :cond_0

    .line 2676239
    const/4 v0, 0x5

    aget-char v0, p1, v0

    .line 2676240
    packed-switch v0, :pswitch_data_31

    goto/16 :goto_0

    .line 2676241
    :pswitch_3f
    const/4 v0, 0x6

    const-string v1, "eceiver"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676242
    const/16 v0, 0xd

    if-ge v0, v3, :cond_40

    .line 2676243
    const/16 v0, 0xd

    const/16 v2, 0xe

    aget-char v0, p1, v0

    .line 2676244
    sparse-switch v0, :sswitch_data_20

    goto/16 :goto_0

    .line 2676245
    :sswitch_47
    const/16 v0, 0xe

    if-ge v0, v3, :cond_0

    const/16 v0, 0xe

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676246
    const/16 v2, 0xf

    :sswitch_48
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676247
    invoke-static/range {v0 .. v5}, LX/JHc;->A(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676248
    :cond_40
    const-string v3, "com.facebook.beam.receiver.BeamReceiverActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676249
    :pswitch_40
    const/4 v0, 0x6

    const-string v1, "ender"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676250
    const/16 v0, 0xb

    if-ge v0, v3, :cond_41

    .line 2676251
    const/16 v0, 0xb

    const/16 v2, 0xc

    aget-char v0, p1, v0

    .line 2676252
    sparse-switch v0, :sswitch_data_21

    goto/16 :goto_0

    .line 2676253
    :sswitch_49
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    const/16 v0, 0xc

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676254
    const/16 v2, 0xd

    :sswitch_4a
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676255
    invoke-static/range {v0 .. v5}, LX/JHc;->B(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676256
    :cond_41
    const-string v3, "com.facebook.beam.sender.activity.BeamSenderActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676257
    :pswitch_41
    const/16 v0, 0x15

    const-string v1, "ettings"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676258
    const/16 v0, 0x1c

    if-lt v0, v3, :cond_0

    .line 2676259
    const-string v3, "com.facebook.backgroundlocation.settings.BackgroundLocationSettingsActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676260
    :sswitch_4b
    const-string v0, "owser_lite_feedback"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676261
    const/16 v0, 0x15

    if-ge v0, v3, :cond_0

    .line 2676262
    const/16 v0, 0x15

    const/16 v2, 0x16

    aget-char v0, p1, v0

    .line 2676263
    sparse-switch v0, :sswitch_data_22

    goto/16 :goto_0

    .line 2676264
    :sswitch_4c
    const/16 v0, 0x16

    if-ge v0, v3, :cond_0

    const/16 v0, 0x16

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676265
    const/16 v2, 0x17

    :sswitch_4d
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676266
    invoke-static/range {v0 .. v5}, LX/JHc;->D(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676267
    :pswitch_42
    if-ge v2, v3, :cond_0

    .line 2676268
    aget-char v0, p1, v2

    .line 2676269
    sparse-switch v0, :sswitch_data_23

    goto/16 :goto_0

    .line 2676270
    :sswitch_4e
    const-string v0, "hayta"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676271
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676272
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676273
    sparse-switch v0, :sswitch_data_24

    goto/16 :goto_0

    .line 2676274
    :sswitch_4f
    if-ge v8, v3, :cond_0

    aget-char v0, p1, v8

    if-ne v0, v5, :cond_0

    .line 2676275
    const/16 v2, 0x9

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676276
    invoke-static/range {v0 .. v5}, LX/JHc;->L(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676277
    :sswitch_50
    if-ge v1, v3, :cond_0

    .line 2676278
    aget-char v0, p1, v1

    .line 2676279
    sparse-switch v0, :sswitch_data_25

    goto/16 :goto_0

    .line 2676280
    :sswitch_51
    const/4 v0, 0x3

    const-string v1, "uritycheckup"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676281
    const/16 v0, 0xf

    if-ge v0, v3, :cond_0

    .line 2676282
    const/16 v0, 0xf

    const/16 v2, 0x10

    aget-char v0, p1, v0

    .line 2676283
    sparse-switch v0, :sswitch_data_26

    goto/16 :goto_0

    .line 2676284
    :sswitch_52
    const/16 v0, 0x10

    if-ge v0, v3, :cond_0

    const/16 v0, 0x10

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676285
    const/16 v2, 0x11

    :sswitch_53
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676286
    invoke-static/range {v0 .. v5}, LX/JHc;->E(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676287
    :sswitch_54
    const/4 v0, 0x3

    const-string v1, "tings"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676288
    if-ge v8, v3, :cond_43

    .line 2676289
    aget-char v0, p1, v8

    .line 2676290
    packed-switch v0, :pswitch_data_32

    goto/16 :goto_0

    .line 2676291
    :pswitch_43
    const/16 v0, 0x9

    if-ge v0, v3, :cond_42

    const/16 v0, 0x9

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_0

    .line 2676292
    :cond_42
    const/16 v0, 0x9

    const-string v1, "autoplay"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676293
    const/16 v0, 0x11

    if-lt v0, v3, :cond_0

    .line 2676294
    const-string v3, "com.facebook.video.settings.VideoAutoPlaySettingsActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676295
    :cond_43
    const-string v3, "com.facebook.katana.settings.activity.SettingsActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676296
    :sswitch_55
    const-string v0, "acks_"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676297
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676298
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676299
    sparse-switch v0, :sswitch_data_27

    goto/16 :goto_0

    .line 2676300
    :sswitch_56
    const-string v0, "nbox"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676301
    const/16 v0, 0xc

    if-lt v0, v3, :cond_0

    .line 2676302
    const/16 v3, 0x113

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676303
    :sswitch_57
    const-string v0, "eply_thread/"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676304
    const/16 v8, 0x14

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->G(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676305
    :sswitch_58
    const-string v0, "rofile"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676306
    const/16 v0, 0xe

    if-lt v0, v3, :cond_0

    .line 2676307
    const-string v3, "com.facebook.audience.snacks.app.SnacksStoryActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676308
    :sswitch_59
    const-string v0, "haresheet"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676309
    const/16 v0, 0x11

    if-lt v0, v3, :cond_0

    .line 2676310
    const/16 v3, 0x115

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676311
    :sswitch_5a
    const-string v0, "orts"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676312
    const/4 v0, 0x6

    if-ge v0, v3, :cond_0

    .line 2676313
    const/4 v0, 0x6

    aget-char v0, p1, v0

    .line 2676314
    sparse-switch v0, :sswitch_data_28

    goto/16 :goto_0

    .line 2676315
    :sswitch_5b
    const/4 v0, 0x7

    if-ge v0, v3, :cond_44

    const/4 v0, 0x7

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_0

    .line 2676316
    :cond_44
    const/4 v8, 0x7

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->H(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676317
    :sswitch_5c
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676318
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676319
    sparse-switch v0, :sswitch_data_29

    goto/16 :goto_0

    .line 2676320
    :sswitch_5d
    const-string v0, "ashboard"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676321
    const/16 v0, 0x10

    if-ge v0, v3, :cond_45

    .line 2676322
    const/16 v0, 0x10

    const/16 v2, 0x11

    aget-char v0, p1, v0

    .line 2676323
    sparse-switch v0, :sswitch_data_2a

    goto/16 :goto_0

    .line 2676324
    :sswitch_5e
    const/16 v0, 0x11

    if-ge v0, v3, :cond_0

    const/16 v0, 0x11

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676325
    const/16 v2, 0x12

    :sswitch_5f
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676326
    invoke-static/range {v0 .. v5}, LX/JHc;->J(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676327
    :sswitch_60
    const-string v0, "eague/"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676328
    const/16 v8, 0xe

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->I(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676329
    :cond_45
    const/16 v3, 0xfd

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676330
    :sswitch_61
    const-string v0, "orygallerysurvey/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676331
    const/16 v8, 0x13

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->K(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676332
    :pswitch_44
    if-ge v2, v3, :cond_0

    .line 2676333
    aget-char v0, p1, v2

    .line 2676334
    packed-switch v0, :pswitch_data_33

    goto/16 :goto_0

    .line 2676335
    :pswitch_45
    if-ge v1, v3, :cond_0

    .line 2676336
    aget-char v0, p1, v1

    .line 2676337
    packed-switch v0, :pswitch_data_34

    :pswitch_46
    goto/16 :goto_0

    .line 2676338
    :pswitch_47
    const/4 v0, 0x3

    const-string v1, "tant"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676339
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676340
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676341
    sparse-switch v0, :sswitch_data_2b

    goto/16 :goto_0

    .line 2676342
    :sswitch_62
    const-string v0, "experiences"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676343
    const/16 v0, 0x13

    if-ge v0, v3, :cond_0

    .line 2676344
    const/16 v0, 0x13

    const/16 v2, 0x14

    aget-char v0, p1, v0

    .line 2676345
    sparse-switch v0, :sswitch_data_2c

    goto/16 :goto_0

    .line 2676346
    :sswitch_63
    const/16 v0, 0x14

    if-ge v0, v3, :cond_46

    const/16 v0, 0x14

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_47

    .line 2676347
    :cond_46
    const/16 v0, 0x14

    if-ge v0, v3, :cond_0

    .line 2676348
    const/16 v0, 0x14

    aget-char v0, p1, v0

    .line 2676349
    sparse-switch v0, :sswitch_data_2d

    goto/16 :goto_0

    .line 2676350
    :sswitch_64
    const/16 v0, 0x15

    const-string v1, "ome_screen"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676351
    const/16 v0, 0x1f

    if-ge v0, v3, :cond_0

    .line 2676352
    const/16 v0, 0x1f

    const/16 v2, 0x20

    aget-char v0, p1, v0

    .line 2676353
    sparse-switch v0, :sswitch_data_2e

    goto/16 :goto_0

    .line 2676354
    :sswitch_65
    const/16 v0, 0x20

    if-ge v0, v3, :cond_0

    const/16 v0, 0x20

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676355
    const/16 v2, 0x21

    :sswitch_66
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676356
    invoke-static/range {v0 .. v5}, LX/JHc;->M(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676357
    :sswitch_67
    const/16 v0, 0x15

    if-ge v0, v3, :cond_0

    .line 2676358
    const/16 v0, 0x15

    aget-char v0, p1, v0

    .line 2676359
    sparse-switch v0, :sswitch_data_2f

    goto/16 :goto_0

    .line 2676360
    :sswitch_68
    const/16 v0, 0x16

    const-string v1, "fers"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676361
    const/16 v0, 0x1a

    if-ge v0, v3, :cond_0

    .line 2676362
    const/16 v0, 0x1a

    const/16 v2, 0x1b

    aget-char v0, p1, v0

    .line 2676363
    sparse-switch v0, :sswitch_data_30

    goto/16 :goto_0

    .line 2676364
    :sswitch_69
    const/16 v0, 0x1b

    if-ge v0, v3, :cond_0

    const/16 v0, 0x1b

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676365
    const/16 v2, 0x1c

    :sswitch_6a
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676366
    invoke-static/range {v0 .. v5}, LX/JHc;->O(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676367
    :sswitch_6b
    const/16 v0, 0x16

    const-string v1, "ganic_share"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676368
    const/16 v0, 0x21

    if-ge v0, v3, :cond_0

    .line 2676369
    const/16 v0, 0x21

    const/16 v2, 0x22

    aget-char v0, p1, v0

    .line 2676370
    sparse-switch v0, :sswitch_data_31

    goto/16 :goto_0

    .line 2676371
    :sswitch_6c
    const/16 v0, 0x22

    if-ge v0, v3, :cond_0

    const/16 v0, 0x22

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676372
    const/16 v2, 0x23

    :sswitch_6d
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676373
    invoke-static/range {v0 .. v5}, LX/JHc;->N(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676374
    :sswitch_6e
    const/16 v0, 0x15

    const-string v1, "age_cta"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676375
    const/16 v0, 0x1c

    if-ge v0, v3, :cond_0

    .line 2676376
    const/16 v0, 0x1c

    const/16 v2, 0x1d

    aget-char v0, p1, v0

    .line 2676377
    sparse-switch v0, :sswitch_data_32

    goto/16 :goto_0

    .line 2676378
    :sswitch_6f
    const/16 v0, 0x1d

    if-ge v0, v3, :cond_0

    const/16 v0, 0x1d

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676379
    const/16 v2, 0x1e

    :sswitch_70
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676380
    invoke-static/range {v0 .. v5}, LX/JHc;->P(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676381
    :cond_47
    const/16 v2, 0x15

    :sswitch_71
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676382
    invoke-static/range {v0 .. v5}, LX/JHc;->Q(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676383
    :sswitch_72
    const-string v0, "ames"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676384
    const/16 v0, 0xc

    if-ge v0, v3, :cond_0

    .line 2676385
    const/16 v0, 0xc

    const/16 v2, 0xd

    aget-char v0, p1, v0

    .line 2676386
    sparse-switch v0, :sswitch_data_33

    goto/16 :goto_0

    .line 2676387
    :sswitch_73
    const/16 v0, 0xd

    if-ge v0, v3, :cond_0

    const/16 v0, 0xd

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676388
    const/16 v2, 0xe

    :sswitch_74
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676389
    invoke-static/range {v0 .. v5}, LX/JHc;->R(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676390
    :pswitch_48
    const/4 v0, 0x3

    const-string v1, "ite_coworker"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676391
    const/16 v0, 0xf

    if-ge v0, v3, :cond_48

    .line 2676392
    const/16 v0, 0xf

    const/16 v2, 0x10

    aget-char v0, p1, v0

    .line 2676393
    sparse-switch v0, :sswitch_data_34

    goto/16 :goto_0

    .line 2676394
    :sswitch_75
    const/16 v0, 0x10

    if-ge v0, v3, :cond_0

    const/16 v0, 0x10

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676395
    const/16 v2, 0x11

    :sswitch_76
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676396
    invoke-static/range {v0 .. v5}, LX/JHc;->S(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676397
    :cond_48
    const-string v3, "com.facebook.work.invitecoworker.InviteCoworkerActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676398
    :pswitch_49
    if-ge v2, v3, :cond_0

    .line 2676399
    aget-char v0, p1, v2

    .line 2676400
    sparse-switch v0, :sswitch_data_35

    goto/16 :goto_0

    .line 2676401
    :sswitch_77
    const-string v0, "nguage_switch"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676402
    const/16 v0, 0xf

    if-lt v0, v3, :cond_0

    .line 2676403
    const-string v3, "com.facebook.languages.switcher.activity.LanguageSwitcherBookmarksActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676404
    :sswitch_78
    if-ge v1, v3, :cond_0

    .line 2676405
    aget-char v0, p1, v1

    .line 2676406
    sparse-switch v0, :sswitch_data_36

    goto/16 :goto_0

    .line 2676407
    :sswitch_79
    const/4 v0, 0x3

    const-string v1, "d_gen"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676408
    if-ge v8, v3, :cond_0

    .line 2676409
    const/16 v2, 0x9

    aget-char v0, p1, v8

    .line 2676410
    sparse-switch v0, :sswitch_data_37

    goto/16 :goto_0

    .line 2676411
    :sswitch_7a
    const/16 v0, 0x9

    if-ge v0, v3, :cond_0

    const/16 v0, 0x9

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676412
    const/16 v2, 0xa

    :sswitch_7b
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676413
    invoke-static/range {v0 .. v5}, LX/JHc;->U(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676414
    :sswitch_7c
    const/4 v0, 0x3

    const-string v1, "acy_contact/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676415
    const/16 v8, 0xf

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->T(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676416
    :sswitch_7d
    const-string v0, "ve_map"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676417
    if-ge v8, v3, :cond_49

    .line 2676418
    const/16 v2, 0x9

    aget-char v0, p1, v8

    .line 2676419
    sparse-switch v0, :sswitch_data_38

    goto/16 :goto_0

    .line 2676420
    :sswitch_7e
    const/16 v0, 0x9

    if-ge v0, v3, :cond_0

    const/16 v0, 0x9

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676421
    const/16 v2, 0xa

    :sswitch_7f
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676422
    invoke-static/range {v0 .. v5}, LX/JHc;->V(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676423
    :cond_49
    const/16 v3, 0xff

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676424
    :sswitch_80
    const-string v0, "ca"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676425
    const/4 v0, 0x4

    if-ge v0, v3, :cond_0

    .line 2676426
    const/4 v0, 0x4

    aget-char v0, p1, v0

    .line 2676427
    sparse-switch v0, :sswitch_data_39

    goto/16 :goto_0

    .line 2676428
    :sswitch_81
    const/4 v0, 0x5

    const-string v1, "_surface"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676429
    const/16 v0, 0xd

    if-lt v0, v3, :cond_0

    .line 2676430
    const-string v3, "com.facebook.local.surface.LocalSurfaceFragmentActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676431
    :sswitch_82
    const/4 v0, 0x5

    const-string v1, "ion_settings"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676432
    const/16 v0, 0x11

    if-lt v0, v3, :cond_0

    .line 2676433
    const/16 v3, 0x1e

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676434
    :pswitch_4a
    if-ge v2, v3, :cond_0

    .line 2676435
    aget-char v0, p1, v2

    .line 2676436
    packed-switch v0, :pswitch_data_35

    :pswitch_4b
    goto/16 :goto_0

    .line 2676437
    :pswitch_4c
    const-string v0, "pic/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676438
    const/4 v8, 0x6

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->W(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676439
    :pswitch_4d
    const-string v0, "ansliteration"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676440
    const/16 v0, 0xf

    if-ge v0, v3, :cond_0

    .line 2676441
    const/16 v0, 0xf

    const/16 v2, 0x10

    aget-char v0, p1, v0

    .line 2676442
    sparse-switch v0, :sswitch_data_3a

    goto/16 :goto_0

    .line 2676443
    :sswitch_83
    const/16 v0, 0x10

    if-ge v0, v3, :cond_0

    const/16 v0, 0x10

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676444
    const/16 v2, 0x11

    :sswitch_84
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676445
    invoke-static/range {v0 .. v5}, LX/JHc;->X(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676446
    :pswitch_4e
    if-ge v2, v3, :cond_0

    .line 2676447
    aget-char v0, p1, v2

    .line 2676448
    sparse-switch v0, :sswitch_data_3b

    goto/16 :goto_0

    .line 2676449
    :sswitch_85
    if-ge v1, v3, :cond_0

    .line 2676450
    aget-char v0, p1, v1

    .line 2676451
    sparse-switch v0, :sswitch_data_3c

    goto/16 :goto_0

    .line 2676452
    :sswitch_86
    const/4 v0, 0x3

    const-string v1, "egenerator"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676453
    const/16 v0, 0xd

    if-lt v0, v3, :cond_0

    .line 2676454
    const-string v3, "com.facebook.katana.activity.codegenerator.ui.CodeGeneratorActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676455
    :sswitch_87
    const-string v0, "rated_collection/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676456
    const/16 v8, 0x13

    move-object v5, p0

    move-object v6, p1

    move v7, v3

    move-object v9, v4

    move-object v10, p2

    invoke-static/range {v5 .. v10}, LX/JHc;->Y(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676457
    :sswitch_88
    const/4 v0, 0x3

    const-string v1, "erphoto"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676458
    const/16 v0, 0xa

    if-lt v0, v3, :cond_0

    .line 2676459
    const/16 v3, 0x77

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676460
    :pswitch_4f
    if-ge v2, v3, :cond_0

    .line 2676461
    aget-char v0, p1, v2

    .line 2676462
    sparse-switch v0, :sswitch_data_3d

    goto/16 :goto_0

    .line 2676463
    :sswitch_89
    const-string v0, "tive_template_shell"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676464
    const/16 v0, 0x15

    if-ge v0, v3, :cond_4a

    .line 2676465
    const/16 v0, 0x15

    const/16 v2, 0x16

    aget-char v0, p1, v0

    .line 2676466
    sparse-switch v0, :sswitch_data_3e

    goto/16 :goto_0

    .line 2676467
    :sswitch_8a
    const/16 v0, 0x16

    if-ge v0, v3, :cond_0

    const/16 v0, 0x16

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676468
    const/16 v2, 0x17

    :sswitch_8b
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676469
    invoke-static/range {v0 .. v5}, LX/JHc;->Z(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676470
    :cond_4a
    const/16 v3, 0x12e

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676471
    :sswitch_8c
    const-string v0, "wContactPointFaceweb"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676472
    const/16 v0, 0x16

    if-lt v0, v3, :cond_0

    .line 2676473
    const-string v3, "com.facebook.katana.activity.contactpoints.AddContactpointFacewebActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676474
    :pswitch_50
    if-ge v2, v3, :cond_0

    .line 2676475
    aget-char v0, p1, v2

    .line 2676476
    sparse-switch v0, :sswitch_data_3f

    goto/16 :goto_0

    .line 2676477
    :sswitch_8d
    if-ge v1, v3, :cond_0

    .line 2676478
    aget-char v0, p1, v1

    .line 2676479
    sparse-switch v0, :sswitch_data_40

    goto/16 :goto_0

    .line 2676480
    :sswitch_8e
    const/4 v0, 0x3

    const-string v1, "ly_dialogue_weather_permalink"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676481
    const/16 v0, 0x20

    if-lt v0, v3, :cond_0

    .line 2676482
    const/16 v3, 0x13f

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676483
    :sswitch_8f
    const-string v0, "l_login_activity"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676484
    const/16 v0, 0x12

    if-lt v0, v3, :cond_0

    .line 2676485
    const-string v3, "com.facebook.katana.dbl.activity.DeviceBasedLoginActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676486
    :sswitch_90
    const/4 v0, 0x3

    const-string v1, "a_savings_upsell"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676487
    const/16 v0, 0x13

    if-ge v0, v3, :cond_4b

    .line 2676488
    const/16 v0, 0x13

    const/16 v2, 0x14

    aget-char v0, p1, v0

    .line 2676489
    sparse-switch v0, :sswitch_data_41

    goto/16 :goto_0

    .line 2676490
    :sswitch_91
    const/16 v0, 0x14

    if-ge v0, v3, :cond_0

    const/16 v0, 0x14

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676491
    const/16 v2, 0x15

    :sswitch_92
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676492
    invoke-static/range {v0 .. v5}, LX/JHc;->aa(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676493
    :cond_4b
    const-string v3, "com.facebook.katana.activity.FbMainTabActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676494
    :sswitch_93
    const-string v0, "nate_"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676495
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676496
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676497
    sparse-switch v0, :sswitch_data_42

    goto/16 :goto_0

    .line 2676498
    :sswitch_94
    const-string v0, "reate"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676499
    const/16 v0, 0xd

    if-ge v0, v3, :cond_4c

    .line 2676500
    const/16 v0, 0xd

    const/16 v2, 0xe

    aget-char v0, p1, v0

    .line 2676501
    sparse-switch v0, :sswitch_data_43

    goto/16 :goto_0

    .line 2676502
    :sswitch_95
    const/16 v0, 0xe

    if-ge v0, v3, :cond_0

    const/16 v0, 0xe

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676503
    const/16 v2, 0xf

    :sswitch_96
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676504
    invoke-static/range {v0 .. v5}, LX/JHc;->ab(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676505
    :cond_4c
    const/16 v3, 0x117

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676506
    :sswitch_97
    if-ge v8, v3, :cond_0

    .line 2676507
    aget-char v0, p1, v8

    .line 2676508
    sparse-switch v0, :sswitch_data_44

    goto/16 :goto_0

    .line 2676509
    :sswitch_98
    const/16 v0, 0x9

    const-string v1, "ank_you"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676510
    const/16 v0, 0x10

    if-ge v0, v3, :cond_0

    .line 2676511
    const/16 v0, 0x10

    const/16 v2, 0x11

    aget-char v0, p1, v0

    .line 2676512
    sparse-switch v0, :sswitch_data_45

    goto/16 :goto_0

    .line 2676513
    :sswitch_99
    const/16 v0, 0x11

    if-ge v0, v3, :cond_0

    const/16 v0, 0x11

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676514
    const/16 v2, 0x12

    :sswitch_9a
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676515
    invoke-static/range {v0 .. v5}, LX/JHc;->ad(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676516
    :sswitch_9b
    const/16 v0, 0x9

    const-string v1, "igger"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676517
    const/16 v0, 0xe

    if-ge v0, v3, :cond_0

    .line 2676518
    const/16 v0, 0xe

    const/16 v2, 0xf

    aget-char v0, p1, v0

    .line 2676519
    sparse-switch v0, :sswitch_data_46

    goto/16 :goto_0

    .line 2676520
    :sswitch_9c
    const/16 v0, 0xf

    if-ge v0, v3, :cond_0

    const/16 v0, 0xf

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676521
    const/16 v2, 0x10

    :sswitch_9d
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676522
    invoke-static/range {v0 .. v5}, LX/JHc;->ac(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676523
    :sswitch_9e
    const-string v0, "ingle_click_invite"

    invoke-static {p1, v8, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676524
    const/16 v0, 0x1a

    if-ge v0, v3, :cond_0

    .line 2676525
    const/16 v0, 0x1a

    const/16 v2, 0x1b

    aget-char v0, p1, v0

    .line 2676526
    sparse-switch v0, :sswitch_data_47

    goto/16 :goto_0

    .line 2676527
    :sswitch_9f
    const/16 v0, 0x1b

    if-ge v0, v3, :cond_0

    const/16 v0, 0x1b

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676528
    const/16 v2, 0x1c

    :sswitch_a0
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676529
    invoke-static/range {v0 .. v5}, LX/JHc;->ae(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676530
    :pswitch_51
    if-ge v2, v3, :cond_0

    .line 2676531
    aget-char v0, p1, v2

    .line 2676532
    sparse-switch v0, :sswitch_data_48

    goto/16 :goto_0

    .line 2676533
    :sswitch_a1
    const-string v0, "termark"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676534
    const/16 v0, 0x9

    if-ge v0, v3, :cond_0

    .line 2676535
    const/16 v0, 0x9

    const/16 v2, 0xa

    aget-char v0, p1, v0

    .line 2676536
    sparse-switch v0, :sswitch_data_49

    goto/16 :goto_0

    .line 2676537
    :sswitch_a2
    const/16 v0, 0xa

    if-ge v0, v3, :cond_0

    const/16 v0, 0xa

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676538
    const/16 v2, 0xb

    :sswitch_a3
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676539
    invoke-static/range {v0 .. v5}, LX/JHc;->ag(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676540
    :sswitch_a4
    const-string v0, "rk"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676541
    const/4 v0, 0x4

    if-ge v0, v3, :cond_0

    .line 2676542
    const/4 v0, 0x4

    aget-char v0, p1, v0

    .line 2676543
    sparse-switch v0, :sswitch_data_4a

    goto/16 :goto_0

    .line 2676544
    :sswitch_a5
    const/4 v0, 0x5

    const-string v1, "groups_tab"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676545
    const/16 v0, 0xf

    if-lt v0, v3, :cond_0

    .line 2676546
    const/16 v3, 0x107

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676547
    :sswitch_a6
    const/4 v0, 0x5

    const-string v1, "lace_"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676548
    const/16 v0, 0xa

    if-ge v0, v3, :cond_0

    .line 2676549
    const/16 v0, 0xa

    aget-char v0, p1, v0

    .line 2676550
    sparse-switch v0, :sswitch_data_4b

    goto/16 :goto_0

    .line 2676551
    :sswitch_a7
    const/16 v0, 0xb

    const-string v1, "ysj"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676552
    const/16 v0, 0xe

    if-lt v0, v3, :cond_0

    .line 2676553
    const-string v3, "com.facebook.work.groupsuggestionslink.GroupSuggestionsLinkActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676554
    :sswitch_a8
    const/16 v0, 0xb

    const-string v1, "ysf"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676555
    const/16 v0, 0xe

    if-ge v0, v3, :cond_4d

    .line 2676556
    const/16 v0, 0xe

    const/16 v2, 0xf

    aget-char v0, p1, v0

    .line 2676557
    sparse-switch v0, :sswitch_data_4c

    goto/16 :goto_0

    .line 2676558
    :sswitch_a9
    const/16 v0, 0xf

    if-ge v0, v3, :cond_0

    const/16 v0, 0xf

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_0

    .line 2676559
    const/16 v2, 0x10

    :sswitch_aa
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676560
    invoke-static/range {v0 .. v5}, LX/JHc;->af(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676561
    :cond_4d
    const-string v3, "com.facebook.work.pysf.bookmark.PeopleYouShouldFollowActivity"

    move-object v2, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676562
    :pswitch_52
    const-string v0, "ideo_home"

    invoke-static {p1, v2, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676563
    const/16 v0, 0xa

    if-lt v0, v3, :cond_0

    .line 2676564
    const/16 v3, 0xc5

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, p2

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    .line 2676565
    :pswitch_53
    const-string v0, "eviews"

    invoke-static {p1, v2, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676566
    const/4 v0, 0x7

    if-ge v0, v3, :cond_0

    .line 2676567
    const/4 v0, 0x7

    aget-char v0, p1, v0

    .line 2676568
    sparse-switch v0, :sswitch_data_4d

    goto/16 :goto_0

    .line 2676569
    :sswitch_ab
    if-ge v8, v3, :cond_0

    aget-char v0, p1, v8

    if-ne v0, v5, :cond_0

    .line 2676570
    const/16 v2, 0x9

    :goto_3
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2676571
    invoke-static/range {v0 .. v5}, LX/JHc;->ah(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto/16 :goto_0

    :sswitch_ac
    move v2, v8

    goto :goto_3

    :sswitch_ad
    move v2, v8

    goto/16 :goto_2

    :sswitch_ae
    move v2, v8

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_39
        :pswitch_3d
        :pswitch_4e
        :pswitch_50
        :pswitch_2e
        :pswitch_2f
        :pswitch_35
        :pswitch_0
        :pswitch_44
        :pswitch_0
        :pswitch_0
        :pswitch_49
        :pswitch_0
        :pswitch_4f
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_53
        :pswitch_42
        :pswitch_4a
        :pswitch_0
        :pswitch_52
        :pswitch_51
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2f -> :sswitch_0
        0x69 -> :sswitch_c
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x62 -> :sswitch_1
        0x64 -> :sswitch_4
        0x77 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x2f -> :sswitch_2
        0x3f -> :sswitch_3
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x2f -> :sswitch_5
        0x3f -> :sswitch_8
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x2f -> :sswitch_6
        0x3f -> :sswitch_7
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x2f -> :sswitch_a
        0x3f -> :sswitch_b
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x2f -> :sswitch_d
        0x3f -> :sswitch_e
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x61 -> :sswitch_f
        0x6c -> :sswitch_1f
        0x72 -> :sswitch_14
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0x67 -> :sswitch_10
        0x79 -> :sswitch_13
    .end sparse-switch

    :sswitch_data_9
    .sparse-switch
        0x64 -> :sswitch_11
        0x76 -> :sswitch_12
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x65
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x70
        :pswitch_5
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x6c
        :pswitch_6
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x69
        :pswitch_7
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x6e
        :pswitch_8
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x6b
        :pswitch_9
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x2f
        :pswitch_a
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x6f
        :pswitch_b
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x69
        :pswitch_c
    .end packed-switch

    :pswitch_data_b
    .packed-switch 0x63
        :pswitch_d
    .end packed-switch

    :pswitch_data_c
    .packed-switch 0x65
        :pswitch_e
    .end packed-switch

    :pswitch_data_d
    .packed-switch 0x5f
        :pswitch_f
    .end packed-switch

    :pswitch_data_e
    .packed-switch 0x73
        :pswitch_10
    .end packed-switch

    :pswitch_data_f
    .packed-switch 0x77
        :pswitch_11
    .end packed-switch

    :pswitch_data_10
    .packed-switch 0x69
        :pswitch_12
    .end packed-switch

    :pswitch_data_11
    .packed-switch 0x74
        :pswitch_13
    .end packed-switch

    :pswitch_data_12
    .packed-switch 0x63
        :pswitch_14
    .end packed-switch

    :pswitch_data_13
    .packed-switch 0x68
        :pswitch_15
    .end packed-switch

    :pswitch_data_14
    .packed-switch 0x65
        :pswitch_16
    .end packed-switch

    :pswitch_data_15
    .packed-switch 0x72
        :pswitch_17
    .end packed-switch

    :sswitch_data_a
    .sparse-switch
        0x65 -> :sswitch_15
        0x69 -> :sswitch_16
    .end sparse-switch

    :sswitch_data_b
    .sparse-switch
        0x2f -> :sswitch_17
        0x3f -> :sswitch_ae
        0x5f -> :sswitch_1b
        0x64 -> :sswitch_1c
    .end sparse-switch

    :sswitch_data_c
    .sparse-switch
        0x61 -> :sswitch_18
        0x6c -> :sswitch_1a
        0x72 -> :sswitch_19
    .end sparse-switch

    :pswitch_data_16
    .packed-switch 0x62
        :pswitch_18
    .end packed-switch

    :pswitch_data_17
    .packed-switch 0x6f
        :pswitch_19
    .end packed-switch

    :pswitch_data_18
    .packed-switch 0x75
        :pswitch_1a
    .end packed-switch

    :pswitch_data_19
    .packed-switch 0x74
        :pswitch_1b
    .end packed-switch

    :pswitch_data_1a
    .packed-switch 0x65
        :pswitch_1c
    .end packed-switch

    :pswitch_data_1b
    .packed-switch 0x66
        :pswitch_1d
    .end packed-switch

    :pswitch_data_1c
    .packed-switch 0x72
        :pswitch_1e
    .end packed-switch

    :pswitch_data_1d
    .packed-switch 0x65
        :pswitch_1f
    .end packed-switch

    :pswitch_data_1e
    .packed-switch 0x73
        :pswitch_20
    .end packed-switch

    :pswitch_data_1f
    .packed-switch 0x68
        :pswitch_21
    .end packed-switch

    :pswitch_data_20
    .packed-switch 0x65
        :pswitch_22
    .end packed-switch

    :pswitch_data_21
    .packed-switch 0x72
        :pswitch_23
    .end packed-switch

    :pswitch_data_22
    .packed-switch 0x69
        :pswitch_24
    .end packed-switch

    :pswitch_data_23
    .packed-switch 0x66
        :pswitch_25
    .end packed-switch

    :pswitch_data_24
    .packed-switch 0x65
        :pswitch_26
    .end packed-switch

    :pswitch_data_25
    .packed-switch 0x5f
        :pswitch_27
    .end packed-switch

    :pswitch_data_26
    .packed-switch 0x65
        :pswitch_28
    .end packed-switch

    :pswitch_data_27
    .packed-switch 0x76
        :pswitch_29
    .end packed-switch

    :pswitch_data_28
    .packed-switch 0x65
        :pswitch_2a
    .end packed-switch

    :pswitch_data_29
    .packed-switch 0x6e
        :pswitch_2b
    .end packed-switch

    :pswitch_data_2a
    .packed-switch 0x74
        :pswitch_2c
    .end packed-switch

    :sswitch_data_d
    .sparse-switch
        0x2f -> :sswitch_1d
        0x3f -> :sswitch_1e
    .end sparse-switch

    :pswitch_data_2b
    .packed-switch 0x61
        :pswitch_2d
    .end packed-switch

    :sswitch_data_e
    .sparse-switch
        0x63 -> :sswitch_20
        0x74 -> :sswitch_21
    .end sparse-switch

    :sswitch_data_f
    .sparse-switch
        0x6c -> :sswitch_22
        0x76 -> :sswitch_25
    .end sparse-switch

    :sswitch_data_10
    .sparse-switch
        0x2f -> :sswitch_23
        0x3f -> :sswitch_24
    .end sparse-switch

    :sswitch_data_11
    .sparse-switch
        0x2f -> :sswitch_26
        0x5f -> :sswitch_27
    .end sparse-switch

    :sswitch_data_12
    .sparse-switch
        0x65 -> :sswitch_28
        0x72 -> :sswitch_29
        0x75 -> :sswitch_2c
    .end sparse-switch

    :sswitch_data_13
    .sparse-switch
        0x2f -> :sswitch_2a
        0x68 -> :sswitch_2b
    .end sparse-switch

    :pswitch_data_2c
    .packed-switch 0x62
        :pswitch_30
        :pswitch_31
    .end packed-switch

    :sswitch_data_14
    .sparse-switch
        0x6f -> :sswitch_2d
        0x73 -> :sswitch_32
    .end sparse-switch

    :pswitch_data_2d
    .packed-switch 0x72
        :pswitch_33
        :pswitch_32
        :pswitch_32
        :pswitch_34
    .end packed-switch

    :sswitch_data_15
    .sparse-switch
        0x63 -> :sswitch_2e
        0x73 -> :sswitch_31
    .end sparse-switch

    :sswitch_data_16
    .sparse-switch
        0x2f -> :sswitch_2f
        0x3f -> :sswitch_30
    .end sparse-switch

    :pswitch_data_2e
    .packed-switch 0x6f
        :pswitch_37
        :pswitch_36
        :pswitch_36
        :pswitch_38
    .end packed-switch

    :sswitch_data_17
    .sparse-switch
        0x63 -> :sswitch_33
        0x6e -> :sswitch_34
        0x73 -> :sswitch_35
    .end sparse-switch

    :sswitch_data_18
    .sparse-switch
        0x61 -> :sswitch_36
        0x6f -> :sswitch_39
    .end sparse-switch

    :sswitch_data_19
    .sparse-switch
        0x2f -> :sswitch_37
        0x3f -> :sswitch_38
    .end sparse-switch

    :sswitch_data_1a
    .sparse-switch
        0x62 -> :sswitch_3a
        0x63 -> :sswitch_3b
        0x70 -> :sswitch_40
    .end sparse-switch

    :pswitch_data_2f
    .packed-switch 0x2d
        :pswitch_3b
        :pswitch_3a
        :pswitch_3c
    .end packed-switch

    :sswitch_data_1b
    .sparse-switch
        0x2f -> :sswitch_3c
        0x3f -> :sswitch_3d
    .end sparse-switch

    :sswitch_data_1c
    .sparse-switch
        0x2f -> :sswitch_3e
        0x3f -> :sswitch_3f
    .end sparse-switch

    :sswitch_data_1d
    .sparse-switch
        0x2f -> :sswitch_41
        0x3f -> :sswitch_42
    .end sparse-switch

    :sswitch_data_1e
    .sparse-switch
        0x61 -> :sswitch_43
        0x65 -> :sswitch_46
        0x72 -> :sswitch_4b
    .end sparse-switch

    :pswitch_data_30
    .packed-switch 0x72
        :pswitch_3e
        :pswitch_41
    .end packed-switch

    :sswitch_data_1f
    .sparse-switch
        0x2f -> :sswitch_44
        0x3f -> :sswitch_45
    .end sparse-switch

    :pswitch_data_31
    .packed-switch 0x72
        :pswitch_3f
        :pswitch_40
    .end packed-switch

    :sswitch_data_20
    .sparse-switch
        0x2f -> :sswitch_47
        0x3f -> :sswitch_48
    .end sparse-switch

    :sswitch_data_21
    .sparse-switch
        0x2f -> :sswitch_49
        0x3f -> :sswitch_4a
    .end sparse-switch

    :sswitch_data_22
    .sparse-switch
        0x2f -> :sswitch_4c
        0x3f -> :sswitch_4d
    .end sparse-switch

    :sswitch_data_23
    .sparse-switch
        0x61 -> :sswitch_4e
        0x65 -> :sswitch_50
        0x6e -> :sswitch_55
        0x70 -> :sswitch_5a
        0x74 -> :sswitch_61
    .end sparse-switch

    :sswitch_data_24
    .sparse-switch
        0x2f -> :sswitch_4f
        0x3f -> :sswitch_ad
    .end sparse-switch

    :sswitch_data_25
    .sparse-switch
        0x63 -> :sswitch_51
        0x74 -> :sswitch_54
    .end sparse-switch

    :sswitch_data_26
    .sparse-switch
        0x2f -> :sswitch_52
        0x3f -> :sswitch_53
    .end sparse-switch

    :pswitch_data_32
    .packed-switch 0x2f
        :pswitch_43
    .end packed-switch

    :sswitch_data_27
    .sparse-switch
        0x69 -> :sswitch_56
        0x70 -> :sswitch_58
        0x72 -> :sswitch_57
        0x73 -> :sswitch_59
    .end sparse-switch

    :sswitch_data_28
    .sparse-switch
        0x2f -> :sswitch_5b
        0x5f -> :sswitch_5c
    .end sparse-switch

    :sswitch_data_29
    .sparse-switch
        0x64 -> :sswitch_5d
        0x6c -> :sswitch_60
    .end sparse-switch

    :sswitch_data_2a
    .sparse-switch
        0x2f -> :sswitch_5e
        0x3f -> :sswitch_5f
    .end sparse-switch

    :pswitch_data_33
    .packed-switch 0x6e
        :pswitch_45
    .end packed-switch

    :pswitch_data_34
    .packed-switch 0x73
        :pswitch_47
        :pswitch_46
        :pswitch_46
        :pswitch_48
    .end packed-switch

    :sswitch_data_2b
    .sparse-switch
        0x5f -> :sswitch_62
        0x67 -> :sswitch_72
    .end sparse-switch

    :sswitch_data_2c
    .sparse-switch
        0x2f -> :sswitch_63
        0x3f -> :sswitch_71
    .end sparse-switch

    :sswitch_data_2d
    .sparse-switch
        0x68 -> :sswitch_64
        0x6f -> :sswitch_67
        0x70 -> :sswitch_6e
    .end sparse-switch

    :sswitch_data_2e
    .sparse-switch
        0x2f -> :sswitch_65
        0x3f -> :sswitch_66
    .end sparse-switch

    :sswitch_data_2f
    .sparse-switch
        0x66 -> :sswitch_68
        0x72 -> :sswitch_6b
    .end sparse-switch

    :sswitch_data_30
    .sparse-switch
        0x2f -> :sswitch_69
        0x3f -> :sswitch_6a
    .end sparse-switch

    :sswitch_data_31
    .sparse-switch
        0x2f -> :sswitch_6c
        0x3f -> :sswitch_6d
    .end sparse-switch

    :sswitch_data_32
    .sparse-switch
        0x2f -> :sswitch_6f
        0x3f -> :sswitch_70
    .end sparse-switch

    :sswitch_data_33
    .sparse-switch
        0x2f -> :sswitch_73
        0x3f -> :sswitch_74
    .end sparse-switch

    :sswitch_data_34
    .sparse-switch
        0x2f -> :sswitch_75
        0x3f -> :sswitch_76
    .end sparse-switch

    :sswitch_data_35
    .sparse-switch
        0x61 -> :sswitch_77
        0x65 -> :sswitch_78
        0x69 -> :sswitch_7d
        0x6f -> :sswitch_80
    .end sparse-switch

    :sswitch_data_36
    .sparse-switch
        0x61 -> :sswitch_79
        0x67 -> :sswitch_7c
    .end sparse-switch

    :sswitch_data_37
    .sparse-switch
        0x2f -> :sswitch_7a
        0x3f -> :sswitch_7b
    .end sparse-switch

    :sswitch_data_38
    .sparse-switch
        0x2f -> :sswitch_7e
        0x3f -> :sswitch_7f
    .end sparse-switch

    :sswitch_data_39
    .sparse-switch
        0x6c -> :sswitch_81
        0x74 -> :sswitch_82
    .end sparse-switch

    :pswitch_data_35
    .packed-switch 0x6f
        :pswitch_4c
        :pswitch_4b
        :pswitch_4b
        :pswitch_4d
    .end packed-switch

    :sswitch_data_3a
    .sparse-switch
        0x2f -> :sswitch_83
        0x3f -> :sswitch_84
    .end sparse-switch

    :sswitch_data_3b
    .sparse-switch
        0x6f -> :sswitch_85
        0x75 -> :sswitch_87
    .end sparse-switch

    :sswitch_data_3c
    .sparse-switch
        0x64 -> :sswitch_86
        0x76 -> :sswitch_88
    .end sparse-switch

    :sswitch_data_3d
    .sparse-switch
        0x61 -> :sswitch_89
        0x65 -> :sswitch_8c
    .end sparse-switch

    :sswitch_data_3e
    .sparse-switch
        0x2f -> :sswitch_8a
        0x3f -> :sswitch_8b
    .end sparse-switch

    :sswitch_data_3f
    .sparse-switch
        0x61 -> :sswitch_8d
        0x62 -> :sswitch_8f
        0x6f -> :sswitch_93
    .end sparse-switch

    :sswitch_data_40
    .sparse-switch
        0x69 -> :sswitch_8e
        0x74 -> :sswitch_90
    .end sparse-switch

    :sswitch_data_41
    .sparse-switch
        0x2f -> :sswitch_91
        0x3f -> :sswitch_92
    .end sparse-switch

    :sswitch_data_42
    .sparse-switch
        0x63 -> :sswitch_94
        0x73 -> :sswitch_9e
        0x74 -> :sswitch_97
    .end sparse-switch

    :sswitch_data_43
    .sparse-switch
        0x2f -> :sswitch_95
        0x3f -> :sswitch_96
    .end sparse-switch

    :sswitch_data_44
    .sparse-switch
        0x68 -> :sswitch_98
        0x72 -> :sswitch_9b
    .end sparse-switch

    :sswitch_data_45
    .sparse-switch
        0x2f -> :sswitch_99
        0x3f -> :sswitch_9a
    .end sparse-switch

    :sswitch_data_46
    .sparse-switch
        0x2f -> :sswitch_9c
        0x3f -> :sswitch_9d
    .end sparse-switch

    :sswitch_data_47
    .sparse-switch
        0x2f -> :sswitch_9f
        0x3f -> :sswitch_a0
    .end sparse-switch

    :sswitch_data_48
    .sparse-switch
        0x61 -> :sswitch_a1
        0x6f -> :sswitch_a4
    .end sparse-switch

    :sswitch_data_49
    .sparse-switch
        0x2f -> :sswitch_a2
        0x3f -> :sswitch_a3
    .end sparse-switch

    :sswitch_data_4a
    .sparse-switch
        0x5f -> :sswitch_a5
        0x70 -> :sswitch_a6
    .end sparse-switch

    :sswitch_data_4b
    .sparse-switch
        0x67 -> :sswitch_a7
        0x70 -> :sswitch_a8
    .end sparse-switch

    :sswitch_data_4c
    .sparse-switch
        0x2f -> :sswitch_a9
        0x3f -> :sswitch_aa
    .end sparse-switch

    :sswitch_data_4d
    .sparse-switch
        0x2f -> :sswitch_ab
        0x3f -> :sswitch_ac
    .end sparse-switch
.end method

.method private static aa(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675768
    sub-int v7, p3, p2

    .line 2675769
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675770
    const/4 v4, 0x0

    .line 2675771
    const/4 v2, 0x0

    .line 2675772
    const/4 v1, 0x0

    .line 2675773
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675774
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2675775
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675776
    if-gez v0, :cond_0

    .line 2675777
    const/4 v0, 0x0

    .line 2675778
    :goto_1
    return-object v0

    .line 2675779
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675780
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675781
    if-lez v1, :cond_2

    .line 2675782
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675783
    add-int/lit8 v1, v1, 0x1

    .line 2675784
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2675785
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675786
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2675787
    goto :goto_0

    .line 2675788
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675789
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675790
    :pswitch_0
    const-string v10, "qp_h"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2675791
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675792
    if-nez v3, :cond_3

    .line 2675793
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675794
    :cond_3
    const-string v6, "qp"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675795
    goto :goto_0

    .line 2675796
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2675797
    goto :goto_0

    .line 2675798
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2675799
    const/4 v0, 0x0

    goto :goto_1

    .line 2675800
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2675801
    const/4 v0, 0x0

    goto :goto_1

    .line 2675802
    :cond_6
    const-string v1, "com.facebook.katana.activity.FbMainTabActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x350e48
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static ab(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675706
    sub-int v7, p3, p2

    .line 2675707
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675708
    const/4 v5, 0x0

    .line 2675709
    const/4 v2, 0x0

    .line 2675710
    const/4 v1, 0x0

    .line 2675711
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2675712
    :goto_0
    if-ge v1, v7, :cond_8

    .line 2675713
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675714
    if-gez v0, :cond_0

    .line 2675715
    const/4 v0, 0x0

    .line 2675716
    :goto_1
    return-object v0

    .line 2675717
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675718
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675719
    if-lez v1, :cond_2

    .line 2675720
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675721
    add-int/lit8 v1, v1, 0x1

    .line 2675722
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675723
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675724
    if-eqz v0, :cond_7

    move-object v5, v0

    .line 2675725
    goto :goto_0

    .line 2675726
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675727
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675728
    :sswitch_0
    const-string v10, "promotional_source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "fundraiser_charity_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "fundraiser_campaign_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    .line 2675729
    :pswitch_0
    or-int/lit8 v2, v6, 0x2

    .line 2675730
    if-nez v4, :cond_3

    .line 2675731
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675732
    :cond_3
    const-string v6, "promotional_source"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675733
    goto :goto_0

    .line 2675734
    :pswitch_1
    or-int/lit8 v2, v6, 0x4

    .line 2675735
    if-nez v4, :cond_4

    .line 2675736
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675737
    :cond_4
    const-string v6, "fundraiser_charity_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675738
    goto/16 :goto_0

    .line 2675739
    :pswitch_2
    or-int/lit8 v2, v6, 0x1

    .line 2675740
    if-nez v4, :cond_5

    .line 2675741
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675742
    :cond_5
    const-string v6, "source"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675743
    goto/16 :goto_0

    .line 2675744
    :pswitch_3
    or-int/lit8 v2, v6, 0x8

    .line 2675745
    if-nez v4, :cond_6

    .line 2675746
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675747
    :cond_6
    const-string v6, "fundraiser_campaign_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675748
    goto/16 :goto_0

    .line 2675749
    :cond_7
    const/4 v0, 0x1

    move v3, v0

    .line 2675750
    goto/16 :goto_0

    .line 2675751
    :cond_8
    packed-switch v6, :pswitch_data_1

    .line 2675752
    :pswitch_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675753
    :pswitch_5
    if-eqz v3, :cond_9

    .line 2675754
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675755
    :cond_9
    const/4 v1, 0x1

    const/16 v2, 0x117

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675756
    :pswitch_6
    if-eqz v3, :cond_a

    .line 2675757
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675758
    :cond_a
    const/4 v1, 0x1

    const/16 v2, 0x117

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675759
    :pswitch_7
    if-eqz v3, :cond_b

    .line 2675760
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675761
    :cond_b
    const/4 v1, 0x1

    const/16 v2, 0x117

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675762
    :pswitch_8
    if-eqz v3, :cond_c

    .line 2675763
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675764
    :cond_c
    const/4 v1, 0x1

    const/16 v2, 0x117

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675765
    :pswitch_9
    if-eqz v3, :cond_d

    .line 2675766
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675767
    :cond_d
    const/4 v1, 0x1

    const/16 v2, 0x117

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x356f97e5 -> :sswitch_2
        -0x2104b28a -> :sswitch_3
        -0x1fdb132a -> :sswitch_1
        0x1a2eed6c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_4
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_4
        :pswitch_8
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static ac(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675663
    sub-int v7, p3, p2

    .line 2675664
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675665
    const/4 v4, 0x0

    .line 2675666
    const/4 v2, 0x0

    .line 2675667
    const/4 v1, 0x0

    .line 2675668
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675669
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2675670
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675671
    if-gez v0, :cond_0

    .line 2675672
    const/4 v0, 0x0

    .line 2675673
    :goto_1
    return-object v0

    .line 2675674
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675675
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675676
    if-lez v1, :cond_2

    .line 2675677
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675678
    add-int/lit8 v1, v1, 0x1

    .line 2675679
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675680
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675681
    if-eqz v0, :cond_5

    move-object v4, v0

    .line 2675682
    goto :goto_0

    .line 2675683
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675684
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675685
    :sswitch_0
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2675686
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2675687
    if-nez v3, :cond_3

    .line 2675688
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675689
    :cond_3
    const-string v6, "id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675690
    goto :goto_0

    .line 2675691
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675692
    if-nez v3, :cond_4

    .line 2675693
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675694
    :cond_4
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675695
    goto :goto_0

    .line 2675696
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    .line 2675697
    goto :goto_0

    .line 2675698
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2675699
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2675700
    :pswitch_3
    if-eqz v5, :cond_7

    .line 2675701
    const/4 v0, 0x0

    goto :goto_1

    .line 2675702
    :cond_7
    const-string v1, "com.facebook.socialgood.triggers.FundraiserCuratedCharityPickerActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675703
    :pswitch_4
    if-eqz v5, :cond_8

    .line 2675704
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675705
    :cond_8
    const-string v1, "com.facebook.socialgood.triggers.FundraiserCuratedCharityPickerActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x356f97e5 -> :sswitch_1
        0xd1b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static ad(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675620
    sub-int v7, p3, p2

    .line 2675621
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675622
    const/4 v5, 0x0

    .line 2675623
    const/4 v2, 0x0

    .line 2675624
    const/4 v1, 0x0

    .line 2675625
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2675626
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2675627
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675628
    if-gez v0, :cond_0

    .line 2675629
    const/4 v0, 0x0

    .line 2675630
    :goto_1
    return-object v0

    .line 2675631
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675632
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675633
    if-lez v1, :cond_2

    .line 2675634
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675635
    add-int/lit8 v1, v1, 0x1

    .line 2675636
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675637
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675638
    if-eqz v0, :cond_5

    move-object v5, v0

    .line 2675639
    goto :goto_0

    .line 2675640
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675641
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675642
    :sswitch_0
    const-string v10, "overlay_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "fundraiser_campaign_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    .line 2675643
    :pswitch_0
    or-int/lit8 v2, v6, 0x2

    .line 2675644
    if-nez v4, :cond_3

    .line 2675645
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675646
    :cond_3
    const-string v6, "overlay_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675647
    goto :goto_0

    .line 2675648
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675649
    if-nez v4, :cond_4

    .line 2675650
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675651
    :cond_4
    const-string v6, "fundraiser_campaign_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675652
    goto :goto_0

    .line 2675653
    :cond_5
    const/4 v0, 0x1

    move v3, v0

    .line 2675654
    goto :goto_0

    .line 2675655
    :cond_6
    packed-switch v6, :pswitch_data_1

    .line 2675656
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2675657
    :pswitch_3
    if-eqz v3, :cond_7

    .line 2675658
    const/4 v0, 0x0

    goto :goto_1

    .line 2675659
    :cond_7
    const/4 v1, 0x1

    const/16 v2, 0x103

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2675660
    :pswitch_4
    if-eqz v3, :cond_8

    .line 2675661
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675662
    :cond_8
    const/4 v1, 0x1

    const/16 v2, 0x103

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7464f496 -> :sswitch_0
        -0x2104b28a -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private static ae(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675569
    sub-int v7, p3, p2

    .line 2675570
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675571
    const/4 v5, 0x0

    .line 2675572
    const/4 v2, 0x0

    .line 2675573
    const/4 v1, 0x0

    .line 2675574
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2675575
    :goto_0
    if-ge v1, v7, :cond_7

    .line 2675576
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675577
    if-gez v0, :cond_0

    .line 2675578
    const/4 v0, 0x0

    .line 2675579
    :goto_1
    return-object v0

    .line 2675580
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675581
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675582
    if-lez v1, :cond_2

    .line 2675583
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675584
    add-int/lit8 v1, v1, 0x1

    .line 2675585
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675586
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675587
    if-eqz v0, :cond_6

    move-object v5, v0

    .line 2675588
    goto :goto_0

    .line 2675589
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675590
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675591
    :sswitch_0
    const-string v10, "referral_source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "fundraiser_campaign_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    .line 2675592
    :pswitch_0
    or-int/lit8 v2, v6, 0x4

    .line 2675593
    if-nez v4, :cond_3

    .line 2675594
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675595
    :cond_3
    const-string v6, "referral_source"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675596
    goto :goto_0

    .line 2675597
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2675598
    if-nez v4, :cond_4

    .line 2675599
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675600
    :cond_4
    const-string v6, "source"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675601
    goto :goto_0

    .line 2675602
    :pswitch_2
    or-int/lit8 v2, v6, 0x1

    .line 2675603
    if-nez v4, :cond_5

    .line 2675604
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675605
    :cond_5
    const-string v6, "fundraiser_campaign_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675606
    goto/16 :goto_0

    .line 2675607
    :cond_6
    const/4 v0, 0x1

    move v3, v0

    .line 2675608
    goto/16 :goto_0

    .line 2675609
    :cond_7
    sparse-switch v6, :sswitch_data_1

    .line 2675610
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675611
    :sswitch_3
    if-eqz v3, :cond_8

    .line 2675612
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675613
    :cond_8
    const/4 v1, 0x1

    const/16 v2, 0x10a

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675614
    :sswitch_4
    if-eqz v3, :cond_9

    .line 2675615
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675616
    :cond_9
    const/4 v1, 0x1

    const/16 v2, 0x10a

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675617
    :sswitch_5
    if-eqz v3, :cond_a

    .line 2675618
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675619
    :cond_a
    const/4 v1, 0x1

    const/16 v2, 0x10a

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4b05f103 -> :sswitch_0
        -0x356f97e5 -> :sswitch_1
        -0x2104b28a -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_3
        0x3 -> :sswitch_4
        0x7 -> :sswitch_5
    .end sparse-switch
.end method

.method private static af(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675116
    sub-int v7, p3, p2

    .line 2675117
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675118
    const/4 v4, 0x0

    .line 2675119
    const/4 v2, 0x0

    .line 2675120
    const/4 v1, 0x0

    .line 2675121
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675122
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2675123
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675124
    if-gez v0, :cond_0

    .line 2675125
    const/4 v0, 0x0

    .line 2675126
    :goto_1
    return-object v0

    .line 2675127
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675128
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675129
    if-lez v1, :cond_2

    .line 2675130
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675131
    add-int/lit8 v1, v1, 0x1

    .line 2675132
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2675133
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675134
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2675135
    goto :goto_0

    .line 2675136
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675137
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675138
    :pswitch_0
    const-string v10, "_bt_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2675139
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2675140
    if-nez v3, :cond_3

    .line 2675141
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675142
    :cond_3
    const-string v6, "unused"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675143
    goto :goto_0

    .line 2675144
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2675145
    goto :goto_0

    .line 2675146
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2675147
    const/4 v0, 0x0

    goto :goto_1

    .line 2675148
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2675149
    const/4 v0, 0x0

    goto :goto_1

    .line 2675150
    :cond_6
    const-string v1, "com.facebook.work.pysf.bookmark.PeopleYouShouldFollowActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2cad8e
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static ag(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2675492
    sub-int v7, p3, p2

    .line 2675493
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2675494
    const/4 v4, 0x0

    .line 2675495
    const/4 v2, 0x0

    .line 2675496
    const/4 v1, 0x0

    .line 2675497
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2675498
    :goto_0
    if-ge v1, v7, :cond_a

    .line 2675499
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2675500
    if-gez v0, :cond_0

    .line 2675501
    const/4 v0, 0x0

    .line 2675502
    :goto_1
    return-object v0

    .line 2675503
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2675504
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2675505
    if-lez v1, :cond_2

    .line 2675506
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2675507
    add-int/lit8 v1, v1, 0x1

    .line 2675508
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2675509
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2675510
    if-eqz v0, :cond_9

    move-object v4, v0

    .line 2675511
    goto :goto_0

    .line 2675512
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2675513
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2675514
    :sswitch_0
    const-string v10, "photo_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "show_footer"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "qp_h"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "show_picker"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    .line 2675515
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2675516
    if-nez v3, :cond_3

    .line 2675517
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675518
    :cond_3
    const-string v6, "photo_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675519
    goto :goto_0

    .line 2675520
    :pswitch_1
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2675521
    if-eqz v2, :cond_5

    .line 2675522
    or-int/lit8 v0, v6, 0x4

    .line 2675523
    if-nez v3, :cond_4

    .line 2675524
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675525
    :cond_4
    const-string v6, "show_footer"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2675526
    goto/16 :goto_0

    .line 2675527
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675528
    :pswitch_2
    or-int/lit8 v2, v6, 0x8

    .line 2675529
    if-nez v3, :cond_6

    .line 2675530
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675531
    :cond_6
    const-string v6, "qp_h"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2675532
    goto/16 :goto_0

    .line 2675533
    :pswitch_3
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2675534
    if-eqz v2, :cond_8

    .line 2675535
    or-int/lit8 v0, v6, 0x2

    .line 2675536
    if-nez v3, :cond_7

    .line 2675537
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2675538
    :cond_7
    const-string v6, "show_picker"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2675539
    goto/16 :goto_0

    .line 2675540
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675541
    :cond_9
    const/4 v0, 0x1

    move v5, v0

    .line 2675542
    goto/16 :goto_0

    .line 2675543
    :cond_a
    packed-switch v6, :pswitch_data_1

    .line 2675544
    :pswitch_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675545
    :pswitch_5
    if-eqz v5, :cond_b

    .line 2675546
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675547
    :cond_b
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675548
    :pswitch_6
    if-eqz v5, :cond_c

    .line 2675549
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675550
    :cond_c
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675551
    :pswitch_7
    if-eqz v5, :cond_d

    .line 2675552
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675553
    :cond_d
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675554
    :pswitch_8
    if-eqz v5, :cond_e

    .line 2675555
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675556
    :cond_e
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675557
    :pswitch_9
    if-eqz v5, :cond_f

    .line 2675558
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675559
    :cond_f
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675560
    :pswitch_a
    if-eqz v5, :cond_10

    .line 2675561
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675562
    :cond_10
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675563
    :pswitch_b
    if-eqz v5, :cond_11

    .line 2675564
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675565
    :cond_11
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2675566
    :pswitch_c
    if-eqz v5, :cond_12

    .line 2675567
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2675568
    :cond_12
    const-string v1, "com.facebook.wem.watermark.WatermarkActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4bf3d1b8 -> :sswitch_0
        0x350e48 -> :sswitch_2
        0x9b8f11d -> :sswitch_1
        0x1a6f4410 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_a
        :pswitch_4
        :pswitch_b
        :pswitch_4
        :pswitch_9
        :pswitch_4
        :pswitch_6
        :pswitch_4
        :pswitch_c
        :pswitch_4
        :pswitch_7
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method

.method private static ah(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2673526
    sub-int v7, p3, p2

    .line 2673527
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2673528
    const/4 v5, 0x0

    .line 2673529
    const/4 v2, 0x0

    .line 2673530
    const/4 v1, 0x0

    .line 2673531
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2673532
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2673533
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673534
    if-gez v0, :cond_0

    .line 2673535
    const/4 v0, 0x0

    .line 2673536
    :goto_1
    return-object v0

    .line 2673537
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2673538
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673539
    if-lez v1, :cond_2

    .line 2673540
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673541
    add-int/lit8 v1, v1, 0x1

    .line 2673542
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2673543
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673544
    if-eqz v0, :cond_4

    move-object v5, v0

    .line 2673545
    goto :goto_0

    .line 2673546
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673547
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673548
    :pswitch_0
    const-string v10, "user_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2673549
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2673550
    if-nez v4, :cond_3

    .line 2673551
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2673552
    :cond_3
    const-string v6, "com.facebook.katana.profile.id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2673553
    goto :goto_0

    .line 2673554
    :cond_4
    const/4 v0, 0x1

    move v3, v0

    .line 2673555
    goto :goto_0

    .line 2673556
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2673557
    const/4 v0, 0x0

    goto :goto_1

    .line 2673558
    :pswitch_2
    if-eqz v3, :cond_6

    .line 2673559
    const/4 v0, 0x0

    goto :goto_1

    .line 2673560
    :cond_6
    const/4 v1, 0x1

    const/16 v2, 0x4b

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x8c511f1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static ai(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673896
    sub-int v6, p3, p2

    .line 2673897
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673898
    const/4 v4, 0x0

    .line 2673899
    const/4 v0, 0x0

    .line 2673900
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2673901
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_6

    .line 2673902
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673903
    if-gez v0, :cond_1

    .line 2673904
    const/4 v0, 0x0

    .line 2673905
    :goto_1
    return-object v0

    .line 2673906
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673907
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673908
    if-lez v1, :cond_3

    .line 2673909
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673910
    add-int/lit8 v1, v1, 0x1

    .line 2673911
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673912
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673913
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2673914
    goto :goto_0

    .line 2673915
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673916
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673917
    :sswitch_0
    const-string v9, "product_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "product_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    .line 2673918
    :pswitch_0
    or-int/lit8 v2, v5, 0x2

    .line 2673919
    if-nez v3, :cond_4

    .line 2673920
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673921
    :cond_4
    const-string v5, "product_type"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673922
    goto :goto_0

    .line 2673923
    :pswitch_1
    or-int/lit8 v2, v5, 0x1

    .line 2673924
    if-nez v3, :cond_5

    .line 2673925
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673926
    :cond_5
    const-string v5, "product_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673927
    goto :goto_0

    .line 2673928
    :cond_6
    packed-switch v5, :pswitch_data_1

    .line 2673929
    const/4 v0, 0x0

    goto :goto_1

    .line 2673930
    :pswitch_2
    const-string v1, "com.facebook.payments.receipt.PaymentsReceiptActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3c79388a -> :sswitch_0
        0x687cca6b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static aj(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673856
    sub-int v6, p3, p2

    .line 2673857
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673858
    const/4 v4, 0x0

    .line 2673859
    const/4 v0, 0x0

    .line 2673860
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2673861
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_7

    .line 2673862
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673863
    if-gez v0, :cond_1

    .line 2673864
    const/4 v0, 0x0

    .line 2673865
    :goto_1
    return-object v0

    .line 2673866
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673867
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673868
    if-lez v1, :cond_3

    .line 2673869
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673870
    add-int/lit8 v1, v1, 0x1

    .line 2673871
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673872
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673873
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2673874
    goto :goto_0

    .line 2673875
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673876
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673877
    :sswitch_0
    const-string v9, "product_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "product_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "seller_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    .line 2673878
    :pswitch_0
    or-int/lit8 v2, v5, 0x2

    .line 2673879
    if-nez v3, :cond_4

    .line 2673880
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673881
    :cond_4
    const-string v5, "product_type"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673882
    goto :goto_0

    .line 2673883
    :pswitch_1
    or-int/lit8 v2, v5, 0x1

    .line 2673884
    if-nez v3, :cond_5

    .line 2673885
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673886
    :cond_5
    const-string v5, "product_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673887
    goto :goto_0

    .line 2673888
    :pswitch_2
    or-int/lit8 v2, v5, 0x4

    .line 2673889
    if-nez v3, :cond_6

    .line 2673890
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673891
    :cond_6
    const-string v5, "seller_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673892
    goto/16 :goto_0

    .line 2673893
    :cond_7
    packed-switch v5, :pswitch_data_1

    .line 2673894
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673895
    :pswitch_3
    const-string v1, "com.facebook.payments.checkout.CheckoutActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5a3a8225 -> :sswitch_2
        0x3c79388a -> :sswitch_0
        0x687cca6b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_3
    .end packed-switch
.end method

.method private static ak(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2673839
    if-nez p4, :cond_5

    .line 2673840
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2673841
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2673842
    if-nez v0, :cond_0

    .line 2673843
    const/4 v0, 0x0

    .line 2673844
    :goto_1
    return-object v0

    .line 2673845
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2673846
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2673847
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2673848
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2673849
    :cond_2
    const-string v0, "id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2673850
    const-string v0, "/legacy_contact_intro"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2673851
    const/4 v0, 0x0

    goto :goto_1

    .line 2673852
    :cond_3
    add-int/lit8 v0, v1, 0x15

    .line 2673853
    if-lt v0, p2, :cond_4

    .line 2673854
    const-string v3, "com.facebook.timeline.legacycontact.RememberMemorializedActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2673855
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-object v5, p4

    goto :goto_0
.end method

.method private static al(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2673822
    if-nez p4, :cond_5

    .line 2673823
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2673824
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2673825
    if-nez v0, :cond_0

    .line 2673826
    const/4 v0, 0x0

    .line 2673827
    :goto_1
    return-object v0

    .line 2673828
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2673829
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2673830
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2673831
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2673832
    :cond_2
    const-string v0, "id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2673833
    const-string v0, "/legacy_contact_intro"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2673834
    const/4 v0, 0x0

    goto :goto_1

    .line 2673835
    :cond_3
    add-int/lit8 v0, v1, 0x15

    .line 2673836
    if-lt v0, p2, :cond_4

    .line 2673837
    const-string v3, "com.facebook.timeline.legacycontact.RememberMemorializedActivity"

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p0

    move-object v7, p5

    invoke-static/range {v2 .. v7}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2673838
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-object v5, p4

    goto :goto_0
.end method

.method private static am(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673787
    sub-int v6, p3, p2

    .line 2673788
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673789
    const/4 v4, 0x0

    .line 2673790
    const/4 v0, 0x0

    .line 2673791
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2673792
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_6

    .line 2673793
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673794
    if-gez v0, :cond_1

    .line 2673795
    const/4 v0, 0x0

    .line 2673796
    :goto_1
    return-object v0

    .line 2673797
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673798
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673799
    if-lez v1, :cond_3

    .line 2673800
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673801
    add-int/lit8 v1, v1, 0x1

    .line 2673802
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673803
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673804
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2673805
    goto :goto_0

    .line 2673806
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673807
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673808
    :sswitch_0
    const-string v9, "product_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "product_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    .line 2673809
    :pswitch_0
    or-int/lit8 v2, v5, 0x2

    .line 2673810
    if-nez v3, :cond_4

    .line 2673811
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673812
    :cond_4
    const-string v5, "product_type"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673813
    goto :goto_0

    .line 2673814
    :pswitch_1
    or-int/lit8 v2, v5, 0x1

    .line 2673815
    if-nez v3, :cond_5

    .line 2673816
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673817
    :cond_5
    const-string v5, "product_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673818
    goto :goto_0

    .line 2673819
    :cond_6
    packed-switch v5, :pswitch_data_1

    .line 2673820
    const/4 v0, 0x0

    goto :goto_1

    .line 2673821
    :pswitch_2
    const-string v1, "com.facebook.payments.receipt.PaymentsReceiptActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3c79388a -> :sswitch_0
        0x687cca6b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673757
    sub-int v6, p3, p2

    .line 2673758
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673759
    const/4 v5, 0x0

    .line 2673760
    const/4 v0, 0x0

    .line 2673761
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2673762
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_5

    .line 2673763
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673764
    if-gez v0, :cond_1

    .line 2673765
    const/4 v0, 0x0

    .line 2673766
    :goto_1
    return-object v0

    .line 2673767
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673768
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673769
    if-lez v1, :cond_3

    .line 2673770
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673771
    add-int/lit8 v1, v1, 0x1

    .line 2673772
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2673773
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673774
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2673775
    goto :goto_0

    .line 2673776
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673777
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673778
    :pswitch_0
    const-string v9, "coupon_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    .line 2673779
    :pswitch_1
    or-int/lit8 v2, v3, 0x1

    .line 2673780
    if-nez v4, :cond_4

    .line 2673781
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2673782
    :cond_4
    const-string v3, "coupon_id"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2673783
    goto :goto_0

    .line 2673784
    :cond_5
    packed-switch v3, :pswitch_data_2

    .line 2673785
    const/4 v0, 0x0

    goto :goto_1

    .line 2673786
    :pswitch_2
    const/4 v1, 0x1

    const/16 v2, 0xc1

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x53c0dfd4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/16 v6, 0x9

    const/16 v2, 0x12

    const/16 v0, 0x11

    const/4 v4, 0x0

    .line 2673738
    array-length v3, p1

    .line 2673739
    const/4 v1, 0x0

    const-string v5, "payments/"

    invoke-static {p1, v1, v5}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2673740
    :cond_0
    :goto_0
    return-object v4

    .line 2673741
    :cond_1
    if-ge v6, v3, :cond_0

    .line 2673742
    aget-char v1, p1, v6

    .line 2673743
    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 2673744
    :sswitch_0
    const-string v1, "heckout"

    invoke-static {p1, v7, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2673745
    if-ge v0, v3, :cond_0

    .line 2673746
    aget-char v0, p1, v0

    .line 2673747
    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    .line 2673748
    :sswitch_1
    if-ge v2, v3, :cond_0

    aget-char v0, p1, v2

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_0

    .line 2673749
    const/16 v2, 0x13

    :sswitch_2
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2673750
    invoke-static/range {v0 .. v5}, LX/JHc;->aj(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    .line 2673751
    :sswitch_3
    const-string v1, "eceipt"

    invoke-static {p1, v7, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2673752
    const/16 v1, 0x10

    if-ge v1, v3, :cond_0

    .line 2673753
    const/16 v1, 0x10

    aget-char v1, p1, v1

    .line 2673754
    sparse-switch v1, :sswitch_data_2

    goto :goto_0

    .line 2673755
    :sswitch_4
    if-ge v0, v3, :cond_0

    aget-char v0, p1, v0

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_0

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2673756
    invoke-static/range {v0 .. v5}, LX/JHc;->ai(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    :sswitch_5
    move v2, v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_0
        0x72 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2f -> :sswitch_1
        0x3f -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x2f -> :sswitch_4
        0x3f -> :sswitch_5
    .end sparse-switch
.end method

.method private static c(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673708
    sub-int v6, p3, p2

    .line 2673709
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673710
    const/4 v5, 0x0

    .line 2673711
    const/4 v0, 0x0

    .line 2673712
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2673713
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_5

    .line 2673714
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673715
    if-gez v0, :cond_1

    .line 2673716
    const/4 v0, 0x0

    .line 2673717
    :goto_1
    return-object v0

    .line 2673718
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673719
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673720
    if-lez v1, :cond_3

    .line 2673721
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673722
    add-int/lit8 v1, v1, 0x1

    .line 2673723
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2673724
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673725
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2673726
    goto :goto_0

    .line 2673727
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673728
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673729
    :pswitch_0
    const-string v9, "source"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    .line 2673730
    :pswitch_1
    or-int/lit8 v2, v3, 0x1

    .line 2673731
    if-nez v4, :cond_4

    .line 2673732
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2673733
    :cond_4
    const-string v3, "source"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2673734
    goto :goto_0

    .line 2673735
    :cond_5
    packed-switch v3, :pswitch_data_2

    .line 2673736
    const/4 v0, 0x0

    goto :goto_1

    .line 2673737
    :pswitch_2
    const/4 v1, 0x1

    const/16 v2, 0xc0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x356f97e5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2673705
    array-length v2, p1

    .line 2673706
    const/4 v0, 0x0

    const-string v1, "m.facebook.com/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2673707
    :goto_0
    return-object v4

    :cond_0
    const/16 v3, 0xf

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/JHc;->ak(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673670
    sub-int v6, p3, p2

    .line 2673671
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673672
    const/4 v5, 0x0

    .line 2673673
    const/4 v0, 0x0

    .line 2673674
    const/4 v1, 0x0

    move v3, v0

    move-object v4, p4

    .line 2673675
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_6

    .line 2673676
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673677
    if-gez v0, :cond_1

    .line 2673678
    const/4 v0, 0x0

    .line 2673679
    :goto_1
    return-object v0

    .line 2673680
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673681
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673682
    if-lez v1, :cond_3

    .line 2673683
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673684
    add-int/lit8 v1, v1, 0x1

    .line 2673685
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673686
    invoke-static {v5, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673687
    if-eqz v0, :cond_0

    move-object v5, v0

    .line 2673688
    goto :goto_0

    .line 2673689
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673690
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673691
    :sswitch_0
    const-string v9, "extra_image_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    .line 2673692
    :pswitch_0
    or-int/lit8 v2, v3, 0x1

    .line 2673693
    if-nez v4, :cond_4

    .line 2673694
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2673695
    :cond_4
    const-string v3, "extra_image_url"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2673696
    goto :goto_0

    .line 2673697
    :pswitch_1
    or-int/lit8 v2, v3, 0x2

    .line 2673698
    if-nez v4, :cond_5

    .line 2673699
    new-instance v4, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v4, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 2673700
    :cond_5
    const-string v3, "title"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 2673701
    goto :goto_0

    .line 2673702
    :cond_6
    packed-switch v3, :pswitch_data_1

    .line 2673703
    const/4 v0, 0x0

    goto :goto_1

    .line 2673704
    :pswitch_2
    const/4 v1, 0x1

    const/16 v2, 0x108

    const/4 v3, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4e67cce4 -> :sswitch_0
        0x6942258 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method private static d(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2673667
    array-length v2, p1

    .line 2673668
    const/4 v0, 0x0

    const-string v1, "m.facebook.com/"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2673669
    :goto_0
    return-object v4

    :cond_0
    const/16 v3, 0xf

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/JHc;->al(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0
.end method

.method private static e(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2673597
    sub-int v6, p3, p2

    .line 2673598
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2673599
    const/4 v4, 0x0

    .line 2673600
    const/4 v0, 0x0

    .line 2673601
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2673602
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_d

    .line 2673603
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673604
    if-gez v0, :cond_1

    .line 2673605
    const/4 v0, 0x0

    .line 2673606
    :goto_1
    return-object v0

    .line 2673607
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2673608
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673609
    if-lez v1, :cond_3

    .line 2673610
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673611
    add-int/lit8 v1, v1, 0x1

    .line 2673612
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673613
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673614
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2673615
    goto :goto_0

    .line 2673616
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673617
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673618
    :sswitch_0
    const-string v9, "share_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "notif_trigger"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v9, "site_uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v9, "notif_medium"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v9, "rule"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v9, "claim_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x5

    goto :goto_3

    :sswitch_6
    const-string v9, "offer_code"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x6

    goto :goto_3

    :sswitch_7
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x7

    goto :goto_3

    :sswitch_8
    const-string v9, "offer_view_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/16 v2, 0x8

    goto :goto_3

    .line 2673619
    :pswitch_0
    or-int/lit8 v2, v5, 0x40

    .line 2673620
    if-nez v3, :cond_4

    .line 2673621
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673622
    :cond_4
    const-string v5, "share_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673623
    goto/16 :goto_0

    .line 2673624
    :pswitch_1
    or-int/lit8 v2, v5, 0x4

    .line 2673625
    if-nez v3, :cond_5

    .line 2673626
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673627
    :cond_5
    const-string v5, "notif_trigger"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673628
    goto/16 :goto_0

    .line 2673629
    :pswitch_2
    or-int/lit16 v2, v5, 0x80

    .line 2673630
    if-nez v3, :cond_6

    .line 2673631
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673632
    :cond_6
    const-string v5, "site_uri"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673633
    goto/16 :goto_0

    .line 2673634
    :pswitch_3
    or-int/lit8 v2, v5, 0x2

    .line 2673635
    if-nez v3, :cond_7

    .line 2673636
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673637
    :cond_7
    const-string v5, "notif_medium"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673638
    goto/16 :goto_0

    .line 2673639
    :pswitch_4
    or-int/lit8 v2, v5, 0x20

    .line 2673640
    if-nez v3, :cond_8

    .line 2673641
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673642
    :cond_8
    const-string v5, "rule"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673643
    goto/16 :goto_0

    .line 2673644
    :pswitch_5
    or-int/lit8 v2, v5, 0x1

    .line 2673645
    if-nez v3, :cond_9

    .line 2673646
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673647
    :cond_9
    const-string v5, "claim_type"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673648
    goto/16 :goto_0

    .line 2673649
    :pswitch_6
    or-int/lit8 v2, v5, 0x8

    .line 2673650
    if-nez v3, :cond_a

    .line 2673651
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673652
    :cond_a
    const-string v5, "offer_code"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673653
    goto/16 :goto_0

    .line 2673654
    :pswitch_7
    or-int/lit16 v2, v5, 0x100

    .line 2673655
    if-nez v3, :cond_b

    .line 2673656
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673657
    :cond_b
    const-string v5, "title"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673658
    goto/16 :goto_0

    .line 2673659
    :pswitch_8
    or-int/lit8 v2, v5, 0x10

    .line 2673660
    if-nez v3, :cond_c

    .line 2673661
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2673662
    :cond_c
    const-string v5, "offer_view_id"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2673663
    goto/16 :goto_0

    .line 2673664
    :cond_d
    packed-switch v5, :pswitch_data_1

    .line 2673665
    :pswitch_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673666
    :pswitch_a
    const-string v1, "com.facebook.offers.activity.OfferRedirectToWebActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x2c889aa3 -> :sswitch_5
        -0x2c54de85 -> :sswitch_0
        -0x2344e457 -> :sswitch_1
        -0x223361ee -> :sswitch_8
        0x3596fc -> :sswitch_4
        0x6942258 -> :sswitch_7
        0x99b65f0 -> :sswitch_6
        0x23bdc764 -> :sswitch_3
        0x2843d874 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1d0
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method private static e(Landroid/content/Context;[CLX/17c;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v2, 0x11

    const/4 v4, 0x0

    .line 2673588
    array-length v3, p1

    .line 2673589
    const/4 v0, 0x0

    const-string v1, "payments/receipt"

    invoke-static {p1, v0, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2673590
    :cond_0
    :goto_0
    return-object v4

    .line 2673591
    :cond_1
    if-ge v5, v3, :cond_0

    .line 2673592
    aget-char v0, p1, v5

    .line 2673593
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2673594
    :sswitch_0
    if-ge v2, v3, :cond_0

    aget-char v0, p1, v2

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_0

    .line 2673595
    const/16 v2, 0x12

    :sswitch_1
    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 2673596
    invoke-static/range {v0 .. v5}, LX/JHc;->am(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2f -> :sswitch_0
        0x3f -> :sswitch_1
    .end sparse-switch
.end method

.method private static f(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2673574
    if-nez p4, :cond_4

    .line 2673575
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2673576
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "tab_token"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2673577
    if-nez v0, :cond_0

    .line 2673578
    const/4 v0, 0x0

    .line 2673579
    :goto_1
    return-object v0

    .line 2673580
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2673581
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2673582
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2673583
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2673584
    :cond_2
    const-string v0, "tab_token"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2673585
    if-gt p2, v1, :cond_3

    .line 2673586
    const/4 v2, 0x1

    const/16 v3, 0x11e

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2673587
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static g(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2673561
    if-nez p4, :cond_4

    .line 2673562
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2673563
    :goto_0
    const-string v4, "id_or_token"

    move-object v0, p1

    move v1, p3

    move-object v3, v2

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2673564
    if-nez v0, :cond_1

    .line 2673565
    :cond_0
    :goto_1
    return-object v2

    .line 2673566
    :cond_1
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2673567
    const/4 v3, 0x1

    aget v0, v0, v3

    .line 2673568
    if-lez v0, :cond_2

    const/4 v3, 0x3

    if-le v0, v3, :cond_3

    .line 2673569
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2673570
    :cond_3
    const-string v0, "id_or_token"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2673571
    const-string v0, "/tab/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2673572
    add-int/lit8 v4, v1, 0x5

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v6, p5

    .line 2673573
    invoke-static/range {v1 .. v6}, LX/JHc;->f(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static h(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2673474
    if-nez p4, :cond_d

    .line 2673475
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2673476
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "arg_cta_type"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2673477
    if-nez v0, :cond_0

    .line 2673478
    const/4 v0, 0x0

    .line 2673479
    :goto_1
    return-object v0

    .line 2673480
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2673481
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2673482
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2673483
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2673484
    :cond_2
    const-string v0, "arg_cta_type"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2673485
    if-gt p2, v1, :cond_3

    .line 2673486
    const/4 v2, 0x1

    const/16 v3, 0x13c

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2673487
    :cond_3
    if-gt p2, v1, :cond_4

    .line 2673488
    const/4 v0, 0x0

    goto :goto_1

    .line 2673489
    :cond_4
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2673490
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_c

    .line 2673491
    sub-int v7, p2, v0

    .line 2673492
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2673493
    const/4 v3, 0x0

    .line 2673494
    const/4 v2, 0x0

    .line 2673495
    const/4 v1, 0x0

    .line 2673496
    const/4 v0, 0x0

    move v6, v2

    move-object v4, v5

    move-object v5, v3

    move v3, v0

    .line 2673497
    :goto_2
    if-ge v1, v7, :cond_a

    .line 2673498
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673499
    if-gez v0, :cond_5

    .line 2673500
    const/4 v0, 0x0

    goto :goto_1

    .line 2673501
    :cond_5
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2673502
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673503
    if-lez v1, :cond_7

    .line 2673504
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673505
    add-int/lit8 v1, v1, 0x1

    .line 2673506
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_6
    :goto_4
    packed-switch v2, :pswitch_data_1

    .line 2673507
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673508
    if-eqz v0, :cond_9

    move-object v5, v0

    .line 2673509
    goto :goto_2

    .line 2673510
    :cond_7
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673511
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2673512
    :pswitch_0
    const-string v10, "ref"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x0

    goto :goto_4

    .line 2673513
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2673514
    if-nez v4, :cond_8

    .line 2673515
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2673516
    :cond_8
    const-string v6, "arg_ref"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2673517
    goto :goto_2

    .line 2673518
    :cond_9
    const/4 v0, 0x1

    move v3, v0

    .line 2673519
    goto :goto_2

    .line 2673520
    :cond_a
    packed-switch v6, :pswitch_data_2

    .line 2673521
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673522
    :pswitch_2
    if-eqz v3, :cond_b

    .line 2673523
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673524
    :cond_b
    const/4 v1, 0x1

    const/16 v2, 0x13c

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2673525
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_d
    move-object v5, p4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1b893
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static i(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2674294
    if-nez p4, :cond_4

    .line 2674295
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674296
    :goto_0
    const-string v4, "arg_page_id"

    move-object v0, p1

    move v1, p3

    move-object v3, v2

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674297
    if-nez v0, :cond_1

    .line 2674298
    :cond_0
    :goto_1
    return-object v2

    .line 2674299
    :cond_1
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674300
    const/4 v3, 0x1

    aget v0, v0, v3

    .line 2674301
    if-lez v0, :cond_2

    const/4 v3, 0x3

    if-le v0, v3, :cond_3

    .line 2674302
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674303
    :cond_3
    const-string v0, "arg_page_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674304
    const-string v0, "/config_call_to_action/"

    invoke-static {p1, v1, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2674305
    add-int/lit8 v4, v1, 0x17

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v6, p5

    .line 2674306
    invoke-static/range {v1 .. v6}, LX/JHc;->h(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static j(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674691
    sub-int v7, p3, p2

    .line 2674692
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674693
    const/4 v5, 0x0

    .line 2674694
    const/4 v2, 0x0

    .line 2674695
    const/4 v1, 0x0

    .line 2674696
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2674697
    :goto_0
    if-ge v1, v7, :cond_6

    .line 2674698
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674699
    if-gez v0, :cond_0

    .line 2674700
    const/4 v0, 0x0

    .line 2674701
    :goto_1
    return-object v0

    .line 2674702
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674703
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674704
    if-lez v1, :cond_2

    .line 2674705
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674706
    add-int/lit8 v1, v1, 0x1

    .line 2674707
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2674708
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674709
    if-eqz v0, :cond_5

    move-object v5, v0

    .line 2674710
    goto :goto_0

    .line 2674711
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674712
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674713
    :pswitch_0
    const-string v10, "force_creation_flow"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2674714
    :pswitch_1
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2674715
    if-eqz v2, :cond_4

    .line 2674716
    or-int/lit8 v0, v6, 0x1

    .line 2674717
    if-nez v4, :cond_3

    .line 2674718
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674719
    :cond_3
    const-string v6, "arg_force_creation_flow"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v4, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2674720
    goto :goto_0

    .line 2674721
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2674722
    :cond_5
    const/4 v0, 0x1

    move v3, v0

    .line 2674723
    goto :goto_0

    .line 2674724
    :cond_6
    packed-switch v6, :pswitch_data_2

    .line 2674725
    const/4 v0, 0x0

    goto :goto_1

    .line 2674726
    :pswitch_2
    if-eqz v3, :cond_7

    .line 2674727
    const/4 v0, 0x0

    goto :goto_1

    .line 2674728
    :cond_7
    const/4 v1, 0x1

    const/16 v2, 0x88

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0xc37243a
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static k(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2674682
    const-string v1, "/select_call_to_action"

    invoke-static {p1, p2, v1}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2674683
    :cond_0
    :goto_0
    return-object v0

    .line 2674684
    :cond_1
    add-int/lit8 v1, p2, 0x16

    .line 2674685
    if-ge v1, p3, :cond_0

    .line 2674686
    add-int/lit8 v2, v1, 0x1

    aget-char v1, p1, v1

    .line 2674687
    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 2674688
    :sswitch_0
    if-ge v2, p3, :cond_0

    aget-char v1, p1, v2

    const/16 v3, 0x3f

    if-ne v1, v3, :cond_0

    .line 2674689
    add-int/lit8 v2, v2, 0x1

    :sswitch_1
    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 2674690
    invoke-static/range {v0 .. v5}, LX/JHc;->j(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2f -> :sswitch_0
        0x3f -> :sswitch_1
    .end sparse-switch
.end method

.method private static l(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 2674664
    if-nez p4, :cond_6

    .line 2674665
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674666
    :goto_0
    const/4 v2, 0x0

    const-string v3, "arg_page_id"

    const-string v4, "arg_page_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674667
    if-nez v0, :cond_1

    .line 2674668
    const/4 v0, 0x0

    .line 2674669
    :cond_0
    :goto_1
    return-object v0

    .line 2674670
    :cond_1
    const/4 v1, 0x0

    aget v3, v0, v1

    .line 2674671
    const/4 v1, 0x1

    aget v0, v0, v1

    .line 2674672
    if-lez v0, :cond_2

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    .line 2674673
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674674
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object v6, p5

    .line 2674675
    invoke-static/range {v1 .. v6}, LX/JHc;->k(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    .line 2674676
    if-nez v0, :cond_0

    .line 2674677
    :cond_4
    const-string v0, "arg_page_id"

    invoke-static {p1, p3, v3, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674678
    const-string v0, "/config_call_to_action/"

    invoke-static {p1, v3, v0}, LX/48d;->a([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2674679
    const/4 v0, 0x0

    goto :goto_1

    .line 2674680
    :cond_5
    add-int/lit8 v4, v3, 0x17

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v6, p5

    .line 2674681
    invoke-static/range {v1 .. v6}, LX/JHc;->h(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v5, p4

    goto :goto_0
.end method

.method private static m(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674614
    if-nez p4, :cond_c

    .line 2674615
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674616
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "com.facebook.katana.profile.id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674617
    if-nez v0, :cond_0

    .line 2674618
    const/4 v0, 0x0

    .line 2674619
    :goto_1
    return-object v0

    .line 2674620
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674621
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2674622
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2674623
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674624
    :cond_2
    const-string v0, "com.facebook.katana.profile.id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674625
    if-gt p2, v1, :cond_3

    .line 2674626
    const/4 v0, 0x0

    goto :goto_1

    .line 2674627
    :cond_3
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2674628
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_b

    .line 2674629
    sub-int v7, p2, v0

    .line 2674630
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674631
    const/4 v4, 0x0

    .line 2674632
    const/4 v2, 0x0

    .line 2674633
    const/4 v1, 0x0

    .line 2674634
    const/4 v0, 0x0

    move v6, v2

    move-object v3, v5

    move v5, v0

    .line 2674635
    :goto_2
    if-ge v1, v7, :cond_9

    .line 2674636
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674637
    if-gez v0, :cond_4

    .line 2674638
    const/4 v0, 0x0

    goto :goto_1

    .line 2674639
    :cond_4
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674640
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674641
    if-lez v1, :cond_6

    .line 2674642
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674643
    add-int/lit8 v1, v1, 0x1

    .line 2674644
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_5
    :goto_4
    packed-switch v2, :pswitch_data_1

    .line 2674645
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674646
    if-eqz v0, :cond_8

    move-object v4, v0

    .line 2674647
    goto :goto_2

    .line 2674648
    :cond_6
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674649
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2674650
    :pswitch_0
    const-string v10, "profile_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v2, 0x0

    goto :goto_4

    .line 2674651
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674652
    if-nez v3, :cond_7

    .line 2674653
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674654
    :cond_7
    const-string v6, "com.facebook.katana.profile.type"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674655
    goto :goto_2

    .line 2674656
    :cond_8
    const/4 v0, 0x1

    move v5, v0

    .line 2674657
    goto :goto_2

    .line 2674658
    :cond_9
    packed-switch v6, :pswitch_data_2

    .line 2674659
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674660
    :pswitch_2
    if-eqz v5, :cond_a

    .line 2674661
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674662
    :cond_a
    const-string v1, "com.facebook.platform.PlatformCanonicalProfileIdActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674663
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_c
    move-object v5, p4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x48f36010
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static n(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674489
    if-gt p3, p2, :cond_0

    .line 2674490
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p4

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    .line 2674491
    :goto_0
    return-object v0

    .line 2674492
    :cond_0
    if-gt p3, p2, :cond_1

    .line 2674493
    const/4 v0, 0x0

    goto :goto_0

    .line 2674494
    :cond_1
    add-int/lit8 v0, p2, 0x1

    aget-char v1, p1, p2

    .line 2674495
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_1e

    .line 2674496
    sub-int v7, p3, v0

    .line 2674497
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674498
    const/4 v5, 0x0

    .line 2674499
    const/4 v2, 0x0

    .line 2674500
    const/4 v1, 0x0

    .line 2674501
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object v4, p4

    .line 2674502
    :goto_1
    if-ge v1, v7, :cond_13

    .line 2674503
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674504
    if-gez v0, :cond_2

    .line 2674505
    const/4 v0, 0x0

    goto :goto_0

    .line 2674506
    :cond_2
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674507
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674508
    if-lez v1, :cond_4

    .line 2674509
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674510
    add-int/lit8 v1, v1, 0x1

    .line 2674511
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_3
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674512
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674513
    if-eqz v0, :cond_12

    move-object v5, v0

    .line 2674514
    goto :goto_1

    .line 2674515
    :cond_4
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674516
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674517
    :sswitch_0
    const-string v10, "extra_launch_profile_photo_selector"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "skip_popup"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "show_footer"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "footer_link_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v10, "composer"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v10, "short_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x5

    goto :goto_3

    :sswitch_6
    const-string v10, "footer_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x6

    goto :goto_3

    :sswitch_7
    const-string v10, "fref"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v2, 0x7

    goto :goto_3

    :sswitch_8
    const-string v10, "qp_h"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v2, 0x8

    goto :goto_3

    :sswitch_9
    const-string v10, "attachment_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v2, 0x9

    goto :goto_3

    :sswitch_a
    const-string v10, "initial_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v2, 0xa

    goto/16 :goto_3

    .line 2674518
    :pswitch_0
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2674519
    if-eqz v2, :cond_6

    .line 2674520
    or-int/lit8 v0, v6, 0x40

    .line 2674521
    if-nez v4, :cond_5

    .line 2674522
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674523
    :cond_5
    const-string v6, "extra_launch_profile_photo_selector"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v4, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2674524
    goto/16 :goto_1

    .line 2674525
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674526
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674527
    if-nez v4, :cond_7

    .line 2674528
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674529
    :cond_7
    const-string v6, "skip_popup"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674530
    goto/16 :goto_1

    .line 2674531
    :pswitch_2
    invoke-static {v0}, LX/48d;->a(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 2674532
    if-eqz v2, :cond_9

    .line 2674533
    or-int/lit8 v0, v6, 0x10

    .line 2674534
    if-nez v4, :cond_8

    .line 2674535
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674536
    :cond_8
    const-string v6, "show_footer"

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v4, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v6, v0

    .line 2674537
    goto/16 :goto_1

    .line 2674538
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674539
    :pswitch_3
    or-int/lit8 v2, v6, 0x20

    .line 2674540
    if-nez v4, :cond_a

    .line 2674541
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674542
    :cond_a
    const-string v6, "footer_link_text"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674543
    goto/16 :goto_1

    .line 2674544
    :pswitch_4
    or-int/lit16 v2, v6, 0x200

    .line 2674545
    if-nez v4, :cond_b

    .line 2674546
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674547
    :cond_b
    const-string v6, "composer"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674548
    goto/16 :goto_1

    .line 2674549
    :pswitch_5
    or-int/lit8 v2, v6, 0x8

    .line 2674550
    if-nez v4, :cond_c

    .line 2674551
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674552
    :cond_c
    const-string v6, "short_name"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674553
    goto/16 :goto_1

    .line 2674554
    :pswitch_6
    or-int/lit8 v2, v6, 0x4

    .line 2674555
    if-nez v4, :cond_d

    .line 2674556
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674557
    :cond_d
    const-string v6, "footer_text"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674558
    goto/16 :goto_1

    .line 2674559
    :pswitch_7
    or-int/lit8 v2, v6, 0x2

    .line 2674560
    if-nez v4, :cond_e

    .line 2674561
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674562
    :cond_e
    const-string v6, "timeline_friend_request_ref"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674563
    goto/16 :goto_1

    .line 2674564
    :pswitch_8
    or-int/lit16 v2, v6, 0x80

    .line 2674565
    if-nez v4, :cond_f

    .line 2674566
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674567
    :cond_f
    const-string v6, "qp_h"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674568
    goto/16 :goto_1

    .line 2674569
    :pswitch_9
    or-int/lit16 v2, v6, 0x100

    .line 2674570
    if-nez v4, :cond_10

    .line 2674571
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674572
    :cond_10
    const-string v6, "attachment_url"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674573
    goto/16 :goto_1

    .line 2674574
    :pswitch_a
    or-int/lit16 v2, v6, 0x400

    .line 2674575
    if-nez v4, :cond_11

    .line 2674576
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674577
    :cond_11
    const-string v6, "initial_text"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674578
    goto/16 :goto_1

    .line 2674579
    :cond_12
    const/4 v0, 0x1

    move v3, v0

    .line 2674580
    goto/16 :goto_1

    .line 2674581
    :cond_13
    sparse-switch v6, :sswitch_data_1

    .line 2674582
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674583
    :sswitch_b
    if-eqz v3, :cond_14

    .line 2674584
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674585
    :cond_14
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674586
    :sswitch_c
    if-eqz v3, :cond_15

    .line 2674587
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674588
    :cond_15
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674589
    :sswitch_d
    if-eqz v3, :cond_16

    .line 2674590
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674591
    :cond_16
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674592
    :sswitch_e
    if-eqz v3, :cond_17

    .line 2674593
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674594
    :cond_17
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674595
    :sswitch_f
    if-eqz v3, :cond_18

    .line 2674596
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674597
    :cond_18
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674598
    :sswitch_10
    if-eqz v3, :cond_19

    .line 2674599
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674600
    :cond_19
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674601
    :sswitch_11
    if-eqz v3, :cond_1a

    .line 2674602
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674603
    :cond_1a
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674604
    :sswitch_12
    if-eqz v3, :cond_1b

    .line 2674605
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674606
    :cond_1b
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674607
    :sswitch_13
    if-eqz v3, :cond_1c

    .line 2674608
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674609
    :cond_1c
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674610
    :sswitch_14
    if-eqz v3, :cond_1d

    .line 2674611
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2674612
    :cond_1d
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_0

    .line 2674613
    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x23b93ee0 -> :sswitch_4
        -0x18686eb8 -> :sswitch_a
        0x30166d -> :sswitch_7
        0x350e48 -> :sswitch_8
        0x9b8f11d -> :sswitch_2
        0x1e285dec -> :sswitch_1
        0x1fbd0453 -> :sswitch_9
        0x417a9151 -> :sswitch_6
        0x564f06bf -> :sswitch_0
        0x5d541c6e -> :sswitch_5
        0x7628e4ae -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_d
        0x2 -> :sswitch_14
        0x1c -> :sswitch_b
        0x3c -> :sswitch_12
        0x40 -> :sswitch_13
        0xc0 -> :sswitch_c
        0x300 -> :sswitch_e
        0x380 -> :sswitch_10
        0x700 -> :sswitch_f
        0x780 -> :sswitch_11
    .end sparse-switch
.end method

.method private static o(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674436
    if-nez p4, :cond_e

    .line 2674437
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674438
    :goto_0
    const/4 v2, 0x0

    const-string v3, "com.facebook.katana.profile.id"

    const-string v4, "com.facebook.katana.profile.id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674439
    if-nez v0, :cond_1

    .line 2674440
    const/4 v0, 0x0

    .line 2674441
    :cond_0
    :goto_1
    return-object v0

    .line 2674442
    :cond_1
    const/4 v1, 0x0

    aget v3, v0, v1

    .line 2674443
    const/4 v1, 0x1

    aget v0, v0, v1

    .line 2674444
    if-lez v0, :cond_2

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    .line 2674445
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674446
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object/from16 v6, p5

    .line 2674447
    invoke-static/range {v1 .. v6}, LX/JHc;->n(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    .line 2674448
    if-nez v0, :cond_0

    .line 2674449
    :cond_4
    const-string v0, "com.facebook.katana.profile.id"

    invoke-static {p1, p3, v3, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674450
    if-gt p2, v3, :cond_5

    .line 2674451
    const/4 v0, 0x0

    goto :goto_1

    .line 2674452
    :cond_5
    add-int/lit8 v0, v3, 0x1

    aget-char v1, p1, v3

    .line 2674453
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_d

    .line 2674454
    sub-int v7, p2, v0

    .line 2674455
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674456
    const/4 v4, 0x0

    .line 2674457
    const/4 v2, 0x0

    .line 2674458
    const/4 v1, 0x0

    .line 2674459
    const/4 v0, 0x0

    move v6, v2

    move-object v3, v5

    move v5, v0

    .line 2674460
    :goto_2
    if-ge v1, v7, :cond_b

    .line 2674461
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674462
    if-gez v0, :cond_6

    .line 2674463
    const/4 v0, 0x0

    goto :goto_1

    .line 2674464
    :cond_6
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674465
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674466
    if-lez v1, :cond_8

    .line 2674467
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674468
    add-int/lit8 v1, v1, 0x1

    .line 2674469
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_7
    :goto_4
    packed-switch v2, :pswitch_data_1

    .line 2674470
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674471
    if-eqz v0, :cond_a

    move-object v4, v0

    .line 2674472
    goto :goto_2

    .line 2674473
    :cond_8
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674474
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2674475
    :pswitch_0
    const-string v10, "profile_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v2, 0x0

    goto :goto_4

    .line 2674476
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674477
    if-nez v3, :cond_9

    .line 2674478
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674479
    :cond_9
    const-string v6, "com.facebook.katana.profile.type"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674480
    goto :goto_2

    .line 2674481
    :cond_a
    const/4 v0, 0x1

    move v5, v0

    .line 2674482
    goto :goto_2

    .line 2674483
    :cond_b
    packed-switch v6, :pswitch_data_2

    .line 2674484
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674485
    :pswitch_2
    if-eqz v5, :cond_c

    .line 2674486
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674487
    :cond_c
    const-string v1, "com.facebook.platform.PlatformCanonicalProfileIdActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674488
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_e
    move-object v5, p4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x48f36010
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static p(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 12

    .prologue
    .line 2674322
    sub-int v7, p3, p2

    .line 2674323
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674324
    const/4 v5, 0x0

    .line 2674325
    const/4 v2, 0x0

    .line 2674326
    const/4 v1, 0x0

    .line 2674327
    const/4 v0, 0x0

    move v3, v0

    move v6, v2

    move-object/from16 v4, p4

    .line 2674328
    :goto_0
    if-ge v1, v7, :cond_10

    .line 2674329
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674330
    if-gez v0, :cond_0

    .line 2674331
    const/4 v0, 0x0

    .line 2674332
    :goto_1
    return-object v0

    .line 2674333
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674334
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674335
    if-lez v1, :cond_2

    .line 2674336
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674337
    add-int/lit8 v1, v1, 0x1

    .line 2674338
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674339
    invoke-static {v5, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674340
    if-eqz v0, :cond_f

    move-object v5, v0

    .line 2674341
    goto :goto_0

    .line 2674342
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674343
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674344
    :sswitch_0
    const-string v10, "_bt_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "profile_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "surface"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "category_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v10, "action_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x5

    goto :goto_3

    :sswitch_6
    const-string v10, "intro_card"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x6

    goto :goto_3

    :sswitch_7
    const-string v10, "entry_point"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x7

    goto :goto_3

    :sswitch_8
    const-string v10, "frame_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/16 v2, 0x8

    goto :goto_3

    .line 2674345
    :pswitch_0
    or-int/lit16 v2, v6, 0x1000

    .line 2674346
    if-nez v4, :cond_3

    .line 2674347
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674348
    :cond_3
    const-string v6, "_bt_"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674349
    goto/16 :goto_0

    .line 2674350
    :pswitch_1
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    packed-switch v9, :pswitch_data_1

    :cond_4
    move v0, v2

    :goto_4
    packed-switch v0, :pswitch_data_2

    .line 2674351
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674352
    :pswitch_2
    const-string v9, "person"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_4

    .line 2674353
    :pswitch_3
    or-int/lit8 v0, v6, 0x2

    move v6, v0

    .line 2674354
    goto/16 :goto_0

    .line 2674355
    :pswitch_4
    or-int/lit8 v2, v6, 0x20

    .line 2674356
    if-nez v4, :cond_5

    .line 2674357
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674358
    :cond_5
    const-string v6, "surface"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674359
    goto/16 :goto_0

    .line 2674360
    :pswitch_5
    or-int/lit16 v2, v6, 0x200

    .line 2674361
    if-nez v4, :cond_6

    .line 2674362
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674363
    :cond_6
    const-string v6, "heisman_category_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674364
    goto/16 :goto_0

    .line 2674365
    :pswitch_6
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_1

    :cond_7
    move v0, v2

    :goto_5
    packed-switch v0, :pswitch_data_3

    .line 2674366
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674367
    :sswitch_9
    const-string v9, "profile_video_record"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    goto :goto_5

    :sswitch_a
    const-string v9, "profile_video_upload"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    goto :goto_5

    .line 2674368
    :pswitch_7
    or-int/lit16 v0, v6, 0x800

    move v6, v0

    .line 2674369
    goto/16 :goto_0

    .line 2674370
    :pswitch_8
    or-int/lit16 v0, v6, 0x400

    move v6, v0

    .line 2674371
    goto/16 :goto_0

    .line 2674372
    :pswitch_9
    invoke-static {v0}, LX/48d;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    .line 2674373
    if-eqz v2, :cond_9

    .line 2674374
    or-int/lit8 v0, v6, 0x1

    .line 2674375
    if-nez v4, :cond_8

    .line 2674376
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674377
    :cond_8
    const-string v6, "com.facebook.katana.profile.id"

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v4, v6, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move v6, v0

    .line 2674378
    goto/16 :goto_0

    .line 2674379
    :cond_9
    or-int/lit8 v2, v6, 0x4

    .line 2674380
    if-nez v4, :cond_a

    .line 2674381
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674382
    :cond_a
    const-string v6, "profileId"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674383
    goto/16 :goto_0

    .line 2674384
    :pswitch_a
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_2

    :cond_b
    :goto_6
    packed-switch v2, :pswitch_data_4

    .line 2674385
    or-int/lit8 v2, v6, 0x40

    .line 2674386
    if-nez v4, :cond_c

    .line 2674387
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674388
    :cond_c
    const-string v6, "intro_card"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674389
    goto/16 :goto_0

    .line 2674390
    :sswitch_b
    const-string v9, "edit_bio"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v2, 0x0

    goto :goto_6

    :sswitch_c
    const-string v9, "edit_featured_photos"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v2, 0x1

    goto :goto_6

    .line 2674391
    :pswitch_b
    or-int/lit8 v0, v6, 0x10

    move v6, v0

    .line 2674392
    goto/16 :goto_0

    .line 2674393
    :pswitch_c
    or-int/lit8 v0, v6, 0x8

    move v6, v0

    .line 2674394
    goto/16 :goto_0

    .line 2674395
    :pswitch_d
    or-int/lit16 v2, v6, 0x80

    .line 2674396
    if-nez v4, :cond_d

    .line 2674397
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674398
    :cond_d
    const-string v6, "entry_point"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674399
    goto/16 :goto_0

    .line 2674400
    :pswitch_e
    or-int/lit16 v2, v6, 0x100

    .line 2674401
    if-nez v4, :cond_e

    .line 2674402
    new-instance v4, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v4, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674403
    :cond_e
    const-string v6, "frame_id"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674404
    goto/16 :goto_0

    .line 2674405
    :cond_f
    const/4 v0, 0x1

    move v3, v0

    .line 2674406
    goto/16 :goto_0

    .line 2674407
    :cond_10
    sparse-switch v6, :sswitch_data_3

    .line 2674408
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674409
    :sswitch_d
    if-eqz v3, :cond_11

    .line 2674410
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674411
    :cond_11
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674412
    :sswitch_e
    if-eqz v3, :cond_12

    .line 2674413
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674414
    :cond_12
    const-string v2, "com.facebook.timeline.header.intro.bio.edit.TimelineBioEditActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674415
    :sswitch_f
    if-eqz v3, :cond_13

    .line 2674416
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674417
    :cond_13
    const-string v2, "com.facebook.heisman.ProfilePictureOverlayPivotActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674418
    :sswitch_10
    if-eqz v3, :cond_14

    .line 2674419
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674420
    :cond_14
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674421
    :sswitch_11
    if-eqz v3, :cond_15

    .line 2674422
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674423
    :cond_15
    const-string v2, "com.facebook.heisman.ProfilePictureOverlayCameraActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674424
    :sswitch_12
    if-eqz v3, :cond_16

    .line 2674425
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674426
    :cond_16
    const-string v2, "com.facebook.timeline.profilevideo.ExistingProfileVideoActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674427
    :sswitch_13
    if-eqz v3, :cond_17

    .line 2674428
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674429
    :cond_17
    const-string v2, "com.facebook.timeline.profilevideo.CreateProfileVideoActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674430
    :sswitch_14
    if-eqz v3, :cond_18

    .line 2674431
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674432
    :cond_18
    const-string v2, "com.facebook.timeline.header.intro.favphotos.edit.TimelineEditFavPhotosActivity"

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674433
    :sswitch_15
    if-eqz v3, :cond_19

    .line 2674434
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674435
    :cond_19
    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object v0, p0

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v6}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6e761353 -> :sswitch_2
        -0x620c4ad3 -> :sswitch_8
        -0x2fa1dc7d -> :sswitch_7
        0xd1b -> :sswitch_5
        0x2cad8e -> :sswitch_0
        0x48f36010 -> :sswitch_1
        0x4ae8f103 -> :sswitch_6
        0x5ba8abfc -> :sswitch_3
        0x5e663ba3 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x3b1c64ab
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x55d9ff6b -> :sswitch_9
        0x5b97a11b -> :sswitch_a
    .end sparse-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x4feb5e83 -> :sswitch_c
        0x5f820553 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :sswitch_data_3
    .sparse-switch
        0x3 -> :sswitch_d
        0xe -> :sswitch_14
        0x36 -> :sswitch_e
        0x46 -> :sswitch_15
        0x180 -> :sswitch_11
        0x280 -> :sswitch_f
        0x402 -> :sswitch_12
        0x802 -> :sswitch_13
        0x1000 -> :sswitch_10
    .end sparse-switch
.end method

.method private static q(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2674307
    if-nez p4, :cond_5

    .line 2674308
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674309
    :goto_0
    const/4 v2, 0x0

    const-string v3, "com.facebook.katana.profile.id"

    const/4 v4, 0x0

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674310
    if-nez v0, :cond_0

    .line 2674311
    const/4 v0, 0x0

    .line 2674312
    :goto_1
    return-object v0

    .line 2674313
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674314
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2674315
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2674316
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674317
    :cond_2
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 2674318
    if-gt p2, v1, :cond_3

    .line 2674319
    const/4 v2, 0x1

    const/16 v3, 0x131

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2674320
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2674321
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-object v5, p4

    goto :goto_0
.end method

.method private static r(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2673932
    sub-int v7, p3, p2

    .line 2673933
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2673934
    const/4 v4, 0x0

    .line 2673935
    const/4 v2, 0x0

    .line 2673936
    const/4 v1, 0x0

    .line 2673937
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2673938
    :goto_0
    if-ge v1, v7, :cond_7

    .line 2673939
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673940
    if-gez v0, :cond_0

    .line 2673941
    const/4 v0, 0x0

    .line 2673942
    :goto_1
    return-object v0

    .line 2673943
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2673944
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673945
    if-lez v1, :cond_2

    .line 2673946
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673947
    add-int/lit8 v1, v1, 0x1

    .line 2673948
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2673949
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2673950
    if-eqz v0, :cond_6

    move-object v4, v0

    .line 2673951
    goto :goto_0

    .line 2673952
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2673953
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2673954
    :sswitch_0
    const-string v10, "referral_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "bucketid"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "referral_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    .line 2673955
    :pswitch_0
    or-int/lit8 v2, v6, 0x1

    .line 2673956
    if-nez v3, :cond_3

    .line 2673957
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2673958
    :cond_3
    const-string v6, "referral_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2673959
    goto :goto_0

    .line 2673960
    :pswitch_1
    or-int/lit8 v2, v6, 0x4

    .line 2673961
    if-nez v3, :cond_4

    .line 2673962
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2673963
    :cond_4
    const-string v6, "bucketid"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2673964
    goto :goto_0

    .line 2673965
    :pswitch_2
    or-int/lit8 v2, v6, 0x2

    .line 2673966
    if-nez v3, :cond_5

    .line 2673967
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2673968
    :cond_5
    const-string v6, "referral_type"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2673969
    goto/16 :goto_0

    .line 2673970
    :cond_6
    const/4 v0, 0x1

    move v5, v0

    .line 2673971
    goto/16 :goto_0

    .line 2673972
    :cond_7
    packed-switch v6, :pswitch_data_1

    .line 2673973
    :pswitch_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673974
    :pswitch_4
    if-eqz v5, :cond_8

    .line 2673975
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673976
    :cond_8
    const-string v1, "com.facebook.profile.discovery.DiscoveryCardsActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2673977
    :pswitch_5
    if-eqz v5, :cond_9

    .line 2673978
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673979
    :cond_9
    const-string v1, "com.facebook.profile.discovery.DiscoveryDashboardActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2673980
    :pswitch_6
    if-eqz v5, :cond_a

    .line 2673981
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2673982
    :cond_a
    const-string v1, "com.facebook.profile.discovery.DiscoveryCardsActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5f907b7b -> :sswitch_1
        0x540869c -> :sswitch_2
        0x148474fd -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
    .end packed-switch
.end method

.method private static s(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 2674280
    if-nez p4, :cond_4

    .line 2674281
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674282
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "order_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674283
    if-nez v0, :cond_0

    .line 2674284
    const/4 v0, 0x0

    .line 2674285
    :goto_1
    return-object v0

    .line 2674286
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674287
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2674288
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2674289
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674290
    :cond_2
    const-string v0, "order_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674291
    if-gt p2, v1, :cond_3

    .line 2674292
    const/4 v2, 0x1

    const/16 v3, 0x11a

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v7, p5

    invoke-static/range {v1 .. v7}, LX/48d;->a(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2674293
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v5, p4

    goto :goto_0
.end method

.method private static t(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2674243
    sub-int v6, p3, p2

    .line 2674244
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2674245
    const/4 v4, 0x0

    .line 2674246
    const/4 v0, 0x0

    .line 2674247
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2674248
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_6

    .line 2674249
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674250
    if-gez v0, :cond_1

    .line 2674251
    const/4 v0, 0x0

    .line 2674252
    :goto_1
    return-object v0

    .line 2674253
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2674254
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674255
    if-lez v1, :cond_3

    .line 2674256
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674257
    add-int/lit8 v1, v1, 0x1

    .line 2674258
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674259
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674260
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2674261
    goto :goto_0

    .line 2674262
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674263
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674264
    :sswitch_0
    const-string v9, "ref"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v9, "tab"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    .line 2674265
    :pswitch_0
    or-int/lit8 v2, v5, 0x2

    .line 2674266
    if-nez v3, :cond_4

    .line 2674267
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2674268
    :cond_4
    const-string v5, "ref"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2674269
    goto :goto_0

    .line 2674270
    :pswitch_1
    or-int/lit8 v2, v5, 0x1

    .line 2674271
    if-nez v3, :cond_5

    .line 2674272
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2674273
    :cond_5
    const-string v5, "tabId"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2674274
    goto :goto_0

    .line 2674275
    :cond_6
    packed-switch v5, :pswitch_data_1

    .line 2674276
    const/4 v0, 0x0

    goto :goto_1

    .line 2674277
    :pswitch_2
    const-string v1, "com.facebook.civicengagement.elections.ui.ElectionHubActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2674278
    :pswitch_3
    const-string v1, "com.facebook.civicengagement.elections.ui.ElectionHubActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674279
    :pswitch_4
    const-string v1, "com.facebook.civicengagement.elections.ui.ElectionHubActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1b893 -> :sswitch_0
        0x1bf95 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private static u(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674193
    if-nez p4, :cond_c

    .line 2674194
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x2

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 2674195
    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "page_id"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/48d;->a([CILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)[I

    move-result-object v0

    .line 2674196
    if-nez v0, :cond_0

    .line 2674197
    const/4 v0, 0x0

    .line 2674198
    :goto_1
    return-object v0

    .line 2674199
    :cond_0
    const/4 v1, 0x0

    aget v1, v0, v1

    .line 2674200
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 2674201
    if-lez v0, :cond_1

    const/4 v2, 0x3

    if-le v0, v2, :cond_2

    .line 2674202
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected templateType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2674203
    :cond_2
    const-string v0, "page_id"

    invoke-static {p1, p3, v1, v0, v5}, LX/48d;->a([CIILjava/lang/String;Landroid/os/Bundle;)V

    .line 2674204
    if-gt p2, v1, :cond_3

    .line 2674205
    const/4 v0, 0x0

    goto :goto_1

    .line 2674206
    :cond_3
    add-int/lit8 v0, v1, 0x1

    aget-char v1, p1, v1

    .line 2674207
    const/16 v2, 0x3f

    if-ne v1, v2, :cond_b

    .line 2674208
    sub-int v7, p2, v0

    .line 2674209
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, v0, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674210
    const/4 v4, 0x0

    .line 2674211
    const/4 v2, 0x0

    .line 2674212
    const/4 v1, 0x0

    .line 2674213
    const/4 v0, 0x0

    move v6, v2

    move-object v3, v5

    move v5, v0

    .line 2674214
    :goto_2
    if-ge v1, v7, :cond_9

    .line 2674215
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674216
    if-gez v0, :cond_4

    .line 2674217
    const/4 v0, 0x0

    goto :goto_1

    .line 2674218
    :cond_4
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674219
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674220
    if-lez v1, :cond_6

    .line 2674221
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674222
    add-int/lit8 v1, v1, 0x1

    .line 2674223
    :goto_3
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_5
    :goto_4
    packed-switch v2, :pswitch_data_1

    .line 2674224
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674225
    if-eqz v0, :cond_8

    move-object v4, v0

    .line 2674226
    goto :goto_2

    .line 2674227
    :cond_6
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674228
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_3

    .line 2674229
    :pswitch_0
    const-string v10, "entry_point"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v2, 0x0

    goto :goto_4

    .line 2674230
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674231
    if-nez v3, :cond_7

    .line 2674232
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674233
    :cond_7
    const-string v6, "entry_point"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674234
    goto :goto_2

    .line 2674235
    :cond_8
    const/4 v0, 0x1

    move v5, v0

    .line 2674236
    goto :goto_2

    .line 2674237
    :cond_9
    packed-switch v6, :pswitch_data_2

    .line 2674238
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674239
    :pswitch_2
    if-eqz v5, :cond_a

    .line 2674240
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674241
    :cond_a
    const-string v1, "com.facebook.crowdsourcing.feather.activity.FeatherActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    .line 2674242
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_c
    move-object v5, p4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch -0x2fa1dc7d
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static v(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674158
    sub-int v7, p3, p2

    .line 2674159
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674160
    const/4 v4, 0x0

    .line 2674161
    const/4 v2, 0x0

    .line 2674162
    const/4 v1, 0x0

    .line 2674163
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674164
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2674165
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674166
    if-gez v0, :cond_0

    .line 2674167
    const/4 v0, 0x0

    .line 2674168
    :goto_1
    return-object v0

    .line 2674169
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674170
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674171
    if-lez v1, :cond_2

    .line 2674172
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674173
    add-int/lit8 v1, v1, 0x1

    .line 2674174
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2674175
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674176
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2674177
    goto :goto_0

    .line 2674178
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674179
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674180
    :pswitch_0
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2674181
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674182
    if-nez v3, :cond_3

    .line 2674183
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674184
    :cond_3
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674185
    goto :goto_0

    .line 2674186
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2674187
    goto :goto_0

    .line 2674188
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2674189
    const/4 v0, 0x0

    goto :goto_1

    .line 2674190
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2674191
    const/4 v0, 0x0

    goto :goto_1

    .line 2674192
    :cond_6
    const-string v1, "com.facebook.socialgood.ui.FundraiserCreationCharitySearchActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x356f97e5
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static w(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2674128
    sub-int v6, p3, p2

    .line 2674129
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p1, p2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 2674130
    const/4 v4, 0x0

    .line 2674131
    const/4 v0, 0x0

    .line 2674132
    const/4 v1, 0x0

    move v5, v0

    move-object v3, p4

    .line 2674133
    :cond_0
    :goto_0
    if-ge v1, v6, :cond_5

    .line 2674134
    const/16 v0, 0x3d

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674135
    if-gez v0, :cond_1

    .line 2674136
    const/4 v0, 0x0

    .line 2674137
    :goto_1
    return-object v0

    .line 2674138
    :cond_1
    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 2674139
    const/16 v1, 0x26

    invoke-virtual {v7, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674140
    if-lez v1, :cond_3

    .line 2674141
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674142
    add-int/lit8 v1, v1, 0x1

    .line 2674143
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :cond_2
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2674144
    invoke-static {v4, v8, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674145
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 2674146
    goto :goto_0

    .line 2674147
    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674148
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674149
    :pswitch_0
    const-string v9, "entry_point"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v2, 0x0

    goto :goto_3

    .line 2674150
    :pswitch_1
    or-int/lit8 v2, v5, 0x1

    .line 2674151
    if-nez v3, :cond_4

    .line 2674152
    new-instance v3, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v3, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2674153
    :cond_4
    const-string v5, "entry_point"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v2

    .line 2674154
    goto :goto_0

    .line 2674155
    :cond_5
    packed-switch v5, :pswitch_data_2

    .line 2674156
    const/4 v0, 0x0

    goto :goto_1

    .line 2674157
    :pswitch_2
    const-string v1, "com.facebook.crowdsourcing.grapheditor.activity.GraphEditorActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x2fa1dc7d
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static x(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674093
    sub-int v7, p3, p2

    .line 2674094
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674095
    const/4 v4, 0x0

    .line 2674096
    const/4 v2, 0x0

    .line 2674097
    const/4 v1, 0x0

    .line 2674098
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674099
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2674100
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674101
    if-gez v0, :cond_0

    .line 2674102
    const/4 v0, 0x0

    .line 2674103
    :goto_1
    return-object v0

    .line 2674104
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674105
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674106
    if-lez v1, :cond_2

    .line 2674107
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674108
    add-int/lit8 v1, v1, 0x1

    .line 2674109
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2674110
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674111
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2674112
    goto :goto_0

    .line 2674113
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674114
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674115
    :pswitch_0
    const-string v10, "cuids"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2674116
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674117
    if-nez v3, :cond_3

    .line 2674118
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674119
    :cond_3
    const-string v6, "cuids"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674120
    goto :goto_0

    .line 2674121
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2674122
    goto :goto_0

    .line 2674123
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2674124
    const/4 v0, 0x0

    goto :goto_1

    .line 2674125
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2674126
    const/4 v0, 0x0

    goto :goto_1

    .line 2674127
    :cond_6
    const-string v1, "com.facebook.account.recovery.AccountRecoveryActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5a9dd06
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static y(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2674058
    sub-int v7, p3, p2

    .line 2674059
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2674060
    const/4 v4, 0x0

    .line 2674061
    const/4 v2, 0x0

    .line 2674062
    const/4 v1, 0x0

    .line 2674063
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2674064
    :goto_0
    if-ge v1, v7, :cond_5

    .line 2674065
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2674066
    if-gez v0, :cond_0

    .line 2674067
    const/4 v0, 0x0

    .line 2674068
    :goto_1
    return-object v0

    .line 2674069
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2674070
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2674071
    if-lez v1, :cond_2

    .line 2674072
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2674073
    add-int/lit8 v1, v1, 0x1

    .line 2674074
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_1

    .line 2674075
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674076
    if-eqz v0, :cond_4

    move-object v4, v0

    .line 2674077
    goto :goto_0

    .line 2674078
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674079
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674080
    :pswitch_0
    const-string v10, "confirmation_code"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    .line 2674081
    :pswitch_1
    or-int/lit8 v2, v6, 0x1

    .line 2674082
    if-nez v3, :cond_3

    .line 2674083
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674084
    :cond_3
    const-string v6, "confirmation_code"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674085
    goto :goto_0

    .line 2674086
    :cond_4
    const/4 v0, 0x1

    move v5, v0

    .line 2674087
    goto :goto_0

    .line 2674088
    :cond_5
    packed-switch v6, :pswitch_data_2

    .line 2674089
    const/4 v0, 0x0

    goto :goto_1

    .line 2674090
    :pswitch_2
    if-eqz v5, :cond_6

    .line 2674091
    const/4 v0, 0x0

    goto :goto_1

    .line 2674092
    :cond_6
    const-string v1, "com.facebook.platform.auth.activity.AccountKitConfirmationCodeActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x4bf333b7
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method private static z(Landroid/content/Context;[CIILandroid/os/Bundle;LX/17c;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 2673983
    sub-int v7, p3, p2

    .line 2673984
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, p1, p2, v7}, Ljava/lang/String;-><init>([CII)V

    .line 2673985
    const/4 v4, 0x0

    .line 2673986
    const/4 v2, 0x0

    .line 2673987
    const/4 v1, 0x0

    .line 2673988
    const/4 v0, 0x0

    move v5, v0

    move v6, v2

    move-object v3, p4

    .line 2673989
    :goto_0
    if-ge v1, v7, :cond_d

    .line 2673990
    const/16 v0, 0x3d

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 2673991
    if-gez v0, :cond_0

    .line 2673992
    const/4 v0, 0x0

    .line 2673993
    :goto_1
    return-object v0

    .line 2673994
    :cond_0
    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 2673995
    const/16 v1, 0x26

    invoke-virtual {v8, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 2673996
    if-lez v1, :cond_2

    .line 2673997
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2673998
    add-int/lit8 v1, v1, 0x1

    .line 2673999
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    .line 2674000
    invoke-static {v4, v9, v0}, LX/48d;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2674001
    if-eqz v0, :cond_c

    move-object v4, v0

    .line 2674002
    goto :goto_0

    .line 2674003
    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2674004
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_2

    .line 2674005
    :sswitch_0
    const-string v10, "promo_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v10, "application_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "applink_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "preview_image_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v10, "destination"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x4

    goto :goto_3

    :sswitch_5
    const-string v10, "promo_code"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x5

    goto :goto_3

    :sswitch_6
    const-string v10, "source"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x6

    goto :goto_3

    :sswitch_7
    const-string v10, "sponsored_context"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x7

    goto :goto_3

    :sswitch_8
    const-string v10, "application_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/16 v2, 0x8

    goto :goto_3

    .line 2674006
    :pswitch_0
    or-int/lit8 v2, v6, 0x40

    .line 2674007
    if-nez v3, :cond_3

    .line 2674008
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674009
    :cond_3
    const-string v6, "promo_text"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674010
    goto/16 :goto_0

    .line 2674011
    :pswitch_1
    or-int/lit8 v2, v6, 0x2

    .line 2674012
    if-nez v3, :cond_4

    .line 2674013
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674014
    :cond_4
    const-string v6, "application_name"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674015
    goto/16 :goto_0

    .line 2674016
    :pswitch_2
    or-int/lit8 v2, v6, 0x4

    .line 2674017
    if-nez v3, :cond_5

    .line 2674018
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674019
    :cond_5
    const-string v6, "applink_url"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674020
    goto/16 :goto_0

    .line 2674021
    :pswitch_3
    or-int/lit8 v2, v6, 0x10

    .line 2674022
    if-nez v3, :cond_6

    .line 2674023
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674024
    :cond_6
    const-string v6, "preview_image_url"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674025
    goto/16 :goto_0

    .line 2674026
    :pswitch_4
    or-int/lit8 v2, v6, 0x8

    .line 2674027
    if-nez v3, :cond_7

    .line 2674028
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674029
    :cond_7
    const-string v6, "destination"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674030
    goto/16 :goto_0

    .line 2674031
    :pswitch_5
    or-int/lit8 v2, v6, 0x20

    .line 2674032
    if-nez v3, :cond_8

    .line 2674033
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674034
    :cond_8
    const-string v6, "promo_code"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674035
    goto/16 :goto_0

    .line 2674036
    :pswitch_6
    or-int/lit16 v2, v6, 0x80

    .line 2674037
    if-nez v3, :cond_9

    .line 2674038
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674039
    :cond_9
    const-string v6, "source"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674040
    goto/16 :goto_0

    .line 2674041
    :pswitch_7
    or-int/lit16 v2, v6, 0x100

    .line 2674042
    if-nez v3, :cond_a

    .line 2674043
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674044
    :cond_a
    const-string v6, "sponsored_context"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674045
    goto/16 :goto_0

    .line 2674046
    :pswitch_8
    or-int/lit8 v2, v6, 0x1

    .line 2674047
    if-nez v3, :cond_b

    .line 2674048
    new-instance v3, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 2674049
    :cond_b
    const-string v6, "application_id"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v2

    .line 2674050
    goto/16 :goto_0

    .line 2674051
    :cond_c
    const/4 v0, 0x1

    move v5, v0

    .line 2674052
    goto/16 :goto_0

    .line 2674053
    :cond_d
    packed-switch v6, :pswitch_data_1

    .line 2674054
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674055
    :pswitch_9
    if-eqz v5, :cond_e

    .line 2674056
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2674057
    :cond_e
    const-string v1, "com.facebook.appinvites.sdk.AppInviteDialogActivity"

    const/4 v2, 0x0

    move-object v0, p0

    move-object/from16 v5, p5

    invoke-static/range {v0 .. v5}, LX/48d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;LX/17c;)Landroid/content/Intent;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x5539bbf2 -> :sswitch_4
        -0x4cb85596 -> :sswitch_8
        -0x36ba6b57 -> :sswitch_7
        -0x356f97e5 -> :sswitch_6
        -0x2cdcc32c -> :sswitch_3
        0x9001a -> :sswitch_1
        0x3a17ef1d -> :sswitch_5
        0x3a1f865d -> :sswitch_0
        0x6fece0cb -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1ff
        :pswitch_9
    .end packed-switch
.end method
