.class public final LX/IWK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DLO;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;)V
    .locals 0

    .prologue
    .line 2583695
    iput-object p1, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2583712
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->a:Landroid/content/res/Resources;

    const v1, 0x7f020970

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2583711
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->a:Landroid/content/res/Resources;

    const v1, 0x7f083899

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 2583699
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->a:Landroid/content/res/Resources;

    const v1, 0x7f08389a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2583700
    :goto_0
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ay;

    iget-object v2, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v2, v2, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->h:Ljava/lang/String;

    iget-object v3, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2583701
    new-instance v4, LX/89I;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    sget-object v5, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v4, v6, v7, v5}, LX/89I;-><init>(JLX/2rw;)V

    .line 2583702
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2583703
    iput-object v1, v4, LX/89I;->c:Ljava/lang/String;

    .line 2583704
    :cond_0
    new-instance v5, LX/8AA;

    sget-object v6, LX/8AB;->GROUP:LX/8AB;

    invoke-direct {v5, v6}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v6, LX/21D;->GROUP_FEED:LX/21D;

    const-string v7, "groupPhoto"

    invoke-static {v6, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v4}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    .line 2583705
    iput-object v4, v5, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2583706
    move-object v4, v5

    .line 2583707
    invoke-static {v3, v4}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v4

    move-object v1, v4

    .line 2583708
    const/16 v2, 0x6dc

    iget-object v3, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    invoke-virtual {v0, v1, v2, v3}, LX/1ay;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2583709
    return-void

    .line 2583710
    :cond_1
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->i:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2583696
    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->l:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IWK;->a:Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;

    iget-object v0, v0, Lcom/facebook/groups/photos/fragment/GroupPhotosViewPagerContainerFragment;->l:Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosHeaderModels$FetchGroupPhotosHeaderModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v0, v1, :cond_0

    .line 2583697
    const/4 v0, 0x1

    .line 2583698
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
