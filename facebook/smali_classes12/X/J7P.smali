.class public LX/J7P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/J7J;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/J7H;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Or",
            "<",
            "LX/J7J;",
            ">;",
            "LX/0Or",
            "<",
            "LX/J7H;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650848
    iput-object p1, p0, LX/J7P;->a:LX/03V;

    .line 2650849
    iput-object p2, p0, LX/J7P;->b:LX/0Or;

    .line 2650850
    iput-object p3, p0, LX/J7P;->c:LX/0Or;

    .line 2650851
    iput-object p4, p0, LX/J7P;->d:LX/0Or;

    .line 2650852
    return-void
.end method

.method public static a(LX/0QB;)LX/J7P;
    .locals 7

    .prologue
    .line 2650818
    const-class v1, LX/J7P;

    monitor-enter v1

    .line 2650819
    :try_start_0
    sget-object v0, LX/J7P;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2650820
    sput-object v2, LX/J7P;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2650821
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2650822
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2650823
    new-instance v4, LX/J7P;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v5, 0xb83

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3012

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x3011

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/J7P;-><init>(LX/03V;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2650824
    move-object v0, v4

    .line 2650825
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2650826
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/J7P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2650827
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2650828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/os/Bundle;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2650837
    const-string v0, "operationParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 2650838
    iget-object v0, p0, LX/J7P;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    .line 2650839
    :try_start_0
    invoke-virtual {v0, p2, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2650840
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2650841
    :catch_0
    move-exception v0

    .line 2650842
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2650843
    if-eqz v1, :cond_0

    .line 2650844
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2650845
    :cond_0
    iget-object v3, p0, LX/J7P;->a:LX/03V;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "InfoRequestServiceHandler_"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2650846
    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2650829
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2650830
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2650831
    const-string v2, "timeline_delete_info_request"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2650832
    iget-object v0, p0, LX/J7P;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-direct {p0, v1, v0}, LX/J7P;->a(Landroid/os/Bundle;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2650833
    :goto_0
    return-object v0

    .line 2650834
    :cond_0
    const-string v2, "timeline_send_info_request"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2650835
    iget-object v0, p0, LX/J7P;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-direct {p0, v1, v0}, LX/J7P;->a(Landroid/os/Bundle;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2650836
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
