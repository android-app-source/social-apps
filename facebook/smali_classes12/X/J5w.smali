.class public final LX/J5w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qm;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;)V
    .locals 0

    .prologue
    .line 2649032
    iput-object p1, p0, LX/J5w;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 5

    .prologue
    .line 2649033
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v0

    .line 2649034
    iget-object v0, p0, LX/J5w;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    check-cast v0, LX/J5s;

    .line 2649035
    iget-object v2, v0, LX/J5s;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2649036
    iget-object v0, p0, LX/J5w;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    check-cast v0, LX/J5s;

    .line 2649037
    iget-object v3, v0, LX/J5s;->e:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    move-object v0, v3

    .line 2649038
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 2649039
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "Why would we close a selector when composer info isn\'t even ready?"

    invoke-static {v3, v4}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2649040
    :cond_1
    iget-object v3, p0, LX/J5w;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    .line 2649041
    new-instance v4, LX/8QV;

    invoke-direct {v4}, LX/8QV;-><init>()V

    iget-object p1, v3, LX/J45;->k:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object p1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2649042
    iput-object p1, v4, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2649043
    move-object v4, v4

    .line 2649044
    invoke-virtual {v4, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v4

    invoke-virtual {v4}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v4

    iput-object v4, v3, LX/J45;->k:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2649045
    iget-object v4, v3, LX/J45;->e:LX/J49;

    invoke-virtual {v4, v2, v0, v1}, LX/J49;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;LX/1oT;)V

    .line 2649046
    iget-object v4, v3, LX/J45;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {v4, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2649047
    iget-object v0, p0, LX/J5w;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, 0x7723fc57

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649048
    return-void
.end method
