.class public LX/I6X;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/A5q;

.field private final b:LX/0rq;

.field private final c:LX/0sa;

.field private final d:LX/0ad;

.field private final e:LX/0se;


# direct methods
.method public constructor <init>(LX/A5q;LX/0rq;LX/0sa;LX/0ad;LX/0se;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2537679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2537680
    iput-object p1, p0, LX/I6X;->a:LX/A5q;

    .line 2537681
    iput-object p2, p0, LX/I6X;->b:LX/0rq;

    .line 2537682
    iput-object p3, p0, LX/I6X;->c:LX/0sa;

    .line 2537683
    iput-object p4, p0, LX/I6X;->d:LX/0ad;

    .line 2537684
    iput-object p5, p0, LX/I6X;->e:LX/0se;

    .line 2537685
    return-void
.end method

.method public static a(LX/0QB;)LX/I6X;
    .locals 1

    .prologue
    .line 2537686
    invoke-static {p0}, LX/I6X;->b(LX/0QB;)LX/I6X;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/I6X;
    .locals 6

    .prologue
    .line 2537687
    new-instance v0, LX/I6X;

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v1

    check-cast v1, LX/A5q;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v5

    check-cast v5, LX/0se;

    invoke-direct/range {v0 .. v5}, LX/I6X;-><init>(LX/A5q;LX/0rq;LX/0sa;LX/0ad;LX/0se;)V

    .line 2537688
    return-object v0
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 4

    .prologue
    .line 2537689
    iget-object v0, p0, LX/I6X;->e:LX/0se;

    iget-object v1, p0, LX/I6X;->b:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->c()LX/0wF;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2537690
    const-string v0, "image_large_aspect_width"

    iget-object v1, p0, LX/I6X;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2537691
    const-string v0, "image_large_aspect_height"

    iget-object v1, p0, LX/I6X;->c:LX/0sa;

    invoke-virtual {v1}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2537692
    const-string v0, "include_replies_in_total_count"

    iget-object v1, p0, LX/I6X;->d:LX/0ad;

    sget-short v2, LX/0wg;->i:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537693
    const-string v0, "no_recent_story"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2537694
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 2537695
    const-string v1, "default_image_scale"

    if-nez v0, :cond_0

    sget-object v0, LX/0wB;->a:LX/0wC;

    :cond_0
    invoke-virtual {p1, v1, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2537696
    return-void
.end method
