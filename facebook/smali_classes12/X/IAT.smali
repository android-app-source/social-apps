.class public final LX/IAT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventUserBlockMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IAW;


# direct methods
.method public constructor <init>(LX/IAW;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2545710
    iput-object p1, p0, LX/IAT;->c:LX/IAW;

    iput-object p2, p0, LX/IAT;->a:Landroid/content/Context;

    iput-object p3, p0, LX/IAT;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2545711
    iget-object v0, p0, LX/IAT;->c:LX/IAW;

    iget-object v0, v0, LX/IAW;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/IAT;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081ef8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/IAT;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    const/16 v2, 0x11

    .line 2545712
    iput v2, v1, LX/27k;->b:I

    .line 2545713
    move-object v1, v1

    .line 2545714
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2545715
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2545716
    iget-object v0, p0, LX/IAT;->c:LX/IAW;

    iget-object v0, v0, LX/IAW;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/IAT;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081ef7

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/IAT;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    const/16 v2, 0x11

    .line 2545717
    iput v2, v1, LX/27k;->b:I

    .line 2545718
    move-object v1, v1

    .line 2545719
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2545720
    return-void
.end method
