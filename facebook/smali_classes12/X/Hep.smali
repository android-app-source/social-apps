.class public LX/Hep;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public b:LX/0wW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:I

.field public final d:Landroid/view/View;

.field public final e:Landroid/view/View;

.field public final f:LX/0wd;

.field private final g:LX/0xh;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2490830
    new-instance v0, LX/0wT;

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    sput-object v0, LX/Hep;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2490835
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Hep;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2490836
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2490833
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Hep;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490834
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2490837
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490838
    new-instance v0, LX/Heo;

    invoke-direct {v0, p0}, LX/Heo;-><init>(LX/Hep;)V

    iput-object v0, p0, LX/Hep;->g:LX/0xh;

    .line 2490839
    const-class v0, LX/Hep;

    invoke-static {v0, p0}, LX/Hep;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2490840
    iget-object v0, p0, LX/Hep;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/Hep;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 2490841
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2490842
    move-object v0, v0

    .line 2490843
    iget-object v1, p0, LX/Hep;->g:LX/0xh;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Hep;->f:LX/0wd;

    .line 2490844
    const v0, 0x7f031608

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2490845
    const v0, 0x7f0d05d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Hep;->d:Landroid/view/View;

    .line 2490846
    const v0, 0x7f0d318a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Hep;->e:Landroid/view/View;

    .line 2490847
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Hep;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p0

    check-cast p0, LX/0wW;

    iput-object p0, p1, LX/Hep;->b:LX/0wW;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 2490831
    iget-object v0, p0, LX/Hep;->f:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2490832
    return-void
.end method
