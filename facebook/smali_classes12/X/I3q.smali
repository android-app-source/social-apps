.class public LX/I3q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5Z;


# instance fields
.field private final a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

.field private final b:LX/I3p;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2531803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2531804
    iput-object p1, p0, LX/I3q;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    .line 2531805
    new-instance v0, LX/I3p;

    invoke-virtual {p1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->b()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->mD_()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/I3p;-><init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;Ljava/lang/String;)V

    iput-object v0, p0, LX/I3q;->b:LX/I3p;

    .line 2531806
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$FeaturedArticlesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531797
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531802
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531801
    iget-object v0, p0, LX/I3q;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->b()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/B5Y;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531800
    iget-object v0, p0, LX/I3q;->b:LX/I3p;

    return-object v0
.end method

.method public final iS_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlInterfaces$InstantArticleMaster$RelatedArticlesSections;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531799
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531798
    const/4 v0, 0x0

    return-object v0
.end method
