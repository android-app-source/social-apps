.class public final enum LX/HaS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HaS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HaS;

.field public static final enum NULL:LX/HaS;

.field public static final enum OK:LX/HaS;

.field public static final enum STRONG:LX/HaS;

.field public static final enum WEAK:LX/HaS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2483868
    new-instance v0, LX/HaS;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v2}, LX/HaS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaS;->NULL:LX/HaS;

    .line 2483869
    new-instance v0, LX/HaS;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v3}, LX/HaS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaS;->WEAK:LX/HaS;

    .line 2483870
    new-instance v0, LX/HaS;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4}, LX/HaS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaS;->OK:LX/HaS;

    .line 2483871
    new-instance v0, LX/HaS;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v5}, LX/HaS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaS;->STRONG:LX/HaS;

    .line 2483872
    const/4 v0, 0x4

    new-array v0, v0, [LX/HaS;

    sget-object v1, LX/HaS;->NULL:LX/HaS;

    aput-object v1, v0, v2

    sget-object v1, LX/HaS;->WEAK:LX/HaS;

    aput-object v1, v0, v3

    sget-object v1, LX/HaS;->OK:LX/HaS;

    aput-object v1, v0, v4

    sget-object v1, LX/HaS;->STRONG:LX/HaS;

    aput-object v1, v0, v5

    sput-object v0, LX/HaS;->$VALUES:[LX/HaS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2483873
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HaS;
    .locals 1

    .prologue
    .line 2483874
    const-class v0, LX/HaS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HaS;

    return-object v0
.end method

.method public static values()[LX/HaS;
    .locals 1

    .prologue
    .line 2483875
    sget-object v0, LX/HaS;->$VALUES:[LX/HaS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HaS;

    return-object v0
.end method
