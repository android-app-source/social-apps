.class public final LX/IO0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

.field public final synthetic d:LX/IO1;


# direct methods
.method public constructor <init>(LX/IO1;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;ZLcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;)V
    .locals 0

    .prologue
    .line 2571328
    iput-object p1, p0, LX/IO0;->d:LX/IO1;

    iput-object p2, p0, LX/IO0;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    iput-boolean p3, p0, LX/IO0;->b:Z

    iput-object p4, p0, LX/IO0;->c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2571329
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    iget-object v1, p0, LX/IO0;->c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    invoke-static {v0, v1}, LX/IO3;->setJoinButtonState(LX/IO3;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 2571330
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    iget-object v1, p0, LX/IO0;->c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2571331
    iput-object v1, v0, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2571332
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    iget-object v1, v0, LX/IO3;->d:LX/0kL;

    new-instance v2, LX/27k;

    const-string v3, "%s\n\n%s"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v5, p0, LX/IO0;->d:LX/IO1;

    iget-object v5, v5, LX/IO1;->c:LX/IO3;

    invoke-virtual {v5}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f08003c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x1

    iget-boolean v0, p0, LX/IO0;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    invoke-virtual {v0}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f082ff0

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2571333
    return-void

    .line 2571334
    :cond_0
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    invoke-virtual {v0}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f082ff1

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2571335
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->b:LX/IN0;

    if-eqz v0, :cond_0

    .line 2571336
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->b:LX/IN0;

    invoke-virtual {v0}, LX/IN0;->a()V

    .line 2571337
    :cond_0
    iget-object v0, p0, LX/IO0;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/IO0;->b:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LX/IO0;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/IO0;->b:Z

    if-nez v0, :cond_3

    .line 2571338
    :cond_2
    iget-object v0, p0, LX/IO0;->d:LX/IO1;

    iget-object v0, v0, LX/IO1;->c:LX/IO3;

    iget-object v0, v0, LX/IO3;->g:LX/DK3;

    new-instance v1, LX/DK7;

    invoke-direct {v1}, LX/DK7;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2571339
    :cond_3
    return-void
.end method
