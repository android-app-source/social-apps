.class public final LX/HI4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    .line 2450008
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2450009
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2450010
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2450011
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2450012
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_6

    .line 2450013
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450014
    :goto_1
    move v1, v2

    .line 2450015
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2450016
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 2450017
    :cond_1
    const-string v7, "unread_count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2450018
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v4, v1

    move v1, v3

    .line 2450019
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 2450020
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2450021
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_2

    if-eqz v6, :cond_2

    .line 2450023
    const-string v7, "feed_type_enum"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2450024
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_2

    .line 2450025
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 2450026
    :cond_4
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2450027
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2450028
    if-eqz v1, :cond_5

    .line 2450029
    invoke-virtual {p1, v3, v4, v2}, LX/186;->a(III)V

    .line 2450030
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v4, v2

    move v5, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2450031
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2450032
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2450033
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 2450034
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2450035
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 2450036
    if-eqz v2, :cond_0

    .line 2450037
    const-string v2, "feed_type_enum"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450038
    invoke-virtual {p0, v1, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2450039
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 2450040
    if-eqz v2, :cond_1

    .line 2450041
    const-string p3, "unread_count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450042
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2450043
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450044
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2450045
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2450046
    return-void
.end method
