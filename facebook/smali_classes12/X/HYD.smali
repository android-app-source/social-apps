.class public LX/HYD;
.super LX/HYA;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HYD;


# instance fields
.field private a:LX/4hz;


# direct methods
.method public constructor <init>(LX/4hz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480332
    invoke-direct {p0}, LX/HYA;-><init>()V

    .line 2480333
    iput-object p1, p0, LX/HYD;->a:LX/4hz;

    .line 2480334
    return-void
.end method

.method public static a(LX/0QB;)LX/HYD;
    .locals 4

    .prologue
    .line 2480335
    sget-object v0, LX/HYD;->b:LX/HYD;

    if-nez v0, :cond_1

    .line 2480336
    const-class v1, LX/HYD;

    monitor-enter v1

    .line 2480337
    :try_start_0
    sget-object v0, LX/HYD;->b:LX/HYD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480338
    if-eqz v2, :cond_0

    .line 2480339
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480340
    new-instance p0, LX/HYD;

    invoke-static {v0}, LX/4hz;->a(LX/0QB;)LX/4hz;

    move-result-object v3

    check-cast v3, LX/4hz;

    invoke-direct {p0, v3}, LX/HYD;-><init>(LX/4hz;)V

    .line 2480341
    move-object v0, p0

    .line 2480342
    sput-object v0, LX/HYD;->b:LX/HYD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480343
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480344
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480345
    :cond_1
    sget-object v0, LX/HYD;->b:LX/HYD;

    return-object v0

    .line 2480346
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480347
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)LX/HYB;
    .locals 3

    .prologue
    .line 2480348
    new-instance v0, LX/HYC;

    iget-object v1, p0, LX/HYD;->a:LX/4hz;

    invoke-direct {v0, v1, p1}, LX/HYC;-><init>(LX/4hz;Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480349
    const-string v0, "close"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2480350
    const-string v0, "function(results,error){bridge(\'%s\',{\'results\':results,\'error\':error});}"

    invoke-virtual {p0}, LX/HYA;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
