.class public final LX/JO2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JO3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JO3;


# direct methods
.method public constructor <init>(LX/JO3;)V
    .locals 1

    .prologue
    .line 2686817
    iput-object p1, p0, LX/JO2;->c:LX/JO3;

    .line 2686818
    move-object v0, p1

    .line 2686819
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2686820
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2686821
    const-string v0, "ConnectWithFacebookHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2686803
    if-ne p0, p1, :cond_1

    .line 2686804
    :cond_0
    :goto_0
    return v0

    .line 2686805
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2686806
    goto :goto_0

    .line 2686807
    :cond_3
    check-cast p1, LX/JO2;

    .line 2686808
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2686809
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2686810
    if-eq v2, v3, :cond_0

    .line 2686811
    iget-object v2, p0, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2686812
    goto :goto_0

    .line 2686813
    :cond_5
    iget-object v2, p1, LX/JO2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2686814
    :cond_6
    iget-object v2, p0, LX/JO2;->b:LX/1Pq;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JO2;->b:LX/1Pq;

    iget-object v3, p1, LX/JO2;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2686815
    goto :goto_0

    .line 2686816
    :cond_7
    iget-object v2, p1, LX/JO2;->b:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
