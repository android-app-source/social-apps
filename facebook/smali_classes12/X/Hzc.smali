.class public final enum LX/Hzc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hzc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hzc;

.field public static final enum FETCH_PAST_EVENTS:LX/Hzc;

.field public static final enum FETCH_UPCOMING_EVENTS:LX/Hzc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2525817
    new-instance v0, LX/Hzc;

    const-string v1, "FETCH_UPCOMING_EVENTS"

    invoke-direct {v0, v1, v2}, LX/Hzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hzc;->FETCH_UPCOMING_EVENTS:LX/Hzc;

    .line 2525818
    new-instance v0, LX/Hzc;

    const-string v1, "FETCH_PAST_EVENTS"

    invoke-direct {v0, v1, v3}, LX/Hzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hzc;->FETCH_PAST_EVENTS:LX/Hzc;

    .line 2525819
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hzc;

    sget-object v1, LX/Hzc;->FETCH_UPCOMING_EVENTS:LX/Hzc;

    aput-object v1, v0, v2

    sget-object v1, LX/Hzc;->FETCH_PAST_EVENTS:LX/Hzc;

    aput-object v1, v0, v3

    sput-object v0, LX/Hzc;->$VALUES:[LX/Hzc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2525820
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hzc;
    .locals 1

    .prologue
    .line 2525821
    const-class v0, LX/Hzc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hzc;

    return-object v0
.end method

.method public static values()[LX/Hzc;
    .locals 1

    .prologue
    .line 2525822
    sget-object v0, LX/Hzc;->$VALUES:[LX/Hzc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hzc;

    return-object v0
.end method
