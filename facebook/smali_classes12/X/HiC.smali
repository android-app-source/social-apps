.class public final LX/HiC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final f:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/HiB;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:[F

.field public final c:[I

.field public final d:Landroid/util/SparseIntArray;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HiH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2497042
    const-class v0, LX/HiC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HiC;->a:Ljava/lang/String;

    .line 2497043
    new-instance v0, LX/HiA;

    invoke-direct {v0}, LX/HiA;-><init>()V

    sput-object v0, LX/HiC;->f:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/HiD;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2497018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497019
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, LX/HiC;->b:[F

    .line 2497020
    iget v0, p1, LX/HiD;->c:I

    move v2, v0

    .line 2497021
    iget-object v0, p1, LX/HiD;->a:[I

    move-object v4, v0

    .line 2497022
    iget-object v0, p1, LX/HiD;->b:[I

    move-object v3, v0

    .line 2497023
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, LX/HiC;->d:Landroid/util/SparseIntArray;

    move v0, v1

    .line 2497024
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 2497025
    iget-object v5, p0, LX/HiC;->d:Landroid/util/SparseIntArray;

    aget v6, v4, v0

    aget v7, v3, v0

    invoke-virtual {v5, v6, v7}, Landroid/util/SparseIntArray;->append(II)V

    .line 2497026
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2497027
    :cond_0
    new-array v0, v2, [I

    iput-object v0, p0, LX/HiC;->c:[I

    .line 2497028
    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v5, :cond_1

    aget v6, v4, v3

    .line 2497029
    iget-object v0, p0, LX/HiC;->b:[F

    .line 2497030
    invoke-static {v6}, Landroid/graphics/Color;->red(I)I

    move-result v7

    invoke-static {v6}, Landroid/graphics/Color;->green(I)I

    move-result v8

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    invoke-static {v7, v8, p1, v0}, LX/3qk;->a(III[F)V

    .line 2497031
    iget-object v0, p0, LX/HiC;->b:[F

    invoke-static {v0}, LX/HiC;->a([F)Z

    move-result v0

    move v0, v0

    .line 2497032
    if-nez v0, :cond_4

    .line 2497033
    iget-object v7, p0, LX/HiC;->c:[I

    add-int/lit8 v0, v2, 0x1

    aput v6, v7, v2

    .line 2497034
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 2497035
    :cond_1
    if-gt v2, p2, :cond_2

    .line 2497036
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HiC;->e:Ljava/util/List;

    .line 2497037
    iget-object v0, p0, LX/HiC;->c:[I

    array-length v2, v0

    :goto_3
    if-ge v1, v2, :cond_3

    aget v3, v0, v1

    .line 2497038
    iget-object v4, p0, LX/HiC;->e:Ljava/util/List;

    new-instance v5, LX/HiH;

    iget-object v6, p0, LX/HiC;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    invoke-direct {v5, v3, v6}, LX/HiH;-><init>(II)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2497039
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2497040
    :cond_2
    add-int/lit8 v0, v2, -0x1

    invoke-direct {p0, v0, p2}, LX/HiC;->a(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/HiC;->e:Ljava/util/List;

    .line 2497041
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private a(II)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "LX/HiH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2496991
    new-instance v0, Ljava/util/PriorityQueue;

    sget-object v1, LX/HiC;->f:Ljava/util/Comparator;

    invoke-direct {v0, p2, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 2496992
    new-instance v1, LX/HiB;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, p1}, LX/HiB;-><init>(LX/HiC;II)V

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 2496993
    :goto_0
    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v1

    if-ge v1, p2, :cond_0

    .line 2496994
    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HiB;

    .line 2496995
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/HiB;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2496996
    invoke-virtual {v1}, LX/HiB;->c()LX/HiB;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 2496997
    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2496998
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2496999
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HiB;

    .line 2497000
    const/4 v5, 0x0

    .line 2497001
    iget v4, v1, LX/HiB;->b:I

    move v6, v5

    move v7, v5

    move p0, v5

    :goto_2
    iget p1, v1, LX/HiB;->c:I

    if-gt v4, p1, :cond_2

    .line 2497002
    iget-object p1, v1, LX/HiB;->a:LX/HiC;

    iget-object p1, p1, LX/HiC;->c:[I

    aget p1, p1, v4

    .line 2497003
    iget-object p2, v1, LX/HiB;->a:LX/HiC;

    iget-object p2, p2, LX/HiC;->d:Landroid/util/SparseIntArray;

    invoke-virtual {p2, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result p2

    .line 2497004
    add-int/2addr v5, p2

    .line 2497005
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    mul-int/2addr v0, p2

    add-int/2addr p0, v0

    .line 2497006
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    mul-int/2addr v0, p2

    add-int/2addr v7, v0

    .line 2497007
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    mul-int/2addr p1, p2

    add-int/2addr v6, p1

    .line 2497008
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2497009
    :cond_2
    int-to-float v4, p0

    int-to-float p0, v5

    div-float/2addr v4, p0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 2497010
    int-to-float v7, v7

    int-to-float p0, v5

    div-float/2addr v7, p0

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 2497011
    int-to-float v6, v6

    int-to-float p0, v5

    div-float/2addr v6, p0

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 2497012
    new-instance p0, LX/HiH;

    invoke-direct {p0, v4, v7, v6, v5}, LX/HiH;-><init>(IIII)V

    move-object v1, p0

    .line 2497013
    invoke-virtual {v1}, LX/HiH;->b()[F

    move-result-object v4

    invoke-static {v4}, LX/HiC;->a([F)Z

    move-result v4

    move v4, v4

    .line 2497014
    if-nez v4, :cond_1

    .line 2497015
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2497016
    :cond_3
    move-object v0, v2

    .line 2497017
    return-object v0
.end method

.method public static a([F)Z
    .locals 4

    .prologue
    .line 2496975
    const/4 v0, 0x2

    aget v0, p0, v0

    const v1, 0x3f733333    # 0.95f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2496976
    if-nez v0, :cond_0

    .line 2496977
    const/4 v0, 0x2

    aget v0, p0, v0

    const v1, 0x3d4ccccd    # 0.05f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2496978
    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2496979
    aget v2, p0, v1

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    aget v2, p0, v1

    const/high16 v3, 0x42140000    # 37.0f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    aget v2, p0, v0

    const v3, 0x3f51eb85    # 0.82f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    :goto_2
    move v0, v0

    .line 2496980
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static a$redex0(LX/HiC;III)V
    .locals 4

    .prologue
    .line 2496981
    packed-switch p1, :pswitch_data_0

    .line 2496982
    :cond_0
    :pswitch_0
    return-void

    .line 2496983
    :goto_0
    :pswitch_1
    if-gt p2, p3, :cond_0

    .line 2496984
    iget-object v0, p0, LX/HiC;->c:[I

    aget v0, v0, p2

    .line 2496985
    iget-object v1, p0, LX/HiC;->c:[I

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    shr-int/lit8 v3, v0, 0x10

    and-int/lit16 v3, v3, 0xff

    and-int/lit16 v0, v0, 0xff

    invoke-static {v2, v3, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    aput v0, v1, p2

    .line 2496986
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 2496987
    :goto_1
    :pswitch_2
    if-gt p2, p3, :cond_0

    .line 2496988
    iget-object v0, p0, LX/HiC;->c:[I

    aget v0, v0, p2

    .line 2496989
    iget-object v1, p0, LX/HiC;->c:[I

    and-int/lit16 v2, v0, 0xff

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v0, v0, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-static {v2, v3, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    aput v0, v1, p2

    .line 2496990
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
