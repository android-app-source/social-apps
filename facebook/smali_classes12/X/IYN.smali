.class public LX/IYN;
.super LX/Cod;
.source ""

# interfaces
.implements LX/IYM;
.implements LX/CnL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/IXg;",
        ">;",
        "LX/IYM;",
        "LX/CnL;"
    }
.end annotation


# instance fields
.field private final a:LX/CnR;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2587737
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2587738
    check-cast p1, LX/CnR;

    iput-object p1, p0, LX/IYN;->a:LX/CnR;

    .line 2587739
    iget-object v0, p0, LX/IYN;->a:LX/CnR;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CnR;->setIsOverlay(Z)V

    .line 2587740
    return-void
.end method

.method private d()LX/CnL;
    .locals 1

    .prologue
    .line 2587719
    iget-object v0, p0, LX/IYN;->a:LX/CnR;

    instance-of v0, v0, LX/CnL;

    if-eqz v0, :cond_0

    .line 2587720
    iget-object v0, p0, LX/IYN;->a:LX/CnR;

    check-cast v0, LX/CnL;

    .line 2587721
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/ClW;)V
    .locals 1

    .prologue
    .line 2587735
    iget-object v0, p0, LX/IYN;->a:LX/CnR;

    invoke-virtual {v0, p1}, LX/CnR;->setAnnotation(LX/ClW;)V

    .line 2587736
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2587734
    invoke-direct {p0}, LX/IYN;->d()LX/CnL;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setFeedbackHeaderAuthorByline(LX/CmD;)V
    .locals 1
    .param p1    # LX/CmD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587741
    invoke-direct {p0}, LX/IYN;->d()LX/CnL;

    move-result-object v0

    .line 2587742
    if-eqz v0, :cond_0

    .line 2587743
    invoke-interface {v0, p1}, LX/CnL;->setFeedbackHeaderAuthorByline(LX/CmD;)V

    .line 2587744
    :cond_0
    return-void
.end method

.method public final setFeedbackHeaderTitle(LX/Cly;)V
    .locals 1
    .param p1    # LX/Cly;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587730
    invoke-direct {p0}, LX/IYN;->d()LX/CnL;

    move-result-object v0

    .line 2587731
    if-eqz v0, :cond_0

    .line 2587732
    invoke-interface {v0, p1}, LX/CnL;->setFeedbackHeaderTitle(LX/Cly;)V

    .line 2587733
    :cond_0
    return-void
.end method

.method public final setLogoInformation(LX/CmQ;)V
    .locals 1
    .param p1    # LX/CmQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587726
    invoke-direct {p0}, LX/IYN;->d()LX/CnL;

    move-result-object v0

    .line 2587727
    if-eqz v0, :cond_0

    .line 2587728
    invoke-interface {v0, p1}, LX/CnL;->setLogoInformation(LX/CmQ;)V

    .line 2587729
    :cond_0
    return-void
.end method

.method public final setShareUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587722
    invoke-direct {p0}, LX/IYN;->d()LX/CnL;

    move-result-object v0

    .line 2587723
    if-eqz v0, :cond_0

    .line 2587724
    invoke-interface {v0, p1}, LX/CnL;->setShareUrl(Ljava/lang/String;)V

    .line 2587725
    :cond_0
    return-void
.end method
