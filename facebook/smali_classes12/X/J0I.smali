.class public LX/J0I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;",
        "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2636826
    const-class v0, LX/J0I;

    sput-object v0, LX/J0I;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636828
    return-void
.end method

.method public static a(LX/0QB;)LX/J0I;
    .locals 1

    .prologue
    .line 2636823
    new-instance v0, LX/J0I;

    invoke-direct {v0}, LX/J0I;-><init>()V

    .line 2636824
    move-object v0, v0

    .line 2636825
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2636804
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;

    .line 2636805
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636806
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "validate_payment_card_bin"

    .line 2636807
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636808
    move-object v1, v1

    .line 2636809
    const-string v2, "GET"

    .line 2636810
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636811
    move-object v1, v1

    .line 2636812
    const-string v2, "/P2P_BIN_%s"

    .line 2636813
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2636814
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2636815
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636816
    move-object v1, v1

    .line 2636817
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636818
    move-object v0, v1

    .line 2636819
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2636820
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636821
    move-object v0, v0

    .line 2636822
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2636801
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636802
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2636803
    const-class v1, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;

    return-object v0
.end method
