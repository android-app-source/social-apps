.class public final LX/JGy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5pC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I

.field public c:[I

.field public d:[Lcom/facebook/react/uimanager/ReactShadowNode;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2669574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2669575
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, LX/JGy;->c:[I

    .line 2669576
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/react/uimanager/ReactShadowNode;

    iput-object v0, p0, LX/JGy;->d:[Lcom/facebook/react/uimanager/ReactShadowNode;

    return-void
.end method

.method public static a(LX/JGy;III)V
    .locals 2

    .prologue
    .line 2669577
    iget-object v0, p0, LX/JGy;->c:[I

    .line 2669578
    mul-int/lit8 v1, p1, 0x2

    move v1, v1

    .line 2669579
    aput p2, v0, v1

    .line 2669580
    iget-object v0, p0, LX/JGy;->c:[I

    invoke-static {p1}, LX/JGy;->e(I)I

    move-result v1

    aput p3, v0, v1

    .line 2669581
    return-void
.end method

.method private static e(I)I
    .locals 1

    .prologue
    .line 2669582
    mul-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static f(LX/JGy;I)I
    .locals 2

    .prologue
    .line 2669583
    iget-object v0, p0, LX/JGy;->c:[I

    .line 2669584
    mul-int/lit8 v1, p1, 0x2

    move v1, v1

    .line 2669585
    aget v0, v0, v1

    return v0
.end method

.method public static g(LX/JGy;I)I
    .locals 2

    .prologue
    .line 2669586
    iget-object v0, p0, LX/JGy;->c:[I

    invoke-static {p1}, LX/JGy;->e(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public static j(LX/JGy;I)V
    .locals 3

    .prologue
    .line 2669587
    move v0, p1

    :goto_0
    iget v1, p0, LX/JGy;->b:I

    if-ge v0, v1, :cond_0

    .line 2669588
    iget-object v1, p0, LX/JGy;->d:[Lcom/facebook/react/uimanager/ReactShadowNode;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 2669589
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2669590
    :cond_0
    iput p1, p0, LX/JGy;->b:I

    .line 2669591
    return-void
.end method


# virtual methods
.method public final b(I)I
    .locals 1

    .prologue
    .line 2669592
    invoke-static {p0, p1}, LX/JGy;->g(LX/JGy;I)I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 2669593
    iget-object v0, p0, LX/JGy;->a:LX/5pC;

    .line 2669594
    move-object v0, v0

    .line 2669595
    check-cast v0, LX/5pC;

    invoke-interface {v0, p1}, LX/5pC;->getInt(I)I

    move-result v0

    move v0, v0

    .line 2669596
    return v0
.end method
