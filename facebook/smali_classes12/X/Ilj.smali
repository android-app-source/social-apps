.class public LX/Ilj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608102
    const-class v0, LX/Ilj;

    sput-object v0, LX/Ilj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;Landroid/content/res/Resources;LX/03V;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608104
    iput-object p1, p0, LX/Ilj;->b:LX/0Or;

    .line 2608105
    iput-object p2, p0, LX/Ilj;->c:Landroid/content/res/Resources;

    .line 2608106
    iput-object p3, p0, LX/Ilj;->d:LX/03V;

    .line 2608107
    return-void
.end method

.method public static a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;Z)LX/DtN;
    .locals 2

    .prologue
    .line 2608108
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->k()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    .line 2608109
    :goto_0
    new-instance v1, LX/Ilh;

    invoke-direct {v1, v0}, LX/Ilh;-><init>(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;)V

    return-object v1

    .line 2608110
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v0

    goto :goto_0
.end method
