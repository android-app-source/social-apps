.class public LX/Ird;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Iqg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IrC;

.field public c:LX/Iqg;

.field public d:I

.field public e:I

.field public f:I

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2618508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618509
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ird;->a:Ljava/util/List;

    .line 2618510
    new-instance v0, LX/IrC;

    invoke-direct {v0}, LX/IrC;-><init>()V

    iput-object v0, p0, LX/Ird;->b:LX/IrC;

    .line 2618511
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ird;->g:Ljava/util/HashMap;

    .line 2618512
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ird;->h:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final a(I)LX/Iqg;
    .locals 1

    .prologue
    .line 2618505
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2618506
    :cond_0
    const/4 v0, 0x0

    .line 2618507
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqg;

    goto :goto_0
.end method

.method public final a(LX/Iqg;)V
    .locals 6

    .prologue
    .line 2618485
    instance-of v0, p1, LX/IsC;

    if-eqz v0, :cond_1

    .line 2618486
    iget v0, p0, LX/Ird;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ird;->d:I

    .line 2618487
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2618488
    iget-object v0, p0, LX/Ird;->b:LX/IrC;

    new-instance v1, LX/IqC;

    iget-object v2, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, p1, v2}, LX/IqC;-><init>(LX/Iqg;I)V

    invoke-virtual {v0, v1}, LX/IrC;->a(Ljava/lang/Object;)V

    .line 2618489
    invoke-virtual {p0, p1}, LX/Ird;->b(LX/Iqg;)V

    .line 2618490
    return-void

    .line 2618491
    :cond_1
    instance-of v0, p1, LX/Irx;

    if-eqz v0, :cond_2

    .line 2618492
    iget v0, p0, LX/Ird;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ird;->e:I

    move-object v0, p1

    .line 2618493
    check-cast v0, LX/Irx;

    .line 2618494
    iget-object v1, v0, LX/Irx;->a:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v1

    .line 2618495
    const/4 v5, 0x1

    .line 2618496
    iget-object v1, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2618497
    iget-object v1, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618498
    :goto_1
    iget-object v1, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2618499
    iget-object v1, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618500
    :goto_2
    goto :goto_0

    .line 2618501
    :cond_2
    instance-of v0, p1, LX/Iqh;

    if-eqz v0, :cond_0

    .line 2618502
    iget v0, p0, LX/Ird;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ird;->f:I

    goto :goto_0

    .line 2618503
    :cond_3
    iget-object v2, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object v1, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2618504
    :cond_4
    iget-object v2, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v1, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public final b(LX/Iqg;)V
    .locals 3

    .prologue
    .line 2618447
    iget-object v0, p0, LX/Ird;->c:LX/Iqg;

    if-ne p1, v0, :cond_0

    .line 2618448
    :goto_0
    return-void

    .line 2618449
    :cond_0
    iget-object v0, p0, LX/Ird;->c:LX/Iqg;

    .line 2618450
    iput-object p1, p0, LX/Ird;->c:LX/Iqg;

    .line 2618451
    iget-object v1, p0, LX/Ird;->b:LX/IrC;

    new-instance v2, LX/Irt;

    invoke-direct {v2, p1, v0}, LX/Irt;-><init>(LX/Iqg;LX/Iqg;)V

    invoke-virtual {v1, v2}, LX/IrC;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(LX/Iqg;)V
    .locals 6

    .prologue
    .line 2618459
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 2618460
    if-gez v1, :cond_1

    .line 2618461
    :cond_0
    :goto_0
    return-void

    .line 2618462
    :cond_1
    instance-of v0, p1, LX/IsC;

    if-eqz v0, :cond_3

    .line 2618463
    iget v0, p0, LX/Ird;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/Ird;->d:I

    .line 2618464
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2618465
    iget-object v0, p0, LX/Ird;->b:LX/IrC;

    new-instance v2, LX/IqY;

    invoke-direct {v2, p1}, LX/IqY;-><init>(LX/Iqg;)V

    invoke-virtual {v0, v2}, LX/IrC;->a(Ljava/lang/Object;)V

    .line 2618466
    iget-object v0, p0, LX/Ird;->c:LX/Iqg;

    if-ne p1, v0, :cond_0

    .line 2618467
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2618468
    if-ltz v0, :cond_7

    iget-object v1, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqg;

    :goto_2
    invoke-virtual {p0, v0}, LX/Ird;->b(LX/Iqg;)V

    goto :goto_0

    .line 2618469
    :cond_3
    instance-of v0, p1, LX/Irx;

    if-eqz v0, :cond_6

    .line 2618470
    iget v0, p0, LX/Ird;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/Ird;->e:I

    move-object v0, p1

    .line 2618471
    check-cast v0, LX/Irx;

    .line 2618472
    iget-object v2, v0, LX/Irx;->a:Lcom/facebook/stickers/model/Sticker;

    move-object v0, v2

    .line 2618473
    iget-object v2, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2618474
    iget-object v3, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v5, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618475
    iget-object v2, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_4

    .line 2618476
    iget-object v2, p0, LX/Ird;->g:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618477
    :cond_4
    iget-object v2, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2618478
    iget-object v3, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v5, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618479
    iget-object v2, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_5

    .line 2618480
    iget-object v2, p0, LX/Ird;->h:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618481
    :cond_5
    goto/16 :goto_1

    .line 2618482
    :cond_6
    instance-of v0, p1, LX/Iqh;

    if-eqz v0, :cond_2

    .line 2618483
    iget v0, p0, LX/Ird;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/Ird;->f:I

    goto/16 :goto_1

    .line 2618484
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 2618458
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2618452
    iget-object v0, p0, LX/Ird;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqg;

    .line 2618453
    const/4 v2, 0x1

    .line 2618454
    iput-boolean v2, v0, LX/Iqg;->l:Z

    .line 2618455
    sget-object p0, LX/Iqn;->VISIBILITY_CHANGED:LX/Iqn;

    invoke-virtual {v0, p0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2618456
    goto :goto_0

    .line 2618457
    :cond_0
    return-void
.end method
