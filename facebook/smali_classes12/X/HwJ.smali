.class public final LX/HwJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASe;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V
    .locals 0

    .prologue
    .line 2520383
    iput-object p1, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2520384
    iget-object v0, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->n(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    .line 2520385
    return-void
.end method

.method public final a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V
    .locals 2

    .prologue
    .line 2520386
    iget-object v0, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    iget-object v1, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v1}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v1}, LX/7kq;->a(ILcom/facebook/composer/attachments/ComposerAttachment;LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1, p3, p4}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V

    .line 2520387
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 4

    .prologue
    .line 2520388
    iget-object v0, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    iget-object v1, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v1}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/7kq;->a(LX/0Px;Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V

    .line 2520389
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;J)V
    .locals 0

    .prologue
    .line 2520390
    return-void
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/photos/base/tagging/FaceBox;)V
    .locals 3
    .param p2    # Lcom/facebook/photos/base/tagging/FaceBox;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2520391
    iget-object v0, p0, LX/HwJ;->a:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    .line 2520392
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 2520393
    sget-object v1, LX/HwL;->a:[I

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object p0

    invoke-virtual {p0}, LX/4gF;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    .line 2520394
    :cond_0
    :goto_0
    return-void

    .line 2520395
    :pswitch_0
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v1}, LX/HwE;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2520396
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v1}, LX/HwE;->C()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2520397
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HwW;

    invoke-virtual {v1}, LX/HwW;->newBuilder()LX/HwV;

    move-result-object v1

    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {p0}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object p0

    .line 2520398
    iput-object p0, v1, LX/HwV;->b:LX/0Px;

    .line 2520399
    move-object v1, v1

    .line 2520400
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {p0}, LX/HwE;->getSessionId()Ljava/lang/String;

    move-result-object p0

    .line 2520401
    iput-object p0, v1, LX/HwV;->c:Ljava/lang/String;

    .line 2520402
    move-object v1, v1

    .line 2520403
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {p0}, LX/HwE;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object p0

    .line 2520404
    iput-object p0, v1, LX/HwV;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2520405
    move-object v1, v1

    .line 2520406
    const/16 p0, 0x3ea

    .line 2520407
    iput p0, v1, LX/HwV;->e:I

    .line 2520408
    move-object v1, v1

    .line 2520409
    iput-object v0, v1, LX/HwV;->f:Landroid/support/v4/app/Fragment;

    .line 2520410
    move-object p0, v1

    .line 2520411
    move-object v1, v2

    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2520412
    iput-object v1, p0, LX/HwV;->g:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2520413
    move-object v1, p0

    .line 2520414
    iput-object p2, v1, LX/HwV;->h:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 2520415
    move-object v1, v1

    .line 2520416
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520417
    iget-object p0, v2, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    move-object v2, p0

    .line 2520418
    iput-object v2, v1, LX/HwV;->i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2520419
    move-object v1, v1

    .line 2520420
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520421
    iget-boolean p0, v2, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    move v2, p0

    .line 2520422
    iput-boolean v2, v1, LX/HwV;->j:Z

    .line 2520423
    move-object v1, v1

    .line 2520424
    invoke-virtual {v1}, LX/HwV;->a()V

    goto :goto_0

    .line 2520425
    :cond_1
    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->n(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 0

    .prologue
    .line 2520426
    return-void
.end method
