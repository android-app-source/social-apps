.class public LX/IVD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/IV8;

.field public final b:LX/1vg;

.field public final c:LX/IQY;


# direct methods
.method public constructor <init>(LX/IV8;LX/1vg;LX/IQY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2581555
    iput-object p1, p0, LX/IVD;->a:LX/IV8;

    .line 2581556
    iput-object p2, p0, LX/IVD;->b:LX/1vg;

    .line 2581557
    iput-object p3, p0, LX/IVD;->c:LX/IQY;

    .line 2581558
    return-void
.end method

.method public static a(LX/0QB;)LX/IVD;
    .locals 6

    .prologue
    .line 2581559
    const-class v1, LX/IVD;

    monitor-enter v1

    .line 2581560
    :try_start_0
    sget-object v0, LX/IVD;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581561
    sput-object v2, LX/IVD;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581562
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581563
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581564
    new-instance p0, LX/IVD;

    invoke-static {v0}, LX/IV8;->a(LX/0QB;)LX/IV8;

    move-result-object v3

    check-cast v3, LX/IV8;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-static {v0}, LX/IQY;->b(LX/0QB;)LX/IQY;

    move-result-object v5

    check-cast v5, LX/IQY;

    invoke-direct {p0, v3, v4, v5}, LX/IVD;-><init>(LX/IV8;LX/1vg;LX/IQY;)V

    .line 2581565
    move-object v0, p0

    .line 2581566
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581567
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581568
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
