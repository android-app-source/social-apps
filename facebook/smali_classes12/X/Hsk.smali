.class public final LX/Hsk;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hsl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

.field public c:LX/HtA;

.field public final synthetic d:LX/Hsl;


# direct methods
.method public constructor <init>(LX/Hsl;)V
    .locals 1

    .prologue
    .line 2514927
    iput-object p1, p0, LX/Hsk;->d:LX/Hsl;

    .line 2514928
    move-object v0, p1

    .line 2514929
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2514930
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2514931
    const-string v0, "ComposerFeedAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2514932
    if-ne p0, p1, :cond_1

    .line 2514933
    :cond_0
    :goto_0
    return v0

    .line 2514934
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2514935
    goto :goto_0

    .line 2514936
    :cond_3
    check-cast p1, LX/Hsk;

    .line 2514937
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2514938
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2514939
    if-eq v2, v3, :cond_0

    .line 2514940
    iget-object v2, p0, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2514941
    goto :goto_0

    .line 2514942
    :cond_5
    iget-object v2, p1, LX/Hsk;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_4

    .line 2514943
    :cond_6
    iget-object v2, p0, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iget-object v3, p1, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2514944
    goto :goto_0

    .line 2514945
    :cond_8
    iget-object v2, p1, LX/Hsk;->b:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    if-nez v2, :cond_7

    .line 2514946
    :cond_9
    iget-object v2, p0, LX/Hsk;->c:LX/HtA;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Hsk;->c:LX/HtA;

    iget-object v3, p1, LX/Hsk;->c:LX/HtA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2514947
    goto :goto_0

    .line 2514948
    :cond_a
    iget-object v2, p1, LX/Hsk;->c:LX/HtA;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
