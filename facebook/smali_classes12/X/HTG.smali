.class public final LX/HTG;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V
    .locals 0

    .prologue
    .line 2469251
    iput-object p1, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 2469252
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2469253
    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->g:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    .line 2469254
    invoke-virtual {p1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->r:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-nez v1, :cond_1

    .line 2469255
    :cond_0
    :goto_0
    return-void

    .line 2469256
    :cond_1
    invoke-virtual {p1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 2469257
    iget-object v2, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->r:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget v2, v2, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->s:I

    if-eq v2, v1, :cond_2

    iget-object v2, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2469258
    iget-object v2, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->r:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v2, p1, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2469259
    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    .line 2469260
    iput v1, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->s:I

    .line 2469261
    :cond_2
    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->g:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->g:LX/1P1;

    invoke-virtual {v1}, LX/1OR;->D()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2469262
    iget-object v0, p0, LX/HTG;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->d(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    goto :goto_0
.end method
