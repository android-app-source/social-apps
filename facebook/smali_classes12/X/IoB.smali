.class public final enum LX/IoB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IoB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IoB;

.field public static final enum REQUEST_MONEY:LX/IoB;

.field public static final enum SEND_MONEY:LX/IoB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2611592
    new-instance v0, LX/IoB;

    const-string v1, "SEND_MONEY"

    invoke-direct {v0, v1, v2}, LX/IoB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IoB;->SEND_MONEY:LX/IoB;

    .line 2611593
    new-instance v0, LX/IoB;

    const-string v1, "REQUEST_MONEY"

    invoke-direct {v0, v1, v3}, LX/IoB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IoB;->REQUEST_MONEY:LX/IoB;

    .line 2611594
    const/4 v0, 0x2

    new-array v0, v0, [LX/IoB;

    sget-object v1, LX/IoB;->SEND_MONEY:LX/IoB;

    aput-object v1, v0, v2

    sget-object v1, LX/IoB;->REQUEST_MONEY:LX/IoB;

    aput-object v1, v0, v3

    sput-object v0, LX/IoB;->$VALUES:[LX/IoB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2611595
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IoB;
    .locals 1

    .prologue
    .line 2611590
    const-class v0, LX/IoB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IoB;

    return-object v0
.end method

.method public static values()[LX/IoB;
    .locals 1

    .prologue
    .line 2611591
    sget-object v0, LX/IoB;->$VALUES:[LX/IoB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IoB;

    return-object v0
.end method
