.class public LX/HQh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BNM;

.field private final b:LX/0W9;

.field private final c:LX/8Do;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/BNM;LX/0W9;LX/8Do;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2463815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463816
    iput-object p1, p0, LX/HQh;->a:LX/BNM;

    .line 2463817
    iput-object p2, p0, LX/HQh;->b:LX/0W9;

    .line 2463818
    iput-object p3, p0, LX/HQh;->c:LX/8Do;

    .line 2463819
    iput-object p4, p0, LX/HQh;->d:Landroid/content/Context;

    .line 2463820
    return-void
.end method


# virtual methods
.method public final a(LX/CZd;)LX/HQn;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2463821
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2463822
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463823
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2463824
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463825
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2463826
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2463827
    iget-object v1, p0, LX/HQh;->c:LX/8Do;

    invoke-virtual {v1}, LX/8Do;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2463828
    iget-object v1, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v1

    .line 2463829
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_3

    .line 2463830
    iget-object v1, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v1

    .line 2463831
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2463832
    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 2463833
    iget-object v1, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v1

    .line 2463834
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2463835
    iget-object v4, p0, LX/HQh;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0836ec

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v5

    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2463836
    :goto_2
    const/4 v3, 0x0

    .line 2463837
    const-string v4, ""

    .line 2463838
    const/4 v8, 0x0

    .line 2463839
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->NO_SHOW:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2463840
    sget-object v7, LX/HQm;->HIDDEN:LX/HQm;

    .line 2463841
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463842
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_f

    .line 2463843
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463844
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->l(II)D

    move-result-wide v10

    double-to-float v3, v10

    .line 2463845
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463846
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2463847
    iget-object v6, p0, LX/HQh;->a:LX/BNM;

    invoke-virtual {v4, v0, v2}, LX/15i;->l(II)D

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, LX/BNM;->a(D)Ljava/lang/String;

    move-result-object v4

    move v0, v2

    .line 2463848
    :goto_3
    iget-object v6, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v6, v6

    .line 2463849
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    if-eqz v6, :cond_a

    .line 2463850
    iget-object v6, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v6, v6

    .line 2463851
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v6

    iget-object v10, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2463852
    invoke-virtual {v10, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    move v6, v2

    :goto_4
    if-eqz v6, :cond_e

    .line 2463853
    iget-object v6, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v6, v6

    .line 2463854
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 2463855
    iget-object v6, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v6, v6

    .line 2463856
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->z()LX/1vs;

    move-result-object v6

    iget-object v8, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2463857
    invoke-virtual {v8, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2463858
    iget-object v6, p0, LX/HQh;->c:LX/8Do;

    invoke-virtual {v6}, LX/8Do;->c()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2463859
    iget-object v6, p0, LX/HQh;->b:LX/0W9;

    invoke-virtual {v6}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 2463860
    :cond_0
    iget-object v6, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v6, v6

    .line 2463861
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->A()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v6

    .line 2463862
    :goto_5
    if-eqz v0, :cond_b

    if-eqz v2, :cond_b

    .line 2463863
    sget-object v2, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    .line 2463864
    :goto_6
    new-instance v0, LX/HQn;

    invoke-direct/range {v0 .. v6}, LX/HQn;-><init>(Ljava/lang/String;LX/HQm;FLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V

    return-object v0

    :cond_1
    move v1, v5

    .line 2463865
    goto/16 :goto_0

    :cond_2
    move v1, v5

    .line 2463866
    goto/16 :goto_1

    :cond_3
    move v1, v5

    goto/16 :goto_1

    :cond_4
    move-object v1, v0

    .line 2463867
    goto/16 :goto_2

    .line 2463868
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2463869
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2463870
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463871
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2463872
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463873
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2463874
    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_7
    if-eqz v0, :cond_6

    .line 2463875
    const-string v0, " \u00b7 "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2463876
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463877
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2463878
    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_7
    move v0, v5

    .line 2463879
    goto :goto_7

    :cond_8
    move v0, v5

    goto :goto_7

    :cond_9
    move v6, v5

    .line 2463880
    goto/16 :goto_4

    :cond_a
    move v6, v5

    goto/16 :goto_4

    .line 2463881
    :cond_b
    if-eqz v0, :cond_c

    .line 2463882
    sget-object v2, LX/HQm;->RATING_ONLY:LX/HQm;

    goto :goto_6

    .line 2463883
    :cond_c
    if-eqz v2, :cond_d

    .line 2463884
    sget-object v2, LX/HQm;->OPEN_HOURS_ONLY:LX/HQm;

    goto :goto_6

    :cond_d
    move-object v2, v7

    goto :goto_6

    :cond_e
    move v2, v5

    move-object v6, v9

    move-object v5, v8

    goto :goto_5

    :cond_f
    move v0, v5

    goto/16 :goto_3
.end method
