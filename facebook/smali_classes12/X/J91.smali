.class public LX/J91;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652395
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 9

    .prologue
    .line 2652396
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2652397
    const-string v1, "section_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2652398
    const-string v2, "section_tracking"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2652399
    const-string v3, "view_name"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2652400
    const-string v4, "collections_icon"

    invoke-static {p1, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Fb;

    .line 2652401
    const-string v5, "friendship_status"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2652402
    const-string v5, "subscribe_status"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2652403
    const-string v5, "collection"

    invoke-static {p1, v5}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JBL;

    .line 2652404
    new-instance v8, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;

    invoke-direct {v8}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;-><init>()V

    .line 2652405
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2652406
    const-string p1, "profile_id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652407
    const-string p1, "section_id"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652408
    const-string p1, "section_tracking"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652409
    const-string p1, "view_name"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652410
    const-string p1, "collections_icon"

    invoke-static {p0, p1, v4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2652411
    const-string p1, "collection"

    invoke-static {p0, p1, v5}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2652412
    const-string p1, "friendship_status"

    invoke-virtual {p0, p1, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652413
    const-string p1, "subscribe_status"

    invoke-virtual {p0, p1, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652414
    invoke-virtual {v8, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2652415
    move-object v0, v8

    .line 2652416
    return-object v0
.end method
