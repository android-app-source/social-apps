.class public final LX/HMu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMt;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 0

    .prologue
    .line 2457650
    iput-object p1, p0, LX/HMu;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2457651
    iget-object v0, p0, LX/HMu;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->M:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2457652
    iget-object v0, p0, LX/HMu;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2457653
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2457654
    const-string v2, "extra_deleted_tab_type"

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2457655
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2457656
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2457657
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2457658
    iget-object v0, p0, LX/HMu;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817cd

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2457659
    iget-object v0, p0, LX/HMu;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->M:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2457660
    return-void
.end method
