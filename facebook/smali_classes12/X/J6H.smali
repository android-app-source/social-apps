.class public final LX/J6H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field private b:Landroid/view/View;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2649479
    iput-object p1, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2649480
    iput-object p2, p0, LX/J6H;->b:Landroid/view/View;

    .line 2649481
    iput-object p3, p0, LX/J6H;->c:Ljava/lang/String;

    .line 2649482
    iput-object p4, p0, LX/J6H;->d:Ljava/lang/String;

    .line 2649483
    iput-object p5, p0, LX/J6H;->e:Ljava/lang/String;

    .line 2649484
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x2

    const v1, -0x29566d7d

    invoke-static {v0, v8, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2649485
    iget-object v2, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v0, p0, LX/J6H;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v3, p0, LX/J6H;->c:Ljava/lang/String;

    iget-object v4, p0, LX/J6H;->d:Ljava/lang/String;

    const/4 p1, 0x1

    const/4 v10, 0x0

    .line 2649486
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    if-nez v5, :cond_2

    .line 2649487
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->d:Landroid/view/LayoutInflater;

    const v6, 0x7f031015

    invoke-virtual {v5, v6, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    iput-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    .line 2649488
    :cond_0
    :goto_0
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    const v6, 0x7f0d17e0

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2649489
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object v7, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2649490
    iget-object v6, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->e:Landroid/content/res/Resources;

    const v7, 0x7f0838e4

    new-array v9, p1, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2649491
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    const v6, 0x7f0d2698

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 2649492
    iget-object v6, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->e:Landroid/content/res/Resources;

    const v7, 0x7f0838e5

    new-array v9, p1, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649493
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    const v6, 0x7f0d2699

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 2649494
    iget-object v6, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->e:Landroid/content/res/Resources;

    const v7, 0x7f0838e6

    new-array v9, p1, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649495
    new-instance v0, LX/31Y;

    iget-object v2, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->f:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    const v2, 0x7f0838e7

    new-instance v3, LX/J6I;

    iget-object v4, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v5, p0, LX/J6H;->e:Ljava/lang/String;

    iget-object v6, p0, LX/J6H;->b:Landroid/view/View;

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, LX/J6I;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;Landroid/view/View;Z)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v2, LX/J6J;

    iget-object v3, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v4, p0, LX/J6H;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, LX/J6J;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v0

    .line 2649496
    iget-object v2, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v2, v2, LX/J4L;->d:LX/0Px;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2649497
    const v2, 0x7f0838e8

    new-instance v3, LX/J6I;

    iget-object v4, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v5, p0, LX/J6H;->e:Ljava/lang/String;

    iget-object v6, p0, LX/J6H;->b:Landroid/view/View;

    invoke-direct {v3, v4, v5, v6, v8}, LX/J6I;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;Landroid/view/View;Z)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2649498
    :goto_1
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2649499
    iget-object v0, p0, LX/J6H;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->g:LX/J5k;

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_EXPOSED:LX/J5h;

    invoke-virtual {v0, v2}, LX/J5k;->a(LX/J5h;)V

    .line 2649500
    const v0, 0x7dcba15b

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2649501
    :cond_1
    const v2, 0x7f080017

    new-instance v3, LX/J6G;

    invoke-direct {v3, p0}, LX/J6G;-><init>(LX/J6H;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_1

    .line 2649502
    :cond_2
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2649503
    iget-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iget-object v6, v2, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->i:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/16 :goto_0
.end method
