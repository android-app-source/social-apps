.class public LX/IFZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IFR;


# instance fields
.field public a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/net/Uri;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:LX/3OL;

.field private j:LX/0Uh;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/3OL;LX/0Uh;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/3OL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554553
    iput-object p1, p0, LX/IFZ;->c:Ljava/lang/String;

    .line 2554554
    iput-object p2, p0, LX/IFZ;->d:Landroid/net/Uri;

    .line 2554555
    iput-object p3, p0, LX/IFZ;->e:Ljava/lang/String;

    .line 2554556
    iput-object p4, p0, LX/IFZ;->f:Ljava/lang/String;

    .line 2554557
    iput-object p5, p0, LX/IFZ;->g:Ljava/lang/String;

    .line 2554558
    iput-object p6, p0, LX/IFZ;->h:Ljava/lang/String;

    .line 2554559
    iput-object p7, p0, LX/IFZ;->b:Ljava/lang/String;

    .line 2554560
    iput-boolean p8, p0, LX/IFZ;->a:Z

    .line 2554561
    iput-object p9, p0, LX/IFZ;->i:LX/3OL;

    .line 2554562
    iput-object p10, p0, LX/IFZ;->j:LX/0Uh;

    .line 2554563
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;)LX/3OL;
    .locals 2

    .prologue
    .line 2554564
    sget-object v0, LX/IFY;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554565
    sget-object v0, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    :goto_0
    return-object v0

    .line 2554566
    :pswitch_0
    sget-object v0, LX/3OL;->NOT_SENT:LX/3OL;

    goto :goto_0

    .line 2554567
    :pswitch_1
    sget-object v0, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    goto :goto_0

    .line 2554568
    :pswitch_2
    sget-object v0, LX/3OL;->RECEIVED:LX/3OL;

    goto :goto_0

    .line 2554569
    :pswitch_3
    sget-object v0, LX/3OL;->INTERACTED:LX/3OL;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;
    .locals 10
    .param p0    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2554570
    if-nez p0, :cond_1

    .line 2554571
    :cond_0
    :goto_0
    return-object v2

    .line 2554572
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v0

    .line 2554573
    if-eqz v0, :cond_0

    .line 2554574
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2554575
    if-eqz v1, :cond_0

    .line 2554576
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->e()LX/1Fb;

    move-result-object v3

    .line 2554577
    if-eqz v3, :cond_2

    .line 2554578
    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2554579
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$ItemDescriptionModel;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, ""

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$AdditionalItemDescriptionModel;

    move-result-object v5

    if-nez v5, :cond_4

    const-string v5, ""

    :goto_2
    const-string v6, "aura"

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;->a()Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v0

    invoke-static {v0}, LX/IFZ;->a(Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;)LX/3OL;

    move-result-object v9

    move-object v0, p3

    move-object v7, p1

    move v8, p2

    invoke-virtual/range {v0 .. v9}, LX/IFa;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/3OL;)LX/IFZ;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$ItemDescriptionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$ItemDescriptionModel;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$AdditionalItemDescriptionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel$NearbyFriendsContactsSetItemModel$AdditionalItemDescriptionModel;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;
    .locals 10
    .param p0    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2554580
    if-nez p0, :cond_1

    .line 2554581
    :cond_0
    :goto_0
    return-object v0

    .line 2554582
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v1

    .line 2554583
    if-eqz v1, :cond_0

    .line 2554584
    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v3

    .line 2554585
    if-eqz v3, :cond_0

    .line 2554586
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2554587
    if-eqz v1, :cond_0

    .line 2554588
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->d()LX/1Fb;

    move-result-object v2

    .line 2554589
    if-eqz v2, :cond_6

    .line 2554590
    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    .line 2554591
    if-eqz v4, :cond_6

    .line 2554592
    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2554593
    :goto_1
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2554594
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v4

    if-nez v4, :cond_4

    .line 2554595
    const-string v4, ""

    .line 2554596
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;

    move-result-object v5

    .line 2554597
    if-nez v5, :cond_5

    .line 2554598
    const-string v5, ""

    .line 2554599
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->mH_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2554600
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->mH_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemExplanationModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2554601
    :cond_2
    if-eqz v0, :cond_7

    const-string v6, "check_in"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "current_city"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2554602
    :cond_3
    :goto_4
    move-object v6, v0

    .line 2554603
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->b()Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v0

    invoke-static {v0}, LX/IFZ;->a(Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;)LX/3OL;

    move-result-object v9

    move-object v0, p3

    move-object v7, p1

    move v8, p2

    invoke-virtual/range {v0 .. v9}, LX/IFa;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/3OL;)LX/IFZ;

    move-result-object v0

    goto :goto_0

    .line 2554604
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->e()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ItemDescriptionModel;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2554605
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$AdditionalItemDescriptionModel;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_6
    move-object v2, v0

    goto :goto_1

    :cond_7
    const-string v0, "aura"

    goto :goto_4
.end method

.method public static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;
    .locals 11
    .param p0    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 2554606
    if-nez p0, :cond_1

    .line 2554607
    :cond_0
    :goto_0
    return-object v2

    .line 2554608
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2554609
    if-eqz v0, :cond_0

    .line 2554610
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2554611
    if-eqz v0, :cond_2

    .line 2554612
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2554613
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2554614
    const-string v4, ""

    .line 2554615
    if-eqz v0, :cond_3

    .line 2554616
    const/4 v1, 0x2

    invoke-virtual {v7, v0, v1}, LX/15i;->g(II)I

    move-result v1

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2554617
    if-eqz v1, :cond_3

    .line 2554618
    invoke-virtual {v7, v1, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2554619
    :cond_3
    const-string v5, ""

    .line 2554620
    if-eqz v0, :cond_4

    .line 2554621
    invoke-virtual {v7, v0, v6}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_4

    .line 2554622
    invoke-virtual {v7, v0, v6}, LX/15i;->g(II)I

    move-result v1

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2554623
    invoke-virtual {v7, v1, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2554624
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel$FriendsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    const-string v6, "aura"

    if-nez v0, :cond_5

    sget-object v9, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    :goto_1
    move-object v0, p3

    move-object v7, p1

    move v8, p2

    invoke-virtual/range {v0 .. v9}, LX/IFa;->a(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/3OL;)LX/IFZ;

    move-result-object v2

    goto :goto_0

    .line 2554625
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2554626
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2554627
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 2554628
    :cond_5
    const-class v8, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    invoke-virtual {v7, v0, v10, v8, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    invoke-static {v0}, LX/IFZ;->a(Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;)LX/3OL;

    move-result-object v9

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2554629
    new-instance v0, LX/0wM;

    invoke-direct {v0, p1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2554630
    iget-object v1, p0, LX/IFZ;->j:LX/0Uh;

    const/16 v2, 0x583

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2554631
    const v1, 0x7f020740

    const v2, 0x7f0a00e6

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2554632
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, LX/IFZ;->a:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020b9a

    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f020b97

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554633
    iget-object v0, p0, LX/IFZ;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554634
    iget-object v0, p0, LX/IFZ;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/3OL;)V
    .locals 0

    .prologue
    .line 2554635
    iput-object p1, p0, LX/IFZ;->i:LX/3OL;

    .line 2554636
    return-void
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2554637
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IFZ;->g:Ljava/lang/String;

    .line 2554638
    return-void
.end method

.method public final b(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 2554639
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554550
    iget-object v0, p0, LX/IFZ;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2554551
    const v0, 0x7f083842

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/IFZ;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/IFZ;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LX/IFZ;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554549
    iget-object v0, p0, LX/IFZ;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554548
    const-string v0, ""

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554547
    iget-object v0, p0, LX/IFZ;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2554543
    sget-object v0, LX/IFY;->b:[I

    iget-object v1, p0, LX/IFZ;->i:LX/3OL;

    invoke-virtual {v1}, LX/3OL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2554544
    const v0, 0x7f083843

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, LX/IFZ;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2554545
    :pswitch_0
    const v0, 0x7f083844

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, LX/IFZ;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2554546
    :pswitch_1
    const v0, 0x7f083845

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, LX/IFZ;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554542
    iget-object v0, p0, LX/IFZ;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2554541
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2554540
    const/4 v0, 0x0

    return v0
.end method

.method public final h()LX/3OL;
    .locals 1

    .prologue
    .line 2554539
    iget-object v0, p0, LX/IFZ;->i:LX/3OL;

    return-object v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 2554538
    iget-object v0, p0, LX/IFZ;->h:Ljava/lang/String;

    const-string v1, "aura"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2554537
    const/4 v0, 0x1

    return v0
.end method

.method public final k()LX/6VG;
    .locals 1

    .prologue
    .line 2554536
    sget-object v0, LX/6VG;->NONE:LX/6VG;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554535
    iget-object v0, p0, LX/IFZ;->b:Ljava/lang/String;

    return-object v0
.end method
