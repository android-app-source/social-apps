.class public final LX/I3U;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxb;

.field public final synthetic b:LX/I3Y;


# direct methods
.method public constructor <init>(LX/I3Y;LX/Hxb;)V
    .locals 0

    .prologue
    .line 2531440
    iput-object p1, p0, LX/I3U;->b:LX/I3Y;

    iput-object p2, p0, LX/I3U;->a:LX/Hxb;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2531457
    iget-object v0, p0, LX/I3U;->a:LX/Hxb;

    invoke-interface {v0}, LX/Hxb;->a()V

    .line 2531458
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2531455
    invoke-direct {p0}, LX/I3U;->a()V

    .line 2531456
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2531441
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531442
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531443
    if-eqz v0, :cond_1

    .line 2531444
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531445
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel$SuggestedEventCutsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2531446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531447
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel$SuggestedEventCutsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel$SuggestedEventCutsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 2531448
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 2531449
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2531450
    iget-object v1, p0, LX/I3U;->a:LX/Hxb;

    invoke-interface {v1, v0}, LX/Hxb;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V

    .line 2531451
    iget-object v1, p0, LX/I3U;->b:LX/I3Y;

    invoke-static {v1, v0, p1}, LX/I3Y;->a$redex0(LX/I3Y;Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2531452
    :goto_1
    return-void

    .line 2531453
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2531454
    :cond_1
    invoke-direct {p0}, LX/I3U;->a()V

    goto :goto_1
.end method
