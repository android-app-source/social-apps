.class public LX/JUn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2699781
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JUn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699767
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699768
    iput-object p1, p0, LX/JUn;->b:LX/0Ot;

    .line 2699769
    return-void
.end method

.method public static a(LX/0QB;)LX/JUn;
    .locals 4

    .prologue
    .line 2699770
    const-class v1, LX/JUn;

    monitor-enter v1

    .line 2699771
    :try_start_0
    sget-object v0, LX/JUn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699772
    sput-object v2, LX/JUn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699775
    new-instance v3, LX/JUn;

    const/16 p0, 0x2084

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUn;-><init>(LX/0Ot;)V

    .line 2699776
    move-object v0, v3

    .line 2699777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2699742
    check-cast p2, LX/JUm;

    .line 2699743
    iget-object v0, p0, LX/JUn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JUm;->a:Ljava/lang/String;

    iget v1, p2, LX/JUm;->b:F

    .line 2699744
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/8yi;->c(LX/1De;)LX/8yg;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/8yg;->c(F)LX/8yg;

    move-result-object v3

    const p0, 0x7f021af4

    .line 2699745
    iget-object p2, v3, LX/8yg;->a:LX/8yh;

    invoke-virtual {v3, p0}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, p2, LX/8yh;->d:LX/1dc;

    .line 2699746
    move-object v3, v3

    .line 2699747
    const p0, 0x7f02117c

    .line 2699748
    iget-object p2, v3, LX/8yg;->a:LX/8yh;

    invoke-virtual {v3, p0}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, p2, LX/8yh;->c:LX/1dc;

    .line 2699749
    move-object v3, v3

    .line 2699750
    const p0, 0x7f021af3

    .line 2699751
    iget-object p2, v3, LX/8yg;->a:LX/8yh;

    invoke-virtual {v3, p0}, LX/1Dp;->g(I)LX/1dc;

    move-result-object v1

    iput-object v1, p2, LX/8yh;->e:LX/1dc;

    .line 2699752
    move-object v3, v3

    .line 2699753
    const/4 p0, 0x2

    invoke-virtual {v3, p0}, LX/8yg;->h(I)LX/8yg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const p0, 0x7f0b0056

    invoke-virtual {v3, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0a00d5

    invoke-virtual {v3, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const/4 p0, 0x3

    invoke-virtual {v3, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object p0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v3, p0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 p0, 0x1

    const p2, 0x7f0b0069

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b25c1

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b25c2

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f08150e

    invoke-virtual {p1, v3}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f021388

    invoke-interface {v2, v3}, LX/1Dh;->X(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0541

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const p0, 0x7f0b007b

    invoke-interface {v2, v3, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    .line 2699754
    const v3, -0xfcd7edc

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2699755
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2699756
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2699757
    invoke-static {}, LX/1dS;->b()V

    .line 2699758
    iget v0, p1, LX/1dQ;->b:I

    .line 2699759
    packed-switch v0, :pswitch_data_0

    .line 2699760
    :goto_0
    return-object v2

    .line 2699761
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2699762
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2699763
    check-cast v1, LX/JUm;

    .line 2699764
    iget-object v3, p0, LX/JUn;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JUo;

    iget-object v4, v1, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object p2, v1, LX/JUm;->e:Ljava/lang/String;

    .line 2699765
    iget-object p0, v3, LX/JUo;->a:LX/JUQ;

    invoke-virtual {p0, v0, v4, p1, p2}, LX/JUQ;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V

    .line 2699766
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xfcd7edc
        :pswitch_0
    .end packed-switch
.end method
