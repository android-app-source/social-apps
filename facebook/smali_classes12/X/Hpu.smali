.class public LX/Hpu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/1rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510102
    return-void
.end method

.method public static a(LX/0QB;)LX/Hpu;
    .locals 4

    .prologue
    .line 2510103
    const-class v1, LX/Hpu;

    monitor-enter v1

    .line 2510104
    :try_start_0
    sget-object v0, LX/Hpu;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2510105
    sput-object v2, LX/Hpu;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2510106
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2510107
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2510108
    new-instance p0, LX/Hpu;

    invoke-direct {p0}, LX/Hpu;-><init>()V

    .line 2510109
    const-class v3, LX/1rq;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1rq;

    .line 2510110
    iput-object v3, p0, LX/Hpu;->a:LX/1rq;

    .line 2510111
    move-object v0, p0

    .line 2510112
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2510113
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hpu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2510114
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2510115
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
