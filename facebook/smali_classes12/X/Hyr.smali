.class public final LX/Hyr;
.super LX/BlQ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524542
    iput-object p1, p0, LX/Hyr;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/BlQ;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2524543
    check-cast p1, LX/BlP;

    .line 2524544
    iget-object v0, p0, LX/Hyr;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    iget-object v1, p1, LX/BlP;->a:Ljava/lang/String;

    .line 2524545
    iget-object v2, v0, LX/Hz2;->g:LX/I2o;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524546
    if-nez v1, :cond_0

    .line 2524547
    :goto_0
    return-void

    .line 2524548
    :cond_0
    iget-object v2, v0, LX/Hz2;->j:LX/I2Z;

    .line 2524549
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2524550
    :cond_1
    :goto_1
    invoke-static {v0}, LX/Hz2;->m(LX/Hz2;)V

    goto :goto_0

    .line 2524551
    :cond_2
    iget-object v3, v2, LX/I2Z;->g:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hx6;

    .line 2524552
    iget-object p0, v2, LX/I2Z;->g:Ljava/util/HashMap;

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;

    .line 2524553
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2524554
    iget-object p0, v2, LX/I2Z;->g:Ljava/util/HashMap;

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524555
    invoke-static {v2}, LX/I2Z;->b(LX/I2Z;)V

    goto :goto_1
.end method
