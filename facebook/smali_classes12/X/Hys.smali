.class public final LX/Hys;
.super LX/BlS;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524556
    iput-object p1, p0, LX/Hys;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/BlS;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2524557
    check-cast p1, LX/BlR;

    .line 2524558
    iget-object v0, p0, LX/Hys;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    if-nez v0, :cond_1

    .line 2524559
    :cond_0
    :goto_0
    return-void

    .line 2524560
    :cond_1
    iget-object v0, p0, LX/Hys;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    .line 2524561
    iget v1, v0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->j:I

    move v0, v1

    .line 2524562
    iget-object v1, p1, LX/BlR;->a:LX/BlI;

    sget-object v2, LX/BlI;->SENDING:LX/BlI;

    if-ne v1, v2, :cond_2

    .line 2524563
    iget-object v1, p0, LX/Hys;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setBadge(I)V

    goto :goto_0

    .line 2524564
    :cond_2
    iget-object v1, p1, LX/BlR;->a:LX/BlI;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    if-ne v1, v2, :cond_0

    .line 2524565
    iget-object v1, p0, LX/Hys;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->E:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setBadge(I)V

    goto :goto_0
.end method
