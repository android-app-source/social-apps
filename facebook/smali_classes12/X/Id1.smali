.class public final LX/Id1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596023
    iput-object p1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x3753d561

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596024
    iget-object v1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iget-object v2, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v3, 0x7f082d7e

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setActionText(Ljava/lang/String;)V

    .line 2596025
    iget-object v1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596026
    iget-object v1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v1, :cond_0

    .line 2596027
    iget-object v1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iget-object v2, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(DD)V

    .line 2596028
    :cond_0
    iget-object v1, p0, LX/Id1;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596029
    const v1, 0xfd28ea0

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
