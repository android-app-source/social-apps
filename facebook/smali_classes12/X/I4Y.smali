.class public final LX/I4Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I4Z;


# direct methods
.method public constructor <init>(LX/I4Z;)V
    .locals 0

    .prologue
    .line 2534212
    iput-object p1, p0, LX/I4Y;->a:LX/I4Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1995ec8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2534213
    iget-object v1, p0, LX/I4Y;->a:LX/I4Z;

    iget-object v1, v1, LX/I4Z;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2534214
    const v1, 0x433a404b

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2534215
    :goto_0
    return-void

    .line 2534216
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2534217
    iget-object v2, p0, LX/I4Y;->a:LX/I4Z;

    iget-object v2, v2, LX/I4Z;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2534218
    iget-object v2, p0, LX/I4Y;->a:LX/I4Z;

    iget-object v2, v2, LX/I4Z;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2534219
    const v1, 0x4b69d978    # 1.532556E7f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
