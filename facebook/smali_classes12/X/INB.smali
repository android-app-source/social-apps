.class public LX/INB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IN5;
.implements Ljava/io/Serializable;


# instance fields
.field public mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

.field private final mNuxQuestion:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

.field public mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2570635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570636
    iput-object p1, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2570637
    iput-object p2, p0, LX/INB;->mNuxQuestion:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    .line 2570638
    iput-object p3, p0, LX/INB;->mQuery:Ljava/lang/String;

    .line 2570639
    return-void
.end method


# virtual methods
.method public final c()LX/IN8;
    .locals 1

    .prologue
    .line 2570631
    sget-object v0, LX/IN8;->GroupRow:LX/IN8;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2570632
    iget-object v0, p0, LX/INB;->mNuxQuestion:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    if-eqz v0, :cond_0

    .line 2570633
    iget-object v0, p0, LX/INB;->mNuxQuestion:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2570634
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2570625
    iget-object v0, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    if-eqz v0, :cond_0

    .line 2570626
    iget-object v0, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2570627
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2570628
    iget-object v0, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->c()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2570629
    iget-object v0, p0, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->c()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$ParentGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$ParentGroupModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2570630
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
