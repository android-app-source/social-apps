.class public final enum LX/IRH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IRH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IRH;

.field public static final enum NORMAL:LX/IRH;

.field public static final enum PENDING:LX/IRH;

.field public static final enum PINNED:LX/IRH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2576351
    new-instance v0, LX/IRH;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LX/IRH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IRH;->NORMAL:LX/IRH;

    .line 2576352
    new-instance v0, LX/IRH;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, LX/IRH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IRH;->PENDING:LX/IRH;

    .line 2576353
    new-instance v0, LX/IRH;

    const-string v1, "PINNED"

    invoke-direct {v0, v1, v4}, LX/IRH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IRH;->PINNED:LX/IRH;

    .line 2576354
    const/4 v0, 0x3

    new-array v0, v0, [LX/IRH;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    aput-object v1, v0, v2

    sget-object v1, LX/IRH;->PENDING:LX/IRH;

    aput-object v1, v0, v3

    sget-object v1, LX/IRH;->PINNED:LX/IRH;

    aput-object v1, v0, v4

    sput-object v0, LX/IRH;->$VALUES:[LX/IRH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2576355
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IRH;
    .locals 1

    .prologue
    .line 2576356
    const-class v0, LX/IRH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IRH;

    return-object v0
.end method

.method public static values()[LX/IRH;
    .locals 1

    .prologue
    .line 2576357
    sget-object v0, LX/IRH;->$VALUES:[LX/IRH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IRH;

    return-object v0
.end method
