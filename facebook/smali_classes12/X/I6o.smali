.class public final LX/I6o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I6r;


# direct methods
.method public constructor <init>(LX/I6r;)V
    .locals 0

    .prologue
    .line 2537951
    iput-object p1, p0, LX/I6o;->a:LX/I6r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2537952
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2537953
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2537954
    if-eqz p1, :cond_0

    .line 2537955
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537956
    if-nez v0, :cond_1

    .line 2537957
    :cond_0
    :goto_0
    return-void

    .line 2537958
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537959
    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;

    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2537960
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2537961
    const/4 v2, 0x0

    const-class v3, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2537962
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2537963
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537964
    check-cast v0, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;

    invoke-virtual {v0}, Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel;->k()Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v0

    .line 2537965
    iget-object v2, p0, LX/I6o;->a:LX/I6r;

    iget-object v2, v2, LX/I6r;->d:LX/I6m;

    invoke-virtual {v2, v1, v0}, LX/I6m;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    goto :goto_0

    .line 2537966
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2537967
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2537968
    move-object v1, v0

    goto :goto_1
.end method
