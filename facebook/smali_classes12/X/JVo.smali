.class public LX/JVo;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVq;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVo",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701693
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2701694
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JVo;->b:LX/0Zi;

    .line 2701695
    iput-object p1, p0, LX/JVo;->a:LX/0Ot;

    .line 2701696
    return-void
.end method

.method public static a(LX/0QB;)LX/JVo;
    .locals 4

    .prologue
    .line 2701665
    const-class v1, LX/JVo;

    monitor-enter v1

    .line 2701666
    :try_start_0
    sget-object v0, LX/JVo;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701667
    sput-object v2, LX/JVo;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701668
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701669
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701670
    new-instance v3, LX/JVo;

    const/16 p0, 0x20ac

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVo;-><init>(LX/0Ot;)V

    .line 2701671
    move-object v0, v3

    .line 2701672
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701673
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701674
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701675
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2701678
    check-cast p2, LX/JVn;

    .line 2701679
    iget-object v0, p0, LX/JVo;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JVq;

    iget-object v1, p2, LX/JVn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JVn;->b:LX/1Pn;

    const/4 v6, 0x0

    .line 2701680
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2701681
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701682
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3c

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0917

    invoke-interface {v4, v6, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x7f0b0917

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v3

    .line 2701683
    if-nez v3, :cond_0

    .line 2701684
    const/4 v5, 0x0

    .line 2701685
    :goto_0
    move-object v3, v5

    .line 2701686
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    .line 2701687
    iget-object v7, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 2701688
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v7

    new-instance v8, LX/JVp;

    invoke-direct {v8, v0, v1}, LX/JVp;-><init>(LX/JVq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {v7, v8}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v11

    .line 2701689
    iget-object v7, v0, LX/JVq;->a:LX/JVs;

    const/4 v9, 0x0

    const v10, 0x3f333333    # 0.7f

    move-object v8, p1

    move-object v12, v2

    invoke-virtual/range {v7 .. v12}, LX/JVs;->a(LX/1De;IFLX/0Px;Ljava/lang/Object;)LX/JVr;

    move-result-object v7

    .line 2701690
    iget-object v8, v0, LX/JVq;->b:LX/BcE;

    invoke-virtual {v8, p1}, LX/BcE;->c(LX/1De;)LX/BcC;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/BcC;->a(LX/5Jq;)LX/BcC;

    move-result-object v7

    const v8, 0x7f0b0064

    invoke-virtual {v7, v8}, LX/BcC;->h(I)LX/BcC;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->b()LX/1Dg;

    move-result-object v7

    move-object v4, v7

    .line 2701691
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2701692
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a043b

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2701676
    invoke-static {}, LX/1dS;->b()V

    .line 2701677
    const/4 v0, 0x0

    return-object v0
.end method
