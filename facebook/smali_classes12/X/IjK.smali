.class public LX/IjK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6xu;


# instance fields
.field private final a:LX/6yW;


# direct methods
.method public constructor <init>(LX/6yW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605647
    iput-object p1, p0, LX/IjK;->a:LX/6yW;

    .line 2605648
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605645
    const-string v0, "p2p_cancel_edit_card"

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605644
    const-string v0, "p2p_edit_card_details"

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605643
    const-string v0, "p2p_edit_card_success"

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605635
    const-string v0, "p2p_edit_card_fail"

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605642
    iget-object v0, p0, LX/IjK;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605641
    iget-object v0, p0, LX/IjK;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605640
    const-string v0, "p2p_confirm_remove_card"

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605639
    const-string v0, "p2p_remove_card_success"

    return-object v0
.end method

.method public final i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605638
    const-string v0, "p2p_remove_card_fail"

    return-object v0
.end method

.method public final j(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605637
    const-string v0, "p2p_initiate_remove_card"

    return-object v0
.end method

.method public final k(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605636
    const-string v0, "p2p_cancel_remove_card"

    return-object v0
.end method
