.class public LX/JJa;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "SetResultAndroid"
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2679598
    const-class v0, LX/JJa;

    sput-object v0, LX/JJa;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/5pY;LX/03V;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679599
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679600
    iput-object p2, p0, LX/JJa;->b:LX/03V;

    .line 2679601
    return-void
.end method

.method private h()Landroid/app/Activity;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2679602
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2679603
    iget-object v0, p0, LX/JJa;->b:LX/03V;

    sget-object v1, LX/JJa;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "currentAcitvity is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679604
    :cond_0
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679605
    const-string v0, "SetResultAndroid"

    return-object v0
.end method

.method public setResultCanceled()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679606
    invoke-direct {p0}, LX/JJa;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2679607
    if-eqz v0, :cond_0

    .line 2679608
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2679609
    :cond_0
    return-void
.end method

.method public setResultFirstUser()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679610
    invoke-direct {p0}, LX/JJa;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2679611
    if-eqz v0, :cond_0

    .line 2679612
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2679613
    :cond_0
    return-void
.end method

.method public setResultOK()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679614
    invoke-direct {p0}, LX/JJa;->h()Landroid/app/Activity;

    move-result-object v0

    .line 2679615
    if-eqz v0, :cond_0

    .line 2679616
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2679617
    :cond_0
    return-void
.end method
