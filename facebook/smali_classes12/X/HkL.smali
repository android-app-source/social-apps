.class public final enum LX/HkL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HkL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/HkL;

.field public static final enum b:LX/HkL;

.field public static final enum c:LX/HkL;

.field private static final synthetic d:[LX/HkL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/HkL;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/HkL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HkL;->a:LX/HkL;

    new-instance v0, LX/HkL;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, LX/HkL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HkL;->b:LX/HkL;

    new-instance v0, LX/HkL;

    const-string v1, "ADS"

    invoke-direct {v0, v1, v4}, LX/HkL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HkL;->c:LX/HkL;

    const/4 v0, 0x3

    new-array v0, v0, [LX/HkL;

    sget-object v1, LX/HkL;->a:LX/HkL;

    aput-object v1, v0, v2

    sget-object v1, LX/HkL;->b:LX/HkL;

    aput-object v1, v0, v3

    sget-object v1, LX/HkL;->c:LX/HkL;

    aput-object v1, v0, v4

    sput-object v0, LX/HkL;->d:[LX/HkL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HkL;
    .locals 1

    const-class v0, LX/HkL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HkL;

    return-object v0
.end method

.method public static values()[LX/HkL;
    .locals 1

    sget-object v0, LX/HkL;->d:[LX/HkL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HkL;

    return-object v0
.end method
