.class public final LX/HKe;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/HKf;


# direct methods
.method public constructor <init>(LX/HKf;)V
    .locals 1

    .prologue
    .line 2454796
    iput-object p1, p0, LX/HKe;->c:LX/HKf;

    .line 2454797
    move-object v0, p1

    .line 2454798
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2454799
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2454781
    const-string v0, "PageVeryResponsiveToMessagesBadgeUnitComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2454782
    if-ne p0, p1, :cond_1

    .line 2454783
    :cond_0
    :goto_0
    return v0

    .line 2454784
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2454785
    goto :goto_0

    .line 2454786
    :cond_3
    check-cast p1, LX/HKe;

    .line 2454787
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2454788
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2454789
    if-eq v2, v3, :cond_0

    .line 2454790
    iget-object v2, p0, LX/HKe;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKe;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HKe;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2454791
    goto :goto_0

    .line 2454792
    :cond_5
    iget-object v2, p1, LX/HKe;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2454793
    :cond_6
    iget-object v2, p0, LX/HKe;->b:LX/2km;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/HKe;->b:LX/2km;

    iget-object v3, p1, LX/HKe;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2454794
    goto :goto_0

    .line 2454795
    :cond_7
    iget-object v2, p1, LX/HKe;->b:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
