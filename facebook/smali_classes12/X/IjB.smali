.class public final LX/IjB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:LX/6y8;

.field public final synthetic c:LX/IjC;


# direct methods
.method public constructor <init>(LX/IjC;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V
    .locals 0

    .prologue
    .line 2605400
    iput-object p1, p0, LX/IjB;->c:LX/IjC;

    iput-object p2, p0, LX/IjB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object p3, p0, LX/IjB;->b:LX/6y8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2605401
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2605402
    if-eqz v0, :cond_0

    .line 2605403
    iget-object v1, p0, LX/IjB;->c:LX/IjC;

    iget-object v1, v1, LX/IjC;->b:LX/6ye;

    iget-object v2, p0, LX/IjB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v3, p0, LX/IjB;->c:LX/IjC;

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2605404
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2605405
    const/4 v4, 0x0

    :goto_0
    move-object v0, v4

    .line 2605406
    invoke-virtual {v1, v2, p1, v0}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2605407
    :goto_1
    return-void

    .line 2605408
    :cond_0
    iget-object v0, p0, LX/IjB;->c:LX/IjC;

    iget-object v0, v0, LX/IjC;->b:LX/6ye;

    iget-object v1, p0, LX/IjB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_1

    .line 2605409
    :pswitch_0
    iget-object v4, v3, LX/IjC;->a:Landroid/content/Context;

    const p0, 0x7f082c20

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2605410
    :pswitch_1
    iget-object v4, v3, LX/IjC;->a:Landroid/content/Context;

    const p0, 0x7f082c21

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2605411
    :pswitch_2
    iget-object v4, v3, LX/IjC;->a:Landroid/content/Context;

    const p0, 0x7f082c22

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2605412
    :pswitch_3
    iget-object v4, v3, LX/IjC;->a:Landroid/content/Context;

    const p0, 0x7f082c24

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2605413
    :pswitch_4
    iget-object v4, v3, LX/IjC;->a:Landroid/content/Context;

    const p0, 0x7f082c23

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2744
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2605414
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;

    .line 2605415
    iget-object v0, p0, LX/IjB;->c:LX/IjC;

    iget-object v1, p0, LX/IjB;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, p0, LX/IjB;->b:LX/6y8;

    const/4 v12, 0x1

    .line 2605416
    iget-object v3, v0, LX/IjC;->b:LX/6ye;

    invoke-virtual {v3, v1}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;)V

    .line 2605417
    iget-object v3, v0, LX/IjC;->f:LX/6qh;

    if-nez v3, :cond_0

    .line 2605418
    :goto_0
    return-void

    .line 2605419
    :cond_0
    iget-object v3, v2, LX/6y8;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605420
    new-instance v4, Lcom/facebook/payments/p2p/model/PartialPaymentCard;

    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->credentialId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    iget-object v3, v2, LX/6y8;->a:Ljava/lang/String;

    iget-object v7, v2, LX/6y8;->a:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x4

    iget-object v8, v2, LX/6y8;->a:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iget v8, v2, LX/6y8;->c:I

    iget v3, v2, LX/6y8;->d:I

    add-int/lit16 v9, v3, 0x7d0

    new-instance v10, Lcom/facebook/payments/p2p/model/Address;

    iget-object v3, v2, LX/6y8;->f:Ljava/lang/String;

    invoke-direct {v10, v3}, Lcom/facebook/payments/p2p/model/Address;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/6y8;->a()Ljava/lang/String;

    move-result-object v11

    move v13, v12

    invoke-direct/range {v4 .. v13}, Lcom/facebook/payments/p2p/model/PartialPaymentCard;-><init>(JLjava/lang/String;IILcom/facebook/payments/p2p/model/Address;Ljava/lang/String;ZZ)V

    .line 2605421
    new-instance v3, LX/DtS;

    invoke-direct {v3}, LX/DtS;-><init>()V

    move-object v3, v3

    .line 2605422
    iget-object v5, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionType:Ljava/lang/String;

    .line 2605423
    iput-object v5, v3, LX/DtS;->a:Ljava/lang/String;

    .line 2605424
    move-object v3, v3

    .line 2605425
    iget-object v5, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionText:Ljava/lang/String;

    .line 2605426
    iput-object v5, v3, LX/DtS;->b:Ljava/lang/String;

    .line 2605427
    move-object v3, v3

    .line 2605428
    iget-object v5, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionUrl:Ljava/lang/String;

    .line 2605429
    iput-object v5, v3, LX/DtS;->c:Ljava/lang/String;

    .line 2605430
    move-object v3, v3

    .line 2605431
    iget-object v5, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->followUpActionButtonText:Ljava/lang/String;

    .line 2605432
    iput-object v5, v3, LX/DtS;->d:Ljava/lang/String;

    .line 2605433
    move-object v3, v3

    .line 2605434
    new-instance v5, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;

    invoke-direct {v5, v3}, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;-><init>(LX/DtS;)V

    move-object v3, v5

    .line 2605435
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 2605436
    const-string v6, "encoded_credential_id"

    iget-object v7, p1, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;->encodedCredentialId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2605437
    const-string v6, "partial_payment_card"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2605438
    const-string v4, "verification_follow_up_action"

    invoke-virtual {v5, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2605439
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2605440
    const-string v4, "extra_activity_result_data"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2605441
    new-instance v4, LX/73T;

    sget-object v5, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v4, v5, v3}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 2605442
    iget-object v3, v0, LX/IjC;->f:LX/6qh;

    invoke-virtual {v3, v4}, LX/6qh;->a(LX/73T;)V

    goto/16 :goto_0
.end method
