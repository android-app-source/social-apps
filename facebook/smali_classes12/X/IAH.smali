.class public LX/IAH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/0Zb;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z

.field public f:LX/0iA;

.field public g:Landroid/view/View;

.field public h:Lcom/facebook/content/SecureContextHelper;

.field public i:Ljava/lang/String;

.field private j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

.field public k:LX/3kp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Zb;LX/0iA;Lcom/facebook/content/SecureContextHelper;LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2545223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2545224
    iput-object p1, p0, LX/IAH;->a:Landroid/content/Context;

    .line 2545225
    iput-object p2, p0, LX/IAH;->b:LX/0Zb;

    .line 2545226
    iput-object p3, p0, LX/IAH;->f:LX/0iA;

    .line 2545227
    iput-object p4, p0, LX/IAH;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2545228
    iput-object p5, p0, LX/IAH;->k:LX/3kp;

    .line 2545229
    return-void
.end method

.method public static a(LX/IAH;)V
    .locals 2

    .prologue
    .line 2545219
    iget-object v0, p0, LX/IAH;->k:LX/3kp;

    sget-object v1, LX/7vb;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2545220
    iget-object v0, p0, LX/IAH;->k:LX/3kp;

    const/4 v1, 0x2

    .line 2545221
    iput v1, v0, LX/3kp;->b:I

    .line 2545222
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z
    .locals 1

    .prologue
    .line 2545187
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->PRIVATE_TYPE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->GROUP:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IAH;
    .locals 6

    .prologue
    .line 2545217
    new-instance v0, LX/IAH;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v5

    check-cast v5, LX/3kp;

    invoke-direct/range {v0 .. v5}, LX/IAH;-><init>(Landroid/content/Context;LX/0Zb;LX/0iA;Lcom/facebook/content/SecureContextHelper;LX/3kp;)V

    .line 2545218
    return-object v0
.end method

.method private b(LX/Blc;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2545216
    iget-boolean v0, p0, LX/IAH;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/IAH;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Blc;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2545188
    invoke-direct {p0, p1}, LX/IAH;->b(LX/Blc;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/IAH;->k:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2545189
    iget-object v0, p0, LX/IAH;->f:LX/0iA;

    sget-object v1, LX/3kh;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p1, LX/3kh;

    invoke-virtual {v0, v1, p1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 2545190
    if-eqz v0, :cond_3

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3763"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2545191
    if-eqz v0, :cond_2

    .line 2545192
    iget-object v0, p0, LX/IAH;->g:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2545193
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2545194
    :cond_0
    :goto_1
    return-void

    .line 2545195
    :cond_1
    iget-object v0, p0, LX/IAH;->g:Landroid/view/View;

    check-cast v0, Landroid/view/ViewStub;

    .line 2545196
    const v1, 0x7f030589

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2545197
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    iput-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2545198
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    iget-object v1, p0, LX/IAH;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2545199
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2545200
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2545201
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    new-instance v1, LX/IAF;

    invoke-direct {v1, p0}, LX/IAF;-><init>(LX/IAH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2545202
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    new-instance v1, LX/IAG;

    invoke-direct {v1, p0}, LX/IAG;-><init>(LX/IAH;)V

    .line 2545203
    iput-object v1, v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 2545204
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "event_seen_state_megaphone_show"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "event_guest_list"

    .line 2545205
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2545206
    move-object v0, v0

    .line 2545207
    iget-object v1, p0, LX/IAH;->c:Ljava/lang/String;

    .line 2545208
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 2545209
    move-object v0, v0

    .line 2545210
    iget-object v1, p0, LX/IAH;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2545211
    iget-object v0, p0, LX/IAH;->k:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 2545212
    iget-object v0, p0, LX/IAH;->f:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "3763"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2545213
    const/4 v0, 0x0

    iput-object v0, p0, LX/IAH;->g:Landroid/view/View;

    goto :goto_1

    .line 2545214
    :cond_2
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    if-eqz v0, :cond_0

    .line 2545215
    iget-object v0, p0, LX/IAH;->j:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
