.class public LX/J8G;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2651894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2651895
    return-void
.end method

.method public static declared-synchronized a(ZLX/J8I;)V
    .locals 5

    .prologue
    .line 2651896
    const-class v1, LX/J8G;

    monitor-enter v1

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v0, LX/J8G;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 2651897
    invoke-static {}, LX/J8E;->a()Ljava/lang/Object;

    move-result-object v0

    .line 2651898
    new-instance v2, LX/J8H;

    invoke-direct {v2, v0, p1}, LX/J8H;-><init>(Ljava/lang/Object;LX/J8I;)V

    .line 2651899
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-static {}, LX/J8F;->b()Ljava/lang/Class;

    move-result-object p0

    aput-object p0, v3, v4

    .line 2651900
    invoke-static {}, LX/J8F;->b()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-static {v4, v3, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    .line 2651901
    invoke-static {v0}, LX/JEz;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2651902
    sput-object v0, LX/J8G;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 2651903
    new-instance v0, LX/J8L;

    const-string v2, "Failed to get original activity manager."

    invoke-direct {v0, v2}, LX/J8L;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/J8L; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2651904
    :catch_0
    move-exception v0

    .line 2651905
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2651906
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2651907
    :cond_0
    if-nez p0, :cond_1

    :try_start_2
    sget-object v0, LX/J8G;->a:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 2651908
    sget-object v0, LX/J8G;->a:Ljava/lang/Object;

    invoke-static {v0}, LX/JEz;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2651909
    const/4 v2, 0x0

    sput-object v2, LX/J8G;->a:Ljava/lang/Object;

    .line 2651910
    if-nez v0, :cond_1

    .line 2651911
    new-instance v0, LX/J8L;

    const-string v2, "Original activity manager is null."

    invoke-direct {v0, v2}, LX/J8L;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch LX/J8L; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2651912
    :catch_1
    move-exception v0

    .line 2651913
    :try_start_3
    new-instance v2, LX/J8L;

    const-string v3, "Unexpected exception was thrown."

    invoke-direct {v2, v3, v0}, LX/J8L;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2651914
    :cond_1
    monitor-exit v1

    return-void
.end method
