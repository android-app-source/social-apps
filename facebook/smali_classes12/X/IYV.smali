.class public final LX/IYV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V
    .locals 0

    .prologue
    .line 2587799
    iput-object p1, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2587789
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587790
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->d()V

    .line 2587791
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    .line 2587792
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2587793
    sget-object v0, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->af:Ljava/lang/Class;

    const-string v1, "Fetch nonce operation failed"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2587794
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->w(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587795
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->u(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)LX/Gb4;

    move-result-object v0

    invoke-interface {v0}, LX/Gb4;->d()V

    .line 2587796
    iget-object v0, p0, LX/IYV;->a:Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;

    invoke-static {v0}, Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;->s(Lcom/facebook/katana/dbl/activity/DeviceBasedLoginActivity;)V

    .line 2587797
    :cond_0
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2587798
    invoke-direct {p0}, LX/IYV;->a()V

    return-void
.end method
