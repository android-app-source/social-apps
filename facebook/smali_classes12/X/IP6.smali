.class public final LX/IP6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V
    .locals 0

    .prologue
    .line 2573730
    iput-object p1, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iput-object p2, p0, LX/IP6;->a:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    iput-boolean p3, p0, LX/IP6;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2573732
    iget-object v0, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v1, p0, LX/IP6;->a:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-virtual {v0, v1, v6}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V

    .line 2573733
    iget-object v0, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    iget-object v1, v0, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->d:LX/0kL;

    new-instance v2, LX/27k;

    const-string v3, "%s\n\n%s"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v0, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v5, 0x7f08003c

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const/4 v5, 0x1

    iget-boolean v0, p0, LX/IP6;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f083889

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2573734
    return-void

    .line 2573735
    :cond_0
    iget-object v0, p0, LX/IP6;->c:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f08388a

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2573731
    return-void
.end method
