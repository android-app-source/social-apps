.class public LX/HMe;
.super LX/0gG;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:LX/HKh;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2457449
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2457450
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HMe;->c:Ljava/util/List;

    .line 2457451
    iput-object p1, p0, LX/HMe;->a:Landroid/content/Context;

    .line 2457452
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2457453
    const-string v0, ""

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2457454
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2457455
    new-instance v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;

    iget-object v0, p0, LX/HMe;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;-><init>(Landroid/content/Context;)V

    .line 2457456
    iget-object v0, p0, LX/HMe;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Zu;

    .line 2457457
    invoke-static {v0}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/9Zu;->c()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/HMd;

    invoke-direct {v5, p0, v0}, LX/HMd;-><init>(LX/HMe;LX/9Zu;)V

    .line 2457458
    if-eqz v2, :cond_0

    .line 2457459
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object p0, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2457460
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2457461
    :goto_0
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457462
    if-eqz v4, :cond_1

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2457463
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457464
    :goto_1
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->b:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/CustomFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2457465
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2457466
    return-object v1

    .line 2457467
    :cond_0
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p0, 0x8

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 2457468
    :cond_1
    iget-object v0, v1, Lcom/facebook/pages/common/services/PagesServiceCarouselItem;->e:Lcom/facebook/resources/ui/FbTextView;

    const p0, 0x7f0817e5

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2457469
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2457470
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2457471
    iget-object v0, p0, LX/HMe;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2457472
    if-eqz p1, :cond_1

    .line 2457473
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Zu;

    .line 2457474
    invoke-static {v0}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2457475
    if-eqz v2, :cond_0

    .line 2457476
    iget-object v2, p0, LX/HMe;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2457477
    :cond_1
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2457478
    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2457479
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2457480
    iget-object v0, p0, LX/HMe;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
