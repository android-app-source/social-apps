.class public final enum LX/HTM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HTM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HTM;

.field public static final enum LARGE_ITEM:LX/HTM;

.field public static final enum LIST_ITEM:LX/HTM;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2469422
    new-instance v0, LX/HTM;

    const-string v1, "LARGE_ITEM"

    invoke-direct {v0, v1, v2}, LX/HTM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HTM;->LARGE_ITEM:LX/HTM;

    .line 2469423
    new-instance v0, LX/HTM;

    const-string v1, "LIST_ITEM"

    invoke-direct {v0, v1, v3}, LX/HTM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HTM;->LIST_ITEM:LX/HTM;

    .line 2469424
    const/4 v0, 0x2

    new-array v0, v0, [LX/HTM;

    sget-object v1, LX/HTM;->LARGE_ITEM:LX/HTM;

    aput-object v1, v0, v2

    sget-object v1, LX/HTM;->LIST_ITEM:LX/HTM;

    aput-object v1, v0, v3

    sput-object v0, LX/HTM;->$VALUES:[LX/HTM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2469425
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HTM;
    .locals 1

    .prologue
    .line 2469426
    const-class v0, LX/HTM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HTM;

    return-object v0
.end method

.method public static values()[LX/HTM;
    .locals 1

    .prologue
    .line 2469427
    sget-object v0, LX/HTM;->$VALUES:[LX/HTM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HTM;

    return-object v0
.end method
