.class public LX/Iu9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/16I;

.field public final b:LX/3R7;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field private final d:LX/0So;

.field private e:LX/0Yb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/concurrent/Future;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Iu7;",
            "LX/Iu8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/16I;LX/3R7;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2625436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625437
    iput-object v0, p0, LX/Iu9;->e:LX/0Yb;

    .line 2625438
    iput-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;

    .line 2625439
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Iu9;->g:Ljava/util/Map;

    .line 2625440
    iput-object p1, p0, LX/Iu9;->a:LX/16I;

    .line 2625441
    iput-object p2, p0, LX/Iu9;->b:LX/3R7;

    .line 2625442
    iput-object p3, p0, LX/Iu9;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2625443
    iput-object p4, p0, LX/Iu9;->d:LX/0So;

    .line 2625444
    return-void
.end method

.method private declared-synchronized a(LX/Iu7;)V
    .locals 3

    .prologue
    .line 2625433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;-><init>(LX/Iu9;LX/Iu7;)V

    const v2, 0x1f7de79f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625434
    monitor-exit p0

    return-void

    .line 2625435
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(J)Z
    .locals 5

    .prologue
    .line 2625432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(LX/Iu9;)Z
    .locals 1

    .prologue
    .line 2625431
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/Iu9;)V
    .locals 1

    .prologue
    .line 2625427
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/Iu9;->c$redex0(LX/Iu9;)V

    .line 2625428
    invoke-static {p0}, LX/Iu9;->d(LX/Iu9;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625429
    monitor-exit p0

    return-void

    .line 2625430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/Iu9;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2625424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->g:Ljava/util/Map;

    new-instance v1, LX/Iu7;

    invoke-direct {v1, p1, p2}, LX/Iu7;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625425
    monitor-exit p0

    return-void

    .line 2625426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c$redex0(LX/Iu9;)V
    .locals 1

    .prologue
    .line 2625419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->e:LX/0Yb;

    if-eqz v0, :cond_0

    .line 2625420
    iget-object v0, p0, LX/Iu9;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2625421
    const/4 v0, 0x0

    iput-object v0, p0, LX/Iu9;->e:LX/0Yb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625422
    :cond_0
    monitor-exit p0

    return-void

    .line 2625423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/Iu9;)V
    .locals 2

    .prologue
    .line 2625414
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2625415
    iget-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2625416
    const/4 v0, 0x0

    iput-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625417
    :cond_0
    monitor-exit p0

    return-void

    .line 2625418
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized d(LX/Iu9;Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2625445
    monitor-enter p0

    :try_start_0
    new-instance v2, LX/Iu7;

    invoke-direct {v2, p1, p2}, LX/Iu7;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2625446
    iget-object v0, p0, LX/Iu9;->g:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iu8;

    .line 2625447
    if-eqz v0, :cond_0

    .line 2625448
    iget v0, v0, LX/Iu8;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    .line 2625449
    :goto_0
    const/16 v3, 0xa

    if-ne v0, v3, :cond_1

    .line 2625450
    const/4 v0, 0x0

    .line 2625451
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    .line 2625452
    goto :goto_0

    .line 2625453
    :cond_1
    :try_start_1
    iget-object v3, p0, LX/Iu9;->g:Ljava/util/Map;

    new-instance v4, LX/Iu8;

    iget-object v5, p0, LX/Iu9;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    invoke-direct {v4, v0, v6, v7}, LX/Iu8;-><init>(IJ)V

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 2625454
    goto :goto_1

    .line 2625455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized e(LX/Iu9;)V
    .locals 5

    .prologue
    .line 2625410
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/Iu9;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2625411
    :goto_0
    monitor-exit p0

    return-void

    .line 2625412
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Iu9;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$1;-><init>(LX/Iu9;)V

    const-wide/32 v2, 0xea60

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2625413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()Z
    .locals 1

    .prologue
    .line 2625409
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/Iu9;)V
    .locals 1

    .prologue
    .line 2625405
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Iu9;->f:Ljava/util/concurrent/Future;

    .line 2625406
    invoke-static {p0}, LX/Iu9;->h(LX/Iu9;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625407
    monitor-exit p0

    return-void

    .line 2625408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized h(LX/Iu9;)V
    .locals 1

    .prologue
    .line 2625399
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->a:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2625400
    invoke-static {p0}, LX/Iu9;->i(LX/Iu9;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625401
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2625402
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Iu9;->e:LX/0Yb;

    if-nez v0, :cond_0

    .line 2625403
    invoke-static {p0}, LX/Iu9;->j(LX/Iu9;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2625404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized i(LX/Iu9;)V
    .locals 6

    .prologue
    .line 2625388
    monitor-enter p0

    const/4 v0, 0x0

    .line 2625389
    :try_start_0
    iget-object v1, p0, LX/Iu9;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2625390
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iu8;

    iget-wide v4, v1, LX/Iu8;->b:J

    .line 2625391
    invoke-direct {p0, v4, v5}, LX/Iu9;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2625392
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iu7;

    invoke-direct {p0, v0}, LX/Iu9;->a(LX/Iu7;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2625393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2625394
    :cond_0
    const/4 v0, 0x1

    move v2, v0

    .line 2625395
    goto :goto_0

    .line 2625396
    :cond_1
    if-eqz v2, :cond_2

    .line 2625397
    :try_start_1
    invoke-static {p0}, LX/Iu9;->e(LX/Iu9;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2625398
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private static declared-synchronized j(LX/Iu9;)V
    .locals 3

    .prologue
    .line 2625384
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Iu9;->e:LX/0Yb;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625385
    iget-object v0, p0, LX/Iu9;->a:LX/16I;

    sget-object v1, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v2, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$3;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$3;-><init>(LX/Iu9;)V

    invoke-virtual {v0, v1, v2}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/Iu9;->e:LX/0Yb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625386
    monitor-exit p0

    return-void

    .line 2625387
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2625381
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/Iu9;->c(LX/Iu9;Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625382
    monitor-exit p0

    return-void

    .line 2625383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 2625375
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/Iu9;->d(LX/Iu9;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    .line 2625376
    invoke-static {p0}, LX/Iu9;->a(LX/Iu9;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2625377
    invoke-static {p0}, LX/Iu9;->b(LX/Iu9;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625378
    :goto_0
    monitor-exit p0

    return v0

    .line 2625379
    :cond_0
    :try_start_1
    invoke-static {p0}, LX/Iu9;->e(LX/Iu9;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2625380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
