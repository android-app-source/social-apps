.class public LX/HeV;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/46O;


# instance fields
.field public a:Landroid/widget/TextView;

.field private b:LX/HeU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2490431
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HeV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2490432
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2490433
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/HeV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490434
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2490435
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2490436
    sget-object v0, LX/HeU;->CONNECTED:LX/HeU;

    iput-object v0, p0, LX/HeV;->b:LX/HeU;

    .line 2490437
    const v0, 0x7f03035e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2490438
    const v0, 0x7f0d0b18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    .line 2490439
    return-void
.end method

.method private a(LX/HeU;)V
    .locals 2

    .prologue
    .line 2490440
    invoke-virtual {p0}, LX/HeV;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HeV;->b:LX/HeU;

    if-ne v0, p1, :cond_0

    .line 2490441
    :goto_0
    return-void

    .line 2490442
    :cond_0
    sget-object v0, LX/HeT;->a:[I

    invoke-virtual {p1}, LX/HeU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2490443
    :goto_1
    iput-object p1, p0, LX/HeV;->b:LX/HeU;

    .line 2490444
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/HeV;->setVisibility(I)V

    goto :goto_0

    .line 2490445
    :pswitch_0
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490446
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const v1, 0x7f08006d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2490447
    invoke-virtual {p0}, LX/HeV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a024a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/HeV;->setBackgroundColor(I)V

    goto :goto_1

    .line 2490448
    :pswitch_1
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490449
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const v1, 0x7f08003f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2490450
    invoke-virtual {p0}, LX/HeV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0247

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/HeV;->setBackgroundColor(I)V

    goto :goto_1

    .line 2490451
    :pswitch_2
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2490452
    iget-object v0, p0, LX/HeV;->a:Landroid/widget/TextView;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2490453
    invoke-virtual {p0}, LX/HeV;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a08cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/HeV;->setBackgroundColor(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2490454
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/HeV;->setVisibility(I)V

    .line 2490455
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 2490456
    if-eqz p1, :cond_0

    .line 2490457
    invoke-direct {p0}, LX/HeV;->b()V

    .line 2490458
    :goto_0
    return-void

    .line 2490459
    :cond_0
    sget-object v0, LX/HeU;->NO_INTERNET:LX/HeU;

    invoke-direct {p0, v0}, LX/HeV;->a(LX/HeU;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2490460
    if-eqz p1, :cond_0

    .line 2490461
    invoke-direct {p0}, LX/HeV;->b()V

    .line 2490462
    :goto_0
    return-void

    .line 2490463
    :cond_0
    sget-object v0, LX/HeU;->NO_INTERNET:LX/HeU;

    invoke-direct {p0, v0}, LX/HeV;->a(LX/HeU;)V

    goto :goto_0
.end method
