.class public LX/HeS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/16I;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/base/broadcast/FbBroadcastManager$SelfRegistrableReceiver;",
            ">;"
        }
    .end annotation
.end field

.field public c:[LX/46O;


# direct methods
.method public constructor <init>(LX/16I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2490407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2490408
    iput-object p1, p0, LX/HeS;->a:LX/16I;

    .line 2490409
    return-void
.end method

.method public static a$redex0(LX/HeS;LX/1Ed;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2490410
    iget-object v0, p0, LX/HeS;->c:[LX/46O;

    if-nez v0, :cond_1

    .line 2490411
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2490412
    :goto_0
    iget-object v2, p0, LX/HeS;->c:[LX/46O;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2490413
    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    if-ne p1, v2, :cond_2

    const/4 v2, 0x1

    .line 2490414
    :goto_1
    if-eqz p2, :cond_3

    .line 2490415
    iget-object v3, p0, LX/HeS;->c:[LX/46O;

    aget-object v3, v3, v0

    invoke-interface {v3, v2}, LX/46O;->b(Z)V

    .line 2490416
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 2490417
    goto :goto_1

    .line 2490418
    :cond_3
    iget-object v3, p0, LX/HeS;->c:[LX/46O;

    aget-object v3, v3, v0

    invoke-interface {v3, v2}, LX/46O;->a(Z)V

    goto :goto_2
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2490419
    iget-object v0, p0, LX/HeS;->a:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/1Ed;->CONNECTED:LX/1Ed;

    .line 2490420
    :goto_0
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/HeS;->a$redex0(LX/HeS;LX/1Ed;Z)V

    .line 2490421
    return-void

    .line 2490422
    :cond_0
    sget-object v0, LX/1Ed;->NO_INTERNET:LX/1Ed;

    goto :goto_0
.end method
