.class public LX/HW8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475692
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2475693
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2475694
    const-string v1, "com.facebook.katana.profile.id"

    const-string v2, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475695
    const-string v1, "arg_pages_surface_reaction_surface"

    const-string v2, "ANDROID_PAGE_JOBS_TAB"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475696
    const-string v1, "extra_is_inside_page_surface_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475697
    const-string v1, "source_name"

    const-string v2, "fragment_title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475698
    new-instance v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-direct {v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;-><init>()V

    .line 2475699
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475700
    return-object v1
.end method
