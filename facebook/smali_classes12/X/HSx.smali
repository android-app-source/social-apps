.class public LX/HSx;
.super LX/CSL;
.source ""


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/17W;

.field public final e:LX/17Y;

.field private final f:LX/1nC;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/01T;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2468186
    const-class v0, LX/HSx;

    sput-object v0, LX/HSx;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17W;LX/17Y;LX/1nC;LX/01T;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/17W;",
            "LX/17Y;",
            "LX/1nC;",
            "LX/01T;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Dh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2468177
    invoke-direct {p0}, LX/CSL;-><init>()V

    .line 2468178
    iput-object p1, p0, LX/HSx;->c:Landroid/content/Context;

    .line 2468179
    iput-object p2, p0, LX/HSx;->d:LX/17W;

    .line 2468180
    iput-object p3, p0, LX/HSx;->e:LX/17Y;

    .line 2468181
    iput-object p4, p0, LX/HSx;->f:LX/1nC;

    .line 2468182
    iput-object p5, p0, LX/HSx;->h:LX/01T;

    .line 2468183
    iput-object p6, p0, LX/HSx;->g:LX/0Ot;

    .line 2468184
    iput-object p7, p0, LX/HSx;->i:LX/0Ot;

    .line 2468185
    return-void
.end method

.method private a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8AB;ZZZZZ)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2468150
    if-eqz p7, :cond_0

    if-nez p8, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2468151
    new-instance v0, LX/89I;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v0, p1, p2, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0, p3}, LX/89I;->a(Ljava/lang/String;)LX/89I;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/89I;->b(Ljava/lang/String;)LX/89I;

    move-result-object v0

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    .line 2468152
    iget-object v0, p0, LX/CSL;->a:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/CSL;->a:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    .line 2468153
    :goto_1
    invoke-virtual {v0, p3}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    .line 2468154
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    const-string v3, "adminPhotoVideoPost"

    invoke-static {v1, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    if-nez p8, :cond_7

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v3, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v3, "ANDROID_PAGE_ADMIN_COMPOSER"

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2468155
    new-instance v1, LX/8AA;

    invoke-direct {v1, p6}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1, v0}, LX/8AA;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)LX/8AA;

    move-result-object v1

    .line 2468156
    iget-object v0, p0, LX/HSx;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2468157
    invoke-virtual {v1}, LX/8AA;->g()LX/8AA;

    .line 2468158
    iget-object v0, p0, LX/HSx;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Dh;

    invoke-virtual {v0}, LX/7Dh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2468159
    invoke-virtual {v1}, LX/8AA;->h()LX/8AA;

    .line 2468160
    :cond_1
    if-eqz p7, :cond_8

    .line 2468161
    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    .line 2468162
    :cond_2
    :goto_3
    if-eqz p11, :cond_3

    if-nez p8, :cond_3

    .line 2468163
    invoke-virtual {v1}, LX/8AA;->s()LX/8AA;

    .line 2468164
    :cond_3
    iget-object v0, p0, LX/HSx;->h:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v0, v2, :cond_4

    .line 2468165
    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    .line 2468166
    :cond_4
    iget-object v0, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 2468167
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2468168
    :cond_6
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    goto :goto_1

    .line 2468169
    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    .line 2468170
    :cond_8
    if-eqz p8, :cond_9

    .line 2468171
    invoke-virtual {v1}, LX/8AA;->k()LX/8AA;

    .line 2468172
    invoke-virtual {v1}, LX/8AA;->t()LX/8AA;

    goto :goto_3

    .line 2468173
    :cond_9
    if-eqz p9, :cond_a

    .line 2468174
    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    .line 2468175
    :cond_a
    if-eqz p10, :cond_2

    .line 2468176
    const/4 v0, 0x3

    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/8AA;->a(II)LX/8AA;

    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/HSx;
    .locals 8

    .prologue
    .line 2468148
    new-instance v0, LX/HSx;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v2

    check-cast v2, LX/17W;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v4

    check-cast v4, LX/1nC;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v5

    check-cast v5, LX/01T;

    const/16 v6, 0x1032

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3572

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/HSx;-><init>(Landroid/content/Context;LX/17W;LX/17Y;LX/1nC;LX/01T;LX/0Ot;LX/0Ot;)V

    .line 2468149
    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468147
    iget-object v0, p0, LX/HSx;->e:LX/17Y;

    iget-object v1, p0, LX/HSx;->c:Landroid/content/Context;

    sget-object v2, LX/0ax;->bi:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468146
    iget-object v0, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, p3, p4, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/Long;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468134
    new-instance v0, LX/89I;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v0, p1, p2, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 2468135
    iput-object p3, v0, LX/89I;->c:Ljava/lang/String;

    .line 2468136
    move-object v0, v0

    .line 2468137
    iput-object p4, v0, LX/89I;->d:Ljava/lang/String;

    .line 2468138
    move-object v0, v0

    .line 2468139
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2468140
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    const-string v2, "nonAdminPagePhoto"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2468141
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PAGE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2468142
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2468143
    move-object v0, v1

    .line 2468144
    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    .line 2468145
    iget-object v1, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468126
    sget-object v0, LX/0ax;->aT:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2468127
    iget-object v1, p0, LX/HSx;->e:LX/17Y;

    iget-object v2, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2468128
    if-nez v0, :cond_0

    .line 2468129
    const/4 v0, 0x0

    .line 2468130
    :goto_0
    return-object v0

    .line 2468131
    :cond_0
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2468132
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2468133
    const-string v1, "event_ref_mechanism"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 5

    .prologue
    .line 2468067
    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    const-string v1, "pageSurfaceCheckin"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-direct {v1, v2}, LX/89I;-><init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 2468068
    iput-object p3, v1, LX/89I;->c:Ljava/lang/String;

    .line 2468069
    move-object v1, v1

    .line 2468070
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v1

    new-instance v2, LX/5m9;

    invoke-direct {v2}, LX/5m9;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2468071
    iput-object v3, v2, LX/5m9;->f:Ljava/lang/String;

    .line 2468072
    move-object v2, v2

    .line 2468073
    iput-object p3, v2, LX/5m9;->h:Ljava/lang/String;

    .line 2468074
    move-object v2, v2

    .line 2468075
    new-instance v3, LX/5mG;

    invoke-direct {v3}, LX/5mG;-><init>()V

    const/4 v4, -0x1

    .line 2468076
    iput v4, v3, LX/5mG;->a:I

    .line 2468077
    move-object v3, v3

    .line 2468078
    invoke-virtual {v3}, LX/5mG;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v3

    .line 2468079
    iput-object v3, v2, LX/5m9;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 2468080
    move-object v2, v2

    .line 2468081
    invoke-virtual {v2}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 9
    .param p4    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 2468187
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    const-string v2, "page_profile_review_unit"

    if-eqz p6, :cond_0

    move v3, v0

    :goto_0
    const-string v8, "native_page_profile"

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p5

    invoke-static/range {v1 .. v8}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setHideKeyboardIfReachedMinimumHeight(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2468082
    iget-object v0, p0, LX/CSL;->a:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CSL;->a:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    .line 2468083
    :goto_0
    invoke-virtual {v0, p3}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    .line 2468084
    sget-object v1, LX/21D;->PAGE_FEED:LX/21D;

    const-string v2, "adminPagePost"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, LX/89I;

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v2, p1, p2, v3}, LX/89I;-><init>(JLX/2rw;)V

    .line 2468085
    iput-object p3, v2, LX/89I;->c:Ljava/lang/String;

    .line 2468086
    move-object v2, v2

    .line 2468087
    iput-object p4, v2, LX/89I;->d:Ljava/lang/String;

    .line 2468088
    move-object v2, v2

    .line 2468089
    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_PAGE_ADMIN_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0

    .line 2468090
    :cond_0
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 3

    .prologue
    .line 2468091
    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    const-string v1, "nonAdminPagePost"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    sget-object v2, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, p1, p2, v2}, LX/89I;-><init>(JLX/2rw;)V

    .line 2468092
    iput-object p3, v1, LX/89I;->c:Ljava/lang/String;

    .line 2468093
    move-object v1, v1

    .line 2468094
    iput-object p4, v1, LX/89I;->d:Ljava/lang/String;

    .line 2468095
    move-object v1, v1

    .line 2468096
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    if-nez p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;JLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 2468097
    sget-object v3, LX/21D;->PAGE_FEED:LX/21D;

    const-string v4, "review_chevron_dropdown"

    if-eqz p2, :cond_1

    const/4 v5, 0x1

    :goto_0
    const-string v10, "native_page_profile"

    move-wide/from16 v6, p4

    move-object v8, p1

    move-object/from16 v9, p7

    invoke-static/range {v3 .. v10}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    .line 2468098
    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    if-nez p2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setHideKeyboardIfReachedMinimumHeight(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2468099
    if-eqz p3, :cond_0

    .line 2468100
    invoke-static {p3}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2468101
    :cond_0
    return-object v3

    .line 2468102
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 2468103
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2468104
    sget-object v0, LX/0ax;->aM:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2468105
    iget-object v1, p0, LX/HSx;->d:LX/17W;

    iget-object v2, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2468106
    return-void
.end method

.method public final b(JLjava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2468107
    new-instance v0, LX/E0R;

    iget-object v1, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/E0R;-><init>(Landroid/content/Context;)V

    .line 2468108
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/E0R;->a:Landroid/content/Context;

    const-class p0, Lcom/facebook/places/create/home/HomeEditActivity;

    invoke-direct {v1, v2, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2468109
    const-string v2, "home_id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2468110
    const-string v2, "home_name"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2468111
    const-string v2, "home_activity_entry_flow"

    sget-object p0, LX/E0Q;->PAGES:LX/E0Q;

    invoke-virtual {p0}, LX/E0Q;->ordinal()I

    move-result p0

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2468112
    move-object v0, v1

    .line 2468113
    return-object v0
.end method

.method public final b(JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2468114
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2468115
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2468116
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p3, p4}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2468117
    iget-object v2, p0, LX/HSx;->e:LX/17Y;

    iget-object v3, p0, LX/HSx;->c:Landroid/content/Context;

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2468118
    if-nez v0, :cond_0

    .line 2468119
    const/4 v0, 0x0

    .line 2468120
    :goto_0
    return-object v0

    .line 2468121
    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final b(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 13

    .prologue
    .line 2468122
    iget-object v0, p0, LX/HSx;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1EB;->aj:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v10

    .line 2468123
    iget-object v0, p0, LX/HSx;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/1EB;->O:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v11

    .line 2468124
    sget-object v7, LX/8AB;->PAGE:LX/8AB;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v12}, LX/HSx;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8AB;ZZZZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final c(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 13

    .prologue
    .line 2468125
    sget-object v7, LX/8AB;->PAGE_VIDEO_ONLY:LX/8AB;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v12}, LX/HSx;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/8AB;ZZZZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
