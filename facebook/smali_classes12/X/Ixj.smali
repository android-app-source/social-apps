.class public final LX/Ixj;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Ixk;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:I

.field public c:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            "TVoid;>;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Ixk;


# direct methods
.method public constructor <init>(LX/Ixk;)V
    .locals 1

    .prologue
    .line 2632211
    iput-object p1, p0, LX/Ixj;->d:LX/Ixk;

    .line 2632212
    move-object v0, p1

    .line 2632213
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2632214
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2632215
    if-ne p0, p1, :cond_1

    .line 2632216
    :cond_0
    :goto_0
    return v0

    .line 2632217
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2632218
    goto :goto_0

    .line 2632219
    :cond_3
    check-cast p1, LX/Ixj;

    .line 2632220
    iget v2, p0, LX/Ixj;->b:I

    iget v3, p1, LX/Ixj;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2632221
    goto :goto_0

    .line 2632222
    :cond_4
    iget-object v2, p0, LX/Ixj;->c:LX/2kW;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ixj;->c:LX/2kW;

    iget-object v3, p1, LX/Ixj;->c:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2632223
    goto :goto_0

    .line 2632224
    :cond_5
    iget-object v2, p1, LX/Ixj;->c:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
