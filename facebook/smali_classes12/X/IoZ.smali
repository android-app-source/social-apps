.class public LX/IoZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/InV;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2611998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611999
    iput-object p1, p0, LX/IoZ;->a:Landroid/content/res/Resources;

    .line 2612000
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2612001
    iget-object v0, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2612002
    if-eqz v0, :cond_0

    .line 2612003
    iget-object v0, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2612004
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2612005
    :cond_0
    iget-object v0, p0, LX/IoZ;->a:Landroid/content/res/Resources;

    const v1, 0x7f082cda

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2612006
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/IoZ;->a:Landroid/content/res/Resources;

    const v1, 0x7f082cdb

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2612007
    iget-object v4, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v4, v4

    .line 2612008
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
