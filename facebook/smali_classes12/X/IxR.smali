.class public final LX/IxR;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IxS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

.field public final synthetic e:LX/IxS;


# direct methods
.method public constructor <init>(LX/IxS;)V
    .locals 1

    .prologue
    .line 2631673
    iput-object p1, p0, LX/IxR;->e:LX/IxS;

    .line 2631674
    move-object v0, p1

    .line 2631675
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2631676
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2631677
    const-string v0, "DiscoverPageItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2631678
    if-ne p0, p1, :cond_1

    .line 2631679
    :cond_0
    :goto_0
    return v0

    .line 2631680
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2631681
    goto :goto_0

    .line 2631682
    :cond_3
    check-cast p1, LX/IxR;

    .line 2631683
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2631684
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2631685
    if-eq v2, v3, :cond_0

    .line 2631686
    iget-boolean v2, p0, LX/IxR;->a:Z

    iget-boolean v3, p1, LX/IxR;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2631687
    goto :goto_0

    .line 2631688
    :cond_4
    iget v2, p0, LX/IxR;->b:I

    iget v3, p1, LX/IxR;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 2631689
    goto :goto_0

    .line 2631690
    :cond_5
    iget v2, p0, LX/IxR;->c:I

    iget v3, p1, LX/IxR;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2631691
    goto :goto_0

    .line 2631692
    :cond_6
    iget-object v2, p0, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    iget-object v3, p1, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2631693
    goto :goto_0

    .line 2631694
    :cond_7
    iget-object v2, p1, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
