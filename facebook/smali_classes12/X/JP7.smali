.class public final LX/JP7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2eJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2eJ",
        "<",
        "LX/JPv;",
        "TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;LX/0Px;)V
    .locals 0

    .prologue
    .line 2688855
    iput-object p1, p0, LX/JP7;->c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    iput-object p2, p0, LX/JP7;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    iput-object p3, p0, LX/JP7;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2688856
    iget-object v0, p0, LX/JP7;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)LX/1RC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/1RC",
            "<",
            "LX/JPv;",
            "*-TE;*>;"
        }
    .end annotation

    .prologue
    .line 2688857
    iget-object v0, p0, LX/JP7;->c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->b:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2688858
    new-instance v1, LX/JPv;

    iget-object v2, p0, LX/JP7;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    iget-object v0, p0, LX/JP7;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-direct {v1, v2, v0}, LX/JPv;-><init>(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    return-object v1
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2688859
    iget-object v0, p0, LX/JP7;->c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->f:LX/1LV;

    iget-object v1, p0, LX/JP7;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2688860
    iget-object v0, p0, LX/JP7;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    iget-object v1, p0, LX/JP7;->b:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 2688861
    return-void
.end method
