.class public LX/ImC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GH;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7GH",
        "<",
        "Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;",
        "LX/Ipt;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/ImB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImC;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/ImB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609109
    iput-object p1, p0, LX/ImC;->a:LX/ImB;

    .line 2609110
    return-void
.end method

.method public static a(LX/0QB;)LX/ImC;
    .locals 7

    .prologue
    .line 2609078
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609079
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609080
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609081
    if-nez v1, :cond_0

    .line 2609082
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609083
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609084
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609085
    sget-object v1, LX/ImC;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609086
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609087
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609088
    :cond_1
    if-nez v1, :cond_4

    .line 2609089
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609090
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609091
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609092
    new-instance p0, LX/ImC;

    invoke-static {v0}, LX/ImB;->a(LX/0QB;)LX/ImB;

    move-result-object v1

    check-cast v1, LX/ImB;

    invoke-direct {p0, v1}, LX/ImC;-><init>(LX/ImB;)V

    .line 2609093
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609094
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609095
    if-nez v1, :cond_2

    .line 2609096
    sget-object v0, LX/ImC;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609097
    :goto_1
    if-eqz v0, :cond_3

    .line 2609098
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609099
    :goto_3
    check-cast v0, LX/ImC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609100
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609101
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609102
    :catchall_1
    move-exception v0

    .line 2609103
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609104
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609105
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609106
    :cond_2
    :try_start_8
    sget-object v0, LX/ImC;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2609067
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GJ;

    .line 2609068
    iget-object v1, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v1, LX/Ipt;

    .line 2609069
    iget v3, v1, LX/6kT;->setField_:I

    move v1, v3

    .line 2609070
    const/16 v3, 0x8

    if-ne v1, v3, :cond_0

    .line 2609071
    iget-object v0, v0, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->f()LX/Ipn;

    move-result-object v0

    .line 2609072
    iget-object v1, v0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2609073
    iget-object v1, p0, LX/ImC;->a:LX/ImB;

    iget-object v0, v0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ImB;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2609074
    :cond_1
    iget-object v1, v0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2609075
    iget-object v0, v0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2609076
    iget-object v0, p0, LX/ImC;->a:LX/ImB;

    invoke-virtual {v0}, LX/ImB;->b()V

    goto :goto_0

    .line 2609077
    :cond_2
    new-instance v0, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;-><init>()V

    return-object v0
.end method
