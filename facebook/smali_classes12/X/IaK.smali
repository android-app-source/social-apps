.class public final LX/IaK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

.field public final synthetic b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 0

    .prologue
    .line 2590839
    iput-object p1, p0, LX/IaK;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iput-object p2, p0, LX/IaK;->a:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x59ca6f91

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590840
    iget-object v1, p0, LX/IaK;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v2, p0, LX/IaK;->a:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2590841
    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v4

    .line 2590842
    :goto_0
    iget-object p1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->F:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_8

    if-eqz v3, :cond_8

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-eqz v3, :cond_8

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    :goto_1
    move v1, v4

    .line 2590843
    if-eqz v1, :cond_1

    .line 2590844
    iget-object v1, p0, LX/IaK;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v2, "click_create_account_button"

    invoke-virtual {v1, v2}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590845
    iget-object v1, p0, LX/IaK;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v2, p0, LX/IaK;->a:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2590846
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->q()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->c()Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-nez v3, :cond_9

    .line 2590847
    :cond_0
    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->h:LX/03V;

    const-string v4, "BusinessCreateAccountFragment"

    const-string v5, "missing required data when calling doCreateAccount()"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590848
    :goto_2
    const v1, -0x6ae4ebaf

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2590849
    :cond_1
    iget-object v1, p0, LX/IaK;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2590850
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-nez v2, :cond_2

    .line 2590851
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a()V

    .line 2590852
    :cond_2
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2590853
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a()V

    .line 2590854
    :cond_3
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-nez v2, :cond_4

    .line 2590855
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a()V

    .line 2590856
    :cond_4
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-nez v2, :cond_c

    .line 2590857
    new-instance v2, LX/31Y;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f083a4b

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f083a4c

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083a53

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/IaP;

    invoke-direct {v4, v1}, LX/IaP;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/IaO;

    invoke-direct {v4, v1}, LX/IaO;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2590858
    :cond_5
    :goto_3
    goto :goto_2

    .line 2590859
    :cond_6
    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-eqz v3, :cond_7

    move v3, v4

    goto/16 :goto_0

    :cond_7
    move v3, v5

    goto/16 :goto_0

    :cond_8
    move v4, v5

    .line 2590860
    goto/16 :goto_1

    .line 2590861
    :cond_9
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2590862
    new-instance v3, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->q()Ljava/lang/String;

    move-result-object v5

    const-string v6, ","

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590863
    const-string v5, "proxy_login_params"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2590864
    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->c()Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-eqz v3, :cond_a

    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2590865
    iget-wide v10, v3, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v5, v10

    .line 2590866
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2590867
    :goto_4
    new-instance v5, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ThirdPartyRegistrationMethod$Params;

    iget-object v6, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-virtual {v6}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    .line 2590868
    iget-object v8, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2590869
    iget-object v9, v8, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v8, v9

    .line 2590870
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, ""

    .line 2590871
    :goto_5
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "promo_data="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v8, v8

    .line 2590872
    invoke-direct {v5, v6, v3, v7, v8}, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ThirdPartyRegistrationMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2590873
    const-string v3, "third_party_registration_params"

    invoke-virtual {v4, v3, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2590874
    iget-object v3, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->H:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    const-string v5, "create_account"

    invoke-virtual {v3, v5, v4}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->start(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 2590875
    :cond_a
    const-string v3, ""

    goto :goto_4

    .line 2590876
    :cond_b
    iget-object v8, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2590877
    iget-object v9, v8, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v8, v9

    .line 2590878
    goto :goto_5

    .line 2590879
    :cond_c
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-nez v2, :cond_d

    .line 2590880
    new-instance v2, LX/31Y;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f083a4f

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f083a50

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083a53

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/IaR;

    invoke-direct {v4, v1}, LX/IaR;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/IaQ;

    invoke-direct {v4, v1}, LX/IaQ;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2590881
    goto/16 :goto_3

    .line 2590882
    :cond_d
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2590883
    const v2, 0x7f083a4e

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->d(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V

    goto/16 :goto_3
.end method
