.class public final LX/HeG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V
    .locals 0

    .prologue
    .line 2490030
    iput-object p1, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2489994
    iget-object v0, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v0, v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489995
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2489996
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2489997
    iget-object v0, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v0, v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    invoke-virtual {v0, p3}, LX/He8;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2489998
    instance-of v1, v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    if-eqz v1, :cond_0

    .line 2489999
    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2490000
    iget-object v1, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490001
    iget-object v2, v1, LX/He8;->d:Ljava/util/List;

    move-object v1, v2

    .line 2490002
    iget-object v2, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v2, v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->q:LX/G5R;

    iget-object v3, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v3, v3, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->u:Ljava/lang/String;

    invoke-virtual {v2, v0, v3, p3, v1}, LX/G5R;->a(Lcom/facebook/search/api/SearchTypeaheadResult;Ljava/lang/String;ILjava/util/List;)V

    .line 2490003
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2490004
    const-string v1, "search_identifier"

    iget-object v3, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v3}, LX/7BK;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490005
    sget-object v1, LX/HeM;->a:[I

    iget-object v3, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v3}, LX/7BK;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2490006
    iget-object v1, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->p:LX/03V;

    sget-object v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->j:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unrecognized result type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490007
    :cond_0
    :goto_0
    return-void

    .line 2490008
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile/"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2490009
    const-string v3, "timeline_friend_request_ref"

    sget-object v4, LX/5P2;->SEARCH:LX/5P2;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2490010
    new-instance v3, LX/5vi;

    invoke-direct {v3}, LX/5vi;-><init>()V

    .line 2490011
    iget-object v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2490012
    iput-object v4, v3, LX/5vi;->d:Ljava/lang/String;

    .line 2490013
    new-instance v4, LX/4aM;

    invoke-direct {v4}, LX/4aM;-><init>()V

    iget-object v5, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2490014
    iput-object v5, v4, LX/4aM;->b:Ljava/lang/String;

    .line 2490015
    move-object v4, v4

    .line 2490016
    invoke-virtual {v4}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    .line 2490017
    iput-object v4, v3, LX/5vi;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2490018
    iget-object v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v4, :cond_1

    .line 2490019
    iget-object v0, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2490020
    iput-object v0, v3, LX/5vi;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2490021
    :cond_1
    invoke-virtual {v3}, LX/5vi;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;

    move-result-object v0

    invoke-static {v2, v0}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    move-object v0, v1

    .line 2490022
    :goto_1
    iget-object v1, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object v3, p0, LX/HeG;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3, v0, v2}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->a$redex0(Lcom/facebook/uberbar/ui/UberbarResultsFragment;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2490023
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "group/"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2490024
    :pswitch_2
    iget-object v1, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 2490025
    iget-object v0, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2490026
    :cond_2
    iget-object v0, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2490027
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "page/"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2490028
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event/"

    invoke-static {v3}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2490029
    :pswitch_5
    iget-object v0, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
