.class public LX/I2G;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I2F;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530107
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2530108
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;)LX/I2F;
    .locals 8

    .prologue
    .line 2530109
    new-instance v0, LX/I2F;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v4

    check-cast v4, LX/6RZ;

    invoke-static {p0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v5

    check-cast v5, LX/Blh;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, LX/I2F;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;Landroid/content/Context;LX/6RZ;LX/Blh;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    .line 2530110
    return-object v0
.end method
