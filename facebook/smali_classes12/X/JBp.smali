.class public final LX/JBp;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2660691
    const-class v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    const v0, 0x6ebf2692

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchTimelineSingleCollectionViewQuery"

    const-string v6, "1b5829651783c0de534dc1bff094dc92"

    const-string v7, "node"

    const-string v8, "10155207561606729"

    const-string v9, "10155259088486729"

    .line 2660692
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2660693
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2660694
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2660695
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2660696
    sparse-switch v0, :sswitch_data_0

    .line 2660697
    :goto_0
    return-object p1

    .line 2660698
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2660699
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2660700
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2660701
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2660702
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2660703
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2660704
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2660705
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2660706
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2660707
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2660708
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2660709
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2660710
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2660711
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x592bc66d -> :sswitch_0
        -0x41a91745 -> :sswitch_a
        -0x3b73c98f -> :sswitch_3
        -0x30f329a4 -> :sswitch_2
        -0x17fb7f63 -> :sswitch_8
        -0x17e5f441 -> :sswitch_4
        -0x14c64f74 -> :sswitch_c
        -0x9ac82a1 -> :sswitch_9
        0x180aba4 -> :sswitch_6
        0x24991595 -> :sswitch_7
        0x291d8de0 -> :sswitch_b
        0x3052e0ff -> :sswitch_1
        0x4b13ca50 -> :sswitch_d
        0x5f424068 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2660712
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2660713
    :goto_1
    return v0

    .line 2660714
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2660715
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
