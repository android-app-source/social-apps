.class public LX/Hd9;
.super LX/Hcr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Hcr",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487947
    invoke-direct {p0}, LX/Hcr;-><init>()V

    .line 2487948
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2487949
    iput-object p1, p0, LX/Hd9;->a:LX/0Ot;

    .line 2487950
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ">;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2487951
    invoke-virtual {p0, p1}, LX/Hcr;->b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V

    .line 2487952
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2487953
    iget-object v0, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1

    .prologue
    .line 2487954
    iget-object v0, p0, LX/Hd9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    return-object v0
.end method

.method public final b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V
    .locals 2

    .prologue
    .line 2487955
    iget-object v0, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v0}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->l()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->l()Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2487956
    :goto_0
    return-void

    .line 2487957
    :cond_0
    iput-object p1, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2487958
    invoke-virtual {p0}, LX/Hd9;->setChanged()V

    .line 2487959
    invoke-virtual {p0}, LX/Hd9;->notifyObservers()V

    goto :goto_0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2487960
    iget-object v0, p0, LX/Hd9;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    return-object v0
.end method
