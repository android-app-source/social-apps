.class public LX/JSU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/17d;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695409
    iput-object p1, p0, LX/JSU;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2695410
    iput-object p2, p0, LX/JSU;->b:LX/17d;

    .line 2695411
    return-void
.end method

.method public static a(LX/JSU;Landroid/content/Context;[Landroid/net/Uri;I)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .param p2    # [Landroid/net/Uri;
        .annotation build Lcom/facebook/feedplugins/musicstory/SimpleMusicPopupManager$MENU_TYPE;
        .end annotation
    .end param

    .prologue
    .line 2695412
    if-nez p3, :cond_0

    .line 2695413
    new-instance v0, LX/JSS;

    invoke-direct {v0, p0, p1, p2}, LX/JSS;-><init>(LX/JSU;Landroid/content/Context;[Landroid/net/Uri;)V

    .line 2695414
    :goto_0
    return-object v0

    .line 2695415
    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 2695416
    new-instance v0, LX/JST;

    invoke-direct {v0, p0, p1, p2}, LX/JST;-><init>(LX/JSU;Landroid/content/Context;[Landroid/net/Uri;)V

    goto :goto_0

    .line 2695417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
