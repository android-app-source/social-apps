.class public final LX/J6J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2649521
    iput-object p1, p0, LX/J6J;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2649522
    iput-object p2, p0, LX/J6J;->b:Ljava/lang/String;

    .line 2649523
    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 2649524
    iget-object v0, p0, LX/J6J;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->j:Ljava/util/Set;

    iget-object v1, p0, LX/J6J;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2649525
    iget-object v0, p0, LX/J6J;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->g:LX/J5k;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_CANCEL:LX/J5h;

    invoke-virtual {v0, v1}, LX/J5k;->a(LX/J5h;)V

    .line 2649526
    :cond_0
    return-void
.end method
