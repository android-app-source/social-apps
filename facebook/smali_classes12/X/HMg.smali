.class public LX/HMg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2457543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/9Zu;)Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2457544
    const/4 v1, 0x0

    .line 2457545
    invoke-interface {p0}, LX/9Zu;->gE_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2457546
    invoke-interface {p0}, LX/9Zu;->gE_()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;

    move-result-object v0

    .line 2457547
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2457548
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel$OrderedImagesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2457549
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
