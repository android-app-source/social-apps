.class public LX/JQ4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQ2;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQ5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2690928
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQ4;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQ5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690929
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2690930
    iput-object p1, p0, LX/JQ4;->b:LX/0Ot;

    .line 2690931
    return-void
.end method

.method public static a(LX/0QB;)LX/JQ4;
    .locals 4

    .prologue
    .line 2690932
    const-class v1, LX/JQ4;

    monitor-enter v1

    .line 2690933
    :try_start_0
    sget-object v0, LX/JQ4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690934
    sput-object v2, LX/JQ4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690935
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690936
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690937
    new-instance v3, LX/JQ4;

    const/16 p0, 0x1ff2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQ4;-><init>(LX/0Ot;)V

    .line 2690938
    move-object v0, v3

    .line 2690939
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690940
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQ4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690941
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690942
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2690943
    const v0, 0x8f499b6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2690944
    iget-object v0, p0, LX/JQ4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQ5;

    const/4 p2, 0x1

    .line 2690945
    iget-object v1, v0, LX/JQ5;->a:LX/17S;

    .line 2690946
    const/16 v2, 0x589

    invoke-static {v1, v2}, LX/17S;->a(LX/17S;I)Z

    move-result v2

    move v1, v2

    .line 2690947
    if-eqz v1, :cond_0

    const v1, 0x7f0810e3

    .line 2690948
    :goto_0
    iget-object v2, v0, LX/JQ5;->c:LX/1We;

    sget-object v3, LX/1Wi;->TOP:LX/1Wi;

    invoke-virtual {v2, v3}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v2

    .line 2690949
    iget-object v3, v2, LX/1Wj;->c:LX/1Wh;

    .line 2690950
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2, v5, p0}, LX/1Wj;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2690951
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    iget p0, v3, LX/1Wh;->a:F

    float-to-int p0, p0

    invoke-interface {v4, v5, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    iget v5, v3, LX/1Wh;->b:F

    float-to-int v5, v5

    invoke-interface {v4, p2, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    iget v3, v3, LX/1Wh;->c:F

    float-to-int v3, v3

    invoke-interface {v4, v5, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a0442

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0033

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 v3, 0x20

    invoke-interface {v1, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    const v3, 0x7f020a84

    invoke-interface {v1, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2690952
    const v2, 0x8f499b6

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2690953
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2690954
    return-object v0

    .line 2690955
    :cond_0
    const v1, 0x7f0810dc

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2690956
    invoke-static {}, LX/1dS;->b()V

    .line 2690957
    iget v0, p1, LX/1dQ;->b:I

    .line 2690958
    packed-switch v0, :pswitch_data_0

    .line 2690959
    :goto_0
    return-object v2

    .line 2690960
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2690961
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2690962
    check-cast v1, LX/JQ3;

    .line 2690963
    iget-object v3, p0, LX/JQ4;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JQ5;

    iget-object v4, v1, LX/JQ3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2690964
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 2690965
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2690966
    check-cast v5, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    invoke-static {v5}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;

    move-result-object v5

    const/4 p2, 0x0

    invoke-virtual {v5, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/4ZX;

    invoke-virtual {v5}, LX/4ZX;->a()LX/162;

    move-result-object v5

    .line 2690967
    iget-object p2, v3, LX/JQ5;->a:LX/17S;

    sget-object p0, LX/99r;->Ego:LX/99r;

    const/4 v1, 0x0

    invoke-virtual {p2, p1, p0, v1, v5}, LX/17S;->a(Landroid/content/Context;LX/99r;Lcom/facebook/graphql/model/GraphQLStory;LX/0lF;)V

    .line 2690968
    invoke-static {v5}, LX/17Q;->q(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2690969
    iget-object p1, v3, LX/JQ5;->a:LX/17S;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p1, p2, v5}, LX/17S;->a(Landroid/content/Context;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2690970
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8f499b6
        :pswitch_0
    .end packed-switch
.end method
