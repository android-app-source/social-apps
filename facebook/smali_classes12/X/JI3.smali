.class public LX/JI3;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:Z

.field public b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2677314
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2677315
    const v0, 0x7f03042c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2677316
    const v0, 0x7f0d0cc7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/JI3;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2677317
    iget-object v0, p0, LX/JI3;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x1

    .line 2677318
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 2677319
    iget-object v0, p0, LX/JI3;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P0;

    invoke-virtual {p0}, LX/JI3;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2677320
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2677313
    iget-boolean v0, p0, LX/JI3;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4bff2605    # 3.3442826E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677321
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2677322
    const/4 v1, 0x1

    .line 2677323
    iput-boolean v1, p0, LX/JI3;->a:Z

    .line 2677324
    const/16 v1, 0x2d

    const v2, 0x7497828f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x651dd798

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677309
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2677310
    const/4 v1, 0x0

    .line 2677311
    iput-boolean v1, p0, LX/JI3;->a:Z

    .line 2677312
    const/16 v1, 0x2d

    const v2, -0x102a8cce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
