.class public LX/IBh;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/IBX;


# instance fields
.field public a:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0SF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2547469
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547470
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IBh;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v3

    check-cast v3, LX/IBl;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v4

    check-cast v4, LX/0y2;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v5

    check-cast v5, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v6

    check-cast v6, LX/0y3;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SF;

    iput-object v3, v2, LX/IBh;->a:LX/IBl;

    iput-object v4, v2, LX/IBh;->b:LX/0y2;

    iput-object v5, v2, LX/IBh;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v6, v2, LX/IBh;->d:LX/0y3;

    iput-object p1, v2, LX/IBh;->e:LX/0ad;

    iput-object v0, v2, LX/IBh;->f:LX/0SF;

    .line 2547471
    invoke-virtual {p0, p0}, LX/IBh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547472
    return-void
.end method

.method public static a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri$Builder;",
            "Ljava/lang/String;",
            "LX/0am",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2547466
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2547467
    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2547468
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 3
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547423
    iput-object p1, p0, LX/IBh;->g:Lcom/facebook/events/model/Event;

    .line 2547424
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2547425
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ae()I

    move-result v0

    .line 2547426
    if-nez v0, :cond_0

    .line 2547427
    invoke-virtual {p0}, LX/IBh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081ee5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2547428
    :goto_0
    move-object v1, v1

    .line 2547429
    if-nez v0, :cond_1

    invoke-virtual {p0}, LX/IBh;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f081ee6

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object v0, v2

    .line 2547430
    iget-object v2, p0, LX/IBh;->a:LX/IBl;

    invoke-virtual {v2, p0, v1, v0, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547431
    iget-object v0, p0, LX/IBh;->a:LX/IBl;

    const v1, 0x7f020911

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547432
    return-void

    :cond_0
    invoke-virtual {p0}, LX/IBh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00e5

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p1, p2

    invoke-virtual {v1, v2, v0, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 9
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2547457
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->R()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IBh;->e:LX/0ad;

    sget-short v2, LX/347;->z:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2547458
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->af()I

    move-result v3

    .line 2547459
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v5

    int-to-long v3, v3

    const-wide/16 v7, 0x3e8

    mul-long/2addr v3, v7

    sub-long/2addr v5, v3

    .line 2547460
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_1

    const/16 v3, 0xb4

    invoke-static {v3}, LX/1m9;->a(I)J

    move-result-wide v3

    add-long/2addr v3, v5

    .line 2547461
    :goto_0
    iget-object v7, p0, LX/IBh;->f:LX/0SF;

    invoke-virtual {v7}, LX/0SF;->a()J

    move-result-wide v7

    .line 2547462
    cmp-long v5, v7, v5

    if-ltz v5, :cond_2

    cmp-long v3, v7, v3

    if-gtz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 2547463
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 2547464
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v3

    goto :goto_0

    .line 2547465
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x61b5b082

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2547433
    iget-object v1, p0, LX/IBh;->g:Lcom/facebook/events/model/Event;

    if-nez v1, :cond_0

    .line 2547434
    const v1, 0x5b63c6fd

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2547435
    :goto_0
    return-void

    .line 2547436
    :cond_0
    iget-object v1, p0, LX/IBh;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p0}, LX/IBh;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2547437
    iget-object v4, p0, LX/IBh;->d:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->a()LX/0yG;

    move-result-object v5

    .line 2547438
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "/events/location_sharing/map/"

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    .line 2547439
    const-string v4, "event_id"

    iget-object v7, p0, LX/IBh;->g:Lcom/facebook/events/model/Event;

    .line 2547440
    iget-object v8, v7, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2547441
    invoke-virtual {v6, v4, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2547442
    const-string v7, "location_services_enabled"

    sget-object v4, LX/0yG;->OKAY:LX/0yG;

    if-ne v5, v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2547443
    const-string v4, "location_services_status"

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-static {v6, v4, v5}, LX/IBh;->a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V

    .line 2547444
    iget-object v4, p0, LX/IBh;->b:LX/0y2;

    invoke-virtual {v4}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v4

    .line 2547445
    if-eqz v4, :cond_1

    .line 2547446
    const-string v5, "lat"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2547447
    const-string v5, "long"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2547448
    const-string v5, "bearing"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->e()LX/0am;

    move-result-object v7

    invoke-static {v6, v5, v7}, LX/IBh;->a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V

    .line 2547449
    const-string v5, "altm"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->d()LX/0am;

    move-result-object v7

    invoke-static {v6, v5, v7}, LX/IBh;->a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V

    .line 2547450
    const-string v5, "acch"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v7

    invoke-static {v6, v5, v7}, LX/IBh;->a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V

    .line 2547451
    const-string v5, "ts"

    invoke-virtual {v4}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v4

    invoke-static {v6, v5, v4}, LX/IBh;->a(Landroid/net/Uri$Builder;Ljava/lang/String;LX/0am;)V

    .line 2547452
    :cond_1
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2547453
    sget-object v5, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2547454
    invoke-interface {v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2547455
    const v1, -0x7f8e4233

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2547456
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method
