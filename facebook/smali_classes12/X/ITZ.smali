.class public final LX/ITZ;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/B9g;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final synthetic b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public final synthetic c:LX/ITj;


# direct methods
.method public constructor <init>(LX/ITj;LX/DML;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 0

    .prologue
    .line 2579351
    iput-object p1, p0, LX/ITZ;->c:LX/ITj;

    iput-object p3, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iput-object p4, p0, LX/ITZ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2579352
    check-cast p1, LX/B9g;

    .line 2579353
    iget-object v0, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    sget-object v1, LX/1XZ;->a:Ljava/lang/String;

    iget-object v2, p0, LX/ITZ;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {p1, v0, v1, v2}, LX/B9g;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 2579354
    iget-object v0, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2579355
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 2579356
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/ITZ;->c:LX/ITj;

    iget-object v1, v1, LX/ITj;->v:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v0}, LX/B9g;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2579357
    :goto_0
    iget-object v0, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    const-string v1, "groups/add_moderator_quick_promotion"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2579358
    new-instance v0, LX/ITX;

    invoke-direct {v0, p0}, LX/ITX;-><init>(LX/ITZ;)V

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2579359
    :cond_0
    :goto_1
    return-void

    .line 2579360
    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/ITZ;->c:LX/ITj;

    iget-object v1, v1, LX/ITj;->v:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v0}, LX/B9g;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2579361
    :cond_2
    iget-object v0, p0, LX/ITZ;->a:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    sget-object v1, LX/0ax;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2579362
    new-instance v0, LX/ITY;

    invoke-direct {v0, p0}, LX/ITY;-><init>(LX/ITZ;)V

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
