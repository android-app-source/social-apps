.class public LX/IVO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IVP;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVO",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IVP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581805
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581806
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/IVO;->b:LX/0Zi;

    .line 2581807
    iput-object p1, p0, LX/IVO;->a:LX/0Ot;

    .line 2581808
    return-void
.end method

.method public static a(LX/0QB;)LX/IVO;
    .locals 4

    .prologue
    .line 2581809
    const-class v1, LX/IVO;

    monitor-enter v1

    .line 2581810
    :try_start_0
    sget-object v0, LX/IVO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581811
    sput-object v2, LX/IVO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581812
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581813
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581814
    new-instance v3, LX/IVO;

    const/16 p0, 0x2429

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVO;-><init>(LX/0Ot;)V

    .line 2581815
    move-object v0, v3

    .line 2581816
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581817
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581818
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581819
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2581820
    check-cast p2, LX/IVN;

    .line 2581821
    iget-object v0, p0, LX/IVO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IVP;

    iget-object v1, p2, LX/IVN;->a:LX/1Pn;

    iget-object v2, p2, LX/IVN;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2581822
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    .line 2581823
    new-instance v5, LX/IVb;

    invoke-direct {v5}, LX/IVb;-><init>()V

    .line 2581824
    sget-object p0, LX/IVc;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/IVa;

    .line 2581825
    if-nez p0, :cond_0

    .line 2581826
    new-instance p0, LX/IVa;

    invoke-direct {p0}, LX/IVa;-><init>()V

    .line 2581827
    :cond_0
    invoke-static {p0, p1, v4, v4, v5}, LX/IVa;->a$redex0(LX/IVa;LX/1De;IILX/IVb;)V

    .line 2581828
    move-object v5, p0

    .line 2581829
    move-object v4, v5

    .line 2581830
    move-object v4, v4

    .line 2581831
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2581832
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2581833
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2581834
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    .line 2581835
    :cond_1
    iget-object v5, v0, LX/IVP;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    sget-object p0, LX/IVP;->a:Ljava/lang/String;

    const-string p2, "TitleFromRenderLocation is missing while rendering substory hscroll component."

    invoke-virtual {v5, p0, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581836
    const-string v5, ""

    .line 2581837
    :goto_0
    move-object v5, v5

    .line 2581838
    iget-object p0, v4, LX/IVa;->a:LX/IVb;

    iput-object v5, p0, LX/IVb;->a:Ljava/lang/String;

    .line 2581839
    iget-object p0, v4, LX/IVa;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2581840
    move-object v4, v4

    .line 2581841
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/IVP;->b:LX/IVV;

    const/4 v5, 0x0

    .line 2581842
    new-instance p0, LX/IVU;

    invoke-direct {p0, v4}, LX/IVU;-><init>(LX/IVV;)V

    .line 2581843
    iget-object p2, v4, LX/IVV;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/IVT;

    .line 2581844
    if-nez p2, :cond_2

    .line 2581845
    new-instance p2, LX/IVT;

    invoke-direct {p2, v4}, LX/IVT;-><init>(LX/IVV;)V

    .line 2581846
    :cond_2
    invoke-static {p2, p1, v5, v5, p0}, LX/IVT;->a$redex0(LX/IVT;LX/1De;IILX/IVU;)V

    .line 2581847
    move-object p0, p2

    .line 2581848
    move-object v5, p0

    .line 2581849
    move-object v4, v5

    .line 2581850
    iget-object v5, v4, LX/IVT;->a:LX/IVU;

    iput-object v1, v5, LX/IVU;->a:LX/1Pn;

    .line 2581851
    iget-object v5, v4, LX/IVT;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2581852
    move-object v4, v4

    .line 2581853
    iget-object v5, v4, LX/IVT;->a:LX/IVU;

    iput-object v2, v5, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2581854
    iget-object v5, v4, LX/IVT;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2581855
    move-object v4, v4

    .line 2581856
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    .line 2581857
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2581858
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    const v5, 0x6bc3011e

    invoke-static {v4, v5}, LX/1VS;->a(Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2581859
    if-eqz v4, :cond_4

    .line 2581860
    iget-object v4, v0, LX/IVP;->c:LX/IVJ;

    const/4 v5, 0x0

    .line 2581861
    new-instance p0, LX/IVI;

    invoke-direct {p0, v4}, LX/IVI;-><init>(LX/IVJ;)V

    .line 2581862
    sget-object p2, LX/IVJ;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/IVH;

    .line 2581863
    if-nez p2, :cond_3

    .line 2581864
    new-instance p2, LX/IVH;

    invoke-direct {p2}, LX/IVH;-><init>()V

    .line 2581865
    :cond_3
    invoke-static {p2, p1, v5, v5, p0}, LX/IVH;->a$redex0(LX/IVH;LX/1De;IILX/IVI;)V

    .line 2581866
    move-object p0, p2

    .line 2581867
    move-object v5, p0

    .line 2581868
    move-object v4, v5

    .line 2581869
    iget-object v5, v4, LX/IVH;->a:LX/IVI;

    iput-object v2, v5, LX/IVI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2581870
    iget-object v5, v4, LX/IVH;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2581871
    move-object v4, v4

    .line 2581872
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b124c

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2581873
    :cond_4
    iget-object v4, v0, LX/IVP;->d:LX/1g8;

    invoke-static {v2}, LX/IVP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v5

    .line 2581874
    iget-object p0, v4, LX/1g8;->a:LX/0Zb;

    const-string p2, "group_member_info_agg_story_render"

    const/4 v0, 0x0

    invoke-interface {p0, p2, v0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2581875
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 2581876
    const-string p2, "group_id"

    invoke-virtual {p0, p2, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2581877
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2581878
    :cond_5
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2581879
    return-object v0

    .line 2581880
    :cond_6
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2581881
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aV()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_7
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2581882
    invoke-static {}, LX/1dS;->b()V

    .line 2581883
    const/4 v0, 0x0

    return-object v0
.end method
