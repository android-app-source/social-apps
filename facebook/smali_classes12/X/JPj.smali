.class public final LX/JPj;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JPk;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/2dx;

.field public final synthetic e:LX/JPk;


# direct methods
.method public constructor <init>(LX/JPk;)V
    .locals 1

    .prologue
    .line 2690296
    iput-object p1, p0, LX/JPj;->e:LX/JPk;

    .line 2690297
    move-object v0, p1

    .line 2690298
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2690299
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2690300
    const-string v0, "VisitYourPageCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2690301
    if-ne p0, p1, :cond_1

    .line 2690302
    :cond_0
    :goto_0
    return v0

    .line 2690303
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2690304
    goto :goto_0

    .line 2690305
    :cond_3
    check-cast p1, LX/JPj;

    .line 2690306
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2690307
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2690308
    if-eq v2, v3, :cond_0

    .line 2690309
    iget-object v2, p0, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2690310
    goto :goto_0

    .line 2690311
    :cond_5
    iget-object v2, p1, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2690312
    :cond_6
    iget-object v2, p0, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2690313
    goto :goto_0

    .line 2690314
    :cond_8
    iget-object v2, p1, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2690315
    :cond_9
    iget-object v2, p0, LX/JPj;->c:LX/1Pn;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JPj;->c:LX/1Pn;

    iget-object v3, p1, LX/JPj;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2690316
    goto :goto_0

    .line 2690317
    :cond_b
    iget-object v2, p1, LX/JPj;->c:LX/1Pn;

    if-nez v2, :cond_a

    .line 2690318
    :cond_c
    iget-object v2, p0, LX/JPj;->d:LX/2dx;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JPj;->d:LX/2dx;

    iget-object v3, p1, LX/JPj;->d:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2690319
    goto :goto_0

    .line 2690320
    :cond_d
    iget-object v2, p1, LX/JPj;->d:LX/2dx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
