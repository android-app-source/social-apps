.class public LX/HF2;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/HF0;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442961
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2442962
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2442963
    iput-object v0, p0, LX/HF2;->a:LX/0Px;

    .line 2442964
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442965
    new-instance v1, LX/HF1;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030e47

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {v1, p0, v0}, LX/HF1;-><init>(LX/HF2;Lcom/facebook/fbui/widget/contentview/ContentView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2442966
    invoke-virtual {p0, p2}, LX/HF2;->e(I)LX/HF0;

    move-result-object v0

    .line 2442967
    if-eqz v0, :cond_0

    .line 2442968
    check-cast p1, LX/HF1;

    .line 2442969
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2442970
    iget-object p0, p1, LX/HF1;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2442971
    iget-object p2, v0, LX/HF0;->a:Ljava/lang/String;

    move-object p2, p2

    .line 2442972
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2442973
    iget-object p0, p1, LX/HF1;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2442974
    iget-object p2, v0, LX/HF0;->c:Ljava/lang/String;

    move-object p2, p2

    .line 2442975
    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2442976
    iget-object p0, p1, LX/HF1;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object p2, p1, LX/HF1;->l:LX/HF2;

    iget-object p2, p2, LX/HF2;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2442977
    :cond_0
    return-void
.end method

.method public final e(I)LX/HF0;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2442978
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/HF2;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/HF2;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HF0;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2442979
    iget-object v0, p0, LX/HF2;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
