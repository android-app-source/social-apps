.class public LX/HtZ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/composer/header/ComposerHeaderViewController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516436
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2516437
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/ViewStub;LX/Hr0;)Lcom/facebook/composer/header/ComposerHeaderViewController;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesLastXyTagChangeTime;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0jD;",
            ":",
            "LX/0j6;",
            ":",
            "LX/0ip;",
            ":",
            "LX/0jG;",
            "DerivedData::",
            "LX/5Qz;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(TServices;",
            "Landroid/view/ViewStub;",
            "LX/Hr0;",
            ")",
            "Lcom/facebook/composer/header/ComposerHeaderViewController",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2516438
    new-instance v0, Lcom/facebook/composer/header/ComposerHeaderViewController;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/User;

    const-class v6, LX/Htc;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Htc;

    invoke-static {p0}, LX/7l0;->b(LX/0QB;)LX/7l0;

    move-result-object v7

    check-cast v7, LX/7l0;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    move-object v9, p1

    check-cast v9, LX/0il;

    move-object v10, p2

    move-object v11, p3

    invoke-direct/range {v0 .. v11}, Lcom/facebook/composer/header/ComposerHeaderViewController;-><init>(LX/1Ad;Landroid/content/res/Resources;Landroid/content/Context;LX/0ad;Lcom/facebook/user/model/User;LX/Htc;LX/7l0;LX/0Uh;LX/0il;Landroid/view/ViewStub;LX/Hr0;)V

    .line 2516439
    return-object v0
.end method
