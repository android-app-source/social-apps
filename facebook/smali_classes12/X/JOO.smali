.class public LX/JOO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOM;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JOP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2687389
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JOO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JOP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687390
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2687391
    iput-object p1, p0, LX/JOO;->b:LX/0Ot;

    .line 2687392
    return-void
.end method

.method public static a(LX/0QB;)LX/JOO;
    .locals 4

    .prologue
    .line 2687393
    const-class v1, LX/JOO;

    monitor-enter v1

    .line 2687394
    :try_start_0
    sget-object v0, LX/JOO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687395
    sput-object v2, LX/JOO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687398
    new-instance v3, LX/JOO;

    const/16 p0, 0x1f90

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOO;-><init>(LX/0Ot;)V

    .line 2687399
    move-object v0, v3

    .line 2687400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2687404
    check-cast p2, LX/JON;

    .line 2687405
    iget-object v0, p0, LX/JOO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JOP;

    iget-object v1, p2, LX/JON;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JON;->b:LX/1SX;

    const/4 v7, 0x1

    .line 2687406
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2687407
    check-cast v3, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;

    .line 2687408
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 2687409
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b1064

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v8, 0x1010036

    const v9, 0x7f0a0158

    invoke-virtual {v7, v8, v9}, LX/1ne;->f(II)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b1063

    invoke-virtual {v7, v8}, LX/1ne;->s(I)LX/1ne;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    sget-object v8, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v7, v8}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 v8, 0x1

    const v9, 0x7f0b08fe

    invoke-interface {v7, v8, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    const v9, 0x7f0b0917

    invoke-interface {v7, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x5

    const v9, 0x7f0b00e5

    invoke-interface {v7, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v6, v7

    .line 2687410
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 2687411
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b0903

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v8, 0x1010038

    const v9, 0x7f0a0158

    invoke-virtual {v7, v8, v9}, LX/1ne;->f(II)LX/1ne;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/4 v8, 0x6

    const v9, 0x7f0b0917

    invoke-interface {v7, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v6, v7

    .line 2687412
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGreetingCardPromotionFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    const/4 p2, 0x6

    const/4 p0, 0x3

    const/4 v10, 0x2

    .line 2687413
    iget-object v6, v0, LX/JOP;->b:LX/1e4;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v6

    .line 2687414
    iget-object v7, v0, LX/JOP;->c:LX/1vg;

    invoke-virtual {v7, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    const v7, -0x6e685d

    invoke-virtual {v6, v7}, LX/2xv;->i(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    .line 2687415
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v8, 0x5

    invoke-interface {v6, v8, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b0904

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    const v8, 0x1010212

    const v9, 0x7f0a0158

    invoke-virtual {v7, v8, v9}, LX/1ne;->f(II)LX/1ne;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7, v10}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b0917

    invoke-interface {v6, p2, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v3, v6

    .line 2687416
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JOP;->a:LX/1vb;

    invoke-virtual {v4, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v4

    sget-object v5, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v4, v5}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2687417
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2687418
    invoke-static {}, LX/1dS;->b()V

    .line 2687419
    const/4 v0, 0x0

    return-object v0
.end method
