.class public final LX/HZr;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)V
    .locals 0

    .prologue
    .line 2482690
    iput-object p1, p0, LX/HZr;->a:Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    .line 2482691
    iget-object v0, p0, LX/HZr;->a:Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    const/4 v6, 0x0

    .line 2482692
    new-instance v1, LX/HZs;

    invoke-direct {v1, v0}, LX/HZs;-><init>(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)V

    invoke-virtual {v0, v1, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(LX/266;Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 2482693
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2482694
    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)LX/Ha0;

    move-result-object v3

    .line 2482695
    if-eqz v3, :cond_4

    iget-object v2, v3, LX/Ha0;->message:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2482696
    sget-object v1, LX/HYl;->a:Ljava/util/Map;

    iget v2, v3, LX/Ha0;->code:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYi;

    .line 2482697
    if-nez v1, :cond_1

    .line 2482698
    sget-object v1, LX/HYi;->UNKNOWN:LX/HYi;

    .line 2482699
    :cond_1
    iget v2, v3, LX/Ha0;->code:I

    const/16 v4, 0xc29

    if-ne v2, v4, :cond_2

    .line 2482700
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v4, 0x31

    invoke-virtual {v2, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v2

    .line 2482701
    iget-object v4, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    const-string v5, "fb4a_reg_email_taken_step"

    invoke-virtual {v4, v5, v2}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2482702
    invoke-virtual {v2, v6}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2482703
    sget-object v1, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    .line 2482704
    :cond_2
    iget v2, v3, LX/Ha0;->code:I

    const/16 v4, 0xcea

    if-ne v2, v4, :cond_3

    .line 2482705
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->q:LX/0Uh;

    const/16 v4, 0x3a

    invoke-virtual {v2, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v2

    .line 2482706
    iget-object v4, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    const-string v5, "fb4a_reg_phone_taken_step"

    invoke-virtual {v4, v5, v2}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2482707
    invoke-virtual {v2, v6}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2482708
    sget-object v1, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    .line 2482709
    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2482710
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2482711
    const-string v5, "error_code"

    iget v6, v3, LX/Ha0;->code:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482712
    const-string v5, "error_body"

    iget-object v3, v3, LX/Ha0;->message:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482713
    invoke-virtual {v1}, LX/HYi;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    .line 2482714
    :cond_4
    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->p(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)Ljava/lang/String;

    move-result-object v5

    .line 2482715
    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2482716
    :cond_5
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->e:LX/HZv;

    .line 2482717
    iget-object v2, v1, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0x400002

    const-string v4, "RegistrationStepValidationTime"

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 2482718
    invoke-virtual {v0}, Lcom/facebook/registration/fragment/RegistrationNetworkRequestFragment;->n()V

    .line 2482719
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2482720
    if-eqz v1, :cond_6

    .line 2482721
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 2482722
    if-eqz v2, :cond_6

    .line 2482723
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    const-string v3, "unknown"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2482724
    iget-object v6, v1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v6, v6

    .line 2482725
    invoke-virtual {v6}, LX/1nY;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2482726
    iget-object v6, v1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v1, v6

    .line 2482727
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v3, v1}, LX/HZt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2482728
    :goto_0
    return-void

    .line 2482729
    :cond_6
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    const-string v2, "unknown"

    const-string v3, "No response or malformed failed response"

    invoke-virtual {v1, v5, v2, v3}, LX/HZt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2482730
    :cond_7
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/SimpleRegFormData;->p()V

    .line 2482731
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2482732
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 2482733
    if-eqz v2, :cond_8

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "error_code"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "error_body"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2482734
    sget-object v3, LX/HYl;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYi;

    .line 2482735
    if-nez v1, :cond_b

    .line 2482736
    sget-object v1, LX/HYi;->UNKNOWN:LX/HYi;

    move-object v3, v1

    .line 2482737
    :goto_2
    const/4 v4, -0x1

    .line 2482738
    :try_start_0
    const-string v1, "error_code"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v4, v1

    .line 2482739
    :goto_3
    iget-object p0, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    const-string v1, "error_body"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v3, v4, v1}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/HYi;ILjava/lang/String;)V

    .line 2482740
    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v1, "error_body"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v5, v4, v1}, LX/HZt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2482741
    :cond_9
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->e:LX/HZv;

    invoke-virtual {v1, v5}, LX/HZv;->a(Ljava/lang/String;)V

    .line 2482742
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/SimpleRegFormData;->r()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2482743
    sget-object v1, LX/HYj;->VALIDATION_ERROR:LX/HYj;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto/16 :goto_0

    .line 2482744
    :cond_a
    sget-object v1, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto/16 :goto_0

    :catch_0
    goto :goto_3

    :cond_b
    move-object v3, v1

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2482745
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2482746
    iget-object v0, p0, LX/HZr;->a:Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;

    .line 2482747
    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->p(Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2482748
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->c:LX/HZt;

    .line 2482749
    iget-object v3, v2, LX/HZt;->a:LX/0Zb;

    sget-object p0, LX/HZu;->STEP_VALIDATION_SUCCESS:LX/HZu;

    invoke-static {v2, p0}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "step_name"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2482750
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->e:LX/HZv;

    invoke-virtual {v2, v1}, LX/HZv;->a(Ljava/lang/String;)V

    .line 2482751
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/SimpleRegFormData;->p()V

    .line 2482752
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationValidateDataFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/SimpleRegFormData;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2482753
    sget-object v1, LX/HYj;->VALIDATION_ERROR:LX/HYj;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    .line 2482754
    :goto_0
    return-void

    .line 2482755
    :cond_0
    sget-object v1, LX/HYj;->VALIDATION_SUCCESS:LX/HYj;

    invoke-virtual {v0, v1}, Lcom/facebook/registration/fragment/RegistrationFragment;->a(LX/HYj;)V

    goto :goto_0
.end method
