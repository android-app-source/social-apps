.class public final LX/HRt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/15i;

.field public final synthetic c:I

.field public final synthetic d:LX/0am;

.field public final synthetic e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;JLX/15i;ILX/0am;)V
    .locals 0

    .prologue
    .line 2465930
    iput-object p1, p0, LX/HRt;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iput-wide p2, p0, LX/HRt;->a:J

    iput-object p4, p0, LX/HRt;->b:LX/15i;

    iput p5, p0, LX/HRt;->c:I

    iput-object p6, p0, LX/HRt;->d:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x320401f9

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465931
    iget-object v0, p0, LX/HRt;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->i:LX/9XE;

    iget-wide v2, p0, LX/HRt;->a:J

    iget-object v4, p0, LX/HRt;->b:LX/15i;

    iget v5, p0, LX/HRt;->c:I

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "pages_manager_activity_tab"

    invoke-virtual {v0, v2, v3, v4, v5}, LX/9XE;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2465932
    iget-object v0, p0, LX/HRt;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->j:LX/GMm;

    invoke-virtual {v0}, LX/GMm;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HRt;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465933
    iget-object v0, p0, LX/HRt;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    invoke-virtual {v0}, LX/HRS;->a()V

    .line 2465934
    :cond_0
    iget-object v0, p0, LX/HRt;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->j:LX/GMm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, LX/HRt;->a:J

    invoke-virtual {v0, v2, v4, v5}, LX/GMm;->a(Landroid/content/Context;J)V

    .line 2465935
    const v0, -0x2359098f

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
