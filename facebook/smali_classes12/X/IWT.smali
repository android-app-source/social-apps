.class public LX/IWT;
.super LX/Dcc;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/IWT;


# instance fields
.field public a:Ljava/lang/String;

.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/DvI;

.field public e:LX/IWJ;

.field public f:LX/0sX;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/DvI;LX/0sX;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583949
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2583950
    iput-object p1, p0, LX/IWT;->c:Ljava/util/concurrent/ExecutorService;

    .line 2583951
    iput-object p2, p0, LX/IWT;->b:LX/0tX;

    .line 2583952
    iput-object p3, p0, LX/IWT;->d:LX/DvI;

    .line 2583953
    const-string v0, ""

    iput-object v0, p0, LX/IWT;->a:Ljava/lang/String;

    .line 2583954
    iput-object p4, p0, LX/IWT;->f:LX/0sX;

    .line 2583955
    return-void
.end method

.method public static a(LX/0QB;)LX/IWT;
    .locals 7

    .prologue
    .line 2583918
    sget-object v0, LX/IWT;->g:LX/IWT;

    if-nez v0, :cond_1

    .line 2583919
    const-class v1, LX/IWT;

    monitor-enter v1

    .line 2583920
    :try_start_0
    sget-object v0, LX/IWT;->g:LX/IWT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2583921
    if-eqz v2, :cond_0

    .line 2583922
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2583923
    new-instance p0, LX/IWT;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/DvI;->a(LX/0QB;)LX/DvI;

    move-result-object v5

    check-cast v5, LX/DvI;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v6

    check-cast v6, LX/0sX;

    invoke-direct {p0, v3, v4, v5, v6}, LX/IWT;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/DvI;LX/0sX;)V

    .line 2583924
    move-object v0, p0

    .line 2583925
    sput-object v0, LX/IWT;->g:LX/IWT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583926
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2583927
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2583928
    :cond_1
    sget-object v0, LX/IWT;->g:LX/IWT;

    return-object v0

    .line 2583929
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2583930
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2583931
    iget-object v1, p0, LX/IWT;->b:LX/0tX;

    .line 2583932
    new-instance v0, LX/IWj;

    invoke-direct {v0}, LX/IWj;-><init>()V

    move-object v0, v0

    .line 2583933
    iget-object v2, p0, LX/IWT;->d:LX/DvI;

    invoke-virtual {v2, v0}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2583934
    check-cast p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    .line 2583935
    const-string v2, "group_id"

    .line 2583936
    iget-object v3, p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2583937
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583938
    const-string v2, "count"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583939
    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/IWT;->f:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583940
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2583941
    const-string v2, "before"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583942
    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2583943
    const-string v2, "after"

    invoke-virtual {v0, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2583944
    :cond_1
    move-object v0, v0

    .line 2583945
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    if-eqz p5, :cond_2

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_0
    invoke-virtual {v2, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2583946
    new-instance v1, LX/IWS;

    invoke-direct {v1, p0}, LX/IWS;-><init>(LX/IWT;)V

    iget-object v2, p0, LX/IWT;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2583947
    return-object v0

    .line 2583948
    :cond_2
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_0
.end method
