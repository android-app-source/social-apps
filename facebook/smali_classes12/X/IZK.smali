.class public LX/IZK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2589224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2589225
    iput-object p1, p0, LX/IZK;->a:Landroid/content/Context;

    .line 2589226
    return-void
.end method

.method public static a(LX/0QB;)LX/IZK;
    .locals 1

    .prologue
    .line 2589235
    invoke-static {p0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IZK;
    .locals 2

    .prologue
    .line 2589233
    new-instance v1, LX/IZK;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/IZK;-><init>(Landroid/content/Context;)V

    .line 2589234
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2589231
    iget-object v0, p0, LX/IZK;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IZK;->a(Ljava/lang/String;)V

    .line 2589232
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2589227
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2589228
    invoke-virtual {p0}, LX/IZK;->a()V

    .line 2589229
    :goto_0
    return-void

    .line 2589230
    :cond_0
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/IZK;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/IZK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081ca1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IZJ;

    invoke-direct {v2, p0}, LX/IZJ;-><init>(LX/IZK;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method
