.class public final LX/IE1;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/IE2;

.field private b:I


# direct methods
.method public constructor <init>(LX/IE2;)V
    .locals 0

    .prologue
    .line 2551387
    iput-object p1, p0, LX/IE1;->a:LX/IE2;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2551388
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2551389
    new-instance v5, Landroid/widget/Filter$FilterResults;

    invoke-direct {v5}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 2551390
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2551391
    iput v3, v5, Landroid/widget/Filter$FilterResults;->count:I

    .line 2551392
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v5, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 2551393
    :goto_0
    return-object v5

    .line 2551394
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2551395
    iget-object v0, p0, LX/IE1;->a:LX/IE2;

    invoke-static {v0}, LX/IE2;->f(LX/IE2;)Ljava/util/List;

    move-result-object v1

    .line 2551396
    iget-object v0, p0, LX/IE1;->a:LX/IE2;

    iget-object v0, v0, LX/IE2;->h:Ljava/lang/String;

    invoke-static {v4, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2551397
    iget-object v0, p0, LX/IE1;->a:LX/IE2;

    iget-object v0, v0, LX/IE2;->f:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2551398
    iget v0, p0, LX/IE1;->b:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 2551399
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IDH;

    .line 2551400
    invoke-virtual {v0}, LX/IDH;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2551401
    const-string v2, "\\s"

    invoke-virtual {v8, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v10, v9

    move v2, v3

    :goto_3
    if-ge v2, v10, :cond_1

    aget-object v11, v9, v2

    .line 2551402
    invoke-virtual {v11, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v8, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2551403
    :cond_2
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2551404
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2551405
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, LX/IE1;->b:I

    .line 2551406
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v5, Landroid/widget/Filter$FilterResults;->count:I

    .line 2551407
    iput-object v6, v5, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 2551408
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2551409
    iget-object v2, p0, LX/IE1;->a:LX/IE2;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 2551410
    iput-object v0, v2, LX/IE2;->f:Ljava/util/List;

    .line 2551411
    iget-object v0, p0, LX/IE1;->a:LX/IE2;

    .line 2551412
    iput-object v1, v0, LX/IE2;->h:Ljava/lang/String;

    .line 2551413
    return-void
.end method
