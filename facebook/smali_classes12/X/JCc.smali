.class public LX/JCc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/JCc;


# instance fields
.field public final a:LX/JCx;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/J93;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z


# direct methods
.method public constructor <init>(LX/JCx;LX/0Or;LX/J93;LX/03V;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsNativeCollectionsSubscribeEnabled;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsOGCollectionsSectionViewFacewebEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsPlacesFacewebEnabled;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JCx;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/J93;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2662662
    iput-object p1, p0, LX/JCc;->a:LX/JCx;

    .line 2662663
    iput-object p2, p0, LX/JCc;->b:LX/0Or;

    .line 2662664
    iput-object p3, p0, LX/JCc;->d:LX/J93;

    .line 2662665
    iput-object p4, p0, LX/JCc;->c:LX/03V;

    .line 2662666
    iput-object p5, p0, LX/JCc;->e:LX/0Or;

    .line 2662667
    iput-object p6, p0, LX/JCc;->f:LX/0Or;

    .line 2662668
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/JCc;->g:Z

    .line 2662669
    return-void
.end method

.method public static a(LX/0QB;)LX/JCc;
    .locals 11

    .prologue
    .line 2662648
    sget-object v0, LX/JCc;->h:LX/JCc;

    if-nez v0, :cond_1

    .line 2662649
    const-class v1, LX/JCc;

    monitor-enter v1

    .line 2662650
    :try_start_0
    sget-object v0, LX/JCc;->h:LX/JCc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2662651
    if-eqz v2, :cond_0

    .line 2662652
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2662653
    new-instance v3, LX/JCc;

    invoke-static {v0}, LX/JCx;->a(LX/0QB;)LX/JCx;

    move-result-object v4

    check-cast v4, LX/JCx;

    const/16 v5, 0x1585

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/J93;->a(LX/0QB;)LX/J93;

    move-result-object v6

    check-cast v6, LX/J93;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const/16 v8, 0x366

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x367

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v10}, LX/JCc;-><init>(LX/JCx;LX/0Or;LX/J93;LX/03V;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V

    .line 2662654
    move-object v0, v3

    .line 2662655
    sput-object v0, LX/JCc;->h:LX/JCc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2662656
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2662657
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2662658
    :cond_1
    sget-object v0, LX/JCc;->h:LX/JCc;

    return-object v0

    .line 2662659
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2662660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getArgs"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2662597
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2662598
    invoke-interface {p0}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v1, v2, :cond_0

    .line 2662599
    const-string v1, "has_tagged_mediaset"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2662600
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662601
    :cond_0
    return-object v0
.end method

.method public static a(LX/JAc;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2662647
    if-eqz p1, :cond_0

    invoke-interface {p0}, LX/JAb;->j()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/JAb;->mU_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2662644
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2662645
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2662646
    sget-object v0, LX/0ax;->en:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;LX/9lP;Z)Ljava/lang/String;
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUrl"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2662602
    sget-object v0, LX/JCb;->a:[I

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2662603
    const/4 v0, 0x1

    .line 2662604
    iget-object v1, p0, LX/JCc;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_a

    invoke-static {p1, p3}, LX/JCc;->a(LX/JAc;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MAP:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v1, v2, :cond_a

    .line 2662605
    :cond_0
    :goto_0
    move v0, v0

    .line 2662606
    if-nez v0, :cond_1

    iget-object v0, p0, LX/JCc;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v0, v1, :cond_1

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v1, :cond_5

    .line 2662607
    :cond_1
    invoke-static {p1, p3}, LX/JCc;->a(LX/JAc;Z)Ljava/lang/String;

    move-result-object v0

    .line 2662608
    :goto_1
    return-object v0

    .line 2662609
    :pswitch_0
    if-eqz p3, :cond_3

    .line 2662610
    iget-boolean v0, p0, LX/JCc;->g:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/0ax;->bG:Ljava/lang/String;

    .line 2662611
    :goto_2
    iget-object v1, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2662612
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget-object v0, LX/0ax;->bF:Ljava/lang/String;

    goto :goto_2

    .line 2662613
    :cond_3
    :pswitch_1
    invoke-static {p1, p3}, LX/JCc;->a(LX/JAc;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2662614
    :pswitch_2
    sget-object v0, LX/0ax;->bO:Ljava/lang/String;

    .line 2662615
    iget-object v1, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2662616
    sget-object v2, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP:LX/DHr;

    invoke-virtual {v3}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2662617
    :pswitch_3
    invoke-virtual {p2}, LX/9lP;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2662618
    sget-object v0, LX/0ax;->bY:Ljava/lang/String;

    goto :goto_1

    .line 2662619
    :cond_4
    sget-object v0, LX/0ax;->bZ:Ljava/lang/String;

    .line 2662620
    iget-object v1, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2662621
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2662622
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    if-nez v0, :cond_6

    .line 2662623
    iget-object v0, p0, LX/JCc;->c:LX/03V;

    const-string v1, "TimelineAppSectionUrlBuilder_nullCollectionsPeek"

    const-string v2, "collectionsPeek is null, collectionsNoItems will be ignored"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662624
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->a()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2662625
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_c

    move v1, v2

    .line 2662626
    :goto_3
    move v0, v1

    .line 2662627
    if-eqz v0, :cond_9

    .line 2662628
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 2662629
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsNoItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JAV;

    .line 2662630
    iget-object v1, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2662631
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/JAb;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/J93;->a(LX/JAV;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2662632
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->a()I

    move-result v0

    if-ne v0, v4, :cond_8

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2662633
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JAV;

    .line 2662634
    iget-object v1, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2662635
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/JAb;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/J93;->a(LX/JAV;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2662636
    :cond_8
    iget-object v0, p2, LX/9lP;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2662637
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/JAb;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/JCc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2662638
    :cond_9
    invoke-static {p1, p3}, LX/JCc;->a(LX/JAc;Z)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    iget-object v1, p0, LX/JCc;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/03R;->YES:LX/03R;

    if-ne v1, v2, :cond_b

    invoke-static {p1, p3}, LX/JCc;->a(LX/JAc;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v1, v2, :cond_0

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v1, v2, :cond_0

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v1, v2, :cond_0

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v1, v2, :cond_0

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2662639
    :cond_c
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsPeekFieldsModel;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_4
    if-ge v6, v8, :cond_e

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JAW;

    .line 2662640
    invoke-interface {v1}, LX/JAW;->d()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v1

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v1, v9}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    move v1, v5

    .line 2662641
    goto/16 :goto_3

    .line 2662642
    :cond_d
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_4

    :cond_e
    move v1, v2

    .line 2662643
    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
