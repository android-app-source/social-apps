.class public final enum LX/HUp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HUp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HUp;

.field public static final enum FETCH_VIDEO_LISTS_WITH_VIDEOS:LX/HUp;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2473859
    new-instance v0, LX/HUp;

    const-string v1, "FETCH_VIDEO_LISTS_WITH_VIDEOS"

    invoke-direct {v0, v1, v2}, LX/HUp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUp;->FETCH_VIDEO_LISTS_WITH_VIDEOS:LX/HUp;

    .line 2473860
    const/4 v0, 0x1

    new-array v0, v0, [LX/HUp;

    sget-object v1, LX/HUp;->FETCH_VIDEO_LISTS_WITH_VIDEOS:LX/HUp;

    aput-object v1, v0, v2

    sput-object v0, LX/HUp;->$VALUES:[LX/HUp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2473858
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HUp;
    .locals 1

    .prologue
    .line 2473857
    const-class v0, LX/HUp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HUp;

    return-object v0
.end method

.method public static values()[LX/HUp;
    .locals 1

    .prologue
    .line 2473856
    sget-object v0, LX/HUp;->$VALUES:[LX/HUp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HUp;

    return-object v0
.end method
