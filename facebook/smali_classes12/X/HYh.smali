.class public final enum LX/HYh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HYh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HYh;

.field public static final enum ADDED:LX/HYh;

.field public static final enum NO_GOOGLE_ACCOUNT:LX/HYh;

.field public static final enum PLAY_SERVICE_NOT_AVAILABLE:LX/HYh;

.field public static final enum SKIPPED:LX/HYh;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2480967
    new-instance v0, LX/HYh;

    const-string v1, "ADDED"

    invoke-direct {v0, v1, v2}, LX/HYh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYh;->ADDED:LX/HYh;

    .line 2480968
    new-instance v0, LX/HYh;

    const-string v1, "NO_GOOGLE_ACCOUNT"

    invoke-direct {v0, v1, v3}, LX/HYh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYh;->NO_GOOGLE_ACCOUNT:LX/HYh;

    .line 2480969
    new-instance v0, LX/HYh;

    const-string v1, "PLAY_SERVICE_NOT_AVAILABLE"

    invoke-direct {v0, v1, v4}, LX/HYh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYh;->PLAY_SERVICE_NOT_AVAILABLE:LX/HYh;

    .line 2480970
    new-instance v0, LX/HYh;

    const-string v1, "SKIPPED"

    invoke-direct {v0, v1, v5}, LX/HYh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYh;->SKIPPED:LX/HYh;

    .line 2480971
    const/4 v0, 0x4

    new-array v0, v0, [LX/HYh;

    sget-object v1, LX/HYh;->ADDED:LX/HYh;

    aput-object v1, v0, v2

    sget-object v1, LX/HYh;->NO_GOOGLE_ACCOUNT:LX/HYh;

    aput-object v1, v0, v3

    sget-object v1, LX/HYh;->PLAY_SERVICE_NOT_AVAILABLE:LX/HYh;

    aput-object v1, v0, v4

    sget-object v1, LX/HYh;->SKIPPED:LX/HYh;

    aput-object v1, v0, v5

    sput-object v0, LX/HYh;->$VALUES:[LX/HYh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2480973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HYh;
    .locals 1

    .prologue
    .line 2480974
    const-class v0, LX/HYh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HYh;

    return-object v0
.end method

.method public static values()[LX/HYh;
    .locals 1

    .prologue
    .line 2480972
    sget-object v0, LX/HYh;->$VALUES:[LX/HYh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HYh;

    return-object v0
.end method
