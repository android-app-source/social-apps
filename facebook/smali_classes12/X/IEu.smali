.class public final LX/IEu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V
    .locals 0

    .prologue
    .line 2553436
    iput-object p1, p0, LX/IEu;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3e897ed4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2553437
    iget-object v1, p0, LX/IEu;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->d:LX/IEx;

    iget-object v2, p0, LX/IEu;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->j:Ljava/lang/String;

    .line 2553438
    iget-object v4, v1, LX/IEx;->a:LX/0Zb;

    const-string v5, "close_sticker_picker"

    invoke-static {v1, v5}, LX/IEx;->c(LX/IEx;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "composer_session_id"

    invoke-virtual {v5, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2553439
    iget-object v1, p0, LX/IEu;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2553440
    const v1, 0x1fd0ecda

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
