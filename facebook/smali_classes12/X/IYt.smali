.class public LX/IYt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0gh;

.field private final c:LX/2Mk;

.field public final d:LX/6f6;

.field private final e:LX/DdY;

.field private final f:LX/2Ou;

.field private final g:Lcom/facebook/debug/activitytracer/ActivityTracer;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0gh;LX/2Mk;LX/6f6;LX/DdY;LX/2Ou;Lcom/facebook/debug/activitytracer/ActivityTracer;LX/0Or;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/analytics/tagging/ForAnalyticsAppInterface;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0gh;",
            "LX/2Mk;",
            "LX/6f6;",
            "LX/DdY;",
            "LX/2Ou;",
            "Lcom/facebook/debug/activitytracer/ActivityTracer;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2588282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2588283
    iput-object p1, p0, LX/IYt;->a:LX/0Zb;

    .line 2588284
    iput-object p2, p0, LX/IYt;->b:LX/0gh;

    .line 2588285
    iput-object p3, p0, LX/IYt;->c:LX/2Mk;

    .line 2588286
    iput-object p4, p0, LX/IYt;->d:LX/6f6;

    .line 2588287
    iput-object p5, p0, LX/IYt;->e:LX/DdY;

    .line 2588288
    iput-object p6, p0, LX/IYt;->f:LX/2Ou;

    .line 2588289
    iput-object p7, p0, LX/IYt;->g:Lcom/facebook/debug/activitytracer/ActivityTracer;

    .line 2588290
    iput-object p8, p0, LX/IYt;->h:LX/0Or;

    .line 2588291
    return-void
.end method

.method public static a(LX/0QB;)LX/IYt;
    .locals 10

    .prologue
    .line 2588292
    new-instance v1, LX/IYt;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v3

    check-cast v3, LX/0gh;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v4

    check-cast v4, LX/2Mk;

    invoke-static {p0}, LX/6f6;->b(LX/0QB;)LX/6f6;

    move-result-object v5

    check-cast v5, LX/6f6;

    invoke-static {p0}, LX/DdY;->a(LX/0QB;)LX/DdY;

    move-result-object v6

    check-cast v6, LX/DdY;

    invoke-static {p0}, LX/2Ou;->a(LX/0QB;)LX/2Ou;

    move-result-object v7

    check-cast v7, LX/2Ou;

    invoke-static {p0}, Lcom/facebook/debug/activitytracer/ActivityTracer;->a(LX/0QB;)Lcom/facebook/debug/activitytracer/ActivityTracer;

    move-result-object v8

    check-cast v8, Lcom/facebook/debug/activitytracer/ActivityTracer;

    const/16 v9, 0x15e4

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, LX/IYt;-><init>(LX/0Zb;LX/0gh;LX/2Mk;LX/6f6;LX/DdY;LX/2Ou;Lcom/facebook/debug/activitytracer/ActivityTracer;LX/0Or;)V

    .line 2588293
    move-object v0, v1

    .line 2588294
    return-object v0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2588295
    if-lez p2, :cond_0

    .line 2588296
    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2588297
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2588298
    if-lez p2, :cond_0

    .line 2588299
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2588300
    :cond_0
    return-void
.end method
