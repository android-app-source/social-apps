.class public final LX/HH3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V
    .locals 0

    .prologue
    .line 2446779
    iput-object p1, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2446780
    iget-object v0, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "loadPagesAlbumsListFromCache"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2446781
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2446782
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2446783
    iget-object v0, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->C:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2446784
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2446785
    if-nez v0, :cond_1

    .line 2446786
    :cond_0
    :goto_0
    return-void

    .line 2446787
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2446788
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    if-eqz v1, :cond_0

    .line 2446789
    iget-object v1, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V

    .line 2446790
    iget-object v0, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    if-eqz v0, :cond_0

    .line 2446791
    iget-object v0, p0, LX/HH3;->a:Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method
