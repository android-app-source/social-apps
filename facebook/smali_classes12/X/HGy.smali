.class public LX/HGy;
.super LX/3x6;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2446742
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2446743
    iput-object p1, p0, LX/HGy;->a:Landroid/graphics/drawable/Drawable;

    .line 2446744
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 2446745
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)LX/HMF;
    .locals 4

    .prologue
    .line 2446746
    new-instance v0, LX/HMF;

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, LX/HMF;-><init>(II)V

    return-object v0
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 7

    .prologue
    .line 2446747
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v2

    .line 2446748
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2446749
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2446750
    invoke-virtual {p0, v3, p2}, LX/HGy;->a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2446751
    invoke-virtual {p0, v3, p2}, LX/HGy;->b(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)LX/HMF;

    move-result-object v0

    .line 2446752
    iget v4, v0, LX/HMF;->a:I

    .line 2446753
    iget v5, v0, LX/HMF;->b:I

    .line 2446754
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 2446755
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    .line 2446756
    iget-object v3, p0, LX/HGy;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 2446757
    iget-object v6, p0, LX/HGy;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v4, v0, v5, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2446758
    iget-object v0, p0, LX/HGy;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2446759
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2446760
    :cond_1
    return-void
.end method
