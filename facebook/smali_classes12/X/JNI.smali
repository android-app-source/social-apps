.class public LX/JNI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/7vW;

.field public final c:LX/7vZ;

.field public final d:LX/1Ck;

.field private final e:LX/1vg;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1nQ;

.field public final h:LX/0kx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2685595
    const-class v0, LX/JNI;

    sput-object v0, LX/JNI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/7vW;LX/7vZ;LX/1Ck;LX/1vg;LX/0Ot;LX/1nQ;LX/0kx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7vW;",
            "LX/7vZ;",
            "LX/1Ck;",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/1nQ;",
            "LX/0kx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2685587
    iput-object p1, p0, LX/JNI;->b:LX/7vW;

    .line 2685588
    iput-object p2, p0, LX/JNI;->c:LX/7vZ;

    .line 2685589
    iput-object p3, p0, LX/JNI;->d:LX/1Ck;

    .line 2685590
    iput-object p4, p0, LX/JNI;->e:LX/1vg;

    .line 2685591
    iput-object p5, p0, LX/JNI;->f:LX/0Ot;

    .line 2685592
    iput-object p6, p0, LX/JNI;->g:LX/1nQ;

    .line 2685593
    iput-object p7, p0, LX/JNI;->h:LX/0kx;

    .line 2685594
    return-void
.end method

.method public static a(LX/JNI;LX/1De;IIIZ)LX/1Di;
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 2685580
    iget-object v0, p0, LX/JNI;->e:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 2685581
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    if-eqz p5, :cond_0

    const v0, 0x7f0812af

    :goto_0
    invoke-interface {v2, v0}, LX/1Dh;->Y(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v3, 0x7f0b0b61

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1ne;->m(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    if-eqz p5, :cond_1

    .line 2685582
    const v0, -0x2cc49489

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 2685583
    :goto_1
    invoke-interface {v1, v0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0812ae

    goto :goto_0

    .line 2685584
    :cond_1
    const v0, 0x43bb1758

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    move-object v0, v0

    .line 2685585
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/JNI;
    .locals 11

    .prologue
    .line 2685569
    const-class v1, LX/JNI;

    monitor-enter v1

    .line 2685570
    :try_start_0
    sget-object v0, LX/JNI;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685571
    sput-object v2, LX/JNI;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685572
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685573
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685574
    new-instance v3, LX/JNI;

    invoke-static {v0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v4

    check-cast v4, LX/7vW;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v5

    check-cast v5, LX/7vZ;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v7

    check-cast v7, LX/1vg;

    const/16 v8, 0x3be

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v9

    check-cast v9, LX/1nQ;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v10

    check-cast v10, LX/0kx;

    invoke-direct/range {v3 .. v10}, LX/JNI;-><init>(LX/7vW;LX/7vZ;LX/1Ck;LX/1vg;LX/0Ot;LX/1nQ;LX/0kx;)V

    .line 2685575
    move-object v0, v3

    .line 2685576
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685577
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685578
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2685564
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2685565
    :try_start_0
    const-string v0, "events_suggestion_type"

    invoke-virtual {v1, v0, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2685566
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2685567
    :catch_0
    move-exception v0

    .line 2685568
    sget-object v2, LX/JNI;->a:Ljava/lang/Class;

    const-string v3, "Error add events suggestion type"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
