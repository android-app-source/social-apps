.class public final LX/IPJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IPK;


# direct methods
.method public constructor <init>(LX/IPK;)V
    .locals 0

    .prologue
    .line 2573960
    iput-object p1, p0, LX/IPJ;->a:LX/IPK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2573961
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v1, v0, LX/IPK;->b:LX/IPM;

    monitor-enter v1

    .line 2573962
    :try_start_0
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v0, v0, LX/IPK;->b:LX/IPM;

    invoke-static {v0, p1}, LX/IPM;->a$redex0(LX/IPM;Ljava/lang/Throwable;)V

    .line 2573963
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2573964
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2573965
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v1, v0, LX/IPK;->b:LX/IPM;

    monitor-enter v1

    .line 2573966
    :try_start_0
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v0, v0, LX/IPK;->b:LX/IPM;

    iget-object v0, v0, LX/IPM;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2573967
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v0, v0, LX/IPK;->b:LX/IPM;

    iget-object v0, v0, LX/IPM;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2573968
    :cond_0
    iget-object v0, p0, LX/IPJ;->a:LX/IPK;

    iget-object v0, v0, LX/IPK;->b:LX/IPM;

    .line 2573969
    invoke-static {v0}, LX/IPM;->k(LX/IPM;)V

    .line 2573970
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2573971
    check-cast p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2573972
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2573973
    check-cast p0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;->a()I

    move-result p0

    if-nez p0, :cond_2

    .line 2573974
    :cond_1
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/IPM;->l:Z

    .line 2573975
    invoke-static {v0}, LX/IPM;->h(LX/IPM;)V

    .line 2573976
    :goto_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2573977
    :cond_2
    invoke-static {v0, p1}, LX/IPM;->b(LX/IPM;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method
