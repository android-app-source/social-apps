.class public final LX/Hfb;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Hff;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Hfe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Hff",
            "<TTGroupInfo;>.WorkGroupsTabSectionImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Hff;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Hff;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2492022
    iput-object p1, p0, LX/Hfb;->b:LX/Hff;

    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 2492023
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "recyclerEventsController"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "alphabeticalGroupsConnectionControllerLease"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "recentGroupsConnectionControllerLease"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "favoritedGroupsConnectionControllerLease"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "topGroupsConnectionControllerLease"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "groupHScrollComponent"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Hfb;->c:[Ljava/lang/String;

    .line 2492024
    iput v3, p0, LX/Hfb;->d:I

    .line 2492025
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Hfb;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Hfb;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2492026
    invoke-super {p0}, LX/BcN;->a()V

    .line 2492027
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hfb;->a:LX/Hfe;

    .line 2492028
    iget-object v0, p0, LX/Hfb;->b:LX/Hff;

    iget-object v0, v0, LX/Hff;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2492029
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Hff;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2492030
    iget-object v1, p0, LX/Hfb;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Hfb;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Hfb;->d:I

    if-ge v1, v2, :cond_2

    .line 2492031
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2492032
    :goto_0
    iget v2, p0, LX/Hfb;->d:I

    if-ge v0, v2, :cond_1

    .line 2492033
    iget-object v2, p0, LX/Hfb;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2492034
    iget-object v2, p0, LX/Hfb;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492035
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2492036
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2492037
    :cond_2
    iget-object v0, p0, LX/Hfb;->a:LX/Hfe;

    .line 2492038
    invoke-virtual {p0}, LX/Hfb;->a()V

    .line 2492039
    return-object v0
.end method

.method public final b(LX/BcQ;)LX/Hfb;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Hff",
            "<TTGroupInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2492040
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 2492041
    return-object p0
.end method
