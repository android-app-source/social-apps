.class public final enum LX/JTV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTV;

.field public static final enum auth_fail:LX/JTV;

.field public static final enum auth_success:LX/JTV;

.field public static final enum deep_link:LX/JTV;

.field public static final enum spotify_save:LX/JTV;

.field public static final enum spotify_unsave:LX/JTV;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2696774
    new-instance v0, LX/JTV;

    const-string v1, "deep_link"

    invoke-direct {v0, v1, v2}, LX/JTV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTV;->deep_link:LX/JTV;

    .line 2696775
    new-instance v0, LX/JTV;

    const-string v1, "spotify_save"

    invoke-direct {v0, v1, v3}, LX/JTV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTV;->spotify_save:LX/JTV;

    .line 2696776
    new-instance v0, LX/JTV;

    const-string v1, "spotify_unsave"

    invoke-direct {v0, v1, v4}, LX/JTV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTV;->spotify_unsave:LX/JTV;

    .line 2696777
    new-instance v0, LX/JTV;

    const-string v1, "auth_success"

    invoke-direct {v0, v1, v5}, LX/JTV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTV;->auth_success:LX/JTV;

    .line 2696778
    new-instance v0, LX/JTV;

    const-string v1, "auth_fail"

    invoke-direct {v0, v1, v6}, LX/JTV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JTV;->auth_fail:LX/JTV;

    .line 2696779
    const/4 v0, 0x5

    new-array v0, v0, [LX/JTV;

    sget-object v1, LX/JTV;->deep_link:LX/JTV;

    aput-object v1, v0, v2

    sget-object v1, LX/JTV;->spotify_save:LX/JTV;

    aput-object v1, v0, v3

    sget-object v1, LX/JTV;->spotify_unsave:LX/JTV;

    aput-object v1, v0, v4

    sget-object v1, LX/JTV;->auth_success:LX/JTV;

    aput-object v1, v0, v5

    sget-object v1, LX/JTV;->auth_fail:LX/JTV;

    aput-object v1, v0, v6

    sput-object v0, LX/JTV;->$VALUES:[LX/JTV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2696780
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTV;
    .locals 1

    .prologue
    .line 2696781
    const-class v0, LX/JTV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTV;

    return-object v0
.end method

.method public static values()[LX/JTV;
    .locals 1

    .prologue
    .line 2696782
    sget-object v0, LX/JTV;->$VALUES:[LX/JTV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTV;

    return-object v0
.end method
