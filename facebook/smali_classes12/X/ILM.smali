.class public final LX/ILM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/ILO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;)V
    .locals 0

    .prologue
    .line 2567857
    iput-object p1, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2567848
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    const/16 p1, 0x8

    const/4 p0, 0x0

    .line 2567849
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2567850
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2567851
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->k:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2567852
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->n:Lcom/facebook/widget/error/GenericErrorView;

    if-nez v1, :cond_0

    .line 2567853
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->m:Lcom/facebook/widget/error/GenericErrorViewStub;

    invoke-virtual {v1}, LX/4nv;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/error/GenericErrorView;

    iput-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->n:Lcom/facebook/widget/error/GenericErrorView;

    .line 2567854
    :cond_0
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->n:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2567855
    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->n:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v1}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    .line 2567856
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2567827
    check-cast p1, LX/ILO;

    const/4 v2, 0x0

    .line 2567828
    if-nez p1, :cond_0

    .line 2567829
    :goto_0
    return-void

    .line 2567830
    :cond_0
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->o:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2567831
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2567832
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->k:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2567833
    iget-object v0, p1, LX/ILO;->a:LX/0Px;

    if-eqz v0, :cond_1

    .line 2567834
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-object v1, p1, LX/ILO;->b:Ljava/lang/String;

    .line 2567835
    iput-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567836
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-boolean v1, p1, LX/ILO;->c:Z

    .line 2567837
    iput-boolean v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->j:Z

    .line 2567838
    :goto_1
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->q:LX/ILI;

    iget-object v1, p1, LX/ILO;->a:LX/0Px;

    .line 2567839
    if-nez v1, :cond_2

    .line 2567840
    :goto_2
    goto :goto_0

    .line 2567841
    :cond_1
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    const/4 v1, 0x0

    .line 2567842
    iput-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->i:Ljava/lang/String;

    .line 2567843
    iget-object v0, p0, LX/ILM;->a:Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;

    .line 2567844
    iput-boolean v2, v0, Lcom/facebook/groups/community/fragments/CommunityGroupsListFragment;->j:Z

    .line 2567845
    goto :goto_1

    .line 2567846
    :cond_2
    iget-object v2, v0, LX/ILI;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2567847
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_2
.end method
