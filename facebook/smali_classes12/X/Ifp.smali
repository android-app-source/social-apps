.class public LX/Ifp;
.super LX/6LI;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/6LJ;

.field public c:LX/Ifo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6LJ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2600125
    const-string v0, "InviteToMessengerNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2600126
    iput-object p1, p0, LX/Ifp;->a:Landroid/content/Context;

    .line 2600127
    iput-object p2, p0, LX/Ifp;->b:LX/6LJ;

    .line 2600128
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2600129
    iget-object v0, p0, LX/Ifp;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030995

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2600130
    new-instance v0, LX/Ifm;

    invoke-direct {v0, p0}, LX/Ifm;-><init>(LX/Ifp;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2600131
    const v0, 0x7f0d0827

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2600132
    iget-object v1, p0, LX/Ifp;->d:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2600133
    iget-object v1, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2600134
    packed-switch v1, :pswitch_data_0

    .line 2600135
    iget-object v5, p0, LX/Ifp;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f002d

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v8, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v7, v3

    iget-object v3, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v7, v2

    add-int/lit8 v2, v1, -0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v7, v9

    invoke-virtual {v5, v6, v1, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2600136
    :goto_1
    const v0, 0x7f0d1870

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2600137
    new-instance v1, LX/Ifn;

    invoke-direct {v1, p0}, LX/Ifn;-><init>(LX/Ifp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2600138
    return-object v4

    :cond_0
    move v1, v3

    .line 2600139
    goto :goto_0

    .line 2600140
    :pswitch_0
    iget-object v1, p0, LX/Ifp;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f08067a

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v1, v5, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2600141
    :pswitch_1
    iget-object v1, p0, LX/Ifp;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f08067b

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v3, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2600142
    :pswitch_2
    iget-object v1, p0, LX/Ifp;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f08067c

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v3

    iget-object v3, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v6, v2

    iget-object v2, p0, LX/Ifp;->d:LX/0Px;

    invoke-virtual {v2, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v6, v9

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
