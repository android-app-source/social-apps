.class public LX/JO0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/JNv;

.field public final b:LX/3mL;

.field public final c:LX/1LV;


# direct methods
.method public constructor <init>(LX/JNv;LX/3mL;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686766
    iput-object p1, p0, LX/JO0;->a:LX/JNv;

    .line 2686767
    iput-object p2, p0, LX/JO0;->b:LX/3mL;

    .line 2686768
    iput-object p3, p0, LX/JO0;->c:LX/1LV;

    .line 2686769
    return-void
.end method

.method public static a(LX/0QB;)LX/JO0;
    .locals 6

    .prologue
    .line 2686770
    const-class v1, LX/JO0;

    monitor-enter v1

    .line 2686771
    :try_start_0
    sget-object v0, LX/JO0;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686772
    sput-object v2, LX/JO0;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686775
    new-instance p0, LX/JO0;

    const-class v3, LX/JNv;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JNv;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    invoke-direct {p0, v3, v4, v5}, LX/JO0;-><init>(LX/JNv;LX/3mL;LX/1LV;)V

    .line 2686776
    move-object v0, p0

    .line 2686777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JO0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
