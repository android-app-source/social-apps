.class public final LX/J6d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J64;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V
    .locals 0

    .prologue
    .line 2649938
    iput-object p1, p0, LX/J6d;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1oT;)V
    .locals 5

    .prologue
    .line 2649929
    iget-object v0, p0, LX/J6d;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget-object v1, p0, LX/J6d;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    .line 2649930
    iget-object v2, v0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v2, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2649931
    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v2

    iget-object v2, v2, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J4J;

    .line 2649932
    iget-object v3, v2, LX/J4J;->f:LX/8SR;

    invoke-virtual {v3}, LX/8SR;->b()LX/1oT;

    move-result-object v3

    .line 2649933
    iget-object v4, v0, LX/J3t;->k:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 2649934
    iget-object v4, v0, LX/J3t;->k:Ljava/util/Map;

    invoke-interface {v4, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2649935
    :cond_0
    iget-object v2, v2, LX/J4J;->f:LX/8SR;

    invoke-virtual {v2, p2}, LX/8SR;->b(LX/1oT;)V

    .line 2649936
    iget-object v0, p0, LX/J6d;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->j:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, -0x7430c10

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649937
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2649939
    iget-object v0, p0, LX/J6d;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->j:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, -0x1cb3e267

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649940
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2649926
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2649927
    return-void
.end method

.method public final c()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 1

    .prologue
    .line 2649928
    const/4 v0, 0x0

    return-object v0
.end method
