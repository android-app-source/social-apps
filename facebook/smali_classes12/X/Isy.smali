.class public final LX/Isy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/service/model/FetchThreadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/send/client/NewMessageSenderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V
    .locals 0

    .prologue
    .line 2621515
    iput-object p1, p0, LX/Isy;->a:Lcom/facebook/messaging/send/client/NewMessageSenderFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2621521
    iget-object v0, p0, LX/Isy;->a:Lcom/facebook/messaging/send/client/NewMessageSenderFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a$redex0(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;ZLjava/lang/Throwable;)V

    .line 2621522
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2621516
    check-cast p1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2621517
    iget-object v0, p0, LX/Isy;->a:Lcom/facebook/messaging/send/client/NewMessageSenderFragment;

    .line 2621518
    iget-object v1, v0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v3, v0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    invoke-static {v0, v1, v3, v4}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2621519
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2621520
    return-void
.end method
