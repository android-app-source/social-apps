.class public LX/Hjr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/HjN;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/HjN;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/HjN;->getDefaultErrorMessage()Ljava/lang/String;

    move-result-object p2

    :cond_0
    iput-object p1, p0, LX/Hjr;->a:LX/HjN;

    iput-object p2, p0, LX/Hjr;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final b()LX/Hj0;
    .locals 3

    iget-object v0, p0, LX/Hjr;->a:LX/HjN;

    invoke-virtual {v0}, LX/HjN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/Hj0;

    iget-object v1, p0, LX/Hjr;->a:LX/HjN;

    invoke-virtual {v1}, LX/HjN;->getErrorCode()I

    move-result v1

    iget-object v2, p0, LX/Hjr;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Hj0;

    sget-object v1, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    invoke-virtual {v1}, LX/HjN;->getErrorCode()I

    move-result v1

    sget-object v2, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    invoke-virtual {v2}, LX/HjN;->getDefaultErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method
