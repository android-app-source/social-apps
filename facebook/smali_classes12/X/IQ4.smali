.class public LX/IQ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574958
    iput-object p1, p0, LX/IQ4;->a:LX/0ad;

    .line 2574959
    iput-object p2, p0, LX/IQ4;->b:LX/0Or;

    .line 2574960
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2574961
    iget-object v0, p0, LX/IQ4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574962
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2574963
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2574964
    const-string v2, "propertyToUpdate"

    const-string v3, "pending"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574965
    const-string v2, "group"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2574966
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v2, "/group_pending_posts"

    .line 2574967
    iput-object v2, v0, LX/98r;->a:Ljava/lang/String;

    .line 2574968
    move-object v0, v0

    .line 2574969
    const-string v2, "GroupsPendingPostsRoute"

    .line 2574970
    iput-object v2, v0, LX/98r;->b:Ljava/lang/String;

    .line 2574971
    move-object v0, v0

    .line 2574972
    iput-object v1, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 2574973
    move-object v0, v0

    .line 2574974
    const v1, 0x7f081bb0

    .line 2574975
    iput v1, v0, LX/98r;->d:I

    .line 2574976
    move-object v0, v0

    .line 2574977
    const-string v1, "pending_posts_admin"

    .line 2574978
    iput-object v1, v0, LX/98r;->g:Ljava/lang/String;

    .line 2574979
    move-object v0, v0

    .line 2574980
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2574981
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 2574982
    :goto_0
    return-object v0

    .line 2574983
    :cond_0
    new-instance v0, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;

    invoke-direct {v0}, Lcom/facebook/groups/feed/ui/GroupPendingPostsFragment;-><init>()V

    .line 2574984
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method
