.class public final LX/IiP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2604247
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2604248
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604249
    :goto_0
    return v1

    .line 2604250
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604251
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2604252
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2604253
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2604254
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2604255
    const-string v3, "matched_messages"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2604256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2604257
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2604258
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2604259
    const/4 v3, 0x0

    .line 2604260
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_e

    .line 2604261
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604262
    :goto_3
    move v2, v3

    .line 2604263
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2604264
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2604265
    goto :goto_1

    .line 2604266
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2604267
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2604268
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2604269
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604270
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_d

    .line 2604271
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2604272
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2604273
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_6

    if-eqz v9, :cond_6

    .line 2604274
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2604275
    :cond_7
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_4

    .line 2604276
    :cond_8
    const-string v10, "message_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2604277
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 2604278
    :cond_9
    const-string v10, "message_sender"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2604279
    invoke-static {p0, p1}, LX/IiN;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_4

    .line 2604280
    :cond_a
    const-string v10, "message_thread_key"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2604281
    const/4 v9, 0x0

    .line 2604282
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_13

    .line 2604283
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604284
    :goto_5
    move v5, v9

    .line 2604285
    goto :goto_4

    .line 2604286
    :cond_b
    const-string v10, "snippet"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2604287
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 2604288
    :cond_c
    const-string v10, "timestamp_precise"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2604289
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_4

    .line 2604290
    :cond_d
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2604291
    invoke-virtual {p1, v3, v8}, LX/186;->b(II)V

    .line 2604292
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v7}, LX/186;->b(II)V

    .line 2604293
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 2604294
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v5}, LX/186;->b(II)V

    .line 2604295
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2604296
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2604297
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_e
    move v2, v3

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    goto/16 :goto_4

    .line 2604298
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604299
    :cond_10
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_12

    .line 2604300
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2604301
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2604302
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_10

    if-eqz v11, :cond_10

    .line 2604303
    const-string v12, "other_user_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 2604304
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_6

    .line 2604305
    :cond_11
    const-string v12, "thread_fbid"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 2604306
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 2604307
    :cond_12
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2604308
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 2604309
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 2604310
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_5

    :cond_13
    move v5, v9

    move v10, v9

    goto :goto_6
.end method
