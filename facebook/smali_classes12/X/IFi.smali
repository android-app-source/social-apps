.class public final LX/IFi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IIN;

.field public final synthetic b:LX/IFl;


# direct methods
.method public constructor <init>(LX/IFl;LX/IIN;)V
    .locals 0

    .prologue
    .line 2554745
    iput-object p1, p0, LX/IFi;->b:LX/IFl;

    iput-object p2, p0, LX/IFi;->a:LX/IIN;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2554746
    iget-object v0, p0, LX/IFi;->b:LX/IFl;

    sget-object v1, LX/IFk;->ERROR:LX/IFk;

    .line 2554747
    iput-object v1, v0, LX/IFl;->h:LX/IFk;

    .line 2554748
    sget-object v0, LX/IFl;->a:Ljava/lang/Class;

    const-string v1, "Failed to load more friends"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2554749
    iget-object v0, p0, LX/IFi;->a:LX/IIN;

    if-eqz v0, :cond_0

    .line 2554750
    iget-object v0, p0, LX/IFi;->a:LX/IIN;

    invoke-virtual {v0}, LX/IIN;->a()V

    .line 2554751
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2554752
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2554753
    if-nez p1, :cond_1

    .line 2554754
    :cond_0
    :goto_0
    return-void

    .line 2554755
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2554756
    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;

    .line 2554757
    iget-object v1, p0, LX/IFi;->b:LX/IFl;

    invoke-static {v1, v0}, LX/IFl;->b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)LX/IFd;

    move-result-object v1

    .line 2554758
    iget-object v2, p0, LX/IFi;->b:LX/IFl;

    iget-object v3, p0, LX/IFi;->b:LX/IFl;

    invoke-static {v3, v0}, LX/IFl;->b(LX/IFl;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySearchQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2554759
    iput-object v0, v2, LX/IFl;->i:Ljava/lang/String;

    .line 2554760
    iget-object v0, p0, LX/IFi;->b:LX/IFl;

    iget-object v0, v0, LX/IFl;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2554761
    iget-object v0, p0, LX/IFi;->b:LX/IFl;

    sget-object v2, LX/IFk;->COMPLETE:LX/IFk;

    .line 2554762
    iput-object v2, v0, LX/IFl;->h:LX/IFk;

    .line 2554763
    :goto_1
    iget-object v0, p0, LX/IFi;->a:LX/IIN;

    if-eqz v0, :cond_0

    .line 2554764
    iget-object v0, p0, LX/IFi;->a:LX/IIN;

    .line 2554765
    invoke-static {v0, v1}, LX/IIN;->c(LX/IIN;LX/IFd;)V

    .line 2554766
    iget-object v2, v0, LX/IIN;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/SplitHideableListView;->setSelection(I)V

    .line 2554767
    goto :goto_0

    .line 2554768
    :cond_2
    iget-object v0, p0, LX/IFi;->b:LX/IFl;

    sget-object v2, LX/IFk;->HAS_MORE:LX/IFk;

    .line 2554769
    iput-object v2, v0, LX/IFl;->h:LX/IFk;

    .line 2554770
    goto :goto_1
.end method
