.class public LX/JVe;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public final a:LX/JVd;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2701268
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JVe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2701269
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2701279
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2701280
    const v0, 0x7f030f3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2701281
    new-instance v0, LX/JVd;

    invoke-direct {v0, p0}, LX/JVd;-><init>(LX/JVe;)V

    iput-object v0, p0, LX/JVe;->a:LX/JVd;

    .line 2701282
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2701278
    iget-boolean v0, p0, LX/JVe;->b:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x307f63d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2701274
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2701275
    const/4 v1, 0x1

    .line 2701276
    iput-boolean v1, p0, LX/JVe;->b:Z

    .line 2701277
    const/16 v1, 0x2d

    const v2, 0x7df16f6a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4319c242

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2701270
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2701271
    const/4 v1, 0x0

    .line 2701272
    iput-boolean v1, p0, LX/JVe;->b:Z

    .line 2701273
    const/16 v1, 0x2d

    const v2, -0x333aa026    # -1.0348104E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
