.class public LX/J8q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652218
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 9

    .prologue
    .line 2652194
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2652195
    const-string v1, "section_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2652196
    const-string v2, "collections_icon"

    invoke-static {p1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1Fb;

    .line 2652197
    const-string v2, "collection_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2652198
    const-string v3, "friendship_status"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2652199
    const-string v4, "subscribe_status"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2652200
    const-string v5, "view_name"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2652201
    const-string v5, "collection"

    invoke-static {p1, v5}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/JBL;

    .line 2652202
    const-string v5, "collections_section_type"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2652203
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2652204
    const-string p1, "profile_id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652205
    const-string p1, "section_id"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652206
    const-string p1, "collection_id"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652207
    const-string p1, "friendship_status"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652208
    const-string p1, "subscribe_status"

    invoke-virtual {p0, p1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652209
    const-string p1, "collections_section_type"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2652210
    const-string p1, "view_name"

    invoke-virtual {p0, p1, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652211
    const-string p1, "collections_icon"

    invoke-static {p0, p1, v7}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2652212
    const-string p1, "collection"

    invoke-static {p0, p1, v8}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2652213
    new-instance p1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-direct {p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;-><init>()V

    .line 2652214
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2652215
    move-object v0, p1

    .line 2652216
    return-object v0
.end method
