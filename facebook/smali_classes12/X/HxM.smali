.class public LX/HxM;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field public a:LX/HxA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2522362
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2522363
    invoke-direct {p0}, LX/HxM;->a()V

    .line 2522364
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2522365
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2522366
    invoke-direct {p0}, LX/HxM;->a()V

    .line 2522367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2522368
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2522369
    invoke-direct {p0}, LX/HxM;->a()V

    .line 2522370
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2522371
    const-class v0, LX/HxM;

    invoke-static {v0, p0}, LX/HxM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2522372
    new-instance v0, LX/HxL;

    invoke-direct {v0, p0}, LX/HxL;-><init>(LX/HxM;)V

    invoke-virtual {p0, v0}, LX/HxM;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522373
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/HxM;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/HxM;

    invoke-static {v0}, LX/HxA;->b(LX/0QB;)LX/HxA;

    move-result-object v0

    check-cast v0, LX/HxA;

    iput-object v0, p0, LX/HxM;->a:LX/HxA;

    return-void
.end method
