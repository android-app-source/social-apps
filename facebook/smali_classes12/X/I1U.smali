.class public LX/I1U;
.super LX/1a1;
.source ""


# instance fields
.field private l:Lcom/facebook/fig/listitem/FigListItem;

.field public final m:Landroid/content/Context;

.field public final n:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final o:LX/6RZ;

.field public final p:LX/Blh;


# direct methods
.method public constructor <init>(Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/6RZ;LX/Blh;)V
    .locals 0
    .param p1    # Lcom/facebook/fig/listitem/FigListItem;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528979
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2528980
    iput-object p1, p0, LX/I1U;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2528981
    iput-object p3, p0, LX/I1U;->m:Landroid/content/Context;

    .line 2528982
    iput-object p2, p0, LX/I1U;->n:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528983
    iput-object p4, p0, LX/I1U;->o:LX/6RZ;

    .line 2528984
    iput-object p5, p0, LX/I1U;->p:LX/Blh;

    .line 2528985
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V
    .locals 14

    .prologue
    .line 2528986
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    .line 2528987
    iget-object v1, p0, LX/I1U;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/I1U;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0821f4

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, LX/7oa;->eR_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, LX/I1U;->o:LX/6RZ;

    invoke-interface {v0}, LX/7oa;->eQ_()Z

    move-result v7

    new-instance v8, Ljava/util/Date;

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0}, LX/7oa;->j()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    new-instance v9, Ljava/util/Date;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0}, LX/7oa;->b()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7, v8, v9}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2528988
    iget-object v1, p0, LX/I1U;->l:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v2, LX/I1T;

    invoke-direct {v2, p0, v0}, LX/I1T;-><init>(LX/I1U;LX/7oa;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528989
    return-void
.end method
