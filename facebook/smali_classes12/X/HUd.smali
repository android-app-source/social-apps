.class public final LX/HUd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V
    .locals 0

    .prologue
    .line 2473329
    iput-object p1, p0, LX/HUd;->b:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iput-boolean p2, p0, LX/HUd;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2473330
    iget-object v0, p0, LX/HUd;->b:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->c:LX/0tX;

    iget-object v1, p0, LX/HUd;->b:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v2, p0, LX/HUd;->a:Z

    .line 2473331
    invoke-static {}, LX/HUu;->a()LX/HUt;

    move-result-object v3

    .line 2473332
    const-string v4, "page_id"

    iget-wide v5, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "video_list_entries_per_fetch"

    const-string v6, "3"

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "page_video_list_max_videos"

    const-string v6, "3"

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "media_type"

    sget-object v6, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v5, "count"

    const/16 v6, 0x9

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0e91

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "should_fetch_videos"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2473333
    iget-object v4, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->k:LX/0se;

    iget-object v5, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->j:LX/0rq;

    invoke-virtual {v5}, LX/0rq;->c()LX/0wF;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2473334
    iget-object v4, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->x:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2473335
    const-string v4, "after_video_list_page"

    iget-object v5, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->x:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2473336
    :cond_0
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    move-object v1, v3

    .line 2473337
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2473338
    return-object v0
.end method
