.class public final LX/JGb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/JGa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field

.field public d:LX/JGa;

.field public e:I


# direct methods
.method public constructor <init>([Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TE;)V"
        }
    .end annotation

    .prologue
    .line 2667730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2667731
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JGb;->a:Ljava/util/ArrayList;

    .line 2667732
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    .line 2667733
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGb;->d:LX/JGa;

    .line 2667734
    const/4 v0, 0x0

    iput v0, p0, LX/JGb;->e:I

    .line 2667735
    iput-object p1, p0, LX/JGb;->c:[Ljava/lang/Object;

    .line 2667736
    iget-object v0, p0, LX/JGb;->a:Ljava/util/ArrayList;

    iget-object v1, p0, LX/JGb;->d:LX/JGa;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2667737
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 2667697
    iget-object v0, p0, LX/JGb;->d:LX/JGa;

    move-object v0, v0

    .line 2667698
    iget v1, v0, LX/JGa;->b:I

    iget-object v2, v0, LX/JGa;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    iget-object v1, v0, LX/JGa;->a:[Ljava/lang/Object;

    iget v2, v0, LX/JGa;->b:I

    aget-object v1, v1, v2

    if-ne v1, p1, :cond_0

    .line 2667699
    iget v1, v0, LX/JGa;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/JGa;->b:I

    .line 2667700
    :goto_0
    iget-object v0, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 2667701
    return-void

    .line 2667702
    :cond_0
    const v1, 0x7fffffff

    iput v1, v0, LX/JGa;->b:I

    goto :goto_0
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2667720
    iget v0, p0, LX/JGb;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/JGb;->e:I

    .line 2667721
    iget v0, p0, LX/JGb;->e:I

    iget-object v1, p0, LX/JGb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2667722
    new-instance v0, LX/JGa;

    invoke-direct {v0}, LX/JGa;-><init>()V

    iput-object v0, p0, LX/JGb;->d:LX/JGa;

    .line 2667723
    iget-object v0, p0, LX/JGb;->a:Ljava/util/ArrayList;

    iget-object v1, p0, LX/JGb;->d:LX/JGa;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2667724
    :goto_0
    iget-object v0, p0, LX/JGb;->d:LX/JGa;

    move-object v0, v0

    .line 2667725
    iput-object p1, v0, LX/JGa;->a:[Ljava/lang/Object;

    .line 2667726
    const/4 v1, 0x0

    iput v1, v0, LX/JGa;->b:I

    .line 2667727
    iget-object v1, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v1

    iput v1, v0, LX/JGa;->c:I

    .line 2667728
    return-void

    .line 2667729
    :cond_0
    iget-object v0, p0, LX/JGb;->a:Ljava/util/ArrayList;

    iget v1, p0, LX/JGb;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGa;

    iput-object v0, p0, LX/JGb;->d:LX/JGa;

    goto :goto_0
.end method

.method public final a()[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TE;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2667703
    iget-object v0, p0, LX/JGb;->d:LX/JGa;

    move-object v2, v0

    .line 2667704
    iget v0, p0, LX/JGb;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/JGb;->e:I

    .line 2667705
    iget-object v0, p0, LX/JGb;->a:Ljava/util/ArrayList;

    iget v3, p0, LX/JGb;->e:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JGa;

    iput-object v0, p0, LX/JGb;->d:LX/JGa;

    .line 2667706
    iget-object v0, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->size()I

    move-result v0

    iget v3, v2, LX/JGa;->c:I

    sub-int v3, v0, v3

    .line 2667707
    iget v0, v2, LX/JGa;->b:I

    iget-object v4, v2, LX/JGa;->a:[Ljava/lang/Object;

    array-length v4, v4

    if-eq v0, v4, :cond_1

    .line 2667708
    if-nez v3, :cond_3

    .line 2667709
    iget-object v0, p0, LX/JGb;->c:[Ljava/lang/Object;

    .line 2667710
    :cond_0
    move-object v0, v0

    .line 2667711
    :goto_0
    iput-object v1, v2, LX/JGa;->a:[Ljava/lang/Object;

    .line 2667712
    return-object v0

    .line 2667713
    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    .line 2667714
    iget-object v4, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->pollLast()Ljava/lang/Object;

    .line 2667715
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 2667716
    :cond_3
    iget-object v0, p0, LX/JGb;->c:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 2667717
    add-int/lit8 v4, v3, -0x1

    :goto_2
    if-ltz v4, :cond_0

    .line 2667718
    iget-object v5, p0, LX/JGb;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v5}, Ljava/util/ArrayDeque;->pollLast()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v0, v4

    .line 2667719
    add-int/lit8 v4, v4, -0x1

    goto :goto_2
.end method
