.class public final LX/Hfe;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Hff;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:LX/Hfw;

.field public d:LX/5K7;

.field public e:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;"
        }
    .end annotation
.end field

.field public f:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;"
        }
    .end annotation
.end field

.field public g:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;"
        }
    .end annotation
.end field

.field public h:LX/95R;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95R",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            "TTGroupInfo;>;"
        }
    .end annotation
.end field

.field public i:LX/1X1;

.field public final synthetic j:LX/Hff;


# direct methods
.method public constructor <init>(LX/Hff;)V
    .locals 1

    .prologue
    .line 2492069
    iput-object p1, p0, LX/Hfe;->j:LX/Hff;

    .line 2492070
    move-object v0, p1

    .line 2492071
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2492072
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 2492073
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Hfe;

    .line 2492074
    if-nez p1, :cond_0

    .line 2492075
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Hfe;->b:Z

    .line 2492076
    const/4 v1, 0x0

    iput-object v1, v0, LX/Hfe;->c:LX/Hfw;

    .line 2492077
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2492078
    if-ne p0, p1, :cond_1

    .line 2492079
    :cond_0
    :goto_0
    return v0

    .line 2492080
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2492081
    goto :goto_0

    .line 2492082
    :cond_3
    check-cast p1, LX/Hfe;

    .line 2492083
    iget-boolean v2, p0, LX/Hfe;->b:Z

    iget-boolean v3, p1, LX/Hfe;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2492084
    goto :goto_0

    .line 2492085
    :cond_4
    iget-object v2, p0, LX/Hfe;->c:LX/Hfw;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Hfe;->c:LX/Hfw;

    iget-object v3, p1, LX/Hfe;->c:LX/Hfw;

    invoke-virtual {v2, v3}, LX/Hfw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2492086
    goto :goto_0

    .line 2492087
    :cond_6
    iget-object v2, p1, LX/Hfe;->c:LX/Hfw;

    if-nez v2, :cond_5

    .line 2492088
    :cond_7
    iget-object v2, p0, LX/Hfe;->d:LX/5K7;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Hfe;->d:LX/5K7;

    iget-object v3, p1, LX/Hfe;->d:LX/5K7;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2492089
    goto :goto_0

    .line 2492090
    :cond_9
    iget-object v2, p1, LX/Hfe;->d:LX/5K7;

    if-nez v2, :cond_8

    .line 2492091
    :cond_a
    iget-object v2, p0, LX/Hfe;->e:LX/95R;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Hfe;->e:LX/95R;

    iget-object v3, p1, LX/Hfe;->e:LX/95R;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2492092
    goto :goto_0

    .line 2492093
    :cond_c
    iget-object v2, p1, LX/Hfe;->e:LX/95R;

    if-nez v2, :cond_b

    .line 2492094
    :cond_d
    iget-object v2, p0, LX/Hfe;->f:LX/95R;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/Hfe;->f:LX/95R;

    iget-object v3, p1, LX/Hfe;->f:LX/95R;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2492095
    goto :goto_0

    .line 2492096
    :cond_f
    iget-object v2, p1, LX/Hfe;->f:LX/95R;

    if-nez v2, :cond_e

    .line 2492097
    :cond_10
    iget-object v2, p0, LX/Hfe;->g:LX/95R;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/Hfe;->g:LX/95R;

    iget-object v3, p1, LX/Hfe;->g:LX/95R;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2492098
    goto :goto_0

    .line 2492099
    :cond_12
    iget-object v2, p1, LX/Hfe;->g:LX/95R;

    if-nez v2, :cond_11

    .line 2492100
    :cond_13
    iget-object v2, p0, LX/Hfe;->h:LX/95R;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/Hfe;->h:LX/95R;

    iget-object v3, p1, LX/Hfe;->h:LX/95R;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 2492101
    goto/16 :goto_0

    .line 2492102
    :cond_15
    iget-object v2, p1, LX/Hfe;->h:LX/95R;

    if-nez v2, :cond_14

    .line 2492103
    :cond_16
    iget-object v2, p0, LX/Hfe;->i:LX/1X1;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/Hfe;->i:LX/1X1;

    iget-object v3, p1, LX/Hfe;->i:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2492104
    goto/16 :goto_0

    .line 2492105
    :cond_17
    iget-object v2, p1, LX/Hfe;->i:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
