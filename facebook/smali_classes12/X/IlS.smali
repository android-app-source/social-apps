.class public LX/IlS;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:LX/IlP;

.field private b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IlP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607808
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2607809
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2607810
    iput-object v0, p0, LX/IlS;->b:LX/0Px;

    .line 2607811
    iput-object p1, p0, LX/IlS;->a:LX/IlP;

    .line 2607812
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2607813
    iget-object v0, p0, LX/IlS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2607814
    iget-object v0, p0, LX/IlS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IlS;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2607815
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2607816
    iget-object v0, p0, LX/IlS;->a:LX/IlP;

    iget-object v1, p0, LX/IlS;->b:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, LX/IlP;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method
