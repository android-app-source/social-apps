.class public final LX/HiH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field public final d:I

.field public final e:I

.field private f:Z

.field public g:I

.field public h:I

.field private i:[F


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 2497167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497168
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    iput v0, p0, LX/HiH;->a:I

    .line 2497169
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    iput v0, p0, LX/HiH;->b:I

    .line 2497170
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    iput v0, p0, LX/HiH;->c:I

    .line 2497171
    iput p1, p0, LX/HiH;->d:I

    .line 2497172
    iput p2, p0, LX/HiH;->e:I

    .line 2497173
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 2497214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497215
    iput p1, p0, LX/HiH;->a:I

    .line 2497216
    iput p2, p0, LX/HiH;->b:I

    .line 2497217
    iput p3, p0, LX/HiH;->c:I

    .line 2497218
    invoke-static {p1, p2, p3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, LX/HiH;->d:I

    .line 2497219
    iput p4, p0, LX/HiH;->e:I

    .line 2497220
    return-void
.end method

.method public static f(LX/HiH;)V
    .locals 8

    .prologue
    const/high16 v3, 0x40900000    # 4.5f

    const/high16 v7, 0x40400000    # 3.0f

    const/4 v6, 0x1

    const/high16 v5, -0x1000000

    const/4 v4, -0x1

    .line 2497195
    iget-boolean v0, p0, LX/HiH;->f:Z

    if-nez v0, :cond_0

    .line 2497196
    iget v0, p0, LX/HiH;->d:I

    invoke-static {v4, v0, v3}, LX/3qk;->a(IIF)I

    move-result v0

    .line 2497197
    iget v1, p0, LX/HiH;->d:I

    invoke-static {v4, v1, v7}, LX/3qk;->a(IIF)I

    move-result v1

    .line 2497198
    if-eq v0, v4, :cond_1

    if-eq v1, v4, :cond_1

    .line 2497199
    invoke-static {v4, v0}, LX/3qk;->b(II)I

    move-result v0

    iput v0, p0, LX/HiH;->h:I

    .line 2497200
    invoke-static {v4, v1}, LX/3qk;->b(II)I

    move-result v0

    iput v0, p0, LX/HiH;->g:I

    .line 2497201
    iput-boolean v6, p0, LX/HiH;->f:Z

    .line 2497202
    :cond_0
    :goto_0
    return-void

    .line 2497203
    :cond_1
    iget v2, p0, LX/HiH;->d:I

    invoke-static {v5, v2, v3}, LX/3qk;->a(IIF)I

    move-result v2

    .line 2497204
    iget v3, p0, LX/HiH;->d:I

    invoke-static {v5, v3, v7}, LX/3qk;->a(IIF)I

    move-result v3

    .line 2497205
    if-eq v2, v4, :cond_2

    if-eq v2, v4, :cond_2

    .line 2497206
    invoke-static {v5, v2}, LX/3qk;->b(II)I

    move-result v0

    iput v0, p0, LX/HiH;->h:I

    .line 2497207
    invoke-static {v5, v3}, LX/3qk;->b(II)I

    move-result v0

    iput v0, p0, LX/HiH;->g:I

    .line 2497208
    iput-boolean v6, p0, LX/HiH;->f:Z

    goto :goto_0

    .line 2497209
    :cond_2
    if-eq v0, v4, :cond_3

    invoke-static {v4, v0}, LX/3qk;->b(II)I

    move-result v0

    :goto_1
    iput v0, p0, LX/HiH;->h:I

    .line 2497210
    if-eq v1, v4, :cond_4

    invoke-static {v4, v1}, LX/3qk;->b(II)I

    move-result v0

    :goto_2
    iput v0, p0, LX/HiH;->g:I

    .line 2497211
    iput-boolean v6, p0, LX/HiH;->f:Z

    goto :goto_0

    .line 2497212
    :cond_3
    invoke-static {v5, v2}, LX/3qk;->b(II)I

    move-result v0

    goto :goto_1

    .line 2497213
    :cond_4
    invoke-static {v5, v3}, LX/3qk;->b(II)I

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public final b()[F
    .locals 4

    .prologue
    .line 2497191
    iget-object v0, p0, LX/HiH;->i:[F

    if-nez v0, :cond_0

    .line 2497192
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, LX/HiH;->i:[F

    .line 2497193
    iget v0, p0, LX/HiH;->a:I

    iget v1, p0, LX/HiH;->b:I

    iget v2, p0, LX/HiH;->c:I

    iget-object v3, p0, LX/HiH;->i:[F

    invoke-static {v0, v1, v2, v3}, LX/3qk;->a(III[F)V

    .line 2497194
    :cond_0
    iget-object v0, p0, LX/HiH;->i:[F

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2497190
    iget v0, p0, LX/HiH;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2497184
    if-ne p0, p1, :cond_1

    .line 2497185
    :cond_0
    :goto_0
    return v0

    .line 2497186
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2497187
    goto :goto_0

    .line 2497188
    :cond_3
    check-cast p1, LX/HiH;

    .line 2497189
    iget v2, p0, LX/HiH;->e:I

    iget v3, p1, LX/HiH;->e:I

    if-ne v2, v3, :cond_4

    iget v2, p0, LX/HiH;->d:I

    iget v3, p1, LX/HiH;->d:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2497183
    iget v0, p0, LX/HiH;->d:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/HiH;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x5d

    .line 2497174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " [RGB: #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2497175
    iget v1, p0, LX/HiH;->d:I

    move v1, v1

    .line 2497176
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [HSL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/HiH;->b()[F

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Population: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/HiH;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Title Text: #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2497177
    invoke-static {p0}, LX/HiH;->f(LX/HiH;)V

    .line 2497178
    iget v1, p0, LX/HiH;->g:I

    move v1, v1

    .line 2497179
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Body Text: #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2497180
    invoke-static {p0}, LX/HiH;->f(LX/HiH;)V

    .line 2497181
    iget v1, p0, LX/HiH;->h:I

    move v1, v1

    .line 2497182
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
