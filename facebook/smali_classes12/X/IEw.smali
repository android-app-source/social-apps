.class public final LX/IEw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;)V
    .locals 0

    .prologue
    .line 2553444
    iput-object p1, p0, LX/IEw;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2553445
    iget-object v0, p0, LX/IEw;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    const/4 v5, 0x3

    .line 2553446
    iget-object v1, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2553447
    new-instance v1, LX/62T;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v5}, LX/62T;-><init>(Landroid/content/Context;I)V

    .line 2553448
    iget-object v2, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2553449
    iget-object v1, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, LX/62Y;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1ca3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v2, v3}, LX/62Y;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2553450
    new-instance v1, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->e:LX/IEt;

    invoke-direct {v1, v2, p1, v5, v3}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;ILX/IEt;)V

    .line 2553451
    iget-object v2, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->f:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2553452
    iget-object v0, p0, LX/IEw;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2553453
    iget-object v0, p0, LX/IEw;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2553454
    return-void
.end method
