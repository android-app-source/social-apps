.class public LX/Hbs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2486360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486361
    iput-object p1, p0, LX/Hbs;->a:Landroid/content/Context;

    .line 2486362
    return-void
.end method

.method public static b(LX/0QB;)LX/Hbs;
    .locals 3

    .prologue
    .line 2486363
    new-instance v1, LX/Hbs;

    const-class v0, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Hbs;-><init>(Landroid/content/Context;)V

    .line 2486364
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2486365
    iget-object v0, p0, LX/Hbs;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageItemInfo;->icon:I

    return v0
.end method
