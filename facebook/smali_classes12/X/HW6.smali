.class public LX/HW6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/9XD;

.field private final b:LX/2U1;

.field private final c:LX/8Do;

.field private final d:LX/0if;


# direct methods
.method public constructor <init>(LX/9XD;LX/2U1;LX/8Do;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475682
    iput-object p1, p0, LX/HW6;->a:LX/9XD;

    .line 2475683
    iput-object p2, p0, LX/HW6;->b:LX/2U1;

    .line 2475684
    iput-object p3, p0, LX/HW6;->c:LX/8Do;

    .line 2475685
    iput-object p4, p0, LX/HW6;->d:LX/0if;

    .line 2475686
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    .line 2475666
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2475667
    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2475668
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 2475669
    iget-object v0, p0, LX/HW6;->b:LX/2U1;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "extra_is_admin"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2475670
    :cond_0
    new-instance v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-direct {v0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;-><init>()V

    .line 2475671
    :goto_1
    const-string v5, "page_view_referrer"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2475672
    const-string v5, "page_view_referrer"

    iget-object v6, p0, LX/HW6;->a:LX/9XD;

    invoke-virtual {v6}, LX/9XD;->a()LX/89z;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2475673
    :cond_1
    const-string v5, "page_fragment_uuid"

    new-instance v6, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2475674
    const-string v5, "extra_is_landing_fragment"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475675
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475676
    iget-object v1, p0, LX/HW6;->d:LX/0if;

    sget-object v4, LX/0ig;->an:LX/0ih;

    invoke-virtual {v1, v4}, LX/0if;->a(LX/0ih;)V

    .line 2475677
    iget-object v1, p0, LX/HW6;->d:LX/0if;

    sget-object v4, LX/0ig;->an:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "page_id:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2475678
    return-object v0

    .line 2475679
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2475680
    :cond_3
    new-instance v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;-><init>()V

    goto :goto_1
.end method
