.class public final LX/J6I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field private b:Ljava/lang/String;

.field private c:Landroid/view/View;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 2649504
    iput-object p1, p0, LX/J6I;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2649505
    iput-object p2, p0, LX/J6I;->b:Ljava/lang/String;

    .line 2649506
    iput-object p3, p0, LX/J6I;->c:Landroid/view/View;

    .line 2649507
    iput-boolean p4, p0, LX/J6I;->d:Z

    .line 2649508
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 2649509
    iget-object v0, p0, LX/J6I;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a:LX/J64;

    invoke-interface {v0}, LX/J64;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2649510
    iget-object v0, p0, LX/J6I;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v1, p0, LX/J6I;->b:Ljava/lang/String;

    iget-boolean v2, p0, LX/J6I;->d:Z

    iget-object v3, p0, LX/J6I;->c:Landroid/view/View;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;ZLandroid/view/View;)V

    .line 2649511
    :goto_0
    return-void

    .line 2649512
    :cond_0
    iget-object v0, p0, LX/J6I;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    iget-object v1, p0, LX/J6I;->b:Ljava/lang/String;

    iget-boolean v2, p0, LX/J6I;->d:Z

    iget-object v3, p0, LX/J6I;->c:Landroid/view/View;

    const/4 p2, 0x0

    .line 2649513
    iget-object v4, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->d:Landroid/view/LayoutInflater;

    const p0, 0x7f031015

    const/4 p1, 0x0

    invoke-virtual {v4, p0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 2649514
    const v4, 0x7f0d17e0

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 p1, 0x8

    invoke-virtual {v4, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2649515
    const v4, 0x7f0d2698

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2649516
    const p1, 0x7f0838f0

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2649517
    const v4, 0x7f0d2699

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2649518
    const p1, 0x7f0838f1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2649519
    new-instance v4, LX/31Y;

    iget-object p1, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->f:Landroid/content/Context;

    invoke-direct {v4, p1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, p0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v4

    const p0, 0x7f080018

    new-instance p1, LX/J6E;

    invoke-direct {p1, v0, v1, v2, v3}, LX/J6E;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;ZLandroid/view/View;)V

    invoke-virtual {v4, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    .line 2649520
    goto :goto_0
.end method
