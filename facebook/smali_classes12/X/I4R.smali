.class public LX/I4R;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/events/eventcollections/view/impl/block/DateHeaderBlockView;",
        "Lcom/facebook/events/eventcollections/model/data/DateHeaderBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/I4Z;)V
    .locals 0

    .prologue
    .line 2534024
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2534025
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 3

    .prologue
    .line 2534026
    check-cast p1, LX/I4I;

    .line 2534027
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534028
    check-cast v0, LX/I4Z;

    .line 2534029
    iget-object v1, p1, LX/I4I;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2534030
    invoke-virtual {v0, v1}, LX/I4Z;->a(Ljava/lang/String;)V

    .line 2534031
    iget-object v1, p1, LX/I4I;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2534032
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2534033
    iget-object v2, v0, LX/I4Z;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 2534034
    :goto_0
    iget-object v1, p1, LX/I4I;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2534035
    iput-object v1, v0, LX/I4Z;->d:Ljava/lang/String;

    .line 2534036
    return-void

    .line 2534037
    :cond_0
    iget-object v2, v0, LX/I4Z;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 2534038
    iget-object v2, v0, LX/I4Z;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2534039
    iget-object p0, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, p0

    .line 2534040
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
