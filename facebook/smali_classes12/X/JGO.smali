.class public abstract LX/JGO;
.super LX/JGN;
.source ""


# static fields
.field public static final c:Landroid/graphics/Paint;

.field public static final d:Landroid/graphics/RectF;


# instance fields
.field private e:I

.field public f:I

.field public g:F

.field public h:F

.field private i:Landroid/graphics/Path;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2667193
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LX/JGO;->c:Landroid/graphics/Paint;

    .line 2667194
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, LX/JGO;->d:Landroid/graphics/RectF;

    .line 2667195
    sget-object v0, LX/JGO;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2667196
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2667172
    invoke-direct {p0}, LX/JGN;-><init>()V

    .line 2667173
    const/high16 v0, -0x1000000

    iput v0, p0, LX/JGO;->f:I

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 2667192
    iget v0, p0, LX/JGO;->g:F

    return v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 2667191
    iget v0, p0, LX/JGO;->e:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2667190
    iget v0, p0, LX/JGO;->f:I

    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2667188
    iget v0, p0, LX/JGO;->e:I

    or-int/2addr v0, p1

    iput v0, p0, LX/JGO;->e:I

    .line 2667189
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2667186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JGO;->c(I)V

    .line 2667187
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2667184
    iget v0, p0, LX/JGO;->e:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LX/JGO;->e:I

    .line 2667185
    return-void
.end method

.method public e()Landroid/graphics/PathEffect;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667183
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/graphics/Path;
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 2667174
    invoke-virtual {p0, v3}, LX/JGO;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2667175
    iget-object v0, p0, LX/JGO;->i:Landroid/graphics/Path;

    if-nez v0, :cond_0

    .line 2667176
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/JGO;->i:Landroid/graphics/Path;

    .line 2667177
    :cond_0
    iget-object v0, p0, LX/JGO;->i:Landroid/graphics/Path;

    iget v1, p0, LX/JGO;->g:F

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    .line 2667178
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 2667179
    sget-object v2, LX/JGO;->d:Landroid/graphics/RectF;

    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v4

    add-float/2addr v4, v1

    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v5

    add-float/2addr v5, v1

    invoke-virtual {p0}, LX/JGN;->l()F

    move-result v6

    sub-float/2addr v6, v1

    invoke-virtual {p0}, LX/JGN;->m()F

    move-result v7

    sub-float/2addr v7, v1

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 2667180
    sget-object v2, LX/JGO;->d:Landroid/graphics/RectF;

    iget v4, p0, LX/JGO;->h:F

    iget v5, p0, LX/JGO;->h:F

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 2667181
    invoke-virtual {p0, v3}, LX/JGO;->d(I)V

    .line 2667182
    :cond_1
    iget-object v0, p0, LX/JGO;->i:Landroid/graphics/Path;

    return-object v0
.end method
