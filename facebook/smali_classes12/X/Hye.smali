.class public final LX/Hye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524382
    iput-object p1, p0, LX/Hye;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2524383
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2524373
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524374
    if-nez p1, :cond_1

    .line 2524375
    :cond_0
    :goto_0
    return-void

    .line 2524376
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524377
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2524378
    iget-object v1, p0, LX/Hye;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2524379
    iget-object v1, p0, LX/Hye;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->e:LX/Hyx;

    .line 2524380
    iget-object v2, v1, LX/Hyx;->n:LX/0TD;

    new-instance v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$1;

    invoke-direct {v3, v1, v0}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardPager$1;-><init>(LX/Hyx;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)V

    const p1, -0x4170fe7d

    invoke-static {v2, v3, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2524381
    iget-object v1, p0, LX/Hye;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Hz2;->a(Lcom/facebook/events/model/Event;)V

    goto :goto_0
.end method
