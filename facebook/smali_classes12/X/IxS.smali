.class public LX/IxS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IxQ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IxT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2631695
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IxS;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IxT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631696
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2631697
    iput-object p1, p0, LX/IxS;->b:LX/0Ot;

    .line 2631698
    return-void
.end method

.method public static a(LX/0QB;)LX/IxS;
    .locals 4

    .prologue
    .line 2631699
    const-class v1, LX/IxS;

    monitor-enter v1

    .line 2631700
    :try_start_0
    sget-object v0, LX/IxS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2631701
    sput-object v2, LX/IxS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2631702
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631703
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2631704
    new-instance v3, LX/IxS;

    const/16 p0, 0x2c6b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IxS;-><init>(LX/0Ot;)V

    .line 2631705
    move-object v0, v3

    .line 2631706
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2631707
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IxS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631708
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2631709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2631710
    check-cast p2, LX/IxR;

    .line 2631711
    iget-object v0, p0, LX/IxS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IxT;

    iget-boolean v2, p2, LX/IxR;->a:Z

    iget v3, p2, LX/IxR;->b:I

    iget v4, p2, LX/IxR;->c:I

    iget-object v5, p2, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    move-object v1, p1

    const/4 v6, 0x0

    .line 2631712
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0060

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 2631713
    mul-int v8, v3, v7

    div-int/2addr v8, v4

    sub-int v8, v7, v8

    .line 2631714
    add-int/lit8 v9, v3, 0x1

    mul-int/2addr v9, v7

    div-int/2addr v9, v4

    .line 2631715
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    iget-object p1, v0, LX/IxT;->a:LX/Ixc;

    const/4 p2, 0x0

    .line 2631716
    new-instance v3, LX/Ixb;

    invoke-direct {v3, p1}, LX/Ixb;-><init>(LX/Ixc;)V

    .line 2631717
    sget-object v4, LX/Ixc;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ixa;

    .line 2631718
    if-nez v4, :cond_0

    .line 2631719
    new-instance v4, LX/Ixa;

    invoke-direct {v4}, LX/Ixa;-><init>()V

    .line 2631720
    :cond_0
    invoke-static {v4, v1, p2, p2, v3}, LX/Ixa;->a$redex0(LX/Ixa;LX/1De;IILX/Ixb;)V

    .line 2631721
    move-object v3, v4

    .line 2631722
    move-object p2, v3

    .line 2631723
    move-object p1, p2

    .line 2631724
    iget-object p2, p1, LX/Ixa;->a:LX/Ixb;

    iput-object v5, p2, LX/Ixb;->a:LX/IxU;

    .line 2631725
    iget-object p2, p1, LX/Ixa;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2631726
    move-object p1, p1

    .line 2631727
    invoke-interface {p0, p1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p0

    iget-object p1, v0, LX/IxT;->b:LX/IxH;

    const/4 p2, 0x0

    .line 2631728
    new-instance v0, LX/IxG;

    invoke-direct {v0, p1}, LX/IxG;-><init>(LX/IxH;)V

    .line 2631729
    sget-object v3, LX/IxH;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IxF;

    .line 2631730
    if-nez v3, :cond_1

    .line 2631731
    new-instance v3, LX/IxF;

    invoke-direct {v3}, LX/IxF;-><init>()V

    .line 2631732
    :cond_1
    invoke-static {v3, v1, p2, p2, v0}, LX/IxF;->a$redex0(LX/IxF;LX/1De;IILX/IxG;)V

    .line 2631733
    move-object v0, v3

    .line 2631734
    move-object p2, v0

    .line 2631735
    move-object p1, p2

    .line 2631736
    iget-object p2, p1, LX/IxF;->a:LX/IxG;

    iput-object v5, p2, LX/IxG;->a:LX/IxK;

    .line 2631737
    iget-object p2, p1, LX/IxF;->d:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 2631738
    move-object p1, p1

    .line 2631739
    invoke-interface {p0, p1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v6, v8}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x2

    invoke-interface {v8, p0, v9}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x1

    if-eqz v2, :cond_2

    move v6, v7

    :cond_2
    invoke-interface {v8, v9, v6}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x3

    invoke-interface {v6, v8, v7}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2631740
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2631741
    invoke-static {}, LX/1dS;->b()V

    .line 2631742
    const/4 v0, 0x0

    return-object v0
.end method
