.class public final LX/HZq;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationSuccessFragment;JJ)V
    .locals 0

    .prologue
    .line 2482640
    iput-object p1, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2482641
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2482642
    iget-object v1, v0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    move-object v0, v1

    .line 2482643
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2482644
    iget-object v1, v0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    move-object v0, v1

    .line 2482645
    iget-object v0, v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2482646
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2482647
    iget-object v1, v0, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    move-object v0, v1

    .line 2482648
    iget-object v0, v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2482649
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2482650
    const-string v2, "extra_uid"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2482651
    const-string v0, "extra_pwd"

    iget-object v2, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v2, v2, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2482652
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    .line 2482653
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->e:LX/0Uh;

    const/16 v3, 0x38

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 2482654
    if-eqz v0, :cond_2

    .line 2482655
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/registration/activity/RegistrationLoginActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2482656
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2482657
    iget-object v1, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v1, v1, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2482658
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2482659
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    .line 2482660
    :goto_1
    return-void

    .line 2482661
    :cond_0
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v0

    sget-object v1, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2482662
    :cond_2
    iget-object v0, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationSuccessFragment;->c:LX/GvB;

    iget-object v2, p0, LX/HZq;->a:Lcom/facebook/registration/fragment/RegistrationSuccessFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/GvB;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final onTick(J)V
    .locals 0

    .prologue
    .line 2482663
    return-void
.end method
