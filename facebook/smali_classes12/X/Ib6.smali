.class public final enum LX/Ib6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ib6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ib6;

.field public static final enum DESTINATION:LX/Ib6;

.field public static final enum ORIGIN:LX/Ib6;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2591766
    new-instance v0, LX/Ib6;

    const-string v1, "ORIGIN"

    invoke-direct {v0, v1, v2}, LX/Ib6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ib6;->ORIGIN:LX/Ib6;

    .line 2591767
    new-instance v0, LX/Ib6;

    const-string v1, "DESTINATION"

    invoke-direct {v0, v1, v3}, LX/Ib6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ib6;->DESTINATION:LX/Ib6;

    .line 2591768
    const/4 v0, 0x2

    new-array v0, v0, [LX/Ib6;

    sget-object v1, LX/Ib6;->ORIGIN:LX/Ib6;

    aput-object v1, v0, v2

    sget-object v1, LX/Ib6;->DESTINATION:LX/Ib6;

    aput-object v1, v0, v3

    sput-object v0, LX/Ib6;->$VALUES:[LX/Ib6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2591763
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ib6;
    .locals 1

    .prologue
    .line 2591765
    const-class v0, LX/Ib6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ib6;

    return-object v0
.end method

.method public static values()[LX/Ib6;
    .locals 1

    .prologue
    .line 2591764
    sget-object v0, LX/Ib6;->$VALUES:[LX/Ib6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ib6;

    return-object v0
.end method
