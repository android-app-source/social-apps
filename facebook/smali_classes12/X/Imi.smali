.class public LX/Imi;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/IzG;

.field private final c:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final d:LX/2Og;

.field private final e:LX/03V;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2Oi;

.field private final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentPlatformContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2610123
    const-class v0, LX/Imi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Imi;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/IzG;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/2Og;LX/03V;LX/0Or;LX/2Oi;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IzG;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "LX/2Og;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/2Oi;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610125
    new-instance v0, LX/Imh;

    invoke-direct {v0, p0}, LX/Imh;-><init>(LX/Imi;)V

    iput-object v0, p0, LX/Imi;->h:Ljava/util/Comparator;

    .line 2610126
    iput-object p1, p0, LX/Imi;->b:LX/IzG;

    .line 2610127
    iput-object p2, p0, LX/Imi;->c:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2610128
    iput-object p3, p0, LX/Imi;->d:LX/2Og;

    .line 2610129
    iput-object p4, p0, LX/Imi;->e:LX/03V;

    .line 2610130
    iput-object p5, p0, LX/Imi;->f:LX/0Or;

    .line 2610131
    iput-object p6, p0, LX/Imi;->g:LX/2Oi;

    .line 2610132
    return-void
.end method
