.class public LX/IPM;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public final b:LX/1Ck;

.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:LX/0kb;

.field public final f:LX/1My;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;",
            ">;"
        }
    .end annotation
.end field

.field private final h:I

.field public i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:LX/IPE;

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:LX/IOK;

.field public n:J

.field public o:I

.field public p:Landroid/content/res/Resources;

.field public q:F

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2574088
    const-class v0, LX/IPM;

    sput-object v0, LX/IPM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/0kb;LX/1My;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574078
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IPM;->g:Ljava/util/List;

    .line 2574079
    const/4 v0, 0x3

    iput v0, p0, LX/IPM;->h:I

    .line 2574080
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/IPM;->n:J

    .line 2574081
    const/4 v0, 0x0

    iput v0, p0, LX/IPM;->o:I

    .line 2574082
    iput-object p1, p0, LX/IPM;->b:LX/1Ck;

    .line 2574083
    iput-object p2, p0, LX/IPM;->c:LX/0tX;

    .line 2574084
    iput-object p3, p0, LX/IPM;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2574085
    iput-object p4, p0, LX/IPM;->e:LX/0kb;

    .line 2574086
    iput-object p5, p0, LX/IPM;->f:LX/1My;

    .line 2574087
    return-void
.end method

.method public static a$redex0(LX/IPM;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2574059
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 2574060
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 2574061
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2574062
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 2574063
    instance-of v1, v0, Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v1, 0x198f03

    if-ne v0, v1, :cond_0

    .line 2574064
    iget-object v0, p0, LX/IPM;->b:LX/1Ck;

    sget-object v1, LX/IPL;->FETCH_GROUPS:LX/IPL;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2574065
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IPM;->l:Z

    .line 2574066
    const/4 v0, 0x0

    iput-object v0, p0, LX/IPM;->k:Ljava/lang/String;

    .line 2574067
    invoke-static {p0}, LX/IPM;->k(LX/IPM;)V

    .line 2574068
    :cond_0
    const/4 v3, 0x1

    .line 2574069
    iget v1, p0, LX/IPM;->o:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    iget-object v1, p0, LX/IPM;->e:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2574070
    iget-object v1, p0, LX/IPM;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverPagedListLoader$4;

    invoke-direct {v2, p0}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverPagedListLoader$4;-><init>(LX/IPM;)V

    iget-wide v3, p0, LX/IPM;->n:J

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2574071
    iget v1, p0, LX/IPM;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/IPM;->o:I

    .line 2574072
    iget-wide v1, p0, LX/IPM;->n:J

    const-wide/16 v3, 0x2

    mul-long/2addr v1, v3

    iput-wide v1, p0, LX/IPM;->n:J

    .line 2574073
    :goto_0
    return-void

    .line 2574074
    :cond_1
    iput-boolean v3, p0, LX/IPM;->l:Z

    .line 2574075
    iput-boolean v3, p0, LX/IPM;->r:Z

    .line 2574076
    invoke-static {p0}, LX/IPM;->h(LX/IPM;)V

    goto :goto_0
.end method

.method public static b(LX/IPM;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2574049
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2574050
    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2574051
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2574052
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/IPM;->k:Ljava/lang/String;

    .line 2574053
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/IPM;->l:Z

    .line 2574054
    invoke-static {p0, p1}, LX/IPM;->c(LX/IPM;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2574055
    invoke-static {p0}, LX/IPM;->h(LX/IPM;)V

    .line 2574056
    return-void

    .line 2574057
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2574058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/IPM;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2574036
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2574037
    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel$GroupsYouShouldJoinModel;->j()LX/0Px;

    move-result-object v2

    .line 2574038
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2574039
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 2574040
    :cond_0
    :goto_1
    iget-object v4, p0, LX/IPM;->g:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2574041
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2574042
    :cond_1
    return-void

    .line 2574043
    :cond_2
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 2574044
    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v11, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2574045
    new-instance v6, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2574046
    iget-object v5, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v8, v5

    .line 2574047
    iget-wide v9, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-object v7, v0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2574048
    iget-object v5, p0, LX/IPM;->f:LX/1My;

    iget-object v7, p0, LX/IPM;->i:LX/0TF;

    invoke-virtual {v0}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1
.end method

.method public static h(LX/IPM;)V
    .locals 4

    .prologue
    .line 2574009
    iget-object v0, p0, LX/IPM;->j:LX/IPE;

    if-eqz v0, :cond_3

    .line 2574010
    iget-object v0, p0, LX/IPM;->j:LX/IPE;

    iget-object v1, p0, LX/IPM;->g:Ljava/util/List;

    .line 2574011
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    .line 2574012
    iget-boolean v3, v2, LX/IPM;->l:Z

    move v2, v3

    .line 2574013
    if-eqz v2, :cond_0

    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->a:LX/IPM;

    .line 2574014
    iget-boolean v3, v2, LX/IPM;->r:Z

    move v2, v3

    .line 2574015
    if-nez v2, :cond_0

    .line 2574016
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/IPA;->a(Z)V

    .line 2574017
    :cond_0
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    .line 2574018
    iput-object v1, v2, LX/IPA;->d:Ljava/util/List;

    .line 2574019
    const v3, -0x28e10cca

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2574020
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    invoke-virtual {v2}, LX/IPA;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2574021
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    .line 2574022
    iget-object v3, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v3, :cond_1

    .line 2574023
    iget-object v3, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->g:Lcom/facebook/widget/text/BetterTextView;

    const/16 p0, 0x8

    invoke-virtual {v3, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2574024
    :cond_1
    iget-object v3, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2574025
    const v3, 0x7f0d1080

    invoke-virtual {v2, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2574026
    :cond_2
    :goto_0
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->b:LX/IPA;

    iget-object v3, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->c:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    iget-object p0, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object p0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->j:LX/1DI;

    .line 2574027
    iput-object p0, v2, LX/IPA;->a:LX/1DI;

    .line 2574028
    iget-boolean v0, v2, LX/IPA;->b:Z

    if-eq v3, v0, :cond_3

    .line 2574029
    iput-boolean v3, v2, LX/IPA;->b:Z

    .line 2574030
    const v0, -0x2e3b9c62

    invoke-static {v2, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2574031
    :cond_3
    return-void

    .line 2574032
    :cond_4
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2574033
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    invoke-static {v2}, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->d(Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;)V

    goto :goto_0

    .line 2574034
    :cond_5
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2574035
    iget-object v2, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v3, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p0, 0x7f08006f

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object p0, v0, LX/IPE;->a:Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;

    iget-object p0, p0, Lcom/facebook/groups/fb4a/discover/FB4AGroupsDiscoverFragment;->j:LX/1DI;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0
.end method

.method public static k(LX/IPM;)V
    .locals 2

    .prologue
    .line 2573999
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, LX/IPM;->n:J

    .line 2574000
    const/4 v0, 0x0

    iput v0, p0, LX/IPM;->o:I

    .line 2574001
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 4

    .prologue
    .line 2574002
    sget-object v0, LX/0zS;->c:LX/0zS;

    .line 2574003
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/IPM;->r:Z

    .line 2574004
    iget-object v1, p0, LX/IPM;->b:LX/1Ck;

    sget-object v2, LX/IPL;->FETCH_GROUPS:LX/IPL;

    .line 2574005
    new-instance v3, LX/IPK;

    invoke-direct {v3, p0, v0}, LX/IPK;-><init>(LX/IPM;LX/0zS;)V

    .line 2574006
    move-object v0, v3

    .line 2574007
    new-instance v3, LX/IPI;

    invoke-direct {v3, p0}, LX/IPI;-><init>(LX/IPM;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2574008
    return-void
.end method
