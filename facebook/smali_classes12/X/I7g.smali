.class public final LX/I7g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I7i;


# direct methods
.method public constructor <init>(LX/I7i;)V
    .locals 0

    .prologue
    .line 2540031
    iput-object p1, p0, LX/I7g;->a:LX/I7i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2540032
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/4 v4, 0x0

    .line 2540033
    iget-object v0, p0, LX/I7g;->a:LX/I7i;

    iget-object v0, v0, LX/I7i;->c:LX/1Kf;

    iget-object v1, p0, LX/I7g;->a:LX/I7i;

    iget-object v1, v1, LX/I7i;->b:LX/IBH;

    iget-object v2, p0, LX/I7g;->a:LX/I7i;

    iget-object v2, v2, LX/I7i;->k:Lcom/facebook/events/model/Event;

    const/4 v10, 0x1

    .line 2540034
    iget-object v5, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2540035
    const v6, 0x403827a

    invoke-static {v5, v6}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    .line 2540036
    iget-object v5, v2, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v5, v5

    .line 2540037
    new-instance v7, LX/39x;

    invoke-direct {v7}, LX/39x;-><init>()V

    .line 2540038
    iget-object v8, v2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v8, v8

    .line 2540039
    iput-object v8, v7, LX/39x;->t:Ljava/lang/String;

    .line 2540040
    move-object v7, v7

    .line 2540041
    new-instance v8, LX/4XB;

    invoke-direct {v8}, LX/4XB;-><init>()V

    new-instance v9, LX/2dc;

    invoke-direct {v9}, LX/2dc;-><init>()V

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2540042
    :goto_0
    iput-object v5, v9, LX/2dc;->h:Ljava/lang/String;

    .line 2540043
    move-object v5, v9

    .line 2540044
    invoke-virtual {v5}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2540045
    iput-object v5, v8, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2540046
    move-object v5, v8

    .line 2540047
    invoke-virtual {v5}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 2540048
    iput-object v5, v7, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2540049
    move-object v5, v7

    .line 2540050
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 2540051
    sget-object v7, LX/21D;->EVENT:LX/21D;

    const-string v8, "shareEvent"

    invoke-static {v6}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v6

    .line 2540052
    iput-object v5, v6, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2540053
    move-object v5, v6

    .line 2540054
    invoke-virtual {v5}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    invoke-static {v7, v8, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v5

    .line 2540055
    iget-object v6, v1, LX/IBH;->d:LX/01T;

    sget-object v7, LX/01T;->PAA:LX/01T;

    if-ne v6, v7, :cond_0

    .line 2540056
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2540057
    invoke-virtual {v5, v10}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    new-instance v7, LX/89I;

    .line 2540058
    iget-object v8, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2540059
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    sget-object v8, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v7, v9, v10, v8}, LX/89I;-><init>(JLX/2rw;)V

    .line 2540060
    iget-object v8, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v8, v8

    .line 2540061
    iput-object v8, v7, LX/89I;->c:Ljava/lang/String;

    .line 2540062
    move-object v7, v7

    .line 2540063
    iget-object v8, v2, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v8, v8

    .line 2540064
    iput-object v8, v7, LX/89I;->d:Ljava/lang/String;

    .line 2540065
    move-object v7, v7

    .line 2540066
    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-static {v2, p1}, LX/IBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2540067
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    move-object v1, v5

    .line 2540068
    const/16 v2, 0x1f7

    iget-object v3, p0, LX/I7g;->a:LX/I7i;

    iget-object v3, v3, LX/I7i;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v4, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2540069
    return-object v4

    .line 2540070
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
