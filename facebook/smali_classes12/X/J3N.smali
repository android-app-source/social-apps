.class public final LX/J3N;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)V
    .locals 0

    .prologue
    .line 2642278
    iput-object p1, p0, LX/J3N;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 6

    .prologue
    .line 2642279
    iget-object v0, p0, LX/J3N;->a:Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;

    .line 2642280
    new-instance v1, LX/0cA;

    invoke-direct {v1}, LX/0cA;-><init>()V

    .line 2642281
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    if-eqz v2, :cond_0

    .line 2642282
    sget-object v2, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642283
    :cond_0
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    if-eqz v2, :cond_1

    .line 2642284
    sget-object v2, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2642285
    :cond_1
    invoke-static {}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->newBuilder()LX/6tR;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v3, v3, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    .line 2642286
    iput-object v3, v2, LX/6tR;->c:Ljava/lang/String;

    .line 2642287
    move-object v2, v2

    .line 2642288
    iget-object v3, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v3, v3, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    .line 2642289
    iput-object v3, v2, LX/6tR;->d:Ljava/lang/String;

    .line 2642290
    move-object v2, v2

    .line 2642291
    sget-object v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->a:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    iget-object v3, v3, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->d:Landroid/net/Uri;

    .line 2642292
    iput-object v3, v2, LX/6tR;->b:Landroid/net/Uri;

    .line 2642293
    move-object v2, v2

    .line 2642294
    invoke-virtual {v2}, LX/6tR;->a()Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    move-result-object v2

    .line 2642295
    sget-object v3, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v3}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v3

    invoke-virtual {v3}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v3

    invoke-virtual {v3}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v3

    .line 2642296
    new-instance v4, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    new-instance v5, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    iget-object p1, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p1, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    iget-object p2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p2, p2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    invoke-direct {v5, p0, p1, p2}, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    invoke-direct {v4, v5, p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;-><init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V

    .line 2642297
    iget-object v5, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v5, v5, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    invoke-static {v5}, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a(Ljava/lang/String;)LX/6rU;

    move-result-object v5

    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    .line 2642298
    iput-object p0, v5, LX/6rU;->b:Ljava/lang/String;

    .line 2642299
    move-object v5, v5

    .line 2642300
    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    .line 2642301
    iput-object p0, v5, LX/6rU;->c:Ljava/lang/String;

    .line 2642302
    move-object v5, v5

    .line 2642303
    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    .line 2642304
    iput-object p0, v5, LX/6rU;->d:Ljava/lang/String;

    .line 2642305
    move-object v5, v5

    .line 2642306
    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p0, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    .line 2642307
    iput-object p0, v5, LX/6rU;->e:Ljava/lang/String;

    .line 2642308
    move-object v5, v5

    .line 2642309
    invoke-virtual {v5}, LX/6rU;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    move-result-object v5

    .line 2642310
    sget-object p0, LX/6qw;->PAYMENTS_FLOW_SAMPLE:LX/6qw;

    sget-object p1, LX/6xg;->MOR_NONE:LX/6xg;

    iget-object p2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-virtual {p2}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a()LX/0Rf;

    move-result-object p2

    invoke-static {p0, p1, p2, v3}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v3

    iget-object p0, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    .line 2642311
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    .line 2642312
    iget-boolean p2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    if-eqz p2, :cond_5

    .line 2642313
    sget-object p2, LX/6so;->ENTITY:LX/6so;

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642314
    :cond_2
    :goto_0
    iget p2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    if-eqz p2, :cond_3

    .line 2642315
    sget-object p2, LX/6so;->EXPANDING_ELLIPSIZING_TEXT:LX/6so;

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642316
    :cond_3
    iget-boolean p2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    if-eqz p2, :cond_4

    .line 2642317
    sget-object p2, LX/6so;->PRICE_TABLE:LX/6so;

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642318
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a()LX/0Rf;

    move-result-object p2

    invoke-static {p1, p2}, LX/6tK;->a(LX/0Pz;LX/0Rf;)V

    .line 2642319
    sget-object p2, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2642320
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    move-object p0, p1

    .line 2642321
    iput-object p0, v3, LX/6qZ;->y:LX/0Px;

    .line 2642322
    move-object v3, v3

    .line 2642323
    iput-object v4, v3, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 2642324
    move-object v3, v3

    .line 2642325
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 2642326
    iput-object v1, v3, LX/6qZ;->t:LX/0Rf;

    .line 2642327
    move-object v1, v3

    .line 2642328
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2642329
    iput-object v3, v1, LX/6qZ;->q:Ljava/lang/String;

    .line 2642330
    move-object v1, v1

    .line 2642331
    iput-object v2, v1, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 2642332
    move-object v1, v1

    .line 2642333
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    .line 2642334
    iput-boolean v2, v1, LX/6qZ;->o:Z

    .line 2642335
    move-object v1, v1

    .line 2642336
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    .line 2642337
    iput-boolean v2, v1, LX/6qZ;->n:Z

    .line 2642338
    move-object v1, v1

    .line 2642339
    invoke-static {v0}, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->k(Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;)LX/0Px;

    move-result-object v2

    .line 2642340
    iput-object v2, v1, LX/6qZ;->h:LX/0Px;

    .line 2642341
    move-object v1, v1

    .line 2642342
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2642343
    iput-object v2, v1, LX/6qZ;->i:LX/0Px;

    .line 2642344
    move-object v1, v1

    .line 2642345
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2642346
    iput-object v2, v1, LX/6qZ;->z:Ljava/lang/String;

    .line 2642347
    move-object v1, v1

    .line 2642348
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2642349
    iput-object v2, v1, LX/6qZ;->r:Ljava/lang/String;

    .line 2642350
    move-object v1, v1

    .line 2642351
    invoke-virtual {v1}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    .line 2642352
    new-instance v2, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;

    iget-object v3, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->n:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-direct {v2, v3, v1}, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutParams;-><init>(Lcom/facebook/payments/sample/PaymentsFlowSampleData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 2642353
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v1

    .line 2642354
    iget-object v2, v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2642355
    return-void

    .line 2642356
    :cond_5
    iget-boolean p2, p0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    if-eqz p2, :cond_2

    .line 2642357
    sget-object p2, LX/6so;->PURCHASE_REVIEW_CELL:LX/6so;

    invoke-virtual {p1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0
.end method
