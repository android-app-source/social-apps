.class public final LX/HO3;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 0

    .prologue
    .line 2459169
    iput-object p1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 11

    .prologue
    .line 2459170
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v6

    .line 2459171
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v2, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->isShown()Z

    move-result v2

    invoke-static {v2, v6}, LX/CYR;->a(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CY3;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459172
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0, v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2459173
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->a(Ljava/lang/String;)V

    .line 2459174
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    const-string v2, "tap_create_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2459175
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2459176
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v3, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    iget-object v4, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    iget-object v5, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v6, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2459177
    new-instance v7, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-direct {v7}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;-><init>()V

    .line 2459178
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2459179
    const-string p1, "arg_page_id"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459180
    const-string p1, "arg_config_action_data"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459181
    const-string p1, "arg_setup_info"

    invoke-static {p0, p1, v4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459182
    const-string p1, "arg_admin_config"

    invoke-static {p0, p1, v5}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459183
    const-string p1, "arg_page_admin_cta"

    invoke-static {p0, p1, v6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459184
    invoke-virtual {v7, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459185
    move-object v2, v7

    .line 2459186
    invoke-static {v0, v1, v2}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459187
    :goto_0
    return-void

    .line 2459188
    :cond_0
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2459189
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->y(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    goto :goto_0

    .line 2459190
    :cond_1
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459191
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->isShown()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a()LX/CY5;

    move-result-object v1

    :goto_1
    move-object v0, v1

    .line 2459192
    sget-object v1, LX/CY5;->NONE:LX/CY5;

    if-eq v0, v1, :cond_2

    .line 2459193
    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v2, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/CY3;->a(Ljava/lang/String;LX/CY5;)V

    goto :goto_0

    .line 2459194
    :cond_2
    iget-object v7, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->n:LX/HNd;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v2, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    iget-object v3, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v5, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual/range {v0 .. v5}, LX/HNd;->a(Ljava/lang/String;LX/CYE;Ljava/util/Map;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)LX/HNc;

    move-result-object v0

    .line 2459195
    iput-object v0, v7, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->G:LX/HNc;

    .line 2459196
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459197
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459198
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2459199
    invoke-virtual {v0, v1}, LX/CYR;->a(Landroid/view/View;)V

    .line 2459200
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v0, :cond_6

    .line 2459201
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->ak:LX/0ih;

    const-string v2, "tap_save_cta_button"

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459202
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459203
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->G:LX/HNc;

    new-instance v2, LX/HO0;

    invoke-direct {v2, v0}, LX/HO0;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459204
    iput-object v2, v1, LX/HNc;->e:LX/HO0;

    .line 2459205
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->G:LX/HNc;

    .line 2459206
    iget-object v2, v1, LX/HNc;->a:LX/1Ck;

    const-string v3, "edit_cta_mutation"

    iget-object v4, v1, LX/HNc;->b:LX/HNV;

    iget-object v5, v1, LX/HNc;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v4

    iget-object v5, v1, LX/HNc;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iget-object v6, v1, LX/HNc;->g:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v7, v1, LX/HNc;->j:Ljava/util/Map;

    .line 2459207
    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v10, v8

    .line 2459208
    :goto_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 2459209
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 2459210
    new-instance v0, LX/4Hi;

    invoke-direct {v0}, LX/4Hi;-><init>()V

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/4Hi;->a(Ljava/lang/String;)LX/4Hi;

    move-result-object v9

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v9, v8}, LX/4Hi;->b(Ljava/lang/String;)LX/4Hi;

    move-result-object v8

    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2459211
    :cond_3
    const-string v8, "NONE"

    move-object v10, v8

    goto :goto_2

    .line 2459212
    :cond_4
    new-instance v8, LX/4Hj;

    invoke-direct {v8}, LX/4Hj;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->n()Ljava/lang/String;

    move-result-object v9

    .line 2459213
    const-string p2, "id"

    invoke-virtual {v8, p2, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459214
    move-object v8, v8

    .line 2459215
    const-string v9, "MOBILE_PAGE_PRESENCE_CALL_TO_ACTION"

    .line 2459216
    const-string p2, "source"

    invoke-virtual {v8, p2, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459217
    move-object v8, v8

    .line 2459218
    const-string v9, "cta_type"

    invoke-virtual {v8, v9, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459219
    move-object v8, v8

    .line 2459220
    const-string v9, "fields_data"

    invoke-virtual {v8, v9, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2459221
    move-object v8, v8

    .line 2459222
    new-instance v9, LX/8FK;

    invoke-direct {v9}, LX/8FK;-><init>()V

    move-object v9, v9

    .line 2459223
    const-string v10, "input"

    invoke-virtual {v9, v10, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v8

    check-cast v8, LX/8FK;

    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    .line 2459224
    iget-object v9, v4, LX/HNU;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-static {v8}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v4, v8

    .line 2459225
    new-instance v5, LX/HNa;

    invoke-direct {v5, v1}, LX/HNa;-><init>(LX/HNc;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2459226
    :goto_4
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v1, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v2, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b()Ljava/util/Map;

    move-result-object v2

    .line 2459227
    sget-object v3, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_SAVE:LX/CY4;

    invoke-static {v3, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2459228
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2459229
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_5

    .line 2459230
    :cond_5
    iget-object v3, v0, LX/CY3;->a:LX/0Zb;

    invoke-interface {v3, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2459231
    goto/16 :goto_0

    .line 2459232
    :cond_6
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->aj:LX/0ih;

    const-string v2, "tap_save_cta_button"

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459233
    iget-object v0, p0, LX/HO3;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    .line 2459234
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->G:LX/HNc;

    new-instance v2, LX/HNn;

    invoke-direct {v2, v0}, LX/HNn;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459235
    iput-object v2, v1, LX/HNc;->d:LX/HNb;

    .line 2459236
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->G:LX/HNc;

    invoke-virtual {v1}, LX/HNc;->a()V

    .line 2459237
    goto :goto_4

    :cond_7
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a()LX/CY5;

    move-result-object v1

    goto/16 :goto_1
.end method
