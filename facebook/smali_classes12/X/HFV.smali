.class public final LX/HFV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443648
    iput-object p1, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x36bd4cbc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2443649
    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const v2, 0x7f0d22d4

    .line 2443650
    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 2443651
    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443652
    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2443653
    iget-object v3, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443654
    iget-object v3, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2443655
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443656
    iget-object v3, v0, LX/HFJ;->e:Ljava/lang/String;

    move-object v0, v3

    .line 2443657
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2443658
    :cond_0
    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "next"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/HFf;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443659
    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b()V

    .line 2443660
    :goto_0
    const v0, 0xb77dae9

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2443661
    :cond_1
    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443662
    iget-object v0, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, p0, LX/HFV;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443663
    iget-object v4, v3, LX/HFJ;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2443664
    iget-object v4, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string v5, "save_website_gql_task_key"

    iget-object v6, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b:LX/HFc;

    .line 2443665
    new-instance v7, LX/4I4;

    invoke-direct {v7}, LX/4I4;-><init>()V

    .line 2443666
    const-string p0, "pageid"

    invoke-virtual {v7, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443667
    move-object v7, v7

    .line 2443668
    const-string p0, "website"

    invoke-virtual {v7, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443669
    move-object v7, v7

    .line 2443670
    new-instance p0, LX/HGH;

    invoke-direct {p0}, LX/HGH;-><init>()V

    move-object p0, p0

    .line 2443671
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/HGH;

    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2443672
    iget-object p0, v6, LX/HFc;->a:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v6, v7

    .line 2443673
    new-instance v7, LX/HFP;

    invoke-direct {v7, v0}, LX/HFP;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443674
    goto :goto_0
.end method
