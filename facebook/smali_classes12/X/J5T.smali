.class public final LX/J5T;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2648079
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2648080
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2648081
    :goto_0
    return v1

    .line 2648082
    :cond_0
    const-string v7, "is_eligible"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2648083
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2648084
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_4

    .line 2648085
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2648086
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2648087
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2648088
    const-string v7, "actions"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2648089
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2648090
    :cond_2
    const-string v7, "checkup_items"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2648091
    invoke-static {p0, p1}, LX/J5S;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2648092
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2648093
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2648094
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2648095
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2648096
    if-eqz v0, :cond_5

    .line 2648097
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2648098
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2648099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2648100
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2648101
    if-eqz v0, :cond_0

    .line 2648102
    const-string v0, "actions"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2648103
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2648104
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2648105
    if-eqz v0, :cond_1

    .line 2648106
    const-string v1, "checkup_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2648107
    invoke-static {p0, v0, p2, p3}, LX/J5S;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2648108
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2648109
    if-eqz v0, :cond_2

    .line 2648110
    const-string v1, "is_eligible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2648111
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2648112
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2648113
    return-void
.end method
