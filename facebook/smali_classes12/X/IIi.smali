.class public final LX/IIi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/62K;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2561953
    iput-object p1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iput-object p2, p0, LX/IIi;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2561939
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-boolean v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ag:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ah:I

    iget-object v2, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 2561940
    :cond_0
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    if-eqz v1, :cond_1

    .line 2561941
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v2, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aJ:LX/IJV;

    invoke-virtual {v1, v2}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(LX/IJV;)V

    .line 2561942
    :cond_1
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2561943
    iput-boolean v0, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ag:Z

    .line 2561944
    :cond_2
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v2

    .line 2561945
    iput v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ah:I

    .line 2561946
    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/SplitHideableListView;->getChildCount()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_3

    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->isAtBottom()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2561947
    iget-object v0, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/SplitHideableListView;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    iget-object v2, p0, LX/IIi;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    invoke-virtual {v2}, Lcom/facebook/widget/listview/SplitHideableListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/SplitHideableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2561948
    :cond_3
    iget-object v1, p0, LX/IIi;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2561949
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v0, v2, :cond_4

    .line 2561950
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2561951
    iget-object v0, p0, LX/IIi;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2561952
    :cond_4
    return-void
.end method
