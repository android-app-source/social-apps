.class public final LX/JOH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JOK;

.field public final synthetic b:Lcom/facebook/greetingcards/model/GreetingCard;

.field public final synthetic c:LX/JOL;

.field public final synthetic d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;LX/JOK;Lcom/facebook/greetingcards/model/GreetingCard;LX/JOL;)V
    .locals 0

    .prologue
    .line 2687203
    iput-object p1, p0, LX/JOH;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iput-object p2, p0, LX/JOH;->a:LX/JOK;

    iput-object p3, p0, LX/JOH;->b:Lcom/facebook/greetingcards/model/GreetingCard;

    iput-object p4, p0, LX/JOH;->c:LX/JOL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x21943e6d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2687204
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2687205
    iget-object v0, p0, LX/JOH;->a:LX/JOK;

    iget-object v0, v0, LX/JOK;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JOH;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iget-object v1, p0, LX/JOH;->b:Lcom/facebook/greetingcards/model/GreetingCard;

    const-string v2, "feed"

    invoke-static {v0, v5, v1, v2}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;Landroid/content/Context;Lcom/facebook/greetingcards/model/GreetingCard;Ljava/lang/String;)Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v4

    .line 2687206
    :goto_0
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->a:LX/Gk2;

    if-eqz v0, :cond_0

    .line 2687207
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->a:LX/Gk2;

    .line 2687208
    iput-object v0, v4, Lcom/facebook/greetingcards/render/RenderCardFragment;->o:LX/Gk2;

    .line 2687209
    :cond_0
    invoke-static {v5}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v6

    .line 2687210
    const v0, 0x7f0d14e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2687211
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 2687212
    if-eqz v2, :cond_1

    .line 2687213
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    new-instance v1, LX/Gjt;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-direct {v1, v7}, LX/Gjt;-><init>(Landroid/view/Window;)V

    iput-object v1, v0, LX/JOL;->f:LX/Gjt;

    .line 2687214
    :cond_1
    if-nez v3, :cond_6

    .line 2687215
    if-nez v2, :cond_4

    .line 2687216
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a:Ljava/lang/Class;

    const-string v1, "couldn\'t find activity"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2687217
    const v0, -0x4d55c617

    invoke-static {v0, v8}, LX/02F;->a(II)V

    .line 2687218
    :goto_1
    return-void

    .line 2687219
    :cond_2
    iget-object v0, p0, LX/JOH;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iget-object v1, p0, LX/JOH;->c:LX/JOL;

    iget-object v2, p0, LX/JOH;->b:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object v3, p0, LX/JOH;->a:LX/JOK;

    iget-object v3, v3, LX/JOK;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2687220
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2687221
    new-instance v6, LX/Gjq;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, LX/Gjq;-><init>(I)V

    .line 2687222
    if-eqz v2, :cond_3

    .line 2687223
    invoke-virtual {v6, v2}, LX/GjU;->a(Lcom/facebook/greetingcards/model/GreetingCard;)LX/GjU;

    .line 2687224
    :cond_3
    if-nez v3, :cond_b

    .line 2687225
    const v7, 0x7f0835f3

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/GjU;->a(LX/0Px;)LX/GjU;

    .line 2687226
    :goto_2
    invoke-virtual {v6}, LX/GjU;->b()Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v4

    .line 2687227
    new-instance v6, LX/JOJ;

    invoke-direct {v6, v0, v5, v1}, LX/JOJ;-><init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;Landroid/content/Context;LX/JOL;)V

    .line 2687228
    iput-object v6, v4, Lcom/facebook/greetingcards/render/RenderCardFragment;->n:LX/CEO;

    .line 2687229
    move-object v4, v4

    .line 2687230
    goto :goto_0

    .line 2687231
    :cond_4
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    invoke-static {v5}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v1

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v4, v1, v2, v6}, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->a(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    move-result-object v1

    iput-object v1, v0, LX/JOL;->d:Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    .line 2687232
    :cond_5
    :goto_3
    const v0, -0x64626b7b

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto :goto_1

    .line 2687233
    :cond_6
    :try_start_0
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    if-nez v0, :cond_7

    .line 2687234
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    new-instance v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-direct {v1}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;-><init>()V

    iput-object v1, v0, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    .line 2687235
    :cond_7
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    .line 2687236
    iput-object v4, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->B:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2687237
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->j()Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_8

    .line 2687238
    const v0, -0x8b215b4

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto :goto_1

    .line 2687239
    :cond_8
    :try_start_1
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v9

    new-instance v0, LX/JOG;

    move-object v1, p0

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/JOG;-><init>(LX/JOH;Landroid/app/Activity;Landroid/view/View;Lcom/facebook/greetingcards/render/RenderCardFragment;Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v9, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2687240
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->f:LX/Gjt;

    if-eqz v0, :cond_5

    .line 2687241
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->f:LX/Gjt;

    invoke-virtual {v0}, LX/Gjt;->a()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 2687242
    :catch_0
    move-exception v0

    .line 2687243
    iget-object v1, p0, LX/JOH;->d:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->c:LX/03V;

    const-string v3, "feedplugins.greetingcard.binder"

    const-string v7, "OutOfMemory creating the bitmap for the cover"

    invoke-virtual {v1, v3, v7, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2687244
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->f:LX/Gjt;

    if-eqz v0, :cond_9

    .line 2687245
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    iget-object v0, v0, LX/JOL;->f:LX/Gjt;

    invoke-virtual {v0}, LX/Gjt;->b()V

    .line 2687246
    :cond_9
    if-nez v2, :cond_a

    .line 2687247
    sget-object v0, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a:Ljava/lang/Class;

    const-string v1, "couldn\'t find activity"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2687248
    const v0, -0x220a54f2

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 2687249
    :cond_a
    iget-object v0, p0, LX/JOH;->c:LX/JOL;

    invoke-static {v5}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v1

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v4, v1, v2, v6}, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->a(Lcom/facebook/greetingcards/render/RenderCardFragment;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    move-result-object v1

    iput-object v1, v0, LX/JOL;->d:Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    goto/16 :goto_3

    .line 2687250
    :cond_b
    const v7, 0x7f0835f5

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/GjU;->a(LX/0Px;)LX/GjU;

    goto/16 :goto_2
.end method
