.class public final LX/IRc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IRb;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576574
    iput-object p1, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 3

    .prologue
    .line 2576575
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2576576
    :cond_0
    :goto_0
    return-void

    .line 2576577
    :cond_1
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-ne p1, v0, :cond_0

    .line 2576578
    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 2576579
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 2576580
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 2576581
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2576582
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2576583
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 2576584
    :cond_5
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0, p2}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a$redex0(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2576570
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;Z)V

    .line 2576571
    if-nez p1, :cond_0

    .line 2576572
    iget-object v0, p0, LX/IRc;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->mJ_()V

    .line 2576573
    :cond_0
    return-void
.end method
