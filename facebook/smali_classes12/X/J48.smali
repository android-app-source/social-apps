.class public final enum LX/J48;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J48;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J48;

.field public static final enum FINAL_FLUSH_REQUESTS:LX/J48;

.field public static final enum SEND_REQUESTS:LX/J48;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2643675
    new-instance v0, LX/J48;

    const-string v1, "SEND_REQUESTS"

    invoke-direct {v0, v1, v2}, LX/J48;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J48;->SEND_REQUESTS:LX/J48;

    .line 2643676
    new-instance v0, LX/J48;

    const-string v1, "FINAL_FLUSH_REQUESTS"

    invoke-direct {v0, v1, v3}, LX/J48;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J48;->FINAL_FLUSH_REQUESTS:LX/J48;

    .line 2643677
    const/4 v0, 0x2

    new-array v0, v0, [LX/J48;

    sget-object v1, LX/J48;->SEND_REQUESTS:LX/J48;

    aput-object v1, v0, v2

    sget-object v1, LX/J48;->FINAL_FLUSH_REQUESTS:LX/J48;

    aput-object v1, v0, v3

    sput-object v0, LX/J48;->$VALUES:[LX/J48;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2643678
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J48;
    .locals 1

    .prologue
    .line 2643679
    const-class v0, LX/J48;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J48;

    return-object v0
.end method

.method public static values()[LX/J48;
    .locals 1

    .prologue
    .line 2643680
    sget-object v0, LX/J48;->$VALUES:[LX/J48;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J48;

    return-object v0
.end method
