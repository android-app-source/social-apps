.class public final LX/I0J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V
    .locals 0

    .prologue
    .line 2526783
    iput-object p1, p0, LX/I0J;->a:Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2526784
    add-int v0, p2, p3

    add-int/lit8 v1, p4, -0x3

    if-le v0, v1, :cond_0

    .line 2526785
    iget-object v0, p0, LX/I0J;->a:Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->e:LX/7v8;

    iget-object v1, p0, LX/I0J;->a:Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->k:Ljava/lang/String;

    .line 2526786
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p2, LX/7v7;->BIRTHDAYS_FETCH_MORE_BIRTHDAYS:LX/7v7;

    invoke-virtual {p2}, LX/7v7;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2526787
    invoke-static {v0, p1, v1}, LX/7v8;->a(LX/7v8;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2526788
    iget-object v0, p0, LX/I0J;->a:Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;->b$redex0(Lcom/facebook/events/dashboard/birthdays/EventsUpcomingBirthdaysFragment;)V

    .line 2526789
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2526790
    return-void
.end method
