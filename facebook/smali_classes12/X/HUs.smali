.class public final enum LX/HUs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HUs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HUs;

.field public static final enum NAV_ITEM:LX/HUs;

.field public static final enum PLAYLIST_INFO_AND_VIDEOS:LX/HUs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2473905
    new-instance v0, LX/HUs;

    const-string v1, "NAV_ITEM"

    invoke-direct {v0, v1, v2}, LX/HUs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUs;->NAV_ITEM:LX/HUs;

    .line 2473906
    new-instance v0, LX/HUs;

    const-string v1, "PLAYLIST_INFO_AND_VIDEOS"

    invoke-direct {v0, v1, v3}, LX/HUs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HUs;->PLAYLIST_INFO_AND_VIDEOS:LX/HUs;

    .line 2473907
    const/4 v0, 0x2

    new-array v0, v0, [LX/HUs;

    sget-object v1, LX/HUs;->NAV_ITEM:LX/HUs;

    aput-object v1, v0, v2

    sget-object v1, LX/HUs;->PLAYLIST_INFO_AND_VIDEOS:LX/HUs;

    aput-object v1, v0, v3

    sput-object v0, LX/HUs;->$VALUES:[LX/HUs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2473908
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HUs;
    .locals 1

    .prologue
    .line 2473909
    const-class v0, LX/HUs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HUs;

    return-object v0
.end method

.method public static values()[LX/HUs;
    .locals 1

    .prologue
    .line 2473910
    sget-object v0, LX/HUs;->$VALUES:[LX/HUs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HUs;

    return-object v0
.end method
