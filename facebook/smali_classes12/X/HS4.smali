.class public abstract LX/HS4;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public b:LX/9X2;

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2466523
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2466524
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/HS4;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466525
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466520
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466521
    invoke-direct {p0, p1, p2}, LX/HS4;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466522
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466517
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466518
    invoke-direct {p0, p1, p2}, LX/HS4;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466519
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2466491
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/HS4;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    iput-object v4, v3, LX/HS4;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v3, LX/HS4;->d:LX/9XE;

    iput-object v7, v3, LX/HS4;->e:LX/17Y;

    iput-object v8, v3, LX/HS4;->f:LX/0wM;

    iput-object v0, v3, LX/HS4;->g:LX/0hL;

    .line 2466492
    const v0, 0x7f030e67

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2466493
    const v0, 0x7f0d2340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2466494
    sget-object v0, LX/03r;->PageIdentityLinkView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 2466495
    const/16 v0, 0x0

    invoke-virtual {v3, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2466496
    const/16 v2, 0x3

    invoke-virtual {v3, v2, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 2466497
    const/16 v4, 0x2

    invoke-virtual {v3, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 2466498
    if-eqz v0, :cond_1

    .line 2466499
    iget-object v5, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v5, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(I)V

    .line 2466500
    if-lez v2, :cond_0

    .line 2466501
    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    .line 2466502
    iget-object v5, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v5}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    if-eq v5, v0, :cond_0

    .line 2466503
    iget-object v5, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v5, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2466504
    :cond_0
    if-lez v4, :cond_1

    .line 2466505
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p0}, LX/HS4;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setTextColor(I)V

    .line 2466506
    :cond_1
    const/16 v0, 0x1

    invoke-virtual {v3, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2466507
    if-eqz v0, :cond_2

    .line 2466508
    iget-object v2, p0, LX/HS4;->f:LX/0wM;

    const v4, -0x423e37

    invoke-virtual {v2, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2466509
    invoke-static {p1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v4

    .line 2466510
    iget-object v5, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    if-eqz v4, :cond_3

    move-object v2, v1

    :goto_0
    if-eqz v4, :cond_4

    :goto_1
    invoke-virtual {v5, v2, v1, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2466511
    :cond_2
    const v0, 0x7f0d2341

    invoke-virtual {p0, v0}, LX/HS4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2466512
    iget-object v1, p0, LX/HS4;->g:LX/0hL;

    const v2, 0x7f0207ed

    invoke-virtual {v1, v2}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2466513
    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2466514
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 2466515
    return-void

    :cond_3
    move-object v2, v0

    .line 2466516
    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2466488
    iget-object v0, p0, LX/HS4;->b:LX/9X2;

    if-eqz v0, :cond_0

    .line 2466489
    iget-object v0, p0, LX/HS4;->d:LX/9XE;

    iget-object v1, p0, LX/HS4;->b:LX/9X2;

    invoke-virtual {v0, v1, p1, p2}, LX/9XE;->b(LX/9X2;J)V

    .line 2466490
    :cond_0
    return-void
.end method

.method public abstract a(Ljava/lang/String;JLX/0am;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2466465
    const-string v2, "%s %s"

    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iget-object v1, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2466466
    iget-object v3, v1, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    move-object v1, v3

    .line 2466467
    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    invoke-static {v2, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2466468
    iget-object v3, v1, Lcom/facebook/fbui/widget/text/BadgeTextView;->h:Ljava/lang/CharSequence;

    move-object v1, v3

    .line 2466469
    goto :goto_1
.end method

.method public setBadgeNumber(J)V
    .locals 7

    .prologue
    .line 2466480
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 2466481
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2466482
    :goto_0
    return-void

    .line 2466483
    :cond_0
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2466484
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x14

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 2466485
    invoke-virtual {p0}, LX/HS4;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08163b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 2466486
    :goto_1
    move-object v1, v3

    .line 2466487
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public setBadgeStyle(LX/HS9;)V
    .locals 3

    .prologue
    .line 2466474
    sget-object v0, LX/HS5;->a:[I

    invoke-virtual {p1}, LX/HS9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466475
    :goto_0
    return-void

    .line 2466476
    :pswitch_0
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p0}, LX/HS4;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e06ee

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 2466477
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const v1, 0x7f020f26

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeBackground(I)V

    goto :goto_0

    .line 2466478
    :pswitch_1
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p0}, LX/HS4;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e06ef

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 2466479
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setIcon(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2466472
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1, v1, v1, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2466473
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2466470
    iget-object v0, p0, LX/HS4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(I)V

    .line 2466471
    return-void
.end method
