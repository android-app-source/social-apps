.class public LX/HK8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/1DR;

.field public final c:LX/HIQ;

.field public final d:LX/HLN;


# direct methods
.method public constructor <init>(LX/3mL;LX/1DR;LX/HIQ;LX/HLN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2453798
    iput-object p1, p0, LX/HK8;->a:LX/3mL;

    .line 2453799
    iput-object p2, p0, LX/HK8;->b:LX/1DR;

    .line 2453800
    iput-object p3, p0, LX/HK8;->c:LX/HIQ;

    .line 2453801
    iput-object p4, p0, LX/HK8;->d:LX/HLN;

    .line 2453802
    return-void
.end method

.method public static a(LX/0QB;)LX/HK8;
    .locals 7

    .prologue
    .line 2453803
    const-class v1, LX/HK8;

    monitor-enter v1

    .line 2453804
    :try_start_0
    sget-object v0, LX/HK8;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453805
    sput-object v2, LX/HK8;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453806
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453807
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453808
    new-instance p0, LX/HK8;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-static {v0}, LX/HIQ;->a(LX/0QB;)LX/HIQ;

    move-result-object v5

    check-cast v5, LX/HIQ;

    const-class v6, LX/HLN;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/HLN;

    invoke-direct {p0, v3, v4, v5, v6}, LX/HK8;-><init>(LX/3mL;LX/1DR;LX/HIQ;LX/HLN;)V

    .line 2453809
    move-object v0, p0

    .line 2453810
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453811
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HK8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453812
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
