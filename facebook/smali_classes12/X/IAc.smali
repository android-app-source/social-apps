.class public final LX/IAc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/IAd;


# direct methods
.method public constructor <init>(LX/IAd;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2545912
    iput-object p1, p0, LX/IAc;->b:LX/IAd;

    iput-object p2, p0, LX/IAc;->a:Landroid/app/Activity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2545913
    iget-object v0, p0, LX/IAc;->b:LX/IAd;

    iget-object v0, v0, LX/IAd;->i:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2545914
    iget-object v0, p0, LX/IAc;->b:LX/IAd;

    iget-object v0, v0, LX/IAd;->b:Landroid/content/Context;

    const v1, 0x7f081f7b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2545915
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2545916
    check-cast p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2545917
    iget-object v0, p0, LX/IAc;->b:LX/IAd;

    iget-object v0, v0, LX/IAd;->i:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2545918
    iget-object v0, p0, LX/IAc;->b:LX/IAd;

    .line 2545919
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2545920
    iget-object v2, v0, LX/IAd;->g:LX/6eF;

    invoke-virtual {v2, p1}, LX/6eF;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2545921
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2545922
    iget-object v2, v0, LX/IAd;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/IAd;->b:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2545923
    iget-object v0, p0, LX/IAc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2545924
    return-void
.end method
