.class public LX/I4D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2533830
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/I4D;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2533831
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2533832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2533833
    iput-object p1, p0, LX/I4D;->a:Landroid/content/Context;

    .line 2533834
    iput-object p2, p0, LX/I4D;->b:Ljava/lang/String;

    .line 2533835
    return-void
.end method


# virtual methods
.method public final a(LX/B5Z;)LX/Clo;
    .locals 11

    .prologue
    .line 2533836
    invoke-interface {p1}, LX/B5Z;->e()LX/B5Y;

    move-result-object v1

    .line 2533837
    new-instance v0, LX/I4B;

    iget-object v2, p0, LX/I4D;->a:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/I4B;-><init>(Landroid/content/Context;)V

    invoke-interface {p1}, LX/B5Z;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/I4B;->a(Ljava/lang/String;)LX/I4B;

    move-result-object v0

    invoke-interface {v1}, LX/B5Y;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/I4B;->b(Ljava/lang/String;)LX/I4B;

    move-result-object v0

    invoke-interface {v1}, LX/B5Y;->c()I

    move-result v2

    invoke-virtual {v0, v2}, LX/I4B;->a(I)LX/I4B;

    move-result-object v0

    invoke-interface {v1}, LX/B5Y;->q()LX/8Z4;

    move-result-object v2

    invoke-interface {v2}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/I4B;->c(Ljava/lang/String;)LX/I4B;

    move-result-object v0

    .line 2533838
    iget-object v2, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Chi;

    .line 2533839
    iget-object v3, v2, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v2, v3

    .line 2533840
    if-eqz v2, :cond_0

    iget-object v2, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Chi;

    .line 2533841
    iget-object v3, v2, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v2, v3

    .line 2533842
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2533843
    iget-object v3, v0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v4, "background_color"

    iget-object v2, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Chi;

    .line 2533844
    iget-object v5, v2, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v2, v5

    .line 2533845
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2533846
    :cond_0
    move-object v0, v0

    .line 2533847
    invoke-interface {v1}, LX/B5Y;->n()LX/8Yp;

    move-result-object v2

    invoke-interface {v2}, LX/8Yp;->e()Ljava/lang/String;

    move-result-object v2

    .line 2533848
    iget-object v3, v0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v4, "publisher_name"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533849
    move-object v2, v0

    .line 2533850
    invoke-static {v1}, LX/IXa;->a(LX/B5Y;)LX/ClZ;

    move-result-object v0

    .line 2533851
    iput-object v0, v2, LX/I4B;->t:LX/ClZ;

    .line 2533852
    move-object v0, v2

    .line 2533853
    invoke-interface {v1}, LX/B5Y;->m()LX/8Z4;

    move-result-object v3

    .line 2533854
    iput-object v3, v0, LX/I4B;->u:LX/8Z4;

    .line 2533855
    move-object v0, v0

    .line 2533856
    invoke-interface {v1}, LX/B5Y;->q()LX/8Z4;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/I4B;->b(LX/8Z4;)LX/I4B;

    move-result-object v0

    invoke-interface {v1}, LX/B5Y;->p()LX/8Z4;

    move-result-object v3

    .line 2533857
    iput-object v3, v0, LX/I4B;->w:LX/8Z4;

    .line 2533858
    move-object v0, v0

    .line 2533859
    invoke-interface {v1}, LX/B5Y;->iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/I4B;->a(Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;)LX/I4B;

    move-result-object v0

    invoke-interface {v1}, LX/B5Y;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;

    move-result-object v3

    invoke-interface {v1}, LX/B5Y;->e()LX/0Px;

    move-result-object v4

    .line 2533860
    new-instance v6, LX/CmC;

    invoke-direct {v6, v3, v4}, LX/CmC;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;Ljava/util/List;)V

    .line 2533861
    iget-object v7, v0, LX/I4B;->l:LX/Clh;

    iget-object v5, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Chi;

    .line 2533862
    iget-object v8, v5, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v5, v8

    .line 2533863
    if-eqz v5, :cond_a

    iget-object v5, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Chi;

    .line 2533864
    iget-object v8, v5, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v5, v8

    .line 2533865
    invoke-virtual {v5}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v5

    :goto_0
    iget-object v8, v0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v7, v5, v6, v8}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533866
    invoke-virtual {v6}, LX/CmC;->c()LX/CmD;

    move-result-object v5

    iput-object v5, v0, LX/I4B;->x:LX/Clr;

    .line 2533867
    invoke-interface {v1}, LX/B5Y;->l()LX/B5X;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/I4B;->a(LX/B5X;)LX/I4B;

    .line 2533868
    invoke-interface {v1}, LX/B5Y;->j()LX/8Z4;

    move-result-object v0

    .line 2533869
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2533870
    new-instance v3, LX/CmL;

    invoke-direct {v3, v0}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v4, LX/Clb;->CREDITS:LX/Clb;

    .line 2533871
    iput-object v4, v3, LX/CmL;->b:LX/Clb;

    .line 2533872
    move-object v4, v3

    .line 2533873
    iget-object v5, v2, LX/I4B;->l:LX/Clh;

    iget-object v3, v2, LX/I4B;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Chi;

    .line 2533874
    iget-object v6, v3, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v3, v6

    .line 2533875
    if-eqz v3, :cond_b

    iget-object v3, v2, LX/I4B;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Chi;

    .line 2533876
    iget-object v6, v3, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v3, v6

    .line 2533877
    invoke-virtual {v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v3

    :goto_1
    iget-object v6, v2, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v5, v3, v4, v6}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533878
    invoke-virtual {v4}, LX/CmL;->c()LX/Cly;

    move-result-object v3

    iput-object v3, v2, LX/I4B;->A:LX/Clr;

    .line 2533879
    :cond_1
    move-object v0, v2

    .line 2533880
    invoke-interface {v1}, LX/B5Y;->iQ_()LX/8Z4;

    move-result-object v3

    .line 2533881
    if-eqz v3, :cond_2

    invoke-interface {v3}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2533882
    new-instance v4, LX/CmL;

    invoke-direct {v4, v3}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v5, LX/Clb;->COPYRIGHT:LX/Clb;

    .line 2533883
    iput-object v5, v4, LX/CmL;->b:LX/Clb;

    .line 2533884
    move-object v5, v4

    .line 2533885
    iget-object v6, v0, LX/I4B;->l:LX/Clh;

    iget-object v4, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Chi;

    .line 2533886
    iget-object v7, v4, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v4, v7

    .line 2533887
    if-eqz v4, :cond_c

    iget-object v4, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Chi;

    .line 2533888
    iget-object v7, v4, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v4, v7

    .line 2533889
    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v4

    :goto_2
    iget-object v7, v0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v6, v4, v5, v7}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533890
    invoke-virtual {v5}, LX/CmL;->c()LX/Cly;

    move-result-object v4

    iput-object v4, v0, LX/I4B;->B:LX/Clr;

    .line 2533891
    :cond_2
    move-object v3, v0

    .line 2533892
    iget-object v0, p0, LX/I4D;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, LX/I4D;->b:Ljava/lang/String;

    .line 2533893
    :goto_3
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, v3, LX/I4B;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2533894
    new-instance v4, LX/Cmc;

    invoke-direct {v4, v0}, LX/Cmc;-><init>(Ljava/lang/String;)V

    iput-object v4, v3, LX/I4B;->C:LX/Clr;

    .line 2533895
    :cond_3
    move-object v0, v3

    .line 2533896
    iget-object v3, p0, LX/I4D;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v1}, LX/B5Y;->k()LX/B5W;

    move-result-object v4

    invoke-interface {v4}, LX/B5W;->a()LX/0Px;

    move-result-object v4

    invoke-interface {v1}, LX/B5Y;->s()J

    .line 2533897
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2533898
    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2533899
    iget-object v1, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chi;

    .line 2533900
    iget-object v6, v1, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v1, v6

    .line 2533901
    if-eqz v1, :cond_4

    iget-object v1, v0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Chi;

    .line 2533902
    iget-object v6, v1, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v1, v6

    .line 2533903
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->u()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2533904
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/I4B;->E:Ljava/util/List;

    .line 2533905
    const v1, 0x7f081c5a

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2533906
    new-instance v6, LX/Cmb;

    invoke-direct {v6}, LX/Cmb;-><init>()V

    .line 2533907
    iput-object v1, v6, LX/Cmb;->a:Ljava/lang/String;

    .line 2533908
    move-object v1, v6

    .line 2533909
    iget-object v6, v0, LX/I4B;->E:Ljava/util/List;

    new-instance v7, LX/CmL;

    const/16 v8, 0x10

    invoke-virtual {v1}, LX/Cmb;->a()LX/8Z4;

    move-result-object v1

    invoke-direct {v7, v8, v1}, LX/CmL;-><init>(ILX/8Z4;)V

    sget-object v1, LX/Clb;->AUTHORS_CONTRIBUTORS_HEADER:LX/Clb;

    .line 2533910
    iput-object v1, v7, LX/CmL;->b:LX/Clb;

    .line 2533911
    move-object v1, v7

    .line 2533912
    invoke-virtual {v1}, LX/CmL;->c()LX/Cly;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533913
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorEdgeModel;

    .line 2533914
    iget-object v7, v0, LX/I4B;->E:Ljava/util/List;

    new-instance v8, LX/Cm6;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;

    move-result-object v1

    invoke-direct {v8, v1}, LX/Cm6;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2533915
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorEdgeModel;

    .line 2533916
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentAuthorModel;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2533917
    :cond_5
    move-object v0, v0

    .line 2533918
    invoke-interface {p1}, LX/B5Z;->iS_()LX/0Px;

    move-result-object v1

    iget-object v3, p0, LX/I4D;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 2533919
    if-nez v1, :cond_d

    .line 2533920
    :cond_6
    move-object v0, v0

    .line 2533921
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->LIKES_AND_COMMENTS:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    invoke-interface {p1}, LX/B5Z;->c()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    const/4 v8, 0x0

    .line 2533922
    iget-object v5, v0, LX/I4B;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0ad;

    sget-short v7, LX/2yD;->B:S

    iget-object v6, v0, LX/I4B;->k:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v9, 0xa9

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, LX/0Uh;->a(IZ)Z

    move-result v6

    invoke-interface {v5, v7, v6}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 2533923
    iget-object v5, v0, LX/I4B;->C:LX/Clr;

    if-eqz v5, :cond_f

    iget-object v5, v0, LX/I4B;->C:LX/Clr;

    instance-of v5, v5, LX/Cmc;

    if-eqz v5, :cond_f

    .line 2533924
    iget-object v5, v0, LX/I4B;->C:LX/Clr;

    check-cast v5, LX/Cmc;

    .line 2533925
    iget-object v6, v5, LX/Cmc;->a:Ljava/lang/String;

    move-object v10, v6

    .line 2533926
    :goto_6
    iget-object v5, v0, LX/I4B;->h:LX/Crz;

    invoke-static {v1, v3, v5}, LX/ClW;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;LX/Crz;)LX/ClW;

    move-result-object v6

    .line 2533927
    if-eqz v6, :cond_8

    if-eqz v7, :cond_8

    iget-object v5, v0, LX/I4B;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, v0, LX/I4B;->i:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/K2B;

    .line 2533928
    const/4 v7, 0x1

    move v5, v7

    .line 2533929
    if-eqz v5, :cond_8

    .line 2533930
    iget-object v5, v0, LX/I4B;->v:LX/8Z4;

    invoke-static {v5}, LX/IXa;->a(LX/8Z4;)LX/Cly;

    move-result-object v7

    .line 2533931
    iget-object v5, v0, LX/I4B;->x:LX/Clr;

    if-eqz v5, :cond_7

    iget-object v5, v0, LX/I4B;->x:LX/Clr;

    instance-of v5, v5, LX/CmD;

    if-eqz v5, :cond_7

    .line 2533932
    iget-object v5, v0, LX/I4B;->x:LX/Clr;

    check-cast v5, LX/CmD;

    move-object v8, v5

    .line 2533933
    :cond_7
    iget-object v5, v0, LX/I4B;->t:LX/ClZ;

    invoke-static {v5}, LX/IXa;->a(LX/ClZ;)LX/CmQ;

    move-result-object v9

    .line 2533934
    new-instance v5, LX/IXe;

    invoke-direct/range {v5 .. v10}, LX/IXe;-><init>(LX/ClW;LX/Cly;LX/CmD;LX/CmQ;Ljava/lang/String;)V

    iput-object v5, v0, LX/I4B;->G:LX/Clr;

    .line 2533935
    :cond_8
    invoke-virtual {v2}, LX/I4B;->b()LX/Clo;

    move-result-object v0

    return-object v0

    .line 2533936
    :cond_9
    invoke-interface {v1}, LX/B5Y;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2533937
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 2533938
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 2533939
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 2533940
    :cond_d
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, LX/I4B;->D:Ljava/util/List;

    .line 2533941
    const/4 v3, 0x0

    .line 2533942
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;

    .line 2533943
    if-eqz v3, :cond_e

    .line 2533944
    invoke-virtual {v3}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;->c()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v6

    .line 2533945
    invoke-static {v0, v6, v3, v4}, LX/I4B;->a(LX/I4B;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;I)V

    .line 2533946
    invoke-virtual {v3}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/2addr v3, v4

    :goto_8
    move v4, v3

    .line 2533947
    goto :goto_7

    :cond_e
    move v3, v4

    goto :goto_8

    :cond_f
    move-object v10, v8

    goto/16 :goto_6
.end method
