.class public final LX/Hix;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;


# direct methods
.method public constructor <init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V
    .locals 0

    .prologue
    .line 2498046
    iput-object p1, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2498047
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498048
    iget-object v0, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v1, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a2a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2498049
    invoke-static {v0, v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V

    .line 2498050
    :goto_0
    return-void

    .line 2498051
    :cond_0
    iget-object v0, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->m(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V

    .line 2498052
    iget-object v0, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->s:LX/Hin;

    .line 2498053
    iget-object v1, v0, LX/Hin;->a:LX/His;

    .line 2498054
    if-eqz p1, :cond_1

    :goto_1
    iput-object p1, v1, LX/His;->b:LX/0Px;

    .line 2498055
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2498056
    goto :goto_0

    .line 2498057
    :cond_1
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 2498058
    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2498059
    iget-object v0, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v1, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a2a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2498060
    invoke-static {v0, v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Ljava/lang/String;)V

    .line 2498061
    iget-object v0, p0, LX/Hix;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->c:LX/03V;

    sget-object v1, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->h:Ljava/lang/String;

    const-string v2, "Can\'t get Address type ahead result"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2498062
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2498063
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/Hix;->a(LX/0Px;)V

    return-void
.end method
