.class public LX/Iud;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iua;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IiA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/297;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627023
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 4

    .prologue
    .line 2627018
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2627019
    iget-object v1, p0, LX/Iud;->c:LX/297;

    invoke-virtual {v1}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Iud;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v2}, LX/0db;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Iud;->b:LX/IiA;

    invoke-virtual {v1, v0}, LX/IiA;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2627020
    sget-object v0, LX/Iuf;->FORCE_BUZZ:LX/Iuf;

    .line 2627021
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2627017
    const-string v0, "MentionNotifySetRule"

    return-object v0
.end method
