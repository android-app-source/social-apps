.class public final LX/HZH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V
    .locals 0

    .prologue
    .line 2482031
    iput-object p1, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2482032
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2482033
    const-string v1, "fail_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482034
    iget-object v1, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v1, v1, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v2, "SEARCH_FAIL"

    iget-object v3, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v3, v3, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget-object v4, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget v4, v4, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    invoke-virtual {v1, v2, v3, v4, v0}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482035
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    .line 2482036
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2482037
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2482038
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    if-eqz v0, :cond_3

    .line 2482039
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;

    .line 2482040
    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethod$Result;->b()LX/0Px;

    move-result-object v0

    .line 2482041
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2482042
    const-string v2, "candidate_size"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2482043
    iget-object v2, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v2, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v3, "SEARCH_SUCCESS"

    iget-object v4, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v4, v4, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget-object v5, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget v5, v5, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    invoke-virtual {v2, v3, v4, v5, v1}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482044
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2482045
    iget-object v1, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2482046
    iput-object v0, v1, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    .line 2482047
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    .line 2482048
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    if-nez v1, :cond_0

    .line 2482049
    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    .line 2482050
    :cond_0
    const v1, 0x7f0835ba

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v2}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2482051
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482052
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v1

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v2, :cond_4

    .line 2482053
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->c:LX/3fx;

    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3fx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2482054
    :goto_0
    const v2, 0x7f0835bb

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v3}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2482055
    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2482056
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v3}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2482057
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p:Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    invoke-virtual {v3}, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2482058
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2482059
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2482060
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2482061
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->j:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2482062
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-boolean v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->q:Z

    if-eqz v0, :cond_1

    .line 2482063
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v1, "ACCOUNT_RECOVERY_AUTO_REDIRECT"

    iget-object v2, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v2, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget-object v3, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget v3, v3, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    invoke-virtual {v0, v1, v2, v3, v7}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482064
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->o(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    .line 2482065
    :cond_1
    :goto_1
    return-void

    .line 2482066
    :cond_2
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-boolean v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->r:Z

    if-eqz v0, :cond_3

    .line 2482067
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->g:LX/HZt;

    const-string v1, "CONTINUE_REG_DUE_TO_NO_MATCH"

    iget-object v2, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget-object v2, v2, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->s:Ljava/lang/String;

    iget-object v3, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    iget v3, v3, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->t:I

    invoke-virtual {v0, v1, v2, v3, v7}, LX/HZt;->a(Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 2482068
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->p(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    .line 2482069
    :cond_3
    iget-object v0, p0, LX/HZH;->a:Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->m(Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;)V

    goto :goto_1

    .line 2482070
    :cond_4
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method
