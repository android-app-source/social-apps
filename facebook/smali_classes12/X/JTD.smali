.class public LX/JTD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/JT2;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/JT2;


# direct methods
.method public constructor <init>(LX/JTM;LX/JT3;LX/JTC;LX/JT7;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696497
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JTD;->a:Ljava/util/List;

    .line 2696498
    iget-object v0, p0, LX/JTD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2696499
    iget-object v0, p0, LX/JTD;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2696500
    iget-object v0, p0, LX/JTD;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2696501
    iput-object p4, p0, LX/JTD;->b:LX/JT2;

    .line 2696502
    return-void
.end method

.method public static b(LX/0QB;)LX/JTD;
    .locals 9

    .prologue
    .line 2696503
    new-instance v4, LX/JTD;

    .line 2696504
    new-instance v3, LX/JTM;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/JTI;->b(LX/0QB;)LX/JTI;

    move-result-object v1

    check-cast v1, LX/JTI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v3, v0, v1, v2}, LX/JTM;-><init>(Landroid/content/Context;LX/JTI;Ljava/util/concurrent/ExecutorService;)V

    .line 2696505
    move-object v0, v3

    .line 2696506
    check-cast v0, LX/JTM;

    .line 2696507
    new-instance v3, LX/JT3;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/JTI;->b(LX/0QB;)LX/JTI;

    move-result-object v2

    check-cast v2, LX/JTI;

    invoke-direct {v3, v1, v2}, LX/JT3;-><init>(Landroid/content/Context;LX/JTI;)V

    .line 2696508
    move-object v1, v3

    .line 2696509
    check-cast v1, LX/JT3;

    .line 2696510
    new-instance v6, LX/JTC;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/JTI;->b(LX/0QB;)LX/JTI;

    move-result-object v3

    check-cast v3, LX/JTI;

    .line 2696511
    new-instance v8, LX/JSU;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v7

    check-cast v7, LX/17d;

    invoke-direct {v8, v5, v7}, LX/JSU;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17d;)V

    .line 2696512
    move-object v5, v8

    .line 2696513
    check-cast v5, LX/JSU;

    invoke-direct {v6, v2, v3, v5}, LX/JTC;-><init>(Landroid/content/Context;LX/JTI;LX/JSU;)V

    .line 2696514
    move-object v2, v6

    .line 2696515
    check-cast v2, LX/JTC;

    .line 2696516
    new-instance v6, LX/JT7;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/JTI;->b(LX/0QB;)LX/JTI;

    move-result-object v5

    check-cast v5, LX/JTI;

    invoke-direct {v6, v3, v5}, LX/JT7;-><init>(Landroid/content/Context;LX/JTI;)V

    .line 2696517
    move-object v3, v6

    .line 2696518
    check-cast v3, LX/JT7;

    invoke-direct {v4, v0, v1, v2, v3}, LX/JTD;-><init>(LX/JTM;LX/JT3;LX/JTC;LX/JT7;)V

    .line 2696519
    return-object v4
.end method


# virtual methods
.method public final a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;
    .locals 4

    .prologue
    .line 2696520
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696521
    iget-object v0, p0, LX/JTD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JT2;

    .line 2696522
    iget-object v2, p1, LX/JSe;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2696523
    invoke-interface {v0, v2}, LX/JT2;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0}, LX/JT2;->a()Ljava/lang/String;

    move-result-object v2

    .line 2696524
    iget-object v3, p1, LX/JSe;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2696525
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2696526
    :cond_1
    invoke-interface {v0, p1, p2, p3, p4}, LX/JT2;->a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;

    move-result-object v0

    .line 2696527
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, LX/JTD;->b:LX/JT2;

    invoke-interface {v0, p1, p2, p3, p4}, LX/JT2;->a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;

    move-result-object v0

    goto :goto_0
.end method
