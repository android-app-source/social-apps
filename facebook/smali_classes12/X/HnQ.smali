.class public LX/HnQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:LX/0ad;

.field public final d:LX/0Zb;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/1Bj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2501841
    const-class v0, LX/HnQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HnQ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;Landroid/os/Handler;LX/0Or;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Bj;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "Landroid/os/Handler;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Zb;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1Bj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2501842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501843
    iput-object p2, p0, LX/HnQ;->b:Landroid/os/Handler;

    .line 2501844
    iput-object p1, p0, LX/HnQ;->c:LX/0ad;

    .line 2501845
    iput-object p4, p0, LX/HnQ;->d:LX/0Zb;

    .line 2501846
    iput-object p3, p0, LX/HnQ;->e:LX/0Or;

    .line 2501847
    iput-object p5, p0, LX/HnQ;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2501848
    iput-object p6, p0, LX/HnQ;->g:LX/1Bj;

    .line 2501849
    return-void
.end method

.method public static a(LX/0QB;)LX/HnQ;
    .locals 8

    .prologue
    .line 2501850
    new-instance v1, LX/HnQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    const/16 v4, 0x2fd

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1Bj;->b(LX/0QB;)LX/1Bj;

    move-result-object v7

    check-cast v7, LX/1Bj;

    invoke-direct/range {v1 .. v7}, LX/HnQ;-><init>(LX/0ad;Landroid/os/Handler;LX/0Or;LX/0Zb;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Bj;)V

    .line 2501851
    move-object v0, v1

    .line 2501852
    return-object v0
.end method

.method private static declared-synchronized a(LX/HnQ;Ljava/util/List;Z)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2501853
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 2501854
    if-eqz p2, :cond_0

    .line 2501855
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    .line 2501856
    :cond_0
    iget-object v1, p0, LX/HnQ;->b:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$2;-><init>(LX/HnQ;Ljava/util/List;Landroid/webkit/CookieManager;)V

    const v0, -0x48f92ef1

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2501857
    monitor-exit p0

    return-void

    .line 2501858
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized c(LX/HnQ;)V
    .locals 9

    .prologue
    .line 2501859
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HnQ;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const-wide/16 v7, -0x1

    .line 2501860
    iget-object v5, p0, LX/HnQ;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/1C0;->k:LX/0Tn;

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    .line 2501861
    cmp-long v7, v5, v7

    if-eqz v7, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v5, v7, v5

    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-gez v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    move v0, v5

    .line 2501862
    if-nez v0, :cond_0

    .line 2501863
    iget-object v0, p0, LX/HnQ;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$1;

    invoke-direct {v1, p0}, Lcom/facebook/browser/liteclient/BrowserInterProcessCookieSyncer$1;-><init>(LX/HnQ;)V

    const-wide/16 v2, 0x7530

    const v4, 0x7fa69542

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2501864
    :cond_0
    monitor-exit p0

    return-void

    .line 2501865
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2501866
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/HnQ;->c:LX/0ad;

    sget-short v2, LX/1Bm;->G:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 2501867
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 2501868
    :cond_1
    :try_start_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 2501869
    iget-object v1, p0, LX/HnQ;->g:LX/1Bj;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/1Bj;->a(I)Ljava/util/List;

    move-result-object v1

    .line 2501870
    if-eqz v1, :cond_0

    .line 2501871
    const/4 v0, 0x0

    invoke-static {p0, v1, v0}, LX/HnQ;->a(LX/HnQ;Ljava/util/List;Z)V

    .line 2501872
    invoke-static {p0}, LX/HnQ;->c(LX/HnQ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2501873
    const/4 v0, 0x1

    goto :goto_0

    .line 2501874
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/0EC;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2501875
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HnQ;->g:LX/1Bj;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1Bj;->a(I)Ljava/util/List;

    move-result-object v0

    .line 2501876
    iget-object v1, p0, LX/HnQ;->g:LX/1Bj;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1Bj;->a(I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2501877
    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 2501878
    :cond_0
    const/4 v0, 0x0

    .line 2501879
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2501880
    :cond_1
    :try_start_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2501881
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2501882
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EC;

    .line 2501883
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    const-string v7, "."

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "+"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, v0, LX/0EC;->c:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2501884
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2501885
    :cond_2
    :try_start_2
    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    goto :goto_2

    .line 2501886
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0EC;

    .line 2501887
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    const-string v6, "."

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "+"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, LX/0EC;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    iget-object v1, v0, LX/0EC;->b:Ljava/lang/String;

    goto :goto_4

    .line 2501888
    :cond_5
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 2501889
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2501890
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 2501891
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2501892
    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2501893
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0EC;

    .line 2501894
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0EC;

    .line 2501895
    iget-wide v8, v1, LX/0EC;->f:J

    iget-wide v10, v2, LX/0EC;->f:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    .line 2501896
    iget-object v10, v1, LX/0EC;->d:Ljava/lang/String;

    iget-object v11, v2, LX/0EC;->d:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2501897
    iget-boolean v0, v1, LX/0EC;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_5

    .line 2501898
    :cond_7
    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v0, 0x1

    iget-object v11, v1, LX/0EC;->d:Ljava/lang/String;

    aput-object v11, v10, v0

    const/4 v0, 0x2

    iget-object v11, v2, LX/0EC;->d:Ljava/lang/String;

    aput-object v11, v10, v0

    const/4 v0, 0x3

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v10, v0

    const/4 v0, 0x4

    iget-boolean v1, v1, LX/0EC;->g:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v10, v0

    const/4 v0, 0x5

    iget-boolean v1, v2, LX/0EC;->g:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v10, v0

    goto :goto_5

    .line 2501899
    :cond_8
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0EC;

    invoke-virtual {v1, v6}, LX/0EC;->a(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0EC;

    iget-object v1, v1, LX/0EC;->i:Ljava/util/Date;

    if-eqz v1, :cond_6

    .line 2501900
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2501901
    :cond_9
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_a
    move-object v0, v3

    .line 2501902
    goto/16 :goto_0
.end method
