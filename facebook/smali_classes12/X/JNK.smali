.class public final LX/JNK;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/JNL;


# direct methods
.method public constructor <init>(LX/JNL;)V
    .locals 1

    .prologue
    .line 2685618
    iput-object p1, p0, LX/JNK;->d:LX/JNL;

    .line 2685619
    move-object v0, p1

    .line 2685620
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2685621
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2685622
    const-string v0, "EventsSuggestionItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2685623
    if-ne p0, p1, :cond_1

    .line 2685624
    :cond_0
    :goto_0
    return v0

    .line 2685625
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2685626
    goto :goto_0

    .line 2685627
    :cond_3
    check-cast p1, LX/JNK;

    .line 2685628
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2685629
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2685630
    if-eq v2, v3, :cond_0

    .line 2685631
    iget-object v2, p0, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v3, p1, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2685632
    goto :goto_0

    .line 2685633
    :cond_5
    iget-object v2, p1, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-nez v2, :cond_4

    .line 2685634
    :cond_6
    iget-object v2, p0, LX/JNK;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JNK;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JNK;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2685635
    goto :goto_0

    .line 2685636
    :cond_8
    iget-object v2, p1, LX/JNK;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2685637
    :cond_9
    iget-object v2, p0, LX/JNK;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JNK;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JNK;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2685638
    goto :goto_0

    .line 2685639
    :cond_a
    iget-object v2, p1, LX/JNK;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
