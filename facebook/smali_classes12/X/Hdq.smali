.class public LX/Hdq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0rm;

.field private b:Ljava/lang/String;

.field private c:LX/0w9;

.field private d:LX/0rq;


# direct methods
.method public constructor <init>(LX/0rm;LX/0w9;LX/0rq;Ljava/lang/String;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2489327
    iput-object p1, p0, LX/Hdq;->a:LX/0rm;

    .line 2489328
    iput-object p2, p0, LX/Hdq;->c:LX/0w9;

    .line 2489329
    iput-object p3, p0, LX/Hdq;->d:LX/0rq;

    .line 2489330
    iput-object p4, p0, LX/Hdq;->b:Ljava/lang/String;

    .line 2489331
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2489332
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2489333
    iput-object v1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2489334
    move-object v0, v0

    .line 2489335
    iget v1, p1, LX/3DR;->e:I

    move v1, v1

    .line 2489336
    iput v1, v0, LX/0rT;->c:I

    .line 2489337
    move-object v0, v0

    .line 2489338
    iget-object v1, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2489339
    iput-object v1, v0, LX/0rT;->g:Ljava/lang/String;

    .line 2489340
    move-object v0, v0

    .line 2489341
    iget-object v1, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2489342
    iput-object v1, v0, LX/0rT;->f:Ljava/lang/String;

    .line 2489343
    move-object v0, v0

    .line 2489344
    sget-object v1, LX/0gf;->INITIALIZATION:LX/0gf;

    invoke-virtual {v0, v1}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    const/4 v1, 0x0

    .line 2489345
    iput-boolean v1, v0, LX/0rT;->j:Z

    .line 2489346
    move-object v0, v0

    .line 2489347
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2489348
    iput-object v1, v0, LX/0rT;->a:LX/0rS;

    .line 2489349
    move-object v0, v0

    .line 2489350
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 2489351
    new-instance v1, LX/Hdi;

    invoke-direct {v1}, LX/Hdi;-><init>()V

    move-object v1, v1

    .line 2489352
    const-string v2, "topic_id"

    iget-object v3, p0, LX/Hdq;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2489353
    invoke-static {v1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 2489354
    const-string v2, "before_home_story_param"

    const-string v3, "after_home_story_param"

    invoke-static {v1, v0, v2, v3}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2489355
    iget-object v2, p0, LX/Hdq;->c:LX/0w9;

    invoke-virtual {v2, v1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 2489356
    iget-object v2, p0, LX/Hdq;->c:LX/0w9;

    invoke-virtual {v2, v1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 2489357
    invoke-static {v1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 2489358
    const-string v2, "first_home_story_param"

    .line 2489359
    iget v3, v0, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v0, v3

    .line 2489360
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "media_type"

    iget-object v3, p0, LX/Hdq;->d:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2489361
    return-object v1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2489362
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2489363
    check-cast v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;

    invoke-virtual {v0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    move-result-object v2

    .line 2489364
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2489365
    invoke-virtual {v2}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel$EdgesModel;

    .line 2489366
    invoke-virtual {v0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    instance-of v6, v6, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v6, :cond_0

    .line 2489367
    invoke-virtual {v0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2489368
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2489369
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v5

    .line 2489370
    new-instance v0, LX/5Mb;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->c()Z

    move-result v4

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->b()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method
