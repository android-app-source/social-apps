.class public final LX/HZE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2481753
    iput-object p1, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    iput-object p2, p0, LX/HZE;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2481754
    iget-object v0, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2481755
    iget-object v1, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    iget-object v1, v1, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->g:LX/5Qo;

    iget-object v2, p0, LX/HZE;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/5Qo;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2481756
    :try_start_0
    iget-object v2, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    iget-object v2, v2, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2481757
    :goto_0
    iget-object v0, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2481758
    return-void

    .line 2481759
    :catch_0
    iget-object v0, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0835a0

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2481760
    iget-object v0, p0, LX/HZE;->b:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->i:LX/03V;

    const-string v1, "RegistrationCompletionUrlBrowserMissing"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ActivityNotFoundException when attempting to open web view to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HZE;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
