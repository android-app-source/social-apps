.class public final enum LX/Hjw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hjw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hjw;

.field public static final enum b:LX/Hjw;

.field public static final enum c:LX/Hjw;

.field public static final enum d:LX/Hjw;

.field private static final synthetic e:[LX/Hjw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hjw;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/Hjw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjw;->a:LX/Hjw;

    new-instance v0, LX/Hjw;

    const-string v1, "BANNER"

    invoke-direct {v0, v1, v3}, LX/Hjw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjw;->b:LX/Hjw;

    new-instance v0, LX/Hjw;

    const-string v1, "INTERSTITIAL"

    invoke-direct {v0, v1, v4}, LX/Hjw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjw;->c:LX/Hjw;

    new-instance v0, LX/Hjw;

    const-string v1, "NATIVE"

    invoke-direct {v0, v1, v5}, LX/Hjw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hjw;->d:LX/Hjw;

    const/4 v0, 0x4

    new-array v0, v0, [LX/Hjw;

    sget-object v1, LX/Hjw;->a:LX/Hjw;

    aput-object v1, v0, v2

    sget-object v1, LX/Hjw;->b:LX/Hjw;

    aput-object v1, v0, v3

    sget-object v1, LX/Hjw;->c:LX/Hjw;

    aput-object v1, v0, v4

    sget-object v1, LX/Hjw;->d:LX/Hjw;

    aput-object v1, v0, v5

    sput-object v0, LX/Hjw;->e:[LX/Hjw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(LX/Hk2;)LX/Hjw;
    .locals 2

    sget-object v0, LX/Hjv;->a:[I

    invoke-virtual {p0}, LX/Hk2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, LX/Hjw;->a:LX/Hjw;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, LX/Hjw;->d:LX/Hjw;

    goto :goto_0

    :pswitch_1
    sget-object v0, LX/Hjw;->b:LX/Hjw;

    goto :goto_0

    :pswitch_2
    sget-object v0, LX/Hjw;->c:LX/Hjw;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hjw;
    .locals 1

    const-class v0, LX/Hjw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjw;

    return-object v0
.end method

.method public static values()[LX/Hjw;
    .locals 1

    sget-object v0, LX/Hjw;->e:[LX/Hjw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hjw;

    return-object v0
.end method
