.class public final LX/HSc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:I

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/HSk;


# direct methods
.method public constructor <init>(LX/HSk;JILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2467375
    iput-object p1, p0, LX/HSc;->d:LX/HSk;

    iput-wide p2, p0, LX/HSc;->a:J

    iput p4, p0, LX/HSc;->b:I

    iput-object p5, p0, LX/HSc;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2467376
    new-instance v0, LX/HSo;

    invoke-direct {v0}, LX/HSo;-><init>()V

    move-object v0, v0

    .line 2467377
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2467378
    new-instance v1, LX/HSo;

    invoke-direct {v1}, LX/HSo;-><init>()V

    const-string v2, "page_id"

    iget-wide v4, p0, LX/HSc;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_count"

    iget v3, p0, LX/HSc;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "end_cursor"

    iget-object v3, p0, LX/HSc;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/HSc;->d:LX/HSk;

    iget v3, v3, LX/HSk;->d:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2467379
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2467380
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2467381
    iget-object v1, p0, LX/HSc;->d:LX/HSk;

    iget-object v1, v1, LX/HSk;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
