.class public final LX/HUl;
.super LX/1Cv;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

.field public b:LX/1Cv;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 1

    .prologue
    .line 2473643
    iput-object p1, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2473644
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HUl;->c:Z

    return-void
.end method

.method private b()I
    .locals 2

    .prologue
    .line 2473640
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0}, LX/1Cv;->getCount()I

    move-result v0

    .line 2473641
    :goto_0
    invoke-virtual {p0}, LX/HUl;->getCount()I

    move-result v1

    sub-int v0, v1, v0

    return v0

    .line 2473642
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2473632
    sget-object v0, LX/HUm;->LOADING_FOOTER:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2473633
    iget-object v0, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f03159e

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2473634
    :goto_0
    return-object v0

    .line 2473635
    :cond_0
    sget-object v0, LX/HUm;->NO_VIDEOS_MESSAGE:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2473636
    iget-object v0, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->p:Landroid/view/LayoutInflater;

    const v1, 0x7f0315e2

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2473637
    :cond_1
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v0, :cond_2

    .line 2473638
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0, p1, p2}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2473639
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2473600
    sget-object v0, LX/HUm;->LOADING_FOOTER:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-eq p4, v0, :cond_0

    .line 2473601
    sget-object v0, LX/HUm;->NO_VIDEOS_MESSAGE:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_2

    .line 2473602
    const v0, 0x7f0d312e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2473603
    if-eqz v0, :cond_0

    .line 2473604
    new-instance v1, LX/HUk;

    invoke-direct {v1, p0}, LX/HUk;-><init>(LX/HUl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2473605
    iget-object v1, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->s:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2473606
    :cond_0
    :goto_1
    return-void

    .line 2473607
    :cond_1
    const/16 v1, 0x8

    goto :goto_0

    .line 2473608
    :cond_2
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v0, :cond_0

    .line 2473609
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method public final a(LX/1Cv;)V
    .locals 1

    .prologue
    .line 2473629
    iput-object p1, p0, LX/HUl;->b:LX/1Cv;

    .line 2473630
    const v0, -0x44fc5fd8

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2473631
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2473645
    const/4 v0, 0x0

    .line 2473646
    iget-object v1, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v1, :cond_0

    .line 2473647
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0}, LX/1Cv;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2473648
    :cond_0
    iget-boolean v1, p0, LX/HUl;->c:Z

    if-eqz v1, :cond_1

    .line 2473649
    add-int/lit8 v0, v0, 0x1

    .line 2473650
    :cond_1
    iget-boolean v1, p0, LX/HUl;->c:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HUl;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2473651
    add-int/lit8 v0, v0, 0x1

    .line 2473652
    :cond_2
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2473625
    invoke-virtual {p0}, LX/HUl;->getCount()I

    move-result v1

    invoke-direct {p0}, LX/HUl;->b()I

    move-result v2

    sub-int/2addr v1, v2

    if-lt p1, v1, :cond_1

    .line 2473626
    :cond_0
    :goto_0
    return-object v0

    .line 2473627
    :cond_1
    iget-object v1, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v1, :cond_0

    .line 2473628
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0, p1}, LX/1Cv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2473619
    invoke-virtual {p0}, LX/HUl;->getCount()I

    move-result v0

    invoke-direct {p0}, LX/HUl;->b()I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    .line 2473620
    int-to-long v0, p1

    .line 2473621
    :goto_0
    return-wide v0

    .line 2473622
    :cond_0
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v0, :cond_1

    .line 2473623
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0, p1}, LX/1Cv;->getItemId(I)J

    move-result-wide v0

    goto :goto_0

    .line 2473624
    :cond_1
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2473611
    invoke-virtual {p0}, LX/HUl;->getCount()I

    move-result v0

    invoke-direct {p0}, LX/HUl;->b()I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_1

    .line 2473612
    iget-boolean v0, p0, LX/HUl;->c:Z

    if-eqz v0, :cond_0

    .line 2473613
    sget-object v0, LX/HUm;->LOADING_FOOTER:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    .line 2473614
    :goto_0
    return v0

    .line 2473615
    :cond_0
    sget-object v0, LX/HUm;->NO_VIDEOS_MESSAGE:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2473616
    :cond_1
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    if-eqz v0, :cond_2

    .line 2473617
    iget-object v0, p0, LX/HUl;->b:LX/1Cv;

    invoke-virtual {v0, p1}, LX/1Cv;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2473618
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2473610
    invoke-static {}, LX/HUm;->values()[LX/HUm;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
