.class public final LX/IR5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/IR6;


# direct methods
.method public constructor <init>(LX/IR6;)V
    .locals 0

    .prologue
    .line 2576103
    iput-object p1, p0, LX/IR5;->a:LX/IR6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 2576104
    iget-object v0, p0, LX/IR5;->a:LX/IR6;

    iget-object v1, v0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v0, p0, LX/IR5;->a:LX/IR6;

    iget-object v0, v0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->v:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->NOT_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2576105
    :goto_0
    iput-object v0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->v:Ljava/lang/String;

    .line 2576106
    iget-object v0, p0, LX/IR5;->a:LX/IR6;

    iget-object v0, v0, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v1, p0, LX/IR5;->a:LX/IR6;

    iget-object v1, v1, LX/IR6;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->v:Ljava/lang/String;

    .line 2576107
    const-string v2, "%1$s %2$s %3$s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    aput-object p1, v3, p0

    const/4 p0, 0x1

    iget-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v3, p0

    const/4 p0, 0x2

    iget-object p1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->n:Ljava/lang/String;

    aput-object p1, v3, p0

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2576108
    new-instance p0, LX/4Fw;

    invoke-direct {p0}, LX/4Fw;-><init>()V

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2576109
    const-string p1, "actor_id"

    invoke-virtual {p0, p1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576110
    move-object v2, p0

    .line 2576111
    const-string p0, "client_mutation_id"

    invoke-virtual {v2, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576112
    move-object v2, v2

    .line 2576113
    iget-object v3, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->s:Ljava/lang/String;

    .line 2576114
    const-string p0, "topic_id"

    invoke-virtual {v2, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576115
    move-object v2, v2

    .line 2576116
    const-string v3, "new_subscription_state"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576117
    move-object v2, v2

    .line 2576118
    new-instance v3, LX/9TP;

    invoke-direct {v3}, LX/9TP;-><init>()V

    move-object v3, v3

    .line 2576119
    const-string p0, "input"

    invoke-virtual {v3, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2576120
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2576121
    iget-object v3, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2576122
    new-instance v3, LX/IRA;

    invoke-direct {v3, v0}, LX/IRA;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    iget-object p0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->c:LX/0Tf;

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576123
    const/4 v0, 0x1

    return v0

    .line 2576124
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
