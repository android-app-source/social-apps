.class public LX/ICV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549456
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2549446
    check-cast p1, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;

    .line 2549447
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2549448
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action_type"

    .line 2549449
    iget-object v2, p1, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2549450
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2549451
    new-instance v0, LX/14N;

    const-string v1, "storyGalelrySurveyActions"

    const-string v2, "POST"

    const-string v3, "/me/story_gallery_survey_actions/"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2549452
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2549453
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2549454
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
