.class public LX/IQQ;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/IQv;

.field public d:LX/IQu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2575219
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2575220
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2575186
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2575187
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2575188
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 2575189
    const v1, 0x7f030849

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2575190
    new-instance v1, LX/IQP;

    iget-object v0, p0, LX/IQQ;->c:LX/IQv;

    invoke-direct {v1, v2, v0}, LX/IQP;-><init>(Landroid/view/View;LX/IQv;)V

    .line 2575191
    const v0, 0x7f0d159a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/IQP;->l:Landroid/widget/TextView;

    .line 2575192
    const v0, 0x7f0d159b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LX/IQP;->m:Landroid/widget/ImageView;

    move-object v0, v1

    .line 2575193
    :goto_0
    return-object v0

    .line 2575194
    :cond_0
    const v1, 0x7f030848

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2575195
    new-instance v0, LX/IQN;

    iget-object v2, p0, LX/IQQ;->d:LX/IQu;

    invoke-direct {v0, v1, v2}, LX/IQN;-><init>(Landroid/view/View;LX/IQu;)V

    goto :goto_0
.end method

.method public final a(LX/0Px;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2575215
    iput-object p1, p0, LX/IQQ;->a:LX/0Px;

    .line 2575216
    iput-object p2, p0, LX/IQQ;->b:Ljava/util/Set;

    .line 2575217
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2575218
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2575202
    instance-of v0, p1, LX/IQP;

    if-eqz v0, :cond_0

    .line 2575203
    check-cast p1, LX/IQP;

    .line 2575204
    add-int/lit8 v1, p2, -0x1

    .line 2575205
    iget-object v0, p0, LX/IQQ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    .line 2575206
    iget-object v2, p1, LX/IQP;->l:Landroid/widget/TextView;

    .line 2575207
    iget-object v3, p1, LX/IQP;->m:Landroid/widget/ImageView;

    .line 2575208
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2575209
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2575210
    :goto_0
    iget-object v2, p0, LX/IQQ;->b:Ljava/util/Set;

    iget-object v0, p0, LX/IQQ;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2575211
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2575212
    :cond_0
    :goto_1
    return-void

    .line 2575213
    :cond_1
    const-string v0, ""

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2575214
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2575199
    if-nez p1, :cond_0

    .line 2575200
    const/4 v0, 0x0

    .line 2575201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2575196
    iget-object v0, p0, LX/IQQ;->a:LX/0Px;

    if-nez v0, :cond_0

    .line 2575197
    const/4 v0, 0x1

    .line 2575198
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IQQ;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
