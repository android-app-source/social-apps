.class public final LX/IpO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IpP;


# direct methods
.method public constructor <init>(LX/IpP;)V
    .locals 0

    .prologue
    .line 2612850
    iput-object p1, p0, LX/IpO;->a:LX/IpP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2612851
    iget-object v0, p0, LX/IpO;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->c:LX/03V;

    const-string v1, "OrionRequestMessengerPayLoader"

    const-string v2, "Failed to fetch the theme list"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612852
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2612853
    check-cast p1, Ljava/util/ArrayList;

    .line 2612854
    iget-object v0, p0, LX/IpO;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(Ljava/util/List;)V

    .line 2612855
    return-void
.end method
