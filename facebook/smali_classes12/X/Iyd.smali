.class public LX/Iyd;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;",
        "Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633710
    const-class v0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;

    invoke-direct {p0, p2, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2633711
    iput-object p1, p0, LX/Iyd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633712
    return-void
.end method

.method private static a(Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;LX/1pN;)Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;
    .locals 7

    .prologue
    .line 2633652
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2633653
    new-instance v1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;

    .line 2633654
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2633655
    const-string v2, "invoice_items"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 2633656
    const-string v4, "edges"

    invoke-static {v2, v4}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0lF;

    .line 2633657
    const-string v5, "node"

    invoke-virtual {v2, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0lF;

    .line 2633658
    sget-object p0, LX/IyK;->SEARCH_ITEM:LX/IyK;

    const-string v6, "title"

    invoke-static {v5, v6}, LX/Iyp;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v5}, LX/Iyp;->c(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object p1

    invoke-static {p0, v6, p1}, Lcom/facebook/payments/cart/model/SimpleCartItem;->a(LX/IyK;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)LX/IyS;

    move-result-object p0

    .line 2633659
    const-string v6, "item_id"

    invoke-virtual {v5, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2633660
    iput-object v6, p0, LX/IyS;->d:Ljava/lang/String;

    .line 2633661
    const-string v6, "subtitle"

    invoke-static {v5, v6}, LX/Iyp;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2633662
    iput-object v6, p0, LX/IyS;->e:Ljava/lang/String;

    .line 2633663
    move-object v6, p0

    .line 2633664
    const-string p0, "subsubtitle"

    invoke-static {v5, p0}, LX/Iyp;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2633665
    iput-object p0, v6, LX/IyS;->f:Ljava/lang/String;

    .line 2633666
    move-object v6, v6

    .line 2633667
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p1

    .line 2633668
    const-string p0, "images"

    invoke-static {v5, p0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0lF;

    .line 2633669
    const-string v2, "uri"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2633670
    :cond_0
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    move-object p0, p0

    .line 2633671
    iput-object p0, v6, LX/IyS;->i:LX/0Px;

    .line 2633672
    move-object v6, v6

    .line 2633673
    const-string p0, "quantity_limit"

    invoke-virtual {v5, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    const p0, 0x7fffffff

    invoke-static {v5, p0}, LX/16N;->a(LX/0lF;I)I

    move-result v5

    .line 2633674
    iput v5, v6, LX/IyS;->h:I

    .line 2633675
    move-object v5, v6

    .line 2633676
    invoke-virtual {v5}, LX/IyS;->a()Lcom/facebook/payments/cart/model/SimpleCartItem;

    move-result-object v5

    move-object v2, v5

    .line 2633677
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2633678
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2633679
    invoke-direct {v1, v0}, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;-><init>(LX/0Px;)V

    return-object v1
.end method

.method public static b(LX/0QB;)LX/Iyd;
    .locals 3

    .prologue
    .line 2633708
    new-instance v2, LX/Iyd;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v2, v0, v1}, LX/Iyd;-><init>(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2633709
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2633683
    check-cast p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;

    .line 2633684
    iget-object v0, p0, LX/Iyd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633685
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2633686
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2633687
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "input"

    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    const-string v3, "text"

    iget-object v4, p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v2

    const-string v3, "client"

    iget-object v4, p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;->b:LX/6xh;

    invoke-virtual {v4}, LX/6xh;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v2

    const-string v3, "seller_id"

    iget-object v4, p0, LX/Iyd;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2633688
    iget-object p0, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2633689
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v2

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 2633690
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2633691
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "q"

    const-string v4, "payment_invoice_item_search(<input>) {    invoice_items.first(10) {      edges {        node {          item_id,          title {text},          subtitle {text},          subsubtitle {text},          merchant_name,          quantity_limit,          price_list {            label {text},            currency_amount {amount, currency},            user_facing_reason {text}          },          images.width(100).height(100) {uri}        }      }    }}"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633692
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "query_params"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2633693
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-class v2, LX/Iyd;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 2633694
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2633695
    move-object v0, v0

    .line 2633696
    const-string v2, "POST"

    .line 2633697
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2633698
    move-object v0, v0

    .line 2633699
    const-string v2, "graphql"

    .line 2633700
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 2633701
    move-object v0, v0

    .line 2633702
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2633703
    move-object v0, v0

    .line 2633704
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2633705
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2633706
    move-object v0, v0

    .line 2633707
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2633682
    check-cast p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;

    invoke-static {p1, p2}, LX/Iyd;->a(Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;LX/1pN;)Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2633681
    check-cast p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;

    invoke-static {p1, p2}, LX/Iyd;->a(Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchParams;LX/1pN;)Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2633680
    const-string v0, "invoice_item_search"

    return-object v0
.end method
