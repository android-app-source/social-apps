.class public LX/IAg;
.super LX/A8X;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/events/graphql/EventFriendsGraphQLInterfaces$BasicEventGuest;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/LayoutInflater;

.field private final c:LX/Blc;

.field public final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/Blc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/events/graphql/EventFriendsGraphQLInterfaces$BasicEventGuest;",
            ">;",
            "LX/Blc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2546029
    invoke-direct {p0}, LX/A8X;-><init>()V

    .line 2546030
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/IAg;->d:Ljava/util/HashSet;

    .line 2546031
    iput-object p2, p0, LX/IAg;->a:LX/0Px;

    .line 2546032
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/IAg;->b:Landroid/view/LayoutInflater;

    .line 2546033
    iput-object p3, p0, LX/IAg;->c:LX/Blc;

    .line 2546034
    return-void
.end method

.method private static a(LX/Blc;)I
    .locals 2

    .prologue
    .line 2546023
    sget-object v0, LX/IAf;->a:[I

    invoke-virtual {p0}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2546024
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown or non-public guest list type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2546025
    :pswitch_0
    const v0, 0x7f081f6b

    .line 2546026
    :goto_0
    return v0

    .line 2546027
    :pswitch_1
    const v0, 0x7f081f6c

    goto :goto_0

    .line 2546028
    :pswitch_2
    const v0, 0x7f081f6d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static f(I)I
    .locals 2

    .prologue
    .line 2546019
    packed-switch p0, :pswitch_data_0

    .line 2546020
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2546021
    :pswitch_0
    const v0, 0x7f0304dc

    .line 2546022
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0304dd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2546007
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2546008
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2546009
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    .line 2546010
    iget-object v0, p0, LX/IAg;->c:LX/Blc;

    invoke-static {v0}, LX/IAg;->a(LX/Blc;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2546011
    :goto_0
    return-void

    .line 2546012
    :pswitch_1
    check-cast p1, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2546013
    iget-object v0, p0, LX/IAg;->a:LX/0Px;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;

    .line 2546014
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2546015
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;->d()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2546016
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;->d()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2546017
    :goto_1
    iget-object v1, p0, LX/IAg;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventFriendsGraphQLModels$BasicEventGuestModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    goto :goto_0

    .line 2546018
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2545997
    iget-object v0, p0, LX/IAg;->b:Landroid/view/LayoutInflater;

    invoke-static {p2}, LX/IAg;->f(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2546005
    iget-object v0, p0, LX/IAg;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2546006
    if-lez v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2546002
    if-nez p1, :cond_0

    .line 2546003
    iget-object v0, p0, LX/IAg;->c:LX/Blc;

    .line 2546004
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IAg;->a:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2545999
    if-nez p1, :cond_0

    .line 2546000
    const/4 v0, 0x1

    .line 2546001
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2545998
    const/4 v0, 0x2

    return v0
.end method
