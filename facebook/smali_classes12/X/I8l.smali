.class public LX/I8l;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I8k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2541987
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2541988
    return-void
.end method


# virtual methods
.method public final a(LX/I5f;LX/I7i;LX/DBC;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/permalink/EventPermalinkFragment;Landroid/content/Context;LX/E93;LX/I91;LX/I98;LX/1P0;)LX/I8k;
    .locals 27

    .prologue
    .line 2541989
    new-instance v1, LX/I8k;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    const-class v2, LX/I89;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/I89;

    invoke-static/range {p0 .. p0}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v14

    check-cast v14, LX/1DS;

    const/16 v2, 0x1b81

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v2, 0x525

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object v17

    check-cast v17, LX/0kv;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v18

    check-cast v18, LX/1Db;

    const-class v2, LX/IBJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/IBJ;

    invoke-static/range {p0 .. p0}, LX/I8T;->a(LX/0QB;)LX/I8T;

    move-result-object v20

    check-cast v20, LX/I8T;

    invoke-static/range {p0 .. p0}, LX/I8u;->a(LX/0QB;)LX/I8u;

    move-result-object v21

    check-cast v21, LX/I8u;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v22

    check-cast v22, LX/0So;

    invoke-static/range {p0 .. p0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v23

    check-cast v23, LX/1vi;

    invoke-static/range {p0 .. p0}, LX/IBk;->a(LX/0QB;)LX/IBk;

    move-result-object v24

    check-cast v24, LX/IBk;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v26

    check-cast v26, LX/0Uh;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v26}, LX/I8k;-><init>(LX/I5f;LX/I7i;LX/DBC;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/permalink/EventPermalinkFragment;Landroid/content/Context;LX/E93;LX/I91;LX/I98;LX/1P0;LX/0Zb;LX/I89;LX/1DS;LX/0Ot;LX/0Ot;LX/0kv;LX/1Db;LX/IBJ;LX/I8T;LX/I8u;LX/0So;LX/1vi;LX/IBk;LX/0ad;LX/0Uh;)V

    .line 2541990
    return-object v1
.end method
