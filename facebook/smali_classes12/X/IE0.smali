.class public final enum LX/IE0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IE0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IE0;

.field public static final enum DISCOVERY_ENTRY_POINT:LX/IE0;

.field public static final enum FRIEND:LX/IE0;

.field public static final enum HEADER:LX/IE0;

.field public static final VALUES:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IE0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2551379
    new-instance v0, LX/IE0;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v2}, LX/IE0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IE0;->FRIEND:LX/IE0;

    .line 2551380
    new-instance v0, LX/IE0;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v3}, LX/IE0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IE0;->HEADER:LX/IE0;

    .line 2551381
    new-instance v0, LX/IE0;

    const-string v1, "DISCOVERY_ENTRY_POINT"

    invoke-direct {v0, v1, v4}, LX/IE0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IE0;->DISCOVERY_ENTRY_POINT:LX/IE0;

    .line 2551382
    const/4 v0, 0x3

    new-array v0, v0, [LX/IE0;

    sget-object v1, LX/IE0;->FRIEND:LX/IE0;

    aput-object v1, v0, v2

    sget-object v1, LX/IE0;->HEADER:LX/IE0;

    aput-object v1, v0, v3

    sget-object v1, LX/IE0;->DISCOVERY_ENTRY_POINT:LX/IE0;

    aput-object v1, v0, v4

    sput-object v0, LX/IE0;->$VALUES:[LX/IE0;

    .line 2551383
    invoke-static {}, LX/IE0;->values()[LX/IE0;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IE0;->VALUES:LX/0Px;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2551384
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IE0;
    .locals 1

    .prologue
    .line 2551385
    const-class v0, LX/IE0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IE0;

    return-object v0
.end method

.method public static values()[LX/IE0;
    .locals 1

    .prologue
    .line 2551386
    sget-object v0, LX/IE0;->$VALUES:[LX/IE0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IE0;

    return-object v0
.end method
