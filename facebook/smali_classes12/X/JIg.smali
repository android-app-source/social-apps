.class public LX/JIg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EpB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/EpB",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLInterfaces$DiscoveryCardActionFields;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JIg;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EpH;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EpG;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JJ1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678264
    return-void
.end method

.method public static a(LX/0QB;)LX/JIg;
    .locals 6

    .prologue
    .line 2678266
    sget-object v0, LX/JIg;->d:LX/JIg;

    if-nez v0, :cond_1

    .line 2678267
    const-class v1, LX/JIg;

    monitor-enter v1

    .line 2678268
    :try_start_0
    sget-object v0, LX/JIg;->d:LX/JIg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678269
    if-eqz v2, :cond_0

    .line 2678270
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2678271
    new-instance v3, LX/JIg;

    invoke-direct {v3}, LX/JIg;-><init>()V

    .line 2678272
    const/16 v4, 0x1adc

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1adb

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x1ad6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678273
    iput-object v4, v3, LX/JIg;->a:LX/0Or;

    iput-object v5, v3, LX/JIg;->b:LX/0Or;

    iput-object p0, v3, LX/JIg;->c:LX/0Or;

    .line 2678274
    move-object v0, v3

    .line 2678275
    sput-object v0, LX/JIg;->d:LX/JIg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678276
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678277
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678278
    :cond_1
    sget-object v0, LX/JIg;->d:LX/JIg;

    return-object v0

    .line 2678279
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V
    .locals 7
    .param p3    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2678281
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2678282
    iget-object v0, p0, LX/JIg;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EpG;

    move-object v1, p1

    move v2, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 2678283
    invoke-virtual/range {v0 .. v6}, LX/EpG;->a(LX/5wN;ILandroid/content/Context;LX/2h7;LX/5P2;LX/EpL;)Z

    move-result v0

    .line 2678284
    if-eqz v0, :cond_1

    .line 2678285
    :cond_0
    :goto_0
    return-void

    .line 2678286
    :cond_1
    iget-object v0, p0, LX/JIg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EpH;

    .line 2678287
    invoke-virtual {v0, p1, p3, v3}, LX/EpH;->a(LX/5wO;ILandroid/content/Context;)Z

    move-result v0

    .line 2678288
    if-nez v0, :cond_0

    .line 2678289
    invoke-static {p1, p3, v3}, LX/EpC;->a(LX/5wL;ILandroid/content/Context;)Z

    move-result v0

    .line 2678290
    if-nez v0, :cond_0

    .line 2678291
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    .line 2678292
    :pswitch_0
    iget-object v0, p0, LX/JIg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJ1;

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SAVE"

    invoke-virtual {v0, v1, v2}, LX/JJ1;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2678293
    :pswitch_1
    iget-object v0, p0, LX/JIg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJ1;

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UNSAVE"

    invoke-virtual {v0, v1, v2}, LX/JJ1;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V
    .locals 7
    .param p3    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2678265
    move-object v1, p1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/JIg;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;Landroid/view/View;ILX/2h7;LX/5P2;LX/EpL;)V

    return-void
.end method
