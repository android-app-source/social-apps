.class public final LX/I6O;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/I6P;


# direct methods
.method public constructor <init>(LX/I6P;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 2537524
    iput-object p1, p0, LX/I6O;->b:LX/I6P;

    iput-object p2, p0, LX/I6O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    .line 2537520
    iget-object v0, p0, LX/I6O;->b:LX/I6P;

    iget-object v0, v0, LX/I6P;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081057

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2537521
    iget-object v0, p0, LX/I6O;->b:LX/I6P;

    iget-object v6, v0, LX/I6P;->b:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/I6O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/I6O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v5, p0, LX/I6O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2537522
    iget-object v0, p0, LX/I6O;->b:LX/I6P;

    iget-object v0, v0, LX/I6P;->b:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2537523
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2537519
    return-void
.end method
