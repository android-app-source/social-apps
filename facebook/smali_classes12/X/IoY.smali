.class public LX/IoY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/J1k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2611995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611996
    iput-object p1, p0, LX/IoY;->b:Landroid/content/Context;

    .line 2611997
    return-void
.end method

.method private a(Landroid/view/MenuItem;Z)V
    .locals 4

    .prologue
    .line 2611983
    if-eqz p2, :cond_0

    .line 2611984
    const/4 p2, 0x0

    .line 2611985
    const p0, 0x7f082c7f

    invoke-interface {p1, p0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2611986
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2611987
    invoke-static {p1, p2}, LX/3rl;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 2611988
    :goto_0
    return-void

    .line 2611989
    :cond_0
    const v0, 0x7f082c7e

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2611990
    const v0, 0x7f021003

    .line 2611991
    iget-object v1, p0, LX/IoY;->b:Landroid/content/Context;

    const v2, 0x7f0e0574

    iget-object v3, p0, LX/IoY;->b:Landroid/content/Context;

    const p2, 0x7f0a0190

    invoke-static {v3, p2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v1, v2, v0, v3}, LX/Hp3;->a(Landroid/content/Context;III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v0, v1

    .line 2611992
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2611993
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/3rl;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 2611994
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IoY;
    .locals 2

    .prologue
    .line 2611958
    new-instance v1, LX/IoY;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/IoY;-><init>(Landroid/content/Context;)V

    .line 2611959
    invoke-static {p0}, LX/J1k;->b(LX/0QB;)LX/J1k;

    move-result-object v0

    check-cast v0, LX/J1k;

    .line 2611960
    iput-object v0, v1, LX/IoY;->a:LX/J1k;

    .line 2611961
    return-object v1
.end method

.method private static c(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2611979
    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2611980
    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2611981
    const v0, 0x7f030022

    invoke-static {p0, v0}, LX/3rl;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 2611982
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;LX/0am;Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;LX/IoT;Z)V
    .locals 3
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0am;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;",
            "Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;",
            "LX/IoT;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2611962
    if-nez p1, :cond_0

    .line 2611963
    :goto_0
    return-void

    .line 2611964
    :cond_0
    if-nez p5, :cond_1

    if-nez p2, :cond_1

    .line 2611965
    const/4 v0, 0x0

    .line 2611966
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2611967
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2611968
    invoke-static {p1, v0}, LX/3rl;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 2611969
    goto :goto_0

    .line 2611970
    :cond_1
    if-nez p5, :cond_2

    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2611971
    :goto_1
    iget-object v1, p0, LX/IoY;->a:LX/J1k;

    invoke-virtual {v1, p3}, LX/J1k;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)Z

    move-result v1

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2611972
    sget-object v1, LX/IoX;->a:[I

    invoke-virtual {p4}, LX/IoT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2611973
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state found + "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611974
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2611975
    :pswitch_0
    invoke-direct {p0, p1, v0}, LX/IoY;->a(Landroid/view/MenuItem;Z)V

    goto :goto_0

    .line 2611976
    :pswitch_1
    invoke-static {p1}, LX/IoY;->c(Landroid/view/MenuItem;)V

    goto :goto_0

    .line 2611977
    :pswitch_2
    invoke-direct {p0, p1, v0}, LX/IoY;->a(Landroid/view/MenuItem;Z)V

    goto :goto_0

    .line 2611978
    :pswitch_3
    invoke-static {p1}, LX/IoY;->c(Landroid/view/MenuItem;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
