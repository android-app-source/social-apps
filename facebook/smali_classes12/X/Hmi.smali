.class public LX/Hmi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/pm/PackageManager;

.field public final b:LX/Hmj;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Hmh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;LX/Hmj;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2501012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501013
    iput-object p1, p0, LX/Hmi;->a:Landroid/content/pm/PackageManager;

    .line 2501014
    iput-object p2, p0, LX/Hmi;->b:LX/Hmj;

    .line 2501015
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Hmi;->c:Ljava/util/List;

    .line 2501016
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2501007
    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2501008
    array-length v2, v1

    if-gtz v2, :cond_0

    .line 2501009
    :goto_0
    return v0

    .line 2501010
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 2501011
    :catch_0
    goto :goto_0
.end method

.method public static b(LX/Hmi;Lcom/facebook/beam/protocol/BeamPreflightInfo;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    .line 2501017
    iget-object v0, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v2, v0

    .line 2501018
    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2501019
    :goto_0
    return v0

    .line 2501020
    :cond_1
    iget-object v0, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v0, v0

    .line 2501021
    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    if-nez v3, :cond_3

    :cond_2
    move v0, v1

    .line 2501022
    goto :goto_0

    .line 2501023
    :cond_3
    iget-object v0, v0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    invoke-static {v0}, LX/Hmi;->a(Ljava/lang/String;)I

    move-result v3

    .line 2501024
    if-nez v3, :cond_4

    move v0, v1

    .line 2501025
    goto :goto_0

    .line 2501026
    :cond_4
    iget-object v0, p0, LX/Hmi;->b:LX/Hmj;

    .line 2501027
    iget-object v7, v0, LX/Hmj;->a:LX/0W3;

    sget-wide v9, LX/0X5;->iu:J

    invoke-interface {v7, v9, v10}, LX/0W4;->e(J)Ljava/lang/String;

    move-result-object v7

    move-object v0, v7

    .line 2501028
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2501029
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 2501030
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2501031
    :cond_5
    move-object v0, v4

    .line 2501032
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hmg;

    .line 2501033
    iget v5, v0, LX/Hmg;->a:I

    if-gt v3, v5, :cond_6

    iget v5, v0, LX/Hmg;->b:F

    iget-object v6, v2, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_6

    iget-object v5, v2, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget v0, v0, LX/Hmg;->c:F

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_6

    move v0, v1

    .line 2501034
    goto :goto_0

    .line 2501035
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 2501036
    :cond_8
    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_5

    aget-object v8, v6, v5

    .line 2501037
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2501038
    new-instance v10, LX/Hmg;

    const/4 p0, 0x0

    aget-object p0, v9, p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    const/4 p1, 0x1

    aget-object p1, v9, p1

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    const/4 v0, 0x2

    aget-object v9, v9, v0

    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-direct {v10, p0, p1, v9}, LX/Hmg;-><init>(IFF)V

    move-object v8, v10

    .line 2501039
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501040
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public static d(Lcom/facebook/beam/protocol/BeamPreflightInfo;Lcom/facebook/beam/protocol/BeamPreflightInfo;)Z
    .locals 3

    .prologue
    .line 2500997
    iget-object v0, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v0, v0

    .line 2500998
    iget-object v1, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v1, v1

    .line 2500999
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 2501000
    :cond_0
    const/4 v0, 0x0

    .line 2501001
    :goto_0
    return v0

    :cond_1
    iget-object v0, v0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    iget-object v1, v1, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    const/4 v2, 0x0

    .line 2501002
    invoke-static {v0}, LX/Hmi;->a(Ljava/lang/String;)I

    move-result p0

    .line 2501003
    invoke-static {v1}, LX/Hmi;->a(Ljava/lang/String;)I

    move-result p1

    .line 2501004
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    .line 2501005
    :cond_2
    :goto_1
    move v0, v2

    .line 2501006
    goto :goto_0

    :cond_3
    if-ge p0, p1, :cond_2

    const/4 v2, 0x1

    goto :goto_1
.end method
