.class public final LX/Htk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:LX/Htl;


# direct methods
.method public constructor <init>(LX/Htl;)V
    .locals 0

    .prologue
    .line 2516597
    iput-object p1, p0, LX/Htk;->a:LX/Htl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 1

    .prologue
    .line 2516594
    iget-object v0, p0, LX/Htk;->a:LX/Htl;

    iget-object v0, v0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->bringToFront()V

    .line 2516595
    iget-object v0, p0, LX/Htk;->a:LX/Htl;

    iget-object v0, v0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2516596
    const/4 v0, 0x0

    return v0
.end method
