.class public final LX/IcG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Jgh;

.field public final synthetic b:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/Jgh;)V
    .locals 0

    .prologue
    .line 2595345
    iput-object p1, p0, LX/IcG;->b:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    iput-object p2, p0, LX/IcG;->a:LX/Jgh;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2595346
    iget-object v0, p0, LX/IcG;->b:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->d:LX/03V;

    sget-object v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595347
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2595348
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2595349
    iget-object v0, p0, LX/IcG;->a:LX/Jgh;

    if-eqz v0, :cond_1

    .line 2595350
    if-eqz p1, :cond_0

    .line 2595351
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595352
    if-eqz v0, :cond_0

    .line 2595353
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595354
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2595355
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595356
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2595357
    :cond_0
    iget-object v0, p0, LX/IcG;->b:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->d:LX/03V;

    sget-object v1, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->a:Ljava/lang/String;

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595358
    iget-object v0, p0, LX/IcG;->a:LX/Jgh;

    .line 2595359
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2595360
    invoke-virtual {v0, v1}, LX/Jgh;->a(LX/0Px;)V

    .line 2595361
    :cond_1
    :goto_0
    return-void

    .line 2595362
    :cond_2
    iget-object v1, p0, LX/IcG;->a:LX/Jgh;

    .line 2595363
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595364
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideProvidersQueryModel$MessengerCommerceModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Jgh;->a(LX/0Px;)V

    goto :goto_0
.end method
