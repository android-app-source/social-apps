.class public LX/IBY;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/IBX;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/events/common/EventAnalyticsParams;

.field private e:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2547255
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547256
    const-class p1, LX/IBY;

    invoke-static {p1, p0}, LX/IBY;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2547257
    invoke-virtual {p0, p0}, LX/IBY;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547258
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IBY;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v1

    check-cast v1, LX/1nQ;

    invoke-static {p0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v2

    check-cast v2, LX/IBl;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, LX/IBY;->a:LX/1nQ;

    iput-object v2, p1, LX/IBY;->b:LX/IBl;

    iput-object p0, p1, LX/IBY;->c:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 5
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547247
    iput-object p1, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    .line 2547248
    iput-object p3, p0, LX/IBY;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2547249
    iget-object v0, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    .line 2547250
    iget-object v1, v0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    move-object v0, v1

    .line 2547251
    invoke-virtual {p0}, LX/IBY;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081ed8

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2547252
    iget-object v1, p0, LX/IBY;->b:LX/IBl;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v0, v2, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547253
    iget-object v0, p0, LX/IBY;->b:LX/IBl;

    const v1, 0x7f02033d

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547254
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 2
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547238
    iget-object v0, p1, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    move-object v0, v0

    .line 2547239
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2547240
    iget-object v0, p1, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v0, v0

    .line 2547241
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2547242
    iget-object v0, p1, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v0, v0

    .line 2547243
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2547244
    iget-object v0, p1, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v0, v0

    .line 2547245
    iget-object v1, p1, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v1, v1

    .line 2547246
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x6e75b524

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2547219
    iget-object v1, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    if-eqz v1, :cond_1

    .line 2547220
    iget-object v1, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    .line 2547221
    iget-object v2, v1, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v1, v2

    .line 2547222
    iget-object v2, p0, LX/IBY;->a:LX/1nQ;

    iget-object v3, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    .line 2547223
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2547224
    iget-object v4, p0, LX/IBY;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547225
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2547226
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    iget-object v5, p0, LX/IBY;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547227
    iget-object v8, v5, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v5, v8

    .line 2547228
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v5

    .line 2547229
    iget-object v8, v2, LX/1nQ;->i:LX/0Zb;

    const-string v9, "event_group_summary_click"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v8

    .line 2547230
    invoke-virtual {v8}, LX/0oG;->a()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2547231
    const-string v9, "event_permalink"

    invoke-virtual {v8, v9}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    iget-object v9, v2, LX/1nQ;->j:LX/0kv;

    iget-object v10, v2, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v9, v10}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "Event"

    invoke-virtual {v8, v9}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "action_source"

    invoke-virtual {v8, v9, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v8

    const-string v9, "action_ref"

    invoke-virtual {v8, v9, v5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v8

    const-string v9, "group_id"

    invoke-virtual {v8, v9, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    invoke-virtual {v8}, LX/0oG;->d()V

    .line 2547232
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2547233
    sget-object v2, LX/0ax;->C:Ljava/lang/String;

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/IBY;->e:Lcom/facebook/events/model/Event;

    .line 2547234
    iget-object v7, v5, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v5, v7

    .line 2547235
    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2547236
    iget-object v2, p0, LX/IBY;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2547237
    :cond_1
    const v1, 0x3ff2ec18    # 1.89783f

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
