.class public final LX/Hh1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hh0;


# instance fields
.field public final synthetic a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;)V
    .locals 0

    .prologue
    .line 2494768
    iput-object p1, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2494782
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2494783
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2494784
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494785
    :cond_0
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2494786
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2494787
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494788
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2494789
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494790
    :cond_0
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494791
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2494769
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2494770
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->q:Landroid/view/ViewGroup;

    .line 2494771
    iget-object v1, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v1, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 2494772
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494773
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    const v1, 0x7f0d1108

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2494774
    const v1, 0x7f08373e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2494775
    iget-object v1, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    const v2, 0x7f08373e

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2494776
    iget-object v0, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v0, v0, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2494777
    return-void

    .line 2494778
    :cond_1
    iget-object v2, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    const v1, 0x7f0d08b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2494779
    iput-object v1, v2, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    .line 2494780
    iget-object v1, p0, LX/Hh1;->a:Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;

    iget-object v1, v1, Lcom/facebook/zero/carrier/fragment/CarrierManagerFragment;->x:Landroid/view/View;

    const v2, 0x7f0d1106

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2494781
    new-instance v2, LX/Hgz;

    invoke-direct {v2, p0}, LX/Hgz;-><init>(LX/Hh1;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
