.class public LX/IFX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/IFd;

.field public static final b:LX/IFd;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:J

.field public B:Ljava/lang/String;

.field public final d:LX/0Sh;

.field public final e:Landroid/content/res/Resources;

.field public final f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

.field public final g:LX/IG5;

.field private final h:LX/0Uh;

.field public final i:LX/IFx;

.field private final j:LX/0ad;

.field public final k:LX/IFa;

.field public l:LX/IG2;

.field public m:LX/IG4;

.field public n:LX/IFz;

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/IFd;

.field public q:LX/IFd;

.field public r:LX/IFd;

.field public s:LX/IFd;

.field public t:LX/IFd;

.field public u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IFd;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IFd;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IFW;",
            ">;"
        }
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2554230
    const-string v0, "Pings"

    invoke-static {v0}, LX/IFd;->b(Ljava/lang/String;)LX/IFd;

    move-result-object v0

    sput-object v0, LX/IFX;->a:LX/IFd;

    .line 2554231
    const-string v0, "Outgoing_Pings"

    invoke-static {v0}, LX/IFd;->b(Ljava/lang/String;)LX/IFd;

    move-result-object v0

    sput-object v0, LX/IFX;->b:LX/IFd;

    .line 2554232
    const-class v0, LX/IFX;

    sput-object v0, LX/IFX;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;Landroid/content/res/Resources;Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;LX/IG5;LX/0Uh;LX/IFx;LX/0ad;LX/IFa;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2554500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554501
    new-instance v0, LX/IG2;

    invoke-direct {v0}, LX/IG2;-><init>()V

    iput-object v0, p0, LX/IFX;->l:LX/IG2;

    .line 2554502
    new-instance v0, LX/IG4;

    invoke-direct {v0}, LX/IG4;-><init>()V

    iput-object v0, p0, LX/IFX;->m:LX/IG4;

    .line 2554503
    new-instance v0, LX/IFz;

    invoke-direct {v0}, LX/IFz;-><init>()V

    iput-object v0, p0, LX/IFX;->n:LX/IFz;

    .line 2554504
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554505
    iput-object v0, p0, LX/IFX;->o:LX/0Px;

    .line 2554506
    sget-object v0, LX/IFX;->a:LX/IFd;

    iput-object v0, p0, LX/IFX;->p:LX/IFd;

    .line 2554507
    sget-object v0, LX/IFX;->b:LX/IFd;

    iput-object v0, p0, LX/IFX;->q:LX/IFd;

    .line 2554508
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->r:LX/IFd;

    .line 2554509
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->s:LX/IFd;

    .line 2554510
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->t:LX/IFd;

    .line 2554511
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554512
    iput-object v0, p0, LX/IFX;->u:LX/0Px;

    .line 2554513
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554514
    iput-object v0, p0, LX/IFX;->v:LX/0Px;

    .line 2554515
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/IFX;->w:Ljava/util/Set;

    .line 2554516
    iput-boolean v1, p0, LX/IFX;->x:Z

    .line 2554517
    iput-boolean v1, p0, LX/IFX;->y:Z

    .line 2554518
    iput-boolean v1, p0, LX/IFX;->z:Z

    .line 2554519
    iput-object p1, p0, LX/IFX;->d:LX/0Sh;

    .line 2554520
    iput-object p2, p0, LX/IFX;->e:Landroid/content/res/Resources;

    .line 2554521
    iput-object p3, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    .line 2554522
    iput-object p4, p0, LX/IFX;->g:LX/IG5;

    .line 2554523
    iput-object p5, p0, LX/IFX;->h:LX/0Uh;

    .line 2554524
    iput-object p6, p0, LX/IFX;->i:LX/IFx;

    .line 2554525
    iput-object p7, p0, LX/IFX;->j:LX/0ad;

    .line 2554526
    iput-object p8, p0, LX/IFX;->k:LX/IFa;

    .line 2554527
    return-void
.end method

.method public static a(LX/IFX;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2554488
    iget-object v0, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object v1, p0, LX/IFX;->l:LX/IG2;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(LX/IG2;)LX/IFd;

    move-result-object v0

    iput-object v0, p0, LX/IFX;->q:LX/IFd;

    .line 2554489
    iget-object v0, p0, LX/IFX;->m:LX/IG4;

    .line 2554490
    invoke-virtual {v0, p1}, LX/IG4;->b(Ljava/lang/String;)LX/0Rf;

    move-result-object v1

    .line 2554491
    if-nez v1, :cond_1

    .line 2554492
    :cond_0
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    invoke-static {v0, p1, p2}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Z)V

    .line 2554493
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    invoke-static {v0, p1, p2}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Z)V

    .line 2554494
    iget-object v0, p0, LX/IFX;->s:LX/IFd;

    invoke-static {v0, p1, p2}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Z)V

    .line 2554495
    return-void

    .line 2554496
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IFR;

    .line 2554497
    check-cast v1, LX/IFZ;

    .line 2554498
    iput-boolean p2, v1, LX/IFZ;->a:Z

    .line 2554499
    goto :goto_0
.end method

.method public static b(LX/IFX;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;)V
    .locals 13

    .prologue
    .line 2554429
    iget-object v0, p0, LX/IFX;->g:LX/IG5;

    iget-object v1, p0, LX/IFX;->m:LX/IG4;

    iget-object v2, p0, LX/IFX;->l:LX/IG2;

    invoke-virtual {v2}, LX/IG2;->c()LX/0Rf;

    move-result-object v2

    iget-object v3, p0, LX/IFX;->n:LX/IFz;

    .line 2554430
    const/4 v6, 0x0

    .line 2554431
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;

    move-result-object v4

    .line 2554432
    if-nez v4, :cond_5

    .line 2554433
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2554434
    :goto_0
    move-object v4, v4

    .line 2554435
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;

    .line 2554436
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2554437
    iget-object v7, v0, LX/IG5;->b:LX/IFa;

    .line 2554438
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->b()Ljava/lang/String;

    move-result-object v10

    .line 2554439
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2554440
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;->a()LX/0Px;

    move-result-object v12

    invoke-virtual {v12}, LX/0Px;->size()I

    move-result p0

    const/4 v8, 0x0

    move v9, v8

    :goto_2
    if-ge v9, p0, :cond_1

    invoke-virtual {v12, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2554441
    invoke-virtual {v8}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {v8, v10, p1, v7}, LX/IFZ;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;

    move-result-object v8

    .line 2554442
    if-eqz v8, :cond_0

    .line 2554443
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554444
    :cond_0
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_2

    .line 2554445
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$TitleModel;

    move-result-object v8

    .line 2554446
    if-nez v8, :cond_b

    .line 2554447
    const-string v8, ""

    .line 2554448
    :goto_3
    new-instance v9, LX/IFe;

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v11

    invoke-direct {v9, v8, v10, v11}, LX/IFe;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    move-object v7, v9

    .line 2554449
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;->b()LX/0us;

    move-result-object v8

    .line 2554450
    if-eqz v8, :cond_2

    .line 2554451
    invoke-interface {v8}, LX/0us;->a()Ljava/lang/String;

    move-result-object v8

    .line 2554452
    iput-object v8, v7, LX/IFd;->b:Ljava/lang/String;

    .line 2554453
    :cond_2
    invoke-virtual {v1, v6, v7}, LX/IG4;->a(Ljava/lang/String;LX/IFd;)V

    .line 2554454
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v8

    .line 2554455
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v6, 0x0

    move v7, v6

    :goto_4
    if-ge v7, v10, :cond_3

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;

    .line 2554456
    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->b()Ljava/lang/String;

    move-result-object v11

    .line 2554457
    new-instance v12, LX/0XI;

    invoke-direct {v12}, LX/0XI;-><init>()V

    sget-object p0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v12, p0, v11}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v12

    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel;->d()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListItemModel$ContactModel$RepresentedProfileModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 2554458
    iput-object v6, v12, LX/0XI;->h:Ljava/lang/String;

    .line 2554459
    move-object v6, v12

    .line 2554460
    invoke-virtual {v6}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v6

    .line 2554461
    invoke-interface {v8, v11, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2554462
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_4

    .line 2554463
    :cond_3
    move-object v4, v8

    .line 2554464
    invoke-virtual {v3, v4}, LX/IFz;->a(Ljava/util/Map;)V

    goto/16 :goto_1

    .line 2554465
    :cond_4
    return-void

    .line 2554466
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2554467
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2554468
    goto/16 :goto_0

    .line 2554469
    :cond_6
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;

    .line 2554470
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel$ContactsTabsModel$NodesModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;

    move-result-object v4

    .line 2554471
    if-nez v4, :cond_7

    .line 2554472
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2554473
    goto/16 :goto_0

    .line 2554474
    :cond_7
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2554475
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbySectionsPageFieldsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v7, v6

    :goto_5
    if-ge v7, v10, :cond_a

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewSectionWrapperModel;

    .line 2554476
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewSectionWrapperModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewSectionWrapperModel$ContactsSetsModel;

    move-result-object v4

    .line 2554477
    if-eqz v4, :cond_9

    .line 2554478
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewSectionWrapperModel$ContactsSetsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v5, v6

    :goto_6
    if-ge v5, v12, :cond_9

    invoke-virtual {v11, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;

    .line 2554479
    if-eqz v4, :cond_8

    .line 2554480
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel;->c()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;

    move-result-object p0

    .line 2554481
    if-eqz p0, :cond_8

    .line 2554482
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$SetItemsModel;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_8

    .line 2554483
    invoke-virtual {v8, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554484
    :cond_8
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_6

    .line 2554485
    :cond_9
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_5

    .line 2554486
    :cond_a
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_0

    .line 2554487
    :cond_b
    invoke-virtual {v8}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyNewListSectionModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_3
.end method

.method public static c(LX/IFX;LX/IFN;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2554369
    iget-object v0, p1, LX/IFN;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2554370
    iget-object v0, p1, LX/IFN;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    .line 2554371
    iget-object v2, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object v3, p0, LX/IFX;->l:LX/IG2;

    iget-object v4, p0, LX/IFX;->n:LX/IFz;

    invoke-virtual {v2, v3, v4, v0}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(LX/IG2;LX/IFz;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/IG2;

    .line 2554372
    iget-object v2, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object v3, p0, LX/IFX;->l:LX/IG2;

    invoke-virtual {v2, v3}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(LX/IG2;)LX/IFd;

    move-result-object v2

    iput-object v2, p0, LX/IFX;->q:LX/IFd;

    .line 2554373
    iget-object v2, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object v3, p0, LX/IFX;->l:LX/IG2;

    .line 2554374
    iget-object v4, v3, LX/IG2;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    const/4 v4, 0x1

    :goto_0
    move v4, v4

    .line 2554375
    if-nez v4, :cond_9

    .line 2554376
    sget-object v4, LX/IFX;->a:LX/IFd;

    .line 2554377
    :goto_1
    move-object v2, v4

    .line 2554378
    iput-object v2, p0, LX/IFX;->p:LX/IFd;

    .line 2554379
    iget-object v3, p0, LX/IFX;->m:LX/IG4;

    iget-object v4, p0, LX/IFX;->l:LX/IG2;

    .line 2554380
    invoke-virtual {v3}, LX/IG4;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v0, :cond_0

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IFd;

    .line 2554381
    invoke-static {v2, v4}, LX/IG5;->a(LX/IFd;LX/IG2;)V

    .line 2554382
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 2554383
    :cond_0
    iget-object v2, p0, LX/IFX;->r:LX/IFd;

    iget-object v3, p0, LX/IFX;->l:LX/IG2;

    invoke-static {v2, v3}, LX/IG5;->a(LX/IFd;LX/IG2;)V

    .line 2554384
    iget-object v2, p0, LX/IFX;->s:LX/IFd;

    iget-object v3, p0, LX/IFX;->l:LX/IG2;

    invoke-static {v2, v3}, LX/IG5;->a(LX/IFd;LX/IG2;)V

    .line 2554385
    iget-object v2, p0, LX/IFX;->i:LX/IFx;

    iget-object v0, p1, LX/IFN;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_8

    move v0, v1

    .line 2554386
    :goto_3
    iput-boolean v0, v2, LX/IFx;->e:Z

    .line 2554387
    iget-object v2, p0, LX/IFX;->i:LX/IFx;

    iget-object v0, p1, LX/IFN;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->mG_()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;

    move-result-object v0

    .line 2554388
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$RegionObjectModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2554389
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$NearbyFriendsRegionModel$DisplayNameModel;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/IFx;->f:Ljava/lang/String;

    .line 2554390
    :cond_1
    iput-boolean v1, p0, LX/IFX;->x:Z

    .line 2554391
    :cond_2
    iget-object v0, p1, LX/IFN;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2554392
    iget-object v0, p1, LX/IFN;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;

    invoke-static {p0, v0}, LX/IFX;->b(LX/IFX;Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyContactsTabModel;)V

    .line 2554393
    :cond_3
    iget-object v0, p1, LX/IFN;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2554394
    iget-object v1, p0, LX/IFX;->i:LX/IFx;

    iget-object v0, p1, LX/IFN;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;

    .line 2554395
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/IFx;->b:Ljava/lang/String;

    .line 2554396
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->d()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2554397
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v1, LX/IFx;->c:Landroid/net/Uri;

    .line 2554398
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyViewerInfoModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/IFx;->d:Ljava/lang/String;

    .line 2554399
    :cond_5
    iget-object v0, p1, LX/IFN;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2554400
    iget-object v0, p1, LX/IFN;->e:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2554401
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2554402
    sget-object v1, LX/IFd;->c:LX/IFd;

    iput-object v1, p0, LX/IFX;->s:LX/IFd;

    .line 2554403
    :cond_6
    :goto_4
    iget-object v0, p0, LX/IFX;->i:LX/IFx;

    .line 2554404
    iget-object v1, v0, LX/IFx;->b:Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, v0, LX/IFx;->c:Landroid/net/Uri;

    if-eqz v1, :cond_f

    iget-object v1, v0, LX/IFx;->d:Ljava/lang/String;

    if-eqz v1, :cond_f

    iget-object v1, v0, LX/IFx;->f:Ljava/lang/String;

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_5
    move v0, v1

    .line 2554405
    if-eqz v0, :cond_7

    .line 2554406
    iget-object v0, p0, LX/IFX;->i:LX/IFx;

    iget-object v1, p0, LX/IFX;->e:Landroid/content/res/Resources;

    const v2, 0x7f083858

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "friends_nearby_self_view_section"

    .line 2554407
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2554408
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554409
    new-instance v4, LX/IFe;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v4, v1, v2, v3}, LX/IFe;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    move-object v0, v4

    .line 2554410
    iput-object v0, p0, LX/IFX;->t:LX/IFd;

    .line 2554411
    :cond_7
    return-void

    .line 2554412
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_9
    const-string v4, "Pings"

    invoke-virtual {v2, v3}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c(LX/IG2;)Ljava/lang/String;

    move-result-object v5

    .line 2554413
    iget-object v0, v3, LX/IG2;->c:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2554414
    new-instance v2, LX/IFe;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v5, v4, v3}, LX/IFe;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    move-object v4, v2

    .line 2554415
    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 2554416
    :cond_b
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2554417
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_6
    if-ge v2, v4, :cond_c

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    .line 2554418
    invoke-virtual {v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554419
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    .line 2554420
    :cond_c
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/IFX;->o:LX/0Px;

    .line 2554421
    iget-object v1, p0, LX/IFX;->e:Landroid/content/res/Resources;

    const v2, 0x7f083857

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "friends_nearby_highlight_section"

    invoke-virtual {p0}, LX/IFX;->d()LX/0Rf;

    move-result-object v3

    iget-object v4, p0, LX/IFX;->k:LX/IFa;

    .line 2554422
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2554423
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    .line 2554424
    invoke-virtual {v5}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {v5, v2, p1, v4}, LX/IFZ;->a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;Ljava/lang/String;ZLX/IFa;)LX/IFZ;

    move-result-object v5

    .line 2554425
    if-eqz v5, :cond_d

    .line 2554426
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 2554427
    :cond_e
    new-instance v5, LX/IFe;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-direct {v5, v1, v2, v6}, LX/IFe;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    move-object v1, v5

    .line 2554428
    iput-object v1, p0, LX/IFX;->s:LX/IFd;

    goto/16 :goto_4

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_5
.end method

.method public static g(LX/IFX;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2554366
    iget-object v0, p0, LX/IFX;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFW;

    .line 2554367
    invoke-interface {v0, p1}, LX/IFW;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2554368
    :cond_0
    return-void
.end method

.method public static o(LX/IFX;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2554350
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2554351
    iget-object v1, p0, LX/IFX;->s:LX/IFd;

    sget-object v3, LX/IFd;->c:LX/IFd;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, LX/IFX;->s:LX/IFd;

    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2554352
    iget-object v1, p0, LX/IFX;->s:LX/IFd;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554353
    :cond_0
    iget-object v1, p0, LX/IFX;->h:LX/0Uh;

    const/16 v3, 0x587

    invoke-virtual {v1, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/IFX;->t:LX/IFd;

    sget-object v3, LX/IFd;->c:LX/IFd;

    if-eq v1, v3, :cond_1

    .line 2554354
    iget-object v1, p0, LX/IFX;->t:LX/IFd;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554355
    :cond_1
    iget-object v1, p0, LX/IFX;->m:LX/IG4;

    invoke-virtual {v1}, LX/IG4;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFd;

    .line 2554356
    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2554357
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554358
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2554359
    :cond_3
    iget-boolean v0, p0, LX/IFX;->z:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2554360
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554361
    :cond_4
    iget-object v0, p0, LX/IFX;->q:LX/IFd;

    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, LX/IFX;->y:Z

    if-eqz v0, :cond_5

    .line 2554362
    iget-object v0, p0, LX/IFX;->q:LX/IFd;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554363
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IFX;->u:LX/0Px;

    .line 2554364
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IFX;->v:LX/0Px;

    .line 2554365
    return-void
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 2554349
    iget-object v0, p0, LX/IFX;->u:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(LX/IFX;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2554327
    iput-boolean v0, p0, LX/IFX;->x:Z

    .line 2554328
    iput-boolean v0, p0, LX/IFX;->y:Z

    .line 2554329
    iget-object v0, p0, LX/IFX;->i:LX/IFx;

    const/4 v1, 0x0

    .line 2554330
    iput-object v1, v0, LX/IFx;->b:Ljava/lang/String;

    .line 2554331
    iput-object v1, v0, LX/IFx;->c:Landroid/net/Uri;

    .line 2554332
    iput-object v1, v0, LX/IFx;->d:Ljava/lang/String;

    .line 2554333
    iput-object v1, v0, LX/IFx;->f:Ljava/lang/String;

    .line 2554334
    new-instance v0, LX/IG2;

    invoke-direct {v0}, LX/IG2;-><init>()V

    iput-object v0, p0, LX/IFX;->l:LX/IG2;

    .line 2554335
    new-instance v0, LX/IG4;

    invoke-direct {v0}, LX/IG4;-><init>()V

    iput-object v0, p0, LX/IFX;->m:LX/IG4;

    .line 2554336
    new-instance v0, LX/IFz;

    invoke-direct {v0}, LX/IFz;-><init>()V

    iput-object v0, p0, LX/IFX;->n:LX/IFz;

    .line 2554337
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554338
    iput-object v0, p0, LX/IFX;->u:LX/0Px;

    .line 2554339
    sget-object v0, LX/IFX;->b:LX/IFd;

    iput-object v0, p0, LX/IFX;->q:LX/IFd;

    .line 2554340
    sget-object v0, LX/IFX;->a:LX/IFd;

    iput-object v0, p0, LX/IFX;->p:LX/IFd;

    .line 2554341
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->r:LX/IFd;

    .line 2554342
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554343
    iput-object v0, p0, LX/IFX;->v:LX/0Px;

    .line 2554344
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->s:LX/IFd;

    .line 2554345
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554346
    iput-object v0, p0, LX/IFX;->o:LX/0Px;

    .line 2554347
    sget-object v0, LX/IFd;->c:LX/IFd;

    iput-object v0, p0, LX/IFX;->t:LX/IFd;

    .line 2554348
    return-void
.end method

.method public static t(LX/IFX;)V
    .locals 2

    .prologue
    .line 2554324
    iget-object v0, p0, LX/IFX;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFW;

    .line 2554325
    invoke-interface {v0}, LX/IFW;->b()V

    goto :goto_0

    .line 2554326
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554316
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554317
    iget-object v0, p0, LX/IFX;->l:LX/IG2;

    .line 2554318
    iget-object p0, v0, LX/IG2;->b:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2554319
    const/4 p0, 0x0

    .line 2554320
    :goto_0
    move-object v0, p0

    .line 2554321
    return-object v0

    :cond_0
    iget-object p0, v0, LX/IG2;->b:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/IFf;

    .line 2554322
    iget-object v0, p0, LX/IFf;->d:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    move-object p0, v0

    .line 2554323
    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2554309
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554310
    invoke-direct {p0}, LX/IFX;->q()Z

    move-result v0

    .line 2554311
    invoke-static {p0}, LX/IFX;->r(LX/IFX;)V

    .line 2554312
    if-eqz v0, :cond_0

    .line 2554313
    iget-object v0, p0, LX/IFX;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFW;

    .line 2554314
    invoke-interface {v0}, LX/IFW;->a()V

    goto :goto_0

    .line 2554315
    :cond_0
    return-void
.end method

.method public final a(LX/IFW;)V
    .locals 1

    .prologue
    .line 2554528
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554529
    iget-object v0, p0, LX/IFX;->w:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2554530
    invoke-direct {p0}, LX/IFX;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554531
    invoke-interface {p1}, LX/IFW;->b()V

    .line 2554532
    :cond_0
    return-void
.end method

.method public final a(LX/IFd;)V
    .locals 2

    .prologue
    .line 2554302
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554303
    iput-object p1, p0, LX/IFX;->r:LX/IFd;

    .line 2554304
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IFX;->v:LX/0Px;

    .line 2554305
    sget-object v0, LX/IFd;->c:LX/IFd;

    if-eq p1, v0, :cond_0

    .line 2554306
    iget-object v0, p0, LX/IFX;->n:LX/IFz;

    invoke-static {p1}, LX/IG5;->a(LX/IFd;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IFz;->a(Ljava/util/Map;)V

    .line 2554307
    :cond_0
    invoke-static {p0}, LX/IFX;->t(LX/IFX;)V

    .line 2554308
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V
    .locals 7

    .prologue
    .line 2554275
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554276
    iget-object v0, p0, LX/IFX;->m:LX/IG4;

    .line 2554277
    iget-object v1, v0, LX/IG4;->b:LX/IG0;

    invoke-virtual {v1, p1}, LX/IG0;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2554278
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2554279
    :cond_0
    const/4 v1, 0x0

    .line 2554280
    :goto_0
    move-object v0, v1

    .line 2554281
    if-nez v0, :cond_1

    .line 2554282
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    invoke-static {v0, p1}, LX/IG5;->a(LX/IFd;Ljava/lang/String;)LX/IFR;

    move-result-object v0

    .line 2554283
    :cond_1
    if-nez v0, :cond_2

    .line 2554284
    iget-object v0, p0, LX/IFX;->s:LX/IFd;

    invoke-static {v0, p1}, LX/IG5;->a(LX/IFd;Ljava/lang/String;)LX/IFR;

    move-result-object v0

    .line 2554285
    :cond_2
    if-nez v0, :cond_3

    .line 2554286
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    invoke-static {v0, p1}, LX/IG5;->a(LX/IFd;Ljava/lang/String;)LX/IFR;

    move-result-object v0

    .line 2554287
    :cond_3
    if-nez v0, :cond_4

    .line 2554288
    iget-object v0, p0, LX/IFX;->q:LX/IFd;

    invoke-static {v0, p1}, LX/IG5;->a(LX/IFd;Ljava/lang/String;)LX/IFR;

    move-result-object v0

    .line 2554289
    :cond_4
    iget-object v1, p0, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object v2, p0, LX/IFX;->l:LX/IG2;

    .line 2554290
    invoke-virtual {v2, p1}, LX/IG2;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2554291
    iget-object v3, v2, LX/IG2;->b:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IFf;

    move-object v3, v3

    .line 2554292
    iput-object p2, v3, LX/IFf;->d:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2554293
    invoke-static {v1, p2}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)Ljava/lang/String;

    move-result-object v4

    .line 2554294
    iput-object v4, v3, LX/IFf;->e:Ljava/lang/String;

    .line 2554295
    :cond_5
    :goto_1
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/IFX;->a(LX/IFX;Ljava/lang/String;Z)V

    .line 2554296
    invoke-static {p0}, LX/IFX;->o(LX/IFX;)V

    .line 2554297
    invoke-static {p0, p1}, LX/IFX;->g(LX/IFX;Ljava/lang/String;)V

    .line 2554298
    return-void

    :cond_6
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IFR;

    goto :goto_0

    .line 2554299
    :cond_7
    if-eqz v0, :cond_5

    .line 2554300
    invoke-interface {v0}, LX/IFR;->b()Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v0}, LX/IFR;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, p2}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->a(Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Outgoing_Pings"

    invoke-static {p2, v3, v4, v5, v6}, LX/IFf;->a(Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/IFf;

    move-result-object v3

    .line 2554301
    invoke-virtual {v2, p1, v3}, LX/IG2;->a(Ljava/lang/String;LX/IFf;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V
    .locals 6

    .prologue
    .line 2554266
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554267
    iget-object v1, p0, LX/IFX;->m:LX/IG4;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 2554268
    invoke-virtual {v1, v2}, LX/IG4;->b(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    invoke-static {v0, v2, v3, v4, v5}, LX/IG5;->a(Ljava/lang/Iterable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2554269
    iget-object v1, p0, LX/IFX;->p:LX/IFd;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v5}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2554270
    iget-object v1, p0, LX/IFX;->q:LX/IFd;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v5}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2554271
    iget-object v1, p0, LX/IFX;->r:LX/IFd;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v5}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2554272
    iget-object v1, p0, LX/IFX;->s:LX/IFd;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v5}, LX/IG5;->a(LX/IFd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    .line 2554273
    invoke-static {p0, p1}, LX/IFX;->g(LX/IFX;Ljava/lang/String;)V

    .line 2554274
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/IFd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554262
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554263
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    sget-object v1, LX/IFd;->c:LX/IFd;

    if-eq v0, v1, :cond_0

    .line 2554264
    iget-object v0, p0, LX/IFX;->v:LX/0Px;

    .line 2554265
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IFX;->u:LX/0Px;

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/user/model/User;
    .locals 1

    .prologue
    .line 2554258
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554259
    iget-object v0, p0, LX/IFX;->n:LX/IFz;

    .line 2554260
    iget-object p0, v0, LX/IFz;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/user/model/User;

    move-object v0, p0

    .line 2554261
    return-object v0
.end method

.method public final d()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554257
    iget-object v0, p0, LX/IFX;->l:LX/IG2;

    invoke-virtual {v0}, LX/IG2;->c()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/IFd;
    .locals 1

    .prologue
    .line 2554242
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554243
    const-string v0, "Pings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2554244
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    .line 2554245
    :goto_0
    return-object v0

    .line 2554246
    :cond_0
    const-string v0, "Outgoing_Pings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2554247
    iget-object v0, p0, LX/IFX;->q:LX/IFd;

    goto :goto_0

    .line 2554248
    :cond_1
    const-string v0, "friends_nearby_search_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2554249
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    goto :goto_0

    .line 2554250
    :cond_2
    const-string v0, "friends_nearby_highlight_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2554251
    iget-object v0, p0, LX/IFX;->s:LX/IFd;

    goto :goto_0

    .line 2554252
    :cond_3
    const-string v0, "friends_nearby_self_view_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2554253
    iget-object v0, p0, LX/IFX;->t:LX/IFd;

    goto :goto_0

    .line 2554254
    :cond_4
    iget-object v0, p0, LX/IFX;->m:LX/IG4;

    .line 2554255
    iget-object p0, v0, LX/IG4;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {p0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/IFd;

    move-object v0, p0

    .line 2554256
    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)LX/IFb;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554237
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554238
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2554239
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2554240
    check-cast v0, LX/IFb;

    .line 2554241
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/IFd;
    .locals 1

    .prologue
    .line 2554235
    iget-object v0, p0, LX/IFX;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2554236
    iget-object v0, p0, LX/IFX;->p:LX/IFd;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2554234
    iget-object v0, p0, LX/IFX;->r:LX/IFd;

    sget-object v1, LX/IFd;->c:LX/IFd;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2554233
    iget-object v1, p0, LX/IFX;->u:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, LX/IFX;->t:LX/IFd;

    sget-object v2, LX/IFd;->c:LX/IFd;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, LX/IFX;->u:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
