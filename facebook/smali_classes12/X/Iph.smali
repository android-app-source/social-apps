.class public final enum LX/Iph;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iph;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iph;

.field public static final enum BUTTON_LEFT_CONFIRMED:LX/Iph;

.field public static final enum BUTTON_LEFT_SELECTED:LX/Iph;

.field public static final enum BUTTON_RIGHT_CONFIRMED:LX/Iph;

.field public static final enum BUTTON_RIGHT_SELECTED:LX/Iph;

.field public static final enum DISABLED:LX/Iph;

.field public static final enum NORMAL:LX/Iph;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2613090
    new-instance v0, LX/Iph;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->DISABLED:LX/Iph;

    .line 2613091
    new-instance v0, LX/Iph;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->NORMAL:LX/Iph;

    .line 2613092
    new-instance v0, LX/Iph;

    const-string v1, "BUTTON_LEFT_SELECTED"

    invoke-direct {v0, v1, v5}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->BUTTON_LEFT_SELECTED:LX/Iph;

    .line 2613093
    new-instance v0, LX/Iph;

    const-string v1, "BUTTON_RIGHT_SELECTED"

    invoke-direct {v0, v1, v6}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->BUTTON_RIGHT_SELECTED:LX/Iph;

    .line 2613094
    new-instance v0, LX/Iph;

    const-string v1, "BUTTON_LEFT_CONFIRMED"

    invoke-direct {v0, v1, v7}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->BUTTON_LEFT_CONFIRMED:LX/Iph;

    .line 2613095
    new-instance v0, LX/Iph;

    const-string v1, "BUTTON_RIGHT_CONFIRMED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Iph;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iph;->BUTTON_RIGHT_CONFIRMED:LX/Iph;

    .line 2613096
    const/4 v0, 0x6

    new-array v0, v0, [LX/Iph;

    sget-object v1, LX/Iph;->DISABLED:LX/Iph;

    aput-object v1, v0, v3

    sget-object v1, LX/Iph;->NORMAL:LX/Iph;

    aput-object v1, v0, v4

    sget-object v1, LX/Iph;->BUTTON_LEFT_SELECTED:LX/Iph;

    aput-object v1, v0, v5

    sget-object v1, LX/Iph;->BUTTON_RIGHT_SELECTED:LX/Iph;

    aput-object v1, v0, v6

    sget-object v1, LX/Iph;->BUTTON_LEFT_CONFIRMED:LX/Iph;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Iph;->BUTTON_RIGHT_CONFIRMED:LX/Iph;

    aput-object v2, v0, v1

    sput-object v0, LX/Iph;->$VALUES:[LX/Iph;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2613097
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iph;
    .locals 1

    .prologue
    .line 2613098
    const-class v0, LX/Iph;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iph;

    return-object v0
.end method

.method public static values()[LX/Iph;
    .locals 1

    .prologue
    .line 2613099
    sget-object v0, LX/Iph;->$VALUES:[LX/Iph;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iph;

    return-object v0
.end method
