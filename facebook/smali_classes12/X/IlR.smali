.class public LX/IlR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/6lN;

.field private final c:LX/Duh;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6lN;LX/Duh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607804
    iput-object p1, p0, LX/IlR;->a:Landroid/content/res/Resources;

    .line 2607805
    iput-object p2, p0, LX/IlR;->b:LX/6lN;

    .line 2607806
    iput-object p3, p0, LX/IlR;->c:LX/Duh;

    .line 2607807
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/IlW;
    .locals 11

    .prologue
    .line 2607753
    instance-of v0, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2607754
    check-cast p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2607755
    iget-object v0, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v0, v0

    .line 2607756
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607757
    invoke-static {}, LX/IlU;->newBuilder()LX/IlV;

    move-result-object v0

    sget-object v1, LX/DtL;->MP:LX/DtL;

    .line 2607758
    iput-object v1, v0, LX/IlV;->a:LX/DtL;

    .line 2607759
    move-object v0, v0

    .line 2607760
    iget-object v1, p0, LX/IlR;->c:LX/Duh;

    .line 2607761
    invoke-static {v1}, LX/Duh;->a(LX/Duh;)Ljava/lang/String;

    move-result-object v2

    .line 2607762
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->d:Lcom/facebook/payments/p2p/model/Sender;

    move-object v3, v3

    .line 2607763
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Sender;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v1, v2

    .line 2607764
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2607765
    iput-object v1, v0, LX/IlV;->e:Ljava/lang/Boolean;

    .line 2607766
    move-object v0, v0

    .line 2607767
    iget-object v1, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v1, v1

    .line 2607768
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/CommerceOrder;->a()Ljava/lang/String;

    move-result-object v1

    .line 2607769
    iput-object v1, v0, LX/IlV;->b:Ljava/lang/String;

    .line 2607770
    move-object v0, v0

    .line 2607771
    iget-object v1, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->j:Lcom/facebook/payments/p2p/model/Amount;

    move-object v1, v1

    .line 2607772
    iput-object v1, v0, LX/IlV;->c:Lcom/facebook/payments/p2p/model/Amount;

    .line 2607773
    move-object v0, v0

    .line 2607774
    invoke-static {}, LX/Ila;->newBuilder()LX/Ilb;

    move-result-object v3

    sget-object v4, LX/Ilc;->COMPLETED:LX/Ilc;

    .line 2607775
    iput-object v4, v3, LX/Ilb;->b:LX/Ilc;

    .line 2607776
    move-object v3, v3

    .line 2607777
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 2607778
    iput-object v4, v3, LX/Ilb;->a:Landroid/graphics/Typeface;

    .line 2607779
    move-object v3, v3

    .line 2607780
    iget-object v4, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2607781
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    .line 2607782
    const-wide/16 v7, 0x0

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    .line 2607783
    iget-object v4, p0, LX/IlR;->a:Landroid/content/res/Resources;

    const v7, 0x7f082c43

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, LX/IlR;->b:LX/6lN;

    invoke-virtual {v10, v5, v6}, LX/6lN;->a(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2607784
    iput-object v4, v3, LX/Ilb;->c:Ljava/lang/String;

    .line 2607785
    :goto_0
    invoke-virtual {v3}, LX/Ilb;->d()LX/Ila;

    move-result-object v3

    move-object v1, v3

    .line 2607786
    iput-object v1, v0, LX/IlV;->d:LX/Ila;

    .line 2607787
    move-object v0, v0

    .line 2607788
    invoke-static {}, LX/Ilk;->newBuilder()LX/Ill;

    move-result-object v1

    .line 2607789
    iget-object v2, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v2, v2

    .line 2607790
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/CommerceOrder;->b()Ljava/lang/String;

    move-result-object v2

    .line 2607791
    iput-object v2, v1, LX/Ill;->a:Ljava/lang/String;

    .line 2607792
    move-object v1, v1

    .line 2607793
    const v2, 0x7f02182c

    .line 2607794
    iput v2, v1, LX/Ill;->b:I

    .line 2607795
    move-object v1, v1

    .line 2607796
    invoke-virtual {v0}, LX/IlV;->f()LX/IlU;

    move-result-object v0

    .line 2607797
    iput-object v0, v1, LX/Ill;->c:LX/IlU;

    .line 2607798
    move-object v0, v1

    .line 2607799
    invoke-virtual {v0}, LX/Ill;->d()LX/Ilk;

    move-result-object v0

    return-object v0

    .line 2607800
    :cond_0
    const-string v4, ""

    .line 2607801
    iput-object v4, v3, LX/Ilb;->c:Ljava/lang/String;

    .line 2607802
    goto :goto_0
.end method
