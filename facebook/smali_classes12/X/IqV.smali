.class public final enum LX/IqV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IqV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IqV;

.field public static final enum PACKLIST:LX/IqV;

.field public static final enum STICKERLIST:LX/IqV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2616947
    new-instance v0, LX/IqV;

    const-string v1, "PACKLIST"

    invoke-direct {v0, v1, v2}, LX/IqV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IqV;->PACKLIST:LX/IqV;

    .line 2616948
    new-instance v0, LX/IqV;

    const-string v1, "STICKERLIST"

    invoke-direct {v0, v1, v3}, LX/IqV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IqV;->STICKERLIST:LX/IqV;

    .line 2616949
    const/4 v0, 0x2

    new-array v0, v0, [LX/IqV;

    sget-object v1, LX/IqV;->PACKLIST:LX/IqV;

    aput-object v1, v0, v2

    sget-object v1, LX/IqV;->STICKERLIST:LX/IqV;

    aput-object v1, v0, v3

    sput-object v0, LX/IqV;->$VALUES:[LX/IqV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2616950
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IqV;
    .locals 1

    .prologue
    .line 2616951
    const-class v0, LX/IqV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IqV;

    return-object v0
.end method

.method public static values()[LX/IqV;
    .locals 1

    .prologue
    .line 2616952
    sget-object v0, LX/IqV;->$VALUES:[LX/IqV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IqV;

    return-object v0
.end method
