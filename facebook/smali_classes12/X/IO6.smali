.class public final LX/IO6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IO9;


# direct methods
.method public constructor <init>(LX/IO9;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2571455
    iput-object p1, p0, LX/IO6;->c:LX/IO9;

    iput-object p2, p0, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    iput-object p3, p0, LX/IO6;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x11f59bb9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571456
    iget-object v1, p0, LX/IO6;->c:LX/IO9;

    sget-object v2, LX/IOE;->INVITED:LX/IOE;

    invoke-static {v1, v2}, LX/IO9;->setInviteButtonState(LX/IO9;LX/IOE;)V

    .line 2571457
    iget-object v1, p0, LX/IO6;->c:LX/IO9;

    iget-object v1, v1, LX/IO9;->e:LX/IOF;

    iget-object v2, p0, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/IOE;->INVITED:LX/IOE;

    invoke-virtual {v1, v2, v3}, LX/IOF;->a(Ljava/lang/String;LX/IOE;)V

    .line 2571458
    iget-object v1, p0, LX/IO6;->c:LX/IO9;

    iget-object v1, v1, LX/IO9;->d:LX/DWD;

    iget-object v2, p0, LX/IO6;->b:Ljava/lang/String;

    iget-object v3, p0, LX/IO6;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "community_invite_friend"

    const/4 v5, 0x0

    .line 2571459
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v7

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    move-object v7, v1

    move-object v8, v2

    move-object v11, v4

    move-object v12, v5

    invoke-virtual/range {v7 .. v12}, LX/DWD;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v1, v7

    .line 2571460
    new-instance v2, LX/IO5;

    invoke-direct {v2, p0}, LX/IO5;-><init>(LX/IO6;)V

    iget-object v3, p0, LX/IO6;->c:LX/IO9;

    iget-object v3, v3, LX/IO9;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2571461
    const v1, 0xa257203

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
