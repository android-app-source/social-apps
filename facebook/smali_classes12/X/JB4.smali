.class public final LX/JB4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2658092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_d

    .line 2658093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2658094
    :goto_0
    return v1

    .line 2658095
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2658096
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 2658097
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2658098
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2658099
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2658100
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2658101
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 2658102
    :cond_3
    const-string v12, "category_names"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2658103
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2658104
    :cond_4
    const-string v12, "concise_description"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2658105
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2658106
    :cond_5
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2658107
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2658108
    :cond_6
    const-string v12, "locally_updated_containing_collection_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2658109
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2658110
    :cond_7
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2658111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 2658112
    :cond_8
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2658113
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2658114
    :cond_9
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2658115
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2658116
    :cond_a
    const-string v12, "viewer_timeline_collections_containing"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 2658117
    invoke-static {p0, p1}, LX/40j;->b(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2658118
    :cond_b
    const-string v12, "viewer_timeline_collections_supported"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2658119
    invoke-static {p0, p1}, LX/40j;->b(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 2658120
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2658121
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2658122
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2658123
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2658124
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2658125
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2658126
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2658127
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2658128
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2658129
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2658130
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2658131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_d
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2658132
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2658133
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2658134
    if-eqz v0, :cond_0

    .line 2658135
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658136
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2658137
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2658138
    if-eqz v0, :cond_1

    .line 2658139
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658140
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2658141
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658142
    if-eqz v0, :cond_2

    .line 2658143
    const-string v1, "concise_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658144
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658145
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658146
    if-eqz v0, :cond_3

    .line 2658147
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658148
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658149
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658150
    if-eqz v0, :cond_4

    .line 2658151
    const-string v1, "locally_updated_containing_collection_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658152
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658153
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658154
    if-eqz v0, :cond_5

    .line 2658155
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658156
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658157
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658158
    if-eqz v0, :cond_6

    .line 2658159
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658160
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2658161
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658162
    if-eqz v0, :cond_7

    .line 2658163
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658164
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658165
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658166
    if-eqz v0, :cond_8

    .line 2658167
    const-string v1, "viewer_timeline_collections_containing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658168
    invoke-static {p0, v0, p2, p3}, LX/40j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2658169
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658170
    if-eqz v0, :cond_9

    .line 2658171
    const-string v1, "viewer_timeline_collections_supported"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658172
    invoke-static {p0, v0, p2, p3}, LX/40j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2658173
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2658174
    return-void
.end method
