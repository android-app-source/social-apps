.class public final enum LX/Ide;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ide;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ide;

.field public static final enum ALL:LX/Ide;

.field public static final enum EXTERNAL:LX/Ide;

.field public static final enum INTERNAL:LX/Ide;

.field public static final enum NONE:LX/Ide;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2597684
    new-instance v0, LX/Ide;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/Ide;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ide;->NONE:LX/Ide;

    new-instance v0, LX/Ide;

    const-string v1, "INTERNAL"

    invoke-direct {v0, v1, v3}, LX/Ide;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ide;->INTERNAL:LX/Ide;

    new-instance v0, LX/Ide;

    const-string v1, "EXTERNAL"

    invoke-direct {v0, v1, v4}, LX/Ide;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ide;->EXTERNAL:LX/Ide;

    new-instance v0, LX/Ide;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v5}, LX/Ide;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ide;->ALL:LX/Ide;

    const/4 v0, 0x4

    new-array v0, v0, [LX/Ide;

    sget-object v1, LX/Ide;->NONE:LX/Ide;

    aput-object v1, v0, v2

    sget-object v1, LX/Ide;->INTERNAL:LX/Ide;

    aput-object v1, v0, v3

    sget-object v1, LX/Ide;->EXTERNAL:LX/Ide;

    aput-object v1, v0, v4

    sget-object v1, LX/Ide;->ALL:LX/Ide;

    aput-object v1, v0, v5

    sput-object v0, LX/Ide;->$VALUES:[LX/Ide;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2597685
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ide;
    .locals 1

    .prologue
    .line 2597686
    const-class v0, LX/Ide;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ide;

    return-object v0
.end method

.method public static values()[LX/Ide;
    .locals 1

    .prologue
    .line 2597687
    sget-object v0, LX/Ide;->$VALUES:[LX/Ide;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ide;

    return-object v0
.end method
