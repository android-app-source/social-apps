.class public LX/HPd;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "LX/HPa;",
        "LX/HPZ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/HPd;


# instance fields
.field private final a:LX/00H;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HPf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HPp;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public f:LX/0Ve;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Ve;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(LX/00H;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/00H;",
            "LX/0Ot",
            "<",
            "LX/HPf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HPp;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2462326
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2462327
    iput-object p1, p0, LX/HPd;->a:LX/00H;

    .line 2462328
    iput-object p2, p0, LX/HPd;->b:LX/0Ot;

    .line 2462329
    iput-object p3, p0, LX/HPd;->c:LX/0Ot;

    .line 2462330
    iput-object p4, p0, LX/HPd;->d:LX/0Ot;

    .line 2462331
    iput-object p5, p0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2462332
    return-void
.end method

.method public static a(LX/0QB;)LX/HPd;
    .locals 9

    .prologue
    .line 2462313
    sget-object v0, LX/HPd;->j:LX/HPd;

    if-nez v0, :cond_1

    .line 2462314
    const-class v1, LX/HPd;

    monitor-enter v1

    .line 2462315
    :try_start_0
    sget-object v0, LX/HPd;->j:LX/HPd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2462316
    if-eqz v2, :cond_0

    .line 2462317
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2462318
    new-instance v3, LX/HPd;

    const-class v4, LX/00H;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/00H;

    const/16 v5, 0x2c01

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2c02

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v8

    check-cast v8, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct/range {v3 .. v8}, LX/HPd;-><init>(LX/00H;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2462319
    move-object v0, v3

    .line 2462320
    sput-object v0, LX/HPd;->j:LX/HPd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2462321
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2462322
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2462323
    :cond_1
    sget-object v0, LX/HPd;->j:LX/HPd;

    return-object v0

    .line 2462324
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2462325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/HPd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2462304
    iget-object v0, p0, LX/HPd;->f:LX/0Ve;

    if-eqz v0, :cond_0

    .line 2462305
    iget-object v0, p0, LX/HPd;->f:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 2462306
    iput-object v2, p0, LX/HPd;->f:LX/0Ve;

    .line 2462307
    :cond_0
    iget-object v0, p0, LX/HPd;->g:LX/0Ve;

    if-eqz v0, :cond_1

    .line 2462308
    iget-object v0, p0, LX/HPd;->g:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 2462309
    iput-object v2, p0, LX/HPd;->g:LX/0Ve;

    .line 2462310
    :cond_1
    iput-boolean v1, p0, LX/HPd;->h:Z

    .line 2462311
    iput-boolean v1, p0, LX/HPd;->i:Z

    .line 2462312
    return-void
.end method

.method public static g(LX/HPd;)V
    .locals 3

    .prologue
    .line 2462301
    iget-boolean v0, p0, LX/HPd;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/HPd;->i:Z

    if-eqz v0, :cond_0

    .line 2462302
    iget-object v0, p0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130082

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2462303
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "LX/HPa;",
            "LX/HPZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const-wide/16 v6, -0x1

    .line 2462271
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2462272
    if-eqz v0, :cond_0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    .line 2462273
    :cond_0
    iget-object v0, p0, LX/HPd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "No PageId available in early fetcher"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462274
    const/4 v0, 0x0

    .line 2462275
    :goto_0
    return-object v0

    .line 2462276
    :cond_1
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2462277
    iget-object v0, p0, LX/HPd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HPf;

    sget-object v2, LX/0zS;->b:LX/0zS;

    invoke-virtual {v0, v1, v2}, LX/HPf;->a(Ljava/lang/Long;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2462278
    new-instance v2, LX/0v6;

    const-string v0, "PageReactionHeaderAndFirstCardBatchRequest"

    invoke-direct {v2, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2462279
    sget-object v0, LX/0vU;->PHASED:LX/0vU;

    .line 2462280
    iput-object v0, v2, LX/0v6;->j:LX/0vU;

    .line 2462281
    iget-object v0, p0, LX/HPd;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HPp;

    .line 2462282
    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, LX/HPp;->a(LX/HPp;Ljava/lang/Long;Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v4

    .line 2462283
    iget-object v5, v0, LX/HPp;->a:LX/CfW;

    const-string v6, "ANDROID_PAGE_HOME"

    .line 2462284
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v4, v0}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/Long;)LX/2jY;

    move-result-object v0

    move-object v4, v0

    .line 2462285
    move-object v0, v4

    .line 2462286
    iget-object v4, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v4

    .line 2462287
    iget-object v0, p0, LX/HPd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HPf;

    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1, v4}, LX/HPf;->b(Ljava/lang/Long;LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2462288
    const/4 v4, 0x0

    .line 2462289
    iput v4, v0, LX/0zO;->B:I

    .line 2462290
    invoke-virtual {v2, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2462291
    invoke-static {p0}, LX/HPd;->e(LX/HPd;)V

    .line 2462292
    iget-object v0, p0, LX/HPd;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x130082

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2462293
    new-instance v0, LX/HPb;

    invoke-direct {v0, p0}, LX/HPb;-><init>(LX/HPd;)V

    iput-object v0, p0, LX/HPd;->f:LX/0Ve;

    .line 2462294
    new-instance v0, LX/HPc;

    invoke-direct {v0, p0}, LX/HPc;-><init>(LX/HPd;)V

    iput-object v0, p0, LX/HPd;->g:LX/0Ve;

    .line 2462295
    iget-object v0, p0, LX/HPd;->f:LX/0Ve;

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v4

    invoke-static {v9, v0, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2462296
    iget-object v0, p0, LX/HPd;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HPp;

    iget-object v4, p0, LX/HPd;->g:LX/0Ve;

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v5

    move v6, v3

    invoke-virtual/range {v0 .. v6}, LX/HPp;->a(Ljava/lang/Long;LX/0v6;ILX/0Ve;Ljava/util/concurrent/ExecutorService;Z)LX/2jY;

    move-result-object v0

    .line 2462297
    iget-object v2, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2462298
    new-instance v2, LX/HPa;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/HPa;-><init>(J)V

    .line 2462299
    new-instance v1, LX/HPZ;

    invoke-direct {v1, v7, v9, v0, v8}, LX/HPZ;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;Ljava/lang/String;)V

    .line 2462300
    new-instance v0, LX/98g;

    invoke-direct {v0, v2, v1}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2462257
    check-cast p1, LX/HPZ;

    const/4 v1, 0x1

    .line 2462258
    invoke-static {p0}, LX/HPd;->e(LX/HPd;)V

    .line 2462259
    if-nez p1, :cond_1

    .line 2462260
    :cond_0
    :goto_0
    return-void

    .line 2462261
    :cond_1
    iget-object v0, p1, LX/HPZ;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    .line 2462262
    iget-object v0, p1, LX/HPZ;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2462263
    iget-object v0, p1, LX/HPZ;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2462264
    :cond_2
    iget-object v0, p1, LX/HPZ;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2462265
    iget-object v0, p0, LX/HPd;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HPp;

    iget-object v1, p1, LX/HPZ;->c:Ljava/lang/String;

    .line 2462266
    iget-object p0, v0, LX/HPp;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/2iz;

    invoke-virtual {p0, v1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2462267
    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2462268
    iget-object v0, p0, LX/HPd;->a:LX/00H;

    .line 2462269
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 2462270
    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
