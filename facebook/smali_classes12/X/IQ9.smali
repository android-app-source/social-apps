.class public final LX/IQ9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IQC;


# direct methods
.method public constructor <init>(LX/IQC;)V
    .locals 0

    .prologue
    .line 2575002
    iput-object p1, p0, LX/IQ9;->a:LX/IQC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x4ed34eeb

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575003
    iget-object v1, p0, LX/IQ9;->a:LX/IQC;

    iget-object v1, v1, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IQ9;->a:LX/IQC;

    iget-object v1, v1, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2575004
    :cond_0
    const v1, -0x89cefec

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2575005
    :goto_0
    return-void

    .line 2575006
    :cond_1
    iget-object v1, p0, LX/IQ9;->a:LX/IQC;

    iget-object v1, v1, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/IQ9;->a:LX/IQC;

    iget-object v1, v1, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v1, v2, :cond_2

    .line 2575007
    const v1, 0x64cf4354

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2575008
    :cond_2
    iget-object v1, p0, LX/IQ9;->a:LX/IQC;

    iget-object v1, v1, LX/IQC;->c:LX/3mF;

    iget-object v2, p0, LX/IQ9;->a:LX/IQC;

    iget-object v2, v2, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mobile_group_join"

    const-string v4, "ALLOW_READD"

    invoke-virtual {v1, v2, v3, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2575009
    iget-object v2, p0, LX/IQ9;->a:LX/IQC;

    iget-object v2, v2, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2575010
    invoke-static {v2}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2575011
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v3, v2, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2575012
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2575013
    iget-object v3, p0, LX/IQ9;->a:LX/IQC;

    iget-object v4, p0, LX/IQ9;->a:LX/IQC;

    iget-object v4, v4, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575014
    iget-object p0, v3, LX/IQC;->a:LX/IRb;

    invoke-interface {p0, v4, v2}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2575015
    iget-object p0, v3, LX/IQC;->d:LX/0Sh;

    new-instance p1, LX/IQA;

    invoke-direct {p1, v3, v2, v4}, LX/IQA;-><init>(LX/IQC;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {p0, v1, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2575016
    const v1, -0x62a27e50

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
