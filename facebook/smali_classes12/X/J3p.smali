.class public final LX/J3p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/J3t;


# direct methods
.method public constructor <init>(LX/J3t;Ljava/lang/String;LX/0Px;)V
    .locals 0

    .prologue
    .line 2643294
    iput-object p1, p0, LX/J3p;->c:LX/J3t;

    iput-object p2, p0, LX/J3p;->a:Ljava/lang/String;

    iput-object p3, p0, LX/J3p;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2643295
    iget-object v0, p0, LX/J3p;->c:LX/J3t;

    iget-object v0, v0, LX/J3t;->m:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/J3p;->a:Ljava/lang/String;

    iget-object v2, p0, LX/J3p;->b:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;LX/0Px;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
