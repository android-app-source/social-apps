.class public final LX/IaJ;
.super LX/4Ae;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2590805
    iput-object p1, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-direct {p0}, LX/4Ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailed(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2590806
    iget-object v0, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->h:LX/03V;

    const-string v1, "BusinessCreateAccountFragment"

    const-string v2, "Failed creating account"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2590807
    iget-object v0, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v1, "error_create_account"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590808
    iget-object v0, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2590809
    if-eqz p1, :cond_0

    .line 2590810
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2590811
    if-eqz v1, :cond_0

    .line 2590812
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2590813
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v2

    .line 2590814
    if-nez v1, :cond_1

    .line 2590815
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    .line 2590816
    :goto_0
    return-void

    .line 2590817
    :cond_1
    const-class v1, LX/2Oo;

    invoke-static {p1, v1}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, LX/2Oo;

    .line 2590818
    if-eqz v1, :cond_2

    .line 2590819
    invoke-virtual {v1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    .line 2590820
    iget p0, v2, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v2, p0

    .line 2590821
    invoke-virtual {v1}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;ILjava/lang/String;)V

    goto :goto_0

    .line 2590822
    :cond_2
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2590823
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v2

    .line 2590824
    const-string v2, "originalExceptionMessage"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2590825
    if-eqz v1, :cond_3

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 p0, -0x1

    if-ne v2, p0, :cond_4

    .line 2590826
    :cond_3
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    goto :goto_0

    .line 2590827
    :cond_4
    add-int/lit8 v2, v2, 0x1

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2590828
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "error"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 2590829
    const-string v2, "error_user_msg"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2590830
    const-string p0, "error_subcode"

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2590831
    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2590832
    :catch_0
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    goto :goto_0
.end method

.method public final onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 3

    .prologue
    .line 2590833
    iget-object v0, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v1, "success_create_account"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590834
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2590835
    const-string v1, "next_step"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2590836
    iget-object v1, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2590837
    iget-object v0, p0, LX/IaJ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2590838
    return-void
.end method
