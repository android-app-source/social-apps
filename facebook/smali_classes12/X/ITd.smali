.class public final LX/ITd;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/IUq;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ITj;


# direct methods
.method public constructor <init>(LX/ITj;LX/DML;)V
    .locals 0

    .prologue
    .line 2579437
    iput-object p1, p0, LX/ITd;->a:LX/ITj;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 2579438
    check-cast p1, LX/IUq;

    .line 2579439
    iget-object v0, p0, LX/ITd;->a:LX/ITj;

    iget-object v0, v0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/ITd;->a:LX/ITj;

    iget-object v1, v1, LX/ITj;->b:LX/IRb;

    .line 2579440
    if-nez v0, :cond_0

    .line 2579441
    :goto_0
    return-void

    .line 2579442
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v2, v3, :cond_2

    .line 2579443
    iget-object v2, p1, LX/IUq;->c:LX/IUp;

    if-nez v2, :cond_1

    .line 2579444
    new-instance v2, LX/IUp;

    invoke-virtual {p1}, LX/IUq;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/IUp;-><init>(Landroid/content/Context;)V

    iput-object v2, p1, LX/IUq;->c:LX/IUp;

    .line 2579445
    :cond_1
    iget-object v2, p1, LX/IUq;->c:LX/IUp;

    invoke-static {p1, v2}, LX/IUq;->a(LX/IUq;Landroid/view/View;)V

    goto :goto_0

    .line 2579446
    :cond_2
    iget-object v2, p1, LX/IUq;->b:LX/IUo;

    if-nez v2, :cond_3

    .line 2579447
    new-instance v2, LX/IUo;

    invoke-virtual {p1}, LX/IUq;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/IUo;-><init>(Landroid/content/Context;)V

    iput-object v2, p1, LX/IUq;->b:LX/IUo;

    .line 2579448
    :cond_3
    iget-object v2, p1, LX/IUq;->b:LX/IUo;

    invoke-static {p1, v2}, LX/IUq;->a(LX/IUq;Landroid/view/View;)V

    .line 2579449
    iget-object v2, p1, LX/IUq;->b:LX/IUo;

    .line 2579450
    if-nez v0, :cond_4

    .line 2579451
    :goto_1
    goto :goto_0

    .line 2579452
    :cond_4
    iget-object v3, v2, LX/IUo;->b:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x21

    const/4 p0, 0x0

    const/4 v9, 0x1

    .line 2579453
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2579454
    :cond_5
    new-instance v4, LX/47x;

    invoke-virtual {v2}, LX/IUo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v2}, LX/IUo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081c32

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    const-string v5, "%1$s"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v8, v7, p0

    invoke-virtual {v4, v5, v6, p1, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    .line 2579455
    :goto_2
    move-object v4, v4

    .line 2579456
    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2579457
    iget-object v3, v2, LX/IUo;->c:Landroid/view/View;

    new-instance v4, LX/IUm;

    invoke-direct {v4, v2, v1, v0}, LX/IUm;-><init>(LX/IUo;LX/IRb;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2579458
    iget-object v3, v2, LX/IUo;->d:Landroid/view/View;

    new-instance v4, LX/IUn;

    invoke-direct {v4, v2}, LX/IUn;-><init>(LX/IUo;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_6
    new-instance v4, LX/47x;

    invoke-virtual {v2}, LX/IUo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v2}, LX/IUo;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081c33

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    const-string v5, "%1$s"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;->d()Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v8, v7, p0

    invoke-virtual {v4, v5, v6, p1, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v4

    const-string v5, "%2$s"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v8, v7, p0

    invoke-virtual {v4, v5, v6, p1, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    goto :goto_2
.end method
