.class public final LX/ILT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/4At;)V
    .locals 0

    .prologue
    .line 2568002
    iput-object p1, p0, LX/ILT;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iput-object p2, p0, LX/ILT;->a:LX/4At;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2568003
    iget-object v0, p0, LX/ILT;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568004
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/ILT;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f082ffe

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080030

    new-instance v2, LX/ILS;

    invoke-direct {v2, p0}, LX/ILS;-><init>(LX/ILT;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    .line 2568005
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2568006
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2568007
    iget-object v0, p0, LX/ILT;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568008
    iget-object v0, p0, LX/ILT;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->h:LX/0if;

    sget-object v1, LX/0ig;->aT:LX/0ih;

    const-string v2, "did_join_forum_groups"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2568009
    iget-object v0, p0, LX/ILT;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->o:LX/IL1;

    invoke-interface {v0}, LX/IL1;->a()V

    .line 2568010
    return-void
.end method
