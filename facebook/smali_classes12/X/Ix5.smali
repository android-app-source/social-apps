.class public final LX/Ix5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V
    .locals 0

    .prologue
    .line 2631169
    iput-object p1, p0, LX/Ix5;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x2acd8b50

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2631170
    iget-object v1, p0, LX/Ix5;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631171
    iget-object v1, p0, LX/Ix5;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->y:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2631172
    iget-object v1, p0, LX/Ix5;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    .line 2631173
    iget-object v4, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-nez v4, :cond_0

    .line 2631174
    :goto_0
    iget-object v1, p0, LX/Ix5;->a:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    .line 2631175
    iget-object v3, v1, LX/Iws;->a:LX/0if;

    sget-object v4, LX/0ig;->S:LX/0ih;

    const-string v5, "branded_content_signed_legal_terms"

    const/4 v6, 0x0

    invoke-static {v1}, LX/Iws;->h(LX/Iws;)LX/1rQ;

    move-result-object v7

    invoke-virtual {v3, v4, v5, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2631176
    const v1, -0x4f28476c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2631177
    :cond_0
    iget-object v4, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->b:LX/9Wi;

    iget-object v5, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v6, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 2631178
    new-instance v6, LX/9Wj;

    invoke-direct {v6}, LX/9Wj;-><init>()V

    move-object v6, v6

    .line 2631179
    new-instance v7, LX/4JR;

    invoke-direct {v7}, LX/4JR;-><init>()V

    .line 2631180
    const-string v8, "actor_id"

    invoke-virtual {v7, v8, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2631181
    iget-object v8, v6, LX/0gW;->h:Ljava/lang/String;

    move-object v8, v8

    .line 2631182
    const-string v5, "client_mutation_id"

    invoke-virtual {v7, v5, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2631183
    const-string v8, "input"

    invoke-virtual {v6, v8, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2631184
    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2631185
    iget-object v7, v4, LX/9Wi;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v4, v6

    .line 2631186
    new-instance v5, LX/Iww;

    invoke-direct {v5, v1}, LX/Iww;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    iget-object v6, v1, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->c:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
