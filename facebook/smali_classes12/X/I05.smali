.class public final enum LX/I05;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I05;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I05;

.field public static final enum ERROR:LX/I05;

.field public static final enum FIRST_LOAD:LX/I05;

.field public static final enum INITIAL:LX/I05;

.field public static final enum LOADED:LX/I05;

.field public static final enum LOADING_MORE:LX/I05;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2526522
    new-instance v0, LX/I05;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/I05;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I05;->INITIAL:LX/I05;

    .line 2526523
    new-instance v0, LX/I05;

    const-string v1, "FIRST_LOAD"

    invoke-direct {v0, v1, v3}, LX/I05;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I05;->FIRST_LOAD:LX/I05;

    .line 2526524
    new-instance v0, LX/I05;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, LX/I05;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I05;->LOADED:LX/I05;

    .line 2526525
    new-instance v0, LX/I05;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v5}, LX/I05;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I05;->LOADING_MORE:LX/I05;

    .line 2526526
    new-instance v0, LX/I05;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, LX/I05;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I05;->ERROR:LX/I05;

    .line 2526527
    const/4 v0, 0x5

    new-array v0, v0, [LX/I05;

    sget-object v1, LX/I05;->INITIAL:LX/I05;

    aput-object v1, v0, v2

    sget-object v1, LX/I05;->FIRST_LOAD:LX/I05;

    aput-object v1, v0, v3

    sget-object v1, LX/I05;->LOADED:LX/I05;

    aput-object v1, v0, v4

    sget-object v1, LX/I05;->LOADING_MORE:LX/I05;

    aput-object v1, v0, v5

    sget-object v1, LX/I05;->ERROR:LX/I05;

    aput-object v1, v0, v6

    sput-object v0, LX/I05;->$VALUES:[LX/I05;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2526528
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I05;
    .locals 1

    .prologue
    .line 2526529
    const-class v0, LX/I05;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I05;

    return-object v0
.end method

.method public static values()[LX/I05;
    .locals 1

    .prologue
    .line 2526530
    sget-object v0, LX/I05;->$VALUES:[LX/I05;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I05;

    return-object v0
.end method
