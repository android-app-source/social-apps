.class public LX/Ijy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/03V;

.field private final c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field public final d:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public final e:Landroid/content/Context;

.field private final f:Landroid/view/LayoutInflater;

.field public final g:LX/0Zb;

.field public final h:LX/73q;

.field public final i:Landroid/app/Activity;

.field public final j:Ljava/util/concurrent/Executor;

.field private final k:Lcom/facebook/content/SecureContextHelper;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/6o9;

.field public final n:LX/6o6;

.field public o:LX/Ik2;

.field public p:LX/Ijx;

.field private q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public r:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2606301
    const-class v0, LX/Ijy;

    sput-object v0, LX/Ijy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/content/Context;Landroid/view/LayoutInflater;LX/0Zb;LX/73q;Landroid/app/Activity;Ljava/util/concurrent/Executor;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/6o9;LX/6o6;)V
    .locals 0
    .param p9    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "Landroid/content/Context;",
            "Landroid/view/LayoutInflater;",
            "LX/0Zb;",
            "LX/73q;",
            "Landroid/app/Activity;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/6o9;",
            "LX/6o6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606287
    iput-object p1, p0, LX/Ijy;->b:LX/03V;

    .line 2606288
    iput-object p2, p0, LX/Ijy;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 2606289
    iput-object p3, p0, LX/Ijy;->d:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2606290
    iput-object p4, p0, LX/Ijy;->e:Landroid/content/Context;

    .line 2606291
    iput-object p5, p0, LX/Ijy;->f:Landroid/view/LayoutInflater;

    .line 2606292
    iput-object p6, p0, LX/Ijy;->g:LX/0Zb;

    .line 2606293
    iput-object p7, p0, LX/Ijy;->h:LX/73q;

    .line 2606294
    iput-object p8, p0, LX/Ijy;->i:Landroid/app/Activity;

    .line 2606295
    iput-object p9, p0, LX/Ijy;->j:Ljava/util/concurrent/Executor;

    .line 2606296
    iput-object p10, p0, LX/Ijy;->k:Lcom/facebook/content/SecureContextHelper;

    .line 2606297
    iput-object p11, p0, LX/Ijy;->l:LX/0Or;

    .line 2606298
    iput-object p12, p0, LX/Ijy;->m:LX/6o9;

    .line 2606299
    iput-object p13, p0, LX/Ijy;->n:LX/6o6;

    .line 2606300
    return-void
.end method

.method public static a(LX/0QB;)LX/Ijy;
    .locals 1

    .prologue
    .line 2606285
    invoke-static {p0}, LX/Ijy;->b(LX/0QB;)LX/Ijy;

    move-result-object v0

    return-object v0
.end method

.method private a(ILandroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p2    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606278
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_1

    .line 2606279
    invoke-virtual {p2, p3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PartialPaymentCard;

    .line 2606280
    const-string v1, "verification_follow_up_action"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;

    .line 2606281
    invoke-static {p0, v0, v1}, LX/Ijy;->a$redex0(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;)V

    .line 2606282
    :cond_0
    :goto_0
    return-void

    .line 2606283
    :cond_1
    if-nez p1, :cond_0

    .line 2606284
    iget-object v0, p0, LX/Ijy;->p:LX/Ijx;

    invoke-interface {v0}, LX/Ijx;->c()V

    goto :goto_0
.end method

.method public static a(LX/Ijy;Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2606274
    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v0, v0, LX/Ik2;->c:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_0

    .line 2606275
    iget-object v0, p0, LX/Ijy;->k:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Ijy;->i:Landroid/app/Activity;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2606276
    :goto_0
    return-void

    .line 2606277
    :cond_0
    iget-object v0, p0, LX/Ijy;->k:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->c:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 11
    .param p1    # Lcom/facebook/payments/p2p/model/PaymentCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606224
    if-nez p1, :cond_3

    .line 2606225
    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v0, v0, LX/Ik2;->e:LX/5g0;

    sget-object v1, LX/5g0;->NUX:LX/5g0;

    if-ne v0, v1, :cond_2

    .line 2606226
    iget-object v2, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v2, v2, LX/Ik2;->b:LX/0Px;

    .line 2606227
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2606228
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606229
    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/PaymentCard;->o()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2606230
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2606231
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2606232
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 2606233
    invoke-static {v2}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->d(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 2606234
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2606235
    invoke-static {p0}, LX/Ijy;->c(LX/Ijy;)V

    .line 2606236
    :goto_1
    return-void

    .line 2606237
    :cond_2
    invoke-static {p0}, LX/Ijy;->c(LX/Ijy;)V

    goto :goto_1

    .line 2606238
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->v()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2606239
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->k()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->h()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2606240
    :cond_4
    const/4 v0, 0x1

    .line 2606241
    iget-object v1, p0, LX/Ijy;->g:LX/0Zb;

    iget-object v2, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v2, v2, LX/Ik2;->e:LX/5g0;

    iget-object v2, v2, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v3, "p2p_initiate_edit_card"

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606242
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    sget-object v2, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v2}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v2

    invoke-virtual {v2}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v1

    const-string v2, "p2p"

    .line 2606243
    iput-object v2, v1, LX/6xw;->d:Ljava/lang/String;

    .line 2606244
    move-object v1, v1

    .line 2606245
    sget-object v2, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    .line 2606246
    iput-object v2, v1, LX/6xw;->c:LX/6xZ;

    .line 2606247
    move-object v1, v1

    .line 2606248
    invoke-virtual {v1}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v1

    .line 2606249
    sget-object v2, LX/6yO;->MESSENGER_PAY_EDIT:LX/6yO;

    iget-object v3, p0, LX/Ijy;->o:LX/Ik2;

    iget-boolean v3, v3, LX/Ik2;->k:Z

    invoke-static {v3}, LX/Due;->a(Z)LX/6xg;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v1

    .line 2606250
    iput-boolean v0, v1, LX/6xy;->f:Z

    .line 2606251
    move-object v1, v1

    .line 2606252
    iput-object p1, v1, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 2606253
    move-object v1, v1

    .line 2606254
    invoke-static {}, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->newBuilder()LX/IjJ;

    move-result-object v2

    invoke-virtual {v1}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    .line 2606255
    iput-object v1, v2, LX/IjJ;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    .line 2606256
    move-object v1, v2

    .line 2606257
    sget-object v2, LX/5g0;->SETTINGS:LX/5g0;

    iget-object v3, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v3, v3, LX/Ik2;->e:LX/5g0;

    invoke-virtual {v2, v3}, LX/5g0;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2606258
    :goto_2
    iput-boolean v0, v1, LX/IjJ;->h:Z

    .line 2606259
    move-object v0, v1

    .line 2606260
    invoke-virtual {v0}, LX/IjJ;->j()Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    move-result-object v0

    .line 2606261
    iget-object v1, p0, LX/Ijy;->e:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2606262
    const/16 v1, 0x3e9

    invoke-static {p0, v0, v1}, LX/Ijy;->a(LX/Ijy;Landroid/content/Intent;I)V

    .line 2606263
    :goto_3
    goto/16 :goto_1

    .line 2606264
    :cond_5
    iget-object v4, p0, LX/Ijy;->g:LX/0Zb;

    iget-object v5, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v5, v5, LX/Ik2;->e:LX/5g0;

    iget-object v5, v5, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v6, "p2p_initiate_select_card"

    invoke-static {v5, v6}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606265
    new-instance v9, LX/Ijo;

    invoke-direct {v9, p0, v2}, LX/Ijo;-><init>(LX/Ijy;LX/0Px;)V

    .line 2606266
    iget-object v4, p0, LX/Ijy;->e:Landroid/content/Context;

    iget-object v5, p0, LX/Ijy;->e:Landroid/content/Context;

    invoke-static {v5, v2}, LX/Gza;->a(Landroid/content/Context;LX/0Px;)LX/0Px;

    move-result-object v5

    iget-object v6, p0, LX/Ijy;->e:Landroid/content/Context;

    const v7, 0x7f082c9a

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/Ijy;->e:Landroid/content/Context;

    const v8, 0x7f082c8e

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/Ijy;->e:Landroid/content/Context;

    const v10, 0x7f082c8f

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v4 .. v9}, LX/Gza;->a(Landroid/content/Context;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GzZ;)LX/2EJ;

    move-result-object v4

    .line 2606267
    new-instance v5, LX/Ijp;

    invoke-direct {v5, p0}, LX/Ijp;-><init>(LX/Ijy;)V

    invoke-virtual {v4, v5}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2606268
    goto/16 :goto_1

    .line 2606269
    :cond_6
    iget-boolean v0, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    move v0, v0

    .line 2606270
    if-eqz v0, :cond_7

    .line 2606271
    iget-object v0, p0, LX/Ijy;->p:LX/Ijx;

    invoke-interface {v0}, LX/Ijx;->a()V

    goto :goto_3

    .line 2606272
    :cond_7
    invoke-static {p0, p1}, LX/Ijy;->d(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V

    goto :goto_3

    .line 2606273
    :cond_8
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a$redex0(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Landroid/content/DialogInterface;Lcom/facebook/resources/ui/FbEditText;)V
    .locals 10

    .prologue
    .line 2606302
    invoke-virtual {p3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2606303
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 2606304
    iget-object v1, p0, LX/Ijy;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2606305
    :goto_0
    return-void

    .line 2606306
    :cond_0
    iget-object v1, p0, LX/Ijy;->g:LX/0Zb;

    iget-object v2, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v2, v2, LX/Ik2;->e:LX/5g0;

    iget-object v2, v2, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v3, "p2p_confirm_csc"

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606307
    iget-object v1, p0, LX/Ijy;->d:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iget-object v2, p0, LX/Ijy;->e:Landroid/content/Context;

    .line 2606308
    iget-wide v8, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v3, v8

    .line 2606309
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2606310
    iget v4, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    move v4, v4

    .line 2606311
    iget v5, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    move v5, v5

    .line 2606312
    iget-object v6, p0, LX/Ijy;->e:Landroid/content/Context;

    const v7, 0x7f082c9e

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v6, v0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/Ijy;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606313
    iget-object v1, p0, LX/Ijy;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Ijh;

    invoke-direct {v2, p0, p1}, LX/Ijh;-><init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V

    iget-object v3, p0, LX/Ijy;->j:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;)V
    .locals 3
    .param p1    # Lcom/facebook/payments/p2p/model/PaymentCard;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606222
    iget-object v0, p0, LX/Ijy;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Iji;

    invoke-direct {v1, p0, p1, p2}, LX/Iji;-><init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;)V

    iget-object v2, p0, LX/Ijy;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2606223
    return-void
.end method

.method public static b(LX/0QB;)LX/Ijy;
    .locals 14

    .prologue
    .line 2606220
    new-instance v0, LX/Ijy;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {p0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v7

    check-cast v7, LX/73q;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    const/16 v11, 0x12cc

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/6o9;->b(LX/0QB;)LX/6o9;

    move-result-object v12

    check-cast v12, LX/6o9;

    invoke-static {p0}, LX/6o6;->b(LX/0QB;)LX/6o6;

    move-result-object v13

    check-cast v13, LX/6o6;

    invoke-direct/range {v0 .. v13}, LX/Ijy;-><init>(LX/03V;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/content/Context;Landroid/view/LayoutInflater;LX/0Zb;LX/73q;Landroid/app/Activity;Ljava/util/concurrent/Executor;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/6o9;LX/6o6;)V

    .line 2606221
    return-object v0
.end method

.method public static c(LX/Ijy;)V
    .locals 4

    .prologue
    .line 2606175
    iget-object v1, p0, LX/Ijy;->e:Landroid/content/Context;

    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-boolean v0, v0, LX/Ik2;->k:Z

    if-eqz v0, :cond_1

    const v0, 0x7f081e79

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2606176
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    sget-object v2, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v2}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v2

    invoke-virtual {v2}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v1

    const-string v2, "p2p"

    .line 2606177
    iput-object v2, v1, LX/6xw;->d:Ljava/lang/String;

    .line 2606178
    move-object v1, v1

    .line 2606179
    sget-object v2, LX/6xZ;->ADD_CARD:LX/6xZ;

    .line 2606180
    iput-object v2, v1, LX/6xw;->c:LX/6xZ;

    .line 2606181
    move-object v1, v1

    .line 2606182
    invoke-virtual {v1}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v1

    .line 2606183
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v2

    .line 2606184
    iput-object v0, v2, LX/6yR;->a:Ljava/lang/String;

    .line 2606185
    move-object v0, v2

    .line 2606186
    iget-object v2, p0, LX/Ijy;->o:LX/Ik2;

    iget-boolean v2, v2, LX/Ik2;->d:Z

    if-eqz v2, :cond_0

    .line 2606187
    iget-object v2, p0, LX/Ijy;->e:Landroid/content/Context;

    const v3, 0x7f082c16

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2606188
    iput-object v2, v0, LX/6yR;->b:Ljava/lang/String;

    .line 2606189
    :cond_0
    sget-object v2, LX/6yO;->MESSENGER_PAY_ADD:LX/6yO;

    iget-object v3, p0, LX/Ijy;->o:LX/Ik2;

    iget-boolean v3, v3, LX/Ik2;->k:Z

    invoke-static {v3}, LX/Due;->a(Z)LX/6xg;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v1

    invoke-virtual {v0}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v0

    .line 2606190
    iput-object v0, v1, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 2606191
    move-object v0, v1

    .line 2606192
    const/4 v1, 0x1

    .line 2606193
    iput-boolean v1, v0, LX/6xy;->f:Z

    .line 2606194
    move-object v0, v0

    .line 2606195
    invoke-virtual {v0}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    .line 2606196
    invoke-static {}, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->newBuilder()LX/IjJ;

    move-result-object v1

    .line 2606197
    iput-object v0, v1, LX/IjJ;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    .line 2606198
    move-object v0, v1

    .line 2606199
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->f:Ljava/lang/String;

    .line 2606200
    iput-object v1, v0, LX/IjJ;->a:Ljava/lang/String;

    .line 2606201
    move-object v0, v0

    .line 2606202
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->i:Ljava/lang/String;

    .line 2606203
    iput-object v1, v0, LX/IjJ;->c:Ljava/lang/String;

    .line 2606204
    move-object v0, v0

    .line 2606205
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->j:Ljava/lang/String;

    .line 2606206
    iput-object v1, v0, LX/IjJ;->d:Ljava/lang/String;

    .line 2606207
    move-object v0, v0

    .line 2606208
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-boolean v1, v1, LX/Ik2;->k:Z

    .line 2606209
    iput-boolean v1, v0, LX/IjJ;->e:Z

    .line 2606210
    move-object v0, v0

    .line 2606211
    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->g:Ljava/lang/String;

    .line 2606212
    iput-object v1, v0, LX/IjJ;->b:Ljava/lang/String;

    .line 2606213
    move-object v0, v0

    .line 2606214
    invoke-virtual {v0}, LX/IjJ;->j()Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;

    move-result-object v0

    .line 2606215
    iget-object v1, p0, LX/Ijy;->e:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2606216
    iget-object v1, p0, LX/Ijy;->g:LX/0Zb;

    iget-object v2, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v2, v2, LX/Ik2;->e:LX/5g0;

    iget-object v2, v2, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v3, "p2p_initiate_add_card"

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606217
    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, LX/Ijy;->a(LX/Ijy;Landroid/content/Intent;I)V

    .line 2606218
    return-void

    .line 2606219
    :cond_1
    const v0, 0x7f082c06

    goto/16 :goto_0
.end method

.method public static c(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 4

    .prologue
    .line 2606168
    iget-object v0, p0, LX/Ijy;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606169
    iget-object v0, p0, LX/Ijy;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2606170
    :cond_0
    iget-object v1, p0, LX/Ijy;->d:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, LX/Ijy;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2606171
    iget-object p1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p1

    .line 2606172
    invoke-virtual {v1, v2, v3, v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Ijy;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606173
    iget-object v0, p0, LX/Ijy;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ijq;

    invoke-direct {v1, p0}, LX/Ijq;-><init>(LX/Ijy;)V

    iget-object v2, p0, LX/Ijy;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2606174
    return-void
.end method

.method public static d(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2606149
    iget-object v0, p0, LX/Ijy;->g:LX/0Zb;

    iget-object v1, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v2, "p2p_initiate_csc"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606150
    iget-object v1, p0, LX/Ijy;->e:Landroid/content/Context;

    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 2606151
    iget-object v2, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2606152
    invoke-static {v2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    if-ne v0, v2, :cond_0

    const v0, 0x7f082c9d

    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 2606153
    iget-object v3, p1, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2606154
    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/PaymentCard;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2606155
    iget-object v0, p0, LX/Ijy;->f:Landroid/view/LayoutInflater;

    const v2, 0x7f0303b8

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2606156
    const v0, 0x7f0d0bb9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2606157
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606158
    const v0, 0x7f0d0bba

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2606159
    new-instance v1, LX/31Y;

    iget-object v3, p0, LX/Ijy;->e:Landroid/content/Context;

    invoke-direct {v1, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f082c9b

    invoke-virtual {v1, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/Ijs;

    invoke-direct {v3, p0, p1, v0}, LX/Ijs;-><init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    new-instance v3, LX/Ijr;

    invoke-direct {v3, p0}, LX/Ijr;-><init>(LX/Ijy;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 2606160
    new-instance v2, LX/Ijt;

    invoke-direct {v2, p0, v0}, LX/Ijt;-><init>(LX/Ijy;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v1, v2}, LX/2EJ;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 2606161
    new-instance v2, LX/Iju;

    invoke-direct {v2, p0}, LX/Iju;-><init>(LX/Ijy;)V

    invoke-virtual {v1, v2}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2606162
    new-instance v2, LX/Ijv;

    invoke-direct {v2, p0}, LX/Ijv;-><init>(LX/Ijy;)V

    invoke-virtual {v1, v2}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2606163
    invoke-virtual {v1, v5}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 2606164
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2606165
    new-instance v2, LX/Ijw;

    invoke-direct {v2, p0, p1, v1, v0}, LX/Ijw;-><init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;LX/2EJ;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2606166
    return-void

    .line 2606167
    :cond_0
    const v0, 0x7f082c9c

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606144
    iget-object v0, p0, LX/Ijy;->p:LX/Ijx;

    if-nez v0, :cond_0

    .line 2606145
    :goto_0
    return-void

    .line 2606146
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2606147
    :pswitch_0
    const-string v0, "partial_payment_card"

    invoke-direct {p0, p2, p3, v0}, LX/Ijy;->a(ILandroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 2606148
    :pswitch_1
    const-string v0, "partial_payment_card"

    invoke-direct {p0, p2, p3, v0}, LX/Ijy;->a(ILandroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/Ik2;LX/Ijx;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2606129
    iput-object p1, p0, LX/Ijy;->o:LX/Ik2;

    .line 2606130
    iput-object p2, p0, LX/Ijy;->p:LX/Ijx;

    .line 2606131
    const/4 v2, 0x1

    .line 2606132
    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v4, v0, LX/Ik2;->b:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606133
    iget-boolean p1, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    move v0, p1

    .line 2606134
    if-eqz v0, :cond_0

    move v0, v1

    .line 2606135
    :goto_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2606136
    iget-object v2, p0, LX/Ijy;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v2}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2606137
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2606138
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Ijy;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606139
    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v0, v0, LX/Ik2;->h:LX/Ik1;

    sget-object v1, LX/Ik1;->NEW:LX/Ik1;

    if-ne v0, v1, :cond_1

    .line 2606140
    invoke-static {p0}, LX/Ijy;->c(LX/Ijy;)V

    .line 2606141
    :goto_2
    return-void

    .line 2606142
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2606143
    :cond_1
    iget-object v0, p0, LX/Ijy;->o:LX/Ik2;

    iget-object v0, v0, LX/Ik2;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-direct {p0, v0}, LX/Ijy;->a(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method
