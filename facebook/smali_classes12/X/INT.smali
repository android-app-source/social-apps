.class public final LX/INT;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/INU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/INW;

.field public b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;


# direct methods
.method public constructor <init>(LX/INW;)V
    .locals 0

    .prologue
    .line 2570864
    iput-object p1, p0, LX/INT;->a:LX/INW;

    invoke-direct {p0}, LX/1OM;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2570860
    sget-object v0, LX/INV;->REGULAR_NEWS_CARD_TYPE:LX/INV;

    invoke-virtual {v0}, LX/INV;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2570861
    new-instance v1, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    iget-object v0, p0, LX/INT;->a:LX/INW;

    invoke-virtual {v0}, LX/INW;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;-><init>(Landroid/content/Context;)V

    .line 2570862
    new-instance v0, LX/INU;

    invoke-direct {v0, v1}, LX/INU;-><init>(Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;)V

    .line 2570863
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2570868
    check-cast p1, LX/INU;

    .line 2570869
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 2570870
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;

    .line 2570871
    iget-object v1, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;

    const/4 p0, 0x0

    .line 2570872
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2570873
    :cond_0
    :goto_0
    return-void

    .line 2570874
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;

    move-result-object v2

    .line 2570875
    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel$ImageModel;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel$ImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 2570876
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel$ImageModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$MediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p2, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2570877
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->c()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_4

    .line 2570878
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570879
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object p0

    if-eqz p0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;->a()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_5

    .line 2570880
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel$SourceModel;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570881
    :goto_3
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->g:Landroid/view/View$OnClickListener;

    if-nez p0, :cond_2

    .line 2570882
    new-instance p0, LX/IOA;

    invoke-direct {p0, v0, v2}, LX/IOA;-><init>(Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel$NodesModel$LocalNewsArticlesMediaModel$AttachmentModel;)V

    iput-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->g:Landroid/view/View$OnClickListener;

    .line 2570883
    :cond_2
    iget-object v2, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2570884
    :cond_3
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    sget-object p2, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 2570885
    :cond_4
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2570886
    :cond_5
    iget-object p0, v0, Lcom/facebook/groups/community/views/CommunityLocalNewsItemView;->f:Lcom/facebook/resources/ui/FbTextView;

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2570887
    sget-object v0, LX/INV;->REGULAR_NEWS_CARD_TYPE:LX/INV;

    invoke-virtual {v0}, LX/INV;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2570865
    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2570866
    :cond_0
    const/4 v0, 0x0

    .line 2570867
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/INT;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
