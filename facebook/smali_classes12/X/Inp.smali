.class public final enum LX/Inp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Inp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Inp;

.field public static final enum TAB_ORION_PAY:LX/Inp;

.field public static final enum TAB_ORION_REQUEST:LX/Inp;


# instance fields
.field public final titleResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2610869
    new-instance v0, LX/Inp;

    const-string v1, "TAB_ORION_PAY"

    const v2, 0x7f082d2b

    invoke-direct {v0, v1, v3, v2}, LX/Inp;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Inp;->TAB_ORION_PAY:LX/Inp;

    .line 2610870
    new-instance v0, LX/Inp;

    const-string v1, "TAB_ORION_REQUEST"

    const v2, 0x7f082d2c

    invoke-direct {v0, v1, v4, v2}, LX/Inp;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Inp;->TAB_ORION_REQUEST:LX/Inp;

    .line 2610871
    const/4 v0, 0x2

    new-array v0, v0, [LX/Inp;

    sget-object v1, LX/Inp;->TAB_ORION_PAY:LX/Inp;

    aput-object v1, v0, v3

    sget-object v1, LX/Inp;->TAB_ORION_REQUEST:LX/Inp;

    aput-object v1, v0, v4

    sput-object v0, LX/Inp;->$VALUES:[LX/Inp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2610864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2610865
    iput p3, p0, LX/Inp;->titleResId:I

    .line 2610866
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Inp;
    .locals 1

    .prologue
    .line 2610868
    const-class v0, LX/Inp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Inp;

    return-object v0
.end method

.method public static values()[LX/Inp;
    .locals 1

    .prologue
    .line 2610867
    sget-object v0, LX/Inp;->$VALUES:[LX/Inp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Inp;

    return-object v0
.end method
