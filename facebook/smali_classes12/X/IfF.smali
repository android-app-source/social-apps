.class public LX/IfF;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/If1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/Ieu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IfP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IfT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ie8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private h:LX/1P0;

.field public i:LX/IfE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:LX/If7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2599444
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2599445
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2599446
    iput-object v0, p0, LX/IfF;->f:LX/0Ot;

    .line 2599447
    new-instance v0, LX/If8;

    invoke-direct {v0, p0}, LX/If8;-><init>(LX/IfF;)V

    iput-object v0, p0, LX/IfF;->j:LX/If7;

    .line 2599448
    invoke-direct {p0}, LX/IfF;->a()V

    .line 2599449
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2599518
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2599519
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2599520
    iput-object v0, p0, LX/IfF;->f:LX/0Ot;

    .line 2599521
    new-instance v0, LX/If8;

    invoke-direct {v0, p0}, LX/If8;-><init>(LX/IfF;)V

    iput-object v0, p0, LX/IfF;->j:LX/If7;

    .line 2599522
    invoke-direct {p0}, LX/IfF;->a()V

    .line 2599523
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2599512
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2599513
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2599514
    iput-object v0, p0, LX/IfF;->f:LX/0Ot;

    .line 2599515
    new-instance v0, LX/If8;

    invoke-direct {v0, p0}, LX/If8;-><init>(LX/IfF;)V

    iput-object v0, p0, LX/IfF;->j:LX/If7;

    .line 2599516
    invoke-direct {p0}, LX/IfF;->a()V

    .line 2599517
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2599497
    const-class v0, LX/IfF;

    invoke-static {v0, p0}, LX/IfF;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2599498
    const v0, 0x7f030f23

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2599499
    const v0, 0x7f0d0b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2599500
    invoke-virtual {p0}, LX/IfF;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b24ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2599501
    invoke-virtual {p0}, LX/IfF;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b24e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2599502
    new-instance v2, LX/1P0;

    invoke-virtual {p0}, LX/IfF;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/IfF;->h:LX/1P0;

    .line 2599503
    iget-object v2, p0, LX/IfF;->h:LX/1P0;

    invoke-virtual {v2, v4}, LX/1P1;->b(I)V

    .line 2599504
    iget-object v2, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/If9;

    invoke-direct {v3, p0, v1, v0}, LX/If9;-><init>(LX/IfF;II)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2599505
    iget-object v0, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/IfF;->h:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2599506
    iget-object v0, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2599507
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v1

    .line 2599508
    iput-boolean v4, v0, LX/1Of;->g:Z

    .line 2599509
    iget-object v0, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/IfF;->a:LX/If1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2599510
    iget-object v0, p0, LX/IfF;->c:LX/IfP;

    iget-object v1, p0, LX/IfF;->j:LX/If7;

    invoke-virtual {v0, v1}, LX/IfP;->a(LX/If7;)V

    .line 2599511
    return-void
.end method

.method private static a(LX/IfF;LX/If1;LX/Ieu;LX/IfP;LX/IfT;Ljava/util/concurrent/Executor;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IfF;",
            "LX/If1;",
            "LX/Ieu;",
            "LX/IfP;",
            "LX/IfT;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Ot",
            "<",
            "LX/Ie8;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2599496
    iput-object p1, p0, LX/IfF;->a:LX/If1;

    iput-object p2, p0, LX/IfF;->b:LX/Ieu;

    iput-object p3, p0, LX/IfF;->c:LX/IfP;

    iput-object p4, p0, LX/IfF;->d:LX/IfT;

    iput-object p5, p0, LX/IfF;->e:Ljava/util/concurrent/Executor;

    iput-object p6, p0, LX/IfF;->f:LX/0Ot;

    return-void
.end method

.method private a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 4

    .prologue
    .line 2599490
    iget-object v0, p0, LX/IfF;->c:LX/IfP;

    const-string v1, "INBOX2"

    iget-object v2, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599491
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2599492
    invoke-virtual {v0, v1, v2}, LX/IfP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599493
    iget-object v0, p0, LX/IfF;->i:LX/IfE;

    if-eqz v0, :cond_0

    .line 2599494
    iget-object v0, p0, LX/IfF;->a:LX/If1;

    invoke-virtual {v0, p1}, LX/If1;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599495
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/IfF;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, LX/IfF;

    new-instance v2, LX/If1;

    invoke-static {v6}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-direct {v2, v1}, LX/If1;-><init>(Landroid/view/LayoutInflater;)V

    move-object v1, v2

    check-cast v1, LX/If1;

    invoke-static {v6}, LX/Ieu;->a(LX/0QB;)LX/Ieu;

    move-result-object v2

    check-cast v2, LX/Ieu;

    invoke-static {v6}, LX/IfP;->a(LX/0QB;)LX/IfP;

    move-result-object v3

    check-cast v3, LX/IfP;

    invoke-static {v6}, LX/IfT;->b(LX/0QB;)LX/IfT;

    move-result-object v4

    check-cast v4, LX/IfT;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    const/16 v7, 0x270b

    invoke-static {v6, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/IfF;->a(LX/IfF;LX/If1;LX/Ieu;LX/IfP;LX/IfT;Ljava/util/concurrent/Executor;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V
    .locals 2

    .prologue
    .line 2599485
    if-eqz p2, :cond_0

    .line 2599486
    iget-object v0, p0, LX/IfF;->d:LX/IfT;

    const-string v1, "cymk_notice_accepted"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599487
    :cond_0
    invoke-direct {p0, p1}, LX/IfF;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599488
    iget-object v0, p0, LX/IfF;->b:LX/Ieu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Ieu;->a(Z)V

    .line 2599489
    return-void
.end method

.method public static b$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V
    .locals 5

    .prologue
    .line 2599468
    if-eqz p2, :cond_0

    .line 2599469
    iget-object v0, p0, LX/IfF;->d:LX/IfT;

    const-string v1, "cymk_notice_declined"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599470
    :cond_0
    iget-object v0, p0, LX/IfF;->a:LX/If1;

    const/4 v1, 0x0

    .line 2599471
    iget-object v2, v0, LX/If1;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v1, v0, LX/If1;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599472
    iget-object p0, v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-object v1, p0

    .line 2599473
    iget-object v1, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599474
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2599475
    iget-object p0, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599476
    iget-object p2, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p2

    .line 2599477
    invoke-static {v1, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2599478
    iget-object v1, v0, LX/If1;->d:Ljava/util/Set;

    iget-object v2, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599479
    iget-object v4, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2599480
    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2599481
    invoke-virtual {v0, v3}, LX/1OM;->i_(I)V

    .line 2599482
    :cond_1
    return-void

    .line 2599483
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 2599484
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method private setupAdapterListener(LX/0gc;)V
    .locals 2

    .prologue
    .line 2599465
    iget-object v0, p0, LX/IfF;->a:LX/If1;

    new-instance v1, LX/IfC;

    invoke-direct {v1, p0, p1}, LX/IfC;-><init>(LX/IfF;LX/0gc;)V

    .line 2599466
    iput-object v1, v0, LX/If1;->c:LX/IfB;

    .line 2599467
    return-void
.end method


# virtual methods
.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2599464
    iget-object v0, p0, LX/IfF;->g:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    return-object v0
.end method

.method public getTrackableItemAdapter()LX/Dct;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Dct",
            "<",
            "Lcom/facebook/messaging/inbox2/items/InboxTrackableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2599463
    iget-object v0, p0, LX/IfF;->a:LX/If1;

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x247fbf65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2599460
    iget-object v1, p0, LX/IfF;->c:LX/IfP;

    iget-object v2, p0, LX/IfF;->j:LX/If7;

    invoke-virtual {v1, v2}, LX/IfP;->b(LX/If7;)V

    .line 2599461
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2599462
    const/16 v1, 0x2d

    const v2, -0x66700eab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setData(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2599454
    iget-object v0, p0, LX/IfF;->a:LX/If1;

    .line 2599455
    iput-object p1, v0, LX/If1;->b:LX/0Px;

    .line 2599456
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, v0, LX/If1;->d:Ljava/util/Set;

    .line 2599457
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2599458
    iget-object v0, p0, LX/IfF;->d:LX/IfT;

    const-string v1, "INBOX2"

    iget-object v2, p0, LX/IfF;->b:LX/Ieu;

    const-string v3, "INBOX2"

    invoke-virtual {v2, v3}, LX/Ieu;->a(Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/IfT;->a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;)V

    .line 2599459
    return-void
.end method

.method public setFragmentManager(LX/0gc;)V
    .locals 0

    .prologue
    .line 2599452
    invoke-direct {p0, p1}, LX/IfF;->setupAdapterListener(LX/0gc;)V

    .line 2599453
    return-void
.end method

.method public setListener(LX/IfE;)V
    .locals 0

    .prologue
    .line 2599450
    iput-object p1, p0, LX/IfF;->i:LX/IfE;

    .line 2599451
    return-void
.end method
