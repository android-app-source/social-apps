.class public LX/I9i;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2543383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2543384
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;LX/0Px;Z)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2543385
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2543386
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    move v6, v2

    :goto_0
    if-ge v6, v8, :cond_d

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    .line 2543387
    if-nez p2, :cond_0

    .line 2543388
    new-instance v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2543389
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2543390
    :cond_0
    sget-object v1, LX/I9h;->a:[I

    invoke-virtual {v0}, LX/Blc;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 2543391
    new-instance v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;)V

    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2543392
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aB()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailMembersModel;->a()I

    move-result v1

    .line 2543393
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aK()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsMembersModel;->a()I

    move-result v3

    .line 2543394
    :goto_3
    new-instance v9, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_4
    add-int/2addr v1, v3

    invoke-direct {v9, v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;Ljava/lang/Integer;I)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    :cond_1
    move v1, v2

    .line 2543395
    goto :goto_2

    :cond_2
    move v3, v2

    .line 2543396
    goto :goto_3

    :cond_3
    move-object v4, v5

    .line 2543397
    goto :goto_4

    .line 2543398
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->ay()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailAssociatesModel;->a()I

    move-result v1

    .line 2543399
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aH()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsAssociatesModel;->a()I

    move-result v3

    .line 2543400
    :goto_6
    new-instance v9, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_7
    add-int/2addr v1, v3

    invoke-direct {v9, v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;Ljava/lang/Integer;I)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    :cond_4
    move v1, v2

    .line 2543401
    goto :goto_5

    :cond_5
    move v3, v2

    .line 2543402
    goto :goto_6

    :cond_6
    move-object v4, v5

    .line 2543403
    goto :goto_7

    .line 2543404
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aA()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailInviteesModel;->a()I

    move-result v1

    .line 2543405
    :goto_8
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aJ()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsInviteesModel;->a()I

    move-result v3

    .line 2543406
    :goto_9
    new-instance v9, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_a
    add-int/2addr v1, v3

    invoke-direct {v9, v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;Ljava/lang/Integer;I)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    :cond_7
    move v1, v2

    .line 2543407
    goto :goto_8

    :cond_8
    move v3, v2

    .line 2543408
    goto :goto_9

    :cond_9
    move-object v4, v5

    .line 2543409
    goto :goto_a

    .line 2543410
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->az()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventEmailDeclinesModel;->a()I

    move-result v1

    .line 2543411
    :goto_b
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aI()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventSmsDeclinesModel;->a()I

    move-result v3

    .line 2543412
    :goto_c
    new-instance v9, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v4

    if-eqz v4, :cond_c

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aw()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventDeclinesModel;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_d
    add-int/2addr v1, v3

    invoke-direct {v9, v0, v4, v1}, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;-><init>(LX/Blc;Ljava/lang/Integer;I)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    :cond_a
    move v1, v2

    .line 2543413
    goto :goto_b

    :cond_b
    move v3, v2

    .line 2543414
    goto :goto_c

    :cond_c
    move-object v4, v5

    .line 2543415
    goto :goto_d

    .line 2543416
    :cond_d
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(ZZZ)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)",
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2543417
    if-eqz p0, :cond_0

    .line 2543418
    sget-object v0, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    sget-object v1, LX/Blc;->PUBLIC_GOING:LX/Blc;

    sget-object v2, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2543419
    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    sget-object v0, LX/Blc;->PRIVATE_GOING:LX/Blc;

    sget-object v1, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    sget-object v2, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    sget-object v3, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, LX/Blc;->PRIVATE_GOING:LX/Blc;

    sget-object v1, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    sget-object v2, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2543420
    if-nez p0, :cond_1

    .line 2543421
    :cond_0
    :goto_0
    return v0

    .line 2543422
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aL()Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-result-object v1

    .line 2543423
    if-eqz v1, :cond_0

    .line 2543424
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;->r()Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    move-result-object v1

    .line 2543425
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->SEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventSeenState;->UNSEEN:Lcom/facebook/graphql/enums/GraphQLEventSeenState;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
