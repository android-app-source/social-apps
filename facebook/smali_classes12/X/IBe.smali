.class public final LX/IBe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/model/Event;

.field public final synthetic b:LX/IBg;


# direct methods
.method public constructor <init>(LX/IBg;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2547351
    iput-object p1, p0, LX/IBe;->b:LX/IBg;

    iput-object p2, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .prologue
    .line 2547352
    iget-object v0, p0, LX/IBe;->b:LX/IBg;

    iget-object v0, v0, LX/IBg;->c:LX/1nQ;

    const-string v1, "event_location_summary_open_maps"

    iget-object v2, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    .line 2547353
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2547354
    iget-object v3, p0, LX/IBe;->b:LX/IBg;

    iget-object v3, v3, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547355
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2547356
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    iget-object v4, p0, LX/IBe;->b:LX/IBg;

    iget-object v4, v4, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547357
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2547358
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1nQ;->c(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2547359
    iget-object v0, p0, LX/IBe;->b:LX/IBg;

    iget-object v1, v0, LX/IBg;->d:LX/6Zi;

    iget-object v0, p0, LX/IBe;->b:LX/IBg;

    invoke-virtual {v0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "native_event"

    iget-object v0, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->S()D

    move-result-wide v4

    iget-object v0, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->T()D

    move-result-wide v6

    iget-object v0, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    .line 2547360
    iget-object v8, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v8, v8

    .line 2547361
    iget-object v0, p0, LX/IBe;->a:Lcom/facebook/events/model/Event;

    .line 2547362
    iget-object v9, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v9, v9

    .line 2547363
    invoke-virtual/range {v1 .. v9}, LX/6Zi;->a(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 2547364
    return-void
.end method
