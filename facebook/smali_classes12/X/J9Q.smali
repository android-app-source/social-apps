.class public LX/J9Q;
.super LX/B0V;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:LX/J90;

.field private final d:LX/J9L;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/J90;LX/J9L;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652793
    invoke-direct {p0}, LX/B0V;-><init>()V

    .line 2652794
    iput-object p1, p0, LX/J9Q;->a:Ljava/lang/String;

    .line 2652795
    iput-object p2, p0, LX/J9Q;->b:Ljava/lang/String;

    .line 2652796
    iput-object p3, p0, LX/J9Q;->c:LX/J90;

    .line 2652797
    iput-object p4, p0, LX/J9Q;->d:LX/J9L;

    .line 2652798
    return-void
.end method

.method public static a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)LX/B0N;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2652772
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2652773
    :cond_0
    const-string v0, "CollectionInitialConnectionConfiguration"

    const-string v1, "Missing connection from section to connection"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652774
    new-instance v0, LX/B0e;

    invoke-direct {v0, p0}, LX/B0e;-><init>(LX/B0d;)V

    .line 2652775
    :goto_0
    return-object v0

    .line 2652776
    :cond_1
    new-instance v2, LX/9JH;

    invoke-direct {v2}, LX/9JH;-><init>()V

    .line 2652777
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    invoke-static {p1}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/9JH;->a(ILX/0w5;)V

    .line 2652778
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2652779
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2652780
    const-string v0, "CollectionInitialConnectionConfiguration"

    const-string v1, "Missing items connection"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652781
    new-instance v0, LX/B0e;

    invoke-direct {v0, p0}, LX/B0e;-><init>(LX/B0d;)V

    goto :goto_0

    .line 2652782
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v1

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v5

    invoke-virtual {v2, v1, v5}, LX/9JH;->a(ILX/0w5;)V

    .line 2652783
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_4

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2652784
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    invoke-static {v1}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v9

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    :goto_2
    invoke-virtual {v2, v8, v9, v1, v4}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2652785
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_3
    move-object v1, v3

    .line 2652786
    goto :goto_2

    .line 2652787
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2652788
    if-nez v0, :cond_5

    .line 2652789
    const-string v0, "CollectionInitialConnectionConfiguration"

    const-string v1, "Missing page info"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v4

    move-object v4, v3

    .line 2652790
    :goto_3
    new-instance v0, LX/B0i;

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2652791
    :cond_5
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 2652792
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_4
    move v5, v0

    move-object v4, v3

    goto :goto_3

    :cond_6
    move v0, v4

    goto :goto_4
.end method


# virtual methods
.method public final a(LX/B0d;)LX/0zO;
    .locals 5

    .prologue
    .line 2652764
    iget-object v0, p0, LX/J9Q;->d:LX/J9L;

    invoke-virtual {v0}, LX/J9L;->h()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    .line 2652765
    iget-object v1, p0, LX/J9Q;->c:LX/J90;

    iget-object v2, p0, LX/J9Q;->a:Ljava/lang/String;

    iget-object v3, p0, LX/J9Q;->b:Ljava/lang/String;

    .line 2652766
    const/4 v4, 0x4

    move v4, v4

    .line 2652767
    invoke-virtual {v1, v2, v3, v0, v4}, LX/J90;->a(Ljava/lang/String;Ljava/lang/String;II)LX/JBp;

    move-result-object v0

    .line 2652768
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 1

    .prologue
    .line 2652770
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2652771
    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-static {p1, v0}, LX/J9Q;->a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)LX/B0N;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2652769
    const-string v0, "CollectionsCollectionInitial"

    return-object v0
.end method
