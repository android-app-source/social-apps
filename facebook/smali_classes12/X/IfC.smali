.class public final LX/IfC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IfB;


# instance fields
.field public final synthetic a:LX/0gc;

.field public final synthetic b:LX/IfF;


# direct methods
.method public constructor <init>(LX/IfF;LX/0gc;)V
    .locals 0

    .prologue
    .line 2599426
    iput-object p1, p0, LX/IfC;->b:LX/IfF;

    iput-object p2, p0, LX/IfC;->a:LX/0gc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 4

    .prologue
    .line 2599427
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->d:LX/IfT;

    const-string v1, "INBOX2"

    invoke-virtual {v0, v1, p1}, LX/IfT;->a(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599428
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ie8;

    iget-object v1, p0, LX/IfC;->b:LX/IfF;

    invoke-virtual {v1}, LX/IfF;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/IfC;->a:LX/0gc;

    iget-object v3, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v0, v1, v2, v3}, LX/Ie8;->a(Landroid/content/Context;LX/0gc;Lcom/facebook/user/model/User;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/IfA;

    invoke-direct {v1, p0, p1}, LX/IfA;-><init>(LX/IfC;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    iget-object v2, p0, LX/IfC;->b:LX/IfF;

    iget-object v2, v2, LX/IfF;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2599429
    return-void
.end method

.method public final b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 2

    .prologue
    .line 2599430
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->d:LX/IfT;

    const-string v1, "INBOX2"

    invoke-virtual {v0, v1, p1}, LX/IfT;->b(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599431
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->a:LX/If1;

    invoke-virtual {v0, p1}, LX/If1;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599432
    return-void
.end method

.method public final c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 4

    .prologue
    .line 2599433
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->c:LX/IfP;

    const-string v1, "INBOX2"

    iget-object v2, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599434
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2599435
    invoke-virtual {v0, v1, v2}, LX/IfP;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599436
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->d:LX/IfT;

    const-string v1, "INBOX2"

    invoke-virtual {v0, v1, p1}, LX/IfT;->c(Ljava/lang/String;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599437
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->a:LX/If1;

    invoke-virtual {v0, p1}, LX/If1;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599438
    return-void
.end method

.method public final d(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Z
    .locals 1

    .prologue
    .line 2599439
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->a:LX/If1;

    invoke-virtual {v0, p1}, LX/If1;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    move-result-object v0

    .line 2599440
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->i:LX/IfE;

    if-eqz v0, :cond_0

    .line 2599441
    iget-object v0, p0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->i:LX/IfE;

    invoke-interface {v0}, LX/IfE;->a()Z

    move-result v0

    .line 2599442
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
