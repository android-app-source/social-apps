.class public final LX/I5I;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I4v;

.field public final synthetic b:LX/I5J;


# direct methods
.method public constructor <init>(LX/I5J;LX/I4v;)V
    .locals 0

    .prologue
    .line 2535246
    iput-object p1, p0, LX/I5I;->b:LX/I5J;

    iput-object p2, p0, LX/I5I;->a:LX/I4v;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2535234
    iget-object v0, p0, LX/I5I;->a:LX/I4v;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I4v;->a(Ljava/util/List;)V

    .line 2535235
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2535236
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2535237
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535238
    if-eqz v0, :cond_0

    .line 2535239
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535240
    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535241
    iget-object v1, p0, LX/I5I;->a:LX/I4v;

    .line 2535242
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535243
    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventDiscoveryCategoryListModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/I4v;->a(Ljava/util/List;)V

    .line 2535244
    :goto_0
    return-void

    .line 2535245
    :cond_0
    iget-object v0, p0, LX/I5I;->a:LX/I4v;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I4v;->a(Ljava/util/List;)V

    goto :goto_0
.end method
