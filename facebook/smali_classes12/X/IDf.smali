.class public final LX/IDf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "LX/IDH;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendlist/fragment/MutualFriendListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendlist/fragment/MutualFriendListFragment;)V
    .locals 0

    .prologue
    .line 2551068
    iput-object p1, p0, LX/IDf;->a:Lcom/facebook/friendlist/fragment/MutualFriendListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2551069
    check-cast p1, LX/IDH;

    .line 2551070
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/IDH;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
