.class public final LX/IVB;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IVC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public b:LX/IRb;

.field public final synthetic c:LX/IVC;


# direct methods
.method public constructor <init>(LX/IVC;)V
    .locals 1

    .prologue
    .line 2581491
    iput-object p1, p0, LX/IVB;->c:LX/IVC;

    .line 2581492
    move-object v0, p1

    .line 2581493
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2581494
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2581495
    const-string v0, "GroupJoinComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2581496
    if-ne p0, p1, :cond_1

    .line 2581497
    :cond_0
    :goto_0
    return v0

    .line 2581498
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2581499
    goto :goto_0

    .line 2581500
    :cond_3
    check-cast p1, LX/IVB;

    .line 2581501
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2581502
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2581503
    if-eq v2, v3, :cond_0

    .line 2581504
    iget-object v2, p0, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v3, p1, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2581505
    goto :goto_0

    .line 2581506
    :cond_5
    iget-object v2, p1, LX/IVB;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-nez v2, :cond_4

    .line 2581507
    :cond_6
    iget-object v2, p0, LX/IVB;->b:LX/IRb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/IVB;->b:LX/IRb;

    iget-object v3, p1, LX/IVB;->b:LX/IRb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2581508
    goto :goto_0

    .line 2581509
    :cond_7
    iget-object v2, p1, LX/IVB;->b:LX/IRb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
