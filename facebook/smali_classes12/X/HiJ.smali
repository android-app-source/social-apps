.class public final LX/HiJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;)V
    .locals 0

    .prologue
    .line 2497231
    iput-object p1, p0, LX/HiJ;->a:Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Address;)V
    .locals 3
    .param p1    # Landroid/location/Address;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2497225
    if-eqz p1, :cond_0

    .line 2497226
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2497227
    const-string v1, "selected_address"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2497228
    iget-object v1, p0, LX/HiJ;->a:Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->setResult(ILandroid/content/Intent;)V

    .line 2497229
    :cond_0
    iget-object v0, p0, LX/HiJ;->a:Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;

    invoke-virtual {v0}, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;->finish()V

    .line 2497230
    return-void
.end method
