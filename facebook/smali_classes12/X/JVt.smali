.class public final LX/JVt;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JVv;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JVu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JVv",
            "<TE;>.PagesStoriesYouMissedHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JVv;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JVv;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2701747
    iput-object p1, p0, LX/JVt;->b:LX/JVv;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2701748
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JVt;->c:[Ljava/lang/String;

    .line 2701749
    iput v3, p0, LX/JVt;->d:I

    .line 2701750
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JVt;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JVt;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JVt;LX/1De;IILX/JVu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JVv",
            "<TE;>.PagesStoriesYouMissedHeaderComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2701761
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2701762
    iput-object p4, p0, LX/JVt;->a:LX/JVu;

    .line 2701763
    iget-object v0, p0, LX/JVt;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2701764
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2701765
    invoke-super {p0}, LX/1X5;->a()V

    .line 2701766
    const/4 v0, 0x0

    iput-object v0, p0, LX/JVt;->a:LX/JVu;

    .line 2701767
    iget-object v0, p0, LX/JVt;->b:LX/JVv;

    iget-object v0, v0, LX/JVv;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2701768
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JVv;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2701751
    iget-object v1, p0, LX/JVt;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JVt;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JVt;->d:I

    if-ge v1, v2, :cond_2

    .line 2701752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2701753
    :goto_0
    iget v2, p0, LX/JVt;->d:I

    if-ge v0, v2, :cond_1

    .line 2701754
    iget-object v2, p0, LX/JVt;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2701755
    iget-object v2, p0, LX/JVt;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2701756
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2701757
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2701758
    :cond_2
    iget-object v0, p0, LX/JVt;->a:LX/JVu;

    .line 2701759
    invoke-virtual {p0}, LX/JVt;->a()V

    .line 2701760
    return-object v0
.end method
