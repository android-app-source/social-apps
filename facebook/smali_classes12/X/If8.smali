.class public final LX/If8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/If7;


# instance fields
.field public final synthetic a:LX/IfF;


# direct methods
.method public constructor <init>(LX/IfF;)V
    .locals 0

    .prologue
    .line 2599384
    iput-object p1, p0, LX/If8;->a:LX/IfF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;LX/IfN;)V
    .locals 7

    .prologue
    .line 2599385
    iget-object v0, p0, LX/If8;->a:LX/IfF;

    iget-object v0, v0, LX/IfF;->a:LX/If1;

    const/4 v1, 0x0

    .line 2599386
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2599387
    iget-object v2, v0, LX/If1;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v1, v0, LX/If1;->b:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;

    .line 2599388
    iget-object v6, v1, Lcom/facebook/messaging/contactsyoumayknow/InboxContactsYouMayKnowUserItem;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-object v6, v6

    .line 2599389
    iget-object v6, v6, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599390
    iget-object p0, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, p0

    .line 2599391
    iget-object p0, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599392
    iget-object p2, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p2

    .line 2599393
    invoke-static {v6, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2599394
    invoke-virtual {v0, v2}, LX/1OM;->d(I)V

    move v1, v2

    .line 2599395
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 2599396
    :cond_0
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2599397
    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    .line 2599398
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/If1;->b:LX/0Px;

    .line 2599399
    return-void
.end method
