.class public LX/I1H;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/I1K;

.field private final d:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0m9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2528809
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f0d01a3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d01a4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/I1H;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I1K;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528810
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2528811
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I1H;->e:Ljava/util/List;

    .line 2528812
    iput-object p2, p0, LX/I1H;->b:Landroid/content/Context;

    .line 2528813
    iput-object p1, p0, LX/I1H;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528814
    iput-object p3, p0, LX/I1H;->c:LX/I1K;

    .line 2528815
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 11

    .prologue
    .line 2528816
    const v0, 0x7f0d01a3

    if-ne p2, v0, :cond_0

    .line 2528817
    new-instance v0, LX/I1P;

    new-instance v1, Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/I1P;-><init>(Lcom/facebook/fig/header/FigHeader;)V

    .line 2528818
    :goto_0
    return-object v0

    .line 2528819
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2528820
    const v1, 0x7f0304d4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2528821
    iget-object v1, p0, LX/I1H;->c:LX/I1K;

    iget-object v2, p0, LX/I1H;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528822
    new-instance v3, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    const-class v4, Landroid/content/Context;

    invoke-interface {v1, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v7

    check-cast v7, LX/1Ad;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v1}, LX/I53;->b(LX/0QB;)LX/I53;

    move-result-object v9

    check-cast v9, LX/I53;

    invoke-static {v1}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v10

    check-cast v10, LX/1nQ;

    move-object v4, v2

    move-object v5, v0

    invoke-direct/range {v3 .. v10}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/fig/listitem/FigListItem;Landroid/content/Context;LX/1Ad;LX/0wM;LX/I53;LX/1nQ;)V

    .line 2528823
    move-object v0, v3

    .line 2528824
    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2528825
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01a3

    if-ne v0, v1, :cond_1

    .line 2528826
    check-cast p1, LX/I1P;

    .line 2528827
    iget-object v0, p0, LX/I1H;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/I1P;->a(Ljava/lang/String;)V

    .line 2528828
    :cond_0
    :goto_0
    return-void

    .line 2528829
    :cond_1
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01a4

    if-ne v0, v1, :cond_0

    .line 2528830
    check-cast p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;

    .line 2528831
    iget-object v0, p0, LX/I1H;->e:Ljava/util/List;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;

    .line 2528832
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2528833
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->k()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/I1H;->f:LX/0m9;

    .line 2528834
    iget-object v4, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->m:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v4, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2528835
    iget-object v4, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->t:LX/1Ad;

    sget-object p0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    .line 2528836
    iget-object p0, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->n:LX/1aX;

    invoke-virtual {p0, v4}, LX/1aX;->a(LX/1aZ;)V

    .line 2528837
    iget-object v4, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->u:LX/0wM;

    iget-object p0, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->n:LX/1aX;

    invoke-virtual {p0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const p2, -0x6f6b64

    invoke-virtual {v4, p0, p2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2528838
    iget-object p0, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->m:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2528839
    iput-object v3, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->o:Ljava/lang/String;

    .line 2528840
    iput-object v0, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->p:Ljava/lang/String;

    .line 2528841
    iput-object v2, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->q:LX/0m9;

    .line 2528842
    iget-object v4, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->m:Lcom/facebook/fig/listitem/FigListItem;

    iget-object p0, p1, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardCategoryViewHolder;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, p0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528843
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2528844
    if-nez p1, :cond_0

    .line 2528845
    const v0, 0x7f0d01a3

    .line 2528846
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d01a4

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2528847
    iget-object v0, p0, LX/I1H;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2528848
    const/4 v0, 0x0

    .line 2528849
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/I1H;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
