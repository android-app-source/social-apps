.class public LX/IBQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/IBO;

.field private static volatile i:LX/IBQ;


# instance fields
.field public final b:LX/0So;

.field public final c:LX/11i;

.field public final d:LX/0id;

.field private e:Z

.field public f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2547085
    new-instance v0, LX/IBO;

    invoke-direct {v0}, LX/IBO;-><init>()V

    sput-object v0, LX/IBQ;->a:LX/IBO;

    return-void
.end method

.method public constructor <init>(LX/0So;LX/11i;LX/0id;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2547076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2547077
    iput-boolean v0, p0, LX/IBQ;->e:Z

    .line 2547078
    iput-boolean v0, p0, LX/IBQ;->f:Z

    .line 2547079
    iput-boolean v0, p0, LX/IBQ;->g:Z

    .line 2547080
    iput-boolean v0, p0, LX/IBQ;->h:Z

    .line 2547081
    iput-object p1, p0, LX/IBQ;->b:LX/0So;

    .line 2547082
    iput-object p2, p0, LX/IBQ;->c:LX/11i;

    .line 2547083
    iput-object p3, p0, LX/IBQ;->d:LX/0id;

    .line 2547084
    return-void
.end method

.method public static a(LX/0QB;)LX/IBQ;
    .locals 6

    .prologue
    .line 2547063
    sget-object v0, LX/IBQ;->i:LX/IBQ;

    if-nez v0, :cond_1

    .line 2547064
    const-class v1, LX/IBQ;

    monitor-enter v1

    .line 2547065
    :try_start_0
    sget-object v0, LX/IBQ;->i:LX/IBQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2547066
    if-eqz v2, :cond_0

    .line 2547067
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2547068
    new-instance p0, LX/IBQ;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v5

    check-cast v5, LX/0id;

    invoke-direct {p0, v3, v4, v5}, LX/IBQ;-><init>(LX/0So;LX/11i;LX/0id;)V

    .line 2547069
    move-object v0, p0

    .line 2547070
    sput-object v0, LX/IBQ;->i:LX/IBQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547071
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2547072
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2547073
    :cond_1
    sget-object v0, LX/IBQ;->i:LX/IBQ;

    return-object v0

    .line 2547074
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2547075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/IBQ;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/IBO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2547062
    iget-object v0, p0, LX/IBQ;->c:LX/11i;

    sget-object v1, LX/IBQ;->a:LX/IBO;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2547057
    iput-boolean v0, p0, LX/IBQ;->e:Z

    .line 2547058
    iput-boolean v0, p0, LX/IBQ;->f:Z

    .line 2547059
    iput-boolean v0, p0, LX/IBQ;->g:Z

    .line 2547060
    iput-boolean v0, p0, LX/IBQ;->h:Z

    .line 2547061
    return-void
.end method

.method public static f(LX/IBQ;)V
    .locals 6

    .prologue
    .line 2547053
    iget-boolean v0, p0, LX/IBQ;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/IBQ;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/IBQ;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/IBQ;->h:Z

    if-eqz v0, :cond_0

    .line 2547054
    invoke-direct {p0}, LX/IBQ;->e()V

    .line 2547055
    iget-object v0, p0, LX/IBQ;->c:LX/11i;

    sget-object v1, LX/IBQ;->a:LX/IBO;

    const/4 v2, 0x0

    iget-object v3, p0, LX/IBQ;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface {v0, v1, v2, v4, v5}, LX/11i;->b(LX/0Pq;LX/0P1;J)V

    .line 2547056
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/IBP;)J
    .locals 9

    .prologue
    .line 2547086
    const/4 v0, 0x0

    .line 2547087
    invoke-static {p0}, LX/IBQ;->d(LX/IBQ;)LX/11o;

    move-result-object v2

    .line 2547088
    iget-object v3, p0, LX/IBQ;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v6

    .line 2547089
    if-eqz v2, :cond_0

    .line 2547090
    invoke-virtual {p1}, LX/IBP;->name()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const v8, 0x5aa55aa7

    move-object v5, v0

    invoke-static/range {v2 .. v8}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2547091
    :cond_0
    move-wide v0, v6

    .line 2547092
    return-wide v0
.end method

.method public final a(LX/IBP;J)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2547049
    invoke-static {p0}, LX/IBQ;->d(LX/IBQ;)LX/11o;

    move-result-object v0

    .line 2547050
    if-nez v0, :cond_0

    .line 2547051
    :goto_0
    return-void

    .line 2547052
    :cond_0
    invoke-virtual {p1}, LX/IBP;->name()Ljava/lang/String;

    move-result-object v1

    const v6, -0x50c11c9d

    move-object v3, v2

    move-wide v4, p2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method

.method public final a(LX/IBP;LX/0P1;)V
    .locals 8
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IBP;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 2547033
    invoke-static {p0}, LX/IBQ;->d(LX/IBQ;)LX/11o;

    move-result-object v0

    .line 2547034
    if-nez v0, :cond_1

    .line 2547035
    :cond_0
    :goto_0
    return-void

    .line 2547036
    :cond_1
    invoke-virtual {p1}, LX/IBP;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/IBQ;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x68ee56cd

    move-object v3, p2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2547037
    sget-object v0, LX/IBP;->RENDERING:LX/IBP;

    if-ne p1, v0, :cond_2

    .line 2547038
    iput-boolean v7, p0, LX/IBQ;->e:Z

    .line 2547039
    invoke-static {p0}, LX/IBQ;->f(LX/IBQ;)V

    goto :goto_0

    .line 2547040
    :cond_2
    sget-object v0, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    if-ne p1, v0, :cond_3

    .line 2547041
    iput-boolean v7, p0, LX/IBQ;->f:Z

    .line 2547042
    invoke-static {p0}, LX/IBQ;->f(LX/IBQ;)V

    goto :goto_0

    .line 2547043
    :cond_3
    sget-object v0, LX/IBP;->CACHED_STORIES_INITIAL_FETCH:LX/IBP;

    if-ne p1, v0, :cond_4

    .line 2547044
    iput-boolean v7, p0, LX/IBQ;->g:Z

    .line 2547045
    invoke-static {p0}, LX/IBQ;->f(LX/IBQ;)V

    goto :goto_0

    .line 2547046
    :cond_4
    sget-object v0, LX/IBP;->FRESH_STORIES_INITIAL_FETCH:LX/IBP;

    if-ne p1, v0, :cond_0

    .line 2547047
    iput-boolean v7, p0, LX/IBQ;->h:Z

    .line 2547048
    invoke-static {p0}, LX/IBQ;->f(LX/IBQ;)V

    goto :goto_0
.end method

.method public final b(LX/IBP;)V
    .locals 14

    .prologue
    .line 2547024
    invoke-static {p0}, LX/IBQ;->d(LX/IBQ;)LX/11o;

    move-result-object v1

    .line 2547025
    if-nez v1, :cond_0

    .line 2547026
    :goto_0
    return-void

    .line 2547027
    :cond_0
    invoke-virtual {p1}, LX/IBP;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v0, p0, LX/IBQ;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x1e58d8a3

    const/16 v7, 0x8

    .line 2547028
    invoke-interface {v1, v2, v3, v4, v5}, LX/11o;->a(Ljava/lang/String;Ljava/lang/String;J)LX/11o;

    .line 2547029
    invoke-static {v7}, Lcom/facebook/loom/core/TraceEvents;->a(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 2547030
    :goto_1
    goto :goto_0

    .line 2547031
    :cond_1
    const/16 v8, 0x1d

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v10

    invoke-static {v1}, LX/096;->a(LX/11o;)J

    move-result-wide v11

    move v9, v6

    invoke-static/range {v7 .. v12}, Lcom/facebook/loom/logger/Logger;->a(IIIIJ)I

    .line 2547032
    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2547021
    invoke-direct {p0}, LX/IBQ;->e()V

    .line 2547022
    iget-object v0, p0, LX/IBQ;->c:LX/11i;

    sget-object v1, LX/IBQ;->a:LX/IBO;

    invoke-interface {v0, v1}, LX/11i;->d(LX/0Pq;)V

    .line 2547023
    return-void
.end method

.method public final c(LX/IBP;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2547017
    invoke-static {p0}, LX/IBQ;->d(LX/IBQ;)LX/11o;

    move-result-object v0

    .line 2547018
    if-nez v0, :cond_0

    .line 2547019
    :goto_0
    return-void

    .line 2547020
    :cond_0
    invoke-virtual {p1}, LX/IBP;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, LX/IBQ;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x35c42f4

    move-object v3, v2

    invoke-static/range {v0 .. v6}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    goto :goto_0
.end method

.method public final d(LX/IBP;)V
    .locals 1

    .prologue
    .line 2547015
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/IBQ;->a(LX/IBP;LX/0P1;)V

    .line 2547016
    return-void
.end method
