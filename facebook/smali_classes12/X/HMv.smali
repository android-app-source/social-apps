.class public final LX/HMv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 0

    .prologue
    .line 2457661
    iput-object p1, p0, LX/HMv;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x53564b05

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457662
    iget-object v1, p0, LX/HMv;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457663
    iget-object v3, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v4, LX/0ig;->ah:LX/0ih;

    const-string v5, "tap_add_edit_button"

    invoke-virtual {v3, v4, v5}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2457664
    iget-object v3, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->i:LX/0Uh;

    const/16 v4, 0x5ba

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2457665
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2457666
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2457667
    const-string v5, "page_id"

    iget-object v6, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v7, v6, LX/HN5;->a:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457668
    const-string v5, "init_props"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2457669
    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    .line 2457670
    iget-object v4, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->h:LX/17W;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, LX/0ax;->fy:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2457671
    :goto_0
    const v1, 0x41d78eea

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2457672
    :cond_0
    sget-object v3, LX/8Dq;->I:Ljava/lang/String;

    iget-object v4, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v5, v4, LX/HN5;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2457673
    iget-object v3, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v3, v5, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2457674
    iget-object v3, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
