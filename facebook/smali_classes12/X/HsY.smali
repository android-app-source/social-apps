.class public LX/HsY;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsFacecastSupported;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$SetsInlineSproutsState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/Hr2;

.field public final c:LX/Hsb;

.field private final d:LX/0iA;

.field private final e:Landroid/content/res/Resources;

.field private f:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;LX/Hsc;LX/0iA;Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "LX/Hsc;",
            "LX/0iA;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514629
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2514630
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HsY;->a:Ljava/lang/ref/WeakReference;

    .line 2514631
    iput-object p2, p0, LX/HsY;->b:LX/Hr2;

    .line 2514632
    new-instance v1, LX/Hsb;

    check-cast p1, LX/0il;

    invoke-static {p3}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v0

    check-cast v0, LX/3kp;

    invoke-direct {v1, p1, v0}, LX/Hsb;-><init>(LX/0il;LX/3kp;)V

    .line 2514633
    move-object v0, v1

    .line 2514634
    iput-object v0, p0, LX/HsY;->c:LX/Hsb;

    .line 2514635
    iput-object p4, p0, LX/HsY;->d:LX/0iA;

    .line 2514636
    iput-object p5, p0, LX/HsY;->e:Landroid/content/res/Resources;

    .line 2514637
    invoke-static {p0}, LX/HsY;->h(LX/HsY;)V

    .line 2514638
    return-void
.end method

.method public static h(LX/HsY;)V
    .locals 4

    .prologue
    .line 2514639
    iget-object v0, p0, LX/HsY;->d:LX/0iA;

    sget-object v1, LX/3ne;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3ne;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ne;

    .line 2514640
    const/4 v1, 0x0

    .line 2514641
    if-eqz v0, :cond_0

    .line 2514642
    iget-object v1, p0, LX/HsY;->e:Landroid/content/res/Resources;

    const v2, 0x7f0813fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2514643
    iget-object v2, v0, LX/3ne;->b:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    const-string v3, "4533"

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2514644
    move-object v0, v1

    .line 2514645
    :goto_0
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v1

    const v2, 0x7f02090e

    .line 2514646
    iput v2, v1, LX/Htz;->a:I

    .line 2514647
    move-object v1, v1

    .line 2514648
    const v2, 0x7f0a04c4

    .line 2514649
    iput v2, v1, LX/Htz;->f:I

    .line 2514650
    move-object v1, v1

    .line 2514651
    iget-object v2, p0, LX/HsY;->e:Landroid/content/res/Resources;

    const v3, 0x7f0813fa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2514652
    iput-object v2, v1, LX/Htz;->b:Ljava/lang/String;

    .line 2514653
    move-object v1, v1

    .line 2514654
    iput-object v0, v1, LX/Htz;->c:Ljava/lang/String;

    .line 2514655
    move-object v0, v1

    .line 2514656
    invoke-virtual {p0}, LX/HsY;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2514657
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2514658
    move-object v0, v0

    .line 2514659
    new-instance v1, LX/HsX;

    invoke-direct {v1, p0}, LX/HsX;-><init>(LX/HsY;)V

    .line 2514660
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2514661
    move-object v0, v0

    .line 2514662
    iget-object v1, p0, LX/HsY;->c:LX/Hsb;

    .line 2514663
    iput-object v1, v0, LX/Htz;->g:LX/Hsa;

    .line 2514664
    move-object v0, v0

    .line 2514665
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/HsY;->f:LX/Hu0;

    .line 2514666
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2514667
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2514668
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2514669
    iget-object v0, p0, LX/HsY;->e:Landroid/content/res/Resources;

    const v1, 0x7f0812b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2514670
    iget-object v0, p0, LX/HsY;->f:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2514671
    iget-object v0, p0, LX/HsY;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HsY;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2514672
    const/4 v0, 0x0

    return v0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2514673
    sget-object v0, LX/Hty;->FACECAST:LX/Hty;

    return-object v0
.end method
