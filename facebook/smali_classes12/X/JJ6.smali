.class public final LX/JJ6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2678919
    iput-object p1, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, 0x614a0668

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2678920
    const v0, 0x7f0d0224

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJD;

    .line 2678921
    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-static {v2, v0}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->a$redex0(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;LX/JJD;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2678922
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    const v3, 0x7f083a74

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678923
    const v0, -0x5fa5c1e1

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2678924
    :goto_0
    return-void

    .line 2678925
    :cond_0
    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    const/4 v3, 0x0

    .line 2678926
    iput-object v3, v2, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    .line 2678927
    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2678928
    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a()V

    .line 2678929
    iget-object v2, p0, LX/JJ6;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 2678930
    const v0, -0x3f284449

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
