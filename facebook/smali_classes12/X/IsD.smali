.class public final LX/IsD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/IsJ;


# direct methods
.method public constructor <init>(LX/IsJ;)V
    .locals 0

    .prologue
    .line 2619390
    iput-object p1, p0, LX/IsD;->a:LX/IsJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2619391
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2619389
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2619383
    iget-object v0, p0, LX/IsD;->a:LX/IsJ;

    iget-object v0, v0, LX/IsJ;->c:Lcom/facebook/messaging/photos/editing/LayerEditText;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 2619384
    iget-object v1, p0, LX/IsD;->a:LX/IsJ;

    iget-object v1, v1, LX/IsJ;->b:LX/1zC;

    invoke-virtual {v1, v0, p2, p4}, LX/1zC;->a(Landroid/text/Editable;II)Z

    .line 2619385
    iget-object v1, p0, LX/IsD;->a:LX/IsJ;

    iget-object v1, v1, LX/IsJ;->g:LX/IsC;

    .line 2619386
    iput-object v0, v1, LX/IsC;->g:Ljava/lang/CharSequence;

    .line 2619387
    sget-object p0, LX/IsB;->TEXT_CHANGE:LX/IsB;

    invoke-virtual {v1, p0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2619388
    return-void
.end method
