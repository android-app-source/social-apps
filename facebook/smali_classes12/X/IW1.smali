.class public final LX/IW1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupConfirmMutationModels$GroupConfirmFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final synthetic b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic c:LX/IW3;


# direct methods
.method public constructor <init>(LX/IW3;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 0

    .prologue
    .line 2583334
    iput-object p1, p0, LX/IW1;->c:LX/IW3;

    iput-object p2, p0, LX/IW1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p3, p0, LX/IW1;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2583335
    iget-object v0, p0, LX/IW1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    move-result v0

    if-lez v0, :cond_0

    .line 2583336
    iget-object v0, p0, LX/IW1;->c:LX/IW3;

    iget-object v1, p0, LX/IW1;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IW1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {v0, v1, v2}, LX/IW3;->a$redex0(LX/IW3;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 2583337
    :cond_0
    iget-object v0, p0, LX/IW1;->c:LX/IW3;

    iget-object v0, v0, LX/IW3;->i:LX/03V;

    sget-object v1, LX/IW3;->a:Ljava/lang/String;

    const-string v2, "Failed to confirm group membership."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2583338
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2583339
    return-void
.end method
