.class public final LX/JH7;
.super LX/1Ah;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Ah",
        "<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JH8;


# direct methods
.method public constructor <init>(LX/JH8;)V
    .locals 0

    .prologue
    .line 2672408
    iput-object p1, p0, LX/JH7;->a:LX/JH8;

    invoke-direct {p0}, LX/1Ah;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2672409
    const/4 v1, 0x0

    .line 2672410
    iget-object v0, p0, LX/JH7;->a:LX/JH8;

    iget-object v0, v0, LX/JH8;->c:LX/1ca;

    if-nez v0, :cond_0

    .line 2672411
    :goto_0
    return-void

    .line 2672412
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/JH7;->a:LX/JH8;

    iget-object v0, v0, LX/JH8;->c:LX/1ca;

    invoke-interface {v0}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2672413
    if-eqz v0, :cond_1

    .line 2672414
    :try_start_1
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 2672415
    if-eqz v1, :cond_1

    instance-of v2, v1, LX/1ll;

    if-eqz v2, :cond_1

    .line 2672416
    check-cast v1, LX/1ll;

    .line 2672417
    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2672418
    if-eqz v1, :cond_1

    .line 2672419
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2672420
    iget-object v2, p0, LX/JH7;->a:LX/JH8;

    invoke-static {v1}, LX/690;->a(Landroid/graphics/Bitmap;)LX/68w;

    move-result-object v1

    .line 2672421
    iput-object v1, v2, LX/JH8;->l:LX/68w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2672422
    :cond_1
    iget-object v1, p0, LX/JH7;->a:LX/JH8;

    iget-object v1, v1, LX/JH8;->c:LX/1ca;

    invoke-interface {v1}, LX/1ca;->g()Z

    .line 2672423
    if-eqz v0, :cond_2

    .line 2672424
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 2672425
    :cond_2
    iget-object v0, p0, LX/JH7;->a:LX/JH8;

    invoke-virtual {v0}, LX/JH8;->a()V

    goto :goto_0

    .line 2672426
    :catchall_0
    move-exception v0

    :goto_1
    iget-object v2, p0, LX/JH7;->a:LX/JH8;

    iget-object v2, v2, LX/JH8;->c:LX/1ca;

    invoke-interface {v2}, LX/1ca;->g()Z

    .line 2672427
    if-eqz v1, :cond_3

    .line 2672428
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :cond_3
    throw v0

    .line 2672429
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1
.end method
