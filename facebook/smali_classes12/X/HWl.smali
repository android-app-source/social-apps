.class public final LX/HWl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477027
    iput-object p1, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 8

    .prologue
    .line 2477028
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->d()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2477029
    :cond_0
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-static {v0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    move-result-object v0

    .line 2477030
    if-eqz v0, :cond_1

    .line 2477031
    iget-object v1, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b(Z)V

    .line 2477032
    :cond_1
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    .line 2477033
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s:LX/9XE;

    iget-object v2, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477034
    iget-wide v4, v2, LX/HX9;->a:J

    move-wide v2, v4

    .line 2477035
    invoke-static {v0, v2, v3, v1}, LX/9XE;->c(LX/9XE;JLcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2477036
    iget-object v4, v0, LX/9XE;->a:LX/0Zb;

    sget-object v5, LX/9XH;->EVENT_PRESENCE_TAB_SWITCH:LX/9XH;

    invoke-static {v5, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2477037
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    .line 2477038
    :goto_0
    iget-object v3, v2, LX/CSN;->a:LX/CSM;

    sget-object v4, LX/CSM;->NONE:LX/CSM;

    if-ne v3, v4, :cond_5

    .line 2477039
    iget-object v3, v2, LX/CSN;->b:LX/0if;

    sget-object v4, LX/0ig;->ag:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "swipeTo_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "position:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2477040
    :cond_2
    :goto_1
    sget-object v3, LX/CSM;->NONE:LX/CSM;

    iput-object v3, v2, LX/CSN;->a:LX/CSM;

    .line 2477041
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    .line 2477042
    invoke-virtual {v0}, LX/8EI;->e()V

    .line 2477043
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v2, "HasSwitchedTab"

    invoke-virtual {v0, v2}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477044
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2477045
    iget-object v0, p0, LX/HWl;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    .line 2477046
    :cond_3
    return-void

    .line 2477047
    :cond_4
    const-string v0, "NULL"

    goto :goto_0

    .line 2477048
    :cond_5
    iget-object v3, v2, LX/CSN;->a:LX/CSM;

    sget-object v4, LX/CSM;->SEEALL_WITH_TAB:LX/CSM;

    if-ne v3, v4, :cond_2

    .line 2477049
    iget-object v3, v2, LX/CSN;->b:LX/0if;

    sget-object v4, LX/0ig;->ag:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "seeAll_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "position:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2477050
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2477051
    return-void
.end method
