.class public LX/IOD;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/39G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1nA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1xv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1xe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1xg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/text/TextPaint;

.field public h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

.field public i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2571604
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571605
    const/4 v1, 0x1

    .line 2571606
    const v0, 0x7f0302fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571607
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/IOD;

    invoke-static {v0}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v4

    check-cast v4, LX/39G;

    invoke-static {v0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v5

    check-cast v5, LX/1nI;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v6

    check-cast v6, LX/1nA;

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v7

    check-cast v7, LX/1xv;

    invoke-static {v0}, LX/1xe;->a(LX/0QB;)LX/1xe;

    move-result-object p1

    check-cast p1, LX/1xe;

    invoke-static {v0}, LX/1xg;->a(LX/0QB;)LX/1xg;

    move-result-object v0

    check-cast v0, LX/1xg;

    iput-object v4, v3, LX/IOD;->a:LX/39G;

    iput-object v5, v3, LX/IOD;->b:LX/1nI;

    iput-object v6, v3, LX/IOD;->c:LX/1nA;

    iput-object v7, v3, LX/IOD;->d:LX/1xv;

    iput-object p1, v3, LX/IOD;->e:LX/1xe;

    iput-object v0, v3, LX/IOD;->f:LX/1xg;

    .line 2571608
    invoke-virtual {p0, v1}, LX/IOD;->setOrientation(I)V

    .line 2571609
    const v0, 0x7f0d0a47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    iput-object v0, p0, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    .line 2571610
    const v0, 0x7f0d0a48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, LX/IOD;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 2571611
    iget-object v0, p0, LX/IOD;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 2571612
    iput-boolean v1, v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->s:Z

    .line 2571613
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, LX/IOD;->g:Landroid/text/TextPaint;

    .line 2571614
    iget-object v0, p0, LX/IOD;->g:Landroid/text/TextPaint;

    invoke-virtual {p0}, LX/IOD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2571615
    return-void
.end method

.method public static setThumnail(LX/IOD;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 2

    .prologue
    .line 2571616
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2571617
    :cond_0
    :goto_0
    return-void

    .line 2571618
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2571619
    iget-object v1, p0, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryImageUri(Landroid/net/Uri;)V

    .line 2571620
    iget-object v0, p0, LX/IOD;->h:Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/hotconversations/views/HotConversationStoryWithThumbnailView;->setStoryOverlayImage(LX/0Px;)V

    goto :goto_0
.end method
