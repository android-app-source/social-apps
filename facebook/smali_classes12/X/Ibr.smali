.class public final LX/Ibr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ibs;


# direct methods
.method public constructor <init>(LX/Ibs;)V
    .locals 0

    .prologue
    .line 2594833
    iput-object p1, p0, LX/Ibr;->a:LX/Ibs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2594857
    iget-object v0, p0, LX/Ibr;->a:LX/Ibs;

    iget-object v0, v0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2594858
    :goto_0
    return-void

    .line 2594859
    :cond_0
    iget-object v0, p0, LX/Ibr;->a:LX/Ibs;

    iget-object v0, v0, LX/Ibs;->b:LX/03V;

    sget-object v1, LX/Ibs;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2594834
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, -0xa51e0a3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2594835
    if-eqz p1, :cond_0

    .line 2594836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594837
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2594838
    iget-object v0, p0, LX/Ibr;->a:LX/Ibs;

    iget-object v0, v0, LX/Ibs;->b:LX/03V;

    sget-object v1, LX/Ibs;->a:Ljava/lang/String;

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594839
    :goto_2
    return-void

    .line 2594840
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594841
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2594842
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2594843
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594844
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2594845
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2594846
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2594847
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/Ibr;->a:LX/Ibs;

    iget-object v3, v3, LX/Ibs;->f:LX/IdF;

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    const/4 p1, 0x2

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 2594848
    if-eqz v0, :cond_6

    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2594849
    :cond_6
    :goto_5
    goto :goto_2

    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    .line 2594850
    :cond_8
    invoke-virtual {v1, v0, p0}, LX/15i;->j(II)I

    move-result v2

    if-eqz v2, :cond_9

    .line 2594851
    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iget-object v4, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v5, 0x7f082d76

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v0, p0}, LX/15i;->j(II)I

    move-result v7

    div-int/lit8 v7, v7, 0x3c

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedSubText(Ljava/lang/String;)V

    .line 2594852
    :goto_6
    invoke-virtual {v1, v0, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 2594853
    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v4, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v1, v0, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v5, 0x7f082d78

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v0, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-virtual {v2, v5, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v4, v2}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedSubText(Ljava/lang/String;)V

    goto :goto_5

    .line 2594854
    :cond_9
    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a()V

    goto :goto_6

    .line 2594855
    :cond_a
    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v5, 0x7f082d77

    new-array v6, p1, [Ljava/lang/Object;

    invoke-virtual {v1, v0, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v5, v6}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 2594856
    :cond_b
    iget-object v2, v3, LX/IdF;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a()V

    goto/16 :goto_5
.end method
