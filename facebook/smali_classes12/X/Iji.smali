.class public final LX/Iji;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/model/PaymentCard;

.field public final synthetic b:Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;

.field public final synthetic c:LX/Ijy;


# direct methods
.method public constructor <init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;)V
    .locals 0

    .prologue
    .line 2606065
    iput-object p1, p0, LX/Iji;->c:LX/Ijy;

    iput-object p2, p0, LX/Iji;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    iput-object p3, p0, LX/Iji;->b:Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2606042
    iget-object v0, p0, LX/Iji;->c:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->b:LX/03V;

    iget-object v1, p0, LX/Iji;->c:LX/Ijy;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to fetch email and PaymentPin for the user."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2606043
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2606044
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2606045
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2606046
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2606047
    iget-object v3, p0, LX/Iji;->c:LX/Ijy;

    iget-object v3, v3, LX/Ijy;->n:LX/6o6;

    invoke-virtual {v3}, LX/6o6;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/Iji;->c:LX/Ijy;

    iget-object v3, v3, LX/Ijy;->m:LX/6o9;

    invoke-virtual {v3}, LX/6o9;->a()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    .line 2606048
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v4

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    .line 2606049
    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    .line 2606050
    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2606051
    invoke-static {}, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->a()LX/5fs;

    move-result-object v1

    .line 2606052
    iput-boolean v3, v1, LX/5fs;->a:Z

    .line 2606053
    move-object v1, v1

    .line 2606054
    iput-boolean v4, v1, LX/5fs;->d:Z

    .line 2606055
    move-object v1, v1

    .line 2606056
    iput-boolean v2, v1, LX/5fs;->b:Z

    .line 2606057
    move-object v1, v1

    .line 2606058
    iput-boolean v0, v1, LX/5fs;->c:Z

    .line 2606059
    move-object v0, v1

    .line 2606060
    invoke-virtual {v0}, LX/5fs;->a()Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-result-object v0

    .line 2606061
    iget-object v1, p0, LX/Iji;->c:LX/Ijy;

    iget-object v1, v1, LX/Ijy;->p:LX/Ijx;

    iget-object v2, p0, LX/Iji;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-interface {v1, v2, v0}, LX/Ijx;->a(Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/auth/model/NuxFollowUpAction;)V

    .line 2606062
    return-void

    :cond_1
    move v0, v2

    .line 2606063
    goto :goto_0

    :cond_2
    move v3, v2

    .line 2606064
    goto :goto_1
.end method
