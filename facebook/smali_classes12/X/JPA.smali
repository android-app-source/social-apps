.class public LX/JPA;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mX",
        "<",
        "LX/JP9;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JPJ;

.field private final d:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final e:LX/JPk;

.field private final f:LX/JP6;

.field private final g:LX/JP2;

.field private final h:LX/JPQ;

.field private final i:LX/JOY;

.field private final j:LX/JPa;

.field private final k:LX/JOm;

.field private final l:LX/JOe;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JPJ;LX/0Px;LX/1Pc;LX/25M;LX/JPk;LX/JP6;LX/JP2;LX/JPQ;LX/JOY;LX/JPa;LX/JOm;LX/JOe;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/JPJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/JPJ;",
            "LX/0Px",
            "<",
            "LX/JP9;",
            ">;TE;",
            "LX/25M;",
            "LX/JPk;",
            "LX/JP6;",
            "LX/JP2;",
            "LX/JPQ;",
            "LX/JOY;",
            "LX/JPa;",
            "LX/JOm;",
            "LX/JOe;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689127
    move-object v0, p4

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p3, v0, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2689128
    iput-object p4, p0, LX/JPA;->d:LX/1Pc;

    .line 2689129
    iput-object p2, p0, LX/JPA;->c:LX/JPJ;

    .line 2689130
    iput-object p6, p0, LX/JPA;->e:LX/JPk;

    .line 2689131
    iput-object p7, p0, LX/JPA;->f:LX/JP6;

    .line 2689132
    iput-object p8, p0, LX/JPA;->g:LX/JP2;

    .line 2689133
    iput-object p9, p0, LX/JPA;->h:LX/JPQ;

    .line 2689134
    iput-object p10, p0, LX/JPA;->i:LX/JOY;

    .line 2689135
    iput-object p11, p0, LX/JPA;->j:LX/JPa;

    .line 2689136
    iput-object p12, p0, LX/JPA;->k:LX/JOm;

    .line 2689137
    iput-object p13, p0, LX/JPA;->l:LX/JOe;

    .line 2689138
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2689126
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2688914
    check-cast p2, LX/JP9;

    .line 2688915
    sget-object v0, LX/JP8;->a:[I

    iget-object v1, p2, LX/JP9;->a:LX/JPM;

    invoke-virtual {v1}, LX/JPM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2688916
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal hpp card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2688917
    :pswitch_0
    iget-object v0, p0, LX/JPA;->i:LX/JOY;

    const/4 v1, 0x0

    .line 2688918
    new-instance v2, LX/JOW;

    invoke-direct {v2, v0}, LX/JOW;-><init>(LX/JOY;)V

    .line 2688919
    iget-object v3, v0, LX/JOY;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOX;

    .line 2688920
    if-nez v3, :cond_0

    .line 2688921
    new-instance v3, LX/JOX;

    invoke-direct {v3, v0}, LX/JOX;-><init>(LX/JOY;)V

    .line 2688922
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JOX;->a$redex0(LX/JOX;LX/1De;IILX/JOW;)V

    .line 2688923
    move-object v2, v3

    .line 2688924
    move-object v1, v2

    .line 2688925
    move-object v1, v1

    .line 2688926
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2688927
    iget-object v2, v1, LX/JOX;->a:LX/JOW;

    iput-object v0, v2, LX/JOW;->c:LX/1Pn;

    .line 2688928
    iget-object v2, v1, LX/JOX;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688929
    move-object v0, v1

    .line 2688930
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2688931
    iget-object v2, v0, LX/JOX;->a:LX/JOW;

    iput-object v1, v2, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688932
    iget-object v2, v0, LX/JOX;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688933
    move-object v0, v0

    .line 2688934
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688935
    iget-object v2, v0, LX/JOX;->a:LX/JOW;

    iput-object v1, v2, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688936
    iget-object v2, v0, LX/JOX;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688937
    move-object v0, v0

    .line 2688938
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2688939
    iget-object v2, v0, LX/JOX;->a:LX/JOW;

    iput-object v1, v2, LX/JOW;->d:LX/2dx;

    .line 2688940
    iget-object v2, v0, LX/JOX;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688941
    move-object v0, v0

    .line 2688942
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2688943
    :goto_0
    return-object v0

    .line 2688944
    :pswitch_1
    iget-object v0, p0, LX/JPA;->k:LX/JOm;

    const/4 v1, 0x0

    .line 2688945
    new-instance v2, LX/JOk;

    invoke-direct {v2, v0}, LX/JOk;-><init>(LX/JOm;)V

    .line 2688946
    iget-object v3, v0, LX/JOm;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOl;

    .line 2688947
    if-nez v3, :cond_1

    .line 2688948
    new-instance v3, LX/JOl;

    invoke-direct {v3, v0}, LX/JOl;-><init>(LX/JOm;)V

    .line 2688949
    :cond_1
    invoke-static {v3, p1, v1, v1, v2}, LX/JOl;->a$redex0(LX/JOl;LX/1De;IILX/JOk;)V

    .line 2688950
    move-object v2, v3

    .line 2688951
    move-object v1, v2

    .line 2688952
    move-object v1, v1

    .line 2688953
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2688954
    iget-object v2, v1, LX/JOl;->a:LX/JOk;

    iput-object v0, v2, LX/JOk;->c:LX/1Pn;

    .line 2688955
    iget-object v2, v1, LX/JOl;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688956
    move-object v0, v1

    .line 2688957
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2688958
    iget-object v2, v0, LX/JOl;->a:LX/JOk;

    iput-object v1, v2, LX/JOk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688959
    iget-object v2, v0, LX/JOl;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688960
    move-object v0, v0

    .line 2688961
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688962
    iget-object v2, v0, LX/JOl;->a:LX/JOk;

    iput-object v1, v2, LX/JOk;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688963
    iget-object v2, v0, LX/JOl;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688964
    move-object v0, v0

    .line 2688965
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2688966
    iget-object v2, v0, LX/JOl;->a:LX/JOk;

    iput-object v1, v2, LX/JOk;->d:LX/2dx;

    .line 2688967
    iget-object v2, v0, LX/JOl;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688968
    move-object v0, v0

    .line 2688969
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 2688970
    :pswitch_2
    iget-object v0, p0, LX/JPA;->l:LX/JOe;

    const/4 v1, 0x0

    .line 2688971
    new-instance v2, LX/JOc;

    invoke-direct {v2, v0}, LX/JOc;-><init>(LX/JOe;)V

    .line 2688972
    iget-object v3, v0, LX/JOe;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOd;

    .line 2688973
    if-nez v3, :cond_2

    .line 2688974
    new-instance v3, LX/JOd;

    invoke-direct {v3, v0}, LX/JOd;-><init>(LX/JOe;)V

    .line 2688975
    :cond_2
    invoke-static {v3, p1, v1, v1, v2}, LX/JOd;->a$redex0(LX/JOd;LX/1De;IILX/JOc;)V

    .line 2688976
    move-object v2, v3

    .line 2688977
    move-object v1, v2

    .line 2688978
    move-object v1, v1

    .line 2688979
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2688980
    iget-object v2, v1, LX/JOd;->a:LX/JOc;

    iput-object v0, v2, LX/JOc;->c:LX/1Pn;

    .line 2688981
    iget-object v2, v1, LX/JOd;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688982
    move-object v0, v1

    .line 2688983
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2688984
    iget-object v2, v0, LX/JOd;->a:LX/JOc;

    iput-object v1, v2, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688985
    iget-object v2, v0, LX/JOd;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688986
    move-object v0, v0

    .line 2688987
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688988
    iget-object v2, v0, LX/JOd;->a:LX/JOc;

    iput-object v1, v2, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2688989
    iget-object v2, v0, LX/JOd;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688990
    move-object v0, v0

    .line 2688991
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2688992
    iget-object v2, v0, LX/JOd;->a:LX/JOc;

    iput-object v1, v2, LX/JOc;->d:LX/2dx;

    .line 2688993
    iget-object v2, v0, LX/JOd;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2688994
    move-object v0, v0

    .line 2688995
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2688996
    :pswitch_3
    iget-object v0, p0, LX/JPA;->f:LX/JP6;

    const/4 v1, 0x0

    .line 2688997
    new-instance v2, LX/JP5;

    invoke-direct {v2, v0}, LX/JP5;-><init>(LX/JP6;)V

    .line 2688998
    iget-object v3, v0, LX/JP6;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JP4;

    .line 2688999
    if-nez v3, :cond_3

    .line 2689000
    new-instance v3, LX/JP4;

    invoke-direct {v3, v0}, LX/JP4;-><init>(LX/JP6;)V

    .line 2689001
    :cond_3
    invoke-static {v3, p1, v1, v1, v2}, LX/JP4;->a$redex0(LX/JP4;LX/1De;IILX/JP5;)V

    .line 2689002
    move-object v2, v3

    .line 2689003
    move-object v1, v2

    .line 2689004
    move-object v1, v1

    .line 2689005
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2689006
    iget-object v2, v1, LX/JP4;->a:LX/JP5;

    iput-object v0, v2, LX/JP5;->c:LX/1Pn;

    .line 2689007
    iget-object v2, v1, LX/JP4;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689008
    move-object v0, v1

    .line 2689009
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689010
    iget-object v2, v0, LX/JP4;->a:LX/JP5;

    iput-object v1, v2, LX/JP5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689011
    iget-object v2, v0, LX/JP4;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689012
    move-object v0, v0

    .line 2689013
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689014
    iget-object v2, v0, LX/JP4;->a:LX/JP5;

    iput-object v1, v2, LX/JP5;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689015
    iget-object v2, v0, LX/JP4;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689016
    move-object v0, v0

    .line 2689017
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2689018
    iget-object v2, v0, LX/JP4;->a:LX/JP5;

    iput-object v1, v2, LX/JP5;->d:LX/2dx;

    .line 2689019
    iget-object v2, v0, LX/JP4;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689020
    move-object v0, v0

    .line 2689021
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2689022
    :pswitch_4
    iget-object v0, p0, LX/JPA;->e:LX/JPk;

    const/4 v1, 0x0

    .line 2689023
    new-instance v2, LX/JPj;

    invoke-direct {v2, v0}, LX/JPj;-><init>(LX/JPk;)V

    .line 2689024
    iget-object v3, v0, LX/JPk;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPi;

    .line 2689025
    if-nez v3, :cond_4

    .line 2689026
    new-instance v3, LX/JPi;

    invoke-direct {v3, v0}, LX/JPi;-><init>(LX/JPk;)V

    .line 2689027
    :cond_4
    invoke-static {v3, p1, v1, v1, v2}, LX/JPi;->a$redex0(LX/JPi;LX/1De;IILX/JPj;)V

    .line 2689028
    move-object v2, v3

    .line 2689029
    move-object v1, v2

    .line 2689030
    move-object v1, v1

    .line 2689031
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2689032
    iget-object v2, v1, LX/JPi;->a:LX/JPj;

    iput-object v0, v2, LX/JPj;->c:LX/1Pn;

    .line 2689033
    iget-object v2, v1, LX/JPi;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689034
    move-object v0, v1

    .line 2689035
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689036
    iget-object v2, v0, LX/JPi;->a:LX/JPj;

    iput-object v1, v2, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689037
    iget-object v2, v0, LX/JPi;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689038
    move-object v0, v0

    .line 2689039
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689040
    iget-object v2, v0, LX/JPi;->a:LX/JPj;

    iput-object v1, v2, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689041
    iget-object v2, v0, LX/JPi;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689042
    move-object v0, v0

    .line 2689043
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2689044
    iget-object v2, v0, LX/JPi;->a:LX/JPj;

    iput-object v1, v2, LX/JPj;->d:LX/2dx;

    .line 2689045
    iget-object v2, v0, LX/JPi;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689046
    move-object v0, v0

    .line 2689047
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2689048
    :pswitch_5
    iget-object v0, p0, LX/JPA;->h:LX/JPQ;

    const/4 v1, 0x0

    .line 2689049
    new-instance v2, LX/JPP;

    invoke-direct {v2, v0}, LX/JPP;-><init>(LX/JPQ;)V

    .line 2689050
    iget-object v3, v0, LX/JPQ;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPO;

    .line 2689051
    if-nez v3, :cond_5

    .line 2689052
    new-instance v3, LX/JPO;

    invoke-direct {v3, v0}, LX/JPO;-><init>(LX/JPQ;)V

    .line 2689053
    :cond_5
    invoke-static {v3, p1, v1, v1, v2}, LX/JPO;->a$redex0(LX/JPO;LX/1De;IILX/JPP;)V

    .line 2689054
    move-object v2, v3

    .line 2689055
    move-object v1, v2

    .line 2689056
    move-object v1, v1

    .line 2689057
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2689058
    iget-object v2, v1, LX/JPO;->a:LX/JPP;

    iput-object v0, v2, LX/JPP;->c:LX/1Pn;

    .line 2689059
    iget-object v2, v1, LX/JPO;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689060
    move-object v0, v1

    .line 2689061
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689062
    iget-object v2, v0, LX/JPO;->a:LX/JPP;

    iput-object v1, v2, LX/JPP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689063
    iget-object v2, v0, LX/JPO;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689064
    move-object v0, v0

    .line 2689065
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689066
    iget-object v2, v0, LX/JPO;->a:LX/JPP;

    iput-object v1, v2, LX/JPP;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689067
    iget-object v2, v0, LX/JPO;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689068
    move-object v0, v0

    .line 2689069
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2689070
    iget-object v2, v0, LX/JPO;->a:LX/JPP;

    iput-object v1, v2, LX/JPP;->d:LX/2dx;

    .line 2689071
    iget-object v2, v0, LX/JPO;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689072
    move-object v0, v0

    .line 2689073
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2689074
    :pswitch_6
    iget-object v0, p0, LX/JPA;->j:LX/JPa;

    const/4 v1, 0x0

    .line 2689075
    new-instance v2, LX/JPZ;

    invoke-direct {v2, v0}, LX/JPZ;-><init>(LX/JPa;)V

    .line 2689076
    iget-object v3, v0, LX/JPa;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPY;

    .line 2689077
    if-nez v3, :cond_6

    .line 2689078
    new-instance v3, LX/JPY;

    invoke-direct {v3, v0}, LX/JPY;-><init>(LX/JPa;)V

    .line 2689079
    :cond_6
    invoke-static {v3, p1, v1, v1, v2}, LX/JPY;->a$redex0(LX/JPY;LX/1De;IILX/JPZ;)V

    .line 2689080
    move-object v2, v3

    .line 2689081
    move-object v1, v2

    .line 2689082
    move-object v1, v1

    .line 2689083
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2689084
    iget-object v2, v1, LX/JPY;->a:LX/JPZ;

    iput-object v0, v2, LX/JPZ;->c:LX/1Pn;

    .line 2689085
    iget-object v2, v1, LX/JPY;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689086
    move-object v0, v1

    .line 2689087
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689088
    iget-object v2, v0, LX/JPY;->a:LX/JPZ;

    iput-object v1, v2, LX/JPZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689089
    iget-object v2, v0, LX/JPY;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689090
    move-object v0, v0

    .line 2689091
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689092
    iget-object v2, v0, LX/JPY;->a:LX/JPZ;

    iput-object v1, v2, LX/JPZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689093
    iget-object v2, v0, LX/JPY;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689094
    move-object v0, v0

    .line 2689095
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2689096
    iget-object v2, v0, LX/JPY;->a:LX/JPZ;

    iput-object v1, v2, LX/JPZ;->d:LX/2dx;

    .line 2689097
    iget-object v2, v0, LX/JPY;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689098
    move-object v0, v0

    .line 2689099
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2689100
    :pswitch_7
    iget-object v0, p0, LX/JPA;->g:LX/JP2;

    const/4 v1, 0x0

    .line 2689101
    new-instance v2, LX/JP1;

    invoke-direct {v2, v0}, LX/JP1;-><init>(LX/JP2;)V

    .line 2689102
    iget-object v3, v0, LX/JP2;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JP0;

    .line 2689103
    if-nez v3, :cond_7

    .line 2689104
    new-instance v3, LX/JP0;

    invoke-direct {v3, v0}, LX/JP0;-><init>(LX/JP2;)V

    .line 2689105
    :cond_7
    invoke-static {v3, p1, v1, v1, v2}, LX/JP0;->a$redex0(LX/JP0;LX/1De;IILX/JP1;)V

    .line 2689106
    move-object v2, v3

    .line 2689107
    move-object v1, v2

    .line 2689108
    move-object v1, v1

    .line 2689109
    iget-object v0, p0, LX/JPA;->d:LX/1Pc;

    check-cast v0, LX/1Pn;

    .line 2689110
    iget-object v2, v1, LX/JP0;->a:LX/JP1;

    iput-object v0, v2, LX/JP1;->c:LX/1Pn;

    .line 2689111
    iget-object v2, v1, LX/JP0;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689112
    move-object v0, v1

    .line 2689113
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689114
    iget-object v2, v0, LX/JP0;->a:LX/JP1;

    iput-object v1, v2, LX/JP1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689115
    iget-object v2, v0, LX/JP0;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689116
    move-object v0, v0

    .line 2689117
    iget-object v1, p2, LX/JP9;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689118
    iget-object v2, v0, LX/JP0;->a:LX/JP1;

    iput-object v1, v2, LX/JP1;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689119
    iget-object v2, v0, LX/JP0;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689120
    move-object v0, v0

    .line 2689121
    iget-object v1, p0, LX/JPA;->c:LX/JPJ;

    iget-object v1, v1, LX/JPJ;->b:LX/2dx;

    .line 2689122
    iget-object v2, v0, LX/JP0;->a:LX/JP1;

    iput-object v1, v2, LX/JP1;->d:LX/2dx;

    .line 2689123
    iget-object v2, v0, LX/JP0;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689124
    move-object v0, v0

    .line 2689125
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1

    .prologue
    .line 2688910
    invoke-super {p0, p1}, LX/3mX;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 2688911
    iget-object v0, p0, LX/JPA;->c:LX/JPJ;

    iget-object v0, v0, LX/JPJ;->b:LX/2dx;

    .line 2688912
    iput-object p1, v0, LX/2dx;->a:Landroid/view/View;

    .line 2688913
    return-void
.end method

.method public final b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 2689139
    invoke-super {p0, p1}, LX/3mX;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 2689140
    iget-object v0, p0, LX/JPA;->c:LX/JPJ;

    iget-object v0, v0, LX/JPJ;->b:LX/2dx;

    const/4 v1, 0x0

    .line 2689141
    iput-object v1, v0, LX/2dx;->a:Landroid/view/View;

    .line 2689142
    return-void
.end method

.method public final synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2688909
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/JPA;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2688908
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/JPA;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2688907
    const/4 v0, 0x0

    return v0
.end method
