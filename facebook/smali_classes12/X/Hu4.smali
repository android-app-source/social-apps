.class public final LX/Hu4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Hu6;


# direct methods
.method public constructor <init>(LX/Hu6;Z)V
    .locals 0

    .prologue
    .line 2517071
    iput-object p1, p0, LX/Hu4;->b:LX/Hu6;

    iput-boolean p2, p0, LX/Hu4;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2517072
    sget-object v0, LX/Hu6;->a:Ljava/lang/Class;

    const-string v1, "Location not received"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2517073
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->e:LX/7kl;

    sget-object v1, LX/2tG;->FAILED:LX/2tG;

    invoke-virtual {v0, v1}, LX/7kl;->a(LX/2tG;)V

    .line 2517074
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2517075
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2517076
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->e:LX/7kl;

    sget-object v1, LX/2tG;->SUCCEEDED:LX/2tG;

    invoke-virtual {v0, v1}, LX/7kl;->a(LX/2tG;)V

    .line 2517077
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v1

    .line 2517078
    iput-object v1, v0, LX/Hu6;->l:Landroid/location/Location;

    .line 2517079
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->c:LX/9jT;

    iget-object v1, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v1, v1, LX/Hu6;->l:Landroid/location/Location;

    .line 2517080
    iput-object v1, v0, LX/9jT;->b:Landroid/location/Location;

    .line 2517081
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v0, v0, LX/Hu6;->i:LX/HqN;

    iget-object v1, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v1, v1, LX/Hu6;->l:Landroid/location/Location;

    .line 2517082
    iget-object v2, v0, LX/HqN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object p1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v2, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Landroid/location/Location;)Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object p1

    .line 2517083
    iget-object v0, v2, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2517084
    iget-object v0, v2, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2517085
    iget-object v0, v2, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v0, :cond_0

    .line 2517086
    iget-object v0, v2, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v0

    iput-object v0, v2, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2517087
    :cond_0
    iget-object v0, v2, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setViewerCoordinates(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2517088
    iget-object v0, v2, LX/0jL;->a:LX/0cA;

    sget-object v1, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2517089
    :cond_1
    move-object v2, v2

    .line 2517090
    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 2517091
    iget-boolean v0, p0, LX/Hu4;->a:Z

    if-eqz v0, :cond_2

    .line 2517092
    iget-object v0, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v1, p0, LX/Hu4;->b:LX/Hu6;

    iget-object v1, v1, LX/Hu6;->l:Landroid/location/Location;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Hu6;->a$redex0(LX/Hu6;Landroid/location/Location;Z)V

    .line 2517093
    :cond_2
    return-void
.end method
