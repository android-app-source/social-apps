.class public LX/JV7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/JVJ;

.field public final b:LX/JVE;

.field public final c:LX/JVA;


# direct methods
.method public constructor <init>(LX/JVJ;LX/JVE;LX/JVA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700249
    iput-object p1, p0, LX/JV7;->a:LX/JVJ;

    .line 2700250
    iput-object p2, p0, LX/JV7;->b:LX/JVE;

    .line 2700251
    iput-object p3, p0, LX/JV7;->c:LX/JVA;

    .line 2700252
    return-void
.end method

.method public static a(LX/0QB;)LX/JV7;
    .locals 6

    .prologue
    .line 2700253
    const-class v1, LX/JV7;

    monitor-enter v1

    .line 2700254
    :try_start_0
    sget-object v0, LX/JV7;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700255
    sput-object v2, LX/JV7;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700256
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700257
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700258
    new-instance p0, LX/JV7;

    invoke-static {v0}, LX/JVJ;->a(LX/0QB;)LX/JVJ;

    move-result-object v3

    check-cast v3, LX/JVJ;

    invoke-static {v0}, LX/JVE;->a(LX/0QB;)LX/JVE;

    move-result-object v4

    check-cast v4, LX/JVE;

    invoke-static {v0}, LX/JVA;->a(LX/0QB;)LX/JVA;

    move-result-object v5

    check-cast v5, LX/JVA;

    invoke-direct {p0, v3, v4, v5}, LX/JV7;-><init>(LX/JVJ;LX/JVE;LX/JVA;)V

    .line 2700259
    move-object v0, p0

    .line 2700260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JV7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
