.class public final LX/I4h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I4k;

.field public final synthetic b:I

.field public final synthetic c:LX/I4m;


# direct methods
.method public constructor <init>(LX/I4m;LX/I4k;I)V
    .locals 0

    .prologue
    .line 2534329
    iput-object p1, p0, LX/I4h;->c:LX/I4m;

    iput-object p2, p0, LX/I4h;->a:LX/I4k;

    iput p3, p0, LX/I4h;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x81c021d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2534330
    iget-object v1, p0, LX/I4h;->a:LX/I4k;

    iget-object v1, v1, LX/I4k;->d:LX/I4j;

    sget-object v2, LX/I4j;->NO_SELECTION:LX/I4j;

    if-eq v1, v2, :cond_0

    .line 2534331
    iget-object v1, p0, LX/I4h;->c:LX/I4m;

    iget v2, p0, LX/I4h;->b:I

    iget-object v3, p0, LX/I4h;->a:LX/I4k;

    invoke-virtual {v1, v2, v3}, LX/I4m;->a(ILX/I4k;)V

    .line 2534332
    :goto_0
    const v1, 0x4051d9be

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2534333
    :cond_0
    iget-object v1, p0, LX/I4h;->c:LX/I4m;

    iget-object v1, v1, LX/I4m;->f:LX/I4o;

    .line 2534334
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-object v2, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    .line 2534335
    const-string v2, "target_fragment"

    sget-object p0, LX/0cQ;->EVENTS_DISCOVERY_LOCATION_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/0cQ;->ordinal()I

    move-result p0

    invoke-virtual {v3, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534336
    iget-object v2, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    .line 2534337
    :goto_1
    const-string p0, "extra_is_current_location_selected"

    invoke-virtual {v3, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2534338
    const-string v2, "extra_location_range"

    iget-object p0, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget p0, p0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->v:I

    invoke-virtual {v3, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534339
    iget-object v2, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->n:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x65

    iget-object p1, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    invoke-interface {v2, v3, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2534340
    goto :goto_0

    .line 2534341
    :cond_1
    iget-object v2, v1, LX/I4o;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->u:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534342
    iget-boolean p0, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v2, p0

    .line 2534343
    goto :goto_1
.end method
