.class public LX/IqX;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/IqW;


# instance fields
.field public a:LX/8kj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8jY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/IqV;

.field public d:LX/IrZ;

.field private e:LX/IrZ;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

.field public h:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

.field public i:Landroid/support/v7/widget/RecyclerView;

.field public j:Landroid/support/v7/widget/RecyclerView;

.field public k:LX/IrK;

.field private l:F

.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field private final q:I

.field private final r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x3

    .line 2616953
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2616954
    const-class v0, LX/IqX;

    invoke-static {v0, p0}, LX/IqX;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2616955
    const v0, 0x7f0313c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2616956
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/IqX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0325

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2616957
    iget-object v0, p0, LX/IqX;->a:LX/8kj;

    invoke-virtual {v0}, LX/8kj;->a()V

    .line 2616958
    iget-object v0, p0, LX/IqX;->a:LX/8kj;

    new-instance v1, LX/IqU;

    invoke-direct {v1, p0}, LX/IqU;-><init>(LX/IqX;)V

    .line 2616959
    iput-object v1, v0, LX/8kj;->d:LX/3Mb;

    .line 2616960
    iget-object v0, p0, LX/IqX;->a:LX/8kj;

    new-instance v1, LX/8kh;

    sget-object v2, LX/8kg;->PREFER_CACHE_IF_UP_TO_DATE:LX/8kg;

    sget-object p1, LX/4m4;->COMPOSER:LX/4m4;

    invoke-direct {v1, v2, p1}, LX/8kh;-><init>(LX/8kg;LX/4m4;)V

    invoke-virtual {v0, v1}, LX/8kj;->a(LX/8kh;)V

    .line 2616961
    iget-object v0, p0, LX/IqX;->a:LX/8kj;

    new-instance v1, LX/8kh;

    sget-object v2, LX/8kg;->CHECK_SERVER_FOR_NEW_DATA:LX/8kg;

    sget-object p1, LX/4m4;->COMPOSER:LX/4m4;

    invoke-direct {v1, v2, p1}, LX/8kh;-><init>(LX/8kg;LX/4m4;)V

    invoke-virtual {v0, v1}, LX/8kj;->a(LX/8kh;)V

    .line 2616962
    invoke-virtual {p0}, LX/IqX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2616963
    iput v3, p0, LX/IqX;->q:I

    .line 2616964
    iput v4, p0, LX/IqX;->r:I

    .line 2616965
    :goto_0
    iput p3, p0, LX/IqX;->m:I

    .line 2616966
    iput p2, p0, LX/IqX;->n:I

    .line 2616967
    iput p2, p0, LX/IqX;->o:I

    .line 2616968
    iput p3, p0, LX/IqX;->p:I

    .line 2616969
    new-instance v0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    invoke-direct {v0}, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;-><init>()V

    iput-object v0, p0, LX/IqX;->g:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    .line 2616970
    iget-object v0, p0, LX/IqX;->g:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    iget-object v1, p0, LX/IqX;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a(Ljava/util/List;)V

    .line 2616971
    iget-object v0, p0, LX/IqX;->g:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    new-instance v1, LX/IqQ;

    invoke-direct {v1, p0}, LX/IqQ;-><init>(LX/IqX;)V

    .line 2616972
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->b:LX/IqQ;

    .line 2616973
    new-instance v0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iget-object v1, p0, LX/IqX;->b:LX/8jY;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;-><init>(LX/8jY;)V

    iput-object v0, p0, LX/IqX;->h:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    .line 2616974
    iget-object v0, p0, LX/IqX;->h:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    new-instance v1, LX/IqR;

    invoke-direct {v1, p0}, LX/IqR;-><init>(LX/IqX;)V

    .line 2616975
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->d:LX/IqR;

    .line 2616976
    const v0, 0x7f0d2d9c

    invoke-virtual {p0, v0}, LX/IqX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2616977
    const v0, 0x7f0d2d9d

    invoke-virtual {p0, v0}, LX/IqX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    .line 2616978
    new-instance v0, LX/IrZ;

    invoke-virtual {p0}, LX/IqX;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LX/IqX;->q:I

    invoke-direct {v0, v1, v2}, LX/IrZ;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/IqX;->e:LX/IrZ;

    .line 2616979
    iget-object v0, p0, LX/IqX;->e:LX/IrZ;

    new-instance v1, LX/IqS;

    invoke-direct {v1, p0}, LX/IqS;-><init>(LX/IqX;)V

    .line 2616980
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2616981
    iget-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IqX;->e:LX/IrZ;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2616982
    iget-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IqX;->g:Lcom/facebook/messaging/photos/editing/StickerPackAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2616983
    new-instance v0, LX/IrZ;

    invoke-virtual {p0}, LX/IqX;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LX/IqX;->q:I

    invoke-direct {v0, v1, v2}, LX/IrZ;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/IqX;->d:LX/IrZ;

    .line 2616984
    iget-object v0, p0, LX/IqX;->d:LX/IrZ;

    new-instance v1, LX/IqT;

    invoke-direct {v1, p0}, LX/IqT;-><init>(LX/IqX;)V

    .line 2616985
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2616986
    iget-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IqX;->d:LX/IrZ;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2616987
    iget-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IqX;->h:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2616988
    return-void

    .line 2616989
    :cond_0
    iput v4, p0, LX/IqX;->q:I

    .line 2616990
    iput v3, p0, LX/IqX;->r:I

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IqX;

    invoke-static {p0}, LX/8kj;->a(LX/0QB;)LX/8kj;

    move-result-object v1

    check-cast v1, LX/8kj;

    invoke-static {p0}, LX/8jY;->b(LX/0QB;)LX/8jY;

    move-result-object p0

    check-cast p0, LX/8jY;

    iput-object v1, p1, LX/IqX;->a:LX/8kj;

    iput-object p0, p1, LX/IqX;->b:LX/8jY;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2616991
    sget-object v0, LX/IqV;->STICKERLIST:LX/IqV;

    iget-object v1, p0, LX/IqX;->c:LX/IqV;

    invoke-virtual {v0, v1}, LX/IqV;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2616992
    sget-object v0, LX/IqV;->PACKLIST:LX/IqV;

    invoke-virtual {p0, v0}, LX/IqX;->setStateAndVisibilities(LX/IqV;)V

    .line 2616993
    const/4 v0, 0x1

    .line 2616994
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2616995
    return-object p0
.end method

.method public setGlobalRotation(F)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v5, 0x43340000    # 180.0f

    const/4 v4, 0x0

    .line 2616996
    iget-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setRotation(F)V

    .line 2616997
    iget-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setRotation(F)V

    .line 2616998
    rem-float v0, p1, v5

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    move v0, v1

    .line 2616999
    :goto_0
    iget v3, p0, LX/IqX;->l:F

    sub-float/2addr v3, p1

    rem-float/2addr v3, v5

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    .line 2617000
    :goto_1
    if-eqz v1, :cond_0

    .line 2617001
    if-eqz v0, :cond_3

    .line 2617002
    iget-object v1, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2617003
    iget v2, p0, LX/IqX;->p:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2617004
    iget v2, p0, LX/IqX;->o:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2617005
    iget-object v1, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2617006
    iget v2, p0, LX/IqX;->p:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2617007
    iget v2, p0, LX/IqX;->o:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2617008
    invoke-virtual {p0}, LX/IqX;->requestLayout()V

    .line 2617009
    :goto_2
    iget-object v2, p0, LX/IqX;->e:LX/IrZ;

    if-eqz v0, :cond_4

    iget v1, p0, LX/IqX;->r:I

    :goto_3
    invoke-virtual {v2, v1}, LX/3wu;->a(I)V

    .line 2617010
    iget-object v1, p0, LX/IqX;->d:LX/IrZ;

    if-eqz v0, :cond_5

    iget v0, p0, LX/IqX;->r:I

    :goto_4
    invoke-virtual {v1, v0}, LX/3wu;->a(I)V

    .line 2617011
    :cond_0
    iput p1, p0, LX/IqX;->l:F

    .line 2617012
    return-void

    :cond_1
    move v0, v2

    .line 2617013
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2617014
    goto :goto_1

    .line 2617015
    :cond_3
    iget-object v1, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2617016
    iget v2, p0, LX/IqX;->n:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2617017
    iget v2, p0, LX/IqX;->m:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2617018
    iget-object v1, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2617019
    iget v2, p0, LX/IqX;->n:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2617020
    iget v2, p0, LX/IqX;->m:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2617021
    invoke-virtual {p0}, LX/IqX;->requestLayout()V

    .line 2617022
    goto :goto_2

    .line 2617023
    :cond_4
    iget v1, p0, LX/IqX;->q:I

    goto :goto_3

    .line 2617024
    :cond_5
    iget v0, p0, LX/IqX;->q:I

    goto :goto_4
.end method

.method public setOnStickerClickListener(LX/IrK;)V
    .locals 0

    .prologue
    .line 2617025
    iput-object p1, p0, LX/IqX;->k:LX/IrK;

    .line 2617026
    return-void
.end method

.method public setStateAndVisibilities(LX/IqV;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2617027
    iput-object p1, p0, LX/IqX;->c:LX/IqV;

    .line 2617028
    sget-object v0, LX/IqV;->PACKLIST:LX/IqV;

    if-ne p1, v0, :cond_1

    .line 2617029
    invoke-virtual {p0, v1}, LX/IqX;->setVisibility(I)V

    .line 2617030
    iget-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2617031
    iget-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2617032
    :cond_0
    :goto_0
    return-void

    .line 2617033
    :cond_1
    sget-object v0, LX/IqV;->STICKERLIST:LX/IqV;

    if-ne p1, v0, :cond_0

    .line 2617034
    invoke-virtual {p0, v1}, LX/IqX;->setVisibility(I)V

    .line 2617035
    iget-object v0, p0, LX/IqX;->i:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2617036
    iget-object v0, p0, LX/IqX;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method
