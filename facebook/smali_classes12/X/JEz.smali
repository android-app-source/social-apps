.class public final LX/JEz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(I)I
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid source: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2666606
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2666607
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2666608
    :goto_0
    return v1

    .line 2666609
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2666610
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2666611
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2666612
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2666613
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2666614
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2666615
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2666616
    :cond_2
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2666617
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2666618
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2666619
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2666620
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2666621
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/JCO;I)I
    .locals 2

    .prologue
    .line 2666605
    sget-object v0, LX/JCP;->A:[LX/JCO;

    array-length v0, v0

    mul-int/2addr v0, p1

    invoke-virtual {p0}, LX/JCO;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(I)LX/JCO;
    .locals 2

    .prologue
    .line 2666603
    sget-object v0, LX/JCP;->A:[LX/JCO;

    array-length v0, v0

    rem-int v0, p0, v0

    .line 2666604
    sget-object v1, LX/JCP;->A:[LX/JCO;

    aget-object v0, v1, v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2666585
    invoke-static {}, LX/J8E;->a()Ljava/lang/Object;

    .line 2666586
    invoke-static {}, LX/JEz;->b()Ljava/lang/Class;

    move-result-object v0

    .line 2666587
    invoke-static {}, LX/J8F;->b()Ljava/lang/Class;

    move-result-object v2

    .line 2666588
    const-string v3, "gDefault"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 2666589
    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2666590
    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 2666591
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2666592
    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2666593
    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2666594
    invoke-virtual {v3, v1, p0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2666595
    :goto_0
    return-object v0

    .line 2666596
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 2666597
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.util.Singleton"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 2666598
    goto :goto_0

    .line 2666599
    :cond_1
    const-string v1, "mInstance"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 2666600
    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2666601
    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2666602
    invoke-virtual {v1, v4, p0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2666622
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2666623
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666624
    if-eqz v0, :cond_0

    .line 2666625
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666626
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666627
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666628
    if-eqz v0, :cond_1

    .line 2666629
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666630
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666631
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2666632
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2666544
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2666545
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2666546
    if-eqz v0, :cond_0

    .line 2666547
    const-string v1, "city"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666548
    invoke-static {p0, v0, p2}, LX/JEz;->a(LX/15i;ILX/0nX;)V

    .line 2666549
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666550
    if-eqz v0, :cond_1

    .line 2666551
    const-string v1, "google_place_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666552
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666553
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2666554
    if-eqz v0, :cond_4

    .line 2666555
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666556
    const-wide/16 v6, 0x0

    .line 2666557
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2666558
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2666559
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_2

    .line 2666560
    const-string v4, "latitude"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666561
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 2666562
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2666563
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_3

    .line 2666564
    const-string v4, "longitude"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666565
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 2666566
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2666567
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666568
    if-eqz v0, :cond_5

    .line 2666569
    const-string v1, "single_line_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666570
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666571
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666572
    if-eqz v0, :cond_6

    .line 2666573
    const-string v1, "street_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666574
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666575
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666576
    if-eqz v0, :cond_7

    .line 2666577
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666578
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666579
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2666580
    if-eqz v0, :cond_8

    .line 2666581
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666582
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2666583
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2666584
    return-void
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 2666543
    sget-object v0, LX/JCP;->A:[LX/JCO;

    array-length v0, v0

    div-int v0, p0, v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2666530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2666531
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2666532
    :goto_0
    return v1

    .line 2666533
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2666534
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2666535
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2666536
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2666537
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2666538
    const-string v3, "node"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2666539
    invoke-static {p0, p1}, LX/Hif;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2666540
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2666541
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2666542
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2666529
    const-string v0, "android.app.ActivityManagerNative"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2666522
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2666523
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2666524
    if-eqz v0, :cond_0

    .line 2666525
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2666526
    invoke-static {p0, v0, p2, p3}, LX/JEz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2666527
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2666528
    return-void
.end method
