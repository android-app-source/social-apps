.class public LX/HoR;
.super LX/2s5;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HoQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;LX/0gc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HoQ;",
            ">;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2505704
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2505705
    iput-object p1, p0, LX/HoR;->a:Ljava/util/List;

    .line 2505706
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2505707
    iget-object v0, p0, LX/HoR;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HoQ;

    iget-object v0, v0, LX/HoQ;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2505708
    iget-object v0, p0, LX/HoR;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HoQ;

    .line 2505709
    iget-object v1, v0, LX/HoQ;->a:Ljava/lang/String;

    .line 2505710
    iget-boolean v2, v0, LX/HoQ;->c:Z

    if-eqz v2, :cond_0

    .line 2505711
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2505712
    const-string v2, "tabId"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2505713
    new-instance v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;

    invoke-direct {v2}, Lcom/facebook/civicengagement/elections/ui/ElectionHubNTFragment;-><init>()V

    .line 2505714
    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2505715
    move-object v0, v2

    .line 2505716
    :goto_0
    return-object v0

    :cond_0
    iget v0, v0, LX/HoQ;->d:I

    invoke-static {v1, v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(Ljava/lang/String;I)Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2505717
    iget-object v0, p0, LX/HoR;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
