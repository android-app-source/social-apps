.class public LX/JSY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/JSc;

.field private final b:LX/1V1;

.field private final c:LX/JTZ;

.field private final d:LX/1V2;

.field private final e:LX/14w;


# direct methods
.method public constructor <init>(LX/1V1;LX/14w;LX/JTZ;LX/1V2;LX/JSc;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2695518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695519
    iput-object p2, p0, LX/JSY;->e:LX/14w;

    .line 2695520
    iput-object p4, p0, LX/JSY;->d:LX/1V2;

    .line 2695521
    iput-object p5, p0, LX/JSY;->a:LX/JSc;

    .line 2695522
    iput-object p1, p0, LX/JSY;->b:LX/1V1;

    .line 2695523
    iput-object p3, p0, LX/JSY;->c:LX/JTZ;

    .line 2695524
    return-void
.end method

.method public static a(LX/0QB;)LX/JSY;
    .locals 9

    .prologue
    .line 2695525
    const-class v1, LX/JSY;

    monitor-enter v1

    .line 2695526
    :try_start_0
    sget-object v0, LX/JSY;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695527
    sput-object v2, LX/JSY;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695530
    new-instance v3, LX/JSY;

    invoke-static {v0}, LX/1V1;->a(LX/0QB;)LX/1V1;

    move-result-object v4

    check-cast v4, LX/1V1;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v5

    check-cast v5, LX/14w;

    const-class v6, LX/JTZ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/JTZ;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v7

    check-cast v7, LX/1V2;

    invoke-static {v0}, LX/JSc;->a(LX/0QB;)LX/JSc;

    move-result-object v8

    check-cast v8, LX/JSc;

    invoke-direct/range {v3 .. v8}, LX/JSY;-><init>(LX/1V1;LX/14w;LX/JTZ;LX/1V2;LX/JSc;)V

    .line 2695531
    move-object v0, v3

    .line 2695532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/JSe;"
        }
    .end annotation

    .prologue
    .line 2695536
    new-instance v0, LX/JTU;

    invoke-direct {v0, p0}, LX/JTU;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2695537
    new-instance v1, LX/JSd;

    invoke-virtual {v0}, LX/JTU;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, LX/JSd;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0}, LX/JTU;->c()Ljava/lang/String;

    move-result-object v2

    .line 2695538
    iput-object v2, v1, LX/JSd;->c:Ljava/lang/String;

    .line 2695539
    move-object v1, v1

    .line 2695540
    invoke-virtual {v0}, LX/JTU;->b()Ljava/lang/String;

    move-result-object v2

    .line 2695541
    iput-object v2, v1, LX/JSd;->f:Ljava/lang/String;

    .line 2695542
    move-object v1, v1

    .line 2695543
    invoke-virtual {v0}, LX/JTU;->d()Landroid/net/Uri;

    move-result-object v2

    .line 2695544
    iput-object v2, v1, LX/JSd;->g:Landroid/net/Uri;

    .line 2695545
    move-object v1, v1

    .line 2695546
    invoke-virtual {v0}, LX/JTU;->e()Landroid/net/Uri;

    move-result-object v2

    .line 2695547
    iput-object v2, v1, LX/JSd;->k:Landroid/net/Uri;

    .line 2695548
    move-object v1, v1

    .line 2695549
    invoke-virtual {v0}, LX/JTU;->f()Ljava/lang/String;

    move-result-object v2

    .line 2695550
    iput-object v2, v1, LX/JSd;->e:Ljava/lang/String;

    .line 2695551
    move-object v1, v1

    .line 2695552
    invoke-virtual {v0}, LX/JTU;->g()Ljava/lang/String;

    move-result-object v2

    .line 2695553
    iput-object v2, v1, LX/JSd;->d:Ljava/lang/String;

    .line 2695554
    move-object v1, v1

    .line 2695555
    invoke-virtual {v0}, LX/JTU;->h()Landroid/net/Uri;

    move-result-object v2

    .line 2695556
    iput-object v2, v1, LX/JSd;->h:Landroid/net/Uri;

    .line 2695557
    move-object v1, v1

    .line 2695558
    invoke-virtual {v0}, LX/JTU;->i()Landroid/net/Uri;

    move-result-object v2

    .line 2695559
    iput-object v2, v1, LX/JSd;->i:Landroid/net/Uri;

    .line 2695560
    move-object v1, v1

    .line 2695561
    invoke-virtual {v0}, LX/JTU;->j()Ljava/lang/String;

    move-result-object v2

    .line 2695562
    iput-object v2, v1, LX/JSd;->j:Ljava/lang/String;

    .line 2695563
    move-object v1, v1

    .line 2695564
    iget-object v2, v0, LX/JTU;->e:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    if-eqz v2, :cond_0

    .line 2695565
    iget-object v2, v0, LX/JTU;->e:Lcom/facebook/graphql/model/GraphQLExternalUrl;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLExternalUrl;->m()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 2695566
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    if-eqz p0, :cond_0

    .line 2695567
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->P()LX/0Px;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2695568
    :goto_0
    move-object v0, v2

    .line 2695569
    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_1
    iput-object v2, v1, LX/JSd;->l:Landroid/net/Uri;

    .line 2695570
    move-object v0, v1

    .line 2695571
    invoke-virtual {v0}, LX/JSd;->a()LX/JSe;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2695572
    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2695573
    iget-object v0, p0, LX/JSY;->a:LX/JSc;

    const/4 v1, 0x0

    .line 2695574
    new-instance v2, LX/JSb;

    invoke-direct {v2, v0}, LX/JSb;-><init>(LX/JSc;)V

    .line 2695575
    iget-object v3, v0, LX/JSc;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JSa;

    .line 2695576
    if-nez v3, :cond_0

    .line 2695577
    new-instance v3, LX/JSa;

    invoke-direct {v3, v0}, LX/JSa;-><init>(LX/JSc;)V

    .line 2695578
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JSa;->a$redex0(LX/JSa;LX/1De;IILX/JSb;)V

    .line 2695579
    move-object v2, v3

    .line 2695580
    move-object v1, v2

    .line 2695581
    move-object v0, v1

    .line 2695582
    iget-object v1, p0, LX/JSY;->c:LX/JTZ;

    new-instance v2, LX/JTc;

    invoke-direct {v2, p2}, LX/JTc;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/JTZ;->a(LX/JTc;)LX/JTY;

    move-result-object v1

    .line 2695583
    iget-object v2, v0, LX/JSa;->a:LX/JSb;

    iput-object v1, v2, LX/JSb;->b:LX/JTY;

    .line 2695584
    iget-object v2, v0, LX/JSa;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2695585
    move-object v0, v0

    .line 2695586
    invoke-static {p2}, LX/JSY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSe;

    move-result-object v1

    .line 2695587
    iget-object v2, v0, LX/JSa;->a:LX/JSb;

    iput-object v1, v2, LX/JSb;->a:LX/JSe;

    .line 2695588
    iget-object v2, v0, LX/JSa;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2695589
    move-object v0, v0

    .line 2695590
    iget-object v1, v0, LX/JSa;->a:LX/JSb;

    iput-object p2, v1, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695591
    iget-object v1, v0, LX/JSa;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695592
    move-object v0, v0

    .line 2695593
    iget-object v1, v0, LX/JSa;->a:LX/JSb;

    iput-object p3, v1, LX/JSb;->d:LX/1Pn;

    .line 2695594
    iget-object v1, v0, LX/JSa;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2695595
    move-object v0, v0

    .line 2695596
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v9

    .line 2695597
    new-instance v10, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    sget-object v1, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v10, v0, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2695598
    iget-object v0, v10, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p0, LX/JSY;->e:LX/14w;

    invoke-static {v0, v1}, LX/1X7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/14w;)I

    move-result v0

    .line 2695599
    iget-object v1, v10, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, v10, LX/1X6;->e:LX/1X9;

    iget-object v3, p0, LX/JSY;->d:LX/1V2;

    move-object v4, p3

    check-cast v4, LX/1Ps;

    invoke-interface {v4}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    move-object v5, p3

    check-cast v5, LX/1Ps;

    invoke-interface {v5}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    move-object v6, p3

    check-cast v6, LX/1Ps;

    invoke-interface {v6}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    move-object v7, p3

    check-cast v7, LX/1Ps;

    invoke-interface {v7}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    check-cast p3, LX/1Ps;

    invoke-interface {p3}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v1

    .line 2695600
    iget-object v2, p0, LX/JSY;->b:LX/1V1;

    invoke-virtual {v2, p1}, LX/1V1;->c(LX/1De;)LX/1XB;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/1XB;->a(LX/1X1;)LX/1XB;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1XB;->a(LX/1X6;)LX/1XB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1XB;->h(I)LX/1XB;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1XB;->a(LX/1X9;)LX/1XB;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
