.class public LX/HlG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/HlG;


# instance fields
.field public a:LX/0tX;

.field public b:LX/HlJ;

.field public c:LX/HlI;

.field public d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0tX;LX/HlJ;LX/HlI;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498282
    iput-object p1, p0, LX/HlG;->a:LX/0tX;

    .line 2498283
    iput-object p2, p0, LX/HlG;->b:LX/HlJ;

    .line 2498284
    iput-object p3, p0, LX/HlG;->c:LX/HlI;

    .line 2498285
    iput-object p4, p0, LX/HlG;->d:Ljava/util/concurrent/ExecutorService;

    .line 2498286
    return-void
.end method

.method public static a(LX/0QB;)LX/HlG;
    .locals 10

    .prologue
    .line 2498287
    sget-object v0, LX/HlG;->e:LX/HlG;

    if-nez v0, :cond_1

    .line 2498288
    const-class v1, LX/HlG;

    monitor-enter v1

    .line 2498289
    :try_start_0
    sget-object v0, LX/HlG;->e:LX/HlG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2498290
    if-eqz v2, :cond_0

    .line 2498291
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2498292
    new-instance v7, LX/HlG;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    .line 2498293
    new-instance v6, LX/HlJ;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct {v6, v4, v5}, LX/HlJ;-><init>(LX/0kb;LX/0Zb;)V

    .line 2498294
    move-object v4, v6

    .line 2498295
    check-cast v4, LX/HlJ;

    .line 2498296
    new-instance p0, LX/HlI;

    invoke-static {v0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v5

    check-cast v5, LX/0oy;

    invoke-static {v0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v6

    check-cast v6, LX/0pn;

    invoke-static {v0}, LX/18A;->b(LX/0QB;)LX/18A;

    move-result-object v8

    check-cast v8, LX/18A;

    invoke-static {v0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v9

    check-cast v9, LX/0Ym;

    invoke-direct {p0, v5, v6, v8, v9}, LX/HlI;-><init>(LX/0oy;LX/0pn;LX/18A;LX/0Ym;)V

    .line 2498297
    move-object v5, p0

    .line 2498298
    check-cast v5, LX/HlI;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v7, v3, v4, v5, v6}, LX/HlG;-><init>(LX/0tX;LX/HlJ;LX/HlI;Ljava/util/concurrent/ExecutorService;)V

    .line 2498299
    move-object v0, v7

    .line 2498300
    sput-object v0, LX/HlG;->e:LX/HlG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2498301
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2498302
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2498303
    :cond_1
    sget-object v0, LX/HlG;->e:LX/HlG;

    return-object v0

    .line 2498304
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2498305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
