.class public LX/J0v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6sf",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2638058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638059
    const-string v0, "bank_code"

    iput-object v0, p0, LX/J0v;->a:Ljava/lang/String;

    .line 2638060
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;
    .locals 4

    .prologue
    .line 2638061
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;

    .line 2638062
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "type"

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 2638063
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    const-string v2, "bank_code"

    .line 2638064
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/NetBankingMethod;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2638065
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v1

    .line 2638066
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2638067
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6zU;
    .locals 1

    .prologue
    .line 2638068
    sget-object v0, LX/6zU;->NET_BANKING:LX/6zU;

    return-object v0
.end method
