.class public LX/IWV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZ7;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V
    .locals 3

    .prologue
    .line 2583960
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2583961
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/DLO;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2583962
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-interface {p2}, LX/DLO;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2583963
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2583964
    move-object v1, v1

    .line 2583965
    invoke-interface {p2}, LX/DLO;->b()Ljava/lang/String;

    move-result-object v2

    .line 2583966
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 2583967
    move-object v1, v1

    .line 2583968
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2583969
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2583970
    new-instance v1, LX/IWU;

    invoke-direct {v1, p0, p2}, LX/IWU;-><init>(LX/IWV;LX/DLO;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2583971
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V
    .locals 1
    .param p3    # LX/DLO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2583972
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2583973
    if-eqz v0, :cond_0

    .line 2583974
    invoke-interface {v0, p2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2583975
    invoke-virtual {p0, p1, p3}, LX/IWV;->a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V

    .line 2583976
    :cond_0
    return-void
.end method
