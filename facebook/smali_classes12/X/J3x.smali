.class public final LX/J3x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;",
        ">;",
        "LX/J4L;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J3y;


# direct methods
.method public constructor <init>(LX/J3y;)V
    .locals 0

    .prologue
    .line 2643424
    iput-object p1, p0, LX/J3x;->a:LX/J3y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643425
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2643426
    iget-object v0, p0, LX/J3x;->a:LX/J3y;

    iget-object v0, v0, LX/J3y;->b:LX/J45;

    iget-object v0, v0, LX/J45;->f:LX/J4N;

    const/4 v3, 0x0

    .line 2643427
    if-eqz p1, :cond_0

    .line 2643428
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643429
    if-eqz v1, :cond_0

    .line 2643430
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643431
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643432
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643433
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643434
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643435
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v3

    .line 2643436
    :goto_0
    move-object v0, v1

    .line 2643437
    return-object v0

    .line 2643438
    :cond_1
    new-instance v2, LX/J4L;

    sget-object v1, LX/J4K;->APPS_STEP:LX/J4K;

    invoke-direct {v2, v1}, LX/J4L;-><init>(LX/J4K;)V

    .line 2643439
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643440
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;

    move-result-object v1

    .line 2643441
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->c()Z

    move-result v4

    if-nez v4, :cond_2

    move-object v1, v2

    .line 2643442
    goto :goto_0

    .line 2643443
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->c()Z

    move-result v4

    .line 2643444
    iput-boolean v4, v2, LX/J4L;->i:Z

    .line 2643445
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/J4L;->a(LX/0Px;)V

    .line 2643446
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;

    move-result-object v4

    sget-object p0, LX/J4K;->APPS_STEP:LX/J4K;

    invoke-static {v0, v4, p0}, LX/J4N;->a(LX/J4N;LX/J5E;LX/J4K;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/J4L;->a(Ljava/util/Collection;)V

    .line 2643447
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2643448
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v3

    invoke-interface {v3}, LX/0us;->b()Z

    move-result v3

    .line 2643449
    iput-boolean v3, v2, LX/J4L;->k:Z

    .line 2643450
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupAppsStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$AppReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    .line 2643451
    iput-object v1, v2, LX/J4L;->j:Ljava/lang/String;

    .line 2643452
    :goto_1
    move-object v1, v2

    .line 2643453
    goto :goto_0

    .line 2643454
    :cond_3
    const/4 v1, 0x0

    .line 2643455
    iput-boolean v1, v2, LX/J4L;->k:Z

    .line 2643456
    iput-object v3, v2, LX/J4L;->j:Ljava/lang/String;

    .line 2643457
    goto :goto_1
.end method
