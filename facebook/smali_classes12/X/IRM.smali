.class public LX/IRM;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2576378
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IRM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2576379
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2576380
    const v0, 0x7f0e0786

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576381
    const-class v0, LX/IRM;

    invoke-static {v0, p0}, LX/IRM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2576382
    const v0, 0x7f03083f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2576383
    const v0, 0x7f0d1587

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2576384
    new-instance p2, LX/IRL;

    invoke-direct {p2, p0, p1}, LX/IRL;-><init>(LX/IRM;Landroid/content/Context;)V

    .line 2576385
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2576386
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IRM;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object p0, p1, LX/IRM;->a:LX/17W;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2576387
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2576388
    iput-object p1, p0, LX/IRM;->b:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2576389
    return-void
.end method
