.class public LX/JNC;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/JNL;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;Ljava/lang/String;Ljava/lang/String;LX/JNL;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;",
            ">;TE;",
            "LX/25M;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/JNL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685409
    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2685410
    iput-object p7, p0, LX/JNC;->c:LX/JNL;

    .line 2685411
    iput-object p5, p0, LX/JNC;->d:Ljava/lang/String;

    .line 2685412
    iput-object p6, p0, LX/JNC;->e:Ljava/lang/String;

    .line 2685413
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2685414
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2685415
    check-cast p2, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    .line 2685416
    iget-object v0, p0, LX/JNC;->c:LX/JNL;

    const/4 v1, 0x0

    .line 2685417
    new-instance v2, LX/JNK;

    invoke-direct {v2, v0}, LX/JNK;-><init>(LX/JNL;)V

    .line 2685418
    iget-object v3, v0, LX/JNL;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JNJ;

    .line 2685419
    if-nez v3, :cond_0

    .line 2685420
    new-instance v3, LX/JNJ;

    invoke-direct {v3, v0}, LX/JNJ;-><init>(LX/JNL;)V

    .line 2685421
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JNJ;->a$redex0(LX/JNJ;LX/1De;IILX/JNK;)V

    .line 2685422
    move-object v2, v3

    .line 2685423
    move-object v1, v2

    .line 2685424
    move-object v0, v1

    .line 2685425
    iget-object v1, v0, LX/JNJ;->a:LX/JNK;

    iput-object p2, v1, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    .line 2685426
    iget-object v1, v0, LX/JNJ;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2685427
    move-object v0, v0

    .line 2685428
    iget-object v1, p0, LX/JNC;->d:Ljava/lang/String;

    .line 2685429
    iget-object v2, v0, LX/JNJ;->a:LX/JNK;

    iput-object v1, v2, LX/JNK;->c:Ljava/lang/String;

    .line 2685430
    iget-object v2, v0, LX/JNJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2685431
    move-object v0, v0

    .line 2685432
    iget-object v1, p0, LX/JNC;->e:Ljava/lang/String;

    .line 2685433
    iget-object v2, v0, LX/JNJ;->a:LX/JNK;

    iput-object v1, v2, LX/JNK;->b:Ljava/lang/String;

    .line 2685434
    iget-object v2, v0, LX/JNJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2685435
    move-object v0, v0

    .line 2685436
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2685437
    const/4 v0, 0x0

    return v0
.end method
