.class public LX/Hmf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Hmf;


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2500975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500976
    iput-object p1, p0, LX/Hmf;->a:LX/0if;

    .line 2500977
    return-void
.end method

.method public static a(LX/0QB;)LX/Hmf;
    .locals 4

    .prologue
    .line 2500962
    sget-object v0, LX/Hmf;->b:LX/Hmf;

    if-nez v0, :cond_1

    .line 2500963
    const-class v1, LX/Hmf;

    monitor-enter v1

    .line 2500964
    :try_start_0
    sget-object v0, LX/Hmf;->b:LX/Hmf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2500965
    if-eqz v2, :cond_0

    .line 2500966
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2500967
    new-instance p0, LX/Hmf;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/Hmf;-><init>(LX/0if;)V

    .line 2500968
    move-object v0, p0

    .line 2500969
    sput-object v0, LX/Hmf;->b:LX/Hmf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2500970
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2500971
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2500972
    :cond_1
    sget-object v0, LX/Hmf;->b:LX/Hmf;

    return-object v0

    .line 2500973
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2500974
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Hmf;LX/Hmd;)V
    .locals 3

    .prologue
    .line 2500960
    iget-object v0, p0, LX/Hmf;->a:LX/0if;

    sget-object v1, LX/0ig;->aX:LX/0ih;

    iget-object v2, p1, LX/Hmd;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2500961
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2500957
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "prefilledConnectionCode"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2500958
    iget-object v1, p0, LX/Hmf;->a:LX/0if;

    sget-object v2, LX/0ig;->aX:LX/0ih;

    sget-object v3, LX/Hmd;->CONNECT_INPUT_SCREEN_OPENED:LX/Hmd;

    iget-object v3, v3, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500959
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2500955
    iget-object v0, p0, LX/Hmf;->a:LX/0if;

    sget-object v1, LX/0ig;->aX:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2500956
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2500946
    if-eqz p1, :cond_0

    sget-object v0, LX/Hmd;->WIFI_CHANGE_SUCCEEDED:LX/Hmd;

    :goto_0
    invoke-static {p0, v0}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500947
    return-void

    .line 2500948
    :cond_0
    sget-object v0, LX/Hmd;->WIFI_CHANGE_FAILED:LX/Hmd;

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2500952
    if-eqz p1, :cond_0

    sget-object v0, LX/Hmd;->SOCKET_CREATE_SUCCEEDED:LX/Hmd;

    :goto_0
    invoke-static {p0, v0}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500953
    return-void

    .line 2500954
    :cond_0
    sget-object v0, LX/Hmd;->SOCKET_CREATE_FAILED:LX/Hmd;

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2500949
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "errorMessage"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2500950
    iget-object v1, p0, LX/Hmf;->a:LX/0if;

    sget-object v2, LX/0ig;->aX:LX/0ih;

    sget-object v3, LX/Hmd;->ERROR_SCREEN_OPENED:LX/Hmd;

    iget-object v3, v3, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500951
    return-void
.end method
