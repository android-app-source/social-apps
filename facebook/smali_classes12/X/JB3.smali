.class public final LX/JB3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2657984
    const/4 v10, 0x0

    .line 2657985
    const/4 v9, 0x0

    .line 2657986
    const/4 v8, 0x0

    .line 2657987
    const/4 v7, 0x0

    .line 2657988
    const/4 v6, 0x0

    .line 2657989
    const/4 v5, 0x0

    .line 2657990
    const/4 v4, 0x0

    .line 2657991
    const/4 v3, 0x0

    .line 2657992
    const/4 v2, 0x0

    .line 2657993
    const/4 v1, 0x0

    .line 2657994
    const/4 v0, 0x0

    .line 2657995
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2657996
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2657997
    const/4 v0, 0x0

    .line 2657998
    :goto_0
    return v0

    .line 2657999
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2658000
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 2658001
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2658002
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2658003
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2658004
    const-string v12, "collection_item_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2658005
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 2658006
    :cond_2
    const-string v12, "icon_image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2658007
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2658008
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2658009
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2658010
    :cond_4
    const-string v12, "listImage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2658011
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2658012
    :cond_5
    const-string v12, "locally_updated_containing_collection_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2658013
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2658014
    :cond_6
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2658015
    invoke-static {p0, p1}, LX/JB8;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2658016
    :cond_7
    const-string v12, "rating"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2658017
    invoke-static {p0, p1}, LX/JAz;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2658018
    :cond_8
    const-string v12, "subtitle_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2658019
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2658020
    :cond_9
    const-string v12, "tableImage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2658021
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2658022
    :cond_a
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 2658023
    invoke-static {p0, p1}, LX/JB2;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 2658024
    :cond_b
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2658025
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 2658026
    :cond_c
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2658027
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2658028
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2658029
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2658030
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 2658031
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2658032
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2658033
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 2658034
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 2658035
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2658036
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 2658037
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2658038
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2658039
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2658040
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2658041
    if-eqz v0, :cond_0

    .line 2658042
    const-string v0, "collection_item_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658043
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658044
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658045
    if-eqz v0, :cond_1

    .line 2658046
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658047
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2658048
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658049
    if-eqz v0, :cond_2

    .line 2658050
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658051
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658052
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658053
    if-eqz v0, :cond_3

    .line 2658054
    const-string v1, "listImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658055
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2658056
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658057
    if-eqz v0, :cond_4

    .line 2658058
    const-string v1, "locally_updated_containing_collection_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658059
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658060
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658061
    if-eqz v0, :cond_5

    .line 2658062
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658063
    invoke-static {p0, v0, p2, p3}, LX/JB8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2658064
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658065
    if-eqz v0, :cond_7

    .line 2658066
    const-string v1, "rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658067
    const-wide/16 v4, 0x0

    .line 2658068
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2658069
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2658070
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_6

    .line 2658071
    const-string v4, "value"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658072
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 2658073
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2658074
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658075
    if-eqz v0, :cond_8

    .line 2658076
    const-string v1, "subtitle_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658077
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2658078
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658079
    if-eqz v0, :cond_9

    .line 2658080
    const-string v1, "tableImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658081
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2658082
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2658083
    if-eqz v0, :cond_a

    .line 2658084
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658085
    invoke-static {p0, v0, p2, p3}, LX/JB2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2658086
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2658087
    if-eqz v0, :cond_b

    .line 2658088
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2658089
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2658090
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2658091
    return-void
.end method
