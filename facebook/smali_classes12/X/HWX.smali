.class public final LX/HWX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;J)V
    .locals 0

    .prologue
    .line 2476516
    iput-object p1, p0, LX/HWX;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    iput-wide p2, p0, LX/HWX;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x5819ea3d

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2476517
    iget-object v0, p0, LX/HWX;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->c:LX/9XE;

    sget-object v2, LX/9XI;->EVENT_TAPPED_REPORT_PROBLEM:LX/9XI;

    iget-wide v4, p0, LX/HWX;->a:J

    invoke-virtual {v0, v2, v4, v5}, LX/9XE;->a(LX/9X2;J)V

    .line 2476518
    iget-object v0, p0, LX/HWX;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v2

    iget-object v3, p0, LX/HWX;->b:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v2

    invoke-virtual {v2}, LX/6FX;->a()LX/6FY;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6G2;->a(LX/6FY;)V

    .line 2476519
    const v0, 0x389731e0

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
