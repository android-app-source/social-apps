.class public final enum LX/IcH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IcH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IcH;

.field public static final enum GET_CURRENT_LOCATION:LX/IcH;

.field public static final enum GET_RIDE_PROVIDER:LX/IcH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2595367
    new-instance v0, LX/IcH;

    const-string v1, "GET_CURRENT_LOCATION"

    invoke-direct {v0, v1, v2}, LX/IcH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IcH;->GET_CURRENT_LOCATION:LX/IcH;

    .line 2595368
    new-instance v0, LX/IcH;

    const-string v1, "GET_RIDE_PROVIDER"

    invoke-direct {v0, v1, v3}, LX/IcH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IcH;->GET_RIDE_PROVIDER:LX/IcH;

    .line 2595369
    const/4 v0, 0x2

    new-array v0, v0, [LX/IcH;

    sget-object v1, LX/IcH;->GET_CURRENT_LOCATION:LX/IcH;

    aput-object v1, v0, v2

    sget-object v1, LX/IcH;->GET_RIDE_PROVIDER:LX/IcH;

    aput-object v1, v0, v3

    sput-object v0, LX/IcH;->$VALUES:[LX/IcH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2595370
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IcH;
    .locals 1

    .prologue
    .line 2595366
    const-class v0, LX/IcH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IcH;

    return-object v0
.end method

.method public static values()[LX/IcH;
    .locals 1

    .prologue
    .line 2595365
    sget-object v0, LX/IcH;->$VALUES:[LX/IcH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IcH;

    return-object v0
.end method
