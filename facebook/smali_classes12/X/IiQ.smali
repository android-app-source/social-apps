.class public final LX/IiQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2604311
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2604312
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604313
    :goto_0
    return v1

    .line 2604314
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604315
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2604316
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2604317
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2604318
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2604319
    const-string v3, "edges"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2604320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2604321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2604322
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2604323
    invoke-static {p0, p1}, LX/IiP;->b(LX/15w;LX/186;)I

    move-result v2

    .line 2604324
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2604325
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2604326
    goto :goto_1

    .line 2604327
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2604328
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2604329
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2604330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604331
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604332
    if-eqz v0, :cond_3

    .line 2604333
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604334
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2604335
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2604336
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2604337
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604338
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2604339
    if-eqz v3, :cond_1

    .line 2604340
    const-string p1, "matched_messages"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604341
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2604342
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 2604343
    invoke-virtual {p0, v3, p1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/IiO;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604344
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 2604345
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2604346
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604347
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2604348
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2604349
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604350
    return-void
.end method
