.class public final LX/IHS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2560308
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2560309
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560310
    :goto_0
    return v1

    .line 2560311
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560312
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2560313
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2560314
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560315
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2560316
    const-string v3, "location_privacy_options"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2560317
    invoke-static {p0, p1}, LX/GUG;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2560318
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2560319
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2560320
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2560321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560322
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560323
    if-eqz v0, :cond_0

    .line 2560324
    const-string v1, "location_privacy_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560325
    invoke-static {p0, v0, p2, p3}, LX/GUG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560326
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560327
    return-void
.end method
