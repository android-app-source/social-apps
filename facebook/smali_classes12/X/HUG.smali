.class public final LX/HUG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/21D;

.field public final synthetic b:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final synthetic e:Lcom/facebook/ipc/composer/intent/ComposerPageData;

.field public final synthetic f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;LX/21D;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V
    .locals 0

    .prologue
    .line 2471689
    iput-object p1, p0, LX/HUG;->f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iput-object p2, p0, LX/HUG;->a:LX/21D;

    iput-object p3, p0, LX/HUG;->b:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    iput-object p4, p0, LX/HUG;->c:Ljava/lang/String;

    iput-object p5, p0, LX/HUG;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object p6, p0, LX/HUG;->e:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, 0x6040e7f8

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2471690
    iget-object v1, p0, LX/HUG;->f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->h:LX/HTY;

    invoke-virtual {v1}, LX/HTY;->b()V

    .line 2471691
    iget-object v1, p0, LX/HUG;->a:LX/21D;

    const-string v2, "pagesEndorsementsTab"

    iget-object v3, p0, LX/HUG;->b:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->q()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/HUG;->f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->d:LX/1Nq;

    iget-object v3, p0, LX/HUG;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/HUG;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/HUG;->e:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2471692
    iget-object v2, p0, LX/HUG;->f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->c:LX/1Kf;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HUG;->f:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v1, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2471693
    const v1, 0x7efcc3b8

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
