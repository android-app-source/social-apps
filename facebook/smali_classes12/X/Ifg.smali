.class public LX/Ifg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/DdT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600053
    return-void
.end method

.method private static a(LX/Ifg;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I
    .locals 1

    .prologue
    .line 2600005
    iget-object v0, p0, LX/Ifg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DdT;

    invoke-virtual {v0, p1}, LX/DdT;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method private static a(LX/Ifg;J)J
    .locals 5

    .prologue
    .line 2600054
    const-wide/16 v0, 0x0

    iget-object v2, p0, LX/Ifg;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(LX/0oG;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5
    .param p3    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2600029
    const-string v0, "reminder_type"

    invoke-virtual {p1, v0, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2600030
    if-eqz p3, :cond_0

    .line 2600031
    const-string v0, "thread_id"

    invoke-virtual {p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "conversation_size"

    invoke-static {p0, p3}, LX/Ifg;->a(LX/Ifg;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2600032
    :cond_0
    const-string v0, "message_id"

    invoke-virtual {p1, v0, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2600033
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-lez v0, :cond_1

    .line 2600034
    const-string v0, "time_until_reminder"

    invoke-static {p0, p5, p6}, LX/Ifg;->a(LX/Ifg;J)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2600035
    :cond_1
    if-eqz p7, :cond_2

    .line 2600036
    const-string v0, "triggered_word"

    invoke-virtual {p1, v0, p7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2600037
    :cond_2
    return-void
.end method

.method public static a(LX/Ifg;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLLightweightEventType;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/enums/GraphQLLightweightEventType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2600038
    iget-object v0, p0, LX/Ifg;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2600039
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2600040
    const-string v0, "event_reminders"

    invoke-virtual {v1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2600041
    const-string v2, "reminder_type"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLightweightEventType;->CALL:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    if-ne p2, v0, :cond_3

    const-string v0, "CALL"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2600042
    if-eqz p4, :cond_0

    .line 2600043
    invoke-virtual {p4}, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->c()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, LX/Ifg;->a(LX/Ifg;J)J

    move-result-wide v2

    .line 2600044
    const-string v0, "reminder_id"

    .line 2600045
    iget-object v4, p4, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2600046
    invoke-virtual {v1, v0, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v4, "time_until_reminder"

    invoke-virtual {v0, v4, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2600047
    :cond_0
    if-eqz p3, :cond_1

    .line 2600048
    const-string v0, "thread_id"

    invoke-virtual {p3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v2, "conversation_size"

    invoke-static {p0, p3}, LX/Ifg;->a(LX/Ifg;Lcom/facebook/messaging/model/threadkey/ThreadKey;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2600049
    :cond_1
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2600050
    :cond_2
    return-void

    .line 2600051
    :cond_3
    const-string v0, "EVENT"

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JLjava/lang/String;)V
    .locals 10
    .param p2    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2600023
    iget-object v0, p0, LX/Ifg;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2600024
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600025
    const-string v0, "event_reminders"

    invoke-virtual {v2, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2600026
    const-string v3, "EVENT"

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/Ifg;->a(LX/0oG;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JLjava/lang/String;)V

    .line 2600027
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2600028
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/Ifg;
    .locals 4

    .prologue
    .line 2600019
    new-instance v2, LX/Ifg;

    invoke-direct {v2}, LX/Ifg;-><init>()V

    .line 2600020
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v3, 0x26ef

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 2600021
    iput-object v0, v2, LX/Ifg;->a:LX/0Zb;

    iput-object v1, v2, LX/Ifg;->b:LX/0SG;

    iput-object v3, v2, LX/Ifg;->c:LX/0Or;

    .line 2600022
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 10

    .prologue
    .line 2600006
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2600007
    :cond_0
    return-void

    .line 2600008
    :cond_1
    iget-object v8, p1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_0

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    .line 2600009
    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    sget-object v2, LX/5do;->CONCEPT:LX/5do;

    if-ne v1, v2, :cond_4

    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-interface {v1}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;->b()LX/5dq;

    move-result-object v1

    sget-object v2, LX/5dq;->TIMESTAMP:LX/5dq;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2600010
    if-eqz v1, :cond_3

    .line 2600011
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    check-cast v1, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    iget-wide v4, v1, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 2600012
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    iget v2, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    iget v3, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    iget v6, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    add-int/2addr v3, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    move-object v6, v1

    .line 2600013
    const-string v1, "event_reminder_timestamp_concept_found"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/Ifg;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JLjava/lang/String;)V

    .line 2600014
    :cond_2
    :goto_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2600015
    :cond_3
    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    sget-object v2, LX/5do;->INTENT:LX/5do;

    if-ne v1, v2, :cond_5

    iget-object v1, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-interface {v1}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;->b()LX/5dq;

    move-result-object v1

    sget-object v2, LX/5dq;->CREATE_EVENT:LX/5dq;

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2600016
    if-eqz v1, :cond_2

    .line 2600017
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    check-cast v0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;

    iget-wide v2, v0, Lcom/facebook/messaging/model/messagemetadata/CreateEventMetadata;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 2600018
    const-string v1, "event_reminder_create_event_intent_found"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LX/Ifg;->a(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method
