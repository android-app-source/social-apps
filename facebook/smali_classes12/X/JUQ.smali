.class public LX/JUQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Zb;

.field private final c:LX/E1f;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/E1f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2698268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2698269
    iput-object p1, p0, LX/JUQ;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2698270
    iput-object p2, p0, LX/JUQ;->b:LX/0Zb;

    .line 2698271
    iput-object p3, p0, LX/JUQ;->c:LX/E1f;

    .line 2698272
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Landroid/content/Context;)LX/Cfl;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 2698254
    iget-object v0, p0, LX/JUQ;->c:LX/E1f;

    const/4 v9, 0x0

    .line 2698255
    if-nez p1, :cond_1

    .line 2698256
    :cond_0
    :goto_0
    move-object v1, v9

    .line 2698257
    const-string v5, "ANDROID_PAGE_CONTEXTUAL_RECOMMENDATIONS"

    move-object v2, p2

    move-object v4, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    return-object v0

    .line 2698258
    :cond_1
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 2698259
    invoke-static {v7, p1}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;)I

    move-result v8

    .line 2698260
    if-eqz v8, :cond_0

    .line 2698261
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 2698262
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2698263
    const/4 v7, 0x0

    invoke-virtual {v8, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2698264
    new-instance v7, LX/15i;

    const/4 v11, 0x1

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2698265
    instance-of v8, p1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v8, :cond_2

    .line 2698266
    const-string v8, "PageContextualRecommendationsConversionsHelper.getReactionStoryAttachmentActionCommonFragment"

    invoke-virtual {v7, v8, p1}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2698267
    :cond_2
    new-instance v9, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;

    invoke-direct {v9, v7}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionCommonFragmentModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/JUQ;
    .locals 4

    .prologue
    .line 2698252
    new-instance v3, LX/JUQ;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v2

    check-cast v2, LX/E1f;

    invoke-direct {v3, v0, v1, v2}, LX/JUQ;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/E1f;)V

    .line 2698253
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2698241
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_1

    .line 2698242
    :cond_0
    :goto_0
    return-void

    .line 2698243
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2698244
    invoke-direct {p0, p3, v0}, LX/JUQ;->a(Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Landroid/content/Context;)LX/Cfl;

    move-result-object v1

    .line 2698245
    if-eqz v1, :cond_0

    .line 2698246
    iget-object v2, v1, LX/Cfl;->a:LX/Cfc;

    invoke-virtual {p0, p2, p4, v2}, LX/JUQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/Cfc;)V

    .line 2698247
    iget-object v1, v1, LX/Cfl;->d:Landroid/content/Intent;

    .line 2698248
    if-eqz v1, :cond_0

    .line 2698249
    const-string v2, "launch_external_activity"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2698250
    iget-object v2, p0, LX/JUQ;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2698251
    :cond_2
    iget-object v2, p0, LX/JUQ;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/Cfc;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "LX/Cfc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2698232
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2698233
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    .line 2698234
    iget-object v1, p0, LX/JUQ;->b:LX/0Zb;

    const-string v2, "reaction_unit_interaction"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2698235
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2698236
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2698237
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    .line 2698238
    const-string v1, "reaction_overlay"

    invoke-virtual {v2, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v3, "component_tracking_data"

    invoke-virtual {v1, v3, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v3, "surface"

    const-string v4, "android_page_contextual_recommendations"

    invoke-virtual {v1, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v3, "impression_info"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "interacted_story_type"

    const-string v3, "509240112603598"

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "interaction_type"

    iget-object v3, p3, LX/Cfc;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2698239
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2698240
    :cond_0
    return-void
.end method
