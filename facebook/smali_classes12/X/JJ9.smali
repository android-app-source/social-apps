.class public final LX/JJ9;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2678960
    iput-object p1, p0, LX/JJ9;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 8

    .prologue
    .line 2678961
    iget-object v0, p0, LX/JJ9;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->h:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2678962
    iget-object v0, p0, LX/JJ9;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->b:LX/JJ5;

    iget-object v1, p0, LX/JJ9;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/JJ9;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    .line 2678963
    iget-object p0, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->i:LX/0Vd;

    if-nez p0, :cond_0

    .line 2678964
    new-instance p0, LX/JJB;

    invoke-direct {p0, v2}, LX/JJB;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    iput-object p0, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->i:LX/0Vd;

    .line 2678965
    :cond_0
    iget-object p0, v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->i:LX/0Vd;

    move-object v2, p0

    .line 2678966
    iget-object v3, v0, LX/JJ5;->d:Ljava/lang/Runnable;

    if-eqz v3, :cond_1

    .line 2678967
    iget-object v3, v0, LX/JJ5;->c:Landroid/os/Handler;

    iget-object v4, v0, LX/JJ5;->d:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2678968
    :cond_1
    new-instance v3, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;-><init>(LX/JJ5;Ljava/lang/String;LX/0Vd;)V

    iput-object v3, v0, LX/JJ5;->d:Ljava/lang/Runnable;

    .line 2678969
    iget-object v3, v0, LX/JJ5;->c:Landroid/os/Handler;

    iget-object v4, v0, LX/JJ5;->d:Ljava/lang/Runnable;

    const-wide/16 v5, 0x12c

    const v7, -0x7d7840f9

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2678970
    return-void
.end method
