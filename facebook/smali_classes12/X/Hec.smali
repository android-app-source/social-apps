.class public final LX/Hec;
.super LX/3Ii;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;


# direct methods
.method public constructor <init>(Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)V
    .locals 0

    .prologue
    .line 2490565
    iput-object p1, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-direct {p0}, LX/3Ii;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2490566
    invoke-super {p0, p1}, LX/3Ii;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 2490567
    iget-object v0, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->c()V

    .line 2490568
    iget-object v0, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2490569
    iget-object v1, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iget-object v1, v1, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->o:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2490570
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2490571
    :goto_0
    iget-object v1, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2490572
    iget-object v0, p0, LX/Hec;->a:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->requestLayout()V

    .line 2490573
    return-void

    .line 2490574
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method
