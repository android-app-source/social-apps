.class public LX/HxN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Kf;

.field public final b:LX/7v8;

.field private final c:LX/CEF;


# direct methods
.method public constructor <init>(LX/1Kf;LX/7v8;LX/CEF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522381
    iput-object p1, p0, LX/HxN;->a:LX/1Kf;

    .line 2522382
    iput-object p2, p0, LX/HxN;->b:LX/7v8;

    .line 2522383
    iput-object p3, p0, LX/HxN;->c:LX/CEF;

    .line 2522384
    return-void
.end method

.method public static a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4

    .prologue
    .line 2522377
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 2522378
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    .line 2522379
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->o()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/HxN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2rX;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4
    .param p3    # LX/2rX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2522385
    new-instance v0, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->USER:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 2522386
    iput-object p1, v0, LX/89I;->c:Ljava/lang/String;

    .line 2522387
    move-object v0, v0

    .line 2522388
    iput-object p2, v0, LX/89I;->d:Ljava/lang/String;

    .line 2522389
    move-object v0, v0

    .line 2522390
    if-eqz p3, :cond_0

    .line 2522391
    invoke-virtual {v0, p3}, LX/89I;->a(LX/2rX;)LX/89I;

    .line 2522392
    :cond_0
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/HxN;
    .locals 4

    .prologue
    .line 2522374
    new-instance v3, LX/HxN;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v0

    check-cast v0, LX/1Kf;

    invoke-static {p0}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v1

    check-cast v1, LX/7v8;

    invoke-static {p0}, LX/CEF;->b(LX/0QB;)LX/CEF;

    move-result-object v2

    check-cast v2, LX/CEF;

    invoke-direct {v3, v0, v1, v2}, LX/HxN;-><init>(LX/1Kf;LX/7v8;LX/CEF;)V

    .line 2522375
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2522376
    iget-object v0, p0, LX/HxN;->c:LX/CEF;

    invoke-virtual {v0}, LX/CEF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/89K;

    invoke-direct {v0}, LX/89K;-><init>()V

    invoke-static {}, LX/CEK;->c()LX/CEK;

    move-result-object v0

    invoke-static {v0}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
