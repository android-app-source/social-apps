.class public final LX/ILD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;",
        ">;",
        "LX/ILO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ILF;


# direct methods
.method public constructor <init>(LX/ILF;)V
    .locals 0

    .prologue
    .line 2567736
    iput-object p1, p0, LX/ILD;->a:LX/ILF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2567737
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2567738
    if-eqz p1, :cond_0

    .line 2567739
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567740
    if-eqz v0, :cond_0

    .line 2567741
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567742
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2567743
    :cond_0
    const/4 v0, 0x0

    .line 2567744
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, LX/ILO;

    .line 2567745
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567746
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 2567747
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567748
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2567749
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567750
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityAllChildGroupsGraphQLModels$FetchCommunityAllChildGroupsModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, LX/ILO;-><init>(LX/0Px;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0
.end method
