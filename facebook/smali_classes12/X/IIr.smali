.class public final LX/IIr;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562216
    iput-object p1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 2562214
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562215
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2562213
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2562176
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x0

    .line 2562177
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562178
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aD:LX/IIm;

    .line 2562179
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562180
    if-eqz v2, :cond_0

    .line 2562181
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562182
    :goto_0
    return-void

    .line 2562183
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562184
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 2562206
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aH:LX/IIm;

    .line 2562207
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562208
    if-eqz v2, :cond_0

    .line 2562209
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562210
    :goto_0
    return-void

    .line 2562211
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562212
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2562199
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aF:LX/IIm;

    .line 2562200
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562201
    if-eqz v2, :cond_0

    .line 2562202
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562203
    :goto_0
    return-void

    .line 2562204
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562205
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2562192
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aG:LX/IIm;

    .line 2562193
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562194
    if-eqz v2, :cond_0

    .line 2562195
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562196
    :goto_0
    return-void

    .line 2562197
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562198
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2562185
    iget-object v0, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIr;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aI:LX/IIm;

    .line 2562186
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562187
    if-eqz v2, :cond_0

    .line 2562188
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562189
    :goto_0
    return-void

    .line 2562190
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562191
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method
