.class public final LX/I9s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/IA4;


# direct methods
.method public constructor <init>(LX/IA4;)V
    .locals 0

    .prologue
    .line 2544304
    iput-object p1, p0, LX/I9s;->a:LX/IA4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2544305
    iget-object v0, p0, LX/I9s;->a:LX/IA4;

    invoke-static {v0}, LX/IA4;->j(LX/IA4;)V

    .line 2544306
    iget-object v0, p0, LX/I9s;->a:LX/IA4;

    invoke-static {v0, v2}, LX/IA4;->a$redex0(LX/IA4;LX/Blc;)V

    .line 2544307
    iget-object v0, p0, LX/I9s;->a:LX/IA4;

    iget-object v0, v0, LX/IA4;->A:LX/I9f;

    iget-object v1, p0, LX/I9s;->a:LX/IA4;

    iget-object v1, v1, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    .line 2544308
    new-instance v3, LX/4EH;

    invoke-direct {v3}, LX/4EH;-><init>()V

    .line 2544309
    new-instance v4, LX/4EG;

    invoke-direct {v4}, LX/4EG;-><init>()V

    .line 2544310
    iget-object v5, v0, LX/I9f;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2544311
    iget-object v6, v5, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v5, v6

    .line 2544312
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2544313
    new-instance v5, LX/4EG;

    invoke-direct {v5}, LX/4EG;-><init>()V

    .line 2544314
    iget-object v6, v0, LX/I9f;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2544315
    iget-object p2, v6, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v6, p2

    .line 2544316
    invoke-virtual {v6}, Lcom/facebook/events/common/ActionSource;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2544317
    iget-object v6, v0, LX/I9f;->e:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v6}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 2544318
    new-instance v6, LX/4EL;

    invoke-direct {v6}, LX/4EL;-><init>()V

    .line 2544319
    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2544320
    move-object v4, v6

    .line 2544321
    const-string v5, "context"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2544322
    move-object v3, v3

    .line 2544323
    iget-object v4, v0, LX/I9f;->a:Ljava/lang/String;

    .line 2544324
    const-string v5, "event_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544325
    move-object v3, v3

    .line 2544326
    iget-object v4, v1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2544327
    const-string v5, "target_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544328
    move-object v3, v3

    .line 2544329
    const/4 v4, 0x0

    invoke-static {v0, v4}, LX/I9f;->b(LX/I9f;LX/Blc;)Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    move-result-object v4

    .line 2544330
    new-instance v5, LX/7uC;

    invoke-direct {v5}, LX/7uC;-><init>()V

    move-object v5, v5

    .line 2544331
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2544332
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v3

    .line 2544333
    iget-object v4, v0, LX/I9f;->g:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2544334
    iget-object v4, v0, LX/I9f;->f:LX/1Ck;

    .line 2544335
    iget-object v5, v1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2544336
    new-instance v6, Ljava/lang/StringBuilder;

    const-string p2, "tasks-adminRemoveGuestEvent:"

    invoke-direct {v6, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p2, v0, LX/I9f;->a:Ljava/lang/String;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string p2, ":"

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2544337
    new-instance v6, LX/I9d;

    invoke-direct {v6, v0}, LX/I9d;-><init>(LX/I9f;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2544338
    iget-object v0, p0, LX/I9s;->a:LX/IA4;

    iget-object v0, v0, LX/IA4;->y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, p0, LX/I9s;->a:LX/IA4;

    iget-object v1, v1, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->a(Lcom/facebook/events/model/EventUser;LX/Blc;)V

    .line 2544339
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2544340
    return-void
.end method
