.class public LX/HwR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7kt;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/photos/annotation/MaxNumberPhotosPerUpload;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7kt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2520877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520878
    iput-object p1, p0, LX/HwR;->a:LX/0Ot;

    .line 2520879
    iput-object p2, p0, LX/HwR;->b:LX/0Ot;

    .line 2520880
    iput-object p3, p0, LX/HwR;->c:LX/0Ot;

    .line 2520881
    iput-object p4, p0, LX/HwR;->d:LX/0Ot;

    .line 2520882
    iput-object p5, p0, LX/HwR;->e:LX/0Ot;

    .line 2520883
    iput-object p6, p0, LX/HwR;->f:LX/0Ot;

    .line 2520884
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;Z)LX/7ks;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z)",
            "LX/7ks;"
        }
    .end annotation

    .prologue
    .line 2520885
    iget-object v0, p0, LX/HwR;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kt;

    invoke-virtual {v0, p2, p3}, LX/7kt;->a(LX/0Px;Z)LX/7ks;

    move-result-object v1

    .line 2520886
    iget-object v0, v1, LX/7ks;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p2}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2520887
    iget-object v0, p0, LX/HwR;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_MOVIE_FAILURE:LX/0ge;

    invoke-virtual {v0, v2, p1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2520888
    :cond_0
    :goto_0
    iget-object v0, v1, LX/7ks;->b:LX/0Px;

    sget-object v2, LX/7kr;->NONEXISTANT_PHOTO:LX/7kr;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2520889
    iget-object v0, p0, LX/HwR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "composer_non_existing_attachment"

    const-string v3, "Tried to share nonexistent photo"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2520890
    iget-object v0, p0, LX/HwR;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f081408

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    const/16 v3, 0x11

    .line 2520891
    iput v3, v2, LX/27k;->b:I

    .line 2520892
    move-object v2, v2

    .line 2520893
    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2520894
    :cond_1
    iget-object v0, v1, LX/7ks;->b:LX/0Px;

    sget-object v2, LX/7kr;->TOO_MANY_PHOTOS:LX/7kr;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2520895
    iget-object v0, p0, LX/HwR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "composer_too_many_attachments"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Tried to attach "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " attachments"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2520896
    iget-object v0, p0, LX/HwR;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f08142e

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/HwR;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, LX/HwR;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2520897
    iget-object v0, p0, LX/HwR;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v3, LX/27k;

    invoke-direct {v3, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2520898
    :cond_2
    return-object v1

    .line 2520899
    :cond_3
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, LX/7ks;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2520900
    iget-object v0, p0, LX/HwR;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ATTACH_PHOTO_FAILURE:LX/0ge;

    invoke-virtual {v0, v2, p1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
