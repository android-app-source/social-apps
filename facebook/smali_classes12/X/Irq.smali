.class public final LX/Irq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618761
    iput-object p1, p0, LX/Irq;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 2618762
    if-nez p2, :cond_1

    .line 2618763
    iget-object v0, p0, LX/Irq;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2618764
    iget-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iqk;

    .line 2618765
    iget-object p2, v1, LX/Iqk;->c:Landroid/view/View;

    move-object p2, p2

    .line 2618766
    if-ne p2, p1, :cond_0

    .line 2618767
    iget-object v2, v1, LX/Iqk;->b:LX/Iqg;

    move-object v1, v2

    .line 2618768
    :goto_0
    move-object v0, v1

    .line 2618769
    if-nez v0, :cond_2

    .line 2618770
    :cond_1
    :goto_1
    return-void

    .line 2618771
    :cond_2
    invoke-virtual {v0}, LX/Iqg;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2618772
    iget-object v1, p0, LX/Irq;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v1, v0}, LX/Ird;->c(LX/Iqg;)V

    goto :goto_1

    .line 2618773
    :cond_3
    iget-object v1, p0, LX/Irq;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618774
    if-eqz v0, :cond_1

    .line 2618775
    invoke-virtual {v0}, LX/Iqk;->g()V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
