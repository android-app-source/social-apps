.class public LX/HK7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HK8;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HK7",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HK8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453713
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453714
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HK7;->b:LX/0Zi;

    .line 2453715
    iput-object p1, p0, LX/HK7;->a:LX/0Ot;

    .line 2453716
    return-void
.end method

.method public static a(LX/0QB;)LX/HK7;
    .locals 4

    .prologue
    .line 2453672
    const-class v1, LX/HK7;

    monitor-enter v1

    .line 2453673
    :try_start_0
    sget-object v0, LX/HK7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453674
    sput-object v2, LX/HK7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453675
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453676
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453677
    new-instance v3, LX/HK7;

    const/16 p0, 0x2bc0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HK7;-><init>(LX/0Ot;)V

    .line 2453678
    move-object v0, v3

    .line 2453679
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453680
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HK7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453681
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453682
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2453685
    check-cast p2, LX/HK6;

    .line 2453686
    iget-object v0, p0, LX/HK7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HK8;

    iget-object v1, p2, LX/HK6;->a:LX/HLO;

    iget-object v2, p2, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p2, LX/HK6;->c:LX/2km;

    const/4 v7, 0x0

    .line 2453687
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a0097

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b163c

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0836a7

    invoke-virtual {v5, v6}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    const v8, 0x7f0b163c

    invoke-interface {v5, v6, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x3

    const v8, 0x7f0b163c

    invoke-interface {v5, v6, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    .line 2453688
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2453689
    iget-object v4, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2453690
    invoke-interface {v4}, LX/9uc;->cn()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v7

    :goto_0
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2453691
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2453692
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2453693
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 2453694
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 2453695
    iget-object v4, v0, LX/HK8;->b:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0b163c

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int v5, v4, v5

    .line 2453696
    iget-object v4, v0, LX/HK8;->c:LX/HIQ;

    invoke-virtual {v4, p1}, LX/HIQ;->c(LX/1De;)LX/HIO;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/HIO;->a(LX/2km;)LX/HIO;

    move-result-object v8

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-virtual {v8, v4}, LX/HIO;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIO;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/HIO;->h(I)LX/HIO;

    move-result-object v4

    invoke-interface {v10, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    .line 2453697
    :goto_1
    move-object v0, v4

    .line 2453698
    return-object v0

    :cond_1
    move-object v4, v3

    .line 2453699
    check-cast v4, LX/1Pr;

    invoke-interface {v4, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HLP;

    .line 2453700
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b232a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    .line 2453701
    iput v7, v5, LX/3mP;->b:I

    .line 2453702
    move-object v5, v5

    .line 2453703
    iget-object v4, v4, LX/HLP;->b:LX/25L;

    .line 2453704
    iput-object v4, v5, LX/3mP;->d:LX/25L;

    .line 2453705
    move-object v4, v5

    .line 2453706
    iget-object v5, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v5

    .line 2453707
    invoke-static {v5}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v5

    .line 2453708
    iput-object v5, v4, LX/3mP;->e:LX/0jW;

    .line 2453709
    move-object v4, v4

    .line 2453710
    invoke-virtual {v4}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2453711
    iget-object v4, v0, LX/HK8;->d:LX/HLN;

    move-object v5, p1

    move-object v7, v3

    move-object v9, v2

    invoke-virtual/range {v4 .. v9}, LX/HLN;->a(Landroid/content/Context;LX/0Px;Ljava/lang/Object;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/HLM;

    move-result-object v4

    .line 2453712
    iget-object v5, v0, LX/HK8;->a:LX/3mL;

    invoke-virtual {v5, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v4

    invoke-interface {v10, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2453683
    invoke-static {}, LX/1dS;->b()V

    .line 2453684
    const/4 v0, 0x0

    return-object v0
.end method
