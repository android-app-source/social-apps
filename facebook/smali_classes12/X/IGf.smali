.class public LX/IGf;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/IGe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2556382
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2556383
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2556384
    iput-object v0, p0, LX/IGf;->b:LX/0Px;

    .line 2556385
    iput-object p1, p0, LX/IGf;->a:Landroid/content/Context;

    .line 2556386
    return-void
.end method


# virtual methods
.method public final a(I)LX/IGe;
    .locals 1

    .prologue
    .line 2556381
    iget-object v0, p0, LX/IGf;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IGe;

    return-object v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2556378
    new-instance v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    iget-object v1, p0, LX/IGf;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;-><init>(Landroid/content/Context;)V

    .line 2556379
    const v1, 0x7f020e5c

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setBackgroundResource(I)V

    .line 2556380
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2556374
    check-cast p3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;

    .line 2556375
    check-cast p2, LX/IGe;

    .line 2556376
    invoke-virtual {p3, p2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingOptionView;->setPingOption(LX/IGe;)V

    .line 2556377
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2556373
    iget-object v0, p0, LX/IGf;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2556371
    invoke-virtual {p0, p1}, LX/IGf;->a(I)LX/IGe;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2556372
    int-to-long v0, p1

    return-wide v0
.end method
