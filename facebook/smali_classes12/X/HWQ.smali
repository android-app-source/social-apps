.class public final LX/HWQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;",
        "Lcom/facebook/graphql/model/GraphQLAttributionEntry;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V
    .locals 0

    .prologue
    .line 2476028
    iput-object p1, p0, LX/HWQ;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2476029
    check-cast p1, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;

    .line 2476030
    if-nez p1, :cond_0

    .line 2476031
    const/4 v0, 0x0

    .line 2476032
    :goto_0
    move-object v0, v0

    .line 2476033
    return-object v0

    .line 2476034
    :cond_0
    new-instance v0, LX/4Vs;

    invoke-direct {v0}, LX/4Vs;-><init>()V

    .line 2476035
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->a()LX/175;

    move-result-object v1

    .line 2476036
    if-nez v1, :cond_1

    .line 2476037
    const/4 v2, 0x0

    .line 2476038
    :goto_1
    move-object v1, v2

    .line 2476039
    iput-object v1, v0, LX/4Vs;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2476040
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2476041
    iput-object v1, v0, LX/4Vs;->d:Ljava/lang/String;

    .line 2476042
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageAttributionModel;->c()Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v1

    .line 2476043
    iput-object v1, v0, LX/4Vs;->e:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    .line 2476044
    new-instance v1, Lcom/facebook/graphql/model/GraphQLAttributionEntry;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;-><init>(LX/4Vs;)V

    .line 2476045
    move-object v0, v1

    .line 2476046
    goto :goto_0

    .line 2476047
    :cond_1
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    .line 2476048
    invoke-interface {v1}, LX/175;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2476049
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2476050
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-interface {v1}, LX/175;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 2476051
    invoke-interface {v1}, LX/175;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1W5;

    .line 2476052
    if-nez v2, :cond_4

    .line 2476053
    const/4 v6, 0x0

    .line 2476054
    :goto_3
    move-object v2, v6

    .line 2476055
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2476056
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2476057
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2476058
    iput-object v2, v4, LX/173;->e:LX/0Px;

    .line 2476059
    :cond_3
    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v2

    .line 2476060
    iput-object v2, v4, LX/173;->f:Ljava/lang/String;

    .line 2476061
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto :goto_1

    .line 2476062
    :cond_4
    new-instance v6, LX/4W6;

    invoke-direct {v6}, LX/4W6;-><init>()V

    .line 2476063
    invoke-interface {v2}, LX/1W5;->a()LX/171;

    move-result-object v7

    .line 2476064
    if-nez v7, :cond_5

    .line 2476065
    const/4 v8, 0x0

    .line 2476066
    :goto_4
    move-object v7, v8

    .line 2476067
    iput-object v7, v6, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2476068
    invoke-interface {v2}, LX/1W5;->b()I

    move-result v7

    .line 2476069
    iput v7, v6, LX/4W6;->c:I

    .line 2476070
    invoke-interface {v2}, LX/1W5;->c()I

    move-result v7

    .line 2476071
    iput v7, v6, LX/4W6;->d:I

    .line 2476072
    invoke-virtual {v6}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v6

    goto :goto_3

    .line 2476073
    :cond_5
    new-instance v8, LX/170;

    invoke-direct {v8}, LX/170;-><init>()V

    .line 2476074
    invoke-interface {v7}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    .line 2476075
    iput-object p0, v8, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2476076
    invoke-interface {v7}, LX/171;->c()LX/0Px;

    move-result-object p0

    .line 2476077
    iput-object p0, v8, LX/170;->b:LX/0Px;

    .line 2476078
    invoke-interface {v7}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object p0

    .line 2476079
    iput-object p0, v8, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 2476080
    invoke-interface {v7}, LX/171;->e()Ljava/lang/String;

    move-result-object p0

    .line 2476081
    iput-object p0, v8, LX/170;->o:Ljava/lang/String;

    .line 2476082
    invoke-interface {v7}, LX/171;->v_()Ljava/lang/String;

    move-result-object p0

    .line 2476083
    iput-object p0, v8, LX/170;->A:Ljava/lang/String;

    .line 2476084
    invoke-interface {v7}, LX/171;->w_()Ljava/lang/String;

    move-result-object p0

    .line 2476085
    iput-object p0, v8, LX/170;->X:Ljava/lang/String;

    .line 2476086
    invoke-interface {v7}, LX/171;->j()Ljava/lang/String;

    move-result-object p0

    .line 2476087
    iput-object p0, v8, LX/170;->Y:Ljava/lang/String;

    .line 2476088
    invoke-virtual {v8}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v8

    goto :goto_4
.end method
