.class public final LX/Hn9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/net/Socket;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501394
    iput-object p1, p0, LX/Hn9;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2501396
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501397
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2501398
    new-instance v0, LX/HnI;

    invoke-direct {v0}, LX/HnI;-><init>()V

    throw v0

    .line 2501399
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2501400
    :cond_1
    iget-object v0, p0, LX/Hn9;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->b()V

    .line 2501401
    iget-object v0, p0, LX/Hn9;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->q:LX/Hmo;

    const v1, 0x493e0

    .line 2501402
    iget-object p0, v0, LX/Hmo;->a:LX/0TD;

    new-instance p1, LX/Hml;

    invoke-direct {p1, v0, v1}, LX/Hml;-><init>(LX/Hmo;I)V

    invoke-interface {p0, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 2501403
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2501395
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/Hn9;->a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
