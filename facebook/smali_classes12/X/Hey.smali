.class public LX/Hey;
.super LX/HeO;
.source ""


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private b:LX/0wW;

.field public c:LX/0wd;

.field public d:LX/0wd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2491022
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Hey;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowManager;LX/Hex;LX/0wW;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const-wide v4, 0x3fd3333333333333L    # 0.3

    .line 2491023
    const/4 v7, -0x2

    .line 2491024
    new-instance v6, Landroid/view/WindowManager$LayoutParams;

    const/16 v9, 0x7d7

    const v10, 0x1000308

    const/4 v11, -0x3

    move v8, v7

    invoke-direct/range {v6 .. v11}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 2491025
    const/16 v7, 0x33

    iput v7, v6, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2491026
    move-object v0, v6

    .line 2491027
    invoke-direct {p0, p2, v0, p1}, LX/HeO;-><init>(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;Landroid/view/WindowManager;)V

    .line 2491028
    iput-object p3, p0, LX/Hey;->b:LX/0wW;

    .line 2491029
    new-instance v0, LX/31s;

    invoke-direct {v0, p0}, LX/31s;-><init>(LX/Hey;)V

    .line 2491030
    iget-object v1, p0, LX/Hey;->b:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/Hey;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 2491031
    iput-wide v4, v1, LX/0wd;->k:D

    .line 2491032
    move-object v1, v1

    .line 2491033
    iput-wide v4, v1, LX/0wd;->l:D

    .line 2491034
    move-object v1, v1

    .line 2491035
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 2491036
    move-object v1, v1

    .line 2491037
    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/Hey;->c:LX/0wd;

    .line 2491038
    iget-object v1, p0, LX/Hey;->b:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/Hey;->a:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    .line 2491039
    iput-wide v4, v1, LX/0wd;->k:D

    .line 2491040
    move-object v1, v1

    .line 2491041
    iput-wide v4, v1, LX/0wd;->l:D

    .line 2491042
    move-object v1, v1

    .line 2491043
    iput-boolean v3, v1, LX/0wd;->c:Z

    .line 2491044
    move-object v1, v1

    .line 2491045
    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Hey;->d:LX/0wd;

    .line 2491046
    return-void
.end method
