.class public LX/Ikl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/ViewSwitcher;

.field public final b:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public final c:Lcom/facebook/widget/text/BetterTextView;

.field public final d:Lcom/facebook/widget/text/BetterTextView;

.field public final e:Lcom/facebook/widget/text/BetterTextView;

.field public final f:Lcom/facebook/widget/text/BetterTextView;

.field public final g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public final h:Lcom/facebook/widget/text/BetterTextView;

.field public final i:Landroid/view/ViewGroup;

.field public final j:Lcom/facebook/widget/text/BetterButton;

.field public final k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final l:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2607043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607044
    const v0, 0x7f0d0a0d

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, LX/Ikl;->a:Landroid/widget/ViewSwitcher;

    .line 2607045
    const v0, 0x7f0d0a0e

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, LX/Ikl;->b:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2607046
    const v0, 0x7f0d0a0f

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ikl;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2607047
    const v0, 0x7f0d0a10

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ikl;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2607048
    const v0, 0x7f0d0a11

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ikl;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2607049
    const v0, 0x7f0d04a5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ikl;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2607050
    const v0, 0x7f0d0a12

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/Ikl;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2607051
    const v0, 0x7f0d0a17

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ikl;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2607052
    const v0, 0x7f0d0a14

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/Ikl;->i:Landroid/view/ViewGroup;

    .line 2607053
    const v0, 0x7f0d0a13

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Ikl;->j:Lcom/facebook/widget/text/BetterButton;

    .line 2607054
    const v0, 0x7f0d0a15

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Ikl;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2607055
    const v0, 0x7f0d0a16

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Ikl;->l:Landroid/widget/ImageView;

    .line 2607056
    iget-object v0, p0, LX/Ikl;->b:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->a()V

    .line 2607057
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2607058
    iget-object v0, p0, LX/Ikl;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
