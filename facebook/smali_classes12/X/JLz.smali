.class public LX/JLz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/common/perftest/PerfTestConfig;

.field private static b:Lcom/facebook/common/perftest/DrawFrameLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2682809
    new-instance v0, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {v0}, Lcom/facebook/common/perftest/PerfTestConfig;-><init>()V

    sput-object v0, LX/JLz;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2682804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/facebook/common/perftest/DrawFrameLogger;
    .locals 3

    .prologue
    .line 2682805
    invoke-static {}, LX/5pe;->b()V

    .line 2682806
    sget-object v0, LX/JLz;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    if-nez v0, :cond_0

    .line 2682807
    new-instance v0, Lcom/facebook/common/perftest/DrawFrameLogger;

    sget-object v1, LX/JLz;->a:Lcom/facebook/common/perftest/PerfTestConfig;

    new-instance v2, LX/4mX;

    invoke-direct {v2}, LX/4mX;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/facebook/common/perftest/DrawFrameLogger;-><init>(Lcom/facebook/common/perftest/PerfTestConfig;LX/0wY;)V

    sput-object v0, LX/JLz;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2682808
    :cond_0
    sget-object v0, LX/JLz;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    return-object v0
.end method
