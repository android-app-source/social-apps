.class public final synthetic LX/Inn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2610841
    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Inn;->b:[I

    :try_start_0
    sget-object v0, LX/Inn;->b:[I

    sget-object v1, LX/Inp;->TAB_ORION_PAY:LX/Inp;

    invoke-virtual {v1}, LX/Inp;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, LX/Inn;->b:[I

    sget-object v1, LX/Inp;->TAB_ORION_REQUEST:LX/Inp;

    invoke-virtual {v1}, LX/Inp;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    .line 2610842
    :goto_1
    invoke-static {}, LX/5g0;->values()[LX/5g0;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Inn;->a:[I

    :try_start_2
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->GROUP_COMMERCE_SEND:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->REQUEST_ACK:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_3
    :try_start_4
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->GROUP_COMMERCE_REQUEST:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->META_RANGE_REQUEST_FLOW:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->REQUEST:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->SEND:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_7
    :try_start_8
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->THREAD_DETAILS_SEND_FLOW:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->TRIGGER_SEND_FLOW:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, LX/Inn;->a:[I

    sget-object v1, LX/5g0;->META_RANGE_SEND_FLOW:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    goto :goto_a

    :catch_1
    goto :goto_9

    :catch_2
    goto :goto_8

    :catch_3
    goto :goto_7

    :catch_4
    goto :goto_6

    :catch_5
    goto :goto_5

    :catch_6
    goto :goto_4

    :catch_7
    goto :goto_3

    :catch_8
    goto :goto_2

    :catch_9
    goto :goto_1

    :catch_a
    goto/16 :goto_0
.end method
