.class public final LX/IfA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Ie7;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

.field public final synthetic b:LX/IfC;


# direct methods
.method public constructor <init>(LX/IfC;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 0

    .prologue
    .line 2599425
    iput-object p1, p0, LX/IfA;->b:LX/IfC;

    iput-object p2, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/Ie7;)V
    .locals 4
    .param p1    # LX/Ie7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2599409
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2599410
    sget-object v0, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    if-eq p1, v0, :cond_0

    .line 2599411
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v0, v0, LX/IfF;->d:LX/IfT;

    const-string v1, "cymk_notice_shown"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599412
    :cond_0
    sget-object v0, LX/IfD;->a:[I

    invoke-virtual {p1}, LX/Ie7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2599413
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v1, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599414
    invoke-static {v0, v1, v2}, LX/IfF;->b$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599415
    :goto_0
    return-void

    .line 2599416
    :pswitch_0
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v1, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599417
    invoke-static {v0, v1, v3}, LX/IfF;->a$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599418
    goto :goto_0

    .line 2599419
    :pswitch_1
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v1, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599420
    invoke-static {v0, v1, v2}, LX/IfF;->a$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599421
    goto :goto_0

    .line 2599422
    :pswitch_2
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v1, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599423
    invoke-static {v0, v1, v3}, LX/IfF;->b$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599424
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2599406
    iget-object v0, p0, LX/IfA;->b:LX/IfC;

    iget-object v0, v0, LX/IfC;->b:LX/IfF;

    iget-object v1, p0, LX/IfA;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    const/4 v2, 0x0

    .line 2599407
    invoke-static {v0, v1, v2}, LX/IfF;->b$redex0(LX/IfF;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599408
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2599405
    check-cast p1, LX/Ie7;

    invoke-direct {p0, p1}, LX/IfA;->a(LX/Ie7;)V

    return-void
.end method
