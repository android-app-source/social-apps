.class public LX/IcM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0tX;

.field public final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595486
    iput-object p1, p0, LX/IcM;->a:LX/03V;

    .line 2595487
    iput-object p2, p0, LX/IcM;->b:LX/0tX;

    .line 2595488
    iput-object p3, p0, LX/IcM;->c:LX/1Ck;

    .line 2595489
    return-void
.end method

.method public static a(LX/0QB;)LX/IcM;
    .locals 4

    .prologue
    .line 2595490
    new-instance v3, LX/IcM;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v0, v1, v2}, LX/IcM;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 2595491
    move-object v0, v3

    .line 2595492
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;DDLX/IdA;)V
    .locals 4

    .prologue
    .line 2595493
    if-nez p6, :cond_0

    .line 2595494
    :goto_0
    return-void

    .line 2595495
    :cond_0
    iget-object v0, p6, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b()V

    .line 2595496
    new-instance v0, LX/IbL;

    invoke-direct {v0}, LX/IbL;-><init>()V

    move-object v0, v0

    .line 2595497
    const-string v1, "provider"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "latitude"

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "longitude"

    invoke-static {p4, p5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/IbL;

    .line 2595498
    iget-object v1, p0, LX/IcM;->c:LX/1Ck;

    const-string v2, "task_key_fetch_ride_type"

    iget-object v3, p0, LX/IcM;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2595499
    new-instance v3, LX/IcL;

    invoke-direct {v3, p0, p6}, LX/IcL;-><init>(LX/IcM;LX/IdA;)V

    move-object v3, v3

    .line 2595500
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
