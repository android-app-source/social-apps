.class public final LX/HrU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASe;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2511067
    iput-object p1, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2511068
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aE(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511069
    return-void
.end method

.method public final a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2511070
    iget-object v3, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {p1, p2, v0}, LX/7kq;->a(ILcom/facebook/composer/attachments/ComposerAttachment;LX/0Px;)LX/0Px;

    move-result-object v4

    if-nez p3, :cond_0

    if-eqz p4, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v3, v4, v0, p4}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2511071
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->hasFaceboxes()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v3, LX/1EB;->T:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2511072
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0, v2}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2511073
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2511074
    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2511075
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 2511076
    iget-object v1, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v2, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v1, v2, :cond_0

    .line 2511077
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v5}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511078
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511079
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2511080
    invoke-static {v0, v1, v3, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2511081
    :goto_0
    return-void

    .line 2511082
    :cond_0
    iget-object v1, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v2, LX/5RF;->STORYLINE:LX/5RF;

    if-ne v1, v2, :cond_1

    .line 2511083
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v5}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511084
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511085
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2511086
    invoke-static {v0, v1, v3, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    goto :goto_0

    .line 2511087
    :cond_1
    if-eqz v0, :cond_2

    .line 2511088
    sget-object v1, LX/HrZ;->c:[I

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    invoke-virtual {v0}, LX/4gF;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2511089
    :cond_2
    :goto_1
    iget-object v1, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/7kq;->a(LX/0Px;Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;

    move-result-object v0

    invoke-static {v1, v0, v3, v4}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    goto :goto_0

    .line 2511090
    :pswitch_0
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v4}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 2511091
    :pswitch_1
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;J)V
    .locals 10

    .prologue
    .line 2511092
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511093
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2511094
    :cond_0
    :goto_0
    return-void

    .line 2511095
    :cond_1
    const/4 v3, 0x0

    .line 2511096
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 2511097
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 2511098
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getTimestamp()J

    move-result-wide v8

    cmp-long v7, v8, p2

    if-nez v7, :cond_2

    .line 2511099
    :goto_2
    if-eqz v2, :cond_0

    .line 2511100
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2511101
    new-instance v4, LX/74k;

    invoke-direct {v4}, LX/74k;-><init>()V

    new-instance v5, LX/4gN;

    invoke-direct {v5}, LX/4gN;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v2

    invoke-virtual {v2}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 2511102
    iput-object v2, v4, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2511103
    move-object v2, v4

    .line 2511104
    invoke-virtual {v2}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2511105
    invoke-static {v0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 2511106
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_2
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/photos/base/tagging/FaceBox;)V
    .locals 4
    .param p2    # Lcom/facebook/photos/base/tagging/FaceBox;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2511107
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511108
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 2511109
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->S(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2511110
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->R$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511111
    :cond_0
    :goto_0
    return-void

    .line 2511112
    :cond_1
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v1, v3, :cond_2

    .line 2511113
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-static {}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->newBuilder()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v3

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setMediaItems(LX/0Px;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    sget-object v3, LX/B66;->SLIDESHOW_ATTACHMENT_EDIT:LX/B66;

    invoke-virtual {v1, v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSource(LX/B66;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v3

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v3

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->a(LX/5Ra;)Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->a()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    move-result-object v1

    invoke-static {v2, v1}, LX/B67;->a(Landroid/content/Context;Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;)Landroid/content/Intent;

    move-result-object v2

    .line 2511114
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x82

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2511115
    :cond_2
    sget-object v1, LX/HrZ;->c:[I

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v3

    invoke-virtual {v3}, LX/4gF;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v1, v2

    .line 2511116
    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2511117
    iget-boolean v3, v1, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v1, v3

    .line 2511118
    if-nez v1, :cond_0

    .line 2511119
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2511120
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5Qz;->C()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5R0;->D()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2511121
    :cond_3
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511122
    iget-object v3, v1, LX/AQ9;->S:LX/AQ4;

    move-object v3, v3

    .line 2511123
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->bE:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HwW;

    invoke-virtual {v1}, LX/HwW;->newBuilder()LX/HwV;

    move-result-object p0

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    .line 2511124
    iput-object v1, p0, LX/HwV;->b:LX/0Px;

    .line 2511125
    move-object p0, p0

    .line 2511126
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2511127
    iput-object v1, p0, LX/HwV;->c:Ljava/lang/String;

    .line 2511128
    move-object p0, p0

    .line 2511129
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 2511130
    iput-object v1, p0, LX/HwV;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2511131
    move-object v1, p0

    .line 2511132
    const/16 p0, 0x7e

    .line 2511133
    iput p0, v1, LX/HwV;->e:I

    .line 2511134
    move-object v1, v1

    .line 2511135
    iput-object v0, v1, LX/HwV;->f:Landroid/support/v4/app/Fragment;

    .line 2511136
    move-object v1, v1

    .line 2511137
    check-cast v2, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2511138
    iput-object v2, v1, LX/HwV;->g:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2511139
    move-object v1, v1

    .line 2511140
    iput-object p2, v1, LX/HwV;->h:Lcom/facebook/photos/base/tagging/FaceBox;

    .line 2511141
    move-object v2, v1

    .line 2511142
    if-nez v3, :cond_4

    const/4 v1, 0x0

    .line 2511143
    :goto_1
    iput-object v1, v2, LX/HwV;->i:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2511144
    move-object v2, v2

    .line 2511145
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->B()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-static {v1}, LX/94d;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    .line 2511146
    :goto_2
    iput-boolean v1, v2, LX/HwV;->j:Z

    .line 2511147
    move-object v1, v2

    .line 2511148
    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v2}, LX/5R0;->D()Z

    move-result v2

    .line 2511149
    iput-boolean v2, v1, LX/HwV;->k:Z

    .line 2511150
    move-object v1, v1

    .line 2511151
    invoke-virtual {v1}, LX/HwV;->a()V

    goto/16 :goto_0

    :cond_4
    invoke-interface {v3}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 2511152
    :cond_6
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aE(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto/16 :goto_0

    .line 2511153
    :pswitch_1
    sget-object v1, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2511154
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object p0, LX/0ge;->COMPOSER_VIDEO_THUMBNAIL_CLICKED:LX/0ge;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p0, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511155
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const-class p0, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;

    invoke-direct {v3, v1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "extra_session_id"

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extra_source"

    sget-object p0, LX/BIf;->COMPOSER:LX/BIf;

    invoke-virtual {p0}, LX/BIf;->ordinal()I

    move-result p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "extra_video_uri"

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    .line 2511156
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v1, v3, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2511157
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 7

    .prologue
    .line 2511158
    iget-object v0, p0, LX/HrU;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511159
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2511160
    :cond_0
    :goto_0
    return-void

    .line 2511161
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2511162
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2511163
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 2511164
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 2511165
    if-eqz v1, :cond_2

    .line 2511166
    new-instance v6, LX/74k;

    invoke-direct {v6}, LX/74k;-><init>()V

    new-instance p0, LX/4gN;

    invoke-direct {p0}, LX/4gN;-><init>()V

    invoke-virtual {p0, v1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v1

    invoke-virtual {v1}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 2511167
    iput-object v1, v6, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2511168
    move-object v1, v6

    .line 2511169
    invoke-virtual {v1}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2511170
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2511171
    :cond_3
    invoke-static {v0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/util/ArrayList;)V

    .line 2511172
    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_VIDEO_TAG_CLICK:LX/0ge;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto :goto_0
.end method
