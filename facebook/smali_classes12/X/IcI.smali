.class public final LX/IcI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IcT;

.field public final synthetic b:LX/IcJ;


# direct methods
.method public constructor <init>(LX/IcJ;LX/IcT;)V
    .locals 0

    .prologue
    .line 2595392
    iput-object p1, p0, LX/IcI;->b:LX/IcJ;

    iput-object p2, p0, LX/IcI;->a:LX/IcT;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2595393
    iget-object v0, p0, LX/IcI;->b:LX/IcJ;

    iget-object v0, v0, LX/IcJ;->a:LX/03V;

    const-string v1, "RideRequestInfoLoader"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595394
    iget-object v0, p0, LX/IcI;->a:LX/IcT;

    .line 2595395
    iget-object v1, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, v1, LX/IcX;->n:LX/03V;

    const-string v2, "RideCurrentLocationController"

    const-string p0, "GraphQL ride request info query failed."

    invoke-virtual {v1, v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595396
    iget-object v1, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, v1, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    iget-object v2, v0, LX/IcT;->a:LX/IcX;

    iget-object v2, v2, LX/IcX;->g:Landroid/content/Context;

    const p0, 0x7f082d95

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setDriverInfoText(Ljava/lang/String;)V

    .line 2595397
    iget-object v1, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, v1, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setEtaText(Ljava/lang/String;)V

    .line 2595398
    iget-object v1, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, v1, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->b()V

    .line 2595399
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2595400
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    .line 2595401
    if-nez p1, :cond_0

    .line 2595402
    iget-object v0, p0, LX/IcI;->b:LX/IcJ;

    iget-object v0, v0, LX/IcJ;->a:LX/03V;

    const-string v1, "RideService"

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595403
    :goto_0
    return-void

    .line 2595404
    :cond_0
    iget-object v0, p0, LX/IcI;->a:LX/IcT;

    .line 2595405
    iget-object v3, v0, LX/IcT;->a:LX/IcX;

    iget-object v3, v3, LX/IcX;->h:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v4, LX/IcS;

    invoke-direct {v4, v0, p1}, LX/IcS;-><init>(LX/IcT;Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;)V

    invoke-virtual {v3, v4}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595406
    iget-object v3, v0, LX/IcT;->a:LX/IcX;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/IcX;->b(LX/IcX;Ljava/lang/String;)V

    .line 2595407
    iget-object v3, v0, LX/IcT;->a:LX/IcX;

    .line 2595408
    const/4 v13, 0x0

    .line 2595409
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2595410
    :cond_1
    iget-object v7, v3, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    iget-object v8, v3, LX/IcX;->g:Landroid/content/Context;

    const v9, 0x7f082d95

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setDriverInfoText(Ljava/lang/String;)V

    .line 2595411
    :goto_1
    iget-object v5, v3, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    .line 2595412
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/IcX;->c(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2595413
    :cond_2
    const-string v6, "-"

    .line 2595414
    :goto_2
    move-object v6, v6

    .line 2595415
    invoke-virtual {v5, v6}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setEtaText(Ljava/lang/String;)V

    .line 2595416
    iget-object v5, v3, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    .line 2595417
    if-nez p1, :cond_5

    .line 2595418
    const-string v6, "-"

    .line 2595419
    :goto_3
    move-object v6, v6

    .line 2595420
    invoke-virtual {v5, v6}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setVehicleInfoText(Ljava/lang/String;)V

    .line 2595421
    iget-object v3, v0, LX/IcT;->a:LX/IcX;

    iget-object v3, v3, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->b()V

    .line 2595422
    goto :goto_0

    .line 2595423
    :cond_3
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2595424
    const-string v8, "%s \u2022 %s "

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->l()D

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2595425
    iget-object v8, v3, LX/IcX;->g:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f021009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 2595426
    iget-object v9, v3, LX/IcX;->g:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b1eeb

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 2595427
    invoke-virtual {v8, v13, v13, v9, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2595428
    new-instance v9, LX/34T;

    const/4 v10, 0x2

    invoke-direct {v9, v8, v10}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    const/16 v11, 0x21

    invoke-virtual {v7, v9, v8, v10, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2595429
    iget-object v8, v3, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    invoke-virtual {v8, v7}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->setDriverInfoText(Landroid/text/SpannableStringBuilder;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->m()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2595430
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2595431
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 2595432
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2595433
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 2595434
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2595435
    :cond_7
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->r()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 2595436
    const-string v7, "\u2022"

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2595437
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->r()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2595438
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->r()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2595439
    :cond_9
    const-string v7, " "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-static {v7, v8}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3
.end method
