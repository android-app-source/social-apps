.class public LX/HaD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/registration/model/RegistrationFormData;",
        "Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lB;

.field private final b:Ljava/lang/String;

.field private final c:LX/0dC;

.field private final d:LX/0hw;


# direct methods
.method public constructor <init>(LX/0lB;Ljava/lang/String;LX/0dC;LX/0hw;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/registration/annotations/RegInstance;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483514
    iput-object p1, p0, LX/HaD;->a:LX/0lB;

    .line 2483515
    iput-object p2, p0, LX/HaD;->b:Ljava/lang/String;

    .line 2483516
    iput-object p3, p0, LX/HaD;->c:LX/0dC;

    .line 2483517
    iput-object p4, p0, LX/HaD;->d:LX/0hw;

    .line 2483518
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2483519
    check-cast p1, Lcom/facebook/registration/model/RegistrationFormData;

    .line 2483520
    invoke-virtual {p1}, Lcom/facebook/registration/model/RegistrationFormData;->k()Ljava/util/List;

    move-result-object v1

    .line 2483521
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "return_multiple_errors"

    const-string v3, "true"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483522
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "attempt_login"

    const-string v3, "true"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483523
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "reg_instance"

    iget-object v3, p0, LX/HaD;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483524
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/HaD;->c:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483525
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483526
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "skip_session_info"

    const-string v3, "true"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483527
    const/4 v0, 0x0

    .line 2483528
    :try_start_0
    iget-object v2, p0, LX/HaD;->d:LX/0hw;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0hw;->a(Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2483529
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "advertising_id"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483530
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    sget-object v2, LX/11I;->REGISTER_ACCOUNT:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 2483531
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2483532
    move-object v0, v0

    .line 2483533
    const-string v2, "POST"

    .line 2483534
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2483535
    move-object v0, v0

    .line 2483536
    const-string v2, "method/user.register"

    .line 2483537
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 2483538
    move-object v0, v0

    .line 2483539
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    .line 2483540
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2483541
    move-object v0, v0

    .line 2483542
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2483543
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2483544
    move-object v0, v0

    .line 2483545
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2483546
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2483547
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2483548
    iget-object v1, p0, LX/HaD;->a:LX/0lB;

    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v0

    iget-object v2, p0, LX/HaD;->a:LX/0lB;

    .line 2483549
    iget-object v3, v2, LX/0lC;->_typeFactory:LX/0li;

    move-object v2, v3

    .line 2483550
    const-class v3, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    invoke-virtual {v2, v3}, LX/0li;->a(Ljava/lang/reflect/Type;)LX/0lJ;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0lC;->a(LX/15w;LX/0lJ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2483551
    return-object v0
.end method
