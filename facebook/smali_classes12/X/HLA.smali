.class public final LX/HLA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HIS;


# instance fields
.field public final synthetic a:LX/HLD;


# direct methods
.method public constructor <init>(LX/HLD;)V
    .locals 0

    .prologue
    .line 2455607
    iput-object p1, p0, LX/HLA;->a:LX/HLD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/HIR;)LX/CY7;
    .locals 5

    .prologue
    .line 2455608
    new-instance v0, LX/CY6;

    invoke-direct {v0}, LX/CY6;-><init>()V

    iget-object v1, p1, LX/HIR;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2455609
    iput-wide v2, v0, LX/CY6;->a:J

    .line 2455610
    move-object v0, v0

    .line 2455611
    iget-object v1, p1, LX/HIR;->f:Ljava/lang/String;

    .line 2455612
    iput-object v1, v0, LX/CY6;->b:Ljava/lang/String;

    .line 2455613
    move-object v0, v0

    .line 2455614
    iget-object v1, p1, LX/HIR;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2455615
    iput-object v1, v0, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2455616
    move-object v0, v0

    .line 2455617
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->CITY_HUB_SOCIAL_MODULE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2455618
    iput-object v1, v0, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2455619
    move-object v0, v0

    .line 2455620
    invoke-virtual {v0}, LX/CY6;->a()LX/CY7;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/HIR;LX/CY7;)V
    .locals 7

    .prologue
    .line 2455621
    iget-object v0, p0, LX/HLA;->a:LX/HLD;

    iget-object v0, v0, LX/HLD;->d:LX/9XE;

    iget-object v1, p0, LX/HLA;->a:LX/HLD;

    iget-object v1, v1, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455622
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v2

    .line 2455623
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p1, LX/HIR;->a:Ljava/lang/String;

    iget-object v4, p2, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    .line 2455624
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_CTA:LX/9XI;

    invoke-static {v6, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "tapped_page_id"

    invoke-virtual {v6, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string p1, "ccta_type"

    invoke-virtual {v6, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2455625
    iget-object v0, p0, LX/HLA;->a:LX/HLD;

    iget-object v1, v0, LX/HLD;->d:LX/9XE;

    iget-wide v2, p2, LX/CY7;->a:J

    iget-object v0, p2, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p2, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v5

    iget-object v6, p2, LX/CY7;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    invoke-virtual/range {v1 .. v6}, LX/9XE;->a(JLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)V

    .line 2455626
    return-void
.end method
