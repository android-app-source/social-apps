.class public final LX/JTB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JT0;


# instance fields
.field public final synthetic a:LX/JTC;

.field private final b:Landroid/view/View$OnClickListener;

.field public final c:LX/JSe;

.field public final d:LX/JTY;


# direct methods
.method public constructor <init>(LX/JTC;LX/JSe;LX/JTY;)V
    .locals 1

    .prologue
    .line 2696481
    iput-object p1, p0, LX/JTB;->a:LX/JTC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696482
    new-instance v0, LX/JTA;

    invoke-direct {v0, p0}, LX/JTA;-><init>(LX/JTB;)V

    iput-object v0, p0, LX/JTB;->b:Landroid/view/View$OnClickListener;

    .line 2696483
    iput-object p2, p0, LX/JTB;->c:LX/JSe;

    .line 2696484
    iput-object p3, p0, LX/JTB;->d:LX/JTY;

    .line 2696485
    return-void
.end method

.method public static synthetic a(LX/JTB;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2696477
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string p0, "fbrpc"

    invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2696478
    :goto_0
    move-object v0, p1

    .line 2696479
    return-object v0

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2696476
    iget-object v0, p0, LX/JTB;->a:LX/JTC;

    iget-object v0, v0, LX/JTC;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083a8a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2696480
    iget-object v0, p0, LX/JTB;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2696475
    const/4 v0, 0x0

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2696474
    const v0, 0x7f020365

    return v0
.end method
