.class public LX/HJX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452901
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2452902
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJX;->b:LX/0Zi;

    .line 2452903
    iput-object p1, p0, LX/HJX;->a:LX/0Ot;

    .line 2452904
    return-void
.end method

.method public static a(LX/0QB;)LX/HJX;
    .locals 4

    .prologue
    .line 2452890
    const-class v1, LX/HJX;

    monitor-enter v1

    .line 2452891
    :try_start_0
    sget-object v0, LX/HJX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452892
    sput-object v2, LX/HJX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452893
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452894
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452895
    new-instance v3, LX/HJX;

    const/16 p0, 0x2baf

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJX;-><init>(LX/0Ot;)V

    .line 2452896
    move-object v0, v3

    .line 2452897
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452898
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452899
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452900
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2452874
    check-cast p1, LX/HJW;

    .line 2452875
    iget-object v0, p0, LX/HJX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;

    iget-object v1, p1, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HJW;->b:LX/2km;

    .line 2452876
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2452877
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2452878
    :goto_0
    return-void

    .line 2452879
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->c:LX/E1f;

    .line 2452880
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2452881
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2452882
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2452883
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2452884
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2452885
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2452886
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2452887
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2452888
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2452889
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2452873
    const v0, -0x5a323369

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2452851
    check-cast p2, LX/HJW;

    .line 2452852
    iget-object v0, p0, LX/HJX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;

    iget-object v1, p2, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v6, 0x0

    const/4 p2, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2452853
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2452854
    invoke-interface {v2}, LX/9uc;->cX()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2452855
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2452856
    invoke-interface {v2}, LX/9uc;->cX()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    .line 2452857
    :goto_0
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2452858
    invoke-interface {v5}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v5

    invoke-interface {v5}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v7

    .line 2452859
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2452860
    invoke-interface {v5}, LX/9uc;->dc()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    .line 2452861
    if-eqz v2, :cond_1

    .line 2452862
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2452863
    invoke-interface {v5}, LX/9uc;->cX()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    .line 2452864
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const v10, 0x7f0213ab

    invoke-interface {v9, v10}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    .line 2452865
    const v10, -0x5a323369

    const/4 p0, 0x0

    invoke-static {p1, v10, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v10

    move-object v10, v10

    .line 2452866
    invoke-interface {v9, v10}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x2

    invoke-interface {v9, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    const v10, 0x7f0b0d5e

    invoke-interface {v9, p2, v10}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x7

    const p0, 0x7f0b0d5f

    invoke-interface {v9, v10, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v10

    .line 2452867
    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->b:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    sget-object v1, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    const v1, 0x7f0a010a

    invoke-virtual {p0, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object p0

    sget-object v1, LX/1Up;->c:LX/1Up;

    invoke-virtual {p0, v1}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const v1, 0x7f0b0d61

    invoke-interface {p0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    const v1, 0x7f0b0d61

    invoke-interface {p0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object p0

    move-object v7, p0

    .line 2452868
    invoke-interface {v10, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v9, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0d5e

    invoke-interface {v3, p2, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    const v4, 0x1010038

    const v9, 0x7f0b0050

    invoke-static {p1, v8, v4, v9}, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->a(LX/1De;Ljava/lang/String;II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    if-nez v2, :cond_2

    :goto_2
    invoke-interface {v3, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v7, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2452869
    return-object v0

    :cond_0
    move v2, v4

    .line 2452870
    goto/16 :goto_0

    :cond_1
    move-object v5, v6

    .line 2452871
    goto/16 :goto_1

    .line 2452872
    :cond_2
    const v2, 0x1010212

    const v4, 0x7f0b004e

    invoke-static {p1, v5, v2, v4}, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->a(LX/1De;Ljava/lang/String;II)LX/1Di;

    move-result-object v6

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2452846
    invoke-static {}, LX/1dS;->b()V

    .line 2452847
    iget v0, p1, LX/1dQ;->b:I

    .line 2452848
    packed-switch v0, :pswitch_data_0

    .line 2452849
    :goto_0
    return-object v1

    .line 2452850
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HJX;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x5a323369
        :pswitch_0
    .end packed-switch
.end method
