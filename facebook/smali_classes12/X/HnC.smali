.class public final LX/HnC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501446
    iput-object p1, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/beam/protocol/BeamPreflightInfo;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Lcom/facebook/beam/protocol/BeamPreflightInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2501427
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501428
    iget-object v0, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    .line 2501429
    iput-object p1, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501430
    iget-object v0, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v0}, LX/Hm1;->a()Ljava/lang/String;

    .line 2501431
    iget-object v0, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501432
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "userid"

    .line 2501433
    iget-object v3, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v3, v3

    .line 2501434
    iget-object v3, v3, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "versionCode"

    .line 2501435
    iget-object v3, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v3, v3

    .line 2501436
    iget v3, v3, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v1

    const-string v2, "deviceBrand"

    .line 2501437
    iget-object v3, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v3, v3

    .line 2501438
    iget-object v3, v3, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    const-string v2, "deviceModel"

    .line 2501439
    iget-object v3, p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v3, v3

    .line 2501440
    iget-object v3, v3, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2501441
    sget-object v2, LX/HnJ;->DECIDE_SHOULD_SEND_APK:LX/HnJ;

    invoke-static {v0, v2, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501442
    iget-object v0, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    iget-object v1, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->r:LX/Hmi;

    iget-object v2, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    iget-object v3, p0, LX/HnC;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v3, v3, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2501443
    iget-object p0, v0, LX/Hmz;->a:LX/0TD;

    new-instance p1, LX/Hmt;

    invoke-direct {p1, v0, v1, v2, v3}, LX/Hmt;-><init>(LX/Hmz;LX/Hmi;Lcom/facebook/beam/protocol/BeamPreflightInfo;Lcom/facebook/beam/protocol/BeamPreflightInfo;)V

    invoke-interface {p0, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v0, p0

    .line 2501444
    return-object v0

    .line 2501445
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501426
    check-cast p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-direct {p0, p1}, LX/HnC;->a(Lcom/facebook/beam/protocol/BeamPreflightInfo;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
