.class public final LX/IQe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/IRb;

.field public final synthetic c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V
    .locals 0

    .prologue
    .line 2575498
    iput-object p1, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iput-object p2, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object p3, p0, LX/IQe;->b:LX/IRb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0xa5e9052

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2575499
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->s:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel$TipsModel;->k()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v0

    .line 2575500
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_GENERAL_ROOM:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2575501
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->h:LX/IK1;

    iget-object v2, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2575502
    new-instance v3, LX/IK0;

    invoke-direct {v3, v0}, LX/IK0;-><init>(LX/IK1;)V

    move-object v3, v3

    .line 2575503
    new-instance v4, LX/4Fj;

    invoke-direct {v4}, LX/4Fj;-><init>()V

    .line 2575504
    const-string v5, "group_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2575505
    new-instance v5, LX/IKg;

    invoke-direct {v5}, LX/IKg;-><init>()V

    move-object v5, v5

    .line 2575506
    const-string p1, "data"

    invoke-virtual {v5, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2575507
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2575508
    iget-object v5, v0, LX/IK1;->c:LX/88n;

    sget-object p1, LX/88p;->CREATE_GENERAL_CHANNEL:LX/88p;

    .line 2575509
    new-instance v2, LX/IJz;

    invoke-direct {v2, v0, v4}, LX/IJz;-><init>(LX/IK1;LX/399;)V

    move-object v4, v2

    .line 2575510
    invoke-virtual {v5, p1, v4, v3}, LX/88n;->a(LX/88p;Ljava/util/concurrent/Callable;LX/0Ve;)V

    .line 2575511
    :cond_0
    :goto_0
    iget-object v2, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v3, p0, LX/IQe;->b:LX/IRb;

    .line 2575512
    invoke-static {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v4

    .line 2575513
    invoke-static {v4}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v5

    .line 2575514
    const/4 v0, 0x0

    .line 2575515
    iput-object v0, v5, LX/9OQ;->y:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    .line 2575516
    invoke-virtual {v5}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v5

    .line 2575517
    invoke-interface {v3, v4, v5}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2575518
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    const-string v3, "CLICK"

    invoke-static {v0, v2, v3}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->a$redex0(Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;)V

    .line 2575519
    const v0, -0x7e1f0b86

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2575520
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POST:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2575521
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->i:LX/ISp;

    invoke-virtual {v0}, LX/ISp;->b()V

    goto :goto_0

    .line 2575522
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->CREATE_GROUP_POLL:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2575523
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575524
    invoke-static {v2}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v3

    sget-object v4, LX/03R;->NO:LX/03R;

    iget-object v5, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->c:LX/0W9;

    invoke-static {v3, v4, v5}, LX/DJw;->a(LX/DZC;LX/03R;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    move-object v3, v3

    .line 2575525
    invoke-static {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    .line 2575526
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2575527
    new-instance v4, LX/89K;

    invoke-direct {v4}, LX/89K;-><init>()V

    const-string v4, "GroupsPollComposerPluginConfig"

    invoke-static {v4}, LX/89L;->a(Ljava/lang/String;)LX/89L;

    move-result-object v4

    invoke-static {v4}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2575528
    iget-object v4, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->d:LX/1Kf;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    const/16 v7, 0x6dc

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class p1, Landroid/app/Activity;

    invoke-static {v3, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-interface {v4, v5, v6, v7, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2575529
    goto/16 :goto_0

    .line 2575530
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2575531
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->a(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2575532
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_MEMBERS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2575533
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->f:LX/DVF;

    iget-object v0, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v4}, LX/IQV;->a(LX/9N6;)Z

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v2, v3, v0, v4, v5}, LX/DVF;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 2575534
    iget-object v2, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2575535
    :cond_5
    iget-object v0, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    goto :goto_1

    .line 2575536
    :cond_6
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ADD_GROUP_ADMINS:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2575537
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2575538
    const-string v2, "group_feed_id"

    iget-object v3, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2575539
    iget-object v2, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->a:LX/17W;

    iget-object v3, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/0ax;->Q:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 2575540
    :cond_7
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->WRITE_GROUP_DESCRIPTION:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2575541
    iget-object v0, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->g:LX/F2N;

    invoke-virtual {v0}, LX/F2N;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2575542
    const-string v2, "group_id"

    iget-object v3, p0, LX/IQe;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575543
    iget-object v2, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/IQe;->c:Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/ui/GroupSuggestionCardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0
.end method
