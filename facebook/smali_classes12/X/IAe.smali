.class public LX/IAe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/CreateGroupParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IAe;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2545958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/IAe;
    .locals 3

    .prologue
    .line 2545959
    sget-object v0, LX/IAe;->a:LX/IAe;

    if-nez v0, :cond_1

    .line 2545960
    const-class v1, LX/IAe;

    monitor-enter v1

    .line 2545961
    :try_start_0
    sget-object v0, LX/IAe;->a:LX/IAe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2545962
    if-eqz v2, :cond_0

    .line 2545963
    :try_start_1
    new-instance v0, LX/IAe;

    invoke-direct {v0}, LX/IAe;-><init>()V

    .line 2545964
    move-object v0, v0

    .line 2545965
    sput-object v0, LX/IAe;->a:LX/IAe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2545966
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2545967
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2545968
    :cond_1
    sget-object v0, LX/IAe;->a:LX/IAe;

    return-object v0

    .line 2545969
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2545970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2545971
    check-cast p1, Lcom/facebook/messaging/service/model/CreateGroupParams;

    .line 2545972
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2545973
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, " "

    .line 2545974
    iget-object v3, p1, Lcom/facebook/messaging/service/model/CreateGroupParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2545975
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545976
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "recipients"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ","

    invoke-static {v4}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    .line 2545977
    iget-object v5, p1, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    move-object v5, v5

    .line 2545978
    invoke-virtual {v4, v5}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2545979
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "createGroupThread"

    .line 2545980
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2545981
    move-object v1, v1

    .line 2545982
    const-string v2, "POST"

    .line 2545983
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2545984
    move-object v1, v1

    .line 2545985
    const-string v2, "me/group_threads"

    .line 2545986
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2545987
    move-object v1, v1

    .line 2545988
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2545989
    move-object v0, v1

    .line 2545990
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2545991
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2545992
    move-object v0, v0

    .line 2545993
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2545994
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2545995
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
