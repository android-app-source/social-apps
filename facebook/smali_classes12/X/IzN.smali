.class public LX/IzN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/IzN;


# instance fields
.field private final a:LX/IzO;


# direct methods
.method public constructor <init>(LX/IzO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634558
    iput-object p1, p0, LX/IzN;->a:LX/IzO;

    .line 2634559
    return-void
.end method

.method public static a(LX/0QB;)LX/IzN;
    .locals 4

    .prologue
    .line 2634560
    sget-object v0, LX/IzN;->b:LX/IzN;

    if-nez v0, :cond_1

    .line 2634561
    const-class v1, LX/IzN;

    monitor-enter v1

    .line 2634562
    :try_start_0
    sget-object v0, LX/IzN;->b:LX/IzN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634563
    if-eqz v2, :cond_0

    .line 2634564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2634565
    new-instance p0, LX/IzN;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-direct {p0, v3}, LX/IzN;-><init>(LX/IzO;)V

    .line 2634566
    move-object v0, p0

    .line 2634567
    sput-object v0, LX/IzN;->b:LX/IzN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634570
    :cond_1
    sget-object v0, LX/IzN;->b:LX/IzN;

    return-object v0

    .line 2634571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2634573
    iget-object v0, p0, LX/IzN;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 2634574
    return-void
.end method
