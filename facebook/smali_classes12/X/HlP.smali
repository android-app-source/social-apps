.class public LX/HlP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2498610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 2498603
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2498604
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498605
    const-string v1, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498606
    const-string v1, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498607
    const-string v1, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.callback"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498608
    invoke-static {p0, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2498609
    return-void
.end method
