.class public final LX/I3D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:LX/I3F;


# direct methods
.method public constructor <init>(LX/I3F;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2531156
    iput-object p1, p0, LX/I3D;->d:LX/I3F;

    iput-object p2, p0, LX/I3D;->a:Ljava/lang/String;

    iput p3, p0, LX/I3D;->b:I

    iput p4, p0, LX/I3D;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2531157
    new-instance v0, LX/7oQ;

    invoke-direct {v0}, LX/7oQ;-><init>()V

    move-object v0, v0

    .line 2531158
    const-string v1, "end_cursor"

    iget-object v2, p0, LX/I3D;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "count"

    iget v2, p0, LX/I3D;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    iget v2, p0, LX/I3D;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/I3D;->d:LX/I3F;

    iget-object v2, v2, LX/I3F;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/I3D;->d:LX/I3F;

    iget-object v2, v2, LX/I3F;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7oQ;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2531159
    iget-object v1, p0, LX/I3D;->d:LX/I3F;

    iget-object v1, v1, LX/I3F;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
