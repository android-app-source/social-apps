.class public LX/Hf5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/Hem;

.field public B:LX/Hf2;

.field public C:LX/04D;

.field public D:LX/0wW;

.field public E:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field

.field public G:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public H:Landroid/util/DisplayMetrics;

.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/WindowManager;

.field public final c:LX/Hf3;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/1C2;

.field private final f:LX/13l;

.field public final g:LX/378;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:LX/3nl;

.field private final j:LX/7Rw;

.field public k:LX/Hex;

.field public l:LX/Hey;

.field public m:LX/Heq;

.field public n:LX/Hep;

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:LX/Her;

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:LX/D7o;

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1C2;LX/13l;LX/378;Lcom/facebook/content/SecureContextHelper;LX/3nl;LX/7Rw;LX/0wW;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491235
    iput-object p1, p0, LX/Hf5;->a:Landroid/content/Context;

    .line 2491236
    iput-object p2, p0, LX/Hf5;->b:Landroid/view/WindowManager;

    .line 2491237
    iput-object p3, p0, LX/Hf5;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2491238
    new-instance v0, LX/Hf3;

    invoke-direct {v0, p0}, LX/Hf3;-><init>(LX/Hf5;)V

    iput-object v0, p0, LX/Hf5;->c:LX/Hf3;

    .line 2491239
    iput-object p4, p0, LX/Hf5;->e:LX/1C2;

    .line 2491240
    iput-object p5, p0, LX/Hf5;->f:LX/13l;

    .line 2491241
    iput-object p6, p0, LX/Hf5;->g:LX/378;

    .line 2491242
    iput-object p7, p0, LX/Hf5;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2491243
    iput-object p8, p0, LX/Hf5;->i:LX/3nl;

    .line 2491244
    iput-object p9, p0, LX/Hf5;->j:LX/7Rw;

    .line 2491245
    iput-object p10, p0, LX/Hf5;->D:LX/0wW;

    .line 2491246
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, LX/Hf5;->C:LX/04D;

    .line 2491247
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    .line 2491248
    iget-object v0, p0, LX/Hf5;->b:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2491249
    return-void
.end method

.method private static a(LX/Hf5;D)V
    .locals 13

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2491250
    iget-object v0, p0, LX/Hf5;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2491251
    const v1, 0x7f0b1be8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2491252
    const v2, 0x7f0b1be9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 2491253
    const v3, 0x7f0b1bea

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 2491254
    iget-object v3, p0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2491255
    int-to-double v4, v3

    const-wide v6, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 2491256
    int-to-double v6, v3

    const-wide v8, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v6, v8

    double-to-int v3, v6

    .line 2491257
    cmpl-double v5, p1, v10

    if-nez v5, :cond_2

    .line 2491258
    iput v3, p0, LX/Hf5;->o:I

    .line 2491259
    iput v3, p0, LX/Hf5;->p:I

    .line 2491260
    :goto_0
    iget v3, p0, LX/Hf5;->o:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    iput v1, p0, LX/Hf5;->o:I

    .line 2491261
    iget v1, p0, LX/Hf5;->p:I

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    iput v0, p0, LX/Hf5;->p:I

    .line 2491262
    iget-object v0, p0, LX/Hf5;->l:LX/Hey;

    iget v1, p0, LX/Hf5;->o:I

    .line 2491263
    invoke-static {v0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 2491264
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v3, v1, :cond_0

    .line 2491265
    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2491266
    invoke-static {v0}, LX/HeO;->g(LX/HeO;)V

    .line 2491267
    :cond_0
    iget-object v0, p0, LX/Hf5;->l:LX/Hey;

    iget v1, p0, LX/Hf5;->p:I

    .line 2491268
    invoke-static {v0}, LX/HeO;->f(LX/HeO;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 2491269
    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v1, :cond_1

    .line 2491270
    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2491271
    invoke-static {v0}, LX/HeO;->g(LX/HeO;)V

    .line 2491272
    :cond_1
    return-void

    .line 2491273
    :cond_2
    cmpl-double v3, p1, v10

    if-lez v3, :cond_3

    .line 2491274
    iput v4, p0, LX/Hf5;->o:I

    .line 2491275
    iget v3, p0, LX/Hf5;->o:I

    int-to-double v4, v3

    div-double/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    iput v3, p0, LX/Hf5;->p:I

    goto :goto_0

    .line 2491276
    :cond_3
    iput v4, p0, LX/Hf5;->p:I

    .line 2491277
    iget v3, p0, LX/Hf5;->p:I

    int-to-double v4, v3

    mul-double/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    iput v3, p0, LX/Hf5;->o:I

    goto :goto_0
.end method

.method public static a(LX/Hf5;LX/Her;Z)V
    .locals 4

    .prologue
    .line 2491304
    iget-object v0, p0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2491305
    iget-object v1, p0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2491306
    sget-object v2, LX/Hf1;->a:[I

    invoke-virtual {p1}, LX/Her;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2491307
    :goto_0
    iput-object p1, p0, LX/Hf5;->s:LX/Her;

    .line 2491308
    iget-object v0, p0, LX/Hf5;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/D7y;->a:LX/0Tn;

    invoke-virtual {p1}, LX/Her;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2491309
    return-void

    .line 2491310
    :pswitch_0
    iget v0, p0, LX/Hf5;->u:I

    iget v1, p0, LX/Hf5;->v:I

    invoke-static {p0, v0, v1, p2}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    goto :goto_0

    .line 2491311
    :pswitch_1
    iget v1, p0, LX/Hf5;->o:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/Hf5;->u:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/Hf5;->v:I

    invoke-static {p0, v0, v1, p2}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    goto :goto_0

    .line 2491312
    :pswitch_2
    iget v0, p0, LX/Hf5;->u:I

    iget v2, p0, LX/Hf5;->p:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/Hf5;->w:I

    sub-int/2addr v1, v2

    invoke-static {p0, v0, v1, p2}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    goto :goto_0

    .line 2491313
    :pswitch_3
    iget v2, p0, LX/Hf5;->o:I

    sub-int/2addr v0, v2

    iget v2, p0, LX/Hf5;->u:I

    sub-int/2addr v0, v2

    iget v2, p0, LX/Hf5;->p:I

    sub-int/2addr v1, v2

    iget v2, p0, LX/Hf5;->w:I

    sub-int/2addr v1, v2

    invoke-static {p0, v0, v1, p2}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(LX/Hf5;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 2491278
    new-instance v0, LX/Hez;

    invoke-direct {v0, p0, p1, p2}, LX/Hez;-><init>(LX/Hf5;Ljava/lang/String;I)V

    .line 2491279
    iget-object v1, p0, LX/Hf5;->g:LX/378;

    invoke-virtual {v1, v0}, LX/378;->a(LX/7Ru;)V

    .line 2491280
    iget-object v1, p0, LX/Hf5;->i:LX/3nl;

    .line 2491281
    iget-object v2, v1, LX/3nl;->b:LX/376;

    .line 2491282
    iget-boolean v3, v2, LX/376;->e:Z

    move v2, v3

    .line 2491283
    if-eqz v2, :cond_0

    .line 2491284
    iget-object v2, v1, LX/3nl;->b:LX/376;

    invoke-virtual {v2}, LX/376;->d()V

    .line 2491285
    iget-object v2, v1, LX/3nl;->c:LX/378;

    invoke-virtual {v2}, LX/378;->d()V

    .line 2491286
    invoke-static {v1}, LX/3nl;->i(LX/3nl;)V

    .line 2491287
    :cond_0
    invoke-static {v1}, LX/3nl;->k(LX/3nl;)V

    .line 2491288
    iget-object v2, v1, LX/3nl;->f:LX/7Ru;

    if-nez v2, :cond_1

    .line 2491289
    new-instance v2, LX/7S0;

    invoke-direct {v2, v1}, LX/7S0;-><init>(LX/3nl;)V

    iput-object v2, v1, LX/3nl;->f:LX/7Ru;

    .line 2491290
    :cond_1
    iget-object v2, v1, LX/3nl;->c:LX/378;

    iget-object v3, v1, LX/3nl;->f:LX/7Ru;

    invoke-virtual {v2, v3}, LX/378;->a(LX/7Ru;)V

    .line 2491291
    iget-object v2, v1, LX/3nl;->b:LX/376;

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    .line 2491292
    const/4 v3, 0x0

    iput-object v3, v2, LX/376;->d:Ljava/lang/String;

    .line 2491293
    iput-boolean v4, v2, LX/376;->e:Z

    .line 2491294
    iput-boolean v4, v2, LX/376;->f:Z

    .line 2491295
    iput-wide v5, v2, LX/376;->g:J

    .line 2491296
    iput-wide v5, v2, LX/376;->h:J

    .line 2491297
    iget-object v2, v1, LX/3nl;->b:LX/376;

    invoke-virtual {v2}, LX/376;->a()V

    .line 2491298
    iget-object v2, v1, LX/3nl;->c:LX/378;

    .line 2491299
    sget-object v3, LX/7Rp;->SESSION_STARTED:LX/7Rp;

    invoke-static {v2, v3}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v3

    invoke-static {v3}, LX/378;->a(LX/0oG;)V

    .line 2491300
    invoke-static {v1}, LX/3nl;->j(LX/3nl;)LX/7S1;

    .line 2491301
    iget-object v2, v1, LX/3nl;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 2491302
    :cond_2
    iget-object v1, p0, LX/Hf5;->g:LX/378;

    invoke-virtual {v1, v0}, LX/378;->b(LX/7Ru;)V

    .line 2491303
    return-void
.end method

.method public static a$redex0(LX/Hf5;IIZ)V
    .locals 5

    .prologue
    .line 2491213
    iput p1, p0, LX/Hf5;->q:I

    .line 2491214
    iput p2, p0, LX/Hf5;->r:I

    .line 2491215
    if-eqz p3, :cond_0

    .line 2491216
    iget-object v0, p0, LX/Hf5;->l:LX/Hey;

    .line 2491217
    iget-object v1, v0, LX/Hey;->c:LX/0wd;

    int-to-double v3, p1

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    .line 2491218
    iget-object v1, v0, LX/Hey;->d:LX/0wd;

    int-to-double v3, p2

    invoke-virtual {v1, v3, v4}, LX/0wd;->a(D)LX/0wd;

    .line 2491219
    :goto_0
    return-void

    .line 2491220
    :cond_0
    iget-object v0, p0, LX/Hf5;->l:LX/Hey;

    .line 2491221
    iget-object v1, v0, LX/Hey;->c:LX/0wd;

    int-to-double v3, p1

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 2491222
    iget-object v1, v0, LX/Hey;->d:LX/0wd;

    int-to-double v3, p2

    invoke-virtual {v1, v3, v4}, LX/0wd;->b(D)LX/0wd;

    .line 2491223
    goto :goto_0
.end method

.method public static a$redex0(LX/Hf5;LX/04G;LX/04G;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 2491224
    iget-object v0, p0, LX/Hf5;->k:LX/Hex;

    .line 2491225
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    move-object v2, v1

    .line 2491226
    iget-object v0, v2, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 2491227
    if-nez v0, :cond_0

    .line 2491228
    :goto_0
    return-void

    .line 2491229
    :cond_0
    iget-object v0, v2, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v0

    .line 2491230
    iget-object v9, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2491231
    iget-object v0, p0, LX/Hf5;->e:LX/1C2;

    iget-object v1, v9, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v4

    .line 2491232
    iget-object v3, v2, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v5, v3

    .line 2491233
    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v6, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v7

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v8

    move-object v2, p1

    move-object v3, p2

    move-object v11, v10

    move-object v12, v10

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Hf5;
    .locals 13

    .prologue
    .line 2491208
    new-instance v0, LX/Hf5;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v4

    check-cast v4, LX/1C2;

    invoke-static {p0}, LX/13l;->a(LX/0QB;)LX/13l;

    move-result-object v5

    check-cast v5, LX/13l;

    invoke-static {p0}, LX/378;->a(LX/0QB;)LX/378;

    move-result-object v6

    check-cast v6, LX/378;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/3nl;->a(LX/0QB;)LX/3nl;

    move-result-object v8

    check-cast v8, LX/3nl;

    .line 2491209
    new-instance v12, LX/7Rw;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-static {p0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v10

    check-cast v10, LX/0Uo;

    invoke-static {p0}, LX/378;->a(LX/0QB;)LX/378;

    move-result-object v11

    check-cast v11, LX/378;

    invoke-direct {v12, v9, v10, v11}, LX/7Rw;-><init>(LX/0Xl;LX/0Uo;LX/378;)V

    .line 2491210
    move-object v9, v12

    .line 2491211
    check-cast v9, LX/7Rw;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v10

    check-cast v10, LX/0wW;

    invoke-direct/range {v0 .. v10}, LX/Hf5;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1C2;LX/13l;LX/378;Lcom/facebook/content/SecureContextHelper;LX/3nl;LX/7Rw;LX/0wW;)V

    .line 2491212
    return-object v0
.end method

.method public static b(LX/0oG;Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 2491202
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491203
    const/4 v0, 0x0

    .line 2491204
    :goto_0
    return v0

    .line 2491205
    :cond_0
    sget-object v0, LX/7Rq;->VIDEO_ID:LX/7Rq;

    iget-object v0, v0, LX/7Rq;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2491206
    sget-object v0, LX/7Rq;->VIDEO_TIME_POSITION:LX/7Rq;

    iget-object v0, v0, LX/7Rq;->value:Ljava/lang/String;

    invoke-virtual {p0, v0, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2491207
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static g(LX/Hf5;)V
    .locals 3

    .prologue
    .line 2491199
    iget-object v0, p0, LX/Hf5;->k:LX/Hex;

    new-instance v1, LX/Hf4;

    invoke-direct {v1, p0}, LX/Hf4;-><init>(LX/Hf5;)V

    invoke-virtual {v0, v1}, LX/Hex;->setTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2491200
    iget-object v0, p0, LX/Hf5;->s:LX/Her;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/Hf5;->a(LX/Hf5;LX/Her;Z)V

    .line 2491201
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D7o;DIILX/04D;LX/04G;LX/04g;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/D7o;",
            "DII",
            "LX/04D;",
            "LX/04G;",
            "LX/04g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2491185
    move-object/from16 v0, p1

    iput-object v0, p0, LX/Hf5;->E:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2491186
    move-object/from16 v0, p2

    iput-object v0, p0, LX/Hf5;->F:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2491187
    move-object/from16 v0, p3

    iput-object v0, p0, LX/Hf5;->G:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2491188
    move-object/from16 v0, p4

    iput-object v0, p0, LX/Hf5;->y:LX/D7o;

    .line 2491189
    move-wide/from16 v0, p5

    invoke-static {p0, v0, v1}, LX/Hf5;->a(LX/Hf5;D)V

    .line 2491190
    invoke-static {p0}, LX/Hf5;->g(LX/Hf5;)V

    .line 2491191
    iget-object v2, p0, LX/Hf5;->j:LX/7Rw;

    invoke-virtual {v2}, LX/7Rw;->a()V

    .line 2491192
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Hf5;->C:LX/04D;

    .line 2491193
    sget-object v2, LX/D7p;->a:LX/2pa;

    iget-object v11, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2491194
    iget-object v2, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move/from16 v0, p7

    invoke-static {p0, v2, v0}, LX/Hf5;->a(LX/Hf5;Ljava/lang/String;I)V

    .line 2491195
    iget-object v2, p0, LX/Hf5;->e:LX/1C2;

    iget-object v3, v11, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v4, LX/04G;->WATCH_AND_GO:LX/04G;

    iget-object v6, v11, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v8, v0, LX/04g;->value:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v5, p10

    move-object/from16 v7, p9

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-virtual/range {v2 .. v14}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 2491196
    iget-object v2, p0, LX/Hf5;->f:LX/13l;

    invoke-static {v2}, LX/13l;->a(LX/13l;)V

    .line 2491197
    iget-object v2, p0, LX/Hf5;->k:LX/Hex;

    sget-object v3, LX/D7p;->a:LX/2pa;

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, LX/Hex;->a(LX/2pa;I)V

    .line 2491198
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2491181
    iget-object v0, p0, LX/Hf5;->k:LX/Hex;

    .line 2491182
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2491183
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object p0, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {v1, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2491184
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 2491148
    iget-object v0, p0, LX/Hf5;->k:LX/Hex;

    .line 2491149
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    move-object v0, v1

    .line 2491150
    sget-object v1, LX/04D;->UNKNOWN:LX/04D;

    iput-object v1, p0, LX/Hf5;->C:LX/04D;

    .line 2491151
    iget-object v1, p0, LX/Hf5;->g:LX/378;

    .line 2491152
    sget-object v2, LX/7Rp;->PLAYER_DISMISSED:LX/7Rp;

    invoke-static {v1, v2}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v2

    invoke-static {v2}, LX/378;->a(LX/0oG;)V

    .line 2491153
    iget-object v1, p0, LX/Hf5;->l:LX/Hey;

    invoke-virtual {v1}, LX/HeO;->b()V

    .line 2491154
    iget-object v1, p0, LX/Hf5;->m:LX/Heq;

    invoke-virtual {v1}, LX/HeO;->b()V

    .line 2491155
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 2491156
    new-instance v2, LX/Hf0;

    invoke-direct {v2, p0, v1, v0}, LX/Hf0;-><init>(LX/Hf5;Ljava/lang/String;I)V

    .line 2491157
    iget-object v3, p0, LX/Hf5;->g:LX/378;

    invoke-virtual {v3, v2}, LX/378;->a(LX/7Ru;)V

    .line 2491158
    iget-object v3, p0, LX/Hf5;->i:LX/3nl;

    .line 2491159
    iget-object v1, v3, LX/3nl;->b:LX/376;

    .line 2491160
    iget-boolean v0, v1, LX/376;->e:Z

    move v1, v0

    .line 2491161
    if-nez v1, :cond_0

    .line 2491162
    :goto_0
    iget-object v3, p0, LX/Hf5;->g:LX/378;

    invoke-virtual {v3, v2}, LX/378;->b(LX/7Ru;)V

    .line 2491163
    iget-object v0, p0, LX/Hf5;->j:LX/7Rw;

    .line 2491164
    iget-object v1, v0, LX/7Rw;->d:LX/0Yb;

    if-nez v1, :cond_2

    .line 2491165
    :goto_1
    iget-object v1, v0, LX/7Rw;->e:LX/7Ru;

    if-nez v1, :cond_3

    .line 2491166
    :goto_2
    iget-object v0, p0, LX/Hf5;->A:LX/Hem;

    invoke-virtual {v0}, LX/Hem;->a()V

    .line 2491167
    return-void

    .line 2491168
    :cond_0
    iget-object v1, v3, LX/3nl;->b:LX/376;

    invoke-virtual {v1}, LX/376;->d()V

    .line 2491169
    iget-object v1, v3, LX/3nl;->c:LX/378;

    invoke-virtual {v1}, LX/378;->d()V

    .line 2491170
    invoke-static {v3}, LX/3nl;->i(LX/3nl;)V

    .line 2491171
    iget-object v1, v3, LX/3nl;->e:LX/7Rz;

    if-nez v1, :cond_1

    .line 2491172
    :goto_3
    iget-object v1, v3, LX/3nl;->c:LX/378;

    iget-object v0, v3, LX/3nl;->f:LX/7Ru;

    invoke-virtual {v1, v0}, LX/378;->b(LX/7Ru;)V

    .line 2491173
    goto :goto_0

    .line 2491174
    :cond_1
    iget-object v1, v3, LX/3nl;->e:LX/7Rz;

    iget-object v0, v3, LX/3nl;->a:Landroid/content/Context;

    .line 2491175
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2491176
    goto :goto_3

    .line 2491177
    :cond_2
    iget-object v1, v0, LX/7Rw;->d:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2491178
    const/4 v1, 0x0

    iput-object v1, v0, LX/7Rw;->d:LX/0Yb;

    goto :goto_1

    .line 2491179
    :cond_3
    iget-object v1, v0, LX/7Rw;->c:LX/378;

    iget-object v2, v0, LX/7Rw;->e:LX/7Ru;

    invoke-virtual {v1, v2}, LX/378;->b(LX/7Ru;)V

    .line 2491180
    const/4 v1, 0x0

    iput-object v1, v0, LX/7Rw;->e:LX/7Ru;

    goto :goto_2
.end method
