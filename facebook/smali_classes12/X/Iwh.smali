.class public final LX/Iwh;
.super LX/Iwe;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;)V
    .locals 0

    .prologue
    .line 2630782
    iput-object p1, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    invoke-direct {p0}, LX/Iwe;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2630783
    check-cast p1, LX/Iwd;

    .line 2630784
    iget-boolean v0, p1, LX/Iwd;->b:Z

    if-eqz v0, :cond_0

    .line 2630785
    iget-object v0, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2630786
    iget-object v1, v0, LX/Iw6;->a:LX/0Zb;

    const-string v4, "tapped_like_page_on_landing"

    invoke-static {v4}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "page_id"

    invoke-virtual {v4, v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v1, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630787
    :goto_0
    iget-object v0, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/Iwd;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a(Ljava/lang/String;Z)V

    .line 2630788
    iget-object v0, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    const v1, -0x7548dd31

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2630789
    iget-object v0, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/Iwd;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b(Ljava/lang/String;Z)V

    .line 2630790
    return-void

    .line 2630791
    :cond_0
    iget-object v0, p0, LX/Iwh;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2630792
    iget-object v1, v0, LX/Iw6;->a:LX/0Zb;

    const-string v4, "tapped_unlike_page_on_landing"

    invoke-static {v4}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "page_id"

    invoke-virtual {v4, v5, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v1, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630793
    goto :goto_0
.end method
