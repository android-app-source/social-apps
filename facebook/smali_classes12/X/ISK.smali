.class public final LX/ISK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/ISL;


# direct methods
.method public constructor <init>(LX/ISL;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2577792
    iput-object p1, p0, LX/ISK;->b:LX/ISL;

    iput-object p2, p0, LX/ISK;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1d0cd4b0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2577793
    iget-object v1, p0, LX/ISK;->b:LX/ISL;

    iget-object v1, v1, LX/ISL;->b:LX/1EV;

    invoke-virtual {v1}, LX/1EV;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2577794
    sget-object v1, LX/0ax;->gd:Ljava/lang/String;

    iget-object v2, p0, LX/ISK;->b:LX/ISL;

    iget-object v2, v2, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2577795
    iget-object v2, p0, LX/ISK;->b:LX/ISL;

    iget-object v2, v2, LX/ISL;->a:LX/17W;

    iget-object v3, p0, LX/ISK;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2577796
    :goto_0
    const v1, -0x1f636b9b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2577797
    :cond_0
    iget-object v1, p0, LX/ISK;->b:LX/ISL;

    iget-object v1, v1, LX/ISL;->c:LX/DOL;

    iget-object v2, p0, LX/ISK;->b:LX/ISL;

    iget-object v2, v2, LX/ISL;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1, v2}, LX/DOL;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;

    move-result-object v1

    .line 2577798
    iget-object v2, p0, LX/ISK;->b:LX/ISL;

    iget-object v2, v2, LX/ISL;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/ISK;->b:LX/ISL;

    invoke-virtual {v3}, LX/ISL;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2577799
    iget-object v1, p0, LX/ISK;->b:LX/ISL;

    iget-object v1, v1, LX/ISL;->e:LX/B0k;

    .line 2577800
    iget-object v2, v1, LX/B0k;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "commerce_inventory_enter"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2577801
    goto :goto_0
.end method
