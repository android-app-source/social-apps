.class public final LX/IUU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DRb;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 0

    .prologue
    .line 2579980
    iput-object p1, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/DRa;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2579981
    iget-object v0, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    sget-object v1, LX/DRa;->LEARNING_UNITS:LX/DRa;

    invoke-virtual {p1, v1}, LX/DRa;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2579982
    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->al:Z

    .line 2579983
    iget-object v0, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->j:LX/DNX;

    iget-object v1, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-static {v1}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->t(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/DNX;->e(Z)V

    .line 2579984
    iget-object v0, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->P:LX/ISH;

    iget-object v1, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->G:LX/IW5;

    .line 2579985
    iget-object v2, v1, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v2

    .line 2579986
    iget-object v2, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ai:LX/IQT;

    invoke-virtual {v2}, LX/IQT;->c()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v5

    :goto_0
    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2579987
    iget-object v0, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    .line 2579988
    iget v2, v1, LX/DRc;->g:I

    move v1, v2

    .line 2579989
    iget-object v2, p0, LX/IUU;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->ae:LX/DRc;

    invoke-virtual {v2}, LX/DRc;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0g8;->c(II)V

    .line 2579990
    return-void

    :cond_0
    move v2, v3

    .line 2579991
    goto :goto_0
.end method
