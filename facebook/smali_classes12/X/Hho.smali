.class public LX/Hho;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Hho;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2496194
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2496195
    sget-object v0, LX/0ax;->gJ:Ljava/lang/String;

    const-class v1, Lcom/facebook/zero/onboarding/FbInviteIncentiveActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2496196
    sget-object v0, LX/0ax;->gK:Ljava/lang/String;

    const-class v1, Lcom/facebook/zero/onboarding/FbLinkIncentiveActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2496197
    return-void
.end method

.method public static a(LX/0QB;)LX/Hho;
    .locals 3

    .prologue
    .line 2496198
    sget-object v0, LX/Hho;->a:LX/Hho;

    if-nez v0, :cond_1

    .line 2496199
    const-class v1, LX/Hho;

    monitor-enter v1

    .line 2496200
    :try_start_0
    sget-object v0, LX/Hho;->a:LX/Hho;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2496201
    if-eqz v2, :cond_0

    .line 2496202
    :try_start_1
    new-instance v0, LX/Hho;

    invoke-direct {v0}, LX/Hho;-><init>()V

    .line 2496203
    move-object v0, v0

    .line 2496204
    sput-object v0, LX/Hho;->a:LX/Hho;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2496205
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2496206
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2496207
    :cond_1
    sget-object v0, LX/Hho;->a:LX/Hho;

    return-object v0

    .line 2496208
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2496209
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
