.class public final LX/HUe;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 0

    .prologue
    .line 2473339
    iput-object p1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2473415
    iget-object v0, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473416
    iget-object v0, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b:LX/03V;

    const-string v1, "PagesVideosFragment"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2473417
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2473340
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2473341
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473342
    if-eqz v0, :cond_0

    .line 2473343
    iget-object v3, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473344
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473345
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2473346
    iput-object v0, v3, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->r:Ljava/lang/String;

    .line 2473347
    iget-object v3, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473348
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473349
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    .line 2473350
    iput-boolean v0, v3, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->s:Z

    .line 2473351
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473352
    if-eqz v0, :cond_4

    .line 2473353
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473354
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2473355
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473356
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2473357
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473358
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2473359
    iget-object v0, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473360
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->A:Z

    .line 2473361
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473362
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2473363
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473364
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473365
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->a()I

    move-result v0

    .line 2473366
    iput v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    .line 2473367
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473368
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    .line 2473369
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    .line 2473370
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473371
    iget-object v2, v1, LX/HUl;->b:LX/1Cv;

    move-object v1, v2

    .line 2473372
    instance-of v1, v1, LX/HUj;

    if-nez v1, :cond_1

    .line 2473373
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    new-instance v3, LX/HUj;

    iget-object v4, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {v3, v4}, LX/HUj;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v1, v3}, LX/HUl;->a(LX/1Cv;)V

    .line 2473374
    :cond_1
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    .line 2473375
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    .line 2473376
    iget-object p0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2473377
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2473378
    :cond_2
    invoke-static {v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    .line 2473379
    :cond_3
    :goto_1
    return-void

    .line 2473380
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473381
    if-eqz v0, :cond_6

    .line 2473382
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473383
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2473384
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473385
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2473386
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473387
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2473388
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473389
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->a()I

    move-result v0

    if-lez v0, :cond_6

    .line 2473390
    iget-object v0, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473391
    iput-boolean v2, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->A:Z

    .line 2473392
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473393
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473394
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->a()I

    move-result v0

    .line 2473395
    iput v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    .line 2473396
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473397
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    .line 2473398
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    .line 2473399
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473400
    iget-object v2, v1, LX/HUl;->b:LX/1Cv;

    move-object v1, v2

    .line 2473401
    instance-of v1, v1, LX/HUh;

    if-nez v1, :cond_5

    .line 2473402
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    new-instance v3, LX/HUh;

    iget-object v4, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {v3, v4}, LX/HUh;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v1, v3}, LX/HUl;->a(LX/1Cv;)V

    .line 2473403
    :cond_5
    iget-object v1, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;LX/0Px;)V

    goto/16 :goto_1

    .line 2473404
    :cond_6
    iget-object v0, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-static {v0, v2}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473405
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473406
    if-eqz v0, :cond_8

    .line 2473407
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473408
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2473409
    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    .line 2473410
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473411
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HUe;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2473412
    iput-object v0, v3, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->w:Ljava/lang/String;

    .line 2473413
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 2473414
    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_2
.end method
