.class public final LX/HUE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V
    .locals 0

    .prologue
    .line 2471670
    iput-object p1, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2471671
    iget-object v0, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471672
    iget-object v0, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    .line 2471673
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471674
    iget-object v0, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPagesApprovedEndorsements"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2471675
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2471676
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2471677
    iget-object v0, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    const/4 v1, 0x0

    .line 2471678
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471679
    if-nez p1, :cond_0

    .line 2471680
    :goto_0
    return-void

    .line 2471681
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2471682
    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsQueryModel;

    .line 2471683
    iget-object v1, p0, LX/HUE;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsQueryModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;)V

    goto :goto_0
.end method
