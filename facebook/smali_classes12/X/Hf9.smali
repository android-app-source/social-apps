.class public LX/Hf9;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hf7;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HfA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2491347
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hf9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HfA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491348
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2491349
    iput-object p1, p0, LX/Hf9;->b:LX/0Ot;

    .line 2491350
    return-void
.end method

.method public static a(LX/0QB;)LX/Hf9;
    .locals 4

    .prologue
    .line 2491351
    const-class v1, LX/Hf9;

    monitor-enter v1

    .line 2491352
    :try_start_0
    sget-object v0, LX/Hf9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491353
    sput-object v2, LX/Hf9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491354
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491355
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491356
    new-instance v3, LX/Hf9;

    const/16 p0, 0x38c5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hf9;-><init>(LX/0Ot;)V

    .line 2491357
    move-object v0, v3

    .line 2491358
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491359
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hf9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491360
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2491362
    invoke-static {}, LX/1dS;->b()V

    .line 2491363
    iget v0, p1, LX/BcQ;->b:I

    .line 2491364
    packed-switch v0, :pswitch_data_0

    .line 2491365
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2491366
    :pswitch_0
    check-cast p2, LX/BdG;

    .line 2491367
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast v0, LX/BcP;

    iget-object v3, p1, LX/BcQ;->a:LX/BcO;

    .line 2491368
    check-cast v3, LX/Hf8;

    .line 2491369
    iget-object v4, p0, LX/Hf9;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HfA;

    iget-object p1, v3, LX/Hf8;->b:LX/2kW;

    .line 2491370
    iget-object p2, v4, LX/HfA;->a:LX/HfJ;

    const/4 p0, 0x0

    .line 2491371
    new-instance v3, LX/HfI;

    invoke-direct {v3, p2}, LX/HfI;-><init>(LX/HfJ;)V

    .line 2491372
    sget-object v4, LX/HfJ;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HfH;

    .line 2491373
    if-nez v4, :cond_0

    .line 2491374
    new-instance v4, LX/HfH;

    invoke-direct {v4}, LX/HfH;-><init>()V

    .line 2491375
    :cond_0
    invoke-static {v4, v0, p0, p0, v3}, LX/HfH;->a$redex0(LX/HfH;LX/1De;IILX/HfI;)V

    .line 2491376
    move-object v3, v4

    .line 2491377
    move-object p0, v3

    .line 2491378
    move-object p2, p0

    .line 2491379
    iget-object p0, p2, LX/HfH;->a:LX/HfI;

    iput v1, p0, LX/HfI;->a:I

    .line 2491380
    iget-object p0, p2, LX/HfH;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491381
    move-object p2, p2

    .line 2491382
    check-cast v2, Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    .line 2491383
    iget-object p0, p2, LX/HfH;->a:LX/HfI;

    iput-object v2, p0, LX/HfI;->b:Lcom/facebook/work/groupstab/protocol/FetchWorkSuggestedGroupsQueryModels$WorkGroupDiscoverDataModel;

    .line 2491384
    iget-object p0, p2, LX/HfH;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491385
    move-object p2, p2

    .line 2491386
    iget-object p0, p2, LX/HfH;->a:LX/HfI;

    iput-object p1, p0, LX/HfI;->c:LX/2kW;

    .line 2491387
    iget-object p0, p2, LX/HfH;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 2491388
    move-object p2, p2

    .line 2491389
    invoke-virtual {p2}, LX/1X5;->d()LX/1X1;

    move-result-object p2

    move-object v4, p2

    .line 2491390
    move-object v0, v4

    .line 2491391
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x59264c65
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 2

    .prologue
    .line 2491392
    check-cast p3, LX/Hf8;

    .line 2491393
    iget-object v0, p0, LX/Hf9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/Hf8;->b:LX/2kW;

    .line 2491394
    invoke-static {}, LX/Bck;->d()LX/Bck;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/Bck;->c(LX/BcP;)LX/Bcg;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bcg;->a(LX/2kW;)LX/Bcg;

    move-result-object v1

    .line 2491395
    const p0, -0x59264c65

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2491396
    invoke-virtual {v1, p0}, LX/Bcg;->b(LX/BcQ;)LX/Bcg;

    move-result-object v1

    const/4 p0, 0x2

    .line 2491397
    iget-object p3, v1, LX/Bcg;->a:LX/Bch;

    iput p0, p3, LX/Bch;->h:I

    .line 2491398
    move-object v1, v1

    .line 2491399
    invoke-static {p1}, LX/BcS;->a(LX/BcP;)LX/BcQ;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/Bcg;->c(LX/BcQ;)LX/Bcg;

    move-result-object v1

    invoke-virtual {v1}, LX/Bcg;->b()LX/BcO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2491400
    return-void
.end method
