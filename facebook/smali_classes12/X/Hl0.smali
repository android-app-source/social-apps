.class public final LX/Hl0;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/Hl3;


# direct methods
.method public constructor <init>(LX/Hl3;)V
    .locals 0

    iput-object p1, p0, LX/Hl0;->a:LX/Hl3;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 1
    .param p2    # Landroid/webkit/SslErrorHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {}, LX/Hko;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    iget-object v0, p0, LX/Hl0;->a:LX/Hl3;

    iget-object v0, v0, LX/Hl3;->a:LX/Hje;

    iget-object v1, v0, LX/Hje;->b:LX/Hjg;

    iget-object v1, v1, LX/Hjg;->d:LX/Hk8;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Hje;->b:LX/Hjg;

    iget-object v1, v1, LX/Hjg;->d:LX/Hk8;

    invoke-static {}, LX/HkC;->b()V

    iget-object v2, v1, LX/Hk8;->b:LX/HkC;

    iget-object v2, v2, LX/HkC;->a:LX/HjO;

    invoke-virtual {v2}, LX/HjO;->a()V

    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, v0, LX/Hje;->b:LX/Hjg;

    iget-object v2, v2, LX/Hjg;->g:Landroid/content/Context;

    invoke-static {v2, v1}, LX/HjQ;->a(Landroid/content/Context;Landroid/net/Uri;)LX/HjP;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v2, v0, LX/Hje;->b:LX/Hjg;

    invoke-virtual {v1}, LX/HjP;->a()LX/Hkf;

    move-result-object v3

    iput-object v3, v2, LX/Hjg;->i:LX/Hkf;

    iget-object v2, v0, LX/Hje;->b:LX/Hjg;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, LX/Hjg;->h:J

    invoke-virtual {v1}, LX/HjP;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v1

    sget-object v2, LX/Hjg;->a:Ljava/lang/String;

    const-string v3, "Error executing action"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
