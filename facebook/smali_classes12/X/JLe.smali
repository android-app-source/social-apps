.class public LX/JLe;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBReactSearchInputNativeModule"
.end annotation


# static fields
.field private static final d:Lcom/facebook/search/api/GraphSearchQuery;


# instance fields
.field public final a:LX/Bnj;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2681393
    sget-object v0, LX/103;->MARKETPLACE:LX/103;

    invoke-static {v0}, LX/JLl;->a(LX/103;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    move-object v0, v0

    .line 2681394
    sput-object v0, LX/JLe;->d:Lcom/facebook/search/api/GraphSearchQuery;

    return-void
.end method

.method public constructor <init>(LX/5pY;LX/Bnj;Ljava/util/concurrent/Executor;Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681395
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681396
    iput-object p2, p0, LX/JLe;->a:LX/Bnj;

    .line 2681397
    iput-object p3, p0, LX/JLe;->b:Ljava/util/concurrent/Executor;

    .line 2681398
    iput-object p4, p0, LX/JLe;->c:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    .line 2681399
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2681400
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2681401
    const-string v1, "marketplaceSearch"

    const-string v2, "MarketplaceSearch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681402
    const-string v1, "marketplaceSearchOther"

    const-string v2, "MarketplaceSearchOther"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681403
    const-string v1, "groupsDiscoverySearch"

    const-string v2, "GroupsDiscoverySearch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681404
    const-string v1, "B2CSearch"

    const-string v2, "B2CSearch"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681405
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2681406
    const-string v2, "modules"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681407
    return-object v1
.end method

.method public focusSearchBox(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681408
    iget-object v0, p0, LX/JLe;->c:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    sget-object v1, LX/JLe;->d:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->b(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2681409
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681410
    const-string v0, "FBReactSearchInputNativeModule"

    return-object v0
.end method

.method public resignKeyboardViewForReactTag(I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681411
    iget-object v0, p0, LX/JLe;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/fbreact/marketplace/FBReactSearchInputNativeModule$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/fbreact/marketplace/FBReactSearchInputNativeModule$2;-><init>(LX/JLe;I)V

    const v2, -0x63c4ea44

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2681412
    return-void
.end method

.method public updateNativeSearchQuery(Ljava/lang/String;I)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681413
    iget-object v0, p0, LX/JLe;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/fbreact/marketplace/FBReactSearchInputNativeModule$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/facebook/fbreact/marketplace/FBReactSearchInputNativeModule$1;-><init>(LX/JLe;ILjava/lang/String;)V

    const v2, 0x45bab83d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2681414
    return-void
.end method
