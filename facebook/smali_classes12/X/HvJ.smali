.class public final LX/HvJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:J

.field public c:Lcom/facebook/composer/system/model/ComposerModelImpl;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2518824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518825
    const/16 v0, 0x46

    iput v0, p0, LX/HvJ;->a:I

    .line 2518826
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V
    .locals 2

    .prologue
    .line 2518827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518828
    const/16 v0, 0x46

    iput v0, p0, LX/HvJ;->a:I

    .line 2518829
    iget v0, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    iput v0, p0, LX/HvJ;->a:I

    .line 2518830
    iget-wide v0, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    iput-wide v0, p0, LX/HvJ;->b:J

    .line 2518831
    iget-object v0, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    iput-object v0, p0, LX/HvJ;->c:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518832
    iget-object v0, p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    iput-object v0, p0, LX/HvJ;->d:Ljava/lang/String;

    .line 2518833
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;
    .locals 2

    .prologue
    .line 2518834
    new-instance v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-direct {v0, p0}, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;-><init>(LX/HvJ;)V

    return-object v0
.end method
