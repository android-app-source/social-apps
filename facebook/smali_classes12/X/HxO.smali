.class public final LX/HxO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Hx6;

.field public final synthetic c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;ILX/Hx6;)V
    .locals 0

    .prologue
    .line 2522393
    iput-object p1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iput p2, p0, LX/HxO;->a:I

    iput-object p3, p0, LX/HxO;->b:LX/Hx6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x5decdbce

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522394
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget v2, p0, LX/HxO;->a:I

    invoke-static {v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->b(Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;I)V

    .line 2522395
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->g:LX/Hx6;

    iget-object v2, p0, LX/HxO;->b:LX/Hx6;

    if-eq v1, v2, :cond_0

    .line 2522396
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v2, p0, LX/HxO;->b:LX/Hx6;

    invoke-virtual {v1, v2}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setDashboardFilterType(LX/Hx6;)V

    .line 2522397
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->b:LX/1nQ;

    iget-object v2, p0, LX/HxO;->b:LX/Hx6;

    invoke-virtual {v2}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1nQ;->c(Ljava/lang/String;)V

    .line 2522398
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->a:LX/HyA;

    iget-object v2, p0, LX/HxO;->b:LX/Hx6;

    invoke-virtual {v2}, LX/Hx6;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HyA;->b(Ljava/lang/String;)V

    .line 2522399
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->k:LX/Hww;

    if-eqz v1, :cond_0

    .line 2522400
    iget-object v1, p0, LX/HxO;->c:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->k:LX/Hww;

    iget-object v2, p0, LX/HxO;->b:LX/Hx6;

    invoke-interface {v1, v2}, LX/Hww;->a(LX/Hx6;)V

    .line 2522401
    :cond_0
    const v1, 0xbbc8dc1

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
