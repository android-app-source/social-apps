.class public LX/I8x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public final c:LX/Bl6;

.field public final d:LX/1nQ;

.field public final e:LX/3RX;


# direct methods
.method public constructor <init>(LX/0Sh;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/Bl6;LX/1nQ;LX/3RX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542163
    iput-object p1, p0, LX/I8x;->a:LX/0Sh;

    .line 2542164
    iput-object p2, p0, LX/I8x;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2542165
    iput-object p3, p0, LX/I8x;->c:LX/Bl6;

    .line 2542166
    iput-object p4, p0, LX/I8x;->d:LX/1nQ;

    .line 2542167
    iput-object p5, p0, LX/I8x;->e:LX/3RX;

    .line 2542168
    return-void
.end method

.method public static a(LX/0QB;)LX/I8x;
    .locals 7

    .prologue
    .line 2542169
    new-instance v1, LX/I8x;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v4

    check-cast v4, LX/Bl6;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v5

    check-cast v5, LX/1nQ;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v6

    check-cast v6, LX/3RX;

    invoke-direct/range {v1 .. v6}, LX/I8x;-><init>(LX/0Sh;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/Bl6;LX/1nQ;LX/3RX;)V

    .line 2542170
    move-object v0, v1

    .line 2542171
    return-object v0
.end method
