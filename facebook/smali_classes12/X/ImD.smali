.class public LX/ImD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GI;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7GI",
        "<",
        "Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;",
        "LX/Ipt;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImJ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImO;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImM;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImI;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImN;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImP;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ImK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609111
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImD;->j:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ImJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ImK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609113
    iput-object p1, p0, LX/ImD;->a:LX/0Ot;

    .line 2609114
    iput-object p2, p0, LX/ImD;->b:LX/0Ot;

    .line 2609115
    iput-object p3, p0, LX/ImD;->c:LX/0Ot;

    .line 2609116
    iput-object p4, p0, LX/ImD;->d:LX/0Ot;

    .line 2609117
    iput-object p5, p0, LX/ImD;->e:LX/0Ot;

    .line 2609118
    iput-object p6, p0, LX/ImD;->f:LX/0Ot;

    .line 2609119
    iput-object p7, p0, LX/ImD;->g:LX/0Ot;

    .line 2609120
    iput-object p8, p0, LX/ImD;->h:LX/0Ot;

    .line 2609121
    iput-object p9, p0, LX/ImD;->i:LX/0Ot;

    .line 2609122
    return-void
.end method

.method public static a(LX/0QB;)LX/ImD;
    .locals 7

    .prologue
    .line 2609123
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609124
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609125
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609126
    if-nez v1, :cond_0

    .line 2609127
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609128
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609129
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609130
    sget-object v1, LX/ImD;->j:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609131
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609132
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609133
    :cond_1
    if-nez v1, :cond_4

    .line 2609134
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609135
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609136
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ImD;->b(LX/0QB;)LX/ImD;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2609137
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609138
    if-nez v1, :cond_2

    .line 2609139
    sget-object v0, LX/ImD;->j:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609140
    :goto_1
    if-eqz v0, :cond_3

    .line 2609141
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609142
    :goto_3
    check-cast v0, LX/ImD;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609143
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609144
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609145
    :catchall_1
    move-exception v0

    .line 2609146
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609147
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609148
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609149
    :cond_2
    :try_start_8
    sget-object v0, LX/ImD;->j:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImD;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ImD;
    .locals 10

    .prologue
    .line 2609150
    new-instance v0, LX/ImD;

    const/16 v1, 0x2895

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2897

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x289c

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x289a

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2898

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2894

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2899

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x289b

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2896

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/ImD;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2609151
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)LX/7GG;
    .locals 1

    .prologue
    .line 2609152
    check-cast p1, LX/Ipt;

    invoke-virtual {p0, p1}, LX/ImD;->a(LX/Ipt;)LX/ImH;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Ipt;)LX/ImH;
    .locals 3

    .prologue
    .line 2609153
    iget v0, p1, LX/6kT;->setField_:I

    move v0, v0

    .line 2609154
    packed-switch v0, :pswitch_data_0

    .line 2609155
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported payment delta type: %s"

    .line 2609156
    iget v2, p1, LX/6kT;->setField_:I

    move v2, v2

    .line 2609157
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609158
    :pswitch_0
    iget-object v0, p0, LX/ImD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    .line 2609159
    :goto_0
    return-object v0

    .line 2609160
    :pswitch_1
    iget-object v0, p0, LX/ImD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609161
    :pswitch_2
    iget-object v0, p0, LX/ImD;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609162
    :pswitch_3
    iget-object v0, p0, LX/ImD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609163
    :pswitch_4
    iget-object v0, p0, LX/ImD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609164
    :pswitch_5
    iget-object v0, p0, LX/ImD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609165
    :pswitch_6
    iget-object v0, p0, LX/ImD;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609166
    :pswitch_7
    iget-object v0, p0, LX/ImD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609167
    :pswitch_8
    iget-object v0, p0, LX/ImD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    .line 2609168
    :pswitch_9
    iget-object v0, p0, LX/ImD;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImH;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
