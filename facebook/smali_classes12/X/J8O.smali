.class public final enum LX/J8O;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/J8M;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J8O;",
        ">;",
        "LX/J8M;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J8O;

.field public static final enum DetectAll:LX/J8O;

.field public static final enum Lax:LX/J8O;

.field public static final enum PenaltyDeath:LX/J8O;

.field public static final enum PenaltyDropBox:LX/J8O;

.field public static final enum PenaltyLog:LX/J8O;

.field public static final enum Reset:LX/J8O;


# instance fields
.field private final mSetter:LX/J8M;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2652051
    new-instance v0, LX/J8O;

    const-string v1, "Lax"

    new-instance v2, LX/J8S;

    invoke-direct {v2}, LX/J8S;-><init>()V

    invoke-direct {v0, v1, v4, v2}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->Lax:LX/J8O;

    .line 2652052
    new-instance v0, LX/J8O;

    const-string v1, "Reset"

    new-instance v2, LX/J8W;

    invoke-direct {v2}, LX/J8W;-><init>()V

    invoke-direct {v0, v1, v5, v2}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->Reset:LX/J8O;

    .line 2652053
    new-instance v0, LX/J8O;

    const-string v1, "DetectAll"

    new-instance v2, LX/J8R;

    invoke-direct {v2}, LX/J8R;-><init>()V

    invoke-direct {v0, v1, v6, v2}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->DetectAll:LX/J8O;

    .line 2652054
    new-instance v0, LX/J8O;

    const-string v1, "PenaltyDeath"

    new-instance v2, LX/J8T;

    invoke-direct {v2}, LX/J8T;-><init>()V

    invoke-direct {v0, v1, v7, v2}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->PenaltyDeath:LX/J8O;

    .line 2652055
    new-instance v0, LX/J8O;

    const-string v1, "PenaltyDropBox"

    new-instance v2, LX/J8U;

    invoke-direct {v2}, LX/J8U;-><init>()V

    invoke-direct {v0, v1, v8, v2}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->PenaltyDropBox:LX/J8O;

    .line 2652056
    new-instance v0, LX/J8O;

    const-string v1, "PenaltyLog"

    const/4 v2, 0x5

    new-instance v3, LX/J8V;

    invoke-direct {v3}, LX/J8V;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/J8O;-><init>(Ljava/lang/String;ILX/J8M;)V

    sput-object v0, LX/J8O;->PenaltyLog:LX/J8O;

    .line 2652057
    const/4 v0, 0x6

    new-array v0, v0, [LX/J8O;

    sget-object v1, LX/J8O;->Lax:LX/J8O;

    aput-object v1, v0, v4

    sget-object v1, LX/J8O;->Reset:LX/J8O;

    aput-object v1, v0, v5

    sget-object v1, LX/J8O;->DetectAll:LX/J8O;

    aput-object v1, v0, v6

    sget-object v1, LX/J8O;->PenaltyDeath:LX/J8O;

    aput-object v1, v0, v7

    sget-object v1, LX/J8O;->PenaltyDropBox:LX/J8O;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/J8O;->PenaltyLog:LX/J8O;

    aput-object v2, v0, v1

    sput-object v0, LX/J8O;->$VALUES:[LX/J8O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILX/J8M;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/J8M;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2652048
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2652049
    iput-object p3, p0, LX/J8O;->mSetter:LX/J8M;

    .line 2652050
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J8O;
    .locals 1

    .prologue
    .line 2652047
    const-class v0, LX/J8O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J8O;

    return-object v0
.end method

.method public static values()[LX/J8O;
    .locals 1

    .prologue
    .line 2652046
    sget-object v0, LX/J8O;->$VALUES:[LX/J8O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J8O;

    return-object v0
.end method


# virtual methods
.method public final set()V
    .locals 1

    .prologue
    .line 2652044
    iget-object v0, p0, LX/J8O;->mSetter:LX/J8M;

    invoke-interface {v0}, LX/J8M;->set()V

    .line 2652045
    return-void
.end method
