.class public LX/JJX;
.super LX/76U;
.source ""


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/JJZ;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/JJR;

.field public final f:LX/JJS;

.field public final g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/JJZ;LX/JJS;LX/0Or;LX/JJR;Ljava/lang/Runnable;LX/78A;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # LX/JJR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/78A;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/JJZ;",
            "LX/JJS;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/JJR;",
            "Ljava/lang/Runnable;",
            "LX/78A;",
            "Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679571
    invoke-direct {p0, p6, p7}, LX/76U;-><init>(Ljava/lang/Runnable;LX/78A;)V

    .line 2679572
    iput-object p1, p0, LX/JJX;->b:Ljava/util/concurrent/ExecutorService;

    .line 2679573
    iput-object p2, p0, LX/JJX;->c:LX/JJZ;

    .line 2679574
    iput-object p3, p0, LX/JJX;->f:LX/JJS;

    .line 2679575
    iput-object p4, p0, LX/JJX;->d:LX/0Or;

    .line 2679576
    iput-object p5, p0, LX/JJX;->e:LX/JJR;

    .line 2679577
    iput-object p8, p0, LX/JJX;->g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2679578
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 1

    .prologue
    .line 2679579
    iget-object v0, p0, LX/JJX;->e:LX/JJR;

    invoke-virtual {v0}, LX/JJR;->a()V

    .line 2679580
    return-void
.end method
