.class public LX/J4h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Boz;


# instance fields
.field private final a:LX/CaG;

.field private final b:Landroid/content/Context;

.field private final c:LX/9hN;


# direct methods
.method public constructor <init>(LX/CaG;Landroid/content/Context;LX/9hN;)V
    .locals 0
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/9hN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2644448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2644449
    iput-object p1, p0, LX/J4h;->a:LX/CaG;

    .line 2644450
    iput-object p2, p0, LX/J4h;->b:Landroid/content/Context;

    .line 2644451
    iput-object p3, p0, LX/J4h;->c:LX/9hN;

    .line 2644452
    return-void
.end method


# virtual methods
.method public final a(LX/5kD;LX/1bf;ZI)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2644453
    iget-object v0, p0, LX/J4h;->a:LX/CaG;

    iget-object v1, p0, LX/J4h;->b:Landroid/content/Context;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/J4h;->c:LX/9hN;

    move-object v5, p2

    move v6, p3

    move v7, p4

    move-object v9, v8

    invoke-virtual/range {v0 .. v9}, LX/CaG;->a(Landroid/content/Context;LX/0Px;Ljava/lang/String;LX/9hN;LX/1bf;ZILX/9hM;LX/74S;)V

    .line 2644454
    return-void
.end method
