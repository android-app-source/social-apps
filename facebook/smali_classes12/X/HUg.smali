.class public final LX/HUg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 0

    .prologue
    .line 2473427
    iput-object p1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2473428
    iget-object v0, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473429
    iget-object v0, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b:LX/03V;

    const-string v1, "VideoTabAllVideosOptimizedFetchingFragment"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2473430
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2473431
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 2473432
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473433
    if-eqz v0, :cond_0

    .line 2473434
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473435
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473436
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2473437
    iput-object v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->r:Ljava/lang/String;

    .line 2473438
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473439
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473440
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    .line 2473441
    iput-boolean v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->s:Z

    .line 2473442
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473443
    if-eqz v0, :cond_2

    .line 2473444
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473445
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2473446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473447
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2473448
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473449
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2473450
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473451
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2473452
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473453
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->a()I

    move-result v0

    if-lez v0, :cond_2

    .line 2473454
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    .line 2473455
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473456
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->a()I

    move-result v0

    .line 2473457
    iput v0, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    .line 2473458
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473459
    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;

    move-result-object v0

    .line 2473460
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V

    .line 2473461
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473462
    iget-object v2, v1, LX/HUl;->b:LX/1Cv;

    move-object v1, v2

    .line 2473463
    instance-of v1, v1, LX/HUh;

    if-nez v1, :cond_1

    .line 2473464
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    new-instance v2, LX/HUh;

    iget-object v3, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {v2, v3}, LX/HUh;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v1, v2}, LX/HUl;->a(LX/1Cv;)V

    .line 2473465
    :cond_1
    iget-object v1, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideosUploadedByUserDetailQueryModel$UploadedVideosModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;LX/0Px;)V

    .line 2473466
    :goto_0
    return-void

    .line 2473467
    :cond_2
    iget-object v0, p0, LX/HUg;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-static {v0, v4}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    goto :goto_0
.end method
