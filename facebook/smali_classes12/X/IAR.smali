.class public final LX/IAR;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2545664
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2545665
    iput-object p1, p0, LX/IAR;->a:Landroid/content/Context;

    .line 2545666
    iput-object p2, p0, LX/IAR;->b:Ljava/util/List;

    .line 2545667
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2545647
    iget-object v0, p0, LX/IAR;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2545648
    iget-object v0, p0, LX/IAR;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2545649
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2545650
    if-eqz p2, :cond_0

    check-cast p2, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;

    .line 2545651
    :goto_0
    iget-object v0, p0, LX/IAR;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545652
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2545653
    iget-object p1, p2, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2545654
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    move-object p0, p0

    .line 2545655
    if-eqz p0, :cond_1

    .line 2545656
    iget-object p0, v0, Lcom/facebook/events/model/EventUser;->d:Ljava/lang/String;

    move-object p0, p0

    .line 2545657
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    :goto_1
    sget-object p3, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, p0, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2545658
    iget-object p0, p2, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;->c:Landroid/widget/TextView;

    .line 2545659
    iget-object p1, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object p1, p1

    .line 2545660
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2545661
    return-object p2

    .line 2545662
    :cond_0
    new-instance p2, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;

    iget-object v0, p0, LX/IAR;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/facebook/events/permalink/hostsinfo/HostInfoRow;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2545663
    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method
