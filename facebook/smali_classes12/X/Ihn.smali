.class public final LX/Ihn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;)V
    .locals 0

    .prologue
    .line 2603015
    iput-object p1, p0, LX/Ihn;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 2603016
    iget-object v0, p0, LX/Ihn;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleX(F)V

    .line 2603017
    iget-object v0, p0, LX/Ihn;->a:Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    iget-object v1, v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleY(F)V

    .line 2603018
    return-void
.end method
