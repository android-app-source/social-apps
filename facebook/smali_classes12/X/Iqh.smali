.class public LX/Iqh;
.super LX/Iqg;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/facebook/messaging/montage/model/art/ArtItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2617158
    invoke-direct/range {p0 .. p6}, LX/Iqg;-><init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;)V

    .line 2617159
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/Iqh;->a:Landroid/net/Uri;

    .line 2617160
    iput-object p8, p0, LX/Iqh;->b:Ljava/lang/String;

    .line 2617161
    return-void
.end method
