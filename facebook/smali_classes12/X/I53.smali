.class public LX/I53;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2535032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2535033
    iput-object p1, p0, LX/I53;->a:Landroid/content/Context;

    .line 2535034
    iput-object p2, p0, LX/I53;->b:LX/0Or;

    .line 2535035
    iput-object p3, p0, LX/I53;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2535036
    iput-object p4, p0, LX/I53;->d:LX/0Or;

    .line 2535037
    return-void
.end method

.method private static a(LX/I53;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 3

    .prologue
    .line 2535029
    invoke-direct/range {p0 .. p8}, LX/I53;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2535030
    iget-object v1, p0, LX/I53;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/I53;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2535031
    return-void
.end method

.method public static b(LX/0QB;)LX/I53;
    .locals 5

    .prologue
    .line 2535027
    new-instance v2, LX/I53;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x2e4

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v2, v0, v3, v1, v4}, LX/I53;-><init>(Landroid/content/Context;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 2535028
    return-object v2
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2535005
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/I53;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 2535006
    const-string v0, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_DISCOVERY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2535007
    const-string v0, "extra_events_discovery_title"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2535008
    const-string v0, "extra_events_discovery_subtitle"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2535009
    const-string v0, "extra_events_discovery_suggestion_token"

    .line 2535010
    new-instance v2, LX/0m9;

    sget-object p0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, p0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2535011
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2535012
    const-string p0, "time"

    invoke-virtual {v2, p0, p3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535013
    :cond_0
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2535014
    const-string p0, "city"

    invoke-virtual {v2, p0, p4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535015
    :cond_1
    if-eqz p5, :cond_2

    .line 2535016
    const-string p0, "lat_lon"

    invoke-virtual {v2, p0, p5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2535017
    :cond_2
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2535018
    const-string p0, "suggestion_type"

    invoke-virtual {v2, p0, p6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535019
    :cond_3
    invoke-static {p7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 2535020
    const-string p0, "event_category"

    invoke-virtual {v2, p0, p7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2535021
    :cond_4
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2535022
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2535023
    const-string v0, "extra_reaction_analytics_params"

    invoke-virtual {v1, v0, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2535024
    const-string v2, "extra_need_fetch_location"

    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez p5, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2535025
    return-object v1

    .line 2535026
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Long;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2535002
    iget-object v9, p0, LX/I53;->d:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/11S;

    sget-object v10, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-interface {v9, v10, v11, v12}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v9

    move-object v3, v9

    .line 2535003
    move-object v0, p0

    move-object v2, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/I53;->a(LX/I53;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2535004
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0m9;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2534996
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    move-object v5, p2

    move-object v6, v1

    move-object v7, v1

    move-object v8, p3

    invoke-static/range {v0 .. v8}, LX/I53;->a(LX/I53;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2534997
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0m9;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2535000
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    move-object v6, v2

    move-object v7, p3

    move-object v8, p4

    invoke-static/range {v0 .. v8}, LX/I53;->a(LX/I53;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2535001
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2534998
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, p1

    move-object v7, v1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/I53;->a(LX/I53;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0m9;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2534999
    return-void
.end method
