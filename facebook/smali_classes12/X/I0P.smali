.class public LX/I0P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/7v8;

.field private b:LX/I08;

.field private c:LX/0ad;

.field public d:Landroid/widget/AbsListView$OnScrollListener;

.field public e:Lcom/facebook/widget/listview/BetterListView;

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/events/dashboard/EventsDashboardFragment;

.field public i:LX/I0H;

.field private j:Ljava/util/GregorianCalendar;

.field public k:I

.field public l:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public m:LX/1PH;

.field public n:Ljava/text/SimpleDateFormat;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/7v8;LX/I08;LX/0ad;LX/I0H;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2526889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2526890
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I0P;->g:Z

    .line 2526891
    iput-object p1, p0, LX/I0P;->a:LX/7v8;

    .line 2526892
    iput-object p2, p0, LX/I0P;->b:LX/I08;

    .line 2526893
    iput-object p3, p0, LX/I0P;->c:LX/0ad;

    .line 2526894
    iput-object p4, p0, LX/I0P;->i:LX/I0H;

    .line 2526895
    return-void
.end method

.method public static a(LX/0QB;)LX/I0P;
    .locals 5

    .prologue
    .line 2526914
    new-instance v4, LX/I0P;

    invoke-static {p0}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v0

    check-cast v0, LX/7v8;

    invoke-static {p0}, LX/I08;->b(LX/0QB;)LX/I08;

    move-result-object v1

    check-cast v1, LX/I08;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/I0H;->b(LX/0QB;)LX/I0H;

    move-result-object v3

    check-cast v3, LX/I0H;

    invoke-direct {v4, v0, v1, v2, v3}, LX/I0P;-><init>(LX/7v8;LX/I08;LX/0ad;LX/I0H;)V

    .line 2526915
    move-object v0, v4

    .line 2526916
    return-object v0
.end method

.method public static a(LX/I0P;Landroid/content/Intent;)Ljava/lang/String;
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v10, 0x6

    const/4 v6, 0x0

    .line 2526899
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 2526900
    if-eqz p1, :cond_4

    .line 2526901
    const-string v0, "birthday_view_referrer_param"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2526902
    const-string v0, "birthday_view_start_date"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2526903
    :goto_0
    :try_start_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2526904
    iget-object v2, p0, LX/I0P;->n:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2526905
    :goto_1
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2526906
    new-instance v8, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v8, p0, LX/I0P;->j:Ljava/util/GregorianCalendar;

    .line 2526907
    cmp-long v4, v2, v4

    if-eqz v4, :cond_3

    .line 2526908
    iget-object v4, p0, LX/I0P;->j:Ljava/util/GregorianCalendar;

    invoke-virtual {v4, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 2526909
    iget-object v2, p0, LX/I0P;->j:Ljava/util/GregorianCalendar;

    invoke-virtual {v2, v10}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v10}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    if-eq v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2526910
    :goto_3
    iget-object v2, p0, LX/I0P;->a:LX/7v8;

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/7v8;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2526911
    :catch_0
    move-exception v0

    .line 2526912
    const-class v2, LX/I0B;

    const-string v3, "Invalid date format"

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move-wide v2, v4

    goto :goto_1

    :cond_2
    move v0, v6

    .line 2526913
    goto :goto_2

    :cond_3
    move-object v0, v7

    goto :goto_3

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static c(LX/I0P;)V
    .locals 6

    .prologue
    .line 2526896
    iget-boolean v0, p0, LX/I0P;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I0P;->b:LX/I08;

    invoke-virtual {v0}, LX/I08;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526897
    :cond_0
    :goto_0
    return-void

    .line 2526898
    :cond_1
    iget-object v0, p0, LX/I0P;->b:LX/I08;

    const/16 v1, 0x18

    iget-object v2, p0, LX/I0P;->p:Ljava/lang/String;

    iget-object v3, p0, LX/I0P;->o:Ljava/lang/String;

    iget-object v4, p0, LX/I0P;->j:Ljava/util/GregorianCalendar;

    new-instance v5, LX/I0O;

    invoke-direct {v5, p0}, LX/I0O;-><init>(LX/I0P;)V

    invoke-virtual/range {v0 .. v5}, LX/I08;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/GregorianCalendar;LX/Hxk;)V

    goto :goto_0
.end method
