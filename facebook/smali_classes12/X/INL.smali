.class public LX/INL;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/Blh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Bm1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/fig/footer/FigFooter;

.field public d:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2570743
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2570744
    const-class v0, LX/INL;

    invoke-static {v0, p0}, LX/INL;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2570745
    const v0, 0x7f0302ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2570746
    const v0, 0x7f0d0a1b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/INL;->d:Landroid/widget/LinearLayout;

    .line 2570747
    const v0, 0x7f0d0a1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/footer/FigFooter;

    iput-object v0, p0, LX/INL;->c:Lcom/facebook/fig/footer/FigFooter;

    .line 2570748
    const/4 p1, 0x0

    .line 2570749
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/INL;->setOrientation(I)V

    .line 2570750
    invoke-virtual {p0}, LX/INL;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2570751
    invoke-virtual {p0, p1, v0, p1, p1}, LX/INL;->setPadding(IIII)V

    .line 2570752
    iget-object v0, p0, LX/INL;->d:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 p1, -0x1

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2570753
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/INL;

    invoke-static {p0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v1

    check-cast v1, LX/Blh;

    invoke-static {p0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object p0

    check-cast p0, LX/Bm1;

    iput-object v1, p1, LX/INL;->a:LX/Blh;

    iput-object p0, p1, LX/INL;->b:LX/Bm1;

    return-void
.end method
