.class public LX/I0H;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/2ht;


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field public final d:Landroid/content/Context;

.field private final e:LX/6RZ;

.field public f:Lcom/facebook/base/fragment/FbFragment;

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/I0F;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Landroid/os/Bundle;

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2526770
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I0H;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6RZ;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2526763
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2526764
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I0H;->h:Z

    .line 2526765
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e08eb

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/I0H;->d:Landroid/content/Context;

    .line 2526766
    iput-object p2, p0, LX/I0H;->e:LX/6RZ;

    .line 2526767
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    .line 2526768
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/I0H;->k:Landroid/os/Bundle;

    .line 2526769
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 13
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 2526725
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2526726
    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    .line 2526727
    new-instance v4, LX/I0F;

    invoke-direct {v4, v0}, LX/I0F;-><init>(LX/I0F;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2526728
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2526729
    :cond_0
    move-object v3, v2

    .line 2526730
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 2526731
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2526732
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    .line 2526733
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2526734
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2526735
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v2, v8}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v6, v2, v12}, LX/15i;->j(II)I

    move-result v2

    .line 2526736
    const/4 v6, 0x1

    invoke-static {v4, v7, v8, v2, v6}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;IIZ)Ljava/util/Calendar;

    move-result-object v6

    move-object v2, v6

    .line 2526737
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    .line 2526738
    const-string v2, ""

    .line 2526739
    iget-object v7, p0, LX/I0H;->e:LX/6RZ;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v7, v8, v9, v10, v11}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v7

    .line 2526740
    sget-object v8, LX/I0E;->a:[I

    invoke-virtual {v7}, LX/6RY;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    .line 2526741
    :goto_3
    if-eqz v1, :cond_2

    iget-object v6, v1, LX/I0F;->a:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2526742
    :cond_2
    new-instance v1, LX/I0F;

    invoke-direct {v1, v2}, LX/I0F;-><init>(Ljava/lang/String;)V

    .line 2526743
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2526744
    :cond_3
    iget-object v2, v1, LX/I0F;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2526745
    iget-boolean v0, p0, LX/I0H;->i:Z

    if-nez v0, :cond_1

    .line 2526746
    iput-boolean v12, p0, LX/I0H;->i:Z

    goto :goto_2

    .line 2526747
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2526748
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2526749
    :pswitch_0
    iget-object v2, p0, LX/I0H;->d:Landroid/content/Context;

    const v6, 0x7f0821af

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2526750
    :pswitch_1
    iget-object v2, p0, LX/I0H;->d:Landroid/content/Context;

    const v6, 0x7f0821b0

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2526751
    :pswitch_2
    iget-object v2, p0, LX/I0H;->d:Landroid/content/Context;

    const v6, 0x7f0821b1

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2526752
    :pswitch_3
    const/4 v11, 0x2

    const/4 v2, 0x1

    .line 2526753
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 2526754
    invoke-virtual {v7, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 2526755
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 2526756
    invoke-virtual {v8, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 2526757
    invoke-virtual {v8, v2}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v7, v2}, Ljava/util/Calendar;->get(I)I

    move-result v10

    if-ne v9, v10, :cond_7

    invoke-virtual {v8, v11}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v11}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-ne v8, v7, :cond_7

    :goto_4
    move v2, v2

    .line 2526758
    if-eqz v2, :cond_5

    .line 2526759
    iget-object v2, p0, LX/I0H;->d:Landroid/content/Context;

    const v6, 0x7f0821b2

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2526760
    :cond_5
    iget-object v2, p0, LX/I0H;->e:LX/6RZ;

    invoke-virtual {v2, v6}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2526761
    :cond_6
    iput-object v3, p0, LX/I0H;->g:Ljava/util/ArrayList;

    .line 2526762
    return-void

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/I0H;
    .locals 3

    .prologue
    .line 2526723
    new-instance v2, LX/I0H;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v1

    check-cast v1, LX/6RZ;

    invoke-direct {v2, v0, v1}, LX/I0H;-><init>(Landroid/content/Context;LX/6RZ;)V

    .line 2526724
    return-object v2
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2526722
    sget-object v0, LX/I0G;->HEADER:LX/I0G;

    invoke-virtual {v0}, LX/I0G;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 2526704
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 2526705
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    .line 2526706
    invoke-virtual {p0, p1, p2}, LX/I0H;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 2526707
    sget-object v1, LX/I0H;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 2526708
    if-eqz p4, :cond_3

    .line 2526709
    check-cast p4, LX/I9b;

    .line 2526710
    :goto_0
    move-object v0, p4

    .line 2526711
    :goto_1
    return-object v0

    .line 2526712
    :cond_0
    if-eqz p4, :cond_1

    check-cast p4, LX/HxG;

    move-object v0, p4

    .line 2526713
    :goto_2
    invoke-virtual {p0, p1, p2}, LX/I0H;->a(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2526714
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v5, v4, v3}, LX/15i;->j(II)I

    move-result v4

    if-gt v4, v2, :cond_2

    .line 2526715
    :goto_3
    iget-object v2, p0, LX/I0H;->f:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v8, -0x1

    .line 2526716
    iget-object v6, p0, LX/I0H;->k:Landroid/os/Bundle;

    invoke-virtual {v6, v4, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    :goto_4
    move v4, v6

    .line 2526717
    iget-object v5, p0, LX/I0H;->j:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/HxG;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/base/fragment/FbFragment;ZZLjava/lang/String;)V

    goto :goto_1

    .line 2526718
    :cond_1
    new-instance v0, LX/HxG;

    iget-object v1, p0, LX/I0H;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/HxG;-><init>(Landroid/content/Context;)V

    goto :goto_2

    .line 2526719
    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    .line 2526720
    :cond_3
    new-instance p4, LX/I9b;

    iget-object v0, p0, LX/I0H;->d:Landroid/content/Context;

    invoke-direct {p4, v0}, LX/I9b;-><init>(Landroid/content/Context;)V

    .line 2526721
    const v0, 0x7f0a00d5

    invoke-virtual {p4, v0}, LX/I9b;->setBackgroundResource(I)V

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2526690
    check-cast p2, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;

    .line 2526691
    if-nez p2, :cond_0

    .line 2526692
    const v0, 0x7f030563

    .line 2526693
    iget-object v1, p0, LX/I0H;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2526694
    const/4 p2, 0x0

    invoke-virtual {v1, v0, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2526695
    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;

    move-object p2, v0

    .line 2526696
    :cond_0
    invoke-virtual {p0, p1}, LX/I0H;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    iget-object v0, v0, LX/I0F;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/events/dashboard/EventsDashboardStickySectionHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 2526697
    iget v0, p0, LX/I0H;->l:I

    if-nez v0, :cond_1

    .line 2526698
    const/4 p3, 0x0

    .line 2526699
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 p1, -0x2

    invoke-direct {v0, v1, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2526700
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 2526701
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    move v0, v0

    .line 2526702
    iput v0, p0, LX/I0H;->l:I

    .line 2526703
    :cond_1
    return-object p2
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2526687
    iget-boolean v0, p0, LX/I0H;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    iget-object v0, v0, LX/I0F;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2526688
    sget-object v0, LX/I0H;->c:Ljava/lang/Object;

    .line 2526689
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    iget-object v0, v0, LX/I0F;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 2526684
    iget-object v0, p0, LX/I0H;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2526685
    const v0, -0x5f616314

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2526686
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2526680
    invoke-direct {p0, p1}, LX/I0H;->a(Ljava/util/List;)V

    .line 2526681
    iput-boolean p2, p0, LX/I0H;->h:Z

    .line 2526682
    const v0, 0x5ce4795

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2526683
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2526678
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 2526679
    invoke-virtual {p0, v0, p2, p3}, LX/I0H;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2526661
    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2526662
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2526663
    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 2526664
    iget-boolean v0, p0, LX/I0H;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    iget-object v0, v0, LX/I0F;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/I0H;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0F;

    iget-object v0, v0, LX/I0F;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 2

    .prologue
    .line 2526665
    invoke-virtual {p0, p1, p2}, LX/I0H;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 2526666
    sget-object v1, LX/I0H;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 2526667
    sget-object v0, LX/I0G;->LOADING:LX/I0G;

    invoke-virtual {v0}, LX/I0G;->ordinal()I

    move-result v0

    .line 2526668
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/I0G;->BIRTHDAY:LX/I0G;

    invoke-virtual {v0}, LX/I0G;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2526669
    iget-object v0, p0, LX/I0H;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2526670
    iget v0, p0, LX/I0H;->l:I

    return v0
.end method

.method public final f(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2526671
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_2

    :cond_0
    move v0, v1

    .line 2526672
    :cond_1
    :goto_0
    return v0

    .line 2526673
    :cond_2
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v2

    .line 2526674
    aget v2, v2, v0

    .line 2526675
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2526676
    invoke-static {}, LX/I0G;->values()[LX/I0G;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2526677
    sget-object v0, LX/I0G;->HEADER:LX/I0G;

    invoke-virtual {v0}, LX/I0G;->ordinal()I

    move-result v0

    return v0
.end method
