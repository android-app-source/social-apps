.class public LX/HuR;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        ":",
        "LX/0j8;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsCheckinSupported;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsLocationLightweightPickerSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/Hr2;

.field private final c:LX/Hu0;

.field public final d:Landroid/content/Context;

.field public final e:LX/HrC;

.field public final f:LX/9j4;

.field public final g:LX/0ad;

.field public final h:LX/Htv;

.field public final i:LX/HuU;

.field public j:Z


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;LX/HrC;Landroid/content/Context;LX/9j4;LX/0ad;LX/Htv;LX/HuU;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/HrC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "Lcom/facebook/composer/location/sprouts/LocationLightweightPickerSproutItem$Listener;",
            "Landroid/content/Context;",
            "LX/9j4;",
            "LX/0ad;",
            "LX/Htv;",
            "LX/HuU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517607
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2517608
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    .line 2517609
    iput-object p2, p0, LX/HuR;->b:LX/Hr2;

    .line 2517610
    iput-object p3, p0, LX/HuR;->e:LX/HrC;

    .line 2517611
    iput-object p4, p0, LX/HuR;->d:Landroid/content/Context;

    .line 2517612
    iput-object p5, p0, LX/HuR;->f:LX/9j4;

    .line 2517613
    iput-object p6, p0, LX/HuR;->g:LX/0ad;

    .line 2517614
    iput-object p7, p0, LX/HuR;->h:LX/Htv;

    .line 2517615
    iput-object p8, p0, LX/HuR;->i:LX/HuU;

    .line 2517616
    new-instance v0, LX/HuQ;

    invoke-direct {v0, p0}, LX/HuQ;-><init>(LX/HuR;)V

    .line 2517617
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v1

    .line 2517618
    iput-object v0, v1, LX/Htz;->h:LX/HuQ;

    .line 2517619
    move-object v0, v1

    .line 2517620
    invoke-virtual {p0}, LX/HuR;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2517621
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2517622
    move-object v0, v0

    .line 2517623
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/HuR;->c:LX/Hu0;

    .line 2517624
    return-void
.end method

.method public static a$redex0(LX/HuR;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2517601
    iget-object v0, p0, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/0il;

    .line 2517602
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0j8;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    .line 2517603
    iget-object v0, p0, LX/HuR;->f:LX/9j4;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->g()LX/0Px;

    move-result-object v5

    move-object v1, p1

    .line 2517604
    invoke-static {v1, v2, v3, v4, v5}, LX/9j4;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2517605
    iget-object p1, v0, LX/9j4;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2517606
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2517592
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2517600
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2517599
    iget-object v0, p0, LX/HuR;->d:Landroid/content/Context;

    const v1, 0x7f0812b6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2517598
    iget-object v0, p0, LX/HuR;->c:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2517596
    iget-object v0, p0, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517597
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->o()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2517594
    iget-object v0, p0, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517595
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    check-cast v0, LX/0j8;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2517593
    sget-object v0, LX/Hty;->LIGHTWEIGHT_LOCATION:LX/Hty;

    return-object v0
.end method
