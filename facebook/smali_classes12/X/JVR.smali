.class public final LX/JVR;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JVS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JVS;


# direct methods
.method public constructor <init>(LX/JVS;)V
    .locals 1

    .prologue
    .line 2700774
    iput-object p1, p0, LX/JVR;->b:LX/JVS;

    .line 2700775
    move-object v0, p1

    .line 2700776
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2700777
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700778
    const-string v0, "PhotoChainingFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2700779
    if-ne p0, p1, :cond_1

    .line 2700780
    :cond_0
    :goto_0
    return v0

    .line 2700781
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2700782
    goto :goto_0

    .line 2700783
    :cond_3
    check-cast p1, LX/JVR;

    .line 2700784
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2700785
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2700786
    if-eq v2, v3, :cond_0

    .line 2700787
    iget-object v2, p0, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2700788
    goto :goto_0

    .line 2700789
    :cond_4
    iget-object v2, p1, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
