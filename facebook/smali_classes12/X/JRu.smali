.class public LX/JRu;
.super LX/3i9;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:Z

.field public final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2694364
    invoke-direct {p0, p1}, LX/3i9;-><init>(Landroid/content/Context;)V

    .line 2694365
    const v0, 0x7f030b9e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2694366
    const v0, 0x7f0d1cc5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/JRu;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2694367
    const v0, 0x7f0d1cc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JRu;->c:Landroid/widget/TextView;

    .line 2694368
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2694369
    iget-boolean v0, p0, LX/JRu;->b:Z

    return v0
.end method

.method public getCallToActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2694370
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x748eec93

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694371
    invoke-super {p0}, LX/3i9;->onAttachedToWindow()V

    .line 2694372
    const/4 v1, 0x1

    .line 2694373
    iput-boolean v1, p0, LX/JRu;->b:Z

    .line 2694374
    const/16 v1, 0x2d

    const v2, 0x49feedd3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3770900c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694375
    invoke-super {p0}, LX/3i9;->onDetachedFromWindow()V

    .line 2694376
    const/4 v1, 0x0

    .line 2694377
    iput-boolean v1, p0, LX/JRu;->b:Z

    .line 2694378
    const/16 v1, 0x2d

    const v2, -0x4afa23b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
