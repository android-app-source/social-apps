.class public LX/JRC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/23i;


# direct methods
.method public constructor <init>(LX/23i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692885
    iput-object p1, p0, LX/JRC;->a:LX/23i;

    .line 2692886
    return-void
.end method

.method public static a(LX/0QB;)LX/JRC;
    .locals 4

    .prologue
    .line 2692887
    const-class v1, LX/JRC;

    monitor-enter v1

    .line 2692888
    :try_start_0
    sget-object v0, LX/JRC;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692889
    sput-object v2, LX/JRC;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692890
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692891
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692892
    new-instance p0, LX/JRC;

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v3

    check-cast v3, LX/23i;

    invoke-direct {p0, v3}, LX/JRC;-><init>(LX/23i;)V

    .line 2692893
    move-object v0, p0

    .line 2692894
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692895
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692896
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
