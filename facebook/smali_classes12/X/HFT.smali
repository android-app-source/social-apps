.class public final LX/HFT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443577
    iput-object p1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 2443578
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2443579
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->B:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->A:Z

    if-nez v1, :cond_1

    .line 2443580
    :cond_0
    iget-object v1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const-string v2, "address_input"

    .line 2443581
    iput-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    .line 2443582
    iget-object v1, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const/16 p1, 0x30

    .line 2443583
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string v3, "city_search_gql_task_key"

    iget-object v4, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b:LX/HFc;

    const/4 p0, 0x3

    invoke-virtual {v4, v0, p0, p1, p1}, LX/HFc;->a(Ljava/lang/String;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p0, LX/HFW;

    invoke-direct {p0, v1}, LX/HFW;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443584
    :goto_0
    return-void

    .line 2443585
    :cond_1
    iget-object v0, p0, LX/HFT;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2443586
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2443587
    return-void
.end method
