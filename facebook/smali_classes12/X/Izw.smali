.class public LX/Izw;
.super LX/Izu;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/IzJ;

.field private final c:LX/IzG;

.field private final d:LX/2JS;

.field private final e:LX/IzI;

.field private final f:LX/J0B;

.field private final g:LX/IzH;


# direct methods
.method public constructor <init>(LX/0Or;LX/IzJ;LX/IzG;LX/2JS;LX/IzI;LX/J0B;LX/IzH;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/IzJ;",
            "LX/IzG;",
            "LX/2JS;",
            "LX/IzI;",
            "LX/J0B;",
            "LX/IzH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636046
    const-string v0, "PaymentCacheServiceHandler"

    invoke-direct {p0, v0}, LX/Izu;-><init>(Ljava/lang/String;)V

    .line 2636047
    iput-object p1, p0, LX/Izw;->a:LX/0Or;

    .line 2636048
    iput-object p2, p0, LX/Izw;->b:LX/IzJ;

    .line 2636049
    iput-object p3, p0, LX/Izw;->c:LX/IzG;

    .line 2636050
    iput-object p4, p0, LX/Izw;->d:LX/2JS;

    .line 2636051
    iput-object p5, p0, LX/Izw;->e:LX/IzI;

    .line 2636052
    iput-object p6, p0, LX/Izw;->f:LX/J0B;

    .line 2636053
    iput-object p7, p0, LX/Izw;->g:LX/IzH;

    .line 2636054
    return-void
.end method

.method public static b(LX/0QB;)LX/Izw;
    .locals 8

    .prologue
    .line 2636044
    new-instance v0, LX/Izw;

    const/16 v1, 0x1547

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/IzJ;->a(LX/0QB;)LX/IzJ;

    move-result-object v2

    check-cast v2, LX/IzJ;

    invoke-static {p0}, LX/IzG;->a(LX/0QB;)LX/IzG;

    move-result-object v3

    check-cast v3, LX/IzG;

    invoke-static {p0}, LX/2JS;->a(LX/0QB;)LX/2JS;

    move-result-object v4

    check-cast v4, LX/2JS;

    invoke-static {p0}, LX/IzI;->a(LX/0QB;)LX/IzI;

    move-result-object v5

    check-cast v5, LX/IzI;

    invoke-static {p0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v6

    check-cast v6, LX/J0B;

    invoke-static {p0}, LX/IzH;->a(LX/0QB;)LX/IzH;

    move-result-object v7

    check-cast v7, LX/IzH;

    invoke-direct/range {v0 .. v7}, LX/Izw;-><init>(LX/0Or;LX/IzJ;LX/IzG;LX/2JS;LX/IzI;LX/J0B;LX/IzH;)V

    .line 2636045
    return-object v0
.end method


# virtual methods
.method public final A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636038
    iget-object v0, p0, LX/Izw;->e:LX/IzI;

    invoke-virtual {v0}, LX/IzI;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2636039
    iget-object v0, p0, LX/Izw;->e:LX/IzI;

    invoke-virtual {v0}, LX/IzI;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636040
    :goto_0
    return-object v0

    .line 2636041
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636042
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableList()Ljava/util/ArrayList;

    move-result-object v1

    .line 2636043
    iget-object v2, p0, LX/Izw;->e:LX/IzI;

    invoke-virtual {v2, v1}, LX/IzI;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2636017
    iget-object v0, p0, LX/Izw;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2636018
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636019
    const-string v1, "fetchPaymentTransactionParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;

    .line 2636020
    iget-object v1, p0, LX/Izw;->b:LX/IzJ;

    .line 2636021
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2636022
    invoke-virtual {v1, v2}, LX/IzJ;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v1

    .line 2636023
    if-eqz v1, :cond_1

    .line 2636024
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    move-object v0, v2

    .line 2636025
    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v0, v2, :cond_0

    .line 2636026
    iget-object v0, v1, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v0, v0

    .line 2636027
    iget-boolean v0, v0, LX/DtQ;->isTerminalStatus:Z

    if-eqz v0, :cond_1

    .line 2636028
    :cond_0
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636029
    :goto_0
    return-object v0

    .line 2636030
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636031
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2636032
    iget-object v2, p0, LX/Izw;->b:LX/IzJ;

    invoke-virtual {v2, v0}, LX/IzJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2636033
    iget-object v2, p0, LX/Izw;->f:LX/J0B;

    .line 2636034
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v3, v3

    .line 2636035
    iget-object v4, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v0, v4

    .line 2636036
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/J0B;->a(LX/DtQ;J)V

    move-object v0, v1

    .line 2636037
    goto :goto_0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2635970
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2635971
    sget-object v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    .line 2635972
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    move-object v1, v1

    .line 2635973
    sget-object v2, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-ne v1, v2, :cond_0

    .line 2635974
    iget-object v1, p0, LX/Izw;->g:LX/IzH;

    .line 2635975
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2635976
    invoke-virtual {v1, v2}, LX/IzH;->a(Ljava/lang/String;)LX/03R;

    move-result-object v1

    .line 2635977
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2635978
    new-instance v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;-><init>(Ljava/lang/Boolean;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2635979
    :goto_0
    return-object v0

    .line 2635980
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2635981
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    .line 2635982
    iget-object v3, p0, LX/Izw;->g:LX/IzH;

    .line 2635983
    iget-object p0, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v0, p0

    .line 2635984
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->a()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, LX/IzH;->a(Ljava/lang/String;Z)V

    move-object v0, v2

    .line 2635985
    goto :goto_0
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636012
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636013
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableList()Ljava/util/ArrayList;

    move-result-object v1

    .line 2636014
    iget-object v2, p0, LX/Izw;->c:LX/IzG;

    invoke-virtual {v2, v1}, LX/IzG;->a(Ljava/util/ArrayList;)V

    .line 2636015
    iget-object v1, p0, LX/Izw;->f:LX/J0B;

    invoke-virtual {v1}, LX/J0B;->f()V

    .line 2636016
    return-object v0
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636007
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636008
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2636009
    iget-object v2, p0, LX/Izw;->d:LX/2JS;

    invoke-virtual {v2, v0}, LX/2JS;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2636010
    iget-object v2, p0, LX/Izw;->f:LX/J0B;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/J0B;->a(Ljava/lang/String;)V

    .line 2636011
    return-object v1
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2635986
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2635987
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    .line 2635988
    iget-object v1, p0, LX/Izw;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2635989
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v1, v1

    .line 2635990
    sget-object v2, LX/J1G;->INCOMING:LX/J1G;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Izw;->d:LX/2JS;

    invoke-virtual {v1}, LX/2JS;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2635991
    new-instance v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, LX/Izw;->d:LX/2JS;

    invoke-virtual {v2}, LX/2JS;->a()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;-><init>(Ljava/util/ArrayList;)V

    .line 2635992
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2635993
    :goto_0
    return-object v0

    .line 2635994
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2635995
    iget-object v1, p0, LX/Izw;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2635996
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v0, v1

    .line 2635997
    sget-object v1, LX/J1G;->INCOMING:LX/J1G;

    if-ne v0, v1, :cond_1

    .line 2635998
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    .line 2635999
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a()LX/0Px;

    move-result-object v0

    .line 2636000
    iget-object v1, p0, LX/Izw;->d:LX/2JS;

    invoke-virtual {v1, v0}, LX/2JS;->a(LX/0Px;)V

    .line 2636001
    iget-object v0, p0, LX/Izw;->f:LX/J0B;

    .line 2636002
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2636003
    const-string p0, "com.facebook.messaging.payment.ACTION_PAYMENT_REQUEST_CACHE_UPDATED"

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636004
    invoke-static {v0, v1}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636005
    :cond_1
    move-object v0, v2

    .line 2636006
    goto :goto_0
.end method
