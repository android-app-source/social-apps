.class public final LX/IS3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DMP;


# instance fields
.field public final synthetic a:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;)V
    .locals 0

    .prologue
    .line 2577430
    iput-object p1, p0, LX/IS3;->a:LX/ISI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V
    .locals 2

    .prologue
    .line 2577426
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)LX/7vF;

    move-result-object v0

    .line 2577427
    iget-object v1, p0, LX/IS3;->a:LX/ISI;

    invoke-static {v1, v0, p3}, LX/ISI;->a$redex0(LX/ISI;LX/7vF;Z)V

    .line 2577428
    iget-object v1, p0, LX/IS3;->a:LX/ISI;

    iget-object v1, v1, LX/ISI;->p:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 2577429
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V
    .locals 2

    .prologue
    .line 2577422
    invoke-static {p1, p2}, LX/7vF;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7vF;

    move-result-object v0

    .line 2577423
    iget-object v1, p0, LX/IS3;->a:LX/ISI;

    invoke-static {v1, v0, p3}, LX/ISI;->a$redex0(LX/ISI;LX/7vF;Z)V

    .line 2577424
    iget-object v1, p0, LX/IS3;->a:LX/ISI;

    iget-object v1, v1, LX/ISI;->p:LX/DMZ;

    invoke-virtual {v1, v0, p2}, LX/DMZ;->a(LX/7vF;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 2577425
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2577420
    iget-object v0, p0, LX/IS3;->a:LX/ISI;

    const v1, -0x39e31961

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2577421
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 2577418
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/IS3;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 2577419
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 1

    .prologue
    .line 2577416
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/IS3;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V

    .line 2577417
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 1

    .prologue
    .line 2577414
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IS3;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Z)V

    .line 2577415
    return-void
.end method

.method public final b(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 1

    .prologue
    .line 2577412
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IS3;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Z)V

    .line 2577413
    return-void
.end method
