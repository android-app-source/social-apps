.class public LX/HMP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field public final a:LX/9XE;

.field private final b:Lcom/facebook/privacy/PrivacyOperationsClient;

.field private final c:LX/HDT;

.field public d:J


# direct methods
.method public constructor <init>(LX/9XE;Lcom/facebook/privacy/PrivacyOperationsClient;LX/HDT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457090
    iput-object p1, p0, LX/HMP;->a:LX/9XE;

    .line 2457091
    iput-object p2, p0, LX/HMP;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2457092
    iput-object p3, p0, LX/HMP;->c:LX/HDT;

    .line 2457093
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2457070
    iget-object v0, p0, LX/HMP;->a:LX/9XE;

    sget-object v1, LX/9XA;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_ERROR:LX/9XA;

    iget-wide v2, p0, LX/HMP;->d:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2457071
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2457088
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457080
    iput-wide p1, p0, LX/HMP;->d:J

    .line 2457081
    const-string v0, "privacy_option"

    invoke-static {p5, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2457082
    if-nez v0, :cond_0

    .line 2457083
    const/4 v0, 0x0

    .line 2457084
    :goto_0
    return-object v0

    .line 2457085
    :cond_0
    new-instance v1, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LX/HMP;->d:J

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;-><init>(Ljava/lang/String;J)V

    .line 2457086
    iget-object v0, p0, LX/HMP;->c:LX/HDT;

    new-instance v2, LX/HDo;

    new-instance v3, LX/HMO;

    invoke-direct {v3, p0}, LX/HMO;-><init>(LX/HMP;)V

    invoke-direct {v2, v3}, LX/HDo;-><init>(LX/HDY;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2457087
    iget-object v0, p0, LX/HMP;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 2457077
    :try_start_0
    iget-object v1, p0, LX/HMP;->a:LX/9XE;

    sget-object v2, LX/9XB;->EVENT_PAGE_EDIT_REVIEW_PRIVACY_SUCCESS:LX/9XB;

    iget-wide v3, p0, LX/HMP;->d:J

    invoke-virtual {v1, v2, v3, v4}, LX/9XE;->a(LX/9X2;J)V
    :try_end_0
    .catch LX/4BK; {:try_start_0 .. :try_end_0} :catch_0

    .line 2457078
    :goto_0
    return-void

    .line 2457079
    :catch_0
    invoke-direct {p0}, LX/HMP;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2457075
    invoke-direct {p0}, LX/HMP;->e()V

    .line 2457076
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457074
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457073
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457072
    const/16 v0, 0x277e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
