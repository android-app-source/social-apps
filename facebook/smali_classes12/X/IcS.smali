.class public final LX/IcS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

.field public final synthetic b:LX/IcT;


# direct methods
.method public constructor <init>(LX/IcT;Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;)V
    .locals 0

    .prologue
    .line 2595541
    iput-object p1, p0, LX/IcS;->b:LX/IcT;

    iput-object p2, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2595523
    iget-object v0, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2595524
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2595525
    iget-object v3, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->o()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    .line 2595526
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2595527
    iget-object v3, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->j()LX/1vs;

    move-result-object v3

    iget-object v6, v3, LX/1vs;->a:LX/15i;

    iget v7, v3, LX/1vs;->b:I

    .line 2595528
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2595529
    if-eqz v5, :cond_2

    .line 2595530
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v4, v5, v10}, LX/15i;->l(II)D

    move-result-wide v8

    invoke-virtual {v4, v5, v11}, LX/15i;->l(II)D

    move-result-wide v4

    invoke-direct {v3, v8, v9, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2595531
    :goto_0
    if-eqz v7, :cond_1

    .line 2595532
    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v6, v7, v10}, LX/15i;->l(II)D

    move-result-wide v8

    invoke-virtual {v6, v7, v11}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-direct {v4, v8, v9, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2595533
    :goto_1
    if-eqz v0, :cond_0

    .line 2595534
    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1, v0, v10}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-virtual {v1, v0, v11}, LX/15i;->l(II)D

    move-result-wide v0

    invoke-direct {v2, v6, v7, v0, v1}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2595535
    :cond_0
    iget-object v0, p0, LX/IcS;->b:LX/IcT;

    iget-object v0, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/IcX;->a(LX/IcX;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;)V

    .line 2595536
    iget-object v0, p0, LX/IcS;->b:LX/IcT;

    iget-object v0, v0, LX/IcT;->a:LX/IcX;

    iget-object v1, p0, LX/IcS;->a:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/IcX;->b(LX/IcX;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;)V

    .line 2595537
    return-void

    .line 2595538
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2595539
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2595540
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_1
    move-object v4, v2

    goto :goto_1

    :cond_2
    move-object v3, v2

    goto :goto_0
.end method
