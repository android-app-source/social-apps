.class public LX/J8m;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2652139
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 4

    .prologue
    .line 2652140
    const-class v1, LX/J8m;

    monitor-enter v1

    .line 2652141
    :try_start_0
    sget-object v0, LX/J8m;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2652142
    sput-object v2, LX/J8m;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2652143
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2652144
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2652145
    invoke-static {v0}, LX/JCA;->a(LX/0QB;)LX/JCA;

    move-result-object v3

    check-cast v3, LX/JCA;

    invoke-static {v0}, LX/J9P;->b(LX/0QB;)LX/Amh;

    move-result-object p0

    check-cast p0, LX/Amh;

    invoke-static {v3, p0}, LX/J9M;->a(LX/JCA;LX/Amh;)LX/1qM;

    move-result-object v3

    move-object v0, v3

    .line 2652146
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2652147
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652148
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2652149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2652150
    invoke-static {p0}, LX/JCA;->a(LX/0QB;)LX/JCA;

    move-result-object v0

    check-cast v0, LX/JCA;

    invoke-static {p0}, LX/J9P;->b(LX/0QB;)LX/Amh;

    move-result-object v1

    check-cast v1, LX/Amh;

    invoke-static {v0, v1}, LX/J9M;->a(LX/JCA;LX/Amh;)LX/1qM;

    move-result-object v0

    return-object v0
.end method
