.class public final LX/ItT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/messaging/model/messages/Message;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ItV;


# direct methods
.method public constructor <init>(LX/ItV;)V
    .locals 0

    .prologue
    .line 2623375
    iput-object p1, p0, LX/ItT;->a:LX/ItV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2623367
    iget-object v0, p0, LX/ItT;->a:LX/ItV;

    .line 2623368
    iget-object v1, v0, LX/ItV;->k:LX/ItU;

    sget-object p0, LX/ItU;->ABORTED:LX/ItU;

    if-ne v1, p0, :cond_0

    .line 2623369
    :goto_0
    return-void

    .line 2623370
    :cond_0
    sget-object v1, LX/ItU;->FAILED:LX/ItU;

    iput-object v1, v0, LX/ItV;->k:LX/ItU;

    .line 2623371
    iget-object v1, v0, LX/ItV;->h:LX/03V;

    const-string p0, "StartupRetryManager"

    const-string p1, "Failed to start send retry during startup."

    invoke-virtual {v1, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2623372
    check-cast p1, Ljava/util/LinkedHashMap;

    .line 2623373
    iget-object v0, p0, LX/ItT;->a:LX/ItV;

    invoke-virtual {v0, p1}, LX/ItV;->a(Ljava/util/LinkedHashMap;)V

    .line 2623374
    return-void
.end method
