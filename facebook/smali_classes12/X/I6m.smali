.class public final LX/I6m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2537915
    iput-object p1, p0, LX/I6m;->a:Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/notificationsettings/protocol/EventsNotificationSettingsGraphQLModels$FetchEventsNotificationSubscriptionLevelsModel$PossibleNotificationSubscriptionLevelsModel$EdgesModel;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2537908
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2537909
    iget-object v0, p0, LX/I6m;->a:Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->d:LX/I6k;

    .line 2537910
    iput-object p1, v0, LX/I6k;->a:Ljava/util/List;

    .line 2537911
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2537912
    :cond_0
    iget-object v0, p0, LX/I6m;->a:Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/events/notificationsettings/EventsNotificationSettingsFragment;->d:LX/I6k;

    .line 2537913
    iput-object p2, v0, LX/I6k;->b:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    .line 2537914
    return-void
.end method
