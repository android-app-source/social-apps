.class public LX/HbH;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final l:Lcom/facebook/resources/ui/FbTextView;

.field public final m:Lcom/facebook/resources/ui/FbTextView;

.field public final n:Landroid/widget/ImageView;

.field public final o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final p:Lcom/facebook/securitycheckup/inner/InertCheckBox;

.field public q:LX/HbD;

.field public r:LX/HbG;

.field public s:Landroid/content/Context;

.field public t:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2485314
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2485315
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485316
    const v0, 0x7f0d2bc5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbH;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2485317
    const v0, 0x7f0d2bc6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbH;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2485318
    const v0, 0x7f0d2bc1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/HbH;->n:Landroid/widget/ImageView;

    .line 2485319
    const v0, 0x7f0d2bc2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HbH;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2485320
    const v0, 0x7f0d2bc4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/inner/InertCheckBox;

    iput-object v0, p0, LX/HbH;->p:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    .line 2485321
    iput-object p2, p0, LX/HbH;->s:Landroid/content/Context;

    .line 2485322
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x27663aac

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2485269
    iget-object v2, p0, LX/HbH;->r:LX/HbG;

    iget-object v3, p0, LX/HbH;->r:LX/HbG;

    .line 2485270
    iget-boolean p1, v3, LX/HbG;->e:Z

    move v3, p1

    .line 2485271
    if-nez v3, :cond_1

    .line 2485272
    :goto_0
    iput-boolean v0, v2, LX/HbG;->e:Z

    .line 2485273
    iget-object v0, p0, LX/HbH;->p:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    iget-object v2, p0, LX/HbH;->r:LX/HbG;

    .line 2485274
    iget-boolean v3, v2, LX/HbG;->e:Z

    move v2, v3

    .line 2485275
    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    .line 2485276
    iget-object v0, p0, LX/HbH;->r:LX/HbG;

    .line 2485277
    iget-boolean v2, v0, LX/HbG;->e:Z

    move v0, v2

    .line 2485278
    if-eqz v0, :cond_2

    .line 2485279
    iget-object v0, p0, LX/HbH;->q:LX/HbD;

    const/4 p1, 0x0

    const/4 p0, 0x1

    .line 2485280
    sget-object v2, LX/HbC;->a:[I

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->k:LX/Hba;

    invoke-virtual {v3}, LX/Hba;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2485281
    :cond_0
    :goto_1
    const v0, 0x32d72ce

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2485282
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2485283
    :cond_2
    iget-object v0, p0, LX/HbH;->q:LX/HbD;

    const/4 p0, 0x1

    const/4 p1, 0x0

    .line 2485284
    sget-object v2, LX/HbC;->a:[I

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->k:LX/Hba;

    invoke-virtual {v3}, LX/Hba;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2485285
    :cond_3
    :goto_2
    goto :goto_1

    .line 2485286
    :pswitch_0
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->d()I

    move-result v2

    .line 2485287
    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, p0}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2485288
    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-eq v3, p0, :cond_4

    if-ne v2, p0, :cond_5

    .line 2485289
    :cond_4
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->i:Landroid/content/Context;

    const v4, 0x7f08363c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2485290
    :cond_5
    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-ne v2, v3, :cond_6

    .line 2485291
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->i:Landroid/content/Context;

    const v4, 0x7f08363b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485292
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    .line 2485293
    iput-boolean p0, v2, LX/HbE;->r:Z

    .line 2485294
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    invoke-virtual {v2, p0}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    goto :goto_1

    .line 2485295
    :cond_6
    if-le v2, p0, :cond_0

    .line 2485296
    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, v0, LX/HbD;->a:LX/HbE;

    iget-object v4, v4, LX/HbE;->i:Landroid/content/Context;

    const v5, 0x7f08363e

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p0, p1

    invoke-virtual {v4, v5, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2485297
    :pswitch_1
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->d()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 2485298
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485299
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 2485300
    :pswitch_2
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-boolean v2, v2, LX/HbE;->r:Z

    if-eqz v2, :cond_7

    .line 2485301
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    .line 2485302
    iput-boolean p1, v2, LX/HbE;->r:Z

    .line 2485303
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    invoke-virtual {v2, p1}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    .line 2485304
    :cond_7
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->d()I

    move-result v2

    .line 2485305
    if-nez v2, :cond_8

    .line 2485306
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2485307
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->i:Landroid/content/Context;

    const v4, 0x7f08363c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2485308
    :cond_8
    if-ne v2, p0, :cond_9

    .line 2485309
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->i:Landroid/content/Context;

    const v4, 0x7f08363c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2485310
    :cond_9
    iget-object v3, v0, LX/HbD;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, v0, LX/HbD;->a:LX/HbE;

    iget-object v4, v4, LX/HbE;->i:Landroid/content/Context;

    const v5, 0x7f08363e

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p0, p1

    invoke-virtual {v4, v5, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2485311
    :pswitch_3
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->d()I

    move-result v2

    if-nez v2, :cond_3

    .line 2485312
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, p1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485313
    iget-object v2, v0, LX/HbD;->a:LX/HbE;

    iget-object v2, v2, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
