.class public final LX/J9A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0L;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;)V
    .locals 1

    .prologue
    .line 2652511
    iput-object p1, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652512
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J9A;->b:Z

    return-void
.end method


# virtual methods
.method public final a(LX/2nf;)V
    .locals 2

    .prologue
    .line 2652513
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l()LX/3tK;

    move-result-object v1

    .line 2652514
    if-nez v1, :cond_1

    .line 2652515
    if-eqz p1, :cond_0

    .line 2652516
    invoke-interface {p1}, LX/2nf;->close()V

    .line 2652517
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 2652518
    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2652519
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2nf;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2652520
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    if-eqz v0, :cond_2

    .line 2652521
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2652522
    :cond_2
    iget-boolean v0, p0, LX/J9A;->b:Z

    if-eqz v0, :cond_4

    .line 2652523
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J9A;->b:Z

    .line 2652524
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    .line 2652525
    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    move-object v0, v1

    .line 2652526
    invoke-virtual {v0}, LX/J8y;->b()V

    .line 2652527
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->w()V

    .line 2652528
    :goto_1
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    .line 2652529
    iget-object v1, v0, LX/B0O;->n:LX/B0d;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/B0O;->n:LX/B0d;

    iget-boolean v1, v1, LX/B0d;->c:Z

    if-eqz v1, :cond_5

    :cond_3
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2652530
    if-nez v0, :cond_0

    .line 2652531
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    iget-object v1, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o()LX/J8x;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J8y;->c(LX/J8x;)V

    goto :goto_0

    .line 2652532
    :cond_4
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->v()V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2652533
    iget-boolean v0, p0, LX/J9A;->b:Z

    if-eqz v0, :cond_1

    .line 2652534
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    .line 2652535
    const/4 p0, 0x1

    iput-boolean p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l:Z

    .line 2652536
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    if-nez p0, :cond_4

    .line 2652537
    :cond_0
    :goto_0
    return-void

    .line 2652538
    :cond_1
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    if-eqz v0, :cond_2

    .line 2652539
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2652540
    :cond_2
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2652541
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    .line 2652542
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz p0, :cond_3

    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    if-nez p0, :cond_5

    .line 2652543
    :cond_3
    :goto_1
    goto :goto_0

    .line 2652544
    :cond_4
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    const p1, 0x7f0d1108

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 2652545
    const p1, 0x7f080039

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2652546
    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2652547
    :cond_5
    iget-object p1, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    iget-object p0, v0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0kb;

    invoke-virtual {p0}, LX/0kb;->d()Z

    move-result p0

    if-eqz p0, :cond_6

    sget-object p0, LX/DBa;->FETCH_TIMELINE_FAILED:LX/DBa;

    :goto_2
    invoke-virtual {p1, p0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_1

    :cond_6
    sget-object p0, LX/DBa;->NO_CONNECTION:LX/DBa;

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2652548
    iget-object v0, p0, LX/J9A;->a:Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l()LX/3tK;

    move-result-object v0

    .line 2652549
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2652550
    check-cast v0, LX/J9E;

    invoke-interface {v0, p1}, LX/J9E;->a(Z)V

    .line 2652551
    :cond_0
    return-void
.end method
