.class public LX/JFJ;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/JFM;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field private final e:Ljava/util/Locale;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;)V
    .locals 7

    const/16 v3, 0x41

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/JFJ;->e:Ljava/util/Locale;

    const/4 v3, 0x0

    iget-object v0, p3, LX/2wA;->a:Landroid/accounts/Account;

    move-object v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/2wA;->a:Landroid/accounts/Account;

    move-object v0, v0

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_0
    new-instance v0, Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v2, p0, LX/JFJ;->e:Ljava/util/Locale;

    iget-object v4, p7, LX/JF5;->b:Ljava/lang/String;

    iget v5, p7, LX/JF5;->c:I

    move-object v1, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/internal/PlacesParams;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, LX/JFJ;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;B)V
    .locals 0

    invoke-direct/range {p0 .. p7}, LX/JFJ;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.location.places.internal.IGooglePlacesService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/JFM;

    if-eqz p0, :cond_1

    check-cast v0, LX/JFM;

    goto :goto_0

    :cond_1
    new-instance v0, LX/JFN;

    invoke-direct {v0, p1}, LX/JFN;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.places.GeoDataApi"

    return-object v0
.end method

.method public final a(LX/JFx;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V
    .locals 6
    .param p3    # Lcom/google/android/gms/maps/model/LatLngBounds;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/gms/location/places/AutocompleteFilter;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const-string v0, "callback == null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_1

    const-string v1, ""

    :goto_0
    if-nez p4, :cond_0

    new-instance v0, LX/JEq;

    invoke-direct {v0}, LX/JEq;-><init>()V

    const/4 p2, 0x1

    const/4 v5, 0x0

    new-instance v2, Lcom/google/android/gms/location/places/AutocompleteFilter;

    new-array v3, p2, [Ljava/lang/Integer;

    iget v4, v0, LX/JEq;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, p2, v5, v3}, Lcom/google/android/gms/location/places/AutocompleteFilter;-><init>(IZLjava/util/List;)V

    move-object v3, v2

    :goto_1
    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/JFM;

    iget-object v4, p0, LX/JFJ;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-object v2, p3

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LX/JFM;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;LX/JFQ;)V

    return-void

    :cond_0
    move-object v3, p4

    goto :goto_1

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.places.internal.IGooglePlacesService"

    return-object v0
.end method
