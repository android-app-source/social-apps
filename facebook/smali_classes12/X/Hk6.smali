.class public LX/Hk6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/Hk6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hk6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/Hk6;->b:Ljava/lang/String;

    iput-boolean p2, p0, LX/Hk6;->c:Z

    return-void
.end method

.method public static a(Landroid/content/Context;LX/Hkn;)LX/Hk6;
    .locals 6

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get advertising info on main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p1, LX/Hkn;->b:Ljava/lang/String;

    invoke-static {v0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, LX/Hk6;

    iget-object v1, p1, LX/Hkn;->b:Ljava/lang/String;

    iget-boolean v2, p1, LX/Hkn;->c:Z

    invoke-direct {v0, v1, v2}, LX/Hk6;-><init>(Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 p1, 0x0

    const-string v0, "com.google.android.gms.common.GooglePlayServicesUtil"

    const-string v2, "isGooglePlayServicesAvailable"

    new-array v3, v5, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    aput-object v4, v3, p1

    invoke-static {v0, v2, v3}, LX/Hko;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_1
    move-object v0, v0

    if-nez v0, :cond_1

    invoke-static {p0}, LX/Hk6;->b(Landroid/content/Context;)LX/Hk6;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-array v2, v5, [Ljava/lang/Object;

    aput-object p0, v2, p1

    invoke-static {v1, v0, v2}, LX/Hko;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    const-string v0, "com.google.android.gms.ads.identifier.AdvertisingIdClient"

    const-string v2, "getAdvertisingIdInfo"

    new-array v3, v5, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    aput-object v4, v3, p1

    invoke-static {v0, v2, v3}, LX/Hko;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-nez v0, :cond_6

    move-object v0, v1

    goto :goto_1

    :cond_6
    new-array v2, v5, [Ljava/lang/Object;

    aput-object p0, v2, p1

    invoke-static {v1, v0, v2}, LX/Hko;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_7

    move-object v0, v1

    goto :goto_1

    :cond_7
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "getId"

    new-array v4, p1, [Ljava/lang/Class;

    invoke-static {v0, v3, v4}, LX/Hko;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "isLimitAdTrackingEnabled"

    new-array v5, p1, [Ljava/lang/Class;

    invoke-static {v3, v4, v5}, LX/Hko;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    if-eqz v0, :cond_8

    if-nez v3, :cond_9

    :cond_8
    move-object v0, v1

    goto :goto_1

    :cond_9
    new-array v1, p1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, LX/Hko;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-array v1, p1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, LX/Hko;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    new-instance v2, LX/Hk6;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v2, v0, v1}, LX/Hk6;-><init>(Ljava/lang/String;Z)V

    move-object v0, v2

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;)LX/Hk6;
    .locals 4

    new-instance v1, LX/Hk5;

    invoke-direct {v1}, LX/Hk5;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x1

    const v3, 0x2ed16c13

    invoke-static {p0, v0, v1, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v2, LX/Hk4;

    invoke-virtual {v1}, LX/Hk5;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {v2, v0}, LX/Hk4;-><init>(Landroid/os/IBinder;)V

    new-instance v0, LX/Hk6;

    invoke-virtual {v2}, LX/Hk4;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LX/Hk4;->b()Z

    move-result v2

    invoke-direct {v0, v3, v2}, LX/Hk6;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v2, 0x55a281e4

    invoke-static {p0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    :goto_0
    return-object v0

    :catch_0
    const v0, 0x78599cfe

    invoke-static {p0, v1, v0}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    const v2, 0x583f5316

    invoke-static {p0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    throw v0
.end method
