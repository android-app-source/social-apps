.class public final LX/ItF;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

.field public final synthetic d:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;Ljava/util/ArrayList;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V
    .locals 0

    .prologue
    .line 2622220
    iput-object p1, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, LX/ItF;->a:Lcom/facebook/messaging/model/messages/Message;

    iput-object p3, p0, LX/ItF;->b:Ljava/util/ArrayList;

    iput-object p4, p0, LX/ItF;->c:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2622221
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v1, p0, LX/ItF;->b:Ljava/util/ArrayList;

    const/4 v7, 0x0

    .line 2622222
    const/16 v5, 0x8

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "offline_threading_id"

    aput-object v8, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, LX/3QL;->d(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x2

    const-string v8, "batch_result"

    aput-object v8, v5, v6

    const/4 v6, 0x3

    const-string v8, "failure"

    aput-object v8, v5, v6

    const/4 v6, 0x4

    const-string v8, "error_type"

    aput-object v8, v5, v6

    const/4 v6, 0x5

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x6

    const-string v8, "error_message"

    aput-object v8, v5, v6

    const/4 v6, 0x7

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v5}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v8

    .line 2622223
    iget-object v5, v0, LX/3QL;->f:LX/2bC;

    const-string v6, "msg_send_batch_responded"

    move-object v9, v7

    move-object v10, v7

    move-object v11, v7

    invoke-virtual/range {v5 .. v11}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2622224
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v0, p0, LX/ItF;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622225
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622226
    :try_start_0
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2622227
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    iget-object v2, p0, LX/ItF;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 2622228
    iget-object v2, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v3, p0, LX/ItF;->c:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v4, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v4, v4, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-static {v2, v3, v4}, Lcom/facebook/messaging/send/client/SendMessageManager;->a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/send/PendingSendQueueKey;Ljava/util/Set;)V

    .line 2622229
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622230
    if-eqz v0, :cond_0

    .line 2622231
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, LX/ItF;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->t(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622232
    :goto_0
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622233
    :try_start_1
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    const/4 v2, 0x0

    .line 2622234
    iput-boolean v2, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    .line 2622235
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2622236
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2622237
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622238
    return-void

    .line 2622239
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2622240
    :cond_0
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, LX/ItF;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622241
    invoke-static {v0, p1, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622242
    goto :goto_0

    .line 2622243
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 10

    .prologue
    .line 2622244
    iget-object v0, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v1, p0, LX/ItF;->b:Ljava/util/ArrayList;

    const/4 v5, 0x0

    .line 2622245
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "offline_threading_id"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, LX/3QL;->d(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string v6, "batch_result"

    aput-object v6, v3, v4

    const/4 v4, 0x3

    const-string v6, "success"

    aput-object v6, v3, v4

    invoke-static {v3}, LX/29E;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 2622246
    iget-object v3, v0, LX/3QL;->f:LX/2bC;

    const-string v4, "msg_send_batch_responded"

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    invoke-virtual/range {v3 .. v9}, LX/2bC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2622247
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableList()Ljava/util/ArrayList;

    move-result-object v0

    .line 2622248
    iget-object v1, p0, LX/ItF;->d:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v2, p0, LX/ItF;->b:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/util/List;Ljava/util/List;)V

    .line 2622249
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2622250
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/ItF;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
