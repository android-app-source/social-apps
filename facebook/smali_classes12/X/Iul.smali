.class public LX/Iul;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements LX/Iua;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Uo;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0ad;

.field private final e:LX/0YZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2627290
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Iul;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Xl;LX/0ad;)V
    .locals 3
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Uo;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Xl;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627283
    new-instance v0, LX/Iuk;

    invoke-direct {v0, p0}, LX/Iuk;-><init>(LX/Iul;)V

    iput-object v0, p0, LX/Iul;->e:LX/0YZ;

    .line 2627284
    iput-object p1, p0, LX/Iul;->a:LX/0Uo;

    .line 2627285
    iput-object p2, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2627286
    iput-object p3, p0, LX/Iul;->b:LX/0Or;

    .line 2627287
    iput-object p5, p0, LX/Iul;->d:LX/0ad;

    .line 2627288
    invoke-interface {p4}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    iget-object v2, p0, LX/Iul;->e:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2627289
    return-void
.end method

.method public static a(LX/0QB;)LX/Iul;
    .locals 13

    .prologue
    .line 2627253
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2627254
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2627255
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2627256
    if-nez v1, :cond_0

    .line 2627257
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2627258
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2627259
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2627260
    sget-object v1, LX/Iul;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2627261
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2627262
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2627263
    :cond_1
    if-nez v1, :cond_4

    .line 2627264
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2627265
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2627266
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2627267
    new-instance v7, LX/Iul;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v8

    check-cast v8, LX/0Uo;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v10, 0x15e8

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v11

    check-cast v11, LX/0Xl;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v7 .. v12}, LX/Iul;-><init>(LX/0Uo;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Xl;LX/0ad;)V

    .line 2627268
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2627269
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2627270
    if-nez v1, :cond_2

    .line 2627271
    sget-object v0, LX/Iul;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iul;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2627272
    :goto_1
    if-eqz v0, :cond_3

    .line 2627273
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627274
    :goto_3
    check-cast v0, LX/Iul;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2627275
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2627276
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2627277
    :catchall_1
    move-exception v0

    .line 2627278
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2627279
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2627280
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2627281
    :cond_2
    :try_start_8
    sget-object v0, LX/Iul;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iul;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 2627291
    iget-object v0, p0, LX/Iul;->d:LX/0ad;

    sget-short v1, LX/IuZ;->b:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2627292
    sget-object v0, LX/Iuf;->UNSET:LX/Iuf;

    .line 2627293
    :goto_0
    return-object v0

    .line 2627294
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0

    .line 2627295
    iget-object v0, p0, LX/Iul;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, LX/2gS;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2627296
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2gS;->a:LX/0Tn;

    iget-wide v4, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-interface {v0, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627297
    sget-object v0, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    goto :goto_0

    .line 2627298
    :cond_1
    iget-object v0, p0, LX/Iul;->a:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2627299
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627300
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0

    .line 2627301
    :cond_2
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2gS;->a:LX/0Tn;

    invoke-interface {v0, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2627302
    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    iget-wide v0, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    .line 2627303
    sget-object v0, LX/Iuf;->SILENT:LX/Iuf;

    goto :goto_0

    .line 2627304
    :cond_3
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2627252
    const-string v0, "OtherDeviceActiveRule"

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2627250
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->a:LX/0Tn;

    invoke-interface {v0, v1, p1, p2}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627251
    return-void
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 2627249
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2gS;->a:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2627247
    iget-object v0, p0, LX/Iul;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/2gS;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2627248
    return-void
.end method
