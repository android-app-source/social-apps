.class public final LX/JHy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/3LW;


# direct methods
.method public constructor <init>(LX/3LW;)V
    .locals 0

    .prologue
    .line 2677288
    iput-object p1, p0, LX/JHy;->a:LX/3LW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x30a30a9b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677289
    iget-object v1, p0, LX/JHy;->a:LX/3LW;

    iget-object v1, v1, LX/3LW;->h:LX/3LQ;

    .line 2677290
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "nearby_friends_dive_bar_see_more_tapping"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "background_location"

    .line 2677291
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2677292
    move-object v2, v2

    .line 2677293
    iget-object v3, v1, LX/3LQ;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2677294
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2677295
    sget-object v2, LX/0ax;->dX:Ljava/lang/String;

    const-string v3, "divebar"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2677296
    iget-object v2, p0, LX/JHy;->a:LX/3LW;

    iget-object v2, v2, LX/3LW;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/JHy;->a:LX/3LW;

    iget-object v3, v3, LX/3LW;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2677297
    const v1, 0x724b05e4

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
