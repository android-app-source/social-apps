.class public final LX/HUV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2473044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2473045
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473046
    :goto_0
    return v1

    .line 2473047
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473048
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2473049
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2473050
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2473051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2473052
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2473053
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2473054
    :cond_2
    const-string v6, "issue"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2473055
    const/4 v5, 0x0

    .line 2473056
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_b

    .line 2473057
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473058
    :goto_2
    move v3, v5

    .line 2473059
    goto :goto_1

    .line 2473060
    :cond_3
    const-string v6, "issue_summary"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2473061
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2473062
    :cond_4
    const-string v6, "opinions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2473063
    invoke-static {p0, p1}, LX/HUU;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2473064
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2473065
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2473066
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2473067
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2473068
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2473069
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 2473070
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473071
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_a

    .line 2473072
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2473073
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2473074
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 2473075
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2473076
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2473077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_9

    .line 2473078
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_9

    .line 2473079
    const/4 v7, 0x0

    .line 2473080
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_f

    .line 2473081
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473082
    :goto_5
    move v6, v7

    .line 2473083
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2473084
    :cond_9
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2473085
    goto :goto_3

    .line 2473086
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2473087
    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 2473088
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_b
    move v3, v5

    goto :goto_3

    .line 2473089
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473090
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 2473091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2473092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2473093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 2473094
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2473095
    invoke-static {p0, p1}, LX/HUQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_6

    .line 2473096
    :cond_e
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2473097
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2473098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_f
    move v6, v7

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2473099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2473100
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2473101
    if-eqz v0, :cond_0

    .line 2473102
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473103
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2473104
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2473105
    if-eqz v0, :cond_4

    .line 2473106
    const-string v1, "issue"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473107
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2473108
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2473109
    if-eqz v1, :cond_3

    .line 2473110
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473111
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2473112
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 2473113
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2473114
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2473115
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2473116
    if-eqz v4, :cond_1

    .line 2473117
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473118
    invoke-static {p0, v4, p2}, LX/HUQ;->a(LX/15i;ILX/0nX;)V

    .line 2473119
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2473120
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2473121
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2473122
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2473123
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2473124
    if-eqz v0, :cond_5

    .line 2473125
    const-string v1, "issue_summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473126
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2473127
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2473128
    if-eqz v0, :cond_6

    .line 2473129
    const-string v1, "opinions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473130
    invoke-static {p0, v0, p2, p3}, LX/HUU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2473131
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2473132
    return-void
.end method
