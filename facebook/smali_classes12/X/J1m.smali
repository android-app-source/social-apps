.class public LX/J1m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public a:Z

.field public b:LX/IoE;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639231
    return-void
.end method

.method public static a(LX/0QB;)LX/J1m;
    .locals 1

    .prologue
    .line 2639224
    new-instance v0, LX/J1m;

    invoke-direct {v0}, LX/J1m;-><init>()V

    .line 2639225
    move-object v0, v0

    .line 2639226
    return-object v0
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2639232
    iget-boolean v0, p0, LX/J1m;->a:Z

    if-eqz v0, :cond_0

    .line 2639233
    :goto_0
    return-void

    .line 2639234
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x24

    if-le v0, v1, :cond_1

    .line 2639235
    iget-object v0, p0, LX/J1m;->b:LX/IoE;

    invoke-interface {v0}, LX/IoE;->b()V

    .line 2639236
    const/4 v2, 0x0

    .line 2639237
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J1m;->a:Z

    .line 2639238
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/J1m;->c:Ljava/lang/String;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2639239
    iput-boolean v2, p0, LX/J1m;->a:Z

    .line 2639240
    goto :goto_0

    .line 2639241
    :cond_1
    iget-object v0, p0, LX/J1m;->b:LX/IoE;

    invoke-interface {v0}, LX/IoE;->a()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2639227
    iget-boolean v0, p0, LX/J1m;->a:Z

    if-eqz v0, :cond_0

    .line 2639228
    :goto_0
    return-void

    .line 2639229
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/J1m;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2639223
    return-void
.end method
