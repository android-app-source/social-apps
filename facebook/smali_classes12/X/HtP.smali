.class public LX/HtP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0ip;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/HsK;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/feedattachment/minutiae/MinutiaeAttachment$Callback;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/IC3;

.field public final d:LX/HtQ;

.field public final e:LX/0Uh;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/Hsu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0il;LX/HqO;LX/HtQ;LX/IC3;LX/0Uh;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HqO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/feedattachment/minutiae/MinutiaeAttachment$Callback;",
            "LX/HtQ;",
            "LX/IC3;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516139
    new-instance v0, LX/HtN;

    invoke-direct {v0, p0}, LX/HtN;-><init>(LX/HtP;)V

    iput-object v0, p0, LX/HtP;->g:Landroid/view/View$OnClickListener;

    .line 2516140
    new-instance v0, LX/HtO;

    invoke-direct {v0, p0}, LX/HtO;-><init>(LX/HtP;)V

    iput-object v0, p0, LX/HtP;->h:LX/0TF;

    .line 2516141
    iput-object p3, p0, LX/HtP;->d:LX/HtQ;

    .line 2516142
    iput-object p4, p0, LX/HtP;->c:LX/IC3;

    .line 2516143
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    .line 2516144
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HtP;->b:Ljava/lang/ref/WeakReference;

    .line 2516145
    iput-object p5, p0, LX/HtP;->e:LX/0Uh;

    .line 2516146
    iput-object p6, p0, LX/HtP;->f:Ljava/util/concurrent/Executor;

    .line 2516147
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2516150
    iput-object p1, p0, LX/HtP;->k:Ljava/lang/String;

    .line 2516151
    iget-object v0, p0, LX/HtP;->i:LX/Hsu;

    .line 2516152
    move-object v0, v0

    .line 2516153
    check-cast v0, LX/Hsu;

    .line 2516154
    iget-object v1, v0, LX/Hsu;->a:Landroid/widget/FrameLayout;

    move-object v1, v1

    .line 2516155
    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2516156
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Hsu;->setLoadingIndicatorVisibility(Z)V

    .line 2516157
    const/4 p1, 0x0

    .line 2516158
    iget-object v1, p0, LX/HtP;->e:LX/0Uh;

    const/16 v2, 0x59

    invoke-virtual {v1, v2, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, p1

    .line 2516159
    :goto_0
    move v1, v1

    .line 2516160
    invoke-virtual {v0, v1}, LX/Hsu;->setShowRemoveButton(Z)V

    .line 2516161
    iget-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516162
    iget-object v1, p0, LX/HtP;->d:LX/HtQ;

    invoke-static {p0}, LX/HtP;->j(LX/HtP;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HtQ;->b(Ljava/util/List;)LX/HtM;

    move-result-object v1

    move-object v1, v1

    .line 2516163
    if-nez v1, :cond_3

    .line 2516164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot find a controller that supports the current composition"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2516165
    :goto_1
    move-object v0, v0

    .line 2516166
    iput-object v0, p0, LX/HtP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2516167
    iget-object v0, p0, LX/HtP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, LX/HtP;->h:LX/0TF;

    iget-object v2, p0, LX/HtP;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2516168
    return-void

    .line 2516169
    :cond_0
    iget-object v1, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2516170
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v1, p1

    goto :goto_0

    :cond_3
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-interface {v1, v0}, LX/HtM;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method private f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2516148
    iget-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516149
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static j(LX/HtP;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516131
    iget-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516132
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 2516133
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2516134
    iget-object v0, v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->k()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel$OpenGraphComposerPreviewModel;->a()LX/0Px;

    move-result-object v0

    .line 2516135
    :goto_0
    return-object v0

    .line 2516136
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2516137
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2516171
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2516126
    new-instance v0, LX/Hsu;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hsu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HtP;->i:LX/Hsu;

    .line 2516127
    iget-object v0, p0, LX/HtP;->i:LX/Hsu;

    iget-object v1, p0, LX/HtP;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516128
    iget-object v0, p0, LX/HtP;->i:LX/Hsu;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2516129
    invoke-direct {p0}, LX/HtP;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/HtP;->a(Ljava/lang/String;)V

    .line 2516130
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2516121
    iget-object v0, p0, LX/HtP;->i:LX/Hsu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HtP;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2516122
    :cond_0
    :goto_0
    return-void

    .line 2516123
    :cond_1
    invoke-direct {p0}, LX/HtP;->f()Ljava/lang/String;

    move-result-object v0

    .line 2516124
    iget-object v1, p0, LX/HtP;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2516125
    invoke-direct {p0, v0}, LX/HtP;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2516096
    iget-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516097
    invoke-static {p0}, LX/HtP;->j(LX/HtP;)LX/0Px;

    move-result-object v1

    .line 2516098
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HtP;->d:LX/HtQ;

    .line 2516099
    invoke-static {v0, v1}, LX/HtQ;->c(LX/HtQ;Ljava/util/List;)Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    move-result-object p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2516100
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2516112
    iget-object v0, p0, LX/HtP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2516113
    iget-object v0, p0, LX/HtP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2516114
    iput-object v2, p0, LX/HtP;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2516115
    :cond_0
    iget-object v0, p0, LX/HtP;->i:LX/Hsu;

    .line 2516116
    move-object v0, v0

    .line 2516117
    check-cast v0, LX/Hsu;

    invoke-virtual {v0, v2}, LX/Hsu;->setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516118
    iput-object v2, p0, LX/HtP;->i:LX/Hsu;

    .line 2516119
    iput-object v2, p0, LX/HtP;->k:Ljava/lang/String;

    .line 2516120
    return-void
.end method

.method public final c()V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2516101
    iget-object v0, p0, LX/HtP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516102
    iget-object v2, p0, LX/HtP;->c:LX/IC3;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2516103
    iget-object v4, v2, LX/IC3;->a:LX/0Zb;

    const-string v5, "minutiae_preview_removed"

    invoke-static {v5, v3, v1}, LX/IC3;->a(Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2516104
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2516105
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0ip;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-static {v0}, LX/2s1;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)LX/2s1;

    move-result-object v0

    const/4 v1, 0x1

    .line 2516106
    iput-boolean v1, v0, LX/2s1;->e:Z

    .line 2516107
    move-object v0, v0

    .line 2516108
    invoke-virtual {v0}, LX/2s1;->a()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    .line 2516109
    iget-object v0, p0, LX/HtP;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqO;

    .line 2516110
    iget-object v2, v0, LX/HqO;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v2, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2516111
    :cond_0
    return-void
.end method
