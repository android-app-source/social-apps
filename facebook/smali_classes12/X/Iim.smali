.class public LX/Iim;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/Iix;

.field public final d:LX/Iix;

.field public final e:LX/Iix;

.field public final f:I

.field public final g:I


# direct methods
.method public constructor <init>(LX/Iin;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2605063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605064
    iget-object v0, p1, LX/Iin;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2605065
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2605066
    iget-object v0, p1, LX/Iin;->c:LX/Iix;

    move-object v0, v0

    .line 2605067
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605068
    iget-object v0, p1, LX/Iin;->d:LX/Iix;

    move-object v0, v0

    .line 2605069
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605070
    iget-object v0, p1, LX/Iin;->e:LX/Iix;

    move-object v0, v0

    .line 2605071
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605072
    iget v0, p1, LX/Iin;->f:I

    move v0, v0

    .line 2605073
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2605074
    iget v0, p1, LX/Iin;->g:I

    move v0, v0

    .line 2605075
    if-eqz v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2605076
    iget-object v0, p1, LX/Iin;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2605077
    iput-object v0, p0, LX/Iim;->a:Ljava/lang/String;

    .line 2605078
    iget-object v0, p1, LX/Iin;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2605079
    iput-object v0, p0, LX/Iim;->b:Ljava/lang/String;

    .line 2605080
    iget-object v0, p1, LX/Iin;->c:LX/Iix;

    move-object v0, v0

    .line 2605081
    iput-object v0, p0, LX/Iim;->c:LX/Iix;

    .line 2605082
    iget-object v0, p1, LX/Iin;->d:LX/Iix;

    move-object v0, v0

    .line 2605083
    iput-object v0, p0, LX/Iim;->d:LX/Iix;

    .line 2605084
    iget-object v0, p1, LX/Iin;->e:LX/Iix;

    move-object v0, v0

    .line 2605085
    iput-object v0, p0, LX/Iim;->e:LX/Iix;

    .line 2605086
    iget v0, p1, LX/Iin;->f:I

    move v0, v0

    .line 2605087
    iput v0, p0, LX/Iim;->f:I

    .line 2605088
    iget v0, p1, LX/Iin;->g:I

    move v0, v0

    .line 2605089
    iput v0, p0, LX/Iim;->g:I

    .line 2605090
    return-void

    :cond_0
    move v0, v2

    .line 2605091
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2605092
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2605093
    goto :goto_2
.end method
