.class public final LX/Iib;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2604924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2604925
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604926
    :goto_0
    return v1

    .line 2604927
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2604928
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 2604929
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2604930
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2604931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2604932
    const-string v7, "cover_photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2604933
    invoke-static {p0, p1}, LX/Iia;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2604934
    :cond_2
    const-string v7, "description"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2604935
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2604936
    :cond_3
    const-string v7, "footer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2604937
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2604938
    :cond_4
    const-string v7, "steps"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2604939
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2604940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_5

    .line 2604941
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_5

    .line 2604942
    invoke-static {p0, p1}, LX/Joo;->b(LX/15w;LX/186;)I

    move-result v6

    .line 2604943
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2604944
    :cond_5
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2604945
    goto :goto_1

    .line 2604946
    :cond_6
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2604947
    invoke-static {p0, p1}, LX/5Q6;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2604948
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2604949
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2604950
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2604951
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2604952
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2604953
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2604954
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2604955
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604956
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604957
    if-eqz v0, :cond_0

    .line 2604958
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604959
    invoke-static {p0, v0, p2}, LX/Iia;->a(LX/15i;ILX/0nX;)V

    .line 2604960
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604961
    if-eqz v0, :cond_1

    .line 2604962
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604963
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604964
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604965
    if-eqz v0, :cond_2

    .line 2604966
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604967
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604968
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604969
    if-eqz v0, :cond_4

    .line 2604970
    const-string v1, "steps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604971
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2604972
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2604973
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/Joo;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604974
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2604975
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2604976
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2604977
    if-eqz v0, :cond_5

    .line 2604978
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604979
    invoke-static {p0, v0, p2, p3}, LX/5Q6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604980
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604981
    return-void
.end method
