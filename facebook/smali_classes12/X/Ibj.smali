.class public final LX/Ibj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2594652
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2594653
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594654
    :goto_0
    return v1

    .line 2594655
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594656
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2594657
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2594658
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594659
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2594660
    const-string v3, "ride_providers"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2594661
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2594662
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2594663
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2594664
    const/4 v3, 0x0

    .line 2594665
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 2594666
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594667
    :goto_3
    move v2, v3

    .line 2594668
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2594669
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2594670
    goto :goto_1

    .line 2594671
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2594672
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2594673
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2594674
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594675
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2594676
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2594677
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594678
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2594679
    const-string v5, "ride_types"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2594680
    const/4 v4, 0x0

    .line 2594681
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_d

    .line 2594682
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594683
    :goto_5
    move v2, v4

    .line 2594684
    goto :goto_4

    .line 2594685
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2594686
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2594687
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    .line 2594688
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594689
    :cond_a
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_c

    .line 2594690
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2594691
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594692
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 2594693
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2594694
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2594695
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_b

    .line 2594696
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_b

    .line 2594697
    const/4 v6, 0x0

    .line 2594698
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_11

    .line 2594699
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594700
    :goto_8
    move v5, v6

    .line 2594701
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2594702
    :cond_b
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2594703
    goto :goto_6

    .line 2594704
    :cond_c
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2594705
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 2594706
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_d
    move v2, v4

    goto :goto_6

    .line 2594707
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594708
    :cond_f
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_10

    .line 2594709
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2594710
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594711
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_f

    if-eqz v7, :cond_f

    .line 2594712
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2594713
    invoke-static {p0, p1}, LX/Ibi;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_9

    .line 2594714
    :cond_10
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2594715
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2594716
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_8

    :cond_11
    move v5, v6

    goto :goto_9
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2594717
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594718
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2594719
    if-eqz v0, :cond_5

    .line 2594720
    const-string v1, "ride_providers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594721
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2594722
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2594723
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2594724
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594725
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2594726
    if-eqz v3, :cond_3

    .line 2594727
    const-string v4, "ride_types"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594728
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594729
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2594730
    if-eqz v4, :cond_2

    .line 2594731
    const-string v5, "edges"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594732
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2594733
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {p0, v4}, LX/15i;->c(I)I

    move-result p1

    if-ge v5, p1, :cond_1

    .line 2594734
    invoke-virtual {p0, v4, v5}, LX/15i;->q(II)I

    move-result p1

    .line 2594735
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2594736
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2594737
    if-eqz v2, :cond_0

    .line 2594738
    const-string v3, "node"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2594739
    invoke-static {p0, v2, p2}, LX/Ibi;->a(LX/15i;ILX/0nX;)V

    .line 2594740
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594741
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2594742
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2594743
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594744
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594745
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2594746
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2594747
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2594748
    return-void
.end method
