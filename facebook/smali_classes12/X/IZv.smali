.class public LX/IZv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field private final b:LX/0tX;

.field private final c:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2590417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590418
    iput-object p1, p0, LX/IZv;->a:LX/03V;

    .line 2590419
    iput-object p2, p0, LX/IZv;->b:LX/0tX;

    .line 2590420
    iput-object p3, p0, LX/IZv;->c:LX/1Ck;

    .line 2590421
    return-void
.end method

.method public static b(LX/0QB;)LX/IZv;
    .locals 4

    .prologue
    .line 2590422
    new-instance v3, LX/IZv;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v0, v1, v2}, LX/IZv;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 2590423
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2590424
    iget-object v0, p0, LX/IZv;->c:LX/1Ck;

    const-string v1, "task_key_fetch_native_sign_up_model"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2590425
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;LX/IZu;)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "loadSignUpData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2590426
    new-instance v0, LX/IZd;

    invoke-direct {v0}, LX/IZd;-><init>()V

    move-object v0, v0

    .line 2590427
    const-string v1, "provider_id"

    .line 2590428
    iget-object v2, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2590429
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2590430
    iget-object v1, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2590431
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2590432
    const-string v1, "promo_data"

    .line 2590433
    iget-object v2, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2590434
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2590435
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2590436
    iget-object v1, p0, LX/IZv;->c:LX/1Ck;

    const-string v2, "task_key_fetch_native_sign_up_model"

    iget-object v3, p0, LX/IZv;->b:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2590437
    new-instance v3, LX/IZs;

    invoke-direct {v3, p0, p2}, LX/IZs;-><init>(LX/IZv;LX/IZu;)V

    move-object v3, v3

    .line 2590438
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2590439
    return-void
.end method
