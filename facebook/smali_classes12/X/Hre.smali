.class public LX/Hre;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ik;
.implements LX/0il;
.implements LX/0im;
.implements LX/0in;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0ik",
        "<",
        "Lcom/facebook/composer/system/api/ComposerDerivedData;",
        ">;",
        "LX/0il",
        "<",
        "Lcom/facebook/composer/system/api/ComposerModel;",
        ">;",
        "LX/0im",
        "<",
        "Lcom/facebook/composer/system/api/ComposerMutation;",
        ">;",
        "LX/0in",
        "<",
        "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
        "<",
        "Lcom/facebook/composer/system/api/ComposerModel;",
        "Lcom/facebook/composer/system/api/ComposerDerivedData;",
        "Lcom/facebook/composer/system/api/ComposerMutation;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/HvN;


# direct methods
.method public constructor <init>(LX/HvN;)V
    .locals 0

    .prologue
    .line 2513423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513424
    iput-object p1, p0, LX/Hre;->a:LX/HvN;

    .line 2513425
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2513422
    iget-object v0, p0, LX/Hre;->a:LX/HvN;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    return-object v0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2513418
    invoke-virtual {p0}, LX/Hre;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0jJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
            "<",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2513421
    iget-object v0, p0, LX/Hre;->a:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2513420
    iget-object v0, p0, LX/Hre;->a:LX/HvN;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    return-object v0
.end method

.method public final e()Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 1

    .prologue
    .line 2513419
    iget-object v0, p0, LX/Hre;->a:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    return-object v0
.end method
