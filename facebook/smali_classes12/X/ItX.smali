.class public LX/ItX;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2623472
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2623473
    return-void
.end method

.method public static a(LX/01T;LX/0Or;)LX/0TP;
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsSendQueuesPriorityUrgentEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/send/service/SendQueue;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/0TP;"
        }
    .end annotation

    .prologue
    .line 2623469
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623470
    sget-object v0, LX/0TP;->URGENT:LX/0TP;

    .line 2623471
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0TP;->NORMAL:LX/0TP;

    goto :goto_0
.end method

.method public static a(LX/Itg;LX/2OQ;)LX/Itf;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .prologue
    .line 2623468
    invoke-virtual {p0, p1}, LX/Itg;->a(LX/2OQ;)LX/Itf;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/01T;LX/0Or;)LX/0TP;
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsSendQueuesPriorityUrgentEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/send/service/PendingSendQueue;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "LX/0TP;"
        }
    .end annotation

    .prologue
    .line 2623463
    sget-object v0, LX/01T;->MESSENGER:LX/01T;

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623464
    sget-object v0, LX/0TP;->URGENT:LX/0TP;

    .line 2623465
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/0TP;->NORMAL:LX/0TP;

    goto :goto_0
.end method

.method public static b(LX/Itg;LX/2OQ;)LX/Itf;
    .locals 1
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ProviderUsage"
        }
    .end annotation

    .annotation runtime Lcom/facebook/auth/userscope/UserScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .prologue
    .line 2623467
    invoke-virtual {p0, p1}, LX/Itg;->a(LX/2OQ;)LX/Itf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2623466
    return-void
.end method
