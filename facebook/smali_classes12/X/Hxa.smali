.class public final LX/Hxa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 0

    .prologue
    .line 2522747
    iput-object p1, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 2522748
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->t(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V

    .line 2522749
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 2522750
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2522751
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->G:I

    if-nez v0, :cond_1

    .line 2522752
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->F:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 2522753
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2522754
    iput p2, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->G:I

    .line 2522755
    return-void

    .line 2522756
    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->F:LX/195;

    .line 2522757
    iget-boolean p1, v0, LX/195;->n:Z

    move v0, p1

    .line 2522758
    if-eqz v0, :cond_0

    .line 2522759
    iget-object v0, p0, LX/Hxa;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->F:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    goto :goto_0
.end method
