.class public LX/JVB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/7j6;


# direct methods
.method public constructor <init>(LX/7j6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700322
    iput-object p1, p0, LX/JVB;->a:LX/7j6;

    .line 2700323
    return-void
.end method

.method public static a(LX/0QB;)LX/JVB;
    .locals 4

    .prologue
    .line 2700324
    const-class v1, LX/JVB;

    monitor-enter v1

    .line 2700325
    :try_start_0
    sget-object v0, LX/JVB;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700326
    sput-object v2, LX/JVB;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700329
    new-instance p0, LX/JVB;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v3

    check-cast v3, LX/7j6;

    invoke-direct {p0, v3}, LX/JVB;-><init>(LX/7j6;)V

    .line 2700330
    move-object v0, p0

    .line 2700331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
