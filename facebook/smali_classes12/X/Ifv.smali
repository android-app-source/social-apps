.class public abstract LX/Ifv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ifr;
.implements LX/Ifs;
.implements LX/Ift;
.implements LX/Ifu;


# static fields
.field public static a:I


# instance fields
.field public final b:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifs;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "Lcom/facebook/messaging/livelocation/model/LiveLocationModel$OnOfflineThreadingIdOfLastCancelledMessageChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifx;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifr;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ift;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifu;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0kD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0kD",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/Ig6;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/Ig6;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "LX/Ig7;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Ljava/lang/String;",
            "LX/Ig6;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljava/lang/String;

.field public m:I

.field private n:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600219
    const/4 v0, -0x1

    sput v0, LX/Ifv;->a:I

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 1

    .prologue
    .line 2600204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600205
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->b:LX/0mY;

    .line 2600206
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->c:LX/0mY;

    .line 2600207
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->d:LX/0mY;

    .line 2600208
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->e:LX/0mY;

    .line 2600209
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->f:LX/0mY;

    .line 2600210
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ifv;->g:LX/0mY;

    .line 2600211
    invoke-static {}, LX/0kA;->f()LX/0kA;

    move-result-object v0

    iput-object v0, p0, LX/Ifv;->h:LX/0kD;

    .line 2600212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ifv;->i:Ljava/util/HashMap;

    .line 2600213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ifv;->j:Ljava/util/Map;

    .line 2600214
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/Ifv;->k:LX/0Ri;

    .line 2600215
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ifv;->l:Ljava/lang/String;

    .line 2600216
    sget v0, LX/Ifv;->a:I

    iput v0, p0, LX/Ifv;->m:I

    .line 2600217
    iput-object p1, p0, LX/Ifv;->n:LX/0SG;

    .line 2600218
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/Ig6;
    .locals 2

    .prologue
    .line 2600197
    iget-object v0, p0, LX/Ifv;->h:LX/0kD;

    invoke-interface {v0, p1, p2}, LX/0kD;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig6;

    .line 2600198
    if-nez v0, :cond_0

    .line 2600199
    new-instance v0, LX/Ig6;

    iget-object v1, p0, LX/Ifv;->n:LX/0SG;

    invoke-direct {v0, v1, p1, p2}, LX/Ig6;-><init>(LX/0SG;Lcom/facebook/user/model/UserKey;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2600200
    iget-object v1, p0, LX/Ifv;->h:LX/0kD;

    invoke-interface {v1, p1, p2, v0}, LX/0kD;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2600201
    iget-object v1, v0, LX/Ig6;->a:LX/0mY;

    invoke-virtual {v1, p0}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600202
    iget-object v1, v0, LX/Ig6;->c:LX/0mY;

    invoke-virtual {v1, p0}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600203
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/facebook/user/model/UserKey;)LX/Ig7;
    .locals 2

    .prologue
    .line 2600152
    iget-object v0, p0, LX/Ifv;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig7;

    .line 2600153
    if-nez v0, :cond_0

    .line 2600154
    new-instance v0, LX/Ig7;

    invoke-direct {v0, p1}, LX/Ig7;-><init>(Lcom/facebook/user/model/UserKey;)V

    .line 2600155
    iget-object v1, p0, LX/Ifv;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2600156
    iget-object v1, v0, LX/Ig7;->a:LX/0mY;

    invoke-virtual {v1, p0}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600157
    :cond_0
    return-object v0
.end method

.method public final a(LX/Ifx;)V
    .locals 1

    .prologue
    .line 2600195
    iget-object v0, p0, LX/Ifv;->d:LX/0mY;

    invoke-virtual {v0, p1}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600196
    return-void
.end method

.method public final a(LX/Ig6;)V
    .locals 3

    .prologue
    .line 2600186
    iget-object v0, p1, LX/Ig6;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2600187
    if-eqz v0, :cond_0

    .line 2600188
    iget-object v1, p0, LX/Ifv;->k:LX/0Ri;

    invoke-interface {v1, v0, p1}, LX/0Ri;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2600189
    :cond_0
    iget-object v0, p0, LX/Ifv;->f:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 2600190
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600191
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ift;

    invoke-interface {v0, p1}, LX/Ift;->a(LX/Ig6;)V

    .line 2600192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2600193
    :cond_1
    iget-object v0, p0, LX/Ifv;->f:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600194
    return-void
.end method

.method public final a(LX/Ig7;)V
    .locals 3

    .prologue
    .line 2600180
    iget-object v0, p0, LX/Ifv;->g:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 2600181
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2600182
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifu;

    invoke-interface {v0, p1}, LX/Ifu;->a(LX/Ig7;)V

    .line 2600183
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2600184
    :cond_0
    iget-object v0, p0, LX/Ifv;->g:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600185
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2600170
    iget-object v0, p0, LX/Ifv;->l:Ljava/lang/String;

    invoke-static {v0, p1}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600171
    :goto_0
    return-void

    .line 2600172
    :cond_0
    iput-object p1, p0, LX/Ifv;->l:Ljava/lang/String;

    .line 2600173
    iget-object v0, p0, LX/Ifv;->c:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object p1

    .line 2600174
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600175
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifz;

    .line 2600176
    invoke-static {v0}, LX/Ifz;->d(LX/Ifz;)V

    .line 2600177
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2600178
    :cond_1
    iget-object v0, p0, LX/Ifv;->c:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600179
    goto :goto_0
.end method

.method public final b(LX/Ig6;)V
    .locals 3

    .prologue
    .line 2600164
    iget-object v0, p0, LX/Ifv;->e:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 2600165
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2600166
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifr;

    invoke-interface {v0, p1}, LX/Ifr;->b(LX/Ig6;)V

    .line 2600167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2600168
    :cond_0
    iget-object v0, p0, LX/Ifv;->e:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600169
    return-void
.end method

.method public final c(LX/Ig6;)V
    .locals 3

    .prologue
    .line 2600158
    iget-object v0, p0, LX/Ifv;->b:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v2

    .line 2600159
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2600160
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifs;

    invoke-interface {v0, p1}, LX/Ifs;->c(LX/Ig6;)V

    .line 2600161
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2600162
    :cond_0
    iget-object v0, p0, LX/Ifv;->b:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600163
    return-void
.end method
