.class public LX/IC8;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IC9;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IC8",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IC9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2548872
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2548873
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/IC8;->b:LX/0Zi;

    .line 2548874
    iput-object p1, p0, LX/IC8;->a:LX/0Ot;

    .line 2548875
    return-void
.end method

.method public static a(LX/0QB;)LX/IC8;
    .locals 4

    .prologue
    .line 2548878
    const-class v1, LX/IC8;

    monitor-enter v1

    .line 2548879
    :try_start_0
    sget-object v0, LX/IC8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2548880
    sput-object v2, LX/IC8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2548881
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2548882
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2548883
    new-instance v3, LX/IC8;

    const/16 p0, 0x2168

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IC8;-><init>(LX/0Ot;)V

    .line 2548884
    move-object v0, v3

    .line 2548885
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2548886
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IC8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2548887
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2548888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2548889
    check-cast p2, LX/IC7;

    .line 2548890
    iget-object v0, p0, LX/IC8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IC9;

    iget-object v1, p2, LX/IC7;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/IC7;->b:LX/1Pf;

    const/4 p0, 0x4

    const/4 v7, 0x2

    .line 2548891
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2548892
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2548893
    invoke-static {v3}, LX/2vB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2548894
    if-nez v3, :cond_0

    .line 2548895
    const/4 v3, 0x0

    .line 2548896
    :goto_0
    move-object v0, v3

    .line 2548897
    return-object v0

    .line 2548898
    :cond_0
    iget-object v4, v0, LX/IC9;->b:LX/ICG;

    const/4 v5, 0x0

    .line 2548899
    new-instance v6, LX/ICF;

    invoke-direct {v6, v4}, LX/ICF;-><init>(LX/ICG;)V

    .line 2548900
    iget-object p2, v4, LX/ICG;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ICE;

    .line 2548901
    if-nez p2, :cond_1

    .line 2548902
    new-instance p2, LX/ICE;

    invoke-direct {p2, v4}, LX/ICE;-><init>(LX/ICG;)V

    .line 2548903
    :cond_1
    invoke-static {p2, p1, v5, v5, v6}, LX/ICE;->a$redex0(LX/ICE;LX/1De;IILX/ICF;)V

    .line 2548904
    move-object v6, p2

    .line 2548905
    move-object v5, v6

    .line 2548906
    move-object v4, v5

    .line 2548907
    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 2548908
    iget-object v6, v4, LX/ICE;->a:LX/ICF;

    iput-object v5, v6, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2548909
    iget-object v6, v4, LX/ICE;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {v6, p2}, Ljava/util/BitSet;->set(I)V

    .line 2548910
    move-object v4, v4

    .line 2548911
    iget-object v5, v4, LX/ICE;->a:LX/ICF;

    iput-object v1, v5, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2548912
    iget-object v5, v4, LX/ICE;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2548913
    move-object v4, v4

    .line 2548914
    iget-object v5, v4, LX/ICE;->a:LX/ICF;

    iput-object v2, v5, LX/ICF;->e:LX/1Po;

    .line 2548915
    iget-object v5, v4, LX/ICE;->e:Ljava/util/BitSet;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2548916
    move-object v4, v4

    .line 2548917
    const v5, 0x7f082516

    .line 2548918
    iget-object v6, v4, LX/ICE;->a:LX/ICF;

    invoke-virtual {v4, v5}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v6, LX/ICF;->c:Ljava/lang/CharSequence;

    .line 2548919
    iget-object v6, v4, LX/ICE;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {v6, p2}, Ljava/util/BitSet;->set(I)V

    .line 2548920
    move-object v4, v4

    .line 2548921
    const v5, 0x7f082515

    .line 2548922
    iget-object v6, v4, LX/ICE;->a:LX/ICF;

    invoke-virtual {v4, v5}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, v6, LX/ICF;->d:Ljava/lang/CharSequence;

    .line 2548923
    iget-object v6, v4, LX/ICE;->e:Ljava/util/BitSet;

    const/4 p2, 0x3

    invoke-virtual {v6, p2}, Ljava/util/BitSet;->set(I)V

    .line 2548924
    move-object v4, v4

    .line 2548925
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x3eb33333    # 0.35f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    .line 2548926
    iget-object v5, v0, LX/IC9;->a:LX/ICC;

    const/4 v6, 0x0

    .line 2548927
    new-instance p2, LX/ICB;

    invoke-direct {p2, v5}, LX/ICB;-><init>(LX/ICC;)V

    .line 2548928
    iget-object v0, v5, LX/ICC;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ICA;

    .line 2548929
    if-nez v0, :cond_2

    .line 2548930
    new-instance v0, LX/ICA;

    invoke-direct {v0, v5}, LX/ICA;-><init>(LX/ICC;)V

    .line 2548931
    :cond_2
    invoke-static {v0, p1, v6, v6, p2}, LX/ICA;->a$redex0(LX/ICA;LX/1De;IILX/ICB;)V

    .line 2548932
    move-object p2, v0

    .line 2548933
    move-object v6, p2

    .line 2548934
    move-object v5, v6

    .line 2548935
    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 2548936
    iget-object p2, v5, LX/ICA;->a:LX/ICB;

    iput-object v6, p2, LX/ICB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2548937
    iget-object p2, v5, LX/ICA;->e:Ljava/util/BitSet;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->set(I)V

    .line 2548938
    move-object v5, v5

    .line 2548939
    iget-object v6, v5, LX/ICA;->a:LX/ICB;

    iput-object v2, v6, LX/ICB;->b:LX/1Po;

    .line 2548940
    iget-object v6, v5, LX/ICA;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {v6, p2}, Ljava/util/BitSet;->set(I)V

    .line 2548941
    move-object v5, v5

    .line 2548942
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x3f266666    # 0.65f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    .line 2548943
    const v6, -0x3625f733

    invoke-static {v3, v6}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2548944
    const/4 v3, 0x0

    const v6, 0x7f0b0062

    invoke-interface {v5, v3, v6}, LX/1Di;->c(II)LX/1Di;

    .line 2548945
    :cond_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x3

    invoke-interface {v3, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v3, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2548876
    invoke-static {}, LX/1dS;->b()V

    .line 2548877
    const/4 v0, 0x0

    return-object v0
.end method
