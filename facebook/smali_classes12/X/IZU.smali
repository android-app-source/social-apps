.class public final LX/IZU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IZR;

.field public final synthetic b:Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;LX/IZR;)V
    .locals 0

    .prologue
    .line 2589341
    iput-object p1, p0, LX/IZU;->b:Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;

    iput-object p2, p0, LX/IZU;->a:LX/IZR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x5189aac5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2589342
    iget-object v1, p0, LX/IZU;->b:Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;

    iget-object v1, v1, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->c:LX/IZV;

    if-eqz v1, :cond_0

    .line 2589343
    iget-object v1, p0, LX/IZU;->b:Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;

    iget-object v2, p0, LX/IZU;->a:LX/IZR;

    const/4 v5, 0x0

    .line 2589344
    move v4, v5

    .line 2589345
    :goto_0
    iget-object v6, v1, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v4, v6, :cond_4

    .line 2589346
    iget-object v6, v1, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v5

    move v6, v4

    :goto_1
    if-ge v7, v8, :cond_3

    iget-object v4, v1, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IZR;

    .line 2589347
    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2589348
    :cond_0
    :goto_2
    iget-object v1, p0, LX/IZU;->a:LX/IZR;

    .line 2589349
    iget-object v2, v1, LX/IZR;->f:Landroid/view/View$OnClickListener;

    move-object v1, v2

    .line 2589350
    if-eqz v1, :cond_1

    .line 2589351
    iget-object v1, p0, LX/IZU;->a:LX/IZR;

    .line 2589352
    iget-object v2, v1, LX/IZR;->f:Landroid/view/View$OnClickListener;

    move-object v1, v2

    .line 2589353
    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2589354
    :cond_1
    const v1, 0x2e663a20

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2589355
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 2589356
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_1

    :cond_3
    move v4, v6

    goto :goto_0

    .line 2589357
    :cond_4
    goto :goto_2
.end method
