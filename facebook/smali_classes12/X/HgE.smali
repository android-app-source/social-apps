.class public LX/HgE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2492801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492802
    iput p1, p0, LX/HgE;->a:I

    .line 2492803
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2492804
    invoke-static {}, LX/HgP;->a()LX/HgO;

    move-result-object v0

    .line 2492805
    const-string v1, "group_order"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2492806
    const-string v1, "limit"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2492807
    const-string v1, "from_page_cursor"

    .line 2492808
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2492809
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2492810
    const-string v1, "iconWidth"

    iget v2, p0, LX/HgE;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2492811
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2492812
    invoke-static {p1}, LX/HfW;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;

    move-result-object v1

    .line 2492813
    invoke-static {v1}, LX/HfW;->a(Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2492814
    const/4 v0, 0x0

    .line 2492815
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    new-instance v0, LX/5Mb;

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/FetchUserGroupsQueryModels$FetchUserGroupsQueryModel$AccountUserModel$GroupsModel;->a()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, v2, v3}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    goto :goto_0
.end method
