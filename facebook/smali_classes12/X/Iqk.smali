.class public LX/Iqk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field private static final a:LX/0wT;


# instance fields
.field public final b:LX/Iqg;

.field public final c:Landroid/view/View;

.field public final d:Landroid/graphics/Matrix;

.field public final e:LX/0wd;

.field private final f:LX/Iqq;

.field public g:Z

.field public h:Z

.field public i:F

.field public j:Z

.field public k:Z

.field public l:LX/Irk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2617221
    new-instance v0, LX/0wT;

    const-wide/high16 v2, 0x4069000000000000L    # 200.0

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    invoke-direct {v0, v2, v3, v4, v5}, LX/0wT;-><init>(DD)V

    sput-object v0, LX/Iqk;->a:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/Iqg;Landroid/view/View;LX/0wW;)V
    .locals 3

    .prologue
    .line 2617224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617225
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/Iqk;->d:Landroid/graphics/Matrix;

    .line 2617226
    new-instance v0, LX/Iqr;

    invoke-direct {v0, p0}, LX/Iqr;-><init>(LX/Iqk;)V

    iput-object v0, p0, LX/Iqk;->f:LX/Iqq;

    .line 2617227
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Iqk;->g:Z

    .line 2617228
    iput-object p1, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617229
    iput-object p2, p0, LX/Iqk;->c:Landroid/view/View;

    .line 2617230
    invoke-virtual {p3}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/Iqk;->a:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v1, LX/Iqt;

    invoke-direct {v1, p0}, LX/Iqt;-><init>(LX/Iqk;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Iqk;->e:LX/0wd;

    .line 2617231
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2617222
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    iget-object v1, p0, LX/Iqk;->f:LX/Iqq;

    invoke-virtual {v0, v1}, LX/Iqg;->a(LX/Iqq;)V

    .line 2617223
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2617202
    instance-of v0, p1, LX/Iqn;

    if-nez v0, :cond_0

    .line 2617203
    :goto_0
    return-void

    .line 2617204
    :cond_0
    sget-object v0, LX/Iqs;->a:[I

    check-cast p1, LX/Iqn;

    invoke-virtual {p1}, LX/Iqn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2617205
    :pswitch_0
    invoke-virtual {p0}, LX/Iqk;->p()V

    goto :goto_0

    .line 2617206
    :pswitch_1
    invoke-virtual {p0}, LX/Iqk;->q()V

    goto :goto_0

    .line 2617207
    :pswitch_2
    invoke-virtual {p0}, LX/Iqk;->r()V

    goto :goto_0

    .line 2617208
    :pswitch_3
    iget-object v4, p0, LX/Iqk;->e:LX/0wd;

    iget-object v2, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617209
    iget-boolean v3, v2, LX/Iqg;->f:Z

    move v2, v3

    .line 2617210
    if-eqz v2, :cond_1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    :goto_1
    invoke-virtual {v4, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2617211
    goto :goto_0

    .line 2617212
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public e()V
    .locals 2

    .prologue
    .line 2617218
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    iget-object v1, p0, LX/Iqk;->f:LX/Iqq;

    .line 2617219
    iget-object p0, v0, LX/Iqg;->a:LX/IrC;

    invoke-virtual {p0, v1}, LX/IrC;->b(LX/Iqq;)Z

    .line 2617220
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 2617217
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 2617216
    return-void
.end method

.method public k()F
    .locals 1

    .prologue
    .line 2617213
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617214
    iget p0, v0, LX/Iqg;->b:F

    move v0, p0

    .line 2617215
    return v0
.end method

.method public l()F
    .locals 1

    .prologue
    .line 2617232
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617233
    iget p0, v0, LX/Iqg;->c:F

    move v0, p0

    .line 2617234
    return v0
.end method

.method public m()F
    .locals 2

    .prologue
    .line 2617177
    iget v0, p0, LX/Iqk;->i:F

    iget-object v1, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617178
    iget p0, v1, LX/Iqg;->e:F

    move v1, p0

    .line 2617179
    add-float/2addr v0, v1

    return v0
.end method

.method public n()F
    .locals 4

    .prologue
    .line 2617180
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617181
    iget v1, v0, LX/Iqg;->d:F

    move v0, v1

    .line 2617182
    iget-object v1, p0, LX/Iqk;->e:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 2617183
    neg-float v2, v0

    invoke-static {v0, v2, v1}, LX/Iqu;->a(FFF)F

    move-result v0

    return v0
.end method

.method public o()F
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 2617184
    iget-object v0, p0, LX/Iqk;->b:LX/Iqg;

    .line 2617185
    iget v1, v0, LX/Iqg;->d:F

    move v1, v1

    .line 2617186
    iget-object v0, p0, LX/Iqk;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v0, v2

    .line 2617187
    cmpg-float v2, v0, v4

    if-gtz v2, :cond_0

    :goto_0
    div-float/2addr v0, v4

    .line 2617188
    const v2, 0x3f99999a    # 1.2f

    mul-float/2addr v2, v1

    invoke-static {v1, v2, v0}, LX/Iqu;->a(FFF)F

    move-result v0

    return v0

    .line 2617189
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v0, v2, v0

    goto :goto_0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 2617190
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 2617191
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Iqk;->j:Z

    .line 2617192
    iget-object v0, p0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/Iqk;->k()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2617193
    iget-object v0, p0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/Iqk;->l()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2617194
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 2617195
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Iqk;->j:Z

    .line 2617196
    iget-object v0, p0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/Iqk;->m()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    .line 2617197
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 2617198
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Iqk;->j:Z

    .line 2617199
    iget-object v0, p0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/Iqk;->n()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 2617200
    iget-object v0, p0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {p0}, LX/Iqk;->o()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 2617201
    return-void
.end method
