.class public LX/JUY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUW;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2699262
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JUY;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699259
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699260
    iput-object p1, p0, LX/JUY;->b:LX/0Ot;

    .line 2699261
    return-void
.end method

.method public static a(LX/0QB;)LX/JUY;
    .locals 4

    .prologue
    .line 2699248
    const-class v1, LX/JUY;

    monitor-enter v1

    .line 2699249
    :try_start_0
    sget-object v0, LX/JUY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699250
    sput-object v2, LX/JUY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699251
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699252
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699253
    new-instance v3, LX/JUY;

    const/16 p0, 0x207c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUY;-><init>(LX/0Ot;)V

    .line 2699254
    move-object v0, v3

    .line 2699255
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699256
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699257
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2699242
    check-cast p2, LX/JUX;

    .line 2699243
    iget-object v0, p0, LX/JUY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsHeaderComponentSpec;

    iget-object v1, p2, LX/JUX;->a:LX/3Ab;

    const/4 p0, 0x2

    const/4 p2, 0x1

    .line 2699244
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b25c1

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b25c0

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsHeaderComponentSpec;->b:LX/3AZ;

    invoke-virtual {v3, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v3

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, p0}, LX/3Aa;->a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Aa;->h(I)LX/3Aa;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Aa;->m(I)LX/3Aa;

    move-result-object v3

    const p0, 0x7f0a00ab

    invoke-virtual {v3, p0}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v3

    const p0, 0x7f0b0050

    invoke-virtual {v3, p0}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 p0, 0x6

    const p2, 0x7f0b0060

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2699245
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2699246
    invoke-static {}, LX/1dS;->b()V

    .line 2699247
    const/4 v0, 0x0

    return-object v0
.end method
