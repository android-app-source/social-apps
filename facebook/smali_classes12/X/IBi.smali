.class public LX/IBi;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/IBX;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CK5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0SF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Lcom/facebook/events/model/Event;

.field private h:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2547473
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547474
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IBi;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v6

    check-cast v6, LX/IBl;

    invoke-static {v0}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object p1

    check-cast p1, LX/CK5;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SF;

    iput-object v3, v2, LX/IBi;->a:LX/0Zb;

    iput-object v4, v2, LX/IBi;->b:LX/0ad;

    iput-object v5, v2, LX/IBi;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, LX/IBi;->d:LX/IBl;

    iput-object p1, v2, LX/IBi;->e:LX/CK5;

    iput-object v0, v2, LX/IBi;->f:LX/0SF;

    .line 2547475
    invoke-virtual {p0, p0}, LX/IBi;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547476
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 6
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547477
    iput-object p1, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    .line 2547478
    iput-object p3, p0, LX/IBi;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2547479
    invoke-virtual {p0}, LX/IBi;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081f67

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2547480
    invoke-virtual {p0}, LX/IBi;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081f68

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/IBi;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2547481
    iget-object v2, p0, LX/IBi;->d:LX/IBl;

    invoke-virtual {v2, p0, v0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 2547482
    iget-object v0, p0, LX/IBi;->d:LX/IBl;

    const v1, 0x7f020339

    invoke-virtual {v0, p0, v1, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547483
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 10
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2547484
    iget-object v1, p0, LX/IBi;->b:LX/0ad;

    sget v2, LX/347;->L:I

    const/16 v3, 0x78

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    .line 2547485
    iget-object v2, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v2

    .line 2547486
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->R()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IBi;->e:LX/CK5;

    invoke-virtual {v2}, LX/CK5;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IBi;->b:LX/0ad;

    sget-short v3, LX/347;->K:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    int-to-long v2, v1

    .line 2547487
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    mul-long/2addr v6, v2

    sub-long v6, v4, v6

    .line 2547488
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v4

    if-nez v4, :cond_1

    const/16 v4, 0xb4

    invoke-static {v4}, LX/1m9;->a(I)J

    move-result-wide v4

    add-long/2addr v4, v6

    .line 2547489
    :goto_0
    iget-object v8, p0, LX/IBi;->f:LX/0SF;

    invoke-virtual {v8}, LX/0SF;->a()J

    move-result-wide v8

    .line 2547490
    cmp-long v6, v8, v6

    if-ltz v6, :cond_2

    cmp-long v4, v8, v4

    if-gtz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    move v1, v4

    .line 2547491
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 2547492
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v4

    goto :goto_0

    .line 2547493
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x68edf00c

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2547494
    iget-object v0, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    .line 2547495
    iget-object v2, v0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v2

    .line 2547496
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2547497
    const/4 v2, 0x0

    .line 2547498
    :goto_0
    move-object v0, v2

    .line 2547499
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    .line 2547500
    iget-object v2, v0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v2

    .line 2547501
    :cond_0
    iget-object v2, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->S()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-object v3, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->T()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "events_summary_view_order_ride"

    invoke-static {v0, v2, v3, v4, v5}, LX/CK5;->a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2547502
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2547503
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2547504
    iget-object v0, p0, LX/IBi;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/IBi;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2547505
    iget-object v0, p0, LX/IBi;->a:LX/0Zb;

    const-string v2, "event_location_summary_order_ride"

    invoke-interface {v0, v2, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2547506
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2547507
    const-string v2, "event_permalink"

    invoke-virtual {v0, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v2, "Event"

    invoke-virtual {v0, v2}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    iget-object v2, p0, LX/IBi;->g:Lcom/facebook/events/model/Event;

    .line 2547508
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2547509
    invoke-virtual {v0, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v2, "source_module"

    iget-object v3, p0, LX/IBi;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v2, "ref_module"

    iget-object v3, p0, LX/IBi;->h:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2547510
    :cond_1
    const v0, -0x4777e9ba

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2547511
    :cond_2
    invoke-virtual {p0}, LX/IBi;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081f16

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string p1, ""

    aput-object p1, v4, v5

    const/4 v5, 0x1

    const-string p1, ""

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2547512
    const-string v3, "\n"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method
