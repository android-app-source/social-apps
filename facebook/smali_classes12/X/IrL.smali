.class public final LX/IrL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/IrP;

.field private b:I


# direct methods
.method public constructor <init>(LX/IrP;)V
    .locals 0

    .prologue
    .line 2617792
    iput-object p1, p0, LX/IrL;->a:LX/IrP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2617793
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->v:LX/Ir4;

    if-eqz v0, :cond_0

    .line 2617794
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->q:LX/Ird;

    invoke-virtual {v0}, LX/Ird;->h()I

    move-result v0

    .line 2617795
    iget v1, p0, LX/IrL;->b:I

    if-eq v1, v0, :cond_0

    .line 2617796
    iput v0, p0, LX/IrL;->b:I

    .line 2617797
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    invoke-virtual {v0}, LX/IrP;->b()Z

    .line 2617798
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2617799
    iget-object v1, p0, LX/IrL;->a:LX/IrP;

    if-eqz p1, :cond_0

    sget-object v0, LX/Iqe;->TRANSFORMING:LX/Iqe;

    :goto_0
    invoke-static {v1, v0}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617800
    return-void

    .line 2617801
    :cond_0
    sget-object v0, LX/Iqe;->IDLE:LX/Iqe;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2617802
    if-eqz p1, :cond_1

    .line 2617803
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    sget-object v1, LX/Iqe;->TEXT:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617804
    :cond_0
    :goto_0
    return-void

    .line 2617805
    :cond_1
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->w:LX/Iqe;

    sget-object v1, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v0, v1, :cond_2

    .line 2617806
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v0, v1}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617807
    :cond_2
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->v:LX/Ir4;

    if-eqz v0, :cond_0

    .line 2617808
    iget-object v0, p0, LX/IrL;->a:LX/IrP;

    iget-object v0, v0, LX/IrP;->v:LX/Ir4;

    .line 2617809
    iget-object v1, v0, LX/Ir4;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 2617810
    goto :goto_0
.end method
