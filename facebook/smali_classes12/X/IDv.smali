.class public LX/IDv;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2551295
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2551296
    const/4 p1, 0x0

    .line 2551297
    invoke-virtual {p0}, LX/IDv;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2551298
    const v1, 0x7f030700

    invoke-virtual {v0, v1, p0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2551299
    const v1, 0x7f0207e8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2551300
    invoke-virtual {p0}, LX/IDv;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2551301
    invoke-virtual {p0, v0}, LX/IDv;->addView(Landroid/view/View;)V

    .line 2551302
    invoke-virtual {p0}, LX/IDv;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, LX/IDv;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/IDv;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, v1, p1, v2}, LX/IDv;->setPadding(IIII)V

    .line 2551303
    const v0, 0x7f02150c

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 2551304
    return-void
.end method
