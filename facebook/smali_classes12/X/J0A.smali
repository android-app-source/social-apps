.class public LX/J0A;
.super LX/Izu;
.source ""


# instance fields
.field private final A:LX/J0W;

.field private final B:LX/J0g;

.field private final C:LX/J0V;

.field private final D:LX/J0N;

.field private final E:LX/J0B;

.field private final a:LX/18V;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/J0f;

.field private final d:LX/J0G;

.field private final e:LX/J0c;

.field private final f:LX/J0X;

.field private final g:LX/J0C;

.field private final h:LX/J0E;

.field private final i:LX/J0D;

.field private final j:LX/J0H;

.field private final k:LX/J0M;

.field private final l:LX/J0k;

.field private final m:LX/J0e;

.field private final n:LX/J0Z;

.field private final o:LX/J0i;

.field private final p:LX/J0I;

.field private final q:LX/J0O;

.field private final r:LX/J0j;

.field private final s:LX/J0l;

.field private final t:LX/J0b;

.field private final u:LX/J0a;

.field private final v:LX/J0Q;

.field private final w:LX/J0S;

.field private final x:LX/J0T;

.field private final y:LX/J0R;

.field private final z:LX/J0P;


# direct methods
.method public constructor <init>(LX/18V;LX/0Or;LX/J0f;LX/J0G;LX/J0c;LX/J0X;LX/J0C;LX/J0E;LX/J0D;LX/J0H;LX/J0M;LX/J0k;LX/J0e;LX/J0Z;LX/J0i;LX/J0I;LX/J0O;LX/J0j;LX/J0l;LX/J0b;LX/J0a;LX/J0Q;LX/J0S;LX/J0T;LX/J0R;LX/J0P;LX/J0W;LX/J0g;LX/J0V;LX/J0N;LX/J0B;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/J0f;",
            "LX/J0G;",
            "LX/J0c;",
            "LX/J0X;",
            "LX/J0C;",
            "LX/J0E;",
            "LX/J0D;",
            "LX/J0H;",
            "LX/J0M;",
            "LX/J0k;",
            "LX/J0e;",
            "LX/J0Z;",
            "LX/J0i;",
            "LX/J0I;",
            "LX/J0O;",
            "LX/J0j;",
            "LX/J0l;",
            "LX/J0b;",
            "LX/J0a;",
            "LX/J0Q;",
            "LX/J0S;",
            "LX/J0T;",
            "LX/J0R;",
            "LX/J0P;",
            "LX/J0W;",
            "LX/J0g;",
            "LX/J0V;",
            "LX/J0N;",
            "LX/J0B;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636500
    const-string v1, "PaymentWebServiceHandler"

    invoke-direct {p0, v1}, LX/Izu;-><init>(Ljava/lang/String;)V

    .line 2636501
    iput-object p1, p0, LX/J0A;->a:LX/18V;

    .line 2636502
    iput-object p2, p0, LX/J0A;->b:LX/0Or;

    .line 2636503
    iput-object p3, p0, LX/J0A;->c:LX/J0f;

    .line 2636504
    iput-object p4, p0, LX/J0A;->d:LX/J0G;

    .line 2636505
    iput-object p5, p0, LX/J0A;->e:LX/J0c;

    .line 2636506
    iput-object p6, p0, LX/J0A;->f:LX/J0X;

    .line 2636507
    iput-object p7, p0, LX/J0A;->g:LX/J0C;

    .line 2636508
    iput-object p8, p0, LX/J0A;->h:LX/J0E;

    .line 2636509
    iput-object p9, p0, LX/J0A;->i:LX/J0D;

    .line 2636510
    iput-object p10, p0, LX/J0A;->j:LX/J0H;

    .line 2636511
    iput-object p11, p0, LX/J0A;->k:LX/J0M;

    .line 2636512
    iput-object p12, p0, LX/J0A;->l:LX/J0k;

    .line 2636513
    iput-object p13, p0, LX/J0A;->m:LX/J0e;

    .line 2636514
    move-object/from16 v0, p14

    iput-object v0, p0, LX/J0A;->n:LX/J0Z;

    .line 2636515
    move-object/from16 v0, p15

    iput-object v0, p0, LX/J0A;->o:LX/J0i;

    .line 2636516
    move-object/from16 v0, p16

    iput-object v0, p0, LX/J0A;->p:LX/J0I;

    .line 2636517
    move-object/from16 v0, p17

    iput-object v0, p0, LX/J0A;->q:LX/J0O;

    .line 2636518
    move-object/from16 v0, p18

    iput-object v0, p0, LX/J0A;->r:LX/J0j;

    .line 2636519
    move-object/from16 v0, p19

    iput-object v0, p0, LX/J0A;->s:LX/J0l;

    .line 2636520
    move-object/from16 v0, p20

    iput-object v0, p0, LX/J0A;->t:LX/J0b;

    .line 2636521
    move-object/from16 v0, p21

    iput-object v0, p0, LX/J0A;->u:LX/J0a;

    .line 2636522
    move-object/from16 v0, p22

    iput-object v0, p0, LX/J0A;->v:LX/J0Q;

    .line 2636523
    move-object/from16 v0, p23

    iput-object v0, p0, LX/J0A;->w:LX/J0S;

    .line 2636524
    move-object/from16 v0, p24

    iput-object v0, p0, LX/J0A;->x:LX/J0T;

    .line 2636525
    move-object/from16 v0, p25

    iput-object v0, p0, LX/J0A;->y:LX/J0R;

    .line 2636526
    move-object/from16 v0, p26

    iput-object v0, p0, LX/J0A;->z:LX/J0P;

    .line 2636527
    move-object/from16 v0, p27

    iput-object v0, p0, LX/J0A;->A:LX/J0W;

    .line 2636528
    move-object/from16 v0, p28

    iput-object v0, p0, LX/J0A;->B:LX/J0g;

    .line 2636529
    move-object/from16 v0, p29

    iput-object v0, p0, LX/J0A;->C:LX/J0V;

    .line 2636530
    move-object/from16 v0, p30

    iput-object v0, p0, LX/J0A;->D:LX/J0N;

    .line 2636531
    move-object/from16 v0, p31

    iput-object v0, p0, LX/J0A;->E:LX/J0B;

    .line 2636532
    return-void
.end method

.method public static b(LX/0QB;)LX/J0A;
    .locals 33

    .prologue
    .line 2636475
    new-instance v1, LX/J0A;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v2

    check-cast v2, LX/18V;

    const/16 v3, 0x1547

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static/range {p0 .. p0}, LX/J0f;->a(LX/0QB;)LX/J0f;

    move-result-object v4

    check-cast v4, LX/J0f;

    invoke-static/range {p0 .. p0}, LX/J0G;->a(LX/0QB;)LX/J0G;

    move-result-object v5

    check-cast v5, LX/J0G;

    invoke-static/range {p0 .. p0}, LX/J0c;->a(LX/0QB;)LX/J0c;

    move-result-object v6

    check-cast v6, LX/J0c;

    invoke-static/range {p0 .. p0}, LX/J0X;->a(LX/0QB;)LX/J0X;

    move-result-object v7

    check-cast v7, LX/J0X;

    invoke-static/range {p0 .. p0}, LX/J0C;->a(LX/0QB;)LX/J0C;

    move-result-object v8

    check-cast v8, LX/J0C;

    invoke-static/range {p0 .. p0}, LX/J0E;->a(LX/0QB;)LX/J0E;

    move-result-object v9

    check-cast v9, LX/J0E;

    invoke-static/range {p0 .. p0}, LX/J0D;->a(LX/0QB;)LX/J0D;

    move-result-object v10

    check-cast v10, LX/J0D;

    invoke-static/range {p0 .. p0}, LX/J0H;->a(LX/0QB;)LX/J0H;

    move-result-object v11

    check-cast v11, LX/J0H;

    invoke-static/range {p0 .. p0}, LX/J0M;->a(LX/0QB;)LX/J0M;

    move-result-object v12

    check-cast v12, LX/J0M;

    invoke-static/range {p0 .. p0}, LX/J0k;->a(LX/0QB;)LX/J0k;

    move-result-object v13

    check-cast v13, LX/J0k;

    invoke-static/range {p0 .. p0}, LX/J0e;->a(LX/0QB;)LX/J0e;

    move-result-object v14

    check-cast v14, LX/J0e;

    invoke-static/range {p0 .. p0}, LX/J0Z;->a(LX/0QB;)LX/J0Z;

    move-result-object v15

    check-cast v15, LX/J0Z;

    invoke-static/range {p0 .. p0}, LX/J0i;->a(LX/0QB;)LX/J0i;

    move-result-object v16

    check-cast v16, LX/J0i;

    invoke-static/range {p0 .. p0}, LX/J0I;->a(LX/0QB;)LX/J0I;

    move-result-object v17

    check-cast v17, LX/J0I;

    invoke-static/range {p0 .. p0}, LX/J0O;->a(LX/0QB;)LX/J0O;

    move-result-object v18

    check-cast v18, LX/J0O;

    invoke-static/range {p0 .. p0}, LX/J0j;->a(LX/0QB;)LX/J0j;

    move-result-object v19

    check-cast v19, LX/J0j;

    invoke-static/range {p0 .. p0}, LX/J0l;->a(LX/0QB;)LX/J0l;

    move-result-object v20

    check-cast v20, LX/J0l;

    invoke-static/range {p0 .. p0}, LX/J0b;->a(LX/0QB;)LX/J0b;

    move-result-object v21

    check-cast v21, LX/J0b;

    invoke-static/range {p0 .. p0}, LX/J0a;->a(LX/0QB;)LX/J0a;

    move-result-object v22

    check-cast v22, LX/J0a;

    invoke-static/range {p0 .. p0}, LX/J0Q;->a(LX/0QB;)LX/J0Q;

    move-result-object v23

    check-cast v23, LX/J0Q;

    invoke-static/range {p0 .. p0}, LX/J0S;->a(LX/0QB;)LX/J0S;

    move-result-object v24

    check-cast v24, LX/J0S;

    invoke-static/range {p0 .. p0}, LX/J0T;->a(LX/0QB;)LX/J0T;

    move-result-object v25

    check-cast v25, LX/J0T;

    invoke-static/range {p0 .. p0}, LX/J0R;->a(LX/0QB;)LX/J0R;

    move-result-object v26

    check-cast v26, LX/J0R;

    invoke-static/range {p0 .. p0}, LX/J0P;->a(LX/0QB;)LX/J0P;

    move-result-object v27

    check-cast v27, LX/J0P;

    invoke-static/range {p0 .. p0}, LX/J0W;->a(LX/0QB;)LX/J0W;

    move-result-object v28

    check-cast v28, LX/J0W;

    invoke-static/range {p0 .. p0}, LX/J0g;->a(LX/0QB;)LX/J0g;

    move-result-object v29

    check-cast v29, LX/J0g;

    invoke-static/range {p0 .. p0}, LX/J0V;->a(LX/0QB;)LX/J0V;

    move-result-object v30

    check-cast v30, LX/J0V;

    invoke-static/range {p0 .. p0}, LX/J0N;->a(LX/0QB;)LX/J0N;

    move-result-object v31

    check-cast v31, LX/J0N;

    invoke-static/range {p0 .. p0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v32

    check-cast v32, LX/J0B;

    invoke-direct/range {v1 .. v32}, LX/J0A;-><init>(LX/18V;LX/0Or;LX/J0f;LX/J0G;LX/J0c;LX/J0X;LX/J0C;LX/J0E;LX/J0D;LX/J0H;LX/J0M;LX/J0k;LX/J0e;LX/J0Z;LX/J0i;LX/J0I;LX/J0O;LX/J0j;LX/J0l;LX/J0b;LX/J0a;LX/J0Q;LX/J0S;LX/J0T;LX/J0R;LX/J0P;LX/J0W;LX/J0g;LX/J0V;LX/J0N;LX/J0B;)V

    .line 2636476
    return-object v1
.end method


# virtual methods
.method public final A(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636477
    iget-object v0, p0, LX/J0A;->a:LX/18V;

    iget-object v1, p0, LX/J0A;->C:LX/J0V;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2636478
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636479
    iget-object v0, p0, LX/J0A;->a:LX/18V;

    iget-object v1, p0, LX/J0A;->D:LX/J0N;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    .line 2636480
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636481
    iget-object v0, p0, LX/J0A;->a:LX/18V;

    iget-object v1, p0, LX/J0A;->d:LX/J0G;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    .line 2636482
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636483
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636484
    const-string v1, "fetchTransactionPaymentCardParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;

    .line 2636485
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->c:LX/J0f;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2636486
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636487
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636488
    const-string v1, "fetchPaymentTransactionParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;

    .line 2636489
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->e:LX/J0c;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2636490
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2636491
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636492
    sget-object v1, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;

    .line 2636493
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->f:LX/J0X;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636494
    iget-object v1, p0, LX/J0A;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2636495
    iget-object v1, p0, LX/J0A;->E:LX/J0B;

    sget-object v2, LX/DtQ;->R_CANCELED_DECLINED:LX/DtQ;

    .line 2636496
    iget-object v3, v0, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;->c:Ljava/lang/String;

    move-object v0, v3

    .line 2636497
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/J0B;->a(LX/DtQ;J)V

    .line 2636498
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636499
    return-object v0
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636550
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636551
    const-string v1, "addPaymentCardParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardParams;

    .line 2636552
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->g:LX/J0C;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/AddPaymentCardResult;

    .line 2636553
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636546
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636547
    const-string v1, "editPaymentCardParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;

    .line 2636548
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->h:LX/J0E;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2636549
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636554
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636555
    sget-object v1, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;

    .line 2636556
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->i:LX/J0D;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636557
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636558
    return-object v0
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636537
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636538
    sget-object v1, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;

    .line 2636539
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->j:LX/J0H;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636540
    iget-object v0, p0, LX/J0A;->E:LX/J0B;

    .line 2636541
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2636542
    const-string v2, "com.facebook.messaging.payment.ACTION_PRESET_CARD_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2636543
    invoke-static {v0, v1}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2636544
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636545
    return-object v0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636533
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636534
    sget-object v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    .line 2636535
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->k:LX/J0M;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    .line 2636536
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636469
    iget-object v0, p0, LX/J0A;->a:LX/18V;

    iget-object v1, p0, LX/J0A;->l:LX/J0k;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2636470
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636471
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636472
    const-string v1, "fetchTransactionListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;

    .line 2636473
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->m:LX/J0e;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;

    .line 2636474
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636405
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636406
    const-string v1, "fetchMoreTransactionsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;

    .line 2636407
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->n:LX/J0Z;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;

    .line 2636408
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636409
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636410
    if-nez v0, :cond_0

    .line 2636411
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null params bundle received"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636412
    :goto_0
    return-object v0

    .line 2636413
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636414
    sget-object v1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;

    .line 2636415
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->o:LX/J0i;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;

    .line 2636416
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636417
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636418
    sget-object v1, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinParams;

    .line 2636419
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->p:LX/J0I;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;

    .line 2636420
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636421
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636422
    sget-object v1, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderParams;

    .line 2636423
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->q:LX/J0O;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/moneypenny/MoneyPennyPlaceOrderResult;

    .line 2636424
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636425
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636426
    sget-object v1, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;

    .line 2636427
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->r:LX/J0j;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;

    .line 2636428
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636429
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636430
    sget-object v1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;

    .line 2636431
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->s:LX/J0l;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;

    .line 2636432
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636433
    iget-object v0, p0, LX/J0A;->a:LX/18V;

    iget-object v1, p0, LX/J0A;->t:LX/J0b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636434
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636435
    const-string v1, "platform_context_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2636436
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->u:LX/J0a;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636465
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636466
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/CreatePaymentRequestParams;

    .line 2636467
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->v:LX/J0Q;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2636468
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636437
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636438
    if-nez v0, :cond_0

    .line 2636439
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null params provided"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636440
    :goto_0
    return-object v0

    .line 2636441
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636442
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;

    .line 2636443
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->w:LX/J0S;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2636444
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636397
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636398
    if-nez v0, :cond_0

    .line 2636399
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null params provided"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636400
    :goto_0
    return-object v0

    .line 2636401
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636402
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    .line 2636403
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->x:LX/J0T;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    .line 2636404
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636445
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636446
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;

    .line 2636447
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->y:LX/J0R;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636448
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636449
    return-object v0
.end method

.method public final x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636450
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636451
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;

    .line 2636452
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->z:LX/J0P;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636453
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636454
    return-object v0
.end method

.method public final y(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636455
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636456
    sget-object v1, Lcom/facebook/payments/p2p/service/model/transactions/CancelPaymentTransactionParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/CancelPaymentTransactionParams;

    .line 2636457
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->A:LX/J0W;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636458
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636459
    return-object v0
.end method

.method public final z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636460
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636461
    const-string v1, "mutatePaymentPlatformContextParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/MutatePaymentPlatformContextParams;

    .line 2636462
    iget-object v1, p0, LX/J0A;->a:LX/18V;

    iget-object v2, p0, LX/J0A;->B:LX/J0g;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636463
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2636464
    return-object v0
.end method
