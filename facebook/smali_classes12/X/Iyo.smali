.class public LX/Iyo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2633922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2633923
    iput-object p1, p0, LX/Iyo;->a:LX/6rs;

    .line 2633924
    return-void
.end method

.method public static b(LX/0QB;)LX/Iyo;
    .locals 2

    .prologue
    .line 2633951
    new-instance v1, LX/Iyo;

    invoke-static {p0}, LX/6rs;->b(LX/0QB;)LX/6rs;

    move-result-object v0

    check-cast v0, LX/6rs;

    invoke-direct {v1, v0}, LX/Iyo;-><init>(LX/6rs;)V

    .line 2633952
    return-object v1
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;
    .locals 14

    .prologue
    .line 2633925
    const-string v0, "data"

    invoke-static {p1, v0}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 2633926
    const-string v1, "invoice_creation_config"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2633927
    const-string v1, "invoice_content_configuration"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 2633928
    const-string v1, "invoice_onboarding_config"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 2633929
    if-eqz v1, :cond_0

    .line 2633930
    new-instance v0, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    const-string v2, "onboarding_url"

    invoke-static {v1, v2}, LX/16N;->i(LX/0lF;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;-><init>(Landroid/net/Uri;)V

    .line 2633931
    :goto_0
    return-object v0

    .line 2633932
    :cond_0
    if-eqz v5, :cond_1

    .line 2633933
    new-instance v7, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;

    new-instance v0, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;

    const-string v1, "title"

    invoke-virtual {v5, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "currency"

    invoke-virtual {v5, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "invoice_item_limit"

    invoke-virtual {v5, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    const v4, 0x7fffffff

    invoke-static {v3, v4}, LX/16N;->a(LX/0lF;I)I

    move-result v3

    const-string v4, "custom_items"

    invoke-virtual {v5, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    const/4 v12, 0x0

    .line 2633934
    if-nez v4, :cond_2

    .line 2633935
    const/4 v8, 0x0

    .line 2633936
    :goto_1
    move-object v4, v8

    .line 2633937
    const-string v8, "catalog_items"

    invoke-virtual {v5, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2633938
    if-nez v5, :cond_4

    .line 2633939
    const/4 v8, 0x0

    .line 2633940
    :goto_2
    move-object v5, v8

    .line 2633941
    iget-object v8, p0, LX/Iyo;->a:LX/6rs;

    const-string v9, "1.1.1"

    invoke-virtual {v8, v9}, LX/6rs;->d(Ljava/lang/String;)LX/6rr;

    move-result-object v8

    const-string v9, "1.1.1"

    invoke-interface {v8, v9, v6}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/Parcelable;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/payments/cart/model/CustomItemsConfig;Lcom/facebook/payments/cart/model/CatalogItemsConfig;Landroid/os/Parcelable;)V

    invoke-direct {v7, v0}, Lcom/facebook/payments/invoice/protocol/InvoiceConfigResult;-><init>(Lcom/facebook/payments/cart/model/SimpleCartScreenConfig;)V

    move-object v0, v7

    goto :goto_0

    .line 2633942
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Json data received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2633943
    :cond_2
    sget-object v8, LX/0Q7;->a:LX/0Px;

    move-object v9, v8

    .line 2633944
    :try_start_0
    iget-object v8, p0, LX/Iyo;->a:LX/6rs;

    const-string v10, "1.1.1"

    invoke-virtual {v8, v10}, LX/6rs;->q(Ljava/lang/String;)LX/6rr;

    move-result-object v8

    const-string v10, "1.1.1"

    const-string v11, "item_info"

    invoke-static {v4, v11}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v11

    invoke-interface {v8, v10, v11}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v10, v8

    .line 2633945
    :goto_3
    new-instance v13, LX/0P2;

    invoke-direct {v13}, LX/0P2;-><init>()V

    .line 2633946
    invoke-virtual {v10}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 2633947
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result p1

    move v11, v12

    :goto_4
    if-ge v11, p1, :cond_3

    invoke-virtual {v10, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/payments/form/model/FormRowDefinition;

    .line 2633948
    iget-object v9, v8, Lcom/facebook/payments/form/model/FormRowDefinition;->a:LX/0Px;

    invoke-virtual {v9, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iget-object v9, v9, Lcom/facebook/payments/form/model/FormFieldAttributes;->b:LX/6xN;

    iget-object v8, v8, Lcom/facebook/payments/form/model/FormRowDefinition;->a:LX/0Px;

    invoke-virtual {v8, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v13, v9, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2633949
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    goto :goto_4

    :catch_0
    move-object v10, v9

    goto :goto_3

    .line 2633950
    :cond_3
    new-instance v8, Lcom/facebook/payments/cart/model/CustomItemsConfig;

    const-string v9, "actionable_title"

    invoke-virtual {v4, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "quantity_limit"

    invoke-virtual {v4, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    const v11, 0x7fffffff

    invoke-static {v10, v11}, LX/16N;->a(LX/0lF;I)I

    move-result v10

    invoke-virtual {v13}, LX/0P2;->b()LX/0P1;

    move-result-object v11

    invoke-direct {v8, v9, v10, v11}, Lcom/facebook/payments/cart/model/CustomItemsConfig;-><init>(Ljava/lang/String;ILX/0P1;)V

    goto/16 :goto_1

    :cond_4
    new-instance v8, Lcom/facebook/payments/cart/model/CatalogItemsConfig;

    const-string v9, "customize_price"

    invoke-virtual {v5, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    invoke-static {v9}, LX/16N;->g(LX/0lF;)Z

    move-result v9

    invoke-direct {v8, v9}, Lcom/facebook/payments/cart/model/CatalogItemsConfig;-><init>(Z)V

    goto/16 :goto_2
.end method
