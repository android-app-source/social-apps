.class public final LX/ISs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V
    .locals 0

    .prologue
    .line 2578451
    iput-object p1, p0, LX/ISs;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2578452
    iget-object v0, p0, LX/ISs;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->d:LX/IW7;

    iget-object v1, p0, LX/ISs;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->h:Ljava/lang/String;

    iget-object v2, p0, LX/ISs;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->i:Ljava/lang/String;

    .line 2578453
    new-instance v3, LX/DUS;

    invoke-direct {v3}, LX/DUS;-><init>()V

    .line 2578454
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    const/16 v6, 0x80

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "find_member"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2578455
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/32 v5, 0x93a80

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2578456
    iget-object v4, v0, LX/IW7;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v0, v3

    .line 2578457
    return-object v0
.end method
