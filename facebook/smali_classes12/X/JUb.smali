.class public LX/JUb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUZ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2699352
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JUb;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699353
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699354
    iput-object p1, p0, LX/JUb;->b:LX/0Ot;

    .line 2699355
    return-void
.end method

.method public static a(LX/0QB;)LX/JUb;
    .locals 4

    .prologue
    .line 2699341
    const-class v1, LX/JUb;

    monitor-enter v1

    .line 2699342
    :try_start_0
    sget-object v0, LX/JUb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699343
    sput-object v2, LX/JUb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699344
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699345
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699346
    new-instance v3, LX/JUb;

    const/16 p0, 0x207e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUb;-><init>(LX/0Ot;)V

    .line 2699347
    move-object v0, v3

    .line 2699348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2699335
    check-cast p2, LX/JUa;

    .line 2699336
    iget-object v0, p0, LX/JUb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;

    iget-object v1, p2, LX/JUa;->a:Ljava/lang/String;

    .line 2699337
    iget-object p0, v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->b:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    sget-object p2, LX/1Up;->g:LX/1Up;

    invoke-virtual {p0, p2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object p0

    const p2, 0x7f0a010a

    invoke-virtual {p0, p2}, LX/1nw;->h(I)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b25c1

    invoke-interface {p0, p2}, LX/1Di;->i(I)LX/1Di;

    move-result-object p0

    const p2, 0x7f0b25c2

    invoke-interface {p0, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    const p2, 0x7f08223b

    invoke-virtual {p1, p2}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p0, p2}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object p0

    const p2, 0x7f021388

    invoke-interface {p0, p2}, LX/1Di;->z(I)LX/1Di;

    move-result-object p0

    .line 2699338
    const p2, -0x585c3ee5

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 2699339
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 2699340
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2699325
    invoke-static {}, LX/1dS;->b()V

    .line 2699326
    iget v0, p1, LX/1dQ;->b:I

    .line 2699327
    packed-switch v0, :pswitch_data_0

    .line 2699328
    :goto_0
    return-object v2

    .line 2699329
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2699330
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2699331
    check-cast v1, LX/JUa;

    .line 2699332
    iget-object v3, p0, LX/JUb;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;

    iget-object v4, v1, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object p2, v1, LX/JUa;->d:Ljava/lang/String;

    .line 2699333
    iget-object p0, v3, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->c:LX/JUQ;

    invoke-virtual {p0, v0, v4, p1, p2}, LX/JUQ;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V

    .line 2699334
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x585c3ee5
        :pswitch_0
    .end packed-switch
.end method
