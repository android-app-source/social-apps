.class public final LX/HPO;
.super LX/DvC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V
    .locals 0

    .prologue
    .line 2461795
    iput-object p1, p0, LX/HPO;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-direct {p0}, LX/DvC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 2461796
    check-cast p1, LX/DvB;

    .line 2461797
    iget-object v0, p0, LX/HPO;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, p1, LX/DvB;->a:Ljava/lang/String;

    iget-object v2, p1, LX/DvB;->b:Landroid/net/Uri;

    .line 2461798
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    if-nez v5, :cond_1

    .line 2461799
    :cond_0
    :goto_0
    iget-object v0, p0, LX/HPO;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v1, p0, LX/HPO;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v1, "pages_photos_tab"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, LX/9XE;->a(JLjava/lang/String;Z)V

    .line 2461800
    return-void

    .line 2461801
    :cond_1
    iget-object v5, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a:LX/Dxd;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    .line 2461802
    iget-object v8, v7, LX/Dvb;->i:LX/Dvc;

    move-object v7, v8

    .line 2461803
    invoke-virtual {v7}, LX/Dvc;->d()LX/0Px;

    move-result-object v9

    sget-object v10, LX/74S;->PAGE_PHOTOS_TAB:LX/74S;

    const/4 v11, 0x0

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v5 .. v11}, LX/Dxd;->a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    goto :goto_0
.end method
