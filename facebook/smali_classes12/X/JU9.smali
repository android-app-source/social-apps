.class public final LX/JU9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JUH;

.field public final synthetic b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/JUH;)V
    .locals 0

    .prologue
    .line 2697741
    iput-object p1, p0, LX/JU9;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iput-object p2, p0, LX/JU9;->a:LX/JUH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0xd01bc35

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697742
    iget-object v1, p0, LX/JU9;->a:LX/JUH;

    iget-object v1, v1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->p()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2697743
    iget-object v1, p0, LX/JU9;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v2, p0, LX/JU9;->a:LX/JUH;

    .line 2697744
    iget-object v3, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->p()Ljava/lang/String;

    move-result-object v3

    .line 2697745
    new-instance v4, LX/89k;

    invoke-direct {v4}, LX/89k;-><init>()V

    .line 2697746
    iput-object v3, v4, LX/89k;->b:Ljava/lang/String;

    .line 2697747
    move-object v3, v4

    .line 2697748
    invoke-virtual {v3}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v3

    .line 2697749
    iget-object v4, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->e:LX/0hy;

    invoke-interface {v4, v3}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v3

    .line 2697750
    iget-object v4, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2697751
    iget-object v1, p0, LX/JU9;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->d:LX/JUO;

    iget-object v2, p0, LX/JU9;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p0, LX/JU9;->a:LX/JUH;

    iget-object v3, v3, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1, v2, v3}, LX/JUO;->c(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2697752
    :goto_0
    const v1, 0x2d31a847

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2697753
    :cond_0
    iget-object v1, p0, LX/JU9;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v2, p0, LX/JU9;->a:LX/JUH;

    .line 2697754
    iget-object v3, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->b:LX/1nA;

    iget-object v4, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v4}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-static {v4}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, v5}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2697755
    iget-object v1, p0, LX/JU9;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->d:LX/JUO;

    iget-object v2, p0, LX/JU9;->a:LX/JUH;

    iget-object v2, v2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p0, LX/JU9;->a:LX/JUH;

    iget-object v3, v3, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1, v2, v3}, LX/JUO;->d(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    goto :goto_0
.end method
