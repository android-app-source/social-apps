.class public LX/JJc;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKAnalytics"
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2679620
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679621
    iput-object p2, p0, LX/JJc;->a:LX/0Ot;

    .line 2679622
    iput-object p3, p0, LX/JJc;->b:LX/0Ot;

    .line 2679623
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679642
    const-string v0, "RKAnalytics"

    return-object v0
.end method

.method public logCounter(Ljava/lang/String;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679641
    return-void
.end method

.method public logEvent(Ljava/lang/String;LX/5pG;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2679643
    iget-object v0, p0, LX/JJc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mh;

    const/4 v1, 0x0

    sget-object v4, LX/0mq;->CLIENT_EVENT:LX/0mq;

    move-object v2, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/0mj;

    move-result-object v0

    .line 2679644
    invoke-virtual {v0}, LX/0mj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2679645
    invoke-static {v0, p2}, LX/GVl;->a(LX/0mj;LX/5pG;)V

    .line 2679646
    invoke-virtual {v0}, LX/0mj;->e()V

    .line 2679647
    :cond_0
    return-void
.end method

.method public logExposure(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679629
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2679630
    new-instance v0, LX/3yb;

    invoke-direct {v0}, LX/3yb;-><init>()V

    .line 2679631
    sget-object v2, LX/1gX;->EXPOSURE:LX/1gX;

    invoke-virtual {v2}, LX/1gX;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 2679632
    iput-object v2, v0, LX/3yb;->c:Ljava/lang/String;

    .line 2679633
    move-object v2, v0

    .line 2679634
    iput-object p1, v2, LX/3yb;->a:Ljava/lang/String;

    .line 2679635
    move-object v2, v2

    .line 2679636
    const-string v3, "QuickExperimentControllerImplLoggingContext"

    .line 2679637
    iput-object v3, v2, LX/3yb;->d:Ljava/lang/String;

    .line 2679638
    const-string v2, "experiment_logging_params"

    invoke-virtual {v0}, LX/3yb;->a()Lcom/facebook/abtest/qe/protocol/sync/QuickExperimentLoggingParams;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2679639
    iget-object v0, p0, LX/JJc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v2, "log_to_qe"

    const v3, -0x6eb33e72

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2679640
    return-void
.end method

.method public logRealtimeEvent(Ljava/lang/String;LX/5pG;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2679624
    iget-object v0, p0, LX/JJc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0mh;

    const/4 v1, 0x0

    sget-object v4, LX/0mq;->CLIENT_EVENT:LX/0mq;

    move-object v2, p1

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0mh;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/0mj;

    move-result-object v0

    .line 2679625
    invoke-virtual {v0}, LX/0mj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2679626
    invoke-static {v0, p2}, LX/GVl;->a(LX/0mj;LX/5pG;)V

    .line 2679627
    invoke-virtual {v0}, LX/0mj;->e()V

    .line 2679628
    :cond_0
    return-void
.end method
