.class public final LX/JH5;
.super LX/JGT;
.source ""


# direct methods
.method public constructor <init>(LX/JGs;[LX/JGM;)V
    .locals 0

    .prologue
    .line 2672277
    invoke-direct {p0, p1, p2}, LX/JGT;-><init>(LX/JGs;[LX/JGM;)V

    .line 2672278
    return-void
.end method

.method public static a([LX/JGM;[F[FLandroid/util/SparseIntArray;)V
    .locals 4

    .prologue
    .line 2672279
    const/4 v1, 0x0

    .line 2672280
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    array-length v0, p0

    if-ge v1, v0, :cond_1

    .line 2672281
    aget-object v0, p0, v1

    instance-of v0, v0, LX/JGY;

    if-eqz v0, :cond_0

    .line 2672282
    aget-object v0, p0, v1

    check-cast v0, LX/JGY;

    .line 2672283
    iget v3, v0, LX/JGY;->d:I

    invoke-virtual {p3, v3, v1}, Landroid/util/SparseIntArray;->append(II)V

    .line 2672284
    iget v0, v0, LX/JGY;->i:F

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2672285
    :goto_1
    aput v2, p1, v1

    .line 2672286
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2672287
    :cond_0
    aget-object v0, p0, v1

    invoke-virtual {v0}, LX/JGM;->m()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_1

    .line 2672288
    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_3

    .line 2672289
    aget-object v0, p0, v1

    instance-of v0, v0, LX/JGY;

    if-eqz v0, :cond_2

    .line 2672290
    aget-object v0, p0, v1

    check-cast v0, LX/JGY;

    iget v0, v0, LX/JGY;->g:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2672291
    :goto_3
    aput v0, p2, v1

    .line 2672292
    add-int/lit8 v1, v1, -0x1

    move v2, v0

    goto :goto_2

    .line 2672293
    :cond_2
    aget-object v0, p0, v1

    invoke-virtual {v0}, LX/JGM;->k()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_3

    .line 2672294
    :cond_3
    return-void
.end method

.method public static b([LX/JGu;[F[F)V
    .locals 3

    .prologue
    .line 2672265
    const/4 v1, 0x0

    .line 2672266
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 2672267
    aget-object v2, p0, v0

    invoke-virtual {v2}, LX/JGu;->d()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2672268
    aput v1, p1, v0

    .line 2672269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2672270
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 2672271
    aget-object v2, p0, v0

    invoke-virtual {v2}, LX/JGu;->b()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2672272
    aput v1, p2, v0

    .line 2672273
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2672274
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2672275
    iget-object v0, p0, LX/JGT;->a:[F

    iget-object v1, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([FF)I

    move-result v0

    .line 2672276
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(FF)I
    .locals 2

    .prologue
    .line 2672263
    iget-object v0, p0, LX/JGT;->d:[F

    const v1, 0x38d1b717    # 1.0E-4f

    add-float/2addr v1, p2

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([FF)I

    move-result v0

    .line 2672264
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 2672261
    iget-object v0, p0, LX/JGT;->b:[F

    iget-object v1, p0, LX/JGT;->b:[F

    array-length v1, v1

    iget-object v2, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    invoke-static {v0, p1, v1, v2}, Ljava/util/Arrays;->binarySearch([FIIF)I

    move-result v0

    .line 2672262
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(IFF)Z
    .locals 1

    .prologue
    .line 2672260
    iget-object v0, p0, LX/JGT;->c:[F

    aget v0, v0, p1

    cmpg-float v0, v0, p3

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
