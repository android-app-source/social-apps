.class public LX/Hst;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/AnS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Anc;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2515123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515124
    return-void
.end method

.method public static a(LX/0QB;)LX/Hst;
    .locals 6

    .prologue
    .line 2515125
    const-class v1, LX/Hst;

    monitor-enter v1

    .line 2515126
    :try_start_0
    sget-object v0, LX/Hst;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2515127
    sput-object v2, LX/Hst;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2515128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2515130
    new-instance p0, LX/Hst;

    invoke-direct {p0}, LX/Hst;-><init>()V

    .line 2515131
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/AnS;->a(LX/0QB;)LX/AnS;

    move-result-object v4

    check-cast v4, LX/AnS;

    invoke-static {v0}, LX/Anc;->a(LX/0QB;)LX/Anc;

    move-result-object v5

    check-cast v5, LX/Anc;

    .line 2515132
    iput-object v3, p0, LX/Hst;->a:Landroid/content/Context;

    iput-object v4, p0, LX/Hst;->b:LX/AnS;

    iput-object v5, p0, LX/Hst;->c:LX/Anc;

    .line 2515133
    move-object v0, p0

    .line 2515134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2515135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hst;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2515136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2515137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Ljava/lang/Runnable;)LX/2EJ;
    .locals 12
    .param p2    # Lcom/facebook/ipc/composer/intent/ComposerShareParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2515138
    const/4 v5, 0x0

    .line 2515139
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    iget-object v6, v6, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v6, :cond_4

    .line 2515140
    :cond_0
    :goto_0
    move-object v0, v5

    .line 2515141
    if-nez v0, :cond_1

    .line 2515142
    const/4 v0, 0x0

    .line 2515143
    :goto_1
    return-object v0

    .line 2515144
    :cond_1
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/Hst;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/Hst;->b:LX/AnS;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bM()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AnS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bH()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/Hst;->b:LX/AnS;

    iget-object v3, p0, LX/Hst;->a:Landroid/content/Context;

    const v4, 0x7f0813f8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AnS;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Hsr;

    invoke-direct {v3, p0, p3}, LX/Hsr;-><init>(LX/Hst;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/Hst;->b:LX/AnS;

    iget-object v3, p0, LX/Hst;->a:Landroid/content/Context;

    const v4, 0x7f081440

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AnS;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Hsq;

    invoke-direct {v3, p0}, LX/Hsq;-><init>(LX/Hst;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    .line 2515145
    const/4 v3, 0x0

    .line 2515146
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_5

    move-object v2, v3

    .line 2515147
    :cond_2
    :goto_2
    move-object v0, v2

    .line 2515148
    if-eqz v0, :cond_3

    .line 2515149
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->k()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Hss;

    invoke-direct {v3, p0, v0}, LX/Hss;-><init>(LX/Hst;Lcom/facebook/graphql/model/GraphQLMisinformationAction;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2515150
    :cond_3
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    goto :goto_1

    .line 2515151
    :cond_4
    if-eqz p2, :cond_0

    iget-object v6, p2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v6, :cond_0

    .line 2515152
    iget-object v6, p2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v7, -0x67292209

    invoke-static {v6, v7}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 2515153
    if-eqz v6, :cond_0

    .line 2515154
    iget-object v7, p0, LX/Hst;->b:LX/AnS;

    .line 2515155
    iget-object v8, v7, LX/AnS;->a:LX/0W3;

    sget-wide v10, LX/0X5;->kw:J

    invoke-interface {v8, v10, v11}, LX/0W4;->b(J)Z

    move-result v8

    move v7, v8

    .line 2515156
    if-eqz v7, :cond_0

    move-object v5, v6

    .line 2515157
    goto/16 :goto_0

    .line 2515158
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v6, :cond_7

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLMisinformationAction;

    .line 2515159
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->a()Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLMisinformationActionType;

    if-ne v7, v8, :cond_6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->l()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMisinformationAction;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2515160
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :cond_7
    move-object v2, v3

    .line 2515161
    goto :goto_2
.end method
