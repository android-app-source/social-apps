.class public final LX/JOG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/greetingcards/render/RenderCardFragment;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Landroid/view/View;

.field public final synthetic f:Landroid/view/View;

.field public final synthetic g:LX/JOH;


# direct methods
.method public constructor <init>(LX/JOH;Landroid/app/Activity;Landroid/view/View;Lcom/facebook/greetingcards/render/RenderCardFragment;Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2687158
    iput-object p1, p0, LX/JOG;->g:LX/JOH;

    iput-object p2, p0, LX/JOG;->a:Landroid/app/Activity;

    iput-object p3, p0, LX/JOG;->b:Landroid/view/View;

    iput-object p4, p0, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    iput-object p5, p0, LX/JOG;->d:Landroid/content/Context;

    iput-object p6, p0, LX/JOG;->e:Landroid/view/View;

    iput-object p7, p0, LX/JOG;->f:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 7

    .prologue
    .line 2687159
    iget-object v0, p0, LX/JOG;->a:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 2687160
    :cond_0
    :goto_0
    return-void

    .line 2687161
    :cond_1
    iget-object v0, p0, LX/JOG;->a:Landroid/app/Activity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 2687162
    iget-object v0, p0, LX/JOG;->b:Landroid/view/View;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 2687163
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 2687164
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 2687165
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 2687166
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 2687167
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2687168
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2687169
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2687170
    iget-object v3, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2687171
    iget-object v3, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    iget-object v6, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 2687172
    iget-object v3, p0, LX/JOG;->b:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2687173
    iget-object v2, p0, LX/JOG;->g:LX/JOH;

    iget-object v2, v2, LX/JOH;->c:LX/JOL;

    iget-object v2, v2, LX/JOL;->a:LX/Gk2;

    if-nez v2, :cond_3

    .line 2687174
    iget-object v2, p0, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    const/4 v3, 0x1

    .line 2687175
    iput-boolean v3, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->p:Z

    .line 2687176
    iget-object v2, p0, LX/JOG;->g:LX/JOH;

    iget-object v2, v2, LX/JOH;->c:LX/JOL;

    iget-object v2, v2, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    new-instance v3, LX/JOF;

    invoke-direct {v3, p0}, LX/JOF;-><init>(LX/JOG;)V

    .line 2687177
    iput-object v3, v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    .line 2687178
    :goto_1
    iget-object v2, p0, LX/JOG;->g:LX/JOH;

    iget-object v2, v2, LX/JOH;->c:LX/JOL;

    iget-object v2, v2, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    .line 2687179
    iput-object v0, v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->q:Landroid/graphics/Rect;

    .line 2687180
    move-object v0, v2

    .line 2687181
    iput-object v1, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->r:Landroid/graphics/Bitmap;

    .line 2687182
    move-object v0, v0

    .line 2687183
    iget-object v1, p0, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2687184
    iput-object v1, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->o:Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;

    .line 2687185
    move-object v0, v0

    .line 2687186
    iget-object v1, p0, LX/JOG;->d:Landroid/content/Context;

    invoke-static {v1}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v1

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, LX/JOG;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget-object v3, p0, LX/JOG;->e:Landroid/view/View;

    .line 2687187
    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2687188
    const-string v4, "Unsafe to commit stateful transactions."

    .line 2687189
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2687190
    iget-object v5, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->t:LX/03V;

    sget-object v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->m:Ljava/lang/String;

    invoke-virtual {v5, v1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2687191
    :cond_2
    :goto_2
    iget-object v0, p0, LX/JOG;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/16 :goto_0

    .line 2687192
    :cond_3
    iget-object v2, p0, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    const/4 v3, 0x0

    .line 2687193
    iput-boolean v3, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->p:Z

    .line 2687194
    iget-object v2, p0, LX/JOG;->g:LX/JOH;

    iget-object v2, v2, LX/JOH;->c:LX/JOL;

    iget-object v2, v2, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    const/4 v3, 0x0

    .line 2687195
    iput-object v3, v2, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    .line 2687196
    goto :goto_1

    .line 2687197
    :cond_4
    new-instance v4, LX/GjZ;

    invoke-direct {v4, v0, v3, v2}, LX/GjZ;-><init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;Landroid/view/View;Landroid/view/Window;)V

    iput-object v4, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    .line 2687198
    iput-object v1, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->z:LX/0gc;

    .line 2687199
    const/4 v4, 0x2

    const v5, 0x7f0e05f9

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2687200
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2687201
    const-string v4, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2687202
    invoke-virtual {v1}, LX/0gc;->b()Z

    goto :goto_2
.end method
