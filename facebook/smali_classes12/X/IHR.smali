.class public final LX/IHR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2560199
    const/16 v17, 0x0

    .line 2560200
    const/16 v16, 0x0

    .line 2560201
    const/4 v15, 0x0

    .line 2560202
    const/4 v14, 0x0

    .line 2560203
    const/4 v13, 0x0

    .line 2560204
    const/4 v12, 0x0

    .line 2560205
    const/4 v9, 0x0

    .line 2560206
    const-wide/16 v10, 0x0

    .line 2560207
    const/4 v8, 0x0

    .line 2560208
    const/4 v7, 0x0

    .line 2560209
    const/4 v6, 0x0

    .line 2560210
    const/4 v5, 0x0

    .line 2560211
    const/4 v4, 0x0

    .line 2560212
    const/4 v3, 0x0

    .line 2560213
    const/4 v2, 0x0

    .line 2560214
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 2560215
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2560216
    const/4 v2, 0x0

    .line 2560217
    :goto_0
    return v2

    .line 2560218
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    if-eq v2, v0, :cond_b

    .line 2560219
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2560220
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2560221
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2560222
    const-string v19, "friends_sharing"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 2560223
    invoke-static/range {p0 .. p1}, LX/IHJ;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2560224
    :cond_1
    const-string v19, "incomingPings"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2560225
    invoke-static/range {p0 .. p1}, LX/IHK;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2560226
    :cond_2
    const-string v19, "is_sharing_enabled"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2560227
    const/4 v2, 0x1

    .line 2560228
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v16, v9

    move v9, v2

    goto :goto_1

    .line 2560229
    :cond_3
    const-string v19, "is_tracking_enabled"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 2560230
    const/4 v2, 0x1

    .line 2560231
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v15, v7

    move v7, v2

    goto :goto_1

    .line 2560232
    :cond_4
    const-string v19, "is_traveling"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2560233
    const/4 v2, 0x1

    .line 2560234
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v14, v6

    move v6, v2

    goto :goto_1

    .line 2560235
    :cond_5
    const-string v19, "nearby_friends_region"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2560236
    invoke-static/range {p0 .. p1}, LX/IHN;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2560237
    :cond_6
    const-string v19, "outgoingPings"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 2560238
    invoke-static/range {p0 .. p1}, LX/IHO;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2560239
    :cond_7
    const-string v19, "pause_expiration_time"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2560240
    const/4 v2, 0x1

    .line 2560241
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2560242
    :cond_8
    const-string v19, "show_dashboard_nux"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 2560243
    const/4 v2, 0x1

    .line 2560244
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v11, v8

    move v8, v2

    goto/16 :goto_1

    .line 2560245
    :cond_9
    const-string v19, "upsell"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2560246
    invoke-static/range {p0 .. p1}, LX/IHQ;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2560247
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2560248
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2560249
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2560250
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2560251
    if-eqz v9, :cond_c

    .line 2560252
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2560253
    :cond_c
    if-eqz v7, :cond_d

    .line 2560254
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 2560255
    :cond_d
    if-eqz v6, :cond_e

    .line 2560256
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->a(IZ)V

    .line 2560257
    :cond_e
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2560258
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2560259
    if-eqz v3, :cond_f

    .line 2560260
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2560261
    :cond_f
    if-eqz v8, :cond_10

    .line 2560262
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 2560263
    :cond_10
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2560264
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_11
    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move/from16 v21, v7

    move v7, v5

    move-wide v4, v10

    move/from16 v10, v21

    move v11, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2560265
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560266
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560267
    if-eqz v0, :cond_0

    .line 2560268
    const-string v1, "friends_sharing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560269
    invoke-static {p0, v0, p2, p3}, LX/IHJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560270
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560271
    if-eqz v0, :cond_1

    .line 2560272
    const-string v1, "incomingPings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560273
    invoke-static {p0, v0, p2, p3}, LX/IHK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560274
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2560275
    if-eqz v0, :cond_2

    .line 2560276
    const-string v1, "is_sharing_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560277
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2560278
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2560279
    if-eqz v0, :cond_3

    .line 2560280
    const-string v1, "is_tracking_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560281
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2560282
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2560283
    if-eqz v0, :cond_4

    .line 2560284
    const-string v1, "is_traveling"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560285
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2560286
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560287
    if-eqz v0, :cond_5

    .line 2560288
    const-string v1, "nearby_friends_region"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560289
    invoke-static {p0, v0, p2, p3}, LX/IHN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560290
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560291
    if-eqz v0, :cond_6

    .line 2560292
    const-string v1, "outgoingPings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560293
    invoke-static {p0, v0, p2, p3}, LX/IHO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560294
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2560295
    cmp-long v2, v0, v2

    if-eqz v2, :cond_7

    .line 2560296
    const-string v2, "pause_expiration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560297
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2560298
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2560299
    if-eqz v0, :cond_8

    .line 2560300
    const-string v1, "show_dashboard_nux"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560301
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2560302
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560303
    if-eqz v0, :cond_9

    .line 2560304
    const-string v1, "upsell"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560305
    invoke-static {p0, v0, p2, p3}, LX/IHQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560306
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560307
    return-void
.end method
