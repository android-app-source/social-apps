.class public LX/Hs7;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j6;",
        "DerivedData::",
        "LX/5RE;",
        "PluginData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginGetters$ProvidesPluginAllowsBirthdaySproutGetter;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0in",
        "<TPluginData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:LX/Hu0;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/Hr2;

.field public final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;Landroid/content/Context;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514008
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2514009
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hs7;->b:Ljava/lang/ref/WeakReference;

    .line 2514010
    iput-object p2, p0, LX/Hs7;->c:LX/Hr2;

    .line 2514011
    iput-object p3, p0, LX/Hs7;->d:Landroid/content/Context;

    .line 2514012
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f0207a9

    .line 2514013
    iput v1, v0, LX/Htz;->a:I

    .line 2514014
    move-object v0, v0

    .line 2514015
    const v1, 0x7f0a07c7

    .line 2514016
    iput v1, v0, LX/Htz;->f:I

    .line 2514017
    move-object v0, v0

    .line 2514018
    iget-object v1, p0, LX/Hs7;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082975

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2514019
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2514020
    move-object v0, v0

    .line 2514021
    iget-object v1, p0, LX/Hs7;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 2514022
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2514023
    const/4 v1, 0x0

    .line 2514024
    :goto_0
    move-object v1, v1

    .line 2514025
    iput-object v1, v0, LX/Htz;->c:Ljava/lang/String;

    .line 2514026
    move-object v0, v0

    .line 2514027
    invoke-virtual {p0}, LX/Hs7;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2514028
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2514029
    move-object v0, v0

    .line 2514030
    iget-object v1, p0, LX/Hs7;->c:LX/Hr2;

    .line 2514031
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2514032
    move-object v0, v0

    .line 2514033
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Hs7;->a:LX/Hu0;

    .line 2514034
    return-void

    :cond_0
    iget-object v2, p0, LX/Hs7;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f082976

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v1, p2, p3

    invoke-virtual {v2, p1, p2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2514039
    iget-object v0, p0, LX/Hs7;->a:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 2514040
    iget-object v0, p0, LX/Hs7;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2514041
    check-cast v1, LX/0in;

    invoke-interface {v1}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AQ9;

    .line 2514042
    iget-object v2, v1, LX/AQ9;->m:LX/ARN;

    move-object v1, v2

    .line 2514043
    if-eqz v1, :cond_1

    check-cast v0, LX/0in;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    .line 2514044
    iget-object v1, v0, LX/AQ9;->m:LX/ARN;

    move-object v0, v1

    .line 2514045
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2514046
    iget-object v0, p0, LX/Hs7;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2514047
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v2, LX/5RF;->STICKER:LX/5RF;

    if-eq v1, v2, :cond_0

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v1, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2514048
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2514036
    iget-object v0, p0, LX/Hs7;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514037
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5RE;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    .line 2514038
    sget-object v1, LX/5RF;->STICKER:LX/5RF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2514035
    sget-object v0, LX/Hty;->BIRTHDAY:LX/Hty;

    return-object v0
.end method
