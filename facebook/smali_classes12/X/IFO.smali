.class public LX/IFO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IFO;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554047
    return-void
.end method

.method public static a(LX/0QB;)LX/IFO;
    .locals 3

    .prologue
    .line 2554048
    sget-object v0, LX/IFO;->a:LX/IFO;

    if-nez v0, :cond_1

    .line 2554049
    const-class v1, LX/IFO;

    monitor-enter v1

    .line 2554050
    :try_start_0
    sget-object v0, LX/IFO;->a:LX/IFO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2554051
    if-eqz v2, :cond_0

    .line 2554052
    :try_start_1
    new-instance v0, LX/IFO;

    invoke-direct {v0}, LX/IFO;-><init>()V

    .line 2554053
    move-object v0, v0

    .line 2554054
    sput-object v0, LX/IFO;->a:LX/IFO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2554055
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2554056
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2554057
    :cond_1
    sget-object v0, LX/IFO;->a:LX/IFO;

    return-object v0

    .line 2554058
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2554059
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;)Lcom/facebook/location/ImmutableLocation;
    .locals 6
    .param p0    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2554060
    if-nez p0, :cond_1

    .line 2554061
    :cond_0
    :goto_0
    return-object v0

    .line 2554062
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->b()LX/1k1;

    move-result-object v1

    .line 2554063
    if-eqz v1, :cond_0

    .line 2554064
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;

    move-result-object v2

    .line 2554065
    if-eqz v2, :cond_0

    .line 2554066
    invoke-interface {v1}, LX/1k1;->a()D

    move-result-wide v4

    invoke-interface {v1}, LX/1k1;->b()D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Lcom/facebook/location/ImmutableLocation;->a(DD)LX/0z7;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel$AccuracyModel;->a()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, LX/0z7;->b(F)LX/0z7;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$IncomingLocationPingWithSenderModel;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0z7;->c(J)LX/0z7;

    move-result-object v0

    invoke-virtual {v0}, LX/0z7;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyLocationSharingFields;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554067
    if-nez p0, :cond_0

    .line 2554068
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554069
    :goto_0
    return-object v0

    .line 2554070
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object v0

    .line 2554071
    if-nez v0, :cond_1

    .line 2554072
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554073
    goto :goto_0

    .line 2554074
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    .line 2554075
    if-nez v0, :cond_2

    .line 2554076
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554077
    goto :goto_0

    .line 2554078
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v0

    .line 2554079
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2554080
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, p0, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2554081
    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2554082
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2554083
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2554084
    goto :goto_0
.end method

.method public static final f(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)I
    .locals 1

    .prologue
    .line 2554085
    invoke-static {p0}, LX/IFO;->o(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object v0

    .line 2554086
    if-nez v0, :cond_0

    .line 2554087
    const/4 v0, 0x0

    .line 2554088
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;
    .locals 1
    .param p0    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554089
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2554090
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->l()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object v0

    .line 2554091
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)LX/0Px;
    .locals 1
    .param p1    # Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryInterfaces$FriendsNearbyLocationSharingFields;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2554092
    const/4 v0, 0x0

    .line 2554093
    invoke-static {p1}, LX/IFO;->o(Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;)Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;

    move-result-object p0

    .line 2554094
    if-nez p0, :cond_4

    .line 2554095
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2554096
    if-eqz v0, :cond_2

    .line 2554097
    :cond_1
    :goto_1
    return-object v0

    .line 2554098
    :cond_2
    const/4 v0, 0x0

    .line 2554099
    if-nez p1, :cond_5

    .line 2554100
    :cond_3
    :goto_2
    move-object v0, v0

    .line 2554101
    if-nez v0, :cond_1

    .line 2554102
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2554103
    goto :goto_1

    .line 2554104
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel;->b()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;

    move-result-object p0

    .line 2554105
    if-eqz p0, :cond_0

    .line 2554106
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$UpsellModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2554107
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel;

    move-result-object p0

    .line 2554108
    if-eqz p0, :cond_3

    .line 2554109
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel$FriendsSharingLocationConnectionModel;

    move-result-object p0

    .line 2554110
    if-eqz p0, :cond_3

    .line 2554111
    invoke-virtual {p0}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyLocationSharingFieldsModel$FriendsSharingModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v0

    goto :goto_2
.end method
