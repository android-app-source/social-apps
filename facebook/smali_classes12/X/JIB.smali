.class public final LX/JIB;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JIC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:LX/Eoo;

.field public final synthetic g:LX/JIC;


# direct methods
.method public constructor <init>(LX/JIC;)V
    .locals 1

    .prologue
    .line 2677448
    iput-object p1, p0, LX/JIB;->g:LX/JIC;

    .line 2677449
    move-object v0, p1

    .line 2677450
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2677451
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2677452
    const-string v0, "DiscoveryContextRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2677453
    if-ne p0, p1, :cond_1

    .line 2677454
    :cond_0
    :goto_0
    return v0

    .line 2677455
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2677456
    goto :goto_0

    .line 2677457
    :cond_3
    check-cast p1, LX/JIB;

    .line 2677458
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2677459
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2677460
    if-eq v2, v3, :cond_0

    .line 2677461
    iget-object v2, p0, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    iget-object v3, p1, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2677462
    goto :goto_0

    .line 2677463
    :cond_5
    iget-object v2, p1, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    if-nez v2, :cond_4

    .line 2677464
    :cond_6
    iget-boolean v2, p0, LX/JIB;->b:Z

    iget-boolean v3, p1, LX/JIB;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2677465
    goto :goto_0

    .line 2677466
    :cond_7
    iget-boolean v2, p0, LX/JIB;->c:Z

    iget-boolean v3, p1, LX/JIB;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2677467
    goto :goto_0

    .line 2677468
    :cond_8
    iget-boolean v2, p0, LX/JIB;->d:Z

    iget-boolean v3, p1, LX/JIB;->d:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2677469
    goto :goto_0

    .line 2677470
    :cond_9
    iget-boolean v2, p0, LX/JIB;->e:Z

    iget-boolean v3, p1, LX/JIB;->e:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2677471
    goto :goto_0

    .line 2677472
    :cond_a
    iget-object v2, p0, LX/JIB;->f:LX/Eoo;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JIB;->f:LX/Eoo;

    iget-object v3, p1, LX/JIB;->f:LX/Eoo;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2677473
    goto :goto_0

    .line 2677474
    :cond_b
    iget-object v2, p1, LX/JIB;->f:LX/Eoo;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
