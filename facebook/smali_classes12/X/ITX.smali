.class public final LX/ITX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/ITZ;


# direct methods
.method public constructor <init>(LX/ITZ;)V
    .locals 0

    .prologue
    .line 2579344
    iput-object p1, p0, LX/ITX;->a:LX/ITZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x573283f3

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2579345
    iget-object v0, p0, LX/ITX;->a:LX/ITZ;

    iget-object v0, v0, LX/ITZ;->c:LX/ITj;

    iget-object v0, v0, LX/ITj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/ITX;->a:LX/ITZ;

    iget-object v0, v0, LX/ITZ;->c:LX/ITj;

    iget-object v0, v0, LX/ITj;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DTp;

    iget-object v1, p0, LX/ITX;->a:LX/ITZ;

    iget-object v1, v1, LX/ITZ;->c:LX/ITj;

    iget-object v1, v1, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/ITX;->a:LX/ITZ;

    iget-object v2, v2, LX/ITZ;->c:LX/ITj;

    iget-object v2, v2, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ITX;->a:LX/ITZ;

    iget-object v3, v3, LX/ITZ;->c:LX/ITj;

    iget-object v3, v3, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v4, p0, LX/ITX;->a:LX/ITZ;

    iget-object v4, v4, LX/ITZ;->c:LX/ITj;

    iget-object v4, v4, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/ITX;->a:LX/ITZ;

    iget-object v5, v5, LX/ITZ;->c:LX/ITj;

    iget-object v5, v5, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->F()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/DTp;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, LX/ITX;->a:LX/ITZ;

    iget-object v1, v1, LX/ITZ;->c:LX/ITj;

    iget-object v1, v1, LX/ITj;->v:Landroid/content/Context;

    invoke-interface {v6, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2579346
    const v0, 0x77774dfe

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
