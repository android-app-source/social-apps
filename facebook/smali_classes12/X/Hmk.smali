.class public final LX/Hmk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hm0;

.field public final synthetic b:LX/Hmo;


# direct methods
.method public constructor <init>(LX/Hmo;LX/Hm0;)V
    .locals 0

    .prologue
    .line 2501049
    iput-object p1, p0, LX/Hmk;->b:LX/Hmo;

    iput-object p2, p0, LX/Hmk;->a:LX/Hm0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 2501050
    iget-object v0, p0, LX/Hmk;->b:LX/Hmo;

    iget-object v0, v0, LX/Hmo;->c:LX/1Ml;

    .line 2501051
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_1

    .line 2501052
    const-string v1, "android.permission.CHANGE_NETWORK_STATE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2501053
    :goto_0
    move v0, v1

    .line 2501054
    if-nez v0, :cond_0

    .line 2501055
    iget-object v0, p0, LX/Hmk;->b:LX/Hmo;

    iget-object v0, v0, LX/Hmo;->c:LX/1Ml;

    .line 2501056
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.action.MANAGE_WRITE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2501057
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "package:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2501058
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2501059
    move-object v1, v1

    .line 2501060
    iget-object v2, v0, LX/1Ml;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2501061
    :cond_0
    iget-object v0, p0, LX/Hmk;->b:LX/Hmo;

    iget-object v1, p0, LX/Hmk;->b:LX/Hmo;

    iget-object v1, v1, LX/Hmo;->b:Landroid/content/Context;

    const/4 v3, 0x0

    .line 2501062
    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 2501063
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    .line 2501064
    :goto_1
    move v1, v2

    .line 2501065
    iput-boolean v1, v0, LX/Hmo;->d:Z

    .line 2501066
    iget-object v0, p0, LX/Hmk;->b:LX/Hmo;

    iget-object v0, v0, LX/Hmo;->b:Landroid/content/Context;

    iget-object v1, p0, LX/Hmk;->a:LX/Hm0;

    .line 2501067
    iget-object v2, v1, LX/Hm0;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2501068
    iget-object v2, p0, LX/Hmk;->a:LX/Hm0;

    .line 2501069
    iget-object v3, v2, LX/Hm0;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2501070
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 2501071
    if-nez v2, :cond_3

    move v3, v4

    .line 2501072
    :goto_2
    move v0, v3

    .line 2501073
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, v0, LX/1Ml;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/Settings$System;->canWrite(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0

    :cond_2
    invoke-static {v2, v3}, LX/Hlz;->a(Landroid/net/wifi/WifiManager;Z)Z

    move-result v2

    goto :goto_1

    .line 2501074
    :cond_3
    const-string v3, "wifi"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 2501075
    invoke-static {v0}, LX/Hlz;->b(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2501076
    invoke-static {v0, v4}, LX/Hlz;->a(Landroid/content/Context;Z)Z

    .line 2501077
    :cond_4
    new-instance v5, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v5}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 2501078
    iput-object v1, v5, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 2501079
    iget-object v6, v5, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2501080
    iput-object v2, v5, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 2501081
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "setWifiApEnabled"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/net/wifi/WifiConfiguration;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 2501082
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    const/4 v5, 0x1

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {v6, v3, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 2501083
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 2501084
    const-wide/16 v13, 0x4e20

    add-long/2addr v11, v13

    .line 2501085
    :goto_3
    invoke-static {v0}, LX/Hlz;->b(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    cmp-long v13, v13, v11

    if-gez v13, :cond_5

    .line 2501086
    const-wide/16 v13, 0xbb8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    goto :goto_3

    .line 2501087
    :catch_0
    goto :goto_3

    .line 2501088
    :cond_5
    invoke-static {v0}, LX/Hlz;->b(Landroid/content/Context;)Z

    move-result v11

    move v3, v11
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 2501089
    goto :goto_2

    .line 2501090
    :catch_1
    move-exception v3

    .line 2501091
    sget-object v5, LX/Hlz;->a:Ljava/lang/Class;

    const-string v6, "enableHotspot"

    invoke-static {v5, v6, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v4

    .line 2501092
    goto/16 :goto_2
.end method
