.class public LX/J93;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J93;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2652430
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2652431
    const-string v0, "profile/{%s}/info/inner"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->COLLECTIONS_SUMMARY_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2652432
    const-string v0, "app_section/{%s}/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    const-string v2, "section_id"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->COLLECTIONS_SECTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2652433
    const-string v0, "collection/{%s}/{%s}/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    const-string v2, "section_id"

    const-string v3, "collection_id"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->COLLECTIONS_COLLECTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2652434
    return-void
.end method

.method public static a(LX/0QB;)LX/J93;
    .locals 3

    .prologue
    .line 2652451
    sget-object v0, LX/J93;->a:LX/J93;

    if-nez v0, :cond_1

    .line 2652452
    const-class v1, LX/J93;

    monitor-enter v1

    .line 2652453
    :try_start_0
    sget-object v0, LX/J93;->a:LX/J93;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652454
    if-eqz v2, :cond_0

    .line 2652455
    :try_start_1
    new-instance v0, LX/J93;

    invoke-direct {v0}, LX/J93;-><init>()V

    .line 2652456
    move-object v0, v0

    .line 2652457
    sput-object v0, LX/J93;->a:LX/J93;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652458
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652459
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652460
    :cond_1
    sget-object v0, LX/J93;->a:LX/J93;

    return-object v0

    .line 2652461
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/JAV;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2652463
    invoke-interface {p0}, LX/JAV;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, LX/J93;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCollectionCurationUri"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2652442
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {p2, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2652443
    invoke-interface {p0}, LX/JAX;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2652444
    invoke-interface {p0}, LX/JAX;->b()Ljava/lang/String;

    move-result-object v0

    .line 2652445
    :cond_0
    :goto_0
    return-object v0

    .line 2652446
    :cond_1
    invoke-interface {p0}, LX/JBK;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {p2, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 2652447
    if-nez p1, :cond_3

    .line 2652448
    :cond_2
    :goto_1
    move v1, v1

    .line 2652449
    if-eqz v1, :cond_0

    .line 2652450
    invoke-interface {p0}, LX/JAX;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-interface {p0}, LX/JAX;->b()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_2

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MOVIES:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq p1, p2, :cond_4

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->BOOKS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq p1, p2, :cond_4

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->TV_SHOWS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq p1, p2, :cond_4

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->MUSIC:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne p1, p2, :cond_2

    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2652435
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2652436
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2652437
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2652438
    sget-object v0, LX/0ax;->eo:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v2

    aput-object p1, v3, v1

    const/4 v1, 0x2

    aput-object p2, v3, v1

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 2652439
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2652440
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2652441
    goto :goto_2
.end method
