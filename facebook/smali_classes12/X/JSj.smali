.class public final LX/JSj;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/JSy;

.field public final synthetic c:LX/JSl;

.field public final synthetic d:LX/1Pq;

.field public final synthetic e:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSy;LX/JSl;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2695860
    iput-object p1, p0, LX/JSj;->e:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    iput-object p2, p0, LX/JSj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/JSj;->b:LX/JSy;

    iput-object p4, p0, LX/JSj;->c:LX/JSl;

    iput-object p5, p0, LX/JSj;->d:LX/1Pq;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2695861
    iget-object v0, p0, LX/JSj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695862
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2695863
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2695864
    iget-object v0, p0, LX/JSj;->b:LX/JSy;

    .line 2695865
    iget-object v2, v0, LX/JSy;->i:LX/0Px;

    move-object v7, v2

    .line 2695866
    iget-object v0, p0, LX/JSj;->c:LX/JSl;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2695867
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move v4, v1

    .line 2695868
    :goto_1
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2695869
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2695870
    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JSe;

    .line 2695871
    iget-object v2, p0, LX/JSj;->e:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->h:LX/JTZ;

    new-instance v3, LX/JTc;

    iget-object v5, p0, LX/JSj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v5, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-direct {v3, v0}, LX/JTc;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v2, v3}, LX/JTZ;->a(LX/JTc;)LX/JTY;

    move-result-object v2

    .line 2695872
    iget-object v0, p0, LX/JSj;->e:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    iget-object v8, v0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->i:Lcom/facebook/feedplugins/musicstory/albums/SimpleMusicStoryInHScrollPartDefinition;

    new-instance v0, LX/JSn;

    iget-object v3, p0, LX/JSj;->c:LX/JSl;

    iget-object v5, p0, LX/JSj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v0 .. v5}, LX/JSn;-><init>(LX/JSe;LX/JTY;LX/JSl;ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v8, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2695873
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2695874
    goto :goto_0

    .line 2695875
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2695876
    iget-object v0, p0, LX/JSj;->c:LX/JSl;

    iput p1, v0, LX/JSl;->a:I

    .line 2695877
    iget-object v0, p0, LX/JSj;->e:Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;

    iget-object v1, p0, LX/JSj;->d:LX/1Pq;

    .line 2695878
    iget-object v2, v0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;->j:Ljava/util/concurrent/ExecutorService;

    new-instance p0, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition$2;

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition$2;-><init>(Lcom/facebook/feedplugins/musicstory/albums/AlbumSharePartDefinition;LX/1Pq;)V

    const p1, -0x4f1d63

    invoke-static {v2, p0, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2695879
    return-void
.end method
