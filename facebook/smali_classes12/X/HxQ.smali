.class public LX/HxQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/base/fragment/FbFragment;

.field public b:LX/1jv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1jv",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Hx6;

.field public d:LX/Hze;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522503
    sget-object v0, LX/Hx6;->UPCOMING:LX/Hx6;

    iput-object v0, p0, LX/HxQ;->c:LX/Hx6;

    .line 2522504
    return-void
.end method

.method public static a(LX/0QB;)LX/HxQ;
    .locals 3

    .prologue
    .line 2522505
    const-class v1, LX/HxQ;

    monitor-enter v1

    .line 2522506
    :try_start_0
    sget-object v0, LX/HxQ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2522507
    sput-object v2, LX/HxQ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2522508
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2522509
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2522510
    new-instance v0, LX/HxQ;

    invoke-direct {v0}, LX/HxQ;-><init>()V

    .line 2522511
    move-object v0, v0

    .line 2522512
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2522513
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HxQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2522514
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2522515
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/HxQ;)Z
    .locals 2

    .prologue
    .line 2522516
    iget-object v0, p0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2522517
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v0, v1

    .line 2522518
    if-nez v0, :cond_0

    iget-object v0, p0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2522519
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v1

    .line 2522520
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Hx6;)V
    .locals 4

    .prologue
    .line 2522521
    invoke-static {p0}, LX/HxQ;->c(LX/HxQ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2522522
    :cond_0
    :goto_0
    return-void

    .line 2522523
    :cond_1
    iget-object v0, p0, LX/HxQ;->d:LX/Hze;

    if-eqz v0, :cond_2

    .line 2522524
    iget-object v0, p0, LX/HxQ;->d:LX/Hze;

    invoke-virtual {v0}, LX/Hze;->a()V

    .line 2522525
    :cond_2
    iget-object v0, p0, LX/HxQ;->c:LX/Hx6;

    if-eq p1, v0, :cond_0

    .line 2522526
    iget-object v0, p0, LX/HxQ;->c:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_3

    .line 2522527
    iget-object v0, p0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    iget-object v1, p0, LX/HxQ;->c:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0k5;->a(I)V

    .line 2522528
    :cond_3
    iput-object p1, p0, LX/HxQ;->c:LX/Hx6;

    .line 2522529
    sget-object v0, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq p1, v0, :cond_0

    .line 2522530
    iget-object v0, p0, LX/HxQ;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    invoke-virtual {p1}, LX/Hx6;->ordinal()I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/HxQ;->b:LX/1jv;

    invoke-virtual {v0, v1, v2, v3}, LX/0k5;->b(ILandroid/os/Bundle;LX/1jv;)LX/0k9;

    goto :goto_0
.end method
