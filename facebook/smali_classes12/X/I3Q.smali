.class public final LX/I3Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hxb;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2531330
    iput-object p1, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2531331
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v0, :cond_0

    .line 2531332
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000f

    const-string v2, "EventsSuggestionsFragment"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2531333
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b:LX/1nQ;

    iget-object v1, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-virtual {v1}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531334
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v2, v3

    .line 2531335
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v2

    iget-object v3, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531336
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2531337
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1nQ;->a(Ljava/lang/String;IIZ)V

    .line 2531338
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V
    .locals 5

    .prologue
    .line 2531339
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->g:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_1

    .line 2531340
    :cond_0
    :goto_0
    return-void

    .line 2531341
    :cond_1
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v0, :cond_2

    .line 2531342
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->f:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x6000f

    const-string v2, "EventsSuggestionsFragment"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2531343
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->b:LX/1nQ;

    iget-object v1, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    invoke-virtual {v1}, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531344
    iget-object v3, v2, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v2, v3

    .line 2531345
    invoke-virtual {v2}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v2

    iget-object v3, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531346
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2531347
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1nQ;->a(Ljava/lang/String;IIZ)V

    .line 2531348
    :cond_2
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    .line 2531349
    iput-object p1, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 2531350
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    new-instance v1, LX/I3P;

    invoke-direct {v1, p0}, LX/I3P;-><init>(LX/I3Q;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 2531351
    iget-object v1, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    invoke-virtual {v1, v0}, LX/I3J;->a(Ljava/util/List;)V

    .line 2531352
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->b()LX/0ut;

    move-result-object v0

    invoke-interface {v0}, LX/0ut;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2531353
    iget-object v0, p0, LX/I3Q;->a:Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/suggestions/EventsSuggestionsFragment;->i:LX/I3J;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I3J;->a(Z)V

    goto :goto_0
.end method
