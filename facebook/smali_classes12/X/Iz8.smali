.class public final LX/Iz8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iz9;


# direct methods
.method public constructor <init>(LX/Iz9;)V
    .locals 0

    .prologue
    .line 2634199
    iput-object p1, p0, LX/Iz8;->a:LX/Iz9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634200
    check-cast p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    const/4 v1, 0x1

    .line 2634201
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2634202
    :goto_0
    iget-object v2, p0, LX/Iz8;->a:LX/Iz9;

    .line 2634203
    iput-boolean v1, v2, LX/Iz9;->d:Z

    .line 2634204
    iget-object v1, p0, LX/Iz8;->a:LX/Iz9;

    iget-object v1, v1, LX/Iz9;->c:LX/Iz5;

    invoke-virtual {v1}, LX/Iz5;->a()V

    .line 2634205
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2634206
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
