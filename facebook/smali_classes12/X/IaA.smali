.class public final LX/IaA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final synthetic a:LX/IaB;


# direct methods
.method public constructor <init>(LX/IaB;)V
    .locals 0

    .prologue
    .line 2590696
    iput-object p1, p0, LX/IaA;->a:LX/IaB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2590680
    const-string v1, "provider_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2590681
    const-string v1, "provider_page_fbid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2590682
    const-string v0, "provider_page_fbid"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, LX/IaA;->a:LX/IaB;

    iget-object v0, v0, LX/IaB;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2590683
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->newBuilder()LX/IZy;

    move-result-object v1

    const-string v2, "provider_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2590684
    iput-object v2, v1, LX/IZy;->a:Ljava/lang/String;

    .line 2590685
    move-object v1, v1

    .line 2590686
    iget-object v2, p0, LX/IaA;->a:LX/IaB;

    iget-object v2, v2, LX/IaB;->b:LX/IZq;

    invoke-virtual {v2}, LX/IZq;->a()Ljava/lang/String;

    move-result-object v2

    .line 2590687
    iput-object v2, v1, LX/IZy;->b:Ljava/lang/String;

    .line 2590688
    move-object v1, v1

    .line 2590689
    const-string v2, "native_sign_up_uri"

    .line 2590690
    iput-object v2, v1, LX/IZy;->c:Ljava/lang/String;

    .line 2590691
    move-object v1, v1

    .line 2590692
    iput-object v0, v1, LX/IZy;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2590693
    move-object v0, v1

    .line 2590694
    invoke-virtual {v0}, LX/IZy;->a()Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a(Landroid/content/Context;Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2590695
    :cond_1
    return-object v0
.end method
