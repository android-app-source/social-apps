.class public LX/JDA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/JDA;


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final b:LX/03V;

.field private final c:LX/J9L;

.field private final d:LX/JCx;

.field private final e:LX/J93;

.field private final f:LX/JDL;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/J9L;LX/JCx;LX/J93;LX/JDL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2663339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2663340
    iput-object p1, p0, LX/JDA;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2663341
    iput-object p2, p0, LX/JDA;->b:LX/03V;

    .line 2663342
    iput-object p3, p0, LX/JDA;->c:LX/J9L;

    .line 2663343
    iput-object p4, p0, LX/JDA;->d:LX/JCx;

    .line 2663344
    iput-object p5, p0, LX/JDA;->e:LX/J93;

    .line 2663345
    iput-object p6, p0, LX/JDA;->f:LX/JDL;

    .line 2663346
    return-void
.end method

.method public static a(LX/0QB;)LX/JDA;
    .locals 10

    .prologue
    .line 2663326
    sget-object v0, LX/JDA;->g:LX/JDA;

    if-nez v0, :cond_1

    .line 2663327
    const-class v1, LX/JDA;

    monitor-enter v1

    .line 2663328
    :try_start_0
    sget-object v0, LX/JDA;->g:LX/JDA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2663329
    if-eqz v2, :cond_0

    .line 2663330
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2663331
    new-instance v3, LX/JDA;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v6

    check-cast v6, LX/J9L;

    invoke-static {v0}, LX/JCx;->a(LX/0QB;)LX/JCx;

    move-result-object v7

    check-cast v7, LX/JCx;

    invoke-static {v0}, LX/J93;->a(LX/0QB;)LX/J93;

    move-result-object v8

    check-cast v8, LX/J93;

    invoke-static {v0}, LX/JDL;->a(LX/0QB;)LX/JDL;

    move-result-object v9

    check-cast v9, LX/JDL;

    invoke-direct/range {v3 .. v9}, LX/JDA;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/03V;LX/J9L;LX/JCx;LX/J93;LX/JDL;)V

    .line 2663332
    move-object v0, v3

    .line 2663333
    sput-object v0, LX/JDA;->g:LX/JDA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2663334
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2663335
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2663336
    :cond_1
    sget-object v0, LX/JDA;->g:LX/JDA;

    return-object v0

    .line 2663337
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2663338
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2663322
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2663323
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, p1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2663324
    const v1, 0x7f0a0974

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2663325
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2663317
    sget-object v0, LX/JD8;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663318
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized view type in createcollectionItemView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2663319
    :pswitch_1
    const v0, 0x7f0302aa

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2663320
    :goto_0
    return-object v0

    .line 2663321
    :pswitch_2
    const v0, 0x7f0302a4

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/JDA;Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;"
        }
    .end annotation

    .prologue
    .line 2663302
    invoke-static {p1}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;LX/1nG;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItemUri"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2663314
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2663315
    if-eqz v0, :cond_0

    .line 2663316
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLInterfaces$CollectionsAppSectionRequestableFields;",
            ")",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLInterfaces$CollectionsAppSectionRequestableField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2663310
    const/4 v0, 0x0

    .line 2663311
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v1, p0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->CONTACT_LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v1, p0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 2663312
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;->a()LX/0Px;

    move-result-object v0

    .line 2663313
    :cond_1
    return-object v0
.end method

.method private static a(LX/JBL;Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2663303
    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2663304
    :goto_0
    const v2, 0x7f0d0989

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2663305
    if-eqz v0, :cond_2

    .line 2663306
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2663307
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2663308
    goto :goto_0

    .line 2663309
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private static a(LX/JDA;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/View;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;LX/J9F;)V
    .locals 7
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLInterfaces$CollectionsAppSectionRequestableField;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;",
            "LX/9lP;",
            "LX/J9F;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2663347
    sget-object v0, LX/JD8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663348
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized view type in innerBindCollectionView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v0, p2

    .line 2663349
    check-cast v0, LX/JDP;

    iget-object v5, p0, LX/JDA;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    move-object v1, p3

    move-object v2, p6

    move-object v3, p7

    move-object v4, p5

    const/16 p5, 0x8

    const/4 p3, 0x0

    .line 2663350
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p0

    iget p1, v0, LX/JDP;->b:I

    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result p4

    move p2, p3

    .line 2663351
    :goto_0
    if-ge p2, p4, :cond_a

    .line 2663352
    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JDK;

    .line 2663353
    add-int/lit8 p1, p2, 0x1

    invoke-virtual {v0, p1}, LX/JDP;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    .line 2663354
    invoke-virtual {p1, p0, v2}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/JDK;LX/9lP;)V

    .line 2663355
    invoke-virtual {p1, p3}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setVisibility(I)V

    .line 2663356
    const p0, 0x7f0d0963

    invoke-virtual {p1, p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 2663357
    add-int/lit8 p1, p4, -0x1

    if-ne p2, p1, :cond_0

    .line 2663358
    const/4 p1, 0x4

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2663359
    :goto_1
    add-int/lit8 p2, p2, 0x1

    .line 2663360
    goto :goto_0

    .line 2663361
    :cond_0
    invoke-virtual {p0, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2663362
    :goto_2
    iget p1, v0, LX/JDP;->b:I

    if-ge p0, p1, :cond_1

    .line 2663363
    add-int/lit8 p1, p0, 0x1

    invoke-virtual {v0, p1}, LX/JDP;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p5}, Landroid/view/View;->setVisibility(I)V

    .line 2663364
    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    .line 2663365
    :cond_1
    if-eqz v4, :cond_9

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {v4, p0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2663366
    iget-object p0, v0, LX/JDP;->c:Landroid/view/View;

    invoke-virtual {p0, p3}, Landroid/view/View;->setVisibility(I)V

    .line 2663367
    const/16 v1, 0x8

    const/4 p6, 0x2

    const/4 p7, 0x1

    const/4 v4, 0x0

    .line 2663368
    iget-object p0, v0, LX/JDP;->c:Landroid/view/View;

    const p1, 0x7f0d0978

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 2663369
    iget-object p1, v0, LX/JDP;->c:Landroid/view/View;

    const p2, 0x7f0d0975

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 2663370
    iget-object p2, v0, LX/JDP;->c:Landroid/view/View;

    const p3, 0x7f0d0976

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    .line 2663371
    invoke-virtual {v2}, LX/9lP;->f()Z

    move-result p3

    if-eqz p3, :cond_b

    .line 2663372
    sget-object p3, LX/0ax;->cV:Ljava/lang/String;

    sget-object p4, LX/5Oz;->TIMELINE_ABOUT_FRIENDS_APP:LX/5Oz;

    invoke-virtual {p4}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 2663373
    const p4, 0x7f080faa

    invoke-virtual {p0, p4}, Landroid/widget/TextView;->setText(I)V

    .line 2663374
    const p0, 0x7f020279

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2663375
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663376
    invoke-virtual {p2, v1}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->setVisibility(I)V

    move-object p0, p3

    .line 2663377
    :goto_3
    iget-object p1, v0, LX/JDP;->c:Landroid/view/View;

    new-instance p2, LX/JDO;

    invoke-direct {p2, v0, v5, p0}, LX/JDO;-><init>(LX/JDP;Lcom/facebook/intent/feed/IFeedIntentBuilder;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663378
    :goto_4
    return-void

    .line 2663379
    :pswitch_1
    check-cast p2, LX/JDI;

    const/4 v4, 0x0

    .line 2663380
    if-eqz p4, :cond_2

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    .line 2663381
    :goto_5
    if-eqz p4, :cond_3

    .line 2663382
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    .line 2663383
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->FILLED:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->e()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2663384
    add-int/lit8 v0, v1, -0x1

    :goto_7
    move v1, v0

    .line 2663385
    goto :goto_6

    :cond_2
    move v0, v4

    .line 2663386
    goto :goto_5

    :cond_3
    move v1, v0

    .line 2663387
    :cond_4
    if-eqz p3, :cond_5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 2663388
    :goto_8
    add-int v0, v3, v1

    iget v1, p2, LX/JDI;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    move v5, v4

    move v6, v4

    .line 2663389
    :goto_9
    if-ge v6, p0, :cond_f

    .line 2663390
    invoke-virtual {p2, v6}, LX/JDI;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2663391
    check-cast v0, LX/JCi;

    .line 2663392
    if-ge v6, v3, :cond_e

    .line 2663393
    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2663394
    invoke-interface {v0, v2}, LX/JCi;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    .line 2663395
    :goto_a
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2663396
    const v0, 0x7f0d0963

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2663397
    add-int/lit8 v1, p0, -0x1

    if-ne v6, v1, :cond_7

    .line 2663398
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2663399
    :goto_b
    add-int/lit8 v6, v6, 0x1

    .line 2663400
    goto :goto_9

    :cond_5
    move v3, v4

    .line 2663401
    goto :goto_8

    :cond_6
    move v2, v5

    .line 2663402
    :goto_c
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_d

    .line 2663403
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    .line 2663404
    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->e()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object p1

    sget-object p7, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->FILLED:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {p1, p7}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 2663405
    invoke-interface {v0, v2, p6, p5}, LX/JCh;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    goto :goto_a

    .line 2663406
    :cond_7
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_b

    .line 2663407
    :goto_d
    iget v1, p2, LX/JDI;->a:I

    if-ge v0, v1, :cond_8

    .line 2663408
    invoke-virtual {p2, v0}, LX/JDI;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2663409
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 2663410
    :cond_8
    goto/16 :goto_4

    .line 2663411
    :pswitch_2
    check-cast p2, LX/JDa;

    invoke-virtual {p2, p3, p6, p5}, LX/JDa;->a(Ljava/util/List;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    goto/16 :goto_4

    .line 2663412
    :cond_9
    iget-object p0, v0, LX/JDP;->c:Landroid/view/View;

    invoke-virtual {p0, p5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_a
    move p0, p2

    goto/16 :goto_2

    .line 2663413
    :cond_b
    sget-object p3, LX/0ax;->bO:Ljava/lang/String;

    const/4 p4, 0x3

    new-array p4, p4, [Ljava/lang/Object;

    .line 2663414
    iget-object p5, v2, LX/9lP;->a:Ljava/lang/String;

    move-object p5, p5

    .line 2663415
    aput-object p5, p4, v4

    sget-object p5, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    invoke-virtual {p5}, LX/DHs;->name()Ljava/lang/String;

    move-result-object p5

    aput-object p5, p4, p7

    sget-object p5, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP_MUTUAL_FRIENDS_LINK:LX/DHr;

    invoke-virtual {p5}, LX/DHr;->name()Ljava/lang/String;

    move-result-object p5

    aput-object p5, p4, p6

    invoke-static {p3, p4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 2663416
    iget p4, v3, LX/J9F;->a:I

    move p4, p4

    .line 2663417
    if-lt p4, p6, :cond_c

    sget-object p4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2663418
    iget-object p5, v2, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object p5, p5

    .line 2663419
    invoke-virtual {p4, p5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_c

    .line 2663420
    invoke-virtual {v0}, LX/JDP;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const p5, 0x7f0f005c

    .line 2663421
    iget p6, v3, LX/J9F;->a:I

    move p6, p6

    .line 2663422
    new-array p7, p7, [Ljava/lang/Object;

    .line 2663423
    iget v1, v3, LX/J9F;->a:I

    move v1, v1

    .line 2663424
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p7, v4

    invoke-virtual {p4, p5, p6, p7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663425
    iget-object p0, v3, LX/J9F;->b:Ljava/util/List;

    move-object p0, p0

    .line 2663426
    invoke-virtual {p2, p0}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->a(Ljava/util/List;)V

    .line 2663427
    const/4 p0, 0x4

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663428
    invoke-virtual {p2, v4}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->setVisibility(I)V

    move-object p0, p3

    goto/16 :goto_3

    .line 2663429
    :cond_c
    iget-object p0, v0, LX/JDP;->c:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    move-object p0, p3

    goto/16 :goto_3

    :cond_d
    move v5, v2

    goto/16 :goto_a

    :cond_e
    move v2, v5

    goto/16 :goto_c

    :cond_f
    move v0, v6

    goto/16 :goto_d

    :cond_10
    move v0, v1

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/JDA;LX/JBL;LX/9lP;)Z
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "supportsCuration"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2663191
    if-nez p1, :cond_1

    .line 2663192
    :cond_0
    :goto_0
    return v0

    .line 2663193
    :cond_1
    invoke-interface {p1}, LX/JAW;->d()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v1

    .line 2663194
    invoke-interface {p1}, LX/JBK;->o()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    if-eq v1, v2, :cond_3

    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    if-ne v1, v2, :cond_0

    invoke-virtual {p2}, LX/9lP;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2663195
    sget-object v0, LX/JD8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized view type %s in createCollectionView"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2663197
    :pswitch_0
    new-instance v1, LX/JDI;

    const v2, 0x7f0302a2

    .line 2663198
    const/4 v3, 0x6

    move v3, v3

    .line 2663199
    invoke-direct {v1, p3, p2, v2, v3}, LX/JDI;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;II)V

    .line 2663200
    move-object v0, v1

    .line 2663201
    :goto_0
    return-object v0

    .line 2663202
    :pswitch_1
    new-instance v1, LX/JDI;

    const v2, 0x7f0302a4

    .line 2663203
    const/4 v3, 0x6

    move v3, v3

    .line 2663204
    invoke-direct {v1, p3, p2, v2, v3}, LX/JDI;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;II)V

    .line 2663205
    move-object v0, v1

    .line 2663206
    goto :goto_0

    .line 2663207
    :pswitch_2
    new-instance v0, LX/JDP;

    invoke-direct {v0, p3}, LX/JDP;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2663208
    :pswitch_3
    new-instance v1, LX/JDI;

    const v2, 0x7f0302aa

    .line 2663209
    const/4 v3, 0x2

    move v3, v3

    .line 2663210
    invoke-direct {v1, p3, p2, v2, v3}, LX/JDI;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;II)V

    .line 2663211
    move-object v0, v1

    .line 2663212
    goto :goto_0

    .line 2663213
    :pswitch_4
    const v0, 0x7f0302ac

    invoke-static {p3, p2, v0}, LX/JDa;->a(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;

    move-result-object v0

    goto :goto_0

    .line 2663214
    :pswitch_5
    const v0, 0x7f0302a5

    invoke-static {p3, p2, v0}, LX/JDa;->a(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;

    move-result-object v0

    goto :goto_0

    .line 2663215
    :pswitch_6
    new-instance v1, LX/JDI;

    const v2, 0x7f0302af

    .line 2663216
    const/4 v3, 0x4

    move v3, v3

    .line 2663217
    invoke-direct {v1, p3, p2, v2, v3}, LX/JDI;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;II)V

    .line 2663218
    move-object v0, v1

    .line 2663219
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I
    .locals 2

    .prologue
    .line 2663220
    sget-object v0, LX/JD8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2663222
    :pswitch_0
    const/4 v0, 0x6

    move v0, v0

    .line 2663223
    :goto_0
    return v0

    .line 2663224
    :pswitch_1
    const/4 v0, 0x6

    move v0, v0

    .line 2663225
    goto :goto_0

    .line 2663226
    :pswitch_2
    const/4 v0, 0x4

    move v0, v0

    .line 2663227
    goto :goto_0

    .line 2663228
    :pswitch_3
    const/4 v0, 0x2

    move v0, v0

    .line 2663229
    goto :goto_0

    .line 2663230
    :pswitch_4
    const/4 v0, 0x2

    move v0, v0

    .line 2663231
    iget-object v1, p0, LX/JDA;->c:LX/J9L;

    .line 2663232
    iget p0, v1, LX/J9L;->a:I

    move v1, p0

    .line 2663233
    mul-int/2addr v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2663234
    const v0, 0x7f0302ab

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2663235
    invoke-direct {p0, p1, p2, p3}, LX/JDA;->b(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 2663236
    const-string v1, "collectionsViewFactory_inner_view"

    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2663237
    const v1, 0x7f0d098a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 2663238
    if-eqz v1, :cond_0

    .line 2663239
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 2663240
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/Exception;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2663241
    iget-object v0, p0, LX/JDA;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2663242
    const-string v0, ""

    .line 2663243
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2663244
    if-eqz v1, :cond_0

    .line 2663245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rendering "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2663246
    if-eqz p3, :cond_0

    .line 2663247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2663248
    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 2663249
    const-string v2, "error_view"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 2663250
    const/16 v2, 0xc8

    invoke-static {v2, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2663251
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663252
    return-object v1
.end method

.method public final a(LX/JBL;Ljava/util/List;Landroid/view/View;LX/9lP;LX/J9F;LX/JAc;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;)V
    .locals 13
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindFullCollectionViewWithCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JBL;",
            "Ljava/util/List",
            "<*>;",
            "Landroid/view/View;",
            "LX/9lP;",
            "LX/J9F;",
            "LX/JAc;",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLInterfaces$CollectionsAppSectionMediaset;",
            "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLInterfaces$CollectionsAppSectionRequestableFields;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2663253
    invoke-interface {p1}, LX/JAW;->d()LX/0Px;

    move-result-object v1

    invoke-static {p0, v1}, LX/JDA;->a(LX/JDA;Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v11

    .line 2663254
    const-string v1, "collectionsViewFactory_inner_view"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v12

    .line 2663255
    if-nez v12, :cond_0

    .line 2663256
    :goto_0
    return-void

    .line 2663257
    :cond_0
    const v1, 0x7f0d0965

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2663258
    if-eqz v1, :cond_2

    .line 2663259
    invoke-interface {p1}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-interface/range {p6 .. p6}, LX/JAb;->d()Ljava/lang/String;

    move-result-object v3

    .line 2663260
    :goto_1
    const/4 v4, 0x0

    .line 2663261
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v2

    if-lez v2, :cond_1

    .line 2663262
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 2663263
    :cond_1
    move-object/from16 v0, p4

    invoke-static {p0, p1, v0}, LX/JDA;->a(LX/JDA;LX/JBL;LX/9lP;)Z

    move-result v2

    .line 2663264
    invoke-virtual {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->setHasCurateButton(Z)V

    .line 2663265
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->setTitleIsLink(Z)V

    .line 2663266
    invoke-interface/range {p6 .. p6}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p4 .. p4}, LX/9lP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface/range {p6 .. p6}, LX/JAb;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v2, v6}, LX/J93;->a(LX/JAV;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface/range {p6 .. p6}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v8

    invoke-static {p1, v8, v11}, LX/J93;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p6 .. p6}, LX/JAb;->b()LX/1Fb;

    move-result-object v9

    invoke-interface/range {p6 .. p6}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v10

    move-object v2, p1

    invoke-virtual/range {v1 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(LX/JBL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    .line 2663267
    :cond_2
    move-object/from16 v0, p3

    invoke-static {p1, v0}, LX/JDA;->a(LX/JBL;Landroid/view/View;)V

    .line 2663268
    move-object/from16 v0, p8

    invoke-static {v11, v0}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;)Ljava/util/List;

    move-result-object v5

    .line 2663269
    if-eqz p7, :cond_3

    .line 2663270
    const v1, 0x7f0d0221

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2663271
    :cond_3
    invoke-interface/range {p6 .. p6}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v6

    move-object v1, p0

    move-object v2, v11

    move-object v3, v12

    move-object v4, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-static/range {v1 .. v8}, LX/JDA;->a(LX/JDA;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/View;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;LX/J9F;)V

    goto/16 :goto_0

    .line 2663272
    :cond_4
    invoke-interface {p1}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;Landroid/view/View;LX/9lP;LX/J9F;LX/JCc;Ljava/lang/String;)V
    .locals 14
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionViewForAdapter"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2663273
    if-nez p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2663274
    :cond_0
    :goto_0
    return-void

    .line 2663275
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    .line 2663276
    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->d()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, LX/JDA;->a(LX/JDA;Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v12

    .line 2663277
    const-string v2, "collectionsViewFactory_inner_view"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v13

    .line 2663278
    if-eqz v13, :cond_0

    .line 2663279
    const v2, 0x7f0d0965

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2663280
    if-eqz v2, :cond_2

    .line 2663281
    move-object/from16 v0, p3

    invoke-static {p0, v3, v0}, LX/JDA;->a(LX/JDA;LX/JBL;LX/9lP;)Z

    move-result v4

    .line 2663282
    invoke-virtual {v2, v4}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->setHasCurateButton(Z)V

    .line 2663283
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->setTitleIsLink(Z)V

    .line 2663284
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->o()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_4

    invoke-interface {p1}, LX/JAb;->d()Ljava/lang/String;

    move-result-object v4

    .line 2663285
    :goto_1
    const/4 v5, 0x0

    invoke-interface {p1}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p5

    move-object/from16 v1, p3

    invoke-virtual {v0, p1, v1, v7}, LX/JCc;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;LX/9lP;Z)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-static {p1, v0}, LX/JCc;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v10

    invoke-static {v3, v10, v12}, LX/J93;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p1}, LX/JAb;->b()LX/1Fb;

    move-result-object v10

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v11

    invoke-virtual/range {v2 .. v11}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(LX/JBL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    .line 2663286
    :cond_2
    move-object/from16 v0, p2

    invoke-static {v3, v0}, LX/JDA;->a(LX/JBL;Landroid/view/View;)V

    .line 2663287
    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->mX_()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;

    move-result-object v2

    invoke-static {v12, v2}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;)Ljava/util/List;

    move-result-object v6

    .line 2663288
    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->e()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2663289
    const v2, 0x7f0d0221

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->e()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v2, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2663290
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v2, v12}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2663291
    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v2

    invoke-static {v3, v2}, LX/JDL;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0Px;

    move-result-object v5

    .line 2663292
    :goto_2
    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v7

    move-object v2, p0

    move-object v3, v12

    move-object v4, v13

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    invoke-static/range {v2 .. v9}, LX/JDA;->a(LX/JDA;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/View;Ljava/util/List;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;LX/J9F;)V

    goto/16 :goto_0

    .line 2663293
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->o()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2663294
    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2663295
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v5

    goto :goto_2
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I
    .locals 2

    .prologue
    .line 2663296
    sget-object v0, LX/JD8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663297
    invoke-virtual {p0, p1}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)I

    move-result v0

    :goto_0
    return v0

    .line 2663298
    :pswitch_0
    iget-object v0, p0, LX/JDA;->c:LX/J9L;

    .line 2663299
    iget v1, v0, LX/J9L;->a:I

    move v0, v1

    .line 2663300
    goto :goto_0

    .line 2663301
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
