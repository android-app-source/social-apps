.class public LX/I32;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/View;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/resources/ui/FbButton;

.field public j:Lcom/facebook/fbui/glyph/GlyphView;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:J

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2530945
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2530946
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/I32;->m:J

    .line 2530947
    const-class v0, LX/I32;

    invoke-static {v0, p0}, LX/I32;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2530948
    const v0, 0x7f03053f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2530949
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/I32;->setOrientation(I)V

    .line 2530950
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, LX/I32;->setBackgroundResource(I)V

    .line 2530951
    const v0, 0x7f0d0ec8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/I32;->e:Landroid/view/View;

    .line 2530952
    const v0, 0x7f0d0ec9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/I32;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2530953
    const v0, 0x7f0d0ecb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/I32;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2530954
    const v0, 0x7f0d0ecc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/I32;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2530955
    const v0, 0x7f0d0ecd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/I32;->i:Lcom/facebook/resources/ui/FbButton;

    .line 2530956
    iget-object v0, p0, LX/I32;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/I30;

    invoke-direct {v1, p0}, LX/I30;-><init>(LX/I32;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2530957
    const v0, 0x7f0d0eca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/I32;->j:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2530958
    iget-object v0, p0, LX/I32;->j:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/I31;

    invoke-direct {v1, p0}, LX/I31;-><init>(LX/I32;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2530959
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I32;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v1

    check-cast v1, LX/Bl6;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v2

    check-cast v2, LX/1nQ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, LX/I32;->a:LX/Bl6;

    iput-object v2, p1, LX/I32;->b:LX/1nQ;

    iput-object v3, p1, LX/I32;->c:LX/0tX;

    iput-object p0, p1, LX/I32;->d:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(LX/Hyy;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2530960
    if-nez p1, :cond_1

    .line 2530961
    :cond_0
    :goto_0
    return-void

    .line 2530962
    :cond_1
    iget-object v0, p1, LX/Hyy;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;

    move-object v3, v0

    .line 2530963
    if-eqz v3, :cond_0

    .line 2530964
    iput-object p2, p0, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2530965
    iget-object v4, p0, LX/I32;->e:Landroid/view/View;

    .line 2530966
    iget-boolean v0, p1, LX/Hyy;->b:Z

    move v0, v0

    .line 2530967
    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2530968
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2530969
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2530970
    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_2
    if-eqz v0, :cond_2

    .line 2530971
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/I32;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2530972
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I32;->k:Ljava/lang/String;

    .line 2530973
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/I32;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2530974
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v5, p0, LX/I32;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2530975
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    move-object v3, v0

    move v0, v1

    .line 2530976
    :goto_3
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2530977
    if-eqz v0, :cond_3

    .line 2530978
    iget-object v4, p0, LX/I32;->i:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2530979
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2530980
    if-eqz v0, :cond_3

    .line 2530981
    invoke-virtual {v3, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LX/I32;->l:Ljava/lang/String;

    .line 2530982
    invoke-virtual {v3, v0, v1}, LX/15i;->k(II)J

    move-result-wide v4

    iput-wide v4, p0, LX/I32;->m:J

    .line 2530983
    const-class v4, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;

    .line 2530984
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2530985
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;->c()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2530986
    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    if-eqz v3, :cond_c

    .line 2530987
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;->c()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2530988
    invoke-virtual {v4, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    :goto_5
    if-eqz v2, :cond_3

    .line 2530989
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/I32;->n:Ljava/lang/String;

    .line 2530990
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel$ActionLinksModel$TemporalEventInfoModel$ThemeModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I32;->o:Ljava/lang/String;

    .line 2530991
    :cond_3
    iget-object v0, p0, LX/I32;->b:LX/1nQ;

    iget-object v1, p0, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v3, p0, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iget-object v4, p0, LX/I32;->k:Ljava/lang/String;

    .line 2530992
    iget-object v5, v0, LX/1nQ;->i:LX/0Zb;

    const-string v6, "event_prompt_impression"

    const/4 p0, 0x0

    invoke-interface {v5, v6, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 2530993
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2530994
    const-string v6, "event_dashboard"

    invoke-virtual {v5, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object v6, v0, LX/1nQ;->j:LX/0kv;

    iget-object p0, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v6, p0}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "EventPrompt"

    invoke-virtual {v5, v6}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "ref_module"

    invoke-virtual {v5, v6, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "ref_mechanism"

    invoke-virtual {v5, v6, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "source_module"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    .line 2530995
    :cond_4
    goto/16 :goto_0

    .line 2530996
    :cond_5
    const/16 v0, 0x8

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 2530997
    goto/16 :goto_2

    :cond_7
    move v0, v1

    goto/16 :goto_2

    .line 2530998
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    goto/16 :goto_3

    .line 2530999
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2531000
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_9
    move v3, v1

    .line 2531001
    goto/16 :goto_4

    :cond_a
    move v3, v1

    goto/16 :goto_4

    :cond_b
    move v2, v1

    goto/16 :goto_5

    :cond_c
    move v2, v1

    goto/16 :goto_5
.end method
