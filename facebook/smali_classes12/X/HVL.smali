.class public LX/HVL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/03V;

.field private final d:LX/6Zi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2474989
    const-class v0, LX/HVL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HVL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/6Zi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2474990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2474991
    iput-object p1, p0, LX/HVL;->b:Landroid/content/Context;

    .line 2474992
    iput-object p2, p0, LX/HVL;->c:LX/03V;

    .line 2474993
    iput-object p3, p0, LX/HVL;->d:LX/6Zi;

    .line 2474994
    return-void
.end method

.method public static a(LX/0QB;)LX/HVL;
    .locals 6

    .prologue
    .line 2474995
    const-class v1, LX/HVL;

    monitor-enter v1

    .line 2474996
    :try_start_0
    sget-object v0, LX/HVL;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2474997
    sput-object v2, LX/HVL;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2474998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2474999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2475000
    new-instance p0, LX/HVL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/6Zi;->a(LX/0QB;)LX/6Zi;

    move-result-object v5

    check-cast v5, LX/6Zi;

    invoke-direct {p0, v3, v4, v5}, LX/HVL;-><init>(Landroid/content/Context;LX/03V;LX/6Zi;)V

    .line 2475001
    move-object v0, p0

    .line 2475002
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2475003
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HVL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2475004
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2475005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 10

    .prologue
    .line 2475006
    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    if-eqz v0, :cond_1

    .line 2475007
    iget-object v1, p0, LX/HVL;->d:LX/6Zi;

    iget-object v2, p0, LX/HVL;->b:Landroid/content/Context;

    const-string v3, "native_page_profile"

    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v4

    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->g:LX/1k1;

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v6

    iget-object v8, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;->b()Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual/range {v1 .. v9}, LX/6Zi;->b(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 2475008
    :goto_1
    return-void

    .line 2475009
    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    .line 2475010
    :cond_1
    iget-object v0, p0, LX/HVL;->c:LX/03V;

    sget-object v1, LX/HVL;->a:Ljava/lang/String;

    const-string v2, "Trying to link to get directions without any address or location"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
