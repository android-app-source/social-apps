.class public LX/HXF;
.super LX/1Cv;
.source ""


# instance fields
.field public a:J

.field public b:Landroid/content/Context;

.field public c:LX/17W;

.field public d:LX/9XE;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/9XE;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2478605
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2478606
    iput-object p1, p0, LX/HXF;->b:Landroid/content/Context;

    .line 2478607
    iput-object p2, p0, LX/HXF;->c:LX/17W;

    .line 2478608
    iput-object p3, p0, LX/HXF;->d:LX/9XE;

    .line 2478609
    new-instance v0, LX/HXE;

    invoke-direct {v0, p0}, LX/HXE;-><init>(LX/HXF;)V

    iput-object v0, p0, LX/HXF;->e:Landroid/view/View$OnClickListener;

    .line 2478610
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2478601
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2478602
    const v1, 0x7f030e5e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2478603
    iget-object v1, p0, LX/HXF;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2478604
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2478600
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2478599
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2478598
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2478596
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2478597
    if-eqz p2, :cond_0

    :goto_0
    return-object p2

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p3}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
