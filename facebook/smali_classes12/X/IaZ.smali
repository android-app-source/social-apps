.class public final LX/IaZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IZu;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2590951
    iput-object p1, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2590958
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2590959
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2590960
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "onSuccess"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2590952
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2590953
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    .line 2590954
    iput-object p1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2590955
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->x()Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V

    .line 2590956
    iget-object v0, p0, LX/IaZ;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-static {v0, p1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    .line 2590957
    return-void
.end method
