.class public final LX/ITC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/9N6;

.field public final synthetic c:LX/ITJ;


# direct methods
.method public constructor <init>(LX/ITJ;ZLX/9N6;)V
    .locals 0

    .prologue
    .line 2579059
    iput-object p1, p0, LX/ITC;->c:LX/ITJ;

    iput-boolean p2, p0, LX/ITC;->a:Z

    iput-object p3, p0, LX/ITC;->b:LX/9N6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2579056
    iget-boolean v0, p0, LX/ITC;->a:Z

    if-eqz v0, :cond_0

    .line 2579057
    iget-object v0, p0, LX/ITC;->c:LX/ITJ;

    iget-object v0, v0, LX/ITJ;->d:LX/3mF;

    iget-object v1, p0, LX/ITC;->b:LX/9N6;

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GROUP_JOIN"

    invoke-virtual {v0, v1, v2}, LX/3mF;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2579058
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ITC;->c:LX/ITJ;

    iget-object v0, v0, LX/ITJ;->d:LX/3mF;

    iget-object v1, p0, LX/ITC;->b:LX/9N6;

    invoke-interface {v1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GROUP_JOIN"

    invoke-virtual {v0, v1, v2}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
