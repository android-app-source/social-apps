.class public final enum LX/Igi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Igi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Igi;

.field public static final enum ALL:LX/Igi;

.field public static final enum SELECTED:LX/Igi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2601431
    new-instance v0, LX/Igi;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, LX/Igi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Igi;->ALL:LX/Igi;

    .line 2601432
    new-instance v0, LX/Igi;

    const-string v1, "SELECTED"

    invoke-direct {v0, v1, v3}, LX/Igi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Igi;->SELECTED:LX/Igi;

    .line 2601433
    const/4 v0, 0x2

    new-array v0, v0, [LX/Igi;

    sget-object v1, LX/Igi;->ALL:LX/Igi;

    aput-object v1, v0, v2

    sget-object v1, LX/Igi;->SELECTED:LX/Igi;

    aput-object v1, v0, v3

    sput-object v0, LX/Igi;->$VALUES:[LX/Igi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2601434
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Igi;
    .locals 1

    .prologue
    .line 2601435
    const-class v0, LX/Igi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Igi;

    return-object v0
.end method

.method public static values()[LX/Igi;
    .locals 1

    .prologue
    .line 2601436
    sget-object v0, LX/Igi;->$VALUES:[LX/Igi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Igi;

    return-object v0
.end method
