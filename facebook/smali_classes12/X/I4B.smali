.class public LX/I4B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;


# static fields
.field public static final H:Ljava/lang/String;


# instance fields
.field public A:LX/Clr;

.field public B:LX/Clr;

.field public C:LX/Clr;

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Clr;",
            ">;"
        }
    .end annotation
.end field

.field public E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Clr;",
            ">;"
        }
    .end annotation
.end field

.field public F:Landroid/os/Bundle;

.field public G:LX/Clr;

.field public a:LX/ClK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ckv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IXQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Clh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cs3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8bZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final r:Landroid/content/Context;

.field public s:Ljava/lang/String;

.field public t:LX/ClZ;

.field public u:LX/8Z4;

.field public v:LX/8Z4;

.field public w:LX/8Z4;

.field public x:LX/Clr;

.field public y:LX/Clr;

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Clr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2533770
    const-class v0, LX/I4B;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I4B;->H:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2533236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2533237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I4B;->z:Ljava/util/List;

    .line 2533238
    iput-object p1, p0, LX/I4B;->r:Landroid/content/Context;

    .line 2533239
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/I4B;->F:Landroid/os/Bundle;

    .line 2533240
    iget-object v1, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-static {p0, v1}, LX/I4B;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2533241
    return-void
.end method

.method private a(LX/B5a;)V
    .locals 12

    .prologue
    .line 2533734
    invoke-interface {p1}, LX/B5a;->F()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;

    move-result-object v0

    .line 2533735
    invoke-interface {p1}, LX/B5a;->M()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v1

    .line 2533736
    if-eqz v0, :cond_2

    .line 2533737
    invoke-interface {p1}, LX/B5a;->k()Ljava/lang/String;

    move-result-object v2

    .line 2533738
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2533739
    new-instance v3, LX/Cmb;

    invoke-direct {v3}, LX/Cmb;-><init>()V

    .line 2533740
    iput-object v2, v3, LX/Cmb;->a:Ljava/lang/String;

    .line 2533741
    move-object v2, v3

    .line 2533742
    invoke-static {v1}, LX/Crt;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2533743
    new-instance v3, LX/CmL;

    const/16 v4, 0x1f

    invoke-virtual {v2}, LX/Cmb;->a()LX/8Z4;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/CmL;-><init>(ILX/8Z4;)V

    sget-object v2, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    .line 2533744
    iput-object v2, v3, LX/CmL;->b:LX/Clb;

    .line 2533745
    move-object v2, v3

    .line 2533746
    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v2

    iget-object v3, p0, LX/I4B;->q:LX/8bZ;

    invoke-static {v3, v1}, LX/Crt;->a(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cn6;

    move-result-object v3

    .line 2533747
    iput-object v3, v2, LX/Cm7;->e:LX/Cml;

    .line 2533748
    move-object v2, v2

    .line 2533749
    invoke-virtual {v2}, LX/Cm7;->b()LX/Clr;

    move-result-object v2

    .line 2533750
    :goto_0
    iget-object v3, p0, LX/I4B;->z:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533751
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;->a()LX/0Px;

    move-result-object v10

    .line 2533752
    const/4 v2, 0x0

    move v9, v2

    :goto_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_2

    .line 2533753
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;

    .line 2533754
    add-int/lit8 v4, v9, 0x1

    .line 2533755
    iget-object v11, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v5, p0, LX/I4B;->s:Ljava/lang/String;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, LX/I4B;->q:LX/8bZ;

    move-object v2, v1

    invoke-static/range {v2 .. v8}, LX/IXf;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleEdgeModel;ILjava/lang/String;Ljava/lang/String;ILX/8bZ;)Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533756
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_COMPRESSED:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->HONEYBEE_FULL_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    if-eq v1, v2, :cond_1

    .line 2533757
    iget-object v2, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v3, LX/CmE;

    const/16 v4, 0x15

    invoke-direct {v3, v4}, LX/CmE;-><init>(I)V

    iget-object v4, p0, LX/I4B;->q:LX/8bZ;

    invoke-static {v4, v1}, LX/Crt;->a(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cn6;

    move-result-object v4

    .line 2533758
    iput-object v4, v3, LX/Cm7;->e:LX/Cml;

    .line 2533759
    move-object v3, v3

    .line 2533760
    invoke-virtual {v3}, LX/Cm7;->b()LX/Clr;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533761
    :cond_1
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_1

    .line 2533762
    :cond_2
    return-void

    .line 2533763
    :cond_3
    new-instance v3, LX/CmL;

    const/16 v4, 0x14

    invoke-virtual {v2}, LX/Cmb;->a()LX/8Z4;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/CmL;-><init>(ILX/8Z4;)V

    sget-object v2, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    .line 2533764
    iput-object v2, v3, LX/CmL;->b:LX/Clb;

    .line 2533765
    move-object v2, v3

    .line 2533766
    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v2

    iget-object v3, p0, LX/I4B;->q:LX/8bZ;

    invoke-static {v3, v1}, LX/Crt;->a(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cn6;

    move-result-object v3

    .line 2533767
    iput-object v3, v2, LX/Cm7;->e:LX/Cml;

    .line 2533768
    move-object v2, v2

    .line 2533769
    invoke-virtual {v2}, LX/Cm7;->b()LX/Clr;

    move-result-object v2

    goto :goto_0
.end method

.method private static a(LX/I4B;LX/ClK;LX/Ckv;LX/IXQ;LX/Cju;LX/03V;LX/0Ot;LX/0Ot;LX/Crz;LX/0Ot;LX/0Ot;LX/0Ot;LX/Clh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/8bZ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I4B;",
            "LX/ClK;",
            "LX/Ckv;",
            "LX/IXQ;",
            "LX/Cju;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/Crz;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/Clh;",
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cs3;",
            ">;",
            "LX/8bZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2533733
    iput-object p1, p0, LX/I4B;->a:LX/ClK;

    iput-object p2, p0, LX/I4B;->b:LX/Ckv;

    iput-object p3, p0, LX/I4B;->c:LX/IXQ;

    iput-object p4, p0, LX/I4B;->d:LX/Cju;

    iput-object p5, p0, LX/I4B;->e:LX/03V;

    iput-object p6, p0, LX/I4B;->f:LX/0Ot;

    iput-object p7, p0, LX/I4B;->g:LX/0Ot;

    iput-object p8, p0, LX/I4B;->h:LX/Crz;

    iput-object p9, p0, LX/I4B;->i:LX/0Ot;

    iput-object p10, p0, LX/I4B;->j:LX/0Ot;

    iput-object p11, p0, LX/I4B;->k:LX/0Ot;

    iput-object p12, p0, LX/I4B;->l:LX/Clh;

    iput-object p13, p0, LX/I4B;->m:LX/0Ot;

    iput-object p14, p0, LX/I4B;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/I4B;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/I4B;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/I4B;->q:LX/8bZ;

    return-void
.end method

.method public static a(LX/I4B;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;I)V
    .locals 8

    .prologue
    .line 2533712
    invoke-virtual {p2}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2533713
    invoke-virtual {p2}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleMasterModel$RelatedArticlesSectionsModel;->a()LX/0Px;

    move-result-object v1

    .line 2533714
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2533715
    :cond_0
    return-void

    .line 2533716
    :cond_1
    new-instance v2, LX/Cmb;

    invoke-direct {v2}, LX/Cmb;-><init>()V

    .line 2533717
    iput-object v0, v2, LX/Cmb;->a:Ljava/lang/String;

    .line 2533718
    move-object v2, v2

    .line 2533719
    invoke-static {p1}, LX/Crt;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x20

    .line 2533720
    :goto_0
    iget-object v3, p0, LX/I4B;->D:Ljava/util/List;

    new-instance v4, LX/CmL;

    invoke-virtual {v2}, LX/Cmb;->a()LX/8Z4;

    move-result-object v2

    invoke-direct {v4, v0, v2}, LX/CmL;-><init>(ILX/8Z4;)V

    sget-object v0, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    .line 2533721
    iput-object v0, v4, LX/CmL;->b:LX/Clb;

    .line 2533722
    move-object v0, v4

    .line 2533723
    iget-object v2, p0, LX/I4B;->q:LX/8bZ;

    invoke-static {v2, p1}, LX/Crt;->a(LX/8bZ;Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;)LX/Cn6;

    move-result-object v2

    .line 2533724
    iput-object v2, v0, LX/Cm7;->e:LX/Cml;

    .line 2533725
    move-object v0, v0

    .line 2533726
    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533727
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 2533728
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;

    .line 2533729
    add-int/lit8 v2, p3, 0x1

    .line 2533730
    iget-object v7, p0, LX/I4B;->D:Ljava/util/List;

    iget-object v3, p0, LX/I4B;->s:Ljava/lang/String;

    iget-object v5, p0, LX/I4B;->q:LX/8bZ;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/IXf;->a(Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticleModel;ILjava/lang/String;ILX/8bZ;)Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move p3, v2

    .line 2533731
    goto :goto_1

    .line 2533732
    :cond_2
    const/16 v0, 0x13

    goto :goto_0
.end method

.method public static a(LX/I4B;Ljava/lang/Object;Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;ILandroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2533628
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;

    if-eqz v0, :cond_5

    .line 2533629
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533630
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2533631
    :goto_0
    move v5, v0

    .line 2533632
    move v3, v1

    move v4, v1

    .line 2533633
    :goto_1
    if-ge v4, v5, :cond_4

    .line 2533634
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;

    if-eqz v0, :cond_a

    .line 2533635
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533636
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel;

    .line 2533637
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;

    move-result-object v0

    .line 2533638
    :goto_2
    move-object v0, v0

    .line 2533639
    invoke-static {v0}, LX/8bR;->b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v2

    .line 2533640
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v2, v6, :cond_2

    .line 2533641
    new-instance v6, LX/CmM;

    iget-object v7, p0, LX/I4B;->r:Landroid/content/Context;

    .line 2533642
    instance-of v2, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;

    if-eqz v2, :cond_f

    .line 2533643
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;->c()LX/8Z4;

    move-result-object v2

    .line 2533644
    :goto_3
    move-object v8, v2

    .line 2533645
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->ORDERED:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_4
    add-int/lit8 v2, v3, 0x1

    invoke-direct {v6, v7, v8, v0, v3}, LX/CmM;-><init>(Landroid/content/Context;LX/8Z4;ZI)V

    .line 2533646
    iput p3, v6, LX/CmM;->a:I

    .line 2533647
    move-object v3, v6

    .line 2533648
    iget-object v6, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533649
    iget-object v7, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v7

    .line 2533650
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533651
    iget-object v7, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v7

    .line 2533652
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    :goto_5
    iget-object v7, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v6, v0, v3, v7}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533653
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v3}, LX/CmL;->c()LX/Cly;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 2533654
    :goto_6
    add-int/lit8 v2, v4, 0x1

    move v3, v0

    move v4, v2

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2533655
    goto :goto_4

    .line 2533656
    :cond_1
    const/4 v0, 0x0

    goto :goto_5

    .line 2533657
    :cond_2
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->LIST_ITEM:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    if-ne v2, v6, :cond_3

    .line 2533658
    invoke-static {v0}, LX/8bR;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 2533659
    instance-of v6, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;

    if-eqz v6, :cond_14

    .line 2533660
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemEdgeModel$RichDocumentListItemModel;->e()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v6

    .line 2533661
    :goto_7
    move-object v0, v6

    .line 2533662
    add-int/lit8 v6, p3, 0x1

    invoke-static {p0, v2, v0, v6, p4}, LX/I4B;->a(LX/I4B;Ljava/lang/Object;Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;ILandroid/content/Context;)V

    :cond_3
    move v0, v3

    goto :goto_6

    .line 2533663
    :cond_4
    return-void

    .line 2533664
    :cond_5
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel;

    if-eqz v0, :cond_6

    .line 2533665
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533666
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto/16 :goto_0

    .line 2533667
    :cond_6
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel;

    if-eqz v0, :cond_7

    .line 2533668
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533669
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto/16 :goto_0

    .line 2533670
    :cond_7
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel;

    if-eqz v0, :cond_8

    .line 2533671
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533672
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto/16 :goto_0

    .line 2533673
    :cond_8
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel$NLENLENLModel;

    if-eqz v0, :cond_9

    .line 2533674
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533675
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto/16 :goto_0

    .line 2533676
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2533677
    :cond_a
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel;

    if-eqz v0, :cond_b

    .line 2533678
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533679
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel;

    .line 2533680
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;

    move-result-object v0

    goto/16 :goto_2

    .line 2533681
    :cond_b
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel;

    if-eqz v0, :cond_c

    .line 2533682
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533683
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel;

    .line 2533684
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;

    move-result-object v0

    goto/16 :goto_2

    .line 2533685
    :cond_c
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel;

    if-eqz v0, :cond_d

    .line 2533686
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533687
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel;

    .line 2533688
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;

    move-result-object v0

    goto/16 :goto_2

    .line 2533689
    :cond_d
    instance-of v0, p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel$NLENLENLModel;

    if-eqz v0, :cond_e

    .line 2533690
    invoke-static {p1}, LX/8bR;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2533691
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel$NLENLENLModel$NLENLENLEModel;

    .line 2533692
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel$NLENLENLModel$NLENLENLEModel;->a()LX/8Yx;

    move-result-object v0

    goto/16 :goto_2

    .line 2533693
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2533694
    :cond_f
    instance-of v2, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;

    if-eqz v2, :cond_10

    .line 2533695
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;->c()LX/8Z4;

    move-result-object v2

    goto/16 :goto_3

    .line 2533696
    :cond_10
    instance-of v2, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;

    if-eqz v2, :cond_11

    .line 2533697
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;->c()LX/8Z4;

    move-result-object v2

    goto/16 :goto_3

    .line 2533698
    :cond_11
    instance-of v2, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;

    if-eqz v2, :cond_12

    .line 2533699
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;->c()LX/8Z4;

    move-result-object v2

    goto/16 :goto_3

    .line 2533700
    :cond_12
    instance-of v2, v0, LX/8Yx;

    if-eqz v2, :cond_13

    .line 2533701
    check-cast v0, LX/8Yx;

    invoke-interface {v0}, LX/8Yx;->c()LX/8Z4;

    move-result-object v2

    goto/16 :goto_3

    .line 2533702
    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2533703
    :cond_14
    instance-of v6, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;

    if-eqz v6, :cond_15

    .line 2533704
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel;->e()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v6

    goto/16 :goto_7

    .line 2533705
    :cond_15
    instance-of v6, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;

    if-eqz v6, :cond_16

    .line 2533706
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel;->e()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v6

    goto/16 :goto_7

    .line 2533707
    :cond_16
    instance-of v6, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;

    if-eqz v6, :cond_17

    .line 2533708
    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentNestedListItemModel$LModel$EModel$NModel$NLModel$NLEModel$NLENModel$NLENLModel$NLENLEModel$NLENLENModel;->e()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v6

    goto/16 :goto_7

    .line 2533709
    :cond_17
    instance-of v6, v0, LX/8Yx;

    if-eqz v6, :cond_18

    .line 2533710
    check-cast v0, LX/8Yx;

    invoke-interface {v0}, LX/8Yx;->e()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v6

    goto/16 :goto_7

    .line 2533711
    :cond_18
    const/4 v6, 0x0

    goto/16 :goto_7
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, LX/I4B;

    invoke-static/range {v19 .. v19}, LX/ClK;->a(LX/0QB;)LX/ClK;

    move-result-object v3

    check-cast v3, LX/ClK;

    invoke-static/range {v19 .. v19}, LX/Ckv;->a(LX/0QB;)LX/Ckv;

    move-result-object v4

    check-cast v4, LX/Ckv;

    invoke-static/range {v19 .. v19}, LX/IXQ;->a(LX/0QB;)LX/IXQ;

    move-result-object v5

    check-cast v5, LX/IXQ;

    invoke-static/range {v19 .. v19}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v6

    check-cast v6, LX/Cju;

    invoke-static/range {v19 .. v19}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const/16 v8, 0x31dc

    move-object/from16 v0, v19

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1032

    move-object/from16 v0, v19

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v19 .. v19}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v10

    check-cast v10, LX/Crz;

    const/16 v11, 0x3227

    move-object/from16 v0, v19

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3222

    move-object/from16 v0, v19

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xac0

    move-object/from16 v0, v19

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v19 .. v19}, LX/Clh;->a(LX/0QB;)LX/Clh;

    move-result-object v14

    check-cast v14, LX/Clh;

    const/16 v15, 0x3231

    move-object/from16 v0, v19

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x3226

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x3216

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3245

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v19 .. v19}, LX/8bZ;->a(LX/0QB;)LX/8bZ;

    move-result-object v19

    check-cast v19, LX/8bZ;

    invoke-static/range {v2 .. v19}, LX/I4B;->a(LX/I4B;LX/ClK;LX/Ckv;LX/IXQ;LX/Cju;LX/03V;LX/0Ot;LX/0Ot;LX/Crz;LX/0Ot;LX/0Ot;LX/0Ot;LX/Clh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/8bZ;)V

    return-void
.end method

.method public static a(LX/8Ys;)Z
    .locals 1

    .prologue
    .line 2533627
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8Ys;->m()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;)Z
    .locals 1

    .prologue
    .line 2533626
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)LX/I4B;
    .locals 2

    .prologue
    .line 2533624
    iget-object v0, p0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v1, "version"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2533625
    return-object p0
.end method

.method public final a(LX/B5X;)LX/I4B;
    .locals 5

    .prologue
    .line 2533617
    if-eqz p1, :cond_1

    .line 2533618
    invoke-interface {p1}, LX/B5X;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B5b;

    .line 2533619
    invoke-interface {v0}, LX/B5b;->a()LX/B5a;

    move-result-object v0

    .line 2533620
    if-eqz v0, :cond_0

    .line 2533621
    iget-object v4, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {p0, v0, v4}, LX/I4B;->a(LX/B5a;Landroid/content/Context;)LX/I4B;

    .line 2533622
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2533623
    :cond_1
    return-object p0
.end method

.method public a(LX/B5a;Landroid/content/Context;)LX/I4B;
    .locals 11

    .prologue
    .line 2533406
    sget-object v0, LX/IXb;->a:[I

    invoke-interface {p1}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2533407
    :cond_0
    :goto_0
    return-object p0

    .line 2533408
    :pswitch_0
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v0

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2533409
    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v0

    invoke-interface {v0}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->BLOCKQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v0, v1, :cond_1

    .line 2533410
    const/16 v0, 0x11

    .line 2533411
    :goto_1
    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v1

    invoke-static {v1}, LX/Clb;->from(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)LX/Clb;

    move-result-object v1

    .line 2533412
    invoke-static {v1}, LX/Cjt;->from(LX/Clb;)LX/Cjt;

    move-result-object v1

    .line 2533413
    new-instance v2, LX/CmL;

    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/CmL;-><init>(ILX/8Z4;)V

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v0

    .line 2533414
    iget-object v2, p0, LX/I4B;->l:LX/Clh;

    invoke-virtual {v2, v1}, LX/Clh;->a(LX/Cjt;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    .line 2533415
    iget-object v2, p0, LX/I4B;->l:LX/Clh;

    invoke-virtual {v2, v1, v0, p2}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533416
    iget-object v1, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v0}, LX/Cm7;->b()LX/Clr;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533417
    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v0

    invoke-interface {v0}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->PULLQUOTE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v0, v1, :cond_0

    .line 2533418
    new-instance v0, LX/CmY;

    invoke-interface {p1}, LX/B5a;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CmY;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)V

    sget-object v1, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    .line 2533419
    iput-object v1, v0, LX/CmY;->b:LX/Clb;

    .line 2533420
    move-object v1, v0

    .line 2533421
    iget-object v2, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533422
    iget-object v3, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v3

    .line 2533423
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    iget-object v3, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v2, v0, v1, v3}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533424
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v1}, LX/CmY;->c()LX/CmZ;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533425
    :cond_1
    invoke-interface {p1}, LX/B5a;->p()LX/8Z4;

    move-result-object v0

    invoke-interface {v0}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedBlockType;->CODE:Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    if-ne v0, v1, :cond_2

    .line 2533426
    const/16 v0, 0x12

    goto :goto_1

    .line 2533427
    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 2533428
    :pswitch_1
    invoke-interface {p1}, LX/B5a;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;

    move-result-object v0

    invoke-interface {p1}, LX/B5a;->w()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2, p2}, LX/I4B;->a(LX/I4B;Ljava/lang/Object;Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;ILandroid/content/Context;)V

    .line 2533429
    goto/16 :goto_0

    .line 2533430
    :pswitch_2
    check-cast p1, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;

    .line 2533431
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->R()LX/0Px;

    move-result-object v2

    .line 2533432
    iget-object v0, p0, LX/I4B;->l:LX/Clh;

    sget-object v1, LX/Cjt;->TEXT_BODY:LX/Cjt;

    invoke-virtual {v0, v1}, LX/Clh;->a(LX/Cjt;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v3

    .line 2533433
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2533434
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2533435
    new-instance v5, LX/B5d;

    invoke-direct {v5}, LX/B5d;-><init>()V

    .line 2533436
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->O()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2533437
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->P()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533438
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->c:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 2533439
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->Q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533440
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->e()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->e:Ljava/lang/String;

    .line 2533441
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->iU_()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->f:Ljava/lang/String;

    .line 2533442
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->k()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->g:Ljava/lang/String;

    .line 2533443
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->R()LX/0Px;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->h:LX/0Px;

    .line 2533444
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->S()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->i:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533445
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->m()I

    move-result p2

    iput p2, v5, LX/B5d;->j:I

    .line 2533446
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->n()I

    move-result p2

    iput p2, v5, LX/B5d;->k:I

    .line 2533447
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->l:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    .line 2533448
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->T()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2533449
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->U()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBVideoModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->n:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBVideoModel;

    .line 2533450
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->q()Z

    move-result p2

    iput-boolean p2, v5, LX/B5d;->o:Z

    .line 2533451
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->p:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2533452
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->q:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 2533453
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->t()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->r:Ljava/lang/String;

    .line 2533454
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->u()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->s:Ljava/lang/String;

    .line 2533455
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->V()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->t:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentListItemsModel$ListElementsModel;

    .line 2533456
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->w()Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->u:Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    .line 2533457
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->W()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->v:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533458
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->y()LX/0Px;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->w:LX/0Px;

    .line 2533459
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->x:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 2533460
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->y:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 2533461
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->z:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    .line 2533462
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->X()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->A:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    .line 2533463
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->Y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->B:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2533464
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->Z()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->C:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoModel;

    .line 2533465
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->D:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2533466
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->aa()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->E:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$RelatedArticleObjsModel;

    .line 2533467
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->ab()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->F:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    .line 2533468
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->ac()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->G:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533469
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->ad()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->H:Ljava/lang/String;

    .line 2533470
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->ae()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->I:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533471
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->J()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->J:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 2533472
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->K()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->K:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 2533473
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->L()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->L:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 2533474
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->M()Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->M:Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    .line 2533475
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleSectionEdgeModel$InstantArticleSectionModel;->N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object p2

    iput-object p2, v5, LX/B5d;->N:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 2533476
    move-object v5, v5

    .line 2533477
    iput-object v0, v5, LX/B5d;->m:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel;

    .line 2533478
    const/4 p2, 0x0

    .line 2533479
    iput-object p2, v5, LX/B5d;->h:LX/0Px;

    .line 2533480
    new-instance v5, LX/CmL;

    invoke-direct {v5, v0}, LX/CmL;-><init>(LX/8Z4;)V

    .line 2533481
    if-eqz v3, :cond_3

    .line 2533482
    iget-object v0, p0, LX/I4B;->l:LX/Clh;

    iget-object p2, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v0, v3, v5, p2}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533483
    :cond_3
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v5}, LX/CmL;->c()LX/Cly;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533484
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 2533485
    :cond_5
    goto/16 :goto_0

    .line 2533486
    :pswitch_3
    invoke-interface {p1}, LX/B5a;->em_()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/B5a;->em_()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2533487
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v1, p0, LX/I4B;->s:Ljava/lang/String;

    iget-object v2, p0, LX/I4B;->n:LX/0Ot;

    invoke-static {p1, v1, v2}, LX/IXf;->a(LX/B5a;Ljava/lang/String;LX/0Ot;)LX/Clw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533488
    :pswitch_4
    invoke-interface {p1}, LX/B5a;->j()LX/8Ys;

    move-result-object v0

    invoke-static {v0}, LX/I4B;->a(LX/8Ys;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2533489
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-static {p1}, LX/IXf;->a(LX/B5a;)LX/Cm4;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533490
    :pswitch_5
    invoke-interface {p1}, LX/B5a;->G()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object v0

    invoke-static {v0}, LX/I4B;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2533491
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v1, p0, LX/I4B;->r:Landroid/content/Context;

    .line 2533492
    invoke-interface {p1}, LX/B5a;->G()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;->a()LX/0Px;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v3

    invoke-static {v2, v1, v3}, LX/IXf;->a(LX/0Px;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Clo;

    move-result-object v2

    .line 2533493
    new-instance v3, LX/Cmd;

    invoke-interface {p1}, LX/B5a;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v4

    invoke-direct {v3, v2, v4}, LX/Cmd;-><init>(LX/Clo;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V

    invoke-interface {p1}, LX/B5a;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    .line 2533494
    iput-object v2, v3, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533495
    move-object v2, v3

    .line 2533496
    invoke-interface {p1}, LX/B5a;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v3

    .line 2533497
    iput-object v3, v2, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533498
    move-object v2, v2

    .line 2533499
    invoke-interface {p1}, LX/B5a;->I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 2533500
    iput-object v3, v2, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533501
    move-object v2, v2

    .line 2533502
    invoke-interface {p1}, LX/B5a;->H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 2533503
    iput-object v3, v2, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533504
    move-object v2, v2

    .line 2533505
    invoke-interface {p1}, LX/B5a;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v4

    invoke-interface {p1}, LX/B5a;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v2

    invoke-virtual {v2}, LX/Cm7;->b()LX/Clr;

    move-result-object v2

    check-cast v2, LX/Cm3;

    move-object v1, v2

    .line 2533506
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533507
    :pswitch_6
    iget-object v0, p0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2533508
    invoke-interface {p1}, LX/B5a;->N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->TRACKER:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v0, v2, :cond_7

    .line 2533509
    iget-object v0, p0, LX/I4B;->c:LX/IXQ;

    invoke-interface {p1}, LX/B5a;->iU_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/B5a;->t()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v3

    .line 2533510
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2533511
    :cond_6
    :goto_3
    goto/16 :goto_0

    .line 2533512
    :cond_7
    invoke-interface {p1}, LX/B5a;->iU_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, LX/B5a;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2533513
    :cond_8
    iget-object v2, p0, LX/I4B;->a:LX/ClK;

    iget-object v0, p0, LX/I4B;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/I4B;->z:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p1}, LX/B5a;->N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v5

    .line 2533514
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2533515
    :goto_4
    iget-object v2, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v0, p0, LX/I4B;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v3, 0x3dd

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v3

    iget-object v0, p0, LX/I4B;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cs3;

    iget-object v4, p0, LX/I4B;->q:LX/8bZ;

    const/4 v9, 0x0

    .line 2533516
    invoke-interface {p1}, LX/B5a;->N()Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v6

    .line 2533517
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-ne v6, v5, :cond_10

    if-eqz v3, :cond_10

    .line 2533518
    new-instance v5, LX/Cmh;

    const/16 v7, 0x19

    invoke-interface {p1}, LX/B5a;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v8

    invoke-direct {v5, v7, v6, v8}, LX/Cmh;-><init>(ILcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    .line 2533519
    :goto_5
    new-instance v7, LX/Cmy;

    invoke-interface {p1}, LX/B5a;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v8

    invoke-static {v6, v8}, LX/Cs3;->a(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)Z

    move-result v8

    .line 2533520
    invoke-virtual {v4}, LX/8bZ;->f()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-virtual {v4}, LX/8bZ;->c()Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2533521
    const/high16 v10, 0x3f800000    # 1.0f

    .line 2533522
    invoke-virtual {v4}, LX/8bZ;->c()Z

    move-result p2

    if-eqz p2, :cond_13

    sget-object p2, LX/8bZ;->a:Ljava/util/Map;

    invoke-interface {p2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_13

    sget-object p2, LX/8bZ;->a:Ljava/util/Map;

    invoke-interface {p2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/8bX;

    iget p2, p2, LX/8bX;->a:I

    :goto_6
    move p2, p2

    .line 2533523
    invoke-static {v10, p2}, LX/Cmv;->a(FI)LX/Cmv;

    move-result-object p2

    .line 2533524
    new-instance v10, LX/Cmw;

    sget-object v3, LX/Cmv;->a:LX/Cmv;

    sget-object v0, LX/Cmv;->a:LX/Cmv;

    invoke-direct {v10, p2, v3, p2, v0}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    .line 2533525
    :goto_7
    move-object v6, v10

    .line 2533526
    const/4 v8, 0x0

    invoke-direct {v7, v6, v9, v9, v8}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    .line 2533527
    invoke-interface {p1}, LX/B5a;->iU_()Ljava/lang/String;

    move-result-object v6

    .line 2533528
    iput-object v6, v5, LX/Cmh;->a:Ljava/lang/String;

    .line 2533529
    move-object v5, v5

    .line 2533530
    invoke-interface {p1}, LX/B5a;->t()Ljava/lang/String;

    move-result-object v6

    .line 2533531
    iput-object v6, v5, LX/Cmh;->b:Ljava/lang/String;

    .line 2533532
    iput-object v1, v5, LX/Cmh;->c:Ljava/lang/String;

    .line 2533533
    move-object v5, v5

    .line 2533534
    invoke-interface {p1}, LX/B5a;->D()LX/8Yr;

    move-result-object v6

    .line 2533535
    iput-object v6, v5, LX/Cmh;->f:LX/8Yr;

    .line 2533536
    move-object v5, v5

    .line 2533537
    invoke-interface {p1}, LX/B5a;->q()Z

    move-result v6

    .line 2533538
    iput-boolean v6, v5, LX/Cmh;->i:Z

    .line 2533539
    move-object v5, v5

    .line 2533540
    invoke-interface {p1}, LX/B5a;->m()I

    move-result v6

    .line 2533541
    iput v6, v5, LX/Cmh;->d:I

    .line 2533542
    move-object v5, v5

    .line 2533543
    invoke-interface {p1}, LX/B5a;->n()I

    move-result v6

    .line 2533544
    iput v6, v5, LX/Cmh;->e:I

    .line 2533545
    move-object v5, v5

    .line 2533546
    invoke-interface {p1}, LX/B5a;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v6

    .line 2533547
    iput-object v6, v5, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533548
    move-object v5, v5

    .line 2533549
    invoke-interface {p1}, LX/B5a;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v6

    .line 2533550
    iput-object v6, v5, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533551
    move-object v5, v5

    .line 2533552
    invoke-interface {p1}, LX/B5a;->I()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v6

    .line 2533553
    iput-object v6, v5, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533554
    move-object v5, v5

    .line 2533555
    invoke-interface {p1}, LX/B5a;->H()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v6

    .line 2533556
    iput-object v6, v5, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533557
    move-object v5, v5

    .line 2533558
    invoke-interface {p1}, LX/B5a;->e()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, LX/B5a;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v8

    invoke-interface {p1}, LX/B5a;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v9

    invoke-virtual {v5, v6, v8, v9}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v5

    invoke-interface {p1}, LX/B5a;->s()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v6

    invoke-interface {p1}, LX/B5a;->r()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v5

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v5

    .line 2533559
    iput-object v7, v5, LX/Cm7;->e:LX/Cml;

    .line 2533560
    move-object v5, v5

    .line 2533561
    invoke-virtual {v5}, LX/Cm7;->b()LX/Clr;

    move-result-object v5

    check-cast v5, LX/Cmi;

    move-object v0, v5

    .line 2533562
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533563
    :pswitch_7
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmR;

    invoke-interface {p1}, LX/B5a;->y()LX/0Px;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->z()Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->m()I

    move-result v4

    invoke-interface {p1}, LX/B5a;->E()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, LX/CmR;-><init>(LX/0Px;Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;ILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v1

    invoke-virtual {v1}, LX/Cm7;->b()LX/Clr;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533564
    :pswitch_8
    invoke-direct {p0, p1}, LX/I4B;->a(LX/B5a;)V

    goto/16 :goto_0

    .line 2533565
    :pswitch_9
    const/4 v1, 0x0

    .line 2533566
    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v0

    .line 2533567
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_9
    move v0, v1

    .line 2533568
    :goto_8
    move v0, v0

    .line 2533569
    if-eqz v0, :cond_0

    .line 2533570
    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v0

    .line 2533571
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    if-ne v0, v1, :cond_16

    const/4 v0, 0x1

    :goto_9
    move v0, v0

    .line 2533572
    if-eqz v0, :cond_a

    .line 2533573
    iget-object v0, p0, LX/I4B;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    invoke-virtual {v0}, LX/8bL;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Clz;->valueOf(Ljava/lang/String;)LX/Clz;

    move-result-object v0

    .line 2533574
    sget-object v1, LX/IXb;->b:[I

    invoke-virtual {v0}, LX/Clz;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 2533575
    :goto_a
    goto/16 :goto_0

    .line 2533576
    :cond_a
    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v0

    .line 2533577
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->EMAIL_SIGNUP:Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    if-ne v0, v1, :cond_17

    const/4 v0, 0x1

    :goto_b
    move v0, v0

    .line 2533578
    if-eqz v0, :cond_0

    .line 2533579
    iget-object v0, p0, LX/I4B;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    .line 2533580
    iget-object v1, v0, LX/8bL;->a:LX/0Uh;

    const/16 v2, 0xaf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 2533581
    const-string v1, "GROWTH_EXPERIMENT"

    .line 2533582
    :goto_c
    move-object v0, v1

    .line 2533583
    invoke-static {v0}, LX/Clt;->valueOf(Ljava/lang/String;)LX/Clt;

    move-result-object v0

    .line 2533584
    sget-object v1, LX/IXb;->c:[I

    invoke-virtual {v0}, LX/Clt;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_2

    .line 2533585
    :goto_d
    goto/16 :goto_0

    .line 2533586
    :pswitch_a
    iget-object v0, p0, LX/I4B;->b:LX/Ckv;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ckv;->a(Ljava/lang/String;)V

    .line 2533587
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    const/4 v5, 0x0

    .line 2533588
    new-instance v1, LX/CmT;

    const/16 v2, 0x17

    invoke-direct {v1, v2}, LX/CmT;-><init>(I)V

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v1

    new-instance v2, LX/Cmy;

    sget-object v3, LX/Cmw;->b:LX/Cmw;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v5, v5, v4}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    .line 2533589
    iput-object v2, v1, LX/Cm7;->e:LX/Cml;

    .line 2533590
    move-object v1, v1

    .line 2533591
    invoke-virtual {v1}, LX/Cm7;->b()LX/Clr;

    move-result-object v1

    check-cast v1, LX/CmU;

    move-object v1, v1

    .line 2533592
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2533593
    :cond_b
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2533594
    :cond_c
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2533595
    new-instance v4, LX/IXL;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v2, v3}, LX/IXL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v4

    .line 2533596
    :goto_e
    iget-object v5, v0, LX/IXQ;->l:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2533597
    :cond_d
    new-instance v4, LX/IXL;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5, v3}, LX/IXL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v4

    .line 2533598
    goto :goto_e

    .line 2533599
    :cond_e
    iget-object v6, v2, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 2533600
    if-nez v6, :cond_f

    .line 2533601
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2533602
    iget-object p2, v2, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {p2, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2533603
    :cond_f
    new-instance p2, LX/ClJ;

    invoke-direct {p2, v4, v5}, LX/ClJ;-><init>(ILcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;)V

    .line 2533604
    invoke-interface {v6, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 2533605
    :cond_10
    new-instance v5, LX/Cmh;

    invoke-interface {p1}, LX/B5a;->A()Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/Cmh;-><init>(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    goto/16 :goto_5

    .line 2533606
    :cond_11
    invoke-virtual {v4}, LX/8bZ;->a()Z

    move-result v10

    if-nez v10, :cond_12

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->AD:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    if-eq v6, v10, :cond_12

    if-eqz v8, :cond_12

    .line 2533607
    sget-object v10, LX/Cmw;->a:LX/Cmw;

    goto/16 :goto_7

    .line 2533608
    :cond_12
    sget-object v10, LX/Cmw;->b:LX/Cmw;

    goto/16 :goto_7

    :cond_13
    const p2, 0x7f0d0128

    goto/16 :goto_6

    .line 2533609
    :cond_14
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;

    .line 2533610
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel$LeadGenDataModel$InfoFieldsDataModel;->a()LX/0Px;

    move-result-object v0

    .line 2533611
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    goto/16 :goto_8

    :cond_15
    move v0, v1

    goto/16 :goto_8

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 2533612
    :pswitch_b
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmJ;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CmJ;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 2533613
    :pswitch_c
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmK;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CmK;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 2533614
    :pswitch_d
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmJ;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CmJ;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_b

    .line 2533615
    :pswitch_e
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmH;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CmH;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d

    .line 2533616
    :pswitch_f
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    new-instance v1, LX/CmI;

    invoke-interface {p1}, LX/B5a;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/B5a;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;

    move-result-object v3

    invoke-interface {p1}, LX/B5a;->B()Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CmI;-><init>(Ljava/lang/String;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentEmailCTASubscriptionOptionModel$OptionLeadGenDataModel;Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d

    :cond_18
    iget-object v1, v0, LX/8bL;->b:LX/0ad;

    sget-char v2, LX/2yD;->n:C

    const-string v3, "BASIC_LAYOUT"

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_c

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final a(Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;)LX/I4B;
    .locals 9

    .prologue
    .line 2533340
    if-eqz p1, :cond_0

    .line 2533341
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2533342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cover media type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->k()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2533343
    :sswitch_0
    iget-object v0, p0, LX/I4B;->s:Ljava/lang/String;

    iget-object v1, p0, LX/I4B;->n:LX/0Ot;

    .line 2533344
    new-instance v2, LX/CmG;

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->em_()LX/8Yr;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->q()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, LX/CmG;-><init>(LX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/0Ot;)V

    .line 2533345
    iput-object v0, v2, LX/CmG;->g:Ljava/lang/String;

    .line 2533346
    move-object v2, v2

    .line 2533347
    const/4 v3, 0x1

    .line 2533348
    iput-boolean v3, v2, LX/CmG;->e:Z

    .line 2533349
    move-object v2, v2

    .line 2533350
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->iR_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 2533351
    iput-object v3, v2, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533352
    move-object v2, v2

    .line 2533353
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v3

    .line 2533354
    iput-object v3, v2, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533355
    move-object v2, v2

    .line 2533356
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 2533357
    iput-object v3, v2, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533358
    move-object v2, v2

    .line 2533359
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v3

    .line 2533360
    iput-object v3, v2, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533361
    move-object v2, v2

    .line 2533362
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v2

    invoke-virtual {v2}, LX/Cm7;->b()LX/Clr;

    move-result-object v2

    check-cast v2, LX/Clw;

    move-object v0, v2

    .line 2533363
    iput-object v0, p0, LX/I4B;->y:LX/Clr;

    .line 2533364
    :cond_0
    :goto_0
    return-object p0

    .line 2533365
    :sswitch_1
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->j()LX/8Ys;

    move-result-object v0

    invoke-static {v0}, LX/I4B;->a(LX/8Ys;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2533366
    new-instance v3, LX/Cmf;

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->j()LX/8Ys;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->q()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->u()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v6

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->w()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/Cmf;-><init>(LX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    const/4 v4, 0x1

    .line 2533367
    iput-boolean v4, v3, LX/Cmf;->g:Z

    .line 2533368
    move-object v3, v3

    .line 2533369
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->p()LX/8Yr;

    move-result-object v4

    .line 2533370
    iput-object v4, v3, LX/Cmf;->b:LX/8Yr;

    .line 2533371
    move-object v3, v3

    .line 2533372
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->iR_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    .line 2533373
    iput-object v4, v3, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533374
    move-object v3, v3

    .line 2533375
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v4

    .line 2533376
    iput-object v4, v3, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533377
    move-object v3, v3

    .line 2533378
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    .line 2533379
    iput-object v4, v3, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533380
    move-object v3, v3

    .line 2533381
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    .line 2533382
    iput-object v4, v3, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533383
    move-object v3, v3

    .line 2533384
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v3

    invoke-virtual {v3}, LX/Cm7;->b()LX/Clr;

    move-result-object v3

    check-cast v3, LX/Cm4;

    move-object v0, v3

    .line 2533385
    iput-object v0, p0, LX/I4B;->y:LX/Clr;

    goto :goto_0

    .line 2533386
    :sswitch_2
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object v0

    invoke-static {v0}, LX/I4B;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2533387
    iget-object v0, p0, LX/I4B;->r:Landroid/content/Context;

    .line 2533388
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideshowModel$SlideEdgesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->q()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/IXf;->a(LX/0Px;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Clo;

    move-result-object v1

    .line 2533389
    new-instance v2, LX/Cmd;

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->q()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v3

    invoke-direct {v2, v1, v3}, LX/Cmd;-><init>(LX/Clo;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V

    const/4 v1, 0x1

    .line 2533390
    iput-boolean v1, v2, LX/Cmd;->c:Z

    .line 2533391
    move-object v1, v2

    .line 2533392
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->iR_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    .line 2533393
    iput-object v2, v1, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533394
    move-object v1, v1

    .line 2533395
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v2

    .line 2533396
    iput-object v2, v1, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2533397
    move-object v1, v1

    .line 2533398
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    .line 2533399
    iput-object v2, v1, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533400
    move-object v1, v1

    .line 2533401
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v2

    .line 2533402
    iput-object v2, v1, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2533403
    move-object v1, v1

    .line 2533404
    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->c()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->m()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->l()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v1

    invoke-virtual {v1}, LX/Cm7;->b()LX/Clr;

    move-result-object v1

    check-cast v1, LX/Cm3;

    move-object v0, v1

    .line 2533405
    iput-object v0, p0, LX/I4B;->y:LX/Clr;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2cd4c524 -> :sswitch_1
        0x3f470545 -> :sswitch_0
        0x42719249 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;)LX/I4B;
    .locals 0

    .prologue
    .line 2533338
    iput-object p1, p0, LX/I4B;->s:Ljava/lang/String;

    .line 2533339
    return-object p0
.end method

.method public b()LX/Clo;
    .locals 10

    .prologue
    const/16 v9, 0xb

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2533248
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2533249
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2533250
    iget-object v0, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2533251
    iget-object v0, p0, LX/I4B;->y:LX/Clr;

    if-eqz v0, :cond_0

    .line 2533252
    iget-object v0, p0, LX/I4B;->y:LX/Clr;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533253
    :cond_0
    iget-object v0, p0, LX/I4B;->t:LX/ClZ;

    if-eqz v0, :cond_1

    .line 2533254
    iget-object v0, p0, LX/I4B;->t:LX/ClZ;

    invoke-static {v0}, LX/IXa;->a(LX/ClZ;)LX/CmQ;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533255
    :cond_1
    iget-object v0, p0, LX/I4B;->u:LX/8Z4;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/I4B;->u:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2533256
    new-instance v0, LX/CmL;

    iget-object v6, p0, LX/I4B;->u:LX/8Z4;

    invoke-direct {v0, v6}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v6, LX/Clb;->KICKER:LX/Clb;

    .line 2533257
    iput-object v6, v0, LX/CmL;->b:LX/Clb;

    .line 2533258
    move-object v6, v0

    .line 2533259
    iget-object v7, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533260
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533261
    if-eqz v0, :cond_d

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533262
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533263
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    :goto_0
    iget-object v8, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v7, v0, v6, v8}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533264
    invoke-virtual {v6}, LX/CmL;->c()LX/Cly;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533265
    :cond_2
    iget-object v0, p0, LX/I4B;->v:LX/8Z4;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/I4B;->v:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2533266
    new-instance v0, LX/CmL;

    iget-object v6, p0, LX/I4B;->v:LX/8Z4;

    invoke-direct {v0, v6}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v6, LX/Clb;->TITLE:LX/Clb;

    .line 2533267
    iput-object v6, v0, LX/CmL;->b:LX/Clb;

    .line 2533268
    move-object v6, v0

    .line 2533269
    iget-object v7, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533270
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533271
    if-eqz v0, :cond_e

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533272
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533273
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    :goto_1
    iget-object v8, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v7, v0, v6, v8}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533274
    invoke-virtual {v6}, LX/CmL;->c()LX/Cly;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533275
    :cond_3
    iget-object v0, p0, LX/I4B;->w:LX/8Z4;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/I4B;->w:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2533276
    new-instance v0, LX/CmL;

    iget-object v6, p0, LX/I4B;->w:LX/8Z4;

    invoke-direct {v0, v6}, LX/CmL;-><init>(LX/8Z4;)V

    sget-object v6, LX/Clb;->SUBTITLE:LX/Clb;

    .line 2533277
    iput-object v6, v0, LX/CmL;->b:LX/Clb;

    .line 2533278
    move-object v6, v0

    .line 2533279
    iget-object v7, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533280
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533281
    if-eqz v0, :cond_f

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533282
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533283
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->B()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    :goto_2
    iget-object v8, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v7, v0, v6, v8}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533284
    invoke-virtual {v6}, LX/CmL;->c()LX/Cly;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533285
    :cond_4
    iget-object v0, p0, LX/I4B;->x:LX/Clr;

    if-eqz v0, :cond_5

    .line 2533286
    iget-object v0, p0, LX/I4B;->x:LX/Clr;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533287
    :cond_5
    iget-object v0, p0, LX/I4B;->A:LX/Clr;

    if-nez v0, :cond_6

    iget-object v0, p0, LX/I4B;->B:LX/Clr;

    if-eqz v0, :cond_8

    .line 2533288
    :cond_6
    new-instance v0, LX/CmE;

    invoke-direct {v0, v9}, LX/CmE;-><init>(I)V

    const v6, 0x7f0a0620

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 2533289
    iput v6, v0, LX/CmE;->a:I

    .line 2533290
    move-object v0, v0

    .line 2533291
    iget-object v6, p0, LX/I4B;->d:LX/Cju;

    const v7, 0x7f0d011e

    invoke-interface {v6, v7}, LX/Cju;->c(I)I

    move-result v6

    .line 2533292
    iput v6, v0, LX/CmE;->b:I

    .line 2533293
    move-object v0, v0

    .line 2533294
    const v6, 0x7f0b128a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2533295
    iput v6, v0, LX/CmE;->c:I

    .line 2533296
    move-object v6, v0

    .line 2533297
    iget-object v7, p0, LX/I4B;->l:LX/Clh;

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533298
    iget-object v8, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v8

    .line 2533299
    if-eqz v0, :cond_7

    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2533300
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 2533301
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v1

    :cond_7
    iget-object v0, p0, LX/I4B;->r:Landroid/content/Context;

    invoke-virtual {v7, v1, v6, v0}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V

    .line 2533302
    invoke-virtual {v6}, LX/CmE;->c()LX/CmF;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533303
    :cond_8
    iget-object v0, p0, LX/I4B;->A:LX/Clr;

    if-eqz v0, :cond_9

    .line 2533304
    iget-object v0, p0, LX/I4B;->A:LX/Clr;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533305
    :cond_9
    iget-object v0, p0, LX/I4B;->B:LX/Clr;

    if-eqz v0, :cond_a

    .line 2533306
    iget-object v0, p0, LX/I4B;->B:LX/Clr;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533307
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 2533308
    iget-object v0, p0, LX/I4B;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    const-string v6, "SHARE_BUTTON"

    invoke-virtual {v0, v6}, LX/8bL;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/IXt;->valueOf(Ljava/lang/String;)LX/IXt;

    move-result-object v0

    .line 2533309
    sget-object v6, LX/IXt;->FEED:LX/IXt;

    if-eq v0, v6, :cond_b

    sget-object v6, LX/IXt;->CONDENSED:LX/IXt;

    if-eq v0, v6, :cond_b

    iget-object v0, p0, LX/I4B;->C:LX/Clr;

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/I4B;->G:LX/Clr;

    if-nez v0, :cond_b

    .line 2533310
    iget-object v0, p0, LX/I4B;->C:LX/Clr;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533311
    :cond_b
    new-instance v6, LX/Clo;

    iget-object v0, p0, LX/I4B;->s:Ljava/lang/String;

    invoke-direct {v6, v0}, LX/Clo;-><init>(Ljava/lang/String;)V

    .line 2533312
    invoke-virtual {v6, v3}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533313
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v6, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533314
    invoke-virtual {v6, v4}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533315
    iget-object v0, p0, LX/I4B;->E:Ljava/util/List;

    invoke-virtual {v6, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533316
    iget-object v0, p0, LX/I4B;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v4, 0xb9

    invoke-virtual {v0, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v4

    .line 2533317
    iget-object v0, p0, LX/I4B;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v7, p0, LX/I4B;->z:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v3, v7

    if-eqz v4, :cond_10

    :goto_3
    add-int/2addr v1, v3

    .line 2533318
    iput v1, v0, LX/Chi;->l:I

    .line 2533319
    iget-object v0, p0, LX/I4B;->G:LX/Clr;

    if-eqz v0, :cond_c

    .line 2533320
    iget-object v0, p0, LX/I4B;->G:LX/Clr;

    invoke-virtual {v6, v0}, LX/Clo;->a(LX/Clr;)V

    .line 2533321
    :cond_c
    iget-object v0, p0, LX/I4B;->D:Ljava/util/List;

    invoke-virtual {v6, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533322
    const v0, 0x7f0b1287

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2533323
    const v1, 0x7f0b1288

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2533324
    new-instance v3, LX/CmE;

    invoke-direct {v3, v9}, LX/CmE;-><init>(I)V

    .line 2533325
    iput v2, v3, LX/CmE;->a:I

    .line 2533326
    move-object v2, v3

    .line 2533327
    sub-int v0, v1, v0

    .line 2533328
    iput v0, v2, LX/CmE;->c:I

    .line 2533329
    move-object v0, v2

    .line 2533330
    invoke-virtual {v0}, LX/CmE;->c()LX/CmF;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/Clo;->a(LX/Clr;)V

    .line 2533331
    iget-object v0, v6, LX/Clo;->b:Landroid/os/Bundle;

    move-object v0, v0

    .line 2533332
    iget-object v1, p0, LX/I4B;->F:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2533333
    return-object v6

    :cond_d
    move-object v0, v1

    .line 2533334
    goto/16 :goto_0

    :cond_e
    move-object v0, v1

    .line 2533335
    goto/16 :goto_1

    :cond_f
    move-object v0, v1

    .line 2533336
    goto/16 :goto_2

    :cond_10
    move v1, v2

    .line 2533337
    goto :goto_3
.end method

.method public b(LX/8Z4;)LX/I4B;
    .locals 0

    .prologue
    .line 2533246
    iput-object p1, p0, LX/I4B;->v:LX/8Z4;

    .line 2533247
    return-object p0
.end method

.method public b(Ljava/lang/String;)LX/I4B;
    .locals 2

    .prologue
    .line 2533244
    iget-object v0, p0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533245
    return-object p0
.end method

.method public c(Ljava/lang/String;)LX/I4B;
    .locals 2

    .prologue
    .line 2533242
    iget-object v0, p0, LX/I4B;->F:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533243
    return-object p0
.end method
