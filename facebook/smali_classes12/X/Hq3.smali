.class public LX/Hq3;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "LX/1OM",
        "<",
        "LX/Hq1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0gd;

.field public final c:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/actionitem/ActionItemController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0gd;LX/0il;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0gd;",
            "TServices;)V"
        }
    .end annotation

    .prologue
    .line 2510185
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2510186
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2510187
    iput-object v0, p0, LX/Hq3;->d:LX/0Px;

    .line 2510188
    iput-object p1, p0, LX/Hq3;->a:Landroid/content/Context;

    .line 2510189
    iput-object p2, p0, LX/Hq3;->b:LX/0gd;

    .line 2510190
    iput-object p3, p0, LX/Hq3;->c:LX/0il;

    .line 2510191
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2510192
    iget-object v0, p0, LX/Hq3;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2510193
    const v1, 0x7f030026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2510194
    new-instance v1, LX/Hq1;

    invoke-direct {v1, v0}, LX/Hq1;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2510195
    check-cast p1, LX/Hq1;

    .line 2510196
    iget-object v0, p0, LX/Hq3;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hq7;

    .line 2510197
    iget-object v1, p1, LX/Hq1;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v2, LX/Hq0;

    invoke-direct {v2, p0, v0}, LX/Hq0;-><init>(LX/Hq3;LX/Hq7;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2510198
    iget-object v1, p1, LX/Hq1;->l:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, v1}, LX/Hq7;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    .line 2510199
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2510200
    iget-object v0, p0, LX/Hq3;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hq7;

    .line 2510201
    sget-object p0, LX/Hq2;->STANDARD:LX/Hq2;

    move-object v0, p0

    .line 2510202
    invoke-virtual {v0}, LX/Hq2;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2510203
    iget-object v0, p0, LX/Hq3;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
