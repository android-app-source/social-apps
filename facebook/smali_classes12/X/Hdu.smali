.class public final LX/Hdu;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Hdu;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hds;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Hdv;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2489395
    const/4 v0, 0x0

    sput-object v0, LX/Hdu;->a:LX/Hdu;

    .line 2489396
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hdu;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2489397
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2489398
    new-instance v0, LX/Hdv;

    invoke-direct {v0}, LX/Hdv;-><init>()V

    iput-object v0, p0, LX/Hdu;->c:LX/Hdv;

    .line 2489399
    return-void
.end method

.method public static declared-synchronized q()LX/Hdu;
    .locals 2

    .prologue
    .line 2489400
    const-class v1, LX/Hdu;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Hdu;->a:LX/Hdu;

    if-nez v0, :cond_0

    .line 2489401
    new-instance v0, LX/Hdu;

    invoke-direct {v0}, LX/Hdu;-><init>()V

    sput-object v0, LX/Hdu;->a:LX/Hdu;

    .line 2489402
    :cond_0
    sget-object v0, LX/Hdu;->a:LX/Hdu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2489403
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2489404
    const/4 p2, 0x0

    .line 2489405
    const v0, 0x7f0e0123

    invoke-static {p1, p2, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    const p0, 0x7f083673

    invoke-virtual {v0, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v0

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v0, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 p0, 0x1

    invoke-interface {v0, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    const p0, 0x7f0b0060

    invoke-interface {v0, p2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/4 p0, 0x2

    const p2, 0x7f0b0060

    invoke-interface {v0, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/16 p0, 0x30

    invoke-interface {v0, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2489406
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2489407
    invoke-static {}, LX/1dS;->b()V

    .line 2489408
    const/4 v0, 0x0

    return-object v0
.end method
