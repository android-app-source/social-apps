.class public LX/JSI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;"
    }
.end annotation


# instance fields
.field private final a:LX/JSl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0SG;

.field private final c:Landroid/content/Context;

.field public final d:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final g:I

.field public final h:LX/JSO;

.field public final i:LX/JTY;

.field private final j:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final k:LX/JSe;


# direct methods
.method public constructor <init>(LX/JSl;Landroid/content/Context;LX/1KL;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;ILcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;LX/JSe;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/JTY;)V
    .locals 2
    .param p1    # LX/JSl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JSl;",
            "Landroid/content/Context;",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/JSO;",
            ">;TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I",
            "Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;",
            "LX/JSe;",
            "LX/0SG;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/JTY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2695080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695081
    iput-object p9, p0, LX/JSI;->b:LX/0SG;

    .line 2695082
    iput-object p2, p0, LX/JSI;->c:Landroid/content/Context;

    .line 2695083
    iput-object p4, p0, LX/JSI;->d:LX/1Pr;

    .line 2695084
    iput-object p1, p0, LX/JSI;->a:LX/JSl;

    .line 2695085
    iput p6, p0, LX/JSI;->g:I

    .line 2695086
    iput-object p11, p0, LX/JSI;->i:LX/JTY;

    .line 2695087
    iput-object p7, p0, LX/JSI;->j:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    .line 2695088
    iput-object p8, p0, LX/JSI;->k:LX/JSe;

    .line 2695089
    iput-object p10, p0, LX/JSI;->e:Ljava/util/concurrent/ExecutorService;

    .line 2695090
    invoke-static {p5}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2695091
    invoke-static {p5}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iput-object v1, p0, LX/JSI;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2695092
    iget-object v1, p0, LX/JSI;->d:LX/1Pr;

    invoke-interface {v1, p3, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSO;

    iput-object v0, p0, LX/JSI;->h:LX/JSO;

    .line 2695093
    return-void
.end method

.method public static a(LX/JSI;LX/JSK;I)Z
    .locals 1

    .prologue
    .line 2695055
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iget-object v0, v0, LX/JSO;->a:LX/JSK;

    if-eq v0, p1, :cond_0

    .line 2695056
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iput-object p1, v0, LX/JSO;->a:LX/JSK;

    .line 2695057
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iput p2, v0, LX/JSO;->b:I

    .line 2695058
    iget-object v0, p0, LX/JSI;->e:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackEnvironmentMutator$1;

    invoke-direct {p1, p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackEnvironmentMutator$1;-><init>(LX/JSI;)V

    const p2, -0x2dd06765

    invoke-static {v0, p1, p2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2695059
    const/4 v0, 0x1

    .line 2695060
    :goto_0
    return v0

    .line 2695061
    :cond_0
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iput p2, v0, LX/JSO;->b:I

    .line 2695062
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/JSI;II)V
    .locals 4

    .prologue
    .line 2695068
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iget-object v0, v0, LX/JSO;->a:LX/JSK;

    sget-object v1, LX/JSK;->PLAY_REQUESTED:LX/JSK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iget-object v0, v0, LX/JSO;->a:LX/JSK;

    sget-object v1, LX/JSK;->PLAYING:LX/JSK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iget-object v0, v0, LX/JSO;->a:LX/JSK;

    sget-object v1, LX/JSK;->LOADING:LX/JSK;

    if-ne v0, v1, :cond_1

    .line 2695069
    :cond_0
    iget-object v0, p0, LX/JSI;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/JSI;->h:LX/JSO;

    iget-wide v2, v2, LX/JSO;->c:J

    sub-long/2addr v0, v2

    .line 2695070
    if-nez p2, :cond_2

    .line 2695071
    iget-object v2, p0, LX/JSI;->i:LX/JTY;

    long-to-int v0, v0

    .line 2695072
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/JTW;->PLAYBACK_FINISHED:LX/JTW;

    invoke-virtual {v3}, LX/JTW;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "newsfeed_music_story_view"

    .line 2695073
    iput-object v3, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2695074
    move-object v1, v1

    .line 2695075
    sget-object v3, LX/JTX;->PLAYBACK_DURATION:LX/JTX;

    invoke-virtual {v3}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2695076
    sget-object v3, LX/JTX;->SONG_DURATION:LX/JTX;

    invoke-virtual {v3}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2695077
    invoke-static {v2, v1}, LX/JTY;->a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2695078
    :cond_1
    :goto_0
    return-void

    .line 2695079
    :cond_2
    iget-object v2, p0, LX/JSI;->i:LX/JTY;

    long-to-int v0, v0

    invoke-virtual {v2, p1, v0}, LX/JTY;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 11

    .prologue
    .line 2695094
    int-to-long v0, p1

    .line 2695095
    iget-object v6, p0, LX/JSI;->h:LX/JSO;

    iget-object v6, v6, LX/JSO;->a:LX/JSK;

    sget-object v7, LX/JSK;->PLAY_REQUESTED:LX/JSK;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, LX/JSI;->h:LX/JSO;

    iget-object v6, v6, LX/JSO;->a:LX/JSK;

    sget-object v7, LX/JSK;->LOADING:LX/JSK;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, LX/JSI;->h:LX/JSO;

    iget-object v6, v6, LX/JSO;->a:LX/JSK;

    sget-object v7, LX/JSK;->PAUSED:LX/JSK;

    if-ne v6, v7, :cond_2

    .line 2695096
    :cond_0
    iget-object v6, p0, LX/JSI;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v8

    .line 2695097
    iget-object v6, p0, LX/JSI;->h:LX/JSO;

    iget-wide v6, v6, LX/JSO;->c:J

    sub-long v6, v8, v6

    .line 2695098
    iget-object v10, p0, LX/JSI;->h:LX/JSO;

    iput-wide v8, v10, LX/JSO;->c:J

    .line 2695099
    iget-object v8, p0, LX/JSI;->i:LX/JTY;

    iget-object v9, p0, LX/JSI;->h:LX/JSO;

    iget-object v9, v9, LX/JSO;->a:LX/JSK;

    sget-object v10, LX/JSK;->PAUSED:LX/JSK;

    if-ne v9, v10, :cond_1

    const-wide/16 v6, -0x1

    .line 2695100
    :cond_1
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v10, LX/JTW;->PLAY:LX/JTW;

    invoke-virtual {v10}, LX/JTW;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "newsfeed_music_story_view"

    .line 2695101
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2695102
    move-object v9, v9

    .line 2695103
    sget-object v10, LX/JTX;->SONG_DURATION:LX/JTX;

    invoke-virtual {v10}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2695104
    sget-object v10, LX/JTX;->STALL_DURATION:LX/JTX;

    invoke-virtual {v10}, LX/JTX;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2695105
    invoke-static {v8, v9}, LX/JTY;->a(LX/JTY;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2695106
    :cond_2
    sget-object v0, LX/JSK;->PLAYING:LX/JSK;

    invoke-static {p0, v0, p2}, LX/JSI;->a(LX/JSI;LX/JSK;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2695107
    iget-object v0, p0, LX/JSI;->j:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    if-eqz v0, :cond_3

    .line 2695108
    iget-object v0, p0, LX/JSI;->j:Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;

    iget-object v1, p0, LX/JSI;->k:LX/JSe;

    iget-object v3, p0, LX/JSI;->a:LX/JSl;

    iget-object v2, p0, LX/JSI;->c:Landroid/content/Context;

    .line 2695109
    const-class v4, LX/0f4;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0f4;

    .line 2695110
    if-nez v4, :cond_5

    const/4 v4, 0x0

    :goto_0
    move-object v4, v4

    .line 2695111
    iget v5, p0, LX/JSI;->g:I

    move-object v2, p0

    .line 2695112
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/JSe;

    iput-object v6, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    .line 2695113
    iput-object v2, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->c:LX/JSI;

    .line 2695114
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->f:Ljava/lang/ref/WeakReference;

    .line 2695115
    iput v5, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->d:I

    .line 2695116
    iput-object v3, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->e:LX/JSl;

    .line 2695117
    iget-object v6, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    iget-object v7, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    invoke-virtual {v6, v7}, LX/JSJ;->b(LX/JSe;)Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 2695118
    iget-object v6, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->a:LX/JSJ;

    iget-object v7, v0, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b:LX/JSe;

    invoke-virtual {v6, v7}, LX/JSJ;->b(LX/JSe;)Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;->b(Lcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    .line 2695119
    :cond_3
    iget-object v0, p0, LX/JSI;->h:LX/JSO;

    iput p2, v0, LX/JSO;->b:I

    .line 2695120
    :cond_4
    return-void

    :cond_5
    invoke-interface {v4}, LX/0f4;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v4

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2695066
    sget-object v0, LX/JSK;->STOPPED:LX/JSK;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/JSI;->a(LX/JSI;LX/JSK;I)Z

    .line 2695067
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 2695063
    invoke-static {p0, p1, p2}, LX/JSI;->d(LX/JSI;II)V

    .line 2695064
    sget-object v0, LX/JSK;->STOPPED:LX/JSK;

    invoke-static {p0, v0, p2}, LX/JSI;->a(LX/JSI;LX/JSK;I)Z

    .line 2695065
    return-void
.end method
