.class public LX/Idu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Idk;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/net/Uri;

.field private final c:LX/Idf;

.field public final d:Landroid/content/ContentResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598124
    const-class v0, LX/Idu;

    sput-object v0, LX/Idu;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Idf;Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2598119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598120
    iput-object p1, p0, LX/Idu;->c:LX/Idf;

    .line 2598121
    iput-object p2, p0, LX/Idu;->d:Landroid/content/ContentResolver;

    .line 2598122
    iput-object p3, p0, LX/Idu;->b:Landroid/net/Uri;

    .line 2598123
    return-void
.end method

.method public static a(LX/Idu;IIZZ)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    .line 2598090
    :try_start_0
    iget-object v0, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2598091
    iget-object v0, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2598092
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2598093
    :goto_0
    :try_start_1
    move-object v0, v0

    .line 2598094
    const/4 v5, 0x0

    .line 2598095
    if-eqz p4, :cond_3

    .line 2598096
    invoke-static {}, LX/IdZ;->a()Landroid/graphics/BitmapFactory$Options;

    move-result-object v8

    :goto_1
    move v3, p1

    move v4, p2

    move-object v6, v5

    move-object v7, v0

    .line 2598097
    invoke-static/range {v3 .. v8}, LX/IdZ;->a(IILandroid/net/Uri;Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v0, v3

    .line 2598098
    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 2598099
    const/4 v1, 0x0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2598100
    :try_start_2
    iget-object v2, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2598101
    iget-object v2, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 2598102
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 2598103
    const-string v2, "Orientation"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    move-result v2

    .line 2598104
    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 2598105
    const/16 v1, 0xb4

    .line 2598106
    :cond_0
    :goto_2
    move v1, v1

    .line 2598107
    invoke-static {v0, v1}, LX/IdZ;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    .line 2598108
    :cond_1
    :goto_3
    return-object v0

    .line 2598109
    :catch_0
    move-exception v0

    .line 2598110
    sget-object v1, LX/Idu;->a:Ljava/lang/Class;

    const-string v2, "got exception decoding bitmap "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598111
    const/4 v0, 0x0

    goto :goto_3

    .line 2598112
    :cond_2
    :try_start_4
    iget-object v0, p0, LX/Idu;->d:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/Idu;->b:Landroid/net/Uri;

    const-string v2, "r"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    move-result-object v0

    goto :goto_0

    .line 2598113
    :catch_1
    const/4 v0, 0x0

    goto :goto_0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :cond_3
    :try_start_6
    move-object v8, v5

    goto :goto_1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 2598114
    :cond_4
    const/4 v3, 0x6

    if-ne v2, v3, :cond_5

    .line 2598115
    const/16 v1, 0x5a

    goto :goto_2

    .line 2598116
    :cond_5
    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 2598117
    const/16 v1, 0x10e

    goto :goto_2

    .line 2598118
    :catch_2
    goto :goto_2
.end method


# virtual methods
.method public final a(II)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 2598083
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/Idu;->a(LX/Idu;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 2598087
    const/16 v0, 0x140

    const/high16 v1, 0x30000

    .line 2598088
    const/4 v2, 0x0

    invoke-static {p0, v0, v1, p1, v2}, LX/Idu;->a(LX/Idu;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v0, v2

    .line 2598089
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2598086
    iget-object v0, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 2598085
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2598084
    iget-object v0, p0, LX/Idu;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
