.class public LX/Iqg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/IrC;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:Z

.field public g:F

.field public h:LX/Iqo;

.field public i:LX/Iqo;

.field public j:Lcom/facebook/messaging/montage/model/art/ArtItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2617153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617154
    new-instance v0, LX/IrC;

    invoke-direct {v0}, LX/IrC;-><init>()V

    iput-object v0, p0, LX/Iqg;->a:LX/IrC;

    .line 2617155
    iput v1, p0, LX/Iqg;->d:F

    .line 2617156
    iput v1, p0, LX/Iqg;->g:F

    .line 2617157
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/montage/model/art/ArtItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # F
        .annotation build Landroid/support/annotation/FloatRange;
        .end annotation
    .end param

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2617140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617141
    new-instance v0, LX/IrC;

    invoke-direct {v0}, LX/IrC;-><init>()V

    iput-object v0, p0, LX/Iqg;->a:LX/IrC;

    .line 2617142
    iput v1, p0, LX/Iqg;->d:F

    .line 2617143
    iput v1, p0, LX/Iqg;->g:F

    .line 2617144
    iput-object p1, p0, LX/Iqg;->j:Lcom/facebook/messaging/montage/model/art/ArtItem;

    .line 2617145
    iget-object v0, p0, LX/Iqg;->j:Lcom/facebook/messaging/montage/model/art/ArtItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Iqg;->k:Z

    .line 2617146
    iput p2, p0, LX/Iqg;->e:F

    .line 2617147
    iput-boolean p3, p0, LX/Iqg;->f:Z

    .line 2617148
    iput p4, p0, LX/Iqg;->g:F

    .line 2617149
    iput-object p5, p0, LX/Iqg;->h:LX/Iqo;

    .line 2617150
    iput-object p6, p0, LX/Iqg;->i:LX/Iqo;

    .line 2617151
    return-void

    .line 2617152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2617137
    iput p1, p0, LX/Iqg;->d:F

    .line 2617138
    sget-object v0, LX/Iqn;->SCALE:LX/Iqn;

    invoke-virtual {p0, v0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2617139
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 2617133
    iput p1, p0, LX/Iqg;->b:F

    .line 2617134
    iput p2, p0, LX/Iqg;->c:F

    .line 2617135
    sget-object v0, LX/Iqn;->TRANSLATE:LX/Iqn;

    invoke-virtual {p0, v0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2617136
    return-void
.end method

.method public final a(LX/Iqq;)V
    .locals 1

    .prologue
    .line 2617131
    iget-object v0, p0, LX/Iqg;->a:LX/IrC;

    invoke-virtual {v0, p1}, LX/IrC;->a(LX/Iqq;)V

    .line 2617132
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2617121
    iget-object v0, p0, LX/Iqg;->a:LX/IrC;

    invoke-virtual {v0, p1}, LX/IrC;->a(Ljava/lang/Object;)V

    .line 2617122
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2617127
    iget-boolean v0, p0, LX/Iqg;->f:Z

    if-eq v0, p1, :cond_0

    .line 2617128
    iput-boolean p1, p0, LX/Iqg;->f:Z

    .line 2617129
    sget-object v0, LX/Iqn;->FLIP:LX/Iqn;

    invoke-virtual {p0, v0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2617130
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2617126
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 2617123
    iput-boolean p1, p0, LX/Iqg;->m:Z

    .line 2617124
    sget-object v0, LX/Iqn;->ASSET_LOADED:LX/Iqn;

    invoke-virtual {p0, v0}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2617125
    return-void
.end method
