.class public final LX/IMs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/search/CommunitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V
    .locals 0

    .prologue
    .line 2570270
    iput-object p1, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2570271
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    .line 2570272
    iput-boolean v2, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    .line 2570273
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    iget-object v1, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-boolean v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    invoke-virtual {v0, v1}, LX/INF;->b(Z)V

    .line 2570274
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 2570275
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->l:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2570276
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    .line 2570277
    sget-object v1, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v1, v0, LX/INF;->a:LX/7B6;

    .line 2570278
    invoke-virtual {v0}, LX/7HQ;->d()V

    .line 2570279
    iget-object v1, v0, LX/INF;->b:LX/7Hg;

    sget-object v2, LX/7B6;->a:LX/7B6;

    invoke-virtual {v1, v2}, LX/7Hg;->a(LX/7B6;)Z

    .line 2570280
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->s:LX/IN9;

    iget-object v2, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v2, v2, Lcom/facebook/groups/community/search/CommunitySearchFragment;->r:LX/DSo;

    .line 2570281
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2570282
    const/4 v4, 0x1

    .line 2570283
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2570284
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    const-string v9, ""

    const/4 v10, 0x0

    move-object v5, v1

    move-object v7, v3

    move v8, v4

    invoke-static/range {v5 .. v10}, LX/IN9;->a(LX/IN9;LX/0Px;LX/0Px;ZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2570285
    const v3, -0x7f0f2892

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2570286
    iget-object v3, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->setSelection(I)V

    .line 2570287
    :goto_0
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2570288
    iput-object v1, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->o:Ljava/lang/String;

    .line 2570289
    return-void

    .line 2570290
    :cond_0
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->l:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2570291
    iget-object v0, p0, LX/IMs;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    .line 2570292
    const-string v1, "intersect(groups-named(%s),groups(%s))"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2570293
    iget-object v2, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/7HQ;->b(LX/7B6;)Z

    .line 2570294
    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2570295
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2570296
    return-void
.end method
