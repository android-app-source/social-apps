.class public final LX/Iwk;
.super LX/Iwe;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V
    .locals 0

    .prologue
    .line 2630890
    iput-object p1, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-direct {p0}, LX/Iwe;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2630891
    check-cast p1, LX/Iwd;

    .line 2630892
    iget-boolean v0, p1, LX/Iwd;->b:Z

    if-eqz v0, :cond_0

    .line 2630893
    iget-object v0, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    .line 2630894
    iget-object v4, v0, LX/Iw6;->a:LX/0Zb;

    const-string v5, "tapped_like_page_in_category"

    invoke-static {v5}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page_id"

    invoke-virtual {v5, v6, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page_id"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630895
    :goto_0
    iget-object v0, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/Iwd;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a(Ljava/lang/String;Z)V

    .line 2630896
    iget-object v0, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    iget-boolean v2, p1, LX/Iwd;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b(Ljava/lang/String;Z)V

    .line 2630897
    iget-object v0, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    const v1, 0x57180873

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2630898
    return-void

    .line 2630899
    :cond_0
    iget-object v0, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v1, p1, LX/Iwd;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/Iwk;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    .line 2630900
    iget-object v4, v0, LX/Iw6;->a:LX/0Zb;

    const-string v5, "tapped_unlike_page_in_category"

    invoke-static {v5}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page_id"

    invoke-virtual {v5, v6, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "page_id"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630901
    goto :goto_0
.end method
