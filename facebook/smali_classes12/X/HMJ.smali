.class public LX/HMJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0kL;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/9XE;

.field private final e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final f:LX/0SI;

.field private g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0SI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2456993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2456994
    iput-object p1, p0, LX/HMJ;->a:Landroid/content/Context;

    .line 2456995
    iput-object p2, p0, LX/HMJ;->b:LX/0kL;

    .line 2456996
    iput-object p3, p0, LX/HMJ;->c:Landroid/content/res/Resources;

    .line 2456997
    iput-object p4, p0, LX/HMJ;->d:LX/9XE;

    .line 2456998
    iput-object p5, p0, LX/HMJ;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2456999
    iput-object p6, p0, LX/HMJ;->f:LX/0SI;

    .line 2457000
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 4

    .prologue
    .line 2457001
    new-instance v0, LX/4At;

    iget-object v1, p0, LX/HMJ;->a:Landroid/content/Context;

    iget-object v2, p0, LX/HMJ;->c:Landroid/content/res/Resources;

    const v3, 0x7f0817d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457002
    iput-wide p1, p0, LX/HMJ;->g:J

    .line 2457003
    const-string v0, "is_uploading_media"

    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2457004
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2457005
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2457006
    :goto_0
    return-object v0

    .line 2457007
    :cond_0
    const-string v0, "extra_actor_viewer_context"

    iget-object v1, p0, LX/HMJ;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2457008
    iget-object v0, p0, LX/HMJ;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2457009
    iget-object v0, p0, LX/HMJ;->d:LX/9XE;

    sget-object v1, LX/9XB;->EVENT_CHECKIN_SUCCESS:LX/9XB;

    iget-wide v2, p0, LX/HMJ;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2457010
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2457011
    iget-object v0, p0, LX/HMJ;->d:LX/9XE;

    sget-object v1, LX/9XA;->EVENT_CHECKIN_ERROR:LX/9XA;

    iget-wide v2, p0, LX/HMJ;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2457012
    iget-object v0, p0, LX/HMJ;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817d3

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457013
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457014
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457015
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457016
    const/16 v0, 0x2774

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
