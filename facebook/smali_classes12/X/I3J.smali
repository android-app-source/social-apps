.class public LX/I3J;
.super Landroid/widget/BaseAdapter;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Lcom/facebook/events/common/EventAnalyticsParams;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:LX/DBW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2531281
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I3J;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531274
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2531275
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I3J;->e:Z

    .line 2531276
    iput-object p1, p0, LX/I3J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2531277
    iput-object p2, p0, LX/I3J;->b:Landroid/content/Context;

    .line 2531278
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/I3J;->d:Ljava/util/List;

    .line 2531279
    new-instance v0, LX/I3H;

    invoke-direct {v0, p0}, LX/I3H;-><init>(LX/I3J;)V

    iput-object v0, p0, LX/I3J;->f:LX/DBW;

    .line 2531280
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2531273
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/facebook/events/model/Event;
    .locals 1

    .prologue
    .line 2531270
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2531271
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2531272
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2531267
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2531268
    const v0, -0x1571515c

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2531269
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2531263
    iget-boolean v0, p0, LX/I3J;->e:Z

    if-eq p1, v0, :cond_0

    .line 2531264
    iput-boolean p1, p0, LX/I3J;->e:Z

    .line 2531265
    const v0, -0x323bde96    # -4.1131552E8f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2531266
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2531262
    iget-boolean v0, p0, LX/I3J;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2531248
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2531249
    iget-object v0, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2531250
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/I3J;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2531261
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2531252
    invoke-virtual {p0, p1}, LX/I3J;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2531253
    sget-object v1, LX/I3J;->a:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 2531254
    instance-of v0, p2, LX/I9b;

    if-nez v0, :cond_0

    .line 2531255
    new-instance p2, LX/I9b;

    iget-object v0, p0, LX/I3J;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/I9b;-><init>(Landroid/content/Context;)V

    .line 2531256
    :cond_0
    :goto_0
    return-object p2

    .line 2531257
    :cond_1
    instance-of v0, p2, LX/DBX;

    if-nez v0, :cond_3

    .line 2531258
    new-instance v6, LX/DBX;

    iget-object v0, p0, LX/I3J;->b:Landroid/content/Context;

    invoke-direct {v6, v0}, LX/DBX;-><init>(Landroid/content/Context;)V

    :goto_1
    move-object v0, v6

    .line 2531259
    check-cast v0, LX/DBX;

    .line 2531260
    iget-object v1, p0, LX/I3J;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/Event;

    iget-object v2, p0, LX/I3J;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz p1, :cond_2

    const/4 v3, 0x1

    :goto_2
    iget-object v4, p0, LX/I3J;->f:LX/DBW;

    invoke-virtual/range {v0 .. v5}, LX/DBX;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;ZLX/DBW;Z)V

    move-object p2, v6

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_2

    :cond_3
    move-object v6, p2

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2531251
    invoke-static {}, LX/I3I;->values()[LX/I3I;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
