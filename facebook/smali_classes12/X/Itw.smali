.class public final enum LX/Itw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Itw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Itw;

.field public static final enum FAILED:LX/Itw;

.field public static final enum SKIPPED:LX/Itw;

.field public static final enum SUCCEEDED:LX/Itw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2625155
    new-instance v0, LX/Itw;

    const-string v1, "SKIPPED"

    invoke-direct {v0, v1, v2}, LX/Itw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Itw;->SKIPPED:LX/Itw;

    .line 2625156
    new-instance v0, LX/Itw;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v3}, LX/Itw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Itw;->FAILED:LX/Itw;

    .line 2625157
    new-instance v0, LX/Itw;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/Itw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Itw;->SUCCEEDED:LX/Itw;

    .line 2625158
    const/4 v0, 0x3

    new-array v0, v0, [LX/Itw;

    sget-object v1, LX/Itw;->SKIPPED:LX/Itw;

    aput-object v1, v0, v2

    sget-object v1, LX/Itw;->FAILED:LX/Itw;

    aput-object v1, v0, v3

    sget-object v1, LX/Itw;->SUCCEEDED:LX/Itw;

    aput-object v1, v0, v4

    sput-object v0, LX/Itw;->$VALUES:[LX/Itw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2625151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Itw;
    .locals 1

    .prologue
    .line 2625152
    const-class v0, LX/Itw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Itw;

    return-object v0
.end method

.method public static values()[LX/Itw;
    .locals 1

    .prologue
    .line 2625153
    sget-object v0, LX/Itw;->$VALUES:[LX/Itw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Itw;

    return-object v0
.end method


# virtual methods
.method public final isFailure()Z
    .locals 1

    .prologue
    .line 2625154
    sget-object v0, LX/Itw;->FAILED:LX/Itw;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
