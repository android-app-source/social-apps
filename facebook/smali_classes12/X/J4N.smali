.class public LX/J4N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J4N;


# instance fields
.field private final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643982
    iput-object p1, p0, LX/J4N;->a:LX/03V;

    .line 2643983
    return-void
.end method

.method public static a(LX/J4N;LX/J5E;LX/J4K;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/J5E;",
            "LX/J4K;",
            ")",
            "LX/0Px",
            "<",
            "LX/J4F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2643984
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, LX/J5E;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2643985
    :cond_0
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_no_items"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Null or zero items received for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2643986
    const/4 v1, 0x0

    .line 2643987
    :goto_0
    return-object v1

    .line 2643988
    :cond_1
    new-instance v12, LX/0Pz;

    invoke-direct {v12}, LX/0Pz;-><init>()V

    .line 2643989
    invoke-interface/range {p1 .. p1}, LX/J5E;->b()LX/0Px;

    move-result-object v13

    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v14

    const/4 v1, 0x0

    move v11, v1

    :goto_1
    if-ge v11, v14, :cond_d

    invoke-virtual {v13, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;

    .line 2643990
    if-nez v6, :cond_2

    .line 2643991
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_item"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Null item received in list for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2643992
    :goto_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_1

    .line 2643993
    :cond_2
    sget-object v1, LX/J4M;->a:[I

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupSectionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupSectionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 2643994
    :pswitch_0
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_3

    .line 2643995
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_section_header"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty section header for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2643996
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2643997
    new-instance v3, LX/J4G;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, LX/J4G;-><init>(Ljava/lang/String;)V

    .line 2643998
    invoke-virtual {v12, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2643999
    :pswitch_1
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_4

    .line 2644000
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_section_header"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty section information for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2644001
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2644002
    new-instance v3, LX/J4H;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, LX/J4H;-><init>(Ljava/lang/String;)V

    .line 2644003
    invoke-virtual {v12, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 2644004
    :pswitch_2
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->d()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v1

    if-nez v1, :cond_6

    .line 2644005
    :cond_5
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_section_data"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty section data or edit privacy scope for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2644006
    :cond_6
    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->d()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;

    move-result-object v8

    .line 2644007
    const/4 v9, 0x0

    .line 2644008
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_7

    .line 2644009
    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2644010
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2dc;->a(Ljava/lang/String;)LX/2dc;

    move-result-object v1

    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 2644011
    :cond_7
    sget-object v1, LX/J4K;->PROFILE_STEP:LX/J4K;

    move-object/from16 v0, p2

    if-eq v0, v1, :cond_8

    sget-object v1, LX/J4K;->APPS_STEP:LX/J4K;

    move-object/from16 v0, p2

    if-ne v0, v1, :cond_b

    :cond_8
    const/4 v1, 0x1

    move v7, v1

    .line 2644012
    :goto_4
    new-instance v1, LX/J4J;

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->d()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->d()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    move-result-object v3

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupSectionDataFragmentModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v7}, LX/J4N;->a(Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;Z)LX/8SR;

    move-result-object v7

    if-eqz v8, :cond_c

    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->a()Ljava/lang/String;

    move-result-object v8

    :goto_5
    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, LX/J4J;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8SR;Ljava/lang/String;LX/1Fd;Z)V

    .line 2644013
    invoke-virtual {v12, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 2644014
    :cond_9
    const/4 v1, 0x0

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    goto :goto_3

    .line 2644015
    :cond_b
    const/4 v1, 0x0

    move v7, v1

    goto :goto_4

    .line 2644016
    :cond_c
    const/4 v8, 0x0

    goto :goto_5

    .line 2644017
    :cond_d
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/J5C;Ljava/lang/String;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/J5C;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/J4F;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2644018
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, LX/J5C;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface/range {p1 .. p1}, LX/J5C;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2644019
    :cond_0
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    .line 2644020
    :goto_0
    return-object v1

    .line 2644021
    :cond_1
    new-instance v12, LX/0Pz;

    invoke-direct {v12}, LX/0Pz;-><init>()V

    .line 2644022
    invoke-interface/range {p1 .. p1}, LX/J5C;->a()LX/0Px;

    move-result-object v13

    invoke-virtual {v13}, LX/0Px;->size()I

    move-result v14

    const/4 v1, 0x0

    move v11, v1

    :goto_1
    if-ge v11, v14, :cond_a

    invoke-virtual {v13, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;

    .line 2644023
    if-nez v5, :cond_2

    .line 2644024
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_review_item"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Null item received in list for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2644025
    :goto_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_1

    .line 2644026
    :cond_2
    sget-object v1, LX/J4M;->b:[I

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->c()Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 2644027
    :pswitch_0
    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_3

    .line 2644028
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_review_section_header"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty section header for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2644029
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2644030
    new-instance v3, LX/J4G;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, LX/J4G;-><init>(Ljava/lang/String;)V

    .line 2644031
    invoke-virtual {v12, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2644032
    :pswitch_1
    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2644033
    :cond_4
    iget-object v1, p0, LX/J4N;->a:LX/03V;

    const-string v2, "privacy_checkup_manager_empty_review_section_data"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Empty section data or edit privacy scope for step: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2644034
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;

    move-result-object v8

    .line 2644035
    const/4 v9, 0x0

    .line 2644036
    if-eqz v8, :cond_8

    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_6

    .line 2644037
    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2644038
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2dc;->a(Ljava/lang/String;)LX/2dc;

    move-result-object v1

    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 2644039
    :cond_6
    new-instance v1, LX/J4J;

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->c()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyScopeForEditFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v8, v7}, LX/J4N;->a(Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;Z)LX/8SR;

    move-result-object v7

    if-eqz v8, :cond_9

    invoke-virtual {v8}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->a()Ljava/lang/String;

    move-result-object v8

    :goto_4
    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, LX/J4J;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8SR;Ljava/lang/String;LX/1Fd;Z)V

    .line 2644040
    invoke-virtual {v12, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 2644041
    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    .line 2644042
    :cond_9
    const/4 v8, 0x0

    goto :goto_4

    .line 2644043
    :cond_a
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;Z)LX/8SR;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2644044
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->b()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->b()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->b()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2644045
    :cond_0
    :goto_0
    return-object v1

    .line 2644046
    :cond_1
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2644047
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->b()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v6

    move-object v3, v1

    move-object v2, v1

    :goto_1
    if-ge v5, v9, :cond_4

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    .line 2644048
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v4

    .line 2644049
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->a()Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v3, v4

    .line 2644050
    :cond_2
    if-eqz p1, :cond_3

    if-eqz v4, :cond_3

    invoke-static {v4}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v10

    sget-object v11, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_OF_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v10, v11, :cond_3

    .line 2644051
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v2, v0

    goto :goto_1

    .line 2644052
    :cond_3
    new-array v0, v12, [LX/1oT;

    aput-object v4, v0, v6

    invoke-virtual {v7, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2644053
    if-eqz v4, :cond_6

    invoke-static {v4}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v4, :cond_6

    if-eqz v2, :cond_6

    .line 2644054
    new-array v0, v12, [LX/1oT;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    aput-object v2, v0, v6

    invoke-virtual {v7, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    .line 2644055
    goto :goto_2

    .line 2644056
    :cond_4
    if-eqz v2, :cond_5

    .line 2644057
    new-array v0, v12, [LX/1oT;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {v7, v0}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2644058
    :cond_5
    new-instance v1, LX/8SR;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0, v3}, LX/8SR;-><init>(LX/0Px;LX/1oT;)V

    goto :goto_0

    :cond_6
    move-object v0, v2

    goto :goto_2
.end method

.method public static a(LX/J4N;Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;Ljava/lang/String;I)LX/J4L;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2644059
    invoke-virtual {p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2644060
    :goto_0
    return-object v0

    .line 2644061
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;

    .line 2644062
    new-instance v1, LX/J4L;

    sget-object v3, LX/J4K;->GENERIC_STEP:LX/J4K;

    invoke-direct {v1, v3}, LX/J4L;-><init>(LX/J4K;)V

    .line 2644063
    iput-boolean v5, v1, LX/J4L;->i:Z

    .line 2644064
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/J4L;->a(LX/0Px;)V

    .line 2644065
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;

    move-result-object v3

    invoke-direct {p0, v3, p2}, LX/J4N;->a(LX/J5C;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/J4L;->a(Ljava/util/Collection;)V

    .line 2644066
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->p()Ljava/lang/String;

    move-result-object v3

    .line 2644067
    iput-object v3, v1, LX/J4L;->e:Ljava/lang/String;

    .line 2644068
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->r()Ljava/lang/String;

    move-result-object v3

    .line 2644069
    iput-object v3, v1, LX/J4L;->f:Ljava/lang/String;

    .line 2644070
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->n()Ljava/lang/String;

    move-result-object v3

    .line 2644071
    iput-object v3, v1, LX/J4L;->g:Ljava/lang/String;

    .line 2644072
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->o()Ljava/lang/String;

    move-result-object v3

    .line 2644073
    iput-object v3, v1, LX/J4L;->h:Ljava/lang/String;

    .line 2644074
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2644075
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v2

    .line 2644076
    iput-boolean v2, v1, LX/J4L;->k:Z

    .line 2644077
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel$ReviewSectionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2644078
    iput-object v2, v1, LX/J4L;->j:Ljava/lang/String;

    .line 2644079
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_2

    .line 2644080
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2644081
    iput-object v2, v1, LX/J4L;->l:Ljava/lang/String;

    .line 2644082
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2644083
    iput-object v2, v1, LX/J4L;->m:Ljava/lang/String;

    .line 2644084
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2644085
    iput-object v2, v1, LX/J4L;->n:Ljava/lang/String;

    .line 2644086
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2644087
    iput-object v2, v1, LX/J4L;->o:Ljava/lang/String;

    .line 2644088
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->m()LX/0Px;

    move-result-object v2

    .line 2644089
    iput-object v2, v1, LX/J4L;->p:LX/0Px;

    .line 2644090
    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel$NodesModel;->q()Ljava/lang/String;

    move-result-object v0

    .line 2644091
    iput-object v0, v1, LX/J4L;->q:Ljava/lang/String;

    .line 2644092
    move-object v0, v1

    .line 2644093
    goto/16 :goto_0

    .line 2644094
    :cond_3
    iput-boolean v4, v1, LX/J4L;->k:Z

    .line 2644095
    iput-object v2, v1, LX/J4L;->j:Ljava/lang/String;

    .line 2644096
    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/J4N;
    .locals 4

    .prologue
    .line 2644097
    sget-object v0, LX/J4N;->b:LX/J4N;

    if-nez v0, :cond_1

    .line 2644098
    const-class v1, LX/J4N;

    monitor-enter v1

    .line 2644099
    :try_start_0
    sget-object v0, LX/J4N;->b:LX/J4N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2644100
    if-eqz v2, :cond_0

    .line 2644101
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2644102
    new-instance p0, LX/J4N;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/J4N;-><init>(LX/03V;)V

    .line 2644103
    move-object v0, p0

    .line 2644104
    sput-object v0, LX/J4N;->b:LX/J4N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2644105
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2644106
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2644107
    :cond_1
    sget-object v0, LX/J4N;->b:LX/J4N;

    return-object v0

    .line 2644108
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2644109
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
