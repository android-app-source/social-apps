.class public LX/HdH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2488149
    iput-object p1, p0, LX/HdH;->a:LX/0tX;

    .line 2488150
    return-void
.end method

.method public static a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;Z)Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;
    .locals 9

    .prologue
    .line 2488151
    new-instance v0, LX/HdC;

    invoke-direct {v0}, LX/HdC;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2488152
    iput-object v1, v0, LX/HdC;->b:Ljava/lang/String;

    .line 2488153
    move-object v0, v0

    .line 2488154
    iput-boolean p1, v0, LX/HdC;->c:Z

    .line 2488155
    move-object v0, v0

    .line 2488156
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 2488157
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2488158
    iget-object v3, v0, LX/HdC;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2488159
    iget-object v5, v0, LX/HdC;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2488160
    const/4 v7, 0x3

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 2488161
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 2488162
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 2488163
    const/4 v3, 0x2

    iget-boolean v5, v0, LX/HdC;->c:Z

    invoke-virtual {v2, v3, v5}, LX/186;->a(IZ)V

    .line 2488164
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2488165
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2488166
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2488167
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2488168
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2488169
    new-instance v3, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;-><init>(LX/15i;)V

    .line 2488170
    move-object v0, v3

    .line 2488171
    return-object v0
.end method
