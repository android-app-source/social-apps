.class public abstract LX/HjX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/HjD;

.field private final b:Landroid/content/Context;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HjD;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/HjX;->b:Landroid/content/Context;

    iput-object p2, p0, LX/HjX;->a:LX/HjD;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-boolean v0, p0, LX/HjX;->c:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/HjX;->a:LX/HjD;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HjX;->a:LX/HjD;

    invoke-virtual {v0}, LX/HjD;->a()V

    :cond_1
    invoke-virtual {p0}, LX/HjX;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HjX;->c:Z

    iget-object v0, p0, LX/HjX;->b:Landroid/content/Context;

    const-string v1, "Impression logged"

    invoke-static {v0, v1}, LX/Hko;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public abstract b()V
.end method
