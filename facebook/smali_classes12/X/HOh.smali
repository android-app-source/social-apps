.class public LX/HOh;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HOg;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2460648
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2460649
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;LX/CYE;Ljava/lang/String;Landroid/app/Activity;)LX/HOg;
    .locals 19

    .prologue
    .line 2460650
    new-instance v1, LX/HOg;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/CY3;->a(LX/0QB;)LX/CY3;

    move-result-object v4

    check-cast v4, LX/CY3;

    const-class v5, LX/HNV;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HNV;

    invoke-static/range {p0 .. p0}, LX/HNW;->a(LX/0QB;)LX/HNW;

    move-result-object v6

    check-cast v6, LX/HNW;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static/range {p0 .. p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v9

    check-cast v9, LX/0if;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object v11

    check-cast v11, LX/CYR;

    const/16 v12, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    move-object/from16 v17, p5

    move-object/from16 v18, p6

    invoke-direct/range {v1 .. v18}, LX/HOg;-><init>(Landroid/content/Context;LX/1Ck;LX/CY3;LX/HNV;LX/HNW;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0if;LX/0kL;LX/CYR;LX/0Ot;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;LX/CYE;Ljava/lang/String;Landroid/app/Activity;)V

    .line 2460651
    return-object v1
.end method
