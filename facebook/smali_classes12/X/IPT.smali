.class public LX/IPT;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IPT;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574182
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2574183
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2574184
    const-string v1, "extra_parent_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2574185
    sget-object v1, LX/0ax;->J:Ljava/lang/String;

    const-string v2, "group_feed_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUP_SUBSCRIPTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2574186
    return-void
.end method

.method public static a(LX/0QB;)LX/IPT;
    .locals 3

    .prologue
    .line 2574187
    sget-object v0, LX/IPT;->a:LX/IPT;

    if-nez v0, :cond_1

    .line 2574188
    const-class v1, LX/IPT;

    monitor-enter v1

    .line 2574189
    :try_start_0
    sget-object v0, LX/IPT;->a:LX/IPT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2574190
    if-eqz v2, :cond_0

    .line 2574191
    :try_start_1
    new-instance v0, LX/IPT;

    invoke-direct {v0}, LX/IPT;-><init>()V

    .line 2574192
    move-object v0, v0

    .line 2574193
    sput-object v0, LX/IPT;->a:LX/IPT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574194
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2574195
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2574196
    :cond_1
    sget-object v0, LX/IPT;->a:LX/IPT;

    return-object v0

    .line 2574197
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2574198
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2574199
    const/4 v0, 0x1

    return v0
.end method
