.class public LX/JUk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JUQ;


# direct methods
.method public constructor <init>(LX/JUQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2699691
    iput-object p1, p0, LX/JUk;->a:LX/JUQ;

    .line 2699692
    return-void
.end method

.method public static a(LX/0QB;)LX/JUk;
    .locals 4

    .prologue
    .line 2699679
    const-class v1, LX/JUk;

    monitor-enter v1

    .line 2699680
    :try_start_0
    sget-object v0, LX/JUk;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699681
    sput-object v2, LX/JUk;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699682
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699683
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699684
    new-instance p0, LX/JUk;

    invoke-static {v0}, LX/JUQ;->b(LX/0QB;)LX/JUQ;

    move-result-object v3

    check-cast v3, LX/JUQ;

    invoke-direct {p0, v3}, LX/JUk;-><init>(LX/JUQ;)V

    .line 2699685
    move-object v0, p0

    .line 2699686
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699687
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699688
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
