.class public LX/Iu5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2625243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/Long;)Lcom/facebook/messaging/model/attachment/Attachment;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2625244
    invoke-static {p0, p1, p3, p2}, LX/Iu5;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/attachment/ImageData;

    move-result-object v0

    .line 2625245
    new-instance v1, LX/5dN;

    invoke-direct {v1, p1, p2}, LX/5dN;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 2625246
    iput-object v2, v1, LX/5dN;->e:Ljava/lang/String;

    .line 2625247
    move-object v1, v1

    .line 2625248
    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/0a4;->a(J)I

    move-result v2

    .line 2625249
    iput v2, v1, LX/5dN;->f:I

    .line 2625250
    move-object v1, v1

    .line 2625251
    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 2625252
    iput-object v2, v1, LX/5dN;->d:Ljava/lang/String;

    .line 2625253
    move-object v1, v1

    .line 2625254
    iput-object v0, v1, LX/5dN;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    .line 2625255
    move-object v0, v1

    .line 2625256
    iput-object p1, v0, LX/5dN;->c:Ljava/lang/String;

    .line 2625257
    move-object v0, v0

    .line 2625258
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->H:Ljava/lang/String;

    .line 2625259
    iput-object v1, v0, LX/5dN;->j:Ljava/lang/String;

    .line 2625260
    move-object v0, v0

    .line 2625261
    iput-object p4, v0, LX/5dN;->k:[B

    .line 2625262
    move-object v0, v0

    .line 2625263
    iput-object p5, v0, LX/5dN;->l:Ljava/lang/String;

    .line 2625264
    move-object v0, v0

    .line 2625265
    invoke-virtual {v0}, LX/5dN;->m()Lcom/facebook/messaging/model/attachment/Attachment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/attachment/ImageData;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2625266
    new-instance v0, Lcom/facebook/messaging/model/attachment/ImageData;

    iget v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    iget v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 2625267
    invoke-static {p1, p3}, LX/DoH;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2625268
    new-instance v4, LX/5dP;

    invoke-direct {v4}, LX/5dP;-><init>()V

    sget-object v5, LX/5dQ;->FULL_SCREEN:LX/5dQ;

    new-instance v6, LX/5dM;

    invoke-direct {v6}, LX/5dM;-><init>()V

    .line 2625269
    iput-object v3, v6, LX/5dM;->c:Ljava/lang/String;

    .line 2625270
    move-object v3, v6

    .line 2625271
    iget v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 2625272
    iput v6, v3, LX/5dM;->a:I

    .line 2625273
    move-object v3, v3

    .line 2625274
    iget v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 2625275
    iput v6, v3, LX/5dM;->b:I

    .line 2625276
    move-object v3, v3

    .line 2625277
    invoke-virtual {v3}, LX/5dM;->d()Lcom/facebook/messaging/model/attachment/ImageUrl;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/5dP;->a(LX/5dQ;Lcom/facebook/messaging/model/attachment/ImageUrl;)LX/5dP;

    move-result-object v3

    invoke-virtual {v3}, LX/5dP;->b()Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    move-result-object v3

    move-object v3, v3

    .line 2625278
    const/4 v4, 0x0

    sget-object v5, LX/5dT;->NONQUICKCAM:LX/5dT;

    const/4 v6, 0x0

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/attachment/ImageData;-><init>(IILcom/facebook/messaging/model/attachment/AttachmentImageMap;Lcom/facebook/messaging/model/attachment/AttachmentImageMap;LX/5dT;ZLjava/lang/String;)V

    return-object v0
.end method
