.class public final LX/IJ0;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562335
    iput-object p1, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    .line 2562336
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2562337
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562338
    :goto_0
    return-void

    .line 2562339
    :cond_0
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ax:LX/IIm;

    .line 2562340
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562341
    if-eqz v2, :cond_1

    .line 2562342
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562343
    :goto_1
    goto :goto_0

    .line 2562344
    :cond_1
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562345
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_1
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 2562346
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562347
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->k()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2562348
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->N$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562349
    :cond_0
    :goto_0
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2562350
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V

    .line 2562351
    :cond_1
    return-void

    .line 2562352
    :cond_2
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    sget-object v1, LX/IIC;->FRIENDSLIST:LX/IIC;

    invoke-virtual {v0, v1}, LX/IID;->a(LX/IIC;)V

    .line 2562353
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->B:LX/IIE;

    .line 2562354
    invoke-virtual {v0}, LX/IIE;->c()I

    move-result v1

    .line 2562355
    iget-object v2, v0, LX/IIE;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/IIE;->b:LX/0Tn;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2562356
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->B:LX/IIE;

    invoke-virtual {v0}, LX/IIE;->c()I

    move-result v0

    .line 2562357
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 2562358
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1520621328229529"

    .line 2562359
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2562360
    move-object v0, v0

    .line 2562361
    iget-object v1, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2562362
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->B$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562363
    iget-object v0, p0, LX/IJ0;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    invoke-virtual {v0}, LX/IID;->k()V

    .line 2562364
    return-void
.end method
