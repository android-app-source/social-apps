.class public final LX/IVu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

.field private final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2582973
    iput-object p1, p0, LX/IVu;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582974
    iput-object p2, p0, LX/IVu;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582975
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2582976
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1y1;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IVu;->c:Ljava/lang/String;

    .line 2582977
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2582964
    iget-object v0, p0, LX/IVu;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->e:LX/1xv;

    iget-object v1, p0, LX/IVu;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v0

    invoke-virtual {v0}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v1

    .line 2582965
    iget-object v0, p0, LX/IVu;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582966
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2582967
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVu;->a:Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/partdefinitions/GroupsHeaderTitlePartDefinition;->a:LX/1xc;

    iget-object v0, p0, LX/IVu;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2582968
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2582969
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v0, v1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2582970
    :goto_0
    new-instance v1, LX/1yT;

    invoke-static {v0}, LX/1yU;->a(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    return-object v1

    :cond_0
    move-object v0, v1

    .line 2582971
    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2582972
    iget-object v0, p0, LX/IVu;->c:Ljava/lang/String;

    return-object v0
.end method
