.class public final LX/HRY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ke;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2ke",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V
    .locals 0

    .prologue
    .line 2465475
    iput-object p1, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 2465473
    iget-object v0, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    invoke-virtual {v0, p1, p2}, LX/2iv;->b(II)V

    .line 2465474
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2465459
    iget-object v0, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    .line 2465460
    iget-object p0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    .line 2465461
    iput-object p1, p0, LX/2iv;->a:LX/2kM;

    .line 2465462
    invoke-interface {p1}, LX/2kM;->b()LX/2nj;

    move-result-object p0

    .line 2465463
    iget-boolean p1, p0, LX/2nj;->d:Z

    move p0, p1

    .line 2465464
    iput-boolean p0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->z:Z

    .line 2465465
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 2465468
    iget-object v0, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    .line 2465469
    iget-object v1, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    invoke-virtual {v1, p1, p2}, LX/2iv;->a(II)V

    .line 2465470
    if-eqz v0, :cond_0

    .line 2465471
    iget-object v0, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 2465472
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 2465466
    iget-object v0, p0, LX/HRY;->a:Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    invoke-virtual {v0, p1, p2}, LX/2iv;->c(II)V

    .line 2465467
    return-void
.end method
