.class public final LX/HRr;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public a:Ljava/lang/Exception;

.field public final synthetic b:LX/HRv;

.field public final synthetic c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;LX/HRv;)V
    .locals 1

    .prologue
    .line 2465909
    iput-object p1, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iput-object p2, p0, LX/HRr;->b:LX/HRv;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2465910
    const/4 v0, 0x0

    iput-object v0, p0, LX/HRr;->a:Ljava/lang/Exception;

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 2465911
    if-eqz p2, :cond_3

    const-string v0, "fbpageinsightschart://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2465912
    const/16 v0, 0x16

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2465913
    iget-object v1, p0, LX/HRr;->b:LX/HRv;

    const/4 v3, 0x1

    iput-boolean v3, v1, LX/HRv;->a:Z

    .line 2465914
    iget-object v1, p0, LX/HRr;->b:LX/HRv;

    iput v0, v1, LX/HRv;->b:I

    .line 2465915
    iget-object v1, p0, LX/HRr;->b:LX/HRv;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    iget-object v5, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-static {v5, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;I)I

    move-result v0

    invoke-direct {v3, v4, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v3, v1, LX/HRv;->c:Landroid/widget/LinearLayout$LayoutParams;

    .line 2465916
    iget-object v0, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2465917
    iget-object v0, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "JavascriptInterface wasn\'t added successfully, additional exception: "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    if-nez v1, :cond_0

    const-string v1, "none"

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 2465918
    :goto_1
    return v0

    .line 2465919
    :cond_0
    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2465920
    :catch_0
    move-exception v0

    .line 2465921
    :try_start_1
    iput-object v0, p0, LX/HRr;->a:Ljava/lang/Exception;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2465922
    iget-object v0, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "JavascriptInterface wasn\'t added successfully, additional exception: "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    if-nez v1, :cond_1

    const-string v1, "none"

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 2465923
    goto :goto_1

    .line 2465924
    :cond_1
    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :catchall_0
    iget-object v0, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/HRr;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "JavascriptInterface wasn\'t added successfully, additional exception: "

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    if-nez v1, :cond_2

    const-string v1, "none"

    :goto_3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 2465925
    goto :goto_1

    .line 2465926
    :cond_2
    iget-object v1, p0, LX/HRr;->a:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2465927
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1
.end method
