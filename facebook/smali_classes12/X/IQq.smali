.class public final LX/IQq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;)V
    .locals 0

    .prologue
    .line 2575782
    iput-object p1, p0, LX/IQq;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;B)V
    .locals 0

    .prologue
    .line 2575767
    invoke-direct {p0, p1}, LX/IQq;-><init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, -0x4502d2a7

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575768
    iget-object v1, p0, LX/IQq;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    .line 2575769
    iput-boolean v2, v1, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->h:Z

    .line 2575770
    iget-object v1, p0, LX/IQq;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->setVisibility(I)V

    .line 2575771
    iget-object v1, p0, LX/IQq;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2575772
    iget-object v1, p0, LX/IQq;->b:Ljava/lang/String;

    .line 2575773
    new-instance v2, LX/4Dt;

    invoke-direct {v2}, LX/4Dt;-><init>()V

    .line 2575774
    const-string v3, "group_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2575775
    move-object v2, v2

    .line 2575776
    new-instance v3, LX/9Ja;

    invoke-direct {v3}, LX/9Ja;-><init>()V

    move-object v3, v3

    .line 2575777
    const-string p1, "input"

    invoke-virtual {v3, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2575778
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    move-object v1, v2

    .line 2575779
    iget-object v2, p0, LX/IQq;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2575780
    new-instance v2, LX/IQp;

    invoke-direct {v2, p0}, LX/IQp;-><init>(LX/IQq;)V

    iget-object v3, p0, LX/IQq;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesView;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2575781
    :cond_0
    const v1, 0x13209694

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
