.class public final LX/IQO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IQP;


# direct methods
.method public constructor <init>(LX/IQP;)V
    .locals 0

    .prologue
    .line 2575173
    iput-object p1, p0, LX/IQO;->a:LX/IQP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x76526710

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575174
    iget-object v1, p0, LX/IQO;->a:LX/IQP;

    iget-object v1, v1, LX/IQP;->n:LX/IQv;

    if-eqz v1, :cond_0

    .line 2575175
    iget-object v1, p0, LX/IQO;->a:LX/IQP;

    iget-object v1, v1, LX/IQP;->n:LX/IQv;

    iget-object v2, p0, LX/IQO;->a:LX/IQP;

    invoke-virtual {v2}, LX/1a1;->d()I

    move-result v2

    .line 2575176
    iget-object p0, v1, LX/IQv;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object p0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    add-int/lit8 p1, v2, -0x1

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object p0

    .line 2575177
    iget-object p1, v1, LX/IQv;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object p1, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2575178
    iget-object p1, v1, LX/IQv;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object p1, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2575179
    :goto_0
    iget-object p0, v1, LX/IQv;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object p0, p0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    invoke-virtual {p0, v2}, LX/1OM;->i_(I)V

    .line 2575180
    :cond_0
    const v1, 0x7438eac8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2575181
    :cond_1
    iget-object p1, v1, LX/IQv;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object p1, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-interface {p1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
