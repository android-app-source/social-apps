.class public LX/IRF;
.super LX/DNC;
.source ""


# instance fields
.field private final a:LX/IRJ;

.field private final b:Landroid/content/Context;

.field private final c:LX/DOp;

.field private final d:LX/IVs;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/DNR;

.field private final g:LX/DNJ;

.field public h:LX/1Qq;


# direct methods
.method public constructor <init>(LX/IRJ;Landroid/content/Context;LX/DOp;LX/IVs;LX/0Ot;LX/DNR;LX/DNJ;)V
    .locals 0
    .param p6    # LX/DNR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/DNJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IRJ;",
            "Landroid/content/Context;",
            "LX/DOp;",
            "LX/IVs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsBasicGroupPartDefinition;",
            ">;",
            "LX/DNR;",
            "LX/DNJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2576336
    invoke-direct {p0}, LX/DNC;-><init>()V

    .line 2576337
    iput-object p1, p0, LX/IRF;->a:LX/IRJ;

    .line 2576338
    iput-object p2, p0, LX/IRF;->b:Landroid/content/Context;

    .line 2576339
    iput-object p3, p0, LX/IRF;->c:LX/DOp;

    .line 2576340
    iput-object p4, p0, LX/IRF;->d:LX/IVs;

    .line 2576341
    iput-object p5, p0, LX/IRF;->e:LX/0Ot;

    .line 2576342
    iput-object p6, p0, LX/IRF;->f:LX/DNR;

    .line 2576343
    iput-object p7, p0, LX/IRF;->g:LX/DNJ;

    .line 2576344
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;)LX/1Pf;
    .locals 6

    .prologue
    .line 2576333
    iget-object v0, p0, LX/IRF;->a:LX/IRJ;

    sget-object v1, LX/IRH;->NORMAL:LX/IRH;

    iget-object v2, p0, LX/IRF;->b:Landroid/content/Context;

    .line 2576334
    sget-object v3, LX/DOr;->a:LX/DOr;

    move-object v3, v3

    .line 2576335
    new-instance v4, Lcom/facebook/groups/feed/ui/GroupsDiveInControllerResponder$1;

    invoke-direct {v4, p0}, Lcom/facebook/groups/feed/ui/GroupsDiveInControllerResponder$1;-><init>(LX/IRF;)V

    invoke-static {p1}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IRJ;->a(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IRI;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/1Qq;
    .locals 1

    .prologue
    .line 2576326
    iget-object v0, p0, LX/IRF;->h:LX/1Qq;

    return-object v0
.end method

.method public final a(LX/0g1;LX/1Pf;)LX/1Qq;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0g1",
            "<",
            "Lcom/facebook/graphql/model/FeedEdge;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2576331
    iget-object v0, p0, LX/IRF;->c:LX/DOp;

    iget-object v1, p0, LX/IRF;->d:LX/IVs;

    iget-object v2, p0, LX/IRF;->e:LX/0Ot;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/IVs;->a(LX/0Ot;LX/0Ot;)LX/0Ot;

    move-result-object v1

    iget-object v2, p0, LX/IRF;->f:LX/DNR;

    invoke-virtual {v0, p1, v1, p2, v2}, LX/DOp;->a(LX/0g1;LX/0Ot;LX/1Pf;LX/DNR;)LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/IRF;->h:LX/1Qq;

    .line 2576332
    iget-object v0, p0, LX/IRF;->h:LX/1Qq;

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2576345
    const v0, 0x7f0d0d65

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2576346
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2576347
    iget-object v1, p0, LX/IRF;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081bb2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2576348
    return-void
.end method

.method public final b()LX/1Cv;
    .locals 1

    .prologue
    .line 2576330
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cv;
    .locals 1

    .prologue
    .line 2576329
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2576327
    iget-object v0, p0, LX/IRF;->g:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->i()V

    .line 2576328
    return-void
.end method
