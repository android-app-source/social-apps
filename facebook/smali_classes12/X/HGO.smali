.class public final LX/HGO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2445915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2445916
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445917
    :goto_0
    return v1

    .line 2445918
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445919
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2445920
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2445921
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2445922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2445923
    const-string v4, "address"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2445924
    const/4 v3, 0x0

    .line 2445925
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_a

    .line 2445926
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445927
    :goto_2
    move v2, v3

    .line 2445928
    goto :goto_1

    .line 2445929
    :cond_2
    const-string v4, "phone_number"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2445930
    const/4 v3, 0x0

    .line 2445931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_e

    .line 2445932
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445933
    :goto_3
    move v0, v3

    .line 2445934
    goto :goto_1

    .line 2445935
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2445936
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2445937
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2445938
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2445939
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445940
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 2445941
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2445942
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2445943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 2445944
    const-string v7, "city"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2445945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 2445946
    :cond_7
    const-string v7, "postal_code"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2445947
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 2445948
    :cond_8
    const-string v7, "street"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2445949
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_4

    .line 2445950
    :cond_9
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2445951
    invoke-virtual {p1, v3, v5}, LX/186;->b(II)V

    .line 2445952
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2445953
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2445954
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_a
    move v2, v3

    move v4, v3

    move v5, v3

    goto :goto_4

    .line 2445955
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2445956
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_d

    .line 2445957
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2445958
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2445959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_c

    if-eqz v4, :cond_c

    .line 2445960
    const-string v5, "national_number"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2445961
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2445962
    :cond_d
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2445963
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2445964
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_e
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2445965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2445966
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2445967
    if-eqz v0, :cond_3

    .line 2445968
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445969
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2445970
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445971
    if-eqz v1, :cond_0

    .line 2445972
    const-string p3, "city"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445973
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2445974
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445975
    if-eqz v1, :cond_1

    .line 2445976
    const-string p3, "postal_code"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445977
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2445978
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445979
    if-eqz v1, :cond_2

    .line 2445980
    const-string p3, "street"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445981
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2445982
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2445983
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2445984
    if-eqz v0, :cond_5

    .line 2445985
    const-string v1, "phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445986
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2445987
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445988
    if-eqz v1, :cond_4

    .line 2445989
    const-string p1, "national_number"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445990
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2445991
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2445992
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2445993
    return-void
.end method
