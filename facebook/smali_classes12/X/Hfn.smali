.class public final LX/Hfn;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Hfn;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hfl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Hfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2492327
    const/4 v0, 0x0

    sput-object v0, LX/Hfn;->a:LX/Hfn;

    .line 2492328
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hfn;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2492344
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2492345
    new-instance v0, LX/Hfo;

    invoke-direct {v0}, LX/Hfo;-><init>()V

    iput-object v0, p0, LX/Hfn;->c:LX/Hfo;

    .line 2492346
    return-void
.end method

.method public static declared-synchronized q()LX/Hfn;
    .locals 2

    .prologue
    .line 2492347
    const-class v1, LX/Hfn;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Hfn;->a:LX/Hfn;

    if-nez v0, :cond_0

    .line 2492348
    new-instance v0, LX/Hfn;

    invoke-direct {v0}, LX/Hfn;-><init>()V

    sput-object v0, LX/Hfn;->a:LX/Hfn;

    .line 2492349
    :cond_0
    sget-object v0, LX/Hfn;->a:LX/Hfn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2492350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2492331
    check-cast p2, LX/Hfm;

    .line 2492332
    iget v0, p2, LX/Hfm;->a:I

    iget-object v1, p2, LX/Hfm;->c:LX/BcO;

    .line 2492333
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/Bdq;->c(LX/1De;)LX/Bdl;

    move-result-object v3

    sget-object p0, LX/Hfo;->a:LX/Bdb;

    invoke-virtual {v3, p0}, LX/Bdl;->a(LX/Bdb;)LX/Bdl;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/Bdl;->a(LX/BcO;)LX/Bdl;

    move-result-object v3

    const/4 p0, 0x0

    .line 2492334
    new-instance p2, LX/HfY;

    invoke-direct {p2}, LX/HfY;-><init>()V

    .line 2492335
    sget-object v1, LX/HfZ;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HfX;

    .line 2492336
    if-nez v1, :cond_0

    .line 2492337
    new-instance v1, LX/HfX;

    invoke-direct {v1}, LX/HfX;-><init>()V

    .line 2492338
    :cond_0
    invoke-static {v1, p1, p0, p0, p2}, LX/HfX;->a$redex0(LX/HfX;LX/1De;IILX/HfY;)V

    .line 2492339
    move-object p2, v1

    .line 2492340
    move-object p0, p2

    .line 2492341
    move-object p0, p0

    .line 2492342
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/Bdl;->a(LX/1X1;)LX/Bdl;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p0, 0x7f0a0941

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2492343
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2492329
    invoke-static {}, LX/1dS;->b()V

    .line 2492330
    const/4 v0, 0x0

    return-object v0
.end method
