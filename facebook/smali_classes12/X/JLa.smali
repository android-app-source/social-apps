.class public final LX/JLa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:LX/JLc;


# direct methods
.method public constructor <init>(LX/JLc;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2681295
    iput-object p1, p0, LX/JLa;->b:LX/JLc;

    iput-object p2, p0, LX/JLa;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2681296
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2681297
    const-string v0, "zipcode"

    iget-object v2, p0, LX/JLa;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681298
    iget-object v0, p0, LX/JLa;->b:LX/JLc;

    invoke-static {v0}, LX/JLc;->a(LX/JLc;)LX/5pY;

    move-result-object v0

    const-class v2, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v2}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v2, "MarketplaceLocationUpdated"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2681299
    return-void
.end method
