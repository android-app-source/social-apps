.class public final LX/Irh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iqq;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618592
    iput-object p1, p0, LX/Irh;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2618593
    iget-object v0, p0, LX/Irh;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2618594
    instance-of v1, p1, LX/IqC;

    if-eqz v1, :cond_1

    .line 2618595
    check-cast p1, LX/IqC;

    .line 2618596
    iget-object v1, p1, LX/IqB;->a:LX/Iqg;

    move-object v1, v1

    .line 2618597
    iget v2, p1, LX/IqC;->a:I

    move v2, v2

    .line 2618598
    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;I)V

    .line 2618599
    iget-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v1, :cond_0

    .line 2618600
    iget-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    invoke-virtual {v1}, LX/IrL;->a()V

    .line 2618601
    :cond_0
    :goto_0
    return-void

    .line 2618602
    :cond_1
    instance-of v1, p1, LX/IqY;

    if-eqz v1, :cond_3

    .line 2618603
    check-cast p1, LX/IqY;

    .line 2618604
    iget-object v1, p1, LX/IqB;->a:LX/Iqg;

    move-object v1, v1

    .line 2618605
    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Iqk;

    .line 2618606
    if-eqz v2, :cond_2

    .line 2618607
    iget-object p0, v2, LX/Iqk;->c:Landroid/view/View;

    move-object p0, p0

    .line 2618608
    iget-object p1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {p1, p0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->removeView(Landroid/view/View;)V

    .line 2618609
    iget-object p1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    invoke-virtual {p1, p0}, LX/Irg;->a(Landroid/view/View;)V

    .line 2618610
    iget-object p0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {p0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618611
    invoke-virtual {v2}, LX/Iqk;->e()V

    .line 2618612
    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v2, :cond_2

    .line 2618613
    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    invoke-virtual {v2}, LX/IrL;->a()V

    .line 2618614
    :cond_2
    goto :goto_0

    .line 2618615
    :cond_3
    instance-of v1, p1, LX/Irt;

    if-eqz v1, :cond_0

    .line 2618616
    check-cast p1, LX/Irt;

    .line 2618617
    iget-object v1, p1, LX/Irt;->a:LX/Iqg;

    move-object v1, v1

    .line 2618618
    iget-object v2, p1, LX/Irt;->b:LX/Iqg;

    move-object v2, v2

    .line 2618619
    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;LX/Iqg;)V

    goto :goto_0
.end method
