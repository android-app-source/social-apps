.class public final LX/HiR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/2wX;",
        "Lcom/google/android/gms/maps/model/LatLng;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/HiS;


# direct methods
.method public constructor <init>(LX/HiS;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2497393
    iput-object p1, p0, LX/HiR;->b:LX/HiS;

    iput-object p2, p0, LX/HiR;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2497394
    check-cast p1, LX/2wX;

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2497395
    if-nez p1, :cond_0

    .line 2497396
    iget-object v0, p0, LX/HiR;->b:LX/HiS;

    iget-object v0, v0, LX/HiS;->c:LX/03V;

    const-string v2, "AddressTypeAheadFetcher"

    const-string v3, "Can\'t connect to Google API client."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2497397
    :goto_0
    return-object v0

    .line 2497398
    :cond_0
    sget-object v0, LX/JF3;->e:LX/JEt;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, LX/HiR;->a:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-interface {v0, p1, v2}, LX/JEt;->a(LX/2wX;[Ljava/lang/String;)LX/2wg;

    move-result-object v0

    .line 2497399
    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, LX/2wg;->a(JLjava/util/concurrent/TimeUnit;)LX/2NW;

    move-result-object v0

    check-cast v0, LX/JEv;

    .line 2497400
    invoke-virtual {v0}, LX/JEv;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    .line 2497401
    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2497402
    iget-object v2, p0, LX/HiR;->b:LX/HiS;

    iget-object v2, v2, LX/HiS;->c:LX/03V;

    const-string v3, "AddressTypeAheadFetcher"

    const-string v4, "Error getting place detail API call."

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2497403
    invoke-virtual {v0}, LX/4sY;->nc_()V

    move-object v0, v1

    .line 2497404
    goto :goto_0

    .line 2497405
    :cond_1
    invoke-static {v0}, LX/4sZ;->a(LX/4sX;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2497406
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2497407
    iget-object v0, p0, LX/HiR;->b:LX/HiS;

    iget-object v0, v0, LX/HiS;->c:LX/03V;

    const-string v2, "AddressTypeAheadFetcher"

    const-string v3, "Can\'t get place detail from google place id."

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2497408
    goto :goto_0

    .line 2497409
    :cond_2
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JEu;

    invoke-interface {v0}, LX/JEu;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method
