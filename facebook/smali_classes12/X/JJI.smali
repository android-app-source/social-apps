.class public LX/JJI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/Date;

.field private b:Ljava/util/Date;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Z


# direct methods
.method public constructor <init>(LX/7oa;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 2679237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2679238
    const/4 v0, -0x1

    iput v0, p0, LX/JJI;->g:I

    .line 2679239
    invoke-interface {p1}, LX/7oa;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->b(J)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->a:Ljava/util/Date;

    .line 2679240
    new-instance v2, Ljava/util/Date;

    invoke-interface {p1}, LX/7oa;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/5O7;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/7oa;->b()J

    move-result-wide v0

    mul-long/2addr v0, v4

    :goto_0
    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, LX/JJI;->b:Ljava/util/Date;

    .line 2679241
    invoke-interface {p1}, LX/7oa;->eR_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->c:Ljava/lang/String;

    .line 2679242
    invoke-interface {p1}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2679243
    invoke-interface {p1}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->d:Ljava/lang/String;

    .line 2679244
    :goto_1
    invoke-interface {p1}, LX/7oa;->E()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->f:Ljava/lang/String;

    .line 2679245
    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->e:Ljava/lang/String;

    .line 2679246
    invoke-interface {p1}, LX/7oa;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJI;->h:Ljava/lang/String;

    .line 2679247
    invoke-interface {p1}, LX/7oa;->eQ_()Z

    move-result v0

    iput-boolean v0, p0, LX/JJI;->i:Z

    .line 2679248
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_3

    .line 2679249
    const/4 v0, 0x1

    iput v0, p0, LX/JJI;->g:I

    .line 2679250
    :cond_0
    :goto_2
    return-void

    .line 2679251
    :cond_1
    invoke-interface {p1}, LX/7oa;->j()J

    move-result-wide v0

    mul-long/2addr v0, v4

    goto :goto_0

    .line 2679252
    :cond_2
    const-string v0, ""

    iput-object v0, p0, LX/JJI;->d:Ljava/lang/String;

    goto :goto_1

    .line 2679253
    :cond_3
    invoke-interface {p1}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_0

    .line 2679254
    const/4 v0, 0x0

    iput v0, p0, LX/JJI;->g:I

    goto :goto_2
.end method


# virtual methods
.method public final b()Ljava/util/Date;
    .locals 1

    .prologue
    .line 2679255
    iget-object v0, p0, LX/JJI;->a:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public final d()Ljava/util/Date;
    .locals 1

    .prologue
    .line 2679256
    iget-object v0, p0, LX/JJI;->b:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method
