.class public LX/Hso;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData:",
        "Ljava/lang/Object;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/HsK;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/HsK;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0il;LX/0Rf;Landroid/view/ViewStub;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Rf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/0Rf",
            "<",
            "LX/HsK;",
            ">;",
            "Landroid/view/ViewStub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515031
    iput-object p2, p0, LX/Hso;->a:LX/0Rf;

    .line 2515032
    new-instance v0, LX/0zw;

    invoke-direct {v0, p3}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, LX/Hso;->b:LX/0zw;

    .line 2515033
    invoke-direct {p0}, LX/Hso;->b()V

    .line 2515034
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2515035
    iget-object v0, p0, LX/Hso;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HsK;

    .line 2515036
    invoke-interface {v0}, LX/HsK;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2515037
    :goto_0
    move-object v0, v0

    .line 2515038
    iget-object v1, p0, LX/Hso;->c:LX/HsK;

    if-ne v1, v0, :cond_2

    .line 2515039
    :cond_1
    :goto_1
    return-void

    .line 2515040
    :cond_2
    invoke-direct {p0}, LX/Hso;->c()V

    .line 2515041
    iput-object v0, p0, LX/Hso;->c:LX/HsK;

    .line 2515042
    iget-object v0, p0, LX/Hso;->c:LX/HsK;

    if-eqz v0, :cond_1

    .line 2515043
    iget-object v1, p0, LX/Hso;->c:LX/HsK;

    iget-object v0, p0, LX/Hso;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-interface {v1, v0}, LX/HsK;->a(Landroid/view/ViewGroup;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2515044
    iget-object v0, p0, LX/Hso;->c:LX/HsK;

    if-eqz v0, :cond_0

    .line 2515045
    iget-object v0, p0, LX/Hso;->c:LX/HsK;

    invoke-interface {v0}, LX/HsK;->b()V

    .line 2515046
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hso;->c:LX/HsK;

    .line 2515047
    iget-object v0, p0, LX/Hso;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2515048
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2515049
    sget-object v0, LX/Hsn;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2515050
    :goto_0
    return-void

    .line 2515051
    :pswitch_0
    invoke-direct {p0}, LX/Hso;->b()V

    goto :goto_0

    .line 2515052
    :pswitch_1
    invoke-direct {p0}, LX/Hso;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;TDerivedData;)V"
        }
    .end annotation

    .prologue
    .line 2515053
    invoke-direct {p0}, LX/Hso;->b()V

    .line 2515054
    return-void
.end method
