.class public LX/HU3;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HU2;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

.field public c:LX/0Uh;

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;LX/0Uh;)V
    .locals 1

    .prologue
    .line 2471526
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2471527
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HU3;->d:Ljava/util/ArrayList;

    .line 2471528
    iput-object p1, p0, LX/HU3;->a:Landroid/content/Context;

    .line 2471529
    iget-object v0, p0, LX/HU3;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/HU3;->e:Landroid/view/LayoutInflater;

    .line 2471530
    iput-object p2, p0, LX/HU3;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    .line 2471531
    iput-object p3, p0, LX/HU3;->c:LX/0Uh;

    .line 2471532
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2471519
    iget-object v0, p0, LX/HU3;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0306a8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2471520
    new-instance v1, LX/HU2;

    invoke-direct {v1, p0, v0}, LX/HU2;-><init>(LX/HU3;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2471522
    check-cast p1, LX/HU2;

    .line 2471523
    iget-object v0, p0, LX/HU3;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    .line 2471524
    invoke-virtual {p1, v0, p2}, LX/HU2;->a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;I)V

    .line 2471525
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2471521
    iget-object v0, p0, LX/HU3;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
