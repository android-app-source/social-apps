.class public LX/JFY;
.super LX/JF9;
.source ""

# interfaces
.implements LX/JEx;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LX/JF9;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput-object p3, p0, LX/JFY;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4

    new-instance v0, LX/JFc;

    iget-object v1, p0, LX/4sg;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget v2, p0, LX/4sg;->b:I

    invoke-direct {v0, v1, v2}, LX/JFc;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    move-object v0, v0

    invoke-interface {v0}, LX/4sd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceEntity;

    const-string v1, "place_likelihood"

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {p0, v1, v2}, LX/JF9;->a(Ljava/lang/String;F)F

    move-result v1

    move v1, v1

    new-instance v3, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;

    const/4 p0, 0x0

    invoke-static {v0}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/places/internal/PlaceEntity;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;-><init>(ILcom/google/android/gms/location/places/internal/PlaceEntity;F)V

    move-object v0, v3

    return-object v0
.end method
