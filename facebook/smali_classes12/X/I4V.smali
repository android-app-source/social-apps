.class public LX/I4V;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/events/eventcollections/view/impl/block/RelatedEventCollectionsBlockView;",
        "Lcom/facebook/events/eventcollections/model/data/RelatedEventCollectionsBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;)V
    .locals 0

    .prologue
    .line 2534132
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2534133
    return-void
.end method

.method private static a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2534134
    const/4 v0, 0x0

    .line 2534135
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->b()LX/8Yr;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->b()LX/8Yr;

    move-result-object v1

    invoke-interface {v1}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2534136
    invoke-virtual {p0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->b()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 2534137
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 8

    .prologue
    .line 2534138
    check-cast p1, LX/I4Q;

    const/4 v4, 0x0

    .line 2534139
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534140
    check-cast v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    .line 2534141
    iget-object v1, p1, LX/I4Q;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    move-object v1, v1

    .line 2534142
    iput-object v1, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2534143
    iget-object v0, p1, LX/I4Q;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    move-object v1, v0

    .line 2534144
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534145
    check-cast v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/I4V;->a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;)Ljava/lang/String;

    move-result-object v1

    .line 2534146
    iput-object v2, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->k:Ljava/lang/String;

    .line 2534147
    iget-object v5, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2534148
    iget-object v6, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v1, :cond_1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    :goto_0
    sget-object v7, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v5, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2534149
    iget-object v0, p1, LX/I4Q;->c:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;

    move-object v1, v0

    .line 2534150
    if-nez v1, :cond_0

    .line 2534151
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534152
    check-cast v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-virtual {v0, v4, v4, v4}, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2534153
    :goto_1
    return-void

    .line 2534154
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534155
    check-cast v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/I4V;->a(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel$CollectionPivotModel$NodesModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2534156
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
