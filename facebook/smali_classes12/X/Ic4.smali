.class public LX/Ic4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/03V;

.field public final d:LX/0tX;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Landroid/view/inputmethod/InputMethodManager;

.field public final g:LX/IZK;

.field public final h:LX/Ib2;

.field public final i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideSignupMessageMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:LX/4At;

.field public m:LX/IdG;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui-thread"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2595124
    const-class v0, LX/Ic4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ic4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;LX/0tX;Landroid/view/inputmethod/InputMethodManager;LX/IZK;LX/Ib2;LX/1Ck;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595115
    iput-object p1, p0, LX/Ic4;->b:Landroid/content/Context;

    .line 2595116
    iput-object p2, p0, LX/Ic4;->c:LX/03V;

    .line 2595117
    iput-object p3, p0, LX/Ic4;->d:LX/0tX;

    .line 2595118
    iput-object p4, p0, LX/Ic4;->f:Landroid/view/inputmethod/InputMethodManager;

    .line 2595119
    iput-object p5, p0, LX/Ic4;->g:LX/IZK;

    .line 2595120
    iput-object p6, p0, LX/Ic4;->h:LX/Ib2;

    .line 2595121
    iput-object p7, p0, LX/Ic4;->i:LX/1Ck;

    .line 2595122
    iput-object p8, p0, LX/Ic4;->e:Ljava/util/concurrent/ExecutorService;

    .line 2595123
    return-void
.end method

.method public static a(LX/0QB;)LX/Ic4;
    .locals 1

    .prologue
    .line 2595113
    invoke-static {p0}, LX/Ic4;->b(LX/0QB;)LX/Ic4;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Ic4;
    .locals 9

    .prologue
    .line 2595111
    new-instance v0, LX/Ic4;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v5

    check-cast v5, LX/IZK;

    invoke-static {p0}, LX/Ib2;->b(LX/0QB;)LX/Ib2;

    move-result-object v6

    check-cast v6, LX/Ib2;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-direct/range {v0 .. v8}, LX/Ic4;-><init>(Landroid/content/Context;LX/03V;LX/0tX;Landroid/view/inputmethod/InputMethodManager;LX/IZK;LX/Ib2;LX/1Ck;Ljava/util/concurrent/ExecutorService;)V

    .line 2595112
    return-object v0
.end method

.method public static b(LX/Ic4;)V
    .locals 1

    .prologue
    .line 2595125
    iget-object v0, p0, LX/Ic4;->l:LX/4At;

    if-eqz v0, :cond_0

    .line 2595126
    iget-object v0, p0, LX/Ic4;->l:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2595127
    :cond_0
    return-void
.end method

.method public static b(LX/Ic4;LX/15i;ILX/IcK;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2595100
    iget-object v0, p0, LX/Ic4;->h:LX/Ib2;

    const-string v1, "show_surge_dialog"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2595101
    iget-object v0, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031228

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2595102
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->l(II)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    .line 2595103
    const v0, 0x7f0d2a83

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2595104
    iget-object v3, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082d71

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v9

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2595105
    const v0, 0x7f0d2a84

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2595106
    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2595107
    new-instance v0, LX/31Y;

    iget-object v4, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-direct {v0, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v7

    iget-object v0, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v0, LX/Ic3;

    move-object v1, p0

    move-object v4, p3

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, LX/Ic3;-><init>(LX/Ic4;Ljava/lang/String;Lcom/facebook/widget/text/BetterEditTextView;LX/IcK;LX/15i;I)V

    invoke-virtual {v7, v8, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/Ic4;->b:Landroid/content/Context;

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ic2;

    invoke-direct {v2, p0}, LX/Ic2;-><init>(LX/Ic4;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2595108
    invoke-virtual {v0}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2595109
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2595110
    return-void
.end method


# virtual methods
.method public final a(LX/15i;ILX/IcK;)V
    .locals 10
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "showSurgeAlert"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2595089
    if-nez p2, :cond_0

    .line 2595090
    iget-object v0, p0, LX/Ic4;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2595091
    :goto_0
    return-void

    .line 2595092
    :cond_0
    iget-object v0, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031229

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2595093
    const v0, 0x7f0d2a85

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2595094
    const v1, 0x7f0d2a87

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 2595095
    iget-object v3, p0, LX/Ic4;->b:Landroid/content/Context;

    const v4, 0x7f082d6e

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v6, 0x2

    invoke-virtual {p1, p2, v6}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2595096
    invoke-virtual {p1, p2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2595097
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595098
    :goto_1
    new-instance v0, LX/31Y;

    iget-object v1, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082d6b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ic1;

    invoke-direct {v2, p0, p1, p2, p3}, LX/Ic1;-><init>(LX/Ic4;LX/15i;ILX/IcK;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    iget-object v1, p0, LX/Ic4;->b:Landroid/content/Context;

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ic0;

    invoke-direct {v2, p0}, LX/Ic0;-><init>(LX/Ic4;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_0

    .line 2595099
    :cond_1
    iget-object v0, p0, LX/Ic4;->b:Landroid/content/Context;

    const v3, 0x7f082d6f

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {p1, p2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(LX/IcK;)V
    .locals 7

    .prologue
    .line 2595045
    iget-object v0, p0, LX/Ic4;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2595046
    :goto_0
    return-void

    .line 2595047
    :cond_0
    iget-object v0, p0, LX/Ic4;->b:Landroid/content/Context;

    const v1, 0x7f082d66

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2595048
    new-instance v1, LX/4At;

    iget-object v2, p0, LX/Ic4;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, LX/Ic4;->l:LX/4At;

    .line 2595049
    iget-object v1, p0, LX/Ic4;->l:LX/4At;

    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2595050
    new-instance v3, LX/4JD;

    invoke-direct {v3}, LX/4JD;-><init>()V

    .line 2595051
    iget-object v4, p1, LX/IcK;->b:Ljava/lang/String;

    .line 2595052
    const-string v5, "thread_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595053
    iget-object v4, p1, LX/IcK;->c:Ljava/lang/String;

    .line 2595054
    const-string v5, "ride_provider_name"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595055
    iget-object v4, p1, LX/IcK;->d:Ljava/lang/String;

    .line 2595056
    const-string v5, "ride_display_name"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595057
    iget-object v4, p1, LX/IcK;->e:Ljava/lang/String;

    .line 2595058
    const-string v5, "ride_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595059
    new-instance v4, LX/2vb;

    invoke-direct {v4}, LX/2vb;-><init>()V

    iget-object v5, p1, LX/IcK;->f:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    move-result-object v4

    iget-object v5, p1, LX/IcK;->f:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    move-result-object v4

    .line 2595060
    const-string v5, "origin"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2595061
    iget-object v4, p1, LX/IcK;->g:Landroid/location/Location;

    if-eqz v4, :cond_1

    .line 2595062
    new-instance v4, LX/2vb;

    invoke-direct {v4}, LX/2vb;-><init>()V

    iget-object v5, p1, LX/IcK;->g:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    move-result-object v4

    iget-object v5, p1, LX/IcK;->g:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    move-result-object v4

    .line 2595063
    const-string v5, "destination"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2595064
    :cond_1
    iget-object v4, p1, LX/IcK;->h:Ljava/lang/String;

    .line 2595065
    const-string v5, "origin_address"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595066
    iget-object v4, p1, LX/IcK;->j:Ljava/lang/String;

    .line 2595067
    const-string v5, "destination_address"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595068
    iget-object v4, p1, LX/IcK;->i:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2595069
    iget-object v4, p1, LX/IcK;->i:Ljava/lang/String;

    .line 2595070
    const-string v5, "origin_place_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595071
    :cond_2
    iget-object v4, p1, LX/IcK;->k:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2595072
    iget-object v4, p1, LX/IcK;->k:Ljava/lang/String;

    .line 2595073
    const-string v5, "destination_place_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595074
    :cond_3
    iget-object v4, p1, LX/IcK;->l:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2595075
    iget-object v4, p1, LX/IcK;->l:Ljava/lang/String;

    .line 2595076
    const-string v5, "payment_credential_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595077
    :cond_4
    iget-object v4, p1, LX/IcK;->m:Ljava/lang/String;

    .line 2595078
    const-string v5, "surge_confirmation_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595079
    iget-object v4, p1, LX/IcK;->n:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2595080
    iget-object v4, p1, LX/IcK;->n:Ljava/lang/String;

    .line 2595081
    const-string v5, "price_lock_token"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595082
    :cond_5
    iget v4, p1, LX/IcK;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2595083
    const-string v5, "seat_count"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2595084
    move-object v0, v3

    .line 2595085
    new-instance v1, LX/Ib7;

    invoke-direct {v1}, LX/Ib7;-><init>()V

    move-object v1, v1

    .line 2595086
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2595087
    iget-object v0, p0, LX/Ic4;->d:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Ic4;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2595088
    iget-object v0, p0, LX/Ic4;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ibx;

    invoke-direct {v1, p0, p1}, LX/Ibx;-><init>(LX/Ic4;LX/IcK;)V

    iget-object v2, p0, LX/Ic4;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0
.end method
