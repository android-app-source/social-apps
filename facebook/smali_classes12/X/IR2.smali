.class public final LX/IR2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V
    .locals 0

    .prologue
    .line 2575994
    iput-object p1, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 2575995
    iget-object v0, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2575996
    iget-object v0, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2575997
    :goto_0
    const-string v1, "%1$s %2$s %3$s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2575998
    new-instance v2, LX/4Ft;

    invoke-direct {v2}, LX/4Ft;-><init>()V

    iget-object v3, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->c:Ljava/lang/String;

    .line 2575999
    const-string v4, "actor_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576000
    move-object v2, v2

    .line 2576001
    iget-object v3, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->m:Ljava/lang/String;

    .line 2576002
    const-string v4, "group_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576003
    move-object v2, v2

    .line 2576004
    const-string v3, "client_mutation_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576005
    move-object v1, v2

    .line 2576006
    const-string v2, "topic_name"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576007
    move-object v0, v1

    .line 2576008
    new-instance v1, LX/9TO;

    invoke-direct {v1}, LX/9TO;-><init>()V

    move-object v1, v1

    .line 2576009
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2576010
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2576011
    iget-object v1, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2576012
    new-instance v1, LX/IR1;

    invoke-direct {v1, p0}, LX/IR1;-><init>(LX/IR2;)V

    iget-object v2, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->b:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576013
    iget-object v0, p0, LX/IR2;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    .line 2576014
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->q:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 2576015
    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->q:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2576016
    :cond_0
    return-void

    .line 2576017
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
