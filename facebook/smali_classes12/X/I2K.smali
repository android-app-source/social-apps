.class public final LX/I2K;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I2L;

.field public final synthetic b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;LX/I2L;)V
    .locals 0

    .prologue
    .line 2530141
    iput-object p1, p0, LX/I2K;->b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    iput-object p2, p0, LX/I2K;->a:LX/I2L;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "Lcom/facebook/events/dashboard/multirow/environment/EventsDashboardEnvironment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2530142
    iget-object v0, p0, LX/I2K;->a:LX/I2L;

    iget-object v2, v0, LX/I2L;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;

    .line 2530143
    iget-object v4, p0, LX/I2K;->b:Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;

    iget-object v4, v4, Lcom/facebook/events/dashboard/multirow/EventCollectionsHScrollPartDefinition;->a:Lcom/facebook/events/dashboard/multirow/EventCollectionPagePartDefinition;

    invoke-virtual {p1, v4, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2530144
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2530145
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2530146
    iget-object v0, p0, LX/I2K;->a:LX/I2L;

    iget-object v0, v0, LX/I2L;->b:LX/99l;

    iget-object v1, p0, LX/I2K;->a:LX/I2L;

    iget-object v1, v1, LX/I2L;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/99l;->a(II)V

    .line 2530147
    return-void
.end method
