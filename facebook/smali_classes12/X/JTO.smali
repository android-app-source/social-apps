.class public LX/JTO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/concurrent/ExecutorService;

.field private b:Ljava/lang/String;

.field private c:LX/0tX;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2696634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696635
    iput-object p1, p0, LX/JTO;->a:Ljava/util/concurrent/ExecutorService;

    .line 2696636
    iput-object p2, p0, LX/JTO;->b:Ljava/lang/String;

    .line 2696637
    iput-object p3, p0, LX/JTO;->c:LX/0tX;

    .line 2696638
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;LX/JTK;LX/JTF;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2696639
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2696640
    new-instance v0, LX/4JS;

    invoke-direct {v0}, LX/4JS;-><init>()V

    iget-object v1, p0, LX/JTO;->b:Ljava/lang/String;

    .line 2696641
    const-string v2, "actor_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696642
    move-object v0, v0

    .line 2696643
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2696644
    const-string v2, "audio_uri"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696645
    move-object v0, v0

    .line 2696646
    if-eqz p1, :cond_0

    .line 2696647
    const-string v1, "auth_code"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2696648
    :cond_0
    new-instance v1, LX/JTP;

    invoke-direct {v1}, LX/JTP;-><init>()V

    move-object v1, v1

    .line 2696649
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2696650
    iget-object v0, p0, LX/JTO;->c:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2696651
    new-instance v1, LX/JTN;

    invoke-direct {v1, p0, p3, p4}, LX/JTN;-><init>(LX/JTO;LX/JTK;LX/JTF;)V

    iget-object v2, p0, LX/JTO;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2696652
    return-void
.end method
