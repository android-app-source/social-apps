.class public LX/Iil;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Iik;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/widget/text/BetterTextView;

.field private final f:Lcom/facebook/widget/text/BetterTextView;

.field private final g:Lcom/facebook/widget/text/BetterButton;

.field private final h:Landroid/support/v7/internal/widget/ViewStubCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2605029
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Iil;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2605030
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605031
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Iil;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605032
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605033
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605034
    const-class v0, LX/Iil;

    invoke-static {v0, p0}, LX/Iil;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605035
    const v0, 0x7f0303fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605036
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Iil;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2605037
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Iil;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2605038
    const v0, 0x7f0d0c44

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Iil;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2605039
    const v0, 0x7f0d0c45

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Iil;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2605040
    const v0, 0x7f0d0c46

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Iil;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2605041
    const v0, 0x7f0d0393

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Iil;->g:Lcom/facebook/widget/text/BetterButton;

    .line 2605042
    const v0, 0x7f0d0c47

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    iput-object v0, p0, LX/Iil;->h:Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 2605043
    return-void
.end method

.method private static a(LX/Iil;Lcom/facebook/widget/text/BetterTextView;LX/Iix;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2605044
    iget v0, p2, LX/Iix;->a:I

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2605045
    iget v0, p2, LX/Iix;->b:I

    invoke-virtual {p1, v0, v1, v1, v1}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2605046
    invoke-virtual {p0}, LX/Iil;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a097f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2605047
    iget-object v1, p0, LX/Iil;->a:LX/0wM;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 p2, 0x0

    aget-object v2, v2, p2

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2605048
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Iil;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/Iil;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public setListener(LX/Iit;)V
    .locals 2

    .prologue
    .line 2605049
    iget-object v0, p0, LX/Iil;->g:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/Iij;

    invoke-direct {v1, p0, p1}, LX/Iij;-><init>(LX/Iil;LX/Iit;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605050
    return-void
.end method

.method public setViewParams(LX/Iim;)V
    .locals 2

    .prologue
    .line 2605051
    iget-object v0, p0, LX/Iil;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iim;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2605052
    iget-object v0, p0, LX/Iil;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iim;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2605053
    iget-object v0, p0, LX/Iil;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iim;->c:LX/Iix;

    invoke-static {p0, v0, v1}, LX/Iil;->a(LX/Iil;Lcom/facebook/widget/text/BetterTextView;LX/Iix;)V

    .line 2605054
    iget-object v0, p0, LX/Iil;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iim;->d:LX/Iix;

    invoke-static {p0, v0, v1}, LX/Iil;->a(LX/Iil;Lcom/facebook/widget/text/BetterTextView;LX/Iix;)V

    .line 2605055
    iget-object v0, p0, LX/Iil;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iim;->e:LX/Iix;

    invoke-static {p0, v0, v1}, LX/Iil;->a(LX/Iil;Lcom/facebook/widget/text/BetterTextView;LX/Iix;)V

    .line 2605056
    iget-object v0, p0, LX/Iil;->g:Lcom/facebook/widget/text/BetterButton;

    iget v1, p1, LX/Iim;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2605057
    iget-object v0, p0, LX/Iil;->h:Landroid/support/v7/internal/widget/ViewStubCompat;

    iget v1, p1, LX/Iim;->g:I

    .line 2605058
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605059
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ViewStubCompat;->setLayoutResource(I)V

    .line 2605060
    new-instance p0, LX/4ob;

    invoke-direct {p0, v0}, LX/4ob;-><init>(Landroid/support/v7/internal/widget/ViewStubCompat;)V

    move-object v0, p0

    .line 2605061
    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2605062
    return-void
.end method
