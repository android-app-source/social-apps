.class public final LX/IBM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;)V
    .locals 0

    .prologue
    .line 2546940
    iput-object p1, p0, LX/IBM;->a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x2

    const v0, 0x59049951

    invoke-static {v2, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2546941
    iget-object v1, p0, LX/IBM;->a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    iget-object v1, v1, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->h:Lcom/facebook/events/model/Event;

    if-nez v1, :cond_0

    .line 2546942
    const v1, -0xda5d41b

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2546943
    :goto_0
    return-void

    .line 2546944
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2546945
    iget-object v2, p0, LX/IBM;->a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    iget-object v2, v2, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->c:LX/17Y;

    sget-object v3, LX/0ax;->cD:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/IBM;->a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    iget-object v6, v6, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->h:Lcom/facebook/events/model/Event;

    .line 2546946
    iget-object p1, v6, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v6, p1

    .line 2546947
    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2546948
    if-eqz v2, :cond_1

    .line 2546949
    iget-object v3, p0, LX/IBM;->a:Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    iget-object v3, v3, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2546950
    :cond_1
    const v1, -0x68affd6e

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
