.class public final LX/IEo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "LX/8li;",
        "LX/8lj;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IEr;


# direct methods
.method public constructor <init>(LX/IEr;)V
    .locals 0

    .prologue
    .line 2553386
    iput-object p1, p0, LX/IEo;->a:LX/IEr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 2553387
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2553388
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2553389
    check-cast p2, LX/8lj;

    .line 2553390
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    iget-object v1, p2, LX/8lj;->a:LX/0Px;

    .line 2553391
    iput-object v1, v0, LX/IEr;->e:LX/0Px;

    .line 2553392
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    sget-object v1, LX/IEq;->LOADED:LX/IEq;

    .line 2553393
    iput-object v1, v0, LX/IEr;->c:LX/IEq;

    .line 2553394
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    iget-object v0, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IEw;

    .line 2553395
    iget-object v2, p0, LX/IEo;->a:LX/IEr;

    iget-object v2, v2, LX/IEr;->e:LX/0Px;

    invoke-virtual {v0, v2}, LX/IEw;->a(LX/0Px;)V

    goto :goto_0

    .line 2553396
    :cond_0
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    iget-object v0, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2553397
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2553398
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    sget-object v1, LX/IEq;->FAILED:LX/IEq;

    .line 2553399
    iput-object v1, v0, LX/IEr;->c:LX/IEq;

    .line 2553400
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    iget-object v0, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IEw;

    .line 2553401
    iget-object v2, v0, LX/IEw;->a:Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerFragment;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    new-instance p1, LX/IEv;

    invoke-direct {p1, v0}, LX/IEv;-><init>(LX/IEw;)V

    const/4 p2, 0x0

    invoke-virtual {v2, p1, p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(LX/1DI;Ljava/lang/Runnable;)V

    .line 2553402
    goto :goto_0

    .line 2553403
    :cond_0
    iget-object v0, p0, LX/IEo;->a:LX/IEr;

    iget-object v0, v0, LX/IEr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2553404
    return-void
.end method
