.class public LX/JJJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final c:Landroid/content/Context;

.field public d:Landroid/content/ContentProviderClient;

.field public e:Landroid/accounts/Account;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2679257
    const-class v0, LX/JJJ;

    sput-object v0, LX/JJJ;->a:Ljava/lang/Class;

    .line 2679258
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, LX/JJJ;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2679260
    iput-object p1, p0, LX/JJJ;->c:Landroid/content/Context;

    .line 2679261
    return-void
.end method

.method public static b(LX/JJJ;)J
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 2679262
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    iget-object v2, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2679263
    :try_start_0
    iget-object v0, p0, LX/JJJ;->d:Landroid/content/ContentProviderClient;

    sget-object v2, LX/JJJ;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2679264
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2679265
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 2679266
    :goto_0
    return-wide v0

    .line 2679267
    :catch_0
    move-wide v0, v6

    goto :goto_0

    :cond_0
    move-wide v0, v6

    .line 2679268
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/JJH;
    .locals 15

    .prologue
    .line 2679269
    const-wide/16 v10, -0x1

    .line 2679270
    invoke-static {p0}, LX/JJJ;->b(LX/JJJ;)J

    move-result-wide v8

    .line 2679271
    cmp-long v12, v8, v10

    if-eqz v12, :cond_1

    .line 2679272
    :goto_0
    move-wide v3, v8

    .line 2679273
    const-wide/16 v1, -0x1

    cmp-long v1, v3, v1

    if-nez v1, :cond_0

    .line 2679274
    const/4 v2, 0x0

    .line 2679275
    :goto_1
    move-object v0, v2

    .line 2679276
    return-object v0

    :cond_0
    new-instance v2, LX/JJH;

    iget-object v5, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v6, p0, LX/JJJ;->d:Landroid/content/ContentProviderClient;

    iget-object v7, p0, LX/JJJ;->c:Landroid/content/Context;

    invoke-direct/range {v2 .. v7}, LX/JJH;-><init>(JLandroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/Context;)V

    goto :goto_1

    .line 2679277
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2679278
    sget-object v9, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v12, "caller_is_syncadapter"

    const-string v13, "true"

    invoke-virtual {v9, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v12, "account_name"

    iget-object v13, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v12, "account_type"

    iget-object v13, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "account_name"

    iget-object v13, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "account_type"

    iget-object v13, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "name"

    const-string v13, "facebook_event"

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "calendar_displayName"

    iget-object v13, p0, LX/JJJ;->c:Landroid/content/Context;

    const v14, 0x7f083a60

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "calendar_access_level"

    const/16 v13, 0xc8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "ownerAccount"

    iget-object v13, p0, LX/JJJ;->e:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    const-string v12, "sync_events"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    .line 2679279
    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2679280
    const/4 v9, 0x0

    .line 2679281
    :try_start_0
    iget-object v12, p0, LX/JJJ;->d:Landroid/content/ContentProviderClient;

    invoke-virtual {v12, v8}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2679282
    const/4 v9, 0x1

    :goto_2
    move v8, v9

    .line 2679283
    if-nez v8, :cond_2

    move-wide v8, v10

    .line 2679284
    goto/16 :goto_0

    .line 2679285
    :cond_2
    invoke-static {p0}, LX/JJJ;->b(LX/JJJ;)J

    move-result-wide v8

    goto/16 :goto_0

    .line 2679286
    :catch_0
    goto :goto_2

    .line 2679287
    :catch_1
    goto :goto_2
.end method
