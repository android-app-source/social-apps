.class public LX/Hwa;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Hwa;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/Hwc;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2521087
    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "connections_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 2521088
    return-void
.end method

.method public static a(LX/0QB;)LX/Hwa;
    .locals 6

    .prologue
    .line 2521089
    sget-object v0, LX/Hwa;->a:LX/Hwa;

    if-nez v0, :cond_1

    .line 2521090
    const-class v1, LX/Hwa;

    monitor-enter v1

    .line 2521091
    :try_start_0
    sget-object v0, LX/Hwa;->a:LX/Hwa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2521092
    if-eqz v2, :cond_0

    .line 2521093
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2521094
    new-instance p0, LX/Hwa;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/Hwc;->a(LX/0QB;)LX/Hwc;

    move-result-object v5

    check-cast v5, LX/Hwc;

    invoke-direct {p0, v3, v4, v5}, LX/Hwa;-><init>(Landroid/content/Context;LX/0Tt;LX/Hwc;)V

    .line 2521095
    move-object v0, p0

    .line 2521096
    sput-object v0, LX/Hwa;->a:LX/Hwa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2521097
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2521098
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2521099
    :cond_1
    sget-object v0, LX/Hwa;->a:LX/Hwa;

    return-object v0

    .line 2521100
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2521101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
