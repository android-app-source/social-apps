.class public final LX/JGw;
.super LX/JGT;
.source ""


# direct methods
.method public static a([LX/JGM;[F[FLandroid/util/SparseIntArray;)V
    .locals 4

    .prologue
    .line 2669477
    const/4 v1, 0x0

    .line 2669478
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    array-length v0, p0

    if-ge v1, v0, :cond_1

    .line 2669479
    aget-object v0, p0, v1

    instance-of v0, v0, LX/JGY;

    if-eqz v0, :cond_0

    .line 2669480
    aget-object v0, p0, v1

    check-cast v0, LX/JGY;

    .line 2669481
    iget v3, v0, LX/JGY;->d:I

    invoke-virtual {p3, v3, v1}, Landroid/util/SparseIntArray;->append(II)V

    .line 2669482
    iget v0, v0, LX/JGY;->h:F

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2669483
    :goto_1
    aput v2, p1, v1

    .line 2669484
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2669485
    :cond_0
    aget-object v0, p0, v1

    invoke-virtual {v0}, LX/JGM;->l()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto :goto_1

    .line 2669486
    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_3

    .line 2669487
    aget-object v0, p0, v1

    instance-of v0, v0, LX/JGY;

    if-eqz v0, :cond_2

    .line 2669488
    aget-object v0, p0, v1

    check-cast v0, LX/JGY;

    iget v0, v0, LX/JGY;->f:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2669489
    :goto_3
    aput v0, p2, v1

    .line 2669490
    add-int/lit8 v1, v1, -0x1

    move v2, v0

    goto :goto_2

    .line 2669491
    :cond_2
    aget-object v0, p0, v1

    invoke-virtual {v0}, LX/JGM;->j()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_3

    .line 2669492
    :cond_3
    return-void
.end method

.method public static b([LX/JGu;[F[F)V
    .locals 3

    .prologue
    .line 2669493
    const/4 v1, 0x0

    .line 2669494
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 2669495
    aget-object v2, p0, v0

    invoke-virtual {v2}, LX/JGu;->c()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2669496
    aput v1, p1, v0

    .line 2669497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2669498
    :cond_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 2669499
    aget-object v2, p0, v0

    invoke-virtual {v2}, LX/JGu;->a()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2669500
    aput v1, p2, v0

    .line 2669501
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2669502
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2669503
    iget-object v0, p0, LX/JGT;->a:[F

    iget-object v1, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([FF)I

    move-result v0

    .line 2669504
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(FF)I
    .locals 2

    .prologue
    .line 2669505
    iget-object v0, p0, LX/JGT;->d:[F

    const v1, 0x38d1b717    # 1.0E-4f

    add-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([FF)I

    move-result v0

    .line 2669506
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 2669507
    iget-object v0, p0, LX/JGT;->b:[F

    iget-object v1, p0, LX/JGT;->b:[F

    array-length v1, v1

    iget-object v2, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    invoke-static {v0, p1, v1, v2}, Ljava/util/Arrays;->binarySearch([FIIF)I

    move-result v0

    .line 2669508
    if-gez v0, :cond_0

    xor-int/lit8 v0, v0, -0x1

    :cond_0
    return v0
.end method

.method public final a(IFF)Z
    .locals 1

    .prologue
    .line 2669509
    iget-object v0, p0, LX/JGT;->c:[F

    aget v0, v0, p1

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
