.class public LX/HaQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/8tu;

.field public final c:LX/HaJ;

.field public final d:Lcom/facebook/registration/model/SimpleRegFormData;

.field private final e:LX/0i4;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9o;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0aG;

.field private final i:LX/HZt;

.field private final j:LX/GvB;

.field private final k:LX/0Uh;

.field public final l:LX/1Ml;

.field public final m:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final n:LX/0lB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8tu;LX/HaJ;Lcom/facebook/registration/model/SimpleRegFormData;LX/0i4;LX/0Ot;LX/0Ot;LX/0aG;LX/HZt;LX/GvB;LX/0Uh;LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;)V
    .locals 0
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/8tu;",
            "LX/HaJ;",
            "Lcom/facebook/registration/model/SimpleRegFormData;",
            "LX/0i4;",
            "LX/0Ot",
            "<",
            "LX/F9o;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0aG;",
            "LX/HZt;",
            "Lcom/facebook/auth/login/ipc/LaunchAuthActivityUtil;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Ml;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483799
    iput-object p1, p0, LX/HaQ;->a:Landroid/content/Context;

    .line 2483800
    iput-object p2, p0, LX/HaQ;->b:LX/8tu;

    .line 2483801
    iput-object p3, p0, LX/HaQ;->c:LX/HaJ;

    .line 2483802
    iput-object p4, p0, LX/HaQ;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2483803
    iput-object p5, p0, LX/HaQ;->e:LX/0i4;

    .line 2483804
    iput-object p6, p0, LX/HaQ;->f:LX/0Ot;

    .line 2483805
    iput-object p7, p0, LX/HaQ;->g:LX/0Ot;

    .line 2483806
    iput-object p8, p0, LX/HaQ;->h:LX/0aG;

    .line 2483807
    iput-object p9, p0, LX/HaQ;->i:LX/HZt;

    .line 2483808
    iput-object p10, p0, LX/HaQ;->j:LX/GvB;

    .line 2483809
    iput-object p11, p0, LX/HaQ;->k:LX/0Uh;

    .line 2483810
    iput-object p12, p0, LX/HaQ;->l:LX/1Ml;

    .line 2483811
    iput-object p13, p0, LX/HaQ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2483812
    iput-object p14, p0, LX/HaQ;->n:LX/0lB;

    .line 2483813
    return-void
.end method

.method public static a$redex0(LX/HaQ;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2483764
    iget-object v0, p0, LX/HaQ;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/F9o;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2483765
    new-instance v2, LX/HaP;

    invoke-direct {v2, p0, p1}, LX/HaP;-><init>(LX/HaQ;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v0, p0, LX/HaQ;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2483766
    return-void
.end method

.method public static b(LX/0QB;)LX/HaQ;
    .locals 15

    .prologue
    .line 2483814
    new-instance v0, LX/HaQ;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v2

    check-cast v2, LX/8tu;

    const-class v3, LX/HaJ;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HaJ;

    invoke-static {p0}, Lcom/facebook/registration/model/SimpleRegFormData;->a(LX/0QB;)Lcom/facebook/registration/model/SimpleRegFormData;

    move-result-object v4

    check-cast v4, Lcom/facebook/registration/model/SimpleRegFormData;

    const-class v5, LX/0i4;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0i4;

    const/16 v6, 0x24d6

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1430

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {p0}, LX/HZt;->b(LX/0QB;)LX/HZt;

    move-result-object v9

    check-cast v9, LX/HZt;

    invoke-static {p0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v10

    check-cast v10, LX/GvB;

    invoke-static {p0}, LX/0WW;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {p0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v12

    check-cast v12, LX/1Ml;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v13

    check-cast v13, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v14

    check-cast v14, LX/0lB;

    invoke-direct/range {v0 .. v14}, LX/HaQ;-><init>(Landroid/content/Context;LX/8tu;LX/HaJ;Lcom/facebook/registration/model/SimpleRegFormData;LX/0i4;LX/0Ot;LX/0Ot;LX/0aG;LX/HZt;LX/GvB;LX/0Uh;LX/1Ml;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;)V

    .line 2483815
    return-object v0
.end method

.method public static f(LX/HaQ;)Z
    .locals 3

    .prologue
    .line 2483790
    iget-object v0, p0, LX/HaQ;->k:LX/0Uh;

    const/16 v1, 0x3d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2483791
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    .line 2483792
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2483793
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HaQ;->e:LX/0i4;

    invoke-virtual {v0, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2483794
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 2483795
    sget-object v2, LX/HYk;->a:[Ljava/lang/String;

    new-instance p1, LX/HaO;

    invoke-direct {p1, p0, v1}, LX/HaO;-><init>(LX/HaQ;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v0, v2, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2483796
    move-object v0, v1

    .line 2483797
    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2483776
    invoke-static {p1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2483777
    if-eqz p3, :cond_0

    .line 2483778
    new-instance v0, LX/0ju;

    invoke-direct {v0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f083599

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08359a

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080020

    new-instance v2, LX/HaN;

    invoke-direct {v2, p0, p1, p2}, LX/HaN;-><init>(LX/HaQ;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080021

    new-instance v2, LX/HaM;

    invoke-direct {v2, p0}, LX/HaM;-><init>(LX/HaQ;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/HaL;

    invoke-direct {v1, p0}, LX/HaL;-><init>(LX/HaQ;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2483779
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2483780
    :goto_0
    return-void

    .line 2483781
    :cond_0
    invoke-static {p0}, LX/HaQ;->f(LX/HaQ;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2483782
    :cond_1
    :goto_1
    iget-object v0, p0, LX/HaQ;->i:LX/HZt;

    invoke-virtual {v0, p2}, LX/HZt;->b(Ljava/lang/String;)V

    .line 2483783
    iget-object v0, p0, LX/HaQ;->j:LX/GvB;

    invoke-virtual {v0, p1}, LX/GvB;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 2483784
    :cond_2
    const/4 v1, 0x0

    .line 2483785
    :try_start_0
    iget-object v0, p0, LX/HaQ;->n:LX/0lB;

    iget-object v2, p0, LX/HaQ;->d:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2483786
    :goto_2
    if-eqz v0, :cond_1

    .line 2483787
    iget-object v1, p0, LX/HaQ;->m:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Ha8;->d:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1

    .line 2483788
    :catch_0
    move-exception v0

    .line 2483789
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string p3, "Error converting RegistrationFormData to JSON"

    invoke-static {v2, p3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 9

    .prologue
    const/16 v8, 0x21

    .line 2483767
    iget-object v0, p0, LX/HaQ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2483768
    new-instance v1, LX/47x;

    invoke-direct {v1, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2483769
    const v2, 0x7f0835ab

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "[[fb_terms]]"

    const-string v4, "[[data_policy]]"

    const-string v5, "[[cookies_Use_policy]]"

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2483770
    const-string v2, "[[fb_terms]]"

    const v3, 0x7f0835b0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/HaQ;->c:LX/HaJ;

    sget-object v5, LX/HaH;->BROWSER:LX/HaH;

    const-string v6, "https://m.facebook.com/legal/terms/"

    invoke-virtual {v4, v5, v6}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    const-string v3, "[[data_policy]]"

    const v4, 0x7f0835b1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/HaQ;->c:LX/HaJ;

    sget-object v6, LX/HaH;->BROWSER:LX/HaH;

    const-string v7, "https://m.facebook.com/about/privacy/"

    invoke-virtual {v5, v6, v7}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    const-string v3, "[[cookies_Use_policy]]"

    const v4, 0x7f0835b2

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, LX/HaQ;->c:LX/HaJ;

    sget-object v5, LX/HaH;->BROWSER:LX/HaH;

    const-string v6, "https://m.facebook.com/help/cookies"

    invoke-virtual {v4, v5, v6}, LX/HaJ;->a(LX/HaH;Ljava/lang/String;)LX/HaI;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2483771
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 2483772
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2483773
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2483774
    iget-object v0, p0, LX/HaQ;->b:LX/8tu;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2483775
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2483763
    iget-object v0, p0, LX/HaQ;->k:LX/0Uh;

    const/16 v1, 0x35

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
