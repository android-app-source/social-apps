.class public LX/Hi5;
.super LX/0gG;
.source ""


# instance fields
.field public final a:LX/0gG;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gG;)V
    .locals 0
    .param p1    # LX/0gG;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2496805
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2496806
    iput-object p1, p0, LX/Hi5;->a:LX/0gG;

    .line 2496807
    return-void
.end method


# virtual methods
.method public G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2496804
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2496803
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2496802
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1, p2}, LX/0gG;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 2496800
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 2496801
    return-void
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 1

    .prologue
    .line 2496798
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1, p2}, LX/0gG;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 2496799
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2496782
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    .line 2496783
    return-void
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2496796
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1, p2, p3}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2496797
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2496795
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1, p2}, LX/0gG;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2496794
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 2496792
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 2496793
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2496790
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 2496791
    return-void
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2496788
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1, p2, p3}, LX/0gG;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2496789
    return-void
.end method

.method public d(I)F
    .locals 1

    .prologue
    .line 2496787
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0, p1}, LX/0gG;->d(I)F

    move-result v0

    return v0
.end method

.method public final kV_()V
    .locals 1

    .prologue
    .line 2496785
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2496786
    return-void
.end method

.method public final lf_()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2496784
    iget-object v0, p0, LX/Hi5;->a:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->lf_()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
