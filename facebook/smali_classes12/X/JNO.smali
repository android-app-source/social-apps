.class public LX/JNO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNM;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemFacepileComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2685802
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JNO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemFacepileComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685803
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2685804
    iput-object p1, p0, LX/JNO;->b:LX/0Ot;

    .line 2685805
    return-void
.end method

.method public static a(LX/0QB;)LX/JNO;
    .locals 4

    .prologue
    .line 2685806
    const-class v1, LX/JNO;

    monitor-enter v1

    .line 2685807
    :try_start_0
    sget-object v0, LX/JNO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685808
    sput-object v2, LX/JNO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685809
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685810
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685811
    new-instance v3, LX/JNO;

    const/16 p0, 0x1ee7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNO;-><init>(LX/0Ot;)V

    .line 2685812
    move-object v0, v3

    .line 2685813
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685814
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685815
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685816
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2685817
    check-cast p2, LX/JNN;

    .line 2685818
    iget-object v0, p0, LX/JNO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemFacepileComponentSpec;

    iget-object v1, p2, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    const/4 p0, 0x2

    const/4 p2, 0x1

    const/4 v4, 0x0

    .line 2685819
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2685820
    :cond_0
    const/4 v2, 0x0

    .line 2685821
    :goto_0
    move-object v0, v2

    .line 2685822
    return-object v0

    .line 2685823
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2685824
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bK()Lcom/facebook/graphql/model/GraphQLEventMembersConnection;

    move-result-object v2

    .line 2685825
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->l()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 2685826
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventMembersConnection;->k()LX/0Px;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2685827
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bL()Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;

    move-result-object v2

    .line 2685828
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->l()I

    move-result v6

    add-int/2addr v6, v3

    .line 2685829
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventWatchersConnection;->k()LX/0Px;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2685830
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v3, v4

    .line 2685831
    :goto_1
    const/4 v2, 0x3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 2685832
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2685833
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2685834
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2685835
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const v5, 0x7f0b010f

    invoke-interface {v2, v3, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemFacepileComponentSpec;->b:LX/8yV;

    invoke-virtual {v3, p1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemFacepileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v3

    const/high16 v5, -0x80000000

    invoke-virtual {v3, v5}, LX/8yU;->q(I)LX/8yU;

    move-result-object v3

    const v5, 0x7f0b254a

    invoke-virtual {v3, v5}, LX/8yU;->l(I)LX/8yU;

    move-result-object v3

    const v5, 0x7f0a00d5

    invoke-virtual {v3, v5}, LX/8yU;->i(I)LX/8yU;

    move-result-object v3

    const v5, 0x7f0b254d

    invoke-virtual {v3, v5}, LX/8yU;->k(I)LX/8yU;

    move-result-object v3

    const v5, 0x7f0b254b

    invoke-virtual {v3, v5}, LX/8yU;->h(I)LX/8yU;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x5

    const v7, 0x7f0b254c

    invoke-interface {v3, v5, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0f0185

    new-array v8, p2, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v8, v4

    invoke-virtual {v5, v7, v6, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b004e

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a010e

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v3, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2685836
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2685837
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v4, 0x7f0a011a

    invoke-virtual {v3, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b0033

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b010f

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2685838
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2685839
    invoke-static {}, LX/1dS;->b()V

    .line 2685840
    const/4 v0, 0x0

    return-object v0
.end method
