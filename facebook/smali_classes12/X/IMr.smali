.class public final LX/IMr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/search/CommunitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/search/CommunitySearchFragment;)V
    .locals 0

    .prologue
    .line 2570269
    iput-object p1, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2570247
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->r:LX/DSo;

    iget-object v1, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->q:LX/7HZ;

    const/4 v3, 0x0

    .line 2570248
    invoke-virtual {v0}, LX/DSo;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2570249
    :cond_0
    :goto_0
    move v0, v3

    .line 2570250
    if-eqz v0, :cond_1

    .line 2570251
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    .line 2570252
    iput-boolean v2, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    .line 2570253
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    iget-object v1, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-boolean v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->p:Z

    invoke-virtual {v0, v1}, LX/INF;->b(Z)V

    .line 2570254
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    .line 2570255
    iget-object v1, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2570256
    if-eqz v0, :cond_2

    .line 2570257
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->l:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2570258
    :cond_1
    :goto_2
    return-void

    .line 2570259
    :cond_2
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->l:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2570260
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->n:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 2570261
    iget-object v2, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->i:LX/INF;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2570262
    iget-object v0, v2, LX/INF;->b:LX/7Hg;

    .line 2570263
    iget-object v1, v0, LX/7Hg;->b:LX/7He;

    invoke-static {v1}, LX/7He;->b(LX/7He;)V

    .line 2570264
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2570265
    invoke-virtual {v2}, LX/7HQ;->d()V

    .line 2570266
    :cond_3
    goto :goto_2

    .line 2570267
    :cond_4
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 2570268
    add-int v4, p2, p3

    add-int/lit8 v4, v4, 0x3

    if-le v4, p4, :cond_0

    add-int v4, p2, p3

    add-int/lit8 p1, p4, -0xa

    if-le v4, p1, :cond_0

    if-le p4, p3, :cond_0

    sget-object v4, LX/7HZ;->ACTIVE:LX/7HZ;

    if-eq v1, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 2570243
    iget-object v0, p0, LX/IMr;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    .line 2570244
    if-eqz p1, :cond_0

    .line 2570245
    iget-object v1, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p0

    const/4 p2, 0x0

    invoke-virtual {v1, p0, p2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2570246
    :cond_0
    return-void
.end method
