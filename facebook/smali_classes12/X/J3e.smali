.class public LX/J3e;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/J3g;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/HuN;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HuN;)V
    .locals 1

    .prologue
    .line 2643166
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2643167
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/J3e;->b:Ljava/util/List;

    .line 2643168
    iput-object p1, p0, LX/J3e;->a:LX/HuN;

    .line 2643169
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2643170
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2643171
    const v1, 0x7f0309e7

    if-eq p2, v1, :cond_0

    const v1, 0x7f0309e8

    if-ne p2, v1, :cond_1

    .line 2643172
    :cond_0
    new-instance v1, LX/J3g;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/J3e;->a:LX/HuN;

    invoke-direct {v1, v0, v2}, LX/J3g;-><init>(Lcom/facebook/resources/ui/FbTextView;LX/HuN;)V

    move-object v0, v1

    .line 2643173
    :goto_0
    return-object v0

    .line 2643174
    :cond_1
    const v1, 0x7f0309e6

    if-ne p2, v1, :cond_2

    .line 2643175
    new-instance v1, LX/J3d;

    invoke-direct {v1, p0}, LX/J3d;-><init>(LX/J3e;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2643176
    :cond_2
    new-instance v1, LX/J3g;

    invoke-direct {v1, v0}, LX/J3g;-><init>(Landroid/view/View;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2643177
    check-cast p1, LX/J3g;

    .line 2643178
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2643179
    const v1, 0x7f0309e6

    if-ne v0, v1, :cond_0

    .line 2643180
    :goto_0
    return-void

    .line 2643181
    :cond_0
    iget-object v0, p0, LX/J3e;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2643182
    iput-object v0, p1, LX/J3g;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2643183
    iput p2, p1, LX/J3g;->n:I

    .line 2643184
    iget-object v1, p1, LX/J3g;->l:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_1

    .line 2643185
    iget-object v1, p1, LX/J3g;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2643186
    :cond_1
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2643187
    if-nez p1, :cond_0

    .line 2643188
    const v0, 0x7f0309e8

    .line 2643189
    :goto_0
    return v0

    .line 2643190
    :cond_0
    iget-object v0, p0, LX/J3e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2643191
    if-ne p1, v0, :cond_1

    .line 2643192
    const v0, 0x7f0309e6

    goto :goto_0

    .line 2643193
    :cond_1
    const v0, 0x7f0309e7

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2643194
    iget-object v0, p0, LX/J3e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
