.class public LX/Hte;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/Htv;

.field public c:LX/Hs6;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/Htv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516522
    iput-object p1, p0, LX/Hte;->a:Landroid/content/res/Resources;

    .line 2516523
    iput-object p2, p0, LX/Hte;->b:LX/Htv;

    .line 2516524
    return-void
.end method

.method public static b(LX/0QB;)LX/Hte;
    .locals 3

    .prologue
    .line 2516525
    new-instance v2, LX/Hte;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/Htv;->a(LX/0QB;)LX/Htv;

    move-result-object v1

    check-cast v1, LX/Htv;

    invoke-direct {v2, v0, v1}, LX/Hte;-><init>(Landroid/content/res/Resources;LX/Htv;)V

    .line 2516526
    return-object v2
.end method
