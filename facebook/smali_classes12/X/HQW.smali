.class public final enum LX/HQW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HQW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HQW;

.field public static final enum NOT_VISIBLE:LX/HQW;

.field public static final enum VISIBLE:LX/HQW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2463673
    new-instance v0, LX/HQW;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, LX/HQW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQW;->VISIBLE:LX/HQW;

    .line 2463674
    new-instance v0, LX/HQW;

    const-string v1, "NOT_VISIBLE"

    invoke-direct {v0, v1, v3}, LX/HQW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQW;->NOT_VISIBLE:LX/HQW;

    .line 2463675
    const/4 v0, 0x2

    new-array v0, v0, [LX/HQW;

    sget-object v1, LX/HQW;->VISIBLE:LX/HQW;

    aput-object v1, v0, v2

    sget-object v1, LX/HQW;->NOT_VISIBLE:LX/HQW;

    aput-object v1, v0, v3

    sput-object v0, LX/HQW;->$VALUES:[LX/HQW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2463676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HQW;
    .locals 1

    .prologue
    .line 2463677
    const-class v0, LX/HQW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HQW;

    return-object v0
.end method

.method public static values()[LX/HQW;
    .locals 1

    .prologue
    .line 2463678
    sget-object v0, LX/HQW;->$VALUES:[LX/HQW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HQW;

    return-object v0
.end method
