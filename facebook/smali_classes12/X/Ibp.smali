.class public LX/Ibp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/Ic4;

.field private final c:LX/Ibu;

.field public final d:LX/IZK;

.field public e:LX/HiV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/4At;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ic4;LX/Ibu;LX/IZK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2594791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2594792
    iput-object p1, p0, LX/Ibp;->a:Landroid/content/Context;

    .line 2594793
    iput-object p2, p0, LX/Ibp;->b:LX/Ic4;

    .line 2594794
    iput-object p3, p0, LX/Ibp;->c:LX/Ibu;

    .line 2594795
    iput-object p4, p0, LX/Ibp;->d:LX/IZK;

    .line 2594796
    return-void
.end method

.method public static c(LX/Ibp;)V
    .locals 1

    .prologue
    .line 2594797
    iget-object v0, p0, LX/Ibp;->f:LX/4At;

    if-eqz v0, :cond_0

    .line 2594798
    iget-object v0, p0, LX/Ibp;->f:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2594799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Address;Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;)V
    .locals 12
    .param p2    # Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2594800
    if-nez p2, :cond_4

    const/4 v0, 0x0

    .line 2594801
    :goto_0
    if-nez v0, :cond_0

    .line 2594802
    iget-object v1, p0, LX/Ibp;->d:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    .line 2594803
    :cond_0
    const-string v1, "arg_message_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2594804
    const-string v1, "arg_ride_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2594805
    iget-object v0, p0, LX/Ibp;->c:LX/Ibu;

    invoke-virtual {v0, p1}, LX/Ibu;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v3

    .line 2594806
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Address;->hasLatitude()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Address;->hasLongitude()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2594807
    :cond_1
    iget-object v0, p0, LX/Ibp;->d:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2594808
    :cond_2
    new-instance v4, Landroid/location/Location;

    const-string v0, ""

    invoke-direct {v4, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2594809
    invoke-virtual {p1}, Landroid/location/Address;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 2594810
    invoke-virtual {p1}, Landroid/location/Address;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLongitude(D)V

    .line 2594811
    iget-object v0, p0, LX/Ibp;->f:LX/4At;

    if-nez v0, :cond_3

    .line 2594812
    new-instance v0, LX/4At;

    iget-object v5, p0, LX/Ibp;->a:Landroid/content/Context;

    const v6, 0x7f082d61

    invoke-direct {v0, v5, v6}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/Ibp;->f:LX/4At;

    .line 2594813
    :cond_3
    iget-object v0, p0, LX/Ibp;->f:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2594814
    iget-object v0, p0, LX/Ibp;->b:LX/Ic4;

    new-instance v5, LX/Ibo;

    invoke-direct {v5, p0}, LX/Ibo;-><init>(LX/Ibp;)V

    .line 2594815
    new-instance v8, LX/4JF;

    invoke-direct {v8}, LX/4JF;-><init>()V

    .line 2594816
    const-string v9, "message_id"

    invoke-virtual {v8, v9, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594817
    const-string v9, "ride_request_id"

    invoke-virtual {v8, v9, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594818
    const-string v9, "destination_address"

    invoke-virtual {v8, v9, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594819
    new-instance v9, LX/2vb;

    invoke-direct {v9}, LX/2vb;-><init>()V

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    move-result-object v9

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    move-result-object v9

    .line 2594820
    const-string v10, "destination"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2594821
    new-instance v9, LX/Ib9;

    invoke-direct {v9}, LX/Ib9;-><init>()V

    move-object v9, v9

    .line 2594822
    const-string v10, "input"

    invoke-virtual {v9, v10, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2594823
    iget-object v8, v0, LX/Ic4;->i:LX/1Ck;

    const-string v10, "update_destination"

    iget-object v11, v0, LX/Ic4;->d:LX/0tX;

    invoke-static {v9}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v9

    invoke-virtual {v11, v9}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    invoke-static {v9}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 2594824
    new-instance v11, LX/Ibw;

    invoke-direct {v11, v0, v5}, LX/Ibw;-><init>(LX/Ic4;LX/Ibo;)V

    move-object v11, v11

    .line 2594825
    invoke-virtual {v8, v10, v9, v11}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2594826
    return-void

    .line 2594827
    :cond_4
    iget-object v0, p2, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 2594828
    goto/16 :goto_0
.end method
