.class public final LX/HMs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 0

    .prologue
    .line 2457640
    iput-object p1, p0, LX/HMs;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 2457641
    iget-object v0, p0, LX/HMs;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457642
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    .line 2457643
    iget-boolean v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    if-eqz v1, :cond_0

    .line 2457644
    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->l:LX/HDT;

    new-instance v2, LX/HDg;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {v2, v3}, LX/HDg;-><init>(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2457645
    :goto_0
    return-void

    .line 2457646
    :cond_0
    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->M:LX/4At;

    if-nez v1, :cond_1

    .line 2457647
    new-instance v1, LX/4At;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0817cf

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->M:LX/4At;

    .line 2457648
    :cond_1
    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->M:LX/4At;

    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2457649
    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HQ9;

    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v3, v1, LX/HN5;->a:J

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v6, 0x0

    new-instance v7, LX/HMu;

    invoke-direct {v7, v0}, LX/HMu;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual/range {v2 .. v7}, LX/HQ9;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HMt;)V

    goto :goto_0
.end method
