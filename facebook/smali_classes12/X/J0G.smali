.class public LX/J0G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/J0G;


# instance fields
.field public b:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2636769
    const-class v0, LX/J0G;

    sput-object v0, LX/J0G;->c:Ljava/lang/Class;

    .line 2636770
    const-string v0, "id, mobile_csc_verified, zip_verified, web_csc_verified, method_category, commerce_payment_eligible, personal_transfer_eligible, is_default_receiving, %s"

    const-string v1, "credit_card { credential_id, number, first_name, last_name, expire_month, expire_year, association, address { postal_code }}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/J0G;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636767
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636768
    return-void
.end method

.method public static a(LX/0QB;)LX/J0G;
    .locals 3

    .prologue
    .line 2636724
    sget-object v0, LX/J0G;->d:LX/J0G;

    if-nez v0, :cond_1

    .line 2636725
    const-class v1, LX/J0G;

    monitor-enter v1

    .line 2636726
    :try_start_0
    sget-object v0, LX/J0G;->d:LX/J0G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636727
    if-eqz v2, :cond_0

    .line 2636728
    :try_start_1
    new-instance v0, LX/J0G;

    invoke-direct {v0}, LX/J0G;-><init>()V

    .line 2636729
    move-object v0, v0

    .line 2636730
    sput-object v0, LX/J0G;->d:LX/J0G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636731
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636732
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636733
    :cond_1
    sget-object v0, LX/J0G;->d:LX/J0G;

    return-object v0

    .line 2636734
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2636749
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636750
    const-string v1, "viewer() { peer_to_peer_payments { peer_to_peer_payment_methods { %s } } }"

    sget-object v2, LX/J0G;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/J0G;->b:Ljava/lang/String;

    .line 2636751
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    iget-object v3, p0, LX/J0G;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636752
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchPaymentCards"

    .line 2636753
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636754
    move-object v1, v1

    .line 2636755
    const-string v2, "GET"

    .line 2636756
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636757
    move-object v1, v1

    .line 2636758
    const-string v2, "graphql"

    .line 2636759
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636760
    move-object v1, v1

    .line 2636761
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636762
    move-object v0, v1

    .line 2636763
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2636764
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636765
    move-object v0, v0

    .line 2636766
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2636736
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2636737
    :goto_0
    invoke-virtual {v0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2636738
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 2636739
    :cond_0
    new-instance v1, LX/J0F;

    invoke-direct {v1, p0}, LX/J0F;-><init>(LX/J0G;)V

    invoke-virtual {v0, v1}, LX/15w;->a(LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2636740
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2636741
    const/4 v3, 0x0

    .line 2636742
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_1

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;

    .line 2636743
    new-instance v2, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-direct {v2, v1}, Lcom/facebook/payments/p2p/model/PaymentCard;-><init>(Lcom/facebook/payments/p2p/model/P2pCreditCardWrapper;)V

    .line 2636744
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2636745
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/PaymentCard;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 2636746
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    goto :goto_1

    .line 2636747
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2636748
    new-instance v1, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    invoke-direct {v1, v3, v0}, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;Ljava/util/List;)V

    return-object v1

    :cond_2
    move-object v1, v3

    goto :goto_2
.end method
