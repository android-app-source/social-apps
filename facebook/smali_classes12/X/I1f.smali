.class public LX/I1f;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/I6c;

.field public final c:LX/I1B;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1Db;

.field public final f:LX/1DS;

.field private final g:LX/I1H;

.field public final h:LX/I1Y;

.field public final i:LX/I1N;

.field public final j:LX/I1R;

.field private final k:Ljava/lang/Object;

.field public final l:LX/0jU;

.field public final m:LX/1My;

.field public n:LX/I6a;

.field public o:LX/1Qq;

.field private p:LX/I1e;

.field public q:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Z


# direct methods
.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/1My;LX/I6c;LX/1DS;LX/1Db;LX/0Ot;LX/I1I;LX/I1Z;LX/I1O;LX/I1S;LX/0jU;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Landroid/content/Context;",
            "LX/1My;",
            "LX/I6c;",
            "LX/1DS;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "LX/I1I;",
            "LX/I1Z;",
            "LX/I1O;",
            "LX/I1S;",
            "LX/0jU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529244
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2529245
    iput-object p2, p0, LX/I1f;->a:Landroid/content/Context;

    .line 2529246
    iput-object p3, p0, LX/I1f;->m:LX/1My;

    .line 2529247
    iput-object p4, p0, LX/I1f;->b:LX/I6c;

    .line 2529248
    new-instance v0, LX/I1B;

    invoke-direct {v0}, LX/I1B;-><init>()V

    iput-object v0, p0, LX/I1f;->c:LX/I1B;

    .line 2529249
    iput-object p5, p0, LX/I1f;->f:LX/1DS;

    .line 2529250
    iput-object p6, p0, LX/I1f;->e:LX/1Db;

    .line 2529251
    iput-object p7, p0, LX/I1f;->d:LX/0Ot;

    .line 2529252
    new-instance p3, LX/I1H;

    const-class v0, Landroid/content/Context;

    invoke-interface {p8, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class p2, LX/I1K;

    invoke-interface {p8, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/I1K;

    invoke-direct {p3, p1, v0, p2}, LX/I1H;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I1K;)V

    .line 2529253
    move-object v0, p3

    .line 2529254
    iput-object v0, p0, LX/I1f;->g:LX/I1H;

    .line 2529255
    new-instance p3, LX/I1Y;

    const-class v0, Landroid/content/Context;

    invoke-interface {p9, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class p2, LX/I1b;

    invoke-interface {p9, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/I1b;

    invoke-direct {p3, p1, v0, p2}, LX/I1Y;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I1b;)V

    .line 2529256
    move-object v0, p3

    .line 2529257
    iput-object v0, p0, LX/I1f;->h:LX/I1Y;

    .line 2529258
    new-instance p2, LX/I1N;

    const-class v0, Landroid/content/Context;

    invoke-interface {p10, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p2, p1, v0}, LX/I1N;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;)V

    .line 2529259
    move-object v0, p2

    .line 2529260
    iput-object v0, p0, LX/I1f;->i:LX/I1N;

    .line 2529261
    new-instance p2, LX/I1R;

    const-class v0, LX/I1V;

    invoke-interface {p11, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/I1V;

    invoke-direct {p2, p1, v0}, LX/I1R;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;LX/I1V;)V

    .line 2529262
    move-object v0, p2

    .line 2529263
    iput-object v0, p0, LX/I1f;->j:LX/I1R;

    .line 2529264
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/I1f;->k:Ljava/lang/Object;

    .line 2529265
    iput-object p12, p0, LX/I1f;->l:LX/0jU;

    .line 2529266
    new-instance v0, LX/I1e;

    invoke-direct {v0}, LX/I1e;-><init>()V

    iput-object v0, p0, LX/I1f;->p:LX/I1e;

    .line 2529267
    invoke-direct {p0}, LX/I1f;->f()V

    .line 2529268
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    if-nez v0, :cond_0

    .line 2529269
    iget-object v0, p0, LX/I1f;->f:LX/1DS;

    iget-object p1, p0, LX/I1f;->d:LX/0Ot;

    iget-object p2, p0, LX/I1f;->c:LX/I1B;

    invoke-virtual {v0, p1, p2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    iget-object p1, p0, LX/I1f;->n:LX/I6a;

    .line 2529270
    iput-object p1, v0, LX/1Ql;->f:LX/1PW;

    .line 2529271
    move-object v0, v0

    .line 2529272
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/I1f;->o:LX/1Qq;

    .line 2529273
    :cond_0
    new-instance v0, LX/I1d;

    invoke-direct {v0, p0}, LX/I1d;-><init>(LX/I1f;)V

    move-object v0, v0

    .line 2529274
    iput-object v0, p0, LX/I1f;->q:LX/0TF;

    .line 2529275
    return-void
.end method

.method private e(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2529225
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v2

    .line 2529226
    iget-object v0, p0, LX/I1f;->j:LX/I1R;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v3

    .line 2529227
    iget-object v0, p0, LX/I1f;->h:LX/I1Y;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    .line 2529228
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v5

    .line 2529229
    iget-boolean v0, p0, LX/I1f;->r:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2529230
    :goto_0
    if-ge p1, v2, :cond_2

    .line 2529231
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v2, p0, LX/I1f;->i:LX/I1N;

    invoke-virtual {v0, v2, v1}, LX/I1e;->a(Ljava/lang/Object;I)V

    .line 2529232
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 2529233
    goto :goto_0

    .line 2529234
    :cond_2
    add-int v1, v2, v3

    if-ge p1, v1, :cond_3

    .line 2529235
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v1, p0, LX/I1f;->j:LX/I1R;

    invoke-virtual {v0, v1, v2}, LX/I1e;->a(Ljava/lang/Object;I)V

    goto :goto_1

    .line 2529236
    :cond_3
    add-int v1, v2, v3

    add-int/2addr v1, v4

    if-ge p1, v1, :cond_4

    .line 2529237
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v1, p0, LX/I1f;->h:LX/I1Y;

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/I1e;->a(Ljava/lang/Object;I)V

    goto :goto_1

    .line 2529238
    :cond_4
    add-int v1, v2, v3

    add-int/2addr v1, v5

    add-int/2addr v1, v4

    if-ge p1, v1, :cond_5

    .line 2529239
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v1, p0, LX/I1f;->o:LX/1Qq;

    add-int/2addr v2, v3

    add-int/2addr v2, v4

    invoke-virtual {v0, v1, v2}, LX/I1e;->a(Ljava/lang/Object;I)V

    goto :goto_1

    .line 2529240
    :cond_5
    add-int v1, v2, v3

    add-int/2addr v1, v4

    add-int/2addr v1, v5

    add-int/2addr v1, v0

    if-ge p1, v1, :cond_6

    .line 2529241
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v1, p0, LX/I1f;->k:Ljava/lang/Object;

    add-int/2addr v2, v3

    add-int/2addr v2, v4

    add-int/2addr v2, v5

    invoke-virtual {v0, v1, v2}, LX/I1e;->a(Ljava/lang/Object;I)V

    goto :goto_1

    .line 2529242
    :cond_6
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2529243
    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget-object v6, p0, LX/I1f;->g:LX/I1H;

    add-int/2addr v2, v3

    add-int/2addr v2, v5

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    invoke-virtual {v1, v6, v0}, LX/I1e;->a(Ljava/lang/Object;I)V

    goto :goto_1
.end method

.method private f()V
    .locals 7

    .prologue
    .line 2529219
    iget-object v0, p0, LX/I1f;->b:LX/I6c;

    const/4 v1, 0x0

    const-string v2, "event_feed"

    iget-object v3, p0, LX/I1f;->a:Landroid/content/Context;

    .line 2529220
    sget-object v4, LX/I6Y;->a:LX/I6Y;

    move-object v4, v4

    .line 2529221
    new-instance v5, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardViewAdapter$1;

    invoke-direct {v5, p0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardViewAdapter$1;-><init>(LX/I1f;)V

    .line 2529222
    new-instance v6, LX/I1c;

    invoke-direct {v6, p0}, LX/I1c;-><init>(LX/I1f;)V

    move-object v6, v6

    .line 2529223
    invoke-virtual/range {v0 .. v6}, LX/I6c;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/I6b;

    move-result-object v0

    iput-object v0, p0, LX/I1f;->n:LX/I6a;

    .line 2529224
    return-void
.end method

.method public static i(LX/I1f;)V
    .locals 1

    .prologue
    .line 2529216
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2529217
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2529218
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2529202
    const v0, 0x7f0d0f00

    if-ne p2, v0, :cond_0

    .line 2529203
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03055c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2529204
    new-instance v0, LX/I14;

    invoke-direct {v0, v1}, LX/I14;-><init>(Landroid/view/View;)V

    .line 2529205
    :goto_0
    return-object v0

    .line 2529206
    :cond_0
    sget-object v0, LX/I1Y;->a:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2529207
    iget-object v0, p0, LX/I1f;->h:LX/I1Y;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2529208
    :cond_1
    sget-object v0, LX/I1H;->a:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2529209
    iget-object v0, p0, LX/I1f;->g:LX/I1H;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2529210
    :cond_2
    sget-object v0, LX/I1N;->a:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2529211
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2529212
    :cond_3
    sget-object v0, LX/I1R;->a:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2529213
    iget-object v0, p0, LX/I1f;->j:LX/I1R;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2529214
    :cond_4
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    invoke-interface {v0, p2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2529215
    new-instance v0, LX/Hyz;

    invoke-direct {v0, v1, p1, p2}, LX/Hyz;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    goto :goto_0
.end method

.method public final a(LX/0m9;)V
    .locals 1
    .param p1    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529197
    iget-object v0, p0, LX/I1f;->g:LX/I1H;

    .line 2529198
    iput-object p1, v0, LX/I1H;->f:LX/0m9;

    .line 2529199
    iget-object v0, p0, LX/I1f;->h:LX/I1Y;

    .line 2529200
    iput-object p1, v0, LX/I1Y;->f:LX/0m9;

    .line 2529201
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2529276
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2529277
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 2529278
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2529178
    invoke-direct {p0, p2}, LX/I1f;->e(I)V

    .line 2529179
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->o:LX/1Qq;

    if-ne v0, v1, :cond_1

    .line 2529180
    check-cast p1, LX/Hyz;

    .line 2529181
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p2, v1

    iget-object v2, p0, LX/I1f;->o:LX/1Qq;

    iget-object v3, p0, LX/I1f;->p:LX/I1e;

    iget v3, v3, LX/I1e;->b:I

    sub-int v3, p2, v3

    invoke-interface {v2, v3}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    .line 2529182
    iget v4, p1, LX/Hyz;->m:I

    move v4, v4

    .line 2529183
    iget-object v5, p1, LX/Hyz;->l:Landroid/view/ViewGroup;

    move-object v5, v5

    .line 2529184
    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 2529185
    :cond_0
    :goto_0
    return-void

    .line 2529186
    :cond_1
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->k:Ljava/lang/Object;

    if-ne v0, v1, :cond_2

    .line 2529187
    check-cast p1, LX/I14;

    .line 2529188
    iget-boolean v0, p0, LX/I1f;->r:Z

    invoke-virtual {p1, v0}, LX/I14;->b(Z)V

    goto :goto_0

    .line 2529189
    :cond_2
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->g:LX/I1H;

    if-ne v0, v1, :cond_3

    .line 2529190
    iget-object v0, p0, LX/I1f;->g:LX/I1H;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0

    .line 2529191
    :cond_3
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->h:LX/I1Y;

    if-ne v0, v1, :cond_4

    .line 2529192
    iget-object v0, p0, LX/I1f;->h:LX/I1Y;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0

    .line 2529193
    :cond_4
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->i:LX/I1N;

    if-ne v0, v1, :cond_5

    .line 2529194
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0

    .line 2529195
    :cond_5
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->j:LX/I1R;

    if-ne v0, v1, :cond_0

    .line 2529196
    iget-object v0, p0, LX/I1f;->j:LX/I1R;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V
    .locals 1
    .param p1    # Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529175
    iget-object v0, p0, LX/I1f;->j:LX/I1R;

    invoke-virtual {v0, p1}, LX/I1R;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)V

    .line 2529176
    invoke-static {p0}, LX/I1f;->i(LX/I1f;)V

    .line 2529177
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2529166
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    .line 2529167
    iget-object v1, v0, LX/I1N;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2529168
    iget-object v1, v0, LX/I1N;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2529169
    iget-object v2, v0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2529170
    invoke-static {v0}, LX/I1N;->f(LX/I1N;)V

    .line 2529171
    invoke-static {v0}, LX/I1N;->e(LX/I1N;)V

    .line 2529172
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2529173
    :cond_0
    invoke-static {p0}, LX/I1f;->i(LX/I1f;)V

    .line 2529174
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel$EventCategoryListModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529160
    iget-object v0, p0, LX/I1f;->g:LX/I1H;

    .line 2529161
    iget-object v1, v0, LX/I1H;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2529162
    iget-object v1, v0, LX/I1H;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2529163
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2529164
    invoke-static {p0}, LX/I1f;->i(LX/I1f;)V

    .line 2529165
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2529156
    iget-boolean v0, p0, LX/I1f;->r:Z

    if-eq v0, p1, :cond_0

    .line 2529157
    iput-boolean p1, p0, LX/I1f;->r:Z

    .line 2529158
    invoke-static {p0}, LX/I1f;->i(LX/I1f;)V

    .line 2529159
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2529152
    iget-object v0, p0, LX/I1f;->c:LX/I1B;

    .line 2529153
    iget-object p0, v0, LX/I1B;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2529154
    iget-object p0, v0, LX/I1B;->b:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->clear()V

    .line 2529155
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2529137
    invoke-direct {p0, p1}, LX/I1f;->e(I)V

    .line 2529138
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->o:LX/1Qq;

    if-ne v0, v1, :cond_0

    .line 2529139
    iget-object v0, p0, LX/I1f;->o:LX/1Qq;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    .line 2529140
    :goto_0
    return v0

    .line 2529141
    :cond_0
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->k:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 2529142
    const v0, 0x7f0d0f00

    goto :goto_0

    .line 2529143
    :cond_1
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->g:LX/I1H;

    if-ne v0, v1, :cond_2

    .line 2529144
    iget-object v0, p0, LX/I1f;->g:LX/I1H;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2529145
    :cond_2
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->h:LX/I1Y;

    if-ne v0, v1, :cond_3

    .line 2529146
    iget-object v0, p0, LX/I1f;->h:LX/I1Y;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2529147
    :cond_3
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->i:LX/I1N;

    if-ne v0, v1, :cond_4

    .line 2529148
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2529149
    :cond_4
    iget-object v0, p0, LX/I1f;->p:LX/I1e;

    iget-object v0, v0, LX/I1e;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I1f;->j:LX/I1R;

    if-ne v0, v1, :cond_5

    .line 2529150
    iget-object v0, p0, LX/I1f;->j:LX/I1R;

    iget-object v1, p0, LX/I1f;->p:LX/I1e;

    iget v1, v1, LX/I1e;->b:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2529151
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalidate position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2529136
    iget-object v0, p0, LX/I1f;->i:LX/I1N;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    iget-object v1, p0, LX/I1f;->j:LX/I1R;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/I1f;->h:LX/I1Y;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/I1f;->o:LX/1Qq;

    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/I1f;->g:LX/I1H;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/2addr v1, v0

    iget-boolean v0, p0, LX/I1f;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
