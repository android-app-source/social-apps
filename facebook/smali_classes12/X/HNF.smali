.class public LX/HNF;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HNE;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458326
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2458327
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/ParcelUuid;)LX/HNE;
    .locals 9

    .prologue
    .line 2458328
    new-instance v0, LX/HNE;

    const-class v1, LX/HNI;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HNI;

    const-class v2, LX/HNN;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/HNN;

    const-class v3, LX/HNQ;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HNQ;

    .line 2458329
    new-instance v6, LX/HNO;

    const/16 v4, 0xc49

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v4, 0x455

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v6, v7, v8, v4, v5}, LX/HNO;-><init>(LX/0Ot;LX/0Ot;LX/0Uh;Landroid/content/Context;)V

    .line 2458330
    move-object v4, v6

    .line 2458331
    check-cast v4, LX/HNO;

    .line 2458332
    new-instance v6, LX/HNR;

    const/16 v5, 0xc49

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v5, 0x455

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {v6, v7, v8, v5}, LX/HNR;-><init>(LX/0Ot;LX/0Ot;Landroid/content/Context;)V

    .line 2458333
    move-object v5, v6

    .line 2458334
    check-cast v5, LX/HNR;

    invoke-static {p0}, LX/HN9;->a(LX/0QB;)LX/HN9;

    move-result-object v6

    check-cast v6, LX/HN9;

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/HNE;-><init>(LX/HNI;LX/HNN;LX/HNQ;LX/HNO;LX/HNR;LX/HN9;Landroid/os/ParcelUuid;)V

    .line 2458335
    return-object v0
.end method
