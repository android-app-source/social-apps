.class public LX/HnN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2501746
    const-class v0, LX/HnN;

    sput-object v0, LX/HnN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2501747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/wifi/WifiManager;Landroid/net/wifi/WifiConfiguration;)I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 2501748
    new-instance v1, LX/HnM;

    invoke-direct {v1, p0, p1, p2}, LX/HnM;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;Landroid/net/wifi/WifiConfiguration;)V

    .line 2501749
    :try_start_0
    iget-object v0, p2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {p1, v0}, LX/HnN;->a(Landroid/net/wifi/WifiManager;Ljava/lang/String;)I

    move-result v0

    .line 2501750
    if-ne v0, v2, :cond_0

    .line 2501751
    invoke-virtual {p1, p2}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    .line 2501752
    :goto_0
    if-ne v0, v2, :cond_1

    .line 2501753
    sget-object v0, LX/HnN;->a:Ljava/lang/Class;

    const-string v2, "Unable to add/update network"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2501754
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Failed to add network"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2501755
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/HnM;->a()V

    throw v0

    .line 2501756
    :cond_0
    :try_start_1
    iput v0, p2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 2501757
    invoke-virtual {p1, p2}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    goto :goto_0

    .line 2501758
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    move-result v2

    .line 2501759
    if-nez v2, :cond_2

    .line 2501760
    sget-object v0, LX/HnN;->a:Ljava/lang/Class;

    const-string v2, "Unable to enable network"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2501761
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Couldn\'t enable network"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2501762
    :cond_2
    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v2, v3}, LX/HnM;->a(J)Z

    move-result v2

    .line 2501763
    if-nez v2, :cond_3

    .line 2501764
    sget-object v0, LX/HnN;->a:Ljava/lang/Class;

    const-string v2, "Unable to connect to network"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2501765
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Didn\'t connect in time"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2501766
    :cond_3
    invoke-virtual {v1}, LX/HnM;->a()V

    return v0
.end method

.method private static a(Landroid/net/wifi/WifiManager;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2501767
    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    .line 2501768
    if-nez v0, :cond_0

    move v0, v1

    .line 2501769
    :goto_0
    return v0

    .line 2501770
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    .line 2501771
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2501772
    iget v0, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2501773
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2501774
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2501775
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 2501776
    invoke-static {p1}, LX/HnN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/HnN;->a(Landroid/net/wifi/WifiManager;Ljava/lang/String;)I

    move-result v1

    .line 2501777
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 2501778
    const/4 v0, 0x0

    .line 2501779
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    move-result v0

    goto :goto_0
.end method
