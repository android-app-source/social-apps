.class public final LX/I7o;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/model/Event;

.field public final synthetic b:Z

.field public final synthetic c:LX/0TF;

.field public final synthetic d:Lcom/facebook/events/model/Event;

.field public final synthetic e:LX/I7w;


# direct methods
.method public constructor <init>(LX/I7w;Lcom/facebook/events/model/Event;ZLX/0TF;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2540171
    iput-object p1, p0, LX/I7o;->e:LX/I7w;

    iput-object p2, p0, LX/I7o;->a:Lcom/facebook/events/model/Event;

    iput-boolean p3, p0, LX/I7o;->b:Z

    iput-object p4, p0, LX/I7o;->c:LX/0TF;

    iput-object p5, p0, LX/I7o;->d:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2540172
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v0, v0, LX/I7w;->g:LX/Bl6;

    new-instance v1, LX/BlT;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    iget-object v3, p0, LX/I7o;->d:Lcom/facebook/events/model/Event;

    iget-object v4, p0, LX/I7o;->a:Lcom/facebook/events/model/Event;

    invoke-direct {v1, v2, v3, v4}, LX/BlT;-><init>(LX/BlI;Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2540173
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v1, p0, LX/I7o;->d:Lcom/facebook/events/model/Event;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    invoke-static {v0, v1, v2}, LX/I7w;->a$redex0(LX/I7w;Lcom/facebook/events/model/Event;LX/BlI;)V

    .line 2540174
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v0, v0, LX/I7w;->r:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081eea

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2540175
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v0, v0, LX/I7w;->h:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/I7o;->e:LX/I7w;

    iget-object v1, v1, LX/I7w;->i:LX/Bky;

    iget-object v2, p0, LX/I7o;->d:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/I7o;->e:LX/I7w;

    iget-object v3, v3, LX/I7w;->j:LX/0TD;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 2540176
    iget-object v0, p0, LX/I7o;->c:LX/0TF;

    if-eqz v0, :cond_0

    .line 2540177
    iget-object v0, p0, LX/I7o;->c:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2540178
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2540179
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v0, v0, LX/I7w;->g:LX/Bl6;

    new-instance v1, LX/BlL;

    iget-object v2, p0, LX/I7o;->a:Lcom/facebook/events/model/Event;

    .line 2540180
    iget-object p1, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2540181
    invoke-direct {v1, v2}, LX/BlL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2540182
    iget-boolean v0, p0, LX/I7o;->b:Z

    if-eqz v0, :cond_0

    .line 2540183
    iget-object v0, p0, LX/I7o;->e:LX/I7w;

    iget-object v1, p0, LX/I7o;->a:Lcom/facebook/events/model/Event;

    .line 2540184
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2540185
    iget-object v2, v0, LX/I7w;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0b008e

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2540186
    iget-object p1, v0, LX/I7w;->k:LX/Bm7;

    invoke-virtual {p1, v2, v1}, LX/Bm7;->a(ILjava/lang/String;)V

    .line 2540187
    :cond_0
    iget-object v0, p0, LX/I7o;->c:LX/0TF;

    if-eqz v0, :cond_1

    .line 2540188
    iget-object v0, p0, LX/I7o;->c:LX/0TF;

    sget-object v1, LX/BlI;->SUCCESS:LX/BlI;

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2540189
    :cond_1
    return-void
.end method
