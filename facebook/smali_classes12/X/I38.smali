.class public final LX/I38;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;)V
    .locals 0

    .prologue
    .line 2531063
    iput-object p1, p0, LX/I38;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2531064
    iget-object v0, p0, LX/I38;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->i:LX/I3J;

    invoke-virtual {v0, p3}, LX/I3J;->a(I)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2531065
    if-eqz v0, :cond_0

    .line 2531066
    iget-object v1, p0, LX/I38;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->b:LX/1nQ;

    .line 2531067
    iget-object v2, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2531068
    iget-object v2, p0, LX/I38;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    invoke-virtual {v2}, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/I38;->a:Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/subscriptions/EventsSubscriptionsFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2531069
    iget-object p0, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, p0

    .line 2531070
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    invoke-virtual {v1, v0, v2, p3, v3}, LX/1nQ;->b(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2531071
    :cond_0
    return-void
.end method
