.class public LX/HkV;
.super Landroid/os/AsyncTask;
.source ""

# interfaces
.implements LX/HkS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "LX/HkW;",
        "Ljava/lang/Void;",
        "LX/Hkb;",
        ">;",
        "LX/HkS;"
    }
.end annotation


# instance fields
.field private a:LX/HkR;

.field private b:LX/HkG;

.field private c:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(LX/HkR;LX/HkG;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, LX/HkV;->a:LX/HkR;

    iput-object p2, p0, LX/HkV;->b:LX/HkG;

    return-void
.end method


# virtual methods
.method public final a(LX/HkW;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [LX/HkW;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, [LX/HkW;

    if-eqz p1, :cond_0

    :try_start_0
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-object v1, p0, LX/HkV;->a:LX/HkR;

    invoke-virtual {v1, v0}, LX/HkR;->a(LX/HkW;)LX/Hkb;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DoHttpRequestTask takes exactly one argument of type HttpRequest"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iput-object v0, p0, LX/HkV;->c:Ljava/lang/Exception;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/HkV;->cancel(Z)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCancelled()V
    .locals 2

    iget-object v0, p0, LX/HkV;->b:LX/HkG;

    iget-object v1, p0, LX/HkV;->c:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, LX/HkG;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, LX/Hkb;

    iget-object v0, p0, LX/HkV;->b:LX/HkG;

    invoke-virtual {v0, p1}, LX/HkG;->a(LX/Hkb;)V

    return-void
.end method
