.class public final LX/HEw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "LX/HEu;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HEz;


# direct methods
.method public constructor <init>(LX/HEz;)V
    .locals 0

    .prologue
    .line 2442899
    iput-object p1, p0, LX/HEw;->a:LX/HEz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2442900
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2442901
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    invoke-virtual {v0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2442902
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2442903
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->l:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    const-string v2, "category fetching failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2442904
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2442905
    check-cast p1, LX/0Px;

    .line 2442906
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2442907
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2442908
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2442909
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->n:LX/HEt;

    .line 2442910
    if-eqz p1, :cond_1

    :goto_0
    iput-object p1, v0, LX/HEt;->a:LX/0Px;

    .line 2442911
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2442912
    iget-object v0, p0, LX/HEw;->a:LX/HEz;

    iget-object v0, v0, LX/HEz;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/HEw;->a:LX/HEz;

    iget-object v1, v1, LX/HEz;->n:LX/HEt;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2442913
    :cond_0
    return-void

    .line 2442914
    :cond_1
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 2442915
    goto :goto_0
.end method
