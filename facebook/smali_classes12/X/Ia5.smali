.class public LX/Ia5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2590592
    const-class v0, LX/Ia5;

    sput-object v0, LX/Ia5;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2590593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590594
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2590595
    check-cast p1, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;

    .line 2590596
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "proxied_app_id"

    iget-object v2, p1, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "permissions"

    iget-object v3, p1, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2590597
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "proxyLogin"

    .line 2590598
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2590599
    move-object v1, v1

    .line 2590600
    const-string v2, "POST"

    .line 2590601
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2590602
    move-object v1, v1

    .line 2590603
    const-string v2, "/v2.5/auth/proxy_login"

    .line 2590604
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2590605
    move-object v1, v1

    .line 2590606
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2590607
    move-object v0, v1

    .line 2590608
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2590609
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2590610
    move-object v0, v0

    .line 2590611
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2590612
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    .line 2590613
    const/4 v0, 0x0

    return-object v0
.end method
