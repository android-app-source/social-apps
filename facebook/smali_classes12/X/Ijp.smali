.class public final LX/Ijp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field public final synthetic a:LX/Ijy;


# direct methods
.method public constructor <init>(LX/Ijy;)V
    .locals 0

    .prologue
    .line 2606089
    iput-object p1, p0, LX/Ijp;->a:LX/Ijy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 2606090
    iget-object v0, p0, LX/Ijp;->a:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->g:LX/0Zb;

    iget-object v1, p0, LX/Ijp;->a:LX/Ijy;

    iget-object v1, v1, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v2, "p2p_cancel_select_card"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606091
    iget-object v0, p0, LX/Ijp;->a:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->p:LX/Ijx;

    invoke-interface {v0}, LX/Ijx;->c()V

    .line 2606092
    return-void
.end method
