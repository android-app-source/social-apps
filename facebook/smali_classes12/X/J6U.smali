.class public final LX/J6U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649686
    iput-object p1, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x1

    const v1, -0x3c9c7cf5

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v13

    .line 2649687
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->b()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2649688
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-static {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    .line 2649689
    const v0, -0x696c2321

    invoke-static {v2, v2, v0, v13}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2649690
    :goto_0
    return-void

    .line 2649691
    :cond_0
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_1

    .line 2649692
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649693
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649694
    const-string v2, "checkup_cancel"

    iget-object v3, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649695
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->finish()V

    .line 2649696
    const v0, -0x480cbba7    # -2.8999684E-5f

    invoke-static {v0, v13}, LX/02F;->a(II)V

    goto :goto_0

    .line 2649697
    :cond_1
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v5, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649698
    iget-object v1, v0, LX/J3t;->i:Ljava/lang/String;

    move-object v6, v1

    .line 2649699
    const-string v7, "step_navigation"

    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object v10, v4

    move-object v11, v4

    move-object v12, v4

    invoke-virtual/range {v5 .. v12}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649700
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2649701
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v1, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2649702
    iget-object v0, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, p0, LX/J6U;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    .line 2649703
    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V

    .line 2649704
    const v0, -0x26372c51

    invoke-static {v0, v13}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
