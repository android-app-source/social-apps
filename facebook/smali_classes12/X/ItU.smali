.class public final enum LX/ItU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ItU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ItU;

.field public static final enum ABORTED:LX/ItU;

.field public static final enum FAILED:LX/ItU;

.field public static final enum IN_PROGRESS:LX/ItU;

.field public static final enum NOT_STARTED:LX/ItU;

.field public static final enum SUCCEEDED:LX/ItU;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2623383
    new-instance v0, LX/ItU;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LX/ItU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ItU;->NOT_STARTED:LX/ItU;

    .line 2623384
    new-instance v0, LX/ItU;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/ItU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ItU;->IN_PROGRESS:LX/ItU;

    .line 2623385
    new-instance v0, LX/ItU;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/ItU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ItU;->SUCCEEDED:LX/ItU;

    .line 2623386
    new-instance v0, LX/ItU;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/ItU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ItU;->FAILED:LX/ItU;

    .line 2623387
    new-instance v0, LX/ItU;

    const-string v1, "ABORTED"

    invoke-direct {v0, v1, v6}, LX/ItU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ItU;->ABORTED:LX/ItU;

    .line 2623388
    const/4 v0, 0x5

    new-array v0, v0, [LX/ItU;

    sget-object v1, LX/ItU;->NOT_STARTED:LX/ItU;

    aput-object v1, v0, v2

    sget-object v1, LX/ItU;->IN_PROGRESS:LX/ItU;

    aput-object v1, v0, v3

    sget-object v1, LX/ItU;->SUCCEEDED:LX/ItU;

    aput-object v1, v0, v4

    sget-object v1, LX/ItU;->FAILED:LX/ItU;

    aput-object v1, v0, v5

    sget-object v1, LX/ItU;->ABORTED:LX/ItU;

    aput-object v1, v0, v6

    sput-object v0, LX/ItU;->$VALUES:[LX/ItU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2623389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isCompleted(LX/ItU;)Z
    .locals 1

    .prologue
    .line 2623390
    sget-object v0, LX/ItU;->SUCCEEDED:LX/ItU;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/ItU;->FAILED:LX/ItU;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/ItU;->ABORTED:LX/ItU;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/ItU;
    .locals 1

    .prologue
    .line 2623391
    const-class v0, LX/ItU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ItU;

    return-object v0
.end method

.method public static values()[LX/ItU;
    .locals 1

    .prologue
    .line 2623392
    sget-object v0, LX/ItU;->$VALUES:[LX/ItU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ItU;

    return-object v0
.end method
