.class public LX/HKQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKQ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454398
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2454399
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HKQ;->b:LX/0Zi;

    .line 2454400
    iput-object p1, p0, LX/HKQ;->a:LX/0Ot;

    .line 2454401
    return-void
.end method

.method public static a(LX/0QB;)LX/HKQ;
    .locals 4

    .prologue
    .line 2454402
    const-class v1, LX/HKQ;

    monitor-enter v1

    .line 2454403
    :try_start_0
    sget-object v0, LX/HKQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454404
    sput-object v2, LX/HKQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454405
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454406
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454407
    new-instance v3, LX/HKQ;

    const/16 p0, 0x2bca

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKQ;-><init>(LX/0Ot;)V

    .line 2454408
    move-object v0, v3

    .line 2454409
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454410
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454411
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2454413
    check-cast p1, LX/HKP;

    .line 2454414
    iget-object v0, p0, LX/HKQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;

    iget-object v1, p1, LX/HKP;->a:LX/HKU;

    iget-object v2, p1, LX/HKP;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HKP;->d:LX/2km;

    .line 2454415
    move-object v5, v3

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v1, LX/HKU;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iget-object v8, v1, LX/HKU;->d:[J

    iget-object v9, v1, LX/HKU;->c:Ljava/lang/String;

    invoke-static/range {v5 .. v9}, LX/E1i;->a(Landroid/content/Context;J[JLjava/lang/String;)LX/Cfl;

    move-result-object v4

    .line 2454416
    iget-object v5, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2454417
    iget-object v6, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2454418
    invoke-interface {v3, v5, v6, v4}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2454419
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2454420
    const v0, -0x657f4066

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2454421
    check-cast p2, LX/HKP;

    .line 2454422
    iget-object v0, p0, LX/HKQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;

    iget-object v1, p2, LX/HKP;->a:LX/HKU;

    iget-boolean v2, p2, LX/HKP;->b:Z

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 2454423
    if-eqz v2, :cond_0

    iget-object v3, v1, LX/HKU;->b:Ljava/lang/String;

    .line 2454424
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    .line 2454425
    const v5, -0x657f4066

    const/4 v1, 0x0

    invoke-static {p1, v5, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2454426
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    .line 2454427
    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;->b:LX/1nu;

    invoke-virtual {p0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    const/high16 p2, 0x3fa00000    # 1.25f

    invoke-virtual {p0, p2}, LX/1nw;->c(F)LX/1nw;

    move-result-object p0

    sget-object p2, Lcom/facebook/pages/common/reaction/components/PagePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/4 p2, 0x4

    invoke-interface {p0, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    move-object v3, p0

    .line 2454428
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2454429
    return-object v0

    .line 2454430
    :cond_0
    iget-object v3, v1, LX/HKU;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2454431
    invoke-static {}, LX/1dS;->b()V

    .line 2454432
    iget v0, p1, LX/1dQ;->b:I

    .line 2454433
    packed-switch v0, :pswitch_data_0

    .line 2454434
    :goto_0
    return-object v1

    .line 2454435
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HKQ;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x657f4066
        :pswitch_0
    .end packed-switch
.end method
