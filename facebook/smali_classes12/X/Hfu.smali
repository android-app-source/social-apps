.class public final LX/Hfu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

.field public final synthetic b:I

.field public final synthetic c:LX/Hfh;

.field public final synthetic d:LX/Hfv;


# direct methods
.method public constructor <init>(LX/Hfv;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;ILX/Hfh;)V
    .locals 0

    .prologue
    .line 2492538
    iput-object p1, p0, LX/Hfu;->d:LX/Hfv;

    iput-object p2, p0, LX/Hfu;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    iput p3, p0, LX/Hfu;->b:I

    iput-object p4, p0, LX/Hfu;->c:LX/Hfh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2492539
    iget-object v0, p0, LX/Hfu;->d:LX/Hfv;

    iget-object v0, v0, LX/Hfv;->f:LX/3mF;

    iget-object v1, p0, LX/Hfu;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "work_mobile_groups_tab"

    const-string v3, "ALLOW_READD"

    .line 2492540
    invoke-static {v1, v2, v3}, LX/3mF;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/399;

    move-result-object v4

    .line 2492541
    iget-object p1, v0, LX/3mF;->a:LX/0tX;

    invoke-virtual {p1, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2492542
    iget-object v1, p0, LX/Hfu;->d:LX/Hfv;

    iget-object v1, v1, LX/Hfv;->i:LX/HgN;

    iget v2, p0, LX/Hfu;->b:I

    iget-object v3, p0, LX/Hfu;->a:Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;

    const-string v4, "leave_group"

    invoke-virtual {v1, v2, v3, v4}, LX/HgN;->a(ILcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/String;)V

    .line 2492543
    new-instance v1, LX/Hft;

    invoke-direct {v1, p0}, LX/Hft;-><init>(LX/Hfu;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2492544
    const/4 v0, 0x1

    return v0
.end method
