.class public final LX/HoA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;)V
    .locals 0

    .prologue
    .line 2505360
    iput-object p1, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const v0, -0x576f3497

    invoke-static {v6, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2505361
    sget-object v1, LX/21D;->ELECTION_HUB:LX/21D;

    .line 2505362
    const-string v2, "electionHubInlineComposer"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v2, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->h:LX/1Nq;

    iget-object v3, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0809ed

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v2, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v2, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->s:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2505363
    iget-object v2, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v2, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->g:LX/1Kf;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, v3, v1, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2505364
    iget-object v1, p0, LX/HoA;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    .line 2505365
    iget-object v2, v1, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->n:LX/Hnj;

    move-object v1, v2

    .line 2505366
    const-string v2, "election_hub_composer_opened"

    invoke-static {v1, v2}, LX/Hnj;->b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2505367
    iget-object v3, v1, LX/Hnj;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2505368
    const v1, 0x6e9d1e60

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
