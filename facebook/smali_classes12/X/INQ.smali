.class public abstract LX/INQ;
.super LX/INP;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2570798
    invoke-direct {p0, p1}, LX/INP;-><init>(Landroid/content/Context;)V

    .line 2570799
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2570800
    invoke-super {p0, p3}, LX/INP;->a(Landroid/view/View$OnClickListener;)V

    .line 2570801
    iget-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 2570802
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2570803
    new-instance v4, LX/IO3;

    invoke-virtual {p0}, LX/INQ;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/IO3;-><init>(Landroid/content/Context;)V

    .line 2570804
    const/4 v5, 0x0

    invoke-virtual {v4, v0, p2, v2, v5}, LX/IO3;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Ljava/lang/String;ZLX/IN0;)V

    .line 2570805
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v0, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2570806
    invoke-virtual {p0}, LX/INQ;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0069

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-virtual {p0}, LX/INQ;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b006c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-virtual {v0, v2, v5, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2570807
    invoke-virtual {v4, v0}, LX/IO3;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2570808
    iget-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2570809
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2570810
    :cond_0
    return-void
.end method
