.class public LX/Iea;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/3NP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598834
    new-instance v0, LX/IeZ;

    invoke-direct {v0}, LX/IeZ;-><init>()V

    sput-object v0, LX/Iea;->a:LX/3NP;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3N9;LX/3NA;)LX/3Mi;
    .locals 6

    .prologue
    .line 2598836
    new-instance v0, LX/3NS;

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2598837
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2598838
    iput-boolean v3, p3, LX/3N9;->e:Z

    .line 2598839
    new-instance v2, LX/3NU;

    invoke-direct {v2, p3, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598840
    const/16 v2, 0xa

    .line 2598841
    iput v2, p4, LX/3NA;->k:I

    .line 2598842
    const/16 v2, 0x14

    .line 2598843
    iput v2, p4, LX/3NA;->j:I

    .line 2598844
    iput-boolean v3, p4, LX/3NA;->l:Z

    .line 2598845
    new-instance v2, LX/3NU;

    .line 2598846
    new-instance v3, LX/3NV;

    sget-object p3, LX/Iea;->a:LX/3NP;

    invoke-direct {v3, p4, p1, p3}, LX/3NV;-><init>(LX/3Mi;Ljava/util/concurrent/ScheduledExecutorService;LX/3NP;)V

    move-object v3, v3

    .line 2598847
    invoke-direct {v2, v3, v5, v4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598848
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2598849
    invoke-direct {v0, v1, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    return-object v0
.end method
