.class public final enum LX/Ipg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ipg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ipg;

.field public static final enum BOTH:LX/Ipg;

.field public static final enum PAY_ONLY:LX/Ipg;

.field public static final enum REQUEST_ONLY:LX/Ipg;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2613083
    new-instance v0, LX/Ipg;

    const-string v1, "REQUEST_ONLY"

    invoke-direct {v0, v1, v2}, LX/Ipg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ipg;->REQUEST_ONLY:LX/Ipg;

    .line 2613084
    new-instance v0, LX/Ipg;

    const-string v1, "PAY_ONLY"

    invoke-direct {v0, v1, v3}, LX/Ipg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ipg;->PAY_ONLY:LX/Ipg;

    .line 2613085
    new-instance v0, LX/Ipg;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, LX/Ipg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ipg;->BOTH:LX/Ipg;

    .line 2613086
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ipg;

    sget-object v1, LX/Ipg;->REQUEST_ONLY:LX/Ipg;

    aput-object v1, v0, v2

    sget-object v1, LX/Ipg;->PAY_ONLY:LX/Ipg;

    aput-object v1, v0, v3

    sget-object v1, LX/Ipg;->BOTH:LX/Ipg;

    aput-object v1, v0, v4

    sput-object v0, LX/Ipg;->$VALUES:[LX/Ipg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2613087
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ipg;
    .locals 1

    .prologue
    .line 2613088
    const-class v0, LX/Ipg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ipg;

    return-object v0
.end method

.method public static values()[LX/Ipg;
    .locals 1

    .prologue
    .line 2613089
    sget-object v0, LX/Ipg;->$VALUES:[LX/Ipg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ipg;

    return-object v0
.end method
