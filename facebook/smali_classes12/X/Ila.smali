.class public LX/Ila;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/graphics/Typeface;

.field public final b:LX/Ilc;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Ilb;)V
    .locals 1

    .prologue
    .line 2607888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607889
    iget-object v0, p1, LX/Ilb;->a:Landroid/graphics/Typeface;

    move-object v0, v0

    .line 2607890
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607891
    iget-object v0, p1, LX/Ilb;->b:LX/Ilc;

    move-object v0, v0

    .line 2607892
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607893
    iget-object v0, p1, LX/Ilb;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2607894
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607895
    iget-object v0, p1, LX/Ilb;->a:Landroid/graphics/Typeface;

    move-object v0, v0

    .line 2607896
    iput-object v0, p0, LX/Ila;->a:Landroid/graphics/Typeface;

    .line 2607897
    iget-object v0, p1, LX/Ilb;->b:LX/Ilc;

    move-object v0, v0

    .line 2607898
    iput-object v0, p0, LX/Ila;->b:LX/Ilc;

    .line 2607899
    iget-object v0, p1, LX/Ilb;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2607900
    iput-object v0, p0, LX/Ila;->c:Ljava/lang/String;

    .line 2607901
    return-void
.end method

.method public static newBuilder()LX/Ilb;
    .locals 1

    .prologue
    .line 2607902
    new-instance v0, LX/Ilb;

    invoke-direct {v0}, LX/Ilb;-><init>()V

    return-object v0
.end method
