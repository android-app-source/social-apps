.class public final LX/HUZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2473234
    iput-object p1, p0, LX/HUZ;->d:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

    iput-object p2, p0, LX/HUZ;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iput-object p3, p0, LX/HUZ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/HUZ;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2c393b03

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473235
    iget-object v1, p0, LX/HUZ;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v2, p0, LX/HUZ;->b:Ljava/lang/String;

    iget-object v3, p0, LX/HUZ;->c:Ljava/lang/String;

    .line 2473236
    iget-object v5, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->d:LX/HU1;

    .line 2473237
    const-string v6, "pages_issues_share_click"

    invoke-static {v5, v6, v2}, LX/HU1;->c(LX/HU1;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2473238
    iget-object p0, v5, LX/HU1;->a:LX/0Zb;

    invoke-interface {p0, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2473239
    iget-object v5, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-interface {v5}, LX/3Sb;->a()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2473240
    invoke-static {v1, v3, v2}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2473241
    iget-object v5, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->m:LX/6WS;

    invoke-virtual {v5, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2473242
    :goto_0
    const v1, 0x410e2965

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2473243
    :cond_0
    const-string v5, "pagesIssuesTab"

    .line 2473244
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f0836ee

    invoke-virtual {v6, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2473245
    invoke-static {v1, v3, v5, v6}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
