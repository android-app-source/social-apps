.class public final LX/JJK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/SyncResult;

.field public final synthetic b:LX/JJL;


# direct methods
.method public constructor <init>(LX/JJL;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 2679295
    iput-object p1, p0, LX/JJK;->b:LX/JJL;

    iput-object p2, p0, LX/JJK;->a:Landroid/content/SyncResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2679293
    const-string v0, "CalendarSyncAdapterImpl"

    const-string v1, "Failure in fetch request:"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2679294
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2679288
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2679289
    :try_start_0
    iget-object v1, p0, LX/JJK;->b:LX/JJL;

    iget-object v2, p0, LX/JJK;->a:Landroid/content/SyncResult;

    .line 2679290
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2679291
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-static {v1, v2, v0}, LX/JJL;->a$redex0(LX/JJL;Landroid/content/SyncResult;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2679292
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
