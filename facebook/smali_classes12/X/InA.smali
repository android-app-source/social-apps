.class public LX/InA;
.super LX/6LI;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/03V;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/view/LayoutInflater;

.field public final e:LX/0Uh;

.field public f:LX/In9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;LX/0Or;Landroid/view/LayoutInflater;LX/0Uh;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/view/LayoutInflater;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610275
    const-string v0, "PaymentPlatformContextBannerNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2610276
    iput-object p1, p0, LX/InA;->a:Landroid/content/Context;

    .line 2610277
    iput-object p2, p0, LX/InA;->b:LX/03V;

    .line 2610278
    iput-object p3, p0, LX/InA;->c:LX/0Or;

    .line 2610279
    iput-object p4, p0, LX/InA;->d:Landroid/view/LayoutInflater;

    .line 2610280
    iput-object p5, p0, LX/InA;->e:LX/0Uh;

    .line 2610281
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2610282
    iget-object v0, p0, LX/InA;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2610283
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, LX/InA;->a:Landroid/content/Context;

    const v2, 0x7f0e0311

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 2610284
    iget-object v1, p0, LX/InA;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2610285
    iget-object v1, p0, LX/InA;->e:LX/0Uh;

    const/16 v2, 0x237

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f030f0c

    :goto_0
    move v1, v1

    .line 2610286
    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2610287
    iget-object v0, p0, LX/InA;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2610288
    iget-object v0, p0, LX/InA;->b:LX/03V;

    const-string v2, "PaymentPlatformContextBannerNotification"

    const-string v3, "null ViewerContextUser found when populating the banner for payment platform context."

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2610289
    :goto_1
    return-object v1

    .line 2610290
    :cond_0
    iget-object v0, p0, LX/InA;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2610291
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2610292
    iget-object v2, p0, LX/InA;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2610293
    check-cast v0, LX/InB;

    .line 2610294
    iget-object v2, p0, LX/InA;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/InB;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V

    .line 2610295
    new-instance v2, LX/In7;

    invoke-direct {v2, p0}, LX/In7;-><init>(LX/InA;)V

    invoke-interface {v0, v2}, LX/InB;->setListener(LX/In6;)V

    .line 2610296
    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 2610297
    check-cast v0, LX/InB;

    .line 2610298
    iget-object v2, p0, LX/InA;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LX/InB;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V

    .line 2610299
    iget-object v2, p0, LX/InA;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v2

    .line 2610300
    new-instance v3, LX/In8;

    invoke-direct {v3, p0, v2}, LX/In8;-><init>(LX/InA;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)V

    invoke-interface {v0, v3}, LX/InB;->setListener(LX/In6;)V

    .line 2610301
    goto :goto_1

    :cond_2
    const v1, 0x7f030f0b

    goto :goto_0
.end method
