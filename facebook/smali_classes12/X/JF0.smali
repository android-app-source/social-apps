.class public LX/JF0;
.super LX/4sY;
.source ""

# interfaces
.implements LX/2NW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4sY",
        "<",
        "LX/JEx;",
        ">;",
        "LX/2NW;"
    }
.end annotation


# static fields
.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:I

.field private final f:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/JEy;

    invoke-direct {v0}, LX/JEy;-><init>()V

    sput-object v0, LX/JF0;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, LX/4sY;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p3, p0, LX/JF0;->d:Landroid/content/Context;

    iget v0, p1, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v0, v0

    invoke-static {v0}, LX/JF6;->b(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, LX/JF0;->f:Lcom/google/android/gms/common/api/Status;

    invoke-static {p2}, LX/JEz;->a(I)I

    move-result v0

    iput v0, p0, LX/JF0;->e:I

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/common/data/DataHolder;->i:Landroid/os/Bundle;

    move-object v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/common/data/DataHolder;->i:Landroid/os/Bundle;

    move-object v0, v0

    const-string v1, "com.google.android.gms.location.places.PlaceLikelihoodBuffer.ATTRIBUTIONS_EXTRA_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JF0;->c:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/JF0;->c:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LX/JF0;->f:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3

    new-instance v0, LX/JFY;

    iget-object v1, p0, LX/4sY;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, LX/JF0;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p1, v2}, LX/JFY;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, LX/2wy;->a(Ljava/lang/Object;)LX/4sw;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p0}, LX/JF0;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4sw;->a(Ljava/lang/String;Ljava/lang/Object;)LX/4sw;

    move-result-object v0

    const-string v1, "attributions"

    iget-object v2, p0, LX/JF0;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/4sw;->a(Ljava/lang/String;Ljava/lang/Object;)LX/4sw;

    move-result-object v0

    invoke-virtual {v0}, LX/4sw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
