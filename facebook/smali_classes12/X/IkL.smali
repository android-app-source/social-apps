.class public LX/IkL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/IkO;

.field private final b:LX/IkM;

.field private final c:LX/IkN;

.field private final d:LX/IkG;

.field private final e:LX/IkP;

.field private final f:LX/IkQ;

.field private final g:LX/IkI;


# direct methods
.method public constructor <init>(LX/IkO;LX/IkM;LX/IkN;LX/IkG;LX/IkP;LX/IkQ;LX/IkI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606622
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606623
    iput-object p1, p0, LX/IkL;->a:LX/IkO;

    .line 2606624
    iput-object p2, p0, LX/IkL;->b:LX/IkM;

    .line 2606625
    iput-object p3, p0, LX/IkL;->c:LX/IkN;

    .line 2606626
    iput-object p4, p0, LX/IkL;->d:LX/IkG;

    .line 2606627
    iput-object p5, p0, LX/IkL;->e:LX/IkP;

    .line 2606628
    iput-object p6, p0, LX/IkL;->f:LX/IkQ;

    .line 2606629
    iput-object p7, p0, LX/IkL;->g:LX/IkI;

    .line 2606630
    return-void
.end method

.method public static a(LX/0QB;)LX/IkL;
    .locals 11

    .prologue
    .line 2606631
    const-class v1, LX/IkL;

    monitor-enter v1

    .line 2606632
    :try_start_0
    sget-object v0, LX/IkL;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606633
    sput-object v2, LX/IkL;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606634
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606635
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606636
    new-instance v3, LX/IkL;

    invoke-static {v0}, LX/IkO;->a(LX/0QB;)LX/IkO;

    move-result-object v4

    check-cast v4, LX/IkO;

    invoke-static {v0}, LX/IkM;->a(LX/0QB;)LX/IkM;

    move-result-object v5

    check-cast v5, LX/IkM;

    invoke-static {v0}, LX/IkN;->a(LX/0QB;)LX/IkN;

    move-result-object v6

    check-cast v6, LX/IkN;

    invoke-static {v0}, LX/IkG;->a(LX/0QB;)LX/IkG;

    move-result-object v7

    check-cast v7, LX/IkG;

    invoke-static {v0}, LX/IkP;->a(LX/0QB;)LX/IkP;

    move-result-object v8

    check-cast v8, LX/IkP;

    invoke-static {v0}, LX/IkQ;->a(LX/0QB;)LX/IkQ;

    move-result-object v9

    check-cast v9, LX/IkQ;

    invoke-static {v0}, LX/IkI;->a(LX/0QB;)LX/IkI;

    move-result-object v10

    check-cast v10, LX/IkI;

    invoke-direct/range {v3 .. v10}, LX/IkL;-><init>(LX/IkO;LX/IkM;LX/IkN;LX/IkG;LX/IkP;LX/IkQ;LX/IkI;)V

    .line 2606637
    move-object v0, v3

    .line 2606638
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606639
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606640
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606641
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
