.class public LX/Ifw;
.super LX/Ifv;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600220
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ifw;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Ify;LX/Ig0;LX/Ig1;LX/Ifz;LX/0SG;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600221
    invoke-direct {p0, p5}, LX/Ifv;-><init>(LX/0SG;)V

    .line 2600222
    invoke-virtual {p2, p0}, LX/Ig0;->a(LX/Ifv;)V

    .line 2600223
    invoke-virtual {p3, p0}, LX/Ig1;->a(LX/Ifv;)V

    .line 2600224
    invoke-virtual {p4, p0}, LX/Ifz;->a(LX/Ifv;)V

    .line 2600225
    iput-object p0, p1, LX/Ify;->d:LX/Ifv;

    .line 2600226
    iget-object p5, p1, LX/Ify;->d:LX/Ifv;

    invoke-virtual {p5, p1}, LX/Ifv;->a(LX/Ifx;)V

    .line 2600227
    invoke-static {p1}, LX/Ify;->b(LX/Ify;)V

    .line 2600228
    iget-object p0, p3, LX/Ig1;->g:LX/Ifv;

    .line 2600229
    iget-object p1, p0, LX/Ifv;->g:LX/0mY;

    invoke-virtual {p1, p3}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600230
    iget-object p0, p3, LX/Ig1;->g:LX/Ifv;

    .line 2600231
    iget-object p1, p0, LX/Ifv;->j:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    move-object p0, p1

    .line 2600232
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ig7;

    .line 2600233
    invoke-static {p3, p0}, LX/Ig1;->b(LX/Ig1;LX/Ig7;)V

    goto :goto_0

    .line 2600234
    :cond_0
    iget-object p0, p2, LX/Ig0;->e:LX/Ifv;

    .line 2600235
    iget-object p1, p0, LX/Ifv;->b:LX/0mY;

    invoke-virtual {p1, p2}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600236
    iget-object p0, p2, LX/Ig0;->e:LX/Ifv;

    .line 2600237
    iget-object p1, p0, LX/Ifv;->f:LX/0mY;

    invoke-virtual {p1, p2}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600238
    iget-object p0, p2, LX/Ig0;->e:LX/Ifv;

    .line 2600239
    iget-object p1, p0, LX/Ifv;->e:LX/0mY;

    invoke-virtual {p1, p2}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600240
    iget-object p0, p2, LX/Ig0;->e:LX/Ifv;

    .line 2600241
    iget-object p1, p0, LX/Ifv;->h:LX/0kD;

    invoke-interface {p1}, LX/0kD;->d()Ljava/util/Collection;

    move-result-object p1

    move-object p0, p1

    .line 2600242
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Ig6;

    .line 2600243
    invoke-static {p2, p0}, LX/Ig0;->d(LX/Ig0;LX/Ig6;)V

    goto :goto_1

    .line 2600244
    :cond_1
    iget-object p0, p4, LX/Ifz;->e:LX/Ifv;

    invoke-virtual {p0, p4}, LX/Ifv;->a(LX/Ifx;)V

    .line 2600245
    iget-object p0, p4, LX/Ifz;->e:LX/Ifv;

    .line 2600246
    iget-object p1, p0, LX/Ifv;->c:LX/0mY;

    invoke-virtual {p1, p4}, LX/0mY;->b(Ljava/lang/Object;)Z

    .line 2600247
    invoke-static {p4}, LX/Ifz;->d(LX/Ifz;)V

    .line 2600248
    return-void
.end method

.method public static a(LX/0QB;)LX/Ifw;
    .locals 15

    .prologue
    .line 2600249
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2600250
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2600251
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2600252
    if-nez v1, :cond_0

    .line 2600253
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2600254
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2600255
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2600256
    sget-object v1, LX/Ifw;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2600257
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2600258
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2600259
    :cond_1
    if-nez v1, :cond_4

    .line 2600260
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2600261
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2600262
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2600263
    new-instance v7, LX/Ifw;

    .line 2600264
    new-instance v10, LX/Ify;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v9

    check-cast v9, LX/0aU;

    invoke-direct {v10, v8, v9}, LX/Ify;-><init>(Landroid/content/Context;LX/0aU;)V

    .line 2600265
    invoke-static {v0}, LX/6Z6;->b(LX/0QB;)LX/6Z6;

    move-result-object v8

    check-cast v8, LX/6Z6;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    .line 2600266
    iput-object v8, v10, LX/Ify;->a:LX/6Z6;

    iput-object v9, v10, LX/Ify;->b:LX/03V;

    .line 2600267
    move-object v8, v10

    .line 2600268
    check-cast v8, LX/Ify;

    .line 2600269
    new-instance v11, LX/Ig0;

    invoke-direct {v11}, LX/Ig0;-><init>()V

    .line 2600270
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v9

    check-cast v9, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v10, 0x12cd

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    .line 2600271
    iput-object v9, v11, LX/Ig0;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v12, v11, LX/Ig0;->b:LX/0Or;

    iput-object v10, v11, LX/Ig0;->c:LX/03V;

    .line 2600272
    move-object v9, v11

    .line 2600273
    check-cast v9, LX/Ig0;

    .line 2600274
    new-instance v13, LX/Ig1;

    invoke-direct {v13}, LX/Ig1;-><init>()V

    .line 2600275
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v11, 0x12cd

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    .line 2600276
    iput-object v10, v13, LX/Ig1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v14, v13, LX/Ig1;->c:LX/0Or;

    iput-object v11, v13, LX/Ig1;->d:LX/03V;

    iput-object v12, v13, LX/Ig1;->e:LX/0SG;

    .line 2600277
    move-object v10, v13

    .line 2600278
    check-cast v10, LX/Ig1;

    .line 2600279
    new-instance v13, LX/Ifz;

    invoke-direct {v13}, LX/Ifz;-><init>()V

    .line 2600280
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v12, 0x12cd

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    .line 2600281
    iput-object v11, v13, LX/Ifz;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v14, v13, LX/Ifz;->b:LX/0Or;

    iput-object v12, v13, LX/Ifz;->c:LX/03V;

    .line 2600282
    move-object v11, v13

    .line 2600283
    check-cast v11, LX/Ifz;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-direct/range {v7 .. v12}, LX/Ifw;-><init>(LX/Ify;LX/Ig0;LX/Ig1;LX/Ifz;LX/0SG;)V

    .line 2600284
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2600285
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2600286
    if-nez v1, :cond_2

    .line 2600287
    sget-object v0, LX/Ifw;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifw;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2600288
    :goto_1
    if-eqz v0, :cond_3

    .line 2600289
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600290
    :goto_3
    check-cast v0, LX/Ifw;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2600291
    invoke-virtual {v3}, LX/0op;->c()V

    goto/16 :goto_0

    .line 2600292
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2600293
    :catchall_1
    move-exception v0

    .line 2600294
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600295
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2600296
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2600297
    :cond_2
    :try_start_8
    sget-object v0, LX/Ifw;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifw;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
