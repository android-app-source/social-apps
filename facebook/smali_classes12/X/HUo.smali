.class public final LX/HUo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2473823
    iput-object p1, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2473824
    iget-object v0, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2473825
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2473826
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2473827
    if-eqz p1, :cond_0

    .line 2473828
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473829
    if-eqz v0, :cond_0

    .line 2473830
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473831
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2473832
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473833
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2473834
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473835
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2473836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473837
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2473838
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473839
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2473840
    :cond_0
    :goto_0
    return-void

    .line 2473841
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2473842
    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;->j()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel$VideoListsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    .line 2473843
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->l()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2473844
    iget-object v2, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    .line 2473845
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2473846
    check-cast v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2473847
    iput-object v1, v2, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->k:Ljava/lang/String;

    .line 2473848
    iget-object v1, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2473849
    iput-object v2, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->l:Ljava/lang/String;

    .line 2473850
    iget-object v1, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2473851
    iput-object v2, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->m:Ljava/lang/String;

    .line 2473852
    iget-object v1, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v0

    .line 2473853
    iget-object v2, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->j:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;->j()LX/0Px;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2473854
    invoke-static {v1}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    .line 2473855
    iget-object v0, p0, LX/HUo;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    goto :goto_0
.end method
