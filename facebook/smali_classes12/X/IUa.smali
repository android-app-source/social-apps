.class public LX/IUa;
.super LX/2s5;
.source ""


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/0gc;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2580723
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2580724
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Home"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Posts"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/IUa;->a:[Ljava/lang/String;

    .line 2580725
    iput-object p2, p0, LX/IUa;->b:Landroid/os/Bundle;

    .line 2580726
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2580711
    iget-object v0, p0, LX/IUa;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2580713
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2580714
    new-instance v0, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {v0}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;-><init>()V

    .line 2580715
    iget-object v1, p0, LX/IUa;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2580716
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IUa;->b:Landroid/os/Bundle;

    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2580717
    new-instance v1, Lcom/facebook/groups/feed/ui/GroupsPostTabFragment;

    invoke-direct {v1}, Lcom/facebook/groups/feed/ui/GroupsPostTabFragment;-><init>()V

    .line 2580718
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2580719
    const-string p1, "group_feed_id"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2580720
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2580721
    move-object v0, v1

    .line 2580722
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2580712
    iget-object v0, p0, LX/IUa;->a:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method
