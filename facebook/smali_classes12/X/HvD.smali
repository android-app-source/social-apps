.class public LX/HvD;
.super LX/0jL;
.source ""


# instance fields
.field public final a:LX/0jK;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/system/mutator/ComposerMutationImpl$Committer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Sh;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0jK;LX/HvF;LX/0Sh;)V
    .locals 2
    .param p1    # Lcom/facebook/composer/system/model/ComposerModelImpl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0jK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/HvF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518775
    invoke-direct {p0, p1, p4}, LX/0jL;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0Sh;)V

    .line 2518776
    iput-object p2, p0, LX/HvD;->a:LX/0jK;

    .line 2518777
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HvD;->b:Ljava/lang/ref/WeakReference;

    .line 2518778
    iput-object p4, p0, LX/HvD;->c:LX/0Sh;

    .line 2518779
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2518780
    iget-object v0, p0, LX/HvD;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2518781
    iget-object v0, p0, LX/HvD;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HvF;

    .line 2518782
    iget-object v1, v0, LX/HvF;->a:LX/0jJ;

    invoke-static {v1, p0}, LX/0jJ;->a$redex0(LX/0jJ;LX/HvD;)V

    .line 2518783
    return-void
.end method
