.class public LX/JSy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/net/Uri;

.field private final e:Landroid/net/Uri;

.field private final f:Landroid/net/Uri;

.field private final g:Landroid/net/Uri;

.field private final h:Ljava/lang/String;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/JSe;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I


# direct methods
.method public constructor <init>(LX/JSx;)V
    .locals 2

    .prologue
    .line 2696273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696274
    iget-object v0, p1, LX/JSx;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, LX/JSy;->a:Ljava/lang/String;

    .line 2696275
    iget-object v0, p1, LX/JSx;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    iput-object v0, p0, LX/JSy;->b:Ljava/lang/String;

    .line 2696276
    iget-object v0, p1, LX/JSx;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    iput-object v0, p0, LX/JSy;->c:Ljava/lang/String;

    .line 2696277
    iget-object v0, p1, LX/JSx;->d:Landroid/net/Uri;

    iput-object v0, p0, LX/JSy;->d:Landroid/net/Uri;

    .line 2696278
    iget-object v0, p1, LX/JSx;->h:Landroid/net/Uri;

    iput-object v0, p0, LX/JSy;->e:Landroid/net/Uri;

    .line 2696279
    iget-object v0, p1, LX/JSx;->e:Landroid/net/Uri;

    iput-object v0, p0, LX/JSy;->f:Landroid/net/Uri;

    .line 2696280
    iget-object v0, p1, LX/JSx;->f:Landroid/net/Uri;

    iput-object v0, p0, LX/JSy;->g:Landroid/net/Uri;

    .line 2696281
    iget-object v0, p1, LX/JSx;->g:Ljava/lang/String;

    iput-object v0, p0, LX/JSy;->h:Ljava/lang/String;

    .line 2696282
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p1, LX/JSx;->i:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/JSy;->i:LX/0Px;

    .line 2696283
    iget-object v0, p1, LX/JSx;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, LX/JSy;->j:I

    .line 2696284
    return-void

    .line 2696285
    :cond_0
    iget-object v0, p1, LX/JSx;->a:Ljava/lang/String;

    goto :goto_0

    .line 2696286
    :cond_1
    iget-object v0, p1, LX/JSx;->b:Ljava/lang/String;

    goto :goto_1

    .line 2696287
    :cond_2
    iget-object v0, p1, LX/JSx;->c:Ljava/lang/String;

    goto :goto_2
.end method
