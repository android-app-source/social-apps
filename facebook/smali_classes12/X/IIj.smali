.class public final LX/IIj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2561954
    iput-object p1, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2561963
    add-int v0, p2, p3

    add-int/lit8 v1, p4, -0x3

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    .line 2561964
    :goto_0
    iget-boolean v1, p0, LX/IIj;->b:Z

    if-ne v0, v1, :cond_2

    .line 2561965
    :cond_0
    :goto_1
    return-void

    .line 2561966
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2561967
    :cond_2
    iput-boolean v0, p0, LX/IIj;->b:Z

    .line 2561968
    if-eqz v0, :cond_0

    .line 2561969
    iget-object v0, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->s:LX/IFl;

    .line 2561970
    iget-object v1, v0, LX/IFl;->h:LX/IFk;

    move-object v0, v1

    .line 2561971
    sget-object v1, LX/IFk;->HAS_MORE:LX/IFk;

    if-ne v0, v1, :cond_4

    .line 2561972
    iget-object v0, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->s:LX/IFl;

    iget-object v1, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aM:LX/IIN;

    .line 2561973
    iget-object p1, v0, LX/IFl;->h:LX/IFk;

    sget-object p2, LX/IFk;->COMPLETE:LX/IFk;

    if-eq p1, p2, :cond_3

    iget-object p1, v0, LX/IFl;->h:LX/IFk;

    sget-object p2, LX/IFk;->ERROR:LX/IFk;

    if-eq p1, p2, :cond_3

    iget-object p1, v0, LX/IFl;->h:LX/IFk;

    sget-object p2, LX/IFk;->LOADING:LX/IFk;

    if-ne p1, p2, :cond_5

    .line 2561974
    :cond_3
    :goto_2
    iget-object v0, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2561975
    :cond_4
    goto :goto_1

    .line 2561976
    :cond_5
    sget-object p1, LX/IFk;->LOADING:LX/IFk;

    iput-object p1, v0, LX/IFl;->h:LX/IFk;

    .line 2561977
    invoke-static {}, LX/IGs;->d()LX/IGr;

    move-result-object p1

    .line 2561978
    const-string p2, "cursor"

    iget-object p3, v0, LX/IFl;->i:Ljava/lang/String;

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p2

    const-string p3, "pic_size"

    iget-object p4, v0, LX/IFl;->f:Ljava/lang/String;

    invoke-virtual {p2, p3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p2

    const-string p3, "searchText"

    iget-object p4, v0, LX/IFl;->j:Ljava/lang/String;

    invoke-virtual {p2, p3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p2

    const-string p3, "order"

    sget-object p4, LX/IFl;->b:Ljava/util/List;

    invoke-virtual {p2, p3, p4}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object p2

    const-string p3, "count"

    const-string p4, "20"

    invoke-virtual {p2, p3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2561979
    iget-object p2, v0, LX/IFl;->d:LX/0tX;

    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p1

    .line 2561980
    iget-object p2, v0, LX/IFl;->c:LX/1Ck;

    sget-object p3, LX/IFy;->k:LX/IFy;

    new-instance p4, LX/IFj;

    invoke-direct {p4, v0, v1}, LX/IFj;-><init>(LX/IFl;LX/IIN;)V

    invoke-virtual {p2, p3, p1, p4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2561981
    goto :goto_2
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 2561955
    packed-switch p2, :pswitch_data_0

    .line 2561956
    :goto_0
    return-void

    .line 2561957
    :pswitch_0
    iget-object v0, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2561958
    iget-boolean p1, v0, LX/IID;->f:Z

    if-eqz p1, :cond_0

    .line 2561959
    :goto_1
    iget-object v0, p0, LX/IIj;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->v$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    goto :goto_0

    .line 2561960
    :cond_0
    const-string p1, "friends_nearby_dashboard_scroll_forward_during_session"

    invoke-static {v0, p1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 2561961
    iget-object p2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561962
    const/4 p1, 0x1

    iput-boolean p1, v0, LX/IID;->f:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
