.class public LX/Ig9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final m:Ljava/lang/Object;


# instance fields
.field private a:LX/Ifw;

.field public b:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/It1;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/Ig3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0y3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/Ig4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ig5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600654
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ig9;->m:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/Ifw;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600656
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2600657
    iput-object v0, p0, LX/Ig9;->d:LX/0Ot;

    .line 2600658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2600659
    iput-object v0, p0, LX/Ig9;->l:LX/0Ot;

    .line 2600660
    iput-object p1, p0, LX/Ig9;->a:LX/Ifw;

    .line 2600661
    return-void
.end method

.method public static a(LX/0QB;)LX/Ig9;
    .locals 7

    .prologue
    .line 2600662
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2600663
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2600664
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2600665
    if-nez v1, :cond_0

    .line 2600666
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2600667
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2600668
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2600669
    sget-object v1, LX/Ig9;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2600670
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2600671
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2600672
    :cond_1
    if-nez v1, :cond_4

    .line 2600673
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2600674
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2600675
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Ig9;->b(LX/0QB;)LX/Ig9;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2600676
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2600677
    if-nez v1, :cond_2

    .line 2600678
    sget-object v0, LX/Ig9;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig9;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2600679
    :goto_1
    if-eqz v0, :cond_3

    .line 2600680
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600681
    :goto_3
    check-cast v0, LX/Ig9;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2600682
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2600683
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2600684
    :catchall_1
    move-exception v0

    .line 2600685
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600686
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2600687
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2600688
    :cond_2
    :try_start_8
    sget-object v0, LX/Ig9;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig9;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Ig9;
    .locals 12

    .prologue
    .line 2600689
    new-instance v0, LX/Ig9;

    invoke-static {p0}, LX/Ifw;->a(LX/0QB;)LX/Ifw;

    move-result-object v1

    check-cast v1, LX/Ifw;

    invoke-direct {v0, v1}, LX/Ig9;-><init>(LX/Ifw;)V

    .line 2600690
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 v2, 0x12cd

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x291f

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/Ig3;->a(LX/0QB;)LX/Ig3;

    move-result-object v6

    check-cast v6, LX/Ig3;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v7

    check-cast v7, LX/0y3;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const-class v9, Landroid/content/Context;

    invoke-interface {p0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {p0}, LX/Ig4;->b(LX/0QB;)LX/Ig4;

    move-result-object v10

    check-cast v10, LX/Ig4;

    const/16 v11, 0x27cd

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 2600691
    iput-object v1, v0, LX/Ig9;->b:LX/0SG;

    iput-object v2, v0, LX/Ig9;->c:LX/0Or;

    iput-object v3, v0, LX/Ig9;->d:LX/0Ot;

    iput-object v4, v0, LX/Ig9;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v0, LX/Ig9;->f:LX/0tX;

    iput-object v6, v0, LX/Ig9;->g:LX/Ig3;

    iput-object v7, v0, LX/Ig9;->h:LX/0y3;

    iput-object v8, v0, LX/Ig9;->i:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v0, LX/Ig9;->j:Landroid/content/Context;

    iput-object v10, v0, LX/Ig9;->k:LX/Ig4;

    iput-object v11, v0, LX/Ig9;->l:LX/0Ot;

    .line 2600692
    return-object v0
.end method
