.class public LX/Hw8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Hvw;

.field public final b:Landroid/content/Context;

.field public final c:LX/Hw7;

.field public d:Ljava/util/Calendar;

.field public e:LX/0kL;

.field public f:LX/0SG;

.field public final g:LX/Hvy;


# direct methods
.method public constructor <init>(LX/Hvw;Ljava/lang/Long;LX/0kL;LX/0SG;Landroid/content/Context;LX/Hvy;)V
    .locals 6
    .param p1    # LX/Hvw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2520151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520152
    iput-object p1, p0, LX/Hw8;->a:LX/Hvw;

    .line 2520153
    iput-object p5, p0, LX/Hw8;->b:Landroid/content/Context;

    .line 2520154
    iput-object p3, p0, LX/Hw8;->e:LX/0kL;

    .line 2520155
    iput-object p4, p0, LX/Hw8;->f:LX/0SG;

    .line 2520156
    iput-object p6, p0, LX/Hw8;->g:LX/Hvy;

    .line 2520157
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/Hw8;->d:Ljava/util/Calendar;

    .line 2520158
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2520159
    :cond_0
    iget-object v0, p0, LX/Hw8;->d:Ljava/util/Calendar;

    const/16 v1, 0xc

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2520160
    :goto_0
    new-instance v0, LX/Hw7;

    iget-object v1, p0, LX/Hw8;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/Hw7;-><init>(LX/Hw8;Landroid/content/Context;)V

    iput-object v0, p0, LX/Hw8;->c:LX/Hw7;

    .line 2520161
    return-void

    .line 2520162
    :cond_1
    iget-object v0, p0, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method public static a(JJLX/0kL;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2520145
    sub-long v2, p0, p2

    const-wide/32 v4, 0x927c0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 2520146
    new-instance v1, LX/27k;

    const v2, 0x7f08144d

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {p4, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2520147
    :goto_0
    return v0

    .line 2520148
    :cond_0
    sub-long v2, p0, p2

    const-wide v4, 0x39ef8b000L

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 2520149
    new-instance v1, LX/27k;

    const v2, 0x7f08144e

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {p4, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 2520150
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
