.class public final LX/J4R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/1oT;

.field public final synthetic c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;Ljava/lang/String;LX/1oT;)V
    .locals 0

    .prologue
    .line 2644152
    iput-object p1, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iput-object p2, p0, LX/J4R;->a:Ljava/lang/String;

    iput-object p3, p0, LX/J4R;->b:LX/1oT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2644153
    iget-object v0, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->b:LX/J5k;

    sget-object v1, LX/J5j;->PROFILE_PHOTO_CHECKUP:LX/J5j;

    iget-object v1, v1, LX/J5j;->reviewType:Ljava/lang/String;

    sget-object v2, LX/J5i;->PHOTO_CHECKUP_BULK_EDIT_ACCEPTED:LX/J5i;

    iget-object v2, v2, LX/J5i;->eventName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2644154
    iget-object v0, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a:LX/J4p;

    iget-object v1, p0, LX/J4R;->a:Ljava/lang/String;

    sget-object v2, LX/5mv;->PROFILE_PHOTO_CHECKUP:LX/5mv;

    sget-object v3, LX/5mu;->PROFILE_PHOTO_ALBUM:LX/5mu;

    iget-object v4, p0, LX/J4R;->b:LX/1oT;

    invoke-interface {v4}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v5, v5, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->C:LX/0Vd;

    invoke-virtual/range {v0 .. v5}, LX/J4p;->a(Ljava/lang/String;LX/5mv;LX/5mu;Ljava/lang/String;LX/0Vd;)V

    .line 2644155
    iget-object v0, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2644156
    if-eqz v0, :cond_0

    .line 2644157
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2644158
    iget-object v0, p0, LX/J4R;->c:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083900

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2644159
    :cond_0
    return-void
.end method
