.class public LX/JDe;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2664511
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2664512
    const v0, 0x7f0302a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2664513
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JDe;->setOrientation(I)V

    .line 2664514
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b249b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v2, v2, v2, v0}, LX/JDe;->setPadding(IIII)V

    .line 2664515
    const v0, 0x7f0d0965

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    iput-object v0, p0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2664516
    return-void
.end method


# virtual methods
.method public final a(LX/JD9;ZZZ)V
    .locals 1

    .prologue
    .line 2664517
    iget-object v0, p0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2664518
    iput-boolean p3, v0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2664519
    iget-object v0, p0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2664520
    iput-boolean p2, v0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2664521
    iget-object v0, p0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    .line 2664522
    iput-boolean p4, v0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2664523
    iget-object v0, p0, LX/JDe;->a:Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(LX/JD9;)V

    .line 2664524
    return-void
.end method
