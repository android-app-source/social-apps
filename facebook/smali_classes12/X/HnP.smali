.class public final LX/HnP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/webkit/ValueCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/webkit/ValueCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0EC;

.field private b:Landroid/webkit/CookieManager;

.field private c:LX/0Zb;

.field private d:Z


# direct methods
.method public constructor <init>(LX/0EC;Landroid/webkit/CookieManager;ZLX/0Zb;)V
    .locals 0

    .prologue
    .line 2501826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501827
    iput-object p1, p0, LX/HnP;->a:LX/0EC;

    .line 2501828
    iput-object p2, p0, LX/HnP;->b:Landroid/webkit/CookieManager;

    .line 2501829
    iput-object p4, p0, LX/HnP;->c:LX/0Zb;

    .line 2501830
    iput-boolean p3, p0, LX/HnP;->d:Z

    .line 2501831
    return-void
.end method


# virtual methods
.method public final onReceiveValue(Ljava/lang/Object;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 2501832
    check-cast p1, Ljava/lang/Boolean;

    .line 2501833
    iget-object v0, p0, LX/HnP;->b:Landroid/webkit/CookieManager;

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->flush()V

    .line 2501834
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HnP;->d:Z

    if-eqz v0, :cond_0

    .line 2501835
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fb4a_iab_sync_cookie_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2501836
    const-string v1, "status"

    const-string v2, "set_cookie_failed"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2501837
    const-string v1, "url"

    iget-object v2, p0, LX/HnP;->a:LX/0EC;

    invoke-virtual {v2}, LX/0EC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2501838
    const-string v1, "value"

    iget-object v2, p0, LX/HnP;->a:LX/0EC;

    invoke-virtual {v2}, LX/0EC;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2501839
    iget-object v1, p0, LX/HnP;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2501840
    :cond_0
    return-void
.end method
