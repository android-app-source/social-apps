.class public final LX/HND;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/H8Z;

.field public final synthetic b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic d:LX/HNE;


# direct methods
.method public constructor <init>(LX/HNE;LX/H8Z;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 0

    .prologue
    .line 2458222
    iput-object p1, p0, LX/HND;->d:LX/HNE;

    iput-object p2, p0, LX/HND;->a:LX/H8Z;

    iput-object p3, p0, LX/HND;->b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    iput-object p4, p0, LX/HND;->c:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2458223
    iget-object v0, p0, LX/HND;->a:LX/H8Z;

    .line 2458224
    instance-of v1, v0, LX/HNH;

    if-eqz v1, :cond_1

    .line 2458225
    const-string v1, "pages_action_channel_click_copy_tab_link"

    .line 2458226
    :goto_0
    move-object v0, v1

    .line 2458227
    if-eqz v0, :cond_0

    .line 2458228
    iget-object v1, p0, LX/HND;->d:LX/HNE;

    iget-object v1, v1, LX/HNE;->f:LX/HN9;

    iget-object v2, p0, LX/HND;->b:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/HND;->c:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v0, v4}, LX/HN9;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2458229
    :cond_0
    iget-object v0, p0, LX/HND;->a:LX/H8Z;

    invoke-interface {v0}, LX/H8Y;->c()V

    .line 2458230
    const/4 v0, 0x1

    return v0

    .line 2458231
    :cond_1
    instance-of v1, v0, LX/HNP;

    if-eqz v1, :cond_2

    .line 2458232
    const-string v1, "pages_action_channel_click_share_tab"

    goto :goto_0

    .line 2458233
    :cond_2
    instance-of v1, v0, LX/HNO;

    if-eqz v1, :cond_3

    .line 2458234
    const-string v1, "pages_action_channel_click_reorder_tabs"

    goto :goto_0

    .line 2458235
    :cond_3
    instance-of v1, v0, LX/HNM;

    if-eqz v1, :cond_4

    .line 2458236
    const-string v1, "pages_action_channel_click_delete_tab"

    goto :goto_0

    .line 2458237
    :cond_4
    instance-of v1, v0, LX/HNR;

    if-eqz v1, :cond_5

    .line 2458238
    const-string v1, "pages_action_channel_click_visit_page"

    goto :goto_0

    .line 2458239
    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method
