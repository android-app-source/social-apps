.class public final LX/J1w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/J29;

.field public final synthetic b:LX/J2n;

.field public final synthetic c:LX/J1x;


# direct methods
.method public constructor <init>(LX/J1x;LX/J29;LX/J2n;)V
    .locals 0

    .prologue
    .line 2639579
    iput-object p1, p0, LX/J1w;->c:LX/J1x;

    iput-object p2, p0, LX/J1w;->a:LX/J29;

    iput-object p3, p0, LX/J1w;->b:LX/J2n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2b65c077    # -5.2999273E12f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2639580
    iget-object v1, p0, LX/J1w;->a:LX/J29;

    iget-object v2, p0, LX/J1w;->b:LX/J2n;

    .line 2639581
    sget-object v4, LX/J28;->a:[I

    iget-object v5, v2, LX/J2n;->a:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2639582
    :goto_0
    const v1, -0x5902a317

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2639583
    :pswitch_0
    iget-object v5, v2, LX/J2n;->a:Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;

    iget-object v4, v2, LX/J2n;->d:LX/J2g;

    check-cast v4, LX/J2g;

    .line 2639584
    sget-object v6, LX/J28;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPaymentActivityActionIdentifier;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    .line 2639585
    const/4 v6, 0x0

    :goto_1
    move-object v6, v6

    .line 2639586
    if-eqz v6, :cond_0

    if-eqz v4, :cond_0

    .line 2639587
    iget-object v7, v1, LX/J29;->c:LX/Iyi;

    iget-object p0, v4, LX/J2g;->a:Ljava/lang/String;

    iget-object p1, v4, LX/J2g;->b:LX/6xh;

    .line 2639588
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2639589
    invoke-virtual {v7, p0, p1, v6, v2}, LX/Iyi;->a(Ljava/lang/String;LX/6xh;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2639590
    new-instance v7, LX/J26;

    invoke-direct {v7, v1}, LX/J26;-><init>(LX/J29;)V

    iget-object p0, v1, LX/J29;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2639591
    iget-object v7, v1, LX/J29;->e:LX/6qh;

    const/4 p0, 0x1

    invoke-virtual {v7, v6, p0}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 2639592
    :cond_0
    goto :goto_0

    .line 2639593
    :pswitch_1
    const-string v6, "MARK_AS_PAID"

    goto :goto_1

    .line 2639594
    :pswitch_2
    const-string v6, "CANCEL"

    goto :goto_1

    .line 2639595
    :pswitch_3
    const-string v6, "MARK_AS_SHIPPED"

    goto :goto_1

    .line 2639596
    :pswitch_4
    const-string v6, "REQUEST_NEW_RECEIPT"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
