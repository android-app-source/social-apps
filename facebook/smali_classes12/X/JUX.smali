.class public final LX/JUX;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3Ab;

.field public final synthetic b:LX/JUY;


# direct methods
.method public constructor <init>(LX/JUY;)V
    .locals 1

    .prologue
    .line 2699226
    iput-object p1, p0, LX/JUX;->b:LX/JUY;

    .line 2699227
    move-object v0, p1

    .line 2699228
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699229
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699230
    const-string v0, "PageContextualRecommendationsHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2699231
    if-ne p0, p1, :cond_1

    .line 2699232
    :cond_0
    :goto_0
    return v0

    .line 2699233
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699234
    goto :goto_0

    .line 2699235
    :cond_3
    check-cast p1, LX/JUX;

    .line 2699236
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699237
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699238
    if-eq v2, v3, :cond_0

    .line 2699239
    iget-object v2, p0, LX/JUX;->a:LX/3Ab;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JUX;->a:LX/3Ab;

    iget-object v3, p1, LX/JUX;->a:LX/3Ab;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699240
    goto :goto_0

    .line 2699241
    :cond_4
    iget-object v2, p1, LX/JUX;->a:LX/3Ab;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
