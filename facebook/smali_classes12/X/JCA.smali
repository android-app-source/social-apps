.class public LX/JCA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/JCA;


# instance fields
.field private final a:LX/J90;


# direct methods
.method public constructor <init>(LX/J90;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2661629
    iput-object p1, p0, LX/JCA;->a:LX/J90;

    .line 2661630
    return-void
.end method

.method public static a(LX/0QB;)LX/JCA;
    .locals 4

    .prologue
    .line 2661631
    sget-object v0, LX/JCA;->b:LX/JCA;

    if-nez v0, :cond_1

    .line 2661632
    const-class v1, LX/JCA;

    monitor-enter v1

    .line 2661633
    :try_start_0
    sget-object v0, LX/JCA;->b:LX/JCA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2661634
    if-eqz v2, :cond_0

    .line 2661635
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2661636
    new-instance p0, LX/JCA;

    invoke-static {v0}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v3

    check-cast v3, LX/J90;

    invoke-direct {p0, v3}, LX/JCA;-><init>(LX/J90;)V

    .line 2661637
    move-object v0, p0

    .line 2661638
    sput-object v0, LX/JCA;->b:LX/JCA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2661639
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2661640
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2661641
    :cond_1
    sget-object v0, LX/JCA;->b:LX/JCA;

    return-object v0

    .line 2661642
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2661643
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2661644
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2661645
    const-string v0, "update_timeline_app_collection_in_timeline"

    .line 2661646
    iget-object v2, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 2661647
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2661648
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2661649
    const-string v2, "timelineAppCollectionParamsKey"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    .line 2661650
    iget-object v2, p0, LX/JCA;->a:LX/J90;

    .line 2661651
    iget-object p0, v0, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2661652
    invoke-virtual {v2, v0}, LX/J90;->a(Ljava/lang/String;)V

    .line 2661653
    :cond_0
    return-object v1
.end method
