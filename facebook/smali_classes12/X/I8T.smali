.class public LX/I8T;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:Lcom/facebook/events/model/Event;

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2541552
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2541553
    iput-object p1, p0, LX/I8T;->a:Landroid/content/Context;

    .line 2541554
    return-void
.end method

.method public static a(LX/0QB;)LX/I8T;
    .locals 2

    .prologue
    .line 2541549
    new-instance v1, LX/I8T;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/I8T;-><init>(Landroid/content/Context;)V

    .line 2541550
    move-object v0, v1

    .line 2541551
    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2541546
    iget-object v0, p0, LX/I8T;->b:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8T;->b:Lcom/facebook/events/model/Event;

    .line 2541547
    iget-object v1, v0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v1

    .line 2541548
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2541544
    invoke-static {}, LX/I8S;->values()[LX/I8S;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2541545
    iget-object v1, p0, LX/I8T;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget-object v2, LX/I8S;->EVENT_PERMALINK_GAP_VIEW:LX/I8S;

    if-ne v0, v2, :cond_0

    const v0, 0x7f0304f7

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0304e0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2541530
    invoke-static {}, LX/I8S;->values()[LX/I8S;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2541531
    sget-object v1, LX/I8S;->PENDING_POSTS_STATUS:LX/I8S;

    if-ne v0, v1, :cond_0

    .line 2541532
    check-cast p3, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;

    .line 2541533
    iget-object v0, p0, LX/I8T;->b:Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/events/permalink/pendingposts/EventPendingPostsStatusView;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)V

    .line 2541534
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2541541
    iget-boolean v0, p0, LX/I8T;->d:Z

    if-nez v0, :cond_0

    .line 2541542
    const/4 v0, 0x0

    .line 2541543
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/I8T;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2541540
    iget-object v0, p0, LX/I8T;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2541539
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2541536
    if-nez p1, :cond_0

    invoke-direct {p0}, LX/I8T;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2541537
    sget-object v0, LX/I8S;->EVENT_PERMALINK_GAP_VIEW:LX/I8S;

    invoke-virtual {v0}, LX/I8S;->ordinal()I

    move-result v0

    .line 2541538
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/I8S;->PENDING_POSTS_STATUS:LX/I8S;

    invoke-virtual {v0}, LX/I8S;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2541535
    invoke-static {}, LX/I8S;->values()[LX/I8S;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
