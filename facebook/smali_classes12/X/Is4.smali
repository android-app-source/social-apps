.class public final LX/Is4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/StickerListAdapter;I)V
    .locals 0

    .prologue
    .line 2619212
    iput-object p1, p0, LX/Is4;->b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iput p2, p0, LX/Is4;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x570577cd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2619196
    iget-object v0, p0, LX/Is4;->b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->d:LX/IqR;

    if-eqz v0, :cond_0

    iget v0, p0, LX/Is4;->a:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/Is4;->a:I

    iget-object v2, p0, LX/Is4;->b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2619197
    iget-object v0, p0, LX/Is4;->b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->d:LX/IqR;

    iget-object v0, p0, LX/Is4;->b:Lcom/facebook/messaging/photos/editing/StickerListAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    iget v3, p0, LX/Is4;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 2619198
    iget-object v3, v2, LX/IqR;->a:LX/IqX;

    sget-object p0, LX/IqV;->PACKLIST:LX/IqV;

    invoke-virtual {v3, p0}, LX/IqX;->setStateAndVisibilities(LX/IqV;)V

    .line 2619199
    iget-object v3, v2, LX/IqR;->a:LX/IqX;

    .line 2619200
    iget-object p0, v3, LX/IqX;->k:LX/IrK;

    if-eqz p0, :cond_0

    .line 2619201
    iget-object p0, v3, LX/IqX;->k:LX/IrK;

    .line 2619202
    iget-object p1, p0, LX/IrK;->a:LX/IrP;

    .line 2619203
    iget-object v3, p1, LX/IrP;->o:LX/IqH;

    invoke-virtual {v3}, LX/IqH;->d()V

    .line 2619204
    iget-object v3, p1, LX/IrP;->o:LX/IqH;

    invoke-virtual {v3}, LX/IqH;->b()V

    .line 2619205
    new-instance v3, LX/Irx;

    invoke-direct {v3, v0}, LX/Irx;-><init>(Lcom/facebook/stickers/model/Sticker;)V

    .line 2619206
    iget-object p0, p1, LX/IrP;->p:LX/Iqq;

    invoke-virtual {v3, p0}, LX/Iqg;->a(LX/Iqq;)V

    .line 2619207
    iget-object p0, p1, LX/IrP;->q:LX/Ird;

    invoke-virtual {p0, v3}, LX/Ird;->a(LX/Iqg;)V

    .line 2619208
    goto :goto_1

    .line 2619209
    :goto_0
    iget-object v3, p1, LX/IrP;->o:LX/IqH;

    invoke-virtual {v3}, LX/IqH;->a()V

    .line 2619210
    :cond_0
    const v0, -0x5a23cae0    # -3.8199915E-16f

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2619211
    :goto_1
    sget-object v3, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    invoke-static {p1, v3}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    goto :goto_0
.end method
