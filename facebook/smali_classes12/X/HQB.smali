.class public LX/HQB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLPageActionType;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/9Y7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2463078
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sput-object v0, LX/HQB;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2463079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463080
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2463081
    iput-object v0, p0, LX/HQB;->b:LX/0Px;

    return-void
.end method

.method public static a(LX/9Y7;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2463082
    invoke-interface {p0}, LX/9Y7;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/9Y7;->q()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/9Y7;
    .locals 1

    .prologue
    .line 2463083
    iget-object v0, p0, LX/HQB;->b:LX/0Px;

    move-object v0, v0

    .line 2463084
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Y7;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z
    .locals 2

    .prologue
    .line 2463085
    iget-object v0, p0, LX/HQB;->b:LX/0Px;

    move-object v0, v0

    .line 2463086
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Y7;

    .line 2463087
    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 2463088
    const/4 v0, 0x1

    .line 2463089
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2463090
    iget-object v0, p0, LX/HQB;->b:LX/0Px;

    move-object v0, v0

    .line 2463091
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I
    .locals 3

    .prologue
    .line 2463092
    invoke-virtual {p0, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2463093
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Accessing non-existing tab: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2463094
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    .line 2463095
    :goto_0
    iget-object v0, p0, LX/HQB;->b:LX/0Px;

    move-object v0, v0

    .line 2463096
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2463097
    iget-object v0, p0, LX/HQB;->b:LX/0Px;

    move-object v0, v0

    .line 2463098
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Y7;

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 2463099
    :goto_1
    return v1

    .line 2463100
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2463101
    :cond_2
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2463102
    sget-object v0, LX/HQB;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2463103
    sget-object v0, LX/HQB;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2463104
    :goto_0
    return-object v0

    .line 2463105
    :cond_0
    invoke-virtual {p0}, LX/HQB;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 2463106
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    goto :goto_0

    .line 2463107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2463108
    invoke-virtual {p0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    .line 2463109
    invoke-virtual {p0, v0}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2463110
    invoke-virtual {p0, v0}, LX/HQB;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v0

    .line 2463111
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
