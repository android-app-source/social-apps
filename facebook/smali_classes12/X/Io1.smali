.class public final LX/Io1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611233
    iput-object p1, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)V
    .locals 1
    .param p1    # Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2611231
    iget-object v0, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)V

    .line 2611232
    return-void
.end method

.method public final a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V
    .locals 1

    .prologue
    .line 2611218
    iget-object v0, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V

    .line 2611219
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2611229
    iget-object v0, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Ljava/lang/String;)V

    .line 2611230
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2611227
    iget-object v0, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611228
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2611224
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "https://m.facebook.com/help/messenger-app/750020781733477"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2611225
    iget-object v1, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2611226
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2611220
    iget-object v0, p0, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611221
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    sget-object p0, LX/IoB;->SEND_MONEY:LX/IoB;

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(LX/IoB;)V

    .line 2611222
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(Z)V

    .line 2611223
    return-void
.end method
