.class public LX/Ixy;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ixy;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2632643
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2632644
    sget-object v0, LX/0ax;->bb:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PAGES_LAUNCHPOINT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2632645
    return-void
.end method

.method public static a(LX/0QB;)LX/Ixy;
    .locals 3

    .prologue
    .line 2632646
    sget-object v0, LX/Ixy;->a:LX/Ixy;

    if-nez v0, :cond_1

    .line 2632647
    const-class v1, LX/Ixy;

    monitor-enter v1

    .line 2632648
    :try_start_0
    sget-object v0, LX/Ixy;->a:LX/Ixy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2632649
    if-eqz v2, :cond_0

    .line 2632650
    :try_start_1
    new-instance v0, LX/Ixy;

    invoke-direct {v0}, LX/Ixy;-><init>()V

    .line 2632651
    move-object v0, v0

    .line 2632652
    sput-object v0, LX/Ixy;->a:LX/Ixy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2632653
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2632654
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2632655
    :cond_1
    sget-object v0, LX/Ixy;->a:LX/Ixy;

    return-object v0

    .line 2632656
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2632657
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
