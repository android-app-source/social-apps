.class public LX/Hu0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:LX/Hr2;

.field private final f:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public final g:LX/Hsa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/HuQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hr2;LX/Hsa;LX/HuQ;)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/Hsa;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2517022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517023
    iput p1, p0, LX/Hu0;->a:I

    .line 2517024
    iput-object p3, p0, LX/Hu0;->b:Ljava/lang/String;

    .line 2517025
    iput-object p4, p0, LX/Hu0;->c:Ljava/lang/String;

    .line 2517026
    iput-object p5, p0, LX/Hu0;->d:Ljava/lang/String;

    .line 2517027
    iput-object p6, p0, LX/Hu0;->e:LX/Hr2;

    .line 2517028
    iput p2, p0, LX/Hu0;->f:I

    .line 2517029
    iput-object p7, p0, LX/Hu0;->g:LX/Hsa;

    .line 2517030
    iput-object p8, p0, LX/Hu0;->h:LX/HuQ;

    .line 2517031
    return-void
.end method

.method public synthetic constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hr2;LX/Hsa;LX/HuQ;B)V
    .locals 0

    .prologue
    .line 2517032
    invoke-direct/range {p0 .. p8}, LX/Hu0;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hr2;LX/Hsa;LX/HuQ;)V

    return-void
.end method

.method public static newBuilder()LX/Htz;
    .locals 2

    .prologue
    .line 2517033
    new-instance v0, LX/Htz;

    invoke-direct {v0}, LX/Htz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final f()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517034
    iget v0, p0, LX/Hu0;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
