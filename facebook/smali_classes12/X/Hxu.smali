.class public final LX/Hxu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/widget/megaphone/Megaphone;

.field public final synthetic b:LX/Hxz;


# direct methods
.method public constructor <init>(LX/Hxz;Lcom/facebook/fbui/widget/megaphone/Megaphone;)V
    .locals 0

    .prologue
    .line 2523370
    iput-object p1, p0, LX/Hxu;->b:LX/Hxz;

    iput-object p2, p0, LX/Hxu;->a:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x71195396

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2523371
    iget-object v1, p0, LX/Hxu;->b:LX/Hxz;

    iget-object v1, v1, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523372
    invoke-static {v1}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->k$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2523373
    iget-object v2, v1, Lcom/facebook/events/dashboard/EventsDashboardFragment;->l:LX/Hx5;

    const/4 v4, 0x0

    .line 2523374
    move v5, v4

    .line 2523375
    :goto_0
    invoke-virtual {v2}, LX/Hx5;->c()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 2523376
    invoke-static {v2, v4}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v6

    sget-object p1, LX/Hx3;->SUBSCRIPTIONS:LX/Hx3;

    if-ne v6, p1, :cond_1

    .line 2523377
    add-int/lit8 v4, v5, 0x1

    .line 2523378
    :goto_1
    move v2, v4

    .line 2523379
    iget-object v4, v1, Lcom/facebook/events/dashboard/EventsDashboardFragment;->C:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4, v2}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    .line 2523380
    :cond_0
    iget-object v1, p0, LX/Hxu;->a:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setVisibility(I)V

    .line 2523381
    iget-object v1, p0, LX/Hxu;->b:LX/Hxz;

    iget-object v1, v1, LX/Hxz;->k:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->a()V

    .line 2523382
    const v1, 0x7963c52e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2523383
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 2523384
    invoke-virtual {v2, v4}, LX/Hx5;->c(I)I

    move-result v6

    add-int/2addr v5, v6

    .line 2523385
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2523386
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method
