.class public final LX/HoS;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HoH;

.field public final synthetic b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;LX/HoH;)V
    .locals 0

    .prologue
    .line 2505718
    iput-object p1, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    iput-object p2, p0, LX/HoS;->a:LX/HoH;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2505719
    iget-object v0, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchElectionHubVideos"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2505720
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2505721
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2505722
    if-eqz p1, :cond_0

    .line 2505723
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505724
    if-nez v0, :cond_1

    .line 2505725
    :cond_0
    :goto_0
    return-void

    .line 2505726
    :cond_1
    iget-object v0, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    .line 2505727
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mRemoving:Z

    move v0, v2

    .line 2505728
    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/0Vd;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2505729
    iget-object v0, p0, LX/HoS;->a:LX/HoH;

    sget-object v2, LX/HoH;->FIRST_FETCH:LX/HoH;

    if-ne v0, v2, :cond_3

    .line 2505730
    iget-object v2, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    .line 2505731
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505732
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    .line 2505733
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 2505734
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2505735
    const-string v4, ""

    .line 2505736
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move-object v5, v4

    move v4, v3

    :goto_1
    if-ge v4, v8, :cond_2

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$TopVideosModel;

    .line 2505737
    invoke-virtual {v3}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$TopVideosModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2505738
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2505739
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2505740
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2505741
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2505742
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_8

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f08370d

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2505743
    :goto_2
    new-instance v6, LX/4Yw;

    invoke-direct {v6}, LX/4Yw;-><init>()V

    new-instance v7, LX/4Yy;

    invoke-direct {v7}, LX/4Yy;-><init>()V

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    .line 2505744
    iput v8, v7, LX/4Yy;->b:I

    .line 2505745
    move-object v7, v7

    .line 2505746
    iput-object v4, v7, LX/4Yy;->c:LX/0Px;

    .line 2505747
    move-object v4, v7

    .line 2505748
    invoke-virtual {v4}, LX/4Yy;->a()Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    move-result-object v4

    .line 2505749
    iput-object v4, v6, LX/4Yw;->c:Lcom/facebook/graphql/model/GraphQLStorySetStoriesConnection;

    .line 2505750
    move-object v4, v6

    .line 2505751
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LIVE_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-static {v6, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 2505752
    iput-object v6, v4, LX/4Yw;->e:LX/0Px;

    .line 2505753
    move-object v4, v4

    .line 2505754
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 2505755
    iput-object v3, v6, LX/173;->f:Ljava/lang/String;

    .line 2505756
    move-object v3, v6

    .line 2505757
    invoke-virtual {v3}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 2505758
    iput-object v3, v4, LX/4Yw;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2505759
    move-object v3, v4

    .line 2505760
    iput-object v5, v3, LX/4Yw;->d:Ljava/lang/String;

    .line 2505761
    move-object v3, v3

    .line 2505762
    invoke-virtual {v3}, LX/4Yw;->a()Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v3

    .line 2505763
    :goto_3
    move-object v0, v3

    .line 2505764
    if-eqz v0, :cond_3

    .line 2505765
    iget-object v2, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    .line 2505766
    iget-object v3, v2, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->p:LX/HoL;

    invoke-virtual {v3, v0}, LX/HoL;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2505767
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505768
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2505769
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505770
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2505771
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505772
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    const/4 v4, 0x3

    invoke-virtual {v2, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2505773
    iput-object v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->d:Ljava/lang/String;

    .line 2505774
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505775
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2505776
    iput-object v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->e:Ljava/lang/String;

    .line 2505777
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505778
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    .line 2505779
    iput-boolean v0, v3, Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;->f:Z

    .line 2505780
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505781
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2505782
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2505783
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2505784
    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel;->j()Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2505785
    :goto_4
    if-ge v1, v4, :cond_6

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel$EdgesModel;

    .line 2505786
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2505787
    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/graphql/ElectionHubGraphQLModels$ElectionHubVideoStoriesQueryModel$VideoStoriesModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2505788
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2505789
    :cond_6
    iget-object v0, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    iget-object v1, p0, LX/HoS;->a:LX/HoH;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->a(Ljava/util/List;LX/HoH;)V

    .line 2505790
    :cond_7
    iget-object v0, p0, LX/HoS;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubVideoFragment;

    invoke-virtual {v0}, Lcom/facebook/civicengagement/elections/ui/ElectionHubFragment;->k()V

    goto/16 :goto_0

    .line 2505791
    :cond_8
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f08370c

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2505792
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_3
.end method
