.class public final enum LX/I8n;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I8n;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I8n;

.field public static final enum EVENT_PERMALINK_GAP_VIEW:LX/I8n;

.field public static final enum TAB_BAR:LX/I8n;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2542016
    new-instance v0, LX/I8n;

    const-string v1, "EVENT_PERMALINK_GAP_VIEW"

    invoke-direct {v0, v1, v2}, LX/I8n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8n;->EVENT_PERMALINK_GAP_VIEW:LX/I8n;

    .line 2542017
    new-instance v0, LX/I8n;

    const-string v1, "TAB_BAR"

    invoke-direct {v0, v1, v3}, LX/I8n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8n;->TAB_BAR:LX/I8n;

    .line 2542018
    const/4 v0, 0x2

    new-array v0, v0, [LX/I8n;

    sget-object v1, LX/I8n;->EVENT_PERMALINK_GAP_VIEW:LX/I8n;

    aput-object v1, v0, v2

    sget-object v1, LX/I8n;->TAB_BAR:LX/I8n;

    aput-object v1, v0, v3

    sput-object v0, LX/I8n;->$VALUES:[LX/I8n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2542019
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I8n;
    .locals 1

    .prologue
    .line 2542020
    const-class v0, LX/I8n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I8n;

    return-object v0
.end method

.method public static values()[LX/I8n;
    .locals 1

    .prologue
    .line 2542021
    sget-object v0, LX/I8n;->$VALUES:[LX/I8n;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I8n;

    return-object v0
.end method
