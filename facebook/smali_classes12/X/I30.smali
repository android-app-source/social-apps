.class public final LX/I30;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I32;


# direct methods
.method public constructor <init>(LX/I32;)V
    .locals 0

    .prologue
    .line 2530934
    iput-object p1, p0, LX/I30;->a:LX/I32;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x7fad3555

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2530917
    iget-object v1, p0, LX/I30;->a:LX/I32;

    iget-object v1, v1, LX/I32;->b:LX/1nQ;

    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-object v3, v3, LX/I32;->p:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_PROMPT_ACTION_BUTTON:Lcom/facebook/events/common/ActionMechanism;

    iget-object v5, p0, LX/I30;->a:LX/I32;

    iget-object v5, v5, LX/I32;->k:Ljava/lang/String;

    .line 2530918
    iget-object v7, v1, LX/1nQ;->i:LX/0Zb;

    const-string v8, "event_prompt_action_button_click"

    const/4 p1, 0x0

    invoke-interface {v7, v8, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 2530919
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2530920
    const-string v8, "event_dashboard"

    invoke-virtual {v7, v8}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    iget-object v8, v1, LX/1nQ;->j:LX/0kv;

    iget-object p1, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v8, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "EventPrompt"

    invoke-virtual {v7, v8}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "ref_module"

    invoke-virtual {v7, v8, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "source_module"

    invoke-virtual {v7, v8, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v7

    const-string v8, "mechanism"

    invoke-virtual {v7, v8, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v7

    invoke-virtual {v7}, LX/0oG;->d()V

    .line 2530921
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/I30;->a:LX/I32;

    invoke-virtual {v2}, LX/I32;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2530922
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->l:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2530923
    const-string v2, "events_creation_prefill_title"

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-object v3, v3, LX/I32;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530924
    :cond_1
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-wide v2, v2, LX/I32;->m:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 2530925
    const-string v2, "events_creation_prefill_start_time"

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-wide v4, v3, LX/I32;->m:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2530926
    :cond_2
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->n:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2530927
    const-string v2, "events_creation_prefill_theme_id"

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-object v3, v3, LX/I32;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530928
    :cond_3
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->o:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2530929
    const-string v2, "events_creation_prefill_theme_url"

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-object v3, v3, LX/I32;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530930
    :cond_4
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->k:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2530931
    const-string v2, "events_creation_prompt_id"

    iget-object v3, p0, LX/I30;->a:LX/I32;

    iget-object v3, v3, LX/I32;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2530932
    :cond_5
    iget-object v2, p0, LX/I30;->a:LX/I32;

    iget-object v2, v2, LX/I32;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/I30;->a:LX/I32;

    invoke-virtual {v3}, LX/I32;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2530933
    const v1, -0x6958c5f4

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
