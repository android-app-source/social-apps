.class public final LX/JDg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARN;


# instance fields
.field public final synthetic c:LX/JDu;


# direct methods
.method public constructor <init>(LX/JDu;)V
    .locals 0

    .prologue
    .line 2664551
    iput-object p1, p0, LX/JDg;->c:LX/JDu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    .line 2664552
    iget-object v0, p0, LX/JDg;->c:LX/JDu;

    .line 2664553
    new-instance v1, LX/JDh;

    invoke-direct {v1, v0}, LX/JDh;-><init>(LX/JDu;)V

    new-instance v2, LX/JDi;

    invoke-direct {v2, v0}, LX/JDi;-><init>(LX/JDu;)V

    .line 2664554
    iget-object v3, v0, LX/AQ9;->b:Landroid/content/Context;

    move-object v3, v3

    .line 2664555
    new-instance v4, LX/AQw;

    invoke-direct {v4, v1, v2, v3}, LX/AQw;-><init>(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;Landroid/content/Context;)V

    .line 2664556
    move-object v1, v4

    .line 2664557
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    .line 2664558
    iget-object v3, v2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v2, v3

    .line 2664559
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->MARRIED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ENGAGED:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    if-ne v2, v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 2664560
    if-eqz v2, :cond_2

    .line 2664561
    invoke-virtual {v0}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    .line 2664562
    iget-object v3, v2, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2664563
    const/4 p0, 0x1

    const/4 v0, 0x0

    .line 2664564
    new-instance v3, LX/31Y;

    iget-object v4, v1, LX/AQw;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v4, v1, LX/AQw;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0812cf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    iget-object v4, v1, LX/AQw;->c:Landroid/content/Context;

    const v5, 0x7f080020

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, LX/AQw;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v3, v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    iget-object v4, v1, LX/AQw;->c:Landroid/content/Context;

    const v5, 0x7f080021

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/AQv;

    invoke-direct {v5, v1}, LX/AQv;-><init>(LX/AQw;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    iget-object v4, v1, LX/AQw;->b:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v3, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v4

    .line 2664565
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2664566
    iget-object v3, v1, LX/AQw;->c:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f030330

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2664567
    const v3, 0x7f0d0aa0

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2664568
    iget-object v6, v1, LX/AQw;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0812d0

    new-array p0, p0, [Ljava/lang/Object;

    aput-object v2, p0, v0

    invoke-virtual {v6, v7, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664569
    invoke-virtual {v4, v5}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 2664570
    :cond_1
    invoke-virtual {v4}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 2664571
    const/4 v1, 0x1

    .line 2664572
    :goto_1
    move v0, v1

    .line 2664573
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
