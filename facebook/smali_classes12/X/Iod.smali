.class public final LX/Iod;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iog;


# direct methods
.method public constructor <init>(LX/Iog;)V
    .locals 0

    .prologue
    .line 2612045
    iput-object p1, p0, LX/Iod;->a:LX/Iog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2612046
    iget-object v0, p0, LX/Iod;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->c:LX/03V;

    const-string v1, "OrionMessengerPayLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch eligibility of the sender to send money to recipient "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Iod;->a:LX/Iog;

    iget-object v3, v3, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612047
    iget-object p1, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v3, p1

    .line 2612048
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612049
    iget-object v0, p0, LX/Iod;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->h:LX/Io3;

    invoke-virtual {v0}, LX/Io3;->a()V

    .line 2612050
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2612051
    check-cast p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    .line 2612052
    iget-object v0, p0, LX/Iod;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Z)V

    .line 2612053
    return-void
.end method
