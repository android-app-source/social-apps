.class public LX/HR7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/HR7;


# instance fields
.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:LX/3fA;

.field public final f:LX/0lC;

.field public g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

.field public h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2464339
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "pages/app/all_pages"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/HR7;->a:LX/0Tn;

    .line 2464340
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "pages/app/all_pages/last_fetch"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/HR7;->b:LX/0Tn;

    .line 2464341
    const-class v0, LX/HR7;

    sput-object v0, LX/HR7;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3fA;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2464342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2464343
    iput-object p1, p0, LX/HR7;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2464344
    iput-object p2, p0, LX/HR7;->e:LX/3fA;

    .line 2464345
    iput-object p3, p0, LX/HR7;->f:LX/0lC;

    .line 2464346
    return-void
.end method

.method public static a(LX/0QB;)LX/HR7;
    .locals 6

    .prologue
    .line 2464347
    sget-object v0, LX/HR7;->i:LX/HR7;

    if-nez v0, :cond_1

    .line 2464348
    const-class v1, LX/HR7;

    monitor-enter v1

    .line 2464349
    :try_start_0
    sget-object v0, LX/HR7;->i:LX/HR7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2464350
    if-eqz v2, :cond_0

    .line 2464351
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2464352
    new-instance p0, LX/HR7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/3fA;->b(LX/0QB;)LX/3fA;

    move-result-object v4

    check-cast v4, LX/3fA;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-direct {p0, v3, v4, v5}, LX/HR7;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3fA;LX/0lC;)V

    .line 2464353
    move-object v0, p0

    .line 2464354
    sput-object v0, LX/HR7;->i:LX/HR7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2464355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2464356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2464357
    :cond_1
    sget-object v0, LX/HR7;->i:LX/HR7;

    return-object v0

    .line 2464358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2464359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464360
    monitor-enter p0

    :try_start_0
    const-wide/16 v5, 0x0

    const/4 v3, 0x0

    .line 2464361
    iget-object v1, p0, LX/HR7;->g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    if-eqz v1, :cond_0

    .line 2464362
    :goto_0
    iget-object v0, p0, LX/HR7;->g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2464363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2464364
    :cond_0
    :try_start_1
    iget-object v1, p0, LX/HR7;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/HR7;->a:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2464365
    iget-object v1, p0, LX/HR7;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/HR7;->b:LX/0Tn;

    invoke-interface {v1, v2, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v1

    iput-wide v1, p0, LX/HR7;->h:J

    .line 2464366
    if-eqz v4, :cond_1

    iget-wide v1, p0, LX/HR7;->h:J

    cmp-long v1, v1, v5

    if-nez v1, :cond_2

    .line 2464367
    :cond_1
    iput-object v3, p0, LX/HR7;->g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    goto :goto_0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2464368
    :cond_2
    :try_start_2
    sget-object v1, LX/461;->g:LX/461;

    invoke-virtual {v1, v4}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v1

    .line 2464369
    iget-object v2, p0, LX/HR7;->f:LX/0lC;

    invoke-virtual {v2, v1}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2464370
    iget-object v2, p0, LX/HR7;->e:LX/3fA;

    invoke-virtual {v2, v1}, LX/3fA;->a(LX/0lF;)Ljava/util/ArrayList;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 2464371
    :goto_1
    new-instance v1, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-wide v5, p0, LX/HR7;->h:J

    invoke-direct/range {v1 .. v6}, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;-><init>(LX/0ta;Ljava/util/ArrayList;Ljava/lang/String;J)V

    iput-object v1, p0, LX/HR7;->g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    goto :goto_0

    .line 2464372
    :catch_0
    move-exception v1

    .line 2464373
    sget-object v2, LX/HR7;->c:Ljava/lang/Class;

    const-string v5, "IOException parsing all pages info"

    invoke-static {v2, v5, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/facebook/ipc/pages/PageInfo;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2464374
    monitor-enter p0

    if-nez p1, :cond_1

    move-object v0, v1

    .line 2464375
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2464376
    :cond_1
    :try_start_0
    invoke-direct {p0}, LX/HR7;->a()Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    move-result-object v0

    .line 2464377
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2464378
    goto :goto_0

    .line 2464379
    :cond_2
    iget-object v2, v0, Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;->a:Ljava/util/ArrayList;

    move-object v3, v2

    .line 2464380
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/pages/PageInfo;

    .line 2464381
    iget-wide v6, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 2464382
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 2464383
    goto :goto_0

    .line 2464384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized clearUserData()V
    .locals 2

    .prologue
    .line 2464385
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/HR7;->g:Lcom/facebook/pages/adminedpages/protocol/FetchAllPagesResult;

    .line 2464386
    iget-object v0, p0, LX/HR7;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/HR7;->a:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/HR7;->b:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2464387
    monitor-exit p0

    return-void

    .line 2464388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
