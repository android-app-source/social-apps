.class public final LX/HU2;
.super LX/1a1;
.source ""


# instance fields
.field public l:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public m:I

.field public n:Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Landroid/widget/TextView;

.field public final synthetic p:LX/HU3;


# direct methods
.method public constructor <init>(LX/HU3;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2471477
    iput-object p1, p0, LX/HU2;->p:LX/HU3;

    .line 2471478
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2471479
    const/4 v0, -0x1

    iput v0, p0, LX/HU2;->m:I

    .line 2471480
    const/4 v0, 0x0

    iput-object v0, p0, LX/HU2;->n:Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    .line 2471481
    const v0, 0x7f0d1230

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HU2;->o:Landroid/widget/TextView;

    .line 2471482
    const v0, 0x7f0d1231

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, LX/HU2;->l:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2471483
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const v5, 0x72607d7b

    const/4 v2, 0x0

    .line 2471484
    iput-object p1, p0, LX/HU2;->n:Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    .line 2471485
    iput p2, p0, LX/HU2;->m:I

    .line 2471486
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 2471487
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2471488
    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_8

    .line 2471489
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471490
    const-class v4, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_b

    .line 2471491
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471492
    const-class v4, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    :goto_5
    if-eqz v1, :cond_d

    .line 2471493
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_6
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471494
    const-class v3, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/HU2;->p:LX/HU3;

    iget-object v2, v2, LX/HU3;->a:Landroid/content/Context;

    .line 2471495
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0170

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v0, v6, p2

    const/4 p2, 0x1

    aput-object v1, v6, p2

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2471496
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    move-object v0, v3

    .line 2471497
    :goto_7
    iget-object v1, p0, LX/HU2;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471498
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2471499
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    .line 2471500
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2471501
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move v2, v3

    .line 2471502
    :goto_8
    invoke-virtual {v0}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 2471503
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;

    move-result-object v1

    .line 2471504
    if-eqz v1, :cond_0

    .line 2471505
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471506
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 2471507
    :cond_1
    new-instance v1, LX/HU5;

    iget-object v2, p0, LX/HU2;->p:LX/HU3;

    iget-object v2, v2, LX/HU3;->a:Landroid/content/Context;

    iget-object v5, p0, LX/HU2;->p:LX/HU3;

    iget-object v5, v5, LX/HU3;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v6, p0, LX/HU2;->p:LX/HU3;

    iget-object v6, v6, LX/HU3;->c:LX/0Uh;

    invoke-direct {v1, v4, v2, v5, v6}, LX/HU5;-><init>(Ljava/util/List;Landroid/content/Context;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;LX/0Uh;)V

    .line 2471508
    iget-object v2, p0, LX/HU2;->l:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2471509
    iget-object v1, p0, LX/HU2;->l:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v2, LX/1P1;

    invoke-direct {v2, v3, v3}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2471510
    :cond_2
    return-void

    .line 2471511
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 2471512
    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_1

    .line 2471513
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    move v0, v2

    .line 2471514
    goto/16 :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_3

    .line 2471515
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_4

    :cond_a
    move v1, v2

    .line 2471516
    goto/16 :goto_5

    :cond_b
    move v1, v2

    goto/16 :goto_5

    .line 2471517
    :cond_c
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_6

    .line 2471518
    :cond_d
    const-string v0, ""

    goto/16 :goto_7
.end method
