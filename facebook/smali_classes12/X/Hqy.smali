.class public final LX/Hqy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Be0;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2510767
    iput-object p1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 2

    .prologue
    .line 2510813
    iget-object v0, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2510814
    iget-object v0, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    iget-object v0, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2510815
    const/4 p1, 0x0

    invoke-static {v0, p1}, LX/8oj;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 2510816
    invoke-interface {v1, v0}, LX/CfL;->a(LX/0Px;)V

    .line 2510817
    iget-object v0, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510818
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Z)V
    .locals 7

    .prologue
    .line 2510768
    iget-object v0, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2510769
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/CfL;->a(Ljava/lang/CharSequence;)V

    .line 2510770
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 v3, 0x0

    .line 2510771
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    if-eqz v2, :cond_5

    move v2, v3

    .line 2510772
    :goto_0
    move v1, v2

    .line 2510773
    if-eqz v1, :cond_2

    .line 2510774
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510775
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->C:LX/AQ0;

    if-nez v2, :cond_0

    .line 2510776
    new-instance v2, LX/AQ0;

    invoke-direct {v2}, LX/AQ0;-><init>()V

    iput-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->C:LX/AQ0;

    .line 2510777
    :cond_0
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->C:LX/AQ0;

    move-object v2, v2

    .line 2510778
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRemovedUrls()LX/0Px;

    move-result-object v1

    .line 2510779
    if-nez p2, :cond_b

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, LX/AQ0;->h:Ljava/util/regex/Pattern;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-interface {v3, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-nez v4, :cond_b

    .line 2510780
    :cond_1
    const/4 v4, 0x0

    .line 2510781
    :goto_1
    move-object v1, v4

    .line 2510782
    if-eqz v1, :cond_2

    .line 2510783
    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;

    .line 2510784
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    .line 2510785
    sget-object v4, LX/0ge;->COMPOSER_ADD_LINK_ATTACHMENT:LX/0ge;

    invoke-static {v4, v3}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/324;->l(Z)LX/324;

    move-result-object v4

    .line 2510786
    iget-object v5, v4, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v4, v5

    .line 2510787
    iget-object v5, v2, LX/0gd;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2510788
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-boolean v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->bb:Z

    if-nez v1, :cond_3

    .line 2510789
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->ad:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 2510790
    sget-object v1, LX/0ge;->COMPOSER_FIRST_CHARACTER_TYPED:LX/0ge;

    invoke-static {v1, v3}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v1

    .line 2510791
    iget-object v6, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "written_time"

    invoke-virtual {v6, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2510792
    move-object v1, v1

    .line 2510793
    iget-object v6, v1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v1, v6

    .line 2510794
    iget-object v6, v2, LX/0gd;->a:LX/0Zb;

    invoke-interface {v6, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2510795
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 v2, 0x1

    .line 2510796
    iput-boolean v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->bb:Z

    .line 2510797
    :cond_3
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v2

    .line 2510798
    if-eqz p2, :cond_4

    .line 2510799
    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v3, v1, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_TEXT_PASTED:LX/0ge;

    iget-object v1, p0, LX/Hqy;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510800
    invoke-static {v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfPastes()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->setNumberOfCopyPastes(I)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfKeystrokes()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->setNumberOfKeystrokes(I)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Ljava/lang/Object;

    .line 2510801
    :goto_2
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2510802
    return-void

    .line 2510803
    :cond_4
    invoke-static {v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->getNumberOfKeystrokes()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->setNumberOfKeystrokes(I)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Ljava/lang/Object;

    goto :goto_2

    .line 2510804
    :cond_5
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    iget-boolean v2, v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->hideAttachment:Z

    if-nez v2, :cond_6

    move v2, v3

    .line 2510805
    goto/16 :goto_0

    .line 2510806
    :cond_6
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v2}, LX/5RE;->I()LX/5RF;

    move-result-object v2

    sget-object v4, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    if-eq v2, v4, :cond_7

    move v2, v3

    .line 2510807
    goto/16 :goto_0

    .line 2510808
    :cond_7
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->USER:LX/2rw;

    if-eq v2, v4, :cond_8

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    if-eq v2, v4, :cond_8

    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->EVENT:LX/2rw;

    if-ne v2, v4, :cond_9

    :cond_8
    move v2, v3

    .line 2510809
    goto/16 :goto_0

    .line 2510810
    :cond_9
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v2}, LX/AQ9;->i()LX/ARN;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 2510811
    iget-object v2, v1, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v2}, LX/AQ9;->i()LX/ARN;

    move-result-object v2

    invoke-interface {v2}, LX/ARN;->a()Z

    move-result v2

    goto/16 :goto_0

    .line 2510812
    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_b
    invoke-static {v2, v3, v1}, LX/AQ0;->a(LX/AQ0;Ljava/lang/CharSequence;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    goto/16 :goto_1
.end method
