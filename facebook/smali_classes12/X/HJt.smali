.class public final LX/HJt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public c:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/HJu;


# direct methods
.method public constructor <init>(LX/HJu;)V
    .locals 1

    .prologue
    .line 2453402
    iput-object p1, p0, LX/HJt;->d:LX/HJu;

    .line 2453403
    move-object v0, p1

    .line 2453404
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2453405
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2453406
    const-string v0, "PageOpenHoursComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2453407
    if-ne p0, p1, :cond_1

    .line 2453408
    :cond_0
    :goto_0
    return v0

    .line 2453409
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2453410
    goto :goto_0

    .line 2453411
    :cond_3
    check-cast p1, LX/HJt;

    .line 2453412
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2453413
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2453414
    if-eq v2, v3, :cond_0

    .line 2453415
    iget-boolean v2, p0, LX/HJt;->a:Z

    iget-boolean v3, p1, LX/HJt;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2453416
    goto :goto_0

    .line 2453417
    :cond_4
    iget-object v2, p0, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2453418
    goto :goto_0

    .line 2453419
    :cond_6
    iget-object v2, p1, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_5

    .line 2453420
    :cond_7
    iget-object v2, p0, LX/HJt;->c:LX/2km;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HJt;->c:LX/2km;

    iget-object v3, p1, LX/HJt;->c:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2453421
    goto :goto_0

    .line 2453422
    :cond_8
    iget-object v2, p1, LX/HJt;->c:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
