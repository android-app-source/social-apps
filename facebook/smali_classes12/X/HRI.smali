.class public final LX/HRI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2464916
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2464917
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464918
    :goto_0
    return v1

    .line 2464919
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464920
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2464921
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2464922
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2464923
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2464924
    const-string v3, "viewer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2464925
    const/4 v2, 0x0

    .line 2464926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2464927
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464928
    :goto_2
    move v0, v2

    .line 2464929
    goto :goto_1

    .line 2464930
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2464931
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2464932
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2464933
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464934
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2464935
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2464936
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2464937
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_5

    if-eqz v4, :cond_5

    .line 2464938
    const-string v5, "message_threads"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2464939
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2464940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_f

    .line 2464941
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464942
    :goto_4
    move v3, v4

    .line 2464943
    goto :goto_3

    .line 2464944
    :cond_6
    const-string v5, "notification_stories"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2464945
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2464946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_14

    .line 2464947
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2464948
    :goto_5
    move v0, v4

    .line 2464949
    goto :goto_3

    .line 2464950
    :cond_7
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2464951
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2464952
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2464953
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v3, v2

    goto :goto_3

    .line 2464954
    :cond_9
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 2464955
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2464956
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2464957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_9

    if-eqz v9, :cond_9

    .line 2464958
    const-string v10, "unread_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2464959
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v8, v6

    move v6, v5

    goto :goto_6

    .line 2464960
    :cond_a
    const-string v10, "unseen_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2464961
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v5

    goto :goto_6

    .line 2464962
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 2464963
    :cond_c
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2464964
    if-eqz v6, :cond_d

    .line 2464965
    invoke-virtual {p1, v4, v8, v4}, LX/186;->a(III)V

    .line 2464966
    :cond_d
    if-eqz v3, :cond_e

    .line 2464967
    invoke-virtual {p1, v5, v7, v4}, LX/186;->a(III)V

    .line 2464968
    :cond_e
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_f
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    goto :goto_6

    .line 2464969
    :cond_10
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_12

    .line 2464970
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2464971
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2464972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_10

    if-eqz v7, :cond_10

    .line 2464973
    const-string v8, "unseen_count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2464974
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v5

    goto :goto_7

    .line 2464975
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_7

    .line 2464976
    :cond_12
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2464977
    if-eqz v0, :cond_13

    .line 2464978
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2464979
    :cond_13
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_5

    :cond_14
    move v0, v4

    move v6, v4

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2464980
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2464981
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2464982
    if-eqz v0, :cond_5

    .line 2464983
    const-string v1, "viewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2464984
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2464985
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2464986
    if-eqz v1, :cond_2

    .line 2464987
    const-string v2, "message_threads"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2464988
    const/4 p3, 0x0

    .line 2464989
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2464990
    invoke-virtual {p0, v1, p3, p3}, LX/15i;->a(III)I

    move-result v2

    .line 2464991
    if-eqz v2, :cond_0

    .line 2464992
    const-string p1, "unread_count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2464993
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2464994
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 2464995
    if-eqz v2, :cond_1

    .line 2464996
    const-string p1, "unseen_count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2464997
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2464998
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2464999
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2465000
    if-eqz v1, :cond_4

    .line 2465001
    const-string v2, "notification_stories"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465002
    const/4 v2, 0x0

    .line 2465003
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2465004
    invoke-virtual {p0, v1, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 2465005
    if-eqz v2, :cond_3

    .line 2465006
    const-string p1, "unseen_count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465007
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 2465008
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2465009
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2465010
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2465011
    return-void
.end method
