.class public final LX/J46;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/J49;


# direct methods
.method public constructor <init>(LX/J49;)V
    .locals 0

    .prologue
    .line 2643623
    iput-object p1, p0, LX/J46;->a:LX/J49;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 2643624
    iget-object v0, p0, LX/J46;->a:LX/J49;

    invoke-static {v0}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-wide v4, v2, LX/J49;->d:J

    sub-long v4, v0, v4

    .line 2643625
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643626
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->p:LX/J5k;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FAILURE:LX/J5h;

    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v1, v2, v6}, LX/J5k;->a(LX/J5h;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2643627
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2643628
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2643629
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2643630
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    .line 2643631
    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v8, p0, LX/J46;->a:LX/J49;

    iget v8, v8, LX/J49;->c:I

    if-lt v2, v8, :cond_2

    .line 2643632
    invoke-virtual {v6, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2643633
    :cond_2
    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->k:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2643634
    iget-object v8, p0, LX/J46;->a:LX/J49;

    iget-object v8, v8, LX/J49;->k:Ljava/util/Map;

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643635
    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2643636
    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2643637
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2643638
    :cond_4
    iget-object v2, v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;->d:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    if-eqz v2, :cond_9

    iget-object v2, v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;->e:LX/5ni;

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2643639
    if-eqz v0, :cond_5

    .line 2643640
    iget-object v0, p0, LX/J46;->a:LX/J49;

    .line 2643641
    iget v2, v0, LX/J49;->f:I

    add-int/lit8 v8, v2, -0x1

    iput v8, v0, LX/J49;->f:I

    .line 2643642
    :goto_3
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2643643
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->k:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2643644
    :cond_5
    iget-object v0, p0, LX/J46;->a:LX/J49;

    .line 2643645
    iget v2, v0, LX/J49;->e:I

    add-int/lit8 v8, v2, -0x1

    iput v8, v0, LX/J49;->e:I

    .line 2643646
    goto :goto_3

    .line 2643647
    :cond_6
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643648
    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2643649
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->p:LX/J5k;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_DROPPED:LX/J5h;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, LX/J5k;->a(LX/J5h;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2643650
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->n:LX/03V;

    const-string v1, "privacy_checkup_mobile_write_retry_fail"

    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->i:Ljava/util/Map;

    .line 2643651
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v4, 0x400

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2643652
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 2643653
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    invoke-virtual {v4}, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2643654
    const-string v4, "|"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 2643655
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 2643656
    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2643657
    :cond_8
    iget-object v0, p0, LX/J46;->a:LX/J49;

    const/4 v1, 0x0

    .line 2643658
    iput-boolean v1, v0, LX/J49;->a:Z

    .line 2643659
    iget-object v0, p0, LX/J46;->a:LX/J49;

    invoke-virtual {v0, v3}, LX/J49;->a(Z)V

    .line 2643660
    return-void

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2643661
    const/4 v8, 0x0

    .line 2643662
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643663
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->p:LX/J5k;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SUCCESS:LX/J5h;

    iget-object v2, p0, LX/J46;->a:LX/J49;

    iget-object v2, v2, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/J46;->a:LX/J49;

    invoke-static {v3}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v3, p0, LX/J46;->a:LX/J49;

    iget-wide v6, v3, LX/J49;->d:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/J5k;->a(LX/J5h;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2643664
    :cond_0
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643665
    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-boolean v0, v0, LX/J49;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/J46;->a:LX/J49;

    iget-object v0, v0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2643666
    iget-object v0, p0, LX/J46;->a:LX/J49;

    .line 2643667
    iput-boolean v8, v0, LX/J49;->a:Z

    .line 2643668
    iget-object v0, p0, LX/J46;->a:LX/J49;

    invoke-virtual {v0, v8}, LX/J49;->a(Z)V

    .line 2643669
    :cond_1
    return-void
.end method
