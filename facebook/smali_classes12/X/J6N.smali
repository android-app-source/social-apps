.class public final LX/J6N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;)V
    .locals 0

    .prologue
    .line 2649612
    iput-object p1, p0, LX/J6N;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2649614
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x8

    if-le v0, p4, :cond_0

    iget-object v0, p0, LX/J6N;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iget-object v1, p0, LX/J6N;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v0, v1}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v0

    iget-boolean v0, v0, LX/J4L;->k:Z

    if-eqz v0, :cond_0

    .line 2649615
    iget-object v0, p0, LX/J6N;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->c()V

    .line 2649616
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2649613
    return-void
.end method
