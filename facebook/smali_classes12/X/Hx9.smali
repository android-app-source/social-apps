.class public LX/Hx9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static b:J

.field private static volatile c:LX/Hx9;


# instance fields
.field private final a:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522046
    iput-object p1, p0, LX/Hx9;->a:LX/0So;

    .line 2522047
    invoke-virtual {p0}, LX/Hx9;->b()V

    .line 2522048
    return-void
.end method

.method public static a(LX/0QB;)LX/Hx9;
    .locals 4

    .prologue
    .line 2522049
    sget-object v0, LX/Hx9;->c:LX/Hx9;

    if-nez v0, :cond_1

    .line 2522050
    const-class v1, LX/Hx9;

    monitor-enter v1

    .line 2522051
    :try_start_0
    sget-object v0, LX/Hx9;->c:LX/Hx9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2522052
    if-eqz v2, :cond_0

    .line 2522053
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2522054
    new-instance p0, LX/Hx9;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/Hx9;-><init>(LX/0So;)V

    .line 2522055
    move-object v0, p0

    .line 2522056
    sput-object v0, LX/Hx9;->c:LX/Hx9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2522057
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2522058
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2522059
    :cond_1
    sget-object v0, LX/Hx9;->c:LX/Hx9;

    return-object v0

    .line 2522060
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2522061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2522062
    iget-object v0, p0, LX/Hx9;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sput-wide v0, LX/Hx9;->b:J

    .line 2522063
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2522064
    iget-object v0, p0, LX/Hx9;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    sub-long/2addr v0, v2

    sput-wide v0, LX/Hx9;->b:J

    .line 2522065
    return-void
.end method

.method public final c()Z
    .locals 4

    .prologue
    .line 2522066
    iget-object v0, p0, LX/Hx9;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sget-wide v2, LX/Hx9;->b:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
